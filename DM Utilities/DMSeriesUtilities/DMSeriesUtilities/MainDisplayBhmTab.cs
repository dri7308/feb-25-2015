﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using BushingConfigurationLite;
using ConfigurationObjects;
using DataObjects;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class MainDisplay 
    {
        //private static string errorInSetOneTestText = "Error In Set 1 Test - Call Dynamic Ratings";
        //private static string errorInSetTwoTestText = "Error In Set 2 Test - Call Dynamic Ratings";
        //private static string flashWriteErrorText = "Flash write error - Call Dynamic Ratings";
        //private static string flashReadErrorText = "Flash read error - Call Dynamic Ratings";
        //private static string noSignalsSetOneText = "No signals for Set 1";
        //private static string noSignalsSetTwoText = "No signals for Set 2";
        //private static string wrongPhaseRotationSetOneText = "Set 1 - Wrong Phase Rotation - Flip two connections";
        //private static string wrongPhaseRotationSetTwoText = "Set 2 - Wrong Phase Rotation - Flip two connections";
        //private static string calibrationErrorText = "Calibration Error - Call Dynamic Ratings";
        //private static string phaseShiftErrorAAText = "Phase shift error A-a";
        //private static string phaseShiftErrorBBText = "Phase shift error B-b";
        //private static string phaseShiftErrorCCText = "Phase shift error C-c";
        //private static string transformerOffText = "Transformer Off";
        //private static string frequencyMismatchText = "Frequency in Configuration does not match measured frequency";
        //private static string inputMagnitudeIsTooLowText = "Input Magnitude is too low - check resistors";
        //private static string inputMagnitudeIsTooHighText = "Input Magnitude is too high - Check resistors";

        //private static string systemInStopModeText = "System in Stop Mode - To Reset Select Resume";
        //private static string systemInPauseModeText = "System in Pause Mode  - To Reset Select Resume";
        //private static string errorRealTimeClockText = "Error real time clock";
       
//        private List<string> GetBhmDeviceHealthStatus(UInt16 deviceHealthBitEncoded)
//        {
//            List<string> statusStrings = new List<string>();
//            try
//            {
//                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(deviceHealthBitEncoded);
//                /// we are using zero-indexing
//                if (bitIsActive[0])
//                {
//                    statusStrings.Add(errorInSetOneTestText);
//                }
//                if (bitIsActive[1])
//                {
//                    statusStrings.Add(errorInSetTwoTestText);
//                }
//                if (bitIsActive[2])
//                {
//                    statusStrings.Add(flashWriteErrorText);
//                }
//                if (bitIsActive[3])
//                {
//                    statusStrings.Add(flashReadErrorText);
//                }
//                if (bitIsActive[4])
//                {
//                    statusStrings.Add(noSignalsSetOneText);
//                }
//                if (bitIsActive[5])
//                {
//                    statusStrings.Add(noSignalsSetTwoText);
//                }
//                if (bitIsActive[6])
//                {
//                    statusStrings.Add(wrongPhaseRotationSetOneText);
//                }
//                if (bitIsActive[7])
//                {
//                    statusStrings.Add(wrongPhaseRotationSetTwoText);
//                }
//                if (bitIsActive[8])
//                {
//                    statusStrings.Add(calibrationErrorText);
//                }
//                if (bitIsActive[9])
//                {
//                    statusStrings.Add(phaseShiftErrorAAText);
//                }
//                if (bitIsActive[10])
//                {
//                    statusStrings.Add(phaseShiftErrorBBText);
//                }
//                if (bitIsActive[11])
//                {
//                    statusStrings.Add(phaseShiftErrorCCText);
//                }
//                if (bitIsActive[12])
//                {
//                    statusStrings.Add(transformerOffText);
//                }
//                if (bitIsActive[13])
//                {
//                    statusStrings.Add(frequencyMismatchText);
//                }
//                if (bitIsActive[14])
//                {
//                    statusStrings.Add(inputMagnitudeIsTooLowText);
//                }
//                if (bitIsActive[15])
//                {
//                    statusStrings.Add(inputMagnitudeIsTooHighText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetDeviceHealthStatus(UInt16)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return statusStrings;
//        }

//        private List<string> GetBhmExtendedDeviceHealthStatus(UInt16 extendedDeviceHealthBitEncoded)
//        {
//            List<string> statusStrings = new List<string>();
//            try
//            {
//                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(extendedDeviceHealthBitEncoded);
//                /// we are using zero-indexing
//                if (bitIsActive[0])
//                {
//                    statusStrings.Add(systemInStopModeText);
//                }
//                if (bitIsActive[1])
//                {
//                    statusStrings.Add(systemInPauseModeText);
//                }
//                if (bitIsActive[2])
//                {
//                    /// Not used at this time
//                }
//                if (bitIsActive[3])
//                {
//                    /// Not used at this time
//                }
//                if (bitIsActive[4])
//                {
//                    statusStrings.Add(errorRealTimeClockText);
//                }                
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetDeviceHealthStatus(UInt16)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return statusStrings;
//        }


//        private string GetBhmEventCodeAsString(int eventCode)
//        {
//            string eventAsString = string.Empty;
//            try
//            {
//                switch (eventCode)
//                {
//                    case 1:
//                        eventAsString = "Data memory cleared";
//                        break;
//                    case 2:
//                        eventAsString = "Data and balancing info cleared";
//                        break;
//                    case 3:
//                        eventAsString = "Gamma Red Alarm";
//                        break;
//                    case 4:
//                        eventAsString = "Gamma Trend Alarm";
//                        break;
//                    case 5:
//                        eventAsString = "Power turned on";
//                        break;
//                    case 6:
//                        eventAsString = "Flash Write Failure";
//                        break;
//                    case 7:
//                        eventAsString = "Flash Read Failure";
//                        break;
//                    case 8:
//                        eventAsString = "Phase Shift Set 1";
//                        break;
//                    case 9:
//                        eventAsString = "Phase Shift Set 2";
//                        break;
//                    case 10:
//                        eventAsString = "Channels?";
//                        break;
//                    case 11:
//                        eventAsString = "Wrong Frequency";
//                        break;
//                    case 12:
//                        eventAsString = "Low Signal";
//                        break;
//                    case 13:
//                        eventAsString = "High Signal";
//                        break;
//                    case 14:
//                        eventAsString = "Unit Off";
//                        break;
//                    case 15:
//                        eventAsString = "Trend Alarm";
//                        break;
//                    case 16:
//                        eventAsString = "TK Alarm";
//                        break;
//                    case 17:
//                        eventAsString = "Set 1 Off";
//                        break;
//                    case 18:
//                        eventAsString = "Set 2 Off";
//                        break;
//                    case 19:
//                        eventAsString = "System Balanced";
//                        break;
//                    case 20:
//                        eventAsString = "Gamma Yellow Alarm";
//                        break;
//                    case 21:
//                        eventAsString = "Log was Cleared";
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetEventCodeAsString(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return eventAsString;
//        }

//        private List<string> GetBhmEquipmentErrors(UInt16 equipmentErrorBitEncoded)
//        {
//            List<string> errorStrings = new List<string>();
//            try
//            {
//                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(equipmentErrorBitEncoded);

//                if (bitIsActive[0])
//                {
//                    errorStrings.Add("Yellow Alarm");
//                }
//                else if (bitIsActive[1])
//                {
//                    errorStrings.Add("Red Alarm");
//                }
//                else
//                {
//                    errorStrings.Add("No alarms, everything is normal");
//                }

//                if (bitIsActive[8])
//                {
//                    errorStrings.Add("Set 1 BC Phase Swap has occurred");
//                }

//                if (bitIsActive[9])
//                {
//                    errorStrings.Add("Set 1 has high initial balance > 0.5%");
//                }

//                if (bitIsActive[10])
//                {
//                    errorStrings.Add("Set 2 BC Phase Swap has occurred");
//                }

//                if (bitIsActive[11])
//                {
//                    errorStrings.Add("Set 2 has high initial balance > 0.5%");
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetEquipmentErrors(UInt16)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return errorStrings;
//        }


        private void LoadBushingData(int modBusAddress, bool readArchivedData)
        {
            try
            {
                //int deviceIsBusy;
                //ErrorCode errorCode;
              //ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                //Dictionary<int, bool> channelIsActive = null;
                //Dictionary<int, double> channelPdiValue = null;
                //Dictionary<int, double> channelSensitivity = null;

                //errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                //if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                //{
                //    deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                //    if (deviceIsBusy == 0)
                //    {
                //        if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                //        {
                using (DMSeriesUtilities.LoadPhaseResolvedData loadData = new LoadPhaseResolvedData(15002, modBusAddress, readArchivedData, false, this.numberOfNonInteractiveRetries, this.totalRetries))
                {
                    loadData.ShowDialog();
                    loadData.Hide();

                    if (!loadData.Cancelled)
                    {
                        if (readArchivedData && (loadData.BhmConfiguration != null) && (loadData.BhmSingleDataReading != null))
                        {
                            AddDataToBhmLatestDataReadingsGrid(loadData.BhmSingleDataReading);
                            AddDataToBhmInitialDataGrid(loadData.BhmConfiguration);
                        }
                        else if ((loadData.BhmConfiguration != null) && (loadData.BhmRegisterDataReadings != null))
                        {
                            AddDataToBhmLatestDataReadingsGrid(loadData.BhmRegisterDataReadings);
                            AddDataToBhmInitialDataGrid(loadData.BhmConfiguration);
                        }
                        else
                        {
                            RadMessageBox.Show("Data failed to load");
                        }
                    }
                }
                //            InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                //        }
                //        else
                //        {
                //            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                //        }
                //    }
                //    else if (deviceIsBusy == 1)
                //    {
                //        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                //    }
                //    else
                //    {
                //        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                //    }
                //}
                //else
                //{
                //    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.LoadBushingData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void InitializeLatestBhmDataRadGridView()
        {
            try
            {
                this.bhmLatestDataReadingsRadGridView.Rows.Clear();

                GridViewTextBoxColumn sideNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("SideNumber");
                sideNumberGridViewTextBoxColumn.Name = "SideNumber";
                sideNumberGridViewTextBoxColumn.HeaderText = "Set #";
                sideNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                sideNumberGridViewTextBoxColumn.Width = 35;
                sideNumberGridViewTextBoxColumn.AllowSort = false;
                sideNumberGridViewTextBoxColumn.IsPinned = true;
                sideNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn dateTimeGridViewTextBoxColumn = new GridViewTextBoxColumn("DateTime");
                dateTimeGridViewTextBoxColumn.Name = "DateTime";
                dateTimeGridViewTextBoxColumn.HeaderText = "Date";
                dateTimeGridViewTextBoxColumn.DisableHTMLRendering = false;
                dateTimeGridViewTextBoxColumn.Width = 125;
                dateTimeGridViewTextBoxColumn.AllowSort = false;
                dateTimeGridViewTextBoxColumn.IsPinned = true;
                dateTimeGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn gammaGridViewTextBoxColumn = new GridViewTextBoxColumn("Gamma");
                gammaGridViewTextBoxColumn.Name = "Gamma";
                gammaGridViewTextBoxColumn.HeaderText = "Gamma";
                gammaGridViewTextBoxColumn.DisableHTMLRendering = false;
                gammaGridViewTextBoxColumn.Width = 60;
                gammaGridViewTextBoxColumn.AllowSort = false;
                //gammaGridViewTextBoxColumn.IsPinned = true;
                gammaGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn gammaMvGridViewTextBoxColumn = new GridViewTextBoxColumn("GammaPhase");
                gammaMvGridViewTextBoxColumn.Name = "GammaPhase";
                gammaMvGridViewTextBoxColumn.HeaderText = "<html>Gamma<br>Phase</html>";
                gammaMvGridViewTextBoxColumn.DisableHTMLRendering = false;
                gammaMvGridViewTextBoxColumn.Width = 80;
                gammaMvGridViewTextBoxColumn.AllowSort = false;
                //gammaMvGridViewTextBoxColumn.IsPinned = true;
                gammaMvGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn gammaNcGridViewTextBoxColumn = new GridViewTextBoxColumn("Humidity");
                gammaNcGridViewTextBoxColumn.Name = "Humidity";
                gammaNcGridViewTextBoxColumn.HeaderText = "Humidity (%)";
                gammaNcGridViewTextBoxColumn.DisableHTMLRendering = false;
                gammaNcGridViewTextBoxColumn.Width = 80;
                gammaNcGridViewTextBoxColumn.AllowSort = false;
                //gammaNcGridViewTextBoxColumn.IsPinned = true;
                gammaNcGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn pulsesPerSecondGridViewTextBoxColumn = new GridViewTextBoxColumn("Temp");
                pulsesPerSecondGridViewTextBoxColumn.Name = "Temp";
                pulsesPerSecondGridViewTextBoxColumn.HeaderText = "Temp (ºC)";
                pulsesPerSecondGridViewTextBoxColumn.DisableHTMLRendering = false;
                pulsesPerSecondGridViewTextBoxColumn.Width = 80;
                pulsesPerSecondGridViewTextBoxColumn.AllowSort = false;
                //pulsesPerSecondGridViewTextBoxColumn.IsPinned = true;
                pulsesPerSecondGridViewTextBoxColumn.ReadOnly = true;

                bhmLatestDataReadingsRadGridView.Columns.Add(sideNumberGridViewTextBoxColumn);
                bhmLatestDataReadingsRadGridView.Columns.Add(dateTimeGridViewTextBoxColumn);
                bhmLatestDataReadingsRadGridView.Columns.Add(gammaGridViewTextBoxColumn);
                bhmLatestDataReadingsRadGridView.Columns.Add(gammaMvGridViewTextBoxColumn);
                bhmLatestDataReadingsRadGridView.Columns.Add(gammaNcGridViewTextBoxColumn);
                bhmLatestDataReadingsRadGridView.Columns.Add(pulsesPerSecondGridViewTextBoxColumn);

                bhmLatestDataReadingsRadGridView.AllowColumnReorder = false;
                bhmLatestDataReadingsRadGridView.AllowColumnChooser = false;
                // bhmLatestDataReadingsRadGridView.ShowGroupPanel = false;
                bhmLatestDataReadingsRadGridView.EnableGrouping = false;
                bhmLatestDataReadingsRadGridView.AllowAddNewRow = false;
                bhmLatestDataReadingsRadGridView.AllowDeleteRow = false;
                bhmLatestDataReadingsRadGridView.AutoScroll = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.InitializeLatestBhmDataRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeInitialBhmDataRadGridView()
        {
            try
            {
                this.bhmInitialDataRadGridView.Rows.Clear();

                GridViewTextBoxColumn sideNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("SideNumber");
                sideNumberGridViewTextBoxColumn.Name = "SideNumber";
                sideNumberGridViewTextBoxColumn.HeaderText = "Set #";
                sideNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                sideNumberGridViewTextBoxColumn.Width = 35;
                sideNumberGridViewTextBoxColumn.AllowSort = false;
                sideNumberGridViewTextBoxColumn.IsPinned = true;
                sideNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn dateTimeGridViewTextBoxColumn = new GridViewTextBoxColumn("DateTime");
                dateTimeGridViewTextBoxColumn.Name = "DateTime";
                dateTimeGridViewTextBoxColumn.HeaderText = "Date";
                dateTimeGridViewTextBoxColumn.DisableHTMLRendering = false;
                dateTimeGridViewTextBoxColumn.Width = 125;
                dateTimeGridViewTextBoxColumn.AllowSort = false;
                dateTimeGridViewTextBoxColumn.IsPinned = true;
                dateTimeGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn gammaGridViewTextBoxColumn = new GridViewTextBoxColumn("Gamma");
                gammaGridViewTextBoxColumn.Name = "Gamma";
                gammaGridViewTextBoxColumn.HeaderText = "Gamma";
                gammaGridViewTextBoxColumn.DisableHTMLRendering = false;
                gammaGridViewTextBoxColumn.Width = 60;
                gammaGridViewTextBoxColumn.AllowSort = false;
                //gammaGridViewTextBoxColumn.IsPinned = true;
                gammaGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn gammaMvGridViewTextBoxColumn = new GridViewTextBoxColumn("GammaPhase");
                gammaMvGridViewTextBoxColumn.Name = "GammaPhase";
                gammaMvGridViewTextBoxColumn.HeaderText = "<html>Gamma<br>Phase</html>";
                gammaMvGridViewTextBoxColumn.DisableHTMLRendering = false;
                gammaMvGridViewTextBoxColumn.Width = 80;
                gammaMvGridViewTextBoxColumn.AllowSort = false;
                //gammaMvGridViewTextBoxColumn.IsPinned = true;
                gammaMvGridViewTextBoxColumn.ReadOnly = true;

                bhmInitialDataRadGridView.Columns.Add(sideNumberGridViewTextBoxColumn);
                bhmInitialDataRadGridView.Columns.Add(dateTimeGridViewTextBoxColumn);
                bhmInitialDataRadGridView.Columns.Add(gammaGridViewTextBoxColumn);
                bhmInitialDataRadGridView.Columns.Add(gammaMvGridViewTextBoxColumn);

                bhmInitialDataRadGridView.AllowColumnReorder = false;
                bhmInitialDataRadGridView.AllowColumnChooser = false;
                // pdmChannelDataRadGridView.ShowGroupPanel = false;
                bhmInitialDataRadGridView.EnableGrouping = false;
                bhmInitialDataRadGridView.AllowAddNewRow = false;
                bhmInitialDataRadGridView.AllowDeleteRow = false;
                bhmInitialDataRadGridView.AutoScroll = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.InitializeInitialBhmDataRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToBhmLatestDataReadingsGrid()
        {
            try
            {
                int i;
                this.bhmLatestDataReadingsRadGridView.Rows.Clear();
                for (i = 0; i < 2; i++)
                {
                    this.bhmLatestDataReadingsRadGridView.Rows.Add((i + 1).ToString(), ConversionMethods.MinimumDateTime().ToString(), (0.0).ToString(), (0.0).ToString(), (0).ToString(), (0).ToString());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.AddEmptyRowsToBhmLatestDataReadingsGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToBhmLatestDataReadingsGrid(BHM_SingleDataReading latestData)
        {
            try
            {
                string setNumber;
                string readingDateTime;
                string gamma;
                string gammaPhase;
                string humidity;
                string temperature;

                this.bhmLatestDataReadingsRadGridView.Rows.Clear();

                readingDateTime = latestData.readingDateTime.ToString();
                humidity = latestData.insulationParameters.Humidity.ToString();

                setNumber = (1).ToString();
                if (latestData.transformerSideParametersSideOne != null)
                {
                    gamma = Math.Round(latestData.transformerSideParametersSideOne.Gamma, 2).ToString();
                    gammaPhase = Math.Round(latestData.transformerSideParametersSideOne.GammaPhase, 2).ToString();
                    temperature = latestData.transformerSideParametersSideOne.Temperature.ToString();
                }
                else
                {
                    gamma = "no value";
                    gammaPhase = "no value";
                    temperature = "no value";
                }

                this.bhmLatestDataReadingsRadGridView.Rows.Add(setNumber, readingDateTime, gamma, gammaPhase, humidity, temperature);

                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[0].Value = setNumber;
                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[1].Value = readingDateTime;
                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[2].Value = gamma;
                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[3].Value = gammaPhase;
                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[4].Value = humidity;
                //this.bhmLatestDataReadingsRadGridView.Rows[0].Cells[5].Value = temperature;

                setNumber = (2).ToString();
                if (latestData.transformerSideParametersSideTwo != null)
                {
                    gamma = Math.Round(latestData.transformerSideParametersSideTwo.Gamma, 2).ToString();
                    gammaPhase = Math.Round(latestData.transformerSideParametersSideTwo.GammaPhase, 2).ToString();
                    temperature = latestData.transformerSideParametersSideTwo.Temperature.ToString();
                }
                else
                {
                    gamma = "no value";
                    gammaPhase = "no value";
                    temperature = "no value";
                }

                this.bhmLatestDataReadingsRadGridView.Rows.Add(setNumber, readingDateTime, gamma, gammaPhase, humidity, temperature);

                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[0].Value = setNumber;
                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[1].Value = readingDateTime;
                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[2].Value = gamma;
                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[3].Value = gammaPhase;
                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[4].Value = humidity;
                //this.bhmLatestDataReadingsRadGridView.Rows[1].Cells[5].Value = temperature;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.AddDataToBhmLatestDataReadingsGrid(BHM_SingleDataReading)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToBhmLatestDataReadingsGrid(List<BHM_RegisterDataForOneSet> latestData)
        {
            try
            {
                string setNumber;
                string readingDateTime;
                string gamma;
                string gammaPhase;
                string humidity;
                string temperature;

                this.bhmLatestDataReadingsRadGridView.Rows.Clear();

                for (int i = 0; i < 2; i++)
                {
                    setNumber = (i + 1).ToString();
                    readingDateTime = latestData[i].readingDateTime.ToString();
                    gamma = Math.Round(latestData[i].gamma, 2).ToString();
                    gammaPhase = Math.Round(latestData[i].gammaPhase, 2).ToString();
                    temperature = latestData[i].temperature.ToString();
                    humidity = latestData[i].humidity.ToString();

                    this.bhmLatestDataReadingsRadGridView.Rows.Add(setNumber, readingDateTime, gamma, gammaPhase, humidity, temperature);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.AddDataToBhmLatestDataReadingsGrid(BHM_SingleDataReading)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToBhmInitialDataGrid()
        {
            try
            {
                int i, j;
                this.bhmLatestDataReadingsRadGridView.Rows.Clear();
                for (i = 0; i < 2; i++)
                {
                    this.bhmInitialDataRadGridView.Rows.Add((i + 1).ToString(), ConversionMethods.MinimumDateTime().ToString(), (0.0).ToString(), (0.0).ToString());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.AddEmptyRowsToBhmInitialDataGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToBhmInitialDataGrid(BHM_Configuration configuration)
        {
            try
            {
                string setNumber;
                string readingDateTime;
                string gamma;
                string gammaPhase;
                //string humidity;
                //string temperature;

                this.bhmInitialDataRadGridView.Rows.Clear();

                readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(configuration.initialParameters.Month, configuration.initialParameters.Day,
                                                                                                 configuration.initialParameters.Year + 2000, configuration.initialParameters.Hour,
                                                                                                 configuration.initialParameters.Min).ToString();

                setNumber = (1).ToString();
                if (configuration.trSideParamSideOne != null)
                {
                    gamma = Math.Round((configuration.trSideParamSideOne.GammaAmplitude / 100.0), 2).ToString();
                    gammaPhase = Math.Round((configuration.trSideParamSideOne.GammaPhase / 100.0), 2).ToString();
                    //}
                    //else
                    //{
                    //    gamma = "no value";
                    //    gammaPhase = "no value";
                    //    //temperature = "no value";
                    //}

                    this.bhmInitialDataRadGridView.Rows.Add(setNumber, readingDateTime, gamma, gammaPhase);
                }

                //this.bhmInitialDataRadGridView.Rows[0].Cells[0].Value = setNumber;
                //this.bhmInitialDataRadGridView.Rows[0].Cells[1].Value = readingDateTime;
                //this.bhmInitialDataRadGridView.Rows[0].Cells[2].Value = gamma;
                //this.bhmInitialDataRadGridView.Rows[0].Cells[3].Value = gammaPhase;

                setNumber = (2).ToString();
                if (configuration.trSideParamSideTwo != null)
                {
                    gamma = Math.Round((configuration.trSideParamSideTwo.GammaAmplitude / 100.0), 2).ToString();
                    gammaPhase = Math.Round((configuration.trSideParamSideTwo.GammaPhase / 100.0), 2).ToString();
                    //}
                    //else
                    //{
                    //    gamma = "no value";
                    //    gammaPhase = "no value";
                    //    //temperature = "no value";
                    //}

                    this.bhmInitialDataRadGridView.Rows.Add(setNumber, readingDateTime, gamma, gammaPhase);
                }

                //this.bhmInitialDataRadGridView.Rows[1].Cells[0].Value = setNumber;
                //this.bhmInitialDataRadGridView.Rows[1].Cells[1].Value = readingDateTime;
                //this.bhmInitialDataRadGridView.Rows[1].Cells[2].Value = gamma;
                //this.bhmInitialDataRadGridView.Rows[1].Cells[3].Value = gammaPhase;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.AddDataToBhmInitialDataGrid(BHM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void BhmDisplayFirmwareVersion(int modBusAddress)
        {
            try
            {
                double firmwareVersion = GetFirmwareVersion(modBusAddress);
                if (firmwareVersion > .2)
                {
                    bhmFirmwareVersionValueRadLabel.Text = Math.Round(firmwareVersion, 2).ToString();
                }
                else
                {
                    RadMessageBox.Show("Failed to get the BHM firmware version");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.BhmDisplayFirmwareVersion(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
