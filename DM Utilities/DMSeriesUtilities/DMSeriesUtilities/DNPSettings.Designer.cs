﻿namespace DMSeriesUtilities
{
    partial class DNPSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DNPSettings));
            this.readValuesRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.dnpChannelSelectRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.ethernetRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.serialRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.dnpDestinationAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dnpSourceAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.destinationAddressRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.sourceAddressRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            ((System.ComponentModel.ISupportInitialize)(this.readValuesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpChannelSelectRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpDestinationAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpSourceAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationAddressRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceAddressRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // readValuesRadButton
            // 
            this.readValuesRadButton.Location = new System.Drawing.Point(12, 132);
            this.readValuesRadButton.Name = "readValuesRadButton";
            this.readValuesRadButton.Size = new System.Drawing.Size(90, 35);
            this.readValuesRadButton.TabIndex = 4;
            this.readValuesRadButton.Text = "Read Values";
            this.readValuesRadButton.ThemeName = "Office2007Black";
            this.readValuesRadButton.Click += new System.EventHandler(this.readValuesRadButton_Click);
            // 
            // saveChangesRadButton
            // 
            this.saveChangesRadButton.Location = new System.Drawing.Point(108, 132);
            this.saveChangesRadButton.Name = "saveChangesRadButton";
            this.saveChangesRadButton.Size = new System.Drawing.Size(90, 35);
            this.saveChangesRadButton.TabIndex = 5;
            this.saveChangesRadButton.Text = "Save Changes";
            this.saveChangesRadButton.ThemeName = "Office2007Black";
            this.saveChangesRadButton.Click += new System.EventHandler(this.saveChangesRadButton_Click);
            // 
            // cancelChangesRadButton
            // 
            this.cancelChangesRadButton.Location = new System.Drawing.Point(204, 132);
            this.cancelChangesRadButton.Name = "cancelChangesRadButton";
            this.cancelChangesRadButton.Size = new System.Drawing.Size(90, 35);
            this.cancelChangesRadButton.TabIndex = 5;
            this.cancelChangesRadButton.Text = "Exit";
            this.cancelChangesRadButton.ThemeName = "Office2007Black";
            this.cancelChangesRadButton.Click += new System.EventHandler(this.cancelChangesRadButton_Click);
            // 
            // dnpChannelSelectRadLabel
            // 
            this.dnpChannelSelectRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnpChannelSelectRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dnpChannelSelectRadLabel.Location = new System.Drawing.Point(46, 26);
            this.dnpChannelSelectRadLabel.Name = "dnpChannelSelectRadLabel";
            this.dnpChannelSelectRadLabel.Size = new System.Drawing.Size(110, 16);
            this.dnpChannelSelectRadLabel.TabIndex = 7;
            this.dnpChannelSelectRadLabel.Text = "DNP Channel Select";
            this.dnpChannelSelectRadLabel.ThemeName = "Office2007Black";
            // 
            // ethernetRadRadioButton
            // 
            this.ethernetRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ethernetRadRadioButton.Location = new System.Drawing.Point(198, 12);
            this.ethernetRadRadioButton.Name = "ethernetRadRadioButton";
            this.ethernetRadRadioButton.Size = new System.Drawing.Size(70, 18);
            this.ethernetRadRadioButton.TabIndex = 10;
            this.ethernetRadRadioButton.Text = "Ethernet";
            // 
            // serialRadRadioButton
            // 
            this.serialRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serialRadRadioButton.Location = new System.Drawing.Point(198, 36);
            this.serialRadRadioButton.Name = "serialRadRadioButton";
            this.serialRadRadioButton.Size = new System.Drawing.Size(70, 18);
            this.serialRadRadioButton.TabIndex = 11;
            this.serialRadRadioButton.Text = "RS 485";
            // 
            // dnpDestinationAddressRadLabel
            // 
            this.dnpDestinationAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnpDestinationAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dnpDestinationAddressRadLabel.Location = new System.Drawing.Point(46, 68);
            this.dnpDestinationAddressRadLabel.Name = "dnpDestinationAddressRadLabel";
            this.dnpDestinationAddressRadLabel.Size = new System.Drawing.Size(134, 16);
            this.dnpDestinationAddressRadLabel.TabIndex = 12;
            this.dnpDestinationAddressRadLabel.Text = "DNP Destination Address";
            this.dnpDestinationAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // dnpSourceAddressRadLabel
            // 
            this.dnpSourceAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dnpSourceAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dnpSourceAddressRadLabel.Location = new System.Drawing.Point(46, 99);
            this.dnpSourceAddressRadLabel.Name = "dnpSourceAddressRadLabel";
            this.dnpSourceAddressRadLabel.Size = new System.Drawing.Size(114, 16);
            this.dnpSourceAddressRadLabel.TabIndex = 8;
            this.dnpSourceAddressRadLabel.Text = "DNP Source Address";
            this.dnpSourceAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // destinationAddressRadSpinEditor
            // 
            this.destinationAddressRadSpinEditor.InterceptArrowKeys = false;
            this.destinationAddressRadSpinEditor.Location = new System.Drawing.Point(198, 64);
            this.destinationAddressRadSpinEditor.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.destinationAddressRadSpinEditor.Name = "destinationAddressRadSpinEditor";
            // 
            // 
            // 
            this.destinationAddressRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.destinationAddressRadSpinEditor.ShowBorder = true;
            this.destinationAddressRadSpinEditor.Size = new System.Drawing.Size(70, 20);
            this.destinationAddressRadSpinEditor.TabIndex = 34;
            this.destinationAddressRadSpinEditor.TabStop = false;
            // 
            // sourceAddressRadSpinEditor
            // 
            this.sourceAddressRadSpinEditor.InterceptArrowKeys = false;
            this.sourceAddressRadSpinEditor.Location = new System.Drawing.Point(198, 95);
            this.sourceAddressRadSpinEditor.Maximum = new decimal(new int[] {
            65519,
            0,
            0,
            0});
            this.sourceAddressRadSpinEditor.Name = "sourceAddressRadSpinEditor";
            // 
            // 
            // 
            this.sourceAddressRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.sourceAddressRadSpinEditor.ShowBorder = true;
            this.sourceAddressRadSpinEditor.Size = new System.Drawing.Size(70, 20);
            this.sourceAddressRadSpinEditor.TabIndex = 35;
            this.sourceAddressRadSpinEditor.TabStop = false;
            // 
            // DNPSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(305, 180);
            this.Controls.Add(this.sourceAddressRadSpinEditor);
            this.Controls.Add(this.destinationAddressRadSpinEditor);
            this.Controls.Add(this.dnpSourceAddressRadLabel);
            this.Controls.Add(this.dnpDestinationAddressRadLabel);
            this.Controls.Add(this.serialRadRadioButton);
            this.Controls.Add(this.ethernetRadRadioButton);
            this.Controls.Add(this.dnpChannelSelectRadLabel);
            this.Controls.Add(this.cancelChangesRadButton);
            this.Controls.Add(this.saveChangesRadButton);
            this.Controls.Add(this.readValuesRadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DNPSettings";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DNP Settings";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.DNPSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.readValuesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpChannelSelectRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpDestinationAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dnpSourceAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationAddressRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceAddressRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton readValuesRadButton;
        private Telerik.WinControls.UI.RadButton saveChangesRadButton;
        private Telerik.WinControls.UI.RadButton cancelChangesRadButton;
        private Telerik.WinControls.UI.RadLabel dnpChannelSelectRadLabel;
        private Telerik.WinControls.UI.RadRadioButton ethernetRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton serialRadRadioButton;
        private Telerik.WinControls.UI.RadLabel dnpDestinationAddressRadLabel;
        private Telerik.WinControls.UI.RadLabel dnpSourceAddressRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor destinationAddressRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor sourceAddressRadSpinEditor;
    }
}
