﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DMSeriesUtilities
{
    public class PDM_RegisterGeneralData
    {
        public DateTime readingDateTime;
        public UInt16 alarmState;
        public UInt16 alarmSource;
        public UInt16 registeredChannels;
        public int matrix;
        public int temperature;
        public int humidity;

        public PDM_RegisterGeneralData()
        {

        }

        public void SetDataValues(UInt16[] registerValues)
        {
            try
            {
                if ((registerValues != null) && (registerValues.Length >= 11))
                {
                    readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);
                    if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                    {
                        alarmState = registerValues[5];
                        alarmSource = registerValues[6];
                        registeredChannels = registerValues[7];
                        matrix = registerValues[8];
                        temperature = registerValues[9] - 70;
                        humidity = registerValues[10];
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_RegisterGeneralData.SetDataValues(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
