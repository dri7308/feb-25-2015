﻿namespace DMSeriesUtilities
{
    partial class frmBushingCalculator1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject2 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBushingCalculator1));
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView3 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(256, 248);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 24);
            this.radButton3.TabIndex = 34;
            this.radButton3.Text = "Calculate";
            this.radButton3.ThemeName = "Office2007Black";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(526, 12);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 24);
            this.radButton1.TabIndex = 33;
            this.radButton1.Text = "Windows Calculator";
            this.radButton1.ThemeName = "Office2007Black";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(376, 19);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(100, 20);
            this.radTextBox2.TabIndex = 27;
            this.radTextBox2.TabStop = false;
            this.radTextBox2.Text = " 60";
            // 
            // radLabel12
            // 
            this.radLabel12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel12.Location = new System.Drawing.Point(543, 51);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(115, 18);
            this.radLabel12.TabIndex = 32;
            this.radLabel12.Text = "Impedance Calculator";
            // 
            // radLabel9
            // 
            this.radLabel9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel9.Location = new System.Drawing.Point(64, 51);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(78, 18);
            this.radLabel9.TabIndex = 31;
            this.radLabel9.Text = "Balance Check";
            // 
            // radLabel4
            // 
            this.radLabel4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel4.Location = new System.Drawing.Point(182, 269);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(25, 18);
            this.radLabel4.TabIndex = 26;
            this.radLabel4.Text = "Min";
            // 
            // radLabel5
            // 
            this.radLabel5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel5.Location = new System.Drawing.Point(182, 248);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(27, 18);
            this.radLabel5.TabIndex = 25;
            this.radLabel5.Text = "Max";
            // 
            // radLabel6
            // 
            this.radLabel6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel6.Location = new System.Drawing.Point(64, 290);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(72, 18);
            this.radLabel6.TabIndex = 21;
            this.radLabel6.Text = "% Difference:";
            // 
            // radLabel7
            // 
            this.radLabel7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel7.Location = new System.Drawing.Point(64, 269);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(56, 18);
            this.radLabel7.TabIndex = 20;
            this.radLabel7.Text = "Min Volts:";
            // 
            // radLabel8
            // 
            this.radLabel8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel8.Location = new System.Drawing.Point(64, 248);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(58, 18);
            this.radLabel8.TabIndex = 22;
            this.radLabel8.Text = "Max Volts:";
            // 
            // radLabel3
            // 
            this.radLabel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radLabel3.Location = new System.Drawing.Point(182, 290);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(66, 18);
            this.radLabel3.TabIndex = 30;
            this.radLabel3.Text = "Will Balance";
            // 
            // radLabel2
            // 
            this.radLabel2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel2.Location = new System.Drawing.Point(288, 19);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(79, 18);
            this.radLabel2.TabIndex = 24;
            this.radLabel2.Text = "Frequency (hz)";
            // 
            // radGridView2
            // 
            this.radGridView2.Location = new System.Drawing.Point(543, 72);
            // 
            // radGridView2
            // 
            this.radGridView2.MasterTemplate.AllowAddNewRow = false;
            this.radGridView2.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnChooser = false;
            this.radGridView2.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnReorder = false;
            this.radGridView2.MasterTemplate.AllowColumnResize = false;
            this.radGridView2.MasterTemplate.AllowDeleteRow = false;
            this.radGridView2.MasterTemplate.AllowDragToGroup = false;
            this.radGridView2.MasterTemplate.AllowEditRow = false;
            this.radGridView2.MasterTemplate.AllowRowResize = false;
            this.radGridView2.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.HeaderText = "Phase";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn2.HeaderText = "Desired R";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.HeaderText = "Required Parallel R";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn3.WrapText = true;
            this.radGridView2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView2.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView2.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView2.Size = new System.Drawing.Size(245, 154);
            this.radGridView2.TabIndex = 29;
            this.radGridView2.Text = "radGridView2";
            this.radGridView2.ThemeName = "Office2007Black";
            // 
            // radGridView1
            // 
            this.radGridView1.Location = new System.Drawing.Point(64, 72);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView1.MasterTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterTemplate.AllowColumnResize = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn4.HeaderText = "Phase";
            gridViewTextBoxColumn4.Name = "column1";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn5.HeaderText = "C1 (pF)";
            gridViewTextBoxColumn5.Name = "column2";
            gridViewTextBoxColumn6.HeaderText = "Burden  R (Ohms)";
            gridViewTextBoxColumn6.Name = "column3";
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.HeaderText = "Current (ma)";
            gridViewTextBoxColumn7.Name = "column4";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.HeaderText = "Voltage (V)";
            gridViewTextBoxColumn8.Name = "column5";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.HeaderText = "Range Check";
            gridViewTextBoxColumn9.Name = "column6";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 80;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.radGridView1.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.Size = new System.Drawing.Size(467, 154);
            this.radGridView1.TabIndex = 28;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.ThemeName = "Office2007Black";
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(182, 17);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(100, 20);
            this.radTextBox1.TabIndex = 23;
            this.radTextBox1.TabStop = false;
            this.radTextBox1.Text = "500";
            // 
            // radLabel1
            // 
            this.radLabel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Location = new System.Drawing.Point(109, 19);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "Voltage (kV)";
            // 
            // radGridView3
            // 
            this.radGridView3.Location = new System.Drawing.Point(514, 290);
            // 
            // radGridView3
            // 
            this.radGridView3.MasterTemplate.AllowAddNewRow = false;
            this.radGridView3.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView3.MasterTemplate.AllowColumnChooser = false;
            this.radGridView3.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView3.MasterTemplate.AllowColumnReorder = false;
            this.radGridView3.MasterTemplate.AllowColumnResize = false;
            this.radGridView3.MasterTemplate.AllowDeleteRow = false;
            this.radGridView3.MasterTemplate.AllowDragToGroup = false;
            this.radGridView3.MasterTemplate.AllowEditRow = false;
            this.radGridView3.MasterTemplate.AllowRowResize = false;
            this.radGridView3.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn10.HeaderText = "Current (ma)";
            gridViewTextBoxColumn10.Name = "column1";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 70;
            gridViewTextBoxColumn10.WrapText = true;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.Yellow;
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.ConditionType = Telerik.WinControls.UI.ConditionTypes.Between;
            conditionalFormattingObject1.Name = "condition12";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.TValue1 = "0.23";
            conditionalFormattingObject1.TValue2 = "3.41";
            gridViewTextBoxColumn11.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn11.HeaderText = "33 Ohms";
            gridViewTextBoxColumn11.Name = "column2";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.WrapText = true;
            conditionalFormattingObject2.CellBackColor = System.Drawing.Color.Yellow;
            conditionalFormattingObject2.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.ConditionType = Telerik.WinControls.UI.ConditionTypes.Between;
            conditionalFormattingObject2.Name = "condition11";
            conditionalFormattingObject2.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject2.TValue1 = "0.23";
            conditionalFormattingObject2.TValue2 = "3.41";
            gridViewTextBoxColumn12.ConditionalFormattingObjectList.Add(conditionalFormattingObject2);
            gridViewTextBoxColumn12.HeaderText = "50 Ohms";
            gridViewTextBoxColumn12.Name = "column3";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.WrapText = true;
            gridViewTextBoxColumn13.HeaderText = "100 ohms";
            gridViewTextBoxColumn13.Name = "column4";
            gridViewTextBoxColumn13.WrapText = true;
            this.radGridView3.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13});
            this.radGridView3.Name = "radGridView3";
            this.radGridView3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView3.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView3.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView3.Size = new System.Drawing.Size(274, 211);
            this.radGridView3.TabIndex = 30;
            this.radGridView3.Text = "radGridView3";
            this.radGridView3.ThemeName = "Office2007Black";
            // 
            // radLabel10
            // 
            this.radLabel10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel10.Location = new System.Drawing.Point(514, 232);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(224, 18);
            this.radLabel10.TabIndex = 33;
            this.radLabel10.Text = "Volt drop based on Current and Impedance";
            // 
            // radLabel11
            // 
            this.radLabel11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel11.Location = new System.Drawing.Point(514, 248);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(204, 18);
            this.radLabel11.TabIndex = 34;
            this.radLabel11.Text = "Best Range is between 0.5 and 3.3 Volts";
            // 
            // radLabel13
            // 
            this.radLabel13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel13.Location = new System.Drawing.Point(514, 266);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(264, 18);
            this.radLabel13.TabIndex = 35;
            this.radLabel13.Text = "But should balance within yellow highlighted values";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(361, 310);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 182);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // radLabel14
            // 
            this.radLabel14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel14.Location = new System.Drawing.Point(361, 286);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(48, 18);
            this.radLabel14.TabIndex = 36;
            this.radLabel14.Text = "Jumpers";
            // 
            // frmBushingCalculator1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(819, 518);
            this.Controls.Add(this.radLabel14);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radLabel13);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.radGridView3);
            this.Controls.Add(this.radButton3);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radTextBox2);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radGridView2);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.radLabel1);
            this.Name = "frmBushingCalculator1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "frmBushingCalculator1";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.frmBushingCalculator1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadGridView radGridView3;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel radLabel14;
    }
}
