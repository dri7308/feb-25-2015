﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using BushingConfigurationLite;
using DatabaseFileInteraction;
using DatabaseInterface;
using GeneralUtilities;
using MainConfigurationLite;
using MonitorInterface;
using PDMConfigurationLite;
using DeviceHealthAndError;

namespace DMSeriesUtilities
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        private string executablePath;
        private string applicationDataPath;
        private string errorLogFileName = "error_log.txt";

        int numberOfNonInteractiveRetries = 1;
        int totalRetries = 20;

        private string templateDbConnectionStringFileName = "TemplateDbConnectionString.txt";

        private string templateDbConnectionString = string.Empty;

        private bool databaseConnectionStringIsCorrect = false;

        public MainDisplay()
        {
            InitializeComponent();
        }

        private void MainDisplay_Load(object sender, EventArgs e)
        {
            try
            {
                RadMessageBox.ThemeName = "Office2007Black";
                ErrorCode errorCode;

                modBusAddressBhmTabRadMaskedEditBox.Text = "10";
                modBusAddressPdmTabRadMaskedEditBox.Text = "20";
                modBusAddressMainTabRadMaskedEditBox.Text = "1";
                modBusAddressReadRegistersTabRadMaskedEditBox.Text = "10";

                executablePath = Path.GetDirectoryName(Application.ExecutablePath);
#if DEBUG
                LogMessage.SetErrorDirectory(executablePath);
                applicationDataPath = executablePath;
#else
                applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                applicationDataPath = System.IO.Path.Combine(applicationDataPath, ProgramStrings.DynamicRatingsDirectoryName);
                applicationDataPath = System.IO.Path.Combine(applicationDataPath, ProgramStrings.DMSeriesUtilitiesDirectoryName);

                FileUtilities.MakeFullPathToDirectory(applicationDataPath);
                LogMessage.SetErrorDirectory(applicationDataPath);

                findRecorsWithPhaseResolvedDataRadButton.Visible = false;
                searchProgressRadLabel.Visible = false;

#endif
                LogMessage.SetErrorFileName(errorLogFileName);

                InitializeRegisterValuesRadGridView();
                InitializeInitialBhmDataRadGridView();
                InitializeLatestBhmDataRadGridView();
                InitializePdmDataRadGridView();
                InitializeMainDynamicsDataRadGridView();
                InitializeMainDynamicsNamesList();

                SetUpMainMenu();

                this.templateDbConnectionString = LoadDatabaseConnectionStringFromTextFile();

                if (this.templateDbConnectionString == string.Empty)
                {
                    //if (RadMessageBox.Show(this, "This appears to be your first time using Athena.\nWould you like to create an initial database?", "No connection string found", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    //{
                    CreateInitialDatabaseAndConnection();
                    //}
                }
                if (DatabaseFileMethods.DatabaseExists(Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName)) != ErrorCode.DatabaseFound)
                {
                    errorCode = DatabaseFileMethods.CopyDatabase(this, Path.Combine(this.executablePath, ProgramStrings.DefaultTemplateDatabaseName), Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName));
                    if (errorCode != ErrorCode.DatabaseCopySucceeded)
                    {
                        RadMessageBox.Show(this, "Failed to copy the template database to the user's application directory\nReported error was " + ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }

                TestDatabaseConnectionString();

                this.Text = "DM Series Utilities - v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                //AddEmptyRowsToBhmInitialDataGrid();
                //AddEmptyRowsToBhmLatestDataReadingsGrid();
                //AddEmptyRowsToPdmDataGrid();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainDisplay_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        private string LoadDatabaseConnectionStringFromTextFile()
        {
            string connectionString = string.Empty;
            try
            {
                string file = Path.Combine(this.applicationDataPath, this.templateDbConnectionStringFileName);

                CreateApplicationDataPath();

                if (File.Exists(file))
                {
                    using (StreamReader reader = new StreamReader(file))
                    {
                        connectionString = reader.ReadLine();
                        reader.Close();
                    }
                }               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.LoadDatabaseConnectionStringFromTextFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return connectionString;
        }

        public void SaveDatabaseConnectionString()
        {
            try
            {
                if ((this.templateDbConnectionString != null) && (this.templateDbConnectionString != string.Empty))
                {
                    CreateApplicationDataPath();
                    string file = Path.Combine(this.applicationDataPath, this.templateDbConnectionStringFileName);
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        writer.WriteLine(this.templateDbConnectionString);
                        writer.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SaveDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateApplicationDataPath()
        {
            try
            {
                if (!Directory.Exists(this.applicationDataPath))
                {
                    Directory.CreateDirectory(this.applicationDataPath);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateApplicationDataPath()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateInitialDatabaseAndConnection()
        {
            try
            {
                // copy the database to the user's app directory       
                string sourceTemplateDatabaseNameWithFullPath = Path.Combine(this.executablePath, ProgramStrings.DefaultTemplateDatabaseName);
                string templateDatabaseNameWithFullPath = Path.Combine(this.applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName);
                //string userDatabaseLogFilePathWithFileName = Path.Combine(MainDisplay.applicationDataPath, MainDisplay.defaultDatabaseLogfileName);

                if (DatabaseFileMethods.DatabaseExists(sourceTemplateDatabaseNameWithFullPath) == ErrorCode.DatabaseFound)
                {
                    if (DatabaseFileMethods.DatabaseExists(templateDatabaseNameWithFullPath) == ErrorCode.DatabaseNotFound)
                    {
                        if (DatabaseFileMethods.CopyDatabase(this, sourceTemplateDatabaseNameWithFullPath, templateDatabaseNameWithFullPath) != ErrorCode.DatabaseCopySucceeded)
                        {
                            RadMessageBox.Show(this, "Failed to copy the TemplateDB to the user's application directory");
                        }
                    }
                    if (DatabaseFileMethods.DatabaseExists(templateDatabaseNameWithFullPath) == ErrorCode.DatabaseFound)
                    {
                        this.templateDbConnectionString = DatabaseFileMethods.CreateStandardConnectionStringToLocalDatabaseInstance(templateDatabaseNameWithFullPath);
                        SaveDatabaseConnectionString();
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "TemplateDB was missing from the installed program directory\nYou may need to re-install this program");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateInitialDatabaseAndConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetModbusAddress(RadMaskedEditBox modBusRadEditBox)
        {
            int modBusAddress = 0;
            try
            {
                if (!Int32.TryParse(modBusRadEditBox.Text, out modBusAddress))
                {
                    RadMessageBox.Show("Modbus address contains illegal characters.");
                    modBusAddress = -1;
                }
                else if ((modBusAddress < 1) || (modBusAddress > 255))
                {
                    RadMessageBox.Show("Modbus address needs to be between 1 and 255 inclusive");
                    modBusAddress = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetModbusAddress(RadMaskedEditBox)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return modBusAddress;
        }

        //        public static DataDownloadReturnObject DeviceWasBusy(int modBusAddress, ParentWindowInformation parentWindowInformation)
        //        {
        //            DataDownloadReturnObject dataDownloadReturnObject = new DataDownloadReturnObject();
        //            try
        //            {
        //                int deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
        //                if (deviceIsBusy < 0)
        //                {
        //                    dataDownloadReturnObject.errorCode = ErrorCode.DeviceBusyReadFailed;
        //                }
        //                else if (deviceIsBusy == 1)
        //                {
        //                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
        //                    dataDownloadReturnObject.logicalResult = true;
        //                }
        //                else
        //                {
        //                    dataDownloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
        //                    dataDownloadReturnObject.logicalResult = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.DeviceWasBusy(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return dataDownloadReturnObject;
        //        }

        private void SetDeviceDateAndTime(int modBusAddress, bool isMainMonitor)
        {
            try
            {
                if (modBusAddress > 0)
                {
                    using (SetDateAndTime setDate = new SetDateAndTime(modBusAddress, isMainMonitor, this.numberOfNonInteractiveRetries, this.totalRetries))
                    {
                        setDate.ShowDialog();
                        setDate.Hide();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetDeviceTime()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private List<String> ReadEventLogsAndCreateListOfStringsOfValues(int modBusAddress, MonitorType monitorType)
        {
            List<String> eventLogs = new List<string>();
            try
            {
                Int16[] registerValues;
                int month;
                int day;
                int year;
                int hour;
                int minute;
                int eventCode;

                DateTime logDate;
                string eventLogLine;
                int deviceIsBusy;

                int offset = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                ErrorCode errorCode = ErrorCode.None;

                string eventCodeAsString = string.Empty;

                if (modBusAddress > 0)
                {
                    if (monitorType == MonitorType.BHM)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    }
                    else if (monitorType == MonitorType.PDM)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    }
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                if (monitorType == MonitorType.BHM)
                                {
                                    BhmDisplayFirmwareVersion(modBusAddress);
                                }
                                else if (monitorType == MonitorType.PDM)
                                {
                                    PdmDisplayFirmwareVersion(modBusAddress);
                                }
                                registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 451, 120, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if (registerValues != null)
                                {
                                    if (registerValues.Length > 119)
                                    {
                                        eventLogs.Add("Event Date and Time         Event");
                                        for (offset = 0; offset < 115; offset += 6)
                                        {
                                            month = registerValues[offset];
                                            day = registerValues[offset + 1];
                                            year = registerValues[offset + 2];
                                            hour = registerValues[offset + 3];
                                            minute = registerValues[offset + 4];
                                            eventCode = registerValues[offset + 5];

                                            logDate = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);

                                            if (monitorType == MonitorType.BHM)
                                            {
                                                eventCodeAsString = HealthAndErrorMethods.GetBhmEventCodeAsString(eventCode);
                                            }
                                            else if (monitorType == MonitorType.PDM)
                                            {
                                                eventCodeAsString = HealthAndErrorMethods.GetPdmEventCodeAsString(eventCode);
                                            }

                                            eventLogLine = logDate.ToString() + "     " + eventCodeAsString;

                                            eventLogs.Add(eventLogLine);
                                        }
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show("Failed to download the error logs");
                                }
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadEventLogsAndCreateListOfStringsOfValues(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
            return eventLogs;
        }

        private void SetUpMainMenu()
        {
            try
            {
                int mainMenuIndex = 0;

                RadMenuItem databaseRadMenuItem = new RadMenuItem();
                databaseRadMenuItem.Text = "Database";
                mainDisplayRadMenu.Items.Add(databaseRadMenuItem);

                RadMenuItem importTemplateConfigurationsDatabaseRadMenuItem = new RadMenuItem();
                importTemplateConfigurationsDatabaseRadMenuItem.Text = "Import Template Config DB";
                importTemplateConfigurationsDatabaseRadMenuItem.Click += new EventHandler(importTemplateConfigurationsDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(importTemplateConfigurationsDatabaseRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem advancedDatabaseRadMenuItem = new RadMenuItem();
                advancedDatabaseRadMenuItem.Text = "Set database server - Advanced";
                advancedDatabaseRadMenuItem.Click += new EventHandler(advancedDatabaseRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(advancedDatabaseRadMenuItem);

                /// Help Menu
                RadMenuItem helpRadMenuItem = new RadMenuItem();
                helpRadMenuItem.Text = "Help";
                mainDisplayRadMenu.Items.Add(helpRadMenuItem);
                mainMenuIndex++;

                RadMenuItem openHelpFileRadMenuItem = new RadMenuItem();
                openHelpFileRadMenuItem.Text = "Open Help";
                openHelpFileRadMenuItem.Click += new EventHandler(openHelpFileRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openHelpFileRadMenuItem);
              
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem aboutDmUtilitiesRadMenuItem = new RadMenuItem();
                aboutDmUtilitiesRadMenuItem.Text = "About";
                aboutDmUtilitiesRadMenuItem.Click += new EventHandler(aboutDmUtilitiesRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(aboutDmUtilitiesRadMenuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetUpMainMenu()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void importTemplateConfigurationsDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceDatabaseName;
                ErrorCode errorCode;
                //if (!configurationsHaveBeenAccessed)
                //{
                if (this.databaseConnectionStringIsCorrect)
                {
                        sourceDatabaseName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "mdf");
                        errorCode = DatabaseFileMethods.DatabaseExists(sourceDatabaseName);
                        if (errorCode == ErrorCode.DatabaseFound)
                        {
                            errorCode = DatabaseFileMethods.DatabaseExists(Path.Combine(applicationDataPath, ProgramStrings.DefaultTemplateDatabaseName));
                            if (errorCode == ErrorCode.DatabaseFound)
                            {
                                if (RadMessageBox.Show(this, "This will overwrite the current template config DB - continue?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    DatabaseFileMethods.ClearLinqDatabaseConnectionsForOneDatabase(this.templateDbConnectionString);
                                    DatabaseFileMethods.ImportTemplateDatabase(this, sourceDatabaseName, this.applicationDataPath, true);
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                }
                //}
                //else
                //{
                //    RadMessageBox.Show(this, cannotImportTemplateDatabaseAfterViewingConfigurationText);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.importTemplateConfigurationsDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void advancedDatabaseRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (SetDatabaseConnection connect = new SetDatabaseConnection(this.templateDbConnectionString, this.applicationDataPath, true))
                {
                    connect.ShowDialog();
                    connect.Hide();
                    if (connect.SaveConnectionStringAsDefault)
                    {
                        this.templateDbConnectionString = connect.ConnectionString;
                        TestDatabaseConnectionString();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.advancedDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void TestDatabaseConnectionString()
        {
            try
            {
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(this.templateDbConnectionString))
                {
                    this.databaseConnectionStringIsCorrect = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.TestDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void openHelpFileRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = Path.Combine(executablePath, "DM Utilities Manual.chm");

                if (p.StartInfo.FileName != null)
                {
                    p.Start();
                }
                else
                {
                    MessageBox.Show(this, "Could not find the file 'DM Utilities Manual.chm' in the program directory");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void aboutDmUtilitiesRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (AboutDMSeriesUtilities about = new AboutDMSeriesUtilities())
                {
                    about.ShowDialog();
                    about.Hide();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.aboutDmUtilitiesRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #region BHM Tab Event Handlers

        private void readEventLogsBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                List<String> eventLogs;

                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    eventLogs = ReadEventLogsAndCreateListOfStringsOfValues(modBusAddress, MonitorType.BHM);

                    if (eventLogs.Count > 1)
                    {
                        eventLogBhmTabRadListControl.Items.Clear();
                        foreach (string line in eventLogs)
                        {
                            eventLogBhmTabRadListControl.Items.Add(line);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeviceWasBusy(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void balanceBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int deviceIsBusy;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    if (RadMessageBox.Show("This will destroy all data currently saved to the device.  Continue?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (deviceIsBusy == 0)
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                DeviceCommunication.BHM_BalanceDevice(modBusAddress, 200);
                                RadMessageBox.Show(this, "Balance command sent to device");
                            }
                            else if (deviceIsBusy == 1)
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            }
                            DeviceCommunication.CloseConnection();
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.balanceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void startMeasurementBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int deviceIsBusy;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            BhmDisplayFirmwareVersion(modBusAddress);
                            InteractiveDeviceCommunication.StartMeasurement(modBusAddress, 30, 200, parentWindowInformation);
                            //DeviceCommunication.SendStringCommand(modBusAddress, "SINGLE;", 200, 0, 1);
                            //RadMessageBox.Show("It will take a few minutes for the measurement to be completed.");
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }

                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.startMeasurementRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setUpAutobalanceBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime currentDeviceDateTime;
                Int16[] registersToWrite;
                ErrorCode errorCode;
                int deviceIsBusy;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        BhmDisplayFirmwareVersion(modBusAddress);
                        currentDeviceDateTime = InteractiveDeviceCommunication.GetDeviceTime(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        DeviceCommunication.CloseConnection();
                        if (currentDeviceDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                        {
                            RadMessageBox.Show("Could not get current device time, using current PC time");
                            currentDeviceDateTime = DateTime.Now;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("Could not get current device time, using current PC time");
                        currentDeviceDateTime = DateTime.Now;
                    }
                    
                    using (AutoBalance autoBalance = new AutoBalance(currentDeviceDateTime,modBusAddress, numberOfNonInteractiveRetries,totalRetries))
                    {
                        
                        autoBalance.ShowDialog();
                        autoBalance.Hide();
                        

                        if (autoBalance.WriteRegistersToDevice)
                        {
                            registersToWrite = new Int16[7];

                            registersToWrite[0] = (Int16)autoBalance.DateTimeToSet.Month;
                            registersToWrite[1] = (Int16)autoBalance.DateTimeToSet.Day;
                            registersToWrite[2] = (Int16)autoBalance.DateTimeToSet.Year;
                            registersToWrite[3] = (Int16)autoBalance.DateTimeToSet.Hour;

                            registersToWrite[4] = (Int16)autoBalance.BalanceDelayInDays;

                            if (autoBalance.EnableAutoBalance)
                            {
                                registersToWrite[5] = 1;
                            }
                            else
                            {
                                registersToWrite[5] = 0;
                            }

                            if (autoBalance.PerformNoLoadTest)
                            {
                                registersToWrite[6] = 1;
                            }
                            else
                            {
                                registersToWrite[6] = 0;
                            }

                            errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if (deviceIsBusy == 0)
                                {
                                    if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                    {
                                        if (!InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 622, registersToWrite, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                        {
                                            RadMessageBox.Show("Failed to write the auto-balance parameters to the device");
                                        }
                                        InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                                    }
                                }
                                else if (deviceIsBusy == 1)
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                }
                            }
                            else
                            {
                                RadMessageBox.Show("Failed to write the auto-balance parameters to the device");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setUpAutobalanceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void getLatestReadingsBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                int deviceIsBusy;

                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                LoadBushingData(modBusAddress, false);
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getLatestReadingsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void clearListBoxBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.eventLogBhmTabRadListControl.Items.Clear();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.clearListBoxBhmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getEquipmentErrorBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int16[] registerValues;

                int deviceIsBusy;
                int modBusAddress;
                List<string> equipmentErrorList;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                this.eventLogBhmTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 4, 3, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if ((registerValues != null) && (registerValues.Length == 3))
                                {
                                    equipmentErrorList = HealthAndErrorMethods.GetBHMAlarmStatus((UInt16)registerValues[0]);
                                    if (registerValues[0] != 0)
                                    {
                                        equipmentErrorList.AddRange(HealthAndErrorMethods.GetBHMRegisterAlarmSources(registerValues[1], 1));
                                        equipmentErrorList.AddRange(HealthAndErrorMethods.GetBHMRegisterAlarmSources(registerValues[2], 2));
                                    }

                                    this.eventLogBhmTabRadListControl.Items.Clear();
                                    this.eventLogBhmTabRadListControl.Items.Add("Alerts");
                                    // this.eventLogRadListControl.Items.Add("");
                                    foreach (string entry in equipmentErrorList)
                                    {
                                        this.eventLogBhmTabRadListControl.Items.Add(entry);
                                    }
                                }
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getEquipmentErrorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void deviceConfigurationBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                int deviceIsBusy;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                this.eventLogBhmTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                DeviceCommunication.CloseConnection();
                                using (BHM_MonitorConfiguration config = new BHM_MonitorConfiguration(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries, this.templateDbConnectionString))
                                {
                                    config.ShowDialog();
                                    config.Hide();
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deviceConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void getDeviceHealthBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int16[] registerValues;

                int deviceIsBusy;
                int modBusAddress;
                UInt16 regularErrorCode = 0;
                UInt16 extendedErrorCode = 0;
                UInt32 deviceHealthStatusBitEncoded = 0;

                List<string> deviceHealthList = null;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                this.eventLogBhmTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            /// If you pause the device, it will show up in the error report, so don't do it.
                            BhmDisplayFirmwareVersion(modBusAddress);

                            registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 3, 7, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if ((registerValues != null) && (registerValues.Length == 7))
                            {
                                extendedErrorCode = ConversionMethods.Int16BytesToUInt16Value(registerValues[0]);//register 3
                                regularErrorCode = ConversionMethods.Int16BytesToUInt16Value(registerValues[6]);//register 9
                                deviceHealthStatusBitEncoded = ConversionMethods.BitwiseCombineTwoUInt16IntoOneUInt32(regularErrorCode, extendedErrorCode);
                                deviceHealthList = HealthAndErrorMethods.GetBhmDeviceHealthStatus(deviceHealthStatusBitEncoded, ProgramBrand.DynamicRatings);
                            }
                            if (deviceHealthList != null)
                            {
                                this.eventLogBhmTabRadListControl.Items.Clear();
                                this.eventLogBhmTabRadListControl.Items.Add("Device health");
                                foreach (string entry in deviceHealthList)
                                {
                                    this.eventLogBhmTabRadListControl.Items.Add(entry);
                                }
                            }

                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getDeviceHealthRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void setDeviceDateBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCode errorCode;
                int deviceIsBusy;
                int modBusAddress = GetModbusAddress(this.modBusAddressBhmTabRadMaskedEditBox);
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                   deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                   if (deviceIsBusy == 0)
                   {
                       if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                       {
                           BhmDisplayFirmwareVersion(modBusAddress);
                           InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                           DeviceCommunication.CloseConnection();
                           SetDeviceDateAndTime(modBusAddress, false);
                       }
                   }
                   else if (deviceIsBusy == 1)
                   {
                       RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                   }
                   else
                   {
                       RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                   }
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setDeviceDateBhmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        # region PDM Tab Event Handlers

        private void normalModeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (normalModeRadioButton.Checked)
                {
                    normalModeRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    normalModeRadioButton.BackColor = normalModeRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.normalModeRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void testModeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (testModeRadioButton.Checked)
                {
                    testModeRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    testModeRadioButton.BackColor = normalModeRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.testModeRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setModeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int16 saveMode = 0;
                ErrorCode errorCode;
                int deviceIsBusy;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (testModeRadioButton.Checked)
                {
                    saveMode = 1;
                }
                Int16 cycAcq =0;
                cycAcq =(Int16)cycleOfAcqSpinEditor.Value;
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                PdmDisplayFirmwareVersion(modBusAddress);
                                if (InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 1001, saveMode, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                {
                                    InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 1013, cycAcq, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                    RadMessageBox.Show("Wrote current selected save mode to the device");
                                }
                                else
                                {
                                    RadMessageBox.Show("Failed to write the save mode to the device");
                                }
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setModeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void startMeasurementPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int deviceIsBusy;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            PdmDisplayFirmwareVersion(modBusAddress);
                            InteractiveDeviceCommunication.StartMeasurement(modBusAddress, 30, 200, parentWindowInformation);
                            //DeviceCommunication.SendStringCommand(modBusAddress, "SINGLE;", 200, 0, 1);
                            //RadMessageBox.Show("It will take a few minutes for the measurement to be completed.");
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.startMeasurementRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getLatestReadingsPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    LoadChannelData(modBusAddress, false);
                }
                else
                {
                    RadMessageBox.Show("Modbus address is not properly set.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getLatestReadingsPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void viewPhaseResolvedDataPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    LoadPhaseResolvedData(modBusAddress);
                }
                else
                {
                    RadMessageBox.Show("Modbus address is not properly set.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.viewPhaseResolvedDataPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setDeviceDatePdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorCode errorCode;
                int deviceIsBusy;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int modBusAddress = GetModbusAddress(this.modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                DeviceCommunication.CloseConnection();
                                SetDeviceDateAndTime(modBusAddress, false);
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setDeviceDatePdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setDeviceDateMainTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    MainDisplayFirmwareVersion(modBusAddress);
                    SetDeviceDateAndTime(modBusAddress, true);
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.setDeviceDateMainTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deviceConfigurationPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                int deviceIsBusy;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                this.eventLogPdmTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                         deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                         if (deviceIsBusy == 0)
                         {
                             if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                             {
                                 PdmDisplayFirmwareVersion(modBusAddress);
                                 InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                 DeviceCommunication.CloseConnection();

                                 using (PDM_MonitorConfiguration config = new PDM_MonitorConfiguration(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries, this.templateDbConnectionString))
                                 {
                                     config.ShowDialog();
                                     config.Hide();
                                 }
                             }
                             else
                             {
                                 RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                             }
                         }
                         else if (deviceIsBusy == 1)
                         {
                             RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                         }
                         else
                         {
                             RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                         }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deviceConfigurationPdmRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void readEventLogsPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                List<String> eventLogs;

                modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    eventLogs = ReadEventLogsAndCreateListOfStringsOfValues(modBusAddress, MonitorType.PDM);

                    if (eventLogs.Count > 1)
                    {
                        eventLogPdmTabRadListControl.Items.Clear();
                        foreach (string line in eventLogs)
                        {
                            eventLogPdmTabRadListControl.Items.Add(line);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.readEventLogsPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getDeviceErrorsPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                Int16[] registerValues;

                int month;
                int day;
                int year;
                int hour;
                int minute;
                int deviceIsBusy;
                DateTime logDate;
                List<string> deviceErrors;

                int offset = 0;

                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            /// if you pause the device, it will show up in the error listing, so don't do it.
                            PdmDisplayFirmwareVersion(modBusAddress);
                            registerValues = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 9, 6, 200, this.numberOfNonInteractiveRetries, this.totalRetries);
                            if (registerValues != null)
                            {
                                if (registerValues.Length == 6)
                                {
                                    eventLogPdmTabRadListControl.Items.Clear();
                                    month = registerValues[offset + 1];
                                    day = registerValues[offset + 2];
                                    year = registerValues[offset + 3];
                                    hour = registerValues[offset + 4];
                                    minute = registerValues[offset + 5];
                                    logDate = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                                    eventLogPdmTabRadListControl.Items.Add("Date: " + logDate.ToString());

                                    deviceErrors = HealthAndErrorMethods.GetPdmDeviceHealthStatus(ConversionMethods.Int16BytesToUInt16Value(registerValues[0]), false, ProgramBrand.DynamicRatings);
                                    if (deviceErrors.Count == 0)
                                    {
                                        eventLogPdmTabRadListControl.Items.Add("No device health problems");
                                    }
                                    else
                                    {
                                        eventLogPdmTabRadListControl.Items.Add("Device health problems are:");
                                        foreach (string entry in deviceErrors)
                                        {
                                            eventLogPdmTabRadListControl.Items.Add(entry);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show("Failed to download the device health info");
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.readEventLogsPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void getAlertsPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                Int16[] registerValues;

                int month;
                int day;
                int year;
                int hour;
                int minute;
                int deviceIsBusy;
                DateTime logDate;
                List<string> alarmStrings;

                int offset = 0;

                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                            {
                                PdmDisplayFirmwareVersion(modBusAddress);
                                registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 4, 11, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if (registerValues != null)
                                {
                                    if (registerValues.Length == 11)
                                    {
                                        eventLogPdmTabRadListControl.Items.Clear();
                                        month = registerValues[offset + 6];
                                        day = registerValues[offset + 7];
                                        year = registerValues[offset + 8];
                                        hour = registerValues[offset + 9];
                                        minute = registerValues[offset + 10];
                                        logDate = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);

                                        eventLogPdmTabRadListControl.Items.Add("Date: " + logDate.ToString());

                                        alarmStrings = HealthAndErrorMethods.GetPDMAlarmStatus(registerValues[0]);
                                        foreach (string entry in alarmStrings)
                                        {
                                            eventLogPdmTabRadListControl.Items.Add(entry);
                                        }

                                        if (registerValues[0] != 0)
                                        {
                                            alarmStrings = HealthAndErrorMethods.GetPDMRegisterAlarmSources(ConversionMethods.Int16BytesToUInt16Value(registerValues[1]));
                                            if (alarmStrings.Count == 0)
                                            {
                                                eventLogPdmTabRadListControl.Items.Add("No warning or alarm sources");
                                            }
                                            else
                                            {
                                                eventLogPdmTabRadListControl.Items.Add("Sources of warnings or alarms are:");
                                                foreach (string entry in alarmStrings)
                                                {
                                                    eventLogPdmTabRadListControl.Items.Add(entry);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Failed to read the registers");
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show("Failed to download the alarm status info");
                                }
                                InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                            }
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.readEventLogsPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        #endregion

        private void registerValuesRadGridView_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                int registerNumber;
                int registerValue;
                int modBusAddress;
                Point desiredPosition;
                if (e.ColumnIndex == 1)
                {
                    if (registerValuesRadGridView != null)
                    {
                        if (Int32.TryParse(registerValuesRadGridView.Rows[e.RowIndex].Cells[0].Value.ToString(), out registerNumber))
                        {
                            if (Int32.TryParse(registerValuesRadGridView.Rows[e.RowIndex].Cells[1].Value.ToString(), out registerValue))
                            {
                                modBusAddress = GetModbusAddress(modBusAddressReadRegistersTabRadMaskedEditBox);

                                desiredPosition = new Point(this.Location.X + Control.MousePosition.X, this.Location.Y + Control.MousePosition.Y);

                                using (WriteRegisterValue writeValue = new WriteRegisterValue(modBusAddress, registerNumber, registerValue, desiredPosition, this.numberOfNonInteractiveRetries, this.totalRetries))
                                {
                                    writeValue.ShowDialog();
                                    writeValue.Hide();

                                    //cose added by cfk 12/17/13 -- re read data once write is done
                                    readRegistersReadRegistersTabRadButton_Click(sender, null);

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.registerValuesRadGridView_CellDoubleClick(object, GridViewCellEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private void readRegistersReadRegistersTabRadButton_Click(object sender, EventArgs e)
        //{

        //}

        private double GetFirmwareVersion(int modBusAddress)
        {
            double firmwareVersion = 0.00;
            try
            {
                Int16[] registerValues;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (registerValues != null)
                {
                    firmwareVersion = Math.Round((registerValues[0] / 100.0), 2);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.readEventLogsPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return firmwareVersion;
        }

        private void findRecorsWithPhaseResolvedDataRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;
                Int16[] registerValues;

                int month;
                int day;
                int year;
                int hour;
                int minute;
                int eventCode;
                int deviceIsBusy;
                DateTime logDate;
                string eventLogLine;

                int offset = 0;

                ErrorCode errorCode;

                int[] archiveDownloadLimits;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                List<int> recordsWithMatrices = new List<int>();

                modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            archiveDownloadLimits = InteractiveDeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, ConversionMethods.MinimumDateTime(), 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                            for (int i = 1; i <= archiveDownloadLimits[1]; i++)
                            {
                                searchProgressRadLabel.Text = "Looking at record " + i.ToString() + " of " + archiveDownloadLimits[1].ToString();
                                Application.DoEvents();
                                if (InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 619, (Int16)i, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                {
                                    registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 4009, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                    if (registerValues != null)
                                    {
                                        if (registerValues[0] == 1)
                                        {
                                            recordsWithMatrices.Add(i);
                                        }
                                    }
                                }
                            }

                            StringBuilder outputSTring = new StringBuilder();

                            outputSTring.Append("Here are the records that have phase resolved data");
                            outputSTring.Append("\n\n");
                            foreach (int entry in recordsWithMatrices)
                            {
                                outputSTring.Append(entry.ToString());
                                outputSTring.Append(", ");
                            }

                            RadMessageBox.Show(outputSTring.ToString());
                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                        }
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.findRecorsWithPhaseResolvedDataRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getDynamicsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);
                AddDataToMainDynamicsDataRadGridView(modBusAddress);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getDynamicsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deviceConfigurationMainTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.eventLogMainTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        MainDisplayFirmwareVersion(modBusAddress);
                        using (Main_MonitorConfiguration config = new Main_MonitorConfiguration(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries, this.templateDbConnectionString))
                        {
                            config.ShowDialog();
                            config.Hide();
                        }
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deviceConfigurationPdmRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void clearDisplayPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.eventLogPdmTabRadListControl.Items.Clear();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.clearDisplayPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void bhmSaveEventLogsToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                List<String> eventLogs;

                string fileName = FileUtilities.GetSaveFileNameWithFullPath("BHM_LogFile", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "txt", true, false);
                if (fileName != string.Empty)
                {
                    modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);
                    if (modBusAddress > 0)
                    {
                        eventLogs = ReadEventLogsAndCreateListOfStringsOfValues(modBusAddress, MonitorType.BHM);
                        if (eventLogs.Count > 1)
                        {
                            FileUtilities.SaveDataToGenericRegularFile(fileName, eventLogs);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("You must enter a value for the modbus Address");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.bhmSaveEventLogsToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void pdmSaveEventLogsToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                List<String> eventLogs;
                string fileName = FileUtilities.GetSaveFileNameWithFullPath("PDM_LogFile", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "txt", true, false);
                if (fileName != string.Empty)
                {
                    modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                    if (modBusAddress > 0)
                    {
                        eventLogs = ReadEventLogsAndCreateListOfStringsOfValues(modBusAddress, MonitorType.PDM);
                        if (eventLogs.Count > 1)
                        {
                            FileUtilities.SaveDataToGenericRegularFile(fileName, eventLogs);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("You must enter a value for the modbus Address");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.pdmSaveEventLogsToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
     
        private void deleteDeviceDataBhmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);
                int deviceIsBusy;
                bool dataWasDeleted;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddress > 0)
                {
                    if (RadMessageBox.Show("This will delete all data currently saved to the device.  Continue?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (deviceIsBusy == 0)
                            {
                                BhmDisplayFirmwareVersion(modBusAddress);
                                dataWasDeleted = InteractiveDeviceCommunication.ClearDeviceData(modBusAddress, 5, 200, parentWindowInformation);
                                if (dataWasDeleted)
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteSucceeded));
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteFailed));
                                }
                            }
                            else if (deviceIsBusy == 1)
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deleteDeviceDataBhmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deleteDeviceDataPdmTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressPdmTabRadMaskedEditBox);
                int deviceIsBusy;
                bool dataWasDeleted;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddress > 0)
                {
                    if (RadMessageBox.Show("This will delete all data currently saved to the device.  Continue?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (deviceIsBusy == 0)
                            {
                                PdmDisplayFirmwareVersion(modBusAddress);
                                dataWasDeleted = InteractiveDeviceCommunication.ClearDeviceData(modBusAddress, 5, 200, parentWindowInformation);
                                if (dataWasDeleted)
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteSucceeded));
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteFailed));
                                }
                            }
                            else if (deviceIsBusy == 1)
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deleteDeviceDataPdmTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deleteDeviceDataMainTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);
                int deviceIsBusy;
                bool dataWasDeleted;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddress > 0)
                {
                    if (RadMessageBox.Show("This will delete all data currently saved to the device.  Continue?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            //deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            //if (deviceIsBusy == 0)
                            //{
                                MainDisplayFirmwareVersion(modBusAddress);
                                dataWasDeleted = InteractiveDeviceCommunication.ClearDeviceData(modBusAddress, 5, 2, parentWindowInformation);
                                if (dataWasDeleted)
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteSucceeded));
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceDataDeleteFailed));
                                }
                            //}
                            //else if (deviceIsBusy == 1)
                            //{
                            //    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            //}
                            //else
                            //{
                            //    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            //}
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deleteDeviceDataMainTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void calibrateAnalogSignalsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);
               // int deviceIsBusy;
                string commandResult;
                ErrorCode errorCode = ErrorCode.None;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddress > 0)
                {
                    if (RadMessageBox.Show("Disconnect all analog inputs before proceeding.\nIt will take about 20 seconds to complete all calibrations", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            //deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            //if (deviceIsBusy == 0)
                            //{
                                MainDisplayFirmwareVersion(modBusAddress);
                                commandResult = DeviceCommunication.SendStringCommand(modBusAddress, "calnull ;", 2, 0, 1);

                                int x = 0;
                                x++;
                                //if (DeviceCommunication.StringCommandWasSuccessful(commandResult))
                                //{
                                //    RadMessageBox.Show(this, "Calibration in progress");
                                //}
                                //else
                                //{
                                //    RadMessageBox.Show(this, "Command failed");
                                //}
                            //}
                            //else if (deviceIsBusy == 1)
                            //{
                            //    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            //}
                            //else
                            //{
                            //    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            //}
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deleteDeviceDataMainTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void openDnpSettingsRadButton_Click(object sender, EventArgs e)
        {
            int modBusAddress = GetModbusAddress(this.modBusAddressMainTabRadMaskedEditBox);
            using (DNPSettings settings = new DNPSettings(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries))
            {
                settings.ShowDialog();
                settings.Hide();
            }
        }

        private void calibrateChannelsRadButton_Click(object sender, EventArgs e)
        {
            int modBusAddress = GetModbusAddress(this.modBusAddressPdmTabRadMaskedEditBox);
            using (CalibratePDChannels calibrate = new CalibratePDChannels(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries))
            {
                calibrate.ShowDialog();
                calibrate.Hide();
            }
        }

        private void windingHotSpotConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.eventLogMainTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 2, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        MainDisplayFirmwareVersion(modBusAddress);
                        using (Main_WHSMonitorConfiguration config = new Main_WHSMonitorConfiguration(modBusAddress, this.numberOfNonInteractiveRetries, this.totalRetries, this.templateDbConnectionString))
                        {
                            config.ShowDialog();
                            config.Hide();
                        }
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deviceConfigurationPdmRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void mainMonitorRadPageViewPage_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            try
            {
                int modBusAddress;

                eventLogMainTabRadListControl.Items.Clear();
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.eventLogMainTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressMainTabRadMaskedEditBox);
                short[] errorValues = new short[1];
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        errorValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 9, 1, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        DeviceCommunication.CloseConnection();
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                    UInt16 x;
                    x = Convert.ToUInt16(errorValues[0]);

                    if (errorValues[0] != 0)
                    {
                        if (x == 0) // bit 1
                        {
                            // this.eventLogMainTabRadListControl.Items.Add("");
                        }
                        else if (x == 2) //bit 2
                        {
                            // this.eventLogMainTabRadListControl.Items.Add("");
                        }
                        else if (x == 4) // bit 3
                        {
                            this.eventLogMainTabRadListControl.Items.Add("Flash Write Failure");
                        }
                        else if (x == 8) // bit 4
                        {
                            this.eventLogMainTabRadListControl.Items.Add("Flash Read Failure");
                        }
                        else if (x == 16) // bit 5
                        {
                            this.eventLogMainTabRadListControl.Items.Add("Clock/Time Error");
                        }
                        else if (x == 32) // bit 6
                        {
                            this.eventLogMainTabRadListControl.Items.Add("Serial Buffer Overrun");
                        }
                        else if (x == 64) // bit 7
                        {
                            this.eventLogMainTabRadListControl.Items.Add("WHS Error.  See WHS Error Log");
                        }
                    }
                    else
                    {
                        this.eventLogMainTabRadListControl.Items.Add("No Errors"); 
                    }
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.deviceConfigurationPdmRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getDeviceHealthBhmTabRadButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                Int16[] registerValues;

                int deviceIsBusy;
                int modBusAddress;
                UInt16 regularErrorCode = 0;
                UInt16 extendedErrorCode = 0;
                UInt32 deviceHealthStatusBitEncoded = 0;

                List<string> deviceHealthList = null;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                this.eventLogBhmTabRadListControl.Items.Clear();

                modBusAddress = GetModbusAddress(modBusAddressBhmTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceIsBusy == 0)
                        {
                            /// If you pause the device, it will show up in the error report, so don't do it.
                            BhmDisplayFirmwareVersion(modBusAddress);

                            registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 3, 7, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if ((registerValues != null) && (registerValues.Length == 7))
                            {
                                extendedErrorCode = ConversionMethods.Int16BytesToUInt16Value(registerValues[0]);//register 3
                                regularErrorCode = ConversionMethods.Int16BytesToUInt16Value(registerValues[6]);//register 9
                                deviceHealthStatusBitEncoded = ConversionMethods.BitwiseCombineTwoUInt16IntoOneUInt32(regularErrorCode, extendedErrorCode);
                                deviceHealthList = HealthAndErrorMethods.GetBhmDeviceHealthStatus(deviceHealthStatusBitEncoded, ProgramBrand.DynamicRatings);
                            }
                            if (deviceHealthList != null)
                            {
                                this.eventLogBhmTabRadListControl.Items.Clear();
                                this.eventLogBhmTabRadListControl.Items.Add("Device health");
                                foreach (string entry in deviceHealthList)
                                {
                                    this.eventLogBhmTabRadListControl.Items.Add(entry);
                                }
                            }

                        }
                        else if (deviceIsBusy == 1)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getDeviceHealthRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void radButtonBushingCalc_Click(object sender, EventArgs e)
        {
            Form f = new frmBushingCalculator1();
            f.Show();
        }
    }

    public enum MonitorType
    {
        ADM,
        BHM,
        PDM
    }

    //public class DataDownloadReturnObject
    //{
    //    public ErrorCode errorCode = ErrorCode.None;
    //    public bool logicalResult = false;
    //}

}

