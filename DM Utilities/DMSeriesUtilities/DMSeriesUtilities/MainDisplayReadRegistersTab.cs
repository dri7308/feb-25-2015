﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class MainDisplay
    {

        private void InitializeRegisterValuesRadGridView()
        {
            try
            {               
                GridViewDecimalColumn registerNumberGridViewDecimalColumn = new GridViewDecimalColumn();
                registerNumberGridViewDecimalColumn.Name = "RegisterNumber";
                registerNumberGridViewDecimalColumn.HeaderText = "<html>Register<br>Number</html>";
                registerNumberGridViewDecimalColumn.DecimalPlaces = 0;
                registerNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                registerNumberGridViewDecimalColumn.Width = 90;
                registerNumberGridViewDecimalColumn.AllowSort = false;
                // hourEntryGridViewDecimalColumn.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;

                GridViewDecimalColumn registerValueGridViewDecimalColumn = new GridViewDecimalColumn();
                registerValueGridViewDecimalColumn.Name = "RegisterValue";
                registerValueGridViewDecimalColumn.HeaderText = "<html>Value</html>";
                registerValueGridViewDecimalColumn.DecimalPlaces = 0;
                registerValueGridViewDecimalColumn.DisableHTMLRendering = false;
                registerValueGridViewDecimalColumn.Width = 90;
                registerValueGridViewDecimalColumn.AllowSort = false;
                // hourEntryGridViewDecimalColumn.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;

                this.registerValuesRadGridView.Columns.Add(registerNumberGridViewDecimalColumn);
                this.registerValuesRadGridView.Columns.Add(registerValueGridViewDecimalColumn);

                this.registerValuesRadGridView.TableElement.TableHeaderHeight = 30;
                this.registerValuesRadGridView.AllowColumnReorder = false;
                this.registerValuesRadGridView.AllowColumnChooser = false;
                this.registerValuesRadGridView.ShowGroupPanel = false;
                this.registerValuesRadGridView.EnableGrouping = false;
                this.registerValuesRadGridView.AllowAddNewRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeMeasurementSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void AddEmptyRowsToMeasurementSettingsGridView()
//        {
//            try
//            {
//                this.measurementSettingsRadGridView.Rows.Clear();
//                for (int i = 0; i < 50; i++)
//                {
//                    this.measurementSettingsRadGridView.Rows.Add((i + 1), 0, 0);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.FillMeasurementSettingsGridView()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void AddDataToRegisterValuesGridView(UInt16[] registerValues, int startingOffset)
        {
            try
            {
                int count;
                int index = 0;

                if (registerValues != null)
                {
                    count = registerValues.Length;

                    registerValuesRadGridView.Rows.Clear();

                    for (index = 0; index < count; index++)
                    {
                        registerValuesRadGridView.Rows.Add((startingOffset + index), registerValues[index]);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AddDataToMeasurementSettingsGridView(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void readRegistersReadRegistersTabRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int deviceType;
                int modBusAddress;
                int startingRegister;
                int numberOfRegistersToRead;
                ErrorCode errorCode;
                Int16[] registerValues;
                UInt16[] unsignedRegisterValues;

                bool readTheRegisters = false;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                modBusAddress = GetModbusAddress(modBusAddressReadRegistersTabRadMaskedEditBox);

                if (modBusAddress > 0)
                {
                    if (Int32.TryParse(startingRegisterReadRegistersTabRadMaskedEditBox.Text, out startingRegister))
                    {
                        if ((startingRegister > 0) && (startingRegister < 20000))
                        {
                            if (Int32.TryParse(numberOfRegistersReadRegistersTabRadMaskedEditBox.Text, out numberOfRegistersToRead))
                            {
                                if ((numberOfRegistersToRead > 0) && (numberOfRegistersToRead < 101))
                                {
                                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAnyMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries);
                                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                    {
                                        deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                        if (deviceType > 0)
                                        {
                                            if (deviceType == 505)
                                            {
                                                readTheRegisters = !(PDM_RegistersAreOutOfRange(startingRegister, numberOfRegistersToRead));
                                            }
                                            else if (deviceType == 15002)
                                            {
                                                readTheRegisters = !(BHM_RegistersAreOutOfRange(startingRegister, numberOfRegistersToRead));
                                            }
                                            else if (deviceType == 101)
                                            {
                                                readTheRegisters = !(Main_RegistersAreOutOfRange(startingRegister, numberOfRegistersToRead));
                                            }
                                            else
                                            {
                                                RadMessageBox.Show("Unknown device type");
                                            }

                                            if (readTheRegisters)
                                            {
                                                registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, startingRegister, numberOfRegistersToRead, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                                if ((registerValues != null) && (registerValues.Length > 0))
                                                {
                                                    unsignedRegisterValues = ConversionMethods.Int16ArrayToUInt16Array(registerValues);
                                                    AddDataToRegisterValuesGridView(unsignedRegisterValues, startingRegister);
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show("Failed to read the data from the device");
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show("Your input included some undefined registers.");
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show("Failed to read the device type");
                                        }
                                    }
                                    else
                                    {
                                       RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                    }
                                    DeviceCommunication.CloseConnection();
                                }
                                else
                                {
                                    RadMessageBox.Show("The number of registers to read must be\ngreater than zero and less than 101");
                                }
                            }
                            else
                            {
                                RadMessageBox.Show("Incorrect character in number of registers text box");
                            }
                        }
                        else
                        {
                            RadMessageBox.Show("Starting register is out of range");
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("Incorrect character in starting register text box");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool BHM_RegistersAreOutOfRange(int startRegister, int registerCount)
        {
            bool outOfRange = false;
            try
            {
                for (int i = startRegister; i < registerCount; i++)
                {
                    if ((i > 23) && (i < 51))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 330) && (i < 451))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 570) && (i < 601))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 628) && (i < 651))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 774) && (i < 801))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1031) && (i < 1051))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1081) && (i < 2501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if (i > 2584)
                    {
                        outOfRange = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_RegistersAreOutOfRange(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outOfRange;
        }


        private bool PDM_RegistersAreOutOfRange(int startRegister, int registerCount)
        {
            bool outOfRange = false;
            try
            {
                for (int i = startRegister; i < registerCount; i++)
                {
                    if ((i > 39) && (i < 51))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 121) && (i < 451))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 570) && (i < 601))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 623) && (i < 1001))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1075) && (i < 2501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2599) && (i < 3001))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3490) && (i < 4001))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4490) && (i < 5001))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 8072) && (i < 8401))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if (i > 8474)
                    {
                        outOfRange = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_RegistersAreOutOfRange(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outOfRange;
        }


        private bool Main_RegistersAreOutOfRange(int startRegister, int registerCount)
        {
            bool outOfRange = false;
            try
            {
                for (int i = startRegister; i < registerCount; i++)
                {
                    if ((i > 41) && (i < 51))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 95) && (i < 121))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 138) && (i < 151))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 270) && (i < 301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 402) && (i < 501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 602) && (i < 701))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 802) && (i < 901))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1002) && (i < 1101))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1202) && (i < 1301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1402) && (i < 1501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1602) && (i < 1701))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 1802) && (i < 1901))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2002) && (i < 2101))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2202) && (i < 2301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2402) && (i < 2501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2602) && (i < 2701))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 2802) && (i < 2901))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3002) && (i < 3101))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3202) && (i < 3301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3402) && (i < 3501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3602) && (i < 3701))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 3802) && (i < 3901))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4002) && (i < 4101))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4202) && (i < 4301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4402) && (i < 4501))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4602) && (i < 4701))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 4802) && (i < 4901))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 5002) && (i < 5101))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 5202) && (i < 5301))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 5450) && (i < 5601))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 5888) && (i < 6001))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if ((i > 6084) && (i < 6481))
                    {
                        outOfRange = true;
                        break;
                    }
                    else if (i > 7440)
                    {
                        outOfRange = true;
                        break;
                    }
                }                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_RegistersAreOutOfRange(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outOfRange;
        }

    }
}
