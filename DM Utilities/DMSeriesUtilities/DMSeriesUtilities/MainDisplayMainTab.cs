﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class MainDisplay
    {
        List<string> mainDynamicsNames;

        private void MainDisplayFirmwareVersion(int modBusAddress)
        {
            try
            {
                double firmwareVersion = GetFirmwareVersion(modBusAddress);
                if (firmwareVersion > .2)
                {
                    mainFirmwareVersionValueRadLabel.Text = Math.Round(firmwareVersion, 2).ToString();
                }
                else
                {
                    RadMessageBox.Show("Failed to get the Main firmware version");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayMainTab.MainDisplayFirmwareVersion(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeMainDynamicsDataRadGridView()
        {
            try
            {
                this.mainDynamicsDataRadGridView.Rows.Clear();

                GridViewTextBoxColumn dynamicsNameGridViewTextBoxColumn = new GridViewTextBoxColumn("DynamicsName");
                dynamicsNameGridViewTextBoxColumn.Name = "DynamicsName";
                dynamicsNameGridViewTextBoxColumn.HeaderText = "Dynamics Name";
                dynamicsNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                dynamicsNameGridViewTextBoxColumn.Width = 150;
                dynamicsNameGridViewTextBoxColumn.AllowSort = false;
                dynamicsNameGridViewTextBoxColumn.IsPinned = true;
                dynamicsNameGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn dynamicsValueGridViewTextBoxColumn = new GridViewTextBoxColumn("DynamicsValue");
                dynamicsValueGridViewTextBoxColumn.Name = "DynamicsValue";
                dynamicsValueGridViewTextBoxColumn.HeaderText = "Dynamics Value";
                dynamicsValueGridViewTextBoxColumn.DisableHTMLRendering = false;
                dynamicsValueGridViewTextBoxColumn.Width = 125;
                dynamicsValueGridViewTextBoxColumn.AllowSort = false;
                dynamicsValueGridViewTextBoxColumn.IsPinned = true;
                dynamicsValueGridViewTextBoxColumn.ReadOnly = true;
 
                mainDynamicsDataRadGridView.Columns.Add(dynamicsNameGridViewTextBoxColumn);
                mainDynamicsDataRadGridView.Columns.Add(dynamicsValueGridViewTextBoxColumn);

                mainDynamicsDataRadGridView.AllowColumnReorder = false;
                mainDynamicsDataRadGridView.AllowColumnChooser = false;
                // pdmChannelDataRadGridView.ShowGroupPanel = false;
                mainDynamicsDataRadGridView.EnableGrouping = false;
                mainDynamicsDataRadGridView.AllowAddNewRow = false;
                mainDynamicsDataRadGridView.AllowDeleteRow = false;
                mainDynamicsDataRadGridView.AutoScroll = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.InitializePdmDataRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeMainDynamicsNamesList()
        {
            try
            {
                mainDynamicsNames = new List<string>();

                mainDynamicsNames.Add("Vibration 1");
                mainDynamicsNames.Add("Vibration 2");
                mainDynamicsNames.Add("Vibration 3");
                mainDynamicsNames.Add("Vibration 4");
                mainDynamicsNames.Add("Analog In 1");
                mainDynamicsNames.Add("Analog In 2");
                mainDynamicsNames.Add("Analog In 3");
                mainDynamicsNames.Add("Analog In 4");
                mainDynamicsNames.Add("Analog In 5");
                mainDynamicsNames.Add("Analog In 6");
                mainDynamicsNames.Add("Current 1");
                mainDynamicsNames.Add("Current 2");
                mainDynamicsNames.Add("Current 3");
                mainDynamicsNames.Add("Voltage 1");
                mainDynamicsNames.Add("Voltage 2");
                mainDynamicsNames.Add("Voltage 3");
                mainDynamicsNames.Add("Humidity");
                mainDynamicsNames.Add("Temperature 1");
                mainDynamicsNames.Add("Temperature 2");
                mainDynamicsNames.Add("Temperature 3");
                mainDynamicsNames.Add("Temperature 4");
                mainDynamicsNames.Add("Temperature 5");
                mainDynamicsNames.Add("Temperature 6");
                mainDynamicsNames.Add("Temperature 7");
                mainDynamicsNames.Add("Chassis Temperature");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayMainTab.InitializeMainDynamicsNamesList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToMainDynamicsDataRadGridView(int modBusAddress)
        {
            try
            {
                Int16[] registerValues = null;

                ErrorCode errorCode;
                DateTime readingDateTime;
                int offset, index;
                //Dictionary<int, bool> channelIsActive = null;
                //Dictionary<int, double> channelPdiValue = null;
                //Dictionary<int, double> channelSensitivity = null;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.mainDynamicsDataRadGridView.Rows.Clear();

                errorCode = SimplifiedCommunication.OpenUsbConnectionToAMainMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    MainDisplayFirmwareVersion(modBusAddress);
                    registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 6001, 60, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if ((registerValues != null) && (registerValues.Length == 60))
                    {
                        readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);
                        if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                        {
                            offset = 10;
                            for (index = 0; index < 10; index++)
                            {
                                this.mainDynamicsDataRadGridView.Rows.Add(this.mainDynamicsNames[index], Math.Round((registerValues[offset] / 100.0), 2).ToString());
                                offset += 2;
                            }
                            for (; index < 25; index++)
                            {
                                this.mainDynamicsDataRadGridView.Rows.Add(this.mainDynamicsNames[index], Math.Round((registerValues[offset] / 10.0), 1).ToString());
                                offset += 2;
                            }
                            this.mainDynamicsDataRadGridView.TableElement.ScrollToRow(0);
                        }
                        else
                        {
                            RadMessageBox.Show("The data read from the registers may be corrupted");
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("Failed to read the device registers");
                    }
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayMainTab.AddDataToMainDynamicsDataRadGridView(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    
    }
}
