﻿namespace DMSeriesUtilities
{
    partial class CalibratePDChannels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibratePDChannels));
            this.functionDescriptionRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.selectedChannelRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.selectedChannelTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.injectionChargeTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.calibrationChannelRadButton = new Telerik.WinControls.UI.RadButton();
            this.exitRadButton = new Telerik.WinControls.UI.RadButton();
            this.twoMinutesPerChannelTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.injectionChargeRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.calculatedSensitivityTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.calculatedSensitivityValueRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.waitingForCalibrationToFinishTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.waitingForCalibrationToFinishRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            ((System.ComponentModel.ISupportInitialize)(this.functionDescriptionRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedChannelRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedChannelTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.injectionChargeTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationChannelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoMinutesPerChannelTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.injectionChargeRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculatedSensitivityTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculatedSensitivityValueRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingForCalibrationToFinishTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingForCalibrationToFinishRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // functionDescriptionRadLabel
            // 
            this.functionDescriptionRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.functionDescriptionRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.functionDescriptionRadLabel.Location = new System.Drawing.Point(12, 10);
            this.functionDescriptionRadLabel.Name = "functionDescriptionRadLabel";
            this.functionDescriptionRadLabel.Size = new System.Drawing.Size(285, 19);
            this.functionDescriptionRadLabel.TabIndex = 0;
            this.functionDescriptionRadLabel.Text = "<html>You must calibrate one channel at a time</html>";
            // 
            // selectedChannelRadSpinEditor
            // 
            this.selectedChannelRadSpinEditor.Location = new System.Drawing.Point(198, 65);
            this.selectedChannelRadSpinEditor.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.selectedChannelRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectedChannelRadSpinEditor.Name = "selectedChannelRadSpinEditor";
            // 
            // 
            // 
            this.selectedChannelRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.selectedChannelRadSpinEditor.ShowBorder = true;
            this.selectedChannelRadSpinEditor.Size = new System.Drawing.Size(33, 20);
            this.selectedChannelRadSpinEditor.TabIndex = 2;
            this.selectedChannelRadSpinEditor.TabStop = false;
            this.selectedChannelRadSpinEditor.ThemeName = "Office2007Black";
            this.selectedChannelRadSpinEditor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // selectedChannelTextRadLabel
            // 
            this.selectedChannelTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedChannelTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.selectedChannelTextRadLabel.Location = new System.Drawing.Point(65, 68);
            this.selectedChannelTextRadLabel.Name = "selectedChannelTextRadLabel";
            this.selectedChannelTextRadLabel.Size = new System.Drawing.Size(92, 15);
            this.selectedChannelTextRadLabel.TabIndex = 3;
            this.selectedChannelTextRadLabel.Text = "<html>Selected channel</html>";
            // 
            // injectionChargeTextRadLabel
            // 
            this.injectionChargeTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.injectionChargeTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.injectionChargeTextRadLabel.Location = new System.Drawing.Point(65, 100);
            this.injectionChargeTextRadLabel.Name = "injectionChargeTextRadLabel";
            this.injectionChargeTextRadLabel.Size = new System.Drawing.Size(110, 15);
            this.injectionChargeTextRadLabel.TabIndex = 4;
            this.injectionChargeTextRadLabel.Text = "<html>Injection charge (pC)</html>";
            // 
            // calibrationChannelRadButton
            // 
            this.calibrationChannelRadButton.Location = new System.Drawing.Point(12, 221);
            this.calibrationChannelRadButton.Name = "calibrationChannelRadButton";
            this.calibrationChannelRadButton.Size = new System.Drawing.Size(159, 24);
            this.calibrationChannelRadButton.TabIndex = 5;
            this.calibrationChannelRadButton.Text = "Calibrate Selected Channel";
            this.calibrationChannelRadButton.ThemeName = "Office2007Black";
            this.calibrationChannelRadButton.Click += new System.EventHandler(this.calibrationChannelRadButton_Click);
            // 
            // exitRadButton
            // 
            this.exitRadButton.Location = new System.Drawing.Point(217, 221);
            this.exitRadButton.Name = "exitRadButton";
            this.exitRadButton.Size = new System.Drawing.Size(80, 24);
            this.exitRadButton.TabIndex = 6;
            this.exitRadButton.Text = "Exit";
            this.exitRadButton.ThemeName = "Office2007Black";
            this.exitRadButton.Click += new System.EventHandler(this.exitRadButton_Click);
            // 
            // twoMinutesPerChannelTextRadLabel
            // 
            this.twoMinutesPerChannelTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoMinutesPerChannelTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.twoMinutesPerChannelTextRadLabel.Location = new System.Drawing.Point(29, 35);
            this.twoMinutesPerChannelTextRadLabel.Name = "twoMinutesPerChannelTextRadLabel";
            this.twoMinutesPerChannelTextRadLabel.Size = new System.Drawing.Size(243, 15);
            this.twoMinutesPerChannelTextRadLabel.TabIndex = 7;
            this.twoMinutesPerChannelTextRadLabel.Text = "<html>(Calibration takes about 2 minutes per channel)</html>";
            // 
            // injectionChargeRadTextBox
            // 
            this.injectionChargeRadTextBox.Location = new System.Drawing.Point(179, 98);
            this.injectionChargeRadTextBox.Name = "injectionChargeRadTextBox";
            this.injectionChargeRadTextBox.Size = new System.Drawing.Size(52, 20);
            this.injectionChargeRadTextBox.TabIndex = 8;
            this.injectionChargeRadTextBox.TabStop = false;
            this.injectionChargeRadTextBox.Text = "1000";
            this.injectionChargeRadTextBox.ThemeName = "Office2007Black";
            // 
            // calculatedSensitivityTextRadLabel
            // 
            this.calculatedSensitivityTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculatedSensitivityTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.calculatedSensitivityTextRadLabel.Location = new System.Drawing.Point(29, 139);
            this.calculatedSensitivityTextRadLabel.Name = "calculatedSensitivityTextRadLabel";
            this.calculatedSensitivityTextRadLabel.Size = new System.Drawing.Size(146, 15);
            this.calculatedSensitivityTextRadLabel.TabIndex = 9;
            this.calculatedSensitivityTextRadLabel.Text = "<html>Calculated sensitivity (nC/V)</html>";
            // 
            // calculatedSensitivityValueRadTextBox
            // 
            this.calculatedSensitivityValueRadTextBox.Location = new System.Drawing.Point(181, 137);
            this.calculatedSensitivityValueRadTextBox.Name = "calculatedSensitivityValueRadTextBox";
            this.calculatedSensitivityValueRadTextBox.Size = new System.Drawing.Size(65, 20);
            this.calculatedSensitivityValueRadTextBox.TabIndex = 10;
            this.calculatedSensitivityValueRadTextBox.TabStop = false;
            this.calculatedSensitivityValueRadTextBox.Text = "1000";
            this.calculatedSensitivityValueRadTextBox.ThemeName = "Office2007Black";
            // 
            // waitingForCalibrationToFinishTextRadLabel
            // 
            this.waitingForCalibrationToFinishTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitingForCalibrationToFinishTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.waitingForCalibrationToFinishTextRadLabel.Location = new System.Drawing.Point(46, 172);
            this.waitingForCalibrationToFinishTextRadLabel.Name = "waitingForCalibrationToFinishTextRadLabel";
            this.waitingForCalibrationToFinishTextRadLabel.Size = new System.Drawing.Size(111, 27);
            this.waitingForCalibrationToFinishTextRadLabel.TabIndex = 11;
            this.waitingForCalibrationToFinishTextRadLabel.Text = "<html>Waiting for calibration<br>to finish</html>";
            // 
            // waitingForCalibrationToFinishRadWaitingBar
            // 
            this.waitingForCalibrationToFinishRadWaitingBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.waitingForCalibrationToFinishRadWaitingBar.Location = new System.Drawing.Point(166, 178);
            this.waitingForCalibrationToFinishRadWaitingBar.Name = "waitingForCalibrationToFinishRadWaitingBar";
            this.waitingForCalibrationToFinishRadWaitingBar.Size = new System.Drawing.Size(80, 18);
            this.waitingForCalibrationToFinishRadWaitingBar.TabIndex = 36;
            this.waitingForCalibrationToFinishRadWaitingBar.Text = "radWaitingBar1";
            this.waitingForCalibrationToFinishRadWaitingBar.ThemeName = "Office2007Black";
            this.waitingForCalibrationToFinishRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.waitingForCalibrationToFinishRadWaitingBar.WaitingSpeed = 10;
            this.waitingForCalibrationToFinishRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // CalibratePDChannels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(309, 257);
            this.ControlBox = false;
            this.Controls.Add(this.waitingForCalibrationToFinishRadWaitingBar);
            this.Controls.Add(this.waitingForCalibrationToFinishTextRadLabel);
            this.Controls.Add(this.calculatedSensitivityValueRadTextBox);
            this.Controls.Add(this.calculatedSensitivityTextRadLabel);
            this.Controls.Add(this.injectionChargeRadTextBox);
            this.Controls.Add(this.twoMinutesPerChannelTextRadLabel);
            this.Controls.Add(this.exitRadButton);
            this.Controls.Add(this.calibrationChannelRadButton);
            this.Controls.Add(this.injectionChargeTextRadLabel);
            this.Controls.Add(this.selectedChannelTextRadLabel);
            this.Controls.Add(this.selectedChannelRadSpinEditor);
            this.Controls.Add(this.functionDescriptionRadLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CalibratePDChannels";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Calibrate PD Channels";
            this.ThemeName = "Office2007Black";
            ((System.ComponentModel.ISupportInitialize)(this.functionDescriptionRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedChannelRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedChannelTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.injectionChargeTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationChannelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoMinutesPerChannelTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.injectionChargeRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculatedSensitivityTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculatedSensitivityValueRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingForCalibrationToFinishTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitingForCalibrationToFinishRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel functionDescriptionRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor selectedChannelRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel selectedChannelTextRadLabel;
        private Telerik.WinControls.UI.RadLabel injectionChargeTextRadLabel;
        private Telerik.WinControls.UI.RadButton calibrationChannelRadButton;
        private Telerik.WinControls.UI.RadButton exitRadButton;
        private Telerik.WinControls.UI.RadLabel twoMinutesPerChannelTextRadLabel;
        private Telerik.WinControls.UI.RadTextBox injectionChargeRadTextBox;
        private Telerik.WinControls.UI.RadLabel calculatedSensitivityTextRadLabel;
        private Telerik.WinControls.UI.RadTextBox calculatedSensitivityValueRadTextBox;
        private Telerik.WinControls.UI.RadLabel waitingForCalibrationToFinishTextRadLabel;
        private Telerik.WinControls.UI.RadWaitingBar waitingForCalibrationToFinishRadWaitingBar;
    }
}