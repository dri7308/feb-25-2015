﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using ConfigurationObjects;
using DataObjects;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class MainDisplay
    {
//        private List<string> GetPdmDeviceHealthStatus(UInt16 deviceHealthBitEncoded)
//        {
//            List<string> statusStrings = new List<string>();
//            try
//            {
//                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(deviceHealthBitEncoded);
//                /// we are using zero-indexing
//                if (bitIsActive[0])
//                {
//                    statusStrings.Add(flashWriteErrorText);
//                }
//                if (bitIsActive[1])
//                {
//                    statusStrings.Add(flashReadErrorText);
//                }
//                if (bitIsActive[2])
//                {
//                    statusStrings.Add("Monitoring stopped, MUST be resumed by user");
//                }
//                if (bitIsActive[3])
//                {
//                    statusStrings.Add("Monitoring paused, will resume automatically");
//                }
//                if (bitIsActive[4])
//                {
//                    statusStrings.Add("Synchronization malfunction or signal loss");
//                }
//                if (bitIsActive[5])
//                {
//                    statusStrings.Add("Measuring channel did not pass calibration and test");
//                }
//                if (bitIsActive[6])
//                {
//                    statusStrings.Add("Amplitude filter channel did not pass calibration and test");
//                }
//                if (bitIsActive[7])
//                {
//                    statusStrings.Add("Time/Polarity filter channel did not pass calibration and test");
//                }
//                if (bitIsActive[8])
//                {
//                    statusStrings.Add("Clock error.  Clock time is earlier than the time of the last record in memory");
//                }
//                if (bitIsActive[9])
//                {
//                    statusStrings.Add("Error in initial reading");
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetDeviceHealthStatus(UInt16)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return statusStrings;
//        }

//        private string GetPdmEventCodeAsString(int eventCode)
//        {
//            string eventAsString = string.Empty;
//            try
//            {
//                switch (eventCode)
//                {
//                    case 1:
//                        eventAsString = "Data memory cleared";
//                        break;
//                    case 2:
//                        eventAsString = "Data and balancing info cleared";
//                        break;
//                    case 3:
//                        eventAsString = "Gamma Red Alarm";
//                        break;
//                    case 4:
//                        eventAsString = "Gamma Trend Alarm";
//                        break;
//                    case 5:
//                        eventAsString = "Power turned on";
//                        break;
//                    case 6:
//                        eventAsString = "Flash Write Failure";
//                        break;
//                    case 7:
//                        eventAsString = "Flash Read Failure";
//                        break;
//                    case 8:
//                        eventAsString = "Phase Shift Set 1";
//                        break;
//                    case 9:
//                        eventAsString = "Phase Shift Set 2";
//                        break;
//                    case 10:
//                        eventAsString = "Channels?";
//                        break;
//                    case 11:
//                        eventAsString = "Wrong Frequency";
//                        break;
//                    case 12:
//                        eventAsString = "Low Signal";
//                        break;
//                    case 13:
//                        eventAsString = "High Signal";
//                        break;
//                    case 14:
//                        eventAsString = "Unit Off";
//                        break;
//                    case 15:
//                        eventAsString = "Trend Alarm";
//                        break;
//                    case 16:
//                        eventAsString = "TK Alarm";
//                        break;
//                    case 17:
//                        eventAsString = "Set 1 Off";
//                        break;
//                    case 18:
//                        eventAsString = "Set 2 Off";
//                        break;
//                    case 19:
//                        eventAsString = "System Balanced";
//                        break;
//                    case 20:
//                        eventAsString = "Gamma Yellow Alarm";
//                        break;
//                    case 21:
//                        eventAsString = "Log was Cleared";
//                        break;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetEventCodeAsString(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return eventAsString;
//        }

//        private List<string> GetPdmEquipmentAlarmSource(UInt16 equipmentErrorBitEncoded)
//        {
//            List<string> errorStrings = new List<string>();
//            try
//            {
//                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(equipmentErrorBitEncoded);

//                if (bitIsActive[8])
//                {
//                    errorStrings.Add("Q02 - Max pulse amplitude");
//                }

//                if (bitIsActive[9])
//                {
//                    errorStrings.Add("PDI - Partial discharge intensity");
//                }

//                if (bitIsActive[10])
//                {
//                    errorStrings.Add("Q02_t - Max pulse amplitude trend");
//                }

//                if (bitIsActive[11])
//                {
//                    errorStrings.Add("PDI_t - Partial discharge intensity trend");
//                }

//                if (bitIsActive[14])
//                {
//                    errorStrings.Add("Q02_j - Max pulse amplitude change");
//                }

//                if (bitIsActive[15])
//                {
//                    errorStrings.Add("PDI_j - Partial discharge intensity change");
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetPdmEquipmentAlarmSource(UInt16)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return errorStrings;
//        }

        private void LoadChannelData(int modBusAddress, bool readArchiveData)
        {
            try
            {
                int deviceIsBusy;
                ErrorCode errorCode;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                //Dictionary<int, bool> channelIsActive = null;
                //Dictionary<int, double> channelPdiValue = null;
                //Dictionary<int, double> channelSensitivity = null;

                errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (deviceIsBusy == 0)
                    {
                        if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                        {
                            PdmDisplayFirmwareVersion(modBusAddress);
                            using (DMSeriesUtilities.LoadPhaseResolvedData loadData = new LoadPhaseResolvedData(505, modBusAddress, readArchiveData, false, this.numberOfNonInteractiveRetries, this.totalRetries))
                            {
                                loadData.ShowDialog();
                                loadData.Hide();

                                if (!loadData.Cancelled)
                                {
                                    if (readArchiveData && (loadData.PdmConfiguration != null) && (loadData.PdmSingleDataReading != null) && (loadData.PdmSingleDataReading.chPDParameters != null))
                                    {
                                        pdmDataDateRadLabel.Text = "Data date: " + loadData.PdmSingleDataReading.readingDateTime.ToString();
                                        AddDataToPdmChannelDataGrid(loadData.PdmSingleDataReading, loadData.PdmConfiguration);
                                    }
                                    else if ((loadData.PdmRegisterGeneralData != null) && (loadData.PdmRegisterChannelDataReadings != null))
                                    {
                                        pdmDataDateRadLabel.Text = "Data date: " + loadData.PdmRegisterGeneralData.readingDateTime.ToString();
                                        AddDataToPdmChannelDataGrid(loadData.PdmRegisterGeneralData, loadData.PdmRegisterChannelDataReadings);
                                    }
                                }
                            }
                            InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                        }
                    }
                    else if (deviceIsBusy == 1)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                    }
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.LoadChannelData(int, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void LoadPhaseResolvedData(int modBusAddress)
        {
            try
            {
                int deviceIsBusy;
                ErrorCode errorCode;
                Dictionary<int, bool> channelIsActive = null;
                Dictionary<int, double> channelPdiValue = null;
                Dictionary<int, double> channelSensitivity = null;
                bool nonzeroDataIsPresent = false;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (deviceIsBusy == 0)
                    {
                        if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                        {
                            PdmDisplayFirmwareVersion(modBusAddress);
                            using (DMSeriesUtilities.LoadPhaseResolvedData loadData = new LoadPhaseResolvedData(505, modBusAddress, true, true, this.numberOfNonInteractiveRetries, this.totalRetries))
                            {
                                loadData.ShowDialog();
                                loadData.Hide();

                                if (!loadData.Cancelled)
                                {
                                    /// This is the old code that AG complained about.  It will only consider channels that are officially declared
                                    /// active, as opposed to channels used for spillover phase resolved data.  I don't understand that extra phase resolved
                                    /// data thing yet.

                                    if (loadData.PdmConfiguration != null)
                                    {
                                        //    channelIsActive = new Dictionary<int, bool>();
                                        //    foreach (PDM_ConfigComponent_ChannelInfo entry in loadData.PdmConfiguration.channelInfoList)
                                        //    {

                                        //        if (entry.ChannelIsOn == 1)
                                        //        {
                                        //            channelIsActive.Add(entry.ChannelNumber, true);
                                        //        }
                                        //        else
                                        //        {
                                        //            channelIsActive.Add(entry.ChannelNumber, false);
                                        //        }
                                        //    }
                                        //}

                                        if (loadData.PdmSingleDataReading != null)
                                        {
                                            channelPdiValue = new Dictionary<int, double>();
                                            channelSensitivity = new Dictionary<int, double>();
                                           
                                            /// This way we get which channels should have data from the pdParameters.Channels data item, which is what I've been 
                                            /// using in Athena.
                                            channelIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(loadData.PdmSingleDataReading.pdParameters.Channels);

                                            for (int i = 0; i < loadData.PdmSingleDataReading.phaseResolvedData.Count; i++)
                                            {
                                                if (loadData.PdmSingleDataReading.phaseResolvedData[i].NonZeroEntries.Length > 0)
                                                {
                                                    nonzeroDataIsPresent = true;
                                                    break;
                                                }
                                            }

                                            //if (nonzeroDataIsPresent)
                                            //{
                                                foreach (PDM_DataComponent_ChPDParameters entry in loadData.PdmSingleDataReading.chPDParameters)
                                                {
                                                    channelPdiValue.Add(entry.FromChannel, entry.PDI);
                                                    channelSensitivity.Add(entry.FromChannel, entry.ChannelSensitivity);
                                                }

                                                if (loadData.PdmSingleDataReading.phaseResolvedData != null)
                                                {
                                                    pdmDataDateRadLabel.Text = "Data date: " + loadData.PdmSingleDataReading.readingDateTime.ToString();
                                                    PhaseResolvedDataViewer dataViewer = new PhaseResolvedDataViewer(loadData.PdmConfiguration, loadData.PdmSingleDataReading.readingDateTime, loadData.PdmSingleDataReading.phaseResolvedData, channelIsActive, channelSensitivity, channelPdiValue);
                                                    dataViewer.Show();
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show("Failed to load the phase resolved data");
                                                }
                                            //}
                                            //else
                                            //{
                                            //    RadMessageBox.Show(this, "No non-zero data was present for any channel");
                                            //}
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, "Failed to load the common data");
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, "Failed to load the device configuration");
                                    }
                                }
                            }
                            InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                        }
                    }
                    else if (deviceIsBusy == 1)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.LoadPhaseResolvedData(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }


//        /// <summary>
//        /// Returns a 0-indexed boolean array, reading as channel number increasing left to right, 
//        /// such that if the entry is true the channel was active
//        /// </summary>
//        /// <param name="bitString"></param>
//        /// <returns></returns>
//        private bool[] GetActiveChannels(string bitString)
//        {
//            bool[] activeChannels = null;
//            try
//            {
//                if ((bitString != null) && (bitString.Length > 14))
//                {
//                    activeChannels = new bool[15];
//                    for (int i = 0; i < 15; i++)
//                    {
//                        /// Recall that the bit string has 16 bits, even though there are only 15 channels, so
//                        /// the "most significant bit" in the string is ignored.
//                        if (bitString[15 - i].CompareTo('1') == 0)
//                        {
//                            activeChannels[i] = true;
//                        }
//                        else
//                        {
//                            activeChannels[i] = false;
//                        }
//                    }
//                }
//                else
//                {
//                    if (bitString == null) bitString = "Null";
//                    string errorMessage = "Error in MainDisplayPdmTab.GetActiveChannels(string)\nInput string is incorrect, value was " + bitString;
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetActiveChannels(string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return activeChannels;
//        }

        private void InitializePdmDataRadGridView()
        {
            try
            {
//                Grid format
//Channel  1 - 15
//Active
//mV
//nC
//Pulse Per Second
//PDI.
//Phase
                this.pdmChannelDataRadGridView.Rows.Clear();

                GridViewTextBoxColumn channelNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("ChannelNumber");
                channelNumberGridViewTextBoxColumn.Name = "ChannelNumber";
                channelNumberGridViewTextBoxColumn.HeaderText = "Ch #";
                channelNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                channelNumberGridViewTextBoxColumn.Width = 35;
                channelNumberGridViewTextBoxColumn.AllowSort = false;
                channelNumberGridViewTextBoxColumn.IsPinned = true;
                channelNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn channelIsActiveGridViewTextBoxColumn = new GridViewTextBoxColumn("Active");
                channelIsActiveGridViewTextBoxColumn.Name = "Active";
                channelIsActiveGridViewTextBoxColumn.HeaderText = "Active";
                channelIsActiveGridViewTextBoxColumn.DisableHTMLRendering = false;
                channelIsActiveGridViewTextBoxColumn.Width = 60;
                channelIsActiveGridViewTextBoxColumn.AllowSort = false;
                //channelIsActiveGridViewTextBoxColumn.IsPinned = true;
                channelIsActiveGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn magnitudeMvGridViewTextBoxColumn = new GridViewTextBoxColumn("MagnitudeMv");
                magnitudeMvGridViewTextBoxColumn.Name = "MagnitudeMv";
                magnitudeMvGridViewTextBoxColumn.HeaderText = "Magnitude (mV)";
                magnitudeMvGridViewTextBoxColumn.DisableHTMLRendering = false;
                magnitudeMvGridViewTextBoxColumn.Width = 90;
                magnitudeMvGridViewTextBoxColumn.AllowSort = false;
                //magnitudeMvGridViewTextBoxColumn.IsPinned = true;
                magnitudeMvGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn magnitudePcGridViewTextBoxColumn = new GridViewTextBoxColumn("MagnitudePc");
                magnitudePcGridViewTextBoxColumn.Name = "MagnitudePc";
                magnitudePcGridViewTextBoxColumn.HeaderText = "Magnitude (pC)";
                magnitudePcGridViewTextBoxColumn.DisableHTMLRendering = false;
                magnitudePcGridViewTextBoxColumn.Width = 90;
                magnitudePcGridViewTextBoxColumn.AllowSort = false;
                //magnitudePcGridViewTextBoxColumn.IsPinned = true;
                magnitudePcGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn pulsesPerSecondGridViewTextBoxColumn = new GridViewTextBoxColumn("PulsesPerSecond");
                pulsesPerSecondGridViewTextBoxColumn.Name = "PulsesPerSecond";
                pulsesPerSecondGridViewTextBoxColumn.HeaderText = "<html>Pulses per<br>second</html>";
                pulsesPerSecondGridViewTextBoxColumn.DisableHTMLRendering = false;
                pulsesPerSecondGridViewTextBoxColumn.Width = 80;
                pulsesPerSecondGridViewTextBoxColumn.AllowSort = false;
                //pulsesPerSecondGridViewTextBoxColumn.IsPinned = true;
                pulsesPerSecondGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn pdiGridViewTextBoxColumn = new GridViewTextBoxColumn("PDI");
                pdiGridViewTextBoxColumn.Name = "PDI";
                pdiGridViewTextBoxColumn.HeaderText = "PDI";
                pdiGridViewTextBoxColumn.DisableHTMLRendering = false;
                pdiGridViewTextBoxColumn.Width = 80;
                pdiGridViewTextBoxColumn.AllowSort = false;
                //pdiGridViewTextBoxColumn.IsPinned = true;
                pdiGridViewTextBoxColumn.ReadOnly = true;

                //GridViewTextBoxColumn phaseGridViewTextBoxColumn = new GridViewTextBoxColumn("Phase");
                //phaseGridViewTextBoxColumn.Name = "Phase";
                //phaseGridViewTextBoxColumn.HeaderText = "Phase";
                //phaseGridViewTextBoxColumn.DisableHTMLRendering = false;
                //phaseGridViewTextBoxColumn.Width = 80;
                //phaseGridViewTextBoxColumn.AllowSort = false;
                ////phaseGridViewTextBoxColumn.IsPinned = true;
                //phaseGridViewTextBoxColumn.ReadOnly = true;

                pdmChannelDataRadGridView.Columns.Add(channelNumberGridViewTextBoxColumn);
                pdmChannelDataRadGridView.Columns.Add(channelIsActiveGridViewTextBoxColumn);
                pdmChannelDataRadGridView.Columns.Add(magnitudeMvGridViewTextBoxColumn);
                pdmChannelDataRadGridView.Columns.Add(magnitudePcGridViewTextBoxColumn);
                pdmChannelDataRadGridView.Columns.Add(pulsesPerSecondGridViewTextBoxColumn);
                pdmChannelDataRadGridView.Columns.Add(pdiGridViewTextBoxColumn);
               // pdmChannelDataRadGridView.Columns.Add(phaseGridViewTextBoxColumn);
               
                pdmChannelDataRadGridView.AllowColumnReorder = false;
                pdmChannelDataRadGridView.AllowColumnChooser = false;
                // pdmChannelDataRadGridView.ShowGroupPanel = false;
                pdmChannelDataRadGridView.EnableGrouping = false;
                pdmChannelDataRadGridView.AllowAddNewRow = false;
                pdmChannelDataRadGridView.AllowDeleteRow = false;
                pdmChannelDataRadGridView.AutoScroll = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.InitializePdmDataRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToPdmDataGrid()
        {
            try
            {
                int i;
                this.pdmChannelDataRadGridView.Rows.Clear();
                for (i = 0; i < 15; i++)
                {
                    this.pdmChannelDataRadGridView.Rows.Add((i + 1).ToString(),"no value", "no value", "no value", "no value", "no value");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.AddEmptyRowsToPdmDataGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToPdmChannelDataGrid(PDM_SingleDataReading dataReading, PDM_Configuration configuration)
        {
            try
            {
                int i,j;
                string channelNumber = string.Empty;
                string channelIsActive = string.Empty;
                string gammaInMillivolts = string.Empty;
                string gammaInPicoCoulombs = string.Empty;
                string pulsesPerSecond = string.Empty;
                string PDI = string.Empty;
                
                this.pdmChannelDataRadGridView.Rows.Clear();

                j = 0;
                for (i = 0; i < 15; i++)
                {
                    channelNumber = (i+1).ToString();
                    if (configuration.channelInfoList[i].ChannelIsOn == 1)
                    {
                        channelIsActive = "yes";
                        if ((j < dataReading.chPDParameters.Count) && ((i + 1) == dataReading.chPDParameters[j].FromChannel))
                        {
                            gammaInMillivolts = Math.Round((dataReading.chPDParameters[j].Q02mV), 4).ToString();
                            gammaInPicoCoulombs = Math.Round((dataReading.chPDParameters[j].Q02mV * configuration.channelInfoList[i].ChannelSensitivity), 4).ToString();
                            pulsesPerSecond = (dataReading.chPDParameters[j].SumPDAmpl).ToString();
                            PDI = Math.Round((dataReading.chPDParameters[j].PDI), 1).ToString();
                            j++;
                        }
                        else
                        {
                            gammaInMillivolts = "missing";
                            gammaInPicoCoulombs = "missing";
                            pulsesPerSecond = "missing";
                            PDI = "missing";
                        }
                    }
                    else
                    {
                        channelIsActive = "no";
                        gammaInMillivolts = "no entry";
                        gammaInPicoCoulombs = "no entry";
                        pulsesPerSecond = "no entry";
                        PDI = "no entry";
                    }

                    this.pdmChannelDataRadGridView.Rows.Add(channelNumber, channelIsActive, gammaInMillivolts, gammaInPicoCoulombs, pulsesPerSecond, PDI);

                    //this.pdmChannelDataRadGridView.Rows[i].Cells[0].Value = channelNumber;
                    //this.pdmChannelDataRadGridView.Rows[i].Cells[1].Value = channelIsActive;
                    //this.pdmChannelDataRadGridView.Rows[i].Cells[2].Value = gammaInMillivolts;
                    //this.pdmChannelDataRadGridView.Rows[i].Cells[3].Value = gammaInPicoCoulombs;
                    //this.pdmChannelDataRadGridView.Rows[i].Cells[4].Value = pulsesPerSecond;
                    //this.pdmChannelDataRadGridView.Rows[i].Cells[5].Value = PDI;
                }
                this.pdmChannelDataRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.AddDataToPdmChannelDataGrid(PDM_SingleDataReading, PDM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToPdmChannelDataGrid(PDM_RegisterGeneralData generalData, List<PDM_RegisterChannelData> channelData)
        {
            try
            {
                int i;
                string channelNumber = string.Empty;
                string channelIsActive = string.Empty;
                string gammaInMillivolts = string.Empty;
                string gammaInNanoCoulombs = string.Empty;
                string pulsesPerSecond = string.Empty;
                string PDI = string.Empty;

                this.pdmChannelDataRadGridView.Rows.Clear();

                Dictionary<int, bool> channelIsRegistered = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(generalData.registeredChannels);

                for (i = 0; i < 15; i++)
                {
                    channelNumber = (i + 1).ToString();
                    if (channelIsRegistered[i])
                    {
                        channelIsActive = "yes";

                        gammaInMillivolts = Math.Round(channelData[i].gammaMV, 0).ToString();
                        gammaInNanoCoulombs = Math.Round(channelData[i].gammaNC/1000, 2).ToString();
                        pulsesPerSecond = channelData[i].sumPD.ToString();
                        PDI = channelData[i].pdi.ToString();
                    }
                    else
                    {
                        channelIsActive = "no";
                        gammaInMillivolts = "no entry";
                        gammaInNanoCoulombs = "no entry";
                        pulsesPerSecond = "no entry";
                        PDI = "no entry";
                    }

                    this.pdmChannelDataRadGridView.Rows.Add(channelNumber, channelIsActive, gammaInMillivolts, gammaInNanoCoulombs, pulsesPerSecond, PDI);
                }
                this.pdmChannelDataRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.AddDataToPdmChannelDataGrid(PDM_RegisterGeneralData, List<PDM_RegisterChannelData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PdmDisplayFirmwareVersion(int modBusAddress)
        {
            try
            {
                double firmwareVersion = GetFirmwareVersion(modBusAddress);
                if (firmwareVersion > .2)
                {
                    pdmFirmwareVersionValueRadLabel.Text = Math.Round(firmwareVersion, 2).ToString();
                }
                else
                {
                    RadMessageBox.Show("Failed to get the PDM firmware version");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.PdmDisplayFirmwareVersion(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
