﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class CalibratePDChannels : Telerik.WinControls.UI.RadForm
    {
        int modBusAddress;
        int numberOfNonInteractiveRetries;
        int totalRetries;

        private BackgroundWorker calibrationBackgroundWorker;

        public CalibratePDChannels(int argModBusAddress, int argNonInteractiveRetries, int argTotalRetries)
        {
            InitializeComponent();

            this.modBusAddress = argModBusAddress;
            this.numberOfNonInteractiveRetries = argNonInteractiveRetries;
            this.totalRetries = argTotalRetries;

            this.StartPosition = FormStartPosition.CenterParent;
            calculatedSensitivityValueRadTextBox.Text = "N.A.";
            InitializeCalibrationBackgroundWorker();
            this.waitingForCalibrationToFinishTextRadLabel.Visible = false;
            this.waitingForCalibrationToFinishRadWaitingBar.Visible = false;
        }

        private void calibrationChannelRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int32 injectionChargeValueFromTextBox = 0;
                UInt16 injectionChargeValueAsUint16 = 0;
                Int16 injectionChargeValue = 0;
                CalibrationInputArgument inputArgument = new CalibrationInputArgument();
                int channelNumber = (Int32)this.selectedChannelRadSpinEditor.Value;

                if (Int32.TryParse(this.injectionChargeRadTextBox.Text, out injectionChargeValueFromTextBox))
                {
                    if ((injectionChargeValueFromTextBox > 10) && (injectionChargeValueFromTextBox < 600001))
                    {
                        this.exitRadButton.Enabled = false;
                        this.calibrationChannelRadButton.Enabled = false;
                        this.waitingForCalibrationToFinishTextRadLabel.Visible = true;
                        this.waitingForCalibrationToFinishRadWaitingBar.Visible = true;
                        this.waitingForCalibrationToFinishRadWaitingBar.StartWaiting();

                        calculatedSensitivityValueRadTextBox.Text = "N.A.";
                        injectionChargeValueAsUint16 = (UInt16)(injectionChargeValueFromTextBox / 10);
                        injectionChargeValue = ConversionMethods.UInt16BytesToInt16Value(injectionChargeValueAsUint16);

                        inputArgument.channelNumber = channelNumber;
                        inputArgument.injectionCharge = injectionChargeValue;
                        inputArgument.parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                        calibrationBackgroundWorker.RunWorkerAsync(inputArgument); 
                    }
                    else
                    {
                        RadMessageBox.Show(this, "Injection charge must be between 10 and 600000 inclusive");
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Non-numeric character present in injection charge value");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CalibratePDChannels.calibrationChannelRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCalibrationBackgroundWorker()
        {
            try
            {
                /// Set up the download background worker
                calibrationBackgroundWorker = new BackgroundWorker()
                {
                    //WorkerSupportsCancellation = true,
                    //WorkerReportsProgress = true
                };

                calibrationBackgroundWorker.DoWork += calibrationBackgroundWorker_DoWork;
                calibrationBackgroundWorker.RunWorkerCompleted += calibrationBackgroundWorker_RunWorkerCompleted;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.InitializeCommandMonitorBackgroundWorker()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void calibrationBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                long deviceError;
                int deviceIsBusy;
                bool commandSucceeded;
                Int16[] registerValues;
                bool deviceIsCalibratingChannel = true;
                CalibrationInputArgument inputArgument = e.Argument as CalibrationInputArgument;
                CalibrationOutputArgument outputArgument = new CalibrationOutputArgument();
                ParentWindowInformation parentWindowInformation = inputArgument.parentWindowInformation;
                {
                    outputArgument.errorCode = SimplifiedCommunication.OpenUsbConnectionToAPdm(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    if (outputArgument.errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        if (deviceError > -1)
                        {
                            deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (deviceIsBusy < 0)
                            {
                                outputArgument.errorCode = ErrorCode.DeviceBusyReadFailed;
                            }
                            else if (deviceIsBusy == 1)
                            {
                                outputArgument.errorCode = ErrorCode.DeviceWasBusy;
                            }
                            else
                            {
                                if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                {
                                    commandSucceeded = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 1014, inputArgument.injectionCharge, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                    if (commandSucceeded)
                                    {
                                        outputArgument.errorCode = ErrorCode.CommandSucceeded;
                                        InteractiveDeviceCommunication.PDM_RunInitial(this.modBusAddress, inputArgument.channelNumber, 10, 200, parentWindowInformation);
                                        while (deviceIsCalibratingChannel)
                                        {
                                            deviceIsBusy = DeviceCommunication.DeviceIsBusy(modBusAddress, 200, 0, 1);
                                            if (deviceIsBusy == 0)
                                            {
                                                deviceIsCalibratingChannel = false;
                                            }
                                            else
                                            {
                                                Thread.Sleep(5000);
                                            }
                                        }
                                        registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, GetCalculatedSenstivityRegisterForInputChannel(inputArgument.channelNumber), 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                        if ((registerValues != null) && (registerValues.Length > 0))
                                        {
                                            this.Invoke(new MethodInvoker(() => calculatedSensitivityValueRadTextBox.Text = (Math.Round(ConversionMethods.Int16BytesToUInt16Value(registerValues[0]) / 100.0, 2)).ToString()));
                                        }
                                        else
                                        {
                                            outputArgument.errorCode = ErrorCode.CommandFailed;
                                            outputArgument.errorString = "Failed to read the calculated sensitivity";
                                            // this.Invoke(new MethodInvoker(() => calculatedSensitivityValueRadTextBox.Text = "N.A."));
                                        }
                                    }
                                    else
                                    {
                                        outputArgument.errorCode = ErrorCode.CommandFailed;
                                        outputArgument.errorString = "Failed to write the injection charge value to the device";
                                    }
                                    InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                }
                                else
                                {
                                    outputArgument.errorCode = ErrorCode.DevicePauseFailed;
                                }
                            }
                        }
                        else
                        {
                            outputArgument.errorCode = ErrorCode.DeviceErrorReadFailed;
                        }
                    }
                }
                e.Result = outputArgument;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private void calibrationBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                CalibrationOutputArgument calibrationOutputArgument;
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (e.Result != null)
                {
                    calibrationOutputArgument = e.Result as CalibrationOutputArgument;
                    if (calibrationOutputArgument != null)
                    {
                        if (calibrationOutputArgument.errorCode != ErrorCode.CommandSucceeded)
                        {
                            if (calibrationOutputArgument.errorCode == ErrorCode.CommandFailed)
                            {
                                if (calibrationOutputArgument.errorString != string.Empty)
                                {
                                    RadMessageBox.Show(this, calibrationOutputArgument.errorString);
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(calibrationOutputArgument.errorCode));
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(calibrationOutputArgument.errorCode));
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nResult from DoWork was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.exitRadButton.Enabled = true;
                this.calibrationChannelRadButton.Enabled = true;
                this.waitingForCalibrationToFinishTextRadLabel.Visible = false;
                this.waitingForCalibrationToFinishRadWaitingBar.Visible = false;
                this.waitingForCalibrationToFinishRadWaitingBar.StopWaiting();
            }
        }

        private void exitRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Int16 GetCalculatedSenstivityRegisterForInputChannel(int channelNumber)
        {
            Int16 registerNumber = 0;
            try
            {
                switch (channelNumber)
                {
                    case 1:
                        registerNumber = 1033;
                        break;
                    case 2:
                        registerNumber = 1078;
                        break;
                    case 3:
                        registerNumber = 1123;
                        break;
                    case 4:
                        registerNumber = 1168;
                        break;
                    case 5:
                        registerNumber = 1213;
                        break;
                    case 6:
                        registerNumber = 1258;
                        break;
                    case 7:
                        registerNumber = 1303;
                        break;
                    case 8:
                        registerNumber = 1348;
                        break;
                    case 9:
                        registerNumber = 1393;
                        break;
                    case 10:
                        registerNumber = 1438;
                        break;
                    case 11:
                        registerNumber = 1483;
                        break;
                    case 12:
                        registerNumber = 1528;
                        break;
                    case 13:
                        registerNumber = 1573;
                        break;
                    case 14:
                        registerNumber = 1618;
                        break;
                    case 15:
                        registerNumber = 1663;
                        break;
                    default:
                        registerNumber = -1;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CalibratePDChannels.GetCalculatedSenstivityRegisterForInputChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerNumber;
        }
    }

    public class CalibrationInputArgument
    {
        public int channelNumber;
        public Int16 injectionCharge;
        public ParentWindowInformation parentWindowInformation;
    }

    public class CalibrationOutputArgument
    {
        public ErrorCode errorCode = ErrorCode.None;
        public string errorString = string.Empty;
    }
}
