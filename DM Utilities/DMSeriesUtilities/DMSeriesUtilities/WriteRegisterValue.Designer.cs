﻿namespace DMSeriesUtilities
{
    partial class WriteRegisterValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WriteRegisterValue));
            this.registerNumberRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.registerValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.writeRegisterValueRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.registerValueRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.registerNumberRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.registerNumberRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeRegisterValueRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValueRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerNumberRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // registerNumberRadLabel
            // 
            this.registerNumberRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.registerNumberRadLabel.Location = new System.Drawing.Point(26, 27);
            this.registerNumberRadLabel.Name = "registerNumberRadLabel";
            // 
            // 
            // 
            this.registerNumberRadLabel.RootElement.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.registerNumberRadLabel.Size = new System.Drawing.Size(91, 18);
            this.registerNumberRadLabel.TabIndex = 31;
            this.registerNumberRadLabel.Text = "Register Number";
            // 
            // registerValueRadLabel
            // 
            this.registerValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.registerValueRadLabel.Location = new System.Drawing.Point(26, 67);
            this.registerValueRadLabel.Name = "registerValueRadLabel";
            // 
            // 
            // 
            this.registerValueRadLabel.RootElement.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.registerValueRadLabel.Size = new System.Drawing.Size(78, 18);
            this.registerValueRadLabel.TabIndex = 32;
            this.registerValueRadLabel.Text = "Register Value";
            // 
            // writeRegisterValueRadButton
            // 
            this.writeRegisterValueRadButton.Location = new System.Drawing.Point(12, 116);
            this.writeRegisterValueRadButton.Name = "writeRegisterValueRadButton";
            this.writeRegisterValueRadButton.Size = new System.Drawing.Size(118, 34);
            this.writeRegisterValueRadButton.TabIndex = 33;
            this.writeRegisterValueRadButton.Text = "Write New Value";
            this.writeRegisterValueRadButton.ThemeName = "Office2007Black";
            this.writeRegisterValueRadButton.Click += new System.EventHandler(this.writeRegisterValueRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Location = new System.Drawing.Point(148, 116);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(118, 34);
            this.cancelRadButton.TabIndex = 34;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // registerValueRadSpinEditor
            // 
            this.registerValueRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerValueRadSpinEditor.Location = new System.Drawing.Point(159, 67);
            this.registerValueRadSpinEditor.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.registerValueRadSpinEditor.Name = "registerValueRadSpinEditor";
            // 
            // 
            // 
            this.registerValueRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.registerValueRadSpinEditor.ShowBorder = true;
            this.registerValueRadSpinEditor.Size = new System.Drawing.Size(107, 20);
            this.registerValueRadSpinEditor.TabIndex = 36;
            this.registerValueRadSpinEditor.TabStop = false;
            this.registerValueRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // registerNumberRadTextBox
            // 
            this.registerNumberRadTextBox.Location = new System.Drawing.Point(159, 24);
            this.registerNumberRadTextBox.Name = "registerNumberRadTextBox";
            this.registerNumberRadTextBox.ReadOnly = true;
            this.registerNumberRadTextBox.Size = new System.Drawing.Size(94, 20);
            this.registerNumberRadTextBox.TabIndex = 37;
            this.registerNumberRadTextBox.TabStop = false;
            this.registerNumberRadTextBox.Text = "radTextBox1";
            // 
            // WriteRegisterValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(286, 162);
            this.Controls.Add(this.registerNumberRadTextBox);
            this.Controls.Add(this.registerValueRadSpinEditor);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.writeRegisterValueRadButton);
            this.Controls.Add(this.registerValueRadLabel);
            this.Controls.Add(this.registerNumberRadLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WriteRegisterValue";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Write Register Value";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.WriteRegisterValue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.registerNumberRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeRegisterValueRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValueRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerNumberRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel registerNumberRadLabel;
        private Telerik.WinControls.UI.RadLabel registerValueRadLabel;
        private Telerik.WinControls.UI.RadButton writeRegisterValueRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadSpinEditor registerValueRadSpinEditor;
        private Telerik.WinControls.UI.RadTextBox registerNumberRadTextBox;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
    }
}