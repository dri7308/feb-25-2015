﻿namespace DMSeriesUtilities
{
    partial class LoadPhaseResolvedData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadPhaseResolvedData));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.dataDownloadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(12, 97);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(228, 30);
            this.cancelRadButton.TabIndex = 17;
            this.cancelRadButton.Text = "Cancel Data Load";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // dataDownloadRadProgressBar
            // 
            this.dataDownloadRadProgressBar.Dash = false;
            this.dataDownloadRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDownloadRadProgressBar.Location = new System.Drawing.Point(12, 38);
            this.dataDownloadRadProgressBar.Name = "dataDownloadRadProgressBar";
            this.dataDownloadRadProgressBar.Size = new System.Drawing.Size(228, 46);
            this.dataDownloadRadProgressBar.TabIndex = 18;
            this.dataDownloadRadProgressBar.Text = "Loading data from device";
            this.dataDownloadRadProgressBar.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoadPhaseResolvedData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(255, 139);
            this.Controls.Add(this.dataDownloadRadProgressBar);
            this.Controls.Add(this.cancelRadButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadPhaseResolvedData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Loading Data";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.LoadPhaseResolvedData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDownloadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadProgressBar dataDownloadRadProgressBar;
    }
}