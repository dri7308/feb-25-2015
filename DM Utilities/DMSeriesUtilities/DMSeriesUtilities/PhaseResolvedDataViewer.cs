﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using ChartDirector;

using ConfigurationObjects;
using DataObjects;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class PhaseResolvedDataViewer : Telerik.WinControls.UI.RadForm
    {
        /// Note:  This class was ripped from the PD_MonitorUtilities dll and hacked up
        /// to work here with one data reading.  It's a mishmash of code.
        /// 
        /// 
        /// 
        /// Design notes:
        /// 
        /// This is a fairly complicated class, so I did some things to simplify the code before it exploded.
        /// One of the first things one might notice is that I'm using Lists often.  I use lists to hold classes I
        /// created specifically to contain the data read from the database, and I also use lists to point
        /// to the instances of WinChartViewer associated with drawing the matrix data.  Initially I did not do that,
        /// instead I created an instance of each class for each channel, and referenced them all separately.  This
        /// required a lot of functions that contained switch statements in order to properly reference each named
        /// object.  That started to get quite complicated, and I knew it would be difficult to maintain.  So, I moved
        /// to the list representation.
        /// 
        /// Because they are created using VS, I kept the separate instances of WinChartViewer as opposed to creating them
        /// by hand(which would, of course, be possible, but probably would not simplify things all that much).  I think right
        /// now that the only time I directly refer to any instance of WinChartViewer with respect to the matrix charts is
        /// in the code that lays the charts out after the user resizes the window or selects a different chart layout.
        /// Otherwise, I try to refer to the charts using the list I insert them into.  As far as I can tell, that referncing 
        /// seems to work fine.
        /// 
        /// Except for a few specialized lists that have arrays in them that are directly accessed, all lists are loaded one
        /// time, at program load, and then never added to again.  The arrays internal to the classes are allocated anew during
        /// every data load.  This chews up the heap a bit, but it ensures that no data bleed across by mistake.  The use of lists
        /// in more places would prevent that, but the ChartDirector program likes data in simple arrays, so this is also a decision
        /// based on preserving some kind of speed of execution.
        /// 
        /// These arrays are generally organized into classes whenever possible to avoid getting lost in the data.  A class with the name
        /// "Data" in it is set up to handle one channel's worth of information over all data readings recorded.  For instance, the 
        /// ChannelData instance holds all data readings for one channel, and a MatrixData instance holds all phase resolved data for one channel
        /// and one polarity.  They are contained in lists of 15 entries, one entry for each channel.  For the phase resolved data, two lists are required,
        /// one for each polarity.
        /// 
        /// The LoadData function, as well as the rest of the code, has been set up (hopefully) to account for various anomolies 
        /// in the data as it is read from the database.  The proper alignment of all data with respect to the date of the data reading
        /// is ensured by inserting 0-valued data whenever there is not an entry for a particular channel or matrix.  I also track 
        /// which channels are active, and account for the skipping of channels that are inactive if they are followed by an active
        /// channel.
        ///
        /// One thing you should definitely realize is that while we generally refer to channels 1-15 when referencing them 
        /// external to this code (such as in the interface the user sees, or in the database data), internally I have used 0-indexing
        /// for the channels.  Therefore, channel 1 externally is channel 0 internally, and so forth, ending with channel 14 internally.
        /// This might cause some confusion to the code reader, and to any programmers that touch it later, so be warned.
        ///
        /// Because there is the possiblity of skipping channels that are active, I had to come up with the notion of "active channels" 
        /// and "display channels."  An active channel is basically one that is signaled to the user as being available for data display.
        /// Those channels can allow for skipping inactive channels.  A display channel is different in that one should not put blank spaces
        /// on the GUI where a matrix chart should be, so if, for instance, there is no data for channel three, one would go ahead and graph
        /// channel 4 data in the channel three position.  All graphs are lableled with the channel number of the data being shown.
        /// When there is no skipping in channels assigned in the PDM, it doesn't matter and
        /// one can think of active channels and display channels as being the same thing.
        /// 
        /// Other notes:
        /// 
        /// To suppress the radpageview upper-right-hand stuff, go to Properties -> View Element -> StripButtons and select "None."
        ///
        ///

        //private static string htmlPrefix = "<html>";
        //private static string htmlPostfix = "</html>";
        //private static string htmlFontType = "<font=Microsoft Sans Serif>";
        //private static string htmlStandardFontSize = "<size=8.25>";

        //private static string noDataForSelectedDataAgeText = "No data available for the selected Data Age";
        //private static string copyGraphText = "Copy Graph";
        private static string addText = "add";
        private static string multiplyText = "multiply";

        //private static string loadDataByAgeToolTipText = "Load data by its age with respect to the current date";
        //private static string useRightMouseToSelectOrUnselectAllToolTipText = "Use right mouse to select/unselect all";
        //private static string useRightMouseToOpenDynamicsEditorToolTipText = "Use right mouse to open dynamics editor";

        //private static string failedToReadAnyDataToSaveToCsvFile = "No data was available to save to the csv file";

        //private static string selectAllRadButtonText = "All";
        //private static string unselectAllRadButtonText = "None";

        private static string phaseResolvedPartialDischargeDistributionText = "Phase resolved partial discharge distribution";
        private static string phaseResolvedPartialDischargeDistributionPolarText = "Phase resolved partial discharge distribution in polar representation";
        private static string phaseResolvedPartialDischargeDistributionThreeDeeText = "Phase resolved partial discharge distribution in 3D representation";
        private static string pulseHeightDistributionText = "Pulse height distribution";

        //private static string idParametersGridText = "ID";
        //private static string dataRootIdParametersGridText = "Data Root ID";
        //private static string readingDateParametersGridText = "Reading Date";
        //private static string tempfileParametersGridText = "Tempfile";
        //private static string pdExistsParametersGridText = "PD Exists";
        //private static string alarmEnableParametersGridText = "Alarm Enable";
        //private static string initialParametersGridText = "Initial";

        //private static string humidityRadCheckBoxText = "Humidity (%)";
        //private static string temp1RadCheckBoxText = "Correlating Temp (ºC)";
        //private static string temp2RadCheckBoxText = "Temp 2 (ºC)";
        //private static string temp3RadCheckBoxText = "Temp 3 (ºC)";
        //private static string temp4RadCheckBoxText = "Temp 4 (ºC)";
        //private static string loadCurrent1RadCheckBoxText = "Load Current 1 (A)";
        //private static string loadCurrent2RadCheckBoxText = "Load Current 2 (A)";
        //private static string loadCurrent3RadCheckBoxText = "Load Current 3 (A)";
        //private static string voltage1RadCheckBoxText = "Voltage 1 (kV)";
        //private static string voltage2RadCheckBoxText = "Voltage 2 (kV)";
        //private static string voltage3RadCheckBoxText = "Voltage 3 (kV)";

        //private static string idPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>ID</html>";
        //private static string dataRootIdPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Data Root ID</html>";
        //private static string readingDatePdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Reading Date</html>";
        //private static string activeChannelsPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Active Channels</html>";
        //private static string matrixPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Matrix</html>";
        //private static string maxPDIChannelPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>MaxPDIChannel</html>";
        //private static string maxQ02ChannelPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>MaxQ02Channel</html>";
        //private static string numberOfPeriodsPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Number of<br>Periods</html>";
        //private static string frequencyPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Frequency</html>";
        //private static string pSpeed0PdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PSpeed_0</html>";
        //private static string pSpeed1PdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PSpeed_1</html>";
        //private static string pJump0PdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PJump_0</html>";
        //private static string pJump1PdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PJump_1</html>";
        //private static string statePdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>State</html>";
        //private static string zoneWidthPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Zone Width</html>";
        //private static string zonesCountPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Zones Count</html>";
        //private static string angleStepPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Angle Step</html>";
        //private static string anglesCountPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Angles Count</html>";
        //private static string tablesOnChannelPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Tables on<br>Channel</html>";

        //private static string idChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>ID</html>";
        //private static string dataRootIdChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Data Root ID</html>";
        //private static string readingDateChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Reading Date</html>";
        //private static string channelNumberChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Channel<br>Number</html>";
        //private static string withChannelsChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>With<br>Channels</html>";
        //private static string channelSensitivityChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Channel<br>Sensitivity</html>";
        //private static string q02ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02</html>";
        //private static string q02PlusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02 Plus</html>";
        //private static string q02MinusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02 Minus</html>";
        //private static string q02MvChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02 mV</html>";
        //private static string pdiChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PDI</html>";
        //private static string pdiPlusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PDI Plus</html>";
        //private static string pdiMinusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PDI Minus</html>";
        //private static string sumPdAmplitudeChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Sum PD<br>Amplitude</html>";
        //private static string sumPdPlusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Sum PD<br>Plus</html>";
        //private static string sumPdMinusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Sum PD<br>Minus</html>";
        //private static string pdiTChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PDI_t</html>";
        //private static string pdiJChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>PDI_j</html>";
        //private static string q02TChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02_t</html>";
        //private static string q02JChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Q02_j</html>";
        //private static string separations0ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Separations_0</html>";
        //private static string refShift0ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>RefShift_0</html>";
        //private static string wasBlockedByTChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Was Blocked<br>by T</html>";
        //private static string wasBlockedByPChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Was Blocked<br>by P</html>";
        //private static string ratedVoltageChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Rated<br>Voltage</html>";
        //private static string ratedCurrentChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Rated<br>Current</html>";
        //private static string stateChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>State</html>";
        //private static string alarmStatusChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>Alarm<br>Status</html>";
        //private static string p0ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_0</html>";
        //private static string p1ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_1</html>";
        //private static string p2ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_2</html>";
        //private static string p3ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_3</html>";
        //private static string p4ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_4</html>";
        //private static string p5ChPdParametersGridText = "<html><font=Microsoft Sans Serif><size=8.25>P_5</html>";

        //private static string idPhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>ID</html>";
        //private static string dataRootIdPhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>Data Root ID</html>";
        //private static string readingDatePhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>Reading Date</html>";
        //private static string channelNumberPhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>Channel<br>Number</html>";
        //private static string phasePolarityPhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>Phase<br>Polarity</html>";
        //private static string nonzeroEntriesPhaseResolvedDataGridText = "<html><font=Microsoft Sans Serif><size=8.25>NonZero Entries</html>";

        //private static string pdmDataViewerInterfaceTitleText = "Partial Discharge Monitor Data Viewer";

        // labels that appear in many tabs
        //private static string pointerRadioButtonText = "Pointer";
        //private static string zoomInRadioButtonText = "Zoom In";
        //private static string zoomOutRadioButtonText = "Zoom Out";
        //private static string dataAgeRadGroupBoxText = "Data Age";
        //private static string threeMonthsRadRadioButtonText = "3 Months";
        //private static string sixMonthsRadRadioButtonText = "6 Months";
        //private static string oneYearRadRadioButtonText = "1 Year";
        //private static string allDataRadRadioButtonText = "All";
        //private static string graphMethodRadGroupBoxText = "Graph Method";
        //private static string automaticDrawRadRadioButtonText = "Automatic Draw";
        //private static string manualDrawRadRadioButtonText = "Manual Draw";
        //private static string drawGraphsRadButtonText = "Draw Graph";
        //private static string chartLayoutRadGroupBoxText = "Chart Layout";
        //private static string threeByFiveRadRadioButtonText = "3x5";
        //private static string fiveByThreeRadRadioButtonText = "5x3";
        //private static string eightByTwoRadRadioButtonText = "8x2";
        //private static string fifteenByOneRadRadioButtonText = "15x1";
        //private static string displaySettingsRadGroupBoxText = "Display Settings";
        //private static string scaleRadGroupBoxText = "Scale";
        //private static string initialPhaseRadButtonText = "Common Phase Shift";
        //private static string amplitudeRadGroupBoxText = "Amplitude";
        //private static string dbRadRadioButtonText = "dB";
        //private static string vRadRadioButtonText = "V";
        //private static string PcRadRadioButtonText = "nC";
        //private static string dataDateRadGroupBoxText = "Data Date";

        // labels for each separate object
        //private static string trendRadPageViewPageText = "Trend";
        //private static string channelSelectRadGroupBoxText = "Select Channels to Display";
        //private static string channel1SelectRadCheckBoxText = "1";
        //private static string channel2SelectRadCheckBoxText = "2";
        //private static string channel3SelectRadCheckBoxText = "3";
        //private static string channel4SelectRadCheckBoxText = "4";
        //private static string channel5SelectRadCheckBoxText = "5";
        //private static string channel6SelectRadCheckBoxText = "6";
        //private static string channel7SelectRadCheckBoxText = "7";
        //private static string channel8SelectRadCheckBoxText = "8";
        //private static string channel9SelectRadCheckBoxText = "9";
        //private static string channel10SelectRadCheckBoxText = "10";
        //private static string channel11SelectRadCheckBoxText = "11";
        //private static string channel12SelectRadCheckBoxText = "12";
        //private static string channel13SelectRadCheckBoxText = "13";
        //private static string channel14SelectRadCheckBoxText = "14";
        //private static string channel15SelectRadCheckBoxText = "15";
        //private static string magnitudeRadGroupBoxText = "Magnitude";
        //private static string qmaxRadCheckBoxText = "Qmax";
        //private static string milliVoltsRadRadioButtonText = "mV";
        //private static string nanoCoulombsRadRadioButtonText = "nC";
        //private static string qmaxPlusRadCheckBoxText = "Qmax Plus (mV)";
        //private static string qmaxTrendRadCheckBoxText = "Qmax Trend (times/year)";
        //private static string qmaxMinusRadCheckBoxText = "Qmax Minus (mV)";
        //private static string intensityRadGroupBoxText = "Intensity";
        //private static string pdiRadCheckBoxText = "PDI (mW)";
        //private static string pdiTrendRadCheckBoxText = "PDI Trend (times/year)";
        //private static string pdiPlusRadCheckBoxText = "PDI Plus (mW)";
        //private static string pdiMinusRadCheckBoxText = "PDI Minus (mW)";
        //private static string pulseCountRadGroupBoxText = "Pulse Count";
        //private static string pulseCountRadCheckBoxText = "Pulse Count";
        //private static string pulseCountPlusRadCheckBoxText = "Pulse Count Plus";
        //private static string pulseCountMinusRadCheckBoxText = "Pulse Count Minus";
        //private static string pulsesPerCycleRadRadioButtonText = "Pulses Per Cycle";
        //private static string pulsesPerSecondRadRadioButtonText = "Pulses Per Second";
        //private static string trendDisplayOptionsRadGroupBoxText = "Display Options";
        //private static string showTrendLineRadCheckBoxText = "Show Trend Line";
        //private static string linearTrendRadRadioButtonText = "Linear";
        //private static string exponentialTrendRadRadioButtonText = "Exponential";
        //private static string normalizeDataRadCheckBoxText = "Normalize Data";
        //private static string movingAverageRadCheckBoxText = "Show Moving Avg";
        //private static string dataItemsRadLabelText = "data items";
        //private static string dynamicsRadGroupBoxText = "Dynamics";

        //private static string matrixRadPageViewPageText = "PRPDD";
        //private static string matrixDisplaySettingsRadGroupBoxText = displaySettingsRadGroupBoxText;
        //private static string matrixScaleRadGroupBoxText = scaleRadGroupBoxText;
        //private static string matrixInitialPhaseRadButtonText = initialPhaseRadButtonText;
        //private static string matrixAmplitudeRadGroupBoxText = amplitudeRadGroupBoxText;
        //private static string matrixDbRadRadioButtonText = dbRadRadioButtonText;
        //private static string matrixVRadRadioButtonText = vRadRadioButtonText;
        //private static string matrixPcRadRadioButtonText = PcRadRadioButtonText;

        //private static string prpddPolarRadPageViewPageText = "PRPDD Polar";
        ////private static string prpddPolarDisplaySettingsRadGroupBoxText = displaySettingsRadGroupBoxText;
        //private static string showRadialLabelsRadCheckBoxText = "Show Radial Labels";
        ////private static string prpddPolarScaleRadGroupBoxText = scaleRadGroupBoxText;
        ////private static string prpddPolarInitialPhaseRadButtonText = initialPhaseRadButtonText;
        ////private static string prpddPolarAmplitudeRadGroupBoxText = amplitudeRadGroupBoxText;
        ////private static string prpddPolarDbRadRadioButtonText = dbRadRadioButtonText;
        ////private static string prpddPolarVRadRadioButtonText = vRadRadioButtonText;
        ////private static string prpddPolarPcRadRadioButtonText = PcRadRadioButtonText;

        //private static string threeDeeRadPageViewPageText = "PRPDD 3D";
        ////private static string threeDeeScaleRadGroupBoxText = scaleRadGroupBoxText;
        ////private static string threeDeeInitialPhaseRadButtonText = initialPhaseRadButtonText;
        ////private static string threeDeeAmplitudeRadGroupBoxText = amplitudeRadGroupBoxText;
        ////private static string threeDeeDbRadRadioButtonText = dbRadRadioButtonText;
        ////private static string threeDeeVRadRadioButtonText = vRadRadioButtonText;
        ////private static string threeDeePcRadRadioButtonText = PcRadRadioButtonText;
        ////private static string threeDeeDataDateRadGroupBoxText = dataDateRadGroupBoxText;

        //private static string phdRadPageViewPageText = "PHD";
        ////private static string phdScaleRadGroupBoxText = scaleRadGroupBoxText;
        ////private static string phdInitialPhaseRadButtonText = initialPhaseRadButtonText;
        ////private static string phdAmplitudeRadGroupBoxText = amplitudeRadGroupBoxText;
        ////private static string phdDbRadRadioButtonText = dbRadRadioButtonText;
        ////private static string phdVRadRadioButtonText = vRadRadioButtonText;
        ////private static string phdPcRadRadioButtonText = PcRadRadioButtonText;
        ////private static string phdDataDateRadGroupBoxText = dataDateRadGroupBoxText;

        //private static string dataGridRadPageViewPageText = "Data Grid";
        //private static string expandAllRadButtonText = "Expand All";
        //private static string hideAllRadButtonText = "Hide All";
        //private static string gridDataEditRadButtonText = "Edit Data";
        //private static string gridDataSaveChangesRadButtonText = "Save Changes";
        //private static string gridDataCancelChangesRadButtonText = "Cancel Changes";
        //private static string gridDataDeleteSelectedRadButtonText = "Delete Selected Entries";

        enum ChartType
        {
            PRPDD,
            PRPDDPolar,
            PHD,
            ThreeDee
        };

        enum ScaleType
        {
            MilliWatts,
            TimesPerYear,
            MilliVolts,
            NanoCoulombs,
            PulsesPerSecond,
            PulsesPerCycle,
            Percent,
            DegreesCentigrade,
            KilogramsPerMeterCubed,
            Unitless,
            KiloVolts,
            Amps
        };

        public class ScaleLabels
        {
            public static string Percent = "%";
            public static string Unitless = "";
            public static string TimesPerYear = "times/\nyear";
            public static string DegreesCentigrade = "ºC";
            public static string KilogramsPerMeterCubed = "kg/M3";
            public static string MilliWatts = "mW";
            public static string MilliVolts = "mV";
            public static string NanoCoulombs = "nC";
            public static string PulsesPerSecond = "pulse/\nsec";
            public static string PulsesPerCycle = "pulse/\ncycle";
            public static string KiloVolts = "kV";
            public static string Amps = "Amps";
        }

        /// <summary>
        /// Provides a short-hand way of referencing the different data types
        /// </summary>
        enum ChannelDataType
        {
            MagnitudeNC,
            MagnitudeMV,
            MagnitudePlus,
            MagnitudeMinus,
            PDI,
            PDIPlus,
            PDIMinus,
            MagnitudeTrend,
            PDITrend,
            PulseCountPerSecond,
            PulseCountPerCycle,
            PulseCountPlusPerSecond,
            PulseCountPlusPerCycle,
            PulseCountMinusPerSecond,
            PulseCountMinusPerCycle
        }

        enum AmplitudeDisplayType
        {
            dB,
            V,
            pC
        }

        /// <summary>
        /// The monitor ID of the monitor whose data is being displayed
        /// </summary>
       // Guid monitorID;

        CommonData commonData = new CommonData();

        /// <summary>
        /// A list of the max voltages recorded in the pair of matrices (phase resolved data) for each channel, with the array holding the maximum voltage for each date a matrix was recorded
        /// </summary>
        List<double> channelMaxMatrixVoltagesList;

        /// <summary>
        /// This list contains a ChannelData entry for every channel.  A ChannelData object contains all the data for one channel over all data readings downloaded
        /// </summary>
        List<ChannelData> channelDataList;

        /// <summary>
        /// This list contains the "Plus" phase MatrixData entries for every channel.  A MatrixData object contains all phase resolved data for a given polarity over all data readings downloaded.
        /// </summary>
        List<MatrixData> channelPlusMatrixDataList;

        /// <summary>
        /// This list contains the "Minus" phase MatrixData entries for every channel.  A MatrixData object contains all phase resolved data for a given polarity  over all data readings downloaded.
        /// </summary>
        List<MatrixData> channelMinusMatrixDataList;

        /// <summary>
        /// This contains information on whether there are any phase resolved data entries for the given channel and data reading date.  It is used to determine whether the create the PRPDD and other 
        /// graphs of phase resolved data.
        /// </summary>
        List<bool> channelMatrixDataIsNotEmpty;

        /// <summary>
        /// This contains all instances of type matrixChartXWinChartViewer, where X is replaced by the channel number.
        /// </summary>
        List<WinChartViewer> matrixChartWinChartViewerList;

        /// <summary>
        /// This is a list of all the group boxes containing additional graphic elements associated with a matrixChartXWinChartViewer instance.
        /// </summary>
        List<RadGroupBox> matrixChartRadGroupBoxList;

        /// <summary>
        /// This is a list of all the matrixChartXPhaseShiftRadDropDownList items associated with a matrixChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadDropDownList> matrixChartPhaseShiftRadDropDownListList;

        /// <summary>
        /// List of all of the matrixChartXPDIValueRadLabel items associated with a matrixChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadLabel> matrixChartPDIValueRadLabelList;

        /// <summary>
        /// This contains all instances of type prpddPolarChartXWinChartViewer, where X is replaced by the channel number.
        /// </summary>
        List<WinChartViewer> prpddPolarChartWinChartViewerList;

        /// <summary>
        /// This is a list of all the group boxes containing additional graphic elements associated with a prpddPolarChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadGroupBox> prpddPolarChartRadGroupBoxList;

        /// <summary>
        /// This is a list of all the prpddPolarChartXPhaseShiftRadDropDownList items associated with a prpddPolarChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadDropDownList> prpddPolarChartPhaseShiftRadDropDownListList;

        /// <summary>
        /// List of all of the prpddPolarChartXPDIValueRadLabel items associated with a prpddPolarChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadLabel> prpddPolarChartPDIValueRadLabelList;

        /// <summary>
        /// This contains all instances of type threeDeeChartXWinChartViewer, where X is replaced by the channel number.
        /// </summary>
        List<WinChartViewer> threeDeeChartWinChartViewerList;

        /// <summary>
        /// This is a list of all the group boxes containing additional graphic elements associated with a threeDeeChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadGroupBox> threeDeeChartRadGroupBoxList;

        /// <summary>
        /// This is a list of all the threeDeeChartXPhaseShiftRadDropDownList items associated with a prpddPolarChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadDropDownList> threeDeeChartPhaseShiftRadDropDownListList;

        /// <summary>
        /// List of all of the threeDeeChartXPDIValueRadLabel items associated with a prpddPolarChartXWinChartViewer instance, where X is replaced by the channel number.
        /// </summary>
        List<RadLabel> threeDeeChartPDIValueRadLabelList;

        /// <summary>
        /// This contains all instances of type phdChartXWinChartViewer, where X is replaced by the channel number.
        /// </summary>
        List<WinChartViewer> phdChartWinChartViewerList;

        /// <summary>
        /// This is a list of all the group boxes containing additional graphic elements associated with a phdChartXWinChartViewer instance.
        /// </summary>
        List<RadGroupBox> phdChartRadGroupBoxList;

        /// <summary>
        /// List of all PDI value labels for PHD charts
        /// </summary>
        List<RadLabel> phdChartRadLabelList;

        /// <summary>
        /// A 0-indexed list of the active channels
        /// </summary>
        List<int> activeChannelNumberList;

        AmplitudeDisplayType matrixAmplitudeDisplayType;
        AmplitudeDisplayType prpddPolarAmplitudeDisplayType;
        AmplitudeDisplayType threeDeeAmplitudeDisplayType;
        AmplitudeDisplayType phdAmplitudeDisplayType;

        /// <summary>
        /// The keeps track of the maximum voltage seen in the phase resolved data over all channels for each data reading date.  It is
        /// used to set the graph limits for the PRPDD graphs.
        /// </summary>
        double maxVoltageOverAllChannels;

        /// <summary>
        /// Used to create the sine wave in the PRPDD graphs.
        /// </summary>
        double[] sineWaveAmplitudeData;

        /// <summary>
        /// Used to create the sine wave in the PRPDD graphs.
        /// </summary>
        double[] sineWaveDegreeData;

        /// <summary>
        /// Saves a count of how many times a given channel was active over the entire set of data readings.  Mismatches among channels may
        /// indicate a need to adjust the graphs at some point.
        /// </summary>
        private int activeChannelCount;

        /// <summary>
        /// Indicates whether a given channel has any data associated with it.
        /// </summary>
        private Dictionary<int, bool> channelIsActive;

        /// <summary>
        /// Indicates if a given channel was active for the current matrix date selected
        /// </summary>
       // private bool[] activeChannelsForSelectedDate;

        /// <summary>
        /// The total number of common/channel regular data readings
        /// </summary>
        int totalDataReadings;

        /// <summary>
        /// The number of times the phase resolved data were saved and available for graphing.
        /// </summary>
       // int totalTimesMatriciesWereSaved;

        /// <summary>
        /// The highest channel number seen in the data, used as a limit for looping over the channel
        /// data.
        /// </summary>
       // int highestChannelNumber;

        int initialMultiChartWidth;

        int initialMultiChartHeight;

        /// <summary>
        /// This is used to fix the aspect ration of all multi-chart graphs.
        /// </summary>
        //this is from the size of the charts in a 3x5 layout, which is "default"
        double multiChartWidthToHeightRatio;

        /// <summary>
        /// The current height for an individual multi-chart graph.
        /// </summary>
        int multiChartHeight;

        /// <summary>
        /// The current width for an individual multi-chart graph.
        /// </summary>
        int multiChartWidth;

        /// <summary>
        /// The current number of colums used to display the multi-chart graphs.
        /// </summary>
        int multiChartNumberOfColumns;

        /// <summary>
        /// The minimum width of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumWidth = 720;

        /// <summary>
        /// The minimum height of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumHeight = 660;

        /// <summary>
        /// The minimum width of the trend graph
        /// </summary>
        int trendGraphWidthMinimum = 640;

        /// <summary>
        /// The minimum height of the trend graph
        /// </summary>
        int trendGraphHeightMinimum = 510;

        /// <summary>
        /// The minimum amount the trend graph is offset from the left border of the trendWinChartViewer object
        /// </summary>
        int trendGraphOffsetX = 55;

        /// <summary>
        /// The minimum amount the trend graph is offset from the top border of the trendWinChartViewer object
        /// </summary>
        int trendGraphtOffsetY = 100;

        /// <summary>
        /// The current amount the trend graph is offset from the left border of the trendWinChartViewer object.  Increased
        /// offset makes room for the different scales possible for the y axis.
        /// </summary>
        int trendGraphCurrentOffsetX;

        /// <summary>
        /// The maximum Y value that can currently be displayed in the trend chart
        /// </summary>
        double trendChartMaxYValue;

        /// <summary>
        /// The minimum Y value that can currently be displayed in the trend chart
        /// </summary>
        double trendChartMinYValue;

        /// <summary>
        /// The index into the dataTimeAsDouble array associated with the date the cursor is currently covering
        /// </summary>
        int cursorDateTimeAsDoubleIndex = -1;

        /// <summary>
        /// The value in the dataTImesAsDouble array assoicated with the current cursor position
        /// </summary>
        double cursorXval = -1.0;

        /// <summary>
        /// A pointer to preserve the instance of the trend chart after it is created in DrawTrendChart(), used 
        /// to calculate the postion of the cursor.
        /// </summary>
        XYChart trendXYChart;

        /// <summary>
        /// The position where the display for x values on trend graph starts, used to compute the cursor position
        /// and whether it should be visible.
        /// </summary>
        int trendGraphXStart;

        /// <summary>
        /// The position where the display for y values on trend graph starts, used to compute the cursor position
        /// and whether it should be visible.
        /// </summary>
        int trendGraphYStart;

        /// <summary>
        /// The height of the trend graph in graph units, used to compute the height of the cursor
        /// </summary>
        int trendGraphHeight;

        /// <summary>
        /// The width of the trend graph in graph units, mostly used to determine whether the cursor should be
        /// visible or not.
        /// </summary>
        int trendGraphWidth;

        /// <summary>
        /// Used to determine whether to update the trend chart when switching to it from another tab
        /// </summary>
        bool trendChartNeedsUpdating = false;

        /// <summary>
        /// Used to determine whether to update the PRPDD charts when switching to them from another tab
        /// </summary>
        bool matrixChartsNeedUpdating = false;

        /// <summary>
        /// Used to determine whether to update the PRPDDPolar charts when switching to them from another tab
        /// </summary>
        bool prpddPolarChartsNeedUpdating = false;

        /// <summary>
        /// Used to determine whether to update the 3D charts when switching to them from another tab
        /// </summary>
        bool threeDeeChartsNeedUpdating = false;

        /// <summary>
        /// Used to determine whether to update the phd charts when switching to them from another tab
        /// </summary>
        bool phdChartsNeedUpdating = false;

        /// <summary>
        /// The minimum data reading in the data set currently loaded in memory
        /// </summary>
        DateTime minimumDateOfDataSet;

        /// <summary>
        /// The total number of seconds spanned by the data set currently loaded in memory
        /// </summary>
        double dateRange;

        /// <summary>
        /// The double equivalent of all the dates of the data readings, with the values given by a ChartDirector
        /// converions routine.
        /// </summary>
        double[] allDateTimesAsDouble;

        /// <summary>
        /// The dates of all the phase resolved data saves
        /// </summary>
        DateTime[] allMatrixDateTimes;

        /// <summary>
        /// The double equivalent of the dates of all the phase resolved data saves
        /// </summary>
        double[] allMatrixDateTimesAsDouble;

        /// <summary>
        /// The current selected index into the phase resolved data save dates, used to determine which data to select
        /// to create the multi-chart graphs (PRPDD, 3D, PHD)
        /// </summary>
        int allMatrixDateTimeSelectedIndex = -1;

        /// <summary>
        /// The index into the data reading dates for all data for the same date as the currently selected date for the matrix data
        /// </summary>
        int allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex;

        /// <summary>
        /// The index into the RadDropDownLists used to select the voltage scale of the PDPDD and 3D charts
        /// </summary>
        int multiChartScaleSelectedIndex = 2;

        Dictionary<string, string> miscellaneousProperties;

        //int prpddChart1PhaseShiftSelectedIndex = -1;
        //int prpddChart2PhaseShiftSelectedIndex = -1;
        //int prpddChart3PhaseShiftSelectedIndex = -1;
        //int prpddChart4PhaseShiftSelectedIndex = -1;
        //int prpddChart5PhaseShiftSelectedIndex = -1;
        //int prpddChart6PhaseShiftSelectedIndex = -1;
        //int prpddChart7PhaseShiftSelectedIndex = -1;
        //int prpddChart8PhaseShiftSelectedIndex = -1;
        //int prpddChart9PhaseShiftSelectedIndex = -1;
        //int prpddChart10PhaseShiftSelectedIndex = -1;
        //int prpddChart11PhaseShiftSelectedIndex = -1;
        //int prpddChart12PhaseShiftSelectedIndex = -1;
        //int prpddChart13PhaseShiftSelectedIndex = -1;
        //int prpddChart14PhaseShiftSelectedIndex = -1;
        //int prpddChart15PhaseShiftSelectedIndex = -1;

        List<int> prpddChartPhaseShiftSelectedIndexList;

        /// <summary>
        /// The magnitudes for the various rows of the phase resolved data matrices
        /// </summary>
        double[] magnitudeSteps;

        /// <summary>
        /// The phases for the various columns of the phase resolved data matrices
        /// </summary>
        double[] phaseSteps;

        /// <summary>
        /// The amount of voltage spanned by a given row entry.  Since the voltages are sampled from a logarithmic scale, the range
        /// of voltages represented by a row entry shrinks the closer the voltages get to zero.
        /// </summary>
        double[] voltageRangeSpannedByRowEntry;

        /// <summary>
        /// The entries used to select which voltage limit will be used when displaying PRPDD and 3D data
        /// </summary>
        private List<string> voltageScaleEntries;

        /// <summary>
        /// The entries used to select which nC or dB limit will be used when displaying PRPDD and 3D data
        /// </summary>
        private List<string> nonVoltageScaleEntries;

        /// <summary>
        /// The string values used to select the shift in the PRPDD data as displayed
        /// </summary>
        private List<string> phaseOffsetEntries;

        /// <summary>
        /// The phase shift in the matrixdata as selected by the user, applies to the PRPDD and 3D graphs only
        /// </summary>
        private double initialPhaseResolvedDataPhaseShift = 0.0;

        private List<double> channelSensentivitiesList;

        /// <summary>
        /// Used as a switch to disallow graphing until the form is fully loaded.
        /// </summary>
        private bool hasFinishedInitialization;

        /// <summary>
        /// Show a sine wave with no phase offset in the PRPDD graphs
        /// </summary>
        private bool showZeroDegreeSineWave = false;

        /// <summary>
        /// Shows a sine wave with a 120 degree phase offset in the PRPDD graphs
        /// </summary>
        private bool showOneTwentyDegreeSineWave = false;

        /// <summary>
        /// Shows a sine wave with a 240 degree phase offset in the PRPDD graphs
        /// </summary>
        private bool showTwoFortyDegreeSineWave = false;

        private Color sineWaveAColor = Color.Yellow;
        private Color sineWaveBColor = Color.Green;
        private Color sineWaveCColor = Color.Blue;

        private string sineWaveAColorKeyText = "sineWaveAColor";
        private string sineWaveBColorKeyText = "sineWaveBColor";
        private string sineWaveCColorKeyText = "sineWaveCColor";

        private DateTime dataCutoffDate;

        private bool cursorIsEnabled = true;

        //private bool gridDataIsBeingEdited = false;

        //int[] parametersGridColumnWidths;
        //int[] pdParametersGridColumnWidths;
        //int[] chPDParametersGridColumnWidths;
        //int[] phaseResolvedDataGridColumnWidths;

        //string[] parametersGridColumnHeaderText;
        //string[] pdParametersGridColumnHeaderText;
        //string[] chPDParametersGridColumnHeaderText;
        //string[] phaseResolvedDataGridColumnHeaderText;

        private RadContextMenu matrixChart1RadContextMenu;
        private RadContextMenu matrixChart2RadContextMenu;
        private RadContextMenu matrixChart3RadContextMenu;
        private RadContextMenu matrixChart4RadContextMenu;
        private RadContextMenu matrixChart5RadContextMenu;
        private RadContextMenu matrixChart6RadContextMenu;
        private RadContextMenu matrixChart7RadContextMenu;
        private RadContextMenu matrixChart8RadContextMenu;
        private RadContextMenu matrixChart9RadContextMenu;
        private RadContextMenu matrixChart10RadContextMenu;
        private RadContextMenu matrixChart11RadContextMenu;
        private RadContextMenu matrixChart12RadContextMenu;
        private RadContextMenu matrixChart13RadContextMenu;
        private RadContextMenu matrixChart14RadContextMenu;
        private RadContextMenu matrixChart15RadContextMenu;

        private RadContextMenu prpddPolarChart1RadContextMenu;
        private RadContextMenu prpddPolarChart2RadContextMenu;
        private RadContextMenu prpddPolarChart3RadContextMenu;
        private RadContextMenu prpddPolarChart4RadContextMenu;
        private RadContextMenu prpddPolarChart5RadContextMenu;
        private RadContextMenu prpddPolarChart6RadContextMenu;
        private RadContextMenu prpddPolarChart7RadContextMenu;
        private RadContextMenu prpddPolarChart8RadContextMenu;
        private RadContextMenu prpddPolarChart9RadContextMenu;
        private RadContextMenu prpddPolarChart10RadContextMenu;
        private RadContextMenu prpddPolarChart11RadContextMenu;
        private RadContextMenu prpddPolarChart12RadContextMenu;
        private RadContextMenu prpddPolarChart13RadContextMenu;
        private RadContextMenu prpddPolarChart14RadContextMenu;
        private RadContextMenu prpddPolarChart15RadContextMenu;

        private RadContextMenu threeDeeChart1RadContextMenu;
        private RadContextMenu threeDeeChart2RadContextMenu;
        private RadContextMenu threeDeeChart3RadContextMenu;
        private RadContextMenu threeDeeChart4RadContextMenu;
        private RadContextMenu threeDeeChart5RadContextMenu;
        private RadContextMenu threeDeeChart6RadContextMenu;
        private RadContextMenu threeDeeChart7RadContextMenu;
        private RadContextMenu threeDeeChart8RadContextMenu;
        private RadContextMenu threeDeeChart9RadContextMenu;
        private RadContextMenu threeDeeChart10RadContextMenu;
        private RadContextMenu threeDeeChart11RadContextMenu;
        private RadContextMenu threeDeeChart12RadContextMenu;
        private RadContextMenu threeDeeChart13RadContextMenu;
        private RadContextMenu threeDeeChart14RadContextMenu;
        private RadContextMenu threeDeeChart15RadContextMenu;

        private RadContextMenu phdChart1RadContextMenu;
        private RadContextMenu phdChart2RadContextMenu;
        private RadContextMenu phdChart3RadContextMenu;
        private RadContextMenu phdChart4RadContextMenu;
        private RadContextMenu phdChart5RadContextMenu;
        private RadContextMenu phdChart6RadContextMenu;
        private RadContextMenu phdChart7RadContextMenu;
        private RadContextMenu phdChart8RadContextMenu;
        private RadContextMenu phdChart9RadContextMenu;
        private RadContextMenu phdChart10RadContextMenu;
        private RadContextMenu phdChart11RadContextMenu;
        private RadContextMenu phdChart12RadContextMenu;
        private RadContextMenu phdChart13RadContextMenu;
        private RadContextMenu phdChart14RadContextMenu;
        private RadContextMenu phdChart15RadContextMenu;

        //private double humidityScaleFactor = 0.0;
        //private double moistureScaleFactor = 1.0;

        //private double temp1ScaleFactor = 0;
        //private double temp2ScaleFactor = 0;
        //private double temp3ScaleFactor = 0;
        //private double temp4ScaleFactor = 0;

        //private double loadCurrent1ScaleFactor = 1.0;
        //private double loadCurrent2ScaleFactor = 1.0;
        //private double loadCurrent3ScaleFactor = 1.0;

        //private double voltage1ScaleFactor = 1.0;
        //private double voltage2ScaleFactor = 1.0;
        //private double voltage3ScaleFactor = 1.0;

        private string humidityOperation = addText;

        private string moistureOperation = multiplyText;

        private string temp1Operation = addText;
        private string temp2Operation = addText;
        private string temp3Operation = addText;
        private string temp4Operation = addText;

        private string loadCurrent1Operation = multiplyText;
        private string loadCurrent2Operation = multiplyText;
        private string loadCurrent3Operation = multiplyText;

        private string voltage1Operation = multiplyText;
        private string voltage2Operation = multiplyText;
        private string voltage3Operation = multiplyText;

        PDM_Configuration configuration;
        List<PDM_DataComponent_PhaseResolvedData> phaseResolvedData;
        Dictionary<int, double> channelSensitivity;
        Dictionary<int, double> channelPdiValue;

        public PhaseResolvedDataViewer(PDM_Configuration inputConfiguration, DateTime inputDataReadingDate, List<PDM_DataComponent_PhaseResolvedData> inputPhaseResolvedData, Dictionary<int, bool> inputChannelIsActive, Dictionary<int, double> inputChannelSensitivity, Dictionary<int, double> inputChannelPdiValue)
        {
            try
            {
                InitializeComponent();

                this.configuration = inputConfiguration;
                this.phaseResolvedData = inputPhaseResolvedData;
                this.channelIsActive = inputChannelIsActive;
                this.channelSensitivity = inputChannelSensitivity;
                this.channelPdiValue = inputChannelPdiValue;

                this.initialMultiChartWidth = matrixChart1WinChartViewer.Width;
                this.initialMultiChartHeight = matrixChart1WinChartViewer.Height;

                this.multiChartWidthToHeightRatio = (double)this.initialMultiChartWidth / (double)this.initialMultiChartHeight;

                InitializeScaleRadDropDownListEntries();

                InitializePhaseOffsetRadDropDownListEntries();
                FillPhaseOffsetRadDropDownListEntries();

                InitializeMagnitudeAndPhaseStepData();
                ComputeDataDisplayIntervals();

                InitializeChannelLists();
                InitializeListsOfVisualStudioCreatedObjects();

                FillSineWaveDataArrays();

                this.dataReadingDatePrpddTabValueRadLabel.Text = inputDataReadingDate.ToString();
                this.dataReadingDatePrpddPolarTabValueRadLabel.Text = inputDataReadingDate.ToString();
                this.dataReadingDatePrpdd3DTabValueRadLabel.Text = inputDataReadingDate.ToString();
                this.dataReadingDatePhdTabValueRadLabel.Text = inputDataReadingDate.ToString();

                this.hasFinishedInitialization = false;

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhaseResolvedDataViewer(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PhaseResolvedDataViewer_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                // Application.UseWaitCursor = true;
                // This is the default amount of data to load, before the preferences are used

                // this is the default scale setting, it can be overridden when preferences are loaded               
                SetVRadRadioButtonState();

                /// disable states not accounted for yet
                //this.matrixDbRadRadioButton.Enabled = false;
                ////this.matrixDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                //this.matrixPcRadRadioButton.Enabled = false;
                ////this.matrixPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;

                //this.prpddPolarDbRadRadioButton.Enabled = false;
                ////this.prpddPolarDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                //this.prpddPolarPcRadRadioButton.Enabled = false;
                ////this.prpddPolarPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;

                //this.threeDeeDbRadRadioButton.Enabled = false;
                ////this.threeDeeDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                //this.threeDeePcRadRadioButton.Enabled = false;
                ////this.threeDeePcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;

                //this.phdDbRadRadioButton.Enabled = false;
                ////this.phdDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                //this.phdPcRadRadioButton.Enabled = false;
                ////this.phdPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;               


                FillAllScaleRadDropDownListEntries(this.voltageScaleEntries);

                this.matrixScaleRadDropDownList.SelectedIndex = 2;

               
                // setup context menus
                InitializeRadContextMenus();
                SetUpMultiChartRadContextMenus();

                HideAllDisplayObjects();

                ProcessPhaseResolvedData();

                // SelectAllRadDropDownListObjects();

                //InitializeAllRadGridViews();

                //EstablishAllParentChildGridRelations();

                this.hasFinishedInitialization = true;

                ResizeMultiCharts();

                // SetAllMultiChartsAsNeedingUpdating();

                SetAllMultiChartsAsNeedingUpdating();

                DrawAllGraphs();

                SetSensorPhaseForMultiCharts();

                DisableEditingInAllRadDropDownLists();

                // this.trendWinChartViewer.updateViewPort(true, true);
                // set tool tips
                SetToolTips();

                // Application.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhaseResolvedDataViewer_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Application.UseWaitCursor = false;
            }
        }

        private void SetSensorPhaseForMultiCharts()
        {
            try
            {
                int phaseShift;
                if ((this.configuration != null) && (this.configuration.AllMembersAreNonNull()))
                {
                    for (int i = 0; i < this.configuration.channelInfoList.Count; i++)
                    {
                        phaseShift = this.configuration.channelInfoList[i].CHPhase;
                        if ((phaseShift > -1) && (phaseShift < 6))
                        {
                            this.matrixChartPhaseShiftRadDropDownListList[i].SelectedIndex = phaseShift;
                        }
                        else
                        {
                            string errorMessage = "PhaseResolvedDataViewer.SetSensorPhaseForMultiCharts()\nCHPhase value from the current configuration was outside the interval [0,5], value was " + phaseShift.ToString();
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.SetSensorPhaseForMultiCharts()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
   
        private void DisableEditingInAllRadDropDownLists()
        {
            try
            {
                if ((matrixChartPhaseShiftRadDropDownListList != null) &&
                    (prpddPolarChartPhaseShiftRadDropDownListList != null) &&
                    (threeDeeChartPhaseShiftRadDropDownListList != null))
                {
                    if ((matrixChartPhaseShiftRadDropDownListList.Count == 15) &&
                        (prpddPolarChartPhaseShiftRadDropDownListList.Count == 15) &&
                        (threeDeeChartPhaseShiftRadDropDownListList.Count == 15))
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            matrixChartPhaseShiftRadDropDownListList[i].DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                            prpddPolarChartPhaseShiftRadDropDownListList[i].DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                            threeDeeChartPhaseShiftRadDropDownListList[i].DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                        }
                       
                        matrixScaleRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                        prpddPolarScaleRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                        threeDeeScaleRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                        phdScaleRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.DisableEditingInAllRadDropDownLists()\nAt least one of the phaseShiftRadDropDownListList lists did not have 15 elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.DisableEditingInAllRadDropDownLists()\nAt least one of the phaseShiftRadDropDownListList lists was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DisableEditingInAllRadDropDownLists()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void SetToolTips()
        {
            try
            {
                toolTip1.AutoPopDelay = 3000;
                toolTip1.InitialDelay = 1000;
                toolTip1.ReshowDelay = 500;
                toolTip1.SetToolTip(this.matrixRadPageViewPage, phaseResolvedPartialDischargeDistributionText);
                toolTip1.SetToolTip(this.prpddPolarRadPageViewPage, phaseResolvedPartialDischargeDistributionPolarText);
                toolTip1.SetToolTip(this.threeDeeRadPageViewPage, phaseResolvedPartialDischargeDistributionThreeDeeText);
                toolTip1.SetToolTip(this.phdRadPageViewPage, pulseHeightDistributionText);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetToolTips()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

   
        /// <summary>
        /// Hides all multi-chart display objects
        /// </summary>
        private void HideAllDisplayObjects()
        {
            try
            {
                HideAllMatrixDisplayObjects();
                HideAllPRPDDPolarDisplayObjects();
                HideAllPhdDisplayObjects();
                HideAllThreeDeeDisplayObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.HideAllDisplayObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Hides all multi-chart display objects associated with the PRPDD display
        /// </summary>
        private void HideAllMatrixDisplayObjects()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    matrixChartRadGroupBoxList[i].Visible = false;
                    matrixChartWinChartViewerList[i].Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.HideAllMatrixDisplayObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void HideAllPRPDDPolarDisplayObjects()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    prpddPolarChartRadGroupBoxList[i].Visible = false;
                    prpddPolarChartWinChartViewerList[i].Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.HideAllMatrixDisplayObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Hides all multi-chart display objects associated with the PHD display
        /// </summary>
        private void HideAllPhdDisplayObjects()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    phdChartRadGroupBoxList[i].Visible = false;
                    phdChartWinChartViewerList[i].Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.HideAllPhdDisplayObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Hides all multi-chart display objects associated with the 3D display
        /// </summary>
        private void HideAllThreeDeeDisplayObjects()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    threeDeeChartWinChartViewerList[i].Visible = false;
                    threeDeeChartRadGroupBoxList[i].Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.HideAllThreeDeeDisplayObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Makes all multi-chart objects associated with one channel display visible
        /// </summary>
        /// <param name="zeroIndexChannelNumber"></param>
        private void ShowDisplayObjectsForOneChannel(int zeroIndexChannelNumber)
        {
            try
            {
                ShowMatrixDisplayObjectForOneChannel(zeroIndexChannelNumber);
                ShowPRPDDPolarDisplayObjectForOneChannel(zeroIndexChannelNumber);
                ShowThreeDeeDisplayObjectForOneChannel(zeroIndexChannelNumber);
                ShowPhdDisplayObjectForOneChannel(zeroIndexChannelNumber);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShowDisplayObjectsForOneChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Makes all matrix objects associated with one channel display visible
        /// </summary>
        /// <param name="zeroIndexChannelNumber"></param>
        private void ShowMatrixDisplayObjectForOneChannel(int zeroIndexChannelNumber)
        {
            try
            {
                if ((zeroIndexChannelNumber > -1) && (zeroIndexChannelNumber < 15))
                {
                    this.matrixChartRadGroupBoxList[zeroIndexChannelNumber].Visible = true;
                    this.matrixChartWinChartViewerList[zeroIndexChannelNumber].Visible = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShowMatrixDisplayObjectForOneChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ShowPRPDDPolarDisplayObjectForOneChannel(int zeroIndexChannelNumber)
        {
            try
            {
                if ((zeroIndexChannelNumber > -1) && (zeroIndexChannelNumber < 15))
                {
                    this.prpddPolarChartRadGroupBoxList[zeroIndexChannelNumber].Visible = true;
                    this.prpddPolarChartWinChartViewerList[zeroIndexChannelNumber].Visible = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShowPRPDDPolarDisplayObjectForOneChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Makes all 3D objects associated with one channel display visible
        /// </summary>
        /// <param name="zeroIndexChannelNumber"></param>
        private void ShowThreeDeeDisplayObjectForOneChannel(int zeroIndexChannelNumber)
        {
            try
            {
                if ((zeroIndexChannelNumber > -1) && (zeroIndexChannelNumber < 15))
                {
                    this.threeDeeChartWinChartViewerList[zeroIndexChannelNumber].Visible = true;
                    this.threeDeeChartRadGroupBoxList[zeroIndexChannelNumber].Visible = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShowThreeDeeDisplayObjectForOneChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Makes all PHD objects associated with one channel display visible
        /// </summary>
        /// <param name="zeroIndexChannelNumber"></param>
        private void ShowPhdDisplayObjectForOneChannel(int zeroIndexChannelNumber)
        {
            try
            {
                if ((zeroIndexChannelNumber > -1) && (zeroIndexChannelNumber < 15))
                {
                    phdChartWinChartViewerList[zeroIndexChannelNumber].Visible = true;
                    phdChartRadGroupBoxList[zeroIndexChannelNumber].Visible = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShowPhdDisplayObjectForOneChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Returns a PDM_DataComponent_PhaseResolvedData object that has all entries set to zero or the eqiuvalent
        /// </summary>
        /// <returns></returns>
        private PDM_DataComponent_PhaseResolvedData CreateEmptyMatrixData()
        {
            PDM_DataComponent_PhaseResolvedData localMatrixData = new PDM_DataComponent_PhaseResolvedData();
            try
            {
                localMatrixData.ChannelNumber = 0;
                localMatrixData.PhasePolarity = "Blah";
                localMatrixData.NonZeroEntries = "";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.CreateEmptyMatrixData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return localMatrixData;
        }

        public bool ProcessPhaseResolvedData()
        {
            bool dataIsPresent = false;
            try
            {
                DateTime readingDateTime = ConversionMethods.MinimumDateTime();

                /// We will insert these in case there are skipped channels and matrices, so that
                /// the data will all line up with respect to the dates
                PDM_DataComponent_PhaseResolvedData emptyMatrix;

                PDM_DataComponent_PhaseResolvedData currentPhaseResolvedData;

                /// Some lists need to be cleared because they get things added to them every time
                this.channelMaxMatrixVoltagesList.Clear();
                this.channelMatrixDataIsNotEmpty.Clear();
                this.activeChannelNumberList.Clear();

                dataIsPresent = true;
                this.totalDataReadings = 1;
                this.activeChannelCount = 0;

                /// we allocate full data arrays for all active channels only
                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i])
                    {
                        this.channelMaxMatrixVoltagesList.Add(0.00);
                        this.channelMatrixDataIsNotEmpty.Add(false);
                        //this.channelDataList[i].AllocateAllArrays(1);
                        this.channelPlusMatrixDataList[i].AllocateAllArrays();
                        this.channelMinusMatrixDataList[i].AllocateAllArrays();
                        this.activeChannelNumberList.Add(i);
                        this.activeChannelCount++;
                    }
                    else
                    {
                        /// add some placeholder values because these entries will never be accessed, but we always want to assume
                        /// we can iterate through the list using indexing and not have it fail.  Before any data access, we check
                        /// to make sure the channel is active.
                        this.channelMaxMatrixVoltagesList.Add(0.00);
                        this.channelMatrixDataIsNotEmpty.Add(false);
                    }
                }

                /// each data item has multiple channel data entries, 15 are possible but often not all channels are used.  however,
                /// we will still insert dummy entries in place of any channel that is not used.  That way, we can always index the
                /// same way into the lists without worrying about them not having enough entries.
                /// 
                /// THis is also necessary to handle a condition such that the number of active channels changes over time.  IF one adds 
                /// or subtracts channels as data are gathered, then some data would have holes in it that would throw off proper graphing.
                /// THese methods insure that there is at least a placeholder for the data, though that placeholder carries values that are
                /// all zero.
                /// 
                //int itemCount = 0;
                //foreach (PDM_SingleDataReading singleDataReading in allDataReadings)
                //{
                //    itemCount++;
                //    readingDateTime = singleDataReading.readingDateTime;

                //    // first, add the common portion of the data to the internal representation
                //    this.commonData.Add(singleDataReading.parameters, singleDataReading.pdParameters, readingDateTime,
                //                        singleDataReading.GetAlarmStateForAllChannels(), singleDataReading.GetAlarmSourceForAllChannels());

                //    for (int i = 0; i < 15; i++)
                //    {
                //        if (this.channelIsActive[i])
                //        {
                //            currentChPDParameters = FindSpecifiedChannelData(singleDataReading.chPDParameters, i + 1);
                //            if (currentChPDParameters != null)
                //            {
                //                this.channelDataList[i].Add(currentChPDParameters, readingDateTime);
                //            }
                //            else
                //            {
                //                emptyChannelData = CreateEmptyChannelData();
                //                emptyChannelData.FromChannel = i;
                //                this.channelDataList[i].Add(emptyChannelData, readingDateTime);
                //            }
                //        }
                //        //else
                //        //{
                //        //    this.channelDataList[i].Add(emptyChannelData, readingDateTime);
                //        //}
                //    }

                //    if (singleDataReading.MatrixWasRecorded())
                //    {
                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i])
                    {
                        currentPhaseResolvedData = FindSpecifiedMatrixData(phaseResolvedData, i + 1, "Plus");
                        if (currentPhaseResolvedData != null)
                        {
                            channelPlusMatrixDataList[i].Add(currentPhaseResolvedData);
                        }
                        else
                        {
                            emptyMatrix = CreateEmptyMatrixData();
                            emptyMatrix.PhasePolarity = "Plus";
                            emptyMatrix.ChannelNumber = i;
                            channelPlusMatrixDataList[i].Add(emptyMatrix);
                        }

                        currentPhaseResolvedData = FindSpecifiedMatrixData(phaseResolvedData, i + 1, "Minus");
                        if (currentPhaseResolvedData != null)
                        {
                            channelMinusMatrixDataList[i].Add(currentPhaseResolvedData);
                        }
                        else
                        {
                            emptyMatrix = CreateEmptyMatrixData();
                            emptyMatrix.PhasePolarity = "Minus";
                            emptyMatrix.ChannelNumber = i;
                            channelMinusMatrixDataList[i].Add(emptyMatrix);
                        }
                    }
                    //else
                    //{
                    //    emptyMatrix.PhasePolarity = "Plus";
                    //    channelPlusMatrixDataList[i].Add(emptyMatrix, readingDateTime);
                    //    emptyMatrix.PhasePolarity = "Minus";
                    //    channelPlusMatrixDataList[i].Add(emptyMatrix, readingDateTime);
                    //}
                }
                //    }
                //    counter++;
                //    if (counter > 90)
                //    {
                //        int glah = counter;
                //    }
                //}
                //AddDataToParamatersRadGridView(parameters);
                //AddDataToPDParamatersRadGridView(pdParameters);
                //AddDataToCHPDParamatersRadGridView(chPDParameters);
                //AddDataToPhaseResolvedDataRadGridView(phaseResolvedData);

                FillTheLocalMaximumVoltageEntriesForAllChannels();
                FillGlobalMaximumMatrixVoltageValueOverAllChannels();

                this.matrixChartsNeedUpdating = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.LoadData(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataIsPresent;
        }

        //private List<PDM_Data_Parameters> ConvertParametersBindingSourceToList()
        //{
        //    List<PDM_Data_Parameters> parametersList = new List<PDM_Data_Parameters>();

        //    int position = 0;
        //    int count = this.parametersBindingSource.Count;
        //    for(position = 0;position<count;position++)
        //    {
        //        this.parametersBindingSource.Position = position;
        //        parametersList.Add(((PDM_Data_Parameters)((MonitorInterfaceDBDataSet.PDM_Data_ParametersRow)((DataRowView)this.parametersBindingSource.Current).Row)));
        //    }

        //    foreach (DataRow entry in this.parametersDataTable.Rows)
        //    {
        //        parametersList.Add((PDM_Data_Parameters)entry);

        //    }
        //        // ((MonitorInterfaceDBDataSet.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).Name;
        //    // companyNode.Text = ((MonitorInterfaceDBDataSet.CompanyRow)((DataRowView)companyBindingSource.Current).Row).Name;
        //}

        /// <summary>
        /// Finds the matrix(phase resolved data) object that has the specified channelNumber and phasePolarity, using a linear search.
        /// </summary>
        /// <param name="matrixList"></param>
        /// <param name="channelNumber"></param>
        /// <param name="phasePolarity"></param>
        /// <returns></returns>
        public PDM_DataComponent_PhaseResolvedData FindSpecifiedMatrixData(List<PDM_DataComponent_PhaseResolvedData> matrixList, int channelNumber, string phasePolarity)
        {
            PDM_DataComponent_PhaseResolvedData targetMatrix = null;
            try
            {
                if (matrixList != null)
                {
                    foreach (PDM_DataComponent_PhaseResolvedData entry in matrixList)
                    {
                        if ((entry.ChannelNumber == channelNumber) && (entry.PhasePolarity.Trim().CompareTo(phasePolarity) == 0))
                        {
                            targetMatrix = entry;
                            break;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FindSpecifiedMatrixData(List<PDM_DataComponent_PhaseResolvedData>, int, string)\nInput List<PDM_DataComponent_PhaseResolvedData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindSpecifiedMatrixData(List<PDM_DataComponent_PhaseResolvedData>, int, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return targetMatrix;
        }

        /// <summary>
        /// Returns a list of the current active channels, indicated by their 0-indexed channel number
        /// </summary>
        /// <param name="activeChannels"></param>
        /// <returns></returns>
        private List<int> GetActiveChannelNumberList(bool[] activeChannels)
        {
            List<int> channelList = new List<int>();
            try
            {
                if (activeChannels.Length == 15)
                {
                    for (int i = 0; i < 15; i++)
                    {
                        if (activeChannels[i])
                        {
                            channelList.Add(i);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.GetActiveChannelNumberList(bool[])\nInput array of booleans did not contain exactly 15 elements.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetActiveChannelNumberList(bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelList;
        }

        private double[] GetLocalMagnitudeStepsInSelectedUnits(int zeroIndexChannelNumber)
        {
            double[] localMagnitudeSteps = null;
            try
            {
                int count;
                if (this.magnitudeSteps != null)
                {
                    count = this.magnitudeSteps.Length;
                    localMagnitudeSteps = new double[count];
                    for (int i = 0; i < count; i++)
                    {
                        localMagnitudeSteps[i] = ConvertOneVoltageToSelectedUnits(this.magnitudeSteps[i], zeroIndexChannelNumber);
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetMagnitudeInCurrentUnits(int)\nthis.magnitudeSteps was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetMagnitudeInCurrentUnits(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return localMagnitudeSteps;
        }

        private double ConvertOneVoltageToSelectedUnits(double magnitudeValue, int zeroIndexChannelNumber)
        {
            double convertedValue = magnitudeValue;
            try
            {
                double senstivity = 0.0;
                if (matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    convertedValue = magnitudeValue;
                }
                else if (matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (channelSensitivity != null)
                    {
                        if (channelSensitivity.ContainsKey(zeroIndexChannelNumber + 1))
                        {
                            senstivity = channelSensitivity[zeroIndexChannelNumber + 1];
                            if (senstivity < 1.0e-7)
                            {
                                senstivity = 10.0;
                            }
                            convertedValue = magnitudeValue * senstivity;
                        }
                    }
                }
                else if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    convertedValue = 20.0 * Math.Log10(magnitudeValue / 10.0);
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.GetMagnitudeInCurrentUnits(int)\nCannot determine the state of the buttons in the amplitude group box.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetMagnitudeInCurrentUnits(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedValue;
        }

        private double RoundUpDbScaleMaxValue(double scaleMaxValue)
        {
            double newScaleMaxValue = scaleMaxValue;
            try
            {
                /// I commented some of these about because in some cases chartdirector was labeling the axis without putting a "0" in 
                /// and that would cause difficulties with re-labeling the axis with the db values.  So, to make it easier, I just decided
                /// to punt on the in-between scales and go with 10s only.  This way we can always know what the markers will be.
                if (scaleMaxValue < 10.0)
                {
                    newScaleMaxValue = 10.0;
                }
                //else if (scaleMaxValue < 15.0)
                //{
                //    newScaleMaxValue = 15.0;
                //}
                else if (scaleMaxValue < 20.0)
                {
                    newScaleMaxValue = 20.0;
                }
                //else if (scaleMaxValue < 25.0)
                //{
                //    newScaleMaxValue = 25.0;
                //}
                else if (scaleMaxValue < 30.0)
                {
                    newScaleMaxValue = 30.0;
                }
                //else if (scaleMaxValue < 35.0)
                //{
                //    newScaleMaxValue = 35.0;
                //}
                else if (scaleMaxValue < 40.0)
                {
                    newScaleMaxValue = 40.0;
                }
                //else if (scaleMaxValue < 45.0)
                //{
                //    newScaleMaxValue = 45.0;
                //}
                else if (scaleMaxValue < 50.0)
                {
                    newScaleMaxValue = 50.0;
                }
                //else if (scaleMaxValue < 55.0)
                //{
                //    newScaleMaxValue = 55.0;
                //}
                else if (scaleMaxValue < 60.0)
                {
                    newScaleMaxValue = 60.0;
                }
                //else if (scaleMaxValue < 65.0)
                //{
                //    newScaleMaxValue = 65.0;
                //}
                else if (scaleMaxValue < 70.0)
                {
                    newScaleMaxValue = 70.0;
                }
                //else if (scaleMaxValue < 75.0)
                //{
                //    newScaleMaxValue = 75.0;
                //}
                else if (scaleMaxValue < 80.0)
                {
                    newScaleMaxValue = 80.0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.RoundUpDbScaleMaxValue(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return newScaleMaxValue;
        }

        /// <summary>
        /// Draws one PRPDD chart
        /// </summary>
        /// <param name="chartViewer"></param>
        /// <param name="matrixDataPlus"></param>
        /// <param name="matrixDataMinus"></param>
        /// <param name="phaseShift"></param>
        /// <param name="localMaximumVoltage"></param>
        /// <param name="channelNumber"></param>
        private void DrawPRPDDChart(WinChartViewer chartViewer, MatrixData matrixDataPlus, MatrixData matrixDataMinus, double phaseShift, double localMaximumVoltage, int channelNumber)
        {
            try
            {
                double[] phaseData;
                double[] magnitudeData;
                double[] pulseCounts;

                double[] localMagnitudeSteps;

                int[] rowIndices;

                double pixelsPerMagnitudeUnit;
                double pixelsPerPhaseUnit;

                double viewPortStartDegrees;
                double viewPortEndDegrees;
                //int xStartIndex;
                //int xEndIndex;

                double scaleMax = ConvertOneVoltageToSelectedUnits(localMaximumVoltage, (channelNumber - 1));

                double scaleMin = -scaleMax;

                double dbCorrectionConstant = 0.0;

                int index = 0;
                int pointCount = 0;

                int pixelsPerPhasePoint;
                // int pixelsForCurrentMagnitudePoint;
                //int widthExtension = this.Width - 1024;
                //int heightExtension = this.Height - 768;

                int graphX = 52;
                int graphY = 28;
                int graphWidth = this.multiChartWidth - graphX - 18;
                int graphHeight = this.multiChartHeight - graphY - 64;

                bool amplitudeScaleWasCorrectlySelected = true;
                string amplitudeScaleString = string.Empty;

                double actualMagnitudeInDb = 0.0;

                if (phaseShift > 360.0)
                {
                    phaseShift -= 360.0;
                }

                if (chartViewer != null)
                {
                    if (matrixDataPlus != null)
                    {
                        if (matrixDataMinus != null)
                        {
                            localMagnitudeSteps = GetLocalMagnitudeStepsInSelectedUnits(channelNumber - 1);
                            if (localMagnitudeSteps != null)
                            {
                                if (this.matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (dB)";
                                    dbCorrectionConstant = 80.0;
                                    scaleMax += dbCorrectionConstant;
                                    scaleMax = RoundUpDbScaleMaxValue(scaleMax);
                                    scaleMin = -scaleMax;
                                }
                                else if (this.matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (V)";
                                }
                                else if (this.matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (pC)";
                                }
                                else
                                {
                                    amplitudeScaleWasCorrectlySelected = false;
                                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nAmplitude scale was not correctly selected.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (amplitudeScaleWasCorrectlySelected)
                                {
                                    // figure out the scaling needed to graph each point

                                    pixelsPerMagnitudeUnit = graphHeight / (2 * scaleMax);
                                    pixelsPerPhaseUnit = graphWidth / 360.0;

                                    pixelsPerPhasePoint = GetPointDimensionInPixels(pixelsPerPhaseUnit * 7.5);

                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 1 - Configure overall chart appearance. 
                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    XYChart xyChart = new XYChart(this.multiChartWidth, this.multiChartHeight, 0xf0f0ff, 0, 1);

                                    xyChart.setPlotArea(graphX, graphY, graphWidth, graphHeight);
                                    xyChart.setClipping();

                                    //// Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                                    //// light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                                    //xyChart.addTitle("trend", "Times New Roman Bold Italic", 15
                                    //    ).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                    // Add a bottom title to the chart to show the date range of the axis, with a light blue 
                                    // (ccccff) background.
                                    //xyChart.addTitle2(Chart.Bottom, "From <*font=Arial Bold Italic*>"
                                    //    + xyChart.formatValue(viewPortStartDate, "{value|mmm dd, yyyy}")
                                    //    + "<*/font*> to <*font=Arial Bold Italic*>"
                                    //    + xyChart.formatValue(viewPortEndDate, "{value|mmm dd, yyyy}")
                                    //    + "<*/font*> (Duration <*font=Arial Bold Italic*>"
                                    //    + Math.Round(viewPortEndDate.Subtract(viewPortStartDate).TotalSeconds / 86400.0)
                                    //    + "<*/font*> days)", "Arial Italic", 10).setBackground(0xccccff);

                                    // Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                                    // xyChart.addLegend(50, 33, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

                                    // Set axes width to 2 pixels
                                    xyChart.yAxis().setWidth(2);
                                    xyChart.xAxis().setWidth(2);

                                    xyChart.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                    // Add a title to the y-axis
                                    xyChart.yAxis().setTitle(amplitudeScaleString, "Arial", 9);

                                    xyChart.xAxis().setTitle("Phase (deg)", "Arial", 9);

                                    // add data

                                    ///// if we have zoomed, we need to figure out how man points to display
                                    ///// the 360.0 refers to the total possible degrees displayed on the x axis
                                    viewPortStartDegrees = (chartViewer.ViewPortLeft * 360.0);
                                    viewPortEndDegrees = (viewPortStartDegrees + chartViewer.ViewPortWidth * 360.0);

                                    // now adjust the viewport limits
                                    viewPortStartDegrees -= 1.0e-7;
                                    viewPortEndDegrees += 1.0e-7;

                                    /// add the sine waves to the chart
                                    /// 
                                    /// We only add the first wave if no other waves are present
                                    if ((!this.showZeroDegreeSineWave) && (!this.showOneTwentyDegreeSineWave) && (!this.showTwoFortyDegreeSineWave))
                                    {
                                        AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Red));
                                    }
                                    else
                                    {
                                        if (this.showZeroDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveAColor));
                                        }
                                        if (this.showOneTwentyDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 120.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveBColor));
                                        }
                                        if (this.showTwoFortyDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 240.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveCColor));
                                        }
                                    }

                                    //xyChart.addScatterLayer(phaseData, magnitudeData);

                                    //xyChart.addExtraField(pulseCounts);

                                    // set up the data to be graphed, if there is in fact any data to graph

                                    index = 0;

                                    pointCount = matrixDataPlus.matrixEntries.Count +
                                                     matrixDataMinus.matrixEntries.Count;
                                    if (pointCount > 0)
                                    {
                                        // convert the matrix data into scatter array data
                                        phaseData = new double[pointCount];
                                        magnitudeData = new double[pointCount];
                                        pulseCounts = new double[pointCount];

                                        rowIndices = new int[pointCount];

                                        int colorBeingUsed;

                                        // Note that the matricies are saved in 1-index form, but C# uses 0-index form, so we
                                        // need to convert it.
                                        foreach (MatrixEntry matrixEntry in matrixDataPlus.matrixEntries)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            magnitudeData[index] = localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant;
                                            rowIndices[index] = matrixEntry.rowNumber - 1;
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        foreach (MatrixEntry matrixEntry in matrixDataMinus.matrixEntries)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            // since this is minus matrix data we need to change the sign for this value
                                            magnitudeData[index] = -(localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant);
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        if (phaseShift > 1e-7)
                                        {
                                            phaseData = ShiftPhaseData(phaseData, phaseShift);
                                        }

                                        double[] singlePhaseData = new double[1];
                                        double[] singleMagnitudeData = new double[1];
                                        double[] singlePulseCount = new double[1];

                                        double[] singlePointWidth = new double[1];
                                        double[] singlePointHeight = new double[1];
                                        double[] pixelsPerPhaseWidthAsDouble = new double[1];

                                        double[] actualMagnitudeInDbAsArray = new double[1];

                                        double currentPhase;
                                        double currentMagnitude;

                                        pixelsPerPhaseWidthAsDouble[0] = pixelsPerPhaseUnit * 7.5 / (chartViewer.ViewPortWidth * 1.1);

                                        singlePointWidth[0] = 7.5;
                                        double[] currentPointHeightInPixelsAsDouble = new double[1];

                                        // add a 0 point to the graph at -80 db
                                        if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        {

                                        }

                                        for (int i = 0; i < pointCount; i++)
                                        {
                                            //if ((Math.Abs(magnitudeData[i]) < scaleMax)||
                                            //    ((matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)&&(Math.Abs(magnitudeData[i]) < Math.Abs(80 + scaleMax))))
                                            if (Math.Abs(magnitudeData[i]) < scaleMax)
                                            {
                                                currentPhase = phaseData[i];
                                                currentMagnitude = magnitudeData[i];
                                                //if ((currentPhase > viewPortStartDegrees) && (currentPhase < viewPortEndDegrees))
                                                //{
                                                singlePhaseData[0] = currentPhase;
                                                singleMagnitudeData[0] = currentMagnitude;
                                                singlePulseCount[0] = pulseCounts[i];

                                                colorBeingUsed = GetPointColorBasedOnPulseCount((int)(Math.Round(pulseCounts[i])));

                                                //if (matrixRectangleMarkerRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{
                                                //    pixelsForCurrentMagnitudePoint = GetPointDimensionInPixels(this.voltageRangeSpannedByRowEntry[rowIndices[i]] * 2.0 * pixelsPerMagnitudeUnit);
                                                //    //if (pixelsForCurrentMagnitudePoint < 3) pixelsForCurrentMagnitudePoint = 3;
                                                //    currentPointHeightInPixelsAsDouble[0] = pixelsForCurrentMagnitudePoint;

                                                //    singlePointHeight[0] = this.voltageRangeSpannedByRowEntry[rowIndices[i]] * 2.0;

                                                //    //xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed).
                                                //    //    setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointHeight, Chart.YAxisScale);

                                                //    //xyChart.addExtraField(singlePulseCount);

                                                //    ScatterLayer layer = xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed);
                                                //    layer.addExtraField(singlePulseCount);
                                                //    layer.setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointHeight, Chart.YAxisScale);

                                                //    //xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed).
                                                //    //    setSymbolScale(singlePointWidth, Chart.XAxisScale, currentPointHeightInPixelsAsDouble, Chart.PixelScale)

                                                //}
                                                //else if (matrixDotMarkerRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{
                                                ScatterLayer layer = xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.CircleSymbol, 1, colorBeingUsed, colorBeingUsed);
                                                layer.addExtraField(singlePulseCount);
                                                if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                {
                                                    if (currentMagnitude > 0)
                                                    {
                                                        actualMagnitudeInDb = currentMagnitude - dbCorrectionConstant;
                                                    }
                                                    else
                                                    {
                                                        actualMagnitudeInDb = (-currentMagnitude) - dbCorrectionConstant;
                                                    }
                                                    actualMagnitudeInDbAsArray[0] = actualMagnitudeInDb;
                                                    layer.addExtraField(actualMagnitudeInDbAsArray);
                                                }
                                                //layer.setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointWidth, Chart.XAxisScale);
                                                // layer.setSymbolScale(new double[] { pixelsPerPhasePoint }, Chart.PixelScale);
                                                layer.setSymbolScale(pixelsPerPhaseWidthAsDouble, Chart.PixelScale);
                                                //}
                                                //}
                                            }
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 3 - Set up x-axis scale
                                    //////////////////////////////////////////////////////////////////////////////////////

                                    if ((viewPortStartDegrees < 0.0) && (viewPortEndDegrees > 360.0))
                                    {
                                        if (this.multiChartWidth < 400.0)
                                        {
                                            xyChart.xAxis().setLinearScale(0.0, 360.0, 90.0, 15.0);
                                        }
                                        else if (this.multiChartWidth < 600.0)
                                        {
                                            xyChart.xAxis().setLinearScale(0.0, 360.0, 45.0, 5.0);
                                        }
                                        else
                                        {
                                            xyChart.xAxis().setLinearScale(0.0, 360.0, 15.0, 5.0);
                                        }
                                    }
                                    else
                                    {
                                        xyChart.xAxis().setLinearScale(Math.Round(viewPortStartDegrees), Math.Round(viewPortEndDegrees));
                                    }

                                    //// Set x-axis date scale to the view port date range. 
                                    //xyChart.xAxis().setDateScale(viewPortStartDate, viewPortEndDate);

                                    ////
                                    //// In the current demo, the x-axis range can be from a few years to a few days. We can 
                                    //// let ChartDirector auto-determine the date/time format. However, for more beautiful 
                                    //// formatting, we set up several label formats to be applied at different conditions. 
                                    ////

                                    //// If all ticks are yearly aligned, then we use "yyyy" as the label format.
                                    //xyChart.xAxis().setFormatCondition("align", 360 * 86400);
                                    //xyChart.xAxis().setLabelFormat("{value|yyyy}");

                                    //// If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
                                    //// label of a year, and "mmm" for other labels.
                                    //xyChart.xAxis().setFormatCondition("align", 30 * 86400);
                                    //xyChart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                                    //    Chart.AllPassFilter(), "{value|mmm}");

                                    //// If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
                                    //// first label of a year, and "mmm dd" in bold font as the first label of a month, and
                                    //// "dd" for other labels.
                                    //xyChart.xAxis().setFormatCondition("align", 86400);
                                    //xyChart.xAxis().setMultiFormat(
                                    //    Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                                    //    Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
                                    //xyChart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

                                    //// For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
                                    //// a day, and "hh:nn" for other labels.
                                    //xyChart.xAxis().setFormatCondition("else");
                                    //xyChart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                                    //    Chart.AllPassFilter(), "{value|hh:nn}");

                                    /////////////////////////////////////////////////////////////////////////////////////////
                                    //// Step 4 - Set up y-axis scale
                                    /////////////////////////////////////////////////////////////////////////////////////////

                                    //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    //{
                                    //    SetupDbYaxisScale(ref xyChart, graphHeight, scaleMax);
                                    //}
                                    //else
                                    //{
                                    if (chartViewer.ZoomDirection == WinChartDirection.HorizontalVertical)
                                    {
                                        // xy-zoom mode - compute the actual axis scale in the view port 
                                        double axisLowerLimit = scaleMax - (scaleMax - scaleMin) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                        double axisUpperLimit = scaleMax - (scaleMax - scaleMin) * chartViewer.ViewPortTop;
                                        // *** use the following formula if you are using a log scale axis ***
                                        // double axisLowerLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                        // double axisUpperLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop);

                                        // use the zoomed-in scale
                                        xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                                        xyChart.yAxis().setRounding(false, false);

                                    }

                                    if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        ReplaceCurrentAxisLabelsWithDbLabels(ref xyChart);
                                    }


                                    //if (scaleMax < .011)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.01, .01, .002);
                                    //}
                                    //else if (scaleMax < .021)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.02, .02, .005);
                                    //}

                                    //else if (scaleMax < .051)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.05, .05, .01);
                                    //}
                                    //else if (scaleMax < .11)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.1, .1, .02);
                                    //}
                                    //else if (scaleMax < .251)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.25, .25, .05);
                                    //}
                                    //else if (scaleMax < .51)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.5, .5, .1);
                                    //}
                                    //else if (scaleMax < 1.01)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-1.0, 1.0, .2);
                                    //}

                                    // xyChart.yAxis().setLinearScale(-.05, .05, .01);

                                    //if ((chartViewer.ZoomDirection == WinChartDirection.Horizontal) || (trendChartMinYValue == trendChartMaxYValue))
                                    //{
                                    //    // y-axis is auto-scaled - save the chosen y-axis scaled to support xy-zoom mode
                                    //    xyChart.layout();
                                    //    trendChartMinYValue = xyChart.yAxis().gettrendChartMinYValue();
                                    //    trendChartMaxYValue = xyChart.yAxis().gettrendChartMaxYValue();
                                    //}
                                    //else
                                    //if (chartViewer.ZoomDirection == WinChartDirection.HorizontalVertical)
                                    //{
                                    //    // xy-zoom mode - compute the actual axis scale in the view port 
                                    //    double axisLowerLimit = scaleMax - (scaleMax - scaleMin) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                    //    double axisUpperLimit = scaleMax - (scaleMax - scaleMin) * chartViewer.ViewPortTop;
                                    //    // *** use the following formula if you are using a log scale axis ***
                                    //    // double axisLowerLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                    //    // double axisUpperLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop);

                                    //    // use the zoomed-in scale
                                    //    xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                                    //    xyChart.yAxis().setRounding(false, false);
                                    //}

                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 5 - Display the chart
                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    chartViewer.Chart = xyChart;

                                    //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    //{
                                    //    xyChart.layout();

                                    //    //get the label position
                                    //    double[] ticks = xyChart.yAxis().getTicks();
                                    //    for (int i = 0; i < ticks.Length; ++i)
                                    //    {
                                    //        string axisLabel = xyChart.yAxis().getLabel(ticks[i]);
                                    //        // .... modify the label based on the position ticks[i] or the original axisLabel ....
                                    //        xyChart.yAxis().addLabel(ticks[i], GetDbEquivalentLabel(axisLabel));
                                    //    }

                                    //    chartViewer.Chart = xyChart;
                                    //}

                                    if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        chartViewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                                    "title='Phase (deg) = {x} , " + amplitudeScaleString + " = {field1}, PulseCount = {field0}'");
                                    }
                                    else
                                    {
                                        chartViewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                                        "title='Phase (deg) = {x} , " + amplitudeScaleString + " = {value}, PulseCount = {field0}'");
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFailed to compute the local magnitude steps.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nSecond input MatrixData was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFirst input MatrixData was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ReplaceCurrentAxisLabelsWithDbLabels(ref XYChart xyChart)
        {
            try
            {
                xyChart.layout();

                //get the label position
                double[] ticks = xyChart.yAxis().getTicks();
                for (int i = 0; i < ticks.Length; ++i)
                {
                    string axisLabel = xyChart.yAxis().getLabel(ticks[i]);
                    // .... modify the label based on the position ticks[i] or the original axisLabel ....
                    if (axisLabel != null)
                    {
                        xyChart.yAxis().addLabel(ticks[i], GetDbEquivalentLabel(axisLabel));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ReplaceCurrentAxisLabelsWithDbLabels(ref XYChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private string GetDbEquivalentLabel(string inputLabel)
        {
            string dbLabel = inputLabel;
            try
            {
                double labelValueAsDouble = 0;
                double newValue = 0;

                if (Double.TryParse(inputLabel, out labelValueAsDouble))
                {
                    newValue = (80.0 - Math.Abs(labelValueAsDouble));
                    dbLabel = "\\-" + Math.Round(newValue, 1).ToString();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetDbEquivalentLabel(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dbLabel;
        }

        private void ReplaceCurrentAxisLabelsWithDbLabels(ref PolarChart polarChart)
        {
            try
            {
                polarChart.layout();

                //get the label position
                double[] ticks = polarChart.radialAxis().getTicks();
                for (int i = 0; i < ticks.Length; ++i)
                {
                    string axisLabel = polarChart.radialAxis().getLabel(ticks[i]);
                    // .... modify the label based on the position ticks[i] or the original axisLabel ....
                    if (axisLabel != null)
                    {
                        polarChart.radialAxis().addLabel(ticks[i], GetDbEquivalentLabel(axisLabel));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ReplaceCurrentAxisLabelsWithDbLabels(ref PolarChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool scaleIsNotEvenlyDisible(double scaleMax, int majorTicInc)
        {
            bool evenlyDivisible = true;
            try
            {
                int scaleMaxAsInt = (int)Math.Round(scaleMax, 0);

                if ((scaleMaxAsInt % majorTicInc) != 0)
                {
                    evenlyDivisible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.scaleIsNotEvenlyDisible(double, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return evenlyDivisible;
        }

        /// <summary>
        /// Draws one PRPDD Polar chart
        /// </summary>
        /// <param name="chartViewer"></param>
        /// <param name="matrixDataPlus"></param>
        /// <param name="matrixDataMinus"></param>
        /// <param name="phaseShift"></param>
        /// <param name="localMaximumVoltage"></param>
        /// <param name="channelNumber"></param>
        private void DrawPRPDDPolarChart(WinChartViewer chartViewer, MatrixData matrixDataPlus, MatrixData matrixDataMinus, double phaseShift, double localMaximumVoltage, int channelNumber)
        {
            try
            {
                double[] phaseData;
                double[] magnitudeData;
                double[] pulseCounts;

                double[] localMagnitudeSteps;

                int[] rowIndices;

                double pixelsPerMagnitudeUnit;
                double pixelsPerPhaseUnit;

                double viewPortStartDegrees;
                double viewPortEndDegrees;
                //int xStartIndex;
                //int xEndIndex;

                double scaleMax = ConvertOneVoltageToSelectedUnits(localMaximumVoltage, (channelNumber - 1));

                double scaleMin = -scaleMax;

                int index = 0;
                int pointCount = 0;

                int pixelsPerPhasePoint;
                int pixelsForCurrentMagnitudePoint;
                //int widthExtension = this.Width - 1024;
                //int heightExtension = this.Height - 768;

                int graphX = 52;
                int graphY = 28;
                int graphWidth = this.multiChartWidth - graphX - 18;
                int graphHeight = this.multiChartHeight - graphY - 64;

                int polarChartWidth = this.multiChartWidth;
                int polarChartHeight = this.multiChartHeight;

                int polarChartDiameter = Math.Min(polarChartHeight, polarChartWidth);

                int centerX;
                int centerY;
                int radius;

                int radialAxisMajorTic = 90;

                string amplitudeScaleString = string.Empty;
                double dbCorrectionConstant = 0;
                bool amplitudeScaleWasCorrectlySelected = true;

                if (chartViewer != null)
                {
                    if (matrixDataPlus != null)
                    {
                        if (matrixDataMinus != null)
                        {
                            localMagnitudeSteps = GetLocalMagnitudeStepsInSelectedUnits(channelNumber - 1);
                            if (localMagnitudeSteps != null)
                            {
                                if (this.matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (dB)";
                                    dbCorrectionConstant = 80.0;
                                    scaleMax += dbCorrectionConstant;
                                    scaleMax = RoundUpDbScaleMaxValue(scaleMax);
                                }
                                else if (this.matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (V)";
                                }
                                else if (this.matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (pC)";
                                }
                                else
                                {
                                    amplitudeScaleWasCorrectlySelected = false;
                                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nAmplitude scale was not correctly selected.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (amplitudeScaleWasCorrectlySelected)
                                {
                                    // figure out the scaling needed to graph each point

                                    pixelsPerMagnitudeUnit = graphHeight / (2 * scaleMax);
                                    pixelsPerPhaseUnit = graphWidth / 360.0;

                                    pixelsPerPhasePoint = GetPointDimensionInPixels(pixelsPerPhaseUnit * 7.5);

                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 1 - Configure overall chart appearance. 
                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    PolarChart polarChart = new PolarChart(polarChartWidth, polarChartHeight, 0xf0f0ff, 0, 1);
                                    polarChart.setRoundedFrame(Chart.CColor(BackColor));

                                    centerX = (int)((0.5 - chartViewer.ViewPortLeft) * polarChartWidth / chartViewer.ViewPortWidth);
                                    centerY = (int)((0.5 - chartViewer.ViewPortTop) * polarChartHeight / chartViewer.ViewPortHeight);

                                    //centerX += centerXoffset;
                                    //centerY += centerYoffset;

                                    radius = (int)((polarChartDiameter * .40 / 2) + ((this.multiChartWidth - this.initialMultiChartWidth) / 2.0) * .5);

                                    // radius = (int)((((double)polarChartDiameter * 0.70) / 2.0) / chartViewer.ViewPortWidth);

                                    polarChart.setPlotArea(centerX, centerY, radius, 0xffffff);

                                    if (this.showRadialLabelsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                                    {
                                        polarChart.radialAxis().setLabelStyle("", 0, Chart.CColor(Color.Transparent));
                                    }

                                    // Set the plotarea at (55, 100) and of size 600 x 300 pixels. Use white (ffffff) 
                                    // background. Enable both horizontal and vertical grids by setting their colors to 
                                    // grey (cccccc). Set clipping mode to clip the data lines to the plot area.
                                    //polarChart.setPlotArea(340, 265, 230, 0xffffff);
                                    // polarChart.setClipping();

                                    polarChart.setGridStyle(false, false);

                                    if (radius < 50)
                                    {
                                        radialAxisMajorTic = 90;
                                    }
                                    else if (radius < 90)
                                    {
                                        radialAxisMajorTic = 45;
                                    }
                                    else if (radius < 150)
                                    {
                                        radialAxisMajorTic = 30;
                                    }
                                    else if (radius < 590)
                                    {
                                        radialAxisMajorTic = 15;
                                    }
                                    else
                                    {
                                        radialAxisMajorTic = 5;
                                    }
                                    polarChart.angularAxis().setLinearScale(0, 360, radialAxisMajorTic);

                                    polarChart.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                    //XYChart xyChart = new XYChart(this.multiChartWidth, this.multiChartHeight, 0xf0f0ff, 0, 1);

                                    //xyChart.setPlotArea(graphX, graphY, graphWidth, graphHeight);
                                    //xyChart.setClipping();

                                    //// Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                                    //// light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                                    //xyChart.addTitle("trend", "Times New Roman Bold Italic", 15
                                    //    ).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                    // Add a bottom title to the chart to show the date range of the axis, with a light blue 
                                    // (ccccff) background.
                                    //xyChart.addTitle2(Chart.Bottom, "From <*font=Arial Bold Italic*>"
                                    //    + xyChart.formatValue(viewPortStartDate, "{value|mmm dd, yyyy}")
                                    //    + "<*/font*> to <*font=Arial Bold Italic*>"
                                    //    + xyChart.formatValue(viewPortEndDate, "{value|mmm dd, yyyy}")
                                    //    + "<*/font*> (Duration <*font=Arial Bold Italic*>"
                                    //    + Math.Round(viewPortEndDate.Subtract(viewPortStartDate).TotalSeconds / 86400.0)
                                    //    + "<*/font*> days)", "Arial Italic", 10).setBackground(0xccccff);

                                    // Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                                    // xyChart.addLegend(50, 33, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

                                    // Set axes width to 2 pixels
                                    //xyChart.yAxis().setWidth(2);
                                    //xyChart.xAxis().setWidth(2);

                                    //xyChart.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                    //// Add a title to the y-axis
                                    //xyChart.yAxis().setTitle("Amplitude", "Arial", 9);

                                    //xyChart.xAxis().setTitle("Phase (deg)", "Arial", 9);

                                    // add data

                                    ///// if we have zoomed, we need to figure out how man points to display
                                    ///// the 360.0 refers to the total possible degrees displayed on the x axis
                                    viewPortStartDegrees = (chartViewer.ViewPortLeft * 360.0);
                                    viewPortEndDegrees = (viewPortStartDegrees + chartViewer.ViewPortWidth * 360.0);

                                    // now adjust the viewport limits
                                    viewPortStartDegrees -= 1.0e-7;
                                    viewPortEndDegrees += 1.0e-7;

                                    /// add the sine waves to the chart
                                    /// 
                                    /// We only add the first wave if no other waves are present
                                    //if ((!this.showZeroDegreeSineWave) && (!this.showOneTwentyDegreeSineWave) && (!this.showTwoFortyDegreeSineWave))
                                    //{
                                    //    AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Red));
                                    //}
                                    //else
                                    //{
                                    //    if (this.showZeroDegreeSineWave)
                                    //    {
                                    //        AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Yellow));
                                    //    }
                                    //    if (this.showOneTwentyDegreeSineWave)
                                    //    {
                                    //        AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 120.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Green));
                                    //    }
                                    //    if (this.showTwoFortyDegreeSineWave)
                                    //    {
                                    //        AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 240.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Blue));
                                    //    }
                                    //}

                                    //xyChart.addScatterLayer(phaseData, magnitudeData);

                                    //xyChart.addExtraField(pulseCounts);

                                    // set up the data to be graphed, if there is in fact any data to graph

                                    index = 0;
                                    pointCount = matrixDataPlus.matrixEntries.Count +
                                                     matrixDataMinus.matrixEntries.Count;
                                    if (pointCount > 0)
                                    {
                                        // convert the matrix data into scatter array data
                                        phaseData = new double[pointCount];
                                        magnitudeData = new double[pointCount];
                                        pulseCounts = new double[pointCount];

                                        rowIndices = new int[pointCount];

                                        int colorBeingUsed;

                                        // Note that the matricies are saved in 1-index form, but C# uses 0-index form, so we
                                        // need to convert it.
                                        foreach (MatrixEntry matrixEntry in matrixDataPlus.matrixEntries)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            magnitudeData[index] = localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant;
                                            rowIndices[index] = matrixEntry.rowNumber - 1;
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        foreach (MatrixEntry matrixEntry in matrixDataMinus.matrixEntries)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            // since this is minus matrix data we need to change the sign for this value
                                            magnitudeData[index] = localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant;
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        if (phaseShift > 1e-7)
                                        {
                                            phaseData = ShiftPhaseData(phaseData, phaseShift);
                                        }

                                        double[] singlePhaseData = new double[1];
                                        double[] singleMagnitudeData = new double[1];
                                        double[] singlePulseCount = new double[1];

                                        double[] singlePointWidth = new double[1];
                                        double[] singlePointHeight = new double[1];
                                        double[] pixelsPerPhaseWidthAsDouble = new double[1];

                                        double[] actualMagnitudeInDbAsArray = new double[1];
                                        double actualMagnitudeInDb = 0;

                                        double currentPhase;
                                        double currentMagnitude;

                                        pixelsPerPhaseWidthAsDouble[0] = pixelsPerPhaseUnit * 7.5 / (chartViewer.ViewPortWidth * 1.1);

                                        singlePointWidth[0] = 7.5;
                                        double[] currentPointHeightInPixelsAsDouble = new double[1];

                                        for (int i = 0; i < pointCount; i++)
                                        {
                                            if (Math.Abs(magnitudeData[i]) < scaleMax)
                                            {
                                                currentPhase = phaseData[i];
                                                currentMagnitude = magnitudeData[i];
                                                //if ((currentPhase > viewPortStartDegrees) && (currentPhase < viewPortEndDegrees))
                                                //{
                                                singlePhaseData[0] = currentPhase;
                                                singleMagnitudeData[0] = currentMagnitude;
                                                singlePulseCount[0] = pulseCounts[i];

                                                colorBeingUsed = GetPointColorBasedOnPulseCount((int)(Math.Round(pulseCounts[i])));

                                                //if (matrixRectangleMarkerRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{
                                                //    pixelsForCurrentMagnitudePoint = GetPointDimensionInPixels(this.voltageRangeSpannedByRowEntry[rowIndices[i]] * 2.0 * pixelsPerMagnitudeUnit);
                                                //    //if (pixelsForCurrentMagnitudePoint < 3) pixelsForCurrentMagnitudePoint = 3;
                                                //    currentPointHeightInPixelsAsDouble[0] = pixelsForCurrentMagnitudePoint;

                                                //    singlePointHeight[0] = this.voltageRangeSpannedByRowEntry[rowIndices[i]] * 2.0;

                                                //    //xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed).
                                                //    //    setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointHeight, Chart.YAxisScale);

                                                //    //xyChart.addExtraField(singlePulseCount);

                                                //    ScatterLayer layer = xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed);
                                                //    layer.addExtraField(singlePulseCount);
                                                //    layer.setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointHeight, Chart.YAxisScale);

                                                //    //xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.SquareSymbol, 1, colorBeingUsed, colorBeingUsed).
                                                //    //    setSymbolScale(singlePointWidth, Chart.XAxisScale, currentPointHeightInPixelsAsDouble, Chart.PixelScale)

                                                //}
                                                //else if (matrixDotMarkerRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{

                                                PolarLineLayer layer = polarChart.addLineLayer(singleMagnitudeData, colorBeingUsed, "");
                                                layer.setAngles(singlePhaseData);
                                                layer.setLineWidth(0);
                                                layer.setDataSymbol(Chart.CircleSymbol, 1);
                                                layer.setSymbolScale(pixelsPerPhaseWidthAsDouble, Chart.PixelScale);
                                                polarChart.addExtraField(singlePulseCount);

                                                //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{
                                                //    //if (currentMagnitude > 0)
                                                //    //{
                                                //    actualMagnitudeInDb = currentMagnitude - dbCorrectionConstant;
                                                //    //}
                                                //    actualMagnitudeInDbAsArray[0] = actualMagnitudeInDb;
                                                //    polarChart.addExtraField(actualMagnitudeInDbAsArray);
                                                //}

                                                //ScatterLayer layer = xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.CircleSymbol, 1, colorBeingUsed, colorBeingUsed);
                                                //layer.addExtraField(singlePulseCount);
                                                //layer.setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointWidth, Chart.XAxisScale);
                                                // layer.setSymbolScale(new double[] { pixelsPerPhasePoint }, Chart.PixelScale);
                                                // layer.setSymbolScale(pixelsPerPhaseWidthAsDouble, Chart.PixelScale);
                                                //}
                                                //}
                                            }
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 3 - Set up x-axis scale
                                    //////////////////////////////////////////////////////////////////////////////////////

                                    //if ((viewPortStartDegrees < 0.0) && (viewPortEndDegrees > 360.0))
                                    //{
                                    //    if (this.multiChartWidth < 400.0)
                                    //    {
                                    //        xyChart.xAxis().setLinearScale(0.0, 360.0, 90.0, 15.0);
                                    //    }
                                    //    else if (this.multiChartWidth < 600.0)
                                    //    {
                                    //        xyChart.xAxis().setLinearScale(0.0, 360.0, 45.0, 5.0);
                                    //    }
                                    //    else
                                    //    {
                                    //        xyChart.xAxis().setLinearScale(0.0, 360.0, 15.0, 5.0);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    xyChart.xAxis().setLinearScale(Math.Round(viewPortStartDegrees), Math.Round(viewPortEndDegrees));
                                    //}

                                    //// Set x-axis date scale to the view port date range. 
                                    //xyChart.xAxis().setDateScale(viewPortStartDate, viewPortEndDate);

                                    ////
                                    //// In the current demo, the x-axis range can be from a few years to a few days. We can 
                                    //// let ChartDirector auto-determine the date/time format. However, for more beautiful 
                                    //// formatting, we set up several label formats to be applied at different conditions. 
                                    ////

                                    //// If all ticks are yearly aligned, then we use "yyyy" as the label format.
                                    //xyChart.xAxis().setFormatCondition("align", 360 * 86400);
                                    //xyChart.xAxis().setLabelFormat("{value|yyyy}");

                                    //// If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
                                    //// label of a year, and "mmm" for other labels.
                                    //xyChart.xAxis().setFormatCondition("align", 30 * 86400);
                                    //xyChart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                                    //    Chart.AllPassFilter(), "{value|mmm}");

                                    //// If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
                                    //// first label of a year, and "mmm dd" in bold font as the first label of a month, and
                                    //// "dd" for other labels.
                                    //xyChart.xAxis().setFormatCondition("align", 86400);
                                    //xyChart.xAxis().setMultiFormat(
                                    //    Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                                    //    Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
                                    //xyChart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

                                    //// For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
                                    //// a day, and "hh:nn" for other labels.
                                    //xyChart.xAxis().setFormatCondition("else");
                                    //xyChart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                                    //    Chart.AllPassFilter(), "{value|hh:nn}");

                                    /////////////////////////////////////////////////////////////////////////////////////////
                                    //// Step 4 - Set up y-axis scale
                                    /////////////////////////////////////////////////////////////////////////////////////////

                                    if ((matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) && (showRadialLabelsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                                    {
                                        ReplaceCurrentAxisLabelsWithDbLabels(ref polarChart);
                                    }

                                    //if (scaleMax < .011)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.01, .01, .002);
                                    //}
                                    //else if (scaleMax < .021)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.02, .02, .005);
                                    //}

                                    //else if (scaleMax < .051)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.05, .05, .01);
                                    //}
                                    //else if (scaleMax < .11)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.1, .1, .02);
                                    //}
                                    //else if (scaleMax < .251)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.25, .25, .05);
                                    //}
                                    //else if (scaleMax < .51)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-.5, .5, .1);
                                    //}
                                    //else if (scaleMax < 1.01)
                                    //{
                                    //    xyChart.yAxis().setLinearScale(-1.0, 1.0, .2);
                                    //}

                                    // xyChart.yAxis().setLinearScale(-.05, .05, .01);

                                    //if ((chartViewer.ZoomDirection == WinChartDirection.Horizontal) || (trendChartMinYValue == trendChartMaxYValue))
                                    //{
                                    //    // y-axis is auto-scaled - save the chosen y-axis scaled to support xy-zoom mode
                                    //    xyChart.layout();
                                    //    trendChartMinYValue = xyChart.yAxis().gettrendChartMinYValue();
                                    //    trendChartMaxYValue = xyChart.yAxis().gettrendChartMaxYValue();
                                    //}
                                    //else



                                    //if (chartViewer.ZoomDirection == WinChartDirection.HorizontalVertical)
                                    //{
                                    //    // xy-zoom mode - compute the actual axis scale in the view port 
                                    //    double axisLowerLimit = scaleMax - (scaleMax - scaleMin) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                    //    double axisUpperLimit = scaleMax - (scaleMax - scaleMin) * chartViewer.ViewPortTop;
                                    //    // *** use the following formula if you are using a log scale axis ***
                                    //    // double axisLowerLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                    //    // double axisUpperLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop);

                                    //    // use the zoomed-in scale
                                    //    xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                                    //    xyChart.yAxis().setRounding(false, false);
                                    //}


                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 5 - Display the chart
                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    chartViewer.Chart = polarChart;

                                    if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        chartViewer.ImageMap = polarChart.getHTMLImageMap("clickable", "",
                                    "title='Phase (deg) = {x} , " + amplitudeScaleString + " = {={value} - 80}, PulseCount = {dsdiField0}'");
                                    }
                                    else
                                    {
                                        chartViewer.ImageMap = polarChart.getHTMLImageMap("clickable", "",
                                        "title='Phase (deg) = {x} , " + amplitudeScaleString + " = {value}, PulseCount = {dsdiField0}'");
                                    }

                                    //chartViewer.ImageMap = polarChart.getHTMLImageMap("clickable", "",
                                    //"title='Phase (deg) = {x} , Amplitude (V) = {value}, PulseCount = {dsdiField0}'");

                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFailed to compute the local magnitude steps.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nSecond input MatrixData was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFirst input MatrixData was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// Draws one PHD chart
        /// </summary>
        /// <param name="chartViewer"></param>
        /// <param name="matrixDataPlus"></param>
        /// <param name="matrixDataMinus"></param>
        /// <param name="localMaximumVoltage"></param>
        /// <param name="voltageSelectedWasComputed"></param>
        /// <param name="channelNumber"></param>
        private void DrawPHDChart(WinChartViewer chartViewer, MatrixData matrixDataPlus, MatrixData matrixDataMinus, double localMaximumVoltage, bool voltageSelectedWasComputed, int channelNumber)
        {
            try
            {
                double[] pulseCountPlusData;
                double[] pulseCountMinusData;

                double[] localMagnitudeSteps;
                double localMaximumValue = ConvertOneVoltageToSelectedUnits(localMaximumVoltage, (channelNumber - 1));

                double yScaleMax = 0.0;

                double yScaleMin = 0.0;

                double xScaleMin = 0.0;

                double currentPulseCount = 0.0;
                double maxPulseCount = 0.0;

                int magnitudeStepsCount = this.magnitudeSteps.Length;

                int graphX = 60;
                int graphY = 30;
                int graphWidth = this.multiChartWidth - graphX - 20;
                int graphHeight = this.multiChartHeight - graphY - 64;

                string amplitudeScaleString = string.Empty;
                bool amplitudeScaleWasCorrectlySelected = true;

                if (chartViewer != null)
                {
                    if (matrixDataPlus != null)
                    {
                        if (matrixDataMinus != null)
                        {
                            localMagnitudeSteps = GetLocalMagnitudeStepsInSelectedUnits(channelNumber - 1);
                            if (localMagnitudeSteps != null)
                            {
                                if (this.matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (dB)";
                                    xScaleMin = -70.5;
                                }
                                else if (this.matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (V)";
                                }
                                else if (this.matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (pC)";
                                }
                                else
                                {
                                    amplitudeScaleWasCorrectlySelected = false;
                                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nAmplitude scale was not correctly selected.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (amplitudeScaleWasCorrectlySelected)
                                {

                                    //int pointCount = matrixDataPlus.matrixEntries[this.allMatrixDateTimeSelectedIndex].Count +
                                    //                 matrixDataMinus.matrixEntries[this.allMatrixDateTimeSelectedIndex].Count;
                                    //if (pointCount > 0)
                                    {
                                        int index = 0;
                                        bool found = false;

                                        if (voltageSelectedWasComputed)
                                        {
                                            while (!found && (index < magnitudeStepsCount))
                                            {
                                                if (Math.Abs(localMaximumValue - localMagnitudeSteps[index]) < 1.0e-7)
                                                {
                                                    found = true;
                                                }
                                                else
                                                {
                                                    index++;
                                                }
                                            }
                                            index--;
                                            if (index > -1)
                                            {
                                                localMaximumValue = localMagnitudeSteps[index];
                                            }
                                        }

                                        pulseCountPlusData = new double[magnitudeStepsCount];
                                        pulseCountMinusData = new double[magnitudeStepsCount];

                                        for (int i = 0; i < magnitudeStepsCount; i++)
                                        {
                                            pulseCountPlusData[i] = 0.0;
                                            pulseCountMinusData[i] = 0.0;
                                        }

                                        //int colorBeingUsed;

                                        // Note that the matricies are saved in 1-index form, but C# uses 0-index form, so we
                                        // need to convert it.
                                        foreach (MatrixEntry matrixEntry in matrixDataPlus.matrixEntries)
                                        {
                                            pulseCountPlusData[matrixEntry.rowNumber - 1] += matrixEntry.value;
                                        }

                                        foreach (MatrixEntry matrixEntry in matrixDataMinus.matrixEntries)
                                        {
                                            pulseCountMinusData[matrixEntry.rowNumber - 1] += matrixEntry.value;
                                        }

                                        // find the maximum pulse count for a given voltage to set the y scale
                                        for (int i = 0; i < magnitudeStepsCount; i++)
                                        {
                                            currentPulseCount = pulseCountPlusData[i];
                                            if (currentPulseCount > maxPulseCount)
                                            {
                                                maxPulseCount = currentPulseCount;
                                            }
                                        }
                                        for (int i = 0; i < magnitudeStepsCount; i++)
                                        {
                                            currentPulseCount = pulseCountMinusData[i];
                                            if (currentPulseCount > maxPulseCount)
                                            {
                                                maxPulseCount = currentPulseCount;
                                            }
                                        }
                                        yScaleMax = maxPulseCount;

                                        ///////////////////////////////////////////////////////////////////////////////////////
                                        // Step 1 - Configure overall chart appearance. 
                                        ///////////////////////////////////////////////////////////////////////////////////////

                                        XYChart xyChart = new XYChart(this.multiChartWidth, this.multiChartHeight, 0xf0f0ff, 0, 1);

                                        xyChart.setPlotArea(graphX, graphY, graphWidth, graphHeight);
                                        xyChart.setClipping();

                                        //// Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                                        //// light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                                        xyChart.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10
                                            ).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                        //// Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                                        //xyChart.addLegend(50, 33, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

                                        // Set axes width to 2 pixels
                                        xyChart.yAxis().setWidth(2);
                                        xyChart.xAxis().setWidth(2);

                                        // Add a title to the y-axis
                                        xyChart.yAxis().setTitle("Pulse Count", "Arial", 9);

                                        xyChart.xAxis().setTitle(amplitudeScaleString, "Arial", 9);

                                        // add data
                                        Layer lineLayer = xyChart.addLineLayer();
                                        lineLayer.setXData(localMagnitudeSteps);
                                        lineLayer.addDataSet(pulseCountPlusData, Chart.CColor(Color.Red));
                                        lineLayer.addDataSet(pulseCountMinusData, Chart.CColor(Color.Blue));

                                        ///////////////////////////////////////////////////////////////////////////////////////
                                        // Step 3 - Set up x-axis scale
                                        //////////////////////////////////////////////////////////////////////////////////////

                                        if ((chartViewer.ViewPortLeft < 1.0e-7) && (chartViewer.ViewPortWidth > (1.0 - 1.0e-7)))
                                        {
                                            xyChart.xAxis().setLinearScale(xScaleMin, localMaximumValue);
                                        }
                                        else
                                        {
                                            double viewPortWidth = localMaximumValue - xScaleMin;
                                            double viewPortLeft = xScaleMin + chartViewer.ViewPortLeft * viewPortWidth;
                                            double viewPortRight = viewPortLeft + (chartViewer.ViewPortWidth * viewPortWidth);

                                            //double viewPortLeft = chartViewer.ViewPortLeft * localMaximumValue;
                                            //double viewPortRight = viewPortLeft + (chartViewer.ViewPortWidth * localMaximumValue);
                                            xyChart.xAxis().setLinearScale(viewPortLeft, viewPortRight);
                                        }

                                        /////////////////////////////////////////////////////////////////////////////////////////
                                        //// Step 4 - Set up y-axis scale
                                        /////////////////////////////////////////////////////////////////////////////////////////

                                        if (chartViewer.ZoomDirection == WinChartDirection.HorizontalVertical)
                                        {
                                            // xy-zoom mode - compute the actual axis scale in the view port 
                                            double axisLowerLimit = yScaleMax - (yScaleMax - yScaleMin) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                            double axisUpperLimit = yScaleMax - (yScaleMax - yScaleMin) * chartViewer.ViewPortTop;
                                            // *** use the following formula if you are using a log scale axis ***
                                            // double axisLowerLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                                            // double axisUpperLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop);

                                            // use the zoomed-in scale
                                            xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                                            xyChart.yAxis().setRounding(false, false);
                                        }

                                        ///////////////////////////////////////////////////////////////////////////////////////
                                        // Step 5 - Display the chart
                                        ///////////////////////////////////////////////////////////////////////////////////////

                                        chartViewer.Chart = xyChart;

                                        //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        //{
                                        //    chartViewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                                        //"title='Phase (deg) = {x} , " + amplitudeScaleString + " = {field1}, PulseCount = {field0}'");
                                        //}
                                        //else
                                        //{
                                        //    chartViewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                                        //    "title='Phase (deg) = {x} , " + amplitudeScaleString + " = {value}, PulseCount = {field0}'");
                                        //}

                                        chartViewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                                        "title='Amplitude = {x} , Pulse Count = {value}'");
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PhaseResolvedDataViewer.DrawPHDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFailed to compute the local magnitude steps.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawPHDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nSecond input MatrixData was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.DrawPHDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFirst input MatrixData was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPHDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawPHDChart(WinChartViewer, MatrixData, MatrixData, double, bool, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Returns the input phase data shifted by the phaseShift amount.  The shift is accomplished by adding the phaseShift amount (which can be positive or negative) 
        /// to every element, and then adjusting any data that is less than 0 degrees or greater than 360 degrees to a value in that range.
        /// </summary>
        /// <param name="phaseData"></param>
        /// <param name="phaseShift"></param>
        /// <returns></returns>
        private double[] ShiftPhaseData(double[] phaseData, double phaseShift)
        {
            double[] shiftedPhaseData = null;
            try
            {
                int length;
                ArrayMath shiftedPhaseValues;

                if (phaseData != null)
                {
                    length = phaseData.Length;
                    shiftedPhaseValues = new ArrayMath(phaseData);
                    /// this subtracts the phase shift value from every value in the array
                    shiftedPhaseValues.sub2(phaseShift);
                    shiftedPhaseData = shiftedPhaseValues.result();
                    for (int i = 0; i < length; i++)
                    {
                        // I don't like to use comparisons to an exact value for floats/doubles, and we know the phase is either 0.0 or at least -7.5
                        if (shiftedPhaseData[i] < -.1)
                        {
                            shiftedPhaseData[i] = shiftedPhaseData[i] + 360.0;
                        }
                        // same deal here as above, but this time the value is either 360 or at least 367.5
                        else if (shiftedPhaseData[i] > 360.1)
                        {
                            shiftedPhaseData[i] = shiftedPhaseData[i] - 360.0;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.ShiftPhaseData(double[], double)\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ShiftPhaseData(double[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shiftedPhaseData;
        }

        /// <summary>
        /// Converts a double-valued point size to an integer size, used in drawing a PRPDD graph
        /// </summary>
        /// <param name="pointDimension"></param>
        /// <returns></returns>
        private int GetPointDimensionInPixels(double pointDimension)
        {
            int pointDimensionInPixels = 1;
            try
            {
                double roundedValue = Math.Round(pointDimension);
                if (roundedValue > 1.0)
                {
                    pointDimensionInPixels = (int)roundedValue;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetPointDimensionInPixels(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pointDimensionInPixels;
        }

        /// <summary>
        /// Assigns color to the points in a PRPDD graph
        /// </summary>
        /// <param name="pulseCount"></param>
        /// <returns></returns>
        private int GetPointColorBasedOnPulseCount(int pulseCount)
        {
            int color = Chart.CColor(Color.Red);
            try
            {
                if (pulseCount < 12)
                {
                    color = Chart.CColor(Color.Black);
                }
                else if (pulseCount < 34)
                {
                    color = Chart.CColor(Color.Purple);
                }
                else if (pulseCount < 57)
                {
                    color = Chart.CColor(Color.Blue);
                }
                else if (pulseCount < 80)
                {
                    color = Chart.CColor(Color.Green);
                }
                else if (pulseCount < 104)
                {
                    color = Chart.CColor(Color.Yellow);
                }
                else if (pulseCount < 127)
                {
                    color = Chart.CColor(Color.Orange);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetPointColorBasedOnPulseCount(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return color;
        }

        /// <summary>
        /// Returns the index in the data associated with a specific input date for the trend data
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        private int FindGraphStartDateIndex(DateTime startDate)
        {
            int startDateIndex = 0;
            try
            {
                if (this.commonData != null)
                {
                    if (this.commonData.readingDateTime != null)
                    {
                        startDateIndex = Array.BinarySearch(this.commonData.readingDateTime, startDate);
                        if (startDateIndex < 0)
                        {
                            startDateIndex = (~startDateIndex) - 1;
                        }
                        /// Hack inserted because the above formula failed for me on very tiny data sets.  Since it
                        /// worked fine otherwise, I assume there is an anomoly that needed smoothing over.
                        if (startDateIndex < 0)
                        {
                            startDateIndex = 0;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.FindGraphStartDateIndex(DateTime)\nthis.commonData.ReadingDateTime was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FindGraphStartDateIndex(DateTime)\nthis.commonData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindGraphStartDateIndex(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return startDateIndex;
        }

        /// <summary>
        /// Gets the end date index of the trend graph data
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int FindGraphEndDateIndex(DateTime endDate)
        {
            int endDateIndex = 0;
            try
            {
                int arrayLength;
                if (this.commonData != null)
                {
                    if (this.commonData.readingDateTime != null)
                    {
                        arrayLength = this.commonData.readingDateTime.Length;
                        endDateIndex = Array.BinarySearch(this.commonData.readingDateTime, endDate);
                        if (endDateIndex < 0)
                        {
                            if ((~endDateIndex) < arrayLength)
                            {
                                endDateIndex = ~endDateIndex;
                            }
                            else
                            {
                                endDateIndex = arrayLength - 1;
                            }
                        }
                        /// this is here just to keep the program from crashing, I don't know if this 
                        /// condition is even possible at this point in the method.  however, this above is based
                        /// on chartdirector sample code, not my own algorithm.
                        if ((endDateIndex < 0) || (endDateIndex > (arrayLength - 1)))
                        {
                            endDateIndex = arrayLength - 1;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.FindGraphEndDateIndex(DateTime)\nthis.commonData.ReadingDateTime was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FindGraphEndDateIndex(DateTime)\nthis.commonData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindGraphEndDateIndex(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return endDateIndex;
        }

        /// <summary>
        /// Right now returns 1, which means everything has the same symbol, but could be more elaboratre in the future
        /// </summary>
        /// <param name="channelNumber"></param>
        /// <param name="dataIndex"></param>
        /// <returns></returns>
        private int GetSymbol(int channelNumber, ChannelDataType dataType)
        {
            return 1;
        }

        /// <summary>
        /// Draws all charts that show the matrix (phase resolved) data
        /// </summary>
        private void DrawAllMultiChartGraphs()
        {
            try
            {
                DrawAllPRPDDGraphs();
                DrawAllPRPDDPolarGraphs();
                DrawAllPhdGraphs();
                DrawAllThreeDeeGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllMultiChartGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw all PRPDD graphs for all active channels for the current matrix (phase resolved data) display date
        /// </summary>
        private void DrawAllPRPDDGraphs()
        {
            try
            {
                int zeroIndexedChannelNumber;
                int count;
                if (this.matrixChartsNeedUpdating && (this.dataRadPageView.SelectedPage == matrixRadPageViewPage))
                {
                    if (this.hasFinishedInitialization && (this.totalDataReadings > 0))
                    {
                        if (this.activeChannelNumberList != null)
                        {
                            count = this.activeChannelNumberList.Count;
                            HideAllMatrixDisplayObjects();

                            for (int i = 0; i < count; i++)
                            {
                                zeroIndexedChannelNumber = this.activeChannelNumberList[i];
                                SetupAndDrawOnePRPDDGraph(i, zeroIndexedChannelNumber);

                                // this.matrixChartPDIValueRadLabelList[i].Text = this.channelDataList[zeroIndexedChannelNumber].pdi[this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex].ToString();

                                this.matrixChartPDIValueRadLabelList[i].Text = Math.Round(this.channelPdiValue[zeroIndexedChannelNumber + 1], 1).ToString();
                            }
                            PlaceAllMatrixChartGroupBoxes();
                            this.matrixChartsNeedUpdating = false;
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawAllPRPDDGraphs()\nthis.activeChannelNumberList was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllPRPDDGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw one PRPDD graph for a given channel.
        /// </summary>
        /// <param name="zeroIndexChartNumber"></param>
        /// <param name="zeroIndexedChannelNumber"></param>
        private void SetupAndDrawOnePRPDDGraph(int zeroIndexChartNumber, int zeroIndexedChannelNumber)
        {
            try
            {
                if ((zeroIndexChartNumber > -1) && (zeroIndexChartNumber < 15))
                {
                    if ((zeroIndexedChannelNumber > -1) && (zeroIndexedChannelNumber < 15))
                    {
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(this.matrixChartPhaseShiftRadDropDownListList[zeroIndexChartNumber].SelectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.matrixScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        //if ((this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0) ||
                        //           (this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0))
                        {
                            ShowMatrixDisplayObjectForOneChannel(zeroIndexChartNumber);
                            DrawPRPDDChart(this.matrixChartWinChartViewerList[zeroIndexChartNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                                           this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDGraph(int, int)\nSecond input int needs to be between 0 and 14 inclusive";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDGraph(int, int)\nFirst input int needs to be between 0 and 14 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDGraph(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw all PRPDD graphs for all active channels for the current matrix (phase resolved data) display date
        /// </summary>
        private void DrawAllPRPDDPolarGraphs()
        {
            try
            {
                int zeroIndexedChannelNumber;
                int count;
                if (this.prpddPolarChartsNeedUpdating && (this.dataRadPageView.SelectedPage == prpddPolarRadPageViewPage))
                {
                    if (this.hasFinishedInitialization && (this.totalDataReadings > 0))
                    {
                        if (this.activeChannelNumberList != null)
                        {
                            count = this.activeChannelNumberList.Count;
                            HideAllPRPDDPolarDisplayObjects();

                            for (int i = 0; i < count; i++)
                            {
                                zeroIndexedChannelNumber = this.activeChannelNumberList[i];
                                SetupAndDrawOnePRPDDPolarGraph(i, zeroIndexedChannelNumber);

                                // this.prpddPolarChartPDIValueRadLabelList[i].Text = this.channelDataList[zeroIndexedChannelNumber].pdi[this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex].ToString();

                                this.prpddPolarChartPDIValueRadLabelList[i].Text = Math.Round(this.channelPdiValue[zeroIndexedChannelNumber + 1], 1).ToString();
                            }
                            PlaceAllPRPDDPolarChartGroupBoxes();
                            this.prpddPolarChartsNeedUpdating = false;
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawAllPRPDDGraphs()\nthis.activeChannelNumberList was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllPRPDDGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw one PRPDD graph for a given channel.
        /// </summary>
        /// <param name="zeroIndexChartNumber"></param>
        /// <param name="zeroIndexedChannelNumber"></param>
        private void SetupAndDrawOnePRPDDPolarGraph(int zeroIndexChartNumber, int zeroIndexedChannelNumber)
        {
            try
            {
                if ((zeroIndexChartNumber > -1) && (zeroIndexChartNumber < 15))
                {
                    if ((zeroIndexedChannelNumber > -1) && (zeroIndexedChannelNumber < 15))
                    {
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(this.prpddPolarChartPhaseShiftRadDropDownListList[zeroIndexChartNumber].SelectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.prpddPolarScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        //if ((this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0) ||
                        //           (this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0))
                        {
                            ShowPRPDDPolarDisplayObjectForOneChannel(zeroIndexChartNumber);
                            DrawPRPDDPolarChart(this.prpddPolarChartWinChartViewerList[zeroIndexChartNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                                           this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDPolarGraph(int, int)\nSecond input int needs to be between 0 and 14 inclusive";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDPolarGraph(int, int)\nFirst input int needs to be between 0 and 14 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupAndDrawOnePRPDDPolarGraph(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw one PHD graph for a given channel.
        /// </summary>
        /// <param name="zeroIndexChartNumber"></param>
        /// <param name="zeroIndexedChannelNumber"></param>
        private void SetupAndDrawOnePHDGraph(int zeroIndexChartNumber, int zeroIndexedChannelNumber)
        {
            try
            {
                if ((zeroIndexChartNumber > -1) && (zeroIndexChartNumber < 15))
                {
                    if ((zeroIndexedChannelNumber > -1) && (zeroIndexedChannelNumber < 15))
                    {
                        bool voltageSettingWasComputed = false;
                        int selectedIndex = this.matrixScaleRadDropDownList.SelectedIndex;
                        double voltageMax = ComputeMatrixYscaleSettingFromSelectedIndex(selectedIndex, zeroIndexedChannelNumber);
                        if ((selectedIndex == 1) || (selectedIndex == 2))
                        {
                            voltageSettingWasComputed = true;
                        }
                        //if ((this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0) ||
                        //    (this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0))
                        {
                            ShowPhdDisplayObjectForOneChannel(zeroIndexChartNumber);
                            DrawPHDChart(this.phdChartWinChartViewerList[zeroIndexChartNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                                           this.channelMinusMatrixDataList[zeroIndexedChannelNumber], voltageMax, voltageSettingWasComputed, zeroIndexedChannelNumber + 1);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePHDGraph(int, int)\nSecond input int needs to be between 0 and 14 inclusive";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOnePHDGraph(int, int)\nFirst input int needs to be between 0 and 14 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupAndDrawOnePHDGraph(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Displays all multi-chart data objects for all active channels
        /// </summary>
        private void SetDisplayedChannelsForCurrentSelectedMatrixDate()
        {
            try
            {
                HideAllDisplayObjects();
                int activeDisplayChannels = 0;
                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i] && this.channelMatrixDataIsNotEmpty[i])
                    {
                        ShowDisplayObjectsForOneChannel(activeDisplayChannels);
                        activeDisplayChannels++;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetDisplayedChannelsForCurrentSelectedMatrixDate()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw all 3D graphs for all active channels for the current matrix (phase resolved data) display date
        /// </summary>
        private void DrawAllThreeDeeGraphs()
        {
            try
            {
                int zeroIndexedChannelNumber;
                int count;
                if (this.threeDeeChartsNeedUpdating && (this.dataRadPageView.SelectedPage == this.threeDeeRadPageViewPage))
                {
                    if (this.hasFinishedInitialization && (this.totalDataReadings > 0))
                    {
                        if (this.activeChannelNumberList != null)
                        {
                            count = this.activeChannelNumberList.Count;
                            HideAllThreeDeeDisplayObjects();
                            for (int i = 0; i < count; i++)
                            {
                                zeroIndexedChannelNumber = this.activeChannelNumberList[i];
                                SetupAndDrawOneThreeDeeGraph(i, zeroIndexedChannelNumber);

                                // this.threeDeeChartPDIValueRadLabelList[i].Text = this.channelDataList[zeroIndexedChannelNumber].pdi[this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex].ToString();

                                this.threeDeeChartPDIValueRadLabelList[i].Text = Math.Round(this.channelPdiValue[zeroIndexedChannelNumber + 1], 1).ToString();
                            }
                            PlaceAllThreeDeeChartGroupBoxes();
                            this.threeDeeChartsNeedUpdating = false;
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawAllThreeDeeGraphs()\nthis.activeChannelNumberList was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllThreeDeeGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw one 3D graph for a given channel.
        /// </summary>
        /// <param name="zeroIndexChartNumber"></param>
        /// <param name="zeroIndexedChannelNumber"></param>
        private void SetupAndDrawOneThreeDeeGraph(int zeroIndexChartNumber, int zeroIndexedChannelNumber)
        {
            try
            {
                if ((zeroIndexChartNumber > -1) && (zeroIndexChartNumber < 15))
                {
                    if ((zeroIndexedChannelNumber > -1) && (zeroIndexedChannelNumber < 15))
                    {
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(this.matrixChartPhaseShiftRadDropDownListList[zeroIndexChartNumber].SelectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.threeDeeScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        //if ((this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0) ||
                        //           (this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0))
                        {
                            ShowThreeDeeDisplayObjectForOneChannel(zeroIndexChartNumber);
                            DrawThreeDeeChart(this.threeDeeChartWinChartViewerList[zeroIndexChartNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                                           this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOneThreeDeeGraph(int, int)\nSecond input int needs to be between 0 and 14 inclusive";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.SetupAndDrawOneThreeDeeGraph(int, int)\nFirst input int needs to be between 0 and 14 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupAndDrawOneThreeDeeGraph(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Gets the magnitudeSteps row number associated with the maxVoltageDisplayed.  Just used in the DrawThreeDeeChart function to help set up the data for graphing.
        /// </summary>
        /// <param name="maxVoltageDisplayed"></param>
        /// <returns></returns>
        private int GetMinimumRowNumberDisplayed(double maxVoltageDisplayed)
        {
            int minimumRowNumber = 1;
            try
            {
                if (maxVoltageDisplayed > this.magnitudeSteps[0])
                {
                    return 1;
                }
                for (int i = 0; i < 32; i++)
                {
                    if (this.magnitudeSteps[i] < maxVoltageDisplayed)
                    {
                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetMinimumRowNumberDisplayed(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return minimumRowNumber;
        }

        /// <summary>
        /// Draws one 3D chart
        /// </summary>
        /// <param name="chartViewer"></param>
        /// <param name="matrixDataPlus"></param>
        /// <param name="matrixDataMinus"></param>
        /// <param name="phaseShift"></param>
        /// <param name="localMaximumVoltage"></param>
        /// <param name="channelNumber"></param>
        private void DrawThreeDeeChart(WinChartViewer chartViewer, MatrixData matrixDataPlus, MatrixData matrixDataMinus, double phaseShift, double localMaximumVoltage, int channelNumber)
        {
            try
            {
                double[] pulseCounts;
                double[] magnitudeData;
                double[] phaseData;

                double[] localMagnitudeSteps;
                double localMaximumValue = ConvertOneVoltageToSelectedUnits(localMaximumVoltage, (channelNumber - 1));

                int sizeOfPulseCounts = 0;
                int pointCount = 0;
                int pulseCountIndex = 0;

                string amplitudeScaleString = string.Empty;
                double dbCorrectionConstant = 0;
                bool amplitudeScaleWasCorrectlySelected = true;

                if (chartViewer != null)
                {
                    if (matrixDataPlus != null)
                    {
                        if (matrixDataMinus != null)
                        {
                            localMagnitudeSteps = GetLocalMagnitudeStepsInSelectedUnits(channelNumber - 1);
                            if (localMagnitudeSteps != null)
                            {
                                if (this.matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (dB)";
                                    dbCorrectionConstant = 80.0;
                                    //  scaleMax += dbCorrectionConstant;
                                }
                                else if (this.matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (V)";
                                }
                                else if (this.matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    amplitudeScaleString = "Amplitude (pC)";
                                }
                                else
                                {
                                    amplitudeScaleWasCorrectlySelected = false;
                                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawPRPDDChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nAmplitude scale was not correctly selected.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (amplitudeScaleWasCorrectlySelected)
                                {
                                    //pointCount = matrixDataPlus.matrixEntries[this.allMatrixDateTimeSelectedIndex].Count +
                                    //             matrixDataMinus.matrixEntries[this.allMatrixDateTimeSelectedIndex].Count;
                                    //if (pointCount > 0)
                                    {
                                        /// we can use this computation no matter what units we're displaying in
                                        int minimumRowNumberDisplayed = GetMinimumRowNumberDisplayed(localMaximumVoltage);
                                        int numberOfRowsBeingDisplayed = 32 - minimumRowNumberDisplayed + 1;

                                        magnitudeData = new double[numberOfRowsBeingDisplayed];

                                        for (int i = 0; i < numberOfRowsBeingDisplayed; i++)
                                        {
                                            magnitudeData[i] = localMagnitudeSteps[31 - i];
                                        }

                                        //magnitudeData = new double[numberOfRowsBeingDisplayed + 1];
                                        //if (numberOfRowsBeingDisplayed < 32)
                                        //{
                                        //    for (int i = 0; i <= numberOfRowsBeingDisplayed; i++)
                                        //    {
                                        //        magnitudeData[i] = this.magnitudeSteps[31 - i];
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    for (int i = 0; i < 32; i++)
                                        //    {
                                        //        magnitudeData[i] = this.magnitudeSteps[31 - i];
                                        //    }
                                        //    magnitudeData[32] = 10.0;
                                        //}

                                        int totalEntries = numberOfRowsBeingDisplayed * 48;

                                        pulseCounts = new double[totalEntries];



                                        //pulseCounts = new double[(numberOfRowsBeingDisplayed +1) * 48];

                                        sizeOfPulseCounts = pulseCounts.Length;

                                        int colorBeingUsed;

                                        try
                                        {
                                            // Note that the matricies are saved in 1-index form, but C# uses 0-index form, so we
                                            // need to convert it.
                                            foreach (MatrixEntry matrixEntry in matrixDataPlus.matrixEntries)
                                            {
                                                if (matrixEntry.rowNumber >= minimumRowNumberDisplayed)
                                                {
                                                    pulseCountIndex = ((numberOfRowsBeingDisplayed - 2 - (matrixEntry.rowNumber - minimumRowNumberDisplayed - 1)) * 48 + (matrixEntry.columnNumber - 1));
                                                    if (pulseCountIndex > (numberOfRowsBeingDisplayed * 48 - 1))
                                                    {
                                                        pulseCountIndex = 0;
                                                    }
                                                    else if (pulseCountIndex < 0)
                                                    {
                                                        pulseCountIndex = 0;
                                                    }
                                                    pulseCounts[pulseCountIndex] += matrixEntry.value;
                                                }
                                            }

                                            foreach (MatrixEntry matrixEntry in matrixDataMinus.matrixEntries)
                                            {
                                                if (matrixEntry.rowNumber >= minimumRowNumberDisplayed)
                                                {
                                                    pulseCountIndex = ((numberOfRowsBeingDisplayed - 2 - (matrixEntry.rowNumber - minimumRowNumberDisplayed - 1)) * 48 + (matrixEntry.columnNumber - 1));
                                                    if (pulseCountIndex > (numberOfRowsBeingDisplayed * 48 - 1))
                                                    {
                                                        pulseCountIndex = 0;
                                                    }
                                                    else if (pulseCountIndex < 0)
                                                    {
                                                        pulseCountIndex = 0;
                                                    }
                                                    pulseCounts[pulseCountIndex] += matrixEntry.value;
                                                }
                                            }

                                            // now, correct those values that are zero by making them even smaller
                                            for (int i = 0; i < totalEntries; i++)
                                            {
                                                if (pulseCounts[i] < .5)
                                                {
                                                    pulseCounts[i] = -0.01;
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("Error, message is:" + ex.Message);
                                        }

                                        // Create a SurfaceChart object of size 750 x 600 pixels
                                        SurfaceChart c = new SurfaceChart(this.multiChartWidth, this.multiChartHeight);

                                        // Add a title to the chart using 20 points Times New Roman Italic font
                                        //c.addTitle("Surface Energy Density       ", "Times New Roman Italic", 20);

                                        // Set the center of the plot region at (380, 260), and set width x depth
                                        // x height to 360 x 360 x 270 pixels
                                        int width = this.multiChartWidth / 2;
                                        int height = this.multiChartHeight / 2;

                                        int widthAdjust = 0;
                                        int depthAdjust = 0;
                                        int zHeightAdjust = 0;

                                        /// We will start adjusting at the point where the graph is bigger than the one in a 5x3 setting with the window at minimum size
                                        if (width > 158)
                                        {
                                            widthAdjust = (int)((width - 158) * .65);
                                            depthAdjust = (int)((width - 158) * .04);
                                            zHeightAdjust = (int)((width - 158) * .4);
                                        }

                                        // original mulitpliers are .95, .95, .8
                                        c.setPlotRegion(width - 5, height - 20, (int)(width * .95) + widthAdjust, (int)(height * .95) + depthAdjust, (int)(Math.Min(width, height) * .8) + zHeightAdjust);

                                        // Set the elevation and rotation angles to 30 and 210 degrees
                                        c.setViewAngle(30, 210);

                                        // Set the perspective level to 60
                                        // c.setPerspective(60);

                                        phaseData = this.phaseSteps;

                                        if (phaseShift > 1e-7)
                                        {
                                            phaseData = ShiftPhaseData(phaseData, phaseShift);
                                        }

                                        // Set the data to use to plot the chart
                                        c.setData(phaseData, magnitudeData, pulseCounts);

                                        // Spline interpolate data to a 80 x 80 grid for a smooth surface
                                        // c.setInterpolation(10, 10);

                                        // Use semi-transparent black (c0000000) for x and y major surface grid
                                        // lines. Use dotted style for x and y minor surface grid lines.
                                        int majorGridColor = unchecked((int)0xc0000000);
                                        int minorGridColor = c.dashLineColor(majorGridColor, Chart.DotLine);
                                        c.setSurfaceAxisGrid(majorGridColor, majorGridColor, minorGridColor,
                                            minorGridColor);

                                        // Set contour lines to semi-transparent white (80ffffff)
                                        c.setContourColor(unchecked((int)0x80ffffff));

                                        c.setWallThickness(0, -1, -1);

                                        //// Add a color axis (the legend) in which the left center is anchored at
                                        //// (665, 280). Set the length to 200 pixels and the labels on the right
                                        //// side.
                                        //c.setColorAxis(665, 280, Chart.Left, 200, Chart.Right);

                                        c.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10).setBackground(0xccccff, 0x0, Chart.glassEffect());

                                        //// Set the x, y and z axis titles using 12 points Arial Bold font
                                        c.xAxis().setTitle("Phase (deg)", "Arial", 8);
                                        c.yAxis().setTitle(amplitudeScaleString, "Arial", 8);
                                        c.zAxis().setTitle("Pulse Count", "Arial", 8);

                                        c.xAxis().setLinearScale(0, 360, 90, 15);

                                        //c.zAxis().setLinearScale(0, Chart.NoValue);
                                        //c.zAxis().setRounding(false, true);


                                        //c.colorAxis().syncAxis(c.colorAxis());
                                        //c.colorAxis().setLinearScale(0, Chart.NoValue);
                                        //c.colorAxis().setRounding(false, true);
                                        //c.colorAxis().setColorGradient(true, null, Chart.Transparent, Chart.Transparent);

                                        c.colorAxis().syncAxis(c.colorAxis());
                                        c.colorAxis().setLinearScale(0, Chart.NoValue);
                                        c.colorAxis().setColorGradient(true, null, Chart.Transparent, Chart.Transparent);

                                        c.zAxis().setLinearScale(-0.01, Chart.NoValue);
                                        c.zAxis().setRounding(false, true);

                                        // Output the chart
                                        chartViewer.Image = c.makeImage();
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PhaseResolvedDataViewer.DrawThreeDeeChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFailed to compute localMagnitudeSteps.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawThreeDeeChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nSecond MatrixData input was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.DrawThreeDeeChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nFirst MatrixData input was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.DrawThreeDeeChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawThreeDeeChart(WinChartViewer, MatrixData, MatrixData, double, double, int)\nMessage:" + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the data to draw all PHD graphs for all active channels for the current matrix (phase resolved data) display date
        /// </summary>
        private void DrawAllPhdGraphs()
        {
            try
            {
                int zeroIndexedChannelNumber;
                int count;
                if (this.phdChartsNeedUpdating && (this.dataRadPageView.SelectedPage == phdRadPageViewPage))
                {
                    if (this.hasFinishedInitialization && (this.totalDataReadings > 0))
                    {
                        if (this.activeChannelNumberList != null)
                        {
                            count = this.activeChannelNumberList.Count;
                            HideAllPhdDisplayObjects();
                            for (int i = 0; i < count; i++)
                            {
                                zeroIndexedChannelNumber = this.activeChannelNumberList[i];
                                SetupAndDrawOnePHDGraph(i, zeroIndexedChannelNumber);

                                // this.phdChartRadLabelList[i].Text = this.channelDataList[zeroIndexedChannelNumber].pdi[this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex].ToString();

                                this.phdChartRadLabelList[i].Text = Math.Round(this.channelPdiValue[zeroIndexedChannelNumber + 1], 1).ToString();
                            }
                            PlaceAllPhdChartGroupBoxes();
                            this.phdChartsNeedUpdating = false;
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.DrawAllPhdGraphs()\nthis.activeChannelNumberList was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllPhdGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the state of the charts such that they need updating after a resize event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhaseResolvedDataViewer_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                this.trendChartNeedsUpdating = true;
                SetAllMultiChartsAsNeedingUpdating();
                ResizeMultiCharts();
               
                DrawAllGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhaseResolvedDataViewer_SizeChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllMultiChartsAsNeedingUpdating()
        {
            try
            {
            this.matrixChartsNeedUpdating = true;
            this.prpddPolarChartsNeedUpdating = true;
            this.threeDeeChartsNeedUpdating = true;
            this.phdChartsNeedUpdating = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetAllMultiChartsAsNeedingUpdating()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #region Multi-chart Layout and Layout Switching Radio Button Event Handlers

        /// <summary>
        /// Re-sizes all the multi-charts to reflect a newly selected layout and/or window size.
        /// </summary>
        private void ResizeMultiCharts()
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    HideAllDisplayObjects();
                    if (matrixThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        this.multiChartNumberOfColumns = 5;
                        ComputeMultiChartWidthAndHeight();
                        SetupThreeByFiveChartLayout();
                        //if (matrixAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        //{
                        //    DrawAllPRPDDGraphs();
                        //}
                    }
                    else if (matrixEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        this.multiChartNumberOfColumns = 2;
                        ComputeMultiChartWidthAndHeight();
                        SetupEightByTwoChartLayout();
                        //if (matrixAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        //{
                        //    DrawAllPRPDDGraphs();
                        //}
                    }
                    else if (matrixFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        this.multiChartNumberOfColumns = 3;
                        ComputeMultiChartWidthAndHeight();
                        SetupFiveByThreeChartLayout();
                        //if (matrixAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        //{
                        //    DrawAllPRPDDGraphs();
                        //}
                    }
                    else if (matrixFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        this.multiChartNumberOfColumns = 1;
                        ComputeMultiChartWidthAndHeight();
                        SetupFifteenByOneChartLayout();
                        //if (matrixAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        //{
                        //    DrawAllPRPDDGraphs();
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ResizeMultiCharts()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Computes the size of an individual multi-chart entry based on the new layout settings.
        /// </summary>
        private void ComputeMultiChartWidthAndHeight()
        {
            try
            {
                int chartAreaWidth;

                /// 1024 (smallest width of form) - 995 (smallest width of multiChartPanel) = 29
                chartAreaWidth = this.Width - 29;

                /// width allowed = (chartAreaWidth - space for slider  - spaces between each chart)/ total number
                /// of charts in a given row.
                this.multiChartWidth = (int)Math.Floor((chartAreaWidth - 25 - ((this.multiChartNumberOfColumns + 1) * 3)) / (double)this.multiChartNumberOfColumns);
                this.multiChartHeight = (int)Math.Floor(this.multiChartWidth / this.multiChartWidthToHeightRatio);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ComputeMultiChartWidthAndHeight()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Places and sizes all multi-charts for one chart position (out of 15 total positions)
        /// </summary>
        /// <param name="matrixWinChartViewer"></param>
        /// <param name="threeDeeWinChartViewer"></param>
        /// <param name="phdWinChartViewer"></param>
        /// <param name="chartLocation"></param>
        /// <param name="chartSize"></param>
        private void PlaceAndSizeCharts(int zeroBasedChartNumberIndex, Point chartLocation, Size chartSize)
        {
            try
            {
                if ((zeroBasedChartNumberIndex > -1) && (zeroBasedChartNumberIndex < 15))
                {
                    if ((this.matrixChartWinChartViewerList != null) && (this.matrixChartWinChartViewerList.Count > zeroBasedChartNumberIndex))
                    {
                        if ((this.prpddPolarChartWinChartViewerList != null) && (this.prpddPolarChartWinChartViewerList.Count > zeroBasedChartNumberIndex))
                        {
                            if ((this.threeDeeChartWinChartViewerList != null) && (this.threeDeeChartWinChartViewerList.Count > zeroBasedChartNumberIndex))
                            {
                                if ((this.phdChartWinChartViewerList != null) && (this.phdChartWinChartViewerList.Count > zeroBasedChartNumberIndex))
                                {
                                    this.matrixChartWinChartViewerList[zeroBasedChartNumberIndex].Location = chartLocation;
                                    this.prpddPolarChartWinChartViewerList[zeroBasedChartNumberIndex].Location = chartLocation;
                                    this.threeDeeChartWinChartViewerList[zeroBasedChartNumberIndex].Location = chartLocation;
                                    this.phdChartWinChartViewerList[zeroBasedChartNumberIndex].Location = chartLocation;

                                    this.matrixChartWinChartViewerList[zeroBasedChartNumberIndex].Size = chartSize;
                                    this.prpddPolarChartWinChartViewerList[zeroBasedChartNumberIndex].Size = chartSize;
                                    this.threeDeeChartWinChartViewerList[zeroBasedChartNumberIndex].Size = chartSize;
                                    this.phdChartWinChartViewerList[zeroBasedChartNumberIndex].Size = chartSize;
                                }
                                else
                                {
                                    string errorMessage = "Error in PhaseResolvedDataViewer.PlaceAndSizeCharts(int, Point, Size)\nthis.phdChartWinChartViewerList was null or had too few elements";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PhaseResolvedDataViewer.PlaceAndSizeCharts(int, Point, Size)\nthis.threeDeeChartWinChartViewerList was null or had too few elements";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.PlaceAndSizeCharts(int, Point, Size)\nthis.prprrPolarChartWinChartViewerList was null or had too few elements";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.PlaceAndSizeCharts(int, Point, Size)\nthis.matrixChartWinChartViewerList was null or had too few elements";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.PlaceAndSizeCharts(int, Point, Size)\nInput zeroBasedChartNumberIndex was out of range.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceAndSizeCharts(ref WinChartViewer, ref WinChartViewer, ref WinChartViewer, Point, Size)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Calculates the chart positions for a 3x5 chart layout and positions all the charts
        /// </summary>
        private void SetupThreeByFiveChartLayout()
        {
            try
            {
                int xOffset = 3;
                int yOffset = 3;
                Size chartSize = new Size(this.multiChartWidth, this.multiChartHeight);

                ///Row 1
                Point chart1Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(0, chart1Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart2Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(1, chart2Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart3Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(2, chart3Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart4Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(3, chart4Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart5Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(4, chart5Point, chartSize);

                //Row 5

                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart6Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(5, chart6Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart7Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(6, chart7Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart8Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(7, chart8Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart9Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(8, chart9Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart10Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(9, chart10Point, chartSize);

                //Row 3

                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart11Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(10, chart11Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart12Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(11, chart12Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart13Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(12, chart13Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart14Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(13, chart14Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart15Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(14, chart15Point, chartSize);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupThreeByFiveChartLayout()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Calculates the chart positions for a 5x3 chart layout and positions all the charts
        /// </summary>
        private void SetupFiveByThreeChartLayout()
        {
            try
            {
                int xOffset = 3;
                int yOffset = 3;
                Size chartSize = new Size(this.multiChartWidth, this.multiChartHeight);

                ///Row 1
                Point chart1Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(0, chart1Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart2Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(1, chart2Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart3Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(2, chart3Point, chartSize);

                ///Row 2

                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart4Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(3, chart4Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart5Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(4, chart5Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart6Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(5, chart6Point, chartSize);

                /// Row 3
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart7Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(6, chart7Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart8Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(7, chart8Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart9Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(8, chart9Point, chartSize);

                /// Row 4
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart10Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(9, chart10Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart11Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(10, chart11Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart12Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(11, chart12Point, chartSize);

                ///Row 5
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart13Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(12, chart13Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart14Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(13, chart14Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart15Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(14, chart15Point, chartSize);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupFiveByThreeChartLayout()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Calculates the chart positions for a 8x2 chart layout and positions all the charts
        /// </summary>
        private void SetupEightByTwoChartLayout()
        {
            try
            {
                int xOffset = 3;
                int yOffset = 3;
                Size chartSize = new Size(this.multiChartWidth, this.multiChartHeight);

                /// Row 1
                Point chart1Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(0, chart1Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart2Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(1, chart2Point, chartSize);

                /// Row 2
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart3Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(2, chart3Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart4Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(3, chart4Point, chartSize);

                /// Row 3
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart5Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(4, chart5Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart6Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(5, chart6Point, chartSize);

                /// Row 4
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart7Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(6, chart7Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart8Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(7, chart8Point, chartSize);

                /// Row 5
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart9Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(8, chart9Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart10Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(9, chart10Point, chartSize);

                /// Row 6
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart11Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(10, chart11Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart12Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(11, chart12Point, chartSize);

                /// Row 7
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart13Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(12, chart13Point, chartSize);

                xOffset += this.multiChartWidth + 3;
                Point chart14Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(13, chart14Point, chartSize);

                /// Row 8 (1 column entry only)
                xOffset = 3;
                yOffset += this.multiChartHeight + 3;

                Point chart15Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(14, chart15Point, chartSize);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupEightByTwoChartLayout()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Calculates the chart positions for a 15x1 chart layout and positions all the charts
        /// </summary>
        private void SetupFifteenByOneChartLayout()
        {
            try
            {
                int xOffset = 3;
                int yOffset = 3;
                Size chartSize = new Size(this.multiChartWidth, this.multiChartHeight);

                /// Row 1
                Point chart1Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(0, chart1Point, chartSize);

                /// Row 2
                yOffset += this.multiChartHeight + 3;

                Point chart2Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(1, chart2Point, chartSize);

                /// Row 3
                yOffset += this.multiChartHeight + 3;

                Point chart3Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(2, chart3Point, chartSize);

                /// Row 4
                yOffset += this.multiChartHeight + 3;

                Point chart4Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(3, chart4Point, chartSize);

                /// Row 5
                yOffset += this.multiChartHeight + 3;

                Point chart5Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(4, chart5Point, chartSize);

                /// Row 6
                yOffset += this.multiChartHeight + 3;

                Point chart6Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(5, chart6Point, chartSize);

                /// Row 7
                yOffset += this.multiChartHeight + 3;

                Point chart7Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(6, chart7Point, chartSize);

                /// Row 8
                yOffset += this.multiChartHeight + 3;

                Point chart8Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(7, chart8Point, chartSize);

                /// Row 9
                yOffset += this.multiChartHeight + 3;

                Point chart9Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(8, chart9Point, chartSize);

                /// Row 10
                yOffset += this.multiChartHeight + 3;

                Point chart10Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(9, chart10Point, chartSize);

                /// Row 11
                yOffset += this.multiChartHeight + 3;

                Point chart11Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(10, chart11Point, chartSize);

                /// Row 12
                yOffset += this.multiChartHeight + 3;

                Point chart12Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(11, chart12Point, chartSize);

                /// Row 13
                yOffset += this.multiChartHeight + 3;

                Point chart13Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(12, chart13Point, chartSize);

                /// Row 14
                yOffset += this.multiChartHeight + 3;

                Point chart14Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(13, chart14Point, chartSize);

                /// Row 15
                yOffset += this.multiChartHeight + 3;

                Point chart15Point = new Point(xOffset, yOffset);
                PlaceAndSizeCharts(14, chart15Point, chartSize);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupFifteenByOne()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the chart layout radio buttons to the 3x5 state
        /// </summary>
        private void SetThreeByFiveRadRadioButtonState()
        {
            try
            {
                if (matrixThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetThreeByFiveRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the chart layout radio buttons to the 5x3 state
        /// </summary>
        private void SetFiveByThreeRadRadioButtonState()
        {
            try
            {
                if (matrixFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixFiveByThreeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarFiveByThreeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeFiveByThreeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdFiveByThreeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetFiveByThreeRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the chart layout radio buttons to the 8x2 state
        /// </summary>
        private void SetEightByTwoRadRadioButtonState()
        {
            try
            {
                if (matrixEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixEightByTwoRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarEightByTwoRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeEightByTwoRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdEightByTwoRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetEightByTwoRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the chart layout radio buttons to the 15x1 state
        /// </summary>
        private void SetFifteenByOneRadRadioButtonState()
        {
            try
            {
                if (matrixFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixFifteenByOneRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarFifteenByOneRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeFifteenByOneRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdFifteenByOneRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetFifteenByOneRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeByFiveRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetThreeByFiveRadRadioButtonState();
                    ResizeMultiCharts();
                    this.matrixChartsNeedUpdating = true;
    
                        DrawAllPRPDDGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeByFiveRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void eightByTwoRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetEightByTwoRadRadioButtonState();
                    ResizeMultiCharts();
                    this.matrixChartsNeedUpdating = true;
                    
                        DrawAllPRPDDGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.eightByTwoRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void fiveByThreeRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFiveByThreeRadRadioButtonState();
                    ResizeMultiCharts();
                    this.matrixChartsNeedUpdating = true;
                   
                        DrawAllPRPDDGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.fiveByThreeRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void fifteenByOneRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFifteenByOneRadRadioButtonState();
                    ResizeMultiCharts();
                    this.matrixChartsNeedUpdating = true;
                   
                        DrawAllPRPDDGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.fifteenByOneRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarThreeByFiveRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetThreeByFiveRadRadioButtonState();
                    ResizeMultiCharts();
                    this.prpddPolarChartsNeedUpdating = true;
                    
                        DrawAllPRPDDPolarGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeByFiveRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarEightByTwoRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetEightByTwoRadRadioButtonState();
                    ResizeMultiCharts();
                    this.prpddPolarChartsNeedUpdating = true;

                    DrawAllPRPDDPolarGraphs();

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolareightByTwoRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarFiveByThreeRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFiveByThreeRadRadioButtonState();
                    ResizeMultiCharts();
                    this.prpddPolarChartsNeedUpdating = true;

                    DrawAllPRPDDPolarGraphs();

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarFiveByThreeRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarfifteenByOneRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFifteenByOneRadRadioButtonState();
                    ResizeMultiCharts();
                    this.prpddPolarChartsNeedUpdating = true;
       
                        DrawAllPRPDDPolarGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarFifteenByOneRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeThreeByFiveRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetThreeByFiveRadRadioButtonState();
                    this.threeDeeChartsNeedUpdating = true;
         
                        DrawAllThreeDeeGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeThreeByFiveRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeFiveByThreeRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFiveByThreeRadRadioButtonState();
                    this.threeDeeChartsNeedUpdating = true;
               
                        DrawAllThreeDeeGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeFiveByThreeRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeEightByTwoRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetEightByTwoRadRadioButtonState();
                    this.threeDeeChartsNeedUpdating = true;
                
                        DrawAllThreeDeeGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeFiveByThreeRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeFifteenByOneRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFifteenByOneRadRadioButtonState();
                    this.threeDeeChartsNeedUpdating = true;
                
                        DrawAllThreeDeeGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeFifteenByOneRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdThreeByFiveRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdThreeByFiveRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetThreeByFiveRadRadioButtonState();
                    this.phdChartsNeedUpdating = true;
                 
                        DrawAllPhdGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdThreeByFiveRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdFiveByThreeRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdFiveByThreeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFiveByThreeRadRadioButtonState();
                    this.phdChartsNeedUpdating = true;
                
                        DrawAllPhdGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdFiveByThreeRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdEightByTwoRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdEightByTwoRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetEightByTwoRadRadioButtonState();
                    this.phdChartsNeedUpdating = true;
                
                        DrawAllPhdGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdEightByTwoRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdFifteenByOneRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdFifteenByOneRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetFifteenByOneRadRadioButtonState();
                    this.phdChartsNeedUpdating = true;
                    
                        DrawAllPhdGraphs();
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdFifteenByOneRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        /// <summary>
        /// Makes the call to draw all graphs in the interface.  Only the graph in the selected tab will actually be rendered.
        /// </summary>
        private void DrawAllGraphs()
        {
            try
            {
                DrawAllMultiChartGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DrawAllGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

     
        #region Amplitude Select Rad Radio Button Handlers

        /// <summary>
        /// Sets the amplitude radio buttons in all tabs to the dB state
        /// </summary>
        private void SetDbRadRadioButtonState()
        {
            try
            {
                matrixAmplitudeDisplayType = AmplitudeDisplayType.dB;
                prpddPolarAmplitudeDisplayType = AmplitudeDisplayType.dB;
                threeDeeAmplitudeDisplayType = AmplitudeDisplayType.dB;
                phdAmplitudeDisplayType = AmplitudeDisplayType.dB;

                if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdDbRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetDbRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the amplitude radio buttons in all tabs to the V state
        /// </summary>
        private void SetVRadRadioButtonState()
        {
            try
            {
                matrixAmplitudeDisplayType = AmplitudeDisplayType.V;
                prpddPolarAmplitudeDisplayType = AmplitudeDisplayType.V;
                threeDeeAmplitudeDisplayType = AmplitudeDisplayType.V;
                phdAmplitudeDisplayType = AmplitudeDisplayType.V;

                if (matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeeVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeeVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetVRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the amplitude radio buttons in all tabs to the nC state
        /// </summary>
        private void SetPcRadRadioButtonState()
        {
            try
            {
                matrixAmplitudeDisplayType = AmplitudeDisplayType.pC;
                prpddPolarAmplitudeDisplayType = AmplitudeDisplayType.pC;
                threeDeeAmplitudeDisplayType = AmplitudeDisplayType.pC;
                phdAmplitudeDisplayType = AmplitudeDisplayType.pC;

                if (matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    matrixPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (prpddPolarPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    prpddPolarPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (threeDeePcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    threeDeePcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (phdPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    phdPcRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetPcRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void VRadioButtonHandler()
        {
            try
            {
                SetVRadRadioButtonState();
                FillAllScaleRadDropDownListEntries(this.voltageScaleEntries);
                SetAllMultiChartsAsNeedingUpdating();
                DrawAllMultiChartGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.VRadioButtonHandler()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void NcRadioButtonHandler()
        {
            try
            {
                //if (this.channelSensentivitiesList != null)
                //{
                SetPcRadRadioButtonState();
                FillAllScaleRadDropDownListEntries(this.nonVoltageScaleEntries);
                SetAllMultiChartsAsNeedingUpdating();
                DrawAllMultiChartGraphs();
                //}
                //else
                //{
                //    RadMessageBox.Show("No device configuration was found in the database.\nCannot display the results in nano Coulombs without that information.\nPlease add a configuration for this device to the database.");
                //    matrixVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.NcRadioButtonHandler()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DbRadioButtonHandler()
        {
            try
            {
                SetDbRadRadioButtonState();
                FillAllScaleRadDropDownListEntries(this.nonVoltageScaleEntries);
                SetAllMultiChartsAsNeedingUpdating();
                DrawAllMultiChartGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.DbRadioButtonHandler()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixDbRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (matrixAmplitudeDisplayType != AmplitudeDisplayType.dB)
                    {
                        DbRadioButtonHandler();
                        //matrixPointerRadioButton.Checked = true;
                        //MatrixDisableZooming();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixDbRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixVRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (matrixAmplitudeDisplayType != AmplitudeDisplayType.V)
                    {
                        VRadioButtonHandler();
                        //MatrixEnableZooming();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixVRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixPcRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (matrixAmplitudeDisplayType != AmplitudeDisplayType.pC)
                    {
                        NcRadioButtonHandler();
                        //MatrixEnableZooming();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixPcRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarDbRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (prpddPolarAmplitudeDisplayType != AmplitudeDisplayType.dB)
                    {
                        DbRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarDbRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarVRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (prpddPolarAmplitudeDisplayType != AmplitudeDisplayType.V)
                    {
                        VRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarVRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarPcRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (prpddPolarPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (prpddPolarAmplitudeDisplayType != AmplitudeDisplayType.pC)
                    {
                        NcRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarPcRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeDbRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (threeDeeAmplitudeDisplayType != AmplitudeDisplayType.dB)
                    {
                        DbRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeDbRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeVRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeeVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (threeDeeAmplitudeDisplayType != AmplitudeDisplayType.V)
                    {
                        VRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeVRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeePcRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (threeDeePcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (threeDeeAmplitudeDisplayType != AmplitudeDisplayType.pC)
                    {
                        NcRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeePcRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdDbRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (phdAmplitudeDisplayType != AmplitudeDisplayType.dB)
                    {
                        DbRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdDbRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdVRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (phdAmplitudeDisplayType != AmplitudeDisplayType.V)
                    {
                        VRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdVRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdPcRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (phdPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (phdAmplitudeDisplayType != AmplitudeDisplayType.pC)
                    {
                        NcRadioButtonHandler();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdPcRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        private void dataRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {
            try
            {
                DrawAllGraphs();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.dataRadPageView_SelectedPageChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
 
//        private void SetMultiChartSupportValuesForNewDataIndex(int newSelectedIndex)
//        {
//            try
//            {
//                if (newSelectedIndex != this.allMatrixDateTimeSelectedIndex)
//                {
//                    this.allMatrixDateTimeSelectedIndex = newSelectedIndex;
//                    SetAllDataDateRadDropDownListSelectedIndices();
//                    SetDisplayedChannelsForCurrentSelectedMatrixDate();
//                    SetAllDataIndexCorrespondingToCurrentMatrixIndex();
//                    SetAllMultiChartsAsNeedingUpdating();
//                    this.activeChannelsForSelectedDate = GetActiveChannels(this.commonData.registeredChannels[this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex]);
//                    this.activeChannelNumberList = GetActiveChannelNumberList(this.activeChannelsForSelectedDate);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetMultiChartSupportValuesForNewDataIndex(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        /// <summary>
        /// Sets all booleans that indicate that the multi-charts need to be redrawn the next time the user switches to their tab
        /// </summary>
//        private void SetAllMultiChartsAsNeedingUpdating()
//        {
//            try
//            {
//                this.matrixChartsNeedUpdating = true;
//                this.prpddPolarChartsNeedUpdating = true;
//                this.threeDeeChartsNeedUpdating = true;
//                this.phdChartsNeedUpdating = true;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetAllMultiChartsAsNeedingUpdating()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        /// <summary>
        /// Sets all the DataDateRadDropDownLists in all tabs to the same selected index.  This keeps them all in sync with each other.
        /// </summary>
//        private void SetAllDataDateRadDropDownListSelectedIndices()
//        {
//            try
//            {
//                matrixDataDateRadDropDownList.SelectedIndex = this.allMatrixDateTimeSelectedIndex;
//                prpddPolarDataDateRadDropDownList.SelectedIndex = this.allMatrixDateTimeSelectedIndex;
//                threeDeeDataDateRadDropDownList.SelectedIndex = this.allMatrixDateTimeSelectedIndex;
//                phdDataDateRadDropDownList.SelectedIndex = this.allMatrixDateTimeSelectedIndex;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetAllDataDateRadDropDownListSelectedIndices()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        /// <summary>
        /// Sets the value of allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex 
        /// </summary>
//        private void SetAllDataIndexCorrespondingToCurrentMatrixIndex()
//        {
//            try
//            {
//                double selectedDateTime = this.allMatrixDateTimesAsDouble[this.allMatrixDateTimeSelectedIndex];
//                this.allDateTimeIndexWithSameDateAsAllMatrixDateTimeSelectedIndex = FindAllDateTimeAsDoubleIndex(selectedDateTime);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetAllDataIndexCorrespondingToCurrentMatrixIndex()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void matrixScaleRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixScaleRadDropDownList.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.multiChartScaleSelectedIndex)
                    {
                        this.multiChartScaleSelectedIndex = selectedIndex;
                        SetScaleRadDropDownListSelectedIndices();
                       SetAllMultiChartsAsNeedingUpdating();
                       
                            DrawAllPRPDDGraphs();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixScaleRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarScaleRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarScaleRadDropDownList.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.multiChartScaleSelectedIndex)
                    {
                        this.multiChartScaleSelectedIndex = selectedIndex;
                        SetScaleRadDropDownListSelectedIndices();
                        SetAllMultiChartsAsNeedingUpdating();
                       
                            DrawAllPRPDDGraphs();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixScaleRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeScaleRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeScaleRadDropDownList.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.multiChartScaleSelectedIndex)
                    {
                        this.multiChartScaleSelectedIndex = selectedIndex;
                        SetScaleRadDropDownListSelectedIndices();
                        SetAllMultiChartsAsNeedingUpdating();
                        
                            DrawAllThreeDeeGraphs();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeScaleRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdScaleRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = phdScaleRadDropDownList.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.multiChartScaleSelectedIndex)
                    {
                        this.multiChartScaleSelectedIndex = selectedIndex;
                        SetScaleRadDropDownListSelectedIndices();
                        SetAllMultiChartsAsNeedingUpdating();
                       
                            DrawAllPhdGraphs();
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdScaleRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets all the ScaleRadDropDownLists in all tabs to the same selected index.  This keeps them all in sync with each other.
        /// </summary>
        private void SetScaleRadDropDownListSelectedIndices()
        {
            try
            {
                matrixScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                prpddPolarScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                threeDeeScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                phdScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetScaleRadDropDownListSelectedIndices()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        /// <summary>
        /// Fills the scale lists with values
        /// </summary>
        private void InitializeScaleRadDropDownListEntries()
        {
            try
            {
                this.voltageScaleEntries = new List<string>();
                this.nonVoltageScaleEntries = new List<string>();

                this.voltageScaleEntries.Add("Full");
                this.nonVoltageScaleEntries.Add("Full");

                this.voltageScaleEntries.Add("Local maximum");
                this.nonVoltageScaleEntries.Add("Local maximum");

                this.voltageScaleEntries.Add("Maximum");
                this.nonVoltageScaleEntries.Add("Maximum");

                this.voltageScaleEntries.Add("0.01 V");
                this.voltageScaleEntries.Add("0.02 V");
                this.voltageScaleEntries.Add("0.05 V");
                this.voltageScaleEntries.Add("0.1 V");
                this.voltageScaleEntries.Add("0.2 V");
                this.voltageScaleEntries.Add("0.5 V");
                this.voltageScaleEntries.Add("1 V");
                this.voltageScaleEntries.Add("2.5 V");
                this.voltageScaleEntries.Add("5 V");
                this.voltageScaleEntries.Add("10 V");
                // this.voltageScaleEntries.Add("25 V");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeScaleRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Loads any radDropDownList with the strings passed in names
        /// </summary>
        /// <param name="radDropDownList"></param>
        /// <param name="names"></param>
        private void LoadRadDropDownListWithEntries(ref RadDropDownList radDropDownList, List<string> names)
        {
            try
            {
                if (radDropDownList != null)
                {
                    if (names != null)
                    {
                        RadListDataItem entry;
                        radDropDownList.Items.Clear();

                        foreach (string name in names)
                        {
                            entry = new RadListDataItem();
                            entry.Text = name;
                            radDropDownList.Items.Add(entry);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.LoadRadDropDownListWithEntries(ref RadDropDownList, List<string>)\nInput List<string> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.LoadRadDropDownListWithEntries(ref RadDropDownList, List<string>)\nInput RadDropDownList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.LoadRadDropDownListWithEntries(ref RadDropDownList, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills all scale RadDropDownLists with the appropriate values
        /// </summary>
        /// <param name="scaleNames"></param>
        private void FillAllScaleRadDropDownListEntries(List<string> scaleNames)
        {
            try
            {
                if (scaleNames != null)
                {
                    LoadRadDropDownListWithEntries(ref matrixScaleRadDropDownList, scaleNames);
                    LoadRadDropDownListWithEntries(ref prpddPolarScaleRadDropDownList, scaleNames);
                    LoadRadDropDownListWithEntries(ref threeDeeScaleRadDropDownList, scaleNames);
                    LoadRadDropDownListWithEntries(ref phdScaleRadDropDownList, scaleNames);
                    if (this.multiChartScaleSelectedIndex >= scaleNames.Count)
                    {
                        this.matrixScaleRadDropDownList.SelectedIndex = 2;
                    }
                    else
                    {
                        this.matrixScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                        this.prpddPolarScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                        this.threeDeeScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                        this.phdScaleRadDropDownList.SelectedIndex = this.multiChartScaleSelectedIndex;
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FillAllScaleRadDropDownListEntries(List<string>)\nInput List<string> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FillAllScaleRadDropDownListEntries(List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Gets the voltage limit corresponding to the selected index from a scale RadDropDownList.
        /// </summary>
        /// <param name="selectedIndex"></param>
        /// <returns></returns>
        private double GetVoltageScaleForSelectedIndex(int selectedIndex)
        {
            double voltageLimit = 0.0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        voltageLimit = 10.0;
                        break;
                    /// selectedIndex = 1 refers to using the local maximum to do the scaling, and that
                    /// has to be taken care of by looking at the local matrices;
                    case 2:
                        voltageLimit = this.maxVoltageOverAllChannels;
                        break;
                    case 3:
                        voltageLimit = 0.01;
                        break;
                    case 4:
                        voltageLimit = 0.02;
                        break;
                    case 5:
                        voltageLimit = 0.05;
                        break;
                    case 6:
                        voltageLimit = 0.1;
                        break;
                    case 7:
                        voltageLimit = 0.2;
                        break;
                    case 8:
                        voltageLimit = 0.5;
                        break;
                    case 9:
                        voltageLimit = 1.0;
                        break;
                    case 10:
                        voltageLimit = 2.5;
                        break;
                    case 11:
                        voltageLimit = 5.0;
                        break;
                    case 12:
                        voltageLimit = 10.0;
                        break;
                    default:
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.GetVoltageScaleForSelectedIndex(int): unknown value for selectedIndex, value = " + selectedIndex.ToString();
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.GetVoltageScaleForSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return voltageLimit;
        }

        /// <summary>
        /// Fills the magnitudeSteps array with the voltages associated with the phase resolved data readings
        /// </summary>
        private void InitializeMagnitudeAndPhaseStepData()
        {
            try
            {
                this.magnitudeSteps = new double[32];
                this.phaseSteps = new double[48];

                /// I will just use the middle of the phase step to identify it
                double currentPhaseStep = 3.75;

                /// The matrix holding the pulse count data is laid out kind of backwards in memory,
                /// so I have preserved that ordering when saving the data and when accessing it
                /// later;
                this.magnitudeSteps[0] = 8.881235562;
                this.magnitudeSteps[1] = 6.894033459;
                this.magnitudeSteps[2] = 5.351473565;
                this.magnitudeSteps[3] = 4.154065902;
                this.magnitudeSteps[4] = 3.224581661;
                this.magnitudeSteps[5] = 2.503072203;
                this.magnitudeSteps[6] = 1.94300257;
                this.magnitudeSteps[7] = 1.508250134;
                this.magnitudeSteps[8] = 1.170774811;
                this.magnitudeSteps[9] = 0.908810566;
                this.magnitudeSteps[10] = 0.705461578;
                this.magnitudeSteps[11] = 0.547612513;
                this.magnitudeSteps[12] = 0.425082632;
                this.magnitudeSteps[13] = 0.329969165;
                this.magnitudeSteps[14] = 0.256137612;
                this.magnitudeSteps[15] = 0.198826082;
                this.magnitudeSteps[16] = 0.154338172;
                this.magnitudeSteps[17] = 0.11980456;
                this.magnitudeSteps[18] = 0.092997944;
                this.magnitudeSteps[19] = 0.072189385;
                this.magnitudeSteps[20] = 0.056036802;
                this.magnitudeSteps[21] = 0.043498406;
                this.magnitudeSteps[22] = 0.033765512;
                this.magnitudeSteps[23] = 0.026210381;
                this.magnitudeSteps[24] = 0.020345733;
                this.magnitudeSteps[25] = 0.015793316;
                this.magnitudeSteps[26] = 0.012259516;
                this.magnitudeSteps[27] = 0.009516414;
                this.magnitudeSteps[28] = 0.007387089;
                this.magnitudeSteps[29] = 0.005734206;
                this.magnitudeSteps[30] = 0.004451161;
                this.magnitudeSteps[31] = 0.003455201;

                for (int i = 0; i < 48; i++)
                {
                    this.phaseSteps[i] = currentPhaseStep;
                    currentPhaseStep += 7.5;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeMagnitudeAndPhaseStepData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the voltageRangeSpannedByRowEntry array which is used when graphing some of the phase resolved data
        /// </summary>
        private void ComputeDataDisplayIntervals()
        {
            try
            {
                this.voltageRangeSpannedByRowEntry = new double[32];

                double currentHighValue;
                double currentStep = 10.0 - this.magnitudeSteps[0];
                double epsilon = 1.0e-9;

                this.voltageRangeSpannedByRowEntry[0] = currentStep;

                for (int i = 1; i < 32; i++)
                {
                    currentHighValue = this.magnitudeSteps[i - 1] - currentStep - epsilon;
                    currentStep = currentHighValue - this.magnitudeSteps[i];
                    this.voltageRangeSpannedByRowEntry[i] = currentStep;
                }

                // MessageBox.Show("Last values seen were: currentStep = " + currentStep.ToString() + "  last value = " + magnitudeSteps[31].ToString());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ComputeDataDisplayIntervals()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Places the group box containing other objects on the lower portion of a multi-chart
        /// </summary>
        /// <param name="chartviewer"></param>
        /// <param name="groupBox"></param>
        private void PlaceGroupBoxOnLowerPartOfChart(WinChartViewer chartviewer, RadGroupBox groupBox)
        {
            try
            {
                int chartX;
                int chartY;
                int groupBoxY;
                if (chartviewer != null)
                {
                    if (groupBox != null)
                    {
                        chartX = chartviewer.Location.X;
                        chartY = chartviewer.Location.Y;
                        groupBoxY = chartY + chartviewer.Size.Height - groupBox.Height;
                        groupBox.Location = new Point(chartX, groupBoxY);
                        groupBox.Width = chartviewer.Size.Width;
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.PlaceGroupBoxOnLowerPartOfChart(WinChartViewer, RadGroupBox)\nInput RadGroupBox was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.PlaceGroupBoxOnLowerPartOfChart(WinChartViewer, RadGroupBox)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceGroupBoxOnLowerPartOfChart(WinChartViewer, RadGroupBox)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Places all the group boxes containing additional objects on the lower part of a PRPDD chart
        /// </summary>
        private void PlaceAllMatrixChartGroupBoxes()
        {
            try
            {
                for (int i = 0; i < this.activeChannelCount; i++)
                {
                    PlaceGroupBoxOnLowerPartOfChart(matrixChartWinChartViewerList[i], matrixChartRadGroupBoxList[i]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceAllMatrixChartGroupBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PlaceAllPRPDDPolarChartGroupBoxes()
        {
            try
            {
                    for (int i = 0; i < this.activeChannelCount; i++)
                    {
                        PlaceGroupBoxOnLowerPartOfChart(prpddPolarChartWinChartViewerList[i], prpddPolarChartRadGroupBoxList[i]);
                    }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceAllMatrixChartGroupBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Places all the group boxes containing additional objects on the lower part of a PRPDD chart
        /// </summary>
        private void PlaceAllThreeDeeChartGroupBoxes()
        {
            try
            {
                for (int i = 0; i < this.activeChannelCount; i++)
                {
                    PlaceGroupBoxOnLowerPartOfChart(threeDeeChartWinChartViewerList[i], threeDeeChartRadGroupBoxList[i]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceAllMatrixChartGroupBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Places all the group boxes containing additional objects on the lower part of a PHD chart
        /// </summary>
        private void PlaceAllPhdChartGroupBoxes()
        {
            try
            {
                for (int i = 0; i < this.activeChannelCount; i++)
                {
                    PlaceGroupBoxOnLowerPartOfChart(phdChartWinChartViewerList[i], phdChartRadGroupBoxList[i]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PlaceAllPhdChartGroupBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills a list with the strings indicating the phase offset choices available in the PRPDD graphs
        /// </summary>
        private void InitializePhaseOffsetRadDropDownListEntries()
        {
            try
            {
                this.phaseOffsetEntries = new List<string>();

                phaseOffsetEntries.Add("1 [0º]");
                phaseOffsetEntries.Add("2 [120º]");
                phaseOffsetEntries.Add("3 [240º]");
                phaseOffsetEntries.Add("1-2 [60º]");
                phaseOffsetEntries.Add("2-3 [180º]");
                phaseOffsetEntries.Add("3-1 [300º]");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePhaseOffsetRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Loads the phase offset entries into all the PhaseOffsetRadDropDownLists
        /// </summary>
        private void FillPhaseOffsetRadDropDownListEntries()
        {
            try
            {
                FIllMatrixPhaseOffsetRadDropDownListEntries();
                FIllPRPDDPolarPhaseOffsetRadDropDownListEntries();
                FIllThreeDeePhaseOffsetRadDropDownListEntries();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FillPhaseOffsetRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FIllMatrixPhaseOffsetRadDropDownListEntries()
        {
            try
            {
                LoadRadDropDownListWithEntries(ref matrixChart1PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart2PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart3PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart4PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart5PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart6PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart7PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart8PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart9PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart10PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart11PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart12PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart13PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart14PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref matrixChart15PhaseShiftRadDropDownList, this.phaseOffsetEntries);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FIllMatrixPhaseOffsetRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FIllPRPDDPolarPhaseOffsetRadDropDownListEntries()
        {
            try
            {
                LoadRadDropDownListWithEntries(ref prpddPolarChart1PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart2PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart3PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart4PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart5PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart6PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart7PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart8PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart9PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart10PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart11PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart12PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart13PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart14PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref prpddPolarChart15PhaseShiftRadDropDownList, this.phaseOffsetEntries);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FIllPRPDDPolarPhaseOffsetRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void FIllThreeDeePhaseOffsetRadDropDownListEntries()
        {
            try
            {
                LoadRadDropDownListWithEntries(ref threeDeeChart1PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart2PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart3PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart4PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart5PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart6PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart7PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart8PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart9PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart10PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart11PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart12PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart13PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart14PhaseShiftRadDropDownList, this.phaseOffsetEntries);
                LoadRadDropDownListWithEntries(ref threeDeeChart15PhaseShiftRadDropDownList, this.phaseOffsetEntries);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FIllthreeDeePhaseOffsetRadDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetPhaseShiftForAllChartsAndRedrawAffectedChart(int zeroIndexActiveChannelNumber, int selectedIndex)
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    if ((this.matrixChartPhaseShiftRadDropDownListList != null) &&
                       (this.prpddPolarChartPhaseShiftRadDropDownListList != null) &&
                       (this.threeDeeChartPhaseShiftRadDropDownListList != null) &&
                       (this.prpddChartPhaseShiftSelectedIndexList != null))
                    {
                        if ((this.matrixChartPhaseShiftRadDropDownListList.Count > zeroIndexActiveChannelNumber) &&
                           (this.prpddPolarChartPhaseShiftRadDropDownListList.Count > zeroIndexActiveChannelNumber) &&
                           (this.threeDeeChartPhaseShiftRadDropDownListList.Count > zeroIndexActiveChannelNumber) &&
                            (this.prpddChartPhaseShiftSelectedIndexList.Count > zeroIndexActiveChannelNumber))
                        {
                            this.prpddChartPhaseShiftSelectedIndexList[zeroIndexActiveChannelNumber] = selectedIndex;
                            this.matrixChartPhaseShiftRadDropDownListList[zeroIndexActiveChannelNumber].SelectedIndex = selectedIndex;
                            this.prpddPolarChartPhaseShiftRadDropDownListList[zeroIndexActiveChannelNumber].SelectedIndex = selectedIndex;
                            this.threeDeeChartPhaseShiftRadDropDownListList[zeroIndexActiveChannelNumber].SelectedIndex = selectedIndex;
                            if (MatrixAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied())
                            {
                                MatrixDrawChartForNewPhaseShiftValue(zeroIndexActiveChannelNumber, selectedIndex);
                            }
                            else
                            {
                                this.matrixChartsNeedUpdating = true;
                            }
                            if (PRPDDPolarAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied())
                            {
                                PRPDDPolarDrawChartForNewPhaseShiftValue(zeroIndexActiveChannelNumber, selectedIndex);
                            }
                            else
                            {
                                this.prpddPolarChartsNeedUpdating = true;
                            }
                            if (ThreeDeeAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied())
                            {
                                ThreeDeeDrawChartForNewPhaseShiftValue(zeroIndexActiveChannelNumber, selectedIndex);
                            }
                            else
                            {
                                this.threeDeeChartsNeedUpdating = true;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PhaseResolvedDataViewer.SetPhaseShiftForAllChartsAndRedrawAffectedChart(int, int)\nAt least one of the phaseShiftRadDropDownListList lists or the prpddChartPhaseShiftSelectedIndexList had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.SetPhaseShiftForAllCharts(int, int)\nAt least one of the phaseShiftRadDropDownListList list or the prpddChartPhaseShiftSelectedIndexList was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetPhaseShiftForAllCharts(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

        }

        private void PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(int zeroIndexActiveChannelNumber, int selectedIndex)
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    if ((this.prpddChartPhaseShiftSelectedIndexList != null) && (this.prpddChartPhaseShiftSelectedIndexList.Count > zeroIndexActiveChannelNumber))
                    {
                        if ((selectedIndex > -1) && (selectedIndex != this.prpddChartPhaseShiftSelectedIndexList[zeroIndexActiveChannelNumber]))
                        {
                            SetPhaseShiftForAllChartsAndRedrawAffectedChart(zeroIndexActiveChannelNumber, selectedIndex);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(int, int)\nthis.prpddChartPhaseShiftSelectedIndexList has not been properly initialized.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpdddPhaseShiftRadDropDownListSelectedIndexChangedHandler(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void MatrixDrawChartForNewPhaseShiftValue(int zeroIndexActiveChannelNumber, int selectedIndex)
        {
            try
            {
                if (this.activeChannelNumberList != null)
                {
                    if (this.activeChannelNumberList.Count > 0)
                    {
                        int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexActiveChannelNumber];
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(selectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.matrixScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        DrawPRPDDChart(this.matrixChartWinChartViewerList[zeroIndexActiveChannelNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                               this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.MatrixDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.MatrixDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixDrawChartForNewPhaseShiftValue(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MatrixAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()
        {
            bool conditionsAreSatisfied = false;
            try
            {
                //conditionsAreSatisfied = ((matrixAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                //        (this.dataRadPageView.SelectedPage == this.matrixRadPageViewPage) && this.hasFinishedInitialization);
                conditionsAreSatisfied = this.dataRadPageView.SelectedPage == this.matrixRadPageViewPage;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return conditionsAreSatisfied;
        }


        private void matrixChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart1PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(0, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart2PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(1, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart3PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(2, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart4PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(3, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart5PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(4, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart6PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(5, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart7PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(6, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart8PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(7, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart9PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart9PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(8, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart9PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart10PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(9, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart11PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(10, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart12PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(11, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart13PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(12, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart14PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(13, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = matrixChart15PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(14, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarDrawChartForNewPhaseShiftValue(int zeroIndexActiveChannelNumber, int selectedIndex)
        {
            try
            {
                if (this.activeChannelNumberList != null)
                {
                    if (this.activeChannelNumberList.Count > 0)
                    {
                        int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexActiveChannelNumber];
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(selectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.prpddPolarScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        DrawPRPDDPolarChart(this.prpddPolarChartWinChartViewerList[zeroIndexActiveChannelNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                               this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.PRPDDPolarDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.PRPDDPolarDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixDrawChartForNewPhaseShiftValue(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool PRPDDPolarAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()
        {
            bool conditionsAreSatisfied = false;
            try
            {
                //conditionsAreSatisfied = ((prpddPolarAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                //        (this.dataRadPageView.SelectedPage == this.prpddPolarRadPageViewPage) && this.hasFinishedInitialization);
                conditionsAreSatisfied = this.dataRadPageView.SelectedPage == this.prpddPolarRadPageViewPage;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return conditionsAreSatisfied;
        }

        private void prpddPolarChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart1PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(0, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart2PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(1, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart3PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(2, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart4PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(3, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart5PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(4, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart6PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(5, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart7PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(6, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart8PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(7, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart9PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart9PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(8, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart9PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart10PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(9, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart11PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(10, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart12PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(11, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart13PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(12, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart14PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(13, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = prpddPolarChart15PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(14, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ThreeDeeDrawChartForNewPhaseShiftValue(int zeroIndexActiveChannelNumber, int selectedIndex)
        {
            try
            {
                if (this.activeChannelNumberList != null)
                {
                    if (this.activeChannelNumberList.Count > 0)
                    {
                        int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexActiveChannelNumber];
                        double phaseShift = this.initialPhaseResolvedDataPhaseShift + ComputePhaseShiftFromSelectedIndex(selectedIndex);
                        double yScaleMax = ComputeMatrixYscaleSettingFromSelectedIndex(this.threeDeeScaleRadDropDownList.SelectedIndex, zeroIndexedChannelNumber);
                        DrawThreeDeeChart(this.threeDeeChartWinChartViewerList[zeroIndexActiveChannelNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                               this.channelMinusMatrixDataList[zeroIndexedChannelNumber], phaseShift, yScaleMax, zeroIndexedChannelNumber + 1);
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.PRPDDPolarDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.PRPDDPolarDrawChartForNewPhaseShiftValue(int, int)\nthis.activeChannelNumberList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixDrawChartForNewPhaseShiftValue(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ThreeDeeAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()
        {
            bool conditionsAreSatisfied = false;
            try
            {
                //conditionsAreSatisfied = ((threeDeeAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                //        (this.dataRadPageView.SelectedPage == this.threeDeeRadPageViewPage) && this.hasFinishedInitialization);
                conditionsAreSatisfied = this.dataRadPageView.SelectedPage == this.threeDeeRadPageViewPage;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ThreeDeeAutoDrawChartForNewPhaseShiftValueConditionsAreSatisfied()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return conditionsAreSatisfied;
        }

        private void threeDeeChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart1PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(0, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart1PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart2PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(1, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart2PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart3PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(2, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart3PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart4PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(3, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart4PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart5PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(4, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart5PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart6PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(5, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart6PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart7PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(6, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart7PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart8PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(7, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart9PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart9PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(8, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart8PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart10PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(9, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart10PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart11PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(10, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart11PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart12PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(11, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart12PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart13PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(12, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart13PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart14PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(13, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart14PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = threeDeeChart15PhaseShiftRadDropDownList.SelectedIndex;
                PRPDDChartPhaseShiftRadDropDownListSelectedIndexChangedHandler(14, selectedIndex);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart15PhaseShiftRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// Returns the amount of phase shift associated with the selected index of a PhaseOffsetRadDropDownList object
        /// </summary>
        /// <param name="selectedIndex"></param>
        /// <returns></returns>
        private double ComputePhaseShiftFromSelectedIndex(int selectedIndex)
        {
            double phaseShift = 0.0;
            try
            {
                switch (selectedIndex)
                {
                    case -1:
                        break;
                    case 0:
                        break;
                    case 1:
                        phaseShift = 120;
                        break;
                    case 2:
                        phaseShift = 240;
                        break;
                    case 3:
                        phaseShift = 60;
                        break;
                    case 4:
                        phaseShift = 180;
                        break;
                    case 5:
                        phaseShift = 300;
                        break;
                    default:
                        string errorMessage = "Error in PhaseResolvedDataViewer.ComputePhaseShiftFromSelectedIndex(int): selectedIndex = " + selectedIndex.ToString() + " is not a recognized value";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ComputePhaseShiftFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseShift;
        }

        /// <summary>
        /// Returns the voltage scale value associated with the index into the ScaleRadDropDownList.  It also gets the proper value
        /// when the "local minimum" option was selected.
        /// </summary>
        /// <param name="selectedIndex"></param>
        /// <param name="zeroOffsetMatrixDatasetNumber"></param>
        /// <returns></returns>
        private double ComputeMatrixYscaleSettingFromSelectedIndex(int selectedIndex, int zeroOffsetMatrixDatasetNumber)
        {
            double yScaleSetting = 0.0;
            try
            {
                switch (selectedIndex)
                {
                    case -1:
                        break;
                    case 0:
                        yScaleSetting = 10.0;
                        break;
                    case 1:
                        yScaleSetting = this.channelMaxMatrixVoltagesList[zeroOffsetMatrixDatasetNumber];
                        yScaleSetting = AdjustYscaleSetting(yScaleSetting);
                        break;
                    case 2:
                        yScaleSetting = this.maxVoltageOverAllChannels;
                        yScaleSetting = AdjustYscaleSetting(yScaleSetting);
                        break;
                    case 3:
                        yScaleSetting = 0.01;
                        break;
                    case 4:
                        yScaleSetting = 0.02;
                        break;
                    case 5:
                        yScaleSetting = 0.05;
                        break;
                    case 6:
                        yScaleSetting = 0.1;
                        break;
                    case 7:
                        yScaleSetting = 0.2;
                        break;
                    case 8:
                        yScaleSetting = 0.5;
                        break;
                    case 9:
                        yScaleSetting = 1.0;
                        break;
                    case 10:
                        yScaleSetting = 2.0;
                        break;
                    case 11:
                        yScaleSetting = 5.0;
                        break;
                    case 12:
                        yScaleSetting = 10.0;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.ComputeMatrixYscaleSettingFromSelectedIndex(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return yScaleSetting;
        }

        private double AdjustYscaleSetting(double yScaleSetting)
        {
            double adjustedValue = yScaleSetting;
            try
            {
                if (this.magnitudeSteps != null)
                {
                    if (this.magnitudeSteps.Length == 32)
                    {
                        if (Math.Abs(yScaleSetting - this.magnitudeSteps[0]) < 1.0e-7)
                        {
                            adjustedValue = 10.0;
                        }
                        else
                        {
                            for (int i = 1; i < 32; i++)
                            {
                                if (Math.Abs(yScaleSetting - this.magnitudeSteps[i]) < 1.0e-7)
                                {
                                    adjustedValue = this.magnitudeSteps[i - 1];
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PhaseResolvedDataViewer.AdjustYscaleSetting(double)\nthis.magnitudeSteps has an incorrect number of elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.AdjustYscaleSetting(double)\nthis.magnitudeSteps was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.AdjustYscaleSetting(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return adjustedValue;
        }

        /// <summary>
        /// Fills the maxVoltageOverAllChannels array over all data reading dates
        /// </summary>
        private void FillGlobalMaximumMatrixVoltageValueOverAllChannels()
        {
            try
            {
                    this.maxVoltageOverAllChannels = FindTheMaximumVoltageOverAllMatriciesRecordedOnOneDate();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FillGlobalMaximumMatrixVoltageValuesOverAllChannels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Finds the maximum voltage recorded for all channels for one given data reading date.
        /// </summary>
        /// <param name="dateIndex"></param>
        /// <returns></returns>
        private double FindTheMaximumVoltageOverAllMatriciesRecordedOnOneDate()
        {
            double maxVoltage = 0.0;
            try
            {
                double currentVoltage;

                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i])
                    {
                        currentVoltage = this.channelMaxMatrixVoltagesList[i];
                        if (currentVoltage > maxVoltage)
                        {
                            maxVoltage = currentVoltage;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindTheMaximumVoltageOverAllMatriciesRecordedOnOneDate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return maxVoltage;
        }

        /// <summary>
        /// Finds the smallest row entry (which corresponds to the largest voltage entry) for a set of phase resolved data
        /// </summary>
        /// <param name="channelPlusMatrixData"></param>
        /// <param name="channelMinusMatrixData"></param>
        /// <param name="dateIndex"></param>
        /// <returns></returns>
        private int FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(MatrixData channelPlusMatrixData, MatrixData channelMinusMatrixData)
        {
            int smallestRowIndex = 100;
            try
            {
                int currentRowIndex;
                if (channelPlusMatrixData != null)
                {
                    currentRowIndex = channelPlusMatrixData.smallestNumberedRowThatContainsAnEntry;
                    if ((currentRowIndex > 0) && (currentRowIndex < smallestRowIndex))
                    {
                        smallestRowIndex = currentRowIndex;
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(MatrixData, MatrixData, int)\nFirst MatrixData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (channelMinusMatrixData != null)
                {
                    currentRowIndex = channelMinusMatrixData.smallestNumberedRowThatContainsAnEntry;
                    if ((currentRowIndex > 0) && (currentRowIndex < smallestRowIndex))
                    {
                        smallestRowIndex = currentRowIndex;
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(MatrixData, MatrixData, int)\nSecond MatrixData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (smallestRowIndex == 100)
                {
                    smallestRowIndex = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(MatrixData, MatrixData, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return smallestRowIndex;
        }

        /// <summary>
        /// Goes through all matrix (phase resolved data) entries for all channels, and for each channel and each pair of matrices, finds the largest voltage seen in the data
        /// and fills the value in a global array.
        /// </summary>
        private void FillTheLocalMaximumVoltageEntriesForAllChannels()
        {
            try
            {
                int smallestRowNumber;
                double largestVoltageSeen;

                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i])
                    {
                        smallestRowNumber = FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(channelPlusMatrixDataList[i], channelMinusMatrixDataList[i]);
                        if (smallestRowNumber > 0)
                        {
                            largestVoltageSeen = this.magnitudeSteps[smallestRowNumber - 1];
                        }
                        else
                        {
                            largestVoltageSeen = 0.0;
                        }
                        this.channelMaxMatrixVoltagesList[i] = largestVoltageSeen;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FindTheSmallestMatrixRowEntryForOneChannelAndOneDate(MatrixData, MatrixData, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the channelMatrixDataIsNotEmpty dictionary with the values indicating whether there is phase resolved data for a given channel on a given reading date.  The assumption
        /// is that the data might be not present all the time, or the monitor might be re-configured to start recording more channels worth of data at some point.
        /// </summary>
        private void FillTheChannelMatrixDataIsNotEmptyListWithValues()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    if (this.channelIsActive[i])
                    {
                        if ((this.channelPlusMatrixDataList[i].matrixEntries.Count > 0) ||
                            (this.channelMinusMatrixDataList[i].matrixEntries.Count > 0))
                        {
                            this.channelMatrixDataIsNotEmpty[i] = true;
                        }
                        else
                        {
                            this.channelMatrixDataIsNotEmpty[i] = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FillTheChannelMatrixDataIsNotEmptyListWithValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// This is a one-time-use method that creates an entry for every channel (if applicable)
        /// for the various lists used to contain the input data
        /// </summary>
        private void InitializeChannelLists()
        {
            try
            {
                this.channelMaxMatrixVoltagesList = new List<double>();
                this.channelDataList = new List<ChannelData>();
                this.channelPlusMatrixDataList = new List<MatrixData>();
                this.channelMinusMatrixDataList = new List<MatrixData>();
                this.channelMatrixDataIsNotEmpty = new List<bool>();
                this.activeChannelNumberList = new List<int>();
                /// we have to initialize the lists that contain arrays after we have
                /// loaded the data and know how many matrices there are in total.
                for (int i = 0; i < 15; i++)
                {
                    this.channelDataList.Add(new ChannelData());
                    this.channelPlusMatrixDataList.Add(new MatrixData());
                    this.channelMinusMatrixDataList.Add(new MatrixData());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeChannelLists()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fill all lists that contain VS created objects.
        /// </summary>
        private void InitializeListsOfVisualStudioCreatedObjects()
        {
            try
            {
                InitializePRPDDChartPhaseShiftSelectedIndexList();
                InitializeMatrixChartRadGroupBoxList();
                InitializeMatrixChartWinChartViewerList();
                MatrixSetupZoomMode();
                InitializematrixChartPhaseShiftRadDropDownListList();
                InitializematrixChartPDIValueRadLabelList();
                InitializePRPDDPolarChartRadGroupBoxList();
                InitializePRPDDPolarChartWinChartViewerList();
                PRPDDPolarSetupZoomMode();
                InitializePRPDDPolarChartPhaseShiftRadDropDownListList();
                InitializePRPDDPolarChartPDIValueRadLabelList();
                InitializeThreeDeeChartWinChartViewerList();
                InitializeThreeDeeChartRadGroupBoxList();
                InitializeThreeDeeChartPhaseShiftRadDropDownListList();
                InitializeThreeDeeChartPDIValueRadLabelList();
                InitializePhdChartWinChartViewerList();
                InitializePhdChartRadGroupBoxList();
                InitializePhdChartRadLabelList();
                PhdSetupZoomMode();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeListsOfVisualStudioCreatedObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// This is a band-aid attempt to make the drop-down lists work properly, rather than requiring the user to select them once first
        /// before actually being able to select an item from the list.
        /// </summary>
        private void SelectAllRadDropDownListObjects()
        {
            try
            {
                int count;
                if (this.matrixChartPhaseShiftRadDropDownListList != null)
                {
                    count = this.matrixChartPhaseShiftRadDropDownListList.Count;
                    for (int i = 0; i < count; i++)
                    {
                        if (this.matrixChartPhaseShiftRadDropDownListList[i] != null)
                        {
                            this.matrixChartPhaseShiftRadDropDownListList[i].SelectedIndex = 1;
                            this.matrixChartPhaseShiftRadDropDownListList[i].SelectedIndex = 0;
                        }
                    }
                }
                this.matrixScaleRadDropDownList.Select();
                this.phdScaleRadDropDownList.Select();
                this.threeDeeScaleRadDropDownList.Select();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SelectAllRadDropDownListObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializePRPDDChartPhaseShiftSelectedIndexList()
        {
            try
            {
                this.prpddChartPhaseShiftSelectedIndexList = new List<int>();
                for (int i = 0; i < 15; i++)
                {
                    this.prpddChartPhaseShiftSelectedIndexList.Add(-1);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePRPDDChartPhaseShiftSelectedIndexList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the matrixChartRadGroupBoxList with the matrixChartXRadGroupBox objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeMatrixChartRadGroupBoxList()
        {
            try
            {
                this.matrixChartRadGroupBoxList = new List<RadGroupBox>();
                this.matrixChartRadGroupBoxList.Add(matrixChart1RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart2RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart3RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart4RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart5RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart6RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart7RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart8RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart9RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart10RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart11RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart12RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart13RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart14RadGroupBox);
                this.matrixChartRadGroupBoxList.Add(matrixChart15RadGroupBox);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeMatrixChartRadGroupBoxList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the matrixChartWinChartViewerList with the matrixChartXWinChartViewer objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeMatrixChartWinChartViewerList()
        {
            try
            {
                this.matrixChartWinChartViewerList = new List<WinChartViewer>();
                this.matrixChartWinChartViewerList.Add(matrixChart1WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart2WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart3WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart4WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart5WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart6WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart7WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart8WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart9WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart10WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart11WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart12WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart13WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart14WinChartViewer);
                this.matrixChartWinChartViewerList.Add(matrixChart15WinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeMatrixChartWinChartViewerList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the matrixChartPhaseShiftRadDropDownListList with the matrixChartXRadDropDownList objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializematrixChartPhaseShiftRadDropDownListList()
        {
            try
            {
                this.matrixChartPhaseShiftRadDropDownListList = new List<RadDropDownList>();
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart1PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart2PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart3PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart4PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart5PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart6PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart7PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart8PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart9PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart10PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart11PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart12PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart13PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart14PhaseShiftRadDropDownList);
                this.matrixChartPhaseShiftRadDropDownListList.Add(matrixChart15PhaseShiftRadDropDownList);
                for (int i = 0; i < this.matrixChartPhaseShiftRadDropDownListList.Count; i++)
                {
                    this.matrixChartPhaseShiftRadDropDownListList[i].SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializematrixChartPhaseShiftRadDropDownListList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the matrixChartPDIValueRadLabelList with the matrixChartXPDIValueRadLabel objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializematrixChartPDIValueRadLabelList()
        {
            try
            {
                this.matrixChartPDIValueRadLabelList = new List<RadLabel>();
                this.matrixChartPDIValueRadLabelList.Add(matrixChart1PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart2PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart3PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart4PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart5PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart6PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart7PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart8PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart9PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart10PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart11PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart12PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart13PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart14PDIValueRadLabel);
                this.matrixChartPDIValueRadLabelList.Add(matrixChart15PDIValueRadLabel);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializematrixChartPDIValueRadLabelList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// Fills the prpddPolarChartRadGroupBoxList with the matrixChartXRadGroupBox objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePRPDDPolarChartRadGroupBoxList()
        {
            try
            {
                this.prpddPolarChartRadGroupBoxList = new List<RadGroupBox>();
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart1RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart2RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart3RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart4RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart5RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart6RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart7RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart8RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart9RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart10RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart11RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart12RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart13RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart14RadGroupBox);
                this.prpddPolarChartRadGroupBoxList.Add(prpddPolarChart15RadGroupBox);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeprpddPolarChartRadGroupBoxList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        /// <summary>
        /// Fills the prpddPolarChartWinChartViewerList with the prpddPolarChartXWinChartViewer objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePRPDDPolarChartWinChartViewerList()
        {
            try
            {
                this.prpddPolarChartWinChartViewerList = new List<WinChartViewer>();
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart1WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart2WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart3WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart4WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart5WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart6WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart7WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart8WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart9WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart10WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart11WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart12WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart13WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart14WinChartViewer);
                this.prpddPolarChartWinChartViewerList.Add(prpddPolarChart15WinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePRPDDPolarChartWinChartViewerList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the prpddPolarChartPhaseShiftRadDropDownListList with the prpddPolarChartXRadDropDownList objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePRPDDPolarChartPhaseShiftRadDropDownListList()
        {
            try
            {
                this.prpddPolarChartPhaseShiftRadDropDownListList = new List<RadDropDownList>();
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart1PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart2PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart3PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart4PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart5PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart6PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart7PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart8PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart9PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart10PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart11PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart12PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart13PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart14PhaseShiftRadDropDownList);
                this.prpddPolarChartPhaseShiftRadDropDownListList.Add(prpddPolarChart15PhaseShiftRadDropDownList);
                for (int i = 0; i < this.prpddPolarChartPhaseShiftRadDropDownListList.Count; i++)
                {
                    this.prpddPolarChartPhaseShiftRadDropDownListList[i].SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePRPDDPolarChartPhaseShiftRadDropDownListList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the prpddPolarChartPDIValueRadLabelList with the prpddPolarChartXPDIValueRadLabel objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePRPDDPolarChartPDIValueRadLabelList()
        {
            try
            {
                this.prpddPolarChartPDIValueRadLabelList = new List<RadLabel>();
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart1PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart2PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart3PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart4PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart5PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart6PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart7PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart8PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart9PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart10PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart11PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart12PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart13PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart14PDIValueRadLabel);
                this.prpddPolarChartPDIValueRadLabelList.Add(prpddPolarChart15PDIValueRadLabel);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePRPDDPolarChartPDIValueRadLabelList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the threeDeeChartWinChartViewerList with the threeDeeChartXWinChartViewer objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeThreeDeeChartWinChartViewerList()
        {
            try
            {
                this.threeDeeChartWinChartViewerList = new List<WinChartViewer>();
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart1WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart2WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart3WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart4WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart5WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart6WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart7WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart8WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart9WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart10WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart11WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart12WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart13WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart14WinChartViewer);
                this.threeDeeChartWinChartViewerList.Add(threeDeeChart15WinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeThreeDeeChartWinChartViewerList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the threeDeeChartRadGroupBoxList with the matrixChartXRadGroupBox objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeThreeDeeChartRadGroupBoxList()
        {
            try
            {
                this.threeDeeChartRadGroupBoxList = new List<RadGroupBox>();
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart1RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart2RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart3RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart4RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart5RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart6RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart7RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart8RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart9RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart10RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart11RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart12RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart13RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart14RadGroupBox);
                this.threeDeeChartRadGroupBoxList.Add(threeDeeChart15RadGroupBox);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializethreeDeeChartRadGroupBoxList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the threeDeeChartPhaseShiftRadDropDownListList with the prpddPolarChartXRadDropDownList objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeThreeDeeChartPhaseShiftRadDropDownListList()
        {
            try
            {
                this.threeDeeChartPhaseShiftRadDropDownListList = new List<RadDropDownList>();
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart1PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart2PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart3PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart4PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart5PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart6PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart7PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart8PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart9PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart10PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart11PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart12PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart13PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart14PhaseShiftRadDropDownList);
                this.threeDeeChartPhaseShiftRadDropDownListList.Add(threeDeeChart15PhaseShiftRadDropDownList);
                for (int i = 0; i < this.threeDeeChartPhaseShiftRadDropDownListList.Count; i++)
                {
                    this.threeDeeChartPhaseShiftRadDropDownListList[i].SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializethreeDeeChartPhaseShiftRadDropDownListList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the prpddPolarChartPDIValueRadLabelList with the prpddPolarChartXPDIValueRadLabel objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializeThreeDeeChartPDIValueRadLabelList()
        {
            try
            {
                this.threeDeeChartPDIValueRadLabelList = new List<RadLabel>();
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart1PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart2PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart3PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart4PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart5PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart6PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart7PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart8PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart9PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart10PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart11PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart12PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart13PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart14PDIValueRadLabel);
                this.threeDeeChartPDIValueRadLabelList.Add(threeDeeChart15PDIValueRadLabel);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializethreeDeeChartPDIValueRadLabelList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the phdChartWinChartViewerList with the phdChartXWinChartViewer objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePhdChartWinChartViewerList()
        {
            try
            {
                this.phdChartWinChartViewerList = new List<WinChartViewer>();
                this.phdChartWinChartViewerList.Add(phdChart1WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart2WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart3WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart4WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart5WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart6WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart7WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart8WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart9WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart10WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart11WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart12WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart13WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart14WinChartViewer);
                this.phdChartWinChartViewerList.Add(phdChart15WinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePhdChartWinChartViewerList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the phdChartRadLabelList with the phdChartXPDIValueRadLabel objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePhdChartRadLabelList()
        {
            try
            {
                this.phdChartRadLabelList = new List<RadLabel>();
                this.phdChartRadLabelList.Add(phdChart1PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart2PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart3PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart4PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart5PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart6PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart7PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart8PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart9PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart10PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart11PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart12PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart13PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart14PDIValueRadLabel);
                this.phdChartRadLabelList.Add(phdChart15PDIValueRadLabel);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePhdChartRadLabelList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the phdChartRadGroupBoxList with the phdChartXRadGroupBox objects, where X is a value from 1 to 15
        /// </summary>
        private void InitializePhdChartRadGroupBoxList()
        {
            try
            {
                this.phdChartRadGroupBoxList = new List<RadGroupBox>();
                this.phdChartRadGroupBoxList.Add(phdChart1RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart2RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart3RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart4RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart5RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart6RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart7RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart8RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart9RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart10RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart11RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart12RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart13RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart14RadGroupBox);
                this.phdChartRadGroupBoxList.Add(phdChart15RadGroupBox);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializePhdChartRadGroupBoxList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart1WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart1WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart2WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart2WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart3WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart3WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart4WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart4WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart5WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart5WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart6WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 6);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart6WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart7WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 7);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart7WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart8WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 8);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart8WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart9WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 9);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart9WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart10WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 10);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart10WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart11WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 11);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart11WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart12WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 12);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart12WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart13WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 13);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart13WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart14WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 14);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart14WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart15WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                MatrixViewportChangedHandler(sender, e, 15);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart15WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        //private void updateMatrixImageMap(WinChartViewer viewer)
        //{
        //    // Include tool tip for the chart
        //    if (viewer.ImageMap == null)
        //    {
        //        viewer.ImageMap = viewer.Chart.getHTMLImageMap("clickable", "",
        //           "title='Phase (deg) = {x} , Amplitude (V) = {value}, PulseCount = {field0}'");
        //    }
        //}

        private void MatrixViewportChangedHandler(object sender, WinViewPortEventArgs e, int chartNumber)
        {
            try
            {
                int zeroIndexedChartNumber = chartNumber - 1;
                int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexedChartNumber];

                // Update chart and image map if necessary
                // The image map is updated at the end of the chart update, and requires data declared
                // inside the DrawPRPDDChart method to create the image map
                if ((e.NeedUpdateChart) || (e.NeedUpdateImageMap))
                {
                    SetupAndDrawOnePRPDDGraph(zeroIndexedChartNumber, zeroIndexedChannelNumber);
                }
                //if (e.NeedUpdateImageMap)
                //{
                //    updateMatrixImageMap(chartViewer);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixViewportChangedHandler(object, WinViewPortEventArgs, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the zoom mode and scroll mode for all the PRPDD charts to horizontal and vertical zooming and scrolling
        /// </summary>
        private void MatrixSetupZoomMode()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    this.matrixChartWinChartViewerList[i].ZoomDirection = WinChartDirection.HorizontalVertical;
                    this.matrixChartWinChartViewerList[i].ScrollDirection = WinChartDirection.HorizontalVertical;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixSetupZoomMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        /// <summary>
        /// Sets the mouse mode to pointer mode for all displayed PRPDD charts
        /// </summary>
        private void MatrixSetPointerMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.matrixChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixSetPointerMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        /// <summary>
        /// Sets the mouse mode to zoom in mode for all displayed PRPDD charts
        /// </summary>
        private void MatrixSetZoomInMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.matrixChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixSetZoomInMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// Sets the mouse mode to zoom out mode for all displayed PRPDD charts
        /// </summary>
        private void MatrixSetZoomOutMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.matrixChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.MatrixSetZoomOutMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// One-time-use method that fills the arrays used to graph sine waves on the PRPDD charts.
        /// </summary>
        private void FillSineWaveDataArrays()
        {
            try
            {
                this.sineWaveAmplitudeData = new double[360];
                this.sineWaveDegreeData = new double[360];
                for (int x = 1; x < 360; x++)
                {
                    this.sineWaveDegreeData[x] = (double)(x);
                    this.sineWaveAmplitudeData[x] = Math.Sin(x * Math.PI / 180);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.FillSineWaveDataArrays()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Puts a sine wave in a PRPDD chart with the specified phase shift and color
        /// </summary>
        /// <param name="xyChart"></param>
        /// <param name="amplitude"></param>
        /// <param name="phaseShift"></param>
        /// <param name="viewPortStartDegrees"></param>
        /// <param name="viewPortEndDegrees"></param>
        /// <param name="color"></param>
        private void AddSineWaveToPRPDDChart(ref XYChart xyChart, double amplitude, double phaseShift, double viewPortStartDegrees, double viewPortEndDegrees, int color)
        {
            try
            {
                if (xyChart != null)
                {
                    double[] data;
                    double[] phases = ShiftPhaseData(this.sineWaveDegreeData, phaseShift);
                    ArrayMath adjustedData = new ArrayMath(this.sineWaveAmplitudeData);
                    adjustedData.mul(amplitude);
                    data = adjustedData.result();
                    if (Math.Abs(phaseShift) > 1.0e-7)
                    {
                        /// this appears irrelevant now, but it's been months since i wrote it, so who knows?  I think you can
                        /// just graph the sine waves without shifting them, and this code just shifts it back again.  DL  8-14-2011
                        ArrayMath phaseAdjuster = new ArrayMath(phases);
                        int minimumPhaseIndex = phaseAdjuster.minIndex();
                        phases = RotateArrayDataLeftward(phases, minimumPhaseIndex);
                        data = RotateArrayDataLeftward(data, minimumPhaseIndex);
                    }

                    //// Get the starting index of the array using the start date
                    //int startIndex = Array.BinarySearch(phases, viewPortStartDegrees);
                    //if (startIndex < 0)
                    //    startIndex = (~startIndex) - 1;

                    //// Get the ending index of the array using the end date
                    //int endIndex = Array.BinarySearch(phases, viewPortEndDegrees);
                    //if (endIndex < 0)
                    //    endIndex = ((~endIndex) < phases.Length) ? ~endIndex : phases.Length - 1;

                    //// Get the length
                    //int totalPointsBeingGraphed = endIndex - startIndex + 1;

                    //double[] phasesUsed = new double[totalPointsBeingGraphed];
                    //double[] dataUsed = new double[totalPointsBeingGraphed];

                    //Array.Copy(phases, startIndex, phasesUsed, 0, totalPointsBeingGraphed);
                    //Array.Copy(data, startIndex, dataUsed, 0, totalPointsBeingGraphed); 

                    LineLayer lineLayer = xyChart.addLineLayer(data, color);
                    lineLayer.setLineWidth(1);
                    lineLayer.setXData(phases);
                    lineLayer.setHTMLImageMap("{disable}");
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.AddSineWaveToPRPDDChart(ref XYChart, double, double, double, double)\nInput XYChart was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.AddSineWaveToPRPDDChart(ref XYChart, double, double, double, double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Shifts the data in an array a specified amount towards the 0-indexed end of the array, rotating the values that are pushed out back into the high end of the array
        /// </summary>
        /// <param name="data"></param>
        /// <param name="rotateLeftAmount"></param>
        /// <returns></returns>
        private double[] RotateArrayDataLeftward(double[] data, int rotateLeftAmount)
        {
            double[] returnValues = null;
            try
            {
                if (data != null)
                {
                    int dataLength = data.Length;
                    int returnValuesIndex = 0;

                    returnValues = new double[dataLength];

                    for (int i = rotateLeftAmount; i < dataLength; i++)
                    {
                        returnValues[returnValuesIndex] = data[i];
                        returnValuesIndex++;
                    }
                    for (int i = 0; i < rotateLeftAmount; i++)
                    {
                        returnValues[returnValuesIndex] = data[i];
                        returnValuesIndex++;
                    }
                }
                else
                {
                    string errorMessage = "Error in PhaseResolvedDataViewer.RotateArrayDataLeftward(double[], int)\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.RotateArrayDataLeftward(double[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnValues;
        }

        #region PRPDD Polar graph handlers

        private void prpddPolarChart1WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart1WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart2WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart2WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart3WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart3WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart4WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart4WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart5WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart5WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart6WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 6);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart6WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart7WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 7);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart7WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart8WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 8);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart8WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart9WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 9);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart9WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart10WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 10);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart10WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart11WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 11);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart11WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart12WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 12);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart12WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart13WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 13);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart13WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart14WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 14);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart14WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart15WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PRPDDPolarViewportChangedHandler(sender, e, 15);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart15WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarViewportChangedHandler(object sender, WinViewPortEventArgs e, int chartNumber)
        {
            try
            {
                int zeroIndexedChartNumber = chartNumber - 1;
                int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexedChartNumber];

                // Update chart and image map if necessary
                // The image map is updated at the end of the chart update, and requires data declared
                // inside the DrawPRPDDChart method to create the image map
                if ((e.NeedUpdateChart) || (e.NeedUpdateImageMap))
                {
                    SetupAndDrawOnePRPDDPolarGraph(zeroIndexedChartNumber, zeroIndexedChannelNumber);
                }
                //if (e.NeedUpdateImageMap)
                //{
                //    updateMatrixImageMap(chartViewer);
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarViewportChangedHandler(object, WinViewPortEventArgs, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarSetupZoomMode()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    this.prpddPolarChartWinChartViewerList[i].ZoomDirection = WinChartDirection.HorizontalVertical;
                    this.prpddPolarChartWinChartViewerList[i].ScrollDirection = WinChartDirection.HorizontalVertical;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarSetupZoomMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarSetPointerMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.prpddPolarChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarSetPointerMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarSetZoomInMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.prpddPolarChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarSetZoomInMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PRPDDPolarSetZoomOutMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.prpddPolarChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PRPDDPolarSetZoomOutMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        private void phdChart1WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart1WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart2WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart2WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart3WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 3);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart3WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart4WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 4);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart4WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart5WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 5);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart4WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart6WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 6);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart6WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart7WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 7);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart7WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart8WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 8);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart8WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart9WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 9);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart9WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart10WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 10);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart10WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart11WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 11);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart11WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart12WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 12);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart12WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart13WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 13);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart13WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart14WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 14);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart14WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart15WinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                PhdViewportChangedHandler(sender, e, 15);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart15WinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PhdViewportChangedHandler(object sender, WinViewPortEventArgs e, int chartNumber)
        {
            try
            {
                bool voltageMaximumWasComputed = false;
                int zeroIndexedChartNumber = chartNumber - 1;
                int zeroIndexedChannelNumber = this.activeChannelNumberList[zeroIndexedChartNumber];
                int selectedIndex = this.phdScaleRadDropDownList.SelectedIndex;
                double maxVoltage = ComputeMatrixYscaleSettingFromSelectedIndex(selectedIndex, zeroIndexedChannelNumber);

                if ((selectedIndex == 1) || (selectedIndex == 2))
                {
                    voltageMaximumWasComputed = true;
                }
                // Update chart and image map if necessary
                // The image map is updated at the end of the chart update, and requires data declared
                // inside the DrawPhdChart method to create the image map.  Therefore, we cannot do it here.
                if (e.NeedUpdateChart)
                {
                    DrawPHDChart(this.phdChartWinChartViewerList[zeroIndexedChartNumber], this.channelPlusMatrixDataList[zeroIndexedChannelNumber],
                                   this.channelMinusMatrixDataList[zeroIndexedChannelNumber], maxVoltage, voltageMaximumWasComputed, zeroIndexedChannelNumber + 1);
                }
                if (e.NeedUpdateImageMap)
                {
                    UpdatePhdImageMap(this.phdChartWinChartViewerList[zeroIndexedChartNumber]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhdViewportChangedHandler(object, WinViewPortEventArgs, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdatePhdImageMap(WinChartViewer viewer)
        {
            try
            {
                if (viewer != null)
                {
                    if (viewer.Chart != null)
                    {
                        if (viewer.ImageMap != null)
                        {
                            viewer.ImageMap = viewer.Chart.getHTMLImageMap("clickable", "",
                                "title='Voltage = {x} , Pulse Count = {value}'");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.UpdatePhdImageMap(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the zoom and scroll mode for all PHD charts to horizontal and vertical zooming and scrolling.
        /// </summary>
        private void PhdSetupZoomMode()
        {
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    this.phdChartWinChartViewerList[i].ZoomDirection = WinChartDirection.HorizontalVertical;
                    this.phdChartWinChartViewerList[i].ScrollDirection = WinChartDirection.HorizontalVertical;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhdSetupZoomMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the mouse to pointer mode for all active PHD charts
        /// </summary>
        private void PhdSetPointerMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.phdChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhdSetPointerMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the mouse to zoom in mode for all active PHD charts
        /// </summary>
        private void PhdSetZoomInMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.phdChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhdSetZoomInMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the mouse to zoom out mode for all active PHD charts
        /// </summary>
        private void PhdSetZoomOutMode()
        {
            try
            {
                int count = this.activeChannelNumberList.Count;
                for (int i = 0; i < count; i++)
                {
                    this.phdChartWinChartViewerList[i].MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhdSetZoomOutMode()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixInitialPhaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CallInitialPhaseSettingDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixInitialPhaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarInitialPhaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CallInitialPhaseSettingDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarInitialPhaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void threeDeeInitialPhaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CallInitialPhaseSettingDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeInitialPhaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdInitialPhaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CallInitialPhaseSettingDialog();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdInitialPhaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Opens the InitialPhaseSetting form and changes local values for initial phase offset and which sine waves will be graphed in the PRPDD charts.
        /// </summary>
        private void CallInitialPhaseSettingDialog()
        {
            try
            {
                using (InitialPhaseSetting initialPhaseSetting = new InitialPhaseSetting(this.initialPhaseResolvedDataPhaseShift, this.showZeroDegreeSineWave, this.showOneTwentyDegreeSineWave, this.showTwoFortyDegreeSineWave, this.sineWaveAColor, this.sineWaveBColor, this.sineWaveCColor))
                {
                    initialPhaseSetting.ShowDialog();
                    initialPhaseSetting.Hide();
                    if (initialPhaseSetting.UpdatesAreRequired)
                    {
                        this.initialPhaseResolvedDataPhaseShift = initialPhaseSetting.PhaseShift;
                        this.showZeroDegreeSineWave = initialPhaseSetting.ShowZeroDegreeSineWave;
                        this.showOneTwentyDegreeSineWave = initialPhaseSetting.ShowOneTwentyDegreeSineWave;
                        this.showTwoFortyDegreeSineWave = initialPhaseSetting.ShowTwoFortyDegreeSineWave;

                        this.sineWaveAColor = initialPhaseSetting.SineWaveAColor;
                        this.sineWaveBColor = initialPhaseSetting.SineWaveBColor;
                        this.sineWaveCColor = initialPhaseSetting.SineWaveCColor;

                        SetAllMultiChartsAsNeedingUpdating();
                        DrawAllMultiChartGraphs();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.CallInitialPhaseSettingDialog()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

      

//        private void PhaseResolvedDataViewer_FormClosing(object sender, FormClosingEventArgs e)
//        {
//            try
//            {
//                GetAllGridViewColumnWidths();
//                PreferencesSave();
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.PhaseResolvedDataViewer_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void SelectOneActiveChannel(ref RadCheckBox channelSelectRadCheckBox)
        {
            try
            {
                if (channelSelectRadCheckBox.Enabled)
                {
                    channelSelectRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SelectOneActiveChannel(ref RadCheckBox)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UnselectOneActiveChannel(ref RadCheckBox channelSelectRadCheckBox)
        {
            try
            {
                if (channelSelectRadCheckBox.Enabled)
                {
                    channelSelectRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.UnselectOneActiveChannel(ref RadCheckBox)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    graphRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeRadContextMenus()
        {
            try
            {
                matrixChart1RadContextMenu = new RadContextMenu();
                matrixChart2RadContextMenu = new RadContextMenu();
                matrixChart3RadContextMenu = new RadContextMenu();
                matrixChart4RadContextMenu = new RadContextMenu();
                matrixChart5RadContextMenu = new RadContextMenu();
                matrixChart6RadContextMenu = new RadContextMenu();
                matrixChart7RadContextMenu = new RadContextMenu();
                matrixChart8RadContextMenu = new RadContextMenu();
                matrixChart9RadContextMenu = new RadContextMenu();
                matrixChart10RadContextMenu = new RadContextMenu();
                matrixChart11RadContextMenu = new RadContextMenu();
                matrixChart12RadContextMenu = new RadContextMenu();
                matrixChart13RadContextMenu = new RadContextMenu();
                matrixChart14RadContextMenu = new RadContextMenu();
                matrixChart15RadContextMenu = new RadContextMenu();

                prpddPolarChart1RadContextMenu = new RadContextMenu();
                prpddPolarChart2RadContextMenu = new RadContextMenu();
                prpddPolarChart3RadContextMenu = new RadContextMenu();
                prpddPolarChart4RadContextMenu = new RadContextMenu();
                prpddPolarChart5RadContextMenu = new RadContextMenu();
                prpddPolarChart6RadContextMenu = new RadContextMenu();
                prpddPolarChart7RadContextMenu = new RadContextMenu();
                prpddPolarChart8RadContextMenu = new RadContextMenu();
                prpddPolarChart9RadContextMenu = new RadContextMenu();
                prpddPolarChart10RadContextMenu = new RadContextMenu();
                prpddPolarChart11RadContextMenu = new RadContextMenu();
                prpddPolarChart12RadContextMenu = new RadContextMenu();
                prpddPolarChart13RadContextMenu = new RadContextMenu();
                prpddPolarChart14RadContextMenu = new RadContextMenu();
                prpddPolarChart15RadContextMenu = new RadContextMenu();

                threeDeeChart1RadContextMenu = new RadContextMenu();
                threeDeeChart2RadContextMenu = new RadContextMenu();
                threeDeeChart3RadContextMenu = new RadContextMenu();
                threeDeeChart4RadContextMenu = new RadContextMenu();
                threeDeeChart5RadContextMenu = new RadContextMenu();
                threeDeeChart6RadContextMenu = new RadContextMenu();
                threeDeeChart7RadContextMenu = new RadContextMenu();
                threeDeeChart8RadContextMenu = new RadContextMenu();
                threeDeeChart9RadContextMenu = new RadContextMenu();
                threeDeeChart10RadContextMenu = new RadContextMenu();
                threeDeeChart11RadContextMenu = new RadContextMenu();
                threeDeeChart12RadContextMenu = new RadContextMenu();
                threeDeeChart13RadContextMenu = new RadContextMenu();
                threeDeeChart14RadContextMenu = new RadContextMenu();
                threeDeeChart15RadContextMenu = new RadContextMenu();

                phdChart1RadContextMenu = new RadContextMenu();
                phdChart2RadContextMenu = new RadContextMenu();
                phdChart3RadContextMenu = new RadContextMenu();
                phdChart4RadContextMenu = new RadContextMenu();
                phdChart5RadContextMenu = new RadContextMenu();
                phdChart6RadContextMenu = new RadContextMenu();
                phdChart7RadContextMenu = new RadContextMenu();
                phdChart8RadContextMenu = new RadContextMenu();
                phdChart9RadContextMenu = new RadContextMenu();
                phdChart10RadContextMenu = new RadContextMenu();
                phdChart11RadContextMenu = new RadContextMenu();
                phdChart12RadContextMenu = new RadContextMenu();
                phdChart13RadContextMenu = new RadContextMenu();
                phdChart14RadContextMenu = new RadContextMenu();
                phdChart15RadContextMenu = new RadContextMenu();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.InitializeRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetUpMultiChartRadContextMenus()
        {
            try
            {
                SetupMatrixChartRadContextMenus();
                SetupPRPDDPolarChartRadContextMenus();
                SetupThreeDeeChartRadContextMenus();
                SetupPHDChartRadContextMenus();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetUpMultiChartRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #region Matrix Chart Context Menus

        private void SetupMatrixChartRadContextMenus()
        {
            try
            {
                RadMenuItem menuItem;

                matrixChart1RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart1CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart1RadContextMenu.Items.Add(menuItem);

                matrixChart2RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart2CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart2RadContextMenu.Items.Add(menuItem);

                matrixChart3RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart3CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart3RadContextMenu.Items.Add(menuItem);

                matrixChart4RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart4CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart4RadContextMenu.Items.Add(menuItem);

                matrixChart5RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart5CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart5RadContextMenu.Items.Add(menuItem);

                matrixChart6RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart6CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart6RadContextMenu.Items.Add(menuItem);

                matrixChart7RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart7CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart7RadContextMenu.Items.Add(menuItem);

                matrixChart8RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart8CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart8RadContextMenu.Items.Add(menuItem);

                matrixChart9RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart9CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart9RadContextMenu.Items.Add(menuItem);

                matrixChart10RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart10CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart10RadContextMenu.Items.Add(menuItem);

                matrixChart11RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart11CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart11RadContextMenu.Items.Add(menuItem);

                matrixChart12RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart12CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart12RadContextMenu.Items.Add(menuItem);

                matrixChart13RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart13CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart13RadContextMenu.Items.Add(menuItem);

                matrixChart14RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart14CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart14RadContextMenu.Items.Add(menuItem);

                matrixChart15RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += matrixChart15CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                matrixChart15RadContextMenu.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupMatrixChartRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart1WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart1WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart2WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart3WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart3WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void matrixChart4WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart4RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart4WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart5WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart5RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart5WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart6WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart6RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart6WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart7WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart7RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart7WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart8WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart8RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart8WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart9WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart9RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart9WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart10WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart10RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart10WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart11WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart11RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart11WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart12WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart12RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart12WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart13WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart13RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart13WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart14WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart14RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart14WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart15WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref matrixChart15RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart15WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart1CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart1CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart2CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart2WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart2WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart2CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart3CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart3WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart3WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart3CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart4CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart4WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart4WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart4CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart5CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart5WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart5WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart5CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart6CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart6WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart6WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart6CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart7CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart7WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart7WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart7CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart8CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart8WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart8WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart8CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart9CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart9WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart9WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart9CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart10CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart10WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart10WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart10CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart11CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart11WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart11WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart11CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart12CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart12WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart12WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart12CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart13CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart13WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart13WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart13CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart14CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart14WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart14WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart14CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void matrixChart15CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (matrixChart15WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(matrixChart15WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.matrixChart15CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion // Matrix Charts Context Menus

        #region PRPDD Polar Chart Context Menus

        private void SetupPRPDDPolarChartRadContextMenus()
        {
            try
            {
                RadMenuItem menuItem;

                prpddPolarChart1RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart1CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart1RadContextMenu.Items.Add(menuItem);

                prpddPolarChart2RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart2CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart2RadContextMenu.Items.Add(menuItem);

                prpddPolarChart3RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart3CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart3RadContextMenu.Items.Add(menuItem);

                prpddPolarChart4RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart4CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart4RadContextMenu.Items.Add(menuItem);

                prpddPolarChart5RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart5CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart5RadContextMenu.Items.Add(menuItem);

                prpddPolarChart6RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart6CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart6RadContextMenu.Items.Add(menuItem);

                prpddPolarChart7RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart7CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart7RadContextMenu.Items.Add(menuItem);

                prpddPolarChart8RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart8CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart8RadContextMenu.Items.Add(menuItem);

                prpddPolarChart9RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart9CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart9RadContextMenu.Items.Add(menuItem);

                prpddPolarChart10RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart10CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart10RadContextMenu.Items.Add(menuItem);

                prpddPolarChart11RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart11CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart11RadContextMenu.Items.Add(menuItem);

                prpddPolarChart12RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart12CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart12RadContextMenu.Items.Add(menuItem);

                prpddPolarChart13RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart13CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart13RadContextMenu.Items.Add(menuItem);

                prpddPolarChart14RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart14CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart14RadContextMenu.Items.Add(menuItem);

                prpddPolarChart15RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += prpddPolarChart15CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                prpddPolarChart15RadContextMenu.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupPRPDDPolarChartRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart1WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart1WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart2WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart3WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart3WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void prpddPolarChart4WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart4RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart4WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart5WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart5RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart5WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart6WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart6RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart6WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart7WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart7RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart7WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart8WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart8RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart8WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart9WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart9RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart9WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart10WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart10RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart10WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart11WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart11RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart11WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart12WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart12RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart12WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart13WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart13RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart13WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart14WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart14RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart14WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart15WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref prpddPolarChart15RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart15WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart1CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart1CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart2CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart2WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart2WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart2CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart3CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart3WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart3WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart3CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart4CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart4WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart4WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart4CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart5CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart5WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart5WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart5CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart6CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart6WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart6WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart6CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart7CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart7WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart7WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart7CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart8CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart8WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart8WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart8CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart9CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart9WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart9WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart9CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart10CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart10WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart10WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart10CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart11CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart11WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart11WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart11CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart12CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart12WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart12WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart12CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart13CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart13WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart13WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart13CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart14CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart14WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart14WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart14CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void prpddPolarChart15CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (prpddPolarChart15WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(prpddPolarChart15WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.prpddPolarChart15CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion // Matrix Charts Context Menus

        #region Three Dee Charts Context Menus

        private void SetupThreeDeeChartRadContextMenus()
        {
            try
            {
                RadMenuItem menuItem;

                threeDeeChart1RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart1CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart1RadContextMenu.Items.Add(menuItem);

                threeDeeChart2RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart2CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart2RadContextMenu.Items.Add(menuItem);

                threeDeeChart3RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart3CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart3RadContextMenu.Items.Add(menuItem);

                threeDeeChart4RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart4CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart4RadContextMenu.Items.Add(menuItem);

                threeDeeChart5RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart5CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart5RadContextMenu.Items.Add(menuItem);

                threeDeeChart6RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart6CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart6RadContextMenu.Items.Add(menuItem);

                threeDeeChart7RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart7CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart7RadContextMenu.Items.Add(menuItem);

                threeDeeChart8RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart8CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart8RadContextMenu.Items.Add(menuItem);

                threeDeeChart9RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart9CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart9RadContextMenu.Items.Add(menuItem);

                threeDeeChart10RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart10CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart10RadContextMenu.Items.Add(menuItem);

                threeDeeChart11RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart11CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart11RadContextMenu.Items.Add(menuItem);

                threeDeeChart12RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart12CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart12RadContextMenu.Items.Add(menuItem);

                threeDeeChart13RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart13CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart13RadContextMenu.Items.Add(menuItem);

                threeDeeChart14RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart14CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart14RadContextMenu.Items.Add(menuItem);

                threeDeeChart15RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += threeDeeChart15CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                threeDeeChart15RadContextMenu.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupThreeDeeChartRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart1WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart1WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart2WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart3WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart3WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void threeDeeChart4WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart4RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart4WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart5WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart5RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart5WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart6WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart6RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart6WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart7WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart7RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart7WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart8WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart8RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart8WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart9WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart9RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart9WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart10WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart10RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart10WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart11WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart11RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart11WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart12WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart12RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart12WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart13WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart13RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart13WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart14WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart14RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart14WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart15WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref threeDeeChart15RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart15WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart1CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart1CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart2CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart2WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart2WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart2CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart3CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart3WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart3WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart3CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart4CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart4WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart4WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart4CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart5CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart5WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart5WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart5CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart6CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart6WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart6WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart6CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart7CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart7WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart7WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart7CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart8CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart8WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart8WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart8CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart9CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart9WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart9WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart9CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart10CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart10WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart10WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart10CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart11CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart11WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart11WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart11CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart12CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart12WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart12WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart12CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart13CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart13WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart13WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart13CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart14CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart14WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart14WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart14CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void threeDeeChart15CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (threeDeeChart15WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(threeDeeChart15WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.threeDeeChart15CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion  // Three Dee Charts Context Menus

        #region PHD Charts Context Menus

        private void SetupPHDChartRadContextMenus()
        {
            try
            {
                RadMenuItem menuItem;

                phdChart1RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart1CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart1RadContextMenu.Items.Add(menuItem);

                phdChart2RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart2CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart2RadContextMenu.Items.Add(menuItem);

                phdChart3RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart3CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart3RadContextMenu.Items.Add(menuItem);

                phdChart4RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart4CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart4RadContextMenu.Items.Add(menuItem);

                phdChart5RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart5CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart5RadContextMenu.Items.Add(menuItem);

                phdChart6RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart6CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart6RadContextMenu.Items.Add(menuItem);

                phdChart7RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart7CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart7RadContextMenu.Items.Add(menuItem);

                phdChart8RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart8CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart8RadContextMenu.Items.Add(menuItem);

                phdChart9RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart9CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart9RadContextMenu.Items.Add(menuItem);

                phdChart10RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart10CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart10RadContextMenu.Items.Add(menuItem);

                phdChart11RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart11CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart11RadContextMenu.Items.Add(menuItem);

                phdChart12RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart12CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart12RadContextMenu.Items.Add(menuItem);

                phdChart13RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart13CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart13RadContextMenu.Items.Add(menuItem);

                phdChart14RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart14CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart14RadContextMenu.Items.Add(menuItem);

                phdChart15RadContextMenu.Items.Clear();
                menuItem = new RadMenuItem();
                menuItem.Click += phdChart15CopyGraph_Click;
                menuItem.Text = "Copy Graph";
                phdChart15RadContextMenu.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.SetupPHDChartRadContextMenus()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart1WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart1WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart2WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart3WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart3WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void phdChart4WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart4RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart4WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart5WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart5RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart5WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart6WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart6RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart6WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart7WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart7RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart7WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart8WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart8RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart8WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart9WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart9RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart9WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart10WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart10RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart10WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart11WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart11RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart11WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart12WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart12RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart12WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart13WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart13RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart13WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart14WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart14RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart14WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart15WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref phdChart15RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart15WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart1CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart1CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart2CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart2WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart2WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart2CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart3CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart3WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart3WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart3CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart4CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart4WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart4WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart4CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart5CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart5WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart5WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart5CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart6CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart6WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart6WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart6CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart7CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart7WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart7WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart7CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart8CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart8WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart8WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart8CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart9CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart9WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart9WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart9CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart10CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart10WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart10WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart10CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart11CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart11WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart11WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart11CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart12CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart12WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart12WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart12CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart13CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart13WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart13WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart13CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart14CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart14WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart14WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart14CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void phdChart15CopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (phdChart15WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(phdChart15WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PhaseResolvedDataViewer.phdChart15CopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        #endregion

        private void showRadialLabelsRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            this.prpddPolarChartsNeedUpdating = true;
            DrawAllPRPDDPolarGraphs();
        }

    }

    public class CommonData
    {
        public DateTime[] readingDateTime;
        public int[] alarmState;
        public string[] alarmSource;
        public string[] registeredChannels;
        public bool[] matrixWasRecorded;
        public double[] humidity;
        public double[] moisture;
        public double[] loadCurrent1;
        public double[] loadCurrent2;
        public double[] loadCurrent3;
        public double[] temp1;
        public double[] temp2;
        public double[] temp3;
        public double[] temp4;
        public double[] voltage1;
        public double[] voltage2;
        public double[] voltage3;

        int index = 0;

        public void AllocateAllArrays(int numberOfPoints)
        {
            readingDateTime = new DateTime[numberOfPoints];
            alarmState = new int[numberOfPoints];
            alarmSource = new string[numberOfPoints];
            registeredChannels = new string[numberOfPoints];
            matrixWasRecorded = new bool[numberOfPoints];
            humidity = new double[numberOfPoints];
            moisture = new double[numberOfPoints];
            loadCurrent1 = new double[numberOfPoints];
            loadCurrent2 = new double[numberOfPoints];
            loadCurrent3 = new double[numberOfPoints];
            temp1 = new double[numberOfPoints];
            temp2 = new double[numberOfPoints];
            temp3 = new double[numberOfPoints];
            temp4 = new double[numberOfPoints];
            voltage1 = new double[numberOfPoints];
            voltage2 = new double[numberOfPoints];
            voltage3 = new double[numberOfPoints];
            index = 0;
        }

        public void Add(PDM_DataComponent_Parameters parameters, PDM_DataComponent_PDParameters pdParameters, DateTime argReadingDateTime, int argAlarmState, string argAlarmSource)
        {
            try
            {
                if ((parameters != null) && (pdParameters != null))
                {
                    readingDateTime[index] = argReadingDateTime;
                    alarmState[index] = argAlarmState;
                    alarmSource[index] = argAlarmSource;
                    registeredChannels[index] = pdParameters.Channels;
                    if (pdParameters.Matrix > 0)
                    {
                        matrixWasRecorded[index] = true;
                    }
                    else
                    {
                        matrixWasRecorded[index] = false;
                    }
                    humidity[index] = parameters.Humidity;
                    loadCurrent1[index] = parameters.ExtLoadActive;
                    loadCurrent2[index] = parameters.CommonRegisters_4/10.0;
                    loadCurrent3[index] = parameters.CommonRegisters_5/10.0;
                    temp1[index] = parameters.Temperature;
                    temp2[index] = parameters.CommonRegisters_1;
                    temp3[index] = parameters.CommonRegisters_2;
                    temp4[index] = parameters.CommonRegisters_3;
                    voltage1[index] = parameters.CommonRegisters_6/10.0;
                    voltage2[index] = parameters.CommonRegisters_7/10.0;
                    voltage3[index] = parameters.CommonRegisters_8/10.0;

                    moisture[index] = DynamicsCalculations.GetMoisture(temp1[index], humidity[index]);

                    index++;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommonData.Add(PDM_Data_Parameters, PDM_Data_PDParameters, int, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }

    /// <summary>
    /// This class is set up to handle one channel's worth of data over an indefinite number of data readings.
    /// </summary>
    public class ChannelData
    {
        public double magnitudeNC;
        public double magnitudeMV;
        public double magnitudePlus;
        public double magnitudeMinus;
        public double pdi;
        public double pdiPlus;
        public double pdiMinus;
        public double magnitudeTrend;
        public double sensitivity;
        public double pdiTrend;
        public double pulseCount;
        public double pulseCountPlus;
        public double pulseCountMinus;
        public double pulseCountPerCycle;
        public double pulseCountPlusPerCycle;
        public double pulseCountMinusPerCycle;

        public DateTime readingDateTime;

        public void Add(PDM_DataComponent_ChPDParameters chPDParameters, DateTime argReadingDateTime)
        {
            try
            {
                if (chPDParameters != null)
                {
                    magnitudeNC = chPDParameters.Q02mV * chPDParameters.ChannelSensitivity;
                    magnitudeMV = chPDParameters.Q02mV;
                    magnitudePlus = chPDParameters.Q02Plus;
                    magnitudeMinus = chPDParameters.Q02Minus;
                    pdi = chPDParameters.PDI;
                    pdiPlus = chPDParameters.PDIPlus;
                    pdiMinus = chPDParameters.PDIMinus;
                    magnitudeTrend = chPDParameters.Q02_t;
                    sensitivity = chPDParameters.ChannelSensitivity;
                    pdiTrend = chPDParameters.PDI_t;
                    pulseCount = chPDParameters.SumPDAmpl;
                    pulseCountPerCycle = chPDParameters.SumPDAmpl / 60;
                    pulseCountPlus = chPDParameters.SumPDPlus;
                    pulseCountPlusPerCycle = chPDParameters.SumPDPlus / 60;
                    pulseCountMinus = chPDParameters.SumPDMinus;
                    pulseCountMinusPerCycle = chPDParameters.SumPDMinus / 60;

                    readingDateTime = argReadingDateTime;

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ChannelData.Add(PDM_Data_ChPDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void InsertMissingEntry(int location, DateTime date)
        {
            try
            {
                //magnitudeNC = ConversionMethods.InsertZeroEntryInDoubleArray(magnitudeNC, location);
                //magnitudeMV = ConversionMethods.InsertZeroEntryInDoubleArray(magnitudeMV, location);
                //magnitudePlus = ConversionMethods.InsertZeroEntryInDoubleArray(magnitudePlus, location);
                //magnitudeMinus = ConversionMethods.InsertZeroEntryInDoubleArray(magnitudeMinus, location);
                //pdi = ConversionMethods.InsertZeroEntryInDoubleArray(pdi, location);
                //pdiPlus = ConversionMethods.InsertZeroEntryInDoubleArray(pdiPlus, location);
                //pdiMinus = ConversionMethods.InsertZeroEntryInDoubleArray(pdiMinus, location);
                //magnitudeTrend = ConversionMethods.InsertZeroEntryInDoubleArray(magnitudeTrend, location);
                //pdiTrend = ConversionMethods.InsertZeroEntryInDoubleArray(pdiTrend, location);
                //pulseCount = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCount, location);
                //pulseCountPlus = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCountPlus, location);
                //pulseCountMinus = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCountMinus, location);
                //pulseCountPerCycle = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCountPerCycle, location);
                //pulseCountPlusPerCycle = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCountPlusPerCycle, location);
                //pulseCountMinusPerCycle = ConversionMethods.InsertZeroEntryInDoubleArray(pulseCountMinusPerCycle, location);

                //readingDateTime = ConversionMethods.InsertEntryInDateTimeArray(readingDateTime, location, date);

                //if (index < readingDateTime.Length - 1)
                //{
                //    index++;
                //}
                //else
                //{
                //    LogMessage.LogError("Error in ChannelData.InsertMissingEntry: At the limit of the array bounds");
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ChannelData.InsertMissingEntry(int, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }

    /// <summary>
    /// This class is set up to handle one channel and one polarity of phase resolved data over an indefinite number
    /// of data readings.
    /// </summary>
    public class MatrixData
    {
        public List<MatrixEntry> matrixEntries;
        public int smallestNumberedRowThatContainsAnEntry;

        public void AllocateAllArrays()
        {
            matrixEntries = new List<MatrixEntry>();
        }

        public void Add(PDM_DataComponent_PhaseResolvedData phaseResolvedData)
        {
            try
            {
                matrixEntries = ExtractMatrix(phaseResolvedData.NonZeroEntries);
                smallestNumberedRowThatContainsAnEntry = FindSmallestNumberedRowThatContainsAnEntry();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MatrixData.Add(PDM_Data_PhaseResolvedData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        public void InsertMissingEntry(int location, DateTime date)
//        {
//            try
//            {
//                int limit = matrixEntries.Length - 1;

//                for (int i = limit; i > location; i--)
//                {
//                    matrixEntries[i] = matrixEntries[i - 1];
//                }
//                matrixEntries[location] = new List<MatrixEntry>();

//                smallestNumberedRowThatContainsAnEntry = ConversionMethods.InsertZeroEntryInIntegerArray(smallestNumberedRowThatContainsAnEntry, location);

//                readingDateTime = ConversionMethods.InsertEntryInDateTimeArray(readingDateTime, location, date);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in MatrixData.InsertMissingEntry(int, DateTime)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private List<MatrixEntry> ExtractMatrix(string sparseFormatEntries)
        {
            List<MatrixEntry> entries = new List<MatrixEntry>();
            try
            {
                /// entries are separated by a ';'
                string[] splitSparseFormat = sparseFormatEntries.Split(';');
                string[] oneEntry;
                MatrixEntry matrixEntry;
                int count = splitSparseFormat.Length;
                /// make up for trailing ';' in the input string
                count--;
                for (int i = 0; i < count; i++)
                {
                    oneEntry = splitSparseFormat[i].Split(',');
                    matrixEntry = new MatrixEntry(Int32.Parse(oneEntry[0]), Int32.Parse(oneEntry[1]), Int32.Parse(oneEntry[2]));
                    entries.Add(matrixEntry);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MatrixData.ExtractMatrix(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return entries;
        }

        public int FindSmallestNumberedRowThatContainsAnEntry()
        {
            int rowNumber = 100;
            try
            {
                foreach (MatrixEntry entry in matrixEntries)
                {
                    if (entry.rowNumber < rowNumber)
                    {
                        rowNumber = entry.rowNumber;
                    }
                }
                if (rowNumber == 100)
                {
                    rowNumber = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MatrixData.FindSmallestNumberedRowThatContainsAnEntry(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return rowNumber;
        }
    }

    /// <summary>
    /// This cointains a single "sparse" entry in a set of phase resolved data
    /// </summary>
    public class MatrixEntry
    {
        public int rowNumber;
        public int columnNumber;
        public int value;

        public MatrixEntry(int row, int column, int val)
        {
            rowNumber = row;
            columnNumber = column;
            value = val;
        }
    }

    /// <summary>
    /// This code was lifted from an online posting.  It puts up a bunch of junk when you click on a data point.
    /// It will probably be useful in the future if the message is customized.  I'm pretty sure it's sample code
    /// from the chartDirector people.
    /// </summary>
    public class ParamViewer : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button OKPB;
        private System.Windows.Forms.ColumnHeader Key;
        private System.Windows.Forms.ColumnHeader Value;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ListView listView;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public ParamViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView = new System.Windows.Forms.ListView();
            this.OKPB = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.Key = new System.Windows.Forms.ColumnHeader();
            this.Value = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.Key,
																					   this.Value});
            this.listView.GridLines = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.Location = new System.Drawing.Point(8, 56);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(304, 168);
            this.listView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView.TabIndex = 0;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // OKPB
            // 
            this.OKPB.Location = new System.Drawing.Point(120, 232);
            this.OKPB.Name = "OKPB";
            this.OKPB.Size = new System.Drawing.Size(72, 24);
            this.OKPB.TabIndex = 1;
            this.OKPB.Text = "OK";
            this.OKPB.Click += new System.EventHandler(this.OKPB_Click);
            // 
            // label
            // 
            this.label.Location = new System.Drawing.Point(8, 8);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(304, 48);
            this.label.TabIndex = 2;
            this.label.Text = "This is to demonstrate that ChartDirector charts are clickable. In this demo prog" +
                "ram, we just display the information provided to the ClickHotSpot event handler." +
                " ";
            // 
            // Key
            // 
            this.Key.Text = "Key";
            this.Key.Width = 80;
            // 
            // Value
            // 
            this.Value.Text = "Value";
            this.Value.Width = 220;
            // 
            // ParamViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
            this.ClientSize = new System.Drawing.Size(320, 261);
            this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label,
																		  this.OKPB,
																		  this.listView});
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ParamViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hot Spot Parameters";
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public void Display(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            // Add the name of the ChartViewer control that is being clicked
            listView.Items.Add(new ListViewItem(new string[] {"source", 
				((ChartDirector.WinChartViewer)sender).Name}));

            // List out the parameters of the hot spot
            foreach (DictionaryEntry key in e.GetAttrValues())
            {
                listView.Items.Add(new ListViewItem(
                    new string[] { (string)key.Key, (string)key.Value }));
            }

            // Display the form
            ShowDialog();
        }

        /// <summary>
        /// Handler for the OK button
        /// </summary>
        private void OKPB_Click(object sender, System.EventArgs e)
        {
            // Just close the Form
            Close();
        }
    }
}
