﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class WriteRegisterValue : Telerik.WinControls.UI.RadForm
    {
        int modBusAddress;
        int registerNumber;
        int registerValue;

        int numberOfNonInteractiveRetries;
        int totalRetries;

        public WriteRegisterValue(int inputModBusAddress, int inputRegisterNumber, int inputRegisterValue, Point desiredWindowPosition, int argNumberOfNonInteractiveRetries, int argTotalRetries)
        {
            try
            {
                InitializeComponent();

                modBusAddress = inputModBusAddress;
                registerNumber = inputRegisterNumber;
                registerValue = inputRegisterValue;

                this.numberOfNonInteractiveRetries = argNumberOfNonInteractiveRetries;
                this.totalRetries = argTotalRetries;

                // this.Location = desiredWindowPosition;
                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in WriteRegisterValue.WriteRegisterValue(int, int, int, Point)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteRegisterValue_Load(object sender, EventArgs e)
        {
            try
            {
                registerNumberRadTextBox.Text = this.registerNumber.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in WriteRegisterValue.WriteRegisterValue_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            registerValueRadSpinEditor.Value = (decimal)this.registerValue;
        }

        private void writeRegisterValueRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 newRegisterValue = (Int32)registerValueRadSpinEditor.Value;
                // this creates a UInt16 and shoves it into a Int16 by manipulating the bytes in the source int,
                // the value will show as negative for high values but the device registers are positive only
                Int16 newRegisterValueAsFakeUnsignedInt = ConversionMethods.Int32ToFakeUnsignedShort(newRegisterValue);

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                ErrorCode errorCode = SimplifiedCommunication.OpenUsbConnectionToAnyMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries);



                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    if (!InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, registerNumber, newRegisterValueAsFakeUnsignedInt, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                    {
                        RadMessageBox.Show("Failed to write the new value");
                    }
                    else
                    {
                        RadMessageBox.Show("Wrote the new value");
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in WriteRegisterValue.writeRegisterValueRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
