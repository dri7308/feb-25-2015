﻿using System;
using System.Linq;
using System.Windows.Forms;
using GeneralUtilities;
using MonitorInterface;
using Telerik.WinControls;

namespace DMSeriesUtilities
{
    public partial class AutoBalance : Telerik.WinControls.UI.RadForm
    {
        private DateTime deviceDateTime;

        private DateTime dateTimeToSet = ConversionMethods.MinimumDateTime();

        public DateTime DateTimeToSet
        {
            get
            {
                return dateTimeToSet;
            }
        }

        private bool writeRegistersToDevice = false;

        public bool WriteRegistersToDevice
        {
            get
            {
                return writeRegistersToDevice;
            }
        }

        private DateTime autoBalanceDate;

        public DateTime AutoBalanceDate
        {
            get
            {
                return autoBalanceDate;
            }
        }

        private int balanceDelayInDays;

        public int BalanceDelayInDays
        {
            get
            {
                return balanceDelayInDays;
            }
        }

        private bool performNoLoadTest = false;

        public bool PerformNoLoadTest
        {
            get
            {
                return performNoLoadTest;
            }
        }

        private bool enableAutoBalance = false;

        public bool EnableAutoBalance
        {
            get
            {
                return enableAutoBalance;
            }
        }

        public AutoBalance(DateTime inputDeviceDateTime, int modBusAddress, int numberOfNonInteractiveRetries, int totalRetries)
        {

            InitializeComponent();
           // deviceDateTime = inputDeviceDateTime;
            this.StartPosition = FormStartPosition.CenterParent;
            // code added by cfk 12/18/13.  want to ready exisitng setting of auto balance
            // must read registers 622 - 627
            // --------------------
            DateTime currentDeviceDateTime;
            Int16[] registersToRead;
            ErrorCode errorCode;
            DateTime currentSetDate;
            int deviceIsBusy;
            registersToRead = new Int16[7];
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            radCalendar1.AllowMultipleSelect = false;
            radCalendar1.AllowMultipleView = false;
            radCalendar1.RangeMinDate = DateTime.Now;

            try
            {

                errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation);
                    if (deviceIsBusy == 0)
                    {
                        if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation))
                        {
                            registersToRead = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 622, 6, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation);
                            // now set controls to read conditions;
                            if (registersToRead[5] == 0)
                            {
                                enableAutoBalanceRadRadioButton.IsChecked = false;
                                disableAutoBalanceRadRadioButton.IsChecked = true;
                            }
                            else
                            {
                                enableAutoBalanceRadRadioButton.IsChecked = true;
                                disableAutoBalanceRadRadioButton.IsChecked = false;
                            }

                            energizingDelayRadSpinEditor.Value = registersToRead[4];
                            hourRadSpinEditor.Value = registersToRead[3];
                            //minuteRadSpinEditor.Value = registersToRead[

                            currentSetDate = ConversionMethods.ConvertIntegerValuesToDateTime(registersToRead[0], registersToRead[1], registersToRead[2], 0, 0, 0);
                            radCalendar1.SelectedDate = currentSetDate;
                            radCalendar1.FocusedDate = currentSetDate;                           
                           
                          //  MessageBox.Show(radCalendar1.SelectedDate.ToShortDateString());

                            if (registersToRead == null)
                            //modBusAddress, 622, registersToWrite, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation))
                            {
                                RadMessageBox.Show("Failed to read the auto-balance parameters from the device");
                            }
                            InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, numberOfNonInteractiveRetries, totalRetries, parentWindowInformation);
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                        }
                    }
                    else if (deviceIsBusy == 1)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                    }
                }
                else
                {
                    RadMessageBox.Show("Failed to read the auto-balance parameters from the device");
                }

                //------------------------------------------------
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AutoBalance_Load(object sender, EventArgs e)
        { 
            try
            {
               // radCalendar1.SelectedDate = deviceDateTime;
               
                //this.pdTimeRadMaskedEditBox.Text = CreateStringTimeFromDateTime(currentTime);

               // hourRadSpinEditor.Value = (decimal)deviceDateTime.Hour;
                
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMUtility.AutoBalance_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void setAutoBalanceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int hour, minute, second;
                DateTime desiredDate = radCalendar1.SelectedDate;

                hour = (int)hourRadSpinEditor.Value;
                

                dateTimeToSet = new DateTime(desiredDate.Year, desiredDate.Month, desiredDate.Day, hour, 0, 0);

                performNoLoadTest = false;

                if (dateTimeToSet.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                {
                    balanceDelayInDays = (int)this.energizingDelayRadSpinEditor.Value;
                    if (this.noLoadTestRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        performNoLoadTest = true;
                    }
                    if (this.enableAutoBalanceRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        if (MessageBox.Show("Are you sure you want to enable Auto Balance?", "Confirm Auto Balance", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            enableAutoBalance = true;
                            writeRegistersToDevice = true;
                            this.Close();
                        }
                    }
                    else
                    {
                        if (RadMessageBox.Show("This will disable any Auto Balance operation already set up.  Is that what you want to do?", "Warning: will disable Auto Balance", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            enableAutoBalance = false;
                            writeRegistersToDevice = true;
                            this.Close();
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show("Error in Date and Time as entered");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMUtility.AutoBalance_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void delayBeforeBalancineRadLabel_Click(object sender, EventArgs e)
        {
        }
    }
}