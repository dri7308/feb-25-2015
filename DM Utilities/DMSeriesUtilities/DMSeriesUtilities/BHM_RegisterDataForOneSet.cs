﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DMSeriesUtilities
{
    public class BHM_RegisterDataForOneSet
    {
        public DateTime readingDateTime;
        int setNumber;
        public double gamma;
        public double gammaPhase;
        public double gammaTrend;
        public double temperatureCoeff;
        public double phaseTempCoeff;
        public double bushingCurrentA;
        public double bushingCurrentB;
        public double bushingCurrentC;
        public double bushingCurrentPhaseAA;
        public double bushingCurrentPhaseAB;
        public double bushingCurrentPhaseAC;
        public double capacitanceA;
        public double capacitanceB;
        public double capacitanceC;
        public double tangentA;
        public double tangentB;
        public double tangentC;
        public double frequency;
        public int temperature;
        public int loadActive;
        public int loadReactive;
        public int humidity;
        public int ltcPositionNumber;
        public UInt16 alarmStatus;

        public BHM_RegisterDataForOneSet()
        {

        }

        public void SetDataValues(UInt16[] registerValues, int inputSetNumber)
        {
            try
            {
                if ((registerValues != null) && (registerValues.Length >= 29))
                {
                    readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(registerValues[0], registerValues[1], registerValues[2], registerValues[3], registerValues[4]);
                    if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                    {
                        setNumber = inputSetNumber;
                        gamma = registerValues[5] / 100.0;
                        gammaPhase = registerValues[6] / 100.0;
                        gammaTrend = registerValues[7] / 10.0;
                        temperatureCoeff = registerValues[8] / 100.0;
                        phaseTempCoeff = registerValues[9] / 100.0;
                        bushingCurrentA = registerValues[10] / 10.0;
                        bushingCurrentB = registerValues[11] / 10.0;
                        bushingCurrentC = registerValues[12] / 10.0;
                        bushingCurrentPhaseAA = registerValues[13] / 100.0;
                        bushingCurrentPhaseAB = registerValues[14] / 100.0;
                        bushingCurrentPhaseAC = registerValues[15] / 100.0;
                        capacitanceA = registerValues[16] / 10.0;
                        capacitanceB = registerValues[17] / 10.0;
                        capacitanceC = registerValues[18] / 10.0;
                        tangentA = registerValues[19] / 100.0;
                        tangentB = registerValues[20] / 100.0;
                        tangentC = registerValues[21] / 100.0;
                        frequency = registerValues[22] / 100.0;
                        temperature = registerValues[23] - 70;
                        loadActive = registerValues[24];
                        loadReactive = registerValues[25];
                        humidity = registerValues[26];
                        ltcPositionNumber = registerValues[27];
                        //alarmStatus = ConversionMethods.Int16BytesToUInt16Value(registerValues[28]);
                        alarmStatus = registerValues[28];
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_RegisterDataForOneSet.SetDataValues(UInt16[], int)\nInput Int16[] was either null or had too few values.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.SetDataValues(UInt16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
