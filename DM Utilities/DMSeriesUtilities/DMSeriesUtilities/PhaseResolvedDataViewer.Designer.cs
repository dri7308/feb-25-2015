﻿namespace DMSeriesUtilities
{
    partial class PhaseResolvedDataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhaseResolvedDataViewer));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.office2007BlackTheme2 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.phdRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.dataReadingDatePhdTabValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataReadingDatePhdTabTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.phdDisplaySettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdInitialPhaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.phdAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdPcRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdVRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdDbRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdScaleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdScaleRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.phdChartLayoutRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdFifteenByOneRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdEightByTwoRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdFiveByThreeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdThreeByFiveRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.phdRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.phdChart15RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart15PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart14RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart14PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart13RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart13PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart12RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart12PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart11RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart11PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel42 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart10RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart10PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel47 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart9RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart9PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel50 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart8RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart8PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel53 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart7RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart7PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel56 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart6RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart6PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel59 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart5RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart5PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel62 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart4RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart4PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel65 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart3RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart3PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel68 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart2RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart2PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel71 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart1RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phdChart1PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel74 = new Telerik.WinControls.UI.RadLabel();
            this.phdChart15WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart14WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart13WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart12WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart10WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart9WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart8WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart7WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart5WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart4WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart3WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart2WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart11WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart6WinChartViewer = new ChartDirector.WinChartViewer();
            this.phdChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.dataReadingDatePrpdd3DTabValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataReadingDatePrpdd3DTabTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeDisplaySettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeInitialPhaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.threeDeeAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeePcRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeVRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeDbRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeScaleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeScaleRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.threeDeeChart15RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart15PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel119 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel120 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart15PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart14RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart14PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel116 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel117 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart14PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart13RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart13PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel113 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel114 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart13PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart12RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart12PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel110 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel111 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart12PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart11RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart11PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel107 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel108 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart11PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart6RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart6PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel104 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel105 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart6PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart7RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart7PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel101 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel102 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart7PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart8RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart8PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel98 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel99 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart8PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart9RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart9PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel95 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel96 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart9PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart10RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart10PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel92 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel93 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart10PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart5RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart5PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel85 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel88 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart5PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart4RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart4PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel76 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel79 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart4PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart3RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart3PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel63 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel67 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart3PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart2RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart2PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel49 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel54 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart2PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart1RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeChart1PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.threeDeeChart1PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.threeDeeChart15WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart14WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart13WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart12WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart10WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart9WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart8WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart7WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart5WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart4WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart3WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart2WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart11WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart6WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.threeDeeChartLayoutRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.threeDeeFifteenByOneRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeEightByTwoRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeFiveByThreeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.threeDeeThreeByFiveRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.dataReadingDatePrpddPolarTabValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataReadingDatePrpddPolarTabTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.prpddPolarChart15RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart15PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart15PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart14RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart14PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart14PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart13RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart13PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel36 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel39 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart13PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart12RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart12PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel46 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel48 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart12PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart11RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart11PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel51 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel52 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart11PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart10RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart10PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel55 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel57 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart10PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart9RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart9PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel60 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel61 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart9PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart8RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart8PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel64 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel66 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart8PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart7RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart7PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel69 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel70 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart7PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart6RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart6PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel73 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel75 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart6PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart5RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart5PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel77 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel78 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart5PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart4RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart4PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel80 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel81 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart4PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart3RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart3PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel83 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel84 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart3PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart2RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart2PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel86 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel87 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart2PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart1RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarChart1PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel89 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel90 = new Telerik.WinControls.UI.RadLabel();
            this.prpddPolarChart1PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChart15WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart14WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart13WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart12WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart10WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart9WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart8WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart7WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart5WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart4WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart3WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart2WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart11WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart6WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.prpddPolarDisplaySettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.showRadialLabelsRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.prpddPolarInitialPhaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.prpddPolarAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarPcRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarVRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarDbRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarScaleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarScaleRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.prpddPolarChartLayoutRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.prpddPolarFifteenByOneRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarEightByTwoRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarFiveByThreeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.prpddPolarThreeByFiveRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.dataReadingDatePrpddTabValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataReadingDatePrpddTabTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.matrixRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.matrixChart15RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart15PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel43 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel44 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart15PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart14RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart14PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel40 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel41 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart14PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart13RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart13PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel37 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel38 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart13PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart12RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart12PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel34 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel35 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart12PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart11RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart11PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart11PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart10RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart10PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart10PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart9RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart9PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart9PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart8RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart8PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart8PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart7RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart7PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart7PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart6RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart6PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart6PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart5RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart5PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart5PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart4RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart4PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart4PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart3RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart3PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart3PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart2RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart2PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart2PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart1RadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixChart1PDIValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel = new Telerik.WinControls.UI.RadLabel();
            this.matrixChart1PhaseShiftRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChart15WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart14WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart13WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart12WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart10WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart9WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart8WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart7WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart5WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart4WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart3WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart2WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart11WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart6WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.matrixDisplaySettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixInitialPhaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.matrixAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixPcRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixVRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixDbRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixScaleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixScaleRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.matrixChartLayoutRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.matrixFifteenByOneRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixEightByTwoRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixFiveByThreeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.matrixThreeByFiveRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.dataRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.trendRadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.dynamicsRadGroupBoxRadContextMenuStrip = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.channelSelectRadGroupBoxRadContextMenuStrip = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.phdRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePhdTabValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePhdTabTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdDisplaySettingsRadGroupBox)).BeginInit();
            this.phdDisplaySettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdInitialPhaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdAmplitudeRadGroupBox)).BeginInit();
            this.phdAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdPcRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdVRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdDbRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdScaleRadGroupBox)).BeginInit();
            this.phdScaleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdScaleRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChartLayoutRadGroupBox)).BeginInit();
            this.phdChartLayoutRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdFifteenByOneRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdEightByTwoRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdFiveByThreeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdThreeByFiveRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdRadPanel)).BeginInit();
            this.phdRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15RadGroupBox)).BeginInit();
            this.phdChart15RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14RadGroupBox)).BeginInit();
            this.phdChart14RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13RadGroupBox)).BeginInit();
            this.phdChart13RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12RadGroupBox)).BeginInit();
            this.phdChart12RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11RadGroupBox)).BeginInit();
            this.phdChart11RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10RadGroupBox)).BeginInit();
            this.phdChart10RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9RadGroupBox)).BeginInit();
            this.phdChart9RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8RadGroupBox)).BeginInit();
            this.phdChart8RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7RadGroupBox)).BeginInit();
            this.phdChart7RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6RadGroupBox)).BeginInit();
            this.phdChart6RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5RadGroupBox)).BeginInit();
            this.phdChart5RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4RadGroupBox)).BeginInit();
            this.phdChart4RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3RadGroupBox)).BeginInit();
            this.phdChart3RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2RadGroupBox)).BeginInit();
            this.phdChart2RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1RadGroupBox)).BeginInit();
            this.phdChart1RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1WinChartViewer)).BeginInit();
            this.threeDeeRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpdd3DTabValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpdd3DTabTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeDisplaySettingsRadGroupBox)).BeginInit();
            this.threeDeeDisplaySettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeInitialPhaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeAmplitudeRadGroupBox)).BeginInit();
            this.threeDeeAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeePcRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeVRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeDbRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeScaleRadGroupBox)).BeginInit();
            this.threeDeeScaleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeScaleRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeRadPanel)).BeginInit();
            this.threeDeeRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15RadGroupBox)).BeginInit();
            this.threeDeeChart15RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14RadGroupBox)).BeginInit();
            this.threeDeeChart14RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13RadGroupBox)).BeginInit();
            this.threeDeeChart13RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12RadGroupBox)).BeginInit();
            this.threeDeeChart12RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11RadGroupBox)).BeginInit();
            this.threeDeeChart11RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6RadGroupBox)).BeginInit();
            this.threeDeeChart6RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7RadGroupBox)).BeginInit();
            this.threeDeeChart7RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8RadGroupBox)).BeginInit();
            this.threeDeeChart8RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9RadGroupBox)).BeginInit();
            this.threeDeeChart9RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10RadGroupBox)).BeginInit();
            this.threeDeeChart10RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5RadGroupBox)).BeginInit();
            this.threeDeeChart5RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4RadGroupBox)).BeginInit();
            this.threeDeeChart4RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3RadGroupBox)).BeginInit();
            this.threeDeeChart3RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2RadGroupBox)).BeginInit();
            this.threeDeeChart2RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1RadGroupBox)).BeginInit();
            this.threeDeeChart1RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChartLayoutRadGroupBox)).BeginInit();
            this.threeDeeChartLayoutRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeFifteenByOneRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeEightByTwoRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeFiveByThreeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeThreeByFiveRadRadioButton)).BeginInit();
            this.prpddPolarRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddPolarTabValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddPolarTabTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarRadPanel)).BeginInit();
            this.prpddPolarRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15RadGroupBox)).BeginInit();
            this.prpddPolarChart15RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14RadGroupBox)).BeginInit();
            this.prpddPolarChart14RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13RadGroupBox)).BeginInit();
            this.prpddPolarChart13RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12RadGroupBox)).BeginInit();
            this.prpddPolarChart12RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11RadGroupBox)).BeginInit();
            this.prpddPolarChart11RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10RadGroupBox)).BeginInit();
            this.prpddPolarChart10RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9RadGroupBox)).BeginInit();
            this.prpddPolarChart9RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8RadGroupBox)).BeginInit();
            this.prpddPolarChart8RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7RadGroupBox)).BeginInit();
            this.prpddPolarChart7RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6RadGroupBox)).BeginInit();
            this.prpddPolarChart6RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5RadGroupBox)).BeginInit();
            this.prpddPolarChart5RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4RadGroupBox)).BeginInit();
            this.prpddPolarChart4RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3RadGroupBox)).BeginInit();
            this.prpddPolarChart3RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2RadGroupBox)).BeginInit();
            this.prpddPolarChart2RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1RadGroupBox)).BeginInit();
            this.prpddPolarChart1RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarDisplaySettingsRadGroupBox)).BeginInit();
            this.prpddPolarDisplaySettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showRadialLabelsRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarInitialPhaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarAmplitudeRadGroupBox)).BeginInit();
            this.prpddPolarAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarPcRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarVRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarDbRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarScaleRadGroupBox)).BeginInit();
            this.prpddPolarScaleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarScaleRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChartLayoutRadGroupBox)).BeginInit();
            this.prpddPolarChartLayoutRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarFifteenByOneRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarEightByTwoRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarFiveByThreeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarThreeByFiveRadRadioButton)).BeginInit();
            this.matrixRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddTabValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddTabTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixRadPanel)).BeginInit();
            this.matrixRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15RadGroupBox)).BeginInit();
            this.matrixChart15RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14RadGroupBox)).BeginInit();
            this.matrixChart14RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13RadGroupBox)).BeginInit();
            this.matrixChart13RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12RadGroupBox)).BeginInit();
            this.matrixChart12RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11RadGroupBox)).BeginInit();
            this.matrixChart11RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10RadGroupBox)).BeginInit();
            this.matrixChart10RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9RadGroupBox)).BeginInit();
            this.matrixChart9RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8RadGroupBox)).BeginInit();
            this.matrixChart8RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7RadGroupBox)).BeginInit();
            this.matrixChart7RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6RadGroupBox)).BeginInit();
            this.matrixChart6RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5RadGroupBox)).BeginInit();
            this.matrixChart5RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4RadGroupBox)).BeginInit();
            this.matrixChart4RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3RadGroupBox)).BeginInit();
            this.matrixChart3RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2RadGroupBox)).BeginInit();
            this.matrixChart2RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1RadGroupBox)).BeginInit();
            this.matrixChart1RadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1PDIValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1PhaseShiftRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixDisplaySettingsRadGroupBox)).BeginInit();
            this.matrixDisplaySettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixInitialPhaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixAmplitudeRadGroupBox)).BeginInit();
            this.matrixAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixPcRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixVRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixDbRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixScaleRadGroupBox)).BeginInit();
            this.matrixScaleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixScaleRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChartLayoutRadGroupBox)).BeginInit();
            this.matrixChartLayoutRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixFifteenByOneRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixEightByTwoRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixFiveByThreeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixThreeByFiveRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataRadPageView)).BeginInit();
            this.dataRadPageView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // phdRadPageViewPage
            // 
            this.phdRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phdRadPageViewPage.Controls.Add(this.dataReadingDatePhdTabValueRadLabel);
            this.phdRadPageViewPage.Controls.Add(this.dataReadingDatePhdTabTextRadLabel);
            this.phdRadPageViewPage.Controls.Add(this.phdDisplaySettingsRadGroupBox);
            this.phdRadPageViewPage.Controls.Add(this.phdChartLayoutRadGroupBox);
            this.phdRadPageViewPage.Controls.Add(this.phdRadPanel);
            this.phdRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.phdRadPageViewPage.Name = "phdRadPageViewPage";
            this.phdRadPageViewPage.Size = new System.Drawing.Size(1004, 722);
            this.phdRadPageViewPage.Text = "PHD";
            // 
            // dataReadingDatePhdTabValueRadLabel
            // 
            this.dataReadingDatePhdTabValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePhdTabValueRadLabel.Location = new System.Drawing.Point(521, 580);
            this.dataReadingDatePhdTabValueRadLabel.Name = "dataReadingDatePhdTabValueRadLabel";
            this.dataReadingDatePhdTabValueRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePhdTabValueRadLabel.TabIndex = 96;
            this.dataReadingDatePhdTabValueRadLabel.Text = "Data Reading Date:";
            // 
            // dataReadingDatePhdTabTextRadLabel
            // 
            this.dataReadingDatePhdTabTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePhdTabTextRadLabel.Location = new System.Drawing.Point(410, 580);
            this.dataReadingDatePhdTabTextRadLabel.Name = "dataReadingDatePhdTabTextRadLabel";
            this.dataReadingDatePhdTabTextRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePhdTabTextRadLabel.TabIndex = 86;
            this.dataReadingDatePhdTabTextRadLabel.Text = "Data Reading Date:";
            // 
            // phdDisplaySettingsRadGroupBox
            // 
            this.phdDisplaySettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdDisplaySettingsRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.phdDisplaySettingsRadGroupBox.Controls.Add(this.phdInitialPhaseRadButton);
            this.phdDisplaySettingsRadGroupBox.Controls.Add(this.phdAmplitudeRadGroupBox);
            this.phdDisplaySettingsRadGroupBox.Controls.Add(this.phdScaleRadGroupBox);
            this.phdDisplaySettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdDisplaySettingsRadGroupBox.FooterImageIndex = -1;
            this.phdDisplaySettingsRadGroupBox.FooterImageKey = "";
            this.phdDisplaySettingsRadGroupBox.HeaderImageIndex = -1;
            this.phdDisplaySettingsRadGroupBox.HeaderImageKey = "";
            this.phdDisplaySettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdDisplaySettingsRadGroupBox.HeaderText = "Display Settings";
            this.phdDisplaySettingsRadGroupBox.Location = new System.Drawing.Point(125, 573);
            this.phdDisplaySettingsRadGroupBox.Name = "phdDisplaySettingsRadGroupBox";
            this.phdDisplaySettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdDisplaySettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdDisplaySettingsRadGroupBox.Size = new System.Drawing.Size(265, 140);
            this.phdDisplaySettingsRadGroupBox.TabIndex = 85;
            this.phdDisplaySettingsRadGroupBox.Text = "Display Settings";
            this.phdDisplaySettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdInitialPhaseRadButton
            // 
            this.phdInitialPhaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdInitialPhaseRadButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.phdInitialPhaseRadButton.Location = new System.Drawing.Point(14, 93);
            this.phdInitialPhaseRadButton.Name = "phdInitialPhaseRadButton";
            this.phdInitialPhaseRadButton.Size = new System.Drawing.Size(158, 40);
            this.phdInitialPhaseRadButton.TabIndex = 2;
            this.phdInitialPhaseRadButton.Text = "Common Phase Shift";
            this.phdInitialPhaseRadButton.ThemeName = "Office2007Black";
            this.phdInitialPhaseRadButton.Click += new System.EventHandler(this.phdInitialPhaseRadButton_Click);
            // 
            // phdAmplitudeRadGroupBox
            // 
            this.phdAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdAmplitudeRadGroupBox.Controls.Add(this.phdPcRadRadioButton);
            this.phdAmplitudeRadGroupBox.Controls.Add(this.phdVRadRadioButton);
            this.phdAmplitudeRadGroupBox.Controls.Add(this.phdDbRadRadioButton);
            this.phdAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdAmplitudeRadGroupBox.FooterImageIndex = -1;
            this.phdAmplitudeRadGroupBox.FooterImageKey = "";
            this.phdAmplitudeRadGroupBox.HeaderImageIndex = -1;
            this.phdAmplitudeRadGroupBox.HeaderImageKey = "";
            this.phdAmplitudeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdAmplitudeRadGroupBox.HeaderText = "Amplitude";
            this.phdAmplitudeRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.phdAmplitudeRadGroupBox.Location = new System.Drawing.Point(178, 23);
            this.phdAmplitudeRadGroupBox.Name = "phdAmplitudeRadGroupBox";
            this.phdAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdAmplitudeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdAmplitudeRadGroupBox.Size = new System.Drawing.Size(78, 110);
            this.phdAmplitudeRadGroupBox.TabIndex = 1;
            this.phdAmplitudeRadGroupBox.Text = "Amplitude";
            this.phdAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdPcRadRadioButton
            // 
            this.phdPcRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdPcRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdPcRadRadioButton.Location = new System.Drawing.Point(14, 79);
            this.phdPcRadRadioButton.Name = "phdPcRadRadioButton";
            this.phdPcRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.phdPcRadRadioButton.TabIndex = 1;
            this.phdPcRadRadioButton.Text = "pC";
            // 
            // phdVRadRadioButton
            // 
            this.phdVRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdVRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdVRadRadioButton.Location = new System.Drawing.Point(14, 51);
            this.phdVRadRadioButton.Name = "phdVRadRadioButton";
            this.phdVRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.phdVRadRadioButton.TabIndex = 1;
            this.phdVRadRadioButton.TabStop = true;
            this.phdVRadRadioButton.Text = "V";
            this.phdVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.phdVRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdVRadRadioButton_ToggleStateChanged);
            // 
            // phdDbRadRadioButton
            // 
            this.phdDbRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdDbRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdDbRadRadioButton.Location = new System.Drawing.Point(14, 27);
            this.phdDbRadRadioButton.Name = "phdDbRadRadioButton";
            this.phdDbRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.phdDbRadRadioButton.TabIndex = 0;
            this.phdDbRadRadioButton.Text = "dB";
            this.phdDbRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdDbRadRadioButton_ToggleStateChanged);
            // 
            // phdScaleRadGroupBox
            // 
            this.phdScaleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdScaleRadGroupBox.Controls.Add(this.phdScaleRadDropDownList);
            this.phdScaleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdScaleRadGroupBox.FooterImageIndex = -1;
            this.phdScaleRadGroupBox.FooterImageKey = "";
            this.phdScaleRadGroupBox.HeaderImageIndex = -1;
            this.phdScaleRadGroupBox.HeaderImageKey = "";
            this.phdScaleRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdScaleRadGroupBox.HeaderText = "Scale";
            this.phdScaleRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.phdScaleRadGroupBox.Location = new System.Drawing.Point(13, 23);
            this.phdScaleRadGroupBox.Name = "phdScaleRadGroupBox";
            this.phdScaleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdScaleRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdScaleRadGroupBox.Size = new System.Drawing.Size(159, 65);
            this.phdScaleRadGroupBox.TabIndex = 0;
            this.phdScaleRadGroupBox.Text = "Scale";
            this.phdScaleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdScaleRadDropDownList
            // 
            this.phdScaleRadDropDownList.DropDownAnimationEnabled = true;
            this.phdScaleRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdScaleRadDropDownList.Location = new System.Drawing.Point(13, 27);
            this.phdScaleRadDropDownList.Name = "phdScaleRadDropDownList";
            this.phdScaleRadDropDownList.ShowImageInEditorArea = true;
            this.phdScaleRadDropDownList.Size = new System.Drawing.Size(133, 18);
            this.phdScaleRadDropDownList.TabIndex = 0;
            this.phdScaleRadDropDownList.Text = "radDropDownList1";
            this.phdScaleRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.phdScaleRadDropDownList_SelectedIndexChanged);
            // 
            // phdChartLayoutRadGroupBox
            // 
            this.phdChartLayoutRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChartLayoutRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.phdChartLayoutRadGroupBox.Controls.Add(this.phdFifteenByOneRadRadioButton);
            this.phdChartLayoutRadGroupBox.Controls.Add(this.phdEightByTwoRadRadioButton);
            this.phdChartLayoutRadGroupBox.Controls.Add(this.phdFiveByThreeRadRadioButton);
            this.phdChartLayoutRadGroupBox.Controls.Add(this.phdThreeByFiveRadRadioButton);
            this.phdChartLayoutRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChartLayoutRadGroupBox.FooterImageIndex = -1;
            this.phdChartLayoutRadGroupBox.FooterImageKey = "";
            this.phdChartLayoutRadGroupBox.HeaderImageIndex = -1;
            this.phdChartLayoutRadGroupBox.HeaderImageKey = "";
            this.phdChartLayoutRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChartLayoutRadGroupBox.HeaderText = "Chart Layout";
            this.phdChartLayoutRadGroupBox.Location = new System.Drawing.Point(4, 573);
            this.phdChartLayoutRadGroupBox.Name = "phdChartLayoutRadGroupBox";
            this.phdChartLayoutRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChartLayoutRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChartLayoutRadGroupBox.Size = new System.Drawing.Size(115, 68);
            this.phdChartLayoutRadGroupBox.TabIndex = 84;
            this.phdChartLayoutRadGroupBox.Text = "Chart Layout";
            this.phdChartLayoutRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdFifteenByOneRadRadioButton
            // 
            this.phdFifteenByOneRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdFifteenByOneRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdFifteenByOneRadRadioButton.Location = new System.Drawing.Point(60, 45);
            this.phdFifteenByOneRadRadioButton.Name = "phdFifteenByOneRadRadioButton";
            this.phdFifteenByOneRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.phdFifteenByOneRadRadioButton.TabIndex = 3;
            this.phdFifteenByOneRadRadioButton.Text = "15x1";
            this.phdFifteenByOneRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdFifteenByOneRadRadioButton_ToggleStateChanged);
            // 
            // phdEightByTwoRadRadioButton
            // 
            this.phdEightByTwoRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdEightByTwoRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdEightByTwoRadRadioButton.Location = new System.Drawing.Point(60, 24);
            this.phdEightByTwoRadRadioButton.Name = "phdEightByTwoRadRadioButton";
            this.phdEightByTwoRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.phdEightByTwoRadRadioButton.TabIndex = 2;
            this.phdEightByTwoRadRadioButton.Text = "8x2";
            this.phdEightByTwoRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdEightByTwoRadRadioButton_ToggleStateChanged);
            // 
            // phdFiveByThreeRadRadioButton
            // 
            this.phdFiveByThreeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdFiveByThreeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdFiveByThreeRadRadioButton.Location = new System.Drawing.Point(10, 45);
            this.phdFiveByThreeRadRadioButton.Name = "phdFiveByThreeRadRadioButton";
            this.phdFiveByThreeRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.phdFiveByThreeRadRadioButton.TabIndex = 1;
            this.phdFiveByThreeRadRadioButton.Text = "5x3";
            this.phdFiveByThreeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdFiveByThreeRadRadioButton_ToggleStateChanged);
            // 
            // phdThreeByFiveRadRadioButton
            // 
            this.phdThreeByFiveRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdThreeByFiveRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdThreeByFiveRadRadioButton.Location = new System.Drawing.Point(10, 24);
            this.phdThreeByFiveRadRadioButton.Name = "phdThreeByFiveRadRadioButton";
            this.phdThreeByFiveRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.phdThreeByFiveRadRadioButton.TabIndex = 0;
            this.phdThreeByFiveRadRadioButton.TabStop = true;
            this.phdThreeByFiveRadRadioButton.Text = "3x5";
            this.phdThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.phdThreeByFiveRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.phdThreeByFiveRadRadioButton_ToggleStateChanged);
            // 
            // phdRadPanel
            // 
            this.phdRadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phdRadPanel.AutoScroll = true;
            this.phdRadPanel.Controls.Add(this.phdChart15RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart14RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart13RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart12RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart11RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart10RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart9RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart8RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart7RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart6RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart5RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart4RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart3RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart2RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart1RadGroupBox);
            this.phdRadPanel.Controls.Add(this.phdChart15WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart14WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart13WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart12WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart10WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart9WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart8WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart7WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart5WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart4WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart3WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart2WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart11WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart6WinChartViewer);
            this.phdRadPanel.Controls.Add(this.phdChart1WinChartViewer);
            this.phdRadPanel.Location = new System.Drawing.Point(0, 0);
            this.phdRadPanel.Name = "phdRadPanel";
            this.phdRadPanel.Size = new System.Drawing.Size(1001, 567);
            this.phdRadPanel.TabIndex = 24;
            this.phdRadPanel.Text = "PRPDD";
            // 
            // phdChart15RadGroupBox
            // 
            this.phdChart15RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart15RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart15RadGroupBox.Controls.Add(this.phdChart15PDIValueRadLabel);
            this.phdChart15RadGroupBox.Controls.Add(this.radLabel6);
            this.phdChart15RadGroupBox.FooterImageIndex = -1;
            this.phdChart15RadGroupBox.FooterImageKey = "";
            this.phdChart15RadGroupBox.HeaderImageIndex = -1;
            this.phdChart15RadGroupBox.HeaderImageKey = "";
            this.phdChart15RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart15RadGroupBox.HeaderText = "";
            this.phdChart15RadGroupBox.Location = new System.Drawing.Point(783, 514);
            this.phdChart15RadGroupBox.Name = "phdChart15RadGroupBox";
            this.phdChart15RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart15RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart15RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart15RadGroupBox.TabIndex = 54;
            this.phdChart15RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart15PDIValueRadLabel
            // 
            this.phdChart15PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart15PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart15PDIValueRadLabel.Name = "phdChart15PDIValueRadLabel";
            this.phdChart15PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart15PDIValueRadLabel.TabIndex = 3;
            this.phdChart15PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(1, 5);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(32, 16);
            this.radLabel6.TabIndex = 2;
            this.radLabel6.Text = "PDI=";
            // 
            // phdChart14RadGroupBox
            // 
            this.phdChart14RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart14RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart14RadGroupBox.Controls.Add(this.phdChart14PDIValueRadLabel);
            this.phdChart14RadGroupBox.Controls.Add(this.radLabel15);
            this.phdChart14RadGroupBox.FooterImageIndex = -1;
            this.phdChart14RadGroupBox.FooterImageKey = "";
            this.phdChart14RadGroupBox.HeaderImageIndex = -1;
            this.phdChart14RadGroupBox.HeaderImageKey = "";
            this.phdChart14RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart14RadGroupBox.HeaderText = "";
            this.phdChart14RadGroupBox.Location = new System.Drawing.Point(588, 514);
            this.phdChart14RadGroupBox.Name = "phdChart14RadGroupBox";
            this.phdChart14RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart14RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart14RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart14RadGroupBox.TabIndex = 53;
            this.phdChart14RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart14PDIValueRadLabel
            // 
            this.phdChart14PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart14PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart14PDIValueRadLabel.Name = "phdChart14PDIValueRadLabel";
            this.phdChart14PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart14PDIValueRadLabel.TabIndex = 3;
            this.phdChart14PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel15
            // 
            this.radLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel15.Location = new System.Drawing.Point(1, 5);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(32, 16);
            this.radLabel15.TabIndex = 2;
            this.radLabel15.Text = "PDI=";
            // 
            // phdChart13RadGroupBox
            // 
            this.phdChart13RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart13RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart13RadGroupBox.Controls.Add(this.phdChart13PDIValueRadLabel);
            this.phdChart13RadGroupBox.Controls.Add(this.radLabel24);
            this.phdChart13RadGroupBox.FooterImageIndex = -1;
            this.phdChart13RadGroupBox.FooterImageKey = "";
            this.phdChart13RadGroupBox.HeaderImageIndex = -1;
            this.phdChart13RadGroupBox.HeaderImageKey = "";
            this.phdChart13RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart13RadGroupBox.HeaderText = "";
            this.phdChart13RadGroupBox.Location = new System.Drawing.Point(393, 514);
            this.phdChart13RadGroupBox.Name = "phdChart13RadGroupBox";
            this.phdChart13RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart13RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart13RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart13RadGroupBox.TabIndex = 52;
            this.phdChart13RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart13PDIValueRadLabel
            // 
            this.phdChart13PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart13PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart13PDIValueRadLabel.Name = "phdChart13PDIValueRadLabel";
            this.phdChart13PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart13PDIValueRadLabel.TabIndex = 3;
            this.phdChart13PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel24
            // 
            this.radLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel24.Location = new System.Drawing.Point(1, 5);
            this.radLabel24.Name = "radLabel24";
            this.radLabel24.Size = new System.Drawing.Size(32, 16);
            this.radLabel24.TabIndex = 2;
            this.radLabel24.Text = "PDI=";
            // 
            // phdChart12RadGroupBox
            // 
            this.phdChart12RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart12RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart12RadGroupBox.Controls.Add(this.phdChart12PDIValueRadLabel);
            this.phdChart12RadGroupBox.Controls.Add(this.radLabel33);
            this.phdChart12RadGroupBox.FooterImageIndex = -1;
            this.phdChart12RadGroupBox.FooterImageKey = "";
            this.phdChart12RadGroupBox.HeaderImageIndex = -1;
            this.phdChart12RadGroupBox.HeaderImageKey = "";
            this.phdChart12RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart12RadGroupBox.HeaderText = "";
            this.phdChart12RadGroupBox.Location = new System.Drawing.Point(198, 514);
            this.phdChart12RadGroupBox.Name = "phdChart12RadGroupBox";
            this.phdChart12RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart12RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart12RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart12RadGroupBox.TabIndex = 51;
            this.phdChart12RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart12PDIValueRadLabel
            // 
            this.phdChart12PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart12PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart12PDIValueRadLabel.Name = "phdChart12PDIValueRadLabel";
            this.phdChart12PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart12PDIValueRadLabel.TabIndex = 3;
            this.phdChart12PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel33
            // 
            this.radLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel33.Location = new System.Drawing.Point(1, 5);
            this.radLabel33.Name = "radLabel33";
            this.radLabel33.Size = new System.Drawing.Size(32, 16);
            this.radLabel33.TabIndex = 2;
            this.radLabel33.Text = "PDI=";
            // 
            // phdChart11RadGroupBox
            // 
            this.phdChart11RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart11RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart11RadGroupBox.Controls.Add(this.phdChart11PDIValueRadLabel);
            this.phdChart11RadGroupBox.Controls.Add(this.radLabel42);
            this.phdChart11RadGroupBox.FooterImageIndex = -1;
            this.phdChart11RadGroupBox.FooterImageKey = "";
            this.phdChart11RadGroupBox.HeaderImageIndex = -1;
            this.phdChart11RadGroupBox.HeaderImageKey = "";
            this.phdChart11RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart11RadGroupBox.HeaderText = "";
            this.phdChart11RadGroupBox.Location = new System.Drawing.Point(3, 514);
            this.phdChart11RadGroupBox.Name = "phdChart11RadGroupBox";
            this.phdChart11RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart11RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart11RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart11RadGroupBox.TabIndex = 50;
            this.phdChart11RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart11PDIValueRadLabel
            // 
            this.phdChart11PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart11PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart11PDIValueRadLabel.Name = "phdChart11PDIValueRadLabel";
            this.phdChart11PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart11PDIValueRadLabel.TabIndex = 3;
            this.phdChart11PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel42
            // 
            this.radLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel42.Location = new System.Drawing.Point(1, 5);
            this.radLabel42.Name = "radLabel42";
            this.radLabel42.Size = new System.Drawing.Size(32, 16);
            this.radLabel42.TabIndex = 2;
            this.radLabel42.Text = "PDI=";
            // 
            // phdChart10RadGroupBox
            // 
            this.phdChart10RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart10RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart10RadGroupBox.Controls.Add(this.phdChart10PDIValueRadLabel);
            this.phdChart10RadGroupBox.Controls.Add(this.radLabel47);
            this.phdChart10RadGroupBox.FooterImageIndex = -1;
            this.phdChart10RadGroupBox.FooterImageKey = "";
            this.phdChart10RadGroupBox.HeaderImageIndex = -1;
            this.phdChart10RadGroupBox.HeaderImageKey = "";
            this.phdChart10RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart10RadGroupBox.HeaderText = "";
            this.phdChart10RadGroupBox.Location = new System.Drawing.Point(783, 334);
            this.phdChart10RadGroupBox.Name = "phdChart10RadGroupBox";
            this.phdChart10RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart10RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart10RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart10RadGroupBox.TabIndex = 49;
            this.phdChart10RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart10PDIValueRadLabel
            // 
            this.phdChart10PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart10PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart10PDIValueRadLabel.Name = "phdChart10PDIValueRadLabel";
            this.phdChart10PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart10PDIValueRadLabel.TabIndex = 3;
            this.phdChart10PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel47
            // 
            this.radLabel47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel47.Location = new System.Drawing.Point(1, 5);
            this.radLabel47.Name = "radLabel47";
            this.radLabel47.Size = new System.Drawing.Size(32, 16);
            this.radLabel47.TabIndex = 2;
            this.radLabel47.Text = "PDI=";
            // 
            // phdChart9RadGroupBox
            // 
            this.phdChart9RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart9RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart9RadGroupBox.Controls.Add(this.phdChart9PDIValueRadLabel);
            this.phdChart9RadGroupBox.Controls.Add(this.radLabel50);
            this.phdChart9RadGroupBox.FooterImageIndex = -1;
            this.phdChart9RadGroupBox.FooterImageKey = "";
            this.phdChart9RadGroupBox.HeaderImageIndex = -1;
            this.phdChart9RadGroupBox.HeaderImageKey = "";
            this.phdChart9RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart9RadGroupBox.HeaderText = "";
            this.phdChart9RadGroupBox.Location = new System.Drawing.Point(588, 334);
            this.phdChart9RadGroupBox.Name = "phdChart9RadGroupBox";
            this.phdChart9RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart9RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart9RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart9RadGroupBox.TabIndex = 48;
            this.phdChart9RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart9PDIValueRadLabel
            // 
            this.phdChart9PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart9PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart9PDIValueRadLabel.Name = "phdChart9PDIValueRadLabel";
            this.phdChart9PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart9PDIValueRadLabel.TabIndex = 3;
            this.phdChart9PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel50
            // 
            this.radLabel50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel50.Location = new System.Drawing.Point(1, 5);
            this.radLabel50.Name = "radLabel50";
            this.radLabel50.Size = new System.Drawing.Size(32, 16);
            this.radLabel50.TabIndex = 2;
            this.radLabel50.Text = "PDI=";
            // 
            // phdChart8RadGroupBox
            // 
            this.phdChart8RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart8RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart8RadGroupBox.Controls.Add(this.phdChart8PDIValueRadLabel);
            this.phdChart8RadGroupBox.Controls.Add(this.radLabel53);
            this.phdChart8RadGroupBox.FooterImageIndex = -1;
            this.phdChart8RadGroupBox.FooterImageKey = "";
            this.phdChart8RadGroupBox.HeaderImageIndex = -1;
            this.phdChart8RadGroupBox.HeaderImageKey = "";
            this.phdChart8RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart8RadGroupBox.HeaderText = "";
            this.phdChart8RadGroupBox.Location = new System.Drawing.Point(393, 334);
            this.phdChart8RadGroupBox.Name = "phdChart8RadGroupBox";
            this.phdChart8RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart8RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart8RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart8RadGroupBox.TabIndex = 47;
            this.phdChart8RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart8PDIValueRadLabel
            // 
            this.phdChart8PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart8PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart8PDIValueRadLabel.Name = "phdChart8PDIValueRadLabel";
            this.phdChart8PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart8PDIValueRadLabel.TabIndex = 3;
            this.phdChart8PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel53
            // 
            this.radLabel53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel53.Location = new System.Drawing.Point(1, 5);
            this.radLabel53.Name = "radLabel53";
            this.radLabel53.Size = new System.Drawing.Size(32, 16);
            this.radLabel53.TabIndex = 2;
            this.radLabel53.Text = "PDI=";
            // 
            // phdChart7RadGroupBox
            // 
            this.phdChart7RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart7RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart7RadGroupBox.Controls.Add(this.phdChart7PDIValueRadLabel);
            this.phdChart7RadGroupBox.Controls.Add(this.radLabel56);
            this.phdChart7RadGroupBox.FooterImageIndex = -1;
            this.phdChart7RadGroupBox.FooterImageKey = "";
            this.phdChart7RadGroupBox.HeaderImageIndex = -1;
            this.phdChart7RadGroupBox.HeaderImageKey = "";
            this.phdChart7RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart7RadGroupBox.HeaderText = "";
            this.phdChart7RadGroupBox.Location = new System.Drawing.Point(198, 334);
            this.phdChart7RadGroupBox.Name = "phdChart7RadGroupBox";
            this.phdChart7RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart7RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart7RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart7RadGroupBox.TabIndex = 46;
            this.phdChart7RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart7PDIValueRadLabel
            // 
            this.phdChart7PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart7PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart7PDIValueRadLabel.Name = "phdChart7PDIValueRadLabel";
            this.phdChart7PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart7PDIValueRadLabel.TabIndex = 3;
            this.phdChart7PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel56
            // 
            this.radLabel56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel56.Location = new System.Drawing.Point(1, 5);
            this.radLabel56.Name = "radLabel56";
            this.radLabel56.Size = new System.Drawing.Size(32, 16);
            this.radLabel56.TabIndex = 2;
            this.radLabel56.Text = "PDI=";
            // 
            // phdChart6RadGroupBox
            // 
            this.phdChart6RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart6RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart6RadGroupBox.Controls.Add(this.phdChart6PDIValueRadLabel);
            this.phdChart6RadGroupBox.Controls.Add(this.radLabel59);
            this.phdChart6RadGroupBox.FooterImageIndex = -1;
            this.phdChart6RadGroupBox.FooterImageKey = "";
            this.phdChart6RadGroupBox.HeaderImageIndex = -1;
            this.phdChart6RadGroupBox.HeaderImageKey = "";
            this.phdChart6RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart6RadGroupBox.HeaderText = "";
            this.phdChart6RadGroupBox.Location = new System.Drawing.Point(3, 334);
            this.phdChart6RadGroupBox.Name = "phdChart6RadGroupBox";
            this.phdChart6RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart6RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart6RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart6RadGroupBox.TabIndex = 45;
            this.phdChart6RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart6PDIValueRadLabel
            // 
            this.phdChart6PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart6PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart6PDIValueRadLabel.Name = "phdChart6PDIValueRadLabel";
            this.phdChart6PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart6PDIValueRadLabel.TabIndex = 3;
            this.phdChart6PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel59
            // 
            this.radLabel59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel59.Location = new System.Drawing.Point(1, 5);
            this.radLabel59.Name = "radLabel59";
            this.radLabel59.Size = new System.Drawing.Size(32, 16);
            this.radLabel59.TabIndex = 2;
            this.radLabel59.Text = "PDI=";
            // 
            // phdChart5RadGroupBox
            // 
            this.phdChart5RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart5RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart5RadGroupBox.Controls.Add(this.phdChart5PDIValueRadLabel);
            this.phdChart5RadGroupBox.Controls.Add(this.radLabel62);
            this.phdChart5RadGroupBox.FooterImageIndex = -1;
            this.phdChart5RadGroupBox.FooterImageKey = "";
            this.phdChart5RadGroupBox.HeaderImageIndex = -1;
            this.phdChart5RadGroupBox.HeaderImageKey = "";
            this.phdChart5RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart5RadGroupBox.HeaderText = "";
            this.phdChart5RadGroupBox.Location = new System.Drawing.Point(783, 154);
            this.phdChart5RadGroupBox.Name = "phdChart5RadGroupBox";
            this.phdChart5RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart5RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart5RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart5RadGroupBox.TabIndex = 44;
            this.phdChart5RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart5PDIValueRadLabel
            // 
            this.phdChart5PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart5PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart5PDIValueRadLabel.Name = "phdChart5PDIValueRadLabel";
            this.phdChart5PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart5PDIValueRadLabel.TabIndex = 3;
            this.phdChart5PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel62
            // 
            this.radLabel62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel62.Location = new System.Drawing.Point(1, 5);
            this.radLabel62.Name = "radLabel62";
            this.radLabel62.Size = new System.Drawing.Size(32, 16);
            this.radLabel62.TabIndex = 2;
            this.radLabel62.Text = "PDI=";
            // 
            // phdChart4RadGroupBox
            // 
            this.phdChart4RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart4RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart4RadGroupBox.Controls.Add(this.phdChart4PDIValueRadLabel);
            this.phdChart4RadGroupBox.Controls.Add(this.radLabel65);
            this.phdChart4RadGroupBox.FooterImageIndex = -1;
            this.phdChart4RadGroupBox.FooterImageKey = "";
            this.phdChart4RadGroupBox.HeaderImageIndex = -1;
            this.phdChart4RadGroupBox.HeaderImageKey = "";
            this.phdChart4RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart4RadGroupBox.HeaderText = "";
            this.phdChart4RadGroupBox.Location = new System.Drawing.Point(588, 154);
            this.phdChart4RadGroupBox.Name = "phdChart4RadGroupBox";
            this.phdChart4RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart4RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart4RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart4RadGroupBox.TabIndex = 43;
            this.phdChart4RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart4PDIValueRadLabel
            // 
            this.phdChart4PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart4PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart4PDIValueRadLabel.Name = "phdChart4PDIValueRadLabel";
            this.phdChart4PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart4PDIValueRadLabel.TabIndex = 3;
            this.phdChart4PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel65
            // 
            this.radLabel65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel65.Location = new System.Drawing.Point(1, 5);
            this.radLabel65.Name = "radLabel65";
            this.radLabel65.Size = new System.Drawing.Size(32, 16);
            this.radLabel65.TabIndex = 2;
            this.radLabel65.Text = "PDI=";
            // 
            // phdChart3RadGroupBox
            // 
            this.phdChart3RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart3RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart3RadGroupBox.Controls.Add(this.phdChart3PDIValueRadLabel);
            this.phdChart3RadGroupBox.Controls.Add(this.radLabel68);
            this.phdChart3RadGroupBox.FooterImageIndex = -1;
            this.phdChart3RadGroupBox.FooterImageKey = "";
            this.phdChart3RadGroupBox.HeaderImageIndex = -1;
            this.phdChart3RadGroupBox.HeaderImageKey = "";
            this.phdChart3RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart3RadGroupBox.HeaderText = "";
            this.phdChart3RadGroupBox.Location = new System.Drawing.Point(393, 154);
            this.phdChart3RadGroupBox.Name = "phdChart3RadGroupBox";
            this.phdChart3RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart3RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart3RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart3RadGroupBox.TabIndex = 42;
            this.phdChart3RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart3PDIValueRadLabel
            // 
            this.phdChart3PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart3PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart3PDIValueRadLabel.Name = "phdChart3PDIValueRadLabel";
            this.phdChart3PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart3PDIValueRadLabel.TabIndex = 3;
            this.phdChart3PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel68
            // 
            this.radLabel68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel68.Location = new System.Drawing.Point(1, 5);
            this.radLabel68.Name = "radLabel68";
            this.radLabel68.Size = new System.Drawing.Size(32, 16);
            this.radLabel68.TabIndex = 2;
            this.radLabel68.Text = "PDI=";
            // 
            // phdChart2RadGroupBox
            // 
            this.phdChart2RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart2RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart2RadGroupBox.Controls.Add(this.phdChart2PDIValueRadLabel);
            this.phdChart2RadGroupBox.Controls.Add(this.radLabel71);
            this.phdChart2RadGroupBox.FooterImageIndex = -1;
            this.phdChart2RadGroupBox.FooterImageKey = "";
            this.phdChart2RadGroupBox.HeaderImageIndex = -1;
            this.phdChart2RadGroupBox.HeaderImageKey = "";
            this.phdChart2RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart2RadGroupBox.HeaderText = "";
            this.phdChart2RadGroupBox.Location = new System.Drawing.Point(198, 154);
            this.phdChart2RadGroupBox.Name = "phdChart2RadGroupBox";
            this.phdChart2RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart2RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart2RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart2RadGroupBox.TabIndex = 41;
            this.phdChart2RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart2PDIValueRadLabel
            // 
            this.phdChart2PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart2PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart2PDIValueRadLabel.Name = "phdChart2PDIValueRadLabel";
            this.phdChart2PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart2PDIValueRadLabel.TabIndex = 3;
            this.phdChart2PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel71
            // 
            this.radLabel71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel71.Location = new System.Drawing.Point(1, 5);
            this.radLabel71.Name = "radLabel71";
            this.radLabel71.Size = new System.Drawing.Size(32, 16);
            this.radLabel71.TabIndex = 2;
            this.radLabel71.Text = "PDI=";
            // 
            // phdChart1RadGroupBox
            // 
            this.phdChart1RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phdChart1RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.phdChart1RadGroupBox.Controls.Add(this.phdChart1PDIValueRadLabel);
            this.phdChart1RadGroupBox.Controls.Add(this.radLabel74);
            this.phdChart1RadGroupBox.FooterImageIndex = -1;
            this.phdChart1RadGroupBox.FooterImageKey = "";
            this.phdChart1RadGroupBox.HeaderImageIndex = -1;
            this.phdChart1RadGroupBox.HeaderImageKey = "";
            this.phdChart1RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phdChart1RadGroupBox.HeaderText = "";
            this.phdChart1RadGroupBox.Location = new System.Drawing.Point(3, 154);
            this.phdChart1RadGroupBox.Name = "phdChart1RadGroupBox";
            this.phdChart1RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phdChart1RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phdChart1RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.phdChart1RadGroupBox.TabIndex = 40;
            this.phdChart1RadGroupBox.ThemeName = "Office2007Black";
            // 
            // phdChart1PDIValueRadLabel
            // 
            this.phdChart1PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phdChart1PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.phdChart1PDIValueRadLabel.Name = "phdChart1PDIValueRadLabel";
            this.phdChart1PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.phdChart1PDIValueRadLabel.TabIndex = 3;
            this.phdChart1PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel74
            // 
            this.radLabel74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel74.Location = new System.Drawing.Point(1, 5);
            this.radLabel74.Name = "radLabel74";
            this.radLabel74.Size = new System.Drawing.Size(32, 16);
            this.radLabel74.TabIndex = 2;
            this.radLabel74.Text = "PDI=";
            // 
            // phdChart15WinChartViewer
            // 
            this.phdChart15WinChartViewer.Location = new System.Drawing.Point(783, 363);
            this.phdChart15WinChartViewer.Name = "phdChart15WinChartViewer";
            this.phdChart15WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart15WinChartViewer.TabIndex = 24;
            this.phdChart15WinChartViewer.TabStop = false;
            this.phdChart15WinChartViewer.Click += new System.EventHandler(this.phdChart15CopyGraph_Click);
            // 
            // phdChart14WinChartViewer
            // 
            this.phdChart14WinChartViewer.Location = new System.Drawing.Point(588, 363);
            this.phdChart14WinChartViewer.Name = "phdChart14WinChartViewer";
            this.phdChart14WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart14WinChartViewer.TabIndex = 23;
            this.phdChart14WinChartViewer.TabStop = false;
            this.phdChart14WinChartViewer.Click += new System.EventHandler(this.phdChart14CopyGraph_Click);
            // 
            // phdChart13WinChartViewer
            // 
            this.phdChart13WinChartViewer.Location = new System.Drawing.Point(393, 363);
            this.phdChart13WinChartViewer.Name = "phdChart13WinChartViewer";
            this.phdChart13WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart13WinChartViewer.TabIndex = 22;
            this.phdChart13WinChartViewer.TabStop = false;
            this.phdChart13WinChartViewer.Click += new System.EventHandler(this.phdChart13CopyGraph_Click);
            // 
            // phdChart12WinChartViewer
            // 
            this.phdChart12WinChartViewer.Location = new System.Drawing.Point(198, 363);
            this.phdChart12WinChartViewer.Name = "phdChart12WinChartViewer";
            this.phdChart12WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart12WinChartViewer.TabIndex = 21;
            this.phdChart12WinChartViewer.TabStop = false;
            this.phdChart12WinChartViewer.Click += new System.EventHandler(this.phdChart12CopyGraph_Click);
            // 
            // phdChart10WinChartViewer
            // 
            this.phdChart10WinChartViewer.Location = new System.Drawing.Point(783, 183);
            this.phdChart10WinChartViewer.Name = "phdChart10WinChartViewer";
            this.phdChart10WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart10WinChartViewer.TabIndex = 20;
            this.phdChart10WinChartViewer.TabStop = false;
            this.phdChart10WinChartViewer.Click += new System.EventHandler(this.phdChart10CopyGraph_Click);
            // 
            // phdChart9WinChartViewer
            // 
            this.phdChart9WinChartViewer.Location = new System.Drawing.Point(588, 183);
            this.phdChart9WinChartViewer.Name = "phdChart9WinChartViewer";
            this.phdChart9WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart9WinChartViewer.TabIndex = 19;
            this.phdChart9WinChartViewer.TabStop = false;
            this.phdChart9WinChartViewer.Click += new System.EventHandler(this.phdChart9CopyGraph_Click);
            // 
            // phdChart8WinChartViewer
            // 
            this.phdChart8WinChartViewer.Location = new System.Drawing.Point(393, 183);
            this.phdChart8WinChartViewer.Name = "phdChart8WinChartViewer";
            this.phdChart8WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart8WinChartViewer.TabIndex = 18;
            this.phdChart8WinChartViewer.TabStop = false;
            this.phdChart8WinChartViewer.Click += new System.EventHandler(this.phdChart8CopyGraph_Click);
            // 
            // phdChart7WinChartViewer
            // 
            this.phdChart7WinChartViewer.Location = new System.Drawing.Point(198, 183);
            this.phdChart7WinChartViewer.Name = "phdChart7WinChartViewer";
            this.phdChart7WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart7WinChartViewer.TabIndex = 17;
            this.phdChart7WinChartViewer.TabStop = false;
            this.phdChart7WinChartViewer.Click += new System.EventHandler(this.phdChart7CopyGraph_Click);
            // 
            // phdChart5WinChartViewer
            // 
            this.phdChart5WinChartViewer.Location = new System.Drawing.Point(783, 3);
            this.phdChart5WinChartViewer.Name = "phdChart5WinChartViewer";
            this.phdChart5WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart5WinChartViewer.TabIndex = 15;
            this.phdChart5WinChartViewer.TabStop = false;
            this.phdChart5WinChartViewer.Click += new System.EventHandler(this.phdChart5CopyGraph_Click);
            // 
            // phdChart4WinChartViewer
            // 
            this.phdChart4WinChartViewer.Location = new System.Drawing.Point(588, 3);
            this.phdChart4WinChartViewer.Name = "phdChart4WinChartViewer";
            this.phdChart4WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart4WinChartViewer.TabIndex = 14;
            this.phdChart4WinChartViewer.TabStop = false;
            this.phdChart4WinChartViewer.Click += new System.EventHandler(this.phdChart4CopyGraph_Click);
            // 
            // phdChart3WinChartViewer
            // 
            this.phdChart3WinChartViewer.Location = new System.Drawing.Point(393, 3);
            this.phdChart3WinChartViewer.Name = "phdChart3WinChartViewer";
            this.phdChart3WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart3WinChartViewer.TabIndex = 11;
            this.phdChart3WinChartViewer.TabStop = false;
            this.phdChart3WinChartViewer.Click += new System.EventHandler(this.phdChart3CopyGraph_Click);
            // 
            // phdChart2WinChartViewer
            // 
            this.phdChart2WinChartViewer.Location = new System.Drawing.Point(198, 3);
            this.phdChart2WinChartViewer.Name = "phdChart2WinChartViewer";
            this.phdChart2WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart2WinChartViewer.TabIndex = 8;
            this.phdChart2WinChartViewer.TabStop = false;
            this.phdChart2WinChartViewer.Click += new System.EventHandler(this.phdChart2CopyGraph_Click);
            // 
            // phdChart11WinChartViewer
            // 
            this.phdChart11WinChartViewer.Location = new System.Drawing.Point(3, 363);
            this.phdChart11WinChartViewer.Name = "phdChart11WinChartViewer";
            this.phdChart11WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart11WinChartViewer.TabIndex = 7;
            this.phdChart11WinChartViewer.TabStop = false;
            this.phdChart11WinChartViewer.Click += new System.EventHandler(this.phdChart11CopyGraph_Click);
            // 
            // phdChart6WinChartViewer
            // 
            this.phdChart6WinChartViewer.Location = new System.Drawing.Point(3, 183);
            this.phdChart6WinChartViewer.Name = "phdChart6WinChartViewer";
            this.phdChart6WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart6WinChartViewer.TabIndex = 6;
            this.phdChart6WinChartViewer.TabStop = false;
            this.phdChart6WinChartViewer.Click += new System.EventHandler(this.phdChart6CopyGraph_Click);
            // 
            // phdChart1WinChartViewer
            // 
            this.phdChart1WinChartViewer.Location = new System.Drawing.Point(3, 3);
            this.phdChart1WinChartViewer.Name = "phdChart1WinChartViewer";
            this.phdChart1WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.phdChart1WinChartViewer.TabIndex = 5;
            this.phdChart1WinChartViewer.TabStop = false;
            this.phdChart1WinChartViewer.Click += new System.EventHandler(this.phdChart1CopyGraph_Click);
            // 
            // threeDeeRadPageViewPage
            // 
            this.threeDeeRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.threeDeeRadPageViewPage.Controls.Add(this.dataReadingDatePrpdd3DTabValueRadLabel);
            this.threeDeeRadPageViewPage.Controls.Add(this.dataReadingDatePrpdd3DTabTextRadLabel);
            this.threeDeeRadPageViewPage.Controls.Add(this.threeDeeDisplaySettingsRadGroupBox);
            this.threeDeeRadPageViewPage.Controls.Add(this.threeDeeRadPanel);
            this.threeDeeRadPageViewPage.Controls.Add(this.threeDeeChartLayoutRadGroupBox);
            this.threeDeeRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.threeDeeRadPageViewPage.Name = "threeDeeRadPageViewPage";
            this.threeDeeRadPageViewPage.Size = new System.Drawing.Size(1004, 722);
            this.threeDeeRadPageViewPage.Text = "PRPDD 3D";
            // 
            // dataReadingDatePrpdd3DTabValueRadLabel
            // 
            this.dataReadingDatePrpdd3DTabValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpdd3DTabValueRadLabel.Location = new System.Drawing.Point(521, 580);
            this.dataReadingDatePrpdd3DTabValueRadLabel.Name = "dataReadingDatePrpdd3DTabValueRadLabel";
            this.dataReadingDatePrpdd3DTabValueRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpdd3DTabValueRadLabel.TabIndex = 95;
            this.dataReadingDatePrpdd3DTabValueRadLabel.Text = "Data Reading Date:";
            // 
            // dataReadingDatePrpdd3DTabTextRadLabel
            // 
            this.dataReadingDatePrpdd3DTabTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpdd3DTabTextRadLabel.Location = new System.Drawing.Point(410, 580);
            this.dataReadingDatePrpdd3DTabTextRadLabel.Name = "dataReadingDatePrpdd3DTabTextRadLabel";
            this.dataReadingDatePrpdd3DTabTextRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpdd3DTabTextRadLabel.TabIndex = 86;
            this.dataReadingDatePrpdd3DTabTextRadLabel.Text = "Data Reading Date:";
            // 
            // threeDeeDisplaySettingsRadGroupBox
            // 
            this.threeDeeDisplaySettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeDisplaySettingsRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.threeDeeDisplaySettingsRadGroupBox.Controls.Add(this.threeDeeInitialPhaseRadButton);
            this.threeDeeDisplaySettingsRadGroupBox.Controls.Add(this.threeDeeAmplitudeRadGroupBox);
            this.threeDeeDisplaySettingsRadGroupBox.Controls.Add(this.threeDeeScaleRadGroupBox);
            this.threeDeeDisplaySettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeDisplaySettingsRadGroupBox.FooterImageIndex = -1;
            this.threeDeeDisplaySettingsRadGroupBox.FooterImageKey = "";
            this.threeDeeDisplaySettingsRadGroupBox.HeaderImageIndex = -1;
            this.threeDeeDisplaySettingsRadGroupBox.HeaderImageKey = "";
            this.threeDeeDisplaySettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeDisplaySettingsRadGroupBox.HeaderText = "Display Settings";
            this.threeDeeDisplaySettingsRadGroupBox.Location = new System.Drawing.Point(125, 573);
            this.threeDeeDisplaySettingsRadGroupBox.Name = "threeDeeDisplaySettingsRadGroupBox";
            this.threeDeeDisplaySettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeDisplaySettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeDisplaySettingsRadGroupBox.Size = new System.Drawing.Size(265, 140);
            this.threeDeeDisplaySettingsRadGroupBox.TabIndex = 85;
            this.threeDeeDisplaySettingsRadGroupBox.Text = "Display Settings";
            this.threeDeeDisplaySettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeInitialPhaseRadButton
            // 
            this.threeDeeInitialPhaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeInitialPhaseRadButton.Location = new System.Drawing.Point(14, 93);
            this.threeDeeInitialPhaseRadButton.Name = "threeDeeInitialPhaseRadButton";
            this.threeDeeInitialPhaseRadButton.Size = new System.Drawing.Size(158, 40);
            this.threeDeeInitialPhaseRadButton.TabIndex = 2;
            this.threeDeeInitialPhaseRadButton.Text = "Common Phase Shift";
            this.threeDeeInitialPhaseRadButton.ThemeName = "Office2007Black";
            this.threeDeeInitialPhaseRadButton.Click += new System.EventHandler(this.threeDeeInitialPhaseRadButton_Click);
            // 
            // threeDeeAmplitudeRadGroupBox
            // 
            this.threeDeeAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeAmplitudeRadGroupBox.Controls.Add(this.threeDeePcRadRadioButton);
            this.threeDeeAmplitudeRadGroupBox.Controls.Add(this.threeDeeVRadRadioButton);
            this.threeDeeAmplitudeRadGroupBox.Controls.Add(this.threeDeeDbRadRadioButton);
            this.threeDeeAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeAmplitudeRadGroupBox.FooterImageIndex = -1;
            this.threeDeeAmplitudeRadGroupBox.FooterImageKey = "";
            this.threeDeeAmplitudeRadGroupBox.HeaderImageIndex = -1;
            this.threeDeeAmplitudeRadGroupBox.HeaderImageKey = "";
            this.threeDeeAmplitudeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeAmplitudeRadGroupBox.HeaderText = "Amplitude";
            this.threeDeeAmplitudeRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.threeDeeAmplitudeRadGroupBox.Location = new System.Drawing.Point(178, 23);
            this.threeDeeAmplitudeRadGroupBox.Name = "threeDeeAmplitudeRadGroupBox";
            this.threeDeeAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeAmplitudeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeAmplitudeRadGroupBox.Size = new System.Drawing.Size(78, 110);
            this.threeDeeAmplitudeRadGroupBox.TabIndex = 1;
            this.threeDeeAmplitudeRadGroupBox.Text = "Amplitude";
            this.threeDeeAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeePcRadRadioButton
            // 
            this.threeDeePcRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeePcRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeePcRadRadioButton.Location = new System.Drawing.Point(14, 79);
            this.threeDeePcRadRadioButton.Name = "threeDeePcRadRadioButton";
            this.threeDeePcRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.threeDeePcRadRadioButton.TabIndex = 1;
            this.threeDeePcRadRadioButton.Text = "pC";
            // 
            // threeDeeVRadRadioButton
            // 
            this.threeDeeVRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeVRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeVRadRadioButton.Location = new System.Drawing.Point(14, 51);
            this.threeDeeVRadRadioButton.Name = "threeDeeVRadRadioButton";
            this.threeDeeVRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.threeDeeVRadRadioButton.TabIndex = 1;
            this.threeDeeVRadRadioButton.TabStop = true;
            this.threeDeeVRadRadioButton.Text = "V";
            this.threeDeeVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.threeDeeVRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeVRadRadioButton_ToggleStateChanged);
            // 
            // threeDeeDbRadRadioButton
            // 
            this.threeDeeDbRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeDbRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeDbRadRadioButton.Location = new System.Drawing.Point(14, 27);
            this.threeDeeDbRadRadioButton.Name = "threeDeeDbRadRadioButton";
            this.threeDeeDbRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.threeDeeDbRadRadioButton.TabIndex = 0;
            this.threeDeeDbRadRadioButton.Text = "dB";
            this.threeDeeDbRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeDbRadRadioButton_ToggleStateChanged);
            // 
            // threeDeeScaleRadGroupBox
            // 
            this.threeDeeScaleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeScaleRadGroupBox.Controls.Add(this.threeDeeScaleRadDropDownList);
            this.threeDeeScaleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeScaleRadGroupBox.FooterImageIndex = -1;
            this.threeDeeScaleRadGroupBox.FooterImageKey = "";
            this.threeDeeScaleRadGroupBox.HeaderImageIndex = -1;
            this.threeDeeScaleRadGroupBox.HeaderImageKey = "";
            this.threeDeeScaleRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeScaleRadGroupBox.HeaderText = "Scale";
            this.threeDeeScaleRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.threeDeeScaleRadGroupBox.Location = new System.Drawing.Point(13, 23);
            this.threeDeeScaleRadGroupBox.Name = "threeDeeScaleRadGroupBox";
            this.threeDeeScaleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeScaleRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeScaleRadGroupBox.Size = new System.Drawing.Size(159, 65);
            this.threeDeeScaleRadGroupBox.TabIndex = 0;
            this.threeDeeScaleRadGroupBox.Text = "Scale";
            this.threeDeeScaleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeScaleRadDropDownList
            // 
            this.threeDeeScaleRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeScaleRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeScaleRadDropDownList.Location = new System.Drawing.Point(13, 27);
            this.threeDeeScaleRadDropDownList.Name = "threeDeeScaleRadDropDownList";
            this.threeDeeScaleRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeScaleRadDropDownList.Size = new System.Drawing.Size(133, 18);
            this.threeDeeScaleRadDropDownList.TabIndex = 0;
            this.threeDeeScaleRadDropDownList.Text = "radDropDownList1";
            this.threeDeeScaleRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeScaleRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeRadPanel
            // 
            this.threeDeeRadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeRadPanel.AutoScroll = true;
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart15RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart14RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart13RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart12RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart11RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart6RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart7RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart8RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart9RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart10RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart5RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart4RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart3RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart2RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart1RadGroupBox);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart15WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart14WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart13WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart12WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart10WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart9WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart8WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart7WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart5WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart4WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart3WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart2WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart11WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart6WinChartViewer);
            this.threeDeeRadPanel.Controls.Add(this.threeDeeChart1WinChartViewer);
            this.threeDeeRadPanel.Location = new System.Drawing.Point(0, 0);
            this.threeDeeRadPanel.Name = "threeDeeRadPanel";
            this.threeDeeRadPanel.Size = new System.Drawing.Size(1001, 567);
            this.threeDeeRadPanel.TabIndex = 24;
            this.threeDeeRadPanel.Text = "PRPDD";
            // 
            // threeDeeChart15RadGroupBox
            // 
            this.threeDeeChart15RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart15RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart15RadGroupBox.Controls.Add(this.threeDeeChart15PDIValueRadLabel);
            this.threeDeeChart15RadGroupBox.Controls.Add(this.radLabel119);
            this.threeDeeChart15RadGroupBox.Controls.Add(this.radLabel120);
            this.threeDeeChart15RadGroupBox.Controls.Add(this.threeDeeChart15PhaseShiftRadDropDownList);
            this.threeDeeChart15RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart15RadGroupBox.FooterImageKey = "";
            this.threeDeeChart15RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart15RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart15RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart15RadGroupBox.HeaderText = "";
            this.threeDeeChart15RadGroupBox.Location = new System.Drawing.Point(783, 514);
            this.threeDeeChart15RadGroupBox.Name = "threeDeeChart15RadGroupBox";
            this.threeDeeChart15RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart15RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart15RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart15RadGroupBox.TabIndex = 26;
            this.threeDeeChart15RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart15PDIValueRadLabel
            // 
            this.threeDeeChart15PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart15PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart15PDIValueRadLabel.Name = "threeDeeChart15PDIValueRadLabel";
            this.threeDeeChart15PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart15PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart15PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel119
            // 
            this.radLabel119.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel119.Location = new System.Drawing.Point(1, 5);
            this.radLabel119.Name = "radLabel119";
            this.radLabel119.Size = new System.Drawing.Size(32, 16);
            this.radLabel119.TabIndex = 2;
            this.radLabel119.Text = "PDI=";
            // 
            // radLabel120
            // 
            this.radLabel120.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel120.Location = new System.Drawing.Point(83, 5);
            this.radLabel120.Name = "radLabel120";
            this.radLabel120.Size = new System.Drawing.Size(23, 16);
            this.radLabel120.TabIndex = 1;
            this.radLabel120.Text = "Ph.";
            // 
            // threeDeeChart15PhaseShiftRadDropDownList
            // 
            this.threeDeeChart15PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart15PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart15PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart15PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart15PhaseShiftRadDropDownList.Name = "threeDeeChart15PhaseShiftRadDropDownList";
            this.threeDeeChart15PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart15PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart15PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart15PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart15PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart15PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart14RadGroupBox
            // 
            this.threeDeeChart14RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart14RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart14RadGroupBox.Controls.Add(this.threeDeeChart14PDIValueRadLabel);
            this.threeDeeChart14RadGroupBox.Controls.Add(this.radLabel116);
            this.threeDeeChart14RadGroupBox.Controls.Add(this.radLabel117);
            this.threeDeeChart14RadGroupBox.Controls.Add(this.threeDeeChart14PhaseShiftRadDropDownList);
            this.threeDeeChart14RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart14RadGroupBox.FooterImageKey = "";
            this.threeDeeChart14RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart14RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart14RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart14RadGroupBox.HeaderText = "";
            this.threeDeeChart14RadGroupBox.Location = new System.Drawing.Point(588, 514);
            this.threeDeeChart14RadGroupBox.Name = "threeDeeChart14RadGroupBox";
            this.threeDeeChart14RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart14RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart14RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart14RadGroupBox.TabIndex = 26;
            this.threeDeeChart14RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart14PDIValueRadLabel
            // 
            this.threeDeeChart14PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart14PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart14PDIValueRadLabel.Name = "threeDeeChart14PDIValueRadLabel";
            this.threeDeeChart14PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart14PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart14PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel116
            // 
            this.radLabel116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel116.Location = new System.Drawing.Point(1, 5);
            this.radLabel116.Name = "radLabel116";
            this.radLabel116.Size = new System.Drawing.Size(32, 16);
            this.radLabel116.TabIndex = 2;
            this.radLabel116.Text = "PDI=";
            // 
            // radLabel117
            // 
            this.radLabel117.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel117.Location = new System.Drawing.Point(83, 5);
            this.radLabel117.Name = "radLabel117";
            this.radLabel117.Size = new System.Drawing.Size(23, 16);
            this.radLabel117.TabIndex = 1;
            this.radLabel117.Text = "Ph.";
            // 
            // threeDeeChart14PhaseShiftRadDropDownList
            // 
            this.threeDeeChart14PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart14PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart14PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart14PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart14PhaseShiftRadDropDownList.Name = "threeDeeChart14PhaseShiftRadDropDownList";
            this.threeDeeChart14PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart14PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart14PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart14PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart14PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart14PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart13RadGroupBox
            // 
            this.threeDeeChart13RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart13RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart13RadGroupBox.Controls.Add(this.threeDeeChart13PDIValueRadLabel);
            this.threeDeeChart13RadGroupBox.Controls.Add(this.radLabel113);
            this.threeDeeChart13RadGroupBox.Controls.Add(this.radLabel114);
            this.threeDeeChart13RadGroupBox.Controls.Add(this.threeDeeChart13PhaseShiftRadDropDownList);
            this.threeDeeChart13RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart13RadGroupBox.FooterImageKey = "";
            this.threeDeeChart13RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart13RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart13RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart13RadGroupBox.HeaderText = "";
            this.threeDeeChart13RadGroupBox.Location = new System.Drawing.Point(393, 514);
            this.threeDeeChart13RadGroupBox.Name = "threeDeeChart13RadGroupBox";
            this.threeDeeChart13RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart13RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart13RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart13RadGroupBox.TabIndex = 31;
            this.threeDeeChart13RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart13PDIValueRadLabel
            // 
            this.threeDeeChart13PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart13PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart13PDIValueRadLabel.Name = "threeDeeChart13PDIValueRadLabel";
            this.threeDeeChart13PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart13PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart13PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel113
            // 
            this.radLabel113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel113.Location = new System.Drawing.Point(1, 5);
            this.radLabel113.Name = "radLabel113";
            this.radLabel113.Size = new System.Drawing.Size(32, 16);
            this.radLabel113.TabIndex = 2;
            this.radLabel113.Text = "PDI=";
            // 
            // radLabel114
            // 
            this.radLabel114.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel114.Location = new System.Drawing.Point(83, 5);
            this.radLabel114.Name = "radLabel114";
            this.radLabel114.Size = new System.Drawing.Size(23, 16);
            this.radLabel114.TabIndex = 1;
            this.radLabel114.Text = "Ph.";
            // 
            // threeDeeChart13PhaseShiftRadDropDownList
            // 
            this.threeDeeChart13PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart13PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart13PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart13PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart13PhaseShiftRadDropDownList.Name = "threeDeeChart13PhaseShiftRadDropDownList";
            this.threeDeeChart13PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart13PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart13PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart13PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart13PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart13PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart12RadGroupBox
            // 
            this.threeDeeChart12RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart12RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart12RadGroupBox.Controls.Add(this.threeDeeChart12PDIValueRadLabel);
            this.threeDeeChart12RadGroupBox.Controls.Add(this.radLabel110);
            this.threeDeeChart12RadGroupBox.Controls.Add(this.radLabel111);
            this.threeDeeChart12RadGroupBox.Controls.Add(this.threeDeeChart12PhaseShiftRadDropDownList);
            this.threeDeeChart12RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart12RadGroupBox.FooterImageKey = "";
            this.threeDeeChart12RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart12RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart12RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart12RadGroupBox.HeaderText = "";
            this.threeDeeChart12RadGroupBox.Location = new System.Drawing.Point(198, 514);
            this.threeDeeChart12RadGroupBox.Name = "threeDeeChart12RadGroupBox";
            this.threeDeeChart12RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart12RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart12RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart12RadGroupBox.TabIndex = 30;
            this.threeDeeChart12RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart12PDIValueRadLabel
            // 
            this.threeDeeChart12PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart12PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart12PDIValueRadLabel.Name = "threeDeeChart12PDIValueRadLabel";
            this.threeDeeChart12PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart12PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart12PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel110
            // 
            this.radLabel110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel110.Location = new System.Drawing.Point(1, 5);
            this.radLabel110.Name = "radLabel110";
            this.radLabel110.Size = new System.Drawing.Size(32, 16);
            this.radLabel110.TabIndex = 2;
            this.radLabel110.Text = "PDI=";
            // 
            // radLabel111
            // 
            this.radLabel111.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel111.Location = new System.Drawing.Point(83, 5);
            this.radLabel111.Name = "radLabel111";
            this.radLabel111.Size = new System.Drawing.Size(23, 16);
            this.radLabel111.TabIndex = 1;
            this.radLabel111.Text = "Ph.";
            // 
            // threeDeeChart12PhaseShiftRadDropDownList
            // 
            this.threeDeeChart12PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart12PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart12PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart12PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart12PhaseShiftRadDropDownList.Name = "threeDeeChart12PhaseShiftRadDropDownList";
            this.threeDeeChart12PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart12PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart12PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart12PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart12PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart12PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart11RadGroupBox
            // 
            this.threeDeeChart11RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart11RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart11RadGroupBox.Controls.Add(this.threeDeeChart11PDIValueRadLabel);
            this.threeDeeChart11RadGroupBox.Controls.Add(this.radLabel107);
            this.threeDeeChart11RadGroupBox.Controls.Add(this.radLabel108);
            this.threeDeeChart11RadGroupBox.Controls.Add(this.threeDeeChart11PhaseShiftRadDropDownList);
            this.threeDeeChart11RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart11RadGroupBox.FooterImageKey = "";
            this.threeDeeChart11RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart11RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart11RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart11RadGroupBox.HeaderText = "";
            this.threeDeeChart11RadGroupBox.Location = new System.Drawing.Point(3, 514);
            this.threeDeeChart11RadGroupBox.Name = "threeDeeChart11RadGroupBox";
            this.threeDeeChart11RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart11RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart11RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart11RadGroupBox.TabIndex = 29;
            this.threeDeeChart11RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart11PDIValueRadLabel
            // 
            this.threeDeeChart11PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart11PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart11PDIValueRadLabel.Name = "threeDeeChart11PDIValueRadLabel";
            this.threeDeeChart11PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart11PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart11PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel107
            // 
            this.radLabel107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel107.Location = new System.Drawing.Point(1, 5);
            this.radLabel107.Name = "radLabel107";
            this.radLabel107.Size = new System.Drawing.Size(32, 16);
            this.radLabel107.TabIndex = 2;
            this.radLabel107.Text = "PDI=";
            // 
            // radLabel108
            // 
            this.radLabel108.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel108.Location = new System.Drawing.Point(83, 5);
            this.radLabel108.Name = "radLabel108";
            this.radLabel108.Size = new System.Drawing.Size(23, 16);
            this.radLabel108.TabIndex = 1;
            this.radLabel108.Text = "Ph.";
            // 
            // threeDeeChart11PhaseShiftRadDropDownList
            // 
            this.threeDeeChart11PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart11PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart11PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart11PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart11PhaseShiftRadDropDownList.Name = "threeDeeChart11PhaseShiftRadDropDownList";
            this.threeDeeChart11PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart11PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart11PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart11PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart11PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart11PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart6RadGroupBox
            // 
            this.threeDeeChart6RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart6RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart6RadGroupBox.Controls.Add(this.threeDeeChart6PDIValueRadLabel);
            this.threeDeeChart6RadGroupBox.Controls.Add(this.radLabel104);
            this.threeDeeChart6RadGroupBox.Controls.Add(this.radLabel105);
            this.threeDeeChart6RadGroupBox.Controls.Add(this.threeDeeChart6PhaseShiftRadDropDownList);
            this.threeDeeChart6RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart6RadGroupBox.FooterImageKey = "";
            this.threeDeeChart6RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart6RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart6RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart6RadGroupBox.HeaderText = "";
            this.threeDeeChart6RadGroupBox.Location = new System.Drawing.Point(3, 334);
            this.threeDeeChart6RadGroupBox.Name = "threeDeeChart6RadGroupBox";
            this.threeDeeChart6RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart6RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart6RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart6RadGroupBox.TabIndex = 26;
            this.threeDeeChart6RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart6PDIValueRadLabel
            // 
            this.threeDeeChart6PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart6PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart6PDIValueRadLabel.Name = "threeDeeChart6PDIValueRadLabel";
            this.threeDeeChart6PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart6PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart6PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel104
            // 
            this.radLabel104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel104.Location = new System.Drawing.Point(1, 5);
            this.radLabel104.Name = "radLabel104";
            this.radLabel104.Size = new System.Drawing.Size(32, 16);
            this.radLabel104.TabIndex = 2;
            this.radLabel104.Text = "PDI=";
            // 
            // radLabel105
            // 
            this.radLabel105.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel105.Location = new System.Drawing.Point(83, 5);
            this.radLabel105.Name = "radLabel105";
            this.radLabel105.Size = new System.Drawing.Size(23, 16);
            this.radLabel105.TabIndex = 1;
            this.radLabel105.Text = "Ph.";
            // 
            // threeDeeChart6PhaseShiftRadDropDownList
            // 
            this.threeDeeChart6PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart6PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart6PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart6PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart6PhaseShiftRadDropDownList.Name = "threeDeeChart6PhaseShiftRadDropDownList";
            this.threeDeeChart6PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart6PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart6PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart6PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart6PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart6PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart7RadGroupBox
            // 
            this.threeDeeChart7RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart7RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart7RadGroupBox.Controls.Add(this.threeDeeChart7PDIValueRadLabel);
            this.threeDeeChart7RadGroupBox.Controls.Add(this.radLabel101);
            this.threeDeeChart7RadGroupBox.Controls.Add(this.radLabel102);
            this.threeDeeChart7RadGroupBox.Controls.Add(this.threeDeeChart7PhaseShiftRadDropDownList);
            this.threeDeeChart7RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart7RadGroupBox.FooterImageKey = "";
            this.threeDeeChart7RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart7RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart7RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart7RadGroupBox.HeaderText = "";
            this.threeDeeChart7RadGroupBox.Location = new System.Drawing.Point(198, 334);
            this.threeDeeChart7RadGroupBox.Name = "threeDeeChart7RadGroupBox";
            this.threeDeeChart7RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart7RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart7RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart7RadGroupBox.TabIndex = 28;
            this.threeDeeChart7RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart7PDIValueRadLabel
            // 
            this.threeDeeChart7PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart7PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart7PDIValueRadLabel.Name = "threeDeeChart7PDIValueRadLabel";
            this.threeDeeChart7PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart7PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart7PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel101
            // 
            this.radLabel101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel101.Location = new System.Drawing.Point(1, 5);
            this.radLabel101.Name = "radLabel101";
            this.radLabel101.Size = new System.Drawing.Size(32, 16);
            this.radLabel101.TabIndex = 2;
            this.radLabel101.Text = "PDI=";
            // 
            // radLabel102
            // 
            this.radLabel102.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel102.Location = new System.Drawing.Point(83, 5);
            this.radLabel102.Name = "radLabel102";
            this.radLabel102.Size = new System.Drawing.Size(23, 16);
            this.radLabel102.TabIndex = 1;
            this.radLabel102.Text = "Ph.";
            // 
            // threeDeeChart7PhaseShiftRadDropDownList
            // 
            this.threeDeeChart7PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart7PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart7PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart7PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart7PhaseShiftRadDropDownList.Name = "threeDeeChart7PhaseShiftRadDropDownList";
            this.threeDeeChart7PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart7PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart7PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart7PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart7PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart7PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart8RadGroupBox
            // 
            this.threeDeeChart8RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart8RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart8RadGroupBox.Controls.Add(this.threeDeeChart8PDIValueRadLabel);
            this.threeDeeChart8RadGroupBox.Controls.Add(this.radLabel98);
            this.threeDeeChart8RadGroupBox.Controls.Add(this.radLabel99);
            this.threeDeeChart8RadGroupBox.Controls.Add(this.threeDeeChart8PhaseShiftRadDropDownList);
            this.threeDeeChart8RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart8RadGroupBox.FooterImageKey = "";
            this.threeDeeChart8RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart8RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart8RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart8RadGroupBox.HeaderText = "";
            this.threeDeeChart8RadGroupBox.Location = new System.Drawing.Point(393, 334);
            this.threeDeeChart8RadGroupBox.Name = "threeDeeChart8RadGroupBox";
            this.threeDeeChart8RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart8RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart8RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart8RadGroupBox.TabIndex = 26;
            this.threeDeeChart8RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart8PDIValueRadLabel
            // 
            this.threeDeeChart8PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart8PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart8PDIValueRadLabel.Name = "threeDeeChart8PDIValueRadLabel";
            this.threeDeeChart8PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart8PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart8PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel98
            // 
            this.radLabel98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel98.Location = new System.Drawing.Point(1, 5);
            this.radLabel98.Name = "radLabel98";
            this.radLabel98.Size = new System.Drawing.Size(32, 16);
            this.radLabel98.TabIndex = 2;
            this.radLabel98.Text = "PDI=";
            // 
            // radLabel99
            // 
            this.radLabel99.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel99.Location = new System.Drawing.Point(83, 5);
            this.radLabel99.Name = "radLabel99";
            this.radLabel99.Size = new System.Drawing.Size(23, 16);
            this.radLabel99.TabIndex = 1;
            this.radLabel99.Text = "Ph.";
            // 
            // threeDeeChart8PhaseShiftRadDropDownList
            // 
            this.threeDeeChart8PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart8PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart8PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart8PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart8PhaseShiftRadDropDownList.Name = "threeDeeChart8PhaseShiftRadDropDownList";
            this.threeDeeChart8PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart8PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart8PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart8PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart8PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart8PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart9RadGroupBox
            // 
            this.threeDeeChart9RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart9RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart9RadGroupBox.Controls.Add(this.threeDeeChart9PDIValueRadLabel);
            this.threeDeeChart9RadGroupBox.Controls.Add(this.radLabel95);
            this.threeDeeChart9RadGroupBox.Controls.Add(this.radLabel96);
            this.threeDeeChart9RadGroupBox.Controls.Add(this.threeDeeChart9PhaseShiftRadDropDownList);
            this.threeDeeChart9RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart9RadGroupBox.FooterImageKey = "";
            this.threeDeeChart9RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart9RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart9RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart9RadGroupBox.HeaderText = "";
            this.threeDeeChart9RadGroupBox.Location = new System.Drawing.Point(589, 334);
            this.threeDeeChart9RadGroupBox.Name = "threeDeeChart9RadGroupBox";
            this.threeDeeChart9RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart9RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart9RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart9RadGroupBox.TabIndex = 26;
            this.threeDeeChart9RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart9PDIValueRadLabel
            // 
            this.threeDeeChart9PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart9PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart9PDIValueRadLabel.Name = "threeDeeChart9PDIValueRadLabel";
            this.threeDeeChart9PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart9PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart9PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel95
            // 
            this.radLabel95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel95.Location = new System.Drawing.Point(1, 5);
            this.radLabel95.Name = "radLabel95";
            this.radLabel95.Size = new System.Drawing.Size(32, 16);
            this.radLabel95.TabIndex = 2;
            this.radLabel95.Text = "PDI=";
            // 
            // radLabel96
            // 
            this.radLabel96.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel96.Location = new System.Drawing.Point(83, 5);
            this.radLabel96.Name = "radLabel96";
            this.radLabel96.Size = new System.Drawing.Size(23, 16);
            this.radLabel96.TabIndex = 1;
            this.radLabel96.Text = "Ph.";
            // 
            // threeDeeChart9PhaseShiftRadDropDownList
            // 
            this.threeDeeChart9PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart9PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart9PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart9PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart9PhaseShiftRadDropDownList.Name = "threeDeeChart9PhaseShiftRadDropDownList";
            this.threeDeeChart9PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart9PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart9PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart9PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart9PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart9PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart10RadGroupBox
            // 
            this.threeDeeChart10RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart10RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart10RadGroupBox.Controls.Add(this.threeDeeChart10PDIValueRadLabel);
            this.threeDeeChart10RadGroupBox.Controls.Add(this.radLabel92);
            this.threeDeeChart10RadGroupBox.Controls.Add(this.radLabel93);
            this.threeDeeChart10RadGroupBox.Controls.Add(this.threeDeeChart10PhaseShiftRadDropDownList);
            this.threeDeeChart10RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart10RadGroupBox.FooterImageKey = "";
            this.threeDeeChart10RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart10RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart10RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart10RadGroupBox.HeaderText = "";
            this.threeDeeChart10RadGroupBox.Location = new System.Drawing.Point(783, 334);
            this.threeDeeChart10RadGroupBox.Name = "threeDeeChart10RadGroupBox";
            this.threeDeeChart10RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart10RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart10RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart10RadGroupBox.TabIndex = 26;
            this.threeDeeChart10RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart10PDIValueRadLabel
            // 
            this.threeDeeChart10PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart10PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart10PDIValueRadLabel.Name = "threeDeeChart10PDIValueRadLabel";
            this.threeDeeChart10PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart10PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart10PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel92
            // 
            this.radLabel92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel92.Location = new System.Drawing.Point(1, 5);
            this.radLabel92.Name = "radLabel92";
            this.radLabel92.Size = new System.Drawing.Size(32, 16);
            this.radLabel92.TabIndex = 2;
            this.radLabel92.Text = "PDI=";
            // 
            // radLabel93
            // 
            this.radLabel93.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel93.Location = new System.Drawing.Point(83, 5);
            this.radLabel93.Name = "radLabel93";
            this.radLabel93.Size = new System.Drawing.Size(23, 16);
            this.radLabel93.TabIndex = 1;
            this.radLabel93.Text = "Ph.";
            // 
            // threeDeeChart10PhaseShiftRadDropDownList
            // 
            this.threeDeeChart10PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart10PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart10PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart10PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart10PhaseShiftRadDropDownList.Name = "threeDeeChart10PhaseShiftRadDropDownList";
            this.threeDeeChart10PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart10PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart10PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart10PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart10PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart10PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart5RadGroupBox
            // 
            this.threeDeeChart5RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart5RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart5RadGroupBox.Controls.Add(this.threeDeeChart5PDIValueRadLabel);
            this.threeDeeChart5RadGroupBox.Controls.Add(this.radLabel85);
            this.threeDeeChart5RadGroupBox.Controls.Add(this.radLabel88);
            this.threeDeeChart5RadGroupBox.Controls.Add(this.threeDeeChart5PhaseShiftRadDropDownList);
            this.threeDeeChart5RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart5RadGroupBox.FooterImageKey = "";
            this.threeDeeChart5RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart5RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart5RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart5RadGroupBox.HeaderText = "";
            this.threeDeeChart5RadGroupBox.Location = new System.Drawing.Point(783, 154);
            this.threeDeeChart5RadGroupBox.Name = "threeDeeChart5RadGroupBox";
            this.threeDeeChart5RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart5RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart5RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart5RadGroupBox.TabIndex = 26;
            this.threeDeeChart5RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart5PDIValueRadLabel
            // 
            this.threeDeeChart5PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart5PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart5PDIValueRadLabel.Name = "threeDeeChart5PDIValueRadLabel";
            this.threeDeeChart5PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart5PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart5PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel85
            // 
            this.radLabel85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel85.Location = new System.Drawing.Point(1, 5);
            this.radLabel85.Name = "radLabel85";
            this.radLabel85.Size = new System.Drawing.Size(32, 16);
            this.radLabel85.TabIndex = 2;
            this.radLabel85.Text = "PDI=";
            // 
            // radLabel88
            // 
            this.radLabel88.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel88.Location = new System.Drawing.Point(83, 5);
            this.radLabel88.Name = "radLabel88";
            this.radLabel88.Size = new System.Drawing.Size(23, 16);
            this.radLabel88.TabIndex = 1;
            this.radLabel88.Text = "Ph.";
            // 
            // threeDeeChart5PhaseShiftRadDropDownList
            // 
            this.threeDeeChart5PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart5PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart5PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart5PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart5PhaseShiftRadDropDownList.Name = "threeDeeChart5PhaseShiftRadDropDownList";
            this.threeDeeChart5PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart5PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart5PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart5PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart5PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart5PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart4RadGroupBox
            // 
            this.threeDeeChart4RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart4RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart4RadGroupBox.Controls.Add(this.threeDeeChart4PDIValueRadLabel);
            this.threeDeeChart4RadGroupBox.Controls.Add(this.radLabel76);
            this.threeDeeChart4RadGroupBox.Controls.Add(this.radLabel79);
            this.threeDeeChart4RadGroupBox.Controls.Add(this.threeDeeChart4PhaseShiftRadDropDownList);
            this.threeDeeChart4RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart4RadGroupBox.FooterImageKey = "";
            this.threeDeeChart4RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart4RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart4RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart4RadGroupBox.HeaderText = "";
            this.threeDeeChart4RadGroupBox.Location = new System.Drawing.Point(588, 154);
            this.threeDeeChart4RadGroupBox.Name = "threeDeeChart4RadGroupBox";
            this.threeDeeChart4RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart4RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart4RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart4RadGroupBox.TabIndex = 26;
            this.threeDeeChart4RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart4PDIValueRadLabel
            // 
            this.threeDeeChart4PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart4PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart4PDIValueRadLabel.Name = "threeDeeChart4PDIValueRadLabel";
            this.threeDeeChart4PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart4PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart4PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel76
            // 
            this.radLabel76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel76.Location = new System.Drawing.Point(1, 5);
            this.radLabel76.Name = "radLabel76";
            this.radLabel76.Size = new System.Drawing.Size(32, 16);
            this.radLabel76.TabIndex = 2;
            this.radLabel76.Text = "PDI=";
            // 
            // radLabel79
            // 
            this.radLabel79.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel79.Location = new System.Drawing.Point(83, 5);
            this.radLabel79.Name = "radLabel79";
            this.radLabel79.Size = new System.Drawing.Size(23, 16);
            this.radLabel79.TabIndex = 1;
            this.radLabel79.Text = "Ph.";
            // 
            // threeDeeChart4PhaseShiftRadDropDownList
            // 
            this.threeDeeChart4PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart4PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart4PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart4PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart4PhaseShiftRadDropDownList.Name = "threeDeeChart4PhaseShiftRadDropDownList";
            this.threeDeeChart4PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart4PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart4PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart4PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart4PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart4PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart3RadGroupBox
            // 
            this.threeDeeChart3RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart3RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart3RadGroupBox.Controls.Add(this.threeDeeChart3PDIValueRadLabel);
            this.threeDeeChart3RadGroupBox.Controls.Add(this.radLabel63);
            this.threeDeeChart3RadGroupBox.Controls.Add(this.radLabel67);
            this.threeDeeChart3RadGroupBox.Controls.Add(this.threeDeeChart3PhaseShiftRadDropDownList);
            this.threeDeeChart3RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart3RadGroupBox.FooterImageKey = "";
            this.threeDeeChart3RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart3RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart3RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart3RadGroupBox.HeaderText = "";
            this.threeDeeChart3RadGroupBox.Location = new System.Drawing.Point(393, 154);
            this.threeDeeChart3RadGroupBox.Name = "threeDeeChart3RadGroupBox";
            this.threeDeeChart3RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart3RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart3RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart3RadGroupBox.TabIndex = 26;
            this.threeDeeChart3RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart3PDIValueRadLabel
            // 
            this.threeDeeChart3PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart3PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart3PDIValueRadLabel.Name = "threeDeeChart3PDIValueRadLabel";
            this.threeDeeChart3PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart3PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart3PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel63
            // 
            this.radLabel63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel63.Location = new System.Drawing.Point(1, 5);
            this.radLabel63.Name = "radLabel63";
            this.radLabel63.Size = new System.Drawing.Size(32, 16);
            this.radLabel63.TabIndex = 2;
            this.radLabel63.Text = "PDI=";
            // 
            // radLabel67
            // 
            this.radLabel67.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel67.Location = new System.Drawing.Point(83, 5);
            this.radLabel67.Name = "radLabel67";
            this.radLabel67.Size = new System.Drawing.Size(23, 16);
            this.radLabel67.TabIndex = 1;
            this.radLabel67.Text = "Ph.";
            // 
            // threeDeeChart3PhaseShiftRadDropDownList
            // 
            this.threeDeeChart3PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart3PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart3PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart3PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart3PhaseShiftRadDropDownList.Name = "threeDeeChart3PhaseShiftRadDropDownList";
            this.threeDeeChart3PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart3PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart3PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart3PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart3PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart3PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart2RadGroupBox
            // 
            this.threeDeeChart2RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart2RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart2RadGroupBox.Controls.Add(this.threeDeeChart2PDIValueRadLabel);
            this.threeDeeChart2RadGroupBox.Controls.Add(this.radLabel49);
            this.threeDeeChart2RadGroupBox.Controls.Add(this.radLabel54);
            this.threeDeeChart2RadGroupBox.Controls.Add(this.threeDeeChart2PhaseShiftRadDropDownList);
            this.threeDeeChart2RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart2RadGroupBox.FooterImageKey = "";
            this.threeDeeChart2RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart2RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart2RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart2RadGroupBox.HeaderText = "";
            this.threeDeeChart2RadGroupBox.Location = new System.Drawing.Point(198, 154);
            this.threeDeeChart2RadGroupBox.Name = "threeDeeChart2RadGroupBox";
            this.threeDeeChart2RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart2RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart2RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart2RadGroupBox.TabIndex = 27;
            this.threeDeeChart2RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart2PDIValueRadLabel
            // 
            this.threeDeeChart2PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart2PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart2PDIValueRadLabel.Name = "threeDeeChart2PDIValueRadLabel";
            this.threeDeeChart2PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart2PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart2PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel49
            // 
            this.radLabel49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel49.Location = new System.Drawing.Point(1, 5);
            this.radLabel49.Name = "radLabel49";
            this.radLabel49.Size = new System.Drawing.Size(32, 16);
            this.radLabel49.TabIndex = 2;
            this.radLabel49.Text = "PDI=";
            // 
            // radLabel54
            // 
            this.radLabel54.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel54.Location = new System.Drawing.Point(83, 5);
            this.radLabel54.Name = "radLabel54";
            this.radLabel54.Size = new System.Drawing.Size(23, 16);
            this.radLabel54.TabIndex = 1;
            this.radLabel54.Text = "Ph.";
            // 
            // threeDeeChart2PhaseShiftRadDropDownList
            // 
            this.threeDeeChart2PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart2PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart2PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart2PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart2PhaseShiftRadDropDownList.Name = "threeDeeChart2PhaseShiftRadDropDownList";
            this.threeDeeChart2PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart2PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart2PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart2PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart2PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart2PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart1RadGroupBox
            // 
            this.threeDeeChart1RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChart1RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeChart1RadGroupBox.Controls.Add(this.threeDeeChart1PDIValueRadLabel);
            this.threeDeeChart1RadGroupBox.Controls.Add(this.radLabel18);
            this.threeDeeChart1RadGroupBox.Controls.Add(this.radLabel30);
            this.threeDeeChart1RadGroupBox.Controls.Add(this.threeDeeChart1PhaseShiftRadDropDownList);
            this.threeDeeChart1RadGroupBox.FooterImageIndex = -1;
            this.threeDeeChart1RadGroupBox.FooterImageKey = "";
            this.threeDeeChart1RadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChart1RadGroupBox.HeaderImageKey = "";
            this.threeDeeChart1RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChart1RadGroupBox.HeaderText = "";
            this.threeDeeChart1RadGroupBox.Location = new System.Drawing.Point(3, 154);
            this.threeDeeChart1RadGroupBox.Name = "threeDeeChart1RadGroupBox";
            this.threeDeeChart1RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChart1RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChart1RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.threeDeeChart1RadGroupBox.TabIndex = 26;
            this.threeDeeChart1RadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeChart1PDIValueRadLabel
            // 
            this.threeDeeChart1PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart1PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.threeDeeChart1PDIValueRadLabel.Name = "threeDeeChart1PDIValueRadLabel";
            this.threeDeeChart1PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.threeDeeChart1PDIValueRadLabel.TabIndex = 3;
            this.threeDeeChart1PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel18
            // 
            this.radLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel18.Location = new System.Drawing.Point(1, 5);
            this.radLabel18.Name = "radLabel18";
            this.radLabel18.Size = new System.Drawing.Size(32, 16);
            this.radLabel18.TabIndex = 2;
            this.radLabel18.Text = "PDI=";
            // 
            // radLabel30
            // 
            this.radLabel30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel30.Location = new System.Drawing.Point(83, 5);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(23, 16);
            this.radLabel30.TabIndex = 1;
            this.radLabel30.Text = "Ph.";
            // 
            // threeDeeChart1PhaseShiftRadDropDownList
            // 
            this.threeDeeChart1PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.threeDeeChart1PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.threeDeeChart1PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeChart1PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.threeDeeChart1PhaseShiftRadDropDownList.Name = "threeDeeChart1PhaseShiftRadDropDownList";
            this.threeDeeChart1PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.threeDeeChart1PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.threeDeeChart1PhaseShiftRadDropDownList.TabIndex = 0;
            this.threeDeeChart1PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.threeDeeChart1PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.threeDeeChart1PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // threeDeeChart15WinChartViewer
            // 
            this.threeDeeChart15WinChartViewer.Location = new System.Drawing.Point(783, 363);
            this.threeDeeChart15WinChartViewer.Name = "threeDeeChart15WinChartViewer";
            this.threeDeeChart15WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart15WinChartViewer.TabIndex = 24;
            this.threeDeeChart15WinChartViewer.TabStop = false;
            this.threeDeeChart15WinChartViewer.Click += new System.EventHandler(this.threeDeeChart15CopyGraph_Click);
            // 
            // threeDeeChart14WinChartViewer
            // 
            this.threeDeeChart14WinChartViewer.Location = new System.Drawing.Point(588, 363);
            this.threeDeeChart14WinChartViewer.Name = "threeDeeChart14WinChartViewer";
            this.threeDeeChart14WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart14WinChartViewer.TabIndex = 23;
            this.threeDeeChart14WinChartViewer.TabStop = false;
            this.threeDeeChart14WinChartViewer.Click += new System.EventHandler(this.threeDeeChart14CopyGraph_Click);
            // 
            // threeDeeChart13WinChartViewer
            // 
            this.threeDeeChart13WinChartViewer.Location = new System.Drawing.Point(393, 363);
            this.threeDeeChart13WinChartViewer.Name = "threeDeeChart13WinChartViewer";
            this.threeDeeChart13WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart13WinChartViewer.TabIndex = 22;
            this.threeDeeChart13WinChartViewer.TabStop = false;
            this.threeDeeChart13WinChartViewer.Click += new System.EventHandler(this.threeDeeChart13CopyGraph_Click);
            // 
            // threeDeeChart12WinChartViewer
            // 
            this.threeDeeChart12WinChartViewer.Location = new System.Drawing.Point(198, 363);
            this.threeDeeChart12WinChartViewer.Name = "threeDeeChart12WinChartViewer";
            this.threeDeeChart12WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart12WinChartViewer.TabIndex = 21;
            this.threeDeeChart12WinChartViewer.TabStop = false;
            this.threeDeeChart12WinChartViewer.Click += new System.EventHandler(this.threeDeeChart12CopyGraph_Click);
            // 
            // threeDeeChart10WinChartViewer
            // 
            this.threeDeeChart10WinChartViewer.Location = new System.Drawing.Point(783, 183);
            this.threeDeeChart10WinChartViewer.Name = "threeDeeChart10WinChartViewer";
            this.threeDeeChart10WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart10WinChartViewer.TabIndex = 20;
            this.threeDeeChart10WinChartViewer.TabStop = false;
            this.threeDeeChart10WinChartViewer.Click += new System.EventHandler(this.threeDeeChart10CopyGraph_Click);
            // 
            // threeDeeChart9WinChartViewer
            // 
            this.threeDeeChart9WinChartViewer.Location = new System.Drawing.Point(588, 183);
            this.threeDeeChart9WinChartViewer.Name = "threeDeeChart9WinChartViewer";
            this.threeDeeChart9WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart9WinChartViewer.TabIndex = 19;
            this.threeDeeChart9WinChartViewer.TabStop = false;
            this.threeDeeChart9WinChartViewer.Click += new System.EventHandler(this.threeDeeChart9CopyGraph_Click);
            // 
            // threeDeeChart8WinChartViewer
            // 
            this.threeDeeChart8WinChartViewer.Location = new System.Drawing.Point(393, 183);
            this.threeDeeChart8WinChartViewer.Name = "threeDeeChart8WinChartViewer";
            this.threeDeeChart8WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart8WinChartViewer.TabIndex = 18;
            this.threeDeeChart8WinChartViewer.TabStop = false;
            this.threeDeeChart8WinChartViewer.Click += new System.EventHandler(this.threeDeeChart8CopyGraph_Click);
            // 
            // threeDeeChart7WinChartViewer
            // 
            this.threeDeeChart7WinChartViewer.Location = new System.Drawing.Point(198, 183);
            this.threeDeeChart7WinChartViewer.Name = "threeDeeChart7WinChartViewer";
            this.threeDeeChart7WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart7WinChartViewer.TabIndex = 17;
            this.threeDeeChart7WinChartViewer.TabStop = false;
            this.threeDeeChart7WinChartViewer.Click += new System.EventHandler(this.threeDeeChart7CopyGraph_Click);
            // 
            // threeDeeChart5WinChartViewer
            // 
            this.threeDeeChart5WinChartViewer.Location = new System.Drawing.Point(783, 3);
            this.threeDeeChart5WinChartViewer.Name = "threeDeeChart5WinChartViewer";
            this.threeDeeChart5WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart5WinChartViewer.TabIndex = 15;
            this.threeDeeChart5WinChartViewer.TabStop = false;
            this.threeDeeChart5WinChartViewer.Click += new System.EventHandler(this.threeDeeChart5CopyGraph_Click);
            // 
            // threeDeeChart4WinChartViewer
            // 
            this.threeDeeChart4WinChartViewer.Location = new System.Drawing.Point(588, 3);
            this.threeDeeChart4WinChartViewer.Name = "threeDeeChart4WinChartViewer";
            this.threeDeeChart4WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart4WinChartViewer.TabIndex = 14;
            this.threeDeeChart4WinChartViewer.TabStop = false;
            this.threeDeeChart4WinChartViewer.Click += new System.EventHandler(this.threeDeeChart4CopyGraph_Click);
            // 
            // threeDeeChart3WinChartViewer
            // 
            this.threeDeeChart3WinChartViewer.Location = new System.Drawing.Point(393, 3);
            this.threeDeeChart3WinChartViewer.Name = "threeDeeChart3WinChartViewer";
            this.threeDeeChart3WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart3WinChartViewer.TabIndex = 11;
            this.threeDeeChart3WinChartViewer.TabStop = false;
            this.threeDeeChart3WinChartViewer.Click += new System.EventHandler(this.threeDeeChart3CopyGraph_Click);
            // 
            // threeDeeChart2WinChartViewer
            // 
            this.threeDeeChart2WinChartViewer.Location = new System.Drawing.Point(198, 3);
            this.threeDeeChart2WinChartViewer.Name = "threeDeeChart2WinChartViewer";
            this.threeDeeChart2WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart2WinChartViewer.TabIndex = 8;
            this.threeDeeChart2WinChartViewer.TabStop = false;
            this.threeDeeChart2WinChartViewer.Click += new System.EventHandler(this.threeDeeChart2CopyGraph_Click);
            // 
            // threeDeeChart11WinChartViewer
            // 
            this.threeDeeChart11WinChartViewer.Location = new System.Drawing.Point(3, 363);
            this.threeDeeChart11WinChartViewer.Name = "threeDeeChart11WinChartViewer";
            this.threeDeeChart11WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart11WinChartViewer.TabIndex = 7;
            this.threeDeeChart11WinChartViewer.TabStop = false;
            this.threeDeeChart11WinChartViewer.Click += new System.EventHandler(this.threeDeeChart11CopyGraph_Click);
            // 
            // threeDeeChart6WinChartViewer
            // 
            this.threeDeeChart6WinChartViewer.Location = new System.Drawing.Point(3, 183);
            this.threeDeeChart6WinChartViewer.Name = "threeDeeChart6WinChartViewer";
            this.threeDeeChart6WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart6WinChartViewer.TabIndex = 6;
            this.threeDeeChart6WinChartViewer.TabStop = false;
            this.threeDeeChart6WinChartViewer.Click += new System.EventHandler(this.threeDeeChart6CopyGraph_Click);
            // 
            // threeDeeChart1WinChartViewer
            // 
            this.threeDeeChart1WinChartViewer.Location = new System.Drawing.Point(3, 3);
            this.threeDeeChart1WinChartViewer.Name = "threeDeeChart1WinChartViewer";
            this.threeDeeChart1WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.threeDeeChart1WinChartViewer.TabIndex = 5;
            this.threeDeeChart1WinChartViewer.TabStop = false;
            this.threeDeeChart1WinChartViewer.Click += new System.EventHandler(this.threeDeeChart1CopyGraph_Click);
            // 
            // threeDeeChartLayoutRadGroupBox
            // 
            this.threeDeeChartLayoutRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.threeDeeChartLayoutRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.threeDeeChartLayoutRadGroupBox.Controls.Add(this.threeDeeFifteenByOneRadRadioButton);
            this.threeDeeChartLayoutRadGroupBox.Controls.Add(this.threeDeeEightByTwoRadRadioButton);
            this.threeDeeChartLayoutRadGroupBox.Controls.Add(this.threeDeeFiveByThreeRadRadioButton);
            this.threeDeeChartLayoutRadGroupBox.Controls.Add(this.threeDeeThreeByFiveRadRadioButton);
            this.threeDeeChartLayoutRadGroupBox.FooterImageIndex = -1;
            this.threeDeeChartLayoutRadGroupBox.FooterImageKey = "";
            this.threeDeeChartLayoutRadGroupBox.HeaderImageIndex = -1;
            this.threeDeeChartLayoutRadGroupBox.HeaderImageKey = "";
            this.threeDeeChartLayoutRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.threeDeeChartLayoutRadGroupBox.HeaderText = "Chart Layout";
            this.threeDeeChartLayoutRadGroupBox.Location = new System.Drawing.Point(4, 573);
            this.threeDeeChartLayoutRadGroupBox.Name = "threeDeeChartLayoutRadGroupBox";
            this.threeDeeChartLayoutRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.threeDeeChartLayoutRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.threeDeeChartLayoutRadGroupBox.Size = new System.Drawing.Size(115, 68);
            this.threeDeeChartLayoutRadGroupBox.TabIndex = 84;
            this.threeDeeChartLayoutRadGroupBox.Text = "Chart Layout";
            this.threeDeeChartLayoutRadGroupBox.ThemeName = "Office2007Black";
            // 
            // threeDeeFifteenByOneRadRadioButton
            // 
            this.threeDeeFifteenByOneRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeFifteenByOneRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeFifteenByOneRadRadioButton.Location = new System.Drawing.Point(60, 45);
            this.threeDeeFifteenByOneRadRadioButton.Name = "threeDeeFifteenByOneRadRadioButton";
            this.threeDeeFifteenByOneRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.threeDeeFifteenByOneRadRadioButton.TabIndex = 3;
            this.threeDeeFifteenByOneRadRadioButton.Text = "15x1";
            this.threeDeeFifteenByOneRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeFifteenByOneRadRadioButton_ToggleStateChanged);
            // 
            // threeDeeEightByTwoRadRadioButton
            // 
            this.threeDeeEightByTwoRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeEightByTwoRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeEightByTwoRadRadioButton.Location = new System.Drawing.Point(60, 24);
            this.threeDeeEightByTwoRadRadioButton.Name = "threeDeeEightByTwoRadRadioButton";
            this.threeDeeEightByTwoRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.threeDeeEightByTwoRadRadioButton.TabIndex = 2;
            this.threeDeeEightByTwoRadRadioButton.Text = "8x2";
            this.threeDeeEightByTwoRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeEightByTwoRadRadioButton_ToggleStateChanged);
            // 
            // threeDeeFiveByThreeRadRadioButton
            // 
            this.threeDeeFiveByThreeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeFiveByThreeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeFiveByThreeRadRadioButton.Location = new System.Drawing.Point(10, 45);
            this.threeDeeFiveByThreeRadRadioButton.Name = "threeDeeFiveByThreeRadRadioButton";
            this.threeDeeFiveByThreeRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.threeDeeFiveByThreeRadRadioButton.TabIndex = 1;
            this.threeDeeFiveByThreeRadRadioButton.Text = "5x3";
            this.threeDeeFiveByThreeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeFiveByThreeRadRadioButton_ToggleStateChanged);
            // 
            // threeDeeThreeByFiveRadRadioButton
            // 
            this.threeDeeThreeByFiveRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeDeeThreeByFiveRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.threeDeeThreeByFiveRadRadioButton.Location = new System.Drawing.Point(10, 24);
            this.threeDeeThreeByFiveRadRadioButton.Name = "threeDeeThreeByFiveRadRadioButton";
            this.threeDeeThreeByFiveRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.threeDeeThreeByFiveRadRadioButton.TabIndex = 0;
            this.threeDeeThreeByFiveRadRadioButton.TabStop = true;
            this.threeDeeThreeByFiveRadRadioButton.Text = "3x5";
            this.threeDeeThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.threeDeeThreeByFiveRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeDeeThreeByFiveRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarRadPageViewPage
            // 
            this.prpddPolarRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.prpddPolarRadPageViewPage.Controls.Add(this.dataReadingDatePrpddPolarTabValueRadLabel);
            this.prpddPolarRadPageViewPage.Controls.Add(this.dataReadingDatePrpddPolarTabTextRadLabel);
            this.prpddPolarRadPageViewPage.Controls.Add(this.prpddPolarRadPanel);
            this.prpddPolarRadPageViewPage.Controls.Add(this.prpddPolarDisplaySettingsRadGroupBox);
            this.prpddPolarRadPageViewPage.Controls.Add(this.prpddPolarChartLayoutRadGroupBox);
            this.prpddPolarRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.prpddPolarRadPageViewPage.Name = "prpddPolarRadPageViewPage";
            this.prpddPolarRadPageViewPage.Size = new System.Drawing.Size(1004, 722);
            this.prpddPolarRadPageViewPage.Text = "PRPDD Polar";
            // 
            // dataReadingDatePrpddPolarTabValueRadLabel
            // 
            this.dataReadingDatePrpddPolarTabValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpddPolarTabValueRadLabel.Location = new System.Drawing.Point(521, 580);
            this.dataReadingDatePrpddPolarTabValueRadLabel.Name = "dataReadingDatePrpddPolarTabValueRadLabel";
            this.dataReadingDatePrpddPolarTabValueRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpddPolarTabValueRadLabel.TabIndex = 94;
            this.dataReadingDatePrpddPolarTabValueRadLabel.Text = "Data Reading Date:";
            // 
            // dataReadingDatePrpddPolarTabTextRadLabel
            // 
            this.dataReadingDatePrpddPolarTabTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpddPolarTabTextRadLabel.Location = new System.Drawing.Point(410, 580);
            this.dataReadingDatePrpddPolarTabTextRadLabel.Name = "dataReadingDatePrpddPolarTabTextRadLabel";
            this.dataReadingDatePrpddPolarTabTextRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpddPolarTabTextRadLabel.TabIndex = 93;
            this.dataReadingDatePrpddPolarTabTextRadLabel.Text = "Data Reading Date:";
            // 
            // prpddPolarRadPanel
            // 
            this.prpddPolarRadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarRadPanel.AutoScroll = true;
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart15RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart14RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart13RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart12RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart11RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart10RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart9RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart8RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart7RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart6RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart5RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart4RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart3RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart2RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart1RadGroupBox);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart15WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart14WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart13WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart12WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart10WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart9WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart8WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart7WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart5WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart4WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart3WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart2WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart11WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart6WinChartViewer);
            this.prpddPolarRadPanel.Controls.Add(this.prpddPolarChart1WinChartViewer);
            this.prpddPolarRadPanel.Location = new System.Drawing.Point(0, 0);
            this.prpddPolarRadPanel.Name = "prpddPolarRadPanel";
            this.prpddPolarRadPanel.Size = new System.Drawing.Size(1001, 567);
            this.prpddPolarRadPanel.TabIndex = 92;
            // 
            // prpddPolarChart15RadGroupBox
            // 
            this.prpddPolarChart15RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart15RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart15RadGroupBox.Controls.Add(this.prpddPolarChart15PDIValueRadLabel);
            this.prpddPolarChart15RadGroupBox.Controls.Add(this.radLabel9);
            this.prpddPolarChart15RadGroupBox.Controls.Add(this.radLabel12);
            this.prpddPolarChart15RadGroupBox.Controls.Add(this.prpddPolarChart15PhaseShiftRadDropDownList);
            this.prpddPolarChart15RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart15RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart15RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart15RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart15RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart15RadGroupBox.HeaderText = "";
            this.prpddPolarChart15RadGroupBox.Location = new System.Drawing.Point(783, 514);
            this.prpddPolarChart15RadGroupBox.Name = "prpddPolarChart15RadGroupBox";
            this.prpddPolarChart15RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart15RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart15RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart15RadGroupBox.TabIndex = 39;
            this.prpddPolarChart15RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart15PDIValueRadLabel
            // 
            this.prpddPolarChart15PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart15PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart15PDIValueRadLabel.Name = "prpddPolarChart15PDIValueRadLabel";
            this.prpddPolarChart15PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart15PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart15PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel9
            // 
            this.radLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel9.Location = new System.Drawing.Point(1, 5);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(32, 16);
            this.radLabel9.TabIndex = 2;
            this.radLabel9.Text = "PDI=";
            // 
            // radLabel12
            // 
            this.radLabel12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel12.Location = new System.Drawing.Point(83, 5);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(23, 16);
            this.radLabel12.TabIndex = 1;
            this.radLabel12.Text = "Ph.";
            // 
            // prpddPolarChart15PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart15PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart15PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart15PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart15PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart15PhaseShiftRadDropDownList.Name = "prpddPolarChart15PhaseShiftRadDropDownList";
            this.prpddPolarChart15PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart15PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart15PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart15PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart15PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart15PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart14RadGroupBox
            // 
            this.prpddPolarChart14RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart14RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart14RadGroupBox.Controls.Add(this.prpddPolarChart14PDIValueRadLabel);
            this.prpddPolarChart14RadGroupBox.Controls.Add(this.radLabel21);
            this.prpddPolarChart14RadGroupBox.Controls.Add(this.radLabel27);
            this.prpddPolarChart14RadGroupBox.Controls.Add(this.prpddPolarChart14PhaseShiftRadDropDownList);
            this.prpddPolarChart14RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart14RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart14RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart14RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart14RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart14RadGroupBox.HeaderText = "";
            this.prpddPolarChart14RadGroupBox.Location = new System.Drawing.Point(588, 514);
            this.prpddPolarChart14RadGroupBox.Name = "prpddPolarChart14RadGroupBox";
            this.prpddPolarChart14RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart14RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart14RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart14RadGroupBox.TabIndex = 38;
            this.prpddPolarChart14RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart14PDIValueRadLabel
            // 
            this.prpddPolarChart14PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart14PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart14PDIValueRadLabel.Name = "prpddPolarChart14PDIValueRadLabel";
            this.prpddPolarChart14PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart14PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart14PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel21
            // 
            this.radLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel21.Location = new System.Drawing.Point(1, 5);
            this.radLabel21.Name = "radLabel21";
            this.radLabel21.Size = new System.Drawing.Size(32, 16);
            this.radLabel21.TabIndex = 2;
            this.radLabel21.Text = "PDI=";
            // 
            // radLabel27
            // 
            this.radLabel27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel27.Location = new System.Drawing.Point(83, 5);
            this.radLabel27.Name = "radLabel27";
            this.radLabel27.Size = new System.Drawing.Size(23, 16);
            this.radLabel27.TabIndex = 1;
            this.radLabel27.Text = "Ph.";
            // 
            // prpddPolarChart14PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart14PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart14PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart14PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart14PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart14PhaseShiftRadDropDownList.Name = "prpddPolarChart14PhaseShiftRadDropDownList";
            this.prpddPolarChart14PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart14PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart14PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart14PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart14PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart14PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart13RadGroupBox
            // 
            this.prpddPolarChart13RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart13RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart13RadGroupBox.Controls.Add(this.prpddPolarChart13PDIValueRadLabel);
            this.prpddPolarChart13RadGroupBox.Controls.Add(this.radLabel36);
            this.prpddPolarChart13RadGroupBox.Controls.Add(this.radLabel39);
            this.prpddPolarChart13RadGroupBox.Controls.Add(this.prpddPolarChart13PhaseShiftRadDropDownList);
            this.prpddPolarChart13RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart13RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart13RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart13RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart13RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart13RadGroupBox.HeaderText = "";
            this.prpddPolarChart13RadGroupBox.Location = new System.Drawing.Point(393, 514);
            this.prpddPolarChart13RadGroupBox.Name = "prpddPolarChart13RadGroupBox";
            this.prpddPolarChart13RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart13RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart13RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart13RadGroupBox.TabIndex = 37;
            this.prpddPolarChart13RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart13PDIValueRadLabel
            // 
            this.prpddPolarChart13PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart13PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart13PDIValueRadLabel.Name = "prpddPolarChart13PDIValueRadLabel";
            this.prpddPolarChart13PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart13PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart13PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel36
            // 
            this.radLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel36.Location = new System.Drawing.Point(1, 5);
            this.radLabel36.Name = "radLabel36";
            this.radLabel36.Size = new System.Drawing.Size(32, 16);
            this.radLabel36.TabIndex = 2;
            this.radLabel36.Text = "PDI=";
            // 
            // radLabel39
            // 
            this.radLabel39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel39.Location = new System.Drawing.Point(83, 5);
            this.radLabel39.Name = "radLabel39";
            this.radLabel39.Size = new System.Drawing.Size(23, 16);
            this.radLabel39.TabIndex = 1;
            this.radLabel39.Text = "Ph.";
            // 
            // prpddPolarChart13PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart13PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart13PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart13PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart13PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart13PhaseShiftRadDropDownList.Name = "prpddPolarChart13PhaseShiftRadDropDownList";
            this.prpddPolarChart13PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart13PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart13PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart13PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart13PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart13PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart12RadGroupBox
            // 
            this.prpddPolarChart12RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart12RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart12RadGroupBox.Controls.Add(this.prpddPolarChart12PDIValueRadLabel);
            this.prpddPolarChart12RadGroupBox.Controls.Add(this.radLabel46);
            this.prpddPolarChart12RadGroupBox.Controls.Add(this.radLabel48);
            this.prpddPolarChart12RadGroupBox.Controls.Add(this.prpddPolarChart12PhaseShiftRadDropDownList);
            this.prpddPolarChart12RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart12RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart12RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart12RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart12RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart12RadGroupBox.HeaderText = "";
            this.prpddPolarChart12RadGroupBox.Location = new System.Drawing.Point(198, 514);
            this.prpddPolarChart12RadGroupBox.Name = "prpddPolarChart12RadGroupBox";
            this.prpddPolarChart12RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart12RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart12RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart12RadGroupBox.TabIndex = 36;
            this.prpddPolarChart12RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart12PDIValueRadLabel
            // 
            this.prpddPolarChart12PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart12PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart12PDIValueRadLabel.Name = "prpddPolarChart12PDIValueRadLabel";
            this.prpddPolarChart12PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart12PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart12PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel46
            // 
            this.radLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel46.Location = new System.Drawing.Point(1, 5);
            this.radLabel46.Name = "radLabel46";
            this.radLabel46.Size = new System.Drawing.Size(32, 16);
            this.radLabel46.TabIndex = 2;
            this.radLabel46.Text = "PDI=";
            // 
            // radLabel48
            // 
            this.radLabel48.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel48.Location = new System.Drawing.Point(83, 5);
            this.radLabel48.Name = "radLabel48";
            this.radLabel48.Size = new System.Drawing.Size(23, 16);
            this.radLabel48.TabIndex = 1;
            this.radLabel48.Text = "Ph.";
            // 
            // prpddPolarChart12PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart12PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart12PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart12PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart12PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart12PhaseShiftRadDropDownList.Name = "prpddPolarChart12PhaseShiftRadDropDownList";
            this.prpddPolarChart12PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart12PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart12PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart12PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart12PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart12PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart11RadGroupBox
            // 
            this.prpddPolarChart11RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart11RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart11RadGroupBox.Controls.Add(this.prpddPolarChart11PDIValueRadLabel);
            this.prpddPolarChart11RadGroupBox.Controls.Add(this.radLabel51);
            this.prpddPolarChart11RadGroupBox.Controls.Add(this.radLabel52);
            this.prpddPolarChart11RadGroupBox.Controls.Add(this.prpddPolarChart11PhaseShiftRadDropDownList);
            this.prpddPolarChart11RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart11RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart11RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart11RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart11RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart11RadGroupBox.HeaderText = "";
            this.prpddPolarChart11RadGroupBox.Location = new System.Drawing.Point(3, 514);
            this.prpddPolarChart11RadGroupBox.Name = "prpddPolarChart11RadGroupBox";
            this.prpddPolarChart11RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart11RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart11RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart11RadGroupBox.TabIndex = 35;
            this.prpddPolarChart11RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart11PDIValueRadLabel
            // 
            this.prpddPolarChart11PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart11PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart11PDIValueRadLabel.Name = "prpddPolarChart11PDIValueRadLabel";
            this.prpddPolarChart11PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart11PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart11PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel51
            // 
            this.radLabel51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel51.Location = new System.Drawing.Point(1, 5);
            this.radLabel51.Name = "radLabel51";
            this.radLabel51.Size = new System.Drawing.Size(32, 16);
            this.radLabel51.TabIndex = 2;
            this.radLabel51.Text = "PDI=";
            // 
            // radLabel52
            // 
            this.radLabel52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel52.Location = new System.Drawing.Point(83, 5);
            this.radLabel52.Name = "radLabel52";
            this.radLabel52.Size = new System.Drawing.Size(23, 16);
            this.radLabel52.TabIndex = 1;
            this.radLabel52.Text = "Ph.";
            // 
            // prpddPolarChart11PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart11PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart11PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart11PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart11PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart11PhaseShiftRadDropDownList.Name = "prpddPolarChart11PhaseShiftRadDropDownList";
            this.prpddPolarChart11PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart11PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart11PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart11PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart11PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart11PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart10RadGroupBox
            // 
            this.prpddPolarChart10RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart10RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart10RadGroupBox.Controls.Add(this.prpddPolarChart10PDIValueRadLabel);
            this.prpddPolarChart10RadGroupBox.Controls.Add(this.radLabel55);
            this.prpddPolarChart10RadGroupBox.Controls.Add(this.radLabel57);
            this.prpddPolarChart10RadGroupBox.Controls.Add(this.prpddPolarChart10PhaseShiftRadDropDownList);
            this.prpddPolarChart10RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart10RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart10RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart10RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart10RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart10RadGroupBox.HeaderText = "";
            this.prpddPolarChart10RadGroupBox.Location = new System.Drawing.Point(783, 334);
            this.prpddPolarChart10RadGroupBox.Name = "prpddPolarChart10RadGroupBox";
            this.prpddPolarChart10RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart10RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart10RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart10RadGroupBox.TabIndex = 34;
            this.prpddPolarChart10RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart10PDIValueRadLabel
            // 
            this.prpddPolarChart10PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart10PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart10PDIValueRadLabel.Name = "prpddPolarChart10PDIValueRadLabel";
            this.prpddPolarChart10PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart10PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart10PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel55
            // 
            this.radLabel55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel55.Location = new System.Drawing.Point(1, 5);
            this.radLabel55.Name = "radLabel55";
            this.radLabel55.Size = new System.Drawing.Size(32, 16);
            this.radLabel55.TabIndex = 2;
            this.radLabel55.Text = "PDI=";
            // 
            // radLabel57
            // 
            this.radLabel57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel57.Location = new System.Drawing.Point(83, 5);
            this.radLabel57.Name = "radLabel57";
            this.radLabel57.Size = new System.Drawing.Size(23, 16);
            this.radLabel57.TabIndex = 1;
            this.radLabel57.Text = "Ph.";
            // 
            // prpddPolarChart10PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart10PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart10PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart10PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart10PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart10PhaseShiftRadDropDownList.Name = "prpddPolarChart10PhaseShiftRadDropDownList";
            this.prpddPolarChart10PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart10PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart10PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart10PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart10PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart10PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart9RadGroupBox
            // 
            this.prpddPolarChart9RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart9RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart9RadGroupBox.Controls.Add(this.prpddPolarChart9PDIValueRadLabel);
            this.prpddPolarChart9RadGroupBox.Controls.Add(this.radLabel60);
            this.prpddPolarChart9RadGroupBox.Controls.Add(this.radLabel61);
            this.prpddPolarChart9RadGroupBox.Controls.Add(this.prpddPolarChart9PhaseShiftRadDropDownList);
            this.prpddPolarChart9RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart9RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart9RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart9RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart9RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart9RadGroupBox.HeaderText = "";
            this.prpddPolarChart9RadGroupBox.Location = new System.Drawing.Point(588, 334);
            this.prpddPolarChart9RadGroupBox.Name = "prpddPolarChart9RadGroupBox";
            this.prpddPolarChart9RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart9RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart9RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart9RadGroupBox.TabIndex = 33;
            this.prpddPolarChart9RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart9PDIValueRadLabel
            // 
            this.prpddPolarChart9PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart9PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart9PDIValueRadLabel.Name = "prpddPolarChart9PDIValueRadLabel";
            this.prpddPolarChart9PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart9PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart9PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel60
            // 
            this.radLabel60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel60.Location = new System.Drawing.Point(1, 5);
            this.radLabel60.Name = "radLabel60";
            this.radLabel60.Size = new System.Drawing.Size(32, 16);
            this.radLabel60.TabIndex = 2;
            this.radLabel60.Text = "PDI=";
            // 
            // radLabel61
            // 
            this.radLabel61.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel61.Location = new System.Drawing.Point(83, 5);
            this.radLabel61.Name = "radLabel61";
            this.radLabel61.Size = new System.Drawing.Size(23, 16);
            this.radLabel61.TabIndex = 1;
            this.radLabel61.Text = "Ph.";
            // 
            // prpddPolarChart9PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart9PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart9PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart9PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart9PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart9PhaseShiftRadDropDownList.Name = "prpddPolarChart9PhaseShiftRadDropDownList";
            this.prpddPolarChart9PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart9PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart9PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart9PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart9PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart9PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart8RadGroupBox
            // 
            this.prpddPolarChart8RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart8RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart8RadGroupBox.Controls.Add(this.prpddPolarChart8PDIValueRadLabel);
            this.prpddPolarChart8RadGroupBox.Controls.Add(this.radLabel64);
            this.prpddPolarChart8RadGroupBox.Controls.Add(this.radLabel66);
            this.prpddPolarChart8RadGroupBox.Controls.Add(this.prpddPolarChart8PhaseShiftRadDropDownList);
            this.prpddPolarChart8RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart8RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart8RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart8RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart8RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart8RadGroupBox.HeaderText = "";
            this.prpddPolarChart8RadGroupBox.Location = new System.Drawing.Point(393, 334);
            this.prpddPolarChart8RadGroupBox.Name = "prpddPolarChart8RadGroupBox";
            this.prpddPolarChart8RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart8RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart8RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart8RadGroupBox.TabIndex = 32;
            this.prpddPolarChart8RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart8PDIValueRadLabel
            // 
            this.prpddPolarChart8PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart8PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart8PDIValueRadLabel.Name = "prpddPolarChart8PDIValueRadLabel";
            this.prpddPolarChart8PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart8PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart8PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel64
            // 
            this.radLabel64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel64.Location = new System.Drawing.Point(1, 5);
            this.radLabel64.Name = "radLabel64";
            this.radLabel64.Size = new System.Drawing.Size(32, 16);
            this.radLabel64.TabIndex = 2;
            this.radLabel64.Text = "PDI=";
            // 
            // radLabel66
            // 
            this.radLabel66.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel66.Location = new System.Drawing.Point(83, 5);
            this.radLabel66.Name = "radLabel66";
            this.radLabel66.Size = new System.Drawing.Size(23, 16);
            this.radLabel66.TabIndex = 1;
            this.radLabel66.Text = "Ph.";
            // 
            // prpddPolarChart8PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart8PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart8PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart8PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart8PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart8PhaseShiftRadDropDownList.Name = "prpddPolarChart8PhaseShiftRadDropDownList";
            this.prpddPolarChart8PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart8PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart8PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart8PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart8PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart8PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart7RadGroupBox
            // 
            this.prpddPolarChart7RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart7RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart7RadGroupBox.Controls.Add(this.prpddPolarChart7PDIValueRadLabel);
            this.prpddPolarChart7RadGroupBox.Controls.Add(this.radLabel69);
            this.prpddPolarChart7RadGroupBox.Controls.Add(this.radLabel70);
            this.prpddPolarChart7RadGroupBox.Controls.Add(this.prpddPolarChart7PhaseShiftRadDropDownList);
            this.prpddPolarChart7RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart7RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart7RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart7RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart7RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart7RadGroupBox.HeaderText = "";
            this.prpddPolarChart7RadGroupBox.Location = new System.Drawing.Point(198, 334);
            this.prpddPolarChart7RadGroupBox.Name = "prpddPolarChart7RadGroupBox";
            this.prpddPolarChart7RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart7RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart7RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart7RadGroupBox.TabIndex = 31;
            this.prpddPolarChart7RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart7PDIValueRadLabel
            // 
            this.prpddPolarChart7PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart7PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart7PDIValueRadLabel.Name = "prpddPolarChart7PDIValueRadLabel";
            this.prpddPolarChart7PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart7PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart7PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel69
            // 
            this.radLabel69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel69.Location = new System.Drawing.Point(1, 5);
            this.radLabel69.Name = "radLabel69";
            this.radLabel69.Size = new System.Drawing.Size(32, 16);
            this.radLabel69.TabIndex = 2;
            this.radLabel69.Text = "PDI=";
            // 
            // radLabel70
            // 
            this.radLabel70.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel70.Location = new System.Drawing.Point(83, 5);
            this.radLabel70.Name = "radLabel70";
            this.radLabel70.Size = new System.Drawing.Size(23, 16);
            this.radLabel70.TabIndex = 1;
            this.radLabel70.Text = "Ph.";
            // 
            // prpddPolarChart7PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart7PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart7PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart7PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart7PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart7PhaseShiftRadDropDownList.Name = "prpddPolarChart7PhaseShiftRadDropDownList";
            this.prpddPolarChart7PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart7PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart7PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart7PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart7PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart7PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart6RadGroupBox
            // 
            this.prpddPolarChart6RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart6RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart6RadGroupBox.Controls.Add(this.prpddPolarChart6PDIValueRadLabel);
            this.prpddPolarChart6RadGroupBox.Controls.Add(this.radLabel73);
            this.prpddPolarChart6RadGroupBox.Controls.Add(this.radLabel75);
            this.prpddPolarChart6RadGroupBox.Controls.Add(this.prpddPolarChart6PhaseShiftRadDropDownList);
            this.prpddPolarChart6RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart6RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart6RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart6RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart6RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart6RadGroupBox.HeaderText = "";
            this.prpddPolarChart6RadGroupBox.Location = new System.Drawing.Point(3, 334);
            this.prpddPolarChart6RadGroupBox.Name = "prpddPolarChart6RadGroupBox";
            this.prpddPolarChart6RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart6RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart6RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart6RadGroupBox.TabIndex = 30;
            this.prpddPolarChart6RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart6PDIValueRadLabel
            // 
            this.prpddPolarChart6PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart6PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart6PDIValueRadLabel.Name = "prpddPolarChart6PDIValueRadLabel";
            this.prpddPolarChart6PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart6PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart6PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel73
            // 
            this.radLabel73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel73.Location = new System.Drawing.Point(1, 5);
            this.radLabel73.Name = "radLabel73";
            this.radLabel73.Size = new System.Drawing.Size(32, 16);
            this.radLabel73.TabIndex = 2;
            this.radLabel73.Text = "PDI=";
            // 
            // radLabel75
            // 
            this.radLabel75.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel75.Location = new System.Drawing.Point(83, 5);
            this.radLabel75.Name = "radLabel75";
            this.radLabel75.Size = new System.Drawing.Size(23, 16);
            this.radLabel75.TabIndex = 1;
            this.radLabel75.Text = "Ph.";
            // 
            // prpddPolarChart6PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart6PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart6PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart6PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart6PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart6PhaseShiftRadDropDownList.Name = "prpddPolarChart6PhaseShiftRadDropDownList";
            this.prpddPolarChart6PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart6PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart6PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart6PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart6PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart6PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart5RadGroupBox
            // 
            this.prpddPolarChart5RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart5RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart5RadGroupBox.Controls.Add(this.prpddPolarChart5PDIValueRadLabel);
            this.prpddPolarChart5RadGroupBox.Controls.Add(this.radLabel77);
            this.prpddPolarChart5RadGroupBox.Controls.Add(this.radLabel78);
            this.prpddPolarChart5RadGroupBox.Controls.Add(this.prpddPolarChart5PhaseShiftRadDropDownList);
            this.prpddPolarChart5RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart5RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart5RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart5RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart5RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart5RadGroupBox.HeaderText = "";
            this.prpddPolarChart5RadGroupBox.Location = new System.Drawing.Point(783, 154);
            this.prpddPolarChart5RadGroupBox.Name = "prpddPolarChart5RadGroupBox";
            this.prpddPolarChart5RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart5RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart5RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart5RadGroupBox.TabIndex = 29;
            this.prpddPolarChart5RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart5PDIValueRadLabel
            // 
            this.prpddPolarChart5PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart5PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart5PDIValueRadLabel.Name = "prpddPolarChart5PDIValueRadLabel";
            this.prpddPolarChart5PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart5PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart5PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel77
            // 
            this.radLabel77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel77.Location = new System.Drawing.Point(1, 5);
            this.radLabel77.Name = "radLabel77";
            this.radLabel77.Size = new System.Drawing.Size(32, 16);
            this.radLabel77.TabIndex = 2;
            this.radLabel77.Text = "PDI=";
            // 
            // radLabel78
            // 
            this.radLabel78.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel78.Location = new System.Drawing.Point(83, 5);
            this.radLabel78.Name = "radLabel78";
            this.radLabel78.Size = new System.Drawing.Size(23, 16);
            this.radLabel78.TabIndex = 1;
            this.radLabel78.Text = "Ph.";
            // 
            // prpddPolarChart5PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart5PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart5PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart5PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart5PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart5PhaseShiftRadDropDownList.Name = "prpddPolarChart5PhaseShiftRadDropDownList";
            this.prpddPolarChart5PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart5PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart5PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart5PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart5PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart5PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart4RadGroupBox
            // 
            this.prpddPolarChart4RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart4RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart4RadGroupBox.Controls.Add(this.prpddPolarChart4PDIValueRadLabel);
            this.prpddPolarChart4RadGroupBox.Controls.Add(this.radLabel80);
            this.prpddPolarChart4RadGroupBox.Controls.Add(this.radLabel81);
            this.prpddPolarChart4RadGroupBox.Controls.Add(this.prpddPolarChart4PhaseShiftRadDropDownList);
            this.prpddPolarChart4RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart4RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart4RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart4RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart4RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart4RadGroupBox.HeaderText = "";
            this.prpddPolarChart4RadGroupBox.Location = new System.Drawing.Point(588, 154);
            this.prpddPolarChart4RadGroupBox.Name = "prpddPolarChart4RadGroupBox";
            this.prpddPolarChart4RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart4RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart4RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart4RadGroupBox.TabIndex = 28;
            this.prpddPolarChart4RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart4PDIValueRadLabel
            // 
            this.prpddPolarChart4PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart4PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart4PDIValueRadLabel.Name = "prpddPolarChart4PDIValueRadLabel";
            this.prpddPolarChart4PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart4PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart4PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel80
            // 
            this.radLabel80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel80.Location = new System.Drawing.Point(1, 5);
            this.radLabel80.Name = "radLabel80";
            this.radLabel80.Size = new System.Drawing.Size(32, 16);
            this.radLabel80.TabIndex = 2;
            this.radLabel80.Text = "PDI=";
            // 
            // radLabel81
            // 
            this.radLabel81.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel81.Location = new System.Drawing.Point(83, 5);
            this.radLabel81.Name = "radLabel81";
            this.radLabel81.Size = new System.Drawing.Size(23, 16);
            this.radLabel81.TabIndex = 1;
            this.radLabel81.Text = "Ph.";
            // 
            // prpddPolarChart4PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart4PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart4PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart4PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart4PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart4PhaseShiftRadDropDownList.Name = "prpddPolarChart4PhaseShiftRadDropDownList";
            this.prpddPolarChart4PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart4PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart4PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart4PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart4PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart4PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart3RadGroupBox
            // 
            this.prpddPolarChart3RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart3RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart3RadGroupBox.Controls.Add(this.prpddPolarChart3PDIValueRadLabel);
            this.prpddPolarChart3RadGroupBox.Controls.Add(this.radLabel83);
            this.prpddPolarChart3RadGroupBox.Controls.Add(this.radLabel84);
            this.prpddPolarChart3RadGroupBox.Controls.Add(this.prpddPolarChart3PhaseShiftRadDropDownList);
            this.prpddPolarChart3RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart3RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart3RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart3RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart3RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart3RadGroupBox.HeaderText = "";
            this.prpddPolarChart3RadGroupBox.Location = new System.Drawing.Point(393, 154);
            this.prpddPolarChart3RadGroupBox.Name = "prpddPolarChart3RadGroupBox";
            this.prpddPolarChart3RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart3RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart3RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart3RadGroupBox.TabIndex = 27;
            this.prpddPolarChart3RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart3PDIValueRadLabel
            // 
            this.prpddPolarChart3PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart3PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart3PDIValueRadLabel.Name = "prpddPolarChart3PDIValueRadLabel";
            this.prpddPolarChart3PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart3PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart3PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel83
            // 
            this.radLabel83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel83.Location = new System.Drawing.Point(1, 5);
            this.radLabel83.Name = "radLabel83";
            this.radLabel83.Size = new System.Drawing.Size(32, 16);
            this.radLabel83.TabIndex = 2;
            this.radLabel83.Text = "PDI=";
            // 
            // radLabel84
            // 
            this.radLabel84.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel84.Location = new System.Drawing.Point(83, 5);
            this.radLabel84.Name = "radLabel84";
            this.radLabel84.Size = new System.Drawing.Size(23, 16);
            this.radLabel84.TabIndex = 1;
            this.radLabel84.Text = "Ph.";
            // 
            // prpddPolarChart3PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart3PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart3PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart3PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart3PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart3PhaseShiftRadDropDownList.Name = "prpddPolarChart3PhaseShiftRadDropDownList";
            this.prpddPolarChart3PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart3PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart3PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart3PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart3PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart3PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart2RadGroupBox
            // 
            this.prpddPolarChart2RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart2RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart2RadGroupBox.Controls.Add(this.prpddPolarChart2PDIValueRadLabel);
            this.prpddPolarChart2RadGroupBox.Controls.Add(this.radLabel86);
            this.prpddPolarChart2RadGroupBox.Controls.Add(this.radLabel87);
            this.prpddPolarChart2RadGroupBox.Controls.Add(this.prpddPolarChart2PhaseShiftRadDropDownList);
            this.prpddPolarChart2RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart2RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart2RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart2RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart2RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart2RadGroupBox.HeaderText = "";
            this.prpddPolarChart2RadGroupBox.Location = new System.Drawing.Point(198, 154);
            this.prpddPolarChart2RadGroupBox.Name = "prpddPolarChart2RadGroupBox";
            this.prpddPolarChart2RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart2RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart2RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart2RadGroupBox.TabIndex = 26;
            this.prpddPolarChart2RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart2PDIValueRadLabel
            // 
            this.prpddPolarChart2PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart2PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart2PDIValueRadLabel.Name = "prpddPolarChart2PDIValueRadLabel";
            this.prpddPolarChart2PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart2PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart2PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel86
            // 
            this.radLabel86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel86.Location = new System.Drawing.Point(1, 5);
            this.radLabel86.Name = "radLabel86";
            this.radLabel86.Size = new System.Drawing.Size(32, 16);
            this.radLabel86.TabIndex = 2;
            this.radLabel86.Text = "PDI=";
            // 
            // radLabel87
            // 
            this.radLabel87.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel87.Location = new System.Drawing.Point(83, 5);
            this.radLabel87.Name = "radLabel87";
            this.radLabel87.Size = new System.Drawing.Size(23, 16);
            this.radLabel87.TabIndex = 1;
            this.radLabel87.Text = "Ph.";
            // 
            // prpddPolarChart2PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart2PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart2PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart2PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart2PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart2PhaseShiftRadDropDownList.Name = "prpddPolarChart2PhaseShiftRadDropDownList";
            this.prpddPolarChart2PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart2PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart2PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart2PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart2PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart2PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart1RadGroupBox
            // 
            this.prpddPolarChart1RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChart1RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarChart1RadGroupBox.Controls.Add(this.prpddPolarChart1PDIValueRadLabel);
            this.prpddPolarChart1RadGroupBox.Controls.Add(this.radLabel89);
            this.prpddPolarChart1RadGroupBox.Controls.Add(this.radLabel90);
            this.prpddPolarChart1RadGroupBox.Controls.Add(this.prpddPolarChart1PhaseShiftRadDropDownList);
            this.prpddPolarChart1RadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChart1RadGroupBox.FooterImageKey = "";
            this.prpddPolarChart1RadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChart1RadGroupBox.HeaderImageKey = "";
            this.prpddPolarChart1RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChart1RadGroupBox.HeaderText = "";
            this.prpddPolarChart1RadGroupBox.Location = new System.Drawing.Point(3, 154);
            this.prpddPolarChart1RadGroupBox.Name = "prpddPolarChart1RadGroupBox";
            this.prpddPolarChart1RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChart1RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChart1RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.prpddPolarChart1RadGroupBox.TabIndex = 25;
            this.prpddPolarChart1RadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarChart1PDIValueRadLabel
            // 
            this.prpddPolarChart1PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart1PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.prpddPolarChart1PDIValueRadLabel.Name = "prpddPolarChart1PDIValueRadLabel";
            this.prpddPolarChart1PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.prpddPolarChart1PDIValueRadLabel.TabIndex = 3;
            this.prpddPolarChart1PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel89
            // 
            this.radLabel89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel89.Location = new System.Drawing.Point(1, 5);
            this.radLabel89.Name = "radLabel89";
            this.radLabel89.Size = new System.Drawing.Size(32, 16);
            this.radLabel89.TabIndex = 2;
            this.radLabel89.Text = "PDI=";
            // 
            // radLabel90
            // 
            this.radLabel90.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel90.Location = new System.Drawing.Point(83, 5);
            this.radLabel90.Name = "radLabel90";
            this.radLabel90.Size = new System.Drawing.Size(23, 16);
            this.radLabel90.TabIndex = 1;
            this.radLabel90.Text = "Ph.";
            // 
            // prpddPolarChart1PhaseShiftRadDropDownList
            // 
            this.prpddPolarChart1PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prpddPolarChart1PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarChart1PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChart1PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.prpddPolarChart1PhaseShiftRadDropDownList.Name = "prpddPolarChart1PhaseShiftRadDropDownList";
            this.prpddPolarChart1PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarChart1PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.prpddPolarChart1PhaseShiftRadDropDownList.TabIndex = 0;
            this.prpddPolarChart1PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarChart1PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarChart1PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChart15WinChartViewer
            // 
            this.prpddPolarChart15WinChartViewer.Location = new System.Drawing.Point(783, 363);
            this.prpddPolarChart15WinChartViewer.Name = "prpddPolarChart15WinChartViewer";
            this.prpddPolarChart15WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart15WinChartViewer.TabIndex = 24;
            this.prpddPolarChart15WinChartViewer.TabStop = false;
            this.prpddPolarChart15WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart15CopyGraph_Click);
            // 
            // prpddPolarChart14WinChartViewer
            // 
            this.prpddPolarChart14WinChartViewer.Location = new System.Drawing.Point(588, 363);
            this.prpddPolarChart14WinChartViewer.Name = "prpddPolarChart14WinChartViewer";
            this.prpddPolarChart14WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart14WinChartViewer.TabIndex = 23;
            this.prpddPolarChart14WinChartViewer.TabStop = false;
            this.prpddPolarChart14WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart14CopyGraph_Click);
            // 
            // prpddPolarChart13WinChartViewer
            // 
            this.prpddPolarChart13WinChartViewer.Location = new System.Drawing.Point(393, 363);
            this.prpddPolarChart13WinChartViewer.Name = "prpddPolarChart13WinChartViewer";
            this.prpddPolarChart13WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart13WinChartViewer.TabIndex = 22;
            this.prpddPolarChart13WinChartViewer.TabStop = false;
            this.prpddPolarChart13WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart13CopyGraph_Click);
            // 
            // prpddPolarChart12WinChartViewer
            // 
            this.prpddPolarChart12WinChartViewer.Location = new System.Drawing.Point(198, 363);
            this.prpddPolarChart12WinChartViewer.Name = "prpddPolarChart12WinChartViewer";
            this.prpddPolarChart12WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart12WinChartViewer.TabIndex = 21;
            this.prpddPolarChart12WinChartViewer.TabStop = false;
            this.prpddPolarChart12WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart12CopyGraph_Click);
            // 
            // prpddPolarChart10WinChartViewer
            // 
            this.prpddPolarChart10WinChartViewer.Location = new System.Drawing.Point(783, 183);
            this.prpddPolarChart10WinChartViewer.Name = "prpddPolarChart10WinChartViewer";
            this.prpddPolarChart10WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart10WinChartViewer.TabIndex = 20;
            this.prpddPolarChart10WinChartViewer.TabStop = false;
            this.prpddPolarChart10WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart10CopyGraph_Click);
            // 
            // prpddPolarChart9WinChartViewer
            // 
            this.prpddPolarChart9WinChartViewer.Location = new System.Drawing.Point(588, 183);
            this.prpddPolarChart9WinChartViewer.Name = "prpddPolarChart9WinChartViewer";
            this.prpddPolarChart9WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart9WinChartViewer.TabIndex = 19;
            this.prpddPolarChart9WinChartViewer.TabStop = false;
            this.prpddPolarChart9WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart9CopyGraph_Click);
            // 
            // prpddPolarChart8WinChartViewer
            // 
            this.prpddPolarChart8WinChartViewer.Location = new System.Drawing.Point(393, 183);
            this.prpddPolarChart8WinChartViewer.Name = "prpddPolarChart8WinChartViewer";
            this.prpddPolarChart8WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart8WinChartViewer.TabIndex = 18;
            this.prpddPolarChart8WinChartViewer.TabStop = false;
            this.prpddPolarChart8WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart8CopyGraph_Click);
            // 
            // prpddPolarChart7WinChartViewer
            // 
            this.prpddPolarChart7WinChartViewer.Location = new System.Drawing.Point(198, 183);
            this.prpddPolarChart7WinChartViewer.Name = "prpddPolarChart7WinChartViewer";
            this.prpddPolarChart7WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart7WinChartViewer.TabIndex = 17;
            this.prpddPolarChart7WinChartViewer.TabStop = false;
            this.prpddPolarChart7WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart7CopyGraph_Click);
            // 
            // prpddPolarChart5WinChartViewer
            // 
            this.prpddPolarChart5WinChartViewer.Location = new System.Drawing.Point(783, 3);
            this.prpddPolarChart5WinChartViewer.Name = "prpddPolarChart5WinChartViewer";
            this.prpddPolarChart5WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart5WinChartViewer.TabIndex = 15;
            this.prpddPolarChart5WinChartViewer.TabStop = false;
            this.prpddPolarChart5WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart5CopyGraph_Click);
            // 
            // prpddPolarChart4WinChartViewer
            // 
            this.prpddPolarChart4WinChartViewer.Location = new System.Drawing.Point(588, 3);
            this.prpddPolarChart4WinChartViewer.Name = "prpddPolarChart4WinChartViewer";
            this.prpddPolarChart4WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart4WinChartViewer.TabIndex = 14;
            this.prpddPolarChart4WinChartViewer.TabStop = false;
            this.prpddPolarChart4WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart4CopyGraph_Click);
            // 
            // prpddPolarChart3WinChartViewer
            // 
            this.prpddPolarChart3WinChartViewer.Location = new System.Drawing.Point(393, 3);
            this.prpddPolarChart3WinChartViewer.Name = "prpddPolarChart3WinChartViewer";
            this.prpddPolarChart3WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart3WinChartViewer.TabIndex = 11;
            this.prpddPolarChart3WinChartViewer.TabStop = false;
            this.prpddPolarChart3WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart3CopyGraph_Click);
            // 
            // prpddPolarChart2WinChartViewer
            // 
            this.prpddPolarChart2WinChartViewer.Location = new System.Drawing.Point(198, 3);
            this.prpddPolarChart2WinChartViewer.Name = "prpddPolarChart2WinChartViewer";
            this.prpddPolarChart2WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart2WinChartViewer.TabIndex = 8;
            this.prpddPolarChart2WinChartViewer.TabStop = false;
            this.prpddPolarChart2WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart2CopyGraph_Click);
            // 
            // prpddPolarChart11WinChartViewer
            // 
            this.prpddPolarChart11WinChartViewer.Location = new System.Drawing.Point(3, 363);
            this.prpddPolarChart11WinChartViewer.Name = "prpddPolarChart11WinChartViewer";
            this.prpddPolarChart11WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart11WinChartViewer.TabIndex = 7;
            this.prpddPolarChart11WinChartViewer.TabStop = false;
            this.prpddPolarChart11WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart11CopyGraph_Click);
            // 
            // prpddPolarChart6WinChartViewer
            // 
            this.prpddPolarChart6WinChartViewer.Location = new System.Drawing.Point(3, 183);
            this.prpddPolarChart6WinChartViewer.Name = "prpddPolarChart6WinChartViewer";
            this.prpddPolarChart6WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart6WinChartViewer.TabIndex = 6;
            this.prpddPolarChart6WinChartViewer.TabStop = false;
            this.prpddPolarChart6WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart6CopyGraph_Click);
            // 
            // prpddPolarChart1WinChartViewer
            // 
            this.prpddPolarChart1WinChartViewer.Location = new System.Drawing.Point(3, 3);
            this.prpddPolarChart1WinChartViewer.Name = "prpddPolarChart1WinChartViewer";
            this.prpddPolarChart1WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.prpddPolarChart1WinChartViewer.TabIndex = 5;
            this.prpddPolarChart1WinChartViewer.TabStop = false;
            this.prpddPolarChart1WinChartViewer.Click += new System.EventHandler(this.prpddPolarChart1CopyGraph_Click);
            // 
            // prpddPolarDisplaySettingsRadGroupBox
            // 
            this.prpddPolarDisplaySettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarDisplaySettingsRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prpddPolarDisplaySettingsRadGroupBox.Controls.Add(this.showRadialLabelsRadCheckBox);
            this.prpddPolarDisplaySettingsRadGroupBox.Controls.Add(this.prpddPolarInitialPhaseRadButton);
            this.prpddPolarDisplaySettingsRadGroupBox.Controls.Add(this.prpddPolarAmplitudeRadGroupBox);
            this.prpddPolarDisplaySettingsRadGroupBox.Controls.Add(this.prpddPolarScaleRadGroupBox);
            this.prpddPolarDisplaySettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarDisplaySettingsRadGroupBox.FooterImageIndex = -1;
            this.prpddPolarDisplaySettingsRadGroupBox.FooterImageKey = "";
            this.prpddPolarDisplaySettingsRadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarDisplaySettingsRadGroupBox.HeaderImageKey = "";
            this.prpddPolarDisplaySettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarDisplaySettingsRadGroupBox.HeaderText = "Display Settings";
            this.prpddPolarDisplaySettingsRadGroupBox.Location = new System.Drawing.Point(125, 573);
            this.prpddPolarDisplaySettingsRadGroupBox.Name = "prpddPolarDisplaySettingsRadGroupBox";
            this.prpddPolarDisplaySettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarDisplaySettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarDisplaySettingsRadGroupBox.Size = new System.Drawing.Size(265, 140);
            this.prpddPolarDisplaySettingsRadGroupBox.TabIndex = 87;
            this.prpddPolarDisplaySettingsRadGroupBox.Text = "Display Settings";
            this.prpddPolarDisplaySettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // showRadialLabelsRadCheckBox
            // 
            this.showRadialLabelsRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showRadialLabelsRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.showRadialLabelsRadCheckBox.Location = new System.Drawing.Point(14, 23);
            this.showRadialLabelsRadCheckBox.Name = "showRadialLabelsRadCheckBox";
            // 
            // 
            // 
            this.showRadialLabelsRadCheckBox.RootElement.StretchHorizontally = true;
            this.showRadialLabelsRadCheckBox.RootElement.StretchVertically = true;
            this.showRadialLabelsRadCheckBox.Size = new System.Drawing.Size(120, 16);
            this.showRadialLabelsRadCheckBox.TabIndex = 93;
            this.showRadialLabelsRadCheckBox.Text = "Show Radial Labels";
            this.showRadialLabelsRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.showRadialLabelsRadCheckBox_ToggleStateChanged);
            // 
            // prpddPolarInitialPhaseRadButton
            // 
            this.prpddPolarInitialPhaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarInitialPhaseRadButton.Location = new System.Drawing.Point(14, 102);
            this.prpddPolarInitialPhaseRadButton.Name = "prpddPolarInitialPhaseRadButton";
            this.prpddPolarInitialPhaseRadButton.Size = new System.Drawing.Size(158, 31);
            this.prpddPolarInitialPhaseRadButton.TabIndex = 2;
            this.prpddPolarInitialPhaseRadButton.Text = "Common Phase Shift";
            this.prpddPolarInitialPhaseRadButton.ThemeName = "Office2007Black";
            this.prpddPolarInitialPhaseRadButton.Click += new System.EventHandler(this.prpddPolarInitialPhaseRadButton_Click);
            // 
            // prpddPolarAmplitudeRadGroupBox
            // 
            this.prpddPolarAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarAmplitudeRadGroupBox.Controls.Add(this.prpddPolarPcRadRadioButton);
            this.prpddPolarAmplitudeRadGroupBox.Controls.Add(this.prpddPolarVRadRadioButton);
            this.prpddPolarAmplitudeRadGroupBox.Controls.Add(this.prpddPolarDbRadRadioButton);
            this.prpddPolarAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarAmplitudeRadGroupBox.FooterImageIndex = -1;
            this.prpddPolarAmplitudeRadGroupBox.FooterImageKey = "";
            this.prpddPolarAmplitudeRadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarAmplitudeRadGroupBox.HeaderImageKey = "";
            this.prpddPolarAmplitudeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarAmplitudeRadGroupBox.HeaderText = "Amplitude";
            this.prpddPolarAmplitudeRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.prpddPolarAmplitudeRadGroupBox.Location = new System.Drawing.Point(178, 23);
            this.prpddPolarAmplitudeRadGroupBox.Name = "prpddPolarAmplitudeRadGroupBox";
            this.prpddPolarAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarAmplitudeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarAmplitudeRadGroupBox.Size = new System.Drawing.Size(78, 110);
            this.prpddPolarAmplitudeRadGroupBox.TabIndex = 1;
            this.prpddPolarAmplitudeRadGroupBox.Text = "Amplitude";
            this.prpddPolarAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarPcRadRadioButton
            // 
            this.prpddPolarPcRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarPcRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarPcRadRadioButton.Location = new System.Drawing.Point(14, 79);
            this.prpddPolarPcRadRadioButton.Name = "prpddPolarPcRadRadioButton";
            this.prpddPolarPcRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.prpddPolarPcRadRadioButton.TabIndex = 1;
            this.prpddPolarPcRadRadioButton.Text = "pC";
            // 
            // prpddPolarVRadRadioButton
            // 
            this.prpddPolarVRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarVRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarVRadRadioButton.Location = new System.Drawing.Point(14, 51);
            this.prpddPolarVRadRadioButton.Name = "prpddPolarVRadRadioButton";
            this.prpddPolarVRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.prpddPolarVRadRadioButton.TabIndex = 1;
            this.prpddPolarVRadRadioButton.TabStop = true;
            this.prpddPolarVRadRadioButton.Text = "V";
            this.prpddPolarVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.prpddPolarVRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarVRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarDbRadRadioButton
            // 
            this.prpddPolarDbRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarDbRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarDbRadRadioButton.Location = new System.Drawing.Point(14, 27);
            this.prpddPolarDbRadRadioButton.Name = "prpddPolarDbRadRadioButton";
            this.prpddPolarDbRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.prpddPolarDbRadRadioButton.TabIndex = 0;
            this.prpddPolarDbRadRadioButton.Text = "dB";
            this.prpddPolarDbRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarDbRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarScaleRadGroupBox
            // 
            this.prpddPolarScaleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarScaleRadGroupBox.Controls.Add(this.prpddPolarScaleRadDropDownList);
            this.prpddPolarScaleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarScaleRadGroupBox.FooterImageIndex = -1;
            this.prpddPolarScaleRadGroupBox.FooterImageKey = "";
            this.prpddPolarScaleRadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarScaleRadGroupBox.HeaderImageKey = "";
            this.prpddPolarScaleRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarScaleRadGroupBox.HeaderText = "Scale";
            this.prpddPolarScaleRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.prpddPolarScaleRadGroupBox.Location = new System.Drawing.Point(14, 45);
            this.prpddPolarScaleRadGroupBox.Name = "prpddPolarScaleRadGroupBox";
            this.prpddPolarScaleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarScaleRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarScaleRadGroupBox.Size = new System.Drawing.Size(159, 51);
            this.prpddPolarScaleRadGroupBox.TabIndex = 0;
            this.prpddPolarScaleRadGroupBox.Text = "Scale";
            this.prpddPolarScaleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarScaleRadDropDownList
            // 
            this.prpddPolarScaleRadDropDownList.DropDownAnimationEnabled = true;
            this.prpddPolarScaleRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarScaleRadDropDownList.Location = new System.Drawing.Point(13, 21);
            this.prpddPolarScaleRadDropDownList.Name = "prpddPolarScaleRadDropDownList";
            this.prpddPolarScaleRadDropDownList.ShowImageInEditorArea = true;
            this.prpddPolarScaleRadDropDownList.Size = new System.Drawing.Size(133, 18);
            this.prpddPolarScaleRadDropDownList.TabIndex = 0;
            this.prpddPolarScaleRadDropDownList.Text = "radDropDownList1";
            this.prpddPolarScaleRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.prpddPolarScaleRadDropDownList_SelectedIndexChanged);
            // 
            // prpddPolarChartLayoutRadGroupBox
            // 
            this.prpddPolarChartLayoutRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.prpddPolarChartLayoutRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prpddPolarChartLayoutRadGroupBox.Controls.Add(this.prpddPolarFifteenByOneRadRadioButton);
            this.prpddPolarChartLayoutRadGroupBox.Controls.Add(this.prpddPolarEightByTwoRadRadioButton);
            this.prpddPolarChartLayoutRadGroupBox.Controls.Add(this.prpddPolarFiveByThreeRadRadioButton);
            this.prpddPolarChartLayoutRadGroupBox.Controls.Add(this.prpddPolarThreeByFiveRadRadioButton);
            this.prpddPolarChartLayoutRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarChartLayoutRadGroupBox.FooterImageIndex = -1;
            this.prpddPolarChartLayoutRadGroupBox.FooterImageKey = "";
            this.prpddPolarChartLayoutRadGroupBox.HeaderImageIndex = -1;
            this.prpddPolarChartLayoutRadGroupBox.HeaderImageKey = "";
            this.prpddPolarChartLayoutRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.prpddPolarChartLayoutRadGroupBox.HeaderText = "Chart Layout";
            this.prpddPolarChartLayoutRadGroupBox.Location = new System.Drawing.Point(4, 573);
            this.prpddPolarChartLayoutRadGroupBox.Name = "prpddPolarChartLayoutRadGroupBox";
            this.prpddPolarChartLayoutRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.prpddPolarChartLayoutRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.prpddPolarChartLayoutRadGroupBox.Size = new System.Drawing.Size(115, 68);
            this.prpddPolarChartLayoutRadGroupBox.TabIndex = 86;
            this.prpddPolarChartLayoutRadGroupBox.Text = "Chart Layout";
            this.prpddPolarChartLayoutRadGroupBox.ThemeName = "Office2007Black";
            // 
            // prpddPolarFifteenByOneRadRadioButton
            // 
            this.prpddPolarFifteenByOneRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarFifteenByOneRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarFifteenByOneRadRadioButton.Location = new System.Drawing.Point(60, 45);
            this.prpddPolarFifteenByOneRadRadioButton.Name = "prpddPolarFifteenByOneRadRadioButton";
            this.prpddPolarFifteenByOneRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.prpddPolarFifteenByOneRadRadioButton.TabIndex = 3;
            this.prpddPolarFifteenByOneRadRadioButton.Text = "15x1";
            this.prpddPolarFifteenByOneRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarfifteenByOneRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarEightByTwoRadRadioButton
            // 
            this.prpddPolarEightByTwoRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarEightByTwoRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarEightByTwoRadRadioButton.Location = new System.Drawing.Point(60, 24);
            this.prpddPolarEightByTwoRadRadioButton.Name = "prpddPolarEightByTwoRadRadioButton";
            this.prpddPolarEightByTwoRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.prpddPolarEightByTwoRadRadioButton.TabIndex = 2;
            this.prpddPolarEightByTwoRadRadioButton.Text = "8x2";
            this.prpddPolarEightByTwoRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarEightByTwoRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarFiveByThreeRadRadioButton
            // 
            this.prpddPolarFiveByThreeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarFiveByThreeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarFiveByThreeRadRadioButton.Location = new System.Drawing.Point(10, 45);
            this.prpddPolarFiveByThreeRadRadioButton.Name = "prpddPolarFiveByThreeRadRadioButton";
            this.prpddPolarFiveByThreeRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.prpddPolarFiveByThreeRadRadioButton.TabIndex = 1;
            this.prpddPolarFiveByThreeRadRadioButton.Text = "5x3";
            this.prpddPolarFiveByThreeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarFiveByThreeRadRadioButton_ToggleStateChanged);
            // 
            // prpddPolarThreeByFiveRadRadioButton
            // 
            this.prpddPolarThreeByFiveRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prpddPolarThreeByFiveRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.prpddPolarThreeByFiveRadRadioButton.Location = new System.Drawing.Point(10, 24);
            this.prpddPolarThreeByFiveRadRadioButton.Name = "prpddPolarThreeByFiveRadRadioButton";
            this.prpddPolarThreeByFiveRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.prpddPolarThreeByFiveRadRadioButton.TabIndex = 0;
            this.prpddPolarThreeByFiveRadRadioButton.TabStop = true;
            this.prpddPolarThreeByFiveRadRadioButton.Text = "3x5";
            this.prpddPolarThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.prpddPolarThreeByFiveRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.prpddPolarThreeByFiveRadRadioButton_ToggleStateChanged);
            // 
            // matrixRadPageViewPage
            // 
            this.matrixRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.matrixRadPageViewPage.Controls.Add(this.dataReadingDatePrpddTabValueRadLabel);
            this.matrixRadPageViewPage.Controls.Add(this.dataReadingDatePrpddTabTextRadLabel);
            this.matrixRadPageViewPage.Controls.Add(this.matrixRadPanel);
            this.matrixRadPageViewPage.Controls.Add(this.matrixDisplaySettingsRadGroupBox);
            this.matrixRadPageViewPage.Controls.Add(this.matrixChartLayoutRadGroupBox);
            this.matrixRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.matrixRadPageViewPage.Name = "matrixRadPageViewPage";
            this.matrixRadPageViewPage.Size = new System.Drawing.Size(1004, 722);
            this.matrixRadPageViewPage.Text = "PRPDD";
            // 
            // dataReadingDatePrpddTabValueRadLabel
            // 
            this.dataReadingDatePrpddTabValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpddTabValueRadLabel.Location = new System.Drawing.Point(521, 580);
            this.dataReadingDatePrpddTabValueRadLabel.Name = "dataReadingDatePrpddTabValueRadLabel";
            this.dataReadingDatePrpddTabValueRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpddTabValueRadLabel.TabIndex = 25;
            this.dataReadingDatePrpddTabValueRadLabel.Text = "Data Reading Date:";
            // 
            // dataReadingDatePrpddTabTextRadLabel
            // 
            this.dataReadingDatePrpddTabTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataReadingDatePrpddTabTextRadLabel.Location = new System.Drawing.Point(410, 580);
            this.dataReadingDatePrpddTabTextRadLabel.Name = "dataReadingDatePrpddTabTextRadLabel";
            this.dataReadingDatePrpddTabTextRadLabel.Size = new System.Drawing.Size(105, 16);
            this.dataReadingDatePrpddTabTextRadLabel.TabIndex = 24;
            this.dataReadingDatePrpddTabTextRadLabel.Text = "Data Reading Date:";
            // 
            // matrixRadPanel
            // 
            this.matrixRadPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixRadPanel.AutoScroll = true;
            this.matrixRadPanel.Controls.Add(this.matrixChart15RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart14RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart13RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart12RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart11RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart10RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart9RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart8RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart7RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart6RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart5RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart4RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart3RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart2RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart1RadGroupBox);
            this.matrixRadPanel.Controls.Add(this.matrixChart15WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart14WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart13WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart12WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart10WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart9WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart8WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart7WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart5WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart4WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart3WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart2WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart11WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart6WinChartViewer);
            this.matrixRadPanel.Controls.Add(this.matrixChart1WinChartViewer);
            this.matrixRadPanel.Location = new System.Drawing.Point(0, 0);
            this.matrixRadPanel.Name = "matrixRadPanel";
            this.matrixRadPanel.Size = new System.Drawing.Size(1001, 567);
            this.matrixRadPanel.TabIndex = 23;
            // 
            // matrixChart15RadGroupBox
            // 
            this.matrixChart15RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart15RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart15RadGroupBox.Controls.Add(this.matrixChart15PDIValueRadLabel);
            this.matrixChart15RadGroupBox.Controls.Add(this.radLabel43);
            this.matrixChart15RadGroupBox.Controls.Add(this.radLabel44);
            this.matrixChart15RadGroupBox.Controls.Add(this.matrixChart15PhaseShiftRadDropDownList);
            this.matrixChart15RadGroupBox.FooterImageIndex = -1;
            this.matrixChart15RadGroupBox.FooterImageKey = "";
            this.matrixChart15RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart15RadGroupBox.HeaderImageKey = "";
            this.matrixChart15RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart15RadGroupBox.HeaderText = "";
            this.matrixChart15RadGroupBox.Location = new System.Drawing.Point(783, 514);
            this.matrixChart15RadGroupBox.Name = "matrixChart15RadGroupBox";
            this.matrixChart15RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart15RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart15RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart15RadGroupBox.TabIndex = 39;
            this.matrixChart15RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart15PDIValueRadLabel
            // 
            this.matrixChart15PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart15PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart15PDIValueRadLabel.Name = "matrixChart15PDIValueRadLabel";
            this.matrixChart15PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart15PDIValueRadLabel.TabIndex = 3;
            this.matrixChart15PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel43
            // 
            this.radLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel43.Location = new System.Drawing.Point(1, 5);
            this.radLabel43.Name = "radLabel43";
            this.radLabel43.Size = new System.Drawing.Size(32, 16);
            this.radLabel43.TabIndex = 2;
            this.radLabel43.Text = "PDI=";
            // 
            // radLabel44
            // 
            this.radLabel44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel44.Location = new System.Drawing.Point(83, 5);
            this.radLabel44.Name = "radLabel44";
            this.radLabel44.Size = new System.Drawing.Size(23, 16);
            this.radLabel44.TabIndex = 1;
            this.radLabel44.Text = "Ph.";
            // 
            // matrixChart15PhaseShiftRadDropDownList
            // 
            this.matrixChart15PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart15PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart15PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart15PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart15PhaseShiftRadDropDownList.Name = "matrixChart15PhaseShiftRadDropDownList";
            this.matrixChart15PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart15PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart15PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart15PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart15PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart15PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart14RadGroupBox
            // 
            this.matrixChart14RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart14RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart14RadGroupBox.Controls.Add(this.matrixChart14PDIValueRadLabel);
            this.matrixChart14RadGroupBox.Controls.Add(this.radLabel40);
            this.matrixChart14RadGroupBox.Controls.Add(this.radLabel41);
            this.matrixChart14RadGroupBox.Controls.Add(this.matrixChart14PhaseShiftRadDropDownList);
            this.matrixChart14RadGroupBox.FooterImageIndex = -1;
            this.matrixChart14RadGroupBox.FooterImageKey = "";
            this.matrixChart14RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart14RadGroupBox.HeaderImageKey = "";
            this.matrixChart14RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart14RadGroupBox.HeaderText = "";
            this.matrixChart14RadGroupBox.Location = new System.Drawing.Point(588, 514);
            this.matrixChart14RadGroupBox.Name = "matrixChart14RadGroupBox";
            this.matrixChart14RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart14RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart14RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart14RadGroupBox.TabIndex = 38;
            this.matrixChart14RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart14PDIValueRadLabel
            // 
            this.matrixChart14PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart14PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart14PDIValueRadLabel.Name = "matrixChart14PDIValueRadLabel";
            this.matrixChart14PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart14PDIValueRadLabel.TabIndex = 3;
            this.matrixChart14PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel40
            // 
            this.radLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel40.Location = new System.Drawing.Point(1, 5);
            this.radLabel40.Name = "radLabel40";
            this.radLabel40.Size = new System.Drawing.Size(32, 16);
            this.radLabel40.TabIndex = 2;
            this.radLabel40.Text = "PDI=";
            // 
            // radLabel41
            // 
            this.radLabel41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel41.Location = new System.Drawing.Point(83, 5);
            this.radLabel41.Name = "radLabel41";
            this.radLabel41.Size = new System.Drawing.Size(23, 16);
            this.radLabel41.TabIndex = 1;
            this.radLabel41.Text = "Ph.";
            // 
            // matrixChart14PhaseShiftRadDropDownList
            // 
            this.matrixChart14PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart14PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart14PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart14PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart14PhaseShiftRadDropDownList.Name = "matrixChart14PhaseShiftRadDropDownList";
            this.matrixChart14PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart14PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart14PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart14PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart14PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart14PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart13RadGroupBox
            // 
            this.matrixChart13RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart13RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart13RadGroupBox.Controls.Add(this.matrixChart13PDIValueRadLabel);
            this.matrixChart13RadGroupBox.Controls.Add(this.radLabel37);
            this.matrixChart13RadGroupBox.Controls.Add(this.radLabel38);
            this.matrixChart13RadGroupBox.Controls.Add(this.matrixChart13PhaseShiftRadDropDownList);
            this.matrixChart13RadGroupBox.FooterImageIndex = -1;
            this.matrixChart13RadGroupBox.FooterImageKey = "";
            this.matrixChart13RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart13RadGroupBox.HeaderImageKey = "";
            this.matrixChart13RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart13RadGroupBox.HeaderText = "";
            this.matrixChart13RadGroupBox.Location = new System.Drawing.Point(393, 514);
            this.matrixChart13RadGroupBox.Name = "matrixChart13RadGroupBox";
            this.matrixChart13RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart13RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart13RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart13RadGroupBox.TabIndex = 37;
            this.matrixChart13RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart13PDIValueRadLabel
            // 
            this.matrixChart13PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart13PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart13PDIValueRadLabel.Name = "matrixChart13PDIValueRadLabel";
            this.matrixChart13PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart13PDIValueRadLabel.TabIndex = 3;
            this.matrixChart13PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel37
            // 
            this.radLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel37.Location = new System.Drawing.Point(1, 5);
            this.radLabel37.Name = "radLabel37";
            this.radLabel37.Size = new System.Drawing.Size(32, 16);
            this.radLabel37.TabIndex = 2;
            this.radLabel37.Text = "PDI=";
            // 
            // radLabel38
            // 
            this.radLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel38.Location = new System.Drawing.Point(83, 5);
            this.radLabel38.Name = "radLabel38";
            this.radLabel38.Size = new System.Drawing.Size(23, 16);
            this.radLabel38.TabIndex = 1;
            this.radLabel38.Text = "Ph.";
            // 
            // matrixChart13PhaseShiftRadDropDownList
            // 
            this.matrixChart13PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart13PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart13PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart13PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart13PhaseShiftRadDropDownList.Name = "matrixChart13PhaseShiftRadDropDownList";
            this.matrixChart13PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart13PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart13PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart13PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart13PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart13PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart12RadGroupBox
            // 
            this.matrixChart12RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart12RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart12RadGroupBox.Controls.Add(this.matrixChart12PDIValueRadLabel);
            this.matrixChart12RadGroupBox.Controls.Add(this.radLabel34);
            this.matrixChart12RadGroupBox.Controls.Add(this.radLabel35);
            this.matrixChart12RadGroupBox.Controls.Add(this.matrixChart12PhaseShiftRadDropDownList);
            this.matrixChart12RadGroupBox.FooterImageIndex = -1;
            this.matrixChart12RadGroupBox.FooterImageKey = "";
            this.matrixChart12RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart12RadGroupBox.HeaderImageKey = "";
            this.matrixChart12RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart12RadGroupBox.HeaderText = "";
            this.matrixChart12RadGroupBox.Location = new System.Drawing.Point(198, 514);
            this.matrixChart12RadGroupBox.Name = "matrixChart12RadGroupBox";
            this.matrixChart12RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart12RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart12RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart12RadGroupBox.TabIndex = 36;
            this.matrixChart12RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart12PDIValueRadLabel
            // 
            this.matrixChart12PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart12PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart12PDIValueRadLabel.Name = "matrixChart12PDIValueRadLabel";
            this.matrixChart12PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart12PDIValueRadLabel.TabIndex = 3;
            this.matrixChart12PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel34
            // 
            this.radLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel34.Location = new System.Drawing.Point(1, 5);
            this.radLabel34.Name = "radLabel34";
            this.radLabel34.Size = new System.Drawing.Size(32, 16);
            this.radLabel34.TabIndex = 2;
            this.radLabel34.Text = "PDI=";
            // 
            // radLabel35
            // 
            this.radLabel35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel35.Location = new System.Drawing.Point(83, 5);
            this.radLabel35.Name = "radLabel35";
            this.radLabel35.Size = new System.Drawing.Size(23, 16);
            this.radLabel35.TabIndex = 1;
            this.radLabel35.Text = "Ph.";
            // 
            // matrixChart12PhaseShiftRadDropDownList
            // 
            this.matrixChart12PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart12PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart12PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart12PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart12PhaseShiftRadDropDownList.Name = "matrixChart12PhaseShiftRadDropDownList";
            this.matrixChart12PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart12PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart12PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart12PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart12PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart12PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart11RadGroupBox
            // 
            this.matrixChart11RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart11RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart11RadGroupBox.Controls.Add(this.matrixChart11PDIValueRadLabel);
            this.matrixChart11RadGroupBox.Controls.Add(this.radLabel31);
            this.matrixChart11RadGroupBox.Controls.Add(this.radLabel32);
            this.matrixChart11RadGroupBox.Controls.Add(this.matrixChart11PhaseShiftRadDropDownList);
            this.matrixChart11RadGroupBox.FooterImageIndex = -1;
            this.matrixChart11RadGroupBox.FooterImageKey = "";
            this.matrixChart11RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart11RadGroupBox.HeaderImageKey = "";
            this.matrixChart11RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart11RadGroupBox.HeaderText = "";
            this.matrixChart11RadGroupBox.Location = new System.Drawing.Point(3, 514);
            this.matrixChart11RadGroupBox.Name = "matrixChart11RadGroupBox";
            this.matrixChart11RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart11RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart11RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart11RadGroupBox.TabIndex = 35;
            this.matrixChart11RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart11PDIValueRadLabel
            // 
            this.matrixChart11PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart11PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart11PDIValueRadLabel.Name = "matrixChart11PDIValueRadLabel";
            this.matrixChart11PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart11PDIValueRadLabel.TabIndex = 3;
            this.matrixChart11PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel31
            // 
            this.radLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel31.Location = new System.Drawing.Point(1, 5);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(32, 16);
            this.radLabel31.TabIndex = 2;
            this.radLabel31.Text = "PDI=";
            // 
            // radLabel32
            // 
            this.radLabel32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel32.Location = new System.Drawing.Point(83, 5);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(23, 16);
            this.radLabel32.TabIndex = 1;
            this.radLabel32.Text = "Ph.";
            // 
            // matrixChart11PhaseShiftRadDropDownList
            // 
            this.matrixChart11PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart11PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart11PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart11PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart11PhaseShiftRadDropDownList.Name = "matrixChart11PhaseShiftRadDropDownList";
            this.matrixChart11PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart11PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart11PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart11PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart11PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart11PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart10RadGroupBox
            // 
            this.matrixChart10RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart10RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart10RadGroupBox.Controls.Add(this.matrixChart10PDIValueRadLabel);
            this.matrixChart10RadGroupBox.Controls.Add(this.radLabel28);
            this.matrixChart10RadGroupBox.Controls.Add(this.radLabel29);
            this.matrixChart10RadGroupBox.Controls.Add(this.matrixChart10PhaseShiftRadDropDownList);
            this.matrixChart10RadGroupBox.FooterImageIndex = -1;
            this.matrixChart10RadGroupBox.FooterImageKey = "";
            this.matrixChart10RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart10RadGroupBox.HeaderImageKey = "";
            this.matrixChart10RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart10RadGroupBox.HeaderText = "";
            this.matrixChart10RadGroupBox.Location = new System.Drawing.Point(783, 334);
            this.matrixChart10RadGroupBox.Name = "matrixChart10RadGroupBox";
            this.matrixChart10RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart10RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart10RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart10RadGroupBox.TabIndex = 34;
            this.matrixChart10RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart10PDIValueRadLabel
            // 
            this.matrixChart10PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart10PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart10PDIValueRadLabel.Name = "matrixChart10PDIValueRadLabel";
            this.matrixChart10PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart10PDIValueRadLabel.TabIndex = 3;
            this.matrixChart10PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel28
            // 
            this.radLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel28.Location = new System.Drawing.Point(1, 5);
            this.radLabel28.Name = "radLabel28";
            this.radLabel28.Size = new System.Drawing.Size(32, 16);
            this.radLabel28.TabIndex = 2;
            this.radLabel28.Text = "PDI=";
            // 
            // radLabel29
            // 
            this.radLabel29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel29.Location = new System.Drawing.Point(83, 5);
            this.radLabel29.Name = "radLabel29";
            this.radLabel29.Size = new System.Drawing.Size(23, 16);
            this.radLabel29.TabIndex = 1;
            this.radLabel29.Text = "Ph.";
            // 
            // matrixChart10PhaseShiftRadDropDownList
            // 
            this.matrixChart10PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart10PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart10PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart10PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart10PhaseShiftRadDropDownList.Name = "matrixChart10PhaseShiftRadDropDownList";
            this.matrixChart10PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart10PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart10PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart10PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart10PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart10PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart9RadGroupBox
            // 
            this.matrixChart9RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart9RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart9RadGroupBox.Controls.Add(this.matrixChart9PDIValueRadLabel);
            this.matrixChart9RadGroupBox.Controls.Add(this.radLabel25);
            this.matrixChart9RadGroupBox.Controls.Add(this.radLabel26);
            this.matrixChart9RadGroupBox.Controls.Add(this.matrixChart9PhaseShiftRadDropDownList);
            this.matrixChart9RadGroupBox.FooterImageIndex = -1;
            this.matrixChart9RadGroupBox.FooterImageKey = "";
            this.matrixChart9RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart9RadGroupBox.HeaderImageKey = "";
            this.matrixChart9RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart9RadGroupBox.HeaderText = "";
            this.matrixChart9RadGroupBox.Location = new System.Drawing.Point(588, 334);
            this.matrixChart9RadGroupBox.Name = "matrixChart9RadGroupBox";
            this.matrixChart9RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart9RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart9RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart9RadGroupBox.TabIndex = 33;
            this.matrixChart9RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart9PDIValueRadLabel
            // 
            this.matrixChart9PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart9PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart9PDIValueRadLabel.Name = "matrixChart9PDIValueRadLabel";
            this.matrixChart9PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart9PDIValueRadLabel.TabIndex = 3;
            this.matrixChart9PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel25
            // 
            this.radLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel25.Location = new System.Drawing.Point(1, 5);
            this.radLabel25.Name = "radLabel25";
            this.radLabel25.Size = new System.Drawing.Size(32, 16);
            this.radLabel25.TabIndex = 2;
            this.radLabel25.Text = "PDI=";
            // 
            // radLabel26
            // 
            this.radLabel26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel26.Location = new System.Drawing.Point(83, 5);
            this.radLabel26.Name = "radLabel26";
            this.radLabel26.Size = new System.Drawing.Size(23, 16);
            this.radLabel26.TabIndex = 1;
            this.radLabel26.Text = "Ph.";
            // 
            // matrixChart9PhaseShiftRadDropDownList
            // 
            this.matrixChart9PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart9PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart9PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart9PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart9PhaseShiftRadDropDownList.Name = "matrixChart9PhaseShiftRadDropDownList";
            this.matrixChart9PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart9PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart9PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart9PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart9PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart9PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart8RadGroupBox
            // 
            this.matrixChart8RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart8RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart8RadGroupBox.Controls.Add(this.matrixChart8PDIValueRadLabel);
            this.matrixChart8RadGroupBox.Controls.Add(this.radLabel22);
            this.matrixChart8RadGroupBox.Controls.Add(this.radLabel23);
            this.matrixChart8RadGroupBox.Controls.Add(this.matrixChart8PhaseShiftRadDropDownList);
            this.matrixChart8RadGroupBox.FooterImageIndex = -1;
            this.matrixChart8RadGroupBox.FooterImageKey = "";
            this.matrixChart8RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart8RadGroupBox.HeaderImageKey = "";
            this.matrixChart8RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart8RadGroupBox.HeaderText = "";
            this.matrixChart8RadGroupBox.Location = new System.Drawing.Point(393, 334);
            this.matrixChart8RadGroupBox.Name = "matrixChart8RadGroupBox";
            this.matrixChart8RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart8RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart8RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart8RadGroupBox.TabIndex = 32;
            this.matrixChart8RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart8PDIValueRadLabel
            // 
            this.matrixChart8PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart8PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart8PDIValueRadLabel.Name = "matrixChart8PDIValueRadLabel";
            this.matrixChart8PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart8PDIValueRadLabel.TabIndex = 3;
            this.matrixChart8PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel22
            // 
            this.radLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel22.Location = new System.Drawing.Point(1, 5);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(32, 16);
            this.radLabel22.TabIndex = 2;
            this.radLabel22.Text = "PDI=";
            // 
            // radLabel23
            // 
            this.radLabel23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel23.Location = new System.Drawing.Point(83, 5);
            this.radLabel23.Name = "radLabel23";
            this.radLabel23.Size = new System.Drawing.Size(23, 16);
            this.radLabel23.TabIndex = 1;
            this.radLabel23.Text = "Ph.";
            // 
            // matrixChart8PhaseShiftRadDropDownList
            // 
            this.matrixChart8PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart8PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart8PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart8PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart8PhaseShiftRadDropDownList.Name = "matrixChart8PhaseShiftRadDropDownList";
            this.matrixChart8PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart8PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart8PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart8PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart8PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart8PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart7RadGroupBox
            // 
            this.matrixChart7RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart7RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart7RadGroupBox.Controls.Add(this.matrixChart7PDIValueRadLabel);
            this.matrixChart7RadGroupBox.Controls.Add(this.radLabel19);
            this.matrixChart7RadGroupBox.Controls.Add(this.radLabel20);
            this.matrixChart7RadGroupBox.Controls.Add(this.matrixChart7PhaseShiftRadDropDownList);
            this.matrixChart7RadGroupBox.FooterImageIndex = -1;
            this.matrixChart7RadGroupBox.FooterImageKey = "";
            this.matrixChart7RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart7RadGroupBox.HeaderImageKey = "";
            this.matrixChart7RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart7RadGroupBox.HeaderText = "";
            this.matrixChart7RadGroupBox.Location = new System.Drawing.Point(198, 334);
            this.matrixChart7RadGroupBox.Name = "matrixChart7RadGroupBox";
            this.matrixChart7RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart7RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart7RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart7RadGroupBox.TabIndex = 31;
            this.matrixChart7RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart7PDIValueRadLabel
            // 
            this.matrixChart7PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart7PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart7PDIValueRadLabel.Name = "matrixChart7PDIValueRadLabel";
            this.matrixChart7PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart7PDIValueRadLabel.TabIndex = 3;
            this.matrixChart7PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel19
            // 
            this.radLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel19.Location = new System.Drawing.Point(1, 5);
            this.radLabel19.Name = "radLabel19";
            this.radLabel19.Size = new System.Drawing.Size(32, 16);
            this.radLabel19.TabIndex = 2;
            this.radLabel19.Text = "PDI=";
            // 
            // radLabel20
            // 
            this.radLabel20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel20.Location = new System.Drawing.Point(83, 5);
            this.radLabel20.Name = "radLabel20";
            this.radLabel20.Size = new System.Drawing.Size(23, 16);
            this.radLabel20.TabIndex = 1;
            this.radLabel20.Text = "Ph.";
            // 
            // matrixChart7PhaseShiftRadDropDownList
            // 
            this.matrixChart7PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart7PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart7PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart7PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart7PhaseShiftRadDropDownList.Name = "matrixChart7PhaseShiftRadDropDownList";
            this.matrixChart7PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart7PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart7PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart7PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart7PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart7PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart6RadGroupBox
            // 
            this.matrixChart6RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart6RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart6RadGroupBox.Controls.Add(this.matrixChart6PDIValueRadLabel);
            this.matrixChart6RadGroupBox.Controls.Add(this.radLabel16);
            this.matrixChart6RadGroupBox.Controls.Add(this.radLabel17);
            this.matrixChart6RadGroupBox.Controls.Add(this.matrixChart6PhaseShiftRadDropDownList);
            this.matrixChart6RadGroupBox.FooterImageIndex = -1;
            this.matrixChart6RadGroupBox.FooterImageKey = "";
            this.matrixChart6RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart6RadGroupBox.HeaderImageKey = "";
            this.matrixChart6RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart6RadGroupBox.HeaderText = "";
            this.matrixChart6RadGroupBox.Location = new System.Drawing.Point(3, 334);
            this.matrixChart6RadGroupBox.Name = "matrixChart6RadGroupBox";
            this.matrixChart6RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart6RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart6RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart6RadGroupBox.TabIndex = 30;
            this.matrixChart6RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart6PDIValueRadLabel
            // 
            this.matrixChart6PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart6PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart6PDIValueRadLabel.Name = "matrixChart6PDIValueRadLabel";
            this.matrixChart6PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart6PDIValueRadLabel.TabIndex = 3;
            this.matrixChart6PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel16
            // 
            this.radLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel16.Location = new System.Drawing.Point(1, 5);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(32, 16);
            this.radLabel16.TabIndex = 2;
            this.radLabel16.Text = "PDI=";
            // 
            // radLabel17
            // 
            this.radLabel17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel17.Location = new System.Drawing.Point(83, 5);
            this.radLabel17.Name = "radLabel17";
            this.radLabel17.Size = new System.Drawing.Size(23, 16);
            this.radLabel17.TabIndex = 1;
            this.radLabel17.Text = "Ph.";
            // 
            // matrixChart6PhaseShiftRadDropDownList
            // 
            this.matrixChart6PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart6PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart6PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart6PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart6PhaseShiftRadDropDownList.Name = "matrixChart6PhaseShiftRadDropDownList";
            this.matrixChart6PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart6PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart6PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart6PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart6PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart6PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart5RadGroupBox
            // 
            this.matrixChart5RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart5RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart5RadGroupBox.Controls.Add(this.matrixChart5PDIValueRadLabel);
            this.matrixChart5RadGroupBox.Controls.Add(this.radLabel13);
            this.matrixChart5RadGroupBox.Controls.Add(this.radLabel14);
            this.matrixChart5RadGroupBox.Controls.Add(this.matrixChart5PhaseShiftRadDropDownList);
            this.matrixChart5RadGroupBox.FooterImageIndex = -1;
            this.matrixChart5RadGroupBox.FooterImageKey = "";
            this.matrixChart5RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart5RadGroupBox.HeaderImageKey = "";
            this.matrixChart5RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart5RadGroupBox.HeaderText = "";
            this.matrixChart5RadGroupBox.Location = new System.Drawing.Point(783, 154);
            this.matrixChart5RadGroupBox.Name = "matrixChart5RadGroupBox";
            this.matrixChart5RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart5RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart5RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart5RadGroupBox.TabIndex = 29;
            this.matrixChart5RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart5PDIValueRadLabel
            // 
            this.matrixChart5PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart5PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart5PDIValueRadLabel.Name = "matrixChart5PDIValueRadLabel";
            this.matrixChart5PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart5PDIValueRadLabel.TabIndex = 3;
            this.matrixChart5PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel13
            // 
            this.radLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel13.Location = new System.Drawing.Point(1, 5);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(32, 16);
            this.radLabel13.TabIndex = 2;
            this.radLabel13.Text = "PDI=";
            // 
            // radLabel14
            // 
            this.radLabel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel14.Location = new System.Drawing.Point(83, 5);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(23, 16);
            this.radLabel14.TabIndex = 1;
            this.radLabel14.Text = "Ph.";
            // 
            // matrixChart5PhaseShiftRadDropDownList
            // 
            this.matrixChart5PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart5PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart5PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart5PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart5PhaseShiftRadDropDownList.Name = "matrixChart5PhaseShiftRadDropDownList";
            this.matrixChart5PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart5PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart5PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart5PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart5PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart5PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart4RadGroupBox
            // 
            this.matrixChart4RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart4RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart4RadGroupBox.Controls.Add(this.matrixChart4PDIValueRadLabel);
            this.matrixChart4RadGroupBox.Controls.Add(this.radLabel10);
            this.matrixChart4RadGroupBox.Controls.Add(this.radLabel11);
            this.matrixChart4RadGroupBox.Controls.Add(this.matrixChart4PhaseShiftRadDropDownList);
            this.matrixChart4RadGroupBox.FooterImageIndex = -1;
            this.matrixChart4RadGroupBox.FooterImageKey = "";
            this.matrixChart4RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart4RadGroupBox.HeaderImageKey = "";
            this.matrixChart4RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart4RadGroupBox.HeaderText = "";
            this.matrixChart4RadGroupBox.Location = new System.Drawing.Point(588, 154);
            this.matrixChart4RadGroupBox.Name = "matrixChart4RadGroupBox";
            this.matrixChart4RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart4RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart4RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart4RadGroupBox.TabIndex = 28;
            this.matrixChart4RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart4PDIValueRadLabel
            // 
            this.matrixChart4PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart4PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart4PDIValueRadLabel.Name = "matrixChart4PDIValueRadLabel";
            this.matrixChart4PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart4PDIValueRadLabel.TabIndex = 3;
            this.matrixChart4PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(1, 5);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(32, 16);
            this.radLabel10.TabIndex = 2;
            this.radLabel10.Text = "PDI=";
            // 
            // radLabel11
            // 
            this.radLabel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel11.Location = new System.Drawing.Point(83, 5);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(23, 16);
            this.radLabel11.TabIndex = 1;
            this.radLabel11.Text = "Ph.";
            // 
            // matrixChart4PhaseShiftRadDropDownList
            // 
            this.matrixChart4PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart4PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart4PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart4PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart4PhaseShiftRadDropDownList.Name = "matrixChart4PhaseShiftRadDropDownList";
            this.matrixChart4PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart4PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart4PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart4PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart4PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart4PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart3RadGroupBox
            // 
            this.matrixChart3RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart3RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart3RadGroupBox.Controls.Add(this.matrixChart3PDIValueRadLabel);
            this.matrixChart3RadGroupBox.Controls.Add(this.radLabel7);
            this.matrixChart3RadGroupBox.Controls.Add(this.radLabel8);
            this.matrixChart3RadGroupBox.Controls.Add(this.matrixChart3PhaseShiftRadDropDownList);
            this.matrixChart3RadGroupBox.FooterImageIndex = -1;
            this.matrixChart3RadGroupBox.FooterImageKey = "";
            this.matrixChart3RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart3RadGroupBox.HeaderImageKey = "";
            this.matrixChart3RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart3RadGroupBox.HeaderText = "";
            this.matrixChart3RadGroupBox.Location = new System.Drawing.Point(393, 154);
            this.matrixChart3RadGroupBox.Name = "matrixChart3RadGroupBox";
            this.matrixChart3RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart3RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart3RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart3RadGroupBox.TabIndex = 27;
            this.matrixChart3RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart3PDIValueRadLabel
            // 
            this.matrixChart3PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart3PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart3PDIValueRadLabel.Name = "matrixChart3PDIValueRadLabel";
            this.matrixChart3PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart3PDIValueRadLabel.TabIndex = 3;
            this.matrixChart3PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(1, 5);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(32, 16);
            this.radLabel7.TabIndex = 2;
            this.radLabel7.Text = "PDI=";
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel8.Location = new System.Drawing.Point(83, 5);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(23, 16);
            this.radLabel8.TabIndex = 1;
            this.radLabel8.Text = "Ph.";
            // 
            // matrixChart3PhaseShiftRadDropDownList
            // 
            this.matrixChart3PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart3PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart3PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart3PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart3PhaseShiftRadDropDownList.Name = "matrixChart3PhaseShiftRadDropDownList";
            this.matrixChart3PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart3PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart3PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart3PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart3PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart3PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart2RadGroupBox
            // 
            this.matrixChart2RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart2RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart2RadGroupBox.Controls.Add(this.matrixChart2PDIValueRadLabel);
            this.matrixChart2RadGroupBox.Controls.Add(this.radLabel4);
            this.matrixChart2RadGroupBox.Controls.Add(this.radLabel5);
            this.matrixChart2RadGroupBox.Controls.Add(this.matrixChart2PhaseShiftRadDropDownList);
            this.matrixChart2RadGroupBox.FooterImageIndex = -1;
            this.matrixChart2RadGroupBox.FooterImageKey = "";
            this.matrixChart2RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart2RadGroupBox.HeaderImageKey = "";
            this.matrixChart2RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart2RadGroupBox.HeaderText = "";
            this.matrixChart2RadGroupBox.Location = new System.Drawing.Point(198, 154);
            this.matrixChart2RadGroupBox.Name = "matrixChart2RadGroupBox";
            this.matrixChart2RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart2RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart2RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart2RadGroupBox.TabIndex = 26;
            this.matrixChart2RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart2PDIValueRadLabel
            // 
            this.matrixChart2PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart2PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart2PDIValueRadLabel.Name = "matrixChart2PDIValueRadLabel";
            this.matrixChart2PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart2PDIValueRadLabel.TabIndex = 3;
            this.matrixChart2PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(1, 5);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(32, 16);
            this.radLabel4.TabIndex = 2;
            this.radLabel4.Text = "PDI=";
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(83, 5);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(23, 16);
            this.radLabel5.TabIndex = 1;
            this.radLabel5.Text = "Ph.";
            // 
            // matrixChart2PhaseShiftRadDropDownList
            // 
            this.matrixChart2PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart2PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart2PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart2PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart2PhaseShiftRadDropDownList.Name = "matrixChart2PhaseShiftRadDropDownList";
            this.matrixChart2PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart2PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart2PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart2PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart2PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart2PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart1RadGroupBox
            // 
            this.matrixChart1RadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChart1RadGroupBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixChart1RadGroupBox.Controls.Add(this.matrixChart1PDIValueRadLabel);
            this.matrixChart1RadGroupBox.Controls.Add(this.radLabel2);
            this.matrixChart1RadGroupBox.Controls.Add(this.radLabel);
            this.matrixChart1RadGroupBox.Controls.Add(this.matrixChart1PhaseShiftRadDropDownList);
            this.matrixChart1RadGroupBox.FooterImageIndex = -1;
            this.matrixChart1RadGroupBox.FooterImageKey = "";
            this.matrixChart1RadGroupBox.HeaderImageIndex = -1;
            this.matrixChart1RadGroupBox.HeaderImageKey = "";
            this.matrixChart1RadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChart1RadGroupBox.HeaderText = "";
            this.matrixChart1RadGroupBox.Location = new System.Drawing.Point(3, 154);
            this.matrixChart1RadGroupBox.Name = "matrixChart1RadGroupBox";
            this.matrixChart1RadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChart1RadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChart1RadGroupBox.Size = new System.Drawing.Size(192, 26);
            this.matrixChart1RadGroupBox.TabIndex = 25;
            this.matrixChart1RadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixChart1PDIValueRadLabel
            // 
            this.matrixChart1PDIValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart1PDIValueRadLabel.Location = new System.Drawing.Point(32, 5);
            this.matrixChart1PDIValueRadLabel.Name = "matrixChart1PDIValueRadLabel";
            this.matrixChart1PDIValueRadLabel.Size = new System.Drawing.Size(22, 16);
            this.matrixChart1PDIValueRadLabel.TabIndex = 3;
            this.matrixChart1PDIValueRadLabel.Text = "0.0";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(1, 5);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(32, 16);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "PDI=";
            // 
            // radLabel
            // 
            this.radLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel.Location = new System.Drawing.Point(83, 5);
            this.radLabel.Name = "radLabel";
            this.radLabel.Size = new System.Drawing.Size(23, 16);
            this.radLabel.TabIndex = 1;
            this.radLabel.Text = "Ph.";
            // 
            // matrixChart1PhaseShiftRadDropDownList
            // 
            this.matrixChart1PhaseShiftRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixChart1PhaseShiftRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixChart1PhaseShiftRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChart1PhaseShiftRadDropDownList.Location = new System.Drawing.Point(106, 3);
            this.matrixChart1PhaseShiftRadDropDownList.Name = "matrixChart1PhaseShiftRadDropDownList";
            this.matrixChart1PhaseShiftRadDropDownList.ShowImageInEditorArea = true;
            this.matrixChart1PhaseShiftRadDropDownList.Size = new System.Drawing.Size(83, 18);
            this.matrixChart1PhaseShiftRadDropDownList.TabIndex = 0;
            this.matrixChart1PhaseShiftRadDropDownList.Text = "radDropDownList1";
            this.matrixChart1PhaseShiftRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixChart1PhaseShiftRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChart15WinChartViewer
            // 
            this.matrixChart15WinChartViewer.Location = new System.Drawing.Point(783, 363);
            this.matrixChart15WinChartViewer.Name = "matrixChart15WinChartViewer";
            this.matrixChart15WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart15WinChartViewer.TabIndex = 24;
            this.matrixChart15WinChartViewer.TabStop = false;
            this.matrixChart15WinChartViewer.Click += new System.EventHandler(this.matrixChart15CopyGraph_Click);
            // 
            // matrixChart14WinChartViewer
            // 
            this.matrixChart14WinChartViewer.Location = new System.Drawing.Point(588, 363);
            this.matrixChart14WinChartViewer.Name = "matrixChart14WinChartViewer";
            this.matrixChart14WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart14WinChartViewer.TabIndex = 23;
            this.matrixChart14WinChartViewer.TabStop = false;
            this.matrixChart14WinChartViewer.Click += new System.EventHandler(this.matrixChart14CopyGraph_Click);
            // 
            // matrixChart13WinChartViewer
            // 
            this.matrixChart13WinChartViewer.Location = new System.Drawing.Point(393, 363);
            this.matrixChart13WinChartViewer.Name = "matrixChart13WinChartViewer";
            this.matrixChart13WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart13WinChartViewer.TabIndex = 22;
            this.matrixChart13WinChartViewer.TabStop = false;
            this.matrixChart13WinChartViewer.Click += new System.EventHandler(this.matrixChart13CopyGraph_Click);
            // 
            // matrixChart12WinChartViewer
            // 
            this.matrixChart12WinChartViewer.Location = new System.Drawing.Point(198, 363);
            this.matrixChart12WinChartViewer.Name = "matrixChart12WinChartViewer";
            this.matrixChart12WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart12WinChartViewer.TabIndex = 21;
            this.matrixChart12WinChartViewer.TabStop = false;
            this.matrixChart12WinChartViewer.Click += new System.EventHandler(this.matrixChart12CopyGraph_Click);
            // 
            // matrixChart10WinChartViewer
            // 
            this.matrixChart10WinChartViewer.Location = new System.Drawing.Point(783, 183);
            this.matrixChart10WinChartViewer.Name = "matrixChart10WinChartViewer";
            this.matrixChart10WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart10WinChartViewer.TabIndex = 20;
            this.matrixChart10WinChartViewer.TabStop = false;
            this.matrixChart10WinChartViewer.Click += new System.EventHandler(this.matrixChart10CopyGraph_Click);
            // 
            // matrixChart9WinChartViewer
            // 
            this.matrixChart9WinChartViewer.Location = new System.Drawing.Point(588, 183);
            this.matrixChart9WinChartViewer.Name = "matrixChart9WinChartViewer";
            this.matrixChart9WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart9WinChartViewer.TabIndex = 19;
            this.matrixChart9WinChartViewer.TabStop = false;
            this.matrixChart9WinChartViewer.Click += new System.EventHandler(this.matrixChart9CopyGraph_Click);
            // 
            // matrixChart8WinChartViewer
            // 
            this.matrixChart8WinChartViewer.Location = new System.Drawing.Point(393, 183);
            this.matrixChart8WinChartViewer.Name = "matrixChart8WinChartViewer";
            this.matrixChart8WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart8WinChartViewer.TabIndex = 18;
            this.matrixChart8WinChartViewer.TabStop = false;
            this.matrixChart8WinChartViewer.Click += new System.EventHandler(this.matrixChart8CopyGraph_Click);
            // 
            // matrixChart7WinChartViewer
            // 
            this.matrixChart7WinChartViewer.Location = new System.Drawing.Point(198, 183);
            this.matrixChart7WinChartViewer.Name = "matrixChart7WinChartViewer";
            this.matrixChart7WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart7WinChartViewer.TabIndex = 17;
            this.matrixChart7WinChartViewer.TabStop = false;
            this.matrixChart7WinChartViewer.Click += new System.EventHandler(this.matrixChart7CopyGraph_Click);
            // 
            // matrixChart5WinChartViewer
            // 
            this.matrixChart5WinChartViewer.Location = new System.Drawing.Point(783, 3);
            this.matrixChart5WinChartViewer.Name = "matrixChart5WinChartViewer";
            this.matrixChart5WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart5WinChartViewer.TabIndex = 15;
            this.matrixChart5WinChartViewer.TabStop = false;
            this.matrixChart5WinChartViewer.Click += new System.EventHandler(this.matrixChart5CopyGraph_Click);
            // 
            // matrixChart4WinChartViewer
            // 
            this.matrixChart4WinChartViewer.Location = new System.Drawing.Point(588, 3);
            this.matrixChart4WinChartViewer.Name = "matrixChart4WinChartViewer";
            this.matrixChart4WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart4WinChartViewer.TabIndex = 14;
            this.matrixChart4WinChartViewer.TabStop = false;
            this.matrixChart4WinChartViewer.Click += new System.EventHandler(this.matrixChart4CopyGraph_Click);
            // 
            // matrixChart3WinChartViewer
            // 
            this.matrixChart3WinChartViewer.Location = new System.Drawing.Point(393, 3);
            this.matrixChart3WinChartViewer.Name = "matrixChart3WinChartViewer";
            this.matrixChart3WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart3WinChartViewer.TabIndex = 11;
            this.matrixChart3WinChartViewer.TabStop = false;
            this.matrixChart3WinChartViewer.Click += new System.EventHandler(this.matrixChart3CopyGraph_Click);
            // 
            // matrixChart2WinChartViewer
            // 
            this.matrixChart2WinChartViewer.Location = new System.Drawing.Point(198, 3);
            this.matrixChart2WinChartViewer.Name = "matrixChart2WinChartViewer";
            this.matrixChart2WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart2WinChartViewer.TabIndex = 8;
            this.matrixChart2WinChartViewer.TabStop = false;
            this.matrixChart2WinChartViewer.Click += new System.EventHandler(this.matrixChart2CopyGraph_Click);
            // 
            // matrixChart11WinChartViewer
            // 
            this.matrixChart11WinChartViewer.Location = new System.Drawing.Point(3, 363);
            this.matrixChart11WinChartViewer.Name = "matrixChart11WinChartViewer";
            this.matrixChart11WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart11WinChartViewer.TabIndex = 7;
            this.matrixChart11WinChartViewer.TabStop = false;
            this.matrixChart11WinChartViewer.Click += new System.EventHandler(this.matrixChart11CopyGraph_Click);
            // 
            // matrixChart6WinChartViewer
            // 
            this.matrixChart6WinChartViewer.Location = new System.Drawing.Point(3, 183);
            this.matrixChart6WinChartViewer.Name = "matrixChart6WinChartViewer";
            this.matrixChart6WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart6WinChartViewer.TabIndex = 6;
            this.matrixChart6WinChartViewer.TabStop = false;
            this.matrixChart6WinChartViewer.Click += new System.EventHandler(this.matrixChart6CopyGraph_Click);
            // 
            // matrixChart1WinChartViewer
            // 
            this.matrixChart1WinChartViewer.Location = new System.Drawing.Point(3, 3);
            this.matrixChart1WinChartViewer.Name = "matrixChart1WinChartViewer";
            this.matrixChart1WinChartViewer.Size = new System.Drawing.Size(192, 177);
            this.matrixChart1WinChartViewer.TabIndex = 5;
            this.matrixChart1WinChartViewer.TabStop = false;
            this.matrixChart1WinChartViewer.Click += new System.EventHandler(this.matrixChart1CopyGraph_Click);
            // 
            // matrixDisplaySettingsRadGroupBox
            // 
            this.matrixDisplaySettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixDisplaySettingsRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.matrixDisplaySettingsRadGroupBox.Controls.Add(this.matrixInitialPhaseRadButton);
            this.matrixDisplaySettingsRadGroupBox.Controls.Add(this.matrixAmplitudeRadGroupBox);
            this.matrixDisplaySettingsRadGroupBox.Controls.Add(this.matrixScaleRadGroupBox);
            this.matrixDisplaySettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixDisplaySettingsRadGroupBox.FooterImageIndex = -1;
            this.matrixDisplaySettingsRadGroupBox.FooterImageKey = "";
            this.matrixDisplaySettingsRadGroupBox.HeaderImageIndex = -1;
            this.matrixDisplaySettingsRadGroupBox.HeaderImageKey = "";
            this.matrixDisplaySettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixDisplaySettingsRadGroupBox.HeaderText = "Display Settings";
            this.matrixDisplaySettingsRadGroupBox.Location = new System.Drawing.Point(125, 573);
            this.matrixDisplaySettingsRadGroupBox.Name = "matrixDisplaySettingsRadGroupBox";
            this.matrixDisplaySettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixDisplaySettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixDisplaySettingsRadGroupBox.Size = new System.Drawing.Size(265, 140);
            this.matrixDisplaySettingsRadGroupBox.TabIndex = 20;
            this.matrixDisplaySettingsRadGroupBox.Text = "Display Settings";
            this.matrixDisplaySettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixInitialPhaseRadButton
            // 
            this.matrixInitialPhaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixInitialPhaseRadButton.Location = new System.Drawing.Point(14, 93);
            this.matrixInitialPhaseRadButton.Name = "matrixInitialPhaseRadButton";
            this.matrixInitialPhaseRadButton.Size = new System.Drawing.Size(158, 40);
            this.matrixInitialPhaseRadButton.TabIndex = 2;
            this.matrixInitialPhaseRadButton.Text = "Common Phase Shift";
            this.matrixInitialPhaseRadButton.ThemeName = "Office2007Black";
            this.matrixInitialPhaseRadButton.Click += new System.EventHandler(this.matrixInitialPhaseRadButton_Click);
            // 
            // matrixAmplitudeRadGroupBox
            // 
            this.matrixAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixAmplitudeRadGroupBox.Controls.Add(this.matrixPcRadRadioButton);
            this.matrixAmplitudeRadGroupBox.Controls.Add(this.matrixVRadRadioButton);
            this.matrixAmplitudeRadGroupBox.Controls.Add(this.matrixDbRadRadioButton);
            this.matrixAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixAmplitudeRadGroupBox.FooterImageIndex = -1;
            this.matrixAmplitudeRadGroupBox.FooterImageKey = "";
            this.matrixAmplitudeRadGroupBox.HeaderImageIndex = -1;
            this.matrixAmplitudeRadGroupBox.HeaderImageKey = "";
            this.matrixAmplitudeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixAmplitudeRadGroupBox.HeaderText = "Amplitude";
            this.matrixAmplitudeRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.matrixAmplitudeRadGroupBox.Location = new System.Drawing.Point(178, 23);
            this.matrixAmplitudeRadGroupBox.Name = "matrixAmplitudeRadGroupBox";
            this.matrixAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixAmplitudeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixAmplitudeRadGroupBox.Size = new System.Drawing.Size(78, 110);
            this.matrixAmplitudeRadGroupBox.TabIndex = 1;
            this.matrixAmplitudeRadGroupBox.Text = "Amplitude";
            this.matrixAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixPcRadRadioButton
            // 
            this.matrixPcRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixPcRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixPcRadRadioButton.Location = new System.Drawing.Point(14, 79);
            this.matrixPcRadRadioButton.Name = "matrixPcRadRadioButton";
            this.matrixPcRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.matrixPcRadRadioButton.TabIndex = 1;
            this.matrixPcRadRadioButton.Text = "pC";
            // 
            // matrixVRadRadioButton
            // 
            this.matrixVRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixVRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixVRadRadioButton.Location = new System.Drawing.Point(14, 51);
            this.matrixVRadRadioButton.Name = "matrixVRadRadioButton";
            this.matrixVRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.matrixVRadRadioButton.TabIndex = 1;
            this.matrixVRadRadioButton.TabStop = true;
            this.matrixVRadRadioButton.Text = "V";
            this.matrixVRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.matrixVRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.matrixVRadRadioButton_ToggleStateChanged);
            // 
            // matrixDbRadRadioButton
            // 
            this.matrixDbRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixDbRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixDbRadRadioButton.Location = new System.Drawing.Point(14, 27);
            this.matrixDbRadRadioButton.Name = "matrixDbRadRadioButton";
            this.matrixDbRadRadioButton.Size = new System.Drawing.Size(40, 18);
            this.matrixDbRadRadioButton.TabIndex = 0;
            this.matrixDbRadRadioButton.Text = "dB";
            this.matrixDbRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.matrixDbRadRadioButton_ToggleStateChanged);
            // 
            // matrixScaleRadGroupBox
            // 
            this.matrixScaleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixScaleRadGroupBox.Controls.Add(this.matrixScaleRadDropDownList);
            this.matrixScaleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixScaleRadGroupBox.FooterImageIndex = -1;
            this.matrixScaleRadGroupBox.FooterImageKey = "";
            this.matrixScaleRadGroupBox.HeaderImageIndex = -1;
            this.matrixScaleRadGroupBox.HeaderImageKey = "";
            this.matrixScaleRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixScaleRadGroupBox.HeaderText = "Scale";
            this.matrixScaleRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.matrixScaleRadGroupBox.Location = new System.Drawing.Point(13, 23);
            this.matrixScaleRadGroupBox.Name = "matrixScaleRadGroupBox";
            this.matrixScaleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixScaleRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixScaleRadGroupBox.Size = new System.Drawing.Size(159, 65);
            this.matrixScaleRadGroupBox.TabIndex = 0;
            this.matrixScaleRadGroupBox.Text = "Scale";
            this.matrixScaleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixScaleRadDropDownList
            // 
            this.matrixScaleRadDropDownList.DropDownAnimationEnabled = true;
            this.matrixScaleRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixScaleRadDropDownList.Location = new System.Drawing.Point(13, 27);
            this.matrixScaleRadDropDownList.Name = "matrixScaleRadDropDownList";
            this.matrixScaleRadDropDownList.ShowImageInEditorArea = true;
            this.matrixScaleRadDropDownList.Size = new System.Drawing.Size(133, 18);
            this.matrixScaleRadDropDownList.TabIndex = 0;
            this.matrixScaleRadDropDownList.Text = "radDropDownList1";
            this.matrixScaleRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.matrixScaleRadDropDownList_SelectedIndexChanged);
            // 
            // matrixChartLayoutRadGroupBox
            // 
            this.matrixChartLayoutRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.matrixChartLayoutRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.matrixChartLayoutRadGroupBox.Controls.Add(this.matrixFifteenByOneRadRadioButton);
            this.matrixChartLayoutRadGroupBox.Controls.Add(this.matrixEightByTwoRadRadioButton);
            this.matrixChartLayoutRadGroupBox.Controls.Add(this.matrixFiveByThreeRadRadioButton);
            this.matrixChartLayoutRadGroupBox.Controls.Add(this.matrixThreeByFiveRadRadioButton);
            this.matrixChartLayoutRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixChartLayoutRadGroupBox.FooterImageIndex = -1;
            this.matrixChartLayoutRadGroupBox.FooterImageKey = "";
            this.matrixChartLayoutRadGroupBox.HeaderImageIndex = -1;
            this.matrixChartLayoutRadGroupBox.HeaderImageKey = "";
            this.matrixChartLayoutRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.matrixChartLayoutRadGroupBox.HeaderText = "Chart Layout";
            this.matrixChartLayoutRadGroupBox.Location = new System.Drawing.Point(4, 573);
            this.matrixChartLayoutRadGroupBox.Name = "matrixChartLayoutRadGroupBox";
            this.matrixChartLayoutRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.matrixChartLayoutRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.matrixChartLayoutRadGroupBox.Size = new System.Drawing.Size(115, 68);
            this.matrixChartLayoutRadGroupBox.TabIndex = 19;
            this.matrixChartLayoutRadGroupBox.Text = "Chart Layout";
            this.matrixChartLayoutRadGroupBox.ThemeName = "Office2007Black";
            // 
            // matrixFifteenByOneRadRadioButton
            // 
            this.matrixFifteenByOneRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixFifteenByOneRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixFifteenByOneRadRadioButton.Location = new System.Drawing.Point(60, 45);
            this.matrixFifteenByOneRadRadioButton.Name = "matrixFifteenByOneRadRadioButton";
            this.matrixFifteenByOneRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.matrixFifteenByOneRadRadioButton.TabIndex = 3;
            this.matrixFifteenByOneRadRadioButton.Text = "15x1";
            this.matrixFifteenByOneRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.fifteenByOneRadRadioButton_ToggleStateChanged);
            // 
            // matrixEightByTwoRadRadioButton
            // 
            this.matrixEightByTwoRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixEightByTwoRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixEightByTwoRadRadioButton.Location = new System.Drawing.Point(60, 24);
            this.matrixEightByTwoRadRadioButton.Name = "matrixEightByTwoRadRadioButton";
            this.matrixEightByTwoRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.matrixEightByTwoRadRadioButton.TabIndex = 2;
            this.matrixEightByTwoRadRadioButton.Text = "8x2";
            this.matrixEightByTwoRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.eightByTwoRadRadioButton_ToggleStateChanged);
            // 
            // matrixFiveByThreeRadRadioButton
            // 
            this.matrixFiveByThreeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixFiveByThreeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixFiveByThreeRadRadioButton.Location = new System.Drawing.Point(10, 45);
            this.matrixFiveByThreeRadRadioButton.Name = "matrixFiveByThreeRadRadioButton";
            this.matrixFiveByThreeRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.matrixFiveByThreeRadRadioButton.TabIndex = 1;
            this.matrixFiveByThreeRadRadioButton.Text = "5x3";
            this.matrixFiveByThreeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.fiveByThreeRadRadioButton_ToggleStateChanged);
            // 
            // matrixThreeByFiveRadRadioButton
            // 
            this.matrixThreeByFiveRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matrixThreeByFiveRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.matrixThreeByFiveRadRadioButton.Location = new System.Drawing.Point(10, 24);
            this.matrixThreeByFiveRadRadioButton.Name = "matrixThreeByFiveRadRadioButton";
            this.matrixThreeByFiveRadRadioButton.Size = new System.Drawing.Size(44, 18);
            this.matrixThreeByFiveRadRadioButton.TabIndex = 0;
            this.matrixThreeByFiveRadRadioButton.TabStop = true;
            this.matrixThreeByFiveRadRadioButton.Text = "3x5";
            this.matrixThreeByFiveRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.matrixThreeByFiveRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeByFiveRadRadioButton_ToggleStateChanged);
            // 
            // dataRadPageView
            // 
            this.dataRadPageView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dataRadPageView.Controls.Add(this.matrixRadPageViewPage);
            this.dataRadPageView.Controls.Add(this.prpddPolarRadPageViewPage);
            this.dataRadPageView.Controls.Add(this.threeDeeRadPageViewPage);
            this.dataRadPageView.Controls.Add(this.phdRadPageViewPage);
            this.dataRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataRadPageView.Location = new System.Drawing.Point(0, 0);
            this.dataRadPageView.Name = "dataRadPageView";
            this.dataRadPageView.SelectedPage = this.matrixRadPageViewPage;
            this.dataRadPageView.Size = new System.Drawing.Size(1025, 768);
            this.dataRadPageView.TabIndex = 1;
            this.dataRadPageView.Text = "radPageView1";
            this.dataRadPageView.ThemeName = "Office2007Black";
            this.dataRadPageView.SelectedPageChanged += new System.EventHandler(this.dataRadPageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.dataRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // PhaseResolvedDataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 768);
            this.Controls.Add(this.dataRadPageView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PhaseResolvedDataViewer";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "PhaseResolvedDataViewer";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.PhaseResolvedDataViewer_Load);
            this.SizeChanged += new System.EventHandler(this.PhaseResolvedDataViewer_SizeChanged);
            this.phdRadPageViewPage.ResumeLayout(false);
            this.phdRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePhdTabValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePhdTabTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdDisplaySettingsRadGroupBox)).EndInit();
            this.phdDisplaySettingsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phdInitialPhaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdAmplitudeRadGroupBox)).EndInit();
            this.phdAmplitudeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phdPcRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdVRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdDbRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdScaleRadGroupBox)).EndInit();
            this.phdScaleRadGroupBox.ResumeLayout(false);
            this.phdScaleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdScaleRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChartLayoutRadGroupBox)).EndInit();
            this.phdChartLayoutRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phdFifteenByOneRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdEightByTwoRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdFiveByThreeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdThreeByFiveRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdRadPanel)).EndInit();
            this.phdRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15RadGroupBox)).EndInit();
            this.phdChart15RadGroupBox.ResumeLayout(false);
            this.phdChart15RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14RadGroupBox)).EndInit();
            this.phdChart14RadGroupBox.ResumeLayout(false);
            this.phdChart14RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13RadGroupBox)).EndInit();
            this.phdChart13RadGroupBox.ResumeLayout(false);
            this.phdChart13RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12RadGroupBox)).EndInit();
            this.phdChart12RadGroupBox.ResumeLayout(false);
            this.phdChart12RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11RadGroupBox)).EndInit();
            this.phdChart11RadGroupBox.ResumeLayout(false);
            this.phdChart11RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10RadGroupBox)).EndInit();
            this.phdChart10RadGroupBox.ResumeLayout(false);
            this.phdChart10RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9RadGroupBox)).EndInit();
            this.phdChart9RadGroupBox.ResumeLayout(false);
            this.phdChart9RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8RadGroupBox)).EndInit();
            this.phdChart8RadGroupBox.ResumeLayout(false);
            this.phdChart8RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7RadGroupBox)).EndInit();
            this.phdChart7RadGroupBox.ResumeLayout(false);
            this.phdChart7RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6RadGroupBox)).EndInit();
            this.phdChart6RadGroupBox.ResumeLayout(false);
            this.phdChart6RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5RadGroupBox)).EndInit();
            this.phdChart5RadGroupBox.ResumeLayout(false);
            this.phdChart5RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4RadGroupBox)).EndInit();
            this.phdChart4RadGroupBox.ResumeLayout(false);
            this.phdChart4RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3RadGroupBox)).EndInit();
            this.phdChart3RadGroupBox.ResumeLayout(false);
            this.phdChart3RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2RadGroupBox)).EndInit();
            this.phdChart2RadGroupBox.ResumeLayout(false);
            this.phdChart2RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1RadGroupBox)).EndInit();
            this.phdChart1RadGroupBox.ResumeLayout(false);
            this.phdChart1RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart15WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart14WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart13WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart12WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart10WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart9WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart8WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart7WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart5WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart4WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart3WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart2WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart11WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart6WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phdChart1WinChartViewer)).EndInit();
            this.threeDeeRadPageViewPage.ResumeLayout(false);
            this.threeDeeRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpdd3DTabValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpdd3DTabTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeDisplaySettingsRadGroupBox)).EndInit();
            this.threeDeeDisplaySettingsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeInitialPhaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeAmplitudeRadGroupBox)).EndInit();
            this.threeDeeAmplitudeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.threeDeePcRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeVRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeDbRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeScaleRadGroupBox)).EndInit();
            this.threeDeeScaleRadGroupBox.ResumeLayout(false);
            this.threeDeeScaleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeScaleRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeRadPanel)).EndInit();
            this.threeDeeRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15RadGroupBox)).EndInit();
            this.threeDeeChart15RadGroupBox.ResumeLayout(false);
            this.threeDeeChart15RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14RadGroupBox)).EndInit();
            this.threeDeeChart14RadGroupBox.ResumeLayout(false);
            this.threeDeeChart14RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13RadGroupBox)).EndInit();
            this.threeDeeChart13RadGroupBox.ResumeLayout(false);
            this.threeDeeChart13RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12RadGroupBox)).EndInit();
            this.threeDeeChart12RadGroupBox.ResumeLayout(false);
            this.threeDeeChart12RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11RadGroupBox)).EndInit();
            this.threeDeeChart11RadGroupBox.ResumeLayout(false);
            this.threeDeeChart11RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6RadGroupBox)).EndInit();
            this.threeDeeChart6RadGroupBox.ResumeLayout(false);
            this.threeDeeChart6RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7RadGroupBox)).EndInit();
            this.threeDeeChart7RadGroupBox.ResumeLayout(false);
            this.threeDeeChart7RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8RadGroupBox)).EndInit();
            this.threeDeeChart8RadGroupBox.ResumeLayout(false);
            this.threeDeeChart8RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9RadGroupBox)).EndInit();
            this.threeDeeChart9RadGroupBox.ResumeLayout(false);
            this.threeDeeChart9RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10RadGroupBox)).EndInit();
            this.threeDeeChart10RadGroupBox.ResumeLayout(false);
            this.threeDeeChart10RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5RadGroupBox)).EndInit();
            this.threeDeeChart5RadGroupBox.ResumeLayout(false);
            this.threeDeeChart5RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4RadGroupBox)).EndInit();
            this.threeDeeChart4RadGroupBox.ResumeLayout(false);
            this.threeDeeChart4RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3RadGroupBox)).EndInit();
            this.threeDeeChart3RadGroupBox.ResumeLayout(false);
            this.threeDeeChart3RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2RadGroupBox)).EndInit();
            this.threeDeeChart2RadGroupBox.ResumeLayout(false);
            this.threeDeeChart2RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1RadGroupBox)).EndInit();
            this.threeDeeChart1RadGroupBox.ResumeLayout(false);
            this.threeDeeChart1RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart15WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart14WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart13WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart12WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart10WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart9WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart8WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart7WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart5WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart4WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart3WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart2WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart11WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart6WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChart1WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeChartLayoutRadGroupBox)).EndInit();
            this.threeDeeChartLayoutRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeFifteenByOneRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeEightByTwoRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeFiveByThreeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threeDeeThreeByFiveRadRadioButton)).EndInit();
            this.prpddPolarRadPageViewPage.ResumeLayout(false);
            this.prpddPolarRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddPolarTabValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddPolarTabTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarRadPanel)).EndInit();
            this.prpddPolarRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15RadGroupBox)).EndInit();
            this.prpddPolarChart15RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart15RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14RadGroupBox)).EndInit();
            this.prpddPolarChart14RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart14RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13RadGroupBox)).EndInit();
            this.prpddPolarChart13RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart13RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12RadGroupBox)).EndInit();
            this.prpddPolarChart12RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart12RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11RadGroupBox)).EndInit();
            this.prpddPolarChart11RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart11RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10RadGroupBox)).EndInit();
            this.prpddPolarChart10RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart10RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9RadGroupBox)).EndInit();
            this.prpddPolarChart9RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart9RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8RadGroupBox)).EndInit();
            this.prpddPolarChart8RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart8RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7RadGroupBox)).EndInit();
            this.prpddPolarChart7RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart7RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6RadGroupBox)).EndInit();
            this.prpddPolarChart6RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart6RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5RadGroupBox)).EndInit();
            this.prpddPolarChart5RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart5RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4RadGroupBox)).EndInit();
            this.prpddPolarChart4RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart4RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3RadGroupBox)).EndInit();
            this.prpddPolarChart3RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart3RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2RadGroupBox)).EndInit();
            this.prpddPolarChart2RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart2RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1RadGroupBox)).EndInit();
            this.prpddPolarChart1RadGroupBox.ResumeLayout(false);
            this.prpddPolarChart1RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart15WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart14WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart13WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart12WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart10WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart9WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart8WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart7WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart5WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart4WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart3WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart2WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart11WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart6WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChart1WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarDisplaySettingsRadGroupBox)).EndInit();
            this.prpddPolarDisplaySettingsRadGroupBox.ResumeLayout(false);
            this.prpddPolarDisplaySettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showRadialLabelsRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarInitialPhaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarAmplitudeRadGroupBox)).EndInit();
            this.prpddPolarAmplitudeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarPcRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarVRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarDbRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarScaleRadGroupBox)).EndInit();
            this.prpddPolarScaleRadGroupBox.ResumeLayout(false);
            this.prpddPolarScaleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarScaleRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarChartLayoutRadGroupBox)).EndInit();
            this.prpddPolarChartLayoutRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarFifteenByOneRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarEightByTwoRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarFiveByThreeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prpddPolarThreeByFiveRadRadioButton)).EndInit();
            this.matrixRadPageViewPage.ResumeLayout(false);
            this.matrixRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddTabValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataReadingDatePrpddTabTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixRadPanel)).EndInit();
            this.matrixRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15RadGroupBox)).EndInit();
            this.matrixChart15RadGroupBox.ResumeLayout(false);
            this.matrixChart15RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14RadGroupBox)).EndInit();
            this.matrixChart14RadGroupBox.ResumeLayout(false);
            this.matrixChart14RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13RadGroupBox)).EndInit();
            this.matrixChart13RadGroupBox.ResumeLayout(false);
            this.matrixChart13RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12RadGroupBox)).EndInit();
            this.matrixChart12RadGroupBox.ResumeLayout(false);
            this.matrixChart12RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11RadGroupBox)).EndInit();
            this.matrixChart11RadGroupBox.ResumeLayout(false);
            this.matrixChart11RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10RadGroupBox)).EndInit();
            this.matrixChart10RadGroupBox.ResumeLayout(false);
            this.matrixChart10RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9RadGroupBox)).EndInit();
            this.matrixChart9RadGroupBox.ResumeLayout(false);
            this.matrixChart9RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8RadGroupBox)).EndInit();
            this.matrixChart8RadGroupBox.ResumeLayout(false);
            this.matrixChart8RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7RadGroupBox)).EndInit();
            this.matrixChart7RadGroupBox.ResumeLayout(false);
            this.matrixChart7RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6RadGroupBox)).EndInit();
            this.matrixChart6RadGroupBox.ResumeLayout(false);
            this.matrixChart6RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5RadGroupBox)).EndInit();
            this.matrixChart5RadGroupBox.ResumeLayout(false);
            this.matrixChart5RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4RadGroupBox)).EndInit();
            this.matrixChart4RadGroupBox.ResumeLayout(false);
            this.matrixChart4RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3RadGroupBox)).EndInit();
            this.matrixChart3RadGroupBox.ResumeLayout(false);
            this.matrixChart3RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2RadGroupBox)).EndInit();
            this.matrixChart2RadGroupBox.ResumeLayout(false);
            this.matrixChart2RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1RadGroupBox)).EndInit();
            this.matrixChart1RadGroupBox.ResumeLayout(false);
            this.matrixChart1RadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1PDIValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1PhaseShiftRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart15WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart14WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart13WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart12WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart10WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart9WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart8WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart7WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart5WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart4WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart3WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart2WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart11WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart6WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChart1WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixDisplaySettingsRadGroupBox)).EndInit();
            this.matrixDisplaySettingsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.matrixInitialPhaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixAmplitudeRadGroupBox)).EndInit();
            this.matrixAmplitudeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.matrixPcRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixVRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixDbRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixScaleRadGroupBox)).EndInit();
            this.matrixScaleRadGroupBox.ResumeLayout(false);
            this.matrixScaleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matrixScaleRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixChartLayoutRadGroupBox)).EndInit();
            this.matrixChartLayoutRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.matrixFifteenByOneRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixEightByTwoRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixFiveByThreeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matrixThreeByFiveRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataRadPageView)).EndInit();
            this.dataRadPageView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme2;
        private Telerik.WinControls.UI.RadPageViewPage phdRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox phdDisplaySettingsRadGroupBox;
        private Telerik.WinControls.UI.RadButton phdInitialPhaseRadButton;
        private Telerik.WinControls.UI.RadGroupBox phdAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton phdPcRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton phdVRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton phdDbRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox phdScaleRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList phdScaleRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox phdChartLayoutRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton phdFifteenByOneRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton phdEightByTwoRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton phdFiveByThreeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton phdThreeByFiveRadRadioButton;
        private Telerik.WinControls.UI.RadPanel phdRadPanel;
        private Telerik.WinControls.UI.RadGroupBox phdChart15RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart15PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadGroupBox phdChart14RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart14PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadGroupBox phdChart13RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart13PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadGroupBox phdChart12RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart12PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadGroupBox phdChart11RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart11PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel42;
        private Telerik.WinControls.UI.RadGroupBox phdChart10RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart10PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel47;
        private Telerik.WinControls.UI.RadGroupBox phdChart9RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart9PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel50;
        private Telerik.WinControls.UI.RadGroupBox phdChart8RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart8PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel53;
        private Telerik.WinControls.UI.RadGroupBox phdChart7RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart7PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel56;
        private Telerik.WinControls.UI.RadGroupBox phdChart6RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart6PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel59;
        private Telerik.WinControls.UI.RadGroupBox phdChart5RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart5PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel62;
        private Telerik.WinControls.UI.RadGroupBox phdChart4RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart4PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel65;
        private Telerik.WinControls.UI.RadGroupBox phdChart3RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart3PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel68;
        private Telerik.WinControls.UI.RadGroupBox phdChart2RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart2PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel71;
        private Telerik.WinControls.UI.RadGroupBox phdChart1RadGroupBox;
        private Telerik.WinControls.UI.RadLabel phdChart1PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel74;
        private ChartDirector.WinChartViewer phdChart15WinChartViewer;
        private ChartDirector.WinChartViewer phdChart14WinChartViewer;
        private ChartDirector.WinChartViewer phdChart13WinChartViewer;
        private ChartDirector.WinChartViewer phdChart12WinChartViewer;
        private ChartDirector.WinChartViewer phdChart10WinChartViewer;
        private ChartDirector.WinChartViewer phdChart9WinChartViewer;
        private ChartDirector.WinChartViewer phdChart8WinChartViewer;
        private ChartDirector.WinChartViewer phdChart7WinChartViewer;
        private ChartDirector.WinChartViewer phdChart5WinChartViewer;
        private ChartDirector.WinChartViewer phdChart4WinChartViewer;
        private ChartDirector.WinChartViewer phdChart3WinChartViewer;
        private ChartDirector.WinChartViewer phdChart2WinChartViewer;
        private ChartDirector.WinChartViewer phdChart11WinChartViewer;
        private ChartDirector.WinChartViewer phdChart6WinChartViewer;
        private ChartDirector.WinChartViewer phdChart1WinChartViewer;
        private Telerik.WinControls.UI.RadPageViewPage threeDeeRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox threeDeeDisplaySettingsRadGroupBox;
        private Telerik.WinControls.UI.RadButton threeDeeInitialPhaseRadButton;
        private Telerik.WinControls.UI.RadGroupBox threeDeeAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton threeDeePcRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton threeDeeVRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton threeDeeDbRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox threeDeeScaleRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList threeDeeScaleRadDropDownList;
        private Telerik.WinControls.UI.RadPanel threeDeeRadPanel;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart15RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart15PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel119;
        private Telerik.WinControls.UI.RadLabel radLabel120;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart15PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart14RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart14PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel116;
        private Telerik.WinControls.UI.RadLabel radLabel117;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart14PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart13RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart13PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel113;
        private Telerik.WinControls.UI.RadLabel radLabel114;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart13PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart12RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart12PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel110;
        private Telerik.WinControls.UI.RadLabel radLabel111;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart12PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart11RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart11PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel107;
        private Telerik.WinControls.UI.RadLabel radLabel108;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart11PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart6RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart6PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel104;
        private Telerik.WinControls.UI.RadLabel radLabel105;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart6PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart7RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart7PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel101;
        private Telerik.WinControls.UI.RadLabel radLabel102;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart7PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart8RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart8PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel98;
        private Telerik.WinControls.UI.RadLabel radLabel99;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart8PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart9RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart9PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel95;
        private Telerik.WinControls.UI.RadLabel radLabel96;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart9PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart10RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart10PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel92;
        private Telerik.WinControls.UI.RadLabel radLabel93;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart10PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart5RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart5PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel85;
        private Telerik.WinControls.UI.RadLabel radLabel88;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart5PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart4RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart4PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel76;
        private Telerik.WinControls.UI.RadLabel radLabel79;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart4PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart3RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart3PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel63;
        private Telerik.WinControls.UI.RadLabel radLabel67;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart3PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart2RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart2PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel49;
        private Telerik.WinControls.UI.RadLabel radLabel54;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart2PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChart1RadGroupBox;
        private Telerik.WinControls.UI.RadLabel threeDeeChart1PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadDropDownList threeDeeChart1PhaseShiftRadDropDownList;
        private ChartDirector.WinChartViewer threeDeeChart15WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart14WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart13WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart12WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart10WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart9WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart8WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart7WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart5WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart4WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart3WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart2WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart11WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart6WinChartViewer;
        private ChartDirector.WinChartViewer threeDeeChart1WinChartViewer;
        private Telerik.WinControls.UI.RadGroupBox threeDeeChartLayoutRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton threeDeeFifteenByOneRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton threeDeeEightByTwoRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton threeDeeFiveByThreeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton threeDeeThreeByFiveRadRadioButton;
        private Telerik.WinControls.UI.RadPageViewPage prpddPolarRadPageViewPage;
        private Telerik.WinControls.UI.RadPanel prpddPolarRadPanel;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart15RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart15PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart15PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart14RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart14PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart14PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart13RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart13PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel36;
        private Telerik.WinControls.UI.RadLabel radLabel39;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart13PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart12RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart12PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel46;
        private Telerik.WinControls.UI.RadLabel radLabel48;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart12PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart11RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart11PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel51;
        private Telerik.WinControls.UI.RadLabel radLabel52;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart11PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart10RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart10PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel55;
        private Telerik.WinControls.UI.RadLabel radLabel57;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart10PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart9RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart9PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel60;
        private Telerik.WinControls.UI.RadLabel radLabel61;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart9PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart8RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart8PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel64;
        private Telerik.WinControls.UI.RadLabel radLabel66;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart8PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart7RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart7PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel69;
        private Telerik.WinControls.UI.RadLabel radLabel70;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart7PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart6RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart6PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel73;
        private Telerik.WinControls.UI.RadLabel radLabel75;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart6PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart5RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart5PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel77;
        private Telerik.WinControls.UI.RadLabel radLabel78;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart5PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart4RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart4PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel80;
        private Telerik.WinControls.UI.RadLabel radLabel81;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart4PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart3RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart3PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel83;
        private Telerik.WinControls.UI.RadLabel radLabel84;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart3PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart2RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart2PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel86;
        private Telerik.WinControls.UI.RadLabel radLabel87;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart2PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChart1RadGroupBox;
        private Telerik.WinControls.UI.RadLabel prpddPolarChart1PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel89;
        private Telerik.WinControls.UI.RadLabel radLabel90;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarChart1PhaseShiftRadDropDownList;
        private ChartDirector.WinChartViewer prpddPolarChart15WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart14WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart13WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart12WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart10WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart9WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart8WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart7WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart5WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart4WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart3WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart2WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart11WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart6WinChartViewer;
        private ChartDirector.WinChartViewer prpddPolarChart1WinChartViewer;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarDisplaySettingsRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox showRadialLabelsRadCheckBox;
        private Telerik.WinControls.UI.RadButton prpddPolarInitialPhaseRadButton;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarPcRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarVRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarDbRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarScaleRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList prpddPolarScaleRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox prpddPolarChartLayoutRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarFifteenByOneRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarEightByTwoRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarFiveByThreeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton prpddPolarThreeByFiveRadRadioButton;
        private Telerik.WinControls.UI.RadPageViewPage matrixRadPageViewPage;
        private Telerik.WinControls.UI.RadPanel matrixRadPanel;
        private Telerik.WinControls.UI.RadGroupBox matrixChart15RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart15PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel43;
        private Telerik.WinControls.UI.RadLabel radLabel44;
        private Telerik.WinControls.UI.RadDropDownList matrixChart15PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart14RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart14PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel40;
        private Telerik.WinControls.UI.RadLabel radLabel41;
        private Telerik.WinControls.UI.RadDropDownList matrixChart14PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart13RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart13PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel37;
        private Telerik.WinControls.UI.RadLabel radLabel38;
        private Telerik.WinControls.UI.RadDropDownList matrixChart13PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart12RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart12PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel34;
        private Telerik.WinControls.UI.RadLabel radLabel35;
        private Telerik.WinControls.UI.RadDropDownList matrixChart12PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart11RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart11PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadDropDownList matrixChart11PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart10RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart10PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadDropDownList matrixChart10PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart9RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart9PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private Telerik.WinControls.UI.RadDropDownList matrixChart9PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart8RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart8PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadDropDownList matrixChart8PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart7RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart7PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadDropDownList matrixChart7PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart6RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart6PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadDropDownList matrixChart6PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart5RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart5PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadDropDownList matrixChart5PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart4RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart4PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadDropDownList matrixChart4PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart3RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart3PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList matrixChart3PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart2RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart2PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList matrixChart2PhaseShiftRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChart1RadGroupBox;
        private Telerik.WinControls.UI.RadLabel matrixChart1PDIValueRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel;
        private Telerik.WinControls.UI.RadDropDownList matrixChart1PhaseShiftRadDropDownList;
        private ChartDirector.WinChartViewer matrixChart15WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart14WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart13WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart12WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart10WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart9WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart8WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart7WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart5WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart4WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart3WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart2WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart11WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart6WinChartViewer;
        private ChartDirector.WinChartViewer matrixChart1WinChartViewer;
        private Telerik.WinControls.UI.RadGroupBox matrixDisplaySettingsRadGroupBox;
        private Telerik.WinControls.UI.RadButton matrixInitialPhaseRadButton;
        private Telerik.WinControls.UI.RadGroupBox matrixAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton matrixPcRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton matrixVRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton matrixDbRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox matrixScaleRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList matrixScaleRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox matrixChartLayoutRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton matrixFifteenByOneRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton matrixEightByTwoRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton matrixFiveByThreeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton matrixThreeByFiveRadRadioButton;
        private Telerik.WinControls.UI.RadPageView dataRadPageView;
        private Telerik.WinControls.UI.RadContextMenu trendRadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu dynamicsRadGroupBoxRadContextMenuStrip;
        private Telerik.WinControls.UI.RadContextMenu channelSelectRadGroupBoxRadContextMenuStrip;
        private System.Windows.Forms.ToolTip toolTip1;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePhdTabValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePhdTabTextRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpdd3DTabValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpdd3DTabTextRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpddPolarTabValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpddPolarTabTextRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpddTabValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dataReadingDatePrpddTabTextRadLabel;
    }
}