using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class SetDateAndTime : Telerik.WinControls.UI.RadForm
    {      
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string couldNotLoadDeviceDateAndTimeText = "Could not load current device date and time.\nCheck connection cables and communication configuration.";
        private static string failedToReadTheTimeFromTheDeviceText = "Failed to read the time from the device.";
        private static string failedToFindTheMonitorInTheDatabaseText = "Failed to find the monitor in the datbase.";

        private static string setDateAndTimeInterfaceTitleText = "Set Date and Time";

        private static string currentDeviceTimeRadGroupBoxText = "Current Device Time";
        private static string refreshDeviceTimeRadButtonText = "Refresh";
        private static string setTimeRadGroupBoxText = "Time to set";
        private static string timeRadLabelText = "Time";
        private static string resetDateTimeToPCDatetimeRadButtonText = "<html><font=Microsoft Sans Serif>Set to PC<br>date and time</html>";
        private static string setDatetimetoDeviceDateTimeRadButtonText = "<html><font=Microsoft Sans Serif>Set to device<br>date and time</html>";
        private static string setDeviceToSelectedDateTimeRadButtonText = "Set device time to indicated date and time";
        private static string cancelResetOfDateTimeRadButtonText = "Cancel reset of device date and time";

        /// <summary>
        /// This variable should be checked once the form has returned control to the calling program, since
        /// this indicates whether the user eventually decided to alter the date on the device
        /// </summary>
        private bool dateResetCancelled;
        /// <summary>
        /// This variable should be checked once the form has returned control to the calling program, since
        /// this indicates whether the user eventually decided to alter the date on the device
        /// </summary>
        public bool DateResetCancelled
        {
            get
            {
                return dateResetCancelled;
            }
        }

        /// <summary>
        /// This is how we transfer the date-time string in device format
        /// to the calling program
        /// </summary>
        private string deviceDateTimeString;
        /// <summary>
        /// This is how we transfer the date-time string in device format
        /// to the calling program
        /// </summary>
        public string DeviceDateTimeString
        {
            get
            {
                return deviceDateTimeString;
            }
        }

        private int modBusAddress;
        private bool isMainMonitor;

        int numberOfNonInteractiveRetries;
        int totalRetries;

        public SetDateAndTime(int inputModBusAddress, bool inputIsMainMonitor, int argNumberOfNonInteractiveRetries, int argTotalRetries)
        {
            try
            {
                InitializeComponent();
                AssignStringValuesToInterfaceObjects();

                modBusAddress = inputModBusAddress;
                isMainMonitor = inputIsMainMonitor;

                this.numberOfNonInteractiveRetries = argNumberOfNonInteractiveRetries;
                this.totalRetries = argTotalRetries;

                // initialize the time textbox to have the current time
                string testString = CreateStringTimeFromDateTime(DateTime.Now);
                //pdTimeRadMaskedEditBox.Text = testString;

                deviceDateTimeString = String.Empty;

                dateResetCancelled = true;
                // this keeps the calendar from showing the current date, which
                // can be confusing when the user changes the date on the calendar
                // radCalendar1.Show = false;

                // this makes typing the <esc> key the same as canceling the operation
                this.CancelButton = cancelResetOfDateTimeRadButton;

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.SetDateAndTime()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void resetDateToCurrentRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime currentDateTime = DateTime.Now;

                radCalendar1.SelectedDates.Clear();

                radCalendar1.SelectedDate = currentDateTime;
                radCalendar1.FocusedDate = currentDateTime;

                hourRadSpinEditor.Value = (decimal)currentDateTime.Hour;
                minuteRadSpinEditor.Value = (decimal)currentDateTime.Minute;
                secondRadSpinEditor.Value = (decimal)currentDateTime.Second;
                // pdTimeRadMaskedEditBox.Text = CreateStringTimeFromDateTime(currentDateTime);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.resetDateToCurrentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void setDatetimetoDeviceDateTimeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime deviceDateTime = LoadDeviceTime();

                radCalendar1.SelectedDates.Clear();

                //radCalendar1.
                radCalendar1.SelectedDate = deviceDateTime;
                radCalendar1.FocusedDate = deviceDateTime;


                hourRadSpinEditor.Value = (decimal)deviceDateTime.Hour;
                minuteRadSpinEditor.Value = (decimal)deviceDateTime.Minute;
                secondRadSpinEditor.Value = (decimal)deviceDateTime.Second;
                // pdTimeRadMaskedEditBox.Text = CreateStringTimeFromDateTime(currentDateTime);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.resetDateToCurrentRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetDateAndTime_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime deviceTime = LoadDeviceTime();

                DateTime currentTime = DateTime.Now;
                radCalendar1.SelectedDate = deviceTime;
                radCalendar1.AllowMultipleSelect = false;
                radCalendar1.AllowMultipleView = false;
                //this.pdTimeRadMaskedEditBox.Text = CreateStringTimeFromDateTime(currentTime);
              
                hourRadSpinEditor.Value = (decimal)deviceTime.Hour;
                minuteRadSpinEditor.Value = (decimal)deviceTime.Minute;
                secondRadSpinEditor.Value = (decimal)deviceTime.Second;
                
                if (deviceTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                {
                    this.currentTimeRadLabel.Text = deviceTime.ToString();
                }
                else
                {
                    RadMessageBox.Show(couldNotLoadDeviceDateAndTimeText);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.SetDateAndTime_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private DateTime LoadDeviceTime()
        {
            DateTime deviceDateTime = ConversionMethods.MinimumDateTime();
            try
            {
                ErrorCode errorCode;
                int deviceIsBusy;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if (modBusAddress > 0)
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToAnyMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        if (!isMainMonitor)
                        {
                            deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                            if (deviceIsBusy == 0)
                            {
                                if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                {
                                    deviceDateTime = InteractiveDeviceCommunication.GetDeviceTime(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                    InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                                }
                            }
                            else if (deviceIsBusy == 1)
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                            }
                        }
                        else
                        {
                            deviceDateTime = InteractiveDeviceCommunication.GetDeviceTime(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.LoadDeviceTime(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
            return deviceDateTime;
        }

        private void cancelResetOfDateRadButton_Click(object sender, EventArgs e)
        {
            dateResetCancelled = true;
            this.Close();
        }

        /// <summary>
        /// Creates a string in the device time format from an
        /// input .net DateTime object
        /// </summary>
        /// <param name="inputDateTime"></param>
        /// <returns></returns>
        string CreateStringDateAndTimeFromDateTime(DateTime inputDateTime)
        {
            int day, month, year;
            int hour, minute, second;
            StringBuilder stringifiedTime = new StringBuilder();
            day = inputDateTime.Day;
            month = inputDateTime.Month;
            year = inputDateTime.Year;
            hour = inputDateTime.Hour;
            minute = inputDateTime.Minute;
            second = inputDateTime.Second;

            try
            {
                if (month < 10)
                {
                    stringifiedTime.Append("0");
                }
                stringifiedTime.Append(month.ToString());
                stringifiedTime.Append("/");

                if (day < 10)
                {
                    stringifiedTime.Append("0");
                }
                stringifiedTime.Append(day.ToString());
                stringifiedTime.Append("/");

                stringifiedTime.Append(year.ToString());

                stringifiedTime.Append(" ");

                stringifiedTime.Append(CreateStringTimeFromDateTime(inputDateTime));
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.CreateStringTimeFromDateTime(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringifiedTime.ToString();
        }

        private string CreateStringTimeFromDateTime(DateTime inputDateTime)
        {
            int hour, minute, second;
            StringBuilder stringifiedTime = new StringBuilder();
            try
            {
                hour = inputDateTime.Hour;
                minute = inputDateTime.Minute;
                second = inputDateTime.Second;

                /// the device date-time format requires padding for any time
                /// in the single digits
                if (hour < 10)
                {
                    stringifiedTime.Append("0");
                }
                stringifiedTime.Append(hour.ToString());
                stringifiedTime.Append(":");

                if (minute < 10)
                {
                    stringifiedTime.Append("0");
                }
                stringifiedTime.Append(minute.ToString());
                stringifiedTime.Append(":");

                if (second < 10)
                {
                    stringifiedTime.Append("0");
                }
                stringifiedTime.Append(second.ToString());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.CreateStringTimeFromDateTime(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringifiedTime.ToString();
        }

        /// <summary>
        /// Takes an entry in the dateTimeMaskedTextBox format and extracts the hours, minutes, and seconds
        /// from that string and returns them in an integer array.  A value of -1 for any entry indicates that
        /// the value was out of bounds.
        /// </summary>
        /// <param name="timeString"></param>
        /// <returns></returns>
        int[] ExtractTimeElementsFromTimeString(string timeString)
        {
            int hour, minute, second;
            string[] entries;
            int[] timeDigits = new int[3];
            try
            {
                if (timeString != null)
                {
                    entries = timeString.Split(':');

                    /// this will hopefully only show up in a debug situation, but otherwise avoids
                    /// some exceptions due to the user perhaps clearing the text box and then not putting
                    /// values in for some entries
                    if (entries.Length != 3)
                    {
                        MessageBox.Show("In ExtractTimeElementsFromTimeString, wrong input string format");
                    }
                    else
                    {
                        /// we can assume these are always integers because the masked text box that is the source
                        /// of the input string timeString only allows integer values to be typed in
                        hour = Int32.Parse(entries[0]);
                        minute = Int32.Parse(entries[1]);
                        second = Int32.Parse(entries[2]);

                        if ((hour < 0) || (hour > 23))
                        {
                            hour = -1;
                        }
                        if ((minute < 0) || (minute > 59))
                        {
                            minute = -1;
                        }
                        if ((second < 0) || (second > 59))
                        {
                            second = -1;
                        }

                        timeDigits[0] = hour;
                        timeDigits[1] = minute;
                        timeDigits[2] = second;
                    }
                }
                else
                {
                    string errorMessage = "Error in SetDateAndTime.ExtractTimeElementsFromTimeString(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.ExtractTimeElementsFromTimeString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return timeDigits;
        }

        private void setDeviceToSelectedDateRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                int hour, minute, second;
                StringBuilder errorString = new StringBuilder();
                DateTime desiredDate = radCalendar1.SelectedDate;
                ErrorCode errorCode;
                //int[] timeElements = ExtractTimeElementsFromTimeString(pdTimeRadMaskedEditBox.Text);
                //hour = timeElements[0];
                //minute = timeElements[1];
                //second = timeElements[2];
                string commandString;
                string returnString;
                int deviceIsBusy;

                bool goAheadAndSetTheTime = true;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                hour = (int)hourRadSpinEditor.Value;
                minute = (int)minuteRadSpinEditor.Value;
                second = (int)secondRadSpinEditor.Value;

                /// if any of these comparisons are true, the user entered a bad value for at least one of
                /// the time entries
                if ((hour < 0) || (hour > 23) || (minute < 0) || (minute > 59) || (second < 0) || (second > 59))
                {
                    errorString.Append("Error in the time format\n\n");
                    /// this just builds the error string so the user knows which time entries were incorrect
                    if ((hour < 0) || (hour > 23))
                    {
                        errorString.Append("Hour must be between 0 and 23 inclusive\n");
                    }
                    if ((minute < 0) || (minute > 59))
                    {
                        errorString.Append("Minute must be between 0 and 59 inclusive\n");
                    }
                    if ((second < 0) || (second > 59))
                    {
                        errorString.Append("Second must be between 0 and 59 inclusive");
                    }
                    MessageBox.Show(errorString.ToString());
                }
                else
                {
                    commandString = "TIME " + ConversionMethods.CreateDeviceDateTimeString(desiredDate, hour, minute, second) + ";";

                    if (modBusAddress > 0)
                    {
                        errorCode = SimplifiedCommunication.OpenUsbConnectionToAnyMonitor(modBusAddress, 20, 200, this.numberOfNonInteractiveRetries, this.totalRetries);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            if (!this.isMainMonitor)
                            {
                                deviceIsBusy = InteractiveDeviceCommunication.DeviceIsBusy(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if (deviceIsBusy == 0)
                                {
                                    if (InteractiveDeviceCommunication.PauseDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation))
                                    {
                                        goAheadAndSetTheTime = true;
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DevicePauseFailed));
                                        goAheadAndSetTheTime = false;
                                    }
                                }
                                else if (deviceIsBusy == 1)
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceWasBusy));
                                    goAheadAndSetTheTime = false;
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceBusyReadFailed));
                                    goAheadAndSetTheTime = false;
                                }
                            }
                            else
                            {
                                goAheadAndSetTheTime = true;
                            }
                            if (goAheadAndSetTheTime)
                            {
                                returnString = InteractiveDeviceCommunication.SendStringCommand(modBusAddress, commandString, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                if (!DeviceCommunication.StringCommandWasSuccessful(returnString))
                                {
                                    RadMessageBox.Show("Failed to write the new date and time to the device");
                                }
                                else
                                {
                                    RadMessageBox.Show("Wrote the date and time to the device");
                                }
                                if (!this.isMainMonitor)
                                {
                                    InteractiveDeviceCommunication.ResumeDevice(modBusAddress, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                                }
                            }                          
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.setDeviceToSelectedDateRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DeviceCommunication.CloseConnection();
            }
        }

        private void refreshDeviceTimeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
            DateTime deviceTime = LoadDeviceTime();
            if (deviceTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
            {
                this.currentTimeRadLabel.Text = deviceTime.ToString();
            }
            else
            {
                RadMessageBox.Show(couldNotLoadDeviceDateAndTimeText);
            }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.refreshDeviceTimeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = setDateAndTimeInterfaceTitleText;

                currentDeviceTimeRadGroupBox.Text = currentDeviceTimeRadGroupBoxText;
                refreshDeviceTimeRadButton.Text = refreshDeviceTimeRadButtonText;
                setTimeRadGroupBox.Text = setTimeRadGroupBoxText;
                timeRadLabel.Text = timeRadLabelText;
                resetDateTimeToPCDatetimeRadButton.Text = resetDateTimeToPCDatetimeRadButtonText;
                setDatetimetoDeviceDateTimeRadButton.Text = setDatetimetoDeviceDateTimeRadButtonText;
                setDeviceToSelectedDateTimeRadButton.Text = setDeviceToSelectedDateTimeRadButtonText;
                cancelResetOfDateTimeRadButton.Text = cancelResetOfDateTimeRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                couldNotLoadDeviceDateAndTimeText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeCouldNotLoadDeviceDateAndTimeText", couldNotLoadDeviceDateAndTimeText, htmlFontType, htmlStandardFontSize, "");
                failedToReadTheTimeFromTheDeviceText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeFailedToReadTheTimeFromTheDeviceText", failedToReadTheTimeFromTheDeviceText, htmlFontType, htmlStandardFontSize, "");
                failedToFindTheMonitorInTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeFailedToFindTheMonitorInTheDatabaseText", failedToFindTheMonitorInTheDatabaseText, htmlFontType, htmlStandardFontSize, "");

                setDateAndTimeInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceTitleText", setDateAndTimeInterfaceTitleText, htmlFontType, htmlStandardFontSize, "");
                currentDeviceTimeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceCurrentDeviceTimeRadGroupBoxText", currentDeviceTimeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                refreshDeviceTimeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceRefreshDeviceTimeRadButtonText", refreshDeviceTimeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                setTimeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceSetTimeRadGroupBoxText", setTimeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                timeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceTimeRadLabelText", timeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                resetDateTimeToPCDatetimeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceResetDatetimeToPCDateTimeRadButtonText", resetDateTimeToPCDatetimeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                setDatetimetoDeviceDateTimeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceSetDatetimeToDeviceDateTimeRadButtonText", setDatetimetoDeviceDateTimeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                setDeviceToSelectedDateTimeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceSetDeviceToSelectedDateTimeRadButtonText", setDeviceToSelectedDateTimeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelResetOfDateTimeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDateAndTimeInterfaceCancelResetOfDateTimeRadButtonText", cancelResetOfDateTimeRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDateAndTime.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
