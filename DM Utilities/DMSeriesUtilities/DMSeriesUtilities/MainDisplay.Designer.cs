﻿namespace DMSeriesUtilities
{
    partial class MainDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDisplay));
            this.readRegistersRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.searchProgressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.startingRegisterTextReadRegistersTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.startingRegisterReadRegistersTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.readRegistersReadRegistersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.registerValuesRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.modbusAddressTextReadRegistersTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressReadRegistersTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.findRecorsWithPhaseResolvedDataRadButton = new Telerik.WinControls.UI.RadButton();
            this.mainMonitorRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.windingHotSpotConfigurationRadButton = new Telerik.WinControls.UI.RadButton();
            this.openDnpSettingsRadButton = new Telerik.WinControls.UI.RadButton();
            this.calibrateAnalogSignalsRadButton = new Telerik.WinControls.UI.RadButton();
            this.deleteDeviceDataMainTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceConfigurationMainTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.mainFirmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.mainFirmwareVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.eventLogMainTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.setDeviceDateMainTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.modbusAddressTextMainTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressMainTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dynamicsMainRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.getDynamicsRadButton = new Telerik.WinControls.UI.RadButton();
            this.mainDynamicsDataRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.pdMonitorRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.calibrateChannelsRadButton = new Telerik.WinControls.UI.RadButton();
            this.deleteDeviceDataPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.pdmSaveEventLogsToFileRadButton = new Telerik.WinControls.UI.RadButton();
            this.pdmFirmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.pdmFirmwareVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.pmdMeasurementsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.cycleOfAcqSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.setModeRadButton = new Telerik.WinControls.UI.RadButton();
            this.trendZoomButtonsRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.normalModeRadioButton = new System.Windows.Forms.RadioButton();
            this.testModeRadioButton = new System.Windows.Forms.RadioButton();
            this.startMeasurementPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.eventLogPdmTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.pdmChannelDataRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.pdmDataDateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.viewPhaseResolvedDataPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.getLatestReadingsPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceConfigurationPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.getAlertsPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.getDeviceErrorsPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.clearDisplayPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.readEventLogsPdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.setDeviceDatePdmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.modbusAddressTextPdmTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressPdmTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.utilitiesRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.bushingMonitorRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButtonBushingCalc = new Telerik.WinControls.UI.RadButton();
            this.getDeviceHealthBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deleteDeviceDataBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.bhmSaveEventLogsToFileRadButton = new Telerik.WinControls.UI.RadButton();
            this.bhmFirmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.bhmFirmwareVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.setDeviceDateBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceConfigurationBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.getEquipmentErrorBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.clearListBoxBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.latestReadingsBhmTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.bhmInitialDataRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.bhmLatestDataReadingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.initialDataReadingsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.latestDataReadingsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.getLatestReadingsBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.modbusAddressTextBhmTabRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressBhmTabRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.setUpAutobalanceBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.startMeasurementBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.balanceBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.readEventLogsBhmTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.eventLogBhmTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.mainDisplayRadMenu = new Telerik.WinControls.UI.RadMenu();
            this.readRegistersRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchProgressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRegistersToReadTextReadRegistersTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRegistersReadRegistersTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startingRegisterTextReadRegistersTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startingRegisterReadRegistersTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readRegistersReadRegistersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValuesRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValuesRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextReadRegistersTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressReadRegistersTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.findRecorsWithPhaseResolvedDataRadButton)).BeginInit();
            this.mainMonitorRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windingHotSpotConfigurationRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openDnpSettingsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateAnalogSignalsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataMainTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationMainTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFirmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFirmwareVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogMainTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDateMainTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextMainTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressMainTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsMainRadGroupBox)).BeginInit();
            this.dynamicsMainRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.getDynamicsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDynamicsDataRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDynamicsDataRadGridView.MasterTemplate)).BeginInit();
            this.pdMonitorRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateChannelsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmSaveEventLogsToFileRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmFirmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmFirmwareVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmdMeasurementsRadGroupBox)).BeginInit();
            this.pmdMeasurementsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cycleOfAcqSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setModeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).BeginInit();
            this.trendZoomButtonsRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startMeasurementPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogPdmTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdmChannelDataRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmChannelDataRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmDataDateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPhaseResolvedDataPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLatestReadingsPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAlertsPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDeviceErrorsPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearDisplayPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readEventLogsPdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDatePdmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextPdmTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressPdmTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.utilitiesRadPageView)).BeginInit();
            this.utilitiesRadPageView.SuspendLayout();
            this.bushingMonitorRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBushingCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDeviceHealthBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmSaveEventLogsToFileRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmFirmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmFirmwareVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDateBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getEquipmentErrorBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearListBoxBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programLogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.latestReadingsBhmTabRadGroupBox)).BeginInit();
            this.latestReadingsBhmTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bhmInitialDataRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmInitialDataRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmLatestDataReadingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmLatestDataReadingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialDataReadingsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.latestDataReadingsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLatestReadingsBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextBhmTabRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressBhmTabRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setUpAutobalanceBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startMeasurementBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.balanceBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.readEventLogsBhmTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogBhmTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // readRegistersRadPageViewPage
            // 
            this.readRegistersRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.readRegistersRadPageViewPage.Controls.Add(this.searchProgressRadLabel);
            this.readRegistersRadPageViewPage.Controls.Add(this.numberOfRegistersToReadTextReadRegistersTabRadLabel);
            this.readRegistersRadPageViewPage.Controls.Add(this.numberOfRegistersReadRegistersTabRadMaskedEditBox);
            this.readRegistersRadPageViewPage.Controls.Add(this.startingRegisterTextReadRegistersTabRadLabel);
            this.readRegistersRadPageViewPage.Controls.Add(this.startingRegisterReadRegistersTabRadMaskedEditBox);
            this.readRegistersRadPageViewPage.Controls.Add(this.readRegistersReadRegistersTabRadButton);
            this.readRegistersRadPageViewPage.Controls.Add(this.registerValuesRadGridView);
            this.readRegistersRadPageViewPage.Controls.Add(this.modbusAddressTextReadRegistersTabRadLabel);
            this.readRegistersRadPageViewPage.Controls.Add(this.modBusAddressReadRegistersTabRadMaskedEditBox);
            this.readRegistersRadPageViewPage.Controls.Add(this.pictureBox3);
            this.readRegistersRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.readRegistersRadPageViewPage.Name = "readRegistersRadPageViewPage";
            this.readRegistersRadPageViewPage.Size = new System.Drawing.Size(995, 664);
            this.readRegistersRadPageViewPage.Text = "Read Registers";
            // 
            // searchProgressRadLabel
            // 
            this.searchProgressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.searchProgressRadLabel.Location = new System.Drawing.Point(72, 391);
            this.searchProgressRadLabel.Name = "searchProgressRadLabel";
            this.searchProgressRadLabel.Size = new System.Drawing.Size(90, 16);
            this.searchProgressRadLabel.TabIndex = 59;
            this.searchProgressRadLabel.Text = "Search Progress";
            // 
            // numberOfRegistersToReadTextReadRegistersTabRadLabel
            // 
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.Location = new System.Drawing.Point(314, 88);
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.Name = "numberOfRegistersToReadTextReadRegistersTabRadLabel";
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.Size = new System.Drawing.Size(56, 27);
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.TabIndex = 55;
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.Text = "<html>Number of<br>Registers</html>";
            this.numberOfRegistersToReadTextReadRegistersTabRadLabel.ThemeName = "Office2007Black";
            // 
            // numberOfRegistersReadRegistersTabRadMaskedEditBox
            // 
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.AutoSize = true;
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.Location = new System.Drawing.Point(409, 97);
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.Name = "numberOfRegistersReadRegistersTabRadMaskedEditBox";
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.Size = new System.Drawing.Size(62, 18);
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.TabIndex = 2;
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.TabStop = false;
            this.numberOfRegistersReadRegistersTabRadMaskedEditBox.Text = "5";
            // 
            // startingRegisterTextReadRegistersTabRadLabel
            // 
            this.startingRegisterTextReadRegistersTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startingRegisterTextReadRegistersTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.startingRegisterTextReadRegistersTabRadLabel.Location = new System.Drawing.Point(314, 58);
            this.startingRegisterTextReadRegistersTabRadLabel.Name = "startingRegisterTextReadRegistersTabRadLabel";
            this.startingRegisterTextReadRegistersTabRadLabel.Size = new System.Drawing.Size(85, 15);
            this.startingRegisterTextReadRegistersTabRadLabel.TabIndex = 57;
            this.startingRegisterTextReadRegistersTabRadLabel.Text = "<html>Starting register</html>";
            this.startingRegisterTextReadRegistersTabRadLabel.ThemeName = "Office2007Black";
            // 
            // startingRegisterReadRegistersTabRadMaskedEditBox
            // 
            this.startingRegisterReadRegistersTabRadMaskedEditBox.AutoSize = true;
            this.startingRegisterReadRegistersTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startingRegisterReadRegistersTabRadMaskedEditBox.Location = new System.Drawing.Point(409, 58);
            this.startingRegisterReadRegistersTabRadMaskedEditBox.Name = "startingRegisterReadRegistersTabRadMaskedEditBox";
            this.startingRegisterReadRegistersTabRadMaskedEditBox.Size = new System.Drawing.Size(62, 18);
            this.startingRegisterReadRegistersTabRadMaskedEditBox.TabIndex = 1;
            this.startingRegisterReadRegistersTabRadMaskedEditBox.TabStop = false;
            this.startingRegisterReadRegistersTabRadMaskedEditBox.Text = "0";
            // 
            // readRegistersReadRegistersTabRadButton
            // 
            this.readRegistersReadRegistersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readRegistersReadRegistersTabRadButton.Location = new System.Drawing.Point(314, 139);
            this.readRegistersReadRegistersTabRadButton.Name = "readRegistersReadRegistersTabRadButton";
            this.readRegistersReadRegistersTabRadButton.Size = new System.Drawing.Size(157, 30);
            this.readRegistersReadRegistersTabRadButton.TabIndex = 3;
            this.readRegistersReadRegistersTabRadButton.Text = "<html>Read Registers</html>";
            this.readRegistersReadRegistersTabRadButton.ThemeName = "Office2007Black";
            this.readRegistersReadRegistersTabRadButton.Click += new System.EventHandler(this.readRegistersReadRegistersTabRadButton_Click);
            // 
            // registerValuesRadGridView
            // 
            this.registerValuesRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.registerValuesRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerValuesRadGridView.Location = new System.Drawing.Point(477, 3);
            // 
            // 
            // 
            this.registerValuesRadGridView.MasterTemplate.AllowAddNewRow = false;
            this.registerValuesRadGridView.MasterTemplate.AllowColumnReorder = false;
            this.registerValuesRadGridView.Name = "registerValuesRadGridView";
            this.registerValuesRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.registerValuesRadGridView.ReadOnly = true;
            // 
            // 
            // 
            this.registerValuesRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.registerValuesRadGridView.Size = new System.Drawing.Size(515, 550);
            this.registerValuesRadGridView.TabIndex = 54;
            this.registerValuesRadGridView.Text = "radGridView1";
            this.registerValuesRadGridView.ThemeName = "Office2007Black";
            this.registerValuesRadGridView.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.registerValuesRadGridView_CellDoubleClick);
            // 
            // modbusAddressTextReadRegistersTabRadLabel
            // 
            this.modbusAddressTextReadRegistersTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressTextReadRegistersTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressTextReadRegistersTabRadLabel.Location = new System.Drawing.Point(12, 21);
            this.modbusAddressTextReadRegistersTabRadLabel.Name = "modbusAddressTextReadRegistersTabRadLabel";
            this.modbusAddressTextReadRegistersTabRadLabel.Size = new System.Drawing.Size(91, 15);
            this.modbusAddressTextReadRegistersTabRadLabel.TabIndex = 53;
            this.modbusAddressTextReadRegistersTabRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressTextReadRegistersTabRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressReadRegistersTabRadMaskedEditBox
            // 
            this.modBusAddressReadRegistersTabRadMaskedEditBox.AutoSize = true;
            this.modBusAddressReadRegistersTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressReadRegistersTabRadMaskedEditBox.Location = new System.Drawing.Point(107, 21);
            this.modBusAddressReadRegistersTabRadMaskedEditBox.Name = "modBusAddressReadRegistersTabRadMaskedEditBox";
            this.modBusAddressReadRegistersTabRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressReadRegistersTabRadMaskedEditBox.TabIndex = 0;
            this.modBusAddressReadRegistersTabRadMaskedEditBox.TabStop = false;
            this.modBusAddressReadRegistersTabRadMaskedEditBox.Text = "0";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(719, 559);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(273, 102);
            this.pictureBox3.TabIndex = 51;
            this.pictureBox3.TabStop = false;
            // 
            // findRecorsWithPhaseResolvedDataRadButton
            // 
            this.findRecorsWithPhaseResolvedDataRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.findRecorsWithPhaseResolvedDataRadButton.Location = new System.Drawing.Point(354, 193);
            this.findRecorsWithPhaseResolvedDataRadButton.Name = "findRecorsWithPhaseResolvedDataRadButton";
            this.findRecorsWithPhaseResolvedDataRadButton.Size = new System.Drawing.Size(193, 46);
            this.findRecorsWithPhaseResolvedDataRadButton.TabIndex = 58;
            this.findRecorsWithPhaseResolvedDataRadButton.Text = "<html>Find Records with phase resolved data</html>";
            this.findRecorsWithPhaseResolvedDataRadButton.ThemeName = "Office2007Black";
            this.findRecorsWithPhaseResolvedDataRadButton.Visible = false;
            this.findRecorsWithPhaseResolvedDataRadButton.Click += new System.EventHandler(this.findRecorsWithPhaseResolvedDataRadButton_Click);
            // 
            // mainMonitorRadPageViewPage
            // 
            this.mainMonitorRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.mainMonitorRadPageViewPage.Controls.Add(this.radButton1);
            this.mainMonitorRadPageViewPage.Controls.Add(this.windingHotSpotConfigurationRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.openDnpSettingsRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.calibrateAnalogSignalsRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.deleteDeviceDataMainTabRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.deviceConfigurationMainTabRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.mainFirmwareVersionValueRadLabel);
            this.mainMonitorRadPageViewPage.Controls.Add(this.mainFirmwareVersionTextRadLabel);
            this.mainMonitorRadPageViewPage.Controls.Add(this.eventLogMainTabRadListControl);
            this.mainMonitorRadPageViewPage.Controls.Add(this.setDeviceDateMainTabRadButton);
            this.mainMonitorRadPageViewPage.Controls.Add(this.modbusAddressTextMainTabRadLabel);
            this.mainMonitorRadPageViewPage.Controls.Add(this.modBusAddressMainTabRadMaskedEditBox);
            this.mainMonitorRadPageViewPage.Controls.Add(this.pictureBox2);
            this.mainMonitorRadPageViewPage.Controls.Add(this.dynamicsMainRadGroupBox);
            this.mainMonitorRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.mainMonitorRadPageViewPage.Name = "mainMonitorRadPageViewPage";
            this.mainMonitorRadPageViewPage.Size = new System.Drawing.Size(995, 664);
            this.mainMonitorRadPageViewPage.Text = "Main";
            this.mainMonitorRadPageViewPage.Paint += new System.Windows.Forms.PaintEventHandler(this.mainMonitorRadPageViewPage_Paint);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(274, 79);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(125, 35);
            this.radButton1.TabIndex = 67;
            this.radButton1.Text = "Get Errors";
            this.radButton1.ThemeName = "Office2007Black";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // windingHotSpotConfigurationRadButton
            // 
            this.windingHotSpotConfigurationRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windingHotSpotConfigurationRadButton.Location = new System.Drawing.Point(143, 79);
            this.windingHotSpotConfigurationRadButton.Name = "windingHotSpotConfigurationRadButton";
            this.windingHotSpotConfigurationRadButton.Size = new System.Drawing.Size(125, 35);
            this.windingHotSpotConfigurationRadButton.TabIndex = 69;
            this.windingHotSpotConfigurationRadButton.Text = "<html>Winding Hot Spot<br>Configuration</html>";
            this.windingHotSpotConfigurationRadButton.ThemeName = "Office2007Black";
            this.windingHotSpotConfigurationRadButton.Click += new System.EventHandler(this.windingHotSpotConfigurationRadButton_Click);
            // 
            // openDnpSettingsRadButton
            // 
            this.openDnpSettingsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openDnpSettingsRadButton.Location = new System.Drawing.Point(424, 496);
            this.openDnpSettingsRadButton.Name = "openDnpSettingsRadButton";
            this.openDnpSettingsRadButton.Size = new System.Drawing.Size(51, 19);
            this.openDnpSettingsRadButton.TabIndex = 68;
            this.openDnpSettingsRadButton.Text = "<html>Open DNP<br>Settings</html>";
            this.openDnpSettingsRadButton.ThemeName = "Office2007Black";
            this.openDnpSettingsRadButton.Visible = false;
            this.openDnpSettingsRadButton.Click += new System.EventHandler(this.openDnpSettingsRadButton_Click);
            // 
            // calibrateAnalogSignalsRadButton
            // 
            this.calibrateAnalogSignalsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrateAnalogSignalsRadButton.Location = new System.Drawing.Point(12, 79);
            this.calibrateAnalogSignalsRadButton.Name = "calibrateAnalogSignalsRadButton";
            this.calibrateAnalogSignalsRadButton.Size = new System.Drawing.Size(125, 35);
            this.calibrateAnalogSignalsRadButton.TabIndex = 67;
            this.calibrateAnalogSignalsRadButton.Text = "<html>Calibrate Voltage <br> and Current Inputs</html>";
            this.calibrateAnalogSignalsRadButton.ThemeName = "Office2007Black";
            this.calibrateAnalogSignalsRadButton.Click += new System.EventHandler(this.calibrateAnalogSignalsRadButton_Click);
            // 
            // deleteDeviceDataMainTabRadButton
            // 
            this.deleteDeviceDataMainTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteDeviceDataMainTabRadButton.Location = new System.Drawing.Point(274, 43);
            this.deleteDeviceDataMainTabRadButton.Name = "deleteDeviceDataMainTabRadButton";
            this.deleteDeviceDataMainTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deleteDeviceDataMainTabRadButton.TabIndex = 66;
            this.deleteDeviceDataMainTabRadButton.Text = "<html>Delete Device Data</html>";
            this.deleteDeviceDataMainTabRadButton.ThemeName = "Office2007Black";
            this.deleteDeviceDataMainTabRadButton.Click += new System.EventHandler(this.deleteDeviceDataMainTabRadButton_Click);
            // 
            // deviceConfigurationMainTabRadButton
            // 
            this.deviceConfigurationMainTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceConfigurationMainTabRadButton.Location = new System.Drawing.Point(143, 43);
            this.deviceConfigurationMainTabRadButton.Name = "deviceConfigurationMainTabRadButton";
            this.deviceConfigurationMainTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deviceConfigurationMainTabRadButton.TabIndex = 65;
            this.deviceConfigurationMainTabRadButton.Text = "<html>Device Configuration</html>";
            this.deviceConfigurationMainTabRadButton.ThemeName = "Office2007Black";
            this.deviceConfigurationMainTabRadButton.Click += new System.EventHandler(this.deviceConfigurationMainTabRadButton_Click);
            // 
            // mainFirmwareVersionValueRadLabel
            // 
            this.mainFirmwareVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainFirmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.mainFirmwareVersionValueRadLabel.Location = new System.Drawing.Point(272, 11);
            this.mainFirmwareVersionValueRadLabel.Name = "mainFirmwareVersionValueRadLabel";
            this.mainFirmwareVersionValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.mainFirmwareVersionValueRadLabel.TabIndex = 60;
            this.mainFirmwareVersionValueRadLabel.Text = "0.00";
            this.mainFirmwareVersionValueRadLabel.ThemeName = "Office2007Black";
            // 
            // mainFirmwareVersionTextRadLabel
            // 
            this.mainFirmwareVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainFirmwareVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.mainFirmwareVersionTextRadLabel.Location = new System.Drawing.Point(174, 11);
            this.mainFirmwareVersionTextRadLabel.Name = "mainFirmwareVersionTextRadLabel";
            this.mainFirmwareVersionTextRadLabel.Size = new System.Drawing.Size(92, 15);
            this.mainFirmwareVersionTextRadLabel.TabIndex = 59;
            this.mainFirmwareVersionTextRadLabel.Text = "<html>Firmware version</html>";
            this.mainFirmwareVersionTextRadLabel.ThemeName = "Office2007Black";
            // 
            // eventLogMainTabRadListControl
            // 
            this.eventLogMainTabRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLogMainTabRadListControl.AutoScroll = true;
            this.eventLogMainTabRadListControl.CaseSensitiveSort = true;
            this.eventLogMainTabRadListControl.ItemHeight = 18;
            this.eventLogMainTabRadListControl.Location = new System.Drawing.Point(501, 21);
            this.eventLogMainTabRadListControl.Name = "eventLogMainTabRadListControl";
            this.eventLogMainTabRadListControl.Size = new System.Drawing.Size(463, 506);
            this.eventLogMainTabRadListControl.TabIndex = 58;
            this.eventLogMainTabRadListControl.Text = "radListControl2";
            this.eventLogMainTabRadListControl.ThemeName = "ControlDefault";
            // 
            // setDeviceDateMainTabRadButton
            // 
            this.setDeviceDateMainTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setDeviceDateMainTabRadButton.Location = new System.Drawing.Point(12, 43);
            this.setDeviceDateMainTabRadButton.Name = "setDeviceDateMainTabRadButton";
            this.setDeviceDateMainTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.setDeviceDateMainTabRadButton.TabIndex = 56;
            this.setDeviceDateMainTabRadButton.Text = "<html>Set Device Date</html>";
            this.setDeviceDateMainTabRadButton.ThemeName = "Office2007Black";
            this.setDeviceDateMainTabRadButton.Click += new System.EventHandler(this.setDeviceDateMainTabRadButton_Click);
            // 
            // modbusAddressTextMainTabRadLabel
            // 
            this.modbusAddressTextMainTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressTextMainTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressTextMainTabRadLabel.Location = new System.Drawing.Point(12, 11);
            this.modbusAddressTextMainTabRadLabel.Name = "modbusAddressTextMainTabRadLabel";
            this.modbusAddressTextMainTabRadLabel.Size = new System.Drawing.Size(91, 15);
            this.modbusAddressTextMainTabRadLabel.TabIndex = 53;
            this.modbusAddressTextMainTabRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressTextMainTabRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressMainTabRadMaskedEditBox
            // 
            this.modBusAddressMainTabRadMaskedEditBox.AutoSize = true;
            this.modBusAddressMainTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressMainTabRadMaskedEditBox.Location = new System.Drawing.Point(107, 11);
            this.modBusAddressMainTabRadMaskedEditBox.Name = "modBusAddressMainTabRadMaskedEditBox";
            this.modBusAddressMainTabRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressMainTabRadMaskedEditBox.TabIndex = 52;
            this.modBusAddressMainTabRadMaskedEditBox.TabStop = false;
            this.modBusAddressMainTabRadMaskedEditBox.Text = "0";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(719, 559);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(273, 102);
            this.pictureBox2.TabIndex = 51;
            this.pictureBox2.TabStop = false;
            // 
            // dynamicsMainRadGroupBox
            // 
            this.dynamicsMainRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dynamicsMainRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dynamicsMainRadGroupBox.Controls.Add(this.getDynamicsRadButton);
            this.dynamicsMainRadGroupBox.Controls.Add(this.mainDynamicsDataRadGridView);
            this.dynamicsMainRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dynamicsMainRadGroupBox.FooterImageIndex = -1;
            this.dynamicsMainRadGroupBox.FooterImageKey = "";
            this.dynamicsMainRadGroupBox.HeaderImageIndex = -1;
            this.dynamicsMainRadGroupBox.HeaderImageKey = "";
            this.dynamicsMainRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dynamicsMainRadGroupBox.HeaderText = "Dynamics";
            this.dynamicsMainRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dynamicsMainRadGroupBox.Location = new System.Drawing.Point(12, 120);
            this.dynamicsMainRadGroupBox.Name = "dynamicsMainRadGroupBox";
            this.dynamicsMainRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dynamicsMainRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dynamicsMainRadGroupBox.Size = new System.Drawing.Size(387, 541);
            this.dynamicsMainRadGroupBox.TabIndex = 64;
            this.dynamicsMainRadGroupBox.Text = "Dynamics";
            this.dynamicsMainRadGroupBox.ThemeName = "Office2007Black";
            // 
            // getDynamicsRadButton
            // 
            this.getDynamicsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getDynamicsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getDynamicsRadButton.Location = new System.Drawing.Point(13, 503);
            this.getDynamicsRadButton.Name = "getDynamicsRadButton";
            this.getDynamicsRadButton.Size = new System.Drawing.Size(211, 30);
            this.getDynamicsRadButton.TabIndex = 15;
            this.getDynamicsRadButton.Text = "Get Current Dynamics";
            this.getDynamicsRadButton.ThemeName = "Office2007Black";
            this.getDynamicsRadButton.Click += new System.EventHandler(this.getDynamicsRadButton_Click);
            // 
            // mainDynamicsDataRadGridView
            // 
            this.mainDynamicsDataRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainDynamicsDataRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainDynamicsDataRadGridView.Location = new System.Drawing.Point(13, 23);
            this.mainDynamicsDataRadGridView.Name = "mainDynamicsDataRadGridView";
            this.mainDynamicsDataRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.mainDynamicsDataRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.mainDynamicsDataRadGridView.Size = new System.Drawing.Size(361, 469);
            this.mainDynamicsDataRadGridView.TabIndex = 16;
            this.mainDynamicsDataRadGridView.Text = "radGridView1";
            this.mainDynamicsDataRadGridView.ThemeName = "Office2007Black";
            // 
            // pdMonitorRadPageViewPage
            // 
            this.pdMonitorRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.pdMonitorRadPageViewPage.Controls.Add(this.findRecorsWithPhaseResolvedDataRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.calibrateChannelsRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.deleteDeviceDataPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.pdmSaveEventLogsToFileRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.pdmFirmwareVersionValueRadLabel);
            this.pdMonitorRadPageViewPage.Controls.Add(this.pdmFirmwareVersionTextRadLabel);
            this.pdMonitorRadPageViewPage.Controls.Add(this.pmdMeasurementsRadGroupBox);
            this.pdMonitorRadPageViewPage.Controls.Add(this.eventLogPdmTabRadListControl);
            this.pdMonitorRadPageViewPage.Controls.Add(this.radGroupBox1);
            this.pdMonitorRadPageViewPage.Controls.Add(this.deviceConfigurationPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.getAlertsPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.getDeviceErrorsPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.clearDisplayPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.readEventLogsPdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.setDeviceDatePdmTabRadButton);
            this.pdMonitorRadPageViewPage.Controls.Add(this.modbusAddressTextPdmTabRadLabel);
            this.pdMonitorRadPageViewPage.Controls.Add(this.modBusAddressPdmTabRadMaskedEditBox);
            this.pdMonitorRadPageViewPage.Controls.Add(this.pictureBox1);
            this.pdMonitorRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.pdMonitorRadPageViewPage.Name = "pdMonitorRadPageViewPage";
            this.pdMonitorRadPageViewPage.Size = new System.Drawing.Size(995, 664);
            this.pdMonitorRadPageViewPage.Text = "PDM";
            // 
            // calibrateChannelsRadButton
            // 
            this.calibrateChannelsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrateChannelsRadButton.Location = new System.Drawing.Point(206, 193);
            this.calibrateChannelsRadButton.Name = "calibrateChannelsRadButton";
            this.calibrateChannelsRadButton.Size = new System.Drawing.Size(125, 30);
            this.calibrateChannelsRadButton.TabIndex = 62;
            this.calibrateChannelsRadButton.Text = "<html>Calibrate Channels</html>";
            this.calibrateChannelsRadButton.ThemeName = "Office2007Black";
            this.calibrateChannelsRadButton.Click += new System.EventHandler(this.calibrateChannelsRadButton_Click);
            // 
            // deleteDeviceDataPdmTabRadButton
            // 
            this.deleteDeviceDataPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteDeviceDataPdmTabRadButton.Location = new System.Drawing.Point(354, 157);
            this.deleteDeviceDataPdmTabRadButton.Name = "deleteDeviceDataPdmTabRadButton";
            this.deleteDeviceDataPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deleteDeviceDataPdmTabRadButton.TabIndex = 69;
            this.deleteDeviceDataPdmTabRadButton.Text = "<html>Delete Device Data</html>";
            this.deleteDeviceDataPdmTabRadButton.ThemeName = "Office2007Black";
            this.deleteDeviceDataPdmTabRadButton.Click += new System.EventHandler(this.deleteDeviceDataPdmTabRadButton_Click);
            // 
            // pdmSaveEventLogsToFileRadButton
            // 
            this.pdmSaveEventLogsToFileRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdmSaveEventLogsToFileRadButton.Location = new System.Drawing.Point(354, 85);
            this.pdmSaveEventLogsToFileRadButton.Name = "pdmSaveEventLogsToFileRadButton";
            this.pdmSaveEventLogsToFileRadButton.Size = new System.Drawing.Size(125, 30);
            this.pdmSaveEventLogsToFileRadButton.TabIndex = 68;
            this.pdmSaveEventLogsToFileRadButton.Text = "<html>Save Event Logs<br>to File</html>";
            this.pdmSaveEventLogsToFileRadButton.ThemeName = "Office2007Black";
            this.pdmSaveEventLogsToFileRadButton.Click += new System.EventHandler(this.pdmSaveEventLogsToFileRadButton_Click);
            // 
            // pdmFirmwareVersionValueRadLabel
            // 
            this.pdmFirmwareVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdmFirmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pdmFirmwareVersionValueRadLabel.Location = new System.Drawing.Point(277, 21);
            this.pdmFirmwareVersionValueRadLabel.Name = "pdmFirmwareVersionValueRadLabel";
            this.pdmFirmwareVersionValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.pdmFirmwareVersionValueRadLabel.TabIndex = 67;
            this.pdmFirmwareVersionValueRadLabel.Text = "0.00";
            this.pdmFirmwareVersionValueRadLabel.ThemeName = "Office2007Black";
            // 
            // pdmFirmwareVersionTextRadLabel
            // 
            this.pdmFirmwareVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdmFirmwareVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pdmFirmwareVersionTextRadLabel.Location = new System.Drawing.Point(179, 21);
            this.pdmFirmwareVersionTextRadLabel.Name = "pdmFirmwareVersionTextRadLabel";
            this.pdmFirmwareVersionTextRadLabel.Size = new System.Drawing.Size(92, 15);
            this.pdmFirmwareVersionTextRadLabel.TabIndex = 66;
            this.pdmFirmwareVersionTextRadLabel.Text = "<html>Firmware version</html>";
            this.pdmFirmwareVersionTextRadLabel.ThemeName = "Office2007Black";
            // 
            // pmdMeasurementsRadGroupBox
            // 
            this.pmdMeasurementsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.pmdMeasurementsRadGroupBox.Controls.Add(this.cycleOfAcqSpinEditor);
            this.pmdMeasurementsRadGroupBox.Controls.Add(this.radLabel1);
            this.pmdMeasurementsRadGroupBox.Controls.Add(this.setModeRadButton);
            this.pmdMeasurementsRadGroupBox.Controls.Add(this.trendZoomButtonsRadPanel);
            this.pmdMeasurementsRadGroupBox.Controls.Add(this.startMeasurementPdmTabRadButton);
            this.pmdMeasurementsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pmdMeasurementsRadGroupBox.FooterImageIndex = -1;
            this.pmdMeasurementsRadGroupBox.FooterImageKey = "";
            this.pmdMeasurementsRadGroupBox.HeaderImageIndex = -1;
            this.pmdMeasurementsRadGroupBox.HeaderImageKey = "";
            this.pmdMeasurementsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.pmdMeasurementsRadGroupBox.HeaderText = "Measurements";
            this.pmdMeasurementsRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.pmdMeasurementsRadGroupBox.Location = new System.Drawing.Point(12, 49);
            this.pmdMeasurementsRadGroupBox.Name = "pmdMeasurementsRadGroupBox";
            this.pmdMeasurementsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.pmdMeasurementsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.pmdMeasurementsRadGroupBox.Size = new System.Drawing.Size(177, 174);
            this.pmdMeasurementsRadGroupBox.TabIndex = 65;
            this.pmdMeasurementsRadGroupBox.Text = "Measurements";
            this.pmdMeasurementsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // cycleOfAcqSpinEditor
            // 
            this.cycleOfAcqSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cycleOfAcqSpinEditor.Location = new System.Drawing.Point(119, 28);
            this.cycleOfAcqSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.cycleOfAcqSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cycleOfAcqSpinEditor.Name = "cycleOfAcqSpinEditor";
            // 
            // 
            // 
            this.cycleOfAcqSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.cycleOfAcqSpinEditor.ShowBorder = true;
            this.cycleOfAcqSpinEditor.Size = new System.Drawing.Size(48, 19);
            this.cycleOfAcqSpinEditor.TabIndex = 70;
            this.cycleOfAcqSpinEditor.TabStop = false;
            this.cycleOfAcqSpinEditor.ThemeName = "Office2007Black";
            this.cycleOfAcqSpinEditor.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Location = new System.Drawing.Point(3, 30);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(117, 16);
            this.radLabel1.TabIndex = 67;
            this.radLabel1.Text = "Cycles of Acquistions:";
            this.radLabel1.ThemeName = "Office2007Black";
            // 
            // setModeRadButton
            // 
            this.setModeRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setModeRadButton.Location = new System.Drawing.Point(115, 65);
            this.setModeRadButton.Name = "setModeRadButton";
            this.setModeRadButton.Size = new System.Drawing.Size(45, 40);
            this.setModeRadButton.TabIndex = 59;
            this.setModeRadButton.Text = "Set";
            this.setModeRadButton.ThemeName = "Office2007Black";
            this.setModeRadButton.Click += new System.EventHandler(this.setModeRadButton_Click);
            // 
            // trendZoomButtonsRadPanel
            // 
            this.trendZoomButtonsRadPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.trendZoomButtonsRadPanel.Controls.Add(this.normalModeRadioButton);
            this.trendZoomButtonsRadPanel.Controls.Add(this.testModeRadioButton);
            this.trendZoomButtonsRadPanel.Location = new System.Drawing.Point(13, 57);
            this.trendZoomButtonsRadPanel.Name = "trendZoomButtonsRadPanel";
            this.trendZoomButtonsRadPanel.Size = new System.Drawing.Size(96, 55);
            this.trendZoomButtonsRadPanel.TabIndex = 77;
            this.trendZoomButtonsRadPanel.ThemeName = "ControlDefault";
            // 
            // normalModeRadioButton
            // 
            this.normalModeRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.normalModeRadioButton.Checked = true;
            this.normalModeRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.normalModeRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalModeRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalModeRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.normalModeRadioButton.Location = new System.Drawing.Point(3, 1);
            this.normalModeRadioButton.Name = "normalModeRadioButton";
            this.normalModeRadioButton.Size = new System.Drawing.Size(90, 25);
            this.normalModeRadioButton.TabIndex = 9;
            this.normalModeRadioButton.TabStop = true;
            this.normalModeRadioButton.Text = "Normal Mode";
            this.normalModeRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.normalModeRadioButton.CheckedChanged += new System.EventHandler(this.normalModeRadioButton_CheckedChanged);
            // 
            // testModeRadioButton
            // 
            this.testModeRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.testModeRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.testModeRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testModeRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.testModeRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.testModeRadioButton.Location = new System.Drawing.Point(3, 27);
            this.testModeRadioButton.Name = "testModeRadioButton";
            this.testModeRadioButton.Size = new System.Drawing.Size(90, 25);
            this.testModeRadioButton.TabIndex = 10;
            this.testModeRadioButton.Text = "Test Mode";
            this.testModeRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.testModeRadioButton.CheckedChanged += new System.EventHandler(this.testModeRadioButton_CheckedChanged);
            // 
            // startMeasurementPdmTabRadButton
            // 
            this.startMeasurementPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startMeasurementPdmTabRadButton.Location = new System.Drawing.Point(13, 131);
            this.startMeasurementPdmTabRadButton.Name = "startMeasurementPdmTabRadButton";
            this.startMeasurementPdmTabRadButton.Size = new System.Drawing.Size(141, 30);
            this.startMeasurementPdmTabRadButton.TabIndex = 58;
            this.startMeasurementPdmTabRadButton.Text = "<html>Start Measurement</html>";
            this.startMeasurementPdmTabRadButton.ThemeName = "Office2007Black";
            this.startMeasurementPdmTabRadButton.Click += new System.EventHandler(this.startMeasurementPdmTabRadButton_Click);
            // 
            // eventLogPdmTabRadListControl
            // 
            this.eventLogPdmTabRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLogPdmTabRadListControl.AutoScroll = true;
            this.eventLogPdmTabRadListControl.CaseSensitiveSort = true;
            this.eventLogPdmTabRadListControl.ItemHeight = 18;
            this.eventLogPdmTabRadListControl.Location = new System.Drawing.Point(553, 3);
            this.eventLogPdmTabRadListControl.Name = "eventLogPdmTabRadListControl";
            this.eventLogPdmTabRadListControl.Size = new System.Drawing.Size(439, 550);
            this.eventLogPdmTabRadListControl.TabIndex = 64;
            this.eventLogPdmTabRadListControl.Text = "radListControl2";
            this.eventLogPdmTabRadListControl.ThemeName = "ControlDefault";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGroupBox1.Controls.Add(this.pdmChannelDataRadGridView);
            this.radGroupBox1.Controls.Add(this.pdmDataDateRadLabel);
            this.radGroupBox1.Controls.Add(this.viewPhaseResolvedDataPdmTabRadButton);
            this.radGroupBox1.Controls.Add(this.getLatestReadingsPdmTabRadButton);
            this.radGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox1.FooterImageIndex = -1;
            this.radGroupBox1.FooterImageKey = "";
            this.radGroupBox1.HeaderImageIndex = -1;
            this.radGroupBox1.HeaderImageKey = "";
            this.radGroupBox1.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox1.HeaderText = "Latest Readings";
            this.radGroupBox1.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGroupBox1.Location = new System.Drawing.Point(12, 234);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(535, 427);
            this.radGroupBox1.TabIndex = 63;
            this.radGroupBox1.Text = "Latest Readings";
            this.radGroupBox1.ThemeName = "Office2007Black";
            // 
            // pdmChannelDataRadGridView
            // 
            this.pdmChannelDataRadGridView.Location = new System.Drawing.Point(16, 56);
            this.pdmChannelDataRadGridView.Name = "pdmChannelDataRadGridView";
            this.pdmChannelDataRadGridView.Size = new System.Drawing.Size(506, 322);
            this.pdmChannelDataRadGridView.TabIndex = 55;
            this.pdmChannelDataRadGridView.Text = "radGridView1";
            // 
            // pdmDataDateRadLabel
            // 
            this.pdmDataDateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdmDataDateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pdmDataDateRadLabel.Location = new System.Drawing.Point(16, 33);
            this.pdmDataDateRadLabel.Name = "pdmDataDateRadLabel";
            this.pdmDataDateRadLabel.Size = new System.Drawing.Size(60, 16);
            this.pdmDataDateRadLabel.TabIndex = 54;
            this.pdmDataDateRadLabel.Text = "Data Date:";
            this.pdmDataDateRadLabel.ThemeName = "Office2007Black";
            // 
            // viewPhaseResolvedDataPdmTabRadButton
            // 
            this.viewPhaseResolvedDataPdmTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.viewPhaseResolvedDataPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewPhaseResolvedDataPdmTabRadButton.Location = new System.Drawing.Point(247, 384);
            this.viewPhaseResolvedDataPdmTabRadButton.Name = "viewPhaseResolvedDataPdmTabRadButton";
            this.viewPhaseResolvedDataPdmTabRadButton.Size = new System.Drawing.Size(205, 30);
            this.viewPhaseResolvedDataPdmTabRadButton.TabIndex = 17;
            this.viewPhaseResolvedDataPdmTabRadButton.Text = "<html>View Phase Resolved Data</html>";
            this.viewPhaseResolvedDataPdmTabRadButton.ThemeName = "Office2007Black";
            this.viewPhaseResolvedDataPdmTabRadButton.Click += new System.EventHandler(this.viewPhaseResolvedDataPdmTabRadButton_Click);
            // 
            // getLatestReadingsPdmTabRadButton
            // 
            this.getLatestReadingsPdmTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getLatestReadingsPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getLatestReadingsPdmTabRadButton.Location = new System.Drawing.Point(13, 384);
            this.getLatestReadingsPdmTabRadButton.Name = "getLatestReadingsPdmTabRadButton";
            this.getLatestReadingsPdmTabRadButton.Size = new System.Drawing.Size(205, 30);
            this.getLatestReadingsPdmTabRadButton.TabIndex = 15;
            this.getLatestReadingsPdmTabRadButton.Text = "<html>Get Latest Readings</html>";
            this.getLatestReadingsPdmTabRadButton.ThemeName = "Office2007Black";
            this.getLatestReadingsPdmTabRadButton.Click += new System.EventHandler(this.getLatestReadingsPdmTabRadButton_Click);
            // 
            // deviceConfigurationPdmTabRadButton
            // 
            this.deviceConfigurationPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceConfigurationPdmTabRadButton.Location = new System.Drawing.Point(206, 85);
            this.deviceConfigurationPdmTabRadButton.Name = "deviceConfigurationPdmTabRadButton";
            this.deviceConfigurationPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deviceConfigurationPdmTabRadButton.TabIndex = 62;
            this.deviceConfigurationPdmTabRadButton.Text = "<html>Device Configuration</html>";
            this.deviceConfigurationPdmTabRadButton.ThemeName = "Office2007Black";
            this.deviceConfigurationPdmTabRadButton.Click += new System.EventHandler(this.deviceConfigurationPdmTabRadButton_Click);
            // 
            // getAlertsPdmTabRadButton
            // 
            this.getAlertsPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getAlertsPdmTabRadButton.Location = new System.Drawing.Point(206, 157);
            this.getAlertsPdmTabRadButton.Name = "getAlertsPdmTabRadButton";
            this.getAlertsPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.getAlertsPdmTabRadButton.TabIndex = 61;
            this.getAlertsPdmTabRadButton.Text = "<html>Alerts</html>";
            this.getAlertsPdmTabRadButton.ThemeName = "Office2007Black";
            this.getAlertsPdmTabRadButton.Click += new System.EventHandler(this.getAlertsPdmTabRadButton_Click);
            // 
            // getDeviceErrorsPdmTabRadButton
            // 
            this.getDeviceErrorsPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getDeviceErrorsPdmTabRadButton.Location = new System.Drawing.Point(206, 121);
            this.getDeviceErrorsPdmTabRadButton.Name = "getDeviceErrorsPdmTabRadButton";
            this.getDeviceErrorsPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.getDeviceErrorsPdmTabRadButton.TabIndex = 60;
            this.getDeviceErrorsPdmTabRadButton.Text = "<html>Errors</html>";
            this.getDeviceErrorsPdmTabRadButton.ThemeName = "Office2007Black";
            this.getDeviceErrorsPdmTabRadButton.Click += new System.EventHandler(this.getDeviceErrorsPdmTabRadButton_Click);
            // 
            // clearDisplayPdmTabRadButton
            // 
            this.clearDisplayPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearDisplayPdmTabRadButton.Location = new System.Drawing.Point(354, 121);
            this.clearDisplayPdmTabRadButton.Name = "clearDisplayPdmTabRadButton";
            this.clearDisplayPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.clearDisplayPdmTabRadButton.TabIndex = 59;
            this.clearDisplayPdmTabRadButton.Text = "<html>Clear Data from Display</html>";
            this.clearDisplayPdmTabRadButton.ThemeName = "Office2007Black";
            this.clearDisplayPdmTabRadButton.Click += new System.EventHandler(this.clearDisplayPdmTabRadButton_Click);
            // 
            // readEventLogsPdmTabRadButton
            // 
            this.readEventLogsPdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readEventLogsPdmTabRadButton.Location = new System.Drawing.Point(354, 49);
            this.readEventLogsPdmTabRadButton.Name = "readEventLogsPdmTabRadButton";
            this.readEventLogsPdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.readEventLogsPdmTabRadButton.TabIndex = 57;
            this.readEventLogsPdmTabRadButton.Text = "<html>Read Event Logs</html>";
            this.readEventLogsPdmTabRadButton.ThemeName = "Office2007Black";
            this.readEventLogsPdmTabRadButton.Click += new System.EventHandler(this.readEventLogsPdmTabRadButton_Click);
            // 
            // setDeviceDatePdmTabRadButton
            // 
            this.setDeviceDatePdmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setDeviceDatePdmTabRadButton.Location = new System.Drawing.Point(206, 49);
            this.setDeviceDatePdmTabRadButton.Name = "setDeviceDatePdmTabRadButton";
            this.setDeviceDatePdmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.setDeviceDatePdmTabRadButton.TabIndex = 56;
            this.setDeviceDatePdmTabRadButton.Text = "<html>Set Device Date</html>";
            this.setDeviceDatePdmTabRadButton.ThemeName = "Office2007Black";
            this.setDeviceDatePdmTabRadButton.Click += new System.EventHandler(this.setDeviceDatePdmTabRadButton_Click);
            // 
            // modbusAddressTextPdmTabRadLabel
            // 
            this.modbusAddressTextPdmTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressTextPdmTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressTextPdmTabRadLabel.Location = new System.Drawing.Point(12, 21);
            this.modbusAddressTextPdmTabRadLabel.Name = "modbusAddressTextPdmTabRadLabel";
            this.modbusAddressTextPdmTabRadLabel.Size = new System.Drawing.Size(91, 15);
            this.modbusAddressTextPdmTabRadLabel.TabIndex = 53;
            this.modbusAddressTextPdmTabRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressTextPdmTabRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressPdmTabRadMaskedEditBox
            // 
            this.modBusAddressPdmTabRadMaskedEditBox.AutoSize = true;
            this.modBusAddressPdmTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressPdmTabRadMaskedEditBox.Location = new System.Drawing.Point(107, 21);
            this.modBusAddressPdmTabRadMaskedEditBox.Name = "modBusAddressPdmTabRadMaskedEditBox";
            this.modBusAddressPdmTabRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressPdmTabRadMaskedEditBox.TabIndex = 52;
            this.modBusAddressPdmTabRadMaskedEditBox.TabStop = false;
            this.modBusAddressPdmTabRadMaskedEditBox.Text = "0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(719, 559);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(273, 102);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // utilitiesRadPageView
            // 
            this.utilitiesRadPageView.Controls.Add(this.mainMonitorRadPageViewPage);
            this.utilitiesRadPageView.Controls.Add(this.bushingMonitorRadPageViewPage);
            this.utilitiesRadPageView.Controls.Add(this.pdMonitorRadPageViewPage);
            this.utilitiesRadPageView.Controls.Add(this.readRegistersRadPageViewPage);
            this.utilitiesRadPageView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.utilitiesRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.utilitiesRadPageView.Location = new System.Drawing.Point(0, 26);
            this.utilitiesRadPageView.Name = "utilitiesRadPageView";
            this.utilitiesRadPageView.SelectedPage = this.pdMonitorRadPageViewPage;
            this.utilitiesRadPageView.Size = new System.Drawing.Size(1016, 710);
            this.utilitiesRadPageView.TabIndex = 1;
            this.utilitiesRadPageView.Text = "radPageView1";
            this.utilitiesRadPageView.ThemeName = "Office2007Black";
            // 
            // bushingMonitorRadPageViewPage
            // 
            this.bushingMonitorRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingMonitorRadPageViewPage.Controls.Add(this.radButtonBushingCalc);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.getDeviceHealthBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.deleteDeviceDataBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.bhmSaveEventLogsToFileRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.bhmFirmwareVersionValueRadLabel);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.bhmFirmwareVersionTextRadLabel);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.setDeviceDateBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.deviceConfigurationBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.getEquipmentErrorBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.clearListBoxBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.programLogoPictureBox);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.latestReadingsBhmTabRadGroupBox);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.modbusAddressTextBhmTabRadLabel);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.modBusAddressBhmTabRadMaskedEditBox);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.setUpAutobalanceBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.startMeasurementBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.balanceBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.readEventLogsBhmTabRadButton);
            this.bushingMonitorRadPageViewPage.Controls.Add(this.eventLogBhmTabRadListControl);
            this.bushingMonitorRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.bushingMonitorRadPageViewPage.Name = "bushingMonitorRadPageViewPage";
            this.bushingMonitorRadPageViewPage.Size = new System.Drawing.Size(995, 664);
            this.bushingMonitorRadPageViewPage.Text = "BHM";
            // 
            // radButtonBushingCalc
            // 
            this.radButtonBushingCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonBushingCalc.Location = new System.Drawing.Point(291, 81);
            this.radButtonBushingCalc.Name = "radButtonBushingCalc";
            this.radButtonBushingCalc.Size = new System.Drawing.Size(125, 30);
            this.radButtonBushingCalc.TabIndex = 60;
            this.radButtonBushingCalc.Text = "Bushing Calculator";
            this.radButtonBushingCalc.ThemeName = "Office2007Black";
            this.radButtonBushingCalc.Click += new System.EventHandler(this.radButtonBushingCalc_Click);
            // 
            // getDeviceHealthBhmTabRadButton
            // 
            this.getDeviceHealthBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getDeviceHealthBhmTabRadButton.Location = new System.Drawing.Point(160, 117);
            this.getDeviceHealthBhmTabRadButton.Name = "getDeviceHealthBhmTabRadButton";
            this.getDeviceHealthBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.getDeviceHealthBhmTabRadButton.TabIndex = 60;
            this.getDeviceHealthBhmTabRadButton.Text = "<html>Errors</html>";
            this.getDeviceHealthBhmTabRadButton.ThemeName = "Office2007Black";
            this.getDeviceHealthBhmTabRadButton.Click += new System.EventHandler(this.getDeviceHealthBhmTabRadButton_Click_1);
            // 
            // deleteDeviceDataBhmTabRadButton
            // 
            this.deleteDeviceDataBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteDeviceDataBhmTabRadButton.Location = new System.Drawing.Point(291, 45);
            this.deleteDeviceDataBhmTabRadButton.Name = "deleteDeviceDataBhmTabRadButton";
            this.deleteDeviceDataBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deleteDeviceDataBhmTabRadButton.TabIndex = 59;
            this.deleteDeviceDataBhmTabRadButton.Text = "<html>Delete Device Data</html>";
            this.deleteDeviceDataBhmTabRadButton.ThemeName = "Office2007Black";
            this.deleteDeviceDataBhmTabRadButton.Click += new System.EventHandler(this.deleteDeviceDataBhmTabRadButton_Click);
            // 
            // bhmSaveEventLogsToFileRadButton
            // 
            this.bhmSaveEventLogsToFileRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bhmSaveEventLogsToFileRadButton.Location = new System.Drawing.Point(160, 81);
            this.bhmSaveEventLogsToFileRadButton.Name = "bhmSaveEventLogsToFileRadButton";
            this.bhmSaveEventLogsToFileRadButton.Size = new System.Drawing.Size(125, 30);
            this.bhmSaveEventLogsToFileRadButton.TabIndex = 58;
            this.bhmSaveEventLogsToFileRadButton.Text = "<html>Save Event Logs<br>to File</html>";
            this.bhmSaveEventLogsToFileRadButton.ThemeName = "Office2007Black";
            this.bhmSaveEventLogsToFileRadButton.Click += new System.EventHandler(this.bhmSaveEventLogsToFileRadButton_Click);
            // 
            // bhmFirmwareVersionValueRadLabel
            // 
            this.bhmFirmwareVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bhmFirmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bhmFirmwareVersionValueRadLabel.Location = new System.Drawing.Point(274, 21);
            this.bhmFirmwareVersionValueRadLabel.Name = "bhmFirmwareVersionValueRadLabel";
            this.bhmFirmwareVersionValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.bhmFirmwareVersionValueRadLabel.TabIndex = 57;
            this.bhmFirmwareVersionValueRadLabel.Text = "0.00";
            this.bhmFirmwareVersionValueRadLabel.ThemeName = "Office2007Black";
            // 
            // bhmFirmwareVersionTextRadLabel
            // 
            this.bhmFirmwareVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bhmFirmwareVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bhmFirmwareVersionTextRadLabel.Location = new System.Drawing.Point(176, 21);
            this.bhmFirmwareVersionTextRadLabel.Name = "bhmFirmwareVersionTextRadLabel";
            this.bhmFirmwareVersionTextRadLabel.Size = new System.Drawing.Size(92, 15);
            this.bhmFirmwareVersionTextRadLabel.TabIndex = 56;
            this.bhmFirmwareVersionTextRadLabel.Text = "<html>Firmware version</html>";
            this.bhmFirmwareVersionTextRadLabel.ThemeName = "Office2007Black";
            // 
            // setDeviceDateBhmTabRadButton
            // 
            this.setDeviceDateBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setDeviceDateBhmTabRadButton.Location = new System.Drawing.Point(29, 189);
            this.setDeviceDateBhmTabRadButton.Name = "setDeviceDateBhmTabRadButton";
            this.setDeviceDateBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.setDeviceDateBhmTabRadButton.TabIndex = 55;
            this.setDeviceDateBhmTabRadButton.Text = "<html>Set Device Date</html>";
            this.setDeviceDateBhmTabRadButton.ThemeName = "Office2007Black";
            this.setDeviceDateBhmTabRadButton.Click += new System.EventHandler(this.setDeviceDateBhmTabRadButton_Click);
            // 
            // deviceConfigurationBhmTabRadButton
            // 
            this.deviceConfigurationBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceConfigurationBhmTabRadButton.Location = new System.Drawing.Point(29, 153);
            this.deviceConfigurationBhmTabRadButton.Name = "deviceConfigurationBhmTabRadButton";
            this.deviceConfigurationBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.deviceConfigurationBhmTabRadButton.TabIndex = 54;
            this.deviceConfigurationBhmTabRadButton.Text = "<html>Device Configuration</html>";
            this.deviceConfigurationBhmTabRadButton.ThemeName = "Office2007Black";
            this.deviceConfigurationBhmTabRadButton.Click += new System.EventHandler(this.deviceConfigurationBhmTabRadButton_Click);
            // 
            // getEquipmentErrorBhmTabRadButton
            // 
            this.getEquipmentErrorBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getEquipmentErrorBhmTabRadButton.Location = new System.Drawing.Point(160, 153);
            this.getEquipmentErrorBhmTabRadButton.Name = "getEquipmentErrorBhmTabRadButton";
            this.getEquipmentErrorBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.getEquipmentErrorBhmTabRadButton.TabIndex = 53;
            this.getEquipmentErrorBhmTabRadButton.Text = "<html>Alerts</html>";
            this.getEquipmentErrorBhmTabRadButton.ThemeName = "Office2007Black";
            this.getEquipmentErrorBhmTabRadButton.Click += new System.EventHandler(this.getEquipmentErrorBhmTabRadButton_Click);
            // 
            // clearListBoxBhmTabRadButton
            // 
            this.clearListBoxBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearListBoxBhmTabRadButton.Location = new System.Drawing.Point(160, 189);
            this.clearListBoxBhmTabRadButton.Name = "clearListBoxBhmTabRadButton";
            this.clearListBoxBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.clearListBoxBhmTabRadButton.TabIndex = 51;
            this.clearListBoxBhmTabRadButton.Text = "<html>Clear Data from Display</html>";
            this.clearListBoxBhmTabRadButton.ThemeName = "Office2007Black";
            this.clearListBoxBhmTabRadButton.Click += new System.EventHandler(this.clearListBoxBhmTabRadButton_Click);
            // 
            // programLogoPictureBox
            // 
            this.programLogoPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.programLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("programLogoPictureBox.Image")));
            this.programLogoPictureBox.Location = new System.Drawing.Point(719, 559);
            this.programLogoPictureBox.Name = "programLogoPictureBox";
            this.programLogoPictureBox.Size = new System.Drawing.Size(273, 102);
            this.programLogoPictureBox.TabIndex = 50;
            this.programLogoPictureBox.TabStop = false;
            // 
            // latestReadingsBhmTabRadGroupBox
            // 
            this.latestReadingsBhmTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.latestReadingsBhmTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.latestReadingsBhmTabRadGroupBox.Controls.Add(this.bhmInitialDataRadGridView);
            this.latestReadingsBhmTabRadGroupBox.Controls.Add(this.bhmLatestDataReadingsRadGridView);
            this.latestReadingsBhmTabRadGroupBox.Controls.Add(this.initialDataReadingsRadLabel);
            this.latestReadingsBhmTabRadGroupBox.Controls.Add(this.latestDataReadingsRadLabel);
            this.latestReadingsBhmTabRadGroupBox.Controls.Add(this.getLatestReadingsBhmTabRadButton);
            this.latestReadingsBhmTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latestReadingsBhmTabRadGroupBox.FooterImageIndex = -1;
            this.latestReadingsBhmTabRadGroupBox.FooterImageKey = "";
            this.latestReadingsBhmTabRadGroupBox.HeaderImageIndex = -1;
            this.latestReadingsBhmTabRadGroupBox.HeaderImageKey = "";
            this.latestReadingsBhmTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.latestReadingsBhmTabRadGroupBox.HeaderText = "Data Readings";
            this.latestReadingsBhmTabRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.latestReadingsBhmTabRadGroupBox.Location = new System.Drawing.Point(12, 225);
            this.latestReadingsBhmTabRadGroupBox.Name = "latestReadingsBhmTabRadGroupBox";
            this.latestReadingsBhmTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.latestReadingsBhmTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.latestReadingsBhmTabRadGroupBox.Size = new System.Drawing.Size(565, 424);
            this.latestReadingsBhmTabRadGroupBox.TabIndex = 49;
            this.latestReadingsBhmTabRadGroupBox.Text = "Data Readings";
            this.latestReadingsBhmTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // bhmInitialDataRadGridView
            // 
            this.bhmInitialDataRadGridView.Location = new System.Drawing.Point(17, 230);
            this.bhmInitialDataRadGridView.Name = "bhmInitialDataRadGridView";
            this.bhmInitialDataRadGridView.Size = new System.Drawing.Size(535, 133);
            this.bhmInitialDataRadGridView.TabIndex = 38;
            this.bhmInitialDataRadGridView.Text = "radGridView1";
            // 
            // bhmLatestDataReadingsRadGridView
            // 
            this.bhmLatestDataReadingsRadGridView.Location = new System.Drawing.Point(17, 52);
            this.bhmLatestDataReadingsRadGridView.Name = "bhmLatestDataReadingsRadGridView";
            this.bhmLatestDataReadingsRadGridView.Size = new System.Drawing.Size(535, 136);
            this.bhmLatestDataReadingsRadGridView.TabIndex = 37;
            this.bhmLatestDataReadingsRadGridView.Text = "radGridView1";
            // 
            // initialDataReadingsRadLabel
            // 
            this.initialDataReadingsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialDataReadingsRadLabel.Location = new System.Drawing.Point(17, 208);
            this.initialDataReadingsRadLabel.Name = "initialDataReadingsRadLabel";
            this.initialDataReadingsRadLabel.Size = new System.Drawing.Size(60, 16);
            this.initialDataReadingsRadLabel.TabIndex = 36;
            this.initialDataReadingsRadLabel.Text = "Initial Data";
            // 
            // latestDataReadingsRadLabel
            // 
            this.latestDataReadingsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.latestDataReadingsRadLabel.Location = new System.Drawing.Point(17, 29);
            this.latestDataReadingsRadLabel.Name = "latestDataReadingsRadLabel";
            this.latestDataReadingsRadLabel.Size = new System.Drawing.Size(115, 16);
            this.latestDataReadingsRadLabel.TabIndex = 35;
            this.latestDataReadingsRadLabel.Text = "Latest Data Readings";
            // 
            // getLatestReadingsBhmTabRadButton
            // 
            this.getLatestReadingsBhmTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getLatestReadingsBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getLatestReadingsBhmTabRadButton.Location = new System.Drawing.Point(17, 381);
            this.getLatestReadingsBhmTabRadButton.Name = "getLatestReadingsBhmTabRadButton";
            this.getLatestReadingsBhmTabRadButton.Size = new System.Drawing.Size(264, 30);
            this.getLatestReadingsBhmTabRadButton.TabIndex = 15;
            this.getLatestReadingsBhmTabRadButton.Text = "<html>Get Latest Readings</html>";
            this.getLatestReadingsBhmTabRadButton.ThemeName = "Office2007Black";
            this.getLatestReadingsBhmTabRadButton.Click += new System.EventHandler(this.getLatestReadingsBhmTabRadButton_Click);
            // 
            // modbusAddressTextBhmTabRadLabel
            // 
            this.modbusAddressTextBhmTabRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressTextBhmTabRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressTextBhmTabRadLabel.Location = new System.Drawing.Point(12, 21);
            this.modbusAddressTextBhmTabRadLabel.Name = "modbusAddressTextBhmTabRadLabel";
            this.modbusAddressTextBhmTabRadLabel.Size = new System.Drawing.Size(91, 15);
            this.modbusAddressTextBhmTabRadLabel.TabIndex = 48;
            this.modbusAddressTextBhmTabRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressTextBhmTabRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressBhmTabRadMaskedEditBox
            // 
            this.modBusAddressBhmTabRadMaskedEditBox.AutoSize = true;
            this.modBusAddressBhmTabRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressBhmTabRadMaskedEditBox.Location = new System.Drawing.Point(107, 21);
            this.modBusAddressBhmTabRadMaskedEditBox.Name = "modBusAddressBhmTabRadMaskedEditBox";
            this.modBusAddressBhmTabRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressBhmTabRadMaskedEditBox.TabIndex = 47;
            this.modBusAddressBhmTabRadMaskedEditBox.TabStop = false;
            this.modBusAddressBhmTabRadMaskedEditBox.Text = "0";
            // 
            // setUpAutobalanceBhmTabRadButton
            // 
            this.setUpAutobalanceBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setUpAutobalanceBhmTabRadButton.Location = new System.Drawing.Point(29, 117);
            this.setUpAutobalanceBhmTabRadButton.Name = "setUpAutobalanceBhmTabRadButton";
            this.setUpAutobalanceBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.setUpAutobalanceBhmTabRadButton.TabIndex = 46;
            this.setUpAutobalanceBhmTabRadButton.Text = "<html>Set up Auto Balance</html>";
            this.setUpAutobalanceBhmTabRadButton.ThemeName = "Office2007Black";
            this.setUpAutobalanceBhmTabRadButton.Click += new System.EventHandler(this.setUpAutobalanceBhmTabRadButton_Click);
            // 
            // startMeasurementBhmTabRadButton
            // 
            this.startMeasurementBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startMeasurementBhmTabRadButton.Location = new System.Drawing.Point(29, 45);
            this.startMeasurementBhmTabRadButton.Name = "startMeasurementBhmTabRadButton";
            this.startMeasurementBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.startMeasurementBhmTabRadButton.TabIndex = 45;
            this.startMeasurementBhmTabRadButton.Text = "<html>Start Measurement</html>";
            this.startMeasurementBhmTabRadButton.ThemeName = "Office2007Black";
            this.startMeasurementBhmTabRadButton.Click += new System.EventHandler(this.startMeasurementBhmTabRadButton_Click);
            // 
            // balanceBhmTabRadButton
            // 
            this.balanceBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.balanceBhmTabRadButton.Location = new System.Drawing.Point(29, 81);
            this.balanceBhmTabRadButton.Name = "balanceBhmTabRadButton";
            this.balanceBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.balanceBhmTabRadButton.TabIndex = 44;
            this.balanceBhmTabRadButton.Text = "<html>Balance</html>";
            this.balanceBhmTabRadButton.ThemeName = "Office2007Black";
            this.balanceBhmTabRadButton.Click += new System.EventHandler(this.balanceBhmTabRadButton_Click);
            // 
            // readEventLogsBhmTabRadButton
            // 
            this.readEventLogsBhmTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readEventLogsBhmTabRadButton.Location = new System.Drawing.Point(160, 45);
            this.readEventLogsBhmTabRadButton.Name = "readEventLogsBhmTabRadButton";
            this.readEventLogsBhmTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.readEventLogsBhmTabRadButton.TabIndex = 43;
            this.readEventLogsBhmTabRadButton.Text = "<html>Read Event Logs</html>";
            this.readEventLogsBhmTabRadButton.ThemeName = "Office2007Black";
            this.readEventLogsBhmTabRadButton.Click += new System.EventHandler(this.readEventLogsBhmTabRadButton_Click);
            // 
            // eventLogBhmTabRadListControl
            // 
            this.eventLogBhmTabRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLogBhmTabRadListControl.AutoScroll = true;
            this.eventLogBhmTabRadListControl.CaseSensitiveSort = true;
            this.eventLogBhmTabRadListControl.ItemHeight = 18;
            this.eventLogBhmTabRadListControl.Location = new System.Drawing.Point(594, 11);
            this.eventLogBhmTabRadListControl.Name = "eventLogBhmTabRadListControl";
            this.eventLogBhmTabRadListControl.Size = new System.Drawing.Size(392, 542);
            this.eventLogBhmTabRadListControl.TabIndex = 42;
            this.eventLogBhmTabRadListControl.Text = "radListControl2";
            this.eventLogBhmTabRadListControl.ThemeName = "ControlDefault";
            // 
            // mainDisplayRadMenu
            // 
            this.mainDisplayRadMenu.Location = new System.Drawing.Point(0, 0);
            this.mainDisplayRadMenu.Name = "mainDisplayRadMenu";
            this.mainDisplayRadMenu.Size = new System.Drawing.Size(1016, 28);
            this.mainDisplayRadMenu.TabIndex = 2;
            this.mainDisplayRadMenu.Text = "radMenu1";
            this.mainDisplayRadMenu.ThemeName = "Office2007Black";
            // 
            // MainDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 736);
            this.Controls.Add(this.mainDisplayRadMenu);
            this.Controls.Add(this.utilitiesRadPageView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainDisplay";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DM Series Utilities";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.MainDisplay_Load);
            this.readRegistersRadPageViewPage.ResumeLayout(false);
            this.readRegistersRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchProgressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRegistersToReadTextReadRegistersTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRegistersReadRegistersTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startingRegisterTextReadRegistersTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startingRegisterReadRegistersTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readRegistersReadRegistersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValuesRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerValuesRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextReadRegistersTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressReadRegistersTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.findRecorsWithPhaseResolvedDataRadButton)).EndInit();
            this.mainMonitorRadPageViewPage.ResumeLayout(false);
            this.mainMonitorRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windingHotSpotConfigurationRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openDnpSettingsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateAnalogSignalsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataMainTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationMainTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFirmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFirmwareVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogMainTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDateMainTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextMainTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressMainTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsMainRadGroupBox)).EndInit();
            this.dynamicsMainRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.getDynamicsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDynamicsDataRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDynamicsDataRadGridView)).EndInit();
            this.pdMonitorRadPageViewPage.ResumeLayout(false);
            this.pdMonitorRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calibrateChannelsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmSaveEventLogsToFileRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmFirmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmFirmwareVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmdMeasurementsRadGroupBox)).EndInit();
            this.pmdMeasurementsRadGroupBox.ResumeLayout(false);
            this.pmdMeasurementsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cycleOfAcqSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setModeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).EndInit();
            this.trendZoomButtonsRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.startMeasurementPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogPdmTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pdmChannelDataRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmChannelDataRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdmDataDateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewPhaseResolvedDataPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLatestReadingsPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getAlertsPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDeviceErrorsPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearDisplayPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readEventLogsPdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDatePdmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextPdmTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressPdmTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.utilitiesRadPageView)).EndInit();
            this.utilitiesRadPageView.ResumeLayout(false);
            this.bushingMonitorRadPageViewPage.ResumeLayout(false);
            this.bushingMonitorRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonBushingCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDeviceHealthBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteDeviceDataBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmSaveEventLogsToFileRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmFirmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmFirmwareVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceDateBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceConfigurationBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getEquipmentErrorBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clearListBoxBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programLogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.latestReadingsBhmTabRadGroupBox)).EndInit();
            this.latestReadingsBhmTabRadGroupBox.ResumeLayout(false);
            this.latestReadingsBhmTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bhmInitialDataRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmInitialDataRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmLatestDataReadingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhmLatestDataReadingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialDataReadingsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.latestDataReadingsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getLatestReadingsBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressTextBhmTabRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressBhmTabRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setUpAutobalanceBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startMeasurementBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.balanceBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.readEventLogsBhmTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogBhmTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPageViewPage readRegistersRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage mainMonitorRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage pdMonitorRadPageViewPage;
        private Telerik.WinControls.UI.RadPageView utilitiesRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage bushingMonitorRadPageViewPage;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadButton deviceConfigurationBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton getEquipmentErrorBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton clearListBoxBhmTabRadButton;
        private System.Windows.Forms.PictureBox programLogoPictureBox;
        private Telerik.WinControls.UI.RadGroupBox latestReadingsBhmTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton getLatestReadingsBhmTabRadButton;
        private Telerik.WinControls.UI.RadLabel modbusAddressTextBhmTabRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressBhmTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton setUpAutobalanceBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton startMeasurementBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton balanceBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton readEventLogsBhmTabRadButton;
        private Telerik.WinControls.UI.RadListControl eventLogBhmTabRadListControl;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadLabel modbusAddressTextReadRegistersTabRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressReadRegistersTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel modbusAddressTextMainTabRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressMainTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel modbusAddressTextPdmTabRadLabel;
        private Telerik.WinControls.UI.RadGridView registerValuesRadGridView;
        private Telerik.WinControls.UI.RadLabel numberOfRegistersToReadTextReadRegistersTabRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox numberOfRegistersReadRegistersTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel startingRegisterTextReadRegistersTabRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox startingRegisterReadRegistersTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton readRegistersReadRegistersTabRadButton;
        private Telerik.WinControls.UI.RadButton setDeviceDateBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton setDeviceDateMainTabRadButton;
        private Telerik.WinControls.UI.RadButton setDeviceDatePdmTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox pmdMeasurementsRadGroupBox;
        private Telerik.WinControls.UI.RadButton setModeRadButton;
        private Telerik.WinControls.UI.RadPanel trendZoomButtonsRadPanel;
        private System.Windows.Forms.RadioButton normalModeRadioButton;
        private System.Windows.Forms.RadioButton testModeRadioButton;
        private Telerik.WinControls.UI.RadButton startMeasurementPdmTabRadButton;
        private Telerik.WinControls.UI.RadListControl eventLogPdmTabRadListControl;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton viewPhaseResolvedDataPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton getLatestReadingsPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton deviceConfigurationPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton getAlertsPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton getDeviceErrorsPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton clearDisplayPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton readEventLogsPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton findRecorsWithPhaseResolvedDataRadButton;
        private Telerik.WinControls.UI.RadLabel searchProgressRadLabel;
        private Telerik.WinControls.UI.RadLabel initialDataReadingsRadLabel;
        private Telerik.WinControls.UI.RadLabel latestDataReadingsRadLabel;
        private Telerik.WinControls.UI.RadLabel pdmDataDateRadLabel;
        private Telerik.WinControls.UI.RadLabel mainFirmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel mainFirmwareVersionTextRadLabel;
        private Telerik.WinControls.UI.RadLabel pdmFirmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel pdmFirmwareVersionTextRadLabel;
        private Telerik.WinControls.UI.RadLabel bhmFirmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel bhmFirmwareVersionTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox dynamicsMainRadGroupBox;
        private Telerik.WinControls.UI.RadButton getDynamicsRadButton;
        private Telerik.WinControls.UI.RadButton deviceConfigurationMainTabRadButton;
        private Telerik.WinControls.UI.RadButton pdmSaveEventLogsToFileRadButton;
        private Telerik.WinControls.UI.RadButton bhmSaveEventLogsToFileRadButton;
        private Telerik.WinControls.UI.RadButton deleteDeviceDataMainTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteDeviceDataPdmTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteDeviceDataBhmTabRadButton;
        private Telerik.WinControls.UI.RadButton calibrateAnalogSignalsRadButton;
        private Telerik.WinControls.UI.RadButton openDnpSettingsRadButton;
        private Telerik.WinControls.UI.RadMenu mainDisplayRadMenu;
        private Telerik.WinControls.UI.RadButton calibrateChannelsRadButton;
        private Telerik.WinControls.UI.RadButton windingHotSpotConfigurationRadButton;
        private Telerik.WinControls.UI.RadButton getDeviceHealthBhmTabRadButton;
        private Telerik.WinControls.UI.RadListControl eventLogMainTabRadListControl;
        private Telerik.WinControls.UI.RadGridView mainDynamicsDataRadGridView;
        private Telerik.WinControls.UI.RadGridView bhmLatestDataReadingsRadGridView;
        private Telerik.WinControls.UI.RadGridView bhmInitialDataRadGridView;
        private Telerik.WinControls.UI.RadGridView pdmChannelDataRadGridView;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadSpinEditor cycleOfAcqSpinEditor;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressPdmTabRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton radButtonBushingCalc;
    }
}

