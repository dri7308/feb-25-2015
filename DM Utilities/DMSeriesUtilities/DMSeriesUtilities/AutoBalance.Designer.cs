﻿namespace DMSeriesUtilities
{
    partial class AutoBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoBalance));
            this.energizingDelayRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.delayBeforeBalancineRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.setAutoBalanceRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.disableAutoBalanceRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableAutoBalanceRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.noLoadTestRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.hourRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.timeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.office2007BlackTheme2 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.energizingDelayRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayBeforeBalancineRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setAutoBalanceRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.disableAutoBalanceRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableAutoBalanceRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noLoadTestRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // energizingDelayRadSpinEditor
            // 
            this.energizingDelayRadSpinEditor.InterceptArrowKeys = false;
            this.energizingDelayRadSpinEditor.Location = new System.Drawing.Point(160, 268);
            this.energizingDelayRadSpinEditor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.energizingDelayRadSpinEditor.Name = "energizingDelayRadSpinEditor";
            // 
            // 
            // 
            this.energizingDelayRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.energizingDelayRadSpinEditor.ShowBorder = true;
            this.energizingDelayRadSpinEditor.Size = new System.Drawing.Size(39, 20);
            this.energizingDelayRadSpinEditor.TabIndex = 34;
            this.energizingDelayRadSpinEditor.TabStop = false;
            this.energizingDelayRadSpinEditor.ThemeName = "Office2007Black";
            this.energizingDelayRadSpinEditor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // delayBeforeBalancineRadLabel
            // 
            this.delayBeforeBalancineRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delayBeforeBalancineRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.delayBeforeBalancineRadLabel.Location = new System.Drawing.Point(41, 268);
            this.delayBeforeBalancineRadLabel.Name = "delayBeforeBalancineRadLabel";
            this.delayBeforeBalancineRadLabel.Size = new System.Drawing.Size(114, 15);
            this.delayBeforeBalancineRadLabel.TabIndex = 35;
            this.delayBeforeBalancineRadLabel.Text = "<html>Days to be Energized </html>";
            this.delayBeforeBalancineRadLabel.ThemeName = "Office2007Black";
            this.delayBeforeBalancineRadLabel.Click += new System.EventHandler(this.delayBeforeBalancineRadLabel_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Location = new System.Drawing.Point(206, 268);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(30, 15);
            this.radLabel1.TabIndex = 36;
            this.radLabel1.Text = "<html>days</html>";
            this.radLabel1.ThemeName = "Office2007Black";
            // 
            // setAutoBalanceRadButton
            // 
            this.setAutoBalanceRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.setAutoBalanceRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setAutoBalanceRadButton.Location = new System.Drawing.Point(32, 379);
            this.setAutoBalanceRadButton.Name = "setAutoBalanceRadButton";
            this.setAutoBalanceRadButton.Size = new System.Drawing.Size(64, 30);
            this.setAutoBalanceRadButton.TabIndex = 40;
            this.setAutoBalanceRadButton.Text = "<html>Set</html>";
            this.setAutoBalanceRadButton.ThemeName = "Office2007Black";
            this.setAutoBalanceRadButton.Click += new System.EventHandler(this.setAutoBalanceRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(185, 379);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(64, 30);
            this.cancelRadButton.TabIndex = 41;
            this.cancelRadButton.Text = "<html>Cancel</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // disableAutoBalanceRadRadioButton
            // 
            this.disableAutoBalanceRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableAutoBalanceRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableAutoBalanceRadRadioButton.Location = new System.Drawing.Point(44, 348);
            this.disableAutoBalanceRadRadioButton.Name = "disableAutoBalanceRadRadioButton";
            this.disableAutoBalanceRadRadioButton.Size = new System.Drawing.Size(148, 18);
            this.disableAutoBalanceRadRadioButton.TabIndex = 44;
            this.disableAutoBalanceRadRadioButton.Text = "Disable Auto-balance";
            this.disableAutoBalanceRadRadioButton.ThemeName = "Office2007Black";
            // 
            // enableAutoBalanceRadRadioButton
            // 
            this.enableAutoBalanceRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableAutoBalanceRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableAutoBalanceRadRadioButton.Location = new System.Drawing.Point(44, 324);
            this.enableAutoBalanceRadRadioButton.Name = "enableAutoBalanceRadRadioButton";
            this.enableAutoBalanceRadRadioButton.Size = new System.Drawing.Size(148, 18);
            this.enableAutoBalanceRadRadioButton.TabIndex = 43;
            this.enableAutoBalanceRadRadioButton.TabStop = true;
            this.enableAutoBalanceRadRadioButton.Text = "Enable Auto-balance";
            this.enableAutoBalanceRadRadioButton.ThemeName = "Office2007Black";
            this.enableAutoBalanceRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // noLoadTestRadCheckBox
            // 
            this.noLoadTestRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noLoadTestRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.noLoadTestRadCheckBox.Location = new System.Drawing.Point(44, 303);
            this.noLoadTestRadCheckBox.Name = "noLoadTestRadCheckBox";
            this.noLoadTestRadCheckBox.Size = new System.Drawing.Size(192, 15);
            this.noLoadTestRadCheckBox.TabIndex = 42;
            this.noLoadTestRadCheckBox.Text = "<html>Perform no-load test after balancing</html>";
            this.noLoadTestRadCheckBox.Visible = false;
            // 
            // hourRadSpinEditor
            // 
            this.hourRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourRadSpinEditor.Location = new System.Drawing.Point(160, 228);
            this.hourRadSpinEditor.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.hourRadSpinEditor.Name = "hourRadSpinEditor";
            // 
            // 
            // 
            this.hourRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.hourRadSpinEditor.ShowBorder = true;
            this.hourRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.hourRadSpinEditor.TabIndex = 49;
            this.hourRadSpinEditor.TabStop = false;
            this.hourRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // timeRadLabel
            // 
            this.timeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeRadLabel.Location = new System.Drawing.Point(38, 229);
            this.timeRadLabel.Name = "timeRadLabel";
            this.timeRadLabel.Size = new System.Drawing.Size(110, 18);
            this.timeRadLabel.TabIndex = 46;
            this.timeRadLabel.Text = "Time of Day by Hour";
            // 
            // radCalendar1
            // 
            this.radCalendar1.CellAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radCalendar1.CellMargin = new System.Windows.Forms.Padding(0);
            this.radCalendar1.CellPadding = new System.Windows.Forms.Padding(0);
            this.radCalendar1.FastNavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationNextImage")));
            this.radCalendar1.FastNavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationPrevImage")));
            this.radCalendar1.HeaderHeight = 17;
            this.radCalendar1.HeaderWidth = 17;
            this.radCalendar1.Location = new System.Drawing.Point(12, 12);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.NavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationNextImage")));
            this.radCalendar1.NavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationPrevImage")));
            this.radCalendar1.RangeMaxDate = new System.DateTime(2099, 12, 30, 0, 0, 0, 0);
            this.radCalendar1.Size = new System.Drawing.Size(270, 201);
            this.radCalendar1.TabIndex = 45;
            this.radCalendar1.Text = "radCalendar1";
            this.radCalendar1.ThemeName = "Office2007Black";
            // 
            // AutoBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(294, 421);
            this.Controls.Add(this.hourRadSpinEditor);
            this.Controls.Add(this.timeRadLabel);
            this.Controls.Add(this.radCalendar1);
            this.Controls.Add(this.disableAutoBalanceRadRadioButton);
            this.Controls.Add(this.enableAutoBalanceRadRadioButton);
            this.Controls.Add(this.noLoadTestRadCheckBox);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.setAutoBalanceRadButton);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.delayBeforeBalancineRadLabel);
            this.Controls.Add(this.energizingDelayRadSpinEditor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(250, 228);
            this.Name = "AutoBalance";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(0, 0);
            this.Text = "Setup Auto-Balance";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.AutoBalance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.energizingDelayRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.delayBeforeBalancineRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setAutoBalanceRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.disableAutoBalanceRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableAutoBalanceRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noLoadTestRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadSpinEditor energizingDelayRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel delayBeforeBalancineRadLabel;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton setAutoBalanceRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadRadioButton disableAutoBalanceRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableAutoBalanceRadRadioButton;
        private Telerik.WinControls.UI.RadCheckBox noLoadTestRadCheckBox;
        private Telerik.WinControls.UI.RadSpinEditor hourRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel timeRadLabel;
        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme2;
    }
}