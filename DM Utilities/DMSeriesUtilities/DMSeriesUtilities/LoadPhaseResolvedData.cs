﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using ChartDirector;

using ConfigurationObjects;
using DataObjects;
using GeneralUtilities;
using MonitorInterface;

namespace DMSeriesUtilities
{
    public partial class LoadPhaseResolvedData : Telerik.WinControls.UI.RadForm
    {
        private BackgroundWorker dataDownloadBackgroundWorker;

        bool continueDownloading;

        private bool cancelled = false;
        public bool Cancelled
        {
            get
            {
                return cancelled;
            }
        }
    
        private Dictionary<int, bool> channelIsActive;
        //public Dictionary<int, bool> ChannelIsActive
        //{
        //    get
        //    {
        //        return channelIsActive;
        //    }
        //}

        private PDM_SingleDataReading pdmSingleDataReading = null;
        public PDM_SingleDataReading PdmSingleDataReading
        {
            get
            {
                return pdmSingleDataReading;
            }
        }

        private PDM_Configuration pdmConfiguration = null;
        public PDM_Configuration PdmConfiguration
        {
            get
            {
                return pdmConfiguration;
            }
        }

        private PDM_RegisterGeneralData pdmRegisterGeneralData = null;
        public PDM_RegisterGeneralData PdmRegisterGeneralData
        {
            get
            {
                return pdmRegisterGeneralData;
            }
        }

        private List<PDM_RegisterChannelData> pdmRegisterChannelDataReadings = null;
        public List<PDM_RegisterChannelData> PdmRegisterChannelDataReadings
        {
            get
            {
                return pdmRegisterChannelDataReadings;
            }
        }

        private BHM_SingleDataReading bhmSingleDataReading = null;
        public BHM_SingleDataReading BhmSingleDataReading
        {
            get
            {
                return bhmSingleDataReading;
            }
        }

        private List<BHM_RegisterDataForOneSet> bhmRegisterDataReadings = null;
        public List<BHM_RegisterDataForOneSet> BhmRegisterDataReadings
        {
            get
            {
                return bhmRegisterDataReadings;
            }
        }

        private BHM_Configuration bhmConfiguration = null;
        public BHM_Configuration BhmConfiguration
        {
            get
            {
                return bhmConfiguration;
            }
        }

        int deviceType;
        int modBusAddress;

        bool getPhaseResolvedData;
        bool getArchiveData;
        
        int totalNumberOfRecordsBeingDownloaded;
        int numberOfRecordsDownloadedSoFar = 0;

        int numberOfNonInteractiveRetries;
        int totalRetries;

        DateTime fakeFirstMeasurementDateTime;

        public LoadPhaseResolvedData(int inputDeviceType, int inputModBusAddress, bool inputGetArchiveData, bool inputGetPhaseResolvedData, int argNumberOfNonInteractiveRetries, int argTotalRetries)
        {
            try
            {
                InitializeComponent();

                this.deviceType = inputDeviceType;
                this.modBusAddress = inputModBusAddress;
                this.getPhaseResolvedData = inputGetPhaseResolvedData;
                this.getArchiveData = inputGetArchiveData;
                this.numberOfNonInteractiveRetries = argNumberOfNonInteractiveRetries;
                this.totalRetries = argTotalRetries;

                InitializeCommandMonitorBackgroundWorker();
                this.StartPosition = FormStartPosition.CenterParent;

                fakeFirstMeasurementDateTime = new DateTime(2010, 1, 1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.LoadPhaseResolvedData(int, int, bool, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadPhaseResolvedData_Load(object sender, EventArgs e)
        {
            try
            {
                dataDownloadBackgroundWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.LoadPhaseResolvedData_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                continueDownloading = false;
                DeviceCommunication.DisableDataDownload();
                dataDownloadBackgroundWorker.CancelAsync();
                cancelled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.cancelRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void UpdateProgressBar(int tryNumber, int totalTries)
        {
            try
            {
                if ((tryNumber >= 0) && (tryNumber <= totalTries))
                {
                    dataDownloadBackgroundWorker.ReportProgress((tryNumber * 100) / totalTries);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.commandMonitorBackgroundWorker_DoWork(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCommandMonitorBackgroundWorker()
        {
            try
            {
                /// Set up the download background worker
                dataDownloadBackgroundWorker = new BackgroundWorker()
                {
                    WorkerSupportsCancellation = true,
                    WorkerReportsProgress = true
                };

                dataDownloadBackgroundWorker.DoWork += dataDownloadBackgroundWorker_DoWork;
                dataDownloadBackgroundWorker.ProgressChanged += dataDownloadBackgroundWorker_ProgressChanged;
                dataDownloadBackgroundWorker.RunWorkerCompleted += dataDownloadBackgroundWorker_RunWorkerCompleted;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.InitializeCommandMonitorBackgroundWorker()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataDownloadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ErrorCode errorCode = ErrorCode.None;

                if (this.deviceType == 505)
                {
                    if (getArchiveData)
                    {
                        GetMostRecentPhaseResolvedData();
                    }
                    else
                    {
                       GetPdmRegisterData();
                    }
                }
                else if (this.deviceType == 15002)
                {
                    if (getArchiveData)
                    {
                        GetBhmArchiveData();
                    }
                    else
                    {
                        GetBhmRegisterData();
                    }
                }          
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                
            }
        }

        private void GetBhmArchiveData()
        {
            try
            {
                Int32[] archiveDownloadLimits;
                string downloadErrorMessage = string.Empty;
                Int16 recordToDownload;
                Int16[] registerValues;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.numberOfRecordsDownloadedSoFar = 0;
                this.totalNumberOfRecordsBeingDownloaded = 7;

                UpdateProgressBar(0, this.totalNumberOfRecordsBeingDownloaded);

                this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting support data"));

                // archiveDownloadLimits = InteractiveDeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, fakeFirstMeasurementDateTime, 300, this.numberOfNonInteractiveRetries, this.totalRetries);
                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 15, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                this.numberOfRecordsDownloadedSoFar++;
                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                Application.DoEvents();

                //if ((archiveDownloadLimits == null) || (archiveDownloadLimits.Length != 2))
                //{
                //    RadMessageBox.Show("Data download failed");
                //}
                if ((registerValues == null) || (registerValues.Length != 1))
                {
                    RadMessageBox.Show("Data download failed");
                }
                else
                {
                    recordToDownload = registerValues[0];

                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting device configuration"));

                    bhmConfiguration = BHM_LoadConfigurationFromDevice();

                    if (bhmConfiguration == null)
                    {
                        RadMessageBox.Show("Failed to get device configuration data - cancelling");
                    }
                    else
                    {
                        this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting device data"));

                        bhmSingleDataReading = BHM_ReadOneArchivedRecord(modBusAddress, recordToDownload, 200, ref downloadErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.GetBhmArchiveData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private void GetBhmRegisterData()
        {
            try
            {
                string downloadErrorMessage = string.Empty;

                this.numberOfRecordsDownloadedSoFar = 0;
                this.totalNumberOfRecordsBeingDownloaded = 6;
                UpdateProgressBar(0, this.totalNumberOfRecordsBeingDownloaded);

                this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting register data"));

                this.bhmRegisterDataReadings = BHM_ReadCurrentRecordFromTheRegisters(modBusAddress, 200, ref downloadErrorMessage);

                this.numberOfRecordsDownloadedSoFar++;
                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                Application.DoEvents();

                if (bhmRegisterDataReadings == null)
                {
                    RadMessageBox.Show("Data download failed");
                }
                else
                {
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting device configuration"));

                    this.bhmConfiguration = BHM_LoadConfigurationFromDevice();

                    if (bhmConfiguration == null)
                    {
                        RadMessageBox.Show("Failed to get initial balance data");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.GetBhmRegisterData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private ErrorCode GetMostRecentPhaseResolvedData()
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                int totalBytesBeingDownloaded = 0;
                int totalChunksBeingDownloaded = 0;
                int totalRecordsOnDevice;
                int numberOfRecordsExaminedForPhaseResolvedData = 0;
                Int16[] registerValues;
                byte[] commonAndChannelDataAsBytes = null;
                byte[] phaseResolvedDataAsBytes = null;
                string downloadErrorMessage = string.Empty;
                int activeChannelCount;
                Int16 recordToDownload;
                bool phaseResolvedDataFound = false;
                DownloadReturnObject currentDownloadReturnObject;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting support info"));

                this.totalNumberOfRecordsBeingDownloaded = 4;

                UpdateProgressBar(0, this.totalNumberOfRecordsBeingDownloaded);

                //registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 3008, 2, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                //UpdateProgressBar(1, this.totalNumberOfRecordsBeingDownloaded);

                //if ((registerValues != null) && (registerValues.Length == 2))
                //{
                //    /// FOR TESTING ONLY///
                //    //registerValues[1] = 1;
                //    /////////////////////////

                //    if ((this.getPhaseResolvedData) && (registerValues[1] == 0))
                //    {
                //        // RadMessageBox.Show("No phase resolved data saved for current measurement");
                //    }
                //    else
                //    {
                //        /// this is 0-index with reference to channel number
                //        channelIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(ConversionMethods.Int16BytesToUInt16Value(registerValues[0]));

                //archiveDownloadLimits = InteractiveDeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, fakeFirstMeasurementDateTime, 200, this.numberOfNonInteractiveRetries, this.totalRetries);
                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 15, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                UpdateProgressBar(1, this.totalNumberOfRecordsBeingDownloaded);

                //DeviceCommunication.CloseConnection();

                if ((registerValues != null) && (registerValues.Length == 1))
                {
                    /////FOR TESTING ONLY ////////////
                    // registerValues[0] = 3980;
                    //////////////////////////////////

                    recordToDownload = registerValues[0];
                    totalRecordsOnDevice = recordToDownload;

                    this.numberOfRecordsDownloadedSoFar = 2;
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting device configuration"));
                    currentDownloadReturnObject = PDM_LoadConfigurationFromDevice();
                    if (currentDownloadReturnObject.errorCode == ErrorCode.ConfigurationDownloadSucceeded)
                    {
                        this.pdmConfiguration = currentDownloadReturnObject.pdmConfiguration;

                        UpdateProgressBar(0, totalRecordsOnDevice);
                        this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "<html>Searching for most recent archive entry<br>that has phase resolved data</html>"));                        
                        currentDownloadReturnObject = PDM_GetCommonAndChannelData(modBusAddress, recordToDownload, 200);
                        numberOfRecordsExaminedForPhaseResolvedData++;
                        UpdateProgressBar(numberOfRecordsExaminedForPhaseResolvedData, totalRecordsOnDevice);
                        while (!phaseResolvedDataFound && (currentDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded) && (recordToDownload > 0))
                        {
                            //activeChannelCount = PDM_SingleDataReading.ActiveChannelCount(currentDownloadReturnObject.byteData);
                            //if (activeChannelCount > 0)
                            if(PDM_SingleDataReading.PhaseResolvedDataWasSaved(currentDownloadReturnObject.byteData))
                            {
                                phaseResolvedDataFound = true;
                            }
                            else
                            {
                                recordToDownload--;
                                currentDownloadReturnObject = PDM_GetCommonAndChannelData(modBusAddress, recordToDownload, 200);
                                numberOfRecordsExaminedForPhaseResolvedData++;
                                UpdateProgressBar(numberOfRecordsExaminedForPhaseResolvedData, totalRecordsOnDevice);
                            }
                        }

                        if ((recordToDownload > 0) && PDM_SingleDataReading.PhaseResolvedDataWasSaved(currentDownloadReturnObject.byteData))
                        {
                            if (currentDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                            {
                                /// save the byte info from the currentDownloadReturnObject so we can use the return object again
                                commonAndChannelDataAsBytes = currentDownloadReturnObject.byteData;

                                activeChannelCount = PDM_SingleDataReading.ActiveChannelCount(commonAndChannelDataAsBytes);
                                totalBytesBeingDownloaded = activeChannelCount * 6144;
                                totalChunksBeingDownloaded = totalBytesBeingDownloaded / 1000 + 1;

                                this.totalNumberOfRecordsBeingDownloaded = totalChunksBeingDownloaded;
                                this.numberOfRecordsDownloadedSoFar = 0;
                                this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting phase resolved data"));
                                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);

                                currentDownloadReturnObject = PDM_GetPhaseResolvedData(modBusAddress, recordToDownload, activeChannelCount, 200);
                                if (currentDownloadReturnObject.errorCode == ErrorCode.DownloadSucceeded)
                                {
                                    phaseResolvedDataAsBytes = currentDownloadReturnObject.byteData;

                                    this.pdmSingleDataReading = new PDM_SingleDataReading(commonAndChannelDataAsBytes, phaseResolvedDataAsBytes);
                                    errorCode = ErrorCode.DownloadSucceeded;
                                }
                                else
                                {
                                    errorCode = currentDownloadReturnObject.errorCode;
                                }
                            }
                            else
                            {
                                errorCode = currentDownloadReturnObject.errorCode;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.NoPhaseResolvedDataEntriesFound;
                        }
                    }
                    else
                    {
                        errorCode = currentDownloadReturnObject.errorCode;
                    }
                }
                else
                {
                    errorCode = ErrorCode.DownloadFailed;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.GetMostRecentPhaseResolvedData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
            return errorCode;
        }

        private void GetPdmRegisterData()
        {
            try
            {
                string downloadErrorMessage = string.Empty; ;

                this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting common data"));
                this.totalNumberOfRecordsBeingDownloaded = 5;
                this.numberOfRecordsDownloadedSoFar = 0;

                UpdateProgressBar(0, this.totalNumberOfRecordsBeingDownloaded);

                pdmRegisterGeneralData = PDM_ReadCurrentGeneralDataFromTheRegisters(modBusAddress, 200, ref downloadErrorMessage);

                if (pdmRegisterGeneralData != null)
                {
                    this.Invoke(new MethodInvoker(() => this.dataDownloadRadProgressBar.Text = "Getting channel data"));
                    pdmRegisterChannelDataReadings = PDM_ReadCurrentChannelDataFromTheRegisters(modBusAddress, 200, ref downloadErrorMessage);
                    if (pdmRegisterChannelDataReadings == null)
                    {
                        pdmRegisterGeneralData = null;
                        RadMessageBox.Show("Data download failed");
                    }
                }
                else
                {
                    RadMessageBox.Show("Data download failed");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.GetPdmRegisterData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        private void dataDownloadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                int progress = e.ProgressPercentage;
                dataDownloadRadProgressBar.Value1 = progress;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.commandMonitorBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataDownloadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in LoadPhaseResolvedData.dataDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else if (e.Cancelled)
                {
                    cancelled = true;
                }                    
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.commandMonitorBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.Close();
            }
        }

        private DownloadReturnObject PDM_GetCommonAndChannelData(int modBusAddress, int recordNumber, int readDelayInMicroseconds)
        {
            DownloadReturnObject downloadReturnObject = new DownloadReturnObject();
            try
            {
                // really 2048;
                int maximumPossibleBytes = 2100;
                Byte[] allDownloadedBytes = new Byte[maximumPossibleBytes];
                Byte[] currentChunk = null;
                bool allDataReceived = false;
                int offset = 0;
                int loopCount = 0;
                int bytesReturned = 0;
                int downloadProgress = 1;
                int downloadIncrement = 33;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                while (!allDataReceived && (loopCount < 3) && DeviceCommunication.DownloadIsEnabled() &&(!dataDownloadBackgroundWorker.CancellationPending))
                {
                    currentChunk = InteractiveDeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                    if (currentChunk != null)
                    {
                        bytesReturned = currentChunk.Length;

                        // copy the current data to the save array

                        // if this is our third time through the loop, we need to adjust the size of bytesReturned to reflect that
                        // we only need 48 of the possilbe 1000 bytes.
                        if (loopCount == 2)
                        {
                            bytesReturned = 48;
                        }

                        Array.Copy(currentChunk, 0, allDownloadedBytes, offset, bytesReturned);

                        offset += bytesReturned;
                        loopCount++;

                        if ((bytesReturned < 1000) || (loopCount == 3))
                        {
                            allDataReceived = true;
                        }

                        this.numberOfRecordsDownloadedSoFar++;
                    }
                    else
                    {
                        allDataReceived = true;
                        allDownloadedBytes = null;
                    }
                    downloadProgress += downloadIncrement;
                    UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                    Application.DoEvents();
                }

                if (allDownloadedBytes != null)
                {
                    // the value for offset at this point also happens to be the number of bytes we have received
                    if (offset == 2048)
                    {
                        downloadReturnObject.byteData = new Byte[offset];
                        Array.Copy(allDownloadedBytes, 0, downloadReturnObject.byteData, 0, offset);
                        downloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                    else
                    {
                        downloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                    }
                }
                else
                {
                    if (!DeviceCommunication.DownloadIsEnabled())
                    {
                        downloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.PDM_GetCommonAndChannelData(int, int, int, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
               
            }
            return downloadReturnObject;
        }

        private DownloadReturnObject PDM_GetPhaseResolvedData(int modBusAddress, int recordNumber, int activeChannelCount, int readDelayInMicroseconds)
        {
            DownloadReturnObject downloadReturnObject = new DownloadReturnObject();
            try
            {
                // really 92160
                int maximumPossibleBytes = 93000;
                Byte[] allDownloadedBytes = new Byte[maximumPossibleBytes];
                Byte[] currentChunk = null;
                bool allDataReceived = false;
                int offset = 2048;
                int totalBytesReturnedSoFar = 0;
                int loopCount = 0;
                int currentBytesReturned = 0;

                int totalBytesBeingDownloaded = activeChannelCount * 6144;
                int totalChunksBeingDownloaded = totalBytesBeingDownloaded / 1000 + 1;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                while (!allDataReceived && (loopCount < 93) && DeviceCommunication.DownloadIsEnabled() && (!dataDownloadBackgroundWorker.CancellationPending))
                {
                    currentChunk = InteractiveDeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                    if (currentChunk != null)
                    {
                        currentBytesReturned = currentChunk.Length;
                        Array.Copy(currentChunk, 0, allDownloadedBytes, totalBytesReturnedSoFar, currentBytesReturned);

                        offset += currentBytesReturned;
                        totalBytesReturnedSoFar += currentBytesReturned;
                        loopCount++;

                        /// The first condition will stop the loop unless there are 10 active channels.  at that point, the 
                        /// second condition will work.  Of course, the second condition will always work, but the first one is 
                        /// legacy and doesn't hurt anything.  The third condition is a sanity check of sorts, and should not get 
                        /// hit ever if things are working correctly.
                        if ((currentBytesReturned < 1000) || (totalBytesReturnedSoFar >= totalBytesBeingDownloaded) || (loopCount == 93))
                        {
                            allDataReceived = true;
                        }

                        this.numberOfRecordsDownloadedSoFar++;
                    }
                    else
                    {
                        // if we get a null array back, we have failed and are quitting
                        allDataReceived = true;
                        allDownloadedBytes = null;
                    }
                    UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                    Application.DoEvents();
                }

                if (allDownloadedBytes != null)
                {
                    // the value for offset at this point also happens to be the number of bytes we have received
                    if ((totalBytesReturnedSoFar % 6144) == 0)
                    {
                        downloadReturnObject.byteData = new Byte[totalBytesReturnedSoFar];
                        Array.Copy(allDownloadedBytes, 0, downloadReturnObject.byteData, 0, totalBytesReturnedSoFar);
                        downloadReturnObject.errorCode = ErrorCode.DownloadSucceeded;
                    }
                    else
                    {
                        downloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                        string errorMessage = "Error in LoadPhaseResolvedData.PDM_GetPhaseResolvedData(int, int, int, int)\nThe amount of phase resolved data returned was incorrect, error assumed.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    if (!DeviceCommunication.DownloadIsEnabled())
                    {
                        downloadReturnObject.errorCode = ErrorCode.DownloadCancelled;
                    }
                    else
                    {
                        downloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.PDM_GetPhaseResolvedData(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                
            }
            return downloadReturnObject;
        }

        public DownloadReturnObject PDM_LoadConfigurationFromDevice()
        {
            DownloadReturnObject downloadReturnObject = new DownloadReturnObject();
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                int offset = 0;
                //int deviceType;
                //long deviceError;
                int bytesSaved = 0;
                int bytesReturned = 0;
                bool receivedAllData = false;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                downloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadFailed;

                allByteValues = new Byte[2000];

                //deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds);
                //if (deviceType > 0)
                //{
                //    if (deviceType == 505)
                //    {
                //        deviceError = GetDeviceError(modBusAddress, readDelayInMicroseconds);
                //        if (deviceError > -1)
                //        {
                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled() && (!dataDownloadBackgroundWorker.CancellationPending))
                {
                    currentByteValues = InteractiveDeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                    if (currentByteValues != null)
                    {
                        bytesReturned = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                        bytesSaved += bytesReturned;
                        offset += bytesReturned;
                        if (bytesReturned < 1000)
                        {
                            receivedAllData = true;
                        }
                        numberOfRecordsDownloadedSoFar++;
                    }
                    else
                    {
                        receivedAllData = true;
                        downloadReturnObject.errorCode = ErrorCode.DownloadFailed;
                        allByteValues = null;
                        break;
                    }
                    UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                    Application.DoEvents();
                }
                if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled())
                {
                    byteValuesToCreateConfiguration = new Byte[bytesSaved];
                    Array.Copy(allByteValues, byteValuesToCreateConfiguration, bytesSaved);
                    downloadReturnObject.pdmConfiguration = new PDM_Configuration(byteValuesToCreateConfiguration);
                    downloadReturnObject.errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                }              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.PDM_LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
            return downloadReturnObject;
        }

        public BHM_Configuration BHM_LoadConfigurationFromDevice()
        {
            BHM_Configuration configuration = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                int offset = 1;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int bytesPerDataRequest = 200;
                int numberOfInitializationBytes = 300;
                bool receivedAllData = false;

                ErrorCode errorCode = ErrorCode.ConfigurationDownloadFailed;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);

                allByteValues = new Byte[2000];
                currentByteValues = new Byte[numberOfInitializationBytes];

                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled() && (!dataDownloadBackgroundWorker.CancellationPending))
                {
                    currentByteValues = InteractiveDeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, bytesPerDataRequest, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                    if (currentByteValues != null)
                    {
                        bytesReturned = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                        bytesSaved += bytesReturned;
                        offset += bytesReturned;
                        if (bytesReturned < bytesPerDataRequest)
                        {
                            receivedAllData = true;
                        }
                        this.numberOfRecordsDownloadedSoFar++;
                    }
                    else
                    {
                        receivedAllData = true;
                        errorCode = ErrorCode.DownloadFailed;
                        allByteValues = null;
                        break;
                    }

                    UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                    Application.DoEvents();
                }
                if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled())
                {
                    byteValuesToCreateConfiguration = new Byte[bytesSaved];
                    Array.Copy(allByteValues, byteValuesToCreateConfiguration, bytesSaved);
                    configuration = new BHM_Configuration(byteValuesToCreateConfiguration);
                    errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                }
                else
                {
                    if ((!DeviceCommunication.DownloadIsEnabled()) || dataDownloadBackgroundWorker.CancellationPending)
                    {
                        errorCode = ErrorCode.DownloadCancelled;
                    }
                    else
                    {
                        errorCode = ErrorCode.ConfigurationDownloadFailed;
                    }
                }
                if (errorCode != ErrorCode.ConfigurationDownloadSucceeded)
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.BHM_LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
            return configuration;
        }

        private BHM_SingleDataReading BHM_ReadOneArchivedRecord(int modBusAddress, Int16 recordNumber, int readDelayInMicroseconds, ref string downloadErrorMessage)
        {
            BHM_SingleDataReading dataReading = null;
            try
            {
                Byte[] byteData;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                byteData = InteractiveDeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordNumber, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                this.numberOfRecordsDownloadedSoFar++;
                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                Application.DoEvents();

                if (byteData != null)
                {
                    // this.currentOperationRadLabel.Text = "Saving downloaded data to DB";
                    dataReading = new BHM_SingleDataReading(byteData);
                    if (!dataReading.EnoughMembersAreNonNull())
                    {
                        RadMessageBox.Show("Failed to download data readings");
                    }
                }
                else
                {
                    RadMessageBox.Show("Failed to download data readings");
                }                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.BHM_ReadOneArchivedRecord(int, Int16, int, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                
            }
            return dataReading;
        }

        private List<BHM_RegisterDataForOneSet> BHM_ReadCurrentRecordFromTheRegisters(int modBusAddress, int readDelayInMicroseconds, ref string downloadErrorMessage)
        {
            List<BHM_RegisterDataForOneSet> dataReadings = null;
            try
            {
                Int16[] registerData;
                UInt16[] unsignedRegisterData;
                UInt16[] dataForOneReading;
                BHM_RegisterDataForOneSet singleDataReading;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                registerData = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 901, 62, readDelayInMicroseconds, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);

                this.numberOfRecordsDownloadedSoFar++;
                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                Application.DoEvents();

                if ((registerData != null)&&(registerData.Length == 62))
                {
                    unsignedRegisterData = ConversionMethods.Int16ArrayToUInt16Array(registerData);

                    dataReadings = new List<BHM_RegisterDataForOneSet>();

                    dataForOneReading = new UInt16[29];

                    Array.Copy(unsignedRegisterData, 0, dataForOneReading, 0, 29);
                    singleDataReading = new BHM_RegisterDataForOneSet();
                    singleDataReading.SetDataValues(dataForOneReading, 1);
                    dataReadings.Add(singleDataReading);

                    Array.Copy(unsignedRegisterData, 31, dataForOneReading, 0, 29);
                    singleDataReading = new BHM_RegisterDataForOneSet();
                    singleDataReading.SetDataValues(dataForOneReading, 1);
                    dataReadings.Add(singleDataReading);
                }
                else
                {
                    RadMessageBox.Show("Failed to download data readings");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.BHM_ReadCurrentRecordFromTheRegisters(int, int, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
            return dataReadings;
        }

        private PDM_RegisterGeneralData PDM_ReadCurrentGeneralDataFromTheRegisters(int modBusAddress, int readDelayInMicroseconds, ref string downloadErrorMessage)
        {
            PDM_RegisterGeneralData generalData = null;
            try
            {
                Int16[] registerValues;
                UInt16[] unsignedRegisterValues;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                registerValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 3001, 11, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                this.numberOfRecordsDownloadedSoFar++;
                UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);
                if ((registerValues != null) && (registerValues.Length == 11))
                {
                    unsignedRegisterValues = ConversionMethods.Int16ArrayToUInt16Array(registerValues);
                    generalData = new PDM_RegisterGeneralData();
                    generalData.SetDataValues(unsignedRegisterValues);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.PDM_ReadCurrentGeneralDataFromTheRegisters(int, int, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return generalData;
        }

        private List<PDM_RegisterChannelData> PDM_ReadCurrentChannelDataFromTheRegisters(int modBusAddress, int readDelayInMicroseconds, ref string downloadErrorMessage)
        {
            List<PDM_RegisterChannelData> channelData = null;
            try
            {
                PDM_RegisterChannelData oneChannelData;
                Int16[] currentRegisterValues;
                Int16[] allRegisterValues = new Int16[450];
                int offset = 3041;
                int registersToRead = 115;
                int allRegisterValuesIndex = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                for (int i = 0; i < 4; i++)
                {
                    if (i == 3)
                    {
                        registersToRead = 105;
                    }

                    currentRegisterValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, offset, registersToRead, 200, this.numberOfNonInteractiveRetries, this.totalRetries, parentWindowInformation);
                    this.numberOfRecordsDownloadedSoFar++;
                    UpdateProgressBar(this.numberOfRecordsDownloadedSoFar, this.totalNumberOfRecordsBeingDownloaded);

                    if ((currentRegisterValues != null) && (currentRegisterValues.Length == registersToRead))
                    {
                        Array.Copy(currentRegisterValues, 0, allRegisterValues, allRegisterValuesIndex, registersToRead);
                        allRegisterValuesIndex += registersToRead;
                        offset += registersToRead;
                    }
                    else
                    {
                        allRegisterValues = null;
                        break;
                    }
                }

                if (allRegisterValues != null)
                {
                    channelData = new List<PDM_RegisterChannelData>();
                    currentRegisterValues=new Int16[30];
                    allRegisterValuesIndex=0;
                    for (int i = 0; i < 15; i++)
                    {
                        Array.Copy(allRegisterValues, allRegisterValuesIndex, currentRegisterValues, 0, 30);
                        allRegisterValuesIndex += 30;

                        oneChannelData = new PDM_RegisterChannelData();
                        oneChannelData.SetDataValues(currentRegisterValues);
                        channelData.Add(oneChannelData);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in LoadPhaseResolvedData.PDM_ReadCurrentChannelDataFromTheRegisters(int, int, ref string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelData;
        }
    }

    public class DownloadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public PDM_Configuration pdmConfiguration = null;
        public PDM_SingleDataReading pdmSingleDataReading = null;
        public Byte[] byteData = null;
    }


}
