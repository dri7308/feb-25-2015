﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DMSeriesUtilities
{
    public class PDM_RegisterChannelData
    {
        public double gammaNC;
        public double gammaMV;
        public double gammaPlus;
        public double gammaMinus;
        public double pdi;
        public double pdiPlus;
        public double pdiMinus;
        public double gammaTrend;
        public double pdiTrend;
        public double gammaChange;
        public double pdiChange;
        public int sumPD;
        public int sumPDPlus;
        public int sumPDMinus;

        public PDM_RegisterChannelData()
        {

        }

        public void SetDataValues(Int16[] registerValues)
        {
            try
            {
                if ((registerValues != null) && (registerValues.Length >= 14))
                {
                    gammaNC = registerValues[0] / 100.0;
                    gammaMV = registerValues[1];
                    gammaPlus = registerValues[2];
                    gammaMinus = registerValues[3];
                    pdi = registerValues[4] / 10.0;
                    pdiPlus = registerValues[5] / 10.0;
                    pdiMinus = registerValues[6] / 10.0;
                    gammaTrend = registerValues[7] / 100.0;
                    pdiTrend = registerValues[8] / 100.0;
                    gammaChange = registerValues[9];
                    pdiChange = registerValues[10];
                    sumPD = registerValues[11];
                    sumPDPlus = registerValues[12];
                    sumPDMinus = registerValues[13];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_RegisterChannelData.SetDataValues(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
