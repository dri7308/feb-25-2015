/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd . - All rights reserved.
**
** FILENAME: VCUSBDevice.h
**
** PURPOSE:  This file contains the prototypes and constants used for detecting
**			 both statically connected devices and dynamic arrival/removal.
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#ifndef VCUSB_DEV_NOTIFY_H
#define VCUSB_DEV_NOTIFY__H

// Constants

// Structures
typedef struct
{
	HANDLE		hDevice;
	HDEVNOTIFY	hDevNotify;
	WINUSB_INTERFACE_HANDLE	hUsb;
	PWCHAR		szwpDevPath;
	UCHAR		BulkInPipeId;
	UCHAR		BulkOutPipeId;
}
VCUSB_DEVICE, *PVCUSB_DEVICE;

extern "C"
{
ULONG
VCUSBInitDevNotify();

VOID
VCUSBUninitDevNotify();

VOID
VCUSBDetectConnectedDevices();

HDEVNOTIFY
VCUSBRegDevNotification(
	IN HANDLE	hDevice);

VOID
VCUSBDevNotifyThreadProc(
	PVOID pContext);

LRESULT CALLBACK 
WinProc(	
	IN HWND hWnd,
    IN UINT uMsg,
    IN WPARAM wParam,
    IN LPARAM lParam);

BOOL 
OnDeviceChange(
	IN HWND hWnd, 
	IN WPARAM uEvent, 
    IN LPARAM dwEventData);

ULONG
VCUSBCreateDevice(
	IN PWCHAR	szwpPathName);

VOID
VCUSBFreeDeviceResources(
	IN PVCUSB_DEVICE	pDevice);

PVCUSB_DEVICE
VCUSBGetDevice();

VOID 
VCUSBFreeDevice();

HANDLE
VCUSBOpenDeviceHandleAsync(
	IN PWCHAR	szwpPathName);

VOID
VCUSBCloseDeviceHandle(
	IN HANDLE	hDevice);
}

#endif