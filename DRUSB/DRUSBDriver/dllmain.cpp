/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd. - All rights reserved.
**
** FILENAME: DLLMain.cpp
**
** PURPOSE:  This file contains the main DLL entry points. 
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <dbt.h>
#include <SetupApi.h>
#include <Windowsx.h>
#include <initguid.h>

#include "wudfusb.h"

#include "DRUSB.h"
#include "DRUsbUtils.h"
#include "DRUsbDevNotify.h"

BOOL 
APIENTRY DllMain( 
	IN HMODULE	hModule,
    IN DWORD	ul_reason_for_call,
    IN LPVOID	lpReserved)
{
	ULONG status;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

