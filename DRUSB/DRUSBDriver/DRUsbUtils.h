/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd . - All rights reserved.
**
** FILENAME: VCUSBUtils.h
**
** PURPOSE:  This file contains the prototypes and constants used through
**			 out the dll. 
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#ifndef VCUSBUTILS__H
#define VCUSBUTILS__H

VOID
VCUSBDbgPrint(
	IN PWCHAR	szFormat,
	IN				...);

#endif