/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd . - All rights reserved.
**
** FILENAME: VCUSBUtils.cpp
**
** PURPOSE:  This file contains the utility functions that are used through
**			 out the DLL. These utilities include functions for displaying
**			 error messages. 
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#include <Windows.h>
#include <WCHAR.h>

/****************************************************************************
**
** VCUSBDbgPrint
** PURPOSE: 
** 
**	This function will format a string and prints it to the debugger. It takes
**	a variable number of arguments.
**
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID
VCUSBDbgPrint(
	IN PWCHAR	szFormat,
	IN				...)
{
	static TCHAR	szDbgString[MAX_PATH];

	// Initialize the arg pointer to the start of the variable parameters
	va_list	varList;
	va_start(varList, szFormat);

	vswprintf_s(szDbgString, MAX_PATH, szFormat, varList);

	// Display the string
	OutputDebugString(szDbgString);
}