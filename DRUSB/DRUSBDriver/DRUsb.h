/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd. - All rights reserved.
**
** FILENAME: VCUSBDriverUsb.h
**
** PURPOSE:  This file contains the exported API function prototypes along
**			 will all constants and data types that can be used by an external
**			 application. 
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#ifndef VCUSB_DRIVER__H
#define VCUSB_DRIVER__H

// Constants
#define DLLAPI			__declspec(dllexport)
#define DLLCALL			__cdecl


// Data types

// Device Interface
DEFINE_GUID(GUID_DEVINTERFACE_VCUSBDRIVER, 
0x16163238, 0x4CE8, 0x4ba3, 0x99, 0x8D, 0x80, 0x50, 0x16, 0x57, 0x7F, 0x9F);

// Prototypes
extern "C"
{
DLLAPI UINT DLLCALL
VCUSBInit();

DLLAPI UINT DLLCALL
VCUSBCheck();

DLLAPI VOID DLLCALL
VCUSBDone();

DLLAPI UINT DLLCALL
VCUSBWrite(
	IN PVOID	pBuffer, 
	IN ULONG	BufferLength,
	IN PULONG	pBytesWritten,
	IN ULONG	Timeout);

DLLAPI UINT DLLCALL
VCUSBRead(
	IN PVOID	pBuffer, 
	IN ULONG	BufferLength,
	IN PULONG	pBytesRead,
	IN ULONG	Timeout);
}
#endif