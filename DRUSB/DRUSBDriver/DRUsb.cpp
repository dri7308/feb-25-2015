/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd. - All rights reserved.
**
** FILENAME: VCUsb.cpp
**
** PURPOSE:  This file contains the exported API functionality. It contains
**			 all of the functions that the application may call to gain access
**			 to the device. This functionality includes: inttializing the device,
**			 uninitializing the device, reading from and write to the device. 
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#include <Windows.h>
#include <initguid.h>
#include <assert.h>

#include "wudfusb.h"

#include "DRUsbDevNotify.h"
#include "DRUsbUtils.h"
#include "DRUsb.h"

// Externs

BOOL	glbDevInit = FALSE;
/****************************************************************************
**
** VCUSBInit
**	
** PURPOSE: 
** 
** This function is called at the very beginning of a session. It will initialize
** the device notification thread. 
**
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/	
DLLAPI UINT DLLCALL
VCUSBInit()
{
	ULONG status;

#ifdef DISPLAY_DBG_MSG
	MessageBox(NULL, L"VCUSBInit: Enter", L"Debug", MB_OK);
#endif

#ifndef NO_DEVNOTIFY
	if (glbDevInit == FALSE)
	{
		VCUSBDbgPrint(L"VCUSBInit: Init Device.\n");

		status = VCUSBInitDevNotify();
		if (status != ERROR_SUCCESS)
		{
			VCUSBDbgPrint(L"VCUSBInit: Failed to init the device thread. Status: %i.\n",
							status);
			return status;
		}
		glbDevInit = TRUE;
	}
#endif

	// Check to see if a device is present
	PVCUSB_DEVICE	pDevice;
	pDevice = VCUSBGetDevice();

	if (pDevice == NULL)
	{
		VCUSBDbgPrint(L"VCUSBInit: Failed to get the device.\n");
		return ERROR_NOT_READY;
	}

	VCUSBFreeDevice();
	return S_OK;
}


/****************************************************************************
**
** VCUSBCheck
**	
** PURPOSE: 
** 
** This function used to check for the presense of the USB device. It will 
** just get the device and then free it again.
**
** NOTES:   
**
*****************************************************************************/
DLLAPI UINT DLLCALL
VCUSBCheck()
{
	ULONG status;

	// Check to see if a device is present
	PVCUSB_DEVICE	pDevice;
	pDevice = VCUSBGetDevice();

	if (pDevice == NULL)
	{
		VCUSBDbgPrint(L"VCUSBCheck: Failed to get the device.\n");
		return ERROR_NOT_READY;
	}

	VCUSBFreeDevice();
	return S_OK;
}

/****************************************************************************
**
** VCUSBDone
**	
** PURPOSE: 
** 
** This function is the anti-VCUSBInit. It will terminate the device 
** thread to ensure a neat cleanup.
**
** NOTES:   
**
*****************************************************************************/
DLLAPI VOID DLLCALL
VCUSBDone()
{

#ifdef DISPLAY_DBG_MSG
	MessageBox(NULL, L"VCUSBDone: Enter", L"Debug", MB_OK);
#endif	

#ifdef NO_DEVNOTIFY
	if (glbDevInit == FALSE)
	{
		return;
	}
#endif
	
	// Clean up
	VCUSBUninitDevNotify();
	glbDevInit = FALSE;
}

/****************************************************************************
**
** VCUSBWrite
**	
** PURPOSE: 
** 
** This function will perform a bulk out data transfer to the connected 
** devcice. Before performing the transfer, a timeout value will be set.
**
** NOTES:   
**
*****************************************************************************/
DLLAPI UINT DLLCALL
VCUSBWrite(
	IN PVOID	pBuffer, 
	IN ULONG	BufferLength,
	IN PULONG	pBytesWritten,
	IN ULONG	Timeout)
{
#ifdef DISPLAY_DBG_MSG
	MessageBox(NULL, L"VCUSBWrite: Enter", L"Debug", MB_OK);
#endif
	ULONG			status;
	BOOL			success;

	// Initialize the return
	*pBytesWritten = 0;

	PVCUSB_DEVICE	pDevice;
	pDevice = VCUSBGetDevice();

	if (pDevice == NULL)
	{
		VCUSBDbgPrint(L"VCUSBWrite: Failed to get the device.\n");
		return ERROR_INVALID_HANDLE;
	}

	// Set the request timeout
	success = WinUsb_SetPipePolicy(	pDevice->hUsb, 
									pDevice->BulkOutPipeId, 
									PIPE_TRANSFER_TIMEOUT, 
									sizeof(ULONG), 
									(PVOID)&Timeout);
	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBWrite: Failed to set request timeout. Status: %d.\n",
						status);
		VCUSBFreeDevice();
		return status;
	}

	// Perform the write
	success = WinUsb_WritePipe(	pDevice->hUsb, 
								pDevice->BulkOutPipeId, 
								(PUCHAR)pBuffer, 
								BufferLength,
								pBytesWritten, 
								NULL);
	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBWrite: Failed to write the data. Status: %i.\n",
					status);

		VCUSBFreeDevice();
		return status;
	}

	assert(*pBytesWritten == BufferLength);

	// Release the semaphore on the device
	VCUSBFreeDevice();
	return S_OK;
}

/****************************************************************************
**
** VCUSBRead
**	
** PURPOSE: 
** 
** This function will perform a bulk in data transfer from the connected 
** device. Before performing the transfer, a timeout will be set.
**
** NOTES:   
**
*****************************************************************************/
DLLAPI UINT DLLCALL
VCUSBRead(
	IN PVOID	pBuffer, 
	IN ULONG	BufferLength,
	IN PULONG	pBytesRead,
	IN ULONG	Timeout)
{
	ULONG			status;
	BOOL			success;

	// Init the return
	*pBytesRead = 0;

#ifdef DISPLAY_DBG_MSG
	MessageBox(NULL, L"VCUSBRead: Enter", L"Debug", MB_OK);
#endif
	PVCUSB_DEVICE pDevice;
	pDevice = VCUSBGetDevice();

	if (pDevice == NULL)
	{
		VCUSBDbgPrint(L"VCUSBRead: Failed to get the device.\n");
		return ERROR_INVALID_HANDLE;
	}

	// Multiply the timeout by 10 - it just seems kind of short.
	Timeout *= 10;

	// Set the request timeout
	success = WinUsb_SetPipePolicy(	pDevice->hUsb, 
									pDevice->BulkInPipeId, 
									PIPE_TRANSFER_TIMEOUT, 
									sizeof(ULONG), 
									(PVOID)&Timeout);
	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBRead: Failed to set request timeout. Status: %d.\n",
						status);
		VCUSBFreeDevice();
		return status;
	}

	success = WinUsb_ReadPipe(	pDevice->hUsb, 
								pDevice->BulkInPipeId, 
								(PUCHAR)pBuffer, 
								BufferLength,
								pBytesRead, 
								NULL);
	if (success == FALSE)
	{
		status = GetLastError();

		if (status != ERROR_SEM_TIMEOUT)
		{
			VCUSBDbgPrint(L"VCUSBRead: Failed to read the data. Status: %i.\n",
						status);
		}
		VCUSBFreeDevice();
		return status;
	}

	// Release the semaphore on the device
	VCUSBFreeDevice();
	return S_OK;
}


