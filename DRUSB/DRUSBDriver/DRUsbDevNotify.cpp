/****************************************************************************
**
** Copyright 2010 Dynamic Ratings Pty Ltd . - All rights reserved.
**
** FILENAME: VCUSBDevNotify.cpp
**
** PURPOSE:  This file contains the routines required for process dynamic and
**			 static device arrival and removal.
**			
** NOTES:    
**           
** HISTORY:
**       $Log: $
**
****************************************************************************/
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <dbt.h>
#include <SetupApi.h>
#include <Windowsx.h>
#include <initguid.h>

#include "wudfusb.h"

#include "DRUsb.h"
#include "DRUsbDevNotify.h"
#include "DRUsbUtils.h"

// Globals that are shared between the thread and the main line code
HWND					glbhWnd			= NULL;
HANDLE					glbhThread		= NULL;
HANDLE					glbhSyncEvent	= NULL;
VCUSB_DEVICE			glbDevice;
CRITICAL_SECTION		glbDevCritSection;


/****************************************************************************
**
** VCUSBInitDevNotify
**	
** PURPOSE: This is the initialization function for the Device Notification 
**			functionality. It will initalize all of the globals, create
**			a thread that will process notification messages, and then it
**			search the registry for all devices currently connected to the 
**			system.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
ULONG
VCUSBInitDevNotify()
{
	ULONG status;

	// Create a Critical Section that will be used to protect
	// the Device array
	InitializeCriticalSection(&glbDevCritSection);

	// Create the Device Notification Thread. This is the thread that will 
	// receive notification messages when devices have been added or removed 
	// from the system.
	//
	// Create a synchronization event to ensure that the Device Notification
	// thread is setup before return.
	glbhSyncEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if (glbhSyncEvent == NULL)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBInitDevNotify: Failed to create the sync event. Status: %i.\n", 
				status);
		return status;
	}

	glbhThread = CreateThread(NULL, 
							0, 
							(LPTHREAD_START_ROUTINE)VCUSBDevNotifyThreadProc, 
							NULL,	// Context
							0, 
							NULL);
	if (glbhThread == NULL)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBInitDevNotify: Failed to create the device notification thread. Status: %i.\n", 
				status);
		return status;
	}

	// Wait on the sync. event
	status = WaitForSingleObject(glbhSyncEvent, 10000);
	if (status == WAIT_TIMEOUT)
	{
		VCUSBDbgPrint(L"VCUSBInitDevNotify: Failed waiting for the sync event.\n");
	}
	CloseHandle(glbhSyncEvent);

	return status;
}

/****************************************************************************
**
** VCUSBUninitDevNotify
**	
** PURPOSE: This routine is responsible for the orderly shutdown of the 
**			Device Notification thread. This routine will send a WM_CLOSE 
**			message to the thread and then wait for it to terminate.
** 
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID
VCUSBUninitDevNotify()
{
	ULONG status; 

	if (glbhWnd != NULL)
	{
		status = SendMessage(glbhWnd, WM_CLOSE, 0, NULL); 

		if (status != ERROR_SUCCESS)
		{
			VCUSBDbgPrint(L"VCUSBUninitDevNotify: Failed to send the close message to the thread. Status: %i.\n",
						status);
		}
	}

	if (glbhThread != NULL)
	{
		// Wait for the thread to terminate
		status = WaitForSingleObject(glbhThread, 10000);

		if (status != WAIT_OBJECT_0)
		{
			VCUSBDbgPrint(L"VCUSBUninitDevNotify: Thread did not terminate. Status: %i.\n",
						status);
		}

		CloseHandle(glbhThread);
	}

	// Free any resources that are associated with the device
	EnterCriticalSection(&glbDevCritSection);
		VCUSBFreeDeviceResources(&glbDevice);
	LeaveCriticalSection(&glbDevCritSection);

	DeleteCriticalSection(&glbDevCritSection);

	return;
}


//
// Private methods
//

/****************************************************************************
**
** VCUSBDetectConnectedDevices
**	
** PURPOSE: 
**	This routine will scan the registery looking for VCUSB Devices. 
**	Once a device is detected, we'll get the meta data, open a handle, and
**	create a Device Entry.
**
**	This routine will stop searching after it finds one device. 
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID
VCUSBDetectConnectedDevices()
{
	ULONG		status		= ERROR_SUCCESS;
	BOOL		success;
    HDEVINFO	hDevInfo	= INVALID_HANDLE_VALUE;

	// Initialize the device structure
	ZeroMemory(&glbDevice, sizeof(VCUSB_DEVICE));
	glbDevice.hDevice = INVALID_HANDLE_VALUE;

    // Start by opening an enumeration handle for the 
	// VCUSB Driver class GUID
    hDevInfo = SetupDiGetClassDevs((GUID*)&GUID_DEVINTERFACE_VCUSBDRIVER, 
                                       NULL,                      // No Enumerator
                                       NULL,                      // No parent hWnd
                                       DIGCF_PRESENT | 
                                       DIGCF_INTERFACEDEVICE);   
    if (hDevInfo == INVALID_HANDLE_VALUE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: Failed to open a handle to the class key. Status: %i.\n",
				status);
		return;
    }

	// Loop through and enumerate all of the devices in device info.
	SP_INTERFACE_DEVICE_DATA ifDevData;
    ifDevData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);

	SP_DEVINFO_DATA devInfoData;
	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	PSP_INTERFACE_DEVICE_DETAIL_DATA pDevDetailData = NULL;

    ULONG		sizeRequired	= 0;
	ULONG		devIndex		= 0;
	while (1)
	{
		success = SetupDiEnumDeviceInterfaces(	hDevInfo, 
												NULL,
												(GUID*)&GUID_DEVINTERFACE_VCUSBDRIVER,
												devIndex++,
												&ifDevData);
		if (success == FALSE)
		{
			status = GetLastError();
			if (status != ERROR_NO_MORE_ITEMS)
			{
				VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: Failed interface enumeration. Status: %i.\n",
						status);
			}
			break;
		}

		// Get the actual size of the device interface details
		success = SetupDiGetDeviceInterfaceDetail(hDevInfo,
												&ifDevData,
												NULL,
												0,
												&sizeRequired,
												NULL);
		if (success == FALSE)
        {
			status = GetLastError();
            if (status != ERROR_INSUFFICIENT_BUFFER)
            {
				status = GetLastError();
				VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: SetupDiGetDeviceInterfaceDetail Failed. Status: %i.\n",
						status);
				continue;
            }
		}

		// Using the size just returned, allocate a buffer for the details
		pDevDetailData = (PSP_INTERFACE_DEVICE_DETAIL_DATA)new UCHAR[sizeRequired];
        if (pDevDetailData == NULL)
        {
			VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: Failed to alloc buffer of IF Details data.\n");
			continue;
        }

		pDevDetailData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

		//  Get the Device Interface details
		success = SetupDiGetDeviceInterfaceDetail(hDevInfo,
											&ifDevData,
											pDevDetailData,
											sizeRequired,
											NULL,
											&devInfoData);
		if (success == FALSE)
		{
			status = GetLastError();
			VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: SetupDiGetDeviceInterfaceDetail failed. Status: %i.\n",
					status);

			// Free the Device Detail data
			delete [] pDevDetailData;
			continue;
        }

		// The device was found
		//		
		// Create a PVCUSB_DEVICE Device entry
		status = VCUSBCreateDevice(pDevDetailData->DevicePath);

		if (status != ERROR_SUCCESS)
		{
			VCUSBDbgPrint(L"VCUSBDetectConnectedDevice: Failed to create the Device. Status: %i.\n",
						status);
			delete [] pDevDetailData;
			continue;
		}

#ifndef NO_DEVNOTIFY
		// Free the device 
		VCUSBFreeDevice();
#endif
		delete [] pDevDetailData;
		break;
	}

	SetupDiDestroyDeviceInfoList(hDevInfo);
}

/****************************************************************************
**
** VCUSBRegisterDeviceNotification
**	
** PURPOSE: 
**	This function is used to register a device handle so that we will
**	receive notification on removal.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
HDEVNOTIFY
VCUSBRegDevNotification(
	IN HANDLE	hDevice)
{
	ULONG status;

	// Be sure that the windows handle is not NULL
	if (glbhWnd == NULL)
	{
		VCUSBDbgPrint(L"VCUSBRegisterDeviceNotification: Windows handle is NULL. Can't register for device notification.\n");
		return NULL;
	}

	//  Register for handle specific device notficiation - removal.
	DEV_BROADCAST_HANDLE	notifyFilter;
	HDEVNOTIFY				hNotify;	
	ZeroMemory(&notifyFilter, sizeof(notifyFilter));
	notifyFilter.dbch_size = sizeof(notifyFilter);
	notifyFilter.dbch_devicetype = DBT_DEVTYP_HANDLE;
	notifyFilter.dbch_handle = hDevice;
	hNotify = RegisterDeviceNotification(glbhWnd, 
                                         &notifyFilter, 
                                         DEVICE_NOTIFY_WINDOW_HANDLE);
	if (hNotify == NULL)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBRegisterDeviceNotification: Failed to register. Status: %i.\n",
				status);
		return NULL;
	}

	return hNotify;
}

/****************************************************************************
**
** VCUSBDevNotifyThreadProc
**	
** PURPOSE: 
**	This is the main procedure for the Device Notify thread. It 
**	creates a window, registers for notification and then creates
**	a message pump to allow messages to be processed by the window.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID
VCUSBDevNotifyThreadProc(
	PVOID pContext)
{
	ULONG		status = ERROR_SUCCESS;
	MSG			msg;
	BOOL		success;

	assert(pContext == NULL);

	// Create a hidden window so that we can receive Windows messages
	glbhWnd = CreateWindowEx(WS_EX_TOPMOST,	// Extended window style
							L"#32769",					// Class name - Desktop
							L"VCUSB Device Detection",		// Window name
							WS_POPUP,					// Window style
							0,							// X coord
							0,							// Y coord
							100,						// Width
							100,						// Height
							NULL,						// Handle to parent
							NULL,						// Handle to menu
							NULL,						// Handle to instance
							NULL);						// Create parameter
	if (glbhWnd == NULL) 
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBDevNotifyThreadProc: Failed to create message window. Status: %i.\n",
				status);
		ExitThread(0);
	}

	// Set the entry point of our Windows process
	LONG_PTR pWinProc = (LONG_PTR)WinProc;
	SetWindowLongPtr(glbhWnd, GWLP_WNDPROC, (LONG)pWinProc);

	// Finalize the window initialization.
	ShowWindow(glbhWnd, SW_HIDE);
	UpdateWindow(glbhWnd);
	
	//  Register for interface notification.
	HDEVNOTIFY	hInterfaceNotify;
	DEV_BROADCAST_DEVICEINTERFACE notifyFilter;
	ZeroMemory(&notifyFilter, sizeof(DEV_BROADCAST_DEVICEINTERFACE));
	notifyFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	CopyMemory(&notifyFilter.dbcc_classguid, 
               &GUID_DEVINTERFACE_VCUSBDRIVER, 
               sizeof(GUID));

	notifyFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	hInterfaceNotify = RegisterDeviceNotification(glbhWnd, 
                                                  (PVOID)&notifyFilter, 
                                                   DEVICE_NOTIFY_WINDOW_HANDLE);
	if (hInterfaceNotify == NULL)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBDevNotifyThreadProc: Failed to register for device notification: %i.\n",
				status);
		
		ExitThread(0);
	}

	// Detect all connected devices
	VCUSBDetectConnectedDevices();

	// Signal that the device notification has been initialized
	SetEvent(glbhSyncEvent);

	// Implement a message pump
	while ((success = GetMessage(&msg, NULL, 0, 0)) != 0)
	{
		if (success == -1)
		{
			break;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// Unregister the device notification
	if (!UnregisterDeviceNotification(hInterfaceNotify))
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBDevNotifyThread: Failed to de-register device notification. Status: %i.\n",
				status);
	}
}

/****************************************************************************
**
** WinProc
**	
** PURPOSE: 
**	This is the main windows procedure that receives all all of the
**	windows messages. The message that we are most interested in is
**	the WM_DEVICECHANGE notification. This message is sent when a 
**	device either arrives or is removed from the system.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
LRESULT CALLBACK 
WinProc(	
	IN HWND hWnd,
    IN UINT uMsg,
    IN WPARAM wParam,
    IN LPARAM lParam)
{
	switch(uMsg)
    {
    case WM_DEVICECHANGE:
		OnDeviceChange(hWnd, wParam, lParam);
		return 0;
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	break;
	
    }
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

/****************************************************************************
**
** OnDeviceChange
**	
** PURPOSE: 
**	This is a utility function that is called to process the 
**	WM_DEVICECHANGE messages. When processing the message we'll look
**	for two specific conditions: device removal and device arrival. In 
**	either case, once an event is detected, we'll set the appropriate 
**	event to signal waiting thread.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
BOOL 
OnDeviceChange(
	IN HWND hWnd, 
	IN WPARAM uEvent, 
    IN LPARAM dwEventData)
{
	UNREFERENCED_PARAMETER(hWnd);

	ULONG							status				= ERROR_SUCCESS;
    PDEV_BROADCAST_DEVICEINTERFACE	pDeviceInterface	= NULL;
    PDEV_BROADCAST_HANDLE			pDeviceHandle		= NULL;
    PDEV_BROADCAST_HDR				pBroadCastHdr		= NULL;
	PVCUSB_DEVICE					pDevice;

    //  First, cast the event data to type broadcast header. 
    //  The event data always contains a broadcast header -- regardless 
	//  of the actaul type of event.
	pBroadCastHdr = (PDEV_BROADCAST_HDR)dwEventData;

	if (pBroadCastHdr == NULL)
	{
		return TRUE;
	}

	// Route using the device type
	if (pBroadCastHdr->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
	{
		// Re-cast the event data to a device interface structure.
		pDeviceInterface = (PDEV_BROADCAST_DEVICEINTERFACE)dwEventData;

		switch (uEvent)
        {
        case DBT_DEVICEARRIVAL:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEARRIVAL (Interface)): A device is being added to the system. Device: %s.\n",
					pDeviceInterface->dbcc_name);

			// Check to see if we already have a device
			pDevice = VCUSBGetDevice();

			if (pDevice != NULL)
			{
				// A device has already been configured for this system. 
				// We only allow one device to be connected at a time. Just ignore this one
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEARRIVAL (Interface)): A device has already been configured. Device: %s.\n",
								pDeviceInterface->dbcc_name);
				VCUSBFreeDevice();
				break;
			}

			// A new device has arrived. Create an entry
			status = VCUSBCreateDevice(pDeviceInterface->dbcc_name);

			if (status != ERROR_SUCCESS)
			{
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEARRIVAL (Interface)): Failed to create the device entry.\n");
				break;
			}
			
			VCUSBFreeDevice();
			break;

		case DBT_DEVICEREMOVECOMPLETE:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVECOMPLETE (Interface)): A device has been removed. Device: %s\n",
					pDeviceInterface->dbcc_name);

			pDevice = VCUSBGetDevice();

			if (pDevice == NULL)
			{
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVECOMPLETE (Interface)): Could not find an entry for the device.\n");
				break;
			}

			if (wcscmp(pDevice->szwpDevPath, pDeviceInterface->dbcc_name) == 0)
			{
				VCUSBFreeDeviceResources(pDevice);
			}
			else
			{
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVECOMPLETE (Interface)): Device not connected. Device: %s\n",
				pDeviceInterface->dbcc_name);
			}
			VCUSBFreeDevice();
			break;

		default:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVTYP_DEVICEINTERFACE): Unsupported event. Event = %i.\n",
					uEvent);
			break;
	    }
	}
	else if (pBroadCastHdr->dbch_devicetype == DBT_DEVTYP_HANDLE)
    {
		// Re-cast the event data to a device handle structure.
		pDeviceHandle = (PDEV_BROADCAST_HANDLE)dwEventData;

        switch (uEvent)
        {
		case DBT_DEVICEQUERYREMOVE:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEQUERYREMOVE (Handle)): Being queried to remove a device.\n");

			pDevice = VCUSBGetDevice();

			if (pDevice == NULL)
			{
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEQUERYREMOVE (Handle)): Could not find an entry.\n");
				assert(0);
				break;
			}

			VCUSBFreeDeviceResources(pDevice);
			VCUSBFreeDevice();
			break;

		case DBT_DEVICEREMOVEPENDING:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVEPENDING (Handle)): Device Remove Pending\n");
			break;

      	case DBT_DEVICEREMOVECOMPLETE:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVECOMPLETE (Handle)): Device Remove complete\n");

			pDevice = VCUSBGetDevice();
			if (pDevice == NULL)
			{
				VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEREMOVECOMPLETE (Handle)): Could not find an entry.\n");
				assert(0);
				break;
			}
			
			VCUSBFreeDeviceResources(pDevice);
			VCUSBFreeDevice();
			break;
		
		case DBT_DEVICEQUERYREMOVEFAILED:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVICEQUERYREMOVEFAILED (Handle)): Received Device Query Remove Failed message\n");
			break;

		default:
			VCUSBDbgPrint(L"OnDeviceChange (DBT_DEVTYP_HANDLE): Unsupported event. Event = %x\n",
					uEvent);
			break;
        }
    }
    return TRUE;
}

/****************************************************************************
**
** VCUSBCreateDevice
**	
** PURPOSE: 
** This is a utility function used to create a device entry. This will include:
** Creating a Device Entry, opening a device handle, and registering for device 
** notification.
**
** RETURNS: 
**          
** NOTES:   
** If the device entry is successfully created, this function will exit WITH
** THE CRITICAL SECTION BEING HELD. It is the responsiblity of the caller to
** drop the critical section when finished with the device entry.
**
*****************************************************************************/
ULONG
VCUSBCreateDevice(
	IN PWCHAR	szwpPathName)
{
	ULONG	status;
	BOOL	success;

	// Open a handle to the device
	HANDLE hDevice;
	hDevice = VCUSBOpenDeviceHandleAsync(szwpPathName);

	if (hDevice == INVALID_HANDLE_VALUE)
	{
		VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to open handle to the device.\n");
		return ERROR_INVALID_HANDLE;
	}
		
#ifndef NO_DEVNOTIFY
	EnterCriticalSection(&glbDevCritSection);
#endif

	// Save the device handle
	glbDevice.hDevice = hDevice;
			
	// Get a handle to the device interface. We'll use this handle when
	// issuing requests to the device
	success = WinUsb_Initialize(glbDevice.hDevice, 
								&glbDevice.hUsb);
	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to open to the device interface. Status: %i.\n",
					status);
				
		VCUSBFreeDeviceResources(&glbDevice);
		LeaveCriticalSection(&glbDevCritSection);
		return status;
	}

	// Query for the Interface/endpoint descriptors
	USB_INTERFACE_DESCRIPTOR  interfaceDescriptor;
	success = WinUsb_QueryInterfaceSettings(glbDevice.hUsb, 
											0, 
											&interfaceDescriptor);
	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to query interface descriptors. Status: %i.\n",
					status);
					
		VCUSBFreeDeviceResources(&glbDevice);
		LeaveCriticalSection(&glbDevCritSection);
		return status;
	}
    
	// Get the endpoint IDs
	WINUSB_PIPE_INFORMATION pipeInfo;
	for (UCHAR endPointIndex = 0; 
			 endPointIndex < interfaceDescriptor.bNumEndpoints; 
														++endPointIndex)
	{
		success = WinUsb_QueryPipe(glbDevice.hUsb, 
									0, 
									endPointIndex, 
									&pipeInfo);
		if (success == FALSE)
		{
			status = GetLastError();
			VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to query pipe: %i. Status: %i.\n",
						endPointIndex, status);
			VCUSBFreeDeviceResources(&glbDevice);
			LeaveCriticalSection(&glbDevCritSection);
			return status;
		}

		assert(pipeInfo.PipeType == UsbdPipeTypeBulk);
		if (USB_ENDPOINT_DIRECTION_IN(pipeInfo.PipeId) != 0)
		{
			glbDevice.BulkInPipeId = pipeInfo.PipeId;
		}
		else
		{
			glbDevice.BulkOutPipeId = pipeInfo.PipeId;
		}
	}

	// Verify that all the endpoints were reported in the descriptor
	BOOL endPointsOk = TRUE;
	if (glbDevice.BulkOutPipeId == NULL)
	{
		VCUSBDbgPrint(L"VCUSBCreateDevice: No bulk out endpoint reported.\n");
		endPointsOk = FALSE;
	}

	if (glbDevice.BulkInPipeId == NULL)
	{
		VCUSBDbgPrint(L"VCUSBCreateDevice: No bulk in endpoint reported.\n");
		endPointsOk = FALSE;
	}

	if (endPointsOk == FALSE)
	{
		VCUSBFreeDeviceResources(&glbDevice);
		LeaveCriticalSection(&glbDevCritSection);
		return ERROR_INVALID_DRIVE;
	}
			
	// Save the device path
	glbDevice.szwpDevPath = new TCHAR[wcslen(szwpPathName) + 1];
	if (glbDevice.szwpDevPath == NULL)
	{
		VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to allocate the Device Path.\n");
				
		VCUSBFreeDeviceResources(&glbDevice);
		LeaveCriticalSection(&glbDevCritSection);
		return ERROR_OUTOFMEMORY;
	}

	wcscpy_s(	&glbDevice.szwpDevPath[0], 
				wcslen(szwpPathName) + 1, 
				szwpPathName);
	glbDevice.szwpDevPath[wcslen(szwpPathName)] = 0;

#ifdef NO_DEVNOTIFY
	glbDevice.hDevNotify = NULL;
#else
	// Register for device notification	
	glbDevice.hDevNotify = VCUSBRegDevNotification(glbDevice.hDevice);
	if (glbDevice.hDevNotify == NULL)
	{
		VCUSBDbgPrint(L"VCUSBCreateDevice: Failed to register for device notification.\n");

		VCUSBFreeDeviceResources(&glbDevice);
		LeaveCriticalSection(&glbDevCritSection);
		return ERROR_INVALID_HANDLE;
	}
#endif

	return ERROR_SUCCESS;
}

/****************************************************************************
**
** VCUSBFreeDeviceResources
**	
** PURPOSE: 
** This is a utility function used to delete a device entry. It will close all
** the handles and delete all of the memory associated with the device entry.
**
** RETURNS: 
**          
** NOTES:   
** It is assumed that this function is called with the glbDevCritSection 
** being held. 
*****************************************************************************/
VOID
VCUSBFreeDeviceResources(
	IN PVCUSB_DEVICE	pDevice)
{
	if (pDevice->szwpDevPath != NULL)
	{
		delete [] pDevice->szwpDevPath;
		pDevice->szwpDevPath = NULL;
	}

	if (pDevice->hDevNotify != NULL)
	{
		UnregisterDeviceNotification(pDevice->hDevNotify);
		pDevice->hDevNotify = NULL;
	}

	if (pDevice->hUsb != 0)
	{
		WinUsb_Free(pDevice->hUsb);
		pDevice->hUsb = 0;
	}

	if (pDevice->hDevice != INVALID_HANDLE_VALUE)
	{
		VCUSBCloseDeviceHandle(pDevice->hDevice);
		pDevice->hDevice = INVALID_HANDLE_VALUE;
	}
}

/****************************************************************************
**
** VCUSBGetDevice
**	
** PURPOSE: 
** This is a utility function used to get the device entry for the one and
** only device in the system. 
**
** RETURNS: 
**          
** NOTES:  
** In the case of ASync Notification Enabled:
** If the device entry is successfully found, this function will exit with
** the critical section being held. It is the responsiblity of the caller to
** drop the critical section when finished with the device entry.
**
** In the case of Async Notification is Disabled:
** This function will exit with an open handle to the device.
*****************************************************************************/
PVCUSB_DEVICE
VCUSBGetDevice()
{
	PVCUSB_DEVICE	pDevice;

#ifdef NO_DEVNOTIFY
	VCUSBDetectConnectedDevices();

	if (glbDevice.hDevice == INVALID_HANDLE_VALUE)
	{
		VCUSBDbgPrint(L"VCUSBGetDevice: Handle not opened.\n");
		return NULL;
	}
#else
	EnterCriticalSection(&glbDevCritSection);

	if (glbDevice.hDevice == INVALID_HANDLE_VALUE)
	{
		// The device is not valid
		LeaveCriticalSection(&glbDevCritSection);
		return NULL;
	}
#endif
	return &glbDevice;
}

/****************************************************************************
**
** VCUSBFreeDevice
**	
** PURPOSE: This method is used to free the critical section that was taken 
**			when a device item was referenced.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID 
VCUSBFreeDevice()
{
#ifdef NO_DEVNOTIFY
	if (glbDevice.hDevice == INVALID_HANDLE_VALUE)
	{
		VCUSBDbgPrint(L"VCUSBFreeDevice: Handle not opened.\n");
	}
	VCUSBFreeDeviceResources(&glbDevice);
#else
	LeaveCriticalSection(&glbDevCritSection);
#endif
}


/****************************************************************************
**
** VCUSBOpenDeviceHandleAsync
**	
** PURPOSE: 
** This is a utility function used to open a handle to an attached 
** device. This handle can be used for asynchronous IO only.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
HANDLE
VCUSBOpenDeviceHandleAsync(
	IN PWCHAR	szwpPathName)
{
	ULONG status;
	HANDLE	hDevice;

	hDevice = CreateFile(&szwpPathName[0], 
						GENERIC_READ | GENERIC_WRITE, 
						FILE_SHARE_READ | FILE_SHARE_WRITE, 
						NULL, 
						OPEN_EXISTING, 
						FILE_FLAG_OVERLAPPED,				// Flags/Attributes
						NULL);
	if (hDevice == INVALID_HANDLE_VALUE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBOpenDeviceHandleAsync: Failed to open handle to the device. Status: %i.\n",
				status);

		return INVALID_HANDLE_VALUE;
	}

	return hDevice;
}

/****************************************************************************
**
** VCUSBCloseDeviceHandle
**	
** PURPOSE: 
** This is a utility function used to close a device handle. This function
** simply encapsulates the Win32 call.
** 
** RETURNS: 
**          
** NOTES:   
**
*****************************************************************************/
VOID
VCUSBCloseDeviceHandle(
	IN HANDLE	hDevice)
{
	BOOL	success;
	ULONG	status;

	success = CloseHandle(hDevice);

	if (success == FALSE)
	{
		status = GetLastError();
		VCUSBDbgPrint(L"VCUSBCloseDeviceHandle: Failed to close the handle to the device. Status: %i.\n",
				status);
	}
}



