﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;

namespace DMLoader
{
    public partial class MainDisplay : Telerik.WinControls.UI.RadForm
    {
        #region DLL Import

        [DllImport("drusbdll.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 VCUSBCheck();

        [DllImport("drusbdll.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 VCUSBDone();

        [DllImport("drusbdll.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 VCUSBInit();

        [DllImport("drusbdll.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        //public static extern UInt64 VCUSBRead(Byte[] pBuffer, UInt32 cbBuffer, ref UInt32 cbRead, UInt32 uTimeout);
        public static extern UInt32 VCUSBRead(IntPtr pBuffer, UInt32 cbBuffer, ref UInt32 cbRead, UInt32 uTimeout);

        [DllImport("drusbdll.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        //public static extern UInt32 VCUSBWrite(Byte[] pBuffer, UInt32 cbBuffer, ref UInt32 cbWritten, UInt32 uTimeout);
        public static extern UInt32 VCUSBWrite(IntPtr pBuffer, UInt32 cbBuffer, ref UInt32 cbWritten, UInt32 uTimeout);

        #endregion

        private BackgroundWorker firmwareLoadBackgroundWorker;

        const UInt32 S_OK = 0;

        bool continueBootloadProcess = false;

        bool logBootloadProcess = false;

        Byte[] okSignature;
        UInt64 okSignatureLength;

        StartSignature startSignature;
        UInt64 startSignatureLength;

        //  UInt64 timeOut;

        Byte[] allBytesReadFromInputFile;
        Byte[] allBytesToBeWrittenToTheDevice;

        private int totalNumberOfBytesToWriteToDevice;

        string firmwareFileName;

        private static string incorrectFileTypeText = "Incorrect file type";
        private static string fileChecksumIncorrectText = "The computed file checksum does not match the one saved in the file";
        private static string fileContainedIncorrectMagicNumber = "File contained incorrect magic number";
        private static string firmwareFileIncorrect = "The firmware file had incorrect values or format";
        private static string firmwareFileSizeExceededProgramCapacity = "The size of the firmware file is greather than can be handled by this program";
        private static string failedToWriteTheBootblockToTheDevice = "Failed to write the bootblock to the device.";
        private static string failedToWriteTheCurrentBlockToTheDevice = "Failed to write the current block to the device";
        private static string startFirmwareUploadText = "Start";
        private static string stopFirmwareUploadText = "Stop";
        private static string fileNotSpecifiedText = "No firmware file has been selected";
        private static string firmwareLoadCancelledText = "Firmware load cancelled";
        private static string reportedFirmwareDeviceTypeText = "Reported firmware device type was";
        private static string firmwareLoadSucceededText = "Firmware load succeeded";
        private static string waitingText = "Waiting...";
        private static string helloText = "Hello!";
        // private static string errorText = "Error";
        private static string areYouSureYouWantToCancelText = "Are you sure you want to cancel the firmware upload?  You could leave the device in a non-operational state.";
        private static string cancellationMayTakeAFewSecondsText = "Cancellation may take a few seconds to complete";

        private static string noOperationPendingText = "No operation pending";
        private static string deviceTypeFromFirmwareUnknownText = "The device type reported in the firmware is unknown at this time";
        // private static string nothingLoadingYetText = "Nothing loading yet";

        private string preferencesFileName = "preferences.drd";
        private string preferencesFileWithFullPath;
        private string applicationDirectory;

        private string initialFirmwareDirectoryKeyName = "InitialFirmwareDirectory";
        private string initialFirmwareDirectory;

        private string errorLogfileNameText = "error_log.txt";
        private string notepadExeText = "notepad.exe";
        private string executablePath;

        public MainDisplay()
        {
            InitializeComponent();

            InitializeOkSignature();
            InitializeStartSignature();
        }

        private void MainDisplay_Load(object sender, EventArgs e)
        {
            executablePath = Path.GetDirectoryName(Application.ExecutablePath);
#if DEBUG
            applicationDirectory = Path.GetDirectoryName(Application.ExecutablePath);
#else
            applicationDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            applicationDirectory = Path.Combine(applicationDirectory, "Dynamic Ratings", "DM Loader");
#endif
            preferencesFileWithFullPath = Path.Combine(applicationDirectory, preferencesFileName);

            FileUtilities.MakeFullPathToDirectory(applicationDirectory);
            LogMessage.SetErrorDirectory(applicationDirectory);
            LogMessage.SetErrorFileName(errorLogfileNameText);

            startStopProcessRadButton.Text = startFirmwareUploadText;
            InitializeFirmwareLoadBackgroundWorker();

            this.Text = "DM Loader - v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            this.firmwareUploadRadProgressBar.Visible = false;
            this.firmwareUploadRadWaitingBar.Visible = false;

            SetUpMainMenu();
            SetInitialPreferences();
            ReadPreferences();
        }

        private void MainDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            SavePreferences();
        }

        public void InitializeOkSignature()
        {
            try
            {
                okSignature = new byte[64];
                okSignatureLength = 64;

                okSignature[0] = (byte)'V';
                okSignature[1] = (byte)'C';
                okSignature[2] = (byte)'7';
                okSignature[3] = (byte)'O';
                okSignature[4] = (byte)'x';
                okSignature[5] = (byte)'0';
                okSignature[6] = (byte)'1';
                for (int i = 7; i < 64; i++)
                {
                    okSignature[i] = (byte)'\0';
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeOkSignature()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void InitializeStartSignature()
        {
            try
            {
                startSignature = new StartSignature();

                startSignatureLength = 64;

                startSignature.signature[0] = (byte)'V';
                startSignature.signature[1] = (byte)'C';
                startSignature.signature[2] = (byte)'7';
                startSignature.signature[3] = (byte)'S';
                startSignature.signature[4] = (byte)'x';
                startSignature.signature[5] = (byte)'0';
                startSignature.signature[6] = (byte)'1';
                startSignature.signature[7] = (byte)'\0';
                startSignature.blockAddr = 0;
                startSignature.blockLen = 0;
                startSignature.fullLen = 0;
                startSignature.option = 0;
                startSignature.crcProg = 0;
                startSignature.crcHdr = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeStartSignature()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// This initiates all acts that occur when the user presses the start button
        /// </summary>
        private void MainEventLoop()
        {
            try
            {
                byte deviceType;
                int startingOffsetOfBytesBeingWrittenToDevice = 0;
                int completeFirmwareLoadCount = 0;
                int completeFirmwareLoadRetryLimit = 5;
                int individualBlockWriteCount = 0;
                int individualBlockWriteLimit = 100000;
                bool continueWriteloaderLoop;

                int numberOfChunksWritten;
                int totalNumberOfChunksToWrite;

                byte[] currentBytesBeingWritten = new byte[8192];
                int numberOfCurrentBytesBeingWritten;

                byte[] signatureBytesFromDevice = new byte[64];

                UInt32 result;

                byte[] signatureBytesWrittenToDevice;


                this.Invoke(new MethodInvoker(() => completeFirmwareLoadRetryLimit = (Int32)numberOfRetrieslRadSpinEditor.Value));

                if (this.logBootloadProcess)
                {
                    LogMessage.LogError("In MainEventLoop - getting started");
                }

                /// Start doing stuff
                continueBootloadProcess = true;
                ReadFirmwareFile();
                InitializeBytesToWriteToDevice();
                if (continueBootloadProcess)
                {
                    /// We need to copy over rather than assign because we can possibly change how many bytes we are going to write
                    /// later in the code
                    if (this.firmwareFileName.ToLower().Contains("testprj.sim"))
                    {
                        this.totalNumberOfBytesToWriteToDevice = this.allBytesReadFromInputFile.Length;
                        Array.Copy(this.allBytesReadFromInputFile, 0, this.allBytesToBeWrittenToTheDevice, 0, (Int64)this.totalNumberOfBytesToWriteToDevice);
                    }
                    else if (this.firmwareFileName.ToLower().Contains("sim"))
                    {
                        // this function assigns the values to bytesToWriteToDevice and totalNumberOfBytesToWriteToDevice
                        ProcessFirmwareFile();
                        if (this.totalNumberOfBytesToWriteToDevice == 0)
                        {
                            RadMessageBox.Show(firmwareFileIncorrect);
                            this.continueBootloadProcess = false;
                        }
                    }
                    else if (this.firmwareFileName.ToLower().Contains("a79"))
                    {
                        this.totalNumberOfBytesToWriteToDevice = this.allBytesReadFromInputFile.Length;
                        Array.Copy(this.allBytesReadFromInputFile, 0, this.allBytesToBeWrittenToTheDevice, 0, (Int64)this.totalNumberOfBytesToWriteToDevice);
                    }
                    else
                    {
                        string errorMessage = "Error in MainDisplay.MainEventLoop()\nShould have filtered out the firmware file name before getting to this point";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }

                    if (this.logBootloadProcess)
                    {
                        LogMessage.LogError("In MainEventLoop - firmware file read successfully");
                    }

                    /// this boolean can be set to false in the ProcessFirmwareFile function
                    if (this.continueBootloadProcess)
                    {
                        /// adjust the fullLen to be a multiple of 8192.
                        this.startSignature.fullLen = this.totalNumberOfBytesToWriteToDevice;
                        if ((this.startSignature.fullLen % 8192) > 0)
                        {
                            this.startSignature.fullLen = ((this.startSignature.fullLen / 8192) + 1) * 8192;
                            if (this.startSignature.fullLen > this.allBytesToBeWrittenToTheDevice.Length)
                            {
                                RadMessageBox.Show(firmwareFileSizeExceededProgramCapacity);
                                this.continueBootloadProcess = false;
                            }
                        }

                        totalNumberOfChunksToWrite = this.startSignature.fullLen / 8192;

                        /// This line and the few lines afterward are equivalent to the "FindSignature" function in the original code
                        deviceType = FindDeviceTypeFromSignature(this.allBytesToBeWrittenToTheDevice, this.startSignature.fullLen - 8);
                        if (deviceType != 0)
                        {
                            this.startSignature.signature[4] = deviceType;
                            this.okSignature[4] = deviceType;

                            if (reportedFirmwareDeviceTypeRadLabel.InvokeRequired)
                            {
                                this.Invoke(new MethodInvoker(() => reportedFirmwareDeviceTypeRadLabel.Text = reportedFirmwareDeviceTypeText + " " + GetDeviceTypeAsString(deviceType)));
                            }
                            else
                            {
                                reportedFirmwareDeviceTypeRadLabel.Text = reportedFirmwareDeviceTypeText + " " + GetDeviceTypeAsString(deviceType);
                            }
                            /// Here I'm trying to take some code that has labels and loops and a plethora of continue statements and turn it into something reasonable.  This is the part of
                            /// the code that starts at the "Again" label.
                            /// 
                            /// AGAIN:
                            /// 
                            /// I have changed how the code works.  I have added counters to time out the load if a block write fails, and a timer for how many times you attempt
                            /// a full firmware load.  The full firmware load is supposed to be triggered when a block write fails a given number of times.
                            while (this.continueBootloadProcess)
                            {
                                individualBlockWriteCount = 0;
                                numberOfChunksWritten = 0;
                                SetFirmwareUploadoadRadProgressBarValue(0);

                                /// these values are used in the call to "InitializeDeviceForFirmwareLoad()" a few lines down ...  too many global variables here.
                                this.startSignature.blockAddr = 0;
                                this.startSignature.blockLen = 0;
                                this.startSignature.option = 1;
                                this.startSignature.crcProg = CalculateCRC16(this.allBytesToBeWrittenToTheDevice, this.startSignature.fullLen);
                                /// You do this assignment to 0 because you are going to compute the CRC of startSignature, WHICH INCLUDES THIS VARIABLE.
                                this.startSignature.crcHdr = 0;
                                this.startSignature.crcHdr = CalculateCRC16(this.startSignature.GetByteEquivalentArrayOfData(), (Int32)this.startSignatureLength);

                                SetFirmwareLoadProgressValueRadLabelText(waitingText);

                                /// I think this is waiting for the user to power up the device.  This will chew up CPU time like nobody's business.
                                result = VCUSBInit();
                                while (this.continueBootloadProcess && (result != S_OK))
                                {
                                    result = VCUSBInit();
                                    // Application.DoEvents();
                                }

                                // Thread.Sleep(100);

                                if (this.continueBootloadProcess)
                                {
                                    SetFirmwareLoadProgressValueRadLabelText(helloText);

                                    if (this.logBootloadProcess)
                                    {
                                        LogMessage.LogError("In MainEventLoop - sending signature of the entire firmware - otherwise known as the Hello! function");
                                    }

                                    /// This is my equivalent of the SendHello function
                                    if (InitializeDeviceForFirmwareLoad())
                                    {
                                        if (this.logBootloadProcess)
                                        {
                                            LogMessage.LogError("In MainEventLoop - initialized device for firmware load");
                                        }
                                        if (this.startSignature.fullLen == 8192)
                                        {
                                            startingOffsetOfBytesBeingWrittenToDevice = 0;
                                        }
                                        else
                                        {
                                            startingOffsetOfBytesBeingWrittenToDevice = 8192;
                                        }

                                        /// WRITELOADER:     this label is in the original code, but I refuse to use labels

                                        continueWriteloaderLoop = true;
                                        /// Originally I followed the code as closely as possible, but the structure got stupid near then end, so I take care of the third
                                        /// stopping criteria internally
                                        //while (continueWriteloaderLoop &&  this.continueBootloadProcess && (startingOffsetOfBytesBeingWrittenToDevice< this.startSignature.fullLen))
                                        while (continueWriteloaderLoop && this.continueBootloadProcess)
                                        {
                                            if ((startingOffsetOfBytesBeingWrittenToDevice + 8192) < this.startSignature.fullLen)
                                            {
                                                numberOfCurrentBytesBeingWritten = 8192;
                                            }
                                            else
                                            {
                                                numberOfCurrentBytesBeingWritten = this.startSignature.fullLen - startingOffsetOfBytesBeingWrittenToDevice;
                                            }
                                            /// copy the current block of bytes being written to currentBytesBeingWritten
                                            Array.Copy(this.allBytesToBeWrittenToTheDevice, startingOffsetOfBytesBeingWrittenToDevice, currentBytesBeingWritten, 0, numberOfCurrentBytesBeingWritten);

                                            /// indicate to the user which block is being written, don't know how much this helps...
                                            SetFirmwareLoadProgressValueRadLabelText(startingOffsetOfBytesBeingWrittenToDevice.ToString());

                                            this.startSignature.blockAddr = startingOffsetOfBytesBeingWrittenToDevice;
                                            this.startSignature.blockLen = numberOfCurrentBytesBeingWritten;
                                            this.startSignature.crcProg = CalculateCRC16(currentBytesBeingWritten, numberOfCurrentBytesBeingWritten);
                                            /// You do this assignment to 0 because you are going to compute the CRC of startSignature, WHICH INCLUDES THIS VARIABLE.
                                            this.startSignature.crcHdr = 0;
                                            this.startSignature.crcHdr = CalculateCRC16(this.startSignature.GetByteEquivalentArrayOfData(), (Int32)this.startSignatureLength);
                                            Thread.Sleep(10);
                                            result = VCUSBInit();
                                            if (result == S_OK)
                                            {
                                                /// The original code does this, I guess it clears the buffer or something since the result does not matter
                                                ClearingRead(1, false);
                                                signatureBytesWrittenToDevice = this.startSignature.GetByteEquivalentArrayOfData();
                                                /// I believe this is telling the device what block of data to expect next
                                                if (WriteDataToTheDevice(signatureBytesWrittenToDevice, (Int32)startSignatureLength, 50, false))
                                                {
                                                    //Application.DoEvents();
                                                    Thread.Sleep(10);
                                                    result = VCUSBInit();
                                                    if (result == S_OK)
                                                    {
                                                        /// another read of the signature from the device that we don't really care about
                                                        ClearingRead(1, false);
                                                        //Application.DoEvents();

                                                        /// Hey, let's actually write a block of the firmware to the device!
                                                        if (WriteDataToTheDevice(currentBytesBeingWritten, numberOfCurrentBytesBeingWritten, 50, false))
                                                        {
                                                            // Application.DoEvents();
                                                            signatureBytesFromDevice = ReadDataFromTheDevice((Int32)this.okSignatureLength, 300, false);
                                                            if (signatureBytesFromDevice != null)
                                                            {
                                                                //Application.DoEvents();

                                                                if (VerifySignature(signatureBytesFromDevice, (UInt32)signatureBytesFromDevice.Length))
                                                                {
                                                                    /// Since this is a successful write, we reset the block retry counter to 0
                                                                    individualBlockWriteCount = 0;
                                                                    if (startingOffsetOfBytesBeingWrittenToDevice > 0)
                                                                    {
                                                                        if (this.logBootloadProcess)
                                                                        {
                                                                            LogMessage.LogError("In MainEventLoop - successfully wrote the block starting at " + startingOffsetOfBytesBeingWrittenToDevice.ToString());
                                                                        }
                                                                        startingOffsetOfBytesBeingWrittenToDevice += 8192;
                                                                        /// Okay, here is where I move some code around with respect to the original code.  They have labels and such to fall
                                                                        /// back on, and I won't do that.  In the original, they fall out of the structure I have defined for this loop, and use
                                                                        /// a label to get back in.  However, what they want to do at this point is to write the very first block of the firmware 
                                                                        /// to the device, so I will set it up to do that.
                                                                        if (startingOffsetOfBytesBeingWrittenToDevice >= this.startSignature.fullLen)
                                                                        {
                                                                            startingOffsetOfBytesBeingWrittenToDevice = 0;
                                                                        }
                                                                        numberOfChunksWritten++;
                                                                        SetFirmwareUploadoadRadProgressBarValue((numberOfChunksWritten + 1) * 100 / totalNumberOfChunksToWrite);
                                                                    }
                                                                    else
                                                                    {
                                                                        if (this.logBootloadProcess)
                                                                        {
                                                                            LogMessage.LogError("In MainEventLoop - successfully wrote the block starting at 0 - so we're done");
                                                                        }
                                                                        /// if we made it to this point successfully, we are finished loading the firmware
                                                                        this.continueBootloadProcess = false;
                                                                        SetFirmwareLoadProgressValueRadLabelText(firmwareLoadSucceededText);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    /// yet another failure point for the write, count it
                                                                    individualBlockWriteCount++;
                                                                    SetFirmwareLoadProgressValueRadLabelText(failedToWriteTheCurrentBlockToTheDevice);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                /// the device didn't respond properly, we'll consider this a write failure
                                                                individualBlockWriteCount++;
                                                                /// If this is true, we have just attempted to write the boot block and failed
                                                                //if (startingOffsetOfBytesBeingWrittenToDevice == 0)
                                                                //{                                                                    
                                                                //    if (completeFirmwareLoadCount > completeFirmwareLoadRetryLimit)
                                                                //    {
                                                                //        /// Exit the bootload code
                                                                //        this.continueBootloadProcess = false;
                                                                //        SetFirmwareLoadProgressValueRadLabelText(failedToWriteTheBootblockToTheDevice);
                                                                //        if (this.logBootloadProcess)
                                                                //        {
                                                                //            LogMessage.LogError("In MainEventLoop - final retry failed");
                                                                //        }
                                                                //    }
                                                                //    else
                                                                //    {
                                                                //        /// we need to reset all the counters and start over again
                                                                //        numberOfChunksWritten = 0;
                                                                //        if (this.startSignature.fullLen == 8192)
                                                                //        {
                                                                //            startingOffsetOfBytesBeingWrittenToDevice = 0;
                                                                //        }
                                                                //        else
                                                                //        {
                                                                //            startingOffsetOfBytesBeingWrittenToDevice = 8192;
                                                                //        }
                                                                //        if (this.logBootloadProcess)
                                                                //        {
                                                                //            LogMessage.LogError("In MainEventLoop - resetting the firmware load to start over again");
                                                                //        }
                                                                //    }
                                                                //}
                                                            }
                                                        }
                                                        else
                                                        {
                                                            /// if we are here, then the write failed
                                                            individualBlockWriteCount++;
                                                            /// There was a continue statement in the original code which would have migrated here in the translation
                                                            if (this.logBootloadProcess)
                                                            {
                                                                LogMessage.LogError("In MainEventLoop - failed to write block of data at index " + startingOffsetOfBytesBeingWrittenToDevice.ToString());
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        /// This is supposed to send you back to the start of the while(this.continueBootLoadProcess) loop, which is the equivalent
                                                        /// of going to the AGAIN: label if it existed in this code
                                                        continueWriteloaderLoop = false;
                                                        if (this.logBootloadProcess)
                                                        {
                                                            LogMessage.LogError("In MainEventLoop - VCUSBInit() command failed");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    /// we will consider a failure to accept the size and crc of the block as a total write failure
                                                    individualBlockWriteCount++;
                                                    if (this.logBootloadProcess)
                                                    {
                                                        LogMessage.LogError("In MainEventLoop - failed to write the signature of the current block of data at index " + startingOffsetOfBytesBeingWrittenToDevice.ToString());
                                                    }
                                                    /// There was a continue statement in the original code which would have migrated here in the translation
                                                }
                                            }
                                            else
                                            {
                                                /// This is supposed to send you back to the start of the while(this.continueBootLoadProcess) loop, which is the equivalent
                                                /// of going to the AGAIN: label if it existed in this code
                                                continueWriteloaderLoop = false;
                                                if (this.logBootloadProcess)
                                                {
                                                    LogMessage.LogError("In MainEventLoop - VCUSBInit() command failed");
                                                }
                                            }
                                            /// If we have exceeded the block rewrite limit, we start over
                                            if (individualBlockWriteCount > individualBlockWriteLimit)
                                            {
                                                /// we would do this if we wanted to send the initialization command again.  I think this is the only way that makes sense
                                                /// because I was told that if you toggle the stop/start button quickly after a failure, it will sometimes load the second time.
                                                /// that would mean all code must be executed again, including device initialization.
                                                continueWriteloaderLoop = false;
                                                if (this.logBootloadProcess)
                                                {
                                                    LogMessage.LogError("In MainEventLoop - resetting the firmware load to start over again from the initialization point");
                                                }

                                                /// we do this if we just want to start writing the blocks over again
                                                //numberOfChunksWritten = 0;
                                                //if (this.startSignature.fullLen == 8192)
                                                //{
                                                //    startingOffsetOfBytesBeingWrittenToDevice = 0;
                                                //}
                                                //else
                                                //{
                                                //    startingOffsetOfBytesBeingWrittenToDevice = 8192;
                                                //}
                                                //if (this.logBootloadProcess)
                                                //{
                                                //    LogMessage.LogError("In MainEventLoop - resetting the firmware load to start over again with the first block write");
                                                //}
                                            }
                                        } /// End of the loop: while (continueWriteloaderLoop &&  this.continueBootloadProcess && (l < this.startSignature.fullLen))
                                        /// THis is how the original code worked.  I took this condition and moved it inside the write loop
                                        /// Okay, we're done, but we aren't, because the last block we actually write to the device is the FIRST block of firmware code.  I think.
                                        //if (startingOffsetOfBytesBeingWrittenToDevice > 0)
                                        //{
                                        //    startingOffsetOfBytesBeingWrittenToDevice = 0;
                                        //    /// There is a GOTO Writeloader statement in the original code.
                                        //}
                                    }
                                    else
                                    {
                                        SetFirmwareLoadProgressValueRadLabelText("Failed to initialize device");
                                        completeFirmwareLoadCount++;
                                    }
                                }
                                else
                                {
                                    SetFirmwareLoadProgressValueRadLabelText(firmwareLoadCancelledText);
                                }
                                /// I have this test here just in case we have already successfully written the FW to the device, or have otherwise had a reason to exit the loop.  If that's true we don't want
                                /// to have this code block to execute
                                if (this.continueBootloadProcess)
                                {
                                    completeFirmwareLoadCount++;
                                    if (completeFirmwareLoadCount > completeFirmwareLoadRetryLimit)
                                    {
                                        this.continueBootloadProcess = false;
                                        SetFirmwareLoadProgressValueRadLabelText("Maximum number of firmware load attempts is " + completeFirmwareLoadRetryLimit.ToString() + " which has been exceeded");
                                    }
                                }
                            } // end while(this.continueBootloadProcess)
                        }
                        else
                        {
                            SetFirmwareLoadProgressValueRadLabelText(firmwareFileIncorrect);
                            this.continueBootloadProcess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.MainEventLoop()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                //startStopProcessRadButton.Text = startFirmwareUploadText;
                //firmwareLoadProgressValueRadLabel.Text = noOperationPendingText;
                //reportedFirmwareDeviceTypeRadLabel.Text = "";
            }
        }

        private void SetFirmwareLoadProgressValueRadLabelText(string text)
        {
            if (this.firmwareLoadProgressValueRadLabel.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => firmwareLoadProgressValueRadLabel.Text = text));
            }
            else
            {
                firmwareLoadProgressValueRadLabel.Text = text;
            }
        }

        private void SetFirmwareUploadoadRadProgressBarValue(int value)
        {
            if (this.firmwareUploadRadProgressBar.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() => this.firmwareUploadRadProgressBar.Value1 = value));
            }
            else
            {
                this.firmwareUploadRadProgressBar.Value1 = value;
            }
        }

        private bool WriteDataToTheDevice(byte[] dataToWriteToTheDevice, int lengthOfDataArray, int timeout, bool testMode)
        {
            bool success = false;
            IntPtr bytePointer = Marshal.AllocHGlobal(lengthOfDataArray);
            try
            {
                UInt32 result;
                /// I encapsulated the write so I could make changes to the way the code is structured (trying to get it to work)
                /// and only have to worry about doing it in one place.
                UInt32 numberOfBytesWritten = 0;
                Marshal.Copy(dataToWriteToTheDevice, 0, bytePointer, lengthOfDataArray);
                result = VCUSBWrite(bytePointer, (UInt32)lengthOfDataArray, ref numberOfBytesWritten, (UInt32)timeout);
                if (result == S_OK)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }

                if (testMode)
                {
                    MessageBox.Show("You can, maybe, insert a break point in the write to read return values");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.WriteDataToTheDevice(byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Marshal.FreeHGlobal(bytePointer);
            }
            return success;
        }

        private void ClearingRead(int timeout, bool testMode)
        {
            IntPtr bytePointer = Marshal.AllocHGlobal(64);
            //IntPtr bytesReadPointer = Marshal.AllocHGlobal(sizeof(UInt32));
            try
            {
                /// I encapsulated the read so I could make changes to the way the code is structured (trying to get it to work)
                /// and only have to worry about doing it in one place.
                UInt32 numberOfBytesRead = 0;

                UInt32 result;
                // byte[] numberOfBytesReadAsBytes = new byte[sizeof(UInt32)];

                UInt32 longTimeout = (UInt32)timeout;
                UInt32 longNumberOfBytesToRead = 64;
                int repeatCount;

                numberOfBytesRead = 1;
                result = S_OK;
                while ((numberOfBytesRead > 0) && (result == S_OK))
                {
                    result = VCUSBRead(bytePointer, longNumberOfBytesToRead, ref numberOfBytesRead, longTimeout);

                    // Application.DoEvents();
                    if (testMode)
                    {
                        MessageBox.Show("You can insert a break point in the Read function if you like to see the values");
                    }
                }

                /// I saw a lot of reads that were returning 121L, which is the code for semaphore not set, so I slapped this 
                /// questionable code in just in case repeating the read will result in a successful read.
                //repeatCount = 0;
                //while ((result == 121L) && (repeatCount < 3))
                //{
                //    result = VCUSBRead(bytePointer, longNumberOfBytesToRead, ref numberOfBytesRead, longTimeout);
                //    repeatCount++;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ClearingRead(int, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                //Marshal.FreeHGlobal(bytesReadPointer);
                Marshal.FreeHGlobal(bytePointer);
            }
        }

        private byte[] ReadDataFromTheDevice(int numberOfBytesToRead, int timeout, bool testMode)
        {
            byte[] bytesRead = null;
            IntPtr bytePointer = Marshal.AllocHGlobal(numberOfBytesToRead);
            //IntPtr bytesReadPointer = Marshal.AllocHGlobal(sizeof(UInt32));
            try
            {
                /// I encapsulated the read so I could make changes to the way the code is structured (trying to get it to work)
                /// and only have to worry about doing it in one place.
                UInt32 result;
                bool success = true;
                byte[] bytesFromDevice;
                UInt32 numberOfBytesRead = 0;
                int repeatCount;
                // byte[] numberOfBytesReadAsBytes = new byte[sizeof(UInt32)];

                UInt32 longTimeout = (UInt32)timeout;
                UInt32 longNumberOfBytesToRead = (UInt32)numberOfBytesToRead;

                result = VCUSBRead(bytePointer, longNumberOfBytesToRead, ref numberOfBytesRead, longTimeout);

                /// I saw a lot of reads that were returning 121L, which is the code for semaphore not set, so I slapped this 
                /// questionable code in just in case repeating the read will result in a successful read.
                repeatCount = 0;
                //while ((result == 121L) && (repeatCount < 3))
                //{
                //    result = VCUSBRead(bytePointer, longNumberOfBytesToRead, ref numberOfBytesRead, longTimeout);
                //    repeatCount++;
                //    Thread.Sleep(10);
                //    if (testMode)
                //    {
                //        MessageBox.Show("You can insert a break point in the Read function if you like to see the values");
                //    }
                //}

                success = (result == S_OK);

                /// this is a lot older, first pass, never used in working code
                //while (numberOfBytesRead > 0)
                //{
                //    result = VCUSBRead(bytePointer, longNumberOfBytesToRead, ref numberOfBytesRead, longTimeout);
                //    if (result != S_OK)
                //    {
                //        success = false;
                //        break;
                //    }
                //    Application.DoEvents();
                //    
                //}

                if (success)
                {
                    bytesFromDevice = new byte[numberOfBytesToRead];
                    Marshal.Copy(bytePointer, bytesFromDevice, 0, numberOfBytesToRead);
                    bytesRead = bytesFromDevice;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ReadDataFromTheDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                //Marshal.FreeHGlobal(bytesReadPointer);
                Marshal.FreeHGlobal(bytePointer);
            }
            return bytesRead;
        }

        //        private bool ReadSignatureToDetermineIfProcessIsCorrectSoFar()
        //        {
        //            bool success = false;
        //            try
        //            {
        //                Byte[] signatureBytes;
        //                signatureBytes = ReadDataFromTheDevice(64, 1, false);
        //                if (signatureBytes != null)
        //                {
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplay.ReadSignatureToDetermineIfProcessIsCorrectSoFar()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        private void InitializeBytesToWriteToDevice()
        {
            try
            {
                int numberOfBytes = 256 * 1024;

                this.allBytesToBeWrittenToTheDevice = new byte[numberOfBytes];

                for (int i = 0; i < numberOfBytes; i++)
                {
                    this.allBytesToBeWrittenToTheDevice[i] = 0xFF;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeBytesToWriteToDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ReadFirmwareFile()
        {
            try
            {
                string extension;

                this.firmwareFileName = this.firmwareFileNameRadTextBox.Text;
                if (this.firmwareFileName.Length > 0)
                {
                    extension = FileUtilities.GetFileExtension(this.firmwareFileName).ToLower();

                    if ((extension.CompareTo("sim") == 0) || (extension.CompareTo("a79") == 0))
                    {
                        this.allBytesReadFromInputFile = FileUtilities.ReadBinaryFileAsBytes(this.firmwareFileName);
                        if (this.allBytesReadFromInputFile == null)
                        {
                            RadMessageBox.Show(ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.FileReadFailed));
                            this.continueBootloadProcess = false;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(incorrectFileTypeText);
                        this.continueBootloadProcess = false;
                    }
                }
                else
                {
                    RadMessageBox.Show(fileNotSpecifiedText);
                    this.continueBootloadProcess = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.InitializeStartSignature()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// This is mostly the ReadSimpleCodeFile function without the file read.  I read the file in all at once and put it into
        /// the byte array fileBytes[], and this code goes through that array just like the original code read bytes from the file.
        /// </summary>
        private void ProcessFirmwareFile()
        {
            try
            {
                Int64 magic;
                Int64 flags;
                Int64 numberOfBytes;  // was bytes
                UInt16 version;
                Boolean continueProcessingInputBytes;  // was proceed
                Int32 recordTag; // was record_Tag
                Byte dataSegmentType; // was segtype
                Int64 start;
                Int64 fileBytesIndex; // this is basically my "file pointer" in that it points to the "current" part of the input byte array
                Int64 currentAddress; // was curr_addr
                Int32 numberOfBytesInFirmwareCode = 0; // was Len
                Int32 i;
                UInt32 checksumFromFile = 0;
                UInt32 checksumFromFileData = 0;

                this.totalNumberOfBytesToWriteToDevice = 0;

                continueProcessingInputBytes = true;
                fileBytesIndex = 0;

                /// This is the correct indexing, since this is the only way the magic number matched properly
                magic = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                fileBytesIndex += 4;
                flags = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                fileBytesIndex += 4;
                numberOfBytes = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                fileBytesIndex += 4;
                version = ConversionMethods.UnsignedShortBytesToUInt16(this.allBytesReadFromInputFile[fileBytesIndex], this.allBytesReadFromInputFile[fileBytesIndex + 1]);
                fileBytesIndex += 2;


                if (magic == 0x7F494152)
                {
                    while (continueProcessingInputBytes)
                    {
                        recordTag = ConversionMethods.ByteToInt32(this.allBytesReadFromInputFile[fileBytesIndex]);
                        fileBytesIndex++;
                        switch (recordTag)
                        {
                            case 1:
                                dataSegmentType = this.allBytesReadFromInputFile[fileBytesIndex];
                                fileBytesIndex++;
                                flags = ConversionMethods.UnsignedShortBytesToUInt16(this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                                fileBytesIndex += 2;
                                start = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                                fileBytesIndex += 4;
                                numberOfBytes = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                                fileBytesIndex += 4;

                                currentAddress = start;

                                for (i = 0; i < numberOfBytes; i++)
                                {
                                    this.allBytesToBeWrittenToTheDevice[currentAddress] = this.allBytesReadFromInputFile[fileBytesIndex];
                                    fileBytesIndex++;
                                    currentAddress++;
                                    /// I'm not sure if this is a waste of time or not, but it was written this way in the code I'm copying/modifying.  There might be a situation in which this is required
                                    if (numberOfBytesInFirmwareCode < currentAddress)
                                    {
                                        numberOfBytesInFirmwareCode = (Int32)currentAddress;
                                    }
                                }

                                break;
                            case 2:
                                /// Skip over unused values
                                fileBytesIndex += 5;
                                break;
                            case 3:
                                /// Again, I believe this is correct because I can successfully read a file and the CRC checks out fine for every sim file I tried.
                                checksumFromFileData = CalculateModularSumCRC(this.allBytesReadFromInputFile, fileBytesIndex);
                                checksumFromFile = ConversionMethods.UnsignedBytesToUInt32(this.allBytesReadFromInputFile[fileBytesIndex + 3], this.allBytesReadFromInputFile[fileBytesIndex + 2], this.allBytesReadFromInputFile[fileBytesIndex + 1], this.allBytesReadFromInputFile[fileBytesIndex]);
                                if ((checksumFromFile + checksumFromFileData) == 0)
                                {
                                    this.totalNumberOfBytesToWriteToDevice = numberOfBytesInFirmwareCode;
                                }
                                else
                                {
                                    RadMessageBox.Show(fileChecksumIncorrectText);
                                    this.continueBootloadProcess = false;
                                }
                                continueProcessingInputBytes = false;
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(fileContainedIncorrectMagicNumber);
                    this.continueBootloadProcess = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.ProcessFirmwareFile()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private UInt32 CalculateModularSumCRC(Byte[] inputBytes, Int64 length)
        {
            UInt32 checksum = 0;
            try
            {
                for (int i = 0; i < length; i++)
                {
                    checksum += inputBytes[i];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CalculateModularSumCRC(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return checksum;
        }

        private UInt16 CalculateCRC16(Byte[] inputBytes, Int32 length)
        {
            UInt16 crcReturnValue = 0;
            try
            {
                /// I took the notion of not changing the return value of the CRC until the computation is done
                /// from the sample code I used to create this code.  It seems like a good idea and should catch 
                /// some errors if the computation fails early for some reason and throws an exception.
                /// 
                UInt32 i;
                /// I have no idea why this variable is called "Dop" in the sample code, since I don't know what
                /// it's for.  The source code is from a program that apparently works properly, and this is just a
                /// translation of that code.
                Byte Dop;
                UInt16 computedCRC = 0xAAAA;
                Int32 temp;

                for (i = 0; i < length; i++)
                {
                    Dop = 0;
                    if ((computedCRC & 0x8000) != 0)
                    {
                        Dop = 1;
                    }
                    /// The source code says "+" and the algorithm from Allen says "|".  Since we're shifting left 1 bit before the add, that's the same thing.
                    ///computedCRC = (UInt16)(((computedCRC & 0x7FFF) << 1) + Dop);
                    ///
                    temp = (((computedCRC & 0x7FFF) << 1) | Dop);
                    temp = (temp ^ inputBytes[i]);
                    //computedCRC = (UInt16)(((computedCRC & 0x7FFF) << 1) | Dop);
                    //computedCRC = (UInt16)(computedCRC ^ inputBytes[i]);
                    computedCRC = (UInt16)temp;
                    //if (computedCRC != temp)
                    //{
                    //    MessageBox.Show("Oh MY GOD!!!");
                    //}
                }

                crcReturnValue = computedCRC;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CloseConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return crcReturnValue;
        }

        /// <summary>
        /// Equivalent to the TForm1.SendHello function from main.pas.  I consider the source I'm using to write this
        /// to be some of the most horrible code ever written.
        /// </summary>
        /// <returns></returns>
        private bool InitializeDeviceForFirmwareLoad()
        {
            bool isCorrect = false;
            try
            {
                int retryCount = 0;
                UInt32 result;
                Byte[] signatureBytesReadFromDevice;
                Byte[] signatureBytesWrittenToDevice;
                bool continueCheckingInitialDeviceSignature = true;
                while (this.continueBootloadProcess && continueCheckingInitialDeviceSignature)
                {
                    result = VCUSBInit();
                    if (result == S_OK)
                    {
                        ClearingRead(1, false);
                        //Application.DoEvents();
                        //Thread.Sleep(100);
                        signatureBytesWrittenToDevice = this.startSignature.GetByteEquivalentArrayOfData();
                        if (WriteDataToTheDevice(signatureBytesWrittenToDevice, (Int32)startSignatureLength, 50, false))
                        {
                            //Thread.Sleep(10);
                            //Application.DoEvents();
                            signatureBytesReadFromDevice = ReadDataFromTheDevice((Int32)okSignatureLength, 50, false);
                            if (signatureBytesReadFromDevice != null)
                            {
                                if (VerifySignature(signatureBytesReadFromDevice, (UInt32)signatureBytesReadFromDevice.Length))
                                {
                                    isCorrect = true;
                                    /// we are done, so we can get out
                                    continueCheckingInitialDeviceSignature = false;
                                }
                                else
                                {
                                    retryCount++;
                                }
                            }
                            else
                            {
                                retryCount++;
                            }
                        }
                        else
                        {
                            retryCount++;
                            if (this.logBootloadProcess)
                            {
                                LogMessage.LogError("In InitializeDeviceForFirmwareLoad - failed to write the initiliazation data to the device");
                            }
                        }
                    }
                    else
                    {
                        /// Since we aren't properly communicating with the device, we get out of this function
                        continueCheckingInitialDeviceSignature = false;
                    }
                    if (retryCount > 10)
                    {
                        continueCheckingInitialDeviceSignature = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CloseConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isCorrect;
        }

        public bool VerifySignature(Byte[] signatureBeingChecked, UInt32 signatureLength)
        {
            bool signatureIsTheSame = false;
            try
            {
                if ((signatureBeingChecked.Length > 7) &&
                    (signatureLength == this.okSignatureLength) &&
                    (signatureBeingChecked[0] == this.okSignature[0]) &&
                    (signatureBeingChecked[1] == this.okSignature[1]) &&
                    (signatureBeingChecked[2] == this.okSignature[2]) &&
                    (signatureBeingChecked[3] == this.okSignature[3]) &&
                    (signatureBeingChecked[4] == this.okSignature[4]) &&
                    (signatureBeingChecked[5] == this.okSignature[5]) &&
                    (signatureBeingChecked[6] == this.okSignature[6]) &&
                    (signatureBeingChecked[7] == this.okSignature[7]))
                {
                    signatureIsTheSame = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.VerifySignature(Byte[], UInt64)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return signatureIsTheSame;
        }

        /// <summary>
        /// This is the equivalent of the FindSignature function from the original code, except I return the value
        /// of the device type rather than a boolean.
        /// </summary>
        /// <param name="inputBytes"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        private byte FindDeviceTypeFromSignature(Byte[] inputBytes, int limit)
        {
            byte deviceType = 0;
            try
            {
                int i;

                for (i = 0; i < limit; i++)
                {
                    if ((inputBytes[i + 0] == this.okSignature[0]) &&
                        (inputBytes[i + 1] == this.okSignature[1]) &&
                        (inputBytes[i + 2] == this.okSignature[2]) &&
                        (inputBytes[i + 3] == this.okSignature[3]) &&
                        (inputBytes[i + 5] == this.okSignature[5]) &&
                        (inputBytes[i + 6] == this.okSignature[6]) &&
                        (inputBytes[i + 7] == this.okSignature[7]))
                    {
                        deviceType = inputBytes[i + 4];
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.FindDeviceTypeFromSignature(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        private string GetDeviceTypeAsString(byte deviceType)
        {
            string deviceTypeAsString = "Device not defined";
            try
            {
                switch (deviceType)
                {
                    case (byte)'1':
                        deviceTypeAsString = "R400";
                        break;
                    case (byte)'3':
                        deviceTypeAsString = "TIM3";
                        break;
                    case (byte)'4':
                        deviceTypeAsString = "R1500";
                        break;
                    case (byte)'5':
                        deviceTypeAsString = "R1500/6";
                        break;
                    case (byte)'6':
                        deviceTypeAsString = "R1500 LCD";
                        break;
                    case (byte)'7':
                        deviceTypeAsString = "TDM7";
                        break;
                    case (byte)'A':
                        deviceTypeAsString = "AR200";
                        break;
                    case (byte)'C':
                        deviceTypeAsString = "CLTester";
                        break;
                    case (byte)'D':
                        deviceTypeAsString = "LDM";
                        break;
                    case (byte)'K':
                        deviceTypeAsString = "Korsar7";
                        break;
                    case (byte)'k':
                        deviceTypeAsString = "KIT";
                        break;
                    case (byte)'L':
                        deviceTypeAsString = "LMonitor";
                        break;
                    case (byte)'M':
                        deviceTypeAsString = "TDM";
                        break;
                    case (byte)'P':
                        deviceTypeAsString = "PDMonitor";
                        break;
                    case (byte)'Q':
                        deviceTypeAsString = "EII";
                        break;
                    case (byte)'R':
                        deviceTypeAsString = "R505";
                        break;
                    case (byte)'T':
                        deviceTypeAsString = "TEST9";
                        break;
                    case (byte)'V':
                        deviceTypeAsString = "VVTESTER";
                        break;
                    case (byte)'Z':
                        deviceTypeAsString = "TDMT";
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetDeviceTypeAsString(byte)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceTypeAsString;
        }

        private void startStopProcessRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!continueBootloadProcess)
                {
                    if (this.logProgressRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        this.logBootloadProcess = true;
                    }
                    else
                    {
                        this.logBootloadProcess = false;
                    }
                    this.logProgressRadCheckBox.Enabled = false;
                    this.testUsbConnectionRadButton.Enabled = false;
                    this.getFirmwareFileNameRadButton.Enabled = false;

                    continueBootloadProcess = true;
                    startStopProcessRadButton.Text = stopFirmwareUploadText;
                    firmwareUploadRadProgressBar.Visible = true;
                    firmwareUploadRadProgressBar.Value1 = 0;
                    firmwareUploadRadWaitingBar.Visible = true;
                    firmwareUploadRadWaitingBar.StartWaiting();
                    /// You could run the program either by just calling MainEventLoop() or starting the background worker thread, but not both
                    // MainEventLoop();
                    firmwareLoadBackgroundWorker.RunWorkerAsync();
                }
                else
                {
                    if (RadMessageBox.Show(areYouSureYouWantToCancelText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        continueBootloadProcess = false;
                        startStopProcessRadButton.Enabled = false;
                        RadMessageBox.Show(cancellationMayTakeAFewSecondsText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.startStopProcessRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        private void SetInitialPreferences()
        {
#if DEBUG
            initialFirmwareDirectory = Path.GetDirectoryName(Application.ExecutablePath);
#else
            initialFirmwareDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Dynamic Ratings", "Firmware");
#endif
        }

        private void ReadPreferences()
        {
            try
            {
                List<string> entries = new List<string>();
                Dictionary<string, string> preferences = new Dictionary<string, string>();
                if (File.Exists(preferencesFileWithFullPath))
                {
                    entries = FileUtilities.ReadRegularFileContentsAsListOfStrings(preferencesFileWithFullPath);
                    if (entries.Count > 0)
                    {
                        preferences = ConversionMethods.ConvertListOfTwoColumnDrlFileEntriesToDictionary(entries);
                        if (preferences.ContainsKey(initialFirmwareDirectoryKeyName))
                        {
                            initialFirmwareDirectory = preferences[initialFirmwareDirectoryKeyName];


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DR_TranslationTool.LoadPreferencesFile(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SavePreferences()
        {
            try
            {
                List<string> preferencesList = new List<string>();
                if ((initialFirmwareDirectory != null) && (initialFirmwareDirectory != string.Empty))
                {
                    preferencesList.Add(initialFirmwareDirectoryKeyName + ", " + initialFirmwareDirectory);
                }

                FileUtilities.SaveDataToGenericRegularFile(preferencesFileWithFullPath, preferencesList);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DR_TranslationTool.SavePreferences()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void getFirmwareFileNameRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string fileName = FileUtilities.GetFileNameWithFullPath(initialFirmwareDirectory, "sim");
                if ((fileName != null) && (fileName != string.Empty))
                {
                    firmwareFileNameRadTextBox.Text = fileName;
                    initialFirmwareDirectory = Path.GetDirectoryName(fileName);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.getFirmwareFileNameRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        #region threading

        private void InitializeFirmwareLoadBackgroundWorker()
        {
            /// Set up the download background worker
            firmwareLoadBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                //WorkerReportsProgress = true
            };

            firmwareLoadBackgroundWorker.DoWork += firmwareLoadBackgroundWorker_DoWork;
            // firmwareLoadBackgroundWorker.ProgressChanged += firmwareLoadBackgroundWorker_ProgressChanged;
            firmwareLoadBackgroundWorker.RunWorkerCompleted += firmwareLoadBackgroundWorker_RunWorkerCompleted;
        }

        private void firmwareLoadBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                MainEventLoop();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.firmwareLoadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        //        private void firmwareLoadBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //        {
        //            try
        //            {
        //                int progress = e.ProgressPercentage;

        //                if (progress < 0)
        //                {
        //                    progress = 0;
        //                }
        //                if (progress > 100)
        //                {
        //                    progress = 100;
        //                }

        //                this.commandRetriesRadProgressBar.Value1 = progress;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in CommandRetry.firmwareLoadBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void firmwareLoadBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in CommandRetry.firmwareLoadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                //else if (e.Cancelled)
                //{

                //}
                //else
                //{

                //}

                //                if (e.Result != null)
                //                {

                //                }
                //                else
                //                {
                //                    outputCommandInputOutputObject = null;
                //                    string errorMessage = "Error in CommandRetry.firmwareLoadBackgroundWorker_DoWork(object, DoWorkEventArgs)\nResult from DoWork was null.";
                //                    LogMessage.LogError(errorMessage);
                //#if DEBUG
                //                    MessageBox.Show(errorMessage);
                //#endif
                //                }

                //                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.firmwareLoadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.startStopProcessRadButton.Text = startFirmwareUploadText;
                this.startStopProcessRadButton.Enabled = true;
                // this.firmwareLoadProgressValueRadLabel.Text = noOperationPendingText;
                // this.reportedFirmwareDeviceTypeRadLabel.Text = deviceTypeFromFirmwareUnknownText;
                firmwareUploadRadProgressBar.Visible = false;
                firmwareUploadRadWaitingBar.Visible = false;
                firmwareUploadRadWaitingBar.StopWaiting();
                this.continueBootloadProcess = false;
                this.logProgressRadCheckBox.Enabled = true;
                this.testUsbConnectionRadButton.Enabled = true;
                this.getFirmwareFileNameRadButton.Enabled = true;
            }
        }

        #endregion

        private void testUsbConnectionRadButton_Click(object sender, EventArgs e)
        {
            testUsbConnectionRadLabel.Text = "Checking USB connection";
            int deviceType = 0;
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            ErrorCode errorCode = SimplifiedCommunication.OpenUsbConnectionToAnyMonitor(1, 20, 200, 1, 20);
            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
            {
                deviceType = InteractiveDeviceCommunication.GetDeviceType(1, 200, 1, 20, parentWindowInformation);
                if (deviceType > 0)
                {
                    if (deviceType == 505)
                    {
                        testUsbConnectionRadLabel.Text = "USB connection is working\nyou are connected to a PDM";
                    }
                    else if (deviceType == 15002)
                    {
                        testUsbConnectionRadLabel.Text = "USB connection is working\nyou are connected to a BHM";
                    }
                    else if (deviceType == 101)
                    {
                        testUsbConnectionRadLabel.Text = "USB connection is working\nyou are connected to a Main monitor";
                    }
                    else
                    {
                        testUsbConnectionRadLabel.Text = "USB connection is working\nthe device you are connected to is unknown";
                    }
                }
                else
                {
                    testUsbConnectionRadLabel.Text = "Command to get device type failed";
                }
            }
            else
            {
                testUsbConnectionRadLabel.Text = "Failed to open a connection to the device";
            }
        }

        private void viewLogRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = notepadExeText;
                p.StartInfo.Arguments = Path.Combine(this.applicationDirectory, this.errorLogfileNameText);
                p.Start();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.firmwareLoadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetUpMainMenu()
        {
            try
            {
                int mainMenuIndex = 0;

                /// Help Menu
                RadMenuItem helpRadMenuItem = new RadMenuItem();
                helpRadMenuItem.Text = "Help";
                mainDisplayRadMenu.Items.Add(helpRadMenuItem);

                RadMenuItem openHelpFileRadMenuItem = new RadMenuItem();
                openHelpFileRadMenuItem.Text = "Open Help";
                openHelpFileRadMenuItem.Click += new EventHandler(openHelpFileRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(openHelpFileRadMenuItem);

                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(new RadMenuSeparatorItem());

                RadMenuItem aboutDmUtilitiesRadMenuItem = new RadMenuItem();
                aboutDmUtilitiesRadMenuItem.Text = "About";
                aboutDmUtilitiesRadMenuItem.Click += new EventHandler(aboutDmLoaderRadMenuItem_Click);
                ((RadMenuItem)mainDisplayRadMenu.Items[mainMenuIndex]).Items.Add(aboutDmUtilitiesRadMenuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.SetUpMainMenu()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void openHelpFileRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = Path.Combine(executablePath, "DM Loader Manual.chm");

                if (p.StartInfo.FileName != null)
                {
                    p.Start();
                }
                else
                {
                    MessageBox.Show(this, "Could not find the file 'DM Loader Manual.chm' in the program directory");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.openHelpFileRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void aboutDmLoaderRadMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (AboutDMLoader about = new AboutDMLoader())
                {
                    about.ShowDialog();
                    about.Hide();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.aboutDmLoaderRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


    }
}
