﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DMLoader
{
    /// <summary>
    /// This started out as a Pascal Type definition, which would mean that one could take it
    /// and directly cast it to a 64-entry byte array.  That will not work for classes, so we
    /// will create a function that returns the byte equivalent of all the data defined as public.
    /// </summary>
    public class StartSignature
    {
        public Byte[] signature = new byte[8];
        public Int32 blockAddr = 0;
        public Int32 blockLen = 0;
        public Int32 fullLen = 0;
        public Int32 option = 0;
        public UInt16 crcProg = 0;
        public UInt16 crcHdr = 0;
        public Byte[] reserv = new byte[36];

        public StartSignature()
        {
            for (int i = 0; i < 36; i++)
            {
                reserv[i] = (byte)'\0';
            }
        }

        /// Okay, the "Type" definition is done, now we need to add support fuctions to 
        /// create the byte-array equivalent of this class

        public Byte[] GetByteEquivalentArrayOfData()
        {
            Byte[] returnBytes = null;
            try
            {
                Byte[] tempReturnBytes = new byte[64];
                byte[] singleTypeConvertedToBytes;
                int i;

                for (i = 0; i < 8; i++)
                {
                    tempReturnBytes[i] = signature[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.Int32ToSignedBytes(blockAddr);
                for (i = 0; i < 4; i++)
                {
                    tempReturnBytes[i + 8] = singleTypeConvertedToBytes[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.Int32ToSignedBytes(blockLen);
                for (i = 0; i < 4; i++)
                {
                    tempReturnBytes[i + 12] = singleTypeConvertedToBytes[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.Int32ToSignedBytes(fullLen);
                for (i = 0; i < 4; i++)
                {
                    tempReturnBytes[i + 16] = singleTypeConvertedToBytes[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.Int32ToSignedBytes(option);
                for (i = 0; i < 4; i++)
                {
                    tempReturnBytes[i + 20] = singleTypeConvertedToBytes[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.UInt16ToUnsignedShortBytes(crcProg);
                for (i = 0; i < 2; i++)
                {
                    tempReturnBytes[i + 24] = singleTypeConvertedToBytes[i];
                }

                singleTypeConvertedToBytes = ConversionMethods.UInt16ToUnsignedShortBytes(crcHdr);
                for (i = 0; i < 2; i++)
                {
                    tempReturnBytes[i + 26] = singleTypeConvertedToBytes[i];
                }

                for (i = 0; i < 36; i++)
                {
                    tempReturnBytes[i + 28] = reserv[i];
                }

                /// finally, make the assignment
                returnBytes = tempReturnBytes;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in StartSignature.GetByteEquivalentArrayOfData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnBytes;
        }
    }
}
