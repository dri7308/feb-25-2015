﻿namespace DMLoader
{
    partial class MainDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDisplay));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.logProgressRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.testUsbConnectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.testUsbConnectionRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.testUsbConnectionRadButton = new Telerik.WinControls.UI.RadButton();
            this.firmwareUploadRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.firmwareUploadRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.startStopProcessRadButton = new Telerik.WinControls.UI.RadButton();
            this.firmwareLoadProgressValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareLoadProgressTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.reportedFirmwareDeviceTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareFileNameTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareFileNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.loggingRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.viewLogRadButton = new Telerik.WinControls.UI.RadButton();
            this.maximumNumberOfRetriesTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.numberOfRetrieslRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.mainDisplayRadMenu = new Telerik.WinControls.UI.RadMenu();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.getFirmwareFileNameRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.logProgressRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadGroupBox)).BeginInit();
            this.testUsbConnectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareUploadRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareUploadRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startStopProcessRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareLoadProgressValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareLoadProgressTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportedFirmwareDeviceTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFileNameTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFileNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggingRadGroupBox)).BeginInit();
            this.loggingRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewLogRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumNumberOfRetriesTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRetrieslRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getFirmwareFileNameRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // logProgressRadCheckBox
            // 
            this.logProgressRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.logProgressRadCheckBox.Location = new System.Drawing.Point(19, 22);
            this.logProgressRadCheckBox.Name = "logProgressRadCheckBox";
            this.logProgressRadCheckBox.Size = new System.Drawing.Size(94, 32);
            this.logProgressRadCheckBox.TabIndex = 34;
            this.logProgressRadCheckBox.Text = "<html>Log progress of<br>firmware load</html>";
            this.logProgressRadCheckBox.ThemeName = "Office2007Black";
            // 
            // testUsbConnectionRadGroupBox
            // 
            this.testUsbConnectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.testUsbConnectionRadGroupBox.Controls.Add(this.testUsbConnectionRadLabel);
            this.testUsbConnectionRadGroupBox.Controls.Add(this.testUsbConnectionRadButton);
            this.testUsbConnectionRadGroupBox.FooterImageIndex = -1;
            this.testUsbConnectionRadGroupBox.FooterImageKey = "";
            this.testUsbConnectionRadGroupBox.HeaderImageIndex = -1;
            this.testUsbConnectionRadGroupBox.HeaderImageKey = "";
            this.testUsbConnectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.testUsbConnectionRadGroupBox.HeaderText = "Test USB Connection - Device must be powered on";
            this.testUsbConnectionRadGroupBox.Location = new System.Drawing.Point(12, 32);
            this.testUsbConnectionRadGroupBox.Name = "testUsbConnectionRadGroupBox";
            this.testUsbConnectionRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.testUsbConnectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.testUsbConnectionRadGroupBox.Size = new System.Drawing.Size(431, 91);
            this.testUsbConnectionRadGroupBox.TabIndex = 33;
            this.testUsbConnectionRadGroupBox.Text = "Test USB Connection - Device must be powered on";
            this.testUsbConnectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // testUsbConnectionRadLabel
            // 
            this.testUsbConnectionRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.testUsbConnectionRadLabel.Location = new System.Drawing.Point(136, 41);
            this.testUsbConnectionRadLabel.Name = "testUsbConnectionRadLabel";
            this.testUsbConnectionRadLabel.Size = new System.Drawing.Size(201, 18);
            this.testUsbConnectionRadLabel.TabIndex = 21;
            this.testUsbConnectionRadLabel.Text = "Press the button to test the connection";
            // 
            // testUsbConnectionRadButton
            // 
            this.testUsbConnectionRadButton.Location = new System.Drawing.Point(10, 30);
            this.testUsbConnectionRadButton.Name = "testUsbConnectionRadButton";
            this.testUsbConnectionRadButton.Size = new System.Drawing.Size(110, 46);
            this.testUsbConnectionRadButton.TabIndex = 20;
            this.testUsbConnectionRadButton.Text = "<html>Test Connection</html>";
            this.testUsbConnectionRadButton.ThemeName = "Office2007Black";
            this.testUsbConnectionRadButton.Click += new System.EventHandler(this.testUsbConnectionRadButton_Click);
            // 
            // firmwareUploadRadWaitingBar
            // 
            this.firmwareUploadRadWaitingBar.Location = new System.Drawing.Point(453, 281);
            this.firmwareUploadRadWaitingBar.Name = "firmwareUploadRadWaitingBar";
            this.firmwareUploadRadWaitingBar.Size = new System.Drawing.Size(113, 23);
            this.firmwareUploadRadWaitingBar.TabIndex = 31;
            this.firmwareUploadRadWaitingBar.Text = "radWaitingBar1";
            this.firmwareUploadRadWaitingBar.ThemeName = "Office2007Black";
            this.firmwareUploadRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.firmwareUploadRadWaitingBar.WaitingSpeed = 30;
            this.firmwareUploadRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // firmwareUploadRadProgressBar
            // 
            this.firmwareUploadRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.firmwareUploadRadProgressBar.ImageIndex = -1;
            this.firmwareUploadRadProgressBar.ImageKey = "";
            this.firmwareUploadRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.firmwareUploadRadProgressBar.Location = new System.Drawing.Point(40, 281);
            this.firmwareUploadRadProgressBar.Name = "firmwareUploadRadProgressBar";
            this.firmwareUploadRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.firmwareUploadRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.firmwareUploadRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.firmwareUploadRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.firmwareUploadRadProgressBar.Size = new System.Drawing.Size(394, 23);
            this.firmwareUploadRadProgressBar.TabIndex = 30;
            this.firmwareUploadRadProgressBar.Text = "Firmware Upload Progress";
            this.firmwareUploadRadProgressBar.ThemeName = "Office2007Black";
            // 
            // startStopProcessRadButton
            // 
            this.startStopProcessRadButton.Location = new System.Drawing.Point(241, 336);
            this.startStopProcessRadButton.Name = "startStopProcessRadButton";
            this.startStopProcessRadButton.Size = new System.Drawing.Size(158, 71);
            this.startStopProcessRadButton.TabIndex = 29;
            this.startStopProcessRadButton.Text = "radButton1";
            this.startStopProcessRadButton.ThemeName = "Office2007Black";
            this.startStopProcessRadButton.Click += new System.EventHandler(this.startStopProcessRadButton_Click);
            // 
            // firmwareLoadProgressValueRadLabel
            // 
            this.firmwareLoadProgressValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareLoadProgressValueRadLabel.Location = new System.Drawing.Point(175, 242);
            this.firmwareLoadProgressValueRadLabel.Name = "firmwareLoadProgressValueRadLabel";
            this.firmwareLoadProgressValueRadLabel.Size = new System.Drawing.Size(103, 16);
            this.firmwareLoadProgressValueRadLabel.TabIndex = 26;
            this.firmwareLoadProgressValueRadLabel.Text = "Nothing loading yet";
            // 
            // firmwareLoadProgressTextRadLabel
            // 
            this.firmwareLoadProgressTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareLoadProgressTextRadLabel.Location = new System.Drawing.Point(40, 242);
            this.firmwareLoadProgressTextRadLabel.Name = "firmwareLoadProgressTextRadLabel";
            this.firmwareLoadProgressTextRadLabel.Size = new System.Drawing.Size(131, 16);
            this.firmwareLoadProgressTextRadLabel.TabIndex = 25;
            this.firmwareLoadProgressTextRadLabel.Text = "Firmware load progress: ";
            // 
            // reportedFirmwareDeviceTypeRadLabel
            // 
            this.reportedFirmwareDeviceTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.reportedFirmwareDeviceTypeRadLabel.Location = new System.Drawing.Point(73, 205);
            this.reportedFirmwareDeviceTypeRadLabel.Name = "reportedFirmwareDeviceTypeRadLabel";
            this.reportedFirmwareDeviceTypeRadLabel.Size = new System.Drawing.Size(326, 16);
            this.reportedFirmwareDeviceTypeRadLabel.TabIndex = 28;
            this.reportedFirmwareDeviceTypeRadLabel.Text = "The device type reported in the firmware is unknown at this time";
            // 
            // firmwareFileNameTextRadLabel
            // 
            this.firmwareFileNameTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareFileNameTextRadLabel.Location = new System.Drawing.Point(12, 156);
            this.firmwareFileNameTextRadLabel.Name = "firmwareFileNameTextRadLabel";
            this.firmwareFileNameTextRadLabel.Size = new System.Drawing.Size(102, 16);
            this.firmwareFileNameTextRadLabel.TabIndex = 24;
            this.firmwareFileNameTextRadLabel.Text = "Firmware file name";
            // 
            // firmwareFileNameRadTextBox
            // 
            this.firmwareFileNameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.firmwareFileNameRadTextBox.Location = new System.Drawing.Point(119, 156);
            this.firmwareFileNameRadTextBox.Name = "firmwareFileNameRadTextBox";
            this.firmwareFileNameRadTextBox.ReadOnly = true;
            this.firmwareFileNameRadTextBox.Size = new System.Drawing.Size(425, 20);
            this.firmwareFileNameRadTextBox.TabIndex = 23;
            this.firmwareFileNameRadTextBox.TabStop = false;
            this.firmwareFileNameRadTextBox.ThemeName = "Office2007Black";
            // 
            // loggingRadGroupBox
            // 
            this.loggingRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.loggingRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loggingRadGroupBox.Controls.Add(this.viewLogRadButton);
            this.loggingRadGroupBox.Controls.Add(this.logProgressRadCheckBox);
            this.loggingRadGroupBox.FooterImageIndex = -1;
            this.loggingRadGroupBox.FooterImageKey = "";
            this.loggingRadGroupBox.HeaderImageIndex = -1;
            this.loggingRadGroupBox.HeaderImageKey = "";
            this.loggingRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.loggingRadGroupBox.HeaderText = "Logging";
            this.loggingRadGroupBox.Location = new System.Drawing.Point(453, 32);
            this.loggingRadGroupBox.Name = "loggingRadGroupBox";
            this.loggingRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.loggingRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.loggingRadGroupBox.Size = new System.Drawing.Size(137, 91);
            this.loggingRadGroupBox.TabIndex = 34;
            this.loggingRadGroupBox.Text = "Logging";
            this.loggingRadGroupBox.ThemeName = "Office2007Black";
            // 
            // viewLogRadButton
            // 
            this.viewLogRadButton.Location = new System.Drawing.Point(19, 60);
            this.viewLogRadButton.Name = "viewLogRadButton";
            this.viewLogRadButton.Size = new System.Drawing.Size(94, 26);
            this.viewLogRadButton.TabIndex = 20;
            this.viewLogRadButton.Text = "View log";
            this.viewLogRadButton.ThemeName = "Office2007Black";
            this.viewLogRadButton.Click += new System.EventHandler(this.viewLogRadButton_Click);
            // 
            // maximumNumberOfRetriesTextRadLabel
            // 
            this.maximumNumberOfRetriesTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.maximumNumberOfRetriesTextRadLabel.Location = new System.Drawing.Point(40, 350);
            this.maximumNumberOfRetriesTextRadLabel.Name = "maximumNumberOfRetriesTextRadLabel";
            this.maximumNumberOfRetriesTextRadLabel.Size = new System.Drawing.Size(51, 40);
            this.maximumNumberOfRetriesTextRadLabel.TabIndex = 35;
            this.maximumNumberOfRetriesTextRadLabel.Text = "<html>Maximum<br>number<br>of tries</html>";
            // 
            // numberOfRetrieslRadSpinEditor
            // 
            this.numberOfRetrieslRadSpinEditor.Location = new System.Drawing.Point(97, 359);
            this.numberOfRetrieslRadSpinEditor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numberOfRetrieslRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfRetrieslRadSpinEditor.Name = "numberOfRetrieslRadSpinEditor";
            // 
            // 
            // 
            this.numberOfRetrieslRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.numberOfRetrieslRadSpinEditor.ShowBorder = true;
            this.numberOfRetrieslRadSpinEditor.Size = new System.Drawing.Size(45, 20);
            this.numberOfRetrieslRadSpinEditor.TabIndex = 36;
            this.numberOfRetrieslRadSpinEditor.TabStop = false;
            this.numberOfRetrieslRadSpinEditor.ThemeName = "Office2007Black";
            this.numberOfRetrieslRadSpinEditor.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // mainDisplayRadMenu
            // 
            this.mainDisplayRadMenu.Location = new System.Drawing.Point(0, 0);
            this.mainDisplayRadMenu.Name = "mainDisplayRadMenu";
            this.mainDisplayRadMenu.Size = new System.Drawing.Size(595, 26);
            this.mainDisplayRadMenu.TabIndex = 37;
            this.mainDisplayRadMenu.Text = "radMenu1";
            this.mainDisplayRadMenu.ThemeName = "Office2007Black";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::DMLoader.Properties.Resources.DR_Logo_Small_LRHC;
            this.pictureBox1.Location = new System.Drawing.Point(460, 373);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(135, 49);
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            // 
            // getFirmwareFileNameRadButton
            // 
            this.getFirmwareFileNameRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getFirmwareFileNameRadButton.Image = ((System.Drawing.Image)(resources.GetObject("getFirmwareFileNameRadButton.Image")));
            this.getFirmwareFileNameRadButton.Location = new System.Drawing.Point(550, 148);
            this.getFirmwareFileNameRadButton.Name = "getFirmwareFileNameRadButton";
            this.getFirmwareFileNameRadButton.Size = new System.Drawing.Size(40, 36);
            this.getFirmwareFileNameRadButton.TabIndex = 27;
            this.getFirmwareFileNameRadButton.Click += new System.EventHandler(this.getFirmwareFileNameRadButton_Click);
            // 
            // MainDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(595, 422);
            this.Controls.Add(this.mainDisplayRadMenu);
            this.Controls.Add(this.numberOfRetrieslRadSpinEditor);
            this.Controls.Add(this.maximumNumberOfRetriesTextRadLabel);
            this.Controls.Add(this.loggingRadGroupBox);
            this.Controls.Add(this.testUsbConnectionRadGroupBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.firmwareUploadRadWaitingBar);
            this.Controls.Add(this.firmwareUploadRadProgressBar);
            this.Controls.Add(this.startStopProcessRadButton);
            this.Controls.Add(this.firmwareLoadProgressValueRadLabel);
            this.Controls.Add(this.firmwareLoadProgressTextRadLabel);
            this.Controls.Add(this.reportedFirmwareDeviceTypeRadLabel);
            this.Controls.Add(this.getFirmwareFileNameRadButton);
            this.Controls.Add(this.firmwareFileNameTextRadLabel);
            this.Controls.Add(this.firmwareFileNameRadTextBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainDisplay";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DM Loader";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.MainDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.logProgressRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadGroupBox)).EndInit();
            this.testUsbConnectionRadGroupBox.ResumeLayout(false);
            this.testUsbConnectionRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testUsbConnectionRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareUploadRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareUploadRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startStopProcessRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareLoadProgressValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareLoadProgressTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportedFirmwareDeviceTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFileNameTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareFileNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loggingRadGroupBox)).EndInit();
            this.loggingRadGroupBox.ResumeLayout(false);
            this.loggingRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.viewLogRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumNumberOfRetriesTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfRetrieslRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDisplayRadMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getFirmwareFileNameRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadCheckBox logProgressRadCheckBox;
        private Telerik.WinControls.UI.RadGroupBox testUsbConnectionRadGroupBox;
        private Telerik.WinControls.UI.RadLabel testUsbConnectionRadLabel;
        private Telerik.WinControls.UI.RadButton testUsbConnectionRadButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadWaitingBar firmwareUploadRadWaitingBar;
        private Telerik.WinControls.UI.RadProgressBar firmwareUploadRadProgressBar;
        private Telerik.WinControls.UI.RadButton startStopProcessRadButton;
        private Telerik.WinControls.UI.RadLabel firmwareLoadProgressValueRadLabel;
        private Telerik.WinControls.UI.RadLabel firmwareLoadProgressTextRadLabel;
        private Telerik.WinControls.UI.RadLabel reportedFirmwareDeviceTypeRadLabel;
        private Telerik.WinControls.UI.RadButton getFirmwareFileNameRadButton;
        private Telerik.WinControls.UI.RadLabel firmwareFileNameTextRadLabel;
        private Telerik.WinControls.UI.RadTextBox firmwareFileNameRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox loggingRadGroupBox;
        private Telerik.WinControls.UI.RadButton viewLogRadButton;
        private Telerik.WinControls.UI.RadLabel maximumNumberOfRetriesTextRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor numberOfRetrieslRadSpinEditor;
        private Telerik.WinControls.UI.RadMenu mainDisplayRadMenu;
    }
}

