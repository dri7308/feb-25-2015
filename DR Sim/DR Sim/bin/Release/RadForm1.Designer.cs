﻿namespace DR_Sim
{
    partial class RadForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn1 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn2 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn3 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn4 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn5 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn6 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn7 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn8 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn1 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn2 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn9 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn10 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn11 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn12 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn3 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn13 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn14 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn15 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn16 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn17 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn18 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn19 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn20 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn5 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn21 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn22 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn23 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn24 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn25 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn26 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn27 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn28 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn29 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn30 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn31 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn32 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn33 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn6 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.Data.GroupDescriptor groupDescriptor1 = new Telerik.WinControls.Data.GroupDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn7 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn8 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn34 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn35 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            Telerik.WinControls.Data.GroupDescriptor groupDescriptor2 = new Telerik.WinControls.Data.GroupDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDecimalColumn gridViewDecimalColumn36 = new Telerik.WinControls.UI.GridViewDecimalColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadForm1));
            this.highContrastBlackTheme1 = new Telerik.WinControls.Themes.HighContrastBlackTheme();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage7 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton14 = new Telerik.WinControls.UI.RadButton();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radSpinEditor3 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor5 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor4 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.winChartViewer1 = new ChartDirector.WinChartViewer();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGroupBox5 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView8 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel23 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView6 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel21 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView7 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridView5 = new Telerik.WinControls.UI.RadGridView();
            this.radLabel20 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton15 = new Telerik.WinControls.UI.RadButton();
            this.winChartViewer2 = new ChartDirector.WinChartViewer();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.radGridView3 = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel33 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditor13 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor12 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel17 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel18 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel19 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radTrackBar3 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar2 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar4 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.radGridView4 = new Telerik.WinControls.UI.RadGridView();
            this.radPageViewPage6 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton16 = new Telerik.WinControls.UI.RadButton();
            this.radButton13 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox8 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            this.radButton12 = new Telerik.WinControls.UI.RadButton();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.radSpinEditor8 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor9 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel29 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel28 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel27 = new Telerik.WinControls.UI.RadLabel();
            this.radSpinEditor10 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor11 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor7 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel26 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel25 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView10 = new Telerik.WinControls.UI.RadGridView();
            this.rTCalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coeff = new DR_Sim.Coeff();
            this.radLabel24 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView9 = new Telerik.WinControls.UI.RadGridView();
            this.curOffsetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radPageViewPage8 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radSpinEditor6 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.object_0e822de2_cd22_4194_9660_a631cc446635 = new Telerik.WinControls.RootRadElement();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.ellipseShape1 = new Telerik.WinControls.EllipseShape();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.radButton10 = new Telerik.WinControls.UI.RadButton();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.office2010BlackTheme2 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.highContrastBlackTheme2 = new Telerik.WinControls.Themes.HighContrastBlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).BeginInit();
            this.radGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView8.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView6.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView7.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView5.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            this.radPageViewPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).BeginInit();
            this.radPageViewPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).BeginInit();
            this.radPageViewPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView10.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTCalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView9.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.curOffsetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPageView1
            // 
            this.radPageView1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPageView1.Controls.Add(this.radPageViewPage7);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Controls.Add(this.radPageViewPage4);
            this.radPageView1.Controls.Add(this.radPageViewPage5);
            this.radPageView1.Controls.Add(this.radPageViewPage6);
            this.radPageView1.Controls.Add(this.radPageViewPage8);
            this.radPageView1.Location = new System.Drawing.Point(5, 58);
            this.radPageView1.Name = "radPageView1";
            // 
            // 
            // 
            this.radPageView1.RootElement.ControlBounds = new System.Drawing.Rectangle(1, 58, 400, 300);
            this.radPageView1.RootElement.Shape = null;
            this.radPageView1.SelectedPage = this.radPageViewPage8;
            this.radPageView1.Size = new System.Drawing.Size(1031, 618);
            this.radPageView1.TabIndex = 0;
            this.radPageView1.Text = "Control";
            this.radPageView1.ThemeName = "Office2010Black";
            this.radPageView1.ViewMode = Telerik.WinControls.UI.PageViewMode.Backstage;
            this.radPageView1.SelectedPageChanged += new System.EventHandler(this.radPageView1_SelectedPageChanged);
            ((Telerik.WinControls.UI.StripViewItemContainer)(this.radPageView1.GetChildAt(0).GetChildAt(0))).MinSize = new System.Drawing.Size(175, 0);
            // 
            // radPageViewPage7
            // 
            this.radPageViewPage7.Controls.Add(this.radButton14);
            this.radPageViewPage7.Controls.Add(this.radDateTimePicker2);
            this.radPageViewPage7.Controls.Add(this.radDateTimePicker1);
            this.radPageViewPage7.Controls.Add(this.radSpinEditor3);
            this.radPageViewPage7.Controls.Add(this.radSpinEditor5);
            this.radPageViewPage7.Controls.Add(this.radSpinEditor1);
            this.radPageViewPage7.Controls.Add(this.radSpinEditor2);
            this.radPageViewPage7.Controls.Add(this.radSpinEditor4);
            this.radPageViewPage7.Controls.Add(this.radLabel9);
            this.radPageViewPage7.Controls.Add(this.radLabel8);
            this.radPageViewPage7.Controls.Add(this.radLabel7);
            this.radPageViewPage7.Controls.Add(this.radLabel10);
            this.radPageViewPage7.Controls.Add(this.radLabel6);
            this.radPageViewPage7.Controls.Add(this.radButton5);
            this.radPageViewPage7.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage7.Name = "radPageViewPage7";
            this.radPageViewPage7.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage7.Text = "Temps, Humidity, Time";
            // 
            // radButton14
            // 
            this.radButton14.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton14.Location = new System.Drawing.Point(29, 332);
            this.radButton14.Name = "radButton14";
            // 
            // 
            // 
            this.radButton14.RootElement.ControlBounds = new System.Drawing.Rectangle(29, 332, 110, 24);
            this.radButton14.Size = new System.Drawing.Size(168, 24);
            this.radButton14.TabIndex = 2;
            this.radButton14.Text = "Get PC Time";
            this.radButton14.ThemeName = "Office2010Black";
            this.radButton14.Click += new System.EventHandler(this.radButton14_Click);
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDateTimePicker2.CustomFormat = "";
            this.radDateTimePicker2.Location = new System.Drawing.Point(211, 333);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            // 
            // 
            // 
            this.radDateTimePicker2.RootElement.ControlBounds = new System.Drawing.Rectangle(211, 333, 164, 20);
            this.radDateTimePicker2.RootElement.StretchVertically = true;
            this.radDateTimePicker2.ShowItemToolTips = false;
            this.radDateTimePicker2.ShowUpDown = true;
            this.radDateTimePicker2.Size = new System.Drawing.Size(175, 20);
            this.radDateTimePicker2.TabIndex = 35;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.ThemeName = "Office2010Black";
            this.radDateTimePicker2.Value = new System.DateTime(((long)(0)));
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDateTimePicker1.Location = new System.Drawing.Point(211, 292);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            // 
            // 
            // 
            this.radDateTimePicker1.RootElement.ControlBounds = new System.Drawing.Rectangle(211, 292, 164, 20);
            this.radDateTimePicker1.RootElement.StretchVertically = true;
            this.radDateTimePicker1.Size = new System.Drawing.Size(175, 20);
            this.radDateTimePicker1.TabIndex = 34;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.ThemeName = "Office2010Black";
            this.radDateTimePicker1.Value = new System.DateTime(((long)(0)));
            // 
            // radSpinEditor3
            // 
            this.radSpinEditor3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor3.DecimalPlaces = 2;
            this.radSpinEditor3.EnableKeyMap = true;
            this.radSpinEditor3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor3.Location = new System.Drawing.Point(173, 125);
            this.radSpinEditor3.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.radSpinEditor3.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.radSpinEditor3.Name = "radSpinEditor3";
            // 
            // 
            // 
            this.radSpinEditor3.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor3.RootElement.ControlBounds = new System.Drawing.Rectangle(173, 125, 100, 20);
            this.radSpinEditor3.RootElement.StretchVertically = true;
            this.radSpinEditor3.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor3.TabIndex = 29;
            this.radSpinEditor3.TabStop = false;
            this.radSpinEditor3.ThemeName = "Office2010Black";
            // 
            // radSpinEditor5
            // 
            this.radSpinEditor5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor5.DecimalPlaces = 1;
            this.radSpinEditor5.EnableKeyMap = true;
            this.radSpinEditor5.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditor5.Location = new System.Drawing.Point(172, 150);
            this.radSpinEditor5.Name = "radSpinEditor5";
            // 
            // 
            // 
            this.radSpinEditor5.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor5.RootElement.ControlBounds = new System.Drawing.Rectangle(172, 150, 100, 20);
            this.radSpinEditor5.RootElement.StretchVertically = true;
            this.radSpinEditor5.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor5.TabIndex = 31;
            this.radSpinEditor5.TabStop = false;
            this.radSpinEditor5.ThemeName = "Office2010Black";
            this.radSpinEditor5.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor1.DecimalPlaces = 2;
            this.radSpinEditor1.EnableKeyMap = true;
            this.radSpinEditor1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor1.Location = new System.Drawing.Point(173, 50);
            this.radSpinEditor1.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.radSpinEditor1.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.radSpinEditor1.Name = "radSpinEditor1";
            // 
            // 
            // 
            this.radSpinEditor1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor1.RootElement.ControlBounds = new System.Drawing.Rectangle(173, 50, 100, 20);
            this.radSpinEditor1.RootElement.StretchVertically = true;
            this.radSpinEditor1.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor1.TabIndex = 33;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.ThemeName = "Office2010Black";
            this.radSpinEditor1.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // radSpinEditor2
            // 
            this.radSpinEditor2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor2.DecimalPlaces = 2;
            this.radSpinEditor2.EnableKeyMap = true;
            this.radSpinEditor2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor2.Location = new System.Drawing.Point(173, 100);
            this.radSpinEditor2.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.radSpinEditor2.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.radSpinEditor2.Name = "radSpinEditor2";
            // 
            // 
            // 
            this.radSpinEditor2.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor2.RootElement.ControlBounds = new System.Drawing.Rectangle(173, 100, 100, 20);
            this.radSpinEditor2.RootElement.StretchVertically = true;
            this.radSpinEditor2.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor2.TabIndex = 32;
            this.radSpinEditor2.TabStop = false;
            this.radSpinEditor2.ThemeName = "Office2010Black";
            this.radSpinEditor2.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // radSpinEditor4
            // 
            this.radSpinEditor4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor4.DecimalPlaces = 2;
            this.radSpinEditor4.EnableKeyMap = true;
            this.radSpinEditor4.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor4.Location = new System.Drawing.Point(173, 76);
            this.radSpinEditor4.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.radSpinEditor4.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            -2147483648});
            this.radSpinEditor4.Name = "radSpinEditor4";
            // 
            // 
            // 
            this.radSpinEditor4.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor4.RootElement.ControlBounds = new System.Drawing.Rectangle(173, 76, 100, 20);
            this.radSpinEditor4.RootElement.StretchVertically = true;
            this.radSpinEditor4.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor4.TabIndex = 28;
            this.radSpinEditor4.TabStop = false;
            this.radSpinEditor4.ThemeName = "Office2010Black";
            this.radSpinEditor4.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            // 
            // radLabel9
            // 
            this.radLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel9.Location = new System.Drawing.Point(68, 100);
            this.radLabel9.Name = "radLabel9";
            // 
            // 
            // 
            this.radLabel9.RootElement.ControlBounds = new System.Drawing.Rectangle(68, 100, 100, 18);
            this.radLabel9.Size = new System.Drawing.Size(99, 18);
            this.radLabel9.TabIndex = 25;
            this.radLabel9.Text = "Temperature 3 (C):";
            // 
            // radLabel8
            // 
            this.radLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel8.Location = new System.Drawing.Point(68, 124);
            this.radLabel8.Name = "radLabel8";
            // 
            // 
            // 
            this.radLabel8.RootElement.ControlBounds = new System.Drawing.Rectangle(68, 124, 100, 18);
            this.radLabel8.Size = new System.Drawing.Size(99, 18);
            this.radLabel8.TabIndex = 24;
            this.radLabel8.Text = "Temperature 4 (C):";
            // 
            // radLabel7
            // 
            this.radLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel7.Location = new System.Drawing.Point(68, 76);
            this.radLabel7.Name = "radLabel7";
            // 
            // 
            // 
            this.radLabel7.RootElement.ControlBounds = new System.Drawing.Rectangle(68, 76, 100, 18);
            this.radLabel7.Size = new System.Drawing.Size(99, 18);
            this.radLabel7.TabIndex = 27;
            this.radLabel7.Text = "Temperature 2 (C):";
            // 
            // radLabel10
            // 
            this.radLabel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel10.Location = new System.Drawing.Point(93, 148);
            this.radLabel10.Name = "radLabel10";
            // 
            // 
            // 
            this.radLabel10.RootElement.ControlBounds = new System.Drawing.Rectangle(93, 148, 100, 18);
            this.radLabel10.Size = new System.Drawing.Size(74, 18);
            this.radLabel10.TabIndex = 26;
            this.radLabel10.Text = "Humidity (%):";
            // 
            // radLabel6
            // 
            this.radLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel6.Location = new System.Drawing.Point(68, 52);
            this.radLabel6.Name = "radLabel6";
            // 
            // 
            // 
            this.radLabel6.RootElement.ControlBounds = new System.Drawing.Rectangle(68, 52, 100, 18);
            this.radLabel6.Size = new System.Drawing.Size(99, 18);
            this.radLabel6.TabIndex = 23;
            this.radLabel6.Text = "Temperature 1 (C):";
            this.radLabel6.ThemeName = "ControlDefault";
            // 
            // radButton5
            // 
            this.radButton5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton5.Location = new System.Drawing.Point(29, 290);
            this.radButton5.Name = "radButton5";
            // 
            // 
            // 
            this.radButton5.RootElement.ControlBounds = new System.Drawing.Rectangle(29, 290, 110, 24);
            this.radButton5.Size = new System.Drawing.Size(168, 24);
            this.radButton5.TabIndex = 1;
            this.radButton5.Text = "Send Date and Time to DRSim";
            this.radButton5.ThemeName = "Office2010Black";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.winChartViewer1);
            this.radPageViewPage2.Controls.Add(this.radButton6);
            this.radPageViewPage2.Controls.Add(this.radGridView2);
            this.radPageViewPage2.Controls.Add(this.radGridView1);
            this.radPageViewPage2.Controls.Add(this.radLabel11);
            this.radPageViewPage2.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage2.Text = "Load Currents";
            // 
            // winChartViewer1
            // 
            this.winChartViewer1.Location = new System.Drawing.Point(524, 27);
            this.winChartViewer1.Name = "winChartViewer1";
            this.winChartViewer1.Size = new System.Drawing.Size(310, 227);
            this.winChartViewer1.TabIndex = 15;
            this.winChartViewer1.TabStop = false;
            // 
            // radButton6
            // 
            this.radButton6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton6.Location = new System.Drawing.Point(704, 271);
            this.radButton6.Name = "radButton6";
            // 
            // 
            // 
            this.radButton6.RootElement.ControlBounds = new System.Drawing.Rectangle(704, 271, 110, 24);
            this.radButton6.Size = new System.Drawing.Size(130, 24);
            this.radButton6.TabIndex = 14;
            this.radButton6.Text = "Update Chart";
            this.radButton6.ThemeName = "HighContrastBlack";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // radGridView2
            // 
            this.radGridView2.AutoSize = true;
            this.radGridView2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView2.Location = new System.Drawing.Point(0, 301);
            // 
            // radGridView2
            // 
            this.radGridView2.MasterTemplate.AllowAddNewRow = false;
            this.radGridView2.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnChooser = false;
            this.radGridView2.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnReorder = false;
            this.radGridView2.MasterTemplate.AllowColumnResize = false;
            this.radGridView2.MasterTemplate.AllowDeleteRow = false;
            this.radGridView2.MasterTemplate.AllowDragToGroup = false;
            this.radGridView2.MasterTemplate.AllowRowResize = false;
            this.radGridView2.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.AllowSort = false;
            gridViewTextBoxColumn1.HeaderText = "Channel";
            gridViewTextBoxColumn1.Name = "column9";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn2.HeaderText = "Output Value  Pri. Amps";
            gridViewTextBoxColumn2.Name = "column10";
            gridViewTextBoxColumn2.Width = 75;
            gridViewTextBoxColumn2.WrapText = true;
            gridViewDecimalColumn1.AllowSort = false;
            gridViewDecimalColumn1.HeaderText = "% Pri. Amps 1st Harm.";
            gridViewDecimalColumn1.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn1.Name = "column1";
            gridViewDecimalColumn1.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn1.Width = 75;
            gridViewDecimalColumn1.WrapText = true;
            gridViewDecimalColumn2.AllowSort = false;
            gridViewDecimalColumn2.HeaderText = "Angle (deg) 1st Harm.";
            gridViewDecimalColumn2.Maximum = new decimal(new int[] {
            35999,
            0,
            0,
            131072});
            gridViewDecimalColumn2.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn2.Name = "column2";
            gridViewDecimalColumn2.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn2.Width = 75;
            gridViewDecimalColumn2.WrapText = true;
            gridViewDecimalColumn3.AllowSort = false;
            gridViewDecimalColumn3.HeaderText = "% Pri. Amps 3rd Harm.";
            gridViewDecimalColumn3.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn3.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn3.Name = "column3";
            gridViewDecimalColumn3.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn3.Width = 75;
            gridViewDecimalColumn3.WrapText = true;
            gridViewDecimalColumn4.AllowSort = false;
            gridViewDecimalColumn4.HeaderText = "Angle (deg) 3rd Harm";
            gridViewDecimalColumn4.Maximum = new decimal(new int[] {
            35999,
            0,
            0,
            131072});
            gridViewDecimalColumn4.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn4.Name = "column4";
            gridViewDecimalColumn4.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn4.Width = 75;
            gridViewDecimalColumn4.WrapText = true;
            gridViewDecimalColumn5.AllowSort = false;
            gridViewDecimalColumn5.HeaderText = "% Pri. Amps 5th Harm";
            gridViewDecimalColumn5.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn5.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn5.Name = "column5";
            gridViewDecimalColumn5.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn5.Width = 75;
            gridViewDecimalColumn5.WrapText = true;
            gridViewDecimalColumn6.AllowSort = false;
            gridViewDecimalColumn6.HeaderText = "Angle (deg) 5th Harm.";
            gridViewDecimalColumn6.Maximum = new decimal(new int[] {
            3599,
            0,
            0,
            65536});
            gridViewDecimalColumn6.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn6.Name = "column6";
            gridViewDecimalColumn6.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn6.Width = 75;
            gridViewDecimalColumn6.WrapText = true;
            gridViewDecimalColumn7.AllowSort = false;
            gridViewDecimalColumn7.HeaderText = "%Pri. Amps 7th Harm";
            gridViewDecimalColumn7.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn7.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn7.Name = "column7";
            gridViewDecimalColumn7.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn7.Width = 75;
            gridViewDecimalColumn7.WrapText = true;
            gridViewDecimalColumn8.AllowSort = false;
            gridViewDecimalColumn8.HeaderText = "Angle (deg) 7th Harm.";
            gridViewDecimalColumn8.Maximum = new decimal(new int[] {
            35999,
            0,
            0,
            131072});
            gridViewDecimalColumn8.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn8.Name = "column8";
            gridViewDecimalColumn8.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn8.Width = 75;
            gridViewDecimalColumn8.WrapText = true;
            gridViewComboBoxColumn1.HeaderText = "Waveform Type";
            gridViewComboBoxColumn1.Name = "column11";
            gridViewComboBoxColumn1.Width = 90;
            gridViewComboBoxColumn1.WrapText = true;
            this.radGridView2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewDecimalColumn1,
            gridViewDecimalColumn2,
            gridViewDecimalColumn3,
            gridViewDecimalColumn4,
            gridViewDecimalColumn5,
            gridViewDecimalColumn6,
            gridViewDecimalColumn7,
            gridViewDecimalColumn8,
            gridViewComboBoxColumn1});
            this.radGridView2.MasterTemplate.EnableGrouping = false;
            this.radGridView2.MasterTemplate.ShowFilteringRow = false;
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.NewRowEnterKeyMode = Telerik.WinControls.UI.RadGridViewNewRowEnterKeyMode.EnterMovesToNextCell;
            // 
            // 
            // 
            this.radGridView2.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 301, 240, 150);
            this.radGridView2.ShowGroupPanel = false;
            this.radGridView2.Size = new System.Drawing.Size(826, 30);
            this.radGridView2.TabIndex = 13;
            this.radGridView2.Text = "radGridView2";
            this.radGridView2.ThemeName = "Office2010Black";
            this.radGridView2.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.radGridView2_CurrentRowChanged);
            // 
            // radGridView1
            // 
            this.radGridView1.AutoSize = true;
            this.radGridView1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView1.Location = new System.Drawing.Point(3, 27);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterTemplate.AllowColumnResize = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn3.HeaderText = "Channel";
            gridViewTextBoxColumn3.Name = "column7";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.WrapText = true;
            gridViewTextBoxColumn4.HeaderText = "Pri. CT Ratio";
            gridViewTextBoxColumn4.Multiline = true;
            gridViewTextBoxColumn4.Name = "column1";
            gridViewTextBoxColumn4.Width = 75;
            gridViewTextBoxColumn4.WrapText = true;
            gridViewTextBoxColumn5.HeaderText = "Aux, CT Ratio";
            gridViewTextBoxColumn5.Name = "column2";
            gridViewTextBoxColumn5.Width = 75;
            gridViewTextBoxColumn5.WrapText = true;
            gridViewTextBoxColumn6.HeaderText = "Burden (Ohms)";
            gridViewTextBoxColumn6.Multiline = true;
            gridViewTextBoxColumn6.Name = "column3";
            gridViewTextBoxColumn6.Width = 75;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.Expression = "";
            gridViewTextBoxColumn7.HeaderText = "Aux CT Sensitivity (mV/A)";
            gridViewTextBoxColumn7.Name = "column4";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn7.WrapText = true;
            gridViewTextBoxColumn8.Expression = "";
            gridViewTextBoxColumn8.FormatString = "{0:0.000}";
            gridViewTextBoxColumn8.HeaderText = "Overall Sensitivity (A/mv)";
            gridViewTextBoxColumn8.Multiline = true;
            gridViewTextBoxColumn8.Name = "column5";
            gridViewTextBoxColumn8.Width = 100;
            gridViewTextBoxColumn8.WrapText = true;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.radGridView1.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridView1.MasterTemplate.EnableGrouping = false;
            this.radGridView1.MasterTemplate.EnableSorting = false;
            this.radGridView1.MasterTemplate.ShowFilteringRow = false;
            this.radGridView1.Name = "radGridView1";
            // 
            // 
            // 
            this.radGridView1.RootElement.ControlBounds = new System.Drawing.Rectangle(3, 27, 240, 150);
            this.radGridView1.ShowGroupPanel = false;
            this.radGridView1.Size = new System.Drawing.Size(491, 30);
            this.radGridView1.TabIndex = 12;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.ThemeName = "Office2010Black";
            this.radGridView1.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGridView1_CellFormatting);
            this.radGridView1.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView1_CellEndEdit);
            // 
            // radLabel11
            // 
            this.radLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel11.Location = new System.Drawing.Point(3, 3);
            this.radLabel11.Name = "radLabel11";
            // 
            // 
            // 
            this.radLabel11.RootElement.ControlBounds = new System.Drawing.Rectangle(3, 3, 100, 18);
            this.radLabel11.Size = new System.Drawing.Size(56, 18);
            this.radLabel11.TabIndex = 5;
            this.radLabel11.Text = "Calculator";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.radGroupBox5);
            this.radPageViewPage3.Controls.Add(this.radGroupBox3);
            this.radPageViewPage3.Controls.Add(this.radGroupBox4);
            this.radPageViewPage3.Controls.Add(this.radGroupBox2);
            this.radPageViewPage3.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage3.Text = "Bushings";
            // 
            // radGroupBox5
            // 
            this.radGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radGroupBox5.Controls.Add(this.radGridView8);
            this.radGroupBox5.Controls.Add(this.radLabel23);
            this.radGroupBox5.Controls.Add(this.radTextBox7);
            this.radGroupBox5.HeaderText = "Set 4";
            this.radGroupBox5.Location = new System.Drawing.Point(14, 457);
            this.radGroupBox5.Name = "radGroupBox5";
            // 
            // 
            // 
            this.radGroupBox5.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 457, 200, 100);
            this.radGroupBox5.Size = new System.Drawing.Size(821, 148);
            this.radGroupBox5.TabIndex = 8;
            this.radGroupBox5.Text = "Set 4";
            this.radGroupBox5.ThemeName = "Office2010Black";
            // 
            // radGridView8
            // 
            this.radGridView8.AutoSize = true;
            this.radGridView8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView8.Location = new System.Drawing.Point(152, 34);
            // 
            // radGridView8
            // 
            this.radGridView8.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView8.MasterTemplate.AllowAddNewRow = false;
            this.radGridView8.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView8.MasterTemplate.AllowColumnChooser = false;
            this.radGridView8.MasterTemplate.AllowColumnReorder = false;
            this.radGridView8.MasterTemplate.AllowColumnResize = false;
            this.radGridView8.MasterTemplate.AllowDeleteRow = false;
            this.radGridView8.MasterTemplate.AllowDragToGroup = false;
            this.radGridView8.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn9.HeaderText = "Output";
            gridViewTextBoxColumn9.Name = "Output";
            gridViewComboBoxColumn2.HeaderText = "Phase";
            gridViewComboBoxColumn2.Name = "Phase";
            gridViewDecimalColumn9.HeaderText = "Initial Capacitance (pF)";
            gridViewDecimalColumn9.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            gridViewDecimalColumn9.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn9.Name = "Cap";
            gridViewDecimalColumn9.Width = 100;
            gridViewDecimalColumn9.WrapText = true;
            gridViewDecimalColumn10.HeaderText = "Initial % Power Factor";
            gridViewDecimalColumn10.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn10.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn10.Name = "PF";
            gridViewDecimalColumn10.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn10.Width = 100;
            gridViewDecimalColumn10.WrapText = true;
            gridViewDecimalColumn11.HeaderText = "Change in Capacitance (pF)";
            gridViewDecimalColumn11.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            gridViewDecimalColumn11.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn11.Name = "NewCap";
            gridViewDecimalColumn11.Width = 100;
            gridViewDecimalColumn11.WrapText = true;
            gridViewDecimalColumn12.HeaderText = "Change % PF";
            gridViewDecimalColumn12.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn12.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn12.Name = "column2";
            gridViewDecimalColumn12.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn12.Width = 100;
            gridViewTextBoxColumn10.HeaderText = "Current (ma)";
            gridViewTextBoxColumn10.Name = "NewCurrent";
            gridViewTextBoxColumn10.Width = 70;
            gridViewTextBoxColumn10.WrapText = true;
            gridViewTextBoxColumn11.HeaderText = "Phase Angle";
            gridViewTextBoxColumn11.Name = "NewPhase";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 75;
            this.radGridView8.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewComboBoxColumn2,
            gridViewDecimalColumn9,
            gridViewDecimalColumn10,
            gridViewDecimalColumn11,
            gridViewDecimalColumn12,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11});
            this.radGridView8.MasterTemplate.ShowFilteringRow = false;
            this.radGridView8.Name = "radGridView8";
            // 
            // 
            // 
            this.radGridView8.RootElement.ControlBounds = new System.Drawing.Rectangle(152, 34, 240, 150);
            this.radGridView8.ShowGroupPanel = false;
            this.radGridView8.Size = new System.Drawing.Size(659, 30);
            this.radGridView8.TabIndex = 6;
            this.radGridView8.Text = "radGridView8";
            this.radGridView8.ThemeName = "Office2010Black";
            this.radGridView8.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView8_CellEndEdit);
            // 
            // radLabel23
            // 
            this.radLabel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel23.Location = new System.Drawing.Point(16, 34);
            this.radLabel23.Name = "radLabel23";
            // 
            // 
            // 
            this.radLabel23.RootElement.ControlBounds = new System.Drawing.Rectangle(16, 34, 100, 18);
            this.radLabel23.Size = new System.Drawing.Size(70, 18);
            this.radLabel23.TabIndex = 5;
            this.radLabel23.Text = "Voltage (kV):";
            // 
            // radTextBox7
            // 
            this.radTextBox7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox7.Location = new System.Drawing.Point(92, 34);
            this.radTextBox7.Name = "radTextBox7";
            // 
            // 
            // 
            this.radTextBox7.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 34, 100, 20);
            this.radTextBox7.RootElement.StretchVertically = true;
            this.radTextBox7.Size = new System.Drawing.Size(54, 20);
            this.radTextBox7.TabIndex = 0;
            this.radTextBox7.TabStop = false;
            this.radTextBox7.ThemeName = "Office2010Black";
            this.radTextBox7.TextChanged += new System.EventHandler(this.radTextBox7_TextChanged);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radGroupBox3.Controls.Add(this.radGridView6);
            this.radGroupBox3.Controls.Add(this.radLabel21);
            this.radGroupBox3.Controls.Add(this.radTextBox5);
            this.radGroupBox3.HeaderText = "Set 2";
            this.radGroupBox3.Location = new System.Drawing.Point(14, 154);
            this.radGroupBox3.Name = "radGroupBox3";
            // 
            // 
            // 
            this.radGroupBox3.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 154, 200, 100);
            this.radGroupBox3.Size = new System.Drawing.Size(821, 148);
            this.radGroupBox3.TabIndex = 7;
            this.radGroupBox3.Text = "Set 2";
            this.radGroupBox3.ThemeName = "Office2010Black";
            // 
            // radGridView6
            // 
            this.radGridView6.AutoSize = true;
            this.radGridView6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView6.Location = new System.Drawing.Point(152, 34);
            // 
            // radGridView6
            // 
            this.radGridView6.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView6.MasterTemplate.AllowAddNewRow = false;
            this.radGridView6.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView6.MasterTemplate.AllowColumnChooser = false;
            this.radGridView6.MasterTemplate.AllowColumnReorder = false;
            this.radGridView6.MasterTemplate.AllowColumnResize = false;
            this.radGridView6.MasterTemplate.AllowDeleteRow = false;
            this.radGridView6.MasterTemplate.AllowDragToGroup = false;
            this.radGridView6.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn12.HeaderText = "Output";
            gridViewTextBoxColumn12.Name = "Output";
            gridViewComboBoxColumn3.HeaderText = "Phase";
            gridViewComboBoxColumn3.Name = "Phase";
            gridViewDecimalColumn13.HeaderText = "Initial Capacitance (pF)";
            gridViewDecimalColumn13.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            gridViewDecimalColumn13.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn13.Name = "Cap";
            gridViewDecimalColumn13.Width = 100;
            gridViewDecimalColumn13.WrapText = true;
            gridViewDecimalColumn14.HeaderText = "Initial % Power Factor";
            gridViewDecimalColumn14.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn14.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn14.Name = "PF";
            gridViewDecimalColumn14.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn14.Width = 100;
            gridViewDecimalColumn14.WrapText = true;
            gridViewDecimalColumn15.DecimalPlaces = 0;
            gridViewDecimalColumn15.HeaderText = "Change in Capacitance (pF)";
            gridViewDecimalColumn15.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            gridViewDecimalColumn15.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn15.Name = "NewCap";
            gridViewDecimalColumn15.Width = 100;
            gridViewDecimalColumn15.WrapText = true;
            gridViewDecimalColumn16.HeaderText = "Change % PF";
            gridViewDecimalColumn16.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn16.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn16.Name = "column2";
            gridViewDecimalColumn16.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn16.Width = 100;
            gridViewTextBoxColumn13.HeaderText = "Current (ma)";
            gridViewTextBoxColumn13.Name = "NewCurrent";
            gridViewTextBoxColumn13.Width = 70;
            gridViewTextBoxColumn13.WrapText = true;
            gridViewTextBoxColumn14.HeaderText = "Phase Angle";
            gridViewTextBoxColumn14.Name = "NewPhase";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.Width = 75;
            this.radGridView6.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn12,
            gridViewComboBoxColumn3,
            gridViewDecimalColumn13,
            gridViewDecimalColumn14,
            gridViewDecimalColumn15,
            gridViewDecimalColumn16,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.radGridView6.MasterTemplate.ShowFilteringRow = false;
            this.radGridView6.Name = "radGridView6";
            // 
            // 
            // 
            this.radGridView6.RootElement.ControlBounds = new System.Drawing.Rectangle(152, 34, 240, 150);
            this.radGridView6.ShowGroupPanel = false;
            this.radGridView6.Size = new System.Drawing.Size(659, 30);
            this.radGridView6.TabIndex = 6;
            this.radGridView6.Text = "radGridView6";
            this.radGridView6.ThemeName = "Office2010Black";
            this.radGridView6.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView6_CellEndEdit);
            // 
            // radLabel21
            // 
            this.radLabel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel21.Location = new System.Drawing.Point(16, 34);
            this.radLabel21.Name = "radLabel21";
            // 
            // 
            // 
            this.radLabel21.RootElement.ControlBounds = new System.Drawing.Rectangle(16, 34, 100, 18);
            this.radLabel21.Size = new System.Drawing.Size(70, 18);
            this.radLabel21.TabIndex = 5;
            this.radLabel21.Text = "Voltage (kV):";
            // 
            // radTextBox5
            // 
            this.radTextBox5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox5.Location = new System.Drawing.Point(92, 34);
            this.radTextBox5.Name = "radTextBox5";
            // 
            // 
            // 
            this.radTextBox5.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 34, 100, 20);
            this.radTextBox5.RootElement.StretchVertically = true;
            this.radTextBox5.Size = new System.Drawing.Size(54, 20);
            this.radTextBox5.TabIndex = 0;
            this.radTextBox5.TabStop = false;
            this.radTextBox5.ThemeName = "Office2010Black";
            this.radTextBox5.TextChanged += new System.EventHandler(this.radTextBox5_TextChanged);
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radGroupBox4.Controls.Add(this.radGridView7);
            this.radGroupBox4.Controls.Add(this.radLabel22);
            this.radGroupBox4.Controls.Add(this.radTextBox6);
            this.radGroupBox4.HeaderText = "Set 3";
            this.radGroupBox4.Location = new System.Drawing.Point(14, 308);
            this.radGroupBox4.Name = "radGroupBox4";
            // 
            // 
            // 
            this.radGroupBox4.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 308, 200, 100);
            this.radGroupBox4.Size = new System.Drawing.Size(821, 148);
            this.radGroupBox4.TabIndex = 7;
            this.radGroupBox4.Text = "Set 3";
            this.radGroupBox4.ThemeName = "Office2010Black";
            // 
            // radGridView7
            // 
            this.radGridView7.AutoSize = true;
            this.radGridView7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView7.Location = new System.Drawing.Point(152, 34);
            // 
            // radGridView7
            // 
            this.radGridView7.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView7.MasterTemplate.AllowAddNewRow = false;
            this.radGridView7.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView7.MasterTemplate.AllowColumnChooser = false;
            this.radGridView7.MasterTemplate.AllowColumnReorder = false;
            this.radGridView7.MasterTemplate.AllowColumnResize = false;
            this.radGridView7.MasterTemplate.AllowDeleteRow = false;
            this.radGridView7.MasterTemplate.AllowDragToGroup = false;
            this.radGridView7.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn15.HeaderText = "Output";
            gridViewTextBoxColumn15.Name = "Output";
            gridViewComboBoxColumn4.HeaderText = "Phase";
            gridViewComboBoxColumn4.Name = "Phase";
            gridViewDecimalColumn17.HeaderText = "Initial Capacitance (pF)";
            gridViewDecimalColumn17.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            gridViewDecimalColumn17.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn17.Name = "Cap";
            gridViewDecimalColumn17.Width = 100;
            gridViewDecimalColumn17.WrapText = true;
            gridViewDecimalColumn18.HeaderText = "Initial % Power Factor";
            gridViewDecimalColumn18.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn18.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn18.Name = "PF";
            gridViewDecimalColumn18.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn18.Width = 100;
            gridViewDecimalColumn18.WrapText = true;
            gridViewDecimalColumn19.DecimalPlaces = 0;
            gridViewDecimalColumn19.HeaderText = "Change in Capacitance (pF)";
            gridViewDecimalColumn19.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            gridViewDecimalColumn19.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn19.Name = "NewCap";
            gridViewDecimalColumn19.Width = 100;
            gridViewDecimalColumn19.WrapText = true;
            gridViewDecimalColumn20.HeaderText = "Change % PF";
            gridViewDecimalColumn20.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn20.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn20.Name = "column2";
            gridViewDecimalColumn20.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn20.Width = 100;
            gridViewTextBoxColumn16.HeaderText = "Current (ma)";
            gridViewTextBoxColumn16.Name = "NewCurrent";
            gridViewTextBoxColumn16.Width = 70;
            gridViewTextBoxColumn16.WrapText = true;
            gridViewTextBoxColumn17.HeaderText = "Phase Angle";
            gridViewTextBoxColumn17.Name = "NewPhase";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 75;
            this.radGridView7.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewComboBoxColumn4,
            gridViewDecimalColumn17,
            gridViewDecimalColumn18,
            gridViewDecimalColumn19,
            gridViewDecimalColumn20,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.radGridView7.MasterTemplate.ShowFilteringRow = false;
            this.radGridView7.Name = "radGridView7";
            // 
            // 
            // 
            this.radGridView7.RootElement.ControlBounds = new System.Drawing.Rectangle(152, 34, 240, 150);
            this.radGridView7.ShowGroupPanel = false;
            this.radGridView7.Size = new System.Drawing.Size(659, 30);
            this.radGridView7.TabIndex = 6;
            this.radGridView7.Text = "radGridView7";
            this.radGridView7.ThemeName = "Office2010Black";
            this.radGridView7.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView7_CellEndEdit);
            // 
            // radLabel22
            // 
            this.radLabel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel22.Location = new System.Drawing.Point(16, 34);
            this.radLabel22.Name = "radLabel22";
            // 
            // 
            // 
            this.radLabel22.RootElement.ControlBounds = new System.Drawing.Rectangle(16, 34, 100, 18);
            this.radLabel22.Size = new System.Drawing.Size(70, 18);
            this.radLabel22.TabIndex = 5;
            this.radLabel22.Text = "Voltage (kV):";
            // 
            // radTextBox6
            // 
            this.radTextBox6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox6.Location = new System.Drawing.Point(92, 34);
            this.radTextBox6.Name = "radTextBox6";
            // 
            // 
            // 
            this.radTextBox6.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 34, 100, 20);
            this.radTextBox6.RootElement.StretchVertically = true;
            this.radTextBox6.Size = new System.Drawing.Size(54, 20);
            this.radTextBox6.TabIndex = 0;
            this.radTextBox6.TabStop = false;
            this.radTextBox6.ThemeName = "Office2010Black";
            this.radTextBox6.TextChanged += new System.EventHandler(this.radTextBox6_TextChanged);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radGroupBox2.Controls.Add(this.radGridView5);
            this.radGroupBox2.Controls.Add(this.radLabel20);
            this.radGroupBox2.Controls.Add(this.radTextBox4);
            this.radGroupBox2.HeaderText = "Set 1";
            this.radGroupBox2.Location = new System.Drawing.Point(14, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            // 
            // 
            // 
            this.radGroupBox2.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 0, 200, 100);
            this.radGroupBox2.Size = new System.Drawing.Size(821, 148);
            this.radGroupBox2.TabIndex = 0;
            this.radGroupBox2.Text = "Set 1";
            this.radGroupBox2.ThemeName = "Office2010Black";
            // 
            // radGridView5
            // 
            this.radGridView5.AutoSize = true;
            this.radGridView5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView5.Location = new System.Drawing.Point(152, 34);
            // 
            // radGridView5
            // 
            this.radGridView5.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView5.MasterTemplate.AllowAddNewRow = false;
            this.radGridView5.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView5.MasterTemplate.AllowColumnChooser = false;
            this.radGridView5.MasterTemplate.AllowColumnReorder = false;
            this.radGridView5.MasterTemplate.AllowColumnResize = false;
            this.radGridView5.MasterTemplate.AllowDeleteRow = false;
            this.radGridView5.MasterTemplate.AllowDragToGroup = false;
            this.radGridView5.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn18.HeaderText = "Output";
            gridViewTextBoxColumn18.Name = "Output";
            gridViewComboBoxColumn5.HeaderText = "Phase";
            gridViewComboBoxColumn5.Name = "Phase";
            gridViewDecimalColumn21.HeaderText = "Initial Capacitance (pF)";
            gridViewDecimalColumn21.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            gridViewDecimalColumn21.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn21.Name = "Cap";
            gridViewDecimalColumn21.Width = 100;
            gridViewDecimalColumn21.WrapText = true;
            gridViewDecimalColumn22.HeaderText = "Initial % Power Factor";
            gridViewDecimalColumn22.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn22.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn22.Name = "PF";
            gridViewDecimalColumn22.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn22.Width = 100;
            gridViewDecimalColumn22.WrapText = true;
            gridViewDecimalColumn23.DecimalPlaces = 0;
            gridViewDecimalColumn23.HeaderText = "Change in Capacitance (pF)";
            gridViewDecimalColumn23.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            gridViewDecimalColumn23.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn23.Name = "NewCap";
            gridViewDecimalColumn23.Width = 100;
            gridViewDecimalColumn23.WrapText = true;
            gridViewDecimalColumn24.HeaderText = "Change % PF";
            gridViewDecimalColumn24.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            gridViewDecimalColumn24.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            gridViewDecimalColumn24.Name = "column2";
            gridViewDecimalColumn24.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn24.Width = 100;
            gridViewTextBoxColumn19.HeaderText = "Current (ma)";
            gridViewTextBoxColumn19.Name = "NewCurrent";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.Width = 70;
            gridViewTextBoxColumn19.WrapText = true;
            gridViewTextBoxColumn20.HeaderText = "Phase Angle";
            gridViewTextBoxColumn20.Name = "NewPhase";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.Width = 75;
            this.radGridView5.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn18,
            gridViewComboBoxColumn5,
            gridViewDecimalColumn21,
            gridViewDecimalColumn22,
            gridViewDecimalColumn23,
            gridViewDecimalColumn24,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20});
            this.radGridView5.MasterTemplate.ShowFilteringRow = false;
            this.radGridView5.Name = "radGridView5";
            // 
            // 
            // 
            this.radGridView5.RootElement.ControlBounds = new System.Drawing.Rectangle(152, 34, 240, 150);
            this.radGridView5.ShowGroupPanel = false;
            this.radGridView5.Size = new System.Drawing.Size(659, 30);
            this.radGridView5.TabIndex = 6;
            this.radGridView5.Text = "radGridView5";
            this.radGridView5.ThemeName = "Office2010Black";
            this.radGridView5.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridView5_CellEndEdit);
            // 
            // radLabel20
            // 
            this.radLabel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel20.Location = new System.Drawing.Point(16, 34);
            this.radLabel20.Name = "radLabel20";
            // 
            // 
            // 
            this.radLabel20.RootElement.ControlBounds = new System.Drawing.Rectangle(16, 34, 100, 18);
            this.radLabel20.Size = new System.Drawing.Size(70, 18);
            this.radLabel20.TabIndex = 5;
            this.radLabel20.Text = "Voltage (kV):";
            // 
            // radTextBox4
            // 
            this.radTextBox4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox4.Location = new System.Drawing.Point(92, 34);
            this.radTextBox4.Name = "radTextBox4";
            // 
            // 
            // 
            this.radTextBox4.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 34, 100, 20);
            this.radTextBox4.RootElement.StretchVertically = true;
            this.radTextBox4.Size = new System.Drawing.Size(54, 20);
            this.radTextBox4.TabIndex = 0;
            this.radTextBox4.TabStop = false;
            this.radTextBox4.ThemeName = "Office2010Black";
            this.radTextBox4.TextChanged += new System.EventHandler(this.radTextBox4_TextChanged);
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.Controls.Add(this.radButton15);
            this.radPageViewPage4.Controls.Add(this.winChartViewer2);
            this.radPageViewPage4.Controls.Add(this.radButton7);
            this.radPageViewPage4.Controls.Add(this.radGridView3);
            this.radPageViewPage4.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage4.Text = "Advanced Bushing Control";
            // 
            // radButton15
            // 
            this.radButton15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton15.Location = new System.Drawing.Point(18, 160);
            this.radButton15.Name = "radButton15";
            // 
            // 
            // 
            this.radButton15.RootElement.ControlBounds = new System.Drawing.Rectangle(18, 160, 110, 24);
            this.radButton15.Size = new System.Drawing.Size(130, 24);
            this.radButton15.TabIndex = 16;
            this.radButton15.Text = "Reset to Default Values";
            this.radButton15.ThemeName = "HighContrastBlack";
            this.radButton15.Click += new System.EventHandler(this.radButton15_Click);
            // 
            // winChartViewer2
            // 
            this.winChartViewer2.Location = new System.Drawing.Point(530, 3);
            this.winChartViewer2.Name = "winChartViewer2";
            this.winChartViewer2.Size = new System.Drawing.Size(294, 181);
            this.winChartViewer2.TabIndex = 16;
            this.winChartViewer2.TabStop = false;
            // 
            // radButton7
            // 
            this.radButton7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton7.Location = new System.Drawing.Point(387, 3);
            this.radButton7.Name = "radButton7";
            // 
            // 
            // 
            this.radButton7.RootElement.ControlBounds = new System.Drawing.Rectangle(387, 3, 110, 24);
            this.radButton7.Size = new System.Drawing.Size(130, 24);
            this.radButton7.TabIndex = 15;
            this.radButton7.Text = "Update Chart";
            this.radButton7.ThemeName = "HighContrastBlack";
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click_1);
            // 
            // radGridView3
            // 
            this.radGridView3.AutoSize = true;
            this.radGridView3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView3.EnableTheming = false;
            this.radGridView3.GroupExpandAnimationType = Telerik.WinControls.UI.GridExpandAnimationType.Accordion;
            this.radGridView3.Location = new System.Drawing.Point(18, 190);
            // 
            // radGridView3
            // 
            this.radGridView3.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView3.MasterTemplate.AllowAddNewRow = false;
            this.radGridView3.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView3.MasterTemplate.AllowColumnChooser = false;
            this.radGridView3.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView3.MasterTemplate.AllowColumnReorder = false;
            this.radGridView3.MasterTemplate.AllowColumnResize = false;
            this.radGridView3.MasterTemplate.AllowDeleteRow = false;
            this.radGridView3.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn21.HeaderText = "Set";
            gridViewTextBoxColumn21.Name = "Set";
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.HeaderText = "SensorID";
            gridViewTextBoxColumn22.Name = "SensorID";
            gridViewDecimalColumn25.DecimalPlaces = 1;
            gridViewDecimalColumn25.HeaderText = "Burden";
            gridViewDecimalColumn25.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            gridViewDecimalColumn25.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn25.Name = "Burden";
            gridViewDecimalColumn25.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            gridViewDecimalColumn25.WrapText = true;
            gridViewDecimalColumn26.HeaderText = "1st Harm Mag. (ma)";
            gridViewDecimalColumn26.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn26.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn26.Name = "Harm1Amp";
            gridViewDecimalColumn26.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn26.Width = 75;
            gridViewDecimalColumn26.WrapText = true;
            gridViewDecimalColumn27.HeaderText = "Angle (deg) 1st Harm";
            gridViewDecimalColumn27.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            gridViewDecimalColumn27.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn27.Name = "Harm1PS";
            gridViewDecimalColumn27.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn27.Width = 75;
            gridViewDecimalColumn27.WrapText = true;
            gridViewDecimalColumn28.HeaderText = "3rd Harm. Mag. (ma)";
            gridViewDecimalColumn28.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn28.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn28.Name = "Harm3Amp";
            gridViewDecimalColumn28.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn28.Width = 75;
            gridViewDecimalColumn28.WrapText = true;
            gridViewDecimalColumn29.HeaderText = "Angle (deg) 3rd Harm";
            gridViewDecimalColumn29.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            gridViewDecimalColumn29.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn29.Name = "Harm3PS";
            gridViewDecimalColumn29.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn29.Width = 75;
            gridViewDecimalColumn29.WrapText = true;
            gridViewDecimalColumn30.HeaderText = "5th Harm. Mag. (ma)";
            gridViewDecimalColumn30.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn30.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn30.Name = "Harm5Amp";
            gridViewDecimalColumn30.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn30.Width = 75;
            gridViewDecimalColumn30.WrapText = true;
            gridViewDecimalColumn31.HeaderText = "Angle (deg) 5th Harm";
            gridViewDecimalColumn31.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            gridViewDecimalColumn31.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn31.Name = "Harm5PS";
            gridViewDecimalColumn31.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn31.Width = 75;
            gridViewDecimalColumn31.WrapText = true;
            gridViewDecimalColumn32.HeaderText = "7th Harm Mag. (ma)";
            gridViewDecimalColumn32.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            gridViewDecimalColumn32.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn32.Name = "Harm7Amp";
            gridViewDecimalColumn32.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn32.Width = 75;
            gridViewDecimalColumn32.WrapText = true;
            gridViewDecimalColumn33.HeaderText = "Angle (deg) 7th Harm";
            gridViewDecimalColumn33.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            gridViewDecimalColumn33.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn33.Name = "Harm7PS";
            gridViewDecimalColumn33.Step = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            gridViewDecimalColumn33.Width = 75;
            gridViewDecimalColumn33.WrapText = true;
            gridViewComboBoxColumn6.HeaderText = "Waveform";
            gridViewComboBoxColumn6.Name = "WaveType";
            gridViewComboBoxColumn6.Width = 75;
            gridViewComboBoxColumn6.WrapText = true;
            this.radGridView3.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewDecimalColumn25,
            gridViewDecimalColumn26,
            gridViewDecimalColumn27,
            gridViewDecimalColumn28,
            gridViewDecimalColumn29,
            gridViewDecimalColumn30,
            gridViewDecimalColumn31,
            gridViewDecimalColumn32,
            gridViewDecimalColumn33,
            gridViewComboBoxColumn6});
            this.radGridView3.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridView3.MasterTemplate.EnableSorting = false;
            sortDescriptor1.PropertyName = "Set";
            groupDescriptor1.GroupNames.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.radGridView3.MasterTemplate.GroupDescriptors.AddRange(new Telerik.WinControls.Data.GroupDescriptor[] {
            groupDescriptor1});
            this.radGridView3.Name = "radGridView3";
            // 
            // 
            // 
            this.radGridView3.RootElement.ControlBounds = new System.Drawing.Rectangle(18, 190, 240, 150);
            this.radGridView3.ShowGroupPanel = false;
            this.radGridView3.Size = new System.Drawing.Size(807, 30);
            this.radGridView3.TabIndex = 0;
            this.radGridView3.Text = "radGridView3";
            this.radGridView3.ThemeName = "Office2010Black";
            this.radGridView3.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.radGridView3_CurrentRowChanged);
            // 
            // radPageViewPage5
            // 
            this.radPageViewPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radPageViewPage5.Controls.Add(this.radLabel33);
            this.radPageViewPage5.Controls.Add(this.radLabel32);
            this.radPageViewPage5.Controls.Add(this.radSpinEditor13);
            this.radPageViewPage5.Controls.Add(this.radSpinEditor12);
            this.radPageViewPage5.Controls.Add(this.radGroupBox1);
            this.radPageViewPage5.Controls.Add(this.radGridView4);
            this.radPageViewPage5.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage5.Name = "radPageViewPage5";
            this.radPageViewPage5.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage5.Text = "PD Settings";
            // 
            // radLabel33
            // 
            this.radLabel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel33.Location = new System.Drawing.Point(34, 238);
            this.radLabel33.Name = "radLabel33";
            // 
            // 
            // 
            this.radLabel33.RootElement.ControlBounds = new System.Drawing.Rectangle(34, 238, 100, 18);
            this.radLabel33.Size = new System.Drawing.Size(118, 18);
            this.radLabel33.TabIndex = 32;
            this.radLabel33.Text = "PD Pulse Width Range";
            // 
            // radLabel32
            // 
            this.radLabel32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel32.Location = new System.Drawing.Point(22, 191);
            this.radLabel32.Name = "radLabel32";
            // 
            // 
            // 
            this.radLabel32.RootElement.ControlBounds = new System.Drawing.Rectangle(22, 191, 100, 18);
            this.radLabel32.Size = new System.Drawing.Size(130, 18);
            this.radLabel32.TabIndex = 6;
            this.radLabel32.Text = "PD Pulse Repetition Rate";
            // 
            // radSpinEditor13
            // 
            this.radSpinEditor13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor13.EnableKeyMap = true;
            this.radSpinEditor13.Location = new System.Drawing.Point(158, 238);
            this.radSpinEditor13.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.radSpinEditor13.Name = "radSpinEditor13";
            // 
            // 
            // 
            this.radSpinEditor13.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor13.RootElement.ControlBounds = new System.Drawing.Rectangle(158, 238, 100, 20);
            this.radSpinEditor13.RootElement.StretchVertically = true;
            this.radSpinEditor13.Size = new System.Drawing.Size(55, 20);
            this.radSpinEditor13.TabIndex = 33;
            this.radSpinEditor13.TabStop = false;
            this.radSpinEditor13.ThemeName = "Office2010Black";
            this.radSpinEditor13.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            // 
            // radSpinEditor12
            // 
            this.radSpinEditor12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor12.EnableKeyMap = true;
            this.radSpinEditor12.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.radSpinEditor12.Location = new System.Drawing.Point(158, 191);
            this.radSpinEditor12.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.radSpinEditor12.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.radSpinEditor12.Name = "radSpinEditor12";
            // 
            // 
            // 
            this.radSpinEditor12.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor12.RootElement.ControlBounds = new System.Drawing.Rectangle(158, 191, 100, 20);
            this.radSpinEditor12.RootElement.StretchVertically = true;
            this.radSpinEditor12.Size = new System.Drawing.Size(85, 20);
            this.radSpinEditor12.TabIndex = 31;
            this.radSpinEditor12.TabStop = false;
            this.radSpinEditor12.ThemeName = "Office2010Black";
            this.radSpinEditor12.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radGroupBox1.Controls.Add(this.radLabel16);
            this.radGroupBox1.Controls.Add(this.radLabel17);
            this.radGroupBox1.Controls.Add(this.radLabel18);
            this.radGroupBox1.Controls.Add(this.radLabel19);
            this.radGroupBox1.Controls.Add(this.radLabel13);
            this.radGroupBox1.Controls.Add(this.radLabel14);
            this.radGroupBox1.Controls.Add(this.radLabel15);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.radTrackBar3);
            this.radGroupBox1.Controls.Add(this.radTrackBar2);
            this.radGroupBox1.Controls.Add(this.radTrackBar4);
            this.radGroupBox1.Controls.Add(this.radTrackBar1);
            this.radGroupBox1.HeaderText = "PD Phase Angle - Degrees";
            this.radGroupBox1.Location = new System.Drawing.Point(22, 3);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.ControlBounds = new System.Drawing.Rectangle(22, 3, 200, 100);
            this.radGroupBox1.Size = new System.Drawing.Size(420, 143);
            this.radGroupBox1.TabIndex = 5;
            this.radGroupBox1.Text = "PD Phase Angle - Degrees";
            this.radGroupBox1.ThemeName = "Office2010Black";
            // 
            // radLabel16
            // 
            this.radLabel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel16.Location = new System.Drawing.Point(5, 49);
            this.radLabel16.Name = "radLabel16";
            // 
            // 
            // 
            this.radLabel16.RootElement.ControlBounds = new System.Drawing.Rectangle(5, 49, 100, 18);
            this.radLabel16.Size = new System.Drawing.Size(30, 18);
            this.radLabel16.TabIndex = 4;
            this.radLabel16.Text = "Start";
            // 
            // radLabel17
            // 
            this.radLabel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel17.Location = new System.Drawing.Point(7, 98);
            this.radLabel17.Name = "radLabel17";
            // 
            // 
            // 
            this.radLabel17.RootElement.ControlBounds = new System.Drawing.Rectangle(7, 98, 100, 18);
            this.radLabel17.Size = new System.Drawing.Size(28, 18);
            this.radLabel17.TabIndex = 4;
            this.radLabel17.Text = "End ";
            // 
            // radLabel18
            // 
            this.radLabel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel18.Location = new System.Drawing.Point(73, 21);
            this.radLabel18.Name = "radLabel18";
            // 
            // 
            // 
            this.radLabel18.RootElement.ControlBounds = new System.Drawing.Rectangle(73, 21, 100, 18);
            this.radLabel18.Size = new System.Drawing.Size(68, 18);
            this.radLabel18.TabIndex = 4;
            this.radLabel18.Text = "Positive Half";
            // 
            // radLabel19
            // 
            this.radLabel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel19.Location = new System.Drawing.Point(275, 18);
            this.radLabel19.Name = "radLabel19";
            // 
            // 
            // 
            this.radLabel19.RootElement.ControlBounds = new System.Drawing.Rectangle(275, 18, 100, 18);
            this.radLabel19.Size = new System.Drawing.Size(74, 18);
            this.radLabel19.TabIndex = 4;
            this.radLabel19.Text = "Negative Half";
            // 
            // radLabel13
            // 
            this.radLabel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel13.Location = new System.Drawing.Point(202, 102);
            this.radLabel13.Name = "radLabel13";
            // 
            // 
            // 
            this.radLabel13.RootElement.ControlBounds = new System.Drawing.Rectangle(202, 102, 100, 18);
            this.radLabel13.Size = new System.Drawing.Size(12, 18);
            this.radLabel13.TabIndex = 3;
            this.radLabel13.Text = "0";
            // 
            // radLabel14
            // 
            this.radLabel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel14.Location = new System.Drawing.Point(391, 50);
            this.radLabel14.Name = "radLabel14";
            // 
            // 
            // 
            this.radLabel14.RootElement.ControlBounds = new System.Drawing.Rectangle(391, 50, 100, 18);
            this.radLabel14.Size = new System.Drawing.Size(12, 18);
            this.radLabel14.TabIndex = 3;
            this.radLabel14.Text = "0";
            // 
            // radLabel15
            // 
            this.radLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel15.Location = new System.Drawing.Point(391, 101);
            this.radLabel15.Name = "radLabel15";
            // 
            // 
            // 
            this.radLabel15.RootElement.ControlBounds = new System.Drawing.Rectangle(391, 101, 100, 18);
            this.radLabel15.Size = new System.Drawing.Size(12, 18);
            this.radLabel15.TabIndex = 3;
            this.radLabel15.Text = "0";
            // 
            // radLabel12
            // 
            this.radLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel12.Location = new System.Drawing.Point(201, 55);
            this.radLabel12.Name = "radLabel12";
            // 
            // 
            // 
            this.radLabel12.RootElement.ControlBounds = new System.Drawing.Rectangle(201, 55, 100, 18);
            this.radLabel12.Size = new System.Drawing.Size(12, 18);
            this.radLabel12.TabIndex = 2;
            this.radLabel12.Text = "0";
            // 
            // radTrackBar3
            // 
            this.radTrackBar3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radTrackBar3.Location = new System.Drawing.Point(229, 42);
            this.radTrackBar3.Maximum = 360F;
            this.radTrackBar3.Minimum = 180F;
            this.radTrackBar3.Name = "radTrackBar3";
            // 
            // 
            // 
            this.radTrackBar3.RootElement.ControlBounds = new System.Drawing.Rectangle(229, 42, 150, 40);
            this.radTrackBar3.Size = new System.Drawing.Size(150, 39);
            this.radTrackBar3.SmallTickFrequency = 5;
            this.radTrackBar3.TabIndex = 1;
            this.radTrackBar3.Text = "radTrackBar3";
            this.radTrackBar3.ThemeName = "Office2010Black";
            this.radTrackBar3.Value = 180F;
            this.radTrackBar3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.radTrackBar3_Scroll);
            // 
            // radTrackBar2
            // 
            this.radTrackBar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radTrackBar2.Location = new System.Drawing.Point(41, 91);
            this.radTrackBar2.Maximum = 180F;
            this.radTrackBar2.Name = "radTrackBar2";
            // 
            // 
            // 
            this.radTrackBar2.RootElement.ControlBounds = new System.Drawing.Rectangle(41, 91, 150, 40);
            this.radTrackBar2.Size = new System.Drawing.Size(150, 39);
            this.radTrackBar2.SmallTickFrequency = 5;
            this.radTrackBar2.TabIndex = 1;
            this.radTrackBar2.Text = "radTrackBar2";
            this.radTrackBar2.ThemeName = "Office2010Black";
            this.radTrackBar2.Value = 90F;
            this.radTrackBar2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.radTrackBar2_Scroll);
            // 
            // radTrackBar4
            // 
            this.radTrackBar4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radTrackBar4.Location = new System.Drawing.Point(229, 91);
            this.radTrackBar4.Maximum = 360F;
            this.radTrackBar4.Minimum = 180F;
            this.radTrackBar4.Name = "radTrackBar4";
            // 
            // 
            // 
            this.radTrackBar4.RootElement.ControlBounds = new System.Drawing.Rectangle(229, 91, 150, 40);
            this.radTrackBar4.Size = new System.Drawing.Size(150, 39);
            this.radTrackBar4.SmallTickFrequency = 5;
            this.radTrackBar4.TabIndex = 1;
            this.radTrackBar4.Text = "radTrackBar4";
            this.radTrackBar4.ThemeName = "Office2010Black";
            this.radTrackBar4.Value = 270F;
            this.radTrackBar4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.radTrackBar4_Scroll);
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radTrackBar1.Location = new System.Drawing.Point(41, 42);
            this.radTrackBar1.Maximum = 180F;
            this.radTrackBar1.Name = "radTrackBar1";
            // 
            // 
            // 
            this.radTrackBar1.RootElement.ControlBounds = new System.Drawing.Rectangle(41, 42, 150, 40);
            this.radTrackBar1.Size = new System.Drawing.Size(150, 39);
            this.radTrackBar1.SmallTickFrequency = 5;
            this.radTrackBar1.TabIndex = 0;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.ThemeName = "Office2010Black";
            this.radTrackBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.radTrackBar1_Scroll);
            // 
            // radGridView4
            // 
            this.radGridView4.AutoSize = true;
            this.radGridView4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView4.Location = new System.Drawing.Point(458, 3);
            // 
            // radGridView4
            // 
            this.radGridView4.MasterTemplate.AllowAddNewRow = false;
            this.radGridView4.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView4.MasterTemplate.AllowColumnChooser = false;
            this.radGridView4.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView4.MasterTemplate.AllowColumnReorder = false;
            this.radGridView4.MasterTemplate.AllowColumnResize = false;
            this.radGridView4.MasterTemplate.AllowDeleteRow = false;
            this.radGridView4.MasterTemplate.AllowDragToGroup = false;
            this.radGridView4.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn23.HeaderText = "Channel";
            gridViewTextBoxColumn23.Name = "Channel";
            gridViewCheckBoxColumn1.HeaderText = "Enabled";
            gridViewCheckBoxColumn1.Name = "Enabled";
            gridViewComboBoxColumn7.AllowFiltering = false;
            gridViewComboBoxColumn7.AllowGroup = false;
            gridViewComboBoxColumn7.AllowHide = false;
            gridViewComboBoxColumn7.AllowReorder = false;
            gridViewComboBoxColumn7.AllowResize = false;
            gridViewComboBoxColumn7.AllowSort = false;
            gridViewComboBoxColumn7.HeaderText = "Polarity";
            gridViewComboBoxColumn7.Name = "Polarity";
            gridViewComboBoxColumn7.Width = 70;
            gridViewComboBoxColumn8.HeaderText = "Amplitude (mV)";
            gridViewComboBoxColumn8.Name = "Amplitude";
            gridViewComboBoxColumn8.Width = 70;
            gridViewComboBoxColumn8.WrapText = true;
            gridViewCheckBoxColumn2.HeaderText = "Delay";
            gridViewCheckBoxColumn2.Name = "Delay";
            this.radGridView4.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn23,
            gridViewCheckBoxColumn1,
            gridViewComboBoxColumn7,
            gridViewComboBoxColumn8,
            gridViewCheckBoxColumn2});
            this.radGridView4.MasterTemplate.ShowFilteringRow = false;
            this.radGridView4.Name = "radGridView4";
            // 
            // 
            // 
            this.radGridView4.RootElement.ControlBounds = new System.Drawing.Rectangle(458, 3, 240, 150);
            this.radGridView4.ShowGroupPanel = false;
            this.radGridView4.Size = new System.Drawing.Size(307, 30);
            this.radGridView4.TabIndex = 4;
            this.radGridView4.Text = "radGridView4";
            this.radGridView4.ThemeName = "Office2010Black";
            // 
            // radPageViewPage6
            // 
            this.radPageViewPage6.Controls.Add(this.radButton16);
            this.radPageViewPage6.Controls.Add(this.radButton13);
            this.radPageViewPage6.Controls.Add(this.radTextBox8);
            this.radPageViewPage6.Controls.Add(this.radLabel31);
            this.radPageViewPage6.Controls.Add(this.radButton11);
            this.radPageViewPage6.Controls.Add(this.radButton12);
            this.radPageViewPage6.Controls.Add(this.radButton9);
            this.radPageViewPage6.Controls.Add(this.radButton8);
            this.radPageViewPage6.Controls.Add(this.radSpinEditor8);
            this.radPageViewPage6.Controls.Add(this.radSpinEditor9);
            this.radPageViewPage6.Controls.Add(this.radLabel30);
            this.radPageViewPage6.Controls.Add(this.radLabel29);
            this.radPageViewPage6.Controls.Add(this.radLabel28);
            this.radPageViewPage6.Controls.Add(this.radLabel27);
            this.radPageViewPage6.Controls.Add(this.radSpinEditor10);
            this.radPageViewPage6.Controls.Add(this.radSpinEditor11);
            this.radPageViewPage6.Controls.Add(this.radSpinEditor7);
            this.radPageViewPage6.Controls.Add(this.radLabel26);
            this.radPageViewPage6.Controls.Add(this.radLabel25);
            this.radPageViewPage6.Controls.Add(this.radGridView10);
            this.radPageViewPage6.Controls.Add(this.radLabel24);
            this.radPageViewPage6.Controls.Add(this.radGridView9);
            this.radPageViewPage6.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage6.Name = "radPageViewPage6";
            this.radPageViewPage6.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage6.Text = "Calibration Coefficients";
            this.radPageViewPage6.Paint += new System.Windows.Forms.PaintEventHandler(this.radPageViewPage6_Paint);
            // 
            // radButton16
            // 
            this.radButton16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton16.Location = new System.Drawing.Point(229, 164);
            this.radButton16.Name = "radButton16";
            // 
            // 
            // 
            this.radButton16.RootElement.ControlBounds = new System.Drawing.Rectangle(229, 164, 110, 24);
            this.radButton16.Size = new System.Drawing.Size(170, 24);
            this.radButton16.TabIndex = 37;
            this.radButton16.Text = "Calculate Impedances";
            this.radButton16.ThemeName = "Office2010Black";
            this.radButton16.Click += new System.EventHandler(this.radButton16_Click);
            // 
            // radButton13
            // 
            this.radButton13.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton13.Location = new System.Drawing.Point(295, 260);
            this.radButton13.Name = "radButton13";
            // 
            // 
            // 
            this.radButton13.RootElement.ControlBounds = new System.Drawing.Rectangle(295, 260, 110, 24);
            this.radButton13.Size = new System.Drawing.Size(87, 24);
            this.radButton13.TabIndex = 37;
            this.radButton13.Text = "Unlock";
            this.radButton13.ThemeName = "Office2010Black";
            this.radButton13.Click += new System.EventHandler(this.radButton13_Click);
            // 
            // radTextBox8
            // 
            this.radTextBox8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox8.Location = new System.Drawing.Point(286, 234);
            this.radTextBox8.Name = "radTextBox8";
            // 
            // 
            // 
            this.radTextBox8.RootElement.ControlBounds = new System.Drawing.Rectangle(286, 234, 100, 20);
            this.radTextBox8.RootElement.StretchVertically = true;
            this.radTextBox8.Size = new System.Drawing.Size(100, 20);
            this.radTextBox8.TabIndex = 37;
            this.radTextBox8.TabStop = false;
            this.radTextBox8.ThemeName = "Office2010Black";
            // 
            // radLabel31
            // 
            this.radLabel31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel31.Location = new System.Drawing.Point(227, 236);
            this.radLabel31.Name = "radLabel31";
            // 
            // 
            // 
            this.radLabel31.RootElement.ControlBounds = new System.Drawing.Rectangle(227, 236, 100, 18);
            this.radLabel31.Size = new System.Drawing.Size(53, 18);
            this.radLabel31.TabIndex = 3;
            this.radLabel31.Text = "Password";
            // 
            // radButton11
            // 
            this.radButton11.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton11.Location = new System.Drawing.Point(229, 95);
            this.radButton11.Name = "radButton11";
            // 
            // 
            // 
            this.radButton11.RootElement.ControlBounds = new System.Drawing.Rectangle(229, 95, 110, 24);
            this.radButton11.Size = new System.Drawing.Size(170, 24);
            this.radButton11.TabIndex = 36;
            this.radButton11.Text = "Send Coefficients to DRSim";
            this.radButton11.ThemeName = "Office2010Black";
            this.radButton11.Click += new System.EventHandler(this.radButton11_Click);
            // 
            // radButton12
            // 
            this.radButton12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton12.Location = new System.Drawing.Point(229, 125);
            this.radButton12.Name = "radButton12";
            // 
            // 
            // 
            this.radButton12.RootElement.ControlBounds = new System.Drawing.Rectangle(229, 125, 110, 24);
            this.radButton12.Size = new System.Drawing.Size(170, 24);
            this.radButton12.TabIndex = 36;
            this.radButton12.Text = "Load Coefficients From DRSim";
            this.radButton12.ThemeName = "Office2010Black";
            this.radButton12.Click += new System.EventHandler(this.radButton12_Click);
            // 
            // radButton9
            // 
            this.radButton9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton9.Location = new System.Drawing.Point(231, 45);
            this.radButton9.Name = "radButton9";
            // 
            // 
            // 
            this.radButton9.RootElement.ControlBounds = new System.Drawing.Rectangle(231, 45, 110, 24);
            this.radButton9.Size = new System.Drawing.Size(155, 24);
            this.radButton9.TabIndex = 35;
            this.radButton9.Text = "Save Coefficients to  File";
            this.radButton9.ThemeName = "Office2010Black";
            this.radButton9.Click += new System.EventHandler(this.radButton9_Click);
            // 
            // radButton8
            // 
            this.radButton8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton8.Location = new System.Drawing.Point(231, 15);
            this.radButton8.Name = "radButton8";
            // 
            // 
            // 
            this.radButton8.RootElement.ControlBounds = new System.Drawing.Rectangle(231, 15, 110, 24);
            this.radButton8.Size = new System.Drawing.Size(155, 24);
            this.radButton8.TabIndex = 34;
            this.radButton8.Text = "Load Coefficients From File";
            this.radButton8.ThemeName = "Office2010Black";
            this.radButton8.Click += new System.EventHandler(this.radButton8_Click);
            // 
            // radSpinEditor8
            // 
            this.radSpinEditor8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor8.DecimalPlaces = 1;
            this.radSpinEditor8.EnableKeyMap = true;
            this.radSpinEditor8.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditor8.Location = new System.Drawing.Point(415, 429);
            this.radSpinEditor8.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.radSpinEditor8.Name = "radSpinEditor8";
            // 
            // 
            // 
            this.radSpinEditor8.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor8.RootElement.ControlBounds = new System.Drawing.Rectangle(447, 429, 100, 20);
            this.radSpinEditor8.RootElement.StretchVertically = true;
            this.radSpinEditor8.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor8.TabIndex = 32;
            this.radSpinEditor8.TabStop = false;
            this.radSpinEditor8.ThemeName = "Office2010Black";
            this.radSpinEditor8.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // radSpinEditor9
            // 
            this.radSpinEditor9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor9.DecimalPlaces = 1;
            this.radSpinEditor9.EnableKeyMap = true;
            this.radSpinEditor9.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditor9.Location = new System.Drawing.Point(415, 455);
            this.radSpinEditor9.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.radSpinEditor9.Name = "radSpinEditor9";
            // 
            // 
            // 
            this.radSpinEditor9.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor9.RootElement.ControlBounds = new System.Drawing.Rectangle(447, 455, 100, 20);
            this.radSpinEditor9.RootElement.StretchVertically = true;
            this.radSpinEditor9.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor9.TabIndex = 32;
            this.radSpinEditor9.TabStop = false;
            this.radSpinEditor9.ThemeName = "Office2010Black";
            this.radSpinEditor9.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // radLabel30
            // 
            this.radLabel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel30.Location = new System.Drawing.Point(596, 375);
            this.radLabel30.Name = "radLabel30";
            // 
            // 
            // 
            this.radLabel30.RootElement.ControlBounds = new System.Drawing.Rectangle(628, 375, 100, 18);
            this.radLabel30.Size = new System.Drawing.Size(207, 18);
            this.radLabel30.TabIndex = 4;
            this.radLabel30.Text = "RTD Temp Coefficient (Ohms/ohms/deg";
            // 
            // radLabel29
            // 
            this.radLabel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel29.Location = new System.Drawing.Point(635, 440);
            this.radLabel29.Name = "radLabel29";
            // 
            // 
            // 
            this.radLabel29.RootElement.ControlBounds = new System.Drawing.Rectangle(667, 440, 100, 18);
            this.radLabel29.Size = new System.Drawing.Size(131, 18);
            this.radLabel29.TabIndex = 33;
            this.radLabel29.Text = "RTD Resistanc at 0 deg C";
            // 
            // radLabel28
            // 
            this.radLabel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel28.Location = new System.Drawing.Point(378, 429);
            this.radLabel28.Name = "radLabel28";
            // 
            // 
            // 
            this.radLabel28.RootElement.ControlBounds = new System.Drawing.Rectangle(410, 429, 100, 18);
            this.radLabel28.Size = new System.Drawing.Size(31, 18);
            this.radLabel28.TabIndex = 4;
            this.radLabel28.Text = "RES3";
            // 
            // radLabel27
            // 
            this.radLabel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel27.Location = new System.Drawing.Point(378, 455);
            this.radLabel27.Name = "radLabel27";
            // 
            // 
            // 
            this.radLabel27.RootElement.ControlBounds = new System.Drawing.Rectangle(410, 455, 100, 18);
            this.radLabel27.Size = new System.Drawing.Size(31, 18);
            this.radLabel27.TabIndex = 4;
            this.radLabel27.Text = "RES4";
            // 
            // radSpinEditor10
            // 
            this.radSpinEditor10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor10.DecimalPlaces = 5;
            this.radSpinEditor10.EnableKeyMap = true;
            this.radSpinEditor10.Increment = new decimal(new int[] {
            1,
            0,
            0,
            327680});
            this.radSpinEditor10.Location = new System.Drawing.Point(667, 401);
            this.radSpinEditor10.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor10.Name = "radSpinEditor10";
            // 
            // 
            // 
            this.radSpinEditor10.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor10.RootElement.ControlBounds = new System.Drawing.Rectangle(699, 401, 100, 20);
            this.radSpinEditor10.RootElement.StretchVertically = true;
            this.radSpinEditor10.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor10.TabIndex = 32;
            this.radSpinEditor10.TabStop = false;
            this.radSpinEditor10.ThemeName = "Office2010Black";
            this.radSpinEditor10.Value = new decimal(new int[] {
            385,
            0,
            0,
            327680});
            // 
            // radSpinEditor11
            // 
            this.radSpinEditor11.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor11.DecimalPlaces = 1;
            this.radSpinEditor11.EnableKeyMap = true;
            this.radSpinEditor11.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditor11.Location = new System.Drawing.Point(667, 464);
            this.radSpinEditor11.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.radSpinEditor11.Name = "radSpinEditor11";
            // 
            // 
            // 
            this.radSpinEditor11.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor11.RootElement.ControlBounds = new System.Drawing.Rectangle(699, 464, 100, 20);
            this.radSpinEditor11.RootElement.StretchVertically = true;
            this.radSpinEditor11.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor11.TabIndex = 32;
            this.radSpinEditor11.TabStop = false;
            this.radSpinEditor11.ThemeName = "Office2010Black";
            this.radSpinEditor11.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // radSpinEditor7
            // 
            this.radSpinEditor7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor7.DecimalPlaces = 1;
            this.radSpinEditor7.EnableKeyMap = true;
            this.radSpinEditor7.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.radSpinEditor7.Location = new System.Drawing.Point(415, 401);
            this.radSpinEditor7.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.radSpinEditor7.Name = "radSpinEditor7";
            // 
            // 
            // 
            this.radSpinEditor7.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor7.RootElement.ControlBounds = new System.Drawing.Rectangle(447, 401, 100, 20);
            this.radSpinEditor7.RootElement.StretchVertically = true;
            this.radSpinEditor7.Size = new System.Drawing.Size(67, 20);
            this.radSpinEditor7.TabIndex = 31;
            this.radSpinEditor7.TabStop = false;
            this.radSpinEditor7.ThemeName = "Office2010Black";
            this.radSpinEditor7.Value = new decimal(new int[] {
            174,
            0,
            0,
            0});
            this.radSpinEditor7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.radSpinEditor7_Scroll);
            // 
            // radLabel26
            // 
            this.radLabel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel26.Location = new System.Drawing.Point(378, 403);
            this.radLabel26.Name = "radLabel26";
            // 
            // 
            // 
            this.radLabel26.RootElement.ControlBounds = new System.Drawing.Rectangle(410, 403, 100, 18);
            this.radLabel26.Size = new System.Drawing.Size(31, 18);
            this.radLabel26.TabIndex = 3;
            this.radLabel26.Text = "RES0";
            // 
            // radLabel25
            // 
            this.radLabel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel25.Location = new System.Drawing.Point(474, 15);
            this.radLabel25.Name = "radLabel25";
            // 
            // 
            // 
            this.radLabel25.RootElement.ControlBounds = new System.Drawing.Rectangle(474, 15, 100, 18);
            this.radLabel25.Size = new System.Drawing.Size(118, 18);
            this.radLabel25.TabIndex = 5;
            this.radLabel25.Text = "RTD Calibration Points";
            // 
            // radGridView10
            // 
            this.radGridView10.AllowDrop = true;
            this.radGridView10.AutoSize = true;
            this.radGridView10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView10.Location = new System.Drawing.Point(410, 39);
            // 
            // radGridView10
            // 
            this.radGridView10.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView10.MasterTemplate.AllowAddNewRow = false;
            this.radGridView10.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView10.MasterTemplate.AllowColumnChooser = false;
            this.radGridView10.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView10.MasterTemplate.AllowColumnReorder = false;
            this.radGridView10.MasterTemplate.AllowColumnResize = false;
            this.radGridView10.MasterTemplate.AllowDeleteRow = false;
            this.radGridView10.MasterTemplate.AllowDragToGroup = false;
            this.radGridView10.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn24.FieldName = "PotNum";
            gridViewTextBoxColumn24.HeaderText = "PotNum";
            gridViewTextBoxColumn24.Name = "PotNum";
            gridViewTextBoxColumn25.FieldName = "Channel";
            gridViewTextBoxColumn25.HeaderText = "Channel";
            gridViewTextBoxColumn25.Name = "Channel";
            gridViewDecimalColumn34.DataType = typeof(string);
            gridViewDecimalColumn34.DecimalPlaces = 1;
            gridViewDecimalColumn34.FieldName = "TotalR";
            gridViewDecimalColumn34.HeaderText = "Total R (Ohms";
            gridViewDecimalColumn34.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            gridViewDecimalColumn34.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn34.Name = "TotalR";
            gridViewDecimalColumn34.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            gridViewDecimalColumn34.Width = 75;
            gridViewDecimalColumn34.WrapText = true;
            gridViewDecimalColumn35.DataType = typeof(string);
            gridViewDecimalColumn35.DecimalPlaces = 1;
            gridViewDecimalColumn35.FieldName = "WiperR";
            gridViewDecimalColumn35.HeaderText = "Wiper R (Ohms)";
            gridViewDecimalColumn35.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            gridViewDecimalColumn35.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn35.Name = "WiperR";
            gridViewDecimalColumn35.Step = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            gridViewDecimalColumn35.Width = 75;
            gridViewDecimalColumn35.WrapText = true;
            this.radGridView10.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewDecimalColumn34,
            gridViewDecimalColumn35});
            this.radGridView10.MasterTemplate.DataSource = this.rTCalBindingSource;
            sortDescriptor2.PropertyName = "PotNum";
            groupDescriptor2.GroupNames.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.radGridView10.MasterTemplate.GroupDescriptors.AddRange(new Telerik.WinControls.Data.GroupDescriptor[] {
            groupDescriptor2});
            this.radGridView10.MasterTemplate.ShowFilteringRow = false;
            this.radGridView10.Name = "radGridView10";
            // 
            // 
            // 
            this.radGridView10.RootElement.ControlBounds = new System.Drawing.Rectangle(410, 39, 240, 150);
            this.radGridView10.ShowGroupPanel = false;
            this.radGridView10.Size = new System.Drawing.Size(240, 30);
            this.radGridView10.TabIndex = 1;
            this.radGridView10.Text = "radGridView10";
            this.radGridView10.ThemeName = "Office2010Black";
            // 
            // rTCalBindingSource
            // 
            this.rTCalBindingSource.DataMember = "RTCal";
            this.rTCalBindingSource.DataSource = this.coeff;
            // 
            // coeff
            // 
            this.coeff.DataSetName = "Coeff";
            this.coeff.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radLabel24
            // 
            this.radLabel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel24.Location = new System.Drawing.Point(48, 15);
            this.radLabel24.Name = "radLabel24";
            // 
            // 
            // 
            this.radLabel24.RootElement.ControlBounds = new System.Drawing.Rectangle(48, 15, 100, 18);
            this.radLabel24.Size = new System.Drawing.Size(77, 18);
            this.radLabel24.TabIndex = 4;
            this.radLabel24.Text = "Current Offset";
            // 
            // radGridView9
            // 
            this.radGridView9.AllowDrop = true;
            this.radGridView9.AutoSize = true;
            this.radGridView9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridView9.Location = new System.Drawing.Point(14, 39);
            // 
            // radGridView9
            // 
            this.radGridView9.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView9.MasterTemplate.AllowAddNewRow = false;
            this.radGridView9.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView9.MasterTemplate.AllowColumnChooser = false;
            this.radGridView9.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView9.MasterTemplate.AllowColumnReorder = false;
            this.radGridView9.MasterTemplate.AllowColumnResize = false;
            this.radGridView9.MasterTemplate.AllowDeleteRow = false;
            this.radGridView9.MasterTemplate.AllowDragToGroup = false;
            this.radGridView9.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn26.AllowFiltering = false;
            gridViewTextBoxColumn26.AllowGroup = false;
            gridViewTextBoxColumn26.AllowHide = false;
            gridViewTextBoxColumn26.AllowReorder = false;
            gridViewTextBoxColumn26.AllowResize = false;
            gridViewTextBoxColumn26.AllowSort = false;
            gridViewTextBoxColumn26.FieldName = "Channel";
            gridViewTextBoxColumn26.HeaderText = "Channel";
            gridViewTextBoxColumn26.Name = "Channel";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewDecimalColumn36.DataType = typeof(string);
            gridViewDecimalColumn36.DecimalPlaces = 0;
            gridViewDecimalColumn36.FieldName = "Offset";
            gridViewDecimalColumn36.HeaderText = "Offset";
            gridViewDecimalColumn36.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            gridViewDecimalColumn36.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            gridViewDecimalColumn36.Name = "Offset";
            gridViewDecimalColumn36.Width = 70;
            this.radGridView9.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn26,
            gridViewDecimalColumn36});
            this.radGridView9.MasterTemplate.DataSource = this.curOffsetsBindingSource;
            this.radGridView9.MasterTemplate.ShowFilteringRow = false;
            this.radGridView9.Name = "radGridView9";
            // 
            // 
            // 
            this.radGridView9.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 39, 240, 150);
            this.radGridView9.ShowGroupPanel = false;
            this.radGridView9.Size = new System.Drawing.Size(140, 30);
            this.radGridView9.TabIndex = 0;
            this.radGridView9.Text = "radGridView9";
            this.radGridView9.ThemeName = "Office2010Black";
            // 
            // curOffsetsBindingSource
            // 
            this.curOffsetsBindingSource.DataMember = "CurOffsets";
            this.curOffsetsBindingSource.DataSource = this.coeff;
            // 
            // radPageViewPage8
            // 
            this.radPageViewPage8.Location = new System.Drawing.Point(189, 5);
            this.radPageViewPage8.Name = "radPageViewPage8";
            this.radPageViewPage8.Size = new System.Drawing.Size(837, 608);
            this.radPageViewPage8.Text = "Custom Waveforms";
            // 
            // radLabel5
            // 
            this.radLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel5.Location = new System.Drawing.Point(637, 6);
            this.radLabel5.Name = "radLabel5";
            // 
            // 
            // 
            this.radLabel5.RootElement.ControlBounds = new System.Drawing.Rectangle(720, 6, 100, 18);
            this.radLabel5.Size = new System.Drawing.Size(68, 18);
            this.radLabel5.TabIndex = 5;
            this.radLabel5.Text = "Comm Ports";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList1.Location = new System.Drawing.Point(713, 4);
            this.radDropDownList1.Name = "radDropDownList1";
            // 
            // 
            // 
            this.radDropDownList1.RootElement.ControlBounds = new System.Drawing.Rectangle(796, 4, 125, 20);
            this.radDropDownList1.RootElement.StretchVertically = true;
            this.radDropDownList1.Size = new System.Drawing.Size(106, 19);
            this.radDropDownList1.TabIndex = 7;
            this.radDropDownList1.ThemeName = "Office2010Black";
            // 
            // radTextBox2
            // 
            this.radTextBox2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox2.Enabled = false;
            this.radTextBox2.Location = new System.Drawing.Point(237, 30);
            this.radTextBox2.Name = "radTextBox2";
            // 
            // 
            // 
            this.radTextBox2.RootElement.ControlBounds = new System.Drawing.Rectangle(245, 30, 100, 20);
            this.radTextBox2.RootElement.Enabled = false;
            this.radTextBox2.RootElement.StretchVertically = true;
            this.radTextBox2.Size = new System.Drawing.Size(144, 20);
            this.radTextBox2.TabIndex = 6;
            this.radTextBox2.TabStop = false;
            this.radTextBox2.ThemeName = "Office2010Black";
            // 
            // radTextBox3
            // 
            this.radTextBox3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox3.Enabled = false;
            this.radTextBox3.Location = new System.Drawing.Point(546, 30);
            this.radTextBox3.Name = "radTextBox3";
            // 
            // 
            // 
            this.radTextBox3.RootElement.ControlBounds = new System.Drawing.Rectangle(576, 30, 100, 20);
            this.radTextBox3.RootElement.Enabled = false;
            this.radTextBox3.RootElement.StretchVertically = true;
            this.radTextBox3.Size = new System.Drawing.Size(34, 20);
            this.radTextBox3.TabIndex = 6;
            this.radTextBox3.TabStop = false;
            this.radTextBox3.Text = "99";
            this.radTextBox3.ThemeName = "Office2010Black";
            // 
            // radTextBox1
            // 
            this.radTextBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox1.Enabled = false;
            this.radTextBox1.Location = new System.Drawing.Point(106, 30);
            this.radTextBox1.Name = "radTextBox1";
            // 
            // 
            // 
            this.radTextBox1.RootElement.ControlBounds = new System.Drawing.Rectangle(114, 30, 100, 20);
            this.radTextBox1.RootElement.Enabled = false;
            this.radTextBox1.RootElement.StretchVertically = true;
            this.radTextBox1.Size = new System.Drawing.Size(41, 20);
            this.radTextBox1.TabIndex = 5;
            this.radTextBox1.TabStop = false;
            this.radTextBox1.ThemeName = "Office2010Black";
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel3.Location = new System.Drawing.Point(385, 30);
            this.radLabel3.Name = "radLabel3";
            // 
            // 
            // 
            this.radLabel3.RootElement.ControlBounds = new System.Drawing.Rectangle(415, 30, 100, 18);
            this.radLabel3.Size = new System.Drawing.Size(155, 18);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Modbus Address (Defalut 99):";
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel2.Location = new System.Drawing.Point(153, 30);
            this.radLabel2.Name = "radLabel2";
            // 
            // 
            // 
            this.radLabel2.RootElement.ControlBounds = new System.Drawing.Rectangle(161, 30, 100, 18);
            this.radLabel2.Size = new System.Drawing.Size(78, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "Board Serial #:";
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel1.Location = new System.Drawing.Point(4, 30);
            this.radLabel1.Name = "radLabel1";
            // 
            // 
            // 
            this.radLabel1.RootElement.ControlBounds = new System.Drawing.Rectangle(12, 30, 100, 18);
            this.radLabel1.Size = new System.Drawing.Size(95, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Firmware Version:";
            // 
            // radButton2
            // 
            this.radButton2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton2.Location = new System.Drawing.Point(337, 0);
            this.radButton2.Name = "radButton2";
            // 
            // 
            // 
            this.radButton2.RootElement.ControlBounds = new System.Drawing.Rectangle(401, 0, 110, 24);
            this.radButton2.Size = new System.Drawing.Size(130, 24);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "Load Config From File";
            this.radButton2.ThemeName = "Office2010Black";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radButton4
            // 
            this.radButton4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton4.Location = new System.Drawing.Point(473, 0);
            this.radButton4.Name = "radButton4";
            // 
            // 
            // 
            this.radButton4.RootElement.ControlBounds = new System.Drawing.Rectangle(537, 0, 110, 24);
            this.radButton4.Size = new System.Drawing.Size(158, 24);
            this.radButton4.TabIndex = 1;
            this.radButton4.Text = "Save Current Config to File";
            this.radButton4.ThemeName = "Office2010Black";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // radButton3
            // 
            this.radButton3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton3.Location = new System.Drawing.Point(5, 0);
            this.radButton3.Name = "radButton3";
            // 
            // 
            // 
            this.radButton3.RootElement.ControlBounds = new System.Drawing.Rectangle(5, 0, 110, 24);
            this.radButton3.Size = new System.Drawing.Size(137, 24);
            this.radButton3.TabIndex = 1;
            this.radButton3.Text = "Load Config From DRSim";
            this.radButton3.ThemeName = "Office2010Black";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radSpinEditor6
            // 
            this.radSpinEditor6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSpinEditor6.DecimalPlaces = 2;
            this.radSpinEditor6.EnableKeyMap = true;
            this.radSpinEditor6.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.radSpinEditor6.Location = new System.Drawing.Point(713, 32);
            this.radSpinEditor6.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.radSpinEditor6.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.radSpinEditor6.Name = "radSpinEditor6";
            // 
            // 
            // 
            this.radSpinEditor6.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radSpinEditor6.RootElement.ControlBounds = new System.Drawing.Rectangle(773, 32, 100, 20);
            this.radSpinEditor6.RootElement.StretchVertically = true;
            this.radSpinEditor6.Size = new System.Drawing.Size(54, 20);
            this.radSpinEditor6.TabIndex = 30;
            this.radSpinEditor6.TabStop = false;
            this.radSpinEditor6.ThemeName = "Office2010Black";
            this.radSpinEditor6.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // radLabel4
            // 
            this.radLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(182)))), ((int)(((byte)(182)))));
            this.radLabel4.Location = new System.Drawing.Point(588, 32);
            this.radLabel4.Name = "radLabel4";
            // 
            // 
            // 
            this.radLabel4.RootElement.ControlBounds = new System.Drawing.Rectangle(648, 32, 100, 18);
            this.radLabel4.Size = new System.Drawing.Size(119, 18);
            this.radLabel4.TabIndex = 22;
            this.radLabel4.Text = " Frequency (1 - 80 Hz):";
            // 
            // object_0e822de2_cd22_4194_9660_a631cc446635
            // 
            this.object_0e822de2_cd22_4194_9660_a631cc446635.Name = "object_0e822de2_cd22_4194_9660_a631cc446635";
            this.object_0e822de2_cd22_4194_9660_a631cc446635.StretchHorizontally = true;
            this.object_0e822de2_cd22_4194_9660_a631cc446635.StretchVertically = true;
            this.object_0e822de2_cd22_4194_9660_a631cc446635.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButton1
            // 
            this.radButton1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton1.Location = new System.Drawing.Point(148, 0);
            this.radButton1.Name = "radButton1";
            // 
            // 
            // 
            this.radButton1.RootElement.ControlBounds = new System.Drawing.Rectangle(148, 0, 110, 24);
            this.radButton1.Size = new System.Drawing.Size(130, 24);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Send Config to DRSim";
            this.radButton1.ThemeName = "Office2010Black";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click_1);
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radCheckBox1.Location = new System.Drawing.Point(775, 33);
            this.radCheckBox1.Name = "radCheckBox1";
            // 
            // 
            // 
            this.radCheckBox1.RootElement.ControlBounds = new System.Drawing.Rectangle(864, 30, 100, 18);
            this.radCheckBox1.RootElement.StretchHorizontally = true;
            this.radCheckBox1.RootElement.StretchVertically = true;
            this.radCheckBox1.Size = new System.Drawing.Size(175, 18);
            this.radCheckBox1.TabIndex = 31;
            this.radCheckBox1.Text = "Use Advanced Bushing Control";
            this.radCheckBox1.ThemeName = "HighContrastBlack";
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radCheckBox1.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBox1_ToggleStateChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // radButton10
            // 
            this.radButton10.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radButton10.Location = new System.Drawing.Point(825, 4);
            this.radButton10.Name = "radButton10";
            // 
            // 
            // 
            this.radButton10.RootElement.ControlBounds = new System.Drawing.Rectangle(908, 4, 110, 24);
            this.radButton10.Size = new System.Drawing.Size(130, 24);
            this.radButton10.TabIndex = 1;
            this.radButton10.Text = "Refresh Comm Ports";
            this.radButton10.ThemeName = "Office2010Black";
            this.radButton10.Click += new System.EventHandler(this.radButton10_Click);
            // 
            // RadForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1040, 686);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.radButton10);
            this.Controls.Add(this.radCheckBox1);
            this.Controls.Add(this.radSpinEditor6);
            this.Controls.Add(this.radTextBox3);
            this.Controls.Add(this.radTextBox2);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.radButton3);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radButton2);
            this.Controls.Add(this.radButton4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RadForm1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DR Simulator";
            this.ThemeName = "Office2010Black";
            this.Load += new System.EventHandler(this.RadForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage7.ResumeLayout(false);
            this.radPageViewPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            this.radPageViewPage2.ResumeLayout(false);
            this.radPageViewPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).EndInit();
            this.radGroupBox5.ResumeLayout(false);
            this.radGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView8.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView6.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView7.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView5.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            this.radPageViewPage4.ResumeLayout(false);
            this.radPageViewPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winChartViewer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).EndInit();
            this.radPageViewPage5.ResumeLayout(false);
            this.radPageViewPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).EndInit();
            this.radPageViewPage6.ResumeLayout(false);
            this.radPageViewPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView10.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rTCalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView9.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.curOffsetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.HighContrastBlackTheme highContrastBlackTheme1;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.RootRadElement object_0e822de2_cd22_4194_9660_a631cc446635;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage5;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage6;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor6;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadButton radButton6;
        
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage8;
        private Telerik.WinControls.EllipseShape ellipseShape1;
        private Telerik.WinControls.UI.RadGridView radGridView3;
        
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar3;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar2;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar4;
        private Telerik.WinControls.UI.RadGridView radGridView4;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadGridView radGridView5;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox5;
        private Telerik.WinControls.UI.RadGridView radGridView8;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadTextBox radTextBox7;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadGridView radGridView6;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadGridView radGridView7;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadGridView radGridView9;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        private Telerik.WinControls.UI.RadGridView radGridView10;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor8;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor9;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor10;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor11;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor7;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage7;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor3;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor5;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor4;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadButton radButton8;
        private System.Windows.Forms.BindingSource rTCalBindingSource;
        private Coeff coeff;
        private System.Windows.Forms.BindingSource curOffsetsBindingSource;
        private Telerik.WinControls.UI.RadButton radButton10;
        private ChartDirector.WinChartViewer winChartViewer1;
        private ChartDirector.WinChartViewer winChartViewer2;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadButton radButton11;
        private Telerik.WinControls.UI.RadButton radButton12;
        private Telerik.WinControls.UI.RadButton radButton13;
        private Telerik.WinControls.UI.RadTextBox radTextBox8;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadButton radButton14;
        private Telerik.WinControls.UI.RadButton radButton15;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private Telerik.WinControls.UI.RadButton radButton16;
        private Telerik.WinControls.UI.RadLabel radLabel33;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor13;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor12;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme2;
        private Telerik.WinControls.Themes.HighContrastBlackTheme highContrastBlackTheme2;
        

    }
}
