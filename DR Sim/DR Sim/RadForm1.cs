﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using ChartDirector;
using System.IO.Ports;
using Telerik.WinControls.UI;

using FieldTalk;

using System.Linq;

namespace DR_Sim
{
    public partial class RadForm1 : Telerik.WinControls.UI.RadForm
    {
        public RadForm1()
        {
            InitializeComponent();
        }

        public double[,] LCwave = new double[3, 9];
        public string[] LCwaveType = new string[3];
        public double[] LCwave1 = new double[9];
        public string LCwaveType1;
        public double[] BCwave1 = new double[9];
        public string BCwaveType1;
        Boolean formload;
        public string appFileName;
        MbusRtuMasterProtocol mbusProtocol = new MbusRtuMasterProtocol();

        string MyDocdir;  // My Documents Path

        private void radButton1_Click_1(object sender, EventArgs e)  // send config to dr sim
        {
            // send configuration to simulator
            int res;  // used to verify results of writes
            int x = 0;  

            if (radDropDownList1.Text == "")
            {
                MessageBox.Show("Select Comm Port");
                return;
            }
            mbusProtocol.timeout = 1600;
            mbusProtocol.retryCnt = 3;
            int result = openProtocol();

            if (result == 0)
            {
                return;
            }
            // Application.UseWaitCursor = true;
            UInt16[] cv = new UInt16[1];
            cv[0] = Convert.ToUInt16(radSpinEditor6.Value * 600);
            // write Frequency
            // res = mbusProtocol.writeSingleRegister(99,21,cv);
            res = mbusProtocol.writeMultipleRegisters(99, 21, cv);

            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error writing Frequency- " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }
            
            // add temp and humidity seetings

            UInt16[] tempHumSettings = new UInt16[5];

            tempHumSettings[0] = Convert.ToUInt16(32768 + (radSpinEditor1.Value * 100));
            tempHumSettings[1] = Convert.ToUInt16(32768 + (radSpinEditor4.Value * 100));
            tempHumSettings[2] = Convert.ToUInt16(32768 + (radSpinEditor2.Value * 100));
            tempHumSettings[3] = Convert.ToUInt16(32768 + (radSpinEditor3.Value * 100));
            tempHumSettings[4] = Convert.ToUInt16((826.0 + Convert.ToDouble(radSpinEditor5.Value) * 31.483));
            res = mbusProtocol.writeMultipleRegisters(99, 30, tempHumSettings);
            
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error Temps and humidity Settings- " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            // pd settings

            Int16[] PDSettings = new Int16[22];

            PDSettings[0] = Convert.ToInt16(radSpinEditor12.Value);
            PDSettings[1] = Convert.ToInt16(radSpinEditor13.Value);
            PDSettings[2] = Convert.ToInt16(radTrackBar1.Value);
            PDSettings[3] = Convert.ToInt16(radTrackBar2.Value);
            PDSettings[4] = Convert.ToInt16(radTrackBar3.Value);
            PDSettings[5] = Convert.ToInt16(radTrackBar4.Value);
            // add grid stuff

            for (x = 0; x < radGridView4.RowCount; x++)
            {
                PDSettings[x + 6] = Convert.ToInt16(FigureOutPDCode(x));
            }

            res = mbusProtocol.writeMultipleRegisters(99, 172, PDSettings);
            
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error PD Settings Settings- " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            // load current sensitivity
            // add temp and humidity setings

            Int16[] LCSensitivity = new Int16[3];

            LCSensitivity[0] = Convert.ToInt16(Convert.ToDouble(radGridView1.Rows[0].Cells[4].Value) * 10);
            LCSensitivity[1] = Convert.ToInt16(Convert.ToDouble(radGridView1.Rows[1].Cells[4].Value) * 10);
            LCSensitivity[2] = Convert.ToInt16(Convert.ToDouble(radGridView1.Rows[2].Cells[4].Value) * 10);
            
            res = mbusProtocol.writeMultipleRegisters(99, 35, LCSensitivity);

            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error Load Current Sensitivity Settings- " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            UInt16[] AllCurrent = new UInt16[15];
            //----------------------
            // Harm1 Amp
            x = 0;
            if (radCheckBox1.Checked == false)
            {
                // set 1
                AllCurrent[0] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[0].Cells[6].Value) * 100.0);
                AllCurrent[1] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[1].Cells[6].Value) * 100.0);
                AllCurrent[2] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[2].Cells[6].Value) * 100.0);

                // set 2
                AllCurrent[3] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[0].Cells[6].Value) * 100.0);
                AllCurrent[4] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[1].Cells[6].Value) * 100.0);
                AllCurrent[5] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[2].Cells[6].Value) * 100.0);

                // set 3
                AllCurrent[6] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[0].Cells[6].Value) * 100.0);
                AllCurrent[7] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[1].Cells[6].Value) * 100.0);
                AllCurrent[8] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[2].Cells[6].Value) * 100.0);

                // set 4

                AllCurrent[9] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[0].Cells[6].Value) * 100.0);
                AllCurrent[10] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[1].Cells[6].Value) * 100.0);
                AllCurrent[11] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[2].Cells[6].Value) * 100.0);
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[3].Value) * 100.0);  // Harm1 Amp
                }
            }
            // add in load currents
            double[,] lcData = new double[3, 3];
            x = 0;
            for (x = 0; x < 3; x++)
            {
                lcData[x, 0] = Convert.ToDouble(radGridView1.Rows[x].Cells[5].Value); // overal sensitivity
                lcData[x, 1] = Convert.ToDouble(radGridView2.Rows[x].Cells[1].Value); // Pri Amps
                lcData[x, 2] = Convert.ToDouble(radGridView2.Rows[x].Cells[2].Value); // % harm 1
            }

            AllCurrent[12] = Convert.ToUInt16(lcData[0, 1] / lcData[0, 0] * lcData[0, 2] * 100.0 / 2333.0);
            AllCurrent[13] = Convert.ToUInt16(lcData[1, 1] / lcData[1, 0] * lcData[1, 2] * 100.0 / 2333.0);
            AllCurrent[14] = Convert.ToUInt16(lcData[2, 1] / lcData[2, 0] * lcData[2, 2] * 100.0 / 2333.0);      

            res = mbusProtocol.writeMultipleRegisters(99, 52, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }
            
            //harm 1 phase shift
            x = 0;

            if (radCheckBox1.Checked == false)
            {
                // set 1
                AllCurrent[0] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[0].Cells[7].Value) * 100);
                AllCurrent[1] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[1].Cells[7].Value) * 100);
                AllCurrent[2] = Convert.ToUInt16(Convert.ToDouble(radGridView5.Rows[2].Cells[7].Value) * 100);

                // set 2
                AllCurrent[3] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[0].Cells[7].Value) * 100);
                AllCurrent[4] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[1].Cells[7].Value) * 100);
                AllCurrent[5] = Convert.ToUInt16(Convert.ToDouble(radGridView6.Rows[2].Cells[7].Value) * 100);

                // set 3
                AllCurrent[6] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[0].Cells[7].Value) * 100);
                AllCurrent[7] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[1].Cells[7].Value) * 100);
                AllCurrent[8] = Convert.ToUInt16(Convert.ToDouble(radGridView7.Rows[2].Cells[7].Value) * 100);

                // set 4

                AllCurrent[9] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[0].Cells[7].Value) * 100);
                AllCurrent[10] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[1].Cells[7].Value) * 100);
                AllCurrent[11] = Convert.ToUInt16(Convert.ToDouble(radGridView8.Rows[2].Cells[7].Value) * 100);
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[4].Value) * 100);  // Harm1 Amp
                }
            }
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[3].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[3].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[3].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 67, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }
            //----------
            // Harm3 Amp
            x = 0;

            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;  // Harm3 Amp
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[5].Value) * 100);  // Harm3 Amp
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[4].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[4].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[4].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 82, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            //harm 3 phase shift
            x = 0;

            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[6].Value) * 100);  // Harm3 PS
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[5].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[5].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[5].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 97, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }
            //-----------------------
            //harm 5 AMP
            x = 0;
            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[7].Value) * 100);  // Harm1 Amp
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[6].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[6].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[6].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 112, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            //harm 5 phase shift
            x = 0;
            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[8].Value) * 100);  // Harm1 Amp
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[7].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[7].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[7].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 127, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            //-----------------------
            //harm 7 AMP
            x = 0;
            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[9].Value) * 100);  // Harm1 Amp
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[8].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[8].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[8].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 142, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            //harm 7 phase shift
            x = 0;
            if (radCheckBox1.Checked == false)
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = 0;
                }
            }
            else
            {
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    AllCurrent[x] = Convert.ToUInt16(Convert.ToDouble(radGridView3.Rows[x].Cells[10].Value) * 100);  // Harm1 Amp
                }
            }
            // add in load currents
            AllCurrent[12] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[0].Cells[9].Value) * 100);
            AllCurrent[13] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[1].Cells[9].Value) * 100);
            AllCurrent[14] = Convert.ToUInt16(Convert.ToDouble(radGridView2.Rows[2].Cells[9].Value) * 100);

            res = mbusProtocol.writeMultipleRegisters(99, 157, AllCurrent);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error BC and LC Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            // set waveforms
            int[] HRreg23 = new int[15];
            int[] HRreg24 = new int[15];
            int[] HRreg25 = new int[15];
            int y = 0;
            if (radCheckBox1.Checked == false)
            {
                for (y = 0; y < 12; y++)
                {
                    HRreg23[y] = 0;
                    HRreg24[y] = 0;
                    HRreg25[y] = 0;
                }
            }
            else
            {
                // do bushing currents first
                x = 0;
                for (x = 0; x < 11; x++)
                {
                    switch (radGridView3.Rows[x].Cells[11].Value.ToString())
                    {
                        case "Sine":
                            HRreg23[x] = 0;
                            HRreg24[x] = 0;
                            HRreg25[x] = 0;

                            break;
                        case "Square":
                            HRreg23[x] = 0;
                            HRreg24[x] = 0;
                            HRreg25[x] = 1;
                            break;
                        case "Sawtooth":
                            HRreg23[x] = 0;
                            HRreg24[x] = 1;
                            HRreg25[x] = 1;
                            break;
                        case "User Defined":
                            HRreg23[x] = 1;
                            HRreg24[x] = 0;
                            HRreg25[x] = 0;
                            break;
                    }
                }
            }

            // now add the load currnts
            x = 0;
            for (x = 0; x < 3; x++)
            {
                switch (radGridView2.Rows[x].Cells[10].Value.ToString())
                {
                    case "Sine":
                        HRreg23[x + 12] = 0;
                        HRreg24[x + 12] = 0;
                        HRreg25[x + 12] = 0;

                        break;
                    case "Square":
                        HRreg23[x + 12] = 0;
                        HRreg24[x + 12] = 0;
                        HRreg25[x + 12] = 1;
                        break;
                    case "Sawtooth":
                        HRreg23[x + 12] = 0;
                        HRreg24[x + 12] = 1;
                        HRreg25[x + 12] = 1;
                        break;
                    case "User Defined":
                        HRreg23[x + 12] = 1;
                        HRreg24[x + 12] = 0;
                        HRreg25[x + 12] = 0;
                        break;
                }
            }

            // now figure out the decimal equilivant

            // make string out of array
            string strReg23 = "";
            string strReg24 = "";
            string strReg25 = "";
            x = 0;
            for (x = 0; x < 15; x++)
            {
                strReg23 = HRreg23[x] + strReg23;
                strReg24 = HRreg24[x] + strReg24;
                strReg25 = HRreg25[x] + strReg25;
            }

            // now convert to decimal numer and write to registers

            Int16[] waves = new Int16[3];
            waves[0] = Convert.ToInt16(strReg23, 2);
            waves[1] = Convert.ToInt16(strReg24, 2);
            waves[2] = Convert.ToInt16(strReg25, 2);
            res = mbusProtocol.writeMultipleRegisters(99, 23, waves);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error Waveforms Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }

            //write a 1 to reg 29 to trigger system
            res = mbusProtocol.writeSingleRegister(99, 29, 1);
            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Trigger Changes - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mbusProtocol.closeProtocol();
                return;
            }
           
            // close protocol
            mbusProtocol.closeProtocol();

            radButton3.PerformClick();
            //MessageBox.Show("here");
            //Application.UseWaitCursor = false;
        }

        private int FigureOutPDCode(int rowNum)
        {
            int code = 0;

            // enabled - bit 5
            if (radGridView4.Rows[rowNum].Cells[1].Value.ToString() == "False")
            {
                code = 0;
            }
            else
            {
                code = 16;
            }

            // PD Type Code - Polarity or  Amp delay
            //  bit 4
            if (radGridView4.Rows[rowNum].Cells[4].Value.ToString() == "False")
            {
                code = code + 0;
            }
            else
            {
                code = code = code + 8;
            }

            // Polarity Bit 3
            if (radGridView4.Rows[rowNum].Cells[2].Value.ToString() == "Positive")
            {
                code = code + 0;
            }
            else
            {
                code = code = code + 4;
            }

            // voltage bits 1&2

            switch (Convert.ToInt16(radGridView4.Rows[rowNum].Cells[3].Value))
            {
                case 1000:

                    code = code + 0;
                    break;
                case 500:
                    code = code + 1;
                    break;
                case 250:
                    code = code + 2;
                    break;
                case 125:
                    code = code + 3;
                    break;
            }
           
            return code;
        }

        private int openProtocol()
        {
            int result;
            result = mbusProtocol.openProtocol(radDropDownList1.Text, 9600, MbusSerialMasterProtocol.SER_DATABITS_8, MbusSerialMasterProtocol.SER_STOPBITS_1, MbusSerialMasterProtocol.SER_PARITY_NONE);

            if (result != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error Opening Port: " + BusProtocolErrors.getBusProtocolErrorText(result));

                mbusProtocol.closeProtocol();
                return 0;
            }

            return 1;
        }

        private void makeWaveChartLC()
        {
            // get grid data
            LCwave1[0] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column9"].Value);
            LCwave1[1] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column1"].Value) / 100;
            LCwave1[2] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column2"].Value);
            LCwave1[3] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column3"].Value) / 100;
            LCwave1[4] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column4"].Value);
            LCwave1[5] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column5"].Value) / 100;
            LCwave1[6] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column6"].Value);
            LCwave1[7] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column7"].Value) / 100;
            LCwave1[8] = Convert.ToDouble(radGridView2.MasterView.CurrentRow.Cells["column8"].Value);
            LCwaveType1 = radGridView2.MasterView.CurrentRow.Cells["column11"].Value.ToString();

            double[,] sineWaveAmp = new double[3, 720];
            double[] sineWaveDeg = new double[720];
            XYChart c = new XYChart(320, 220);
            // create data
            double[] xData = new double[720];
        
            LineLayer layer = c.addLineLayer2();

            for (int y = 0; y < 720; y++)
            {
                double t = 1.0 / Convert.ToDouble(radSpinEditor6.Text) / 360 * Convert.ToDouble(y);

                double xdata1 = LCwave1[1] * Math.Sin(2 * Math.PI * 60 * 1 * t + (LCwave1[2] * Math.PI / 180));  //1st harm
                double xdata3 = LCwave1[3] * Math.Sin(2 * Math.PI * 60 * 3 * t + (LCwave1[4] * Math.PI / 180));  // 3rd harm
                double xdata5 = LCwave1[5] * Math.Sin(2 * Math.PI * 60 * 5 * t + (LCwave1[6] * Math.PI / 180));  // 5th harm
                double xdata7 = LCwave1[7] * Math.Sin(2 * Math.PI * 60 * 7 * t + (LCwave1[8] * Math.PI / 180));  // 7th Harm
                xData[y] = xdata1 + xdata3 + xdata5 + xdata7;
            }
            layer.addDataSet(xData, -1);

            //get chart ready       
            c.setPlotArea(30, 15, 280, 180);
            c.xAxis().setWidth(2);
            c.yAxis().setWidth(2);
           
            winChartViewer1.Chart = c;
        }

        private void GetCommPorts()
        {
            radDropDownList1.Items.Clear();
            foreach (string s in SerialPort.GetPortNames())
            {
                radDropDownList1.Items.Add(s); 
                radDropDownList1.SelectedIndex = 0;
            }
        }

        private void RadForm1_Load(object sender, EventArgs e)
        {
            try
            {
                MyDocdir = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\DRSimFiles";

                //prevent events from triggering
                formload = true;

                // load avaialbe comm ports into combox

                GetCommPorts();
                InitializeLoadCurrentScreen();
                InitializeAdvBushingCurrentScreen();
                InitializeBushingScreen();
                InitializePDScreen();
                InitializeCurrentOffset();
                InitializeRTDCal();
                Application.DoEvents();

                //set date and time controls
                this.radDateTimePicker2.DateTimePickerElement.ShowCurrentTime = false; 
                this.radDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom; 
                this.radDateTimePicker2.Culture = new System.Globalization.CultureInfo("en-US"); 
                this.radDateTimePicker2.DateTimePickerElement.CustomFormat = "hh:mm:ss tt";
                this.radDateTimePicker2.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = true;
                CalculateLCSensitivity();
                radCheckBox1.Checked = false;
                formload = false;
                radPageView1.SelectedPage = radPageViewPage7;
            }
            catch
            {
                MessageBox.Show("Error Form Load");
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            makeWaveChartLC();
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            // make array
            for (int x = 0; x < 3; x++)
            {
                LCwave[x, 0] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column9"].Value);
                LCwave[x, 1] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column1"].Value) / 100;
                LCwave[x, 2] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column2"].Value);
                LCwave[x, 3] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column3"].Value) / 100;
                LCwave[x, 4] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column4"].Value);
                LCwave[x, 5] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column5"].Value) / 100;
                LCwave[x, 6] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column6"].Value);
                LCwave[x, 7] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column7"].Value) / 100;
                LCwave[x, 8] = Convert.ToDouble(radGridView2.MasterView.Rows[x].Cells["column8"].Value);
                LCwaveType[x] = radGridView2.MasterView.Rows[x].Cells["column11"].Value.ToString();
                makeWaveChartLC();
            }
        }

        private void radGridView2_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            makeWaveChartLC();           
        }

        private void radGridView3_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            // determine if group info row is clicked.  If so ignore otherwise do chart
            if (e.CurrentRow is GridViewGroupRowInfo)
            {
                return;
            }

            MakeBCChart();
        }

        private void MakeBCChart()
        {
            BCwave1[0] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm1Amp"].Value);
            BCwave1[1] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm1PS"].Value);
            BCwave1[2] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm3Amp"].Value);
            BCwave1[3] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm3PS"].Value);
            BCwave1[4] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm5Amp"].Value);
            BCwave1[5] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm5PS"].Value);
            BCwave1[6] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm7Amp"].Value);
            BCwave1[7] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["Harm7PS"].Value);
            //LCwave1[8] = Convert.ToDouble(radGridView3.MasterView.CurrentRow.Cells["column8"].Value);
            try
            {
                BCwaveType1 = radGridView3.MasterView.CurrentRow.Cells["WaveType"].Value.ToString();
            }
            catch
            {
            }

            double[,] sineWaveAmp = new double[3, 720];
            double[] sineWaveDeg = new double[720];
            XYChart c1 = new XYChart(290, 170);
            // create data
            double[] xDataBC = new double[720];
           
            LineLayer layer = c1.addLineLayer2();

            for (int y = 0; y < 720; y++)
            {
                double t = 1.0 / Convert.ToDouble(radSpinEditor6.Text) / 360 * Convert.ToDouble(y);

                double xdata1 = BCwave1[0] * Math.Sin(2 * Math.PI * 60 * 1 * t + (BCwave1[1] * Math.PI / 180));  //1st harm
                double xdata3 = BCwave1[2] * Math.Sin(2 * Math.PI * 60 * 3 * t + (BCwave1[3] * Math.PI / 180));  // 3rd harm
                double xdata5 = BCwave1[4] * Math.Sin(2 * Math.PI * 60 * 5 * t + (BCwave1[5] * Math.PI / 180));  // 5th harm
                double xdata7 = BCwave1[6] * Math.Sin(2 * Math.PI * 60 * 7 * t + (BCwave1[7] * Math.PI / 180));  // 7th Harm
                xDataBC[y] = xdata1 + xdata3 + xdata5 + xdata7;
            }
            layer.addDataSet(xDataBC, -1);

            //get chart ready       
            c1.setPlotArea(30, 15, 250, 130);
            c1.xAxis().setWidth(2);
            c1.yAxis().setWidth(2);
            // c.yAxis().setLinearScale(-1, 1);
            //   c.addLineLayer(sineWaveAmp, 0xff9933);
            winChartViewer2.Chart = c1;
        }

        private void radButton7_Click_1(object sender, EventArgs e)
        {
            MakeBCChart();
        }

        private void radTrackBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (radTrackBar1.Value > radTrackBar2.Value)
            {
                radTrackBar2.Value = radTrackBar1.Value;
            }
            radLabel12.Text = radTrackBar1.Value.ToString();
        }

        private void radTrackBar2_Scroll(object sender, ScrollEventArgs e)
        {
            if (radTrackBar2.Value < radTrackBar1.Value)
            {
                MessageBox.Show("This value cannot be less than " + radTrackBar1.Value.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radTrackBar2.Value = radTrackBar1.Value;
                Application.DoEvents();       
            }
            radLabel13.Text = radTrackBar2.Value.ToString();
        }

        private void radTrackBar3_Scroll(object sender, ScrollEventArgs e)
        { 
            if (radTrackBar3.Value > radTrackBar4.Value)
            {
                radTrackBar4.Value = radTrackBar3.Value;
            }
            radLabel14.Text = radTrackBar3.Value.ToString();
        }

        private void radTrackBar4_Scroll(object sender, ScrollEventArgs e)
        { 
            if (radTrackBar4.Value < radTrackBar3.Value)
            {
                MessageBox.Show("This value cannot be less than " + radTrackBar3.Value.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radTrackBar4.Value = radTrackBar3.Value;
                Application.DoEvents();
            }
            radLabel15.Text = radTrackBar4.Value.ToString();
        }

        private void InitializePDScreen()
        {
            try
            {
                radLabel13.Text = radTrackBar2.Value.ToString();
                radLabel13.Text = radTrackBar2.Value.ToString();
                radLabel14.Text = radTrackBar3.Value.ToString();
                radLabel15.Text = radTrackBar4.Value.ToString();

                // advanced bushing currents Grid Load
                string[] grid31 = new string[] { "BAU01", "true", "Positive", "1000", "false" };
                string[] grid32 = new string[] { "BAU02", "true", "Positive", "1000", "false" };
                string[] grid33 = new string[] { "BAU03", "true", "Positive", "1000", "false" };
                string[] grid34 = new string[] { "BAU04", "true", "Positive", "1000", "false" };
                string[] grid35 = new string[] { "BAU05", "true", "Positive", "1000", "false" };
                string[] grid36 = new string[] { "BAU06", "true", "Positive", "1000", "false" };
                string[] grid37 = new string[] { "BAU07", "true", "Positive", "1000", "false" };
                string[] grid38 = new string[] { "BAU08", "true", "Positive", "1000", "false" };
                string[] grid39 = new string[] { "BAU09", "false", "Negative", "1000", "false" };
                string[] grid310 = new string[] { "BAU10", "false", "Negative", "1000", "false" };
                string[] grid311 = new string[] { "BAU11", "false", "Negative", "1000", "false" };
                string[] grid312 = new string[] { "BAU12", "false", "Negative", "1000", "false" };
                string[] grid313 = new string[] { "BAU13", "false", "Negative", "1000", "false" };
                string[] grid314 = new string[] { "PD14", "false", "Negative", "1000", "false" };
                string[] grid315 = new string[] { "PD15", "false", "Negative", "1000", "false" };
                string[] grid316 = new string[] { "PD16", "false", "Negative", "1000", "false" };

                object[] grid3 = new object[] { grid31, grid32, grid33, grid34, grid35, grid36, grid37, grid38, grid39, grid310, grid311, grid312, grid313, grid314, grid315, grid316 };

                foreach (string[] rowArray2 in grid3)
                {
                    radGridView4.Rows.Add(rowArray2);
                }

                // load Polarity combobox with choices
                GridViewComboBoxColumn comboBoxColumn1 = this.radGridView4.Columns[2] as GridViewComboBoxColumn;
                comboBoxColumn1.DataSource = new string[] { "Positive", "Negative" };

                // load Amplitude combobox with choices
                GridViewComboBoxColumn comboBoxColumn2 = this.radGridView4.Columns[3] as GridViewComboBoxColumn;
                comboBoxColumn2.DataSource = new string[] { "125", "250", "500", "1000" };
            }
            catch
            {
                MessageBox.Show("error - init PD screen");
            }
        }

        private void InitializeAdvBushingCurrentScreen()
        { 
            try
            {
                // advanced bushing currents Grid Load
                string[] grid31 = new string[] { "1", "BAU01", "100.00", "50.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "Sine" };
                string[] grid32 = new string[] { "1", "BAU02", "100.00", "50.00", "120.00", "0.00", "120.00", "0.00", "120.00", "0.00", "120.00", "Sine" };
                string[] grid33 = new string[] { "1", "BAU03", "100.00", "50.00", "240.00", "0.00", "240.00", "0.00", "240.00", "0.00", "240.00", "Sine" };
                string[] grid34 = new string[] { "2", "BAU04", "100.00", "50.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "Sine" };
                string[] grid35 = new string[] { "2", "BAU05", "100.00", "50.00", "120.00", "0.00", "120.00", "0.00", "120.00", "0.00", "120.00", "Sine" };
                string[] grid36 = new string[] { "2", "BAU06", "100.00", "50.00", "240.00", "0.00", "240.00", "0.00", "240.00", "0.00", "240.00", "Sine" };
                string[] grid37 = new string[] { "3", "BAU07", "100.00", "50.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "Sine" };
                string[] grid38 = new string[] { "3", "BAU08", "100.00", "50.00", "120.00", "0.00", "120.00", "0.00", "120.00", "0.00", "120.00", "Sine" };
                string[] grid39 = new string[] { "3", "BAU09", "100.00", "50.00", "240.00", "0.00", "240.00", "0.00", "240.00", "0.00", "240.00", "Sine" };
                string[] grid310 = new string[] { "4", "BAU10", "100.00", "50.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "Sine" };
                string[] grid311 = new string[] { "4", "BAU11", "100.00", "50.00", "120.00", "0.00", "120.00", "0.00", "120.00", "0.00", "120.00", "Sine" };
                string[] grid312 = new string[] { "4", "BAU12", "100.00", "50.00", "240.00", "0.00", "240.00", "0.00", "240.00", "0.00", "240.00", "Sine" };

                object[] grid3 = new object[] { grid31, grid32, grid33, grid34, grid35, grid36, grid37, grid38, grid39, grid310, grid311, grid312 };

                foreach (string[] rowArray2 in grid3)
                {
                    radGridView3.Rows.Add(rowArray2);
                }

                // load combobox with choices
                GridViewComboBoxColumn comboBoxColumn1 = this.radGridView3.Columns[11] as GridViewComboBoxColumn;
                comboBoxColumn1.DataSource = new string[] { "Sine", "Square", "Sawtooth", "User Defined" };

                // allow grouping

                this.radGridView3.EnableGrouping = true;

                //select first row
                this.radGridView3.CurrentRow = this.radGridView3.Rows[0];
            }
            catch
            {
                MessageBox.Show(" Error -Initialize Advanced Bushing Currents");
            }
        }
        
        private void InitializeLoadCurrentScreen()
        {
            try
            {
                // load wavefrom type cb
                //load current grid 1
                string[] row11 = new string[] { "1", "120", "1000", "100", "", "", "" };
                string[] row12 = new string[] { "2", "120", "1000", "100", "", "", "" };
                string[] row13 = new string[] { "3", "120", "1000", "100", "", "", "" };

                object[] rows1 = new object[] { row11, row12, row13 };

                foreach (string[] rowArray in rows1)
                {
                    radGridView1.Rows.Add(rowArray);
                }

                //load current Grid 2 - harmonics
                string[] cbWaveform = new string[] { "Sine", "Square", "Sawtooth", "User Definded" };

                GridViewComboBoxColumn comboBoxColumn = this.radGridView2.Columns[10] as GridViewComboBoxColumn;

                comboBoxColumn.DataSource = new string[] { "Sine", "Square", "Sawtooth", "User Defined" };

                string[] row21 = new string[] { "1", "600.00", "100.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", "Sine" };
                string[] row22 = new string[] { "2", "600.00", "100.00", "120.00", "0.00", "120.00", "0.00", "120.00", "0.00", "120.00", "Sine" };
                string[] row23 = new string[] { "3", "600.00", "100.00", "240.00", "0.00", "240.00", "0.00", "240.00", "0.00", "240.00", "Sine" };

                object[] rows2 = new object[] { row21, row22, row23 };

                foreach (string[] rowArray1 in rows2)
                {
                    radGridView2.Rows.Add(rowArray1);
                }

                this.radGridView2.CurrentRow = this.radGridView2.Rows[0];
                this.radGridView1.CurrentRow = this.radGridView1.Rows[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error -Initialize LC Screen - " + ex.Message);
            }
        }

        private void InitializeBushingScreen()
        {
            //set 1
            try
            {
                string[] row21 = new string[] { "BAU01", "A", "500", "0.35", "0", "0", "0" };
                string[] row22 = new string[] { "BAU02", "B", "500", "0.35", "0", "0", "0" };
                string[] row23 = new string[] { "BAU03", "C", "500", "0.35", "0", "0", "0" };

                object[] rows2 = new object[] { row21, row22, row23 };

                foreach (string[] rowArray1 in rows2)
                {
                    radGridView5.Rows.Add(rowArray1);
                }
                radTextBox4.Text = "500";
                //load combobox
                GridViewComboBoxColumn comboBoxColumn5 = this.radGridView5.Columns["Phase"] as GridViewComboBoxColumn;
                comboBoxColumn5.DataSource = new string[] { "A", "B", "C" };

                //  set 2
                string[] row61 = new string[] { "BAU01", "A", "500", "0.35", "0", "0", "0" };
                string[] row62 = new string[] { "BAU02", "B", "500", "0.35", "0", "0", "0" };
                string[] row63 = new string[] { "BAU03", "C", "500", "0.35", "0", "0", "0" };

                object[] rows62 = new object[] { row61, row62, row63 };

                foreach (string[] rowArray61 in rows62)
                {
                    radGridView6.Rows.Add(rowArray61);
                }
                radTextBox5.Text = "230";
                //load combobox
                GridViewComboBoxColumn comboBoxColumn6 = this.radGridView6.Columns["Phase"] as GridViewComboBoxColumn;
                comboBoxColumn6.DataSource = new string[] { "A", "B", "C" };

                // set 3
                string[] row71 = new string[] { "BAU01", "A", "500", "0.35", "0", "0", "0" };
                string[] row72 = new string[] { "BAU02", "B", "500", "0.35", "0", "0", "0" };
                string[] row73 = new string[] { "BAU03", "C", "500", "0.35", "0", "0", "0" };

                object[] rows72 = new object[] { row71, row72, row73 };

                foreach (string[] rowArray71 in rows72)
                {
                    radGridView7.Rows.Add(rowArray71);
                }
                radTextBox6.Text = "115";
                //load combobox
                GridViewComboBoxColumn comboBoxColumn7 = this.radGridView7.Columns["Phase"] as GridViewComboBoxColumn;
                comboBoxColumn7.DataSource = new string[] { "A", "B", "C" };

                // set 4
                string[] row81 = new string[] { "BAU01", "A", "500", "0.35", "0", "0", "0" };
                string[] row82 = new string[] { "BAU02", "B", "500", "0.35", "0", "0", "0" };
                string[] row83 = new string[] { "BAU03", "C", "500", "0.35", "0", "0", "0" };

                object[] rows82 = new object[] { row81, row82, row83 };

                foreach (string[] rowArray81 in rows82)
                {
                    radGridView8.Rows.Add(rowArray81);
                }
                radTextBox7.Text = "69";
                //load combobox
                GridViewComboBoxColumn comboBoxColumn8 = this.radGridView8.Columns["Phase"] as GridViewComboBoxColumn;
                comboBoxColumn8.DataSource = new string[] { "A", "B", "C" };

                //go calculate current values
                CalculateBCurrentValues();
            }
            catch
            {
                MessageBox.Show("Error Bushing screen");
            }
        }

        private void CalculateLCSensitivity()
        {
            for (int x = 0; x < 3; x++)
            {
                double mvA;
                mvA = (1 / (Convert.ToDouble(radGridView1.Rows[x].Cells[2].Value)) * 1000.00 * Convert.ToDouble(radGridView1.Rows[x].Cells[3].Value));
                radGridView1.Rows[x].Cells[4].Value = mvA.ToString("F");
                radGridView1.Rows[x].Cells[5].Value = (Convert.ToDouble(radGridView1.Rows[x].Cells[1].Value) / mvA).ToString("F");
            }
        }

        private void CalculateBCurrentValues()
        {
            double Z = 0;
            double kV = 0;
            double pf;
            double pa;
            int x = 0;
            try
            {
                //set 1 radgridview5
                kV = Convert.ToDouble(radTextBox4.Text);
                for (x = 0; x < 3; x++)
                {
                    // ma
                    this.radGridView5.CurrentRow = this.radGridView5.Rows[x];
                    double cap = Convert.ToDouble(radGridView5.CurrentRow.Cells["Cap"].Value) + Convert.ToDouble(radGridView5.CurrentRow.Cells["NewCap"].Value);
                    Z = 1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * cap * Math.Pow(10, -12.0));
                    this.radGridView5.CurrentRow.Cells["NewCurrent"].Value = Convert.ToString(Math.Round(kV * 1000000 / Math.Sqrt(3) / Z, 2));
                    // calculate back the cap difference due current rounding

                    //double Xc = (Convert.ToDouble(radTextBox4.Text) *1000000.0 / Math.Sqrt(3)) / Convert.ToDouble(radGridView5.CurrentRow.Cells["NewCurrent"].Value);
                    //  double Cap1 = ((1.0 / Xc) / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value)) * Math.Pow(10,12));
                    //   double tc = Math.Round(1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * Z) * Math.Pow(10, 12), 2);
                    // subtract inital cap
                    //   radGridView5.Rows[x].Cells[4].Value = Convert.ToString(Math.Round(Cap1 - Convert.ToDouble(radGridView5.Rows[x].Cells[2].Value), 0));

                    // phase angle

                    pf = (Convert.ToDouble(radGridView5.CurrentRow.Cells["PF"].Value) + Convert.ToDouble(radGridView5.CurrentRow.Cells["column2"].Value)) / 100;
                    
                    switch (radGridView5.CurrentRow.Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            pa = 0 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView5.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "B":
                            pa = 120 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView5.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "C":
                            pa = 240 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView5.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Set 1 BC  Calcs - " + ex.Message);
            }
            try
            {
                //set 2 radgridview6
                kV = Convert.ToDouble(radTextBox5.Text);
                for (x = 0; x < 3; x++)
                {
                    this.radGridView6.CurrentRow = this.radGridView6.Rows[x];
                    double cap = Convert.ToDouble(radGridView6.CurrentRow.Cells["Cap"].Value) + Convert.ToDouble(radGridView6.CurrentRow.Cells["NewCap"].Value);
                    Z = 1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * cap * Math.Pow(10, -12.0));
                    this.radGridView6.CurrentRow.Cells["NewCurrent"].Value = Convert.ToString(Math.Round(kV * 1000000 / Math.Sqrt(3) / Z, 2));

                    // need to update Cap calcs due to rounding error and accuracy based on the rounded current
                     
                    // phase angle

                    pf = (Convert.ToDouble(radGridView6.CurrentRow.Cells["PF"].Value) + Convert.ToDouble(radGridView6.CurrentRow.Cells["column2"].Value)) / 100;

                    switch (radGridView6.CurrentRow.Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            pa = 0 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView6.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "B":
                            pa = 120 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView6.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "C":
                            pa = 240 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView6.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Set 2 BC  Calcs - " + ex.Message);
            }

            try
            {
                //set 3 radgridview7
                kV = Convert.ToDouble(radTextBox6.Text);
                for (x = 0; x < 3; x++)
                {
                    this.radGridView7.CurrentRow = this.radGridView7.Rows[x];
                    double cap = Convert.ToDouble(radGridView7.CurrentRow.Cells["Cap"].Value) + Convert.ToDouble(radGridView7.CurrentRow.Cells["NewCap"].Value);
                    Z = 1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * cap * Math.Pow(10, -12.0));
                    this.radGridView7.CurrentRow.Cells["NewCurrent"].Value = Convert.ToString(Math.Round(kV * 1000000 / Math.Sqrt(3) / Z, 2));
                    // phase angle

                    pf = (Convert.ToDouble(radGridView7.CurrentRow.Cells["PF"].Value) + Convert.ToDouble(radGridView7.CurrentRow.Cells["column2"].Value)) / 100;

                    switch (radGridView7.CurrentRow.Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            pa = 0 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView7.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "B":
                            pa = 120 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView7.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "C":
                            pa = 240 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView7.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Set 3 BC  Calcs - " + ex.Message);
            }

            try
            {
                //set 4 radgridview8
                kV = Convert.ToDouble(radTextBox7.Text);
                for (x = 0; x < 3; x++)
                {
                    this.radGridView8.CurrentRow = this.radGridView8.Rows[x];
                    double cap = Convert.ToDouble(radGridView8.CurrentRow.Cells["Cap"].Value) + Convert.ToDouble(radGridView8.CurrentRow.Cells["NewCap"].Value);
                    Z = 1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * cap * Math.Pow(10, -12.0));
                    this.radGridView8.CurrentRow.Cells["NewCurrent"].Value = Convert.ToString(Math.Round(kV * 1000000 / Math.Sqrt(3) / Z, 2));

                    // phase angle

                    pf = (Convert.ToDouble(radGridView8.CurrentRow.Cells["PF"].Value) + Convert.ToDouble(radGridView8.CurrentRow.Cells["column2"].Value)) / 100;

                    switch (radGridView8.CurrentRow.Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            pa = 0 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView8.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "B":
                            pa = 120 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView8.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                        case "C":
                            pa = 240 + Math.Atan(pf) * 180 / Math.PI;
                            this.radGridView8.CurrentRow.Cells["NewPhase"].Value = pa.ToString("F");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Set 4 BC  Calcs - " + ex.Message);
            }
        }

        private void radGridView8_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radGridView7_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radGridView6_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radGridView5_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            CalculateBCurrentValues();
        }

        private void radTextBox4_TextChanged(object sender, EventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radTextBox5_TextChanged(object sender, EventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radTextBox6_TextChanged(object sender, EventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radTextBox7_TextChanged(object sender, EventArgs e)
        {
            if (formload == false)
            {
                CalculateBCurrentValues();
            }
        }

        private void radCheckBox1_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox1.Checked)
            {
                radPageViewPage4.Item.Visibility = ElementVisibility.Visible;
                radPageViewPage3.Item.Visibility = ElementVisibility.Collapsed;
                radPageView1.SelectedPage = radPageViewPage4;
            }
            else
            {
                radPageViewPage4.Item.Visibility = ElementVisibility.Collapsed;
                radPageViewPage3.Item.Visibility = ElementVisibility.Visible;
                radPageView1.SelectedPage = radPageViewPage3;
            }
        }

        private void InitializeCurrentOffset()
        {
            try
            {
                string[] grid31 = new string[] { "BAU01", "2048" };
                string[] grid32 = new string[] { "BAU02", "2048" };
                string[] grid33 = new string[] { "BAU03", "2048" };
                string[] grid34 = new string[] { "BAU04", "2048" };
                string[] grid35 = new string[] { "BAU05", "2048" };
                string[] grid36 = new string[] { "BAU06", "2048" };
                string[] grid37 = new string[] { "BAU07", "2048" };
                string[] grid38 = new string[] { "BAU08", "2048" };
                string[] grid39 = new string[] { "BAU09", "2048" };
                string[] grid310 = new string[] { "BAU10", "2048" };
                string[] grid311 = new string[] { "BAU11", "2048" };
                string[] grid312 = new string[] { "BAU12", "2048" };
                string[] grid313 = new string[] { "LC - 1", "2048" };
                string[] grid314 = new string[] { "LC - 2", "2048" };
                string[] grid315 = new string[] { "LC - 3", "2048" };

                object[] grid3 = new object[] { grid31, grid32, grid33, grid34, grid35, grid36, grid37, grid38, grid39, grid310, grid311, grid312, grid313, grid314, grid315 };

                foreach (string[] rowArray2 in grid3)
                {
                    radGridView9.Rows.Add(rowArray2);
                }
            }
            catch
            {
                MessageBox.Show("Error in current offset Initialization");
            }
        }

        private void InitializeRTDCal()
        {
            try
            {
                //allow grouping
                this.radGridView10.EnableGrouping = true;

                // Load Grid
                string[] grid31 = new string[] { "Pot1", "0", "1200.00", "53.00" };
                string[] grid32 = new string[] { "Pot1", "0", "1200.00", "53.00" };
                string[] grid33 = new string[] { "Pot1", "0", "1200.00", "53.00" };
                string[] grid34 = new string[] { "Pot1", "0", "1200.00", "53.00" };
                string[] grid35 = new string[] { "Pot2", "1", "1200.00", "53.00" };
                string[] grid36 = new string[] { "Pot2", "1", "1200.00", "53.00" };
                string[] grid37 = new string[] { "Pot2", "1", "1200.00", "53.00" };
                string[] grid38 = new string[] { "Pot2", "1", "1200.00", "53.00" };
                
                object[] grid3 = new object[] { grid31, grid32, grid33, grid34, grid35, grid36, grid37, grid38 };

                foreach (string[] rowArray2 in grid3)
                {
                    radGridView10.Rows.Add(rowArray2);
                }
                // MessageBox.Show(coeff.CalMisc.RES0Column.ToString());
            }
            catch
            {
                MessageBox.Show("Error in current RTD Coefficients");
            }
        }

        private void radButton4_Click(object sender, EventArgs e) // save config file
        {
            try
            {
                int x = 0;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.InitialDirectory = MyDocdir;
                
                saveFileDialog1.Title = "Save Configuration File";
                saveFileDialog1.CheckFileExists = false;
                saveFileDialog1.CheckPathExists = false;
                saveFileDialog1.DefaultExt = "xml";
                saveFileDialog1.Filter = "XML Files (*.xml)| *.xml";
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    appFileName = saveFileDialog1.FileName;
                }
                else
                {
                    return;
                }

                // now get all data 

                // create data set
                System.Data.DataSet DRSim = new System.Data.DataSet("DRSim");

                // Create LC1 DataTable ( Load Current 1 )
                DataTable LC1 = new DataTable("LC1");
                LC1.Columns.Add("Channel");
                LC1.Columns.Add("PriCTRatio");
                LC1.Columns.Add("AuxCTRatio");
                LC1.Columns.Add("Burden");
                LC1.Columns.Add("Sensitivity");
                LC1.Columns.Add("OverallSensitivity");
                //  LC1.Columns.Add("WaveformType");
                DRSim.Tables.Add(LC1);

                // Get Data from GridView1
                for (x = 0; x < radGridView1.RowCount; x++)
                {
                    DataRow gv1 = LC1.NewRow();
                    gv1[0] = radGridView1.Rows[x].Cells[0].Value;
                    gv1[1] = radGridView1.Rows[x].Cells[1].Value;
                    gv1[2] = radGridView1.Rows[x].Cells[2].Value;
                    gv1[3] = radGridView1.Rows[x].Cells[3].Value;
                    gv1[4] = radGridView1.Rows[x].Cells[4].Value;
                    gv1[5] = radGridView1.Rows[x].Cells[5].Value;
                    //  gv1[6] = radGridView1.Rows[x].Cells[6].Value;
                    LC1.Rows.Add(gv1);
                }

                // will write to xml later

                // lets get load current grid 2
                DataTable LC2 = new DataTable("LC2");
                LC2.Columns.Add("Channel");
                LC2.Columns.Add("PriOutput");
                LC2.Columns.Add("Harm1Amp");
                LC2.Columns.Add("Harm1Angle");
                LC2.Columns.Add("Harm3Amp");
                LC2.Columns.Add("Harm3Angle");
                LC2.Columns.Add("Harm5Amp");
                LC2.Columns.Add("Harm5Angle");
                LC2.Columns.Add("Harm7Amp");
                LC2.Columns.Add("Harm7Angle");
                LC2.Columns.Add("WaveformType");
                DRSim.Tables.Add(LC2);

                // Get Data from GridView2 LC2
                for (x = 0; x < radGridView2.RowCount; x++)
                {
                    DataRow gv2 = LC2.NewRow();
                    gv2[0] = radGridView2.Rows[x].Cells[0].Value;
                    gv2[1] = radGridView2.Rows[x].Cells[1].Value;
                    gv2[2] = radGridView2.Rows[x].Cells[2].Value;
                    gv2[3] = radGridView2.Rows[x].Cells[3].Value;
                    gv2[4] = radGridView2.Rows[x].Cells[4].Value;
                    gv2[5] = radGridView2.Rows[x].Cells[5].Value;
                    gv2[6] = radGridView2.Rows[x].Cells[6].Value;
                    gv2[7] = radGridView2.Rows[x].Cells[7].Value;
                    gv2[8] = radGridView2.Rows[x].Cells[8].Value;
                    gv2[9] = radGridView2.Rows[x].Cells[9].Value;
                    gv2[10] = radGridView2.Rows[x].Cells[10].Value;
                
                    LC2.Rows.Add(gv2);
                }

                // will write to xml later
                // Bushing Tab - 4 radgrids  + one voltage per grid
                // set 1

                DataTable BushSet1 = new DataTable("BushSet1");
                BushSet1.Columns.Add("Channel");
                BushSet1.Columns.Add("Phase");
                BushSet1.Columns.Add("InitCap");
                BushSet1.Columns.Add("InitPF");
                BushSet1.Columns.Add("ChangeCap");
                BushSet1.Columns.Add("ChangePF");
                BushSet1.Columns.Add("Current");           
                DRSim.Tables.Add(BushSet1);

                // Get Data from GridView5 BushSet1
                for (x = 0; x < radGridView5.RowCount; x++)
                {
                    DataRow gv5 = BushSet1.NewRow();
                    gv5[0] = radGridView5.Rows[x].Cells[0].Value;
                    gv5[1] = radGridView5.Rows[x].Cells[1].Value;
                    gv5[2] = radGridView5.Rows[x].Cells[2].Value;
                    gv5[3] = radGridView5.Rows[x].Cells[3].Value;
                    gv5[4] = radGridView5.Rows[x].Cells[4].Value;
                    gv5[5] = radGridView5.Rows[x].Cells[5].Value;
                    gv5[6] = radGridView5.Rows[x].Cells[6].Value;
               
                    BushSet1.Rows.Add(gv5);
                }

                // Set2
                DataTable BushSet2 = new DataTable("BushSet2");
                BushSet2.Columns.Add("Channel");
                BushSet2.Columns.Add("Phase");
                BushSet2.Columns.Add("InitCap");
                BushSet2.Columns.Add("InitPF");
                BushSet2.Columns.Add("ChangeCap");
                BushSet2.Columns.Add("ChangePF");
                BushSet2.Columns.Add("Current");
                DRSim.Tables.Add(BushSet2);

                // Get Data from GridView6 BushSet2
                for (x = 0; x < radGridView5.RowCount; x++)
                {
                    DataRow gv6 = BushSet2.NewRow();
                    gv6[0] = radGridView6.Rows[x].Cells[0].Value;
                    gv6[1] = radGridView6.Rows[x].Cells[1].Value;
                    gv6[2] = radGridView6.Rows[x].Cells[2].Value;
                    gv6[3] = radGridView6.Rows[x].Cells[3].Value;
                    gv6[4] = radGridView6.Rows[x].Cells[4].Value;
                    gv6[5] = radGridView6.Rows[x].Cells[5].Value;
                    gv6[6] = radGridView6.Rows[x].Cells[6].Value;
             
                    BushSet2.Rows.Add(gv6);
                }
                
                // Set3
                DataTable BushSet3 = new DataTable("BushSet3");
                BushSet3.Columns.Add("Channel");
                BushSet3.Columns.Add("Phase");
                BushSet3.Columns.Add("InitCap");
                BushSet3.Columns.Add("InitPF");
                BushSet3.Columns.Add("ChangeCap");
                BushSet3.Columns.Add("ChangePF");
                BushSet3.Columns.Add("Current");
                DRSim.Tables.Add(BushSet3);

                // Get Data from GridView7 BushSet3
                for (x = 0; x < radGridView5.RowCount; x++)
                {
                    DataRow gv7 = BushSet3.NewRow();
                    gv7[0] = radGridView7.Rows[x].Cells[0].Value;
                    gv7[1] = radGridView7.Rows[x].Cells[1].Value;
                    gv7[2] = radGridView7.Rows[x].Cells[2].Value;
                    gv7[3] = radGridView7.Rows[x].Cells[3].Value;
                    gv7[4] = radGridView7.Rows[x].Cells[4].Value;
                    gv7[5] = radGridView7.Rows[x].Cells[5].Value;
                    gv7[6] = radGridView7.Rows[x].Cells[6].Value;
                
                    BushSet3.Rows.Add(gv7);
                }
                // Set4
                DataTable BushSet4 = new DataTable("BushSet4");
                BushSet4.Columns.Add("Channel");
                BushSet4.Columns.Add("Phase");
                BushSet4.Columns.Add("InitCap");
                BushSet4.Columns.Add("InitPF");
                BushSet4.Columns.Add("ChangeCap");
                BushSet4.Columns.Add("ChangePF");
                BushSet4.Columns.Add("Current");
                DRSim.Tables.Add(BushSet4);

                // Get Data from GridView8 BushSet4
                for (x = 0; x < radGridView5.RowCount; x++)
                {
                    DataRow gv8 = BushSet4.NewRow();
                    gv8[0] = radGridView8.Rows[x].Cells[0].Value;
                    gv8[1] = radGridView8.Rows[x].Cells[1].Value;
                    gv8[2] = radGridView8.Rows[x].Cells[2].Value;
                    gv8[3] = radGridView8.Rows[x].Cells[3].Value;
                    gv8[4] = radGridView8.Rows[x].Cells[4].Value;
                    gv8[5] = radGridView8.Rows[x].Cells[5].Value;
                    gv8[6] = radGridView8.Rows[x].Cells[6].Value;
                
                    BushSet4.Rows.Add(gv8);
                }
                // Advanced Bushing current
                DataTable AdvBushCur = new DataTable("AdvBushCur");
                AdvBushCur.Columns.Add("Set");
                AdvBushCur.Columns.Add("Channel");
                AdvBushCur.Columns.Add("Burden");
                AdvBushCur.Columns.Add("Harm1Amp");
                AdvBushCur.Columns.Add("Harm1Angle");
                AdvBushCur.Columns.Add("Harm3Amp");
                AdvBushCur.Columns.Add("Harm3Angle");
                AdvBushCur.Columns.Add("Harm5Amp");
                AdvBushCur.Columns.Add("Harm5Angle");
                AdvBushCur.Columns.Add("Harm7Amp");
                AdvBushCur.Columns.Add("Harm7Angle");
                AdvBushCur.Columns.Add("WaveFormType");

                DRSim.Tables.Add(AdvBushCur);

                // Get Data from GridView3 AdvBushCur
                for (x = 0; x < radGridView3.RowCount; x++)
                {
                    DataRow gv3 = AdvBushCur.NewRow();
                    gv3[0] = radGridView3.Rows[x].Cells[0].Value;
                    gv3[1] = radGridView3.Rows[x].Cells[1].Value;
                    gv3[2] = radGridView3.Rows[x].Cells[2].Value;
                    gv3[3] = radGridView3.Rows[x].Cells[3].Value;
                    gv3[4] = radGridView3.Rows[x].Cells[4].Value;
                    gv3[5] = radGridView3.Rows[x].Cells[5].Value;
                    gv3[6] = radGridView3.Rows[x].Cells[6].Value;
                    gv3[7] = radGridView3.Rows[x].Cells[7].Value;
                    gv3[8] = radGridView3.Rows[x].Cells[8].Value;
                    gv3[9] = radGridView3.Rows[x].Cells[9].Value;
                    gv3[10] = radGridView3.Rows[x].Cells[10].Value;
                    gv3[11] = radGridView3.Rows[x].Cells[11].Value;
                    AdvBushCur.Rows.Add(gv3);         
                }

                // PD Settings
                DataTable PD = new DataTable("PD");
                PD.Columns.Add("Channel");
                PD.Columns.Add("Enabled");
                PD.Columns.Add("Polarity");
                PD.Columns.Add("Amp");
                PD.Columns.Add("Delay");
            
                DRSim.Tables.Add(PD);

                // Get Data from GridView4 PD
                for (x = 0; x < radGridView4.RowCount; x++)
                {
                    DataRow gv4 = PD.NewRow();
                    gv4[0] = radGridView4.Rows[x].Cells[0].Value;
                    gv4[1] = radGridView4.Rows[x].Cells[1].Value;
                    gv4[2] = radGridView4.Rows[x].Cells[2].Value;
                    gv4[3] = radGridView4.Rows[x].Cells[3].Value;
                    gv4[4] = radGridView4.Rows[x].Cells[4].Value;
                
                    PD.Rows.Add(gv4);
                }
                // MiscItems

                DataTable Misc = new DataTable("Misc");
                Misc.Columns.Add("PD+Start");
                Misc.Columns.Add("PD+Stop");
                Misc.Columns.Add("PD-Start");
                Misc.Columns.Add("PD-Stop");
                Misc.Columns.Add("kVBushSet1");
                Misc.Columns.Add("kVBushSet2");
                Misc.Columns.Add("kVBushSet3");
                Misc.Columns.Add("kVBushSet4");
                Misc.Columns.Add("Temp1");
                Misc.Columns.Add("Temp2");
                Misc.Columns.Add("Temp3");
                Misc.Columns.Add("Temp4");
                Misc.Columns.Add("Humidity");
                Misc.Columns.Add("CommPort");
                Misc.Columns.Add("Freq");
                Misc.Columns.Add("PPS");
                Misc.Columns.Add("PulseWidth");

                DRSim.Tables.Add(Misc);

                // Get Data from MISC Fields            
                DataRow dtMisc = Misc.NewRow();
                dtMisc[0] = radTrackBar1.Value;
                dtMisc[1] = radTrackBar2.Value;
                dtMisc[2] = radTrackBar3.Value;
                dtMisc[3] = radTrackBar4.Value;
                dtMisc[4] = radTextBox4.Text;
                dtMisc[5] = radTextBox5.Text;
                dtMisc[6] = radTextBox6.Text;
                dtMisc[7] = radTextBox7.Text;
                dtMisc[8] = radSpinEditor1.Value;
                dtMisc[9] = radSpinEditor4.Value;
                dtMisc[10] = radSpinEditor2.Value;
                dtMisc[11] = radSpinEditor3.Value;
                dtMisc[12] = radSpinEditor5.Value;
                dtMisc[13] = radDropDownList1.Text;
                dtMisc[14] = radSpinEditor6.Value;
                dtMisc[15] = radSpinEditor12.Value;
                dtMisc[16] = radSpinEditor13.Value;
                
                Misc.Rows.Add(dtMisc);
            
                // Write XML FILE
                DRSim.WriteXml(appFileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Save Configuration - " + ex.Message);
            }
        }

        private void radButton2_Click(object sender, EventArgs e)  // load cfg from file.
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = MyDocdir;
            openFileDialog1.Title = "Open Configuration File";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "xml";
            openFileDialog1.Filter = "XML Files (*.xml)| *.xml";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                appFileName = openFileDialog1.FileName;
            }
            else
            {
                return;
            }

            int x = 0;
            System.Data.DataSet ds = new System.Data.DataSet();

            ds.ReadXml(appFileName);
            x = 0;
            foreach (DataRow row in ds.Tables["LC1"].Rows)
            {
                radGridView1.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView1.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView1.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView1.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView1.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView1.Rows[x].Cells[5].Value = row.ItemArray[5];
                // radGridView1.Rows[x].Cells[6].Value = row.ItemArray[6];
                x++;
            }
            
            x = 0;
            foreach (DataRow row in ds.Tables["LC2"].Rows)
            {
                radGridView2.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView2.Rows[x].Cells[1].Value = row.ItemArray[1];                
                radGridView2.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView2.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView2.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView2.Rows[x].Cells[5].Value = row.ItemArray[5];
                radGridView2.Rows[x].Cells[6].Value = row.ItemArray[6];
                radGridView2.Rows[x].Cells[7].Value = row.ItemArray[7];                
                radGridView2.Rows[x].Cells[8].Value = row.ItemArray[8];
                radGridView2.Rows[x].Cells[9].Value = row.ItemArray[9];
                radGridView2.Rows[x].Cells[10].Value = row.ItemArray[10];

                x++;
            }
            x = 0;
            foreach (DataRow row in ds.Tables["BushSet1"].Rows)
            {
                radGridView5.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView5.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView5.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView5.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView5.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView5.Rows[x].Cells[5].Value = row.ItemArray[5];
                radGridView5.Rows[x].Cells[6].Value = row.ItemArray[6];
                x++;
            }
            x = 0;
            foreach (DataRow row in ds.Tables["BushSet2"].Rows)
            {
                radGridView6.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView6.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView6.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView6.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView6.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView6.Rows[x].Cells[5].Value = row.ItemArray[5];
                radGridView6.Rows[x].Cells[6].Value = row.ItemArray[6];
                x++;
            }
            x = 0;
            foreach (DataRow row4 in ds.Tables["BushSet3"].Rows)
            {
                radGridView7.Rows[x].Cells[0].Value = row4.ItemArray[0];
                radGridView7.Rows[x].Cells[1].Value = row4.ItemArray[1];
                radGridView7.Rows[x].Cells[2].Value = row4.ItemArray[2];
                radGridView7.Rows[x].Cells[3].Value = row4.ItemArray[3];
                radGridView7.Rows[x].Cells[4].Value = row4.ItemArray[4];
                radGridView7.Rows[x].Cells[5].Value = row4.ItemArray[5];
                radGridView7.Rows[x].Cells[6].Value = row4.ItemArray[6];
                x++;
            }
            x = 0;
            foreach (DataRow row in ds.Tables["BushSet4"].Rows)
            {
                radGridView8.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView8.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView8.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView8.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView8.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView8.Rows[x].Cells[5].Value = row.ItemArray[5];
                radGridView8.Rows[x].Cells[6].Value = row.ItemArray[6];
                x++;
            }
            x = 0;
            foreach (DataRow row in ds.Tables["AdvBushCur"].Rows)
            {
                radGridView3.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView3.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView3.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView3.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView3.Rows[x].Cells[4].Value = row.ItemArray[4];
                radGridView3.Rows[x].Cells[5].Value = row.ItemArray[5];
                radGridView3.Rows[x].Cells[6].Value = row.ItemArray[6];
                radGridView3.Rows[x].Cells[7].Value = row.ItemArray[7];
                radGridView3.Rows[x].Cells[8].Value = row.ItemArray[8];
                radGridView3.Rows[x].Cells[9].Value = row.ItemArray[9];
                radGridView3.Rows[x].Cells[10].Value = row.ItemArray[10];
                radGridView3.Rows[x].Cells[11].Value = row.ItemArray[11];
                x++;
            }

            x = 0;
            foreach (DataRow row in ds.Tables["PD"].Rows)
            {
                radGridView4.Rows[x].Cells[0].Value = row.ItemArray[0];
                radGridView4.Rows[x].Cells[1].Value = row.ItemArray[1];
                radGridView4.Rows[x].Cells[2].Value = row.ItemArray[2];
                radGridView4.Rows[x].Cells[3].Value = row.ItemArray[3];
                radGridView4.Rows[x].Cells[4].Value = row.ItemArray[4];
                x++;
            }

            x = 0;
            foreach (DataRow row in ds.Tables["Misc"].Rows)
            {
                radTrackBar1.Value = Convert.ToInt16(row.ItemArray[0]);
                radTrackBar2.Value = Convert.ToInt16(row.ItemArray[1]);
                radTrackBar3.Value = Convert.ToInt16(row.ItemArray[2]);
                radTrackBar4.Value = Convert.ToInt16(row.ItemArray[3]);
                radTextBox4.Text = row.ItemArray[4].ToString();
                radTextBox5.Text = row.ItemArray[5].ToString();
                radTextBox6.Text = row.ItemArray[5].ToString();
                radTextBox7.Text = row.ItemArray[7].ToString();
                radSpinEditor1.Value = Convert.ToDecimal(row.ItemArray[8]);
                radSpinEditor4.Value = Convert.ToDecimal(row.ItemArray[9]);
                radSpinEditor2.Value = Convert.ToDecimal(row.ItemArray[10]);
                radSpinEditor3.Value = Convert.ToDecimal(row.ItemArray[11]);
                radSpinEditor5.Value = Convert.ToDecimal(row.ItemArray[12]);
                radDropDownList1.Text = row.ItemArray[13].ToString();
                radSpinEditor6.Value = Convert.ToDecimal(row.ItemArray[14]);
                radSpinEditor12.Value = Convert.ToDecimal(row.ItemArray[15]);
                radSpinEditor13.Value = Convert.ToDecimal(row.ItemArray[16]);

                x++;
            }
            return;
        }

        private void radButton9_Click(object sender, EventArgs e)  // Save coeffs to file
        {
            if (radTextBox2.Text == "")
            {
                MessageBox.Show("You must be connected to a DRSim and have Loaded Config");
                return;
            }
            coeff.Tables["CalMisc"].Clear();
            DataRow newCalMiscRow = coeff.Tables["CalMisc"].NewRow();
            
            newCalMiscRow["RES0"] = radSpinEditor7.Value;
            newCalMiscRow["RES3"] = radSpinEditor8.Value;
            newCalMiscRow["RES4"] = radSpinEditor9.Value;
            newCalMiscRow["RTDTempCoeff"] = radSpinEditor10.Value;
            newCalMiscRow["RTDatZero"] = radSpinEditor11.Value;
            newCalMiscRow["BoardSer"] = radTextBox2.Text;
            coeff.Tables["CalMisc"].Rows.Add(newCalMiscRow);
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "coe files (*.coe)|*.coe";
            // saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.FileName = radTextBox2.Text;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                coeff.WriteXml(saveFileDialog1.FileName);
            }
        }

        private void radButton8_Click(object sender, EventArgs e)  // read coeffs from file
        {
            if (radTextBox2.Text == "")
            {
                MessageBox.Show("You must be connected to a DRSim and have Loaded Config");
                return;
            }
            coeff.Tables["RTCal"].Clear();
            coeff.Tables["CurOffsets"].Clear();
            coeff.Tables["CalMisc"].Clear();
            string fname = "";

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = MyDocdir;
            openFileDialog1.Title = "Open Configuration File";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "coe";
            openFileDialog1.Filter = "coe Files (*.coe)| *.coe";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fname = openFileDialog1.SafeFileName.Replace(".coe","");

                if (fname != radTextBox2.Text)
                {
                    MessageBox.Show("The coefficient file serial numbers do not match this DRSim");
                    return;
                }
                coeff.ReadXml(openFileDialog1.FileName);

                
                foreach (DataRow row in coeff.Tables["CalMisc"].Rows)
                {
                    radSpinEditor7.Value = Convert.ToDecimal(row.ItemArray[0]);
                    radSpinEditor8.Value = Convert.ToDecimal(row.ItemArray[1]);
                    radSpinEditor9.Value = Convert.ToDecimal(row.ItemArray[2]);
                    radSpinEditor10.Value = Convert.ToDecimal(row.ItemArray[3]);
                    radSpinEditor11.Value = Convert.ToDecimal(row.ItemArray[4]);
                    radTextBox2.Text = row.ItemArray[5].ToString();
                     /*if (row.ItemArray[5].ToString() == radTextBox2.Text)
                    {
                        radSpinEditor7.Value = Convert.ToDecimal(row.ItemArray[0]);
                        radSpinEditor8.Value = Convert.ToDecimal(row.ItemArray[1]);
                        radSpinEditor9.Value = Convert.ToDecimal(row.ItemArray[2]);
                        radSpinEditor10.Value = Convert.ToDecimal(row.ItemArray[3]);
                        radSpinEditor11.Value = Convert.ToDecimal(row.ItemArray[4]);
                        radTextBox2.Text = row.ItemArray[5].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Cannot Load.  Board Serial Numbers do not Match");
                        return;
                    }*/
                }
                
               
            }
            else
            {
                return;
            }
        }

        private void radSpinEditor7_Scroll(object sender, ScrollEventArgs e)
        {
        }

        private void radButton5_Click(object sender, EventArgs e)  // time set
        {
            UInt16[] dt = new UInt16[6];
            DateTime curDT = radDateTimePicker1.Value;
            DateTime curTime = radDateTimePicker2.Value;
            //convert date to string

            //  string strYear =

            int res;  // used to verify results of writes

            if (radDropDownList1.Text == "")
            {
                MessageBox.Show("Select Comm Port");
                return;
            }
            mbusProtocol.timeout = 1600;
            mbusProtocol.retryCnt = 3;
            int result = openProtocol();

            if (result == 0)
            {
                return;
            }

            double y1 = (curDT.Year);

            y1 = y1 - 2000;

            y1 = y1 * 100;
            dt[0] = Convert.ToUInt16(curDT.Month * 100.00);
            dt[1] = Convert.ToUInt16(curDT.Day * 100.00);
            dt[2] = Convert.ToUInt16(y1);
            dt[3] = Convert.ToUInt16(curTime.Hour * 100.00);
            dt[4] = Convert.ToUInt16(curTime.Minute * 100.00);
            dt[5] = Convert.ToUInt16(curTime.Second * 100.00);
            res = mbusProtocol.writeMultipleRegisters(99, 3, dt);

            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error writing Current Offset - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            mbusProtocol.closeProtocol();
        }

        private void radButton10_Click(object sender, EventArgs e)
        {
            GetCommPorts();
        }

        private void radPageViewPage6_Paint(object sender, PaintEventArgs e)
        {
        }

        private void radTextBox8_TextChanged(object sender, EventArgs e)
        {
        }

        private void radButton13_Click(object sender, EventArgs e)
        {
            if (radTextBox8.Text == "7308dri153")
            {
                radButton11.Enabled = true;
                radButton12.Enabled = true;
            }
            else
            {
                MessageBox.Show("Wrong Password");
            }
        }

        private void radButton11_Click(object sender, EventArgs e)
        {
            int res;  // used to verify results of writes

            if (radDropDownList1.Text == "")
            {
                MessageBox.Show("Select Comm Port");
                return;
            }
            mbusProtocol.timeout = 1600;
            mbusProtocol.retryCnt = 3;
            int result = openProtocol();

            if (result == 0)
            {
                return;
            }

            //write current offsets

            //get grid data Grid 9

            int x = 0;
            Int16[] currOff = new Int16[15];
            foreach (DataRow row in coeff.Tables["CurOffsets"].Rows)
            {
                currOff[x] = Convert.ToInt16(row.ItemArray[1]);

                x++;
            }
            // send arrary to registers 202 - 216

            res = mbusProtocol.writeMultipleRegisters(99, 202, currOff);

            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error writing Current Offset - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // write resistnce coefficients

            Int16[] restCoeff = new Int16[21];
            x = 0;

            foreach (DataRow row in coeff.Tables["RTCal"].Rows)
            {
                restCoeff[x] = Convert.ToInt16(Convert.ToDouble(row.ItemArray[2]) * 10);
                restCoeff[x + 1] = Convert.ToInt16(Convert.ToDouble(row.ItemArray[3]) * 10);
                x = x + 2;
            }
            restCoeff[16] = Convert.ToInt16(radSpinEditor7.Value * 10);
            restCoeff[17] = Convert.ToInt16(radSpinEditor8.Value * 10);
            restCoeff[18] = Convert.ToInt16(radSpinEditor9.Value * 10);
            restCoeff[19] = Convert.ToInt16(radSpinEditor10.Value * 100000);
            restCoeff[20] = Convert.ToInt16(radSpinEditor11.Value * 10);

            res = mbusProtocol.writeMultipleRegisters(99, 221, restCoeff);

            if (res != BusProtocolErrors.FTALK_SUCCESS)
            {
                MessageBox.Show("Error writing Resistance Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            mbusProtocol.closeProtocol();
        }

        private void radButton14_Click(object sender, EventArgs e)
        {
            radDateTimePicker1.Value = DateTime.Now;
            radDateTimePicker2.Value = DateTime.Now;
        }

        private void radButton3_Click(object sender, EventArgs e)  // load seetings from DR sim
        {
            //Get Settings from the DRSim
            try
            {
                //open protocol
                int res;  // used to verify results of writes
                int x = 0;

                if (radDropDownList1.Text == "")
                {
                    MessageBox.Show("Select Comm Port");
                    return;
                }
                mbusProtocol.timeout = 1600;
                mbusProtocol.retryCnt = 3;
                int result = openProtocol();

                if (result == 0)
                {
                    MessageBox.Show("Unable to Connect to DR Sim");

                    return;
                }
                // get freq

                UInt16[] cv = new UInt16[1];

                res = mbusProtocol.readMultipleRegisters(99, 21, cv);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Frequency - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }
                radSpinEditor6.Value = cv[0] / 600;

                // get firmware version
                UInt16[] fwVer = new UInt16[1];
                res = mbusProtocol.readMultipleRegisters(99, 2, fwVer);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Firmware Version - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }

                radTextBox1.Text = (fwVer[0] / 100).ToString("##.##");
                
                // get board serial number
                UInt16[] boardSer = new UInt16[6];
                res = mbusProtocol.readMultipleRegisters(99, 13, boardSer);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Board Serial Version - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }
                radTextBox2.Text = (boardSer[0].ToString("X") + boardSer[1].ToString("X") + boardSer[2].ToString("X") + boardSer[3].ToString("X") + boardSer[4].ToString("X") + boardSer[5].ToString("X"));

                // get temps and humidity

                UInt16[] tempHumSettings = new UInt16[5];
                res = mbusProtocol.readMultipleRegisters(99, 30, tempHumSettings);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Temps and Humidity - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }

                double yg = (Convert.ToDouble(tempHumSettings[4]) - 826.00) / 31.483;

                radSpinEditor1.Value = Convert.ToDecimal(((Convert.ToDouble(tempHumSettings[0]) - 32768.00) / 100.00));
                radSpinEditor4.Value = Convert.ToDecimal(((Convert.ToDouble(tempHumSettings[1]) - 32768.00) / 100.00));
                radSpinEditor2.Value = Convert.ToDecimal(((Convert.ToDouble(tempHumSettings[2]) - 32768.00) / 100.00));
                radSpinEditor3.Value = Convert.ToDecimal(((Convert.ToDouble(tempHumSettings[3]) - 32768.00) / 100.00));
                radSpinEditor5.Value = Convert.ToDecimal(yg);

                // Get PD Settings
                UInt16[] PDSettings = new UInt16[22];
                res = mbusProtocol.readMultipleRegisters(99, 172, PDSettings);

                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading PD Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }

                radSpinEditor12.Value = PDSettings[0];
                radSpinEditor13.Value = PDSettings[1];
                radTrackBar1.Value = PDSettings[2];
                radTrackBar2.Value = PDSettings[3];
                radTrackBar3.Value = PDSettings[4];
                radTrackBar4.Value = PDSettings[5];

                // now fill PD grid  radgrid4
                x = 0;

                string strDecodePD;
               
                for (x = 0; x < 16; x++)
                {
                    // see if enabled
                    strDecodePD = Convert.ToString(PDSettings[x + 6], 2);
                    strDecodePD = strDecodePD.PadLeft(5, '0');
                    if (strDecodePD.Substring(0, 1) == "0")
                    {
                        radGridView4.Rows[x].Cells[1].Value = "False";
                    }
                    else
                    {
                        radGridView4.Rows[x].Cells[1].Value = "True";
                    }

                    // delay
                    if (strDecodePD.Substring(1, 1) == "0")
                    {
                        radGridView4.Rows[x].Cells[4].Value = "False";
                    }
                    else
                    {
                        radGridView4.Rows[x].Cells[4].Value = "True";
                    }

                    // polarity
                    if (strDecodePD.Substring(2, 1) == "0")
                    {
                        radGridView4.Rows[x].Cells[2].Value = "Positive";
                    }
                    else
                    {
                        radGridView4.Rows[x].Cells[2].Value = "Negative";
                    }

                    // voltage - bits 2 and 1
                    string strVolt = "";
                    strVolt = strDecodePD.Substring(3, 1) + strDecodePD.Substring(4, 1);

                    switch (strVolt)
                    {
                        case "00":
                            radGridView4.Rows[x].Cells[3].Value = 1000;
                            break;
                        case "01":
                            radGridView4.Rows[x].Cells[3].Value = 500;
                            break;
                        case "10":
                            radGridView4.Rows[x].Cells[3].Value = 250;
                            break;
                        case "11":
                            radGridView4.Rows[x].Cells[3].Value = 125;
                            break;
                    }
                }

                // Load currents and Advanced Currents

                UInt16[] AllCurrent = new UInt16[120];

                res = mbusProtocol.readMultipleRegisters(99, 52, AllCurrent);

                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Advanced Current and Load Current Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }
                x = 0;
                //advanced currents rad grid 3
                for (x = 0; x < 12; x++)
                {
                    radGridView3.Rows[x].Cells[3].Value = (Convert.ToDouble(AllCurrent[x]) / 100.00).ToString("F");  //harm 1 amp
                    radGridView3.Rows[x].Cells[4].Value = (Convert.ToDouble(AllCurrent[x + 15]) / 100.00).ToString("F");  //harm 1 PS

                    radGridView3.Rows[x].Cells[5].Value = (Convert.ToDouble(AllCurrent[x + 30]) / 100.00).ToString("F");  //harm 3 amp
                    radGridView3.Rows[x].Cells[6].Value = (Convert.ToDouble(AllCurrent[x + 45]) / 100.00).ToString("F");  //harm 3 PS

                    radGridView3.Rows[x].Cells[7].Value = (Convert.ToDouble(AllCurrent[x + 60]) / 100.00).ToString("F");  //harm 5 amp
                    radGridView3.Rows[x].Cells[8].Value = (Convert.ToDouble(AllCurrent[x + 75]) / 100.00).ToString("F");  //harm 5 PS

                    radGridView3.Rows[x].Cells[9].Value = (Convert.ToDouble(AllCurrent[x + 90]) / 100.00).ToString("F");  //harm 7 amp
                    radGridView3.Rows[x].Cells[10].Value = (Convert.ToDouble(AllCurrent[x + 105]) / 100.00).ToString("F");  //harm 7 PS
                }

                // do math for simple bushing screen and fill grid. 
                // calcs based on existing voltage, inital caps, inital pf and frequency

                // set 1
                double kV;
                
                double[] z = new double[3];
                double[] tc = new double[3];
                double[] tPF = new double[3];
                kV = Convert.ToDouble(radTextBox4.Text) * 1000000 / Math.Sqrt(3);
                x = 0;
                for (x = 0; x < 3; x++)
                {
                    // get ma current
                    // radGridView5.Rows[x].Cells[6].Value = (Convert.ToDouble(AllCurrent[x]) / 100.00).ToString("F");               
                    // figure out total capacitance
                    z[x] = kV / (Convert.ToDouble(AllCurrent[x]) / 100.00);
                    // calculate total caps
                    tc[x] = Math.Round(1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * z[x]) * Math.Pow(10, 12), 2);
                    // subtract inital cap
                    radGridView5.Rows[x].Cells[4].Value = Convert.ToString(Math.Round(tc[x] - Convert.ToDouble(radGridView5.Rows[x].Cells[2].Value), 0));

                    //  set 1 PF Calcs
                    switch (radGridView5.Rows[x].Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 15]) / 100.00) * Math.PI / 180) * 100; 
                            break;
                        case "B":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 15]) / 100.00 - 120) * Math.PI / 180) * 100;
                            break;
                        case "C":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 15]) / 100.00 - 240) * Math.PI / 180) * 100;
                            break;
                    }
                    radGridView5.Rows[x].Cells[5].Value = (Math.Round(tPF[x] - Convert.ToDouble(radGridView5.Rows[x].Cells[3].Value), 2)).ToString("F");
                }
                
                // set 2

                kV = Convert.ToDouble(radTextBox5.Text) * 1000000 / Math.Sqrt(3);
                x = 0;
                for (x = 0; x < 3; x++)
                {
                    // get ma current
                    // radGridView6.Rows[x].Cells[7].Value = (Convert.ToDouble(AllCurrent[x + 3]) / 100.00).ToString("F");
                    // figure out total capacitance
                    z[x] = kV / (Convert.ToDouble(AllCurrent[x + 3]) / 100.00);
                    // calculate total caps
                    tc[x] = Math.Round(1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * z[x]) * Math.Pow(10, 12), 2);
                    // subtract inital cap
                    radGridView6.Rows[x].Cells[4].Value = Convert.ToString(Math.Round(tc[x] - Convert.ToDouble(radGridView6.Rows[x].Cells[2].Value), 0));
                    //   radGridView6.Rows[x].Cells[4].Value = Convert.ToString(tc[x] - Convert.ToInt16(radGridView6.Rows[x].Cells[2].Value));

                    //  set 1 PF Calcs
                    switch (radGridView6.Rows[x].Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 18]) / 100.00) * Math.PI / 180) * 100;
                            break;
                        case "B":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 18]) / 100.00 - 120) * Math.PI / 180) * 100;
                            break;
                        case "C":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 18]) / 100.00 - 240) * Math.PI / 180) * 100;
                            break;
                    }
                    radGridView6.Rows[x].Cells[5].Value = (Math.Round(tPF[x] - Convert.ToDouble(radGridView6.Rows[x].Cells[3].Value), 2)).ToString("F");
                }
                // set 3

                kV = Convert.ToDouble(radTextBox6.Text) * 1000000 / Math.Sqrt(3);
                x = 0;
                for (x = 0; x < 3; x++)
                {
                    // get ma current
                    // radGridView7.Rows[x].Cells[7].Value = (Convert.ToDouble(AllCurrent[x + 6]) / 100.00).ToString("F");
                    // figure out total capacitance
                    z[x] = kV / (Convert.ToDouble(AllCurrent[x + 6]) / 100.00);
                    // calculate total caps
                    tc[x] = Math.Round(1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * z[x]) * Math.Pow(10, 12), 2);
                    // subtract inital cap
                    // radGridView7.Rows[x].Cells[4].Value = Convert.ToString(tc[x] - Convert.ToInt16(radGridView7.Rows[x].Cells[2].Value));
                    radGridView7.Rows[x].Cells[4].Value = Convert.ToString(Math.Round(tc[x] - Convert.ToDouble(radGridView7.Rows[x].Cells[2].Value), 0));
                    //  set 1 PF Calcs
                    switch (radGridView7.Rows[x].Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 21]) / 100.00) * Math.PI / 180) * 100;
                            break;
                        case "B":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 21]) / 100.00 - 120) * Math.PI / 180) * 100;
                            break;
                        case "C":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 21]) / 100.00 - 240) * Math.PI / 180) * 100;
                            break;
                    }
                    radGridView7.Rows[x].Cells[5].Value = (Math.Round(tPF[x] - Convert.ToDouble(radGridView7.Rows[x].Cells[3].Value), 2)).ToString("F");
                }
                // set 4

                kV = Convert.ToDouble(radTextBox7.Text) * 1000000 / Math.Sqrt(3);
                x = 0;
                for (x = 0; x < 3; x++)
                {
                    // get ma current
                    // radGridView8.Rows[x].Cells[7].Value = (Convert.ToDouble(AllCurrent[x + 9]) / 100.00).ToString("F");
                    // figure out total capacitance
                    z[x] = kV / (Convert.ToDouble(AllCurrent[x + 9]) / 100.00);
                    // calculate total caps
                    tc[x] = Math.Round(1 / (2.0 * Math.PI * Convert.ToDouble(radSpinEditor6.Value) * z[x]) * Math.Pow(10, 12), 0);
                    // subtract inital cap
                    //  radGridView8.Rows[x].Cells[4].Value = Convert.ToString(tc[x] - Convert.ToInt16(radGridView8.Rows[x].Cells[2].Value));
                    radGridView8.Rows[x].Cells[4].Value = Convert.ToString(Math.Round(tc[x] - Convert.ToDouble(radGridView8.Rows[x].Cells[2].Value), 2));
                    //  set 1 PF Calcs
                    switch (radGridView5.Rows[x].Cells["Phase"].Value.ToString())
                    {
                        case "A":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 24]) / 100.00) * Math.PI / 180) * 100;
                            break;
                        case "B":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 24]) / 100.00 - 120) * Math.PI / 180) * 100;
                            break;
                        case "C":
                            tPF[x] = Math.Tan((Convert.ToDouble(AllCurrent[x + 24]) / 100.00 - 240) * Math.PI / 180) * 100;
                            break;
                    }
                    radGridView8.Rows[x].Cells[5].Value = (Math.Round(tPF[x] - Convert.ToDouble(radGridView8.Rows[x].Cells[3].Value), 2)).ToString("F");
                }

                CalculateBCurrentValues(); // update bc grids
                // load currents  radgrid2
                Application.DoEvents();
                x = 0;
                double sen;
                double pri;
                for (x = 0; x < 3; x++)
                {
                    sen = Convert.ToDouble(radGridView1.Rows[x].Cells[5].Value);
                    pri = Convert.ToDouble(radGridView2.Rows[x].Cells[1].Value);
                    radGridView2.Rows[x].Cells[2].Value = (Convert.ToDouble(AllCurrent[x + 12]) / 100.00 * 2333 * sen / pri).ToString("F");  //harm 1 amp
                    radGridView2.Rows[x].Cells[3].Value = (Convert.ToDouble(AllCurrent[x + 27]) / 100.00).ToString("F");  //harm 1 PS

                    radGridView2.Rows[x].Cells[4].Value = (Convert.ToDouble(AllCurrent[x + 42]) / 100.00).ToString("F");  //harm 3 amp
                    radGridView2.Rows[x].Cells[5].Value = (Convert.ToDouble(AllCurrent[x + 57]) / 100.00).ToString("F");  //harm 3 PS

                    radGridView2.Rows[x].Cells[6].Value = (Convert.ToDouble(AllCurrent[x + 72]) / 100.00).ToString("F");  //harm 5 amp
                    radGridView2.Rows[x].Cells[7].Value = (Convert.ToDouble(AllCurrent[x + 87]) / 100.00).ToString("F");  //harm 5 PS

                    radGridView2.Rows[x].Cells[8].Value = (Convert.ToDouble(AllCurrent[x + 102]) / 100.00).ToString("F");  //harm 7 amp
                    radGridView2.Rows[x].Cells[9].Value = (Convert.ToDouble(AllCurrent[x + 117]) / 100.00).ToString("F");  //harm 7 PS
                }

                //  now do waveforms for all current channels
                // first advanced channels  radgrid 3
                // first read registers 23, 24 1nd 25
                UInt16[] waves = new UInt16[3];

                res = mbusProtocol.readMultipleRegisters(99, 23, waves);

                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Waveform Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }

                string strReg23 = Convert.ToString(waves[0], 2);
                string strReg24 = Convert.ToString(waves[1], 2);
                string strReg25 = Convert.ToString(waves[2], 2);

                // now figure out conversion to text
                // get lenght of string and pad 0's to the left side until string length = 15

                strReg23 = strReg23.PadLeft(15, '0');
                strReg24 = strReg24.PadLeft(15, '0');
                strReg25 = strReg25.PadLeft(15, '0');

                //now figure out what type of wave
                //read each string from right to left to determine code
                //for bushing currents
                x = 0;
                string strDecode = "";
                for (x = 0; x < 12; x++)
                {
                    strDecode = strReg23.Substring(14 - x, 1) + strReg24.Substring(14 - x, 1) + strReg25.Substring(14 - x, 1);

                    switch (strDecode)
                    {
                        case "000":
                            radGridView3.Rows[x].Cells[11].Value = "Sine";
                            break;
                        case "001":
                            radGridView3.Rows[x].Cells[11].Value = "Square";
                            break;
                        case "011":
                            radGridView3.Rows[x].Cells[11].Value = "Sawtooth";
                            break;
                        case "100":
                            radGridView3.Rows[x].Cells[11].Value = "User Defined";
                            break;
                    }
                }

                //now do Load current
                x = 0;
                
                for (x = 0; x < 3; x++)
                {
                    strDecode = strReg23.Substring(2 - x, 1) + strReg24.Substring(2 - x, 1) + strReg25.Substring(2 - x, 1);

                    switch (strDecode)
                    {
                        case "000":
                            radGridView2.Rows[x].Cells[10].Value = "Sine";
                            break;
                        case "001":
                            radGridView2.Rows[x].Cells[10].Value = "Square";
                            break;
                        case "011":
                            radGridView2.Rows[x].Cells[10].Value = "Sawtooth";
                            break;
                        case "100":
                            radGridView2.Rows[x].Cells[10].Value = "User Defined";
                            break;
                    }
                }
                //get advanced current impedance registers and put into column

                UInt16[] bcZ = new UInt16[12];
                res = mbusProtocol.readMultipleRegisters(99, 245, bcZ);

                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading impedance Settings - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mbusProtocol.closeProtocol();
                    return;
                }

                // add to grid
                for (x = 0; x < 12; x++)
                {
                    radGridView3.Rows[x].Cells[2].Value = (Convert.ToDouble(bcZ[x]) / 100).ToString("F");  //Impedance
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error In Getting Data from the DRSim - " + ex.Message);
                mbusProtocol.closeProtocol();
                return;
            }
            mbusProtocol.closeProtocol();
        }

        private void radButton12_Click(object sender, EventArgs e)
        {
            //Get Coefficients from the DRSim
            try
            {
                //open protocol
                int res;  // used to verify results of writes
                int x = 0;

                if (radDropDownList1.Text == "")
                {
                    MessageBox.Show("Select Comm Port");
                    return;
                }
                mbusProtocol.timeout = 1600;
                mbusProtocol.retryCnt = 3;
                int result = openProtocol();

                // clear datatables

                // coeff.Tables["RTCal"].Clear();
                //  coeff.Tables["CurOffsets"].Clear();
                //  coeff.Tables["CalMisc"].Clear();

                if (result == 0)
                {
                    return;
                }

                Int16[] currOff = new Int16[15];

                res = mbusProtocol.readMultipleRegisters(99, 202, currOff);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Current Offset - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // convert each int to string
                string[] strCurOff = new string[15];

                x = 0;
                for (x = 0; x < 15; x++)
                {
                    strCurOff[x] = Convert.ToString(currOff[x]);
                    radGridView9.Rows[x].Cells[1].Value = strCurOff[x];                
                }

                // do resistance grid

                Int16[] restCoeff = new Int16[21];

                res = mbusProtocol.readMultipleRegisters(99, 221, restCoeff);
                if (res != BusProtocolErrors.FTALK_SUCCESS)
                {
                    MessageBox.Show("Error Reading Resistance Coefficients - " + BusProtocolErrors.getBusProtocolErrorText(res), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int y = 0;
                for (x = 0; x < 8; x++)
                {
                    strCurOff[x] = Convert.ToString(currOff[x]);
                    double rt, rw;

                    rt = Convert.ToDouble(restCoeff[y]) / 10.0;
                    rw = Convert.ToDouble(restCoeff[y + 1]) / 10.0;
                    radGridView10.Rows[x].Cells[2].Value = rt.ToString("F");
                    radGridView10.Rows[x].Cells[3].Value = rw.ToString("F");
                    y = y + 2;
                }

                // do the balance

                radSpinEditor7.Value = restCoeff[16] / 10;
                radSpinEditor8.Value = restCoeff[17] / 10;
                radSpinEditor9.Value = restCoeff[18] / 10;
                double yu = Convert.ToDouble(restCoeff[19]) / 100000;
                radSpinEditor10.Value = Convert.ToDecimal(yu);
                radSpinEditor11.Value = restCoeff[20] / 10;
            }
            catch
            {
                MessageBox.Show("Error in getting Data from the DRSim ");
            }
            mbusProtocol.closeProtocol();
        }

        private void radGridView1_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.CellElement.ColumnInfo.Name == "column5")
            {
                e.CellElement.ForeColor = Color.Blue;
            }
            if (e.CellElement.ColumnInfo.Name == "column4")
            {
                e.CellElement.ForeColor = Color.Blue;
            }
        }

        private void radButton15_Click(object sender, EventArgs e)
        { 
            while (this.radGridView3.Rows.Count > 0)  // clear grid
            {
                this.radGridView3.Rows.RemoveAt(this.radGridView3.Rows.Count - 1);
            }

            InitializeAdvBushingCurrentScreen();
        }

        private void radGridView1_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            CalculateLCSensitivity();
        }

        private void radButton16_Click(object sender, EventArgs e)
        {
            int result = openProtocol();
           
            if (result == 0)
            {
                return;
            }
          
            int res = mbusProtocol.writeSingleRegister(99, 22, 2);

            mbusProtocol.closeProtocol();
        }

        private void radPageView1_SelectedPageChanged(object sender, EventArgs e)
        {

        }
    }
}