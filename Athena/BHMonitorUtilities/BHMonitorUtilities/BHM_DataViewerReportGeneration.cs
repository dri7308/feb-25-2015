﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.RichTextBox;
using Telerik.WinControls.RichTextBox.Model;
using Telerik.WinControls.RichTextBox.FileFormats.Rtf; 

using ChartDirector;

using GeneralUtilities;
using ConfigurationObjects;
using DataObjects;
using PasswordManagement;
using FormatConversion;
using DatabaseInterface;
using DatabaseFileInteraction;
using Dynamics;
using MonitorUtilities;
using StatisticsDisplay;

namespace BHMonitorUtilities
{
    public partial class BHM_DataViewer : Telerik.WinControls.UI.RadForm
    {
        private static List<string> paragraphTextList = null;

        private int chartWidth = 620;
        private int chartHeight = 250;

        private int chartStartX = 60;
        private int chartStartY = 50;

        private int widthAdjustment = 110;
        private int heightAdjustment = 90;

        private int legendX = 50;
        private int legendY=25;

        private Bitmap CreateGammaAndTopOilTempGraph(double[] gamma, double[] topOilTemperature, DateTime[] gammaDateTime, DateTime[] topOilTempDateTime)
        {
            XYChart chart = new XYChart(this.chartWidth, this.chartHeight);

            int numberOfDataPoints = topOilTempDateTime.Length;

            int graphWidth = this.chartWidth - this.widthAdjustment;
            int graphHeight = this.chartHeight - this.heightAdjustment;
                 
            int gammaColor = Chart.CColor(Color.Red);
            int topOilTempColor = Chart.CColor(Color.Blue);

            chart.setPlotArea(chartStartX, chartStartY, graphWidth, graphHeight);

            // chart.xAxis().setTitle("

            chart.yAxis().setTitle("Gamma");
            //chart.yAxis().setColors(gammaColor, gammaColor, gammaColor);

            chart.yAxis2().setTitle("Top Oil Temperature");
            //chart.yAxis().setColors(topOilTempColor, topOilTempColor,topOilTempColor);

            /// set up the x-axis scale

            chart.xAxis().setDateScale(topOilTempDateTime[0], topOilTempDateTime[numberOfDataPoints - 1]);

            //
            // In the current demo, the x-axis range can be from a few years to a few days. We can 
            // let ChartDirector auto-determine the date/time format. However, for more beautiful 
            // formatting, we set up several label formats to be applied at different conditions. 
            //

            // If all ticks are yearly aligned, then we use "yyyy" as the label format.
            chart.xAxis().setFormatCondition("align", 360 * 86400);
            chart.xAxis().setLabelFormat("{value|yyyy}");

            // If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
            // label of a year, and "mmm" for other labels.
            chart.xAxis().setFormatCondition("align", 30 * 86400);
            chart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                Chart.AllPassFilter(), "{value|mmm}");

            // If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
            // first label of a year, and "mmm dd" in bold font as the first label of a month, and
            // "dd" for other labels.
            chart.xAxis().setFormatCondition("align", 86400);
            chart.xAxis().setMultiFormat(
                Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
            chart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

            // For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
            // a day, and "hh:nn" for other labels.
            chart.xAxis().setFormatCondition("else");
            chart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                Chart.AllPassFilter(), "{value|hh:nn}");

            chart.addLegend(legendX, legendY, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

            LineLayer gammaLineLayer = chart.addLineLayer2();
            gammaLineLayer.setLineWidth(2);
            gammaLineLayer.addDataSet(gamma, gammaColor, gammaLegendText);
            gammaLineLayer.setXData(gammaDateTime);

            LineLayer topOilTempLineLayer = chart.addLineLayer2();
            topOilTempLineLayer.addDataSet(topOilTemperature, topOilTempColor, "Top Oil Temperature");
            topOilTempLineLayer.setUseYAxis2();
            topOilTempLineLayer.setXData(topOilTempDateTime);

            Bitmap image = new Bitmap(chart.makeImage(), chartWidth, chartHeight);

            // image.Save(outputFileNameWithFullPath, System.Drawing.Imaging.ImageFormat.Bmp);

            return image;
        }


        private Bitmap CreateTangentDeltaGraph(double[] tangentA, double[] tangentB, double[] tangentC, DateTime[] dateTimes)
        {
            XYChart chart = new XYChart(this.chartWidth, this.chartHeight);

            int numberOfDataPoints = dateTimes.Length;

            int graphWidth = this.chartWidth - widthAdjustment;
            int graphHeight = this.chartHeight - heightAdjustment;
           
            int tangentAColor = Chart.CColor(Color.Red);
            int tangentBColor = Chart.CColor(Color.Blue);
            int tangentCColor = Chart.CColor(Color.Yellow);

            chart.setPlotArea(chartStartX, chartStartY, graphWidth, graphHeight);

            // chart.xAxis().setTitle("

            chart.yAxis().setTitle("Tangent Delta");

            /// set up the x-axis scale

            chart.xAxis().setDateScale(dateTimes[0], dateTimes[numberOfDataPoints - 1]);

            //
            // In the current demo, the x-axis range can be from a few years to a few days. We can 
            // let ChartDirector auto-determine the date/time format. However, for more beautiful 
            // formatting, we set up several label formats to be applied at different conditions. 
            //

            // If all ticks are yearly aligned, then we use "yyyy" as the label format.
            chart.xAxis().setFormatCondition("align", 360 * 86400);
            chart.xAxis().setLabelFormat("{value|yyyy}");

            // If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
            // label of a year, and "mmm" for other labels.
            chart.xAxis().setFormatCondition("align", 30 * 86400);
            chart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                Chart.AllPassFilter(), "{value|mmm}");

            // If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
            // first label of a year, and "mmm dd" in bold font as the first label of a month, and
            // "dd" for other labels.
            chart.xAxis().setFormatCondition("align", 86400);
            chart.xAxis().setMultiFormat(
                Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
            chart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

            // For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
            // a day, and "hh:nn" for other labels.
            chart.xAxis().setFormatCondition("else");
            chart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                Chart.AllPassFilter(), "{value|hh:nn}");

            chart.addLegend(legendX, legendY, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);
       
            LineLayer tangentALineLayer = chart.addLineLayer2();
            tangentALineLayer.setLineWidth(2);
            tangentALineLayer.addDataSet(tangentA, tangentAColor, tangent1LegendText);
            tangentALineLayer.setXData(dateTimes);

            LineLayer tangentBLineLayer = chart.addLineLayer2();
            tangentBLineLayer.setLineWidth(2);
            tangentBLineLayer.addDataSet(tangentB, tangentBColor, tangent2LegendText);
            tangentBLineLayer.setXData(dateTimes);

            LineLayer tangentCLineLayer = chart.addLineLayer2();
            tangentCLineLayer.setLineWidth(2);
            tangentCLineLayer.addDataSet(tangentC, tangentCColor, tangent3LegendText);
            tangentCLineLayer.setXData(dateTimes);

            Bitmap image = new Bitmap(chart.makeImage(), chartWidth, chartHeight);

            return image;
        }


        private Bitmap CreateCapacitanceGraph(double[] capacitancePhaseA, double[] capacitancePhaseB, double[] capacitancePhaseC, DateTime[] dateTimes)
        {
            XYChart chart = new XYChart(this.chartWidth, this.chartHeight);

            int numberOfDataPoints = dateTimes.Length;

            int graphWidth = this.chartWidth - widthAdjustment;
            int graphHeight = this.chartHeight - heightAdjustment;
           
            int capacitancePhaseAColor = Chart.CColor(Color.Red);
            int capacitancePhaseBColor = Chart.CColor(Color.Blue);
            int capacitancePhaseCColor = Chart.CColor(Color.Yellow);

            chart.setPlotArea(chartStartX, chartStartY, graphWidth, graphHeight);

            // chart.xAxis().setTitle("

            chart.yAxis().setTitle("Capacitance");

            /// set up the x-axis scale

            chart.xAxis().setDateScale(dateTimes[0], dateTimes[numberOfDataPoints - 1]);

            //
            // In the current demo, the x-axis range can be from a few years to a few days. We can 
            // let ChartDirector auto-determine the date/time format. However, for more beautiful 
            // formatting, we set up several label formats to be applied at different conditions. 
            //

            // If all ticks are yearly aligned, then we use "yyyy" as the label format.
            chart.xAxis().setFormatCondition("align", 360 * 86400);
            chart.xAxis().setLabelFormat("{value|yyyy}");

            // If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
            // label of a year, and "mmm" for other labels.
            chart.xAxis().setFormatCondition("align", 30 * 86400);
            chart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                Chart.AllPassFilter(), "{value|mmm}");

            // If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
            // first label of a year, and "mmm dd" in bold font as the first label of a month, and
            // "dd" for other labels.
            chart.xAxis().setFormatCondition("align", 86400);
            chart.xAxis().setMultiFormat(
                Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
            chart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

            // For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
            // a day, and "hh:nn" for other labels.
            chart.xAxis().setFormatCondition("else");
            chart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                Chart.AllPassFilter(), "{value|hh:nn}");

            chart.addLegend(legendX, legendY, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

            LineLayer capacitanceALineLayer = chart.addLineLayer2();
            capacitanceALineLayer.setLineWidth(2);
            capacitanceALineLayer.addDataSet(capacitancePhaseA, capacitancePhaseAColor, capacitance1LegendText);
            capacitanceALineLayer.setXData(dateTimes);

            LineLayer capacitanceBLineLayer = chart.addLineLayer2();
            capacitanceBLineLayer.setLineWidth(2);
            capacitanceBLineLayer.addDataSet(capacitancePhaseB, capacitancePhaseBColor, capacitance2LegendText);
            capacitanceBLineLayer.setXData(dateTimes);

            LineLayer capacitanceCLineLayer = chart.addLineLayer2();
            capacitanceCLineLayer.setLineWidth(2);
            capacitanceCLineLayer.addDataSet(capacitancePhaseC, capacitancePhaseCColor, capacitance3LegendText);
            capacitanceCLineLayer.setXData(dateTimes);

            Bitmap image = new Bitmap(chart.makeImage(), chartWidth, chartHeight);

            return image;
        }

        private bool InitializeParagraphTextList()
        {
            bool success = false;
            try
            {
                StreamReader reader;
                List<string> paragraphSourceFileNameList;

                paragraphSourceFileNameList = new List<string>();

                if (BHM_DataViewer.paragraphTextList == null)
                {
                    BHM_DataViewer.paragraphTextList = new List<string>();

                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "bushing_report_paragraph1.txt"));
                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "bushing_report_paragraph2.txt"));
                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "bushing_report_paragraph3.txt"));
                    paragraphTextList = new List<string>();
                   
                    foreach (string fileName in paragraphSourceFileNameList)
                    {
                        if (File.Exists(fileName))
                        {
                            using (reader = new StreamReader(fileName))
                            {
                                if (reader != null)
                                {
                                    paragraphTextList.Add(reader.ReadToEnd());
                                }
                            }
                        }
                    }
                }
                if (BHM_DataViewer.paragraphTextList.Count == 3)
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.InitializeParagraphTextList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        private void CreateReport()
        {
            try
            {
                RtfFormatProvider provider = new RtfFormatProvider();

                RadDocument document = new RadDocument();
                Section section;

                string saveFileName = string.Empty;

                //Table table;
                //TableRow tableRow;
                //TableCell tableCell;

                Paragraph paragraph;
                Span span;
                List<Paragraph> paragraphList = new List<Paragraph>();

                List<string> companyPlantAndEquipmentNames;

                Bitmap logoImage = null;

                Bitmap gammaAndTopOilTempSet1Graph = null;
                Bitmap gammaAndTopOilTempSet2Graph = null;
                Bitmap tangentSet1Graph = null;
                Bitmap tangentSet2Graph = null;
                Bitmap capacitanceSet1Graph = null;
                Bitmap capacitanceSet2Graph = null;

                double[] gammaSet1 = null;
                double[] gammaSet2 = null;

                double[] topOilTemp = null;
                //double[] topOilTempSet1 = null;
                //double[] topOilTempSet2 = null;

                double[] tangentASet1 = null;
                double[] tangentBSet1 = null;
                double[] tangentCSet1 = null;

                double[] tangentASet2 = null;
                double[] tangentBSet2 = null;
                double[] tangentCSet2 = null;

                double[] capacitanceASet1 = null;
                double[] capacitanceBSet1 = null;
                double[] capacitanceCSet1 = null;

                double[] capacitanceASet2 = null;
                double[] capacitanceBSet2 = null;
                double[] capacitanceCSet2 = null;

                // RadMessageBox.Show(this, "At some point this will create a report.  Not right now, though.");

                // RadRichTextBox reportRadRichTextBox = new RadRichTextBox();

                string gammaAndTopOilTempSet1GraphFileName = Path.Combine(this.applicationDataPath, "GammaAndTopOilTempSet1ReportGraph.bmp");
                string gammaAndTopOilTempSet2GraphFileName = Path.Combine(this.applicationDataPath, "GammaAndTopOilTempSet2ReportGraph.bmp");

                // reportRadRichTextBox.Document.Insert(

                DateTime viewPortStartDate = minimumDateOfDataSet.AddSeconds(Math.Round(this.trendWinChartViewer.ViewPortLeft * this.dateRange));
                DateTime viewPortEndDate = viewPortStartDate.AddSeconds(Math.Round(this.trendWinChartViewer.ViewPortWidth * this.dateRange));

                // Get the starting index of the array using the start date
                int commonDataStartIndex = FindGraphStartDateIndex(viewPortStartDate, this.commonData.dataReadingDate);
                int group1StartIndex = 0;
                int group2StartIndex = 0;

                // Get the ending index of the array using the end date
                int commonDataEndIndex = FindGraphEndDateIndex(viewPortEndDate, this.commonData.dataReadingDate);
                int group1EndIndex = 0;
                int group2EndIndex = 0;

                // Get the length
                int numberOfCommonDataPointsBeingGraphed = commonDataEndIndex - commonDataStartIndex + 1;
                int numberOfGroup1DataPointsBeingGraphed = 0;
                int numberOfGroup2DataPointsBeingGraphed = 0;

                // Now, we can just copy the visible data we need into the view port data series
                // DateTime[] viewPortTimeStamps = new DateTime[numberOfPointsBeingGraphed];
                DateTime[] commonDataTimeStamps = new DateTime[numberOfCommonDataPointsBeingGraphed];
                DateTime[] group1TimeStamps = null;
                DateTime[] group2TimeStamps = null;

                Array.Copy(this.commonData.dataReadingDate, commonDataStartIndex, commonDataTimeStamps, 0, numberOfCommonDataPointsBeingGraphed);
                topOilTemp = new double[numberOfCommonDataPointsBeingGraphed];
                Array.Copy(this.commonData.temp1, commonDataStartIndex, topOilTemp, 0, numberOfCommonDataPointsBeingGraphed);

                if (InitializeParagraphTextList())
                {
                    if (this.totalReadingsGroup1 > 0)
                    {
                        group1StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup1.dataReadingDate);
                        group1EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup1.dataReadingDate);
                        numberOfGroup1DataPointsBeingGraphed = group1EndIndex - group1StartIndex + 1;
                        group1TimeStamps = new DateTime[numberOfGroup1DataPointsBeingGraphed];

                        Array.Copy(this.dbDataGroup1.dataReadingDate, group1StartIndex, group1TimeStamps, 0, numberOfGroup1DataPointsBeingGraphed);

                        gammaSet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        tangentASet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        tangentBSet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        tangentCSet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        capacitanceASet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        capacitanceBSet1 = new double[numberOfGroup1DataPointsBeingGraphed];
                        capacitanceCSet1 = new double[numberOfGroup1DataPointsBeingGraphed];

                        Array.Copy(this.dbDataGroup1.gamma, group1StartIndex, gammaSet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.tangentA, group1StartIndex, tangentASet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.tangentB, group1StartIndex, tangentBSet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.tangentC, group1StartIndex, tangentCSet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.capacitanceA, group1StartIndex, capacitanceASet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.capacitanceB, group1StartIndex, capacitanceBSet1, 0, numberOfGroup1DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup1.capacitanceC, group1StartIndex, capacitanceCSet1, 0, numberOfGroup1DataPointsBeingGraphed);
                    }
                    if (this.totalReadingsGroup2 > 0)
                    {
                        group2StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup2.dataReadingDate);
                        group2EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup2.dataReadingDate);
                        numberOfGroup2DataPointsBeingGraphed = group2EndIndex - group2StartIndex + 1;
                        group2TimeStamps = new DateTime[numberOfGroup2DataPointsBeingGraphed];

                        Array.Copy(this.dbDataGroup2.dataReadingDate, group2StartIndex, group2TimeStamps, 0, numberOfGroup2DataPointsBeingGraphed);

                        gammaSet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        tangentASet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        tangentBSet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        tangentCSet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        capacitanceASet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        capacitanceBSet2 = new double[numberOfGroup2DataPointsBeingGraphed];
                        capacitanceCSet2 = new double[numberOfGroup2DataPointsBeingGraphed];

                        Array.Copy(this.dbDataGroup2.gamma, group2StartIndex, gammaSet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.tangentA, group2StartIndex, tangentASet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.tangentB, group2StartIndex, tangentBSet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.tangentC, group2StartIndex, tangentCSet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.capacitanceA, group2StartIndex, capacitanceASet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.capacitanceB, group2StartIndex, capacitanceBSet2, 0, numberOfGroup2DataPointsBeingGraphed);
                        Array.Copy(this.dbDataGroup2.capacitanceC, group2StartIndex, capacitanceCSet2, 0, numberOfGroup2DataPointsBeingGraphed);
                    }

                    //Set up the header for the report

                    // DR Logo
                    section = new Section();
                    logoImage = (Bitmap)Image.FromFile(Path.Combine(this.applicationDataPath, "DR_Logo_DocumentSize.bmp"));
                    paragraph = new Paragraph();
                    paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Right;
                    paragraph.Inlines.Add(new ImageInline(logoImage));
                    section.Blocks.Add(paragraph);
                    document.Sections.Add(section);
                    //paragraphList.Add(paragraph);

                    // Title
                    section = new Section();
                    paragraph = new Paragraph();
                    span = new Span("Bushing Report");
                    span.FontSize = 20;
                    span.FontStyle = TextStyle.Bold;
                    paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                    paragraph.Inlines.Add(span);
                    section.Blocks.Add(paragraph);
                    document.Sections.Add(section);
                    // paragraphList.Add(paragraph);

                    //// Company Plant Equipment info
                    companyPlantAndEquipmentNames = General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy(this.monitorHierarchy);

                    if (companyPlantAndEquipmentNames.Count == 3)
                    {
                        //section = new Section();
                        //table = new Table();
                        //table.LayoutMode = TableLayoutMode.AutoFit;

                        //tableRow = new TableRow();

                        //tableCell = new TableCell();
                        //paragraph = new Paragraph();
                        //span = new Span("Company: " + companyPlantAndEquipmentNames[0]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Left;
                        //paragraph.Inlines.Add(span);
                        //tableCell.Blocks.Add(paragraph);
                        //tableRow.Cells.Add(tableCell);

                        //TableCell tableCell2 = new TableCell();
                        //paragraph = new Paragraph();
                        //span = new Span("Plant: " + companyPlantAndEquipmentNames[1]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                        //paragraph.Inlines.Add(span);
                        //tableCell2.Blocks.Add(paragraph);
                        //tableRow.Cells.Add(tableCell2);

                        //TableCell tableCell3 = new TableCell();
                        //paragraph = new Paragraph();
                        //span = new Span("Equipment: " + companyPlantAndEquipmentNames[2]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Right;
                        //paragraph.Inlines.Add(span);
                        //tableCell3.Blocks.Add(paragraph);
                        //tableRow.Cells.Add(tableCell3);

                        //table.Rows.Add(tableRow);
                        //section.Blocks.Add(table);

                        //document.Sections.Add(section);


                        //paragraph = new Paragraph();
                        //span = new Span("Company: " + companyPlantAndEquipmentNames[0]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Left;
                        //paragraph.Inlines.Add(span);

                        //span = new Span("Plant: " + companyPlantAndEquipmentNames[1]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                        //paragraph.Inlines.Add(span);

                        //span = new Span("Equipment: " + companyPlantAndEquipmentNames[2]);
                        //paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Right;
                        //paragraph.Inlines.Add(span);

                        paragraph = new Paragraph();
                        span = new Span("Company: " + companyPlantAndEquipmentNames[0] + "\t Plant: " + companyPlantAndEquipmentNames[1] + "\t Equipment: " + companyPlantAndEquipmentNames[2]);
                        span.FontSize = 16;
                        // span.FontStyle = TextStyle.Bold;
                        paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                        paragraph.Inlines.Add(span);

                        section = new Section();
                        section.Blocks.Add(paragraph);
                        document.Sections.Add(section);

                        //paragraphList.Add(paragraph);
                    }

                    section = new Section();

                    if (this.totalReadingsGroup1 > 0)
                    {
                        paragraph = CreateSectionHeading("Set 1 Data for " + group1TimeStamps[0].ToString() + " to " + group1TimeStamps[group1TimeStamps.Length-1].ToString());
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        /// some paragraph of text
                        paragraph = CreateParagraph(BHM_DataViewer.paragraphTextList[0]);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        /// Create the report information for Set 1
                        paragraph = CreateInlinedGammaAndTopOilTempGraph(gammaSet1, topOilTemp, group1TimeStamps, commonDataTimeStamps);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        /// some paragraph of text
                        paragraph = CreateParagraph(BHM_DataViewer.paragraphTextList[1]);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        paragraph = CreateInlinedTangentDeltaGraph(tangentASet1, tangentBSet1, tangentCSet1, group1TimeStamps);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        paragraph = CreateParagraph(BHM_DataViewer.paragraphTextList[2]);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        paragraph = CreateInlinedCapacitanceGraph(capacitanceASet1, capacitanceBSet1, capacitanceCSet1, group1TimeStamps);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }
                    }
                    if (this.totalReadingsGroup2 > 0)
                    {
                        paragraph = CreateSectionHeading("Set 2 Data for " + group2TimeStamps[0].ToString() + " to " + group2TimeStamps[group2TimeStamps.Length - 1].ToString());
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }

                        /// Create the report information for Set 2
                        gammaAndTopOilTempSet2Graph = CreateGammaAndTopOilTempGraph(gammaSet2, topOilTemp, group2TimeStamps, commonDataTimeStamps);
                        if (gammaAndTopOilTempSet2Graph != null)
                        {
                            paragraph = new Paragraph();
                            paragraph.Inlines.Add(new ImageInline(gammaAndTopOilTempSet2Graph));
                            paragraphList.Add(paragraph);
                        }
                        tangentSet2Graph = CreateTangentDeltaGraph(tangentASet2, tangentBSet2, tangentCSet2, group2TimeStamps);
                        if (tangentSet2Graph != null)
                        {
                            paragraph = new Paragraph();
                            paragraph.Inlines.Add(new ImageInline(tangentSet2Graph));
                            paragraphList.Add(paragraph);
                        }
                        capacitanceSet2Graph = CreateCapacitanceGraph(capacitanceASet2, capacitanceBSet2, capacitanceCSet2, group2TimeStamps);
                        if (capacitanceSet2Graph != null)
                        {
                            paragraph = new Paragraph();
                            paragraph.Inlines.Add(new ImageInline(capacitanceSet2Graph));
                            paragraphList.Add(paragraph);
                        }
                    }

                    paragraph = new Paragraph();
                    span = new Span("This is the final paragraph.  It is really short.");
                    paragraph.Inlines.Add(span);
                    paragraphList.Add(paragraph);

                    foreach (Paragraph entry in paragraphList)
                    {
                        section.Blocks.Add(entry);
                    }

                    document.Sections.Add(section);

                    saveFileName = "Bushing_Report_" + FileUtilities.GetCurrentDateTimeAsPartialFileNameString() + ".rtf";

                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.DefaultExt = ".rtf";
                    saveDialog.Filter = "Documents|*.rtf";
                    saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    saveDialog.FileName = saveFileName;
                    DialogResult dialogResult = saveDialog.ShowDialog();
                    using (Stream output = saveDialog.OpenFile())
                    {
                        provider.Export(document, output);
                    }

                    //System.Diagnostics.Process p = new System.Diagnostics.Process();
                    //p.StartInfo.FileName = "write.exe";
                    //p.StartInfo.Arguments = saveDialog.FileName;
                    //p.Start();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateReport()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Paragraph CreateInlinedGammaAndTopOilTempGraph(double[] gamma, double[] topOilTemp, DateTime[] gammaTimeStamps, DateTime[] dynamicsTimeStamps)
        {
            Paragraph paragraph = null;
            try
            {
                Bitmap image;

                image = CreateGammaAndTopOilTempGraph(gamma, topOilTemp, gammaTimeStamps, dynamicsTimeStamps);
                if (image != null)
                {
                    paragraph = new Paragraph();
                    paragraph.Inlines.Add(new ImageInline(image));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateInlinedGammaAndTopOilTempGraph(double[], double[], DateTime[], DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private Paragraph CreateInlinedTangentDeltaGraph(double[] tangentA, double[] tangentB, double[] tangentC, DateTime[] tangentTimeStamps)
        {
            Paragraph paragraph = null;
            try
            {
                Bitmap image;

                image = CreateTangentDeltaGraph(tangentA, tangentB, tangentC, tangentTimeStamps);
                if (image != null)
                {
                    paragraph = new Paragraph();
                    paragraph.Inlines.Add(new ImageInline(image));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateInlinedTangentDeltaGraph(double[], double[], double[], DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private Paragraph CreateInlinedCapacitanceGraph(double[] capacitanceA, double[] capacitanceB, double[] capacitanceC, DateTime[] capacitanceTimeStamps)
        {
            Paragraph paragraph = null;
            try
            {
                Bitmap image;

                image = CreateCapacitanceGraph(capacitanceA, capacitanceB, capacitanceC, capacitanceTimeStamps);
                if (image != null)
                {
                    paragraph = new Paragraph();
                    paragraph.Inlines.Add(new ImageInline(image));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateInlinedCapacitanceGraph(double[], double[], double[], DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private Paragraph CreateSectionHeading(string heading)
        {
            Paragraph paragraph = null;
            try
            {
                Span span;
                if ((heading != null) && (heading.Length > 0))
                {  
                   paragraph = new Paragraph();
                   span = new Span(heading);
                   span.FontSize = 14;
                   paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Left;
                   paragraph.Inlines.Add(span);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateSectionHeading(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private Paragraph CreateParagraph(string paragraphContent)
        {
            Paragraph paragraph = null;
            try
            {
                Span span;
                if ((paragraphContent != null) && (paragraphContent.Length > 0))
                {
                    paragraph = new Paragraph();
                    span = new Span(paragraphContent);
                    paragraph.Inlines.Add(span);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewerReportGeneration.CreateParagraph(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

    }
}
