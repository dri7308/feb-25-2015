﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;
using DatabaseInterface;
using FormatConversion;

namespace BHMonitorUtilities
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private void WriteDataToSecondBushingSetTabObjects(BHM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.gammaSideSetupSideTwo != null)
                    {
                        /// derived from other variables
                        int enableMeasurements = 0;
                        int temperatureCoefficientAlarmEnable = 0;
                        int rateOfUnnChangealarmEnable = 0;

                        double voltage = inputConfiguration.gammaSideSetupSideTwo.RatedVoltage;
                        double current = inputConfiguration.gammaSideSetupSideTwo.RatedCurrent;
                        double phaseA = inputConfiguration.gammaSideSetupSideTwo.InputImpedance_0 / 100.0;
                        double phaseB = inputConfiguration.gammaSideSetupSideTwo.InputImpedance_1 / 100.0;
                        double phaseC = inputConfiguration.gammaSideSetupSideTwo.InputImpedance_2 / 100.0;

                        double amplitudeUnnWarningThresholdValue = inputConfiguration.gammaSideSetupSideTwo.GammaYellowThreshold / 20.0;
                        double amplitudeUnnAlarmThresholdValue = inputConfiguration.gammaSideSetupSideTwo.GammaRedThreshold / 20.0;

                        double temperatureCoefficientAlarmThresholdValue = inputConfiguration.gammaSideSetupSideTwo.TempCoefficient / 500.0;
                        double rateOfUnnChangeAlarmThresholdValue = inputConfiguration.gammaSideSetupSideTwo.TrendAlarm / 5.0;

                        double tangentPhaseA = inputConfiguration.gammaSideSetupSideTwo.Tg0_0 / 100.0;
                        double tangentPhaseB = inputConfiguration.gammaSideSetupSideTwo.Tg0_1 / 100.0;
                        double tangentPhaseC = inputConfiguration.gammaSideSetupSideTwo.Tg0_2 / 100.0;
                        double capacitancePhaseA = inputConfiguration.gammaSideSetupSideTwo.C0_0 / 10.0;
                        double capacitancePhaseB = inputConfiguration.gammaSideSetupSideTwo.C0_1 / 10.0;
                        double capacitancePhaseC = inputConfiguration.gammaSideSetupSideTwo.C0_2 / 10.0;

                        int temperatureAtMeasurement = inputConfiguration.gammaSideSetupSideTwo.Temperature0 - 70;
                        int temperatureSensor = inputConfiguration.gammaSideSetupSideTwo.TemperatureConfig;

                        int alarmEnable = inputConfiguration.gammaSideSetupSideTwo.AlarmEnable;
                        int readOnSide = inputConfiguration.gammaSetupInfo.ReadOnSide;

                        if ((readOnSide == 2) || (readOnSide == 3))
                        {
                            enableMeasurements = 1;
                        }

                        if (alarmEnable == 3)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                        }
                        else if (alarmEnable == 5)
                        {
                            rateOfUnnChangealarmEnable = 1;
                        }
                        else if (alarmEnable == 7)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                            rateOfUnnChangealarmEnable = 1;
                        }

                        if (enableMeasurements == 0)
                        {
                            secondBushingSetDisableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            secondBushingSetEnableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        secondBushingSetVoltageRadMaskedEditBox.Text = Math.Round(voltage, 2).ToString();
                        secondBushingSetCurrentRadMaskedEditBox.Text = Math.Round(current, 2).ToString();
                        secondBushingSetPhaseARadSpinEditor.Value = (Decimal)Math.Round(phaseA, 2);
                        secondBushingSetPhaseBRadSpinEditor.Value = (Decimal)Math.Round(phaseB, 2);
                        secondBushingSetPhaseCRadSpinEditor.Value = (Decimal)Math.Round(phaseC, 2);

                        secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text = Math.Round(amplitudeUnnWarningThresholdValue, 1).ToString();
                        secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text = Math.Round(amplitudeUnnAlarmThresholdValue, 1).ToString();

                        if (temperatureCoefficientAlarmEnable == 0)
                        {
                            secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text = Math.Round(temperatureCoefficientAlarmThresholdValue, 3).ToString();

                        if (rateOfUnnChangealarmEnable == 0)
                        {
                            secondBushingSetRateOfChangeAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            secondBushingSetRateOfChangeAlarmOnRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text = rateOfUnnChangeAlarmThresholdValue.ToString();

                        secondBushingSetTangentPhaseARadMaskedEditBox.Text = Math.Round(tangentPhaseA, 2).ToString();
                        secondBushingSetTangentPhaseBRadMaskedEditBox.Text = Math.Round(tangentPhaseB, 2).ToString();
                        secondBushingSetTangentPhaseCRadMaskedEditBox.Text = Math.Round(tangentPhaseC, 2).ToString();

                        secondBushingSetCapacitancePhaseARadMaskedEditBox.Text = Math.Round(capacitancePhaseA, 1).ToString();
                        secondBushingSetCapacitancePhaseBRadMaskedEditBox.Text = Math.Round(capacitancePhaseB, 1).ToString();
                        secondBushingSetCapacitancePhaseCRadMaskedEditBox.Text = Math.Round(capacitancePhaseC, 1).ToString();

                        secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text = temperatureAtMeasurement.ToString();

                        secondBushingSetTemperatureSensorRadDropDownList.SelectedIndex = temperatureSensor;
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteDataToSecondBushingSetTabObjects()\nPrivate data this.inputConfiguration.gammaSideSetupSideTwo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteDataToSecondBushingSetTabObjects()\nPrivate data this.inputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteDataToSecondBushingSetTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInSecondBushingSetTabObjects()
        {
            bool errorIsPresent = false;
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.gammaSideSetupSideTwo != null)
                    {
                        double voltage = 0.0;
                        double current = 0.0;
                        double phaseA = 0.0;
                        double phaseB = 0.0;
                        double phaseC = 0.0;

                        double amplitudeUnnWarningThresholdValue = 0.0;
                        double amplitudeUnnAlarmThresholdValue = 0.0;

                        double temperatureCoefficientAlarmThresholdValue = 0.0;
                        double rateOfUnnChangeAlarmThresholdValue = 0.0;

                        double tangentPhaseA = 0.0;
                        double tangentPhaseB = 0.0;
                        double tangentPhaseC = 0.0;
                        double capacitancePhaseA = 0.0;
                        double capacitancePhaseB = 0.0;
                        double capacitancePhaseC = 0.0;
                        int temperatureAtMeasurement = 0;

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetVoltageRadMaskedEditBox.Text, out voltage)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetVoltageRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetCurrentRadMaskedEditBox.Text, out current)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetCurrentRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }
                   
                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text, out amplitudeUnnWarningThresholdValue)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text, out amplitudeUnnAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text, out temperatureCoefficientAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text, out rateOfUnnChangeAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetTangentPhaseARadMaskedEditBox.Text, out tangentPhaseA)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetTangentPhaseARadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetTangentPhaseBRadMaskedEditBox.Text, out tangentPhaseB)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetTangentPhaseBRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetTangentPhaseCRadMaskedEditBox.Text, out tangentPhaseC)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetTangentPhaseCRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetCapacitancePhaseARadMaskedEditBox.Text, out capacitancePhaseA)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetCapacitancePhaseARadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetCapacitancePhaseBRadMaskedEditBox.Text, out capacitancePhaseB)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetCapacitancePhaseBRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(secondBushingSetCapacitancePhaseCRadMaskedEditBox.Text, out capacitancePhaseC)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetCapacitancePhaseCRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Int32.TryParse(secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text, out temperatureAtMeasurement)))
                        {
                            configurationError = true;
                            SelectSecondBushingSetTab();
                            secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInSecondBushingSetTabObjects()\nPrivate data this.workingConfiguration.gammaSideSetupSideTwo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInSecondBushingSetTabObjects()\nPrivate data this.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ErrorIsPresentInSecondBushingSetTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteSecondBushingSetTabObjectsToConfigurationData()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.gammaSideSetupSideTwo != null)
                    {
                        int alarmEnable = 1;
                        int temperatureCoefficientAlarmEnable = 0;
                        int rateOfUnnChangeEnable = 0;

                        double voltage = 0.0;
                        double current = 0.0;
                        double phaseA = 0.0;
                        double phaseB = 0.0;
                        double phaseC = 0.0;

                        double amplitudeUnnWarningThresholdValue = 0.0;
                        double amplitudeUnnAlarmThresholdValue = 0.0;

                        double temperatureCoefficientAlarmThresholdValue = 0.0;
                        double rateOfUnnChangeAlarmThresholdValue = 0.0;

                        double tangentPhaseA = 0.0;
                        double tangentPhaseB = 0.0;
                        double tangentPhaseC = 0.0;
                        double capacitancePhaseA = 0.0;
                        double capacitancePhaseB = 0.0;
                        double capacitancePhaseC = 0.0;
                        int temperatureAtMeasurement = 0;
                        int temperatureSensor = 0;

                        Double.TryParse(secondBushingSetVoltageRadMaskedEditBox.Text, out voltage);
                        Double.TryParse(secondBushingSetCurrentRadMaskedEditBox.Text, out current);
                        phaseA = (Double)secondBushingSetPhaseARadSpinEditor.Value;
                        phaseB = (Double)secondBushingSetPhaseBRadSpinEditor.Value;
                        phaseC = (Double)secondBushingSetPhaseCRadSpinEditor.Value;
                        Double.TryParse(secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text, out amplitudeUnnWarningThresholdValue);
                        Double.TryParse(secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text, out amplitudeUnnAlarmThresholdValue);

                        if (secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                        }

                        if (secondBushingSetRateOfChangeAlarmOnRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            rateOfUnnChangeEnable = 1;
                        }

                        Double.TryParse(secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text, out temperatureCoefficientAlarmThresholdValue);
                        Double.TryParse(secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text, out rateOfUnnChangeAlarmThresholdValue);
                        Double.TryParse(secondBushingSetTangentPhaseARadMaskedEditBox.Text, out tangentPhaseA);
                        Double.TryParse(secondBushingSetTangentPhaseBRadMaskedEditBox.Text, out tangentPhaseB);
                        Double.TryParse(secondBushingSetTangentPhaseCRadMaskedEditBox.Text, out tangentPhaseC);
                        Double.TryParse(secondBushingSetCapacitancePhaseARadMaskedEditBox.Text, out capacitancePhaseA);
                        Double.TryParse(secondBushingSetCapacitancePhaseBRadMaskedEditBox.Text, out capacitancePhaseB);
                        Double.TryParse(secondBushingSetCapacitancePhaseCRadMaskedEditBox.Text, out capacitancePhaseC);

                        Int32.TryParse(secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text, out temperatureAtMeasurement);

                        /// we have made this control invisible, and will default the value to 0
                        //temperatureSensor = secondBushingSetTemperatureSensorRadDropDownList.SelectedIndex;
                        

                        /// assign the values to the database object

                        if ((temperatureCoefficientAlarmEnable == 1) && (rateOfUnnChangeEnable == 1))
                        {
                            alarmEnable = 7;
                        }
                        else if (temperatureCoefficientAlarmEnable == 1)
                        {
                            alarmEnable = 3;
                        }
                        else if (rateOfUnnChangeEnable == 1)
                        {
                            alarmEnable = 5;
                        }

                        workingConfiguration.gammaSideSetupSideTwo.AlarmEnable = alarmEnable;

                        workingConfiguration.gammaSideSetupSideTwo.RatedVoltage = voltage;
                        workingConfiguration.gammaSideSetupSideTwo.RatedCurrent = current;
                        workingConfiguration.gammaSideSetupSideTwo.InputImpedance_0 = (Int32)Math.Round((phaseA * 100), 0);
                        workingConfiguration.gammaSideSetupSideTwo.InputImpedance_1 = (Int32)Math.Round((phaseB * 100), 0);
                        workingConfiguration.gammaSideSetupSideTwo.InputImpedance_2 = (Int32)Math.Round((phaseC * 100), 0);

                        workingConfiguration.gammaSideSetupSideTwo.GammaYellowThreshold = (Int32)Math.Round((amplitudeUnnWarningThresholdValue * 20.0), 0);
                        workingConfiguration.gammaSideSetupSideTwo.GammaRedThreshold = (Int32)Math.Round((amplitudeUnnAlarmThresholdValue * 20.0), 0);

                        workingConfiguration.gammaSideSetupSideTwo.TempCoefficient = (Int32)Math.Round((temperatureCoefficientAlarmThresholdValue * 500.0), 0);
                        workingConfiguration.gammaSideSetupSideTwo.TrendAlarm = (Int32)Math.Round((rateOfUnnChangeAlarmThresholdValue * 5.0), 0);

                        workingConfiguration.gammaSideSetupSideTwo.Tg0_0 = (Int32)Math.Round((tangentPhaseA * 100), 0);
                        workingConfiguration.gammaSideSetupSideTwo.Tg0_1 = (Int32)Math.Round((tangentPhaseB * 100), 0);
                        workingConfiguration.gammaSideSetupSideTwo.Tg0_2 = (Int32)Math.Round((tangentPhaseC * 100), 0);

                        workingConfiguration.gammaSideSetupSideTwo.C0_0 = (Int32)Math.Round((capacitancePhaseA * 10), 0);
                        workingConfiguration.gammaSideSetupSideTwo.C0_1 = (Int32)Math.Round((capacitancePhaseB * 10), 0);
                        workingConfiguration.gammaSideSetupSideTwo.C0_2 = (Int32)Math.Round((capacitancePhaseC * 10), 0);

                        workingConfiguration.gammaSideSetupSideTwo.Temperature0 = temperatureAtMeasurement + 70;
                        workingConfiguration.gammaSideSetupSideTwo.TemperatureConfig = temperatureSensor;
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteSecondBushingSetTabObjectsToConfigurationData()\nPrivate data this.workingConfiguration.gammaSideSetupSideTwo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteSecondBushingSetTabObjectsToConfigurationData()\nPrivate data this.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteSecondBushingSetTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        private void DisableSecondBushingSetTabObjects()
        {
            secondBushingSetEnableMeasurementsRadRadioButton.Enabled = false;
            secondBushingSetDisableMeasurementsRadRadioButton.Enabled = false;
            secondBushingSetVoltageRadMaskedEditBox.ReadOnly = true;
            secondBushingSetCurrentRadMaskedEditBox.ReadOnly = true;
            secondBushingSetPhaseARadSpinEditor.ReadOnly = true;
            secondBushingSetPhaseBRadSpinEditor.ReadOnly = true;
            secondBushingSetPhaseCRadSpinEditor.ReadOnly = true;
            secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.ReadOnly = true;
            secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Enabled = false;
            secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Enabled = false;
            secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            secondBushingSetRateOfChangeAlarmOnRadRadioButton.Enabled = false;
            secondBushingSetRateOfChangeAlarmOffRadRadioButton.Enabled = false;
            secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            secondBushingSetTangentPhaseARadMaskedEditBox.ReadOnly = true;
            secondBushingSetTangentPhaseBRadMaskedEditBox.ReadOnly = true;
            secondBushingSetTangentPhaseCRadMaskedEditBox.ReadOnly = true;
            secondBushingSetCapacitancePhaseARadMaskedEditBox.ReadOnly = true;
            secondBushingSetCapacitancePhaseBRadMaskedEditBox.ReadOnly = true;
            secondBushingSetCapacitancePhaseCRadMaskedEditBox.ReadOnly = true;
            secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.ReadOnly = true;
            secondBushingSetTemperatureSensorRadDropDownList.Enabled = false;
        }

        private void EnableSecondBushingSetTabObjects()
        {
            secondBushingSetEnableMeasurementsRadRadioButton.Enabled = true;
            secondBushingSetDisableMeasurementsRadRadioButton.Enabled = true;
            secondBushingSetVoltageRadMaskedEditBox.ReadOnly = false;
            secondBushingSetCurrentRadMaskedEditBox.ReadOnly = false;
            secondBushingSetPhaseARadSpinEditor.ReadOnly = false;
            secondBushingSetPhaseBRadSpinEditor.ReadOnly = false;
            secondBushingSetPhaseCRadSpinEditor.ReadOnly = false;
            secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.ReadOnly = false;
            secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Enabled = true;
            secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Enabled = true;
            secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            secondBushingSetRateOfChangeAlarmOnRadRadioButton.Enabled = true;
            secondBushingSetRateOfChangeAlarmOffRadRadioButton.Enabled = true;
            secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            secondBushingSetTangentPhaseARadMaskedEditBox.ReadOnly = false;
            secondBushingSetTangentPhaseBRadMaskedEditBox.ReadOnly = false;
            secondBushingSetTangentPhaseCRadMaskedEditBox.ReadOnly = false;
            secondBushingSetCapacitancePhaseARadMaskedEditBox.ReadOnly = false;
            secondBushingSetCapacitancePhaseBRadMaskedEditBox.ReadOnly = false;
            secondBushingSetCapacitancePhaseCRadMaskedEditBox.ReadOnly = false;
            secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.ReadOnly = false;
            secondBushingSetTemperatureSensorRadDropDownList.Enabled = true;
        }

    }
}
