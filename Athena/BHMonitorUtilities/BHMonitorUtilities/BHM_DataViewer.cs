using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.RichTextBox;

using ChartDirector;

using GeneralUtilities;
using ConfigurationObjects;
using DataObjects;
using PasswordManagement;
using FormatConversion;
using DatabaseInterface;
using DatabaseFileInteraction;
using Dynamics;
using MonitorUtilities;
using StatisticsDisplay;

namespace BHMonitorUtilities
{
    public partial class BHM_DataViewer : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// An in-memory copy of the group 1 database data in a convenient class format
        /// </summary>
        BHM_DataStructure dbDataGroup1 = new BHM_DataStructure();
        /// <summary>
        /// An in-memory copy of the group 2 database data in a convenient class format
        /// </summary>
        BHM_DataStructure dbDataGroup2 = new BHM_DataStructure();

        BHM_CommonDataStructure commonData = new BHM_CommonDataStructure();

        private string moistureRadCheckBoxEnabledKeyText = "moistureRadCheckBoxEnabledKey";
        private string loadCurrent1RadCheckBoxEnabledKeyText = "loadCurrent1RadCheckBoxEnabledKey";
        private string loadCurrent2RadCheckBoxEnabledKeyText = "loadCurrent2RadCheckBoxEnabledKey";
        private string loadCurrent3RadCheckBoxEnabledKeyText = "loadCurrent3RadCheckBoxEnabledKey";
        private string voltage1RadCheckBoxEnabledKeyText = "voltage1RadCheckBoxEnabledKey";
        private string voltage2RadCheckBoxEnabledKeyText = "voltage2RadCheckBoxEnabledKey";
        private string voltage3RadCheckBoxEnabledKeyText = "voltage3adCheckBoxEnabledKey";

        private string trueText = "true";
        private string falseText = "false";

        private Dictionary<string, string> miscellaneousProperties;

        private static string htmlPrefix = "<html>";
        private static string htmlPostfix = "</html>";

        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string warningAndAlarmThresholdsNotAvailableText = "Could not find the current device configuration in the database\nThe warning and alarm threshold values will not be displayed in the polar chart";

        private static string gammaWarningThresholdLegendText = "Gamma Warning Threshold";
        private static string gammaAlarmThresholdLegendText = "Gamma Alarm Threshold";

        private static string setOneText = "Set 1";
        private static string setTwoText = "Set 2";

        private static string noDataForSelectedDataAgeText = "No data available for selected Data Age";
        private static string saveChangesOrCancelBeforeSwitchingTabsText = "You must save changes or cancel before switching tabs.";
        private static string addText = "add";
        private static string multiplyText = "multiply";
        private static string copyGraphText = "Copy Graph";

        private static string failedToReadAnyDataToSaveToCsvFile = "No data was available to save to the csv file";

        private static string loadDataByAgeToolTipText = "Load data by its age with respect to the current date";
        private static string selectTransformerSideToolTipText = "Select which transformer side data to display";
        private static string useRightMouseToolTipText = "Use right mouse to open dynamics editor";

        private static string readingDateInsulationParametersGridText = "Reading Date";

        private static string percentUnitsText = "%";
        private static string degreesAngularUnitsText = "deg.";
        private static string unitlessUnitsText = "";
        private static string percentPerYearUnitsText = "%/year";
        private static string degreesCentigradeUnitsText = "�C";
        private static string milliAmpsUnitsText = "mA";
        private static string picoFaradsUnitsText = "pF";
        private static string hertzUnitsText = "Hz";
        private static string kiloVoltsUnitsText = "kV";
        private static string percentFLUnitsText = "%FL";
        private static string ampsUnitsText = "Amps";

        private static string idGridLabelText = "ID";
        private static string dataRootIdGridLabelText = "Data Root ID";
        private static string sideNumberGridLabelText = "Side Number";
        private static string sourceAmplitude0GridLabelText = "Source Amplitude 0";
        private static string sourceAmplitude1GridLabelText = "Source Amplitude 1";
        private static string sourceAmplitude2GridLabelText = "Source Amplitude 2";
        private static string phaseAmplitude0GridLabelText = "Phase Amplitude 0";
        private static string phaseAmplitude1GridLabelText = "Phase Amplitude 1";
        private static string phaseAmplitude2GridLabelText = "Phase Amplitude 2";
        private static string gammaGridLabelText = "Gamma";
        private static string gammaPhaseGridLabelText = "Gamma Phase";
        private static string ktGridLabelText = "KT";
        private static string alarmStatusGridLabelText = "Alarm Status";
        private static string remainingLife0GridLabelText = "Remaining Life 0";
        private static string remainingLife1GridLabelText = "Remaining Life 1";
        private static string remainingLife2GridLabelText = "Remaining Life 2";
        private static string defectCodeGridLabelText = "Defect Code";
        private static string chPhaseShiftGridLabelText = "Channel Phase Shift";
        private static string trendGridLabelText = "Trend";
        private static string ktPhaseGridLabelText = "KT Phase";
        private static string signalPhase0GridLabelText = "Signal Phase 0";
        private static string signalPhase1GridLabelText = "Signal Phase 1";
        private static string signalPhase2GridLabelText = "Signal Phase 2";
        private static string sourcePhase0GridLabelText = "Source Phase 0";
        private static string sourcePhase1GridLabelText = "Source Phase 1";
        private static string sourcePhase2GridLabelText = "Source Phase 2";
        private static string frequencyGridLabelText = "Frequency";
        private static string temperatureGridLabelText = "Temperature";
        private static string c0GridLabelText = "C 0";
        private static string c1GridLabelText = "C 1";
        private static string c2GridLabelText = "C 2";
        private static string tg0GridLabelText = "Tg 0";
        private static string tg1GridLabelText = "Tg 1";
        private static string tg2GridLabelText = "Tg 2";
        private static string externalSyncShiftGridLabelText = "External Sync Shift";

        // inteface objects text strings

        private static string bhmDataViewerInterfaceTitleText = "Bushing Health Monitor Data Viewer";

        private static string pointerRadioButtonText = "Pointer";
        private static string zoomInRadioButtonText = "Zoom In";
        private static string zoomOutRadioButtonText = "Zoom Out";
        private static string dataAgeRadGroupBoxText = "Data Age";
        private static string threeMonthsRadRadioButtonText = "3 Months";
        private static string sixMonthsRadRadioButtonText = "6 Months";
        private static string oneYearRadRadioButtonText = "1 Year";
        private static string allDataRadRadioButtonText = "All";
        private static string dataGroupSelectionRadGroupBoxText = "Data Group Shown";
        private static string group1RadRadioButtonText = "Set 1";
        private static string group2RadRadioButtonText = "Set 2";
        private static string allGroupsRadRadioButtonText = "Both Sets";
        private static string drawRadGroupBoxText = "Graph Method";
        private static string autoDrawRadRadioButtonText = "Automatic Draw";
        private static string manualDrawRadRadioButtonText = "Manual Draw";
        private static string drawGraphsRadButtonText = "Draw Graph";

        private static string trendRadPageViewPageText = "Trend";
        private static string trendPointerRadioButtonText = "Pointer";
        private static string trendZoomInRadioButtonText = "Zoom In";
        private static string trendZoomOutRadioButtonText = "Zoom Out";
        private static string gammaRadCheckBoxText = "Gamma (%)";
        private static string gammaPhaseRadCheckBoxText = "Gamma Phase (deg)";
        private static string capacitanceARadCheckBoxText = "Capacitance 1 (pF)";
        private static string capacitanceBRadCheckBoxText = "Capacitance 2 (pF)";
        private static string capacitanceCRadCheckBoxText = "Capacitance 3 (pF)";
        private static string tangentARadCheckBoxText = "Tangent 1 (%)";
        private static string tangentBRadCheckBoxText = "Tangent 2 (%)";
        private static string tangentCRadCheckBoxText = "Tangent 3 (%)";
        private static string temperatureCoefficientRadCheckBoxText = "Temperature Coeff";
        private static string gammaTrendRadCheckBoxText = "Gamma Trend (%/year)";
        private static string frequencyRadCheckBoxText = "Frequency (Hz)";
        private static string bushingCurrentPhaseAARadCheckBoxText = "Phase Angle 11 (deg)";
        private static string bushingCurrentPhaseABRadCheckBoxText = "Phase Angle 12 (deg)";
        private static string bushingCurrentPhaseACRadCheckBoxText = "Phase Angle 13 (deg)";
        private static string bushingCurrentARadCheckBoxText = "Bushing Current 1 (mA)";
        private static string bushingCurrentBRadCheckBoxText = "Bushing Current 2 (mA)";
        private static string bushingCurrentCRadCheckBoxText = "Bushing Current 3 (mA)";
        private static string phaseTemperatureCoefficientRadCheckBoxText = "Phase Temp Coeff(deg)";
        private static string trendDataAgeRadGroupBoxText = "Data Age";
        private static string trendThreeMonthsRadRadioButtonText = "3 Months";
        private static string trendSixMonthsRadRadioButtonText = "6 Months";
        private static string trendOneYearRadRadioButtonText = "1 Year";
        private static string trendAllDataRadRadioButtonText = "All";
        private static string trendDataGroupSelectionRadGroupBoxText = "Data Group Shown";
        private static string trendGroup1RadRadioButtonText = "Set 1";
        private static string trendGroup2RadRadioButtonText = "Set 2";
        private static string trendAllGroupsRadRadioButtonText = "Both Sets";
        private static string trendDisplayOptionsRadGroupBoxText = "Display Options";
        private static string trendLineRadCheckBoxText = "Show Trend Line";
        private static string linearTrendRadRadioButtonText = "Linear";
        private static string exponentialTrendRadRadioButtonText = "Exponential";
        private static string normalizeDataRadCheckBoxText = "Normalize Data";
        private static string movingAverageRadCheckBoxText = "Show Moving Avg";
        private static string dataItemsRadLabelText = "data items";
        private static string trendDrawRadGroupBoxText = "Graph Method";
        private static string trendAutoDrawRadRadioButtonText = "Automatic Draw";
        private static string trendManualDrawRadRadioButtonText = "Manual Draw";
        private static string trendDrawGraphsRadButtonText = "Draw Graph";


        private static string dynamicsRadGroupBoxText = "Dynamics";
        private static string humidityRadCheckBoxText = "Humidity (%)";
        private static string moistureContentRadCheckBoxText = "Moisture (kg/M3)";
        private static string temp1RadCheckBoxText = "Correlating Temp (�C)";
        private static string temp2RadCheckBoxText = "Temp 2 (�C)";
        private static string temp3RadCheckBoxText = "Temp 3 (�C)";
        private static string temp4RadCheckBoxText = "Temp 4 (�C)";
        private static string loadCurrent1RadCheckBoxText = "Load Current 1 (A)";
        private static string loadCurrent2RadCheckBoxText = "Load Current 2 (A)";
        private static string loadCurrent3RadCheckBoxText = "Load Current 3 (A)";
        private static string voltage1RadCheckBoxText = "Voltage 1 (kV)";
        private static string voltage2RadCheckBoxText = "Voltage 2 (kV)";
        private static string voltage3RadCheckBoxText = "Voltage 3 (kV)";

        private static string polarRadPageViewPageText = "Polar Diagram";
        private static string polarPointerRadioButtonText = "Pointer";
        private static string polarZoomInRadioButtonText = "Zoom In";
        private static string polarZoomOutRadioButtonText = "Zoom Out";
        private static string showRadialLabelsRadCheckBoxText = "Show Radial Labels";
        private static string polarDataAgeRadGroupBoxText = "Data Age";
        private static string polarThreeMonthsRadRadioButtonText = "3 Months";
        private static string polarSixMonthsRadRadioButtonText = "6 Months";
        private static string polarOneYearRadRadioButtonText = "1 Year";
        private static string polarAllDataRadRadioButtonText = "All";
        private static string polarDataGroupSelectionRadGroupBoxText = "Data Group Shown";
        private static string polarGroup1RadRadioButtonText = "Set 1";
        private static string polarGroup2RadRadioButtonText = "Set 2";
        private static string polarAllGroupsRadRadioButtonText = "Both Groups";
        private static string polarDataDisplayChoiceRadGroupBoxText = "Choose Data to Display";
        private static string gammaPolarRadRadioButtonText = "Gamma";
        private static string temperatureCoefficientPolarRadRadioButtonText = "Temperature Coefficient";
        private static string phaseSegmentPolarRadGroupBoxText = "Phase Segment";
        private static string capacitanceAPolarRadCheckBoxText = "Capacitance 1";
        private static string capacitanceBPolarRadCheckBoxText = "Capacitance 2";
        private static string capacitanceCPolarRadCheckBoxText = "Capacitance 3";
        private static string tangentAPolarRadCheckBoxText = "Tangent 1";
        private static string tangentBPolarRadCheckBoxText = "Tangent 2";
        private static string tangentCPolarRadCheckBoxText = "Tangent 3";
        private static string polarDrawRadGroupBoxText = "Graph Method";
        private static string polarAutoDrawRadRadioButtonText = "Automatic Draw";
        private static string polarManualDrawRadRadioButtonText = "Manual Draw";
        private static string polarDrawGraphsRadButtonText = "Draw Graph";

        private static string gridRadPageViewPageText = "Data Grid";
        private static string gridExpandAllRadButtonText = "Expand All";
        private static string gridHideAllRadButtonText = "Hide All";
        private static string gridDataEditRadButtonText = "Edit Data";
        private static string gridDataSaveChangesRadButtonText = "Save Changes";
        private static string gridDataCancelChangesRadButtonText = "Cancel Changes";
        private static string gridDataDeleteSelectedRadButtonText = "Delete Selected Items";

        private static string gammaLegendText = "Gamma";
        private static string gammaPhaseLegendText = "Gamma Phase";
        private static string gammaTrendLegendText = "Gamma Trend";
        private static string tempCoeffLegendText = "Temp Coeff";
        private static string phaseTempCoeffLegendText = "Phase Temp Coeff";
        private static string bushingCur1LegendText = "Bushing Cur 1";
        private static string bushingCur2LegendText = "Bushing Cur 2";
        private static string bushingCur3LegendText = "Bushing Cur 3";
        private static string bushingCurPhase11LegendText = "Bushing Cur Phase 11";
        private static string bushingCurPhase12LegendText = "Bushing Cur Phase 12";
        private static string bushingCurPhase13LegendText = "Bushing Cur Phase 13";
        private static string capacitance1LegendText = "Capacitance 1";
        private static string capacitance2LegendText = "Capacitance 2";
        private static string capacitance3LegendText = "Capacitance 3";
        private static string tangent1LegendText = "Tangent 1";
        private static string tangent2LegendText = "Tangent 2";
        private static string tangent3LegendText = "Tangent 3";
        private static string frequencyLegendText = "Frequency";



        private static string dynamicsCorrelatedWithText = "Dynamics correlated with";

        // Not sure where I meant to use this string, right now it is not used
        private static string dataComparisonForReadingTakenOnText = "Data comparison for reading taken on";

        private static string phasesSwappedSetOneText = "Phases 2 and 3 are swapped for set 1";
        private static string phasesSwappedSetTwoText = "Phases 2 and 3 are swapped for set 2";
        private static string phasesSwappedBothSetsText = "Phases 2 and 3 are swapped for sets 1 and 2";

        private static string createCorrelationGraphRadButtonText = "Show Correlations";
        private static string createReportRadButtonText = "Create Report";

        private static string saveDataToCsvRadButtonText = "Save Data to CSV";

        private static string initialBalanceDataRadPageViewPageText = "Initial Balance Data";
        private static string initialBalanceDataTabMeasurementDateRadLabelText = "Measurement Date:";
        private static string initialBalanceDataTabMeasurementDateValueRadLabelText = "";
        private static string initialBalanceDataTabBushingSet1RadLabelText = "Bushing Set 1";
        private static string initialBalanceDataTabBushingSet2RadLabelText = "Bushing Set 2";
        private static string initialBalanceDataTabImbalanceRadLabelText = "Imbalance (%)";
        private static string initialBalanceDataTabImbalancePhaseRadLabelText = "Imbalance Phase, gradient";
        private static string initialBalanceDataTabTemperatureRadLabelText = "Temperature (�C)";
        private static string initialBalanceDataTabAmplitudePhaseARadLabelText = "Input signal amplitude Phase 1 (mV)";
        private static string initialBalanceDataTabAmplitudePhaseBRadLabelText = "Input signal amplitude Phase 2 (mV)";
        private static string initialBalanceDataTabAmplitudePhaseCRadLabelText = "Input signal amplitude Phase 3 (mV)";
        private static string initialBalanceDataTabShiftPhaseARadLabelText = "Input signal Shift Phase 1 (deg)";
        private static string initialBalanceDataTabShiftPhaseBRadLabelText = "Input signal Shift Phase 2 (deg)";
        private static string initialBalanceDataTabShiftPhaseCRadLabelText = "Input signal Shift Phase 3 (deg)";

        #region variables used to construct the graphs

        enum ScaleType
        {
            Percent,
            DegreesAngular,
            Unitless,
            PercentPerYear,
            DegreesCentigrade,
            KilogramsPerMeterCubed,
            MilliAmps,
            PicoFarads,
            Hertz,
            KiloVolts,
            PercentFL,
            Amps
        };

        public class ScaleLabels
        {
            public static string Percent = percentUnitsText;
            public static string DegreesAngular = degreesAngularUnitsText;
            public static string Unitless = unitlessUnitsText;
            public static string PercentPerYear = percentPerYearUnitsText;
            public static string DegreesCentigrade = degreesCentigradeUnitsText;
            public static string KilogramsPerMeterCubed = "kg/M3";
            public static string MilliAmps = milliAmpsUnitsText;
            public static string PicoFarads = picoFaradsUnitsText;
            public static string Hertz = hertzUnitsText;
            public static string KiloVolts = kiloVoltsUnitsText;
            public static string PercentFL = percentFLUnitsText;
            public static string Amps = ampsUnitsText;
        }

        #endregion

        #region Color Settings for Graphing the Data Types

        // Claudes colors
        public int gammaColor = Chart.CColor(ColorTranslator.FromHtml("#0055cc")); // blue
        public int gammaPhaseColor = Chart.CColor(ColorTranslator.FromHtml("#ffcc00"));
        public int gammaTrendColor = Chart.CColor(ColorTranslator.FromHtml("#ace600"));
        public int temperatureCoefficientColor = Chart.CColor(ColorTranslator.FromHtml("#ffba00"));
        public int phaseTemperatureCoefficientColor = Chart.CColor(ColorTranslator.FromHtml("#bb00cc"));
        public int bushingCurrentAColor = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int bushingCurrentBColor = Chart.CColor(ColorTranslator.FromHtml("#00cc00"));
        public int bushingCurrentCColor = Chart.CColor(ColorTranslator.FromHtml("#ff8a00"));
        public int bushingCurrentPhaseAAColor = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int bushingCurrentPhaseABColor = Chart.CColor(ColorTranslator.FromHtml("#00cc00"));
        public int bushingCurrentPhaseACColor = Chart.CColor(ColorTranslator.FromHtml("#ff8a00"));
        public int capacitanceAColor = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int capacitanceBColor = Chart.CColor(ColorTranslator.FromHtml("#00cc00"));
        public int capacitanceCColor = Chart.CColor(ColorTranslator.FromHtml("#ff8a00"));
        public int tangentAColor = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int tangentBColor = Chart.CColor(ColorTranslator.FromHtml("#00cc00"));
        public int tangentCColor = Chart.CColor(ColorTranslator.FromHtml("#ff8a00"));
        public int frequencyColor = Chart.CColor(ColorTranslator.FromHtml("#d9007e"));
        public int temperatureColor = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int ltcPositionNumberColor = Chart.CColor(ColorTranslator.FromHtml("#bfa300"));
        public int alarmStatusColor = Chart.CColor(ColorTranslator.FromHtml("#bf5700"));


        // These were the final colors I was using - DL
        //public int gammaColor = Chart.CColor(Color.Red);
        //public int gammaPhaseColor = Chart.CColor(Color.Blue);
        //public int gammaTrendColor = Chart.CColor(Color.BurlyWood);
        //public int temperatureCoefficientColor = Chart.CColor(Color.Brown);
        //public int phaseTemperatureCoefficientColor = Chart.CColor(Color.Orange);
        //public int bushingCurrentAColor = Chart.CColor(Color.Gold);
        //public int bushingCurrentBColor = Chart.CColor(Color.SlateGray);
        //public int bushingCurrentCColor = Chart.CColor(Color.Tan);
        //public int bushingCurrentPhaseAAColor = Chart.CColor(Color.IndianRed);
        //public int bushingCurrentPhaseABColor = Chart.CColor(Color.Purple);
        //public int bushingCurrentPhaseACColor = Chart.CColor(Color.LightBlue);
        //public int capacitanceAColor = Chart.CColor(Color.RoyalBlue);
        //public int capacitanceBColor = Chart.CColor(Color.YellowGreen);
        //public int capacitanceCColor = Chart.CColor(Color.Green);
        //public int tangentAColor = Chart.CColor(Color.DarkKhaki);
        //public int tangentBColor = Chart.CColor(Color.OrangeRed);
        //public int tangentCColor = Chart.CColor(Color.Pink);
        //public int frequencyColor = Chart.CColor(Color.Tomato);
        //public int temperatureColor = Chart.CColor(Color.Fuchsia);
        ////public int loadActiveColor = Chart.CColor(Color.DarkSlateGray);
        ////public int loadReactiveColor = Chart.CColor(Color.SpringGreen);
        //public int ltcPositionNumberColor = Chart.CColor(Color.OliveDrab);
        //public int alarmStatusColor = Chart.CColor(Color.LightGreen);

        // claudes code for colors
        public int temp1Color = Chart.CColor(ColorTranslator.FromHtml("#ff00c7"));
        public int temp2Color = Chart.CColor(ColorTranslator.FromHtml("#ff4800"));
        public int temp3Color = Chart.CColor(ColorTranslator.FromHtml("#ffbb66"));
        public int temp4Color = Chart.CColor(ColorTranslator.FromHtml("#ff6600"));
        public int chassisTempColor = Chart.CColor(ColorTranslator.FromHtml("#00a200"));

        public int humidityColor = Chart.CColor(ColorTranslator.FromHtml("#ff7474"));
        public int moistureColor = Chart.CColor(ColorTranslator.FromHtml("#9272f9"));

        public int loadCurrent1Color = Chart.CColor(ColorTranslator.FromHtml("#c02f2f"));
        public int loadCurrent2Color = Chart.CColor(ColorTranslator.FromHtml("#6b3ff9"));
        public int loadCurrent3Color = Chart.CColor(ColorTranslator.FromHtml("#00f800"));

        public int voltage1Color = Chart.CColor(ColorTranslator.FromHtml("#c02f2f"));
        public int voltage2Color = Chart.CColor(ColorTranslator.FromHtml("#6b3ff9"));
        public int voltage3Color = Chart.CColor(ColorTranslator.FromHtml("#00f800"));

        // These were the final colors I was using - DL
        //public int temp1Color = Chart.CColor(Color.Red);
        //public int temp2Color = Chart.CColor(Color.Yellow);
        //public int temp3Color = Chart.CColor(Color.Indigo);
        //public int temp4Color = Chart.CColor(Color.Green);
        //public int chassisTempColor = Chart.CColor(Color.Orange);

        //public int humidityColor = Chart.CColor(Color.Violet);
        //public int moistureColor = Chart.CColor(Color.SlateGray);

        //public int loadCurrent1Color = Chart.CColor(Color.Chartreuse);
        //public int loadCurrent2Color = Chart.CColor(Color.Teal);
        //public int loadCurrent3Color = Chart.CColor(Color.Brown);

        //public int voltage1Color = Chart.CColor(Color.OrangeRed);
        //public int voltage2Color = Chart.CColor(Color.Blue);
        //public int voltage3Color = Chart.CColor(Color.Purple);

        // THese were all colors from a first pass at doing colors
        //public int gammaColorGroup1 = Chart.CColor(Color.RoyalBlue);
        //public int gammaPhaseColorGroup1 = Chart.CColor(Color.CornflowerBlue);
        //public int gammaTrendColorGroup1 = Chart.CColor(Color.SteelBlue);
        //public int temperatureCoefficientColorGroup1 = Chart.CColor(Color.Azure);
        //public int phaseTemperatureCoefficientColorGroup1 = Chart.CColor(Color.PaleGreen);
        //public int bushingCurrentAColorGroup1 = Chart.CColor(Color.Red);
        //public int bushingCurrentBColorGroup1 = Chart.CColor(Color.DarkOrange);
        //public int bushingCurrentCColorGroup1 = Chart.CColor(Color.Orange);
        //public int bushingCurrentPhaseAAColorGroup1 = Chart.CColor(Color.IndianRed);
        //public int bushingCurrentPhaseABColorGroup1 = Chart.CColor(Color.Tomato);
        //public int bushingCurrentPhaseACColorGroup1 = Chart.CColor(Color.Chocolate);
        //public int capacitanceAColorGroup1 = Chart.CColor(Color.Gold);
        //public int capacitanceBColorGroup1 = Chart.CColor(Color.DarkKhaki);
        //public int capacitanceCColorGroup1 = Chart.CColor(Color.BurlyWood);
        //public int tangentAColorGroup1 = Chart.CColor(Color.PaleVioletRed);
        //public int tangentBColorGroup1 = Chart.CColor(Color.Fuchsia);
        //public int tangentCColorGroup1 = Chart.CColor(Color.Pink);
        //public int frequencyColorGroup1 = Chart.CColor(Color.NavajoWhite);
        //public int temperatureColorGroup1 = Chart.CColor(Color.SlateGray);
        //public int loadActiveColorGroup1 = Chart.CColor(Color.DarkSlateGray);
        //public int loadReactiveColorGroup1 = Chart.CColor(Color.SpringGreen);
        //public int ltcPositionNumberColorGroup1 = Chart.CColor(Color.OliveDrab);
        //public int alarmStatusColorGroup1 = Chart.CColor(Color.LightGreen);

        //public int gammaColorGroup2 = Chart.CColor(Color.LightSteelBlue);
        //public int gammaPhaseColorGroup2 = Chart.CColor(Color.Lavender);
        //public int gammaTrendColorGroup2 = Chart.CColor(Color.MediumBlue);
        //public int temperatureCoefficientColorGroup2 = Chart.CColor(Color.PaleTurquoise);
        //public int phaseTemperatureCoefficientColorGroup2 = Chart.CColor(Color.LightGreen);
        //public int bushingCurrentAColorGroup2 = Chart.CColor(Color.OrangeRed);
        //public int bushingCurrentBColorGroup2 = Chart.CColor(Color.DarkGoldenrod);
        //public int bushingCurrentCColorGroup2 = Chart.CColor(Color.Goldenrod);
        //public int bushingCurrentPhaseAAColorGroup2 = Chart.CColor(Color.Crimson);
        //public int bushingCurrentPhaseABColorGroup2 = Chart.CColor(Color.Coral);
        //public int bushingCurrentPhaseACColorGroup2 = Chart.CColor(Color.Salmon);
        //public int capacitanceAColorGroup2 = Chart.CColor(Color.Yellow);
        //public int capacitanceBColorGroup2 = Chart.CColor(Color.Tan);
        //public int capacitanceCColorGroup2 = Chart.CColor(Color.NavajoWhite);
        //public int tangentAColorGroup2 = Chart.CColor(Color.HotPink);
        //public int tangentBColorGroup2 = Chart.CColor(Color.Orchid);
        //public int tangentCColorGroup2 = Chart.CColor(Color.LightPink);
        //public int frequencyColorGroup2 = Chart.CColor(Color.AntiqueWhite);
        //public int temperatureColorGroup2 = Chart.CColor(Color.LightSlateGray);
        //public int loadActiveColorGroup2 = Chart.CColor(Color.CadetBlue);
        //public int loadReactiveColorGroup2 = Chart.CColor(Color.MediumSpringGreen);
        //public int ltcPositionNumberColorGroup2 = Chart.CColor(Color.YellowGreen);
        //public int alarmStatusColorGroup2 = Chart.CColor(Color.Honeydew);

        //public int humidityColor = Chart.CColor(Color.ForestGreen);

        //public int moistureColor = Chart.CColor(Color.Gainsboro);

        //public int temp1Color = Chart.CColor(Color.AliceBlue);
        //public int temp2Color = Chart.CColor(Color.Beige);
        //public int temp3Color = Chart.CColor(Color.BlueViolet);
        //public int temp4Color = Chart.CColor(Color.CadetBlue);

        //public int extLoadActiveColor = Chart.CColor(Color.DimGray);
        //public int extLoadReactiveColor = Chart.CColor(Color.DarkViolet);

      
        #endregion

        #region Symbol Definitions for Graphing the Data Types

        public int gammaSymbol = Chart.SquareShape;
        public int gammaPhaseSymbol = Chart.SquareShape;
        public int gammaTrendSymbol = Chart.SquareShape;
        public int temperatureCoefficientSymbol = Chart.Cross2Shape(.3);
        public int phaseTemperatureCoefficientSymbol = Chart.Cross2Shape(.3);
        public int bushingCurrentASymbol = Chart.StarShape(4);
        public int bushingCurrentBSymbol = Chart.StarShape(4);
        public int bushingCurrentCSymbol = Chart.StarShape(4);
        public int bushingCurrentPhaseAASymbol = Chart.StarShape(8);
        public int bushingCurrentPhaseABSymbol = Chart.StarShape(8);
        public int bushingCurrentPhaseACSymbol = Chart.StarShape(8);
        public int capacitanceASymbol = Chart.PolygonShape(5);
        public int capacitanceBSymbol = Chart.PolygonShape(5);
        public int capacitanceCSymbol = Chart.PolygonShape(5);
        public int tangentASymbol = Chart.Polygon2Shape(5);
        public int tangentBSymbol = Chart.Polygon2Shape(5);
        public int tangentCSymbol = Chart.Polygon2Shape(5);
        public int frequencySymbol = Chart.Cross2Shape(.3);
        public int ltcPositionNumberSymbol = Chart.Cross2Shape(.3);
        public int alarmStatusSymbol = Chart.Cross2Shape(.3);
        public int temperatureSymbol = Chart.CrossShape(.3);


        //public int gammaSymbol = Chart.DiamondShape;
        //public int gammaPhaseSymbol = Chart.SquareShape;
        //public int gammaTrendSymbol = Chart.TriangleShape;
        //public int temperatureCoefficientSymbol = Chart.LeftTriangleShape;
        //public int phaseTemperatureCoefficientSymbol = Chart.RightTriangleShape;
        //public int bushingCurrentASymbol = Chart.StarShape(3);
        //public int bushingCurrentBSymbol = Chart.StarShape(4);
        //public int bushingCurrentCSymbol = Chart.StarShape(5);
        //public int bushingCurrentPhaseAASymbol = Chart.StarShape(6);
        //public int bushingCurrentPhaseABSymbol = Chart.StarShape(7);
        //public int bushingCurrentPhaseACSymbol = Chart.StarShape(8);
        //public int capacitanceASymbol = Chart.PolygonShape(5);
        //public int capacitanceBSymbol = Chart.PolygonShape(7);
        //public int capacitanceCSymbol = Chart.PolygonShape(9);
        //public int tangentASymbol = Chart.Polygon2Shape(5);
        //public int tangentBSymbol = Chart.Polygon2Shape(7);
        //public int tangentCSymbol = Chart.Polygon2Shape(9);
        //public int frequencySymbol = Chart.InvertedTriangleShape;
        //public int ltcPositionNumberSymbol = Chart.Cross2Shape(.3);
        //public int alarmStatusSymbol = Chart.Cross2Shape(.5);
        //public int temperatureSymbol = Chart.CrossShape(.9);

        //public int humiditySymbol = Chart.Cross2Shape(.7);
        //public int moistureSymbol = Chart.Cross2Shape(.9);

        //public int temp1Symbol = Chart.CrossShape(.1);
        //public int temp2Symbol = Chart.CrossShape(.3);
        //public int temp3Symbol = Chart.CrossShape(.5);
        //public int temp4Symbol = Chart.CrossShape(.7);

        //public int extLoadActiveSymbol = Chart.CircleShape;
        //public int extLoadReactiveSymbol = Chart.GlassSphereShape;

        public int temp1Symbol = Chart.TriangleShape;
        public int temp2Symbol = Chart.TriangleShape;
        public int temp3Symbol = Chart.TriangleShape;
        public int temp4Symbol = Chart.TriangleShape;
        public int chassisTempSymbol = Chart.TriangleShape;

        public int humiditySymbol = Chart.InvertedTriangleShape;
        public int moistureSymbol = Chart.CircleShape;

        public int loadCurrent1Symbol = Chart.LeftTriangleShape;
        public int loadCurrent2Symbol = Chart.LeftTriangleShape;
        public int loadCurrent3Symbol = Chart.LeftTriangleShape;

        public int voltage1Symbol = Chart.RightTriangleShape;
        public int voltage2Symbol = Chart.RightTriangleShape;
        public int voltage3Symbol = Chart.RightTriangleShape;

        #endregion

        /// <summary>
        /// The total number of readings in the Group 1 data
        /// </summary>
        private int totalReadingsGroup1;
        /// <summary>
        /// The total number of readings in the Group 2 data
        /// </summary>
        private int totalReadingsGroup2;

        private int totalReadingsCommonData;

        /// <summary>
        /// This indicates if the data for group 1 and group 2 are an exact match on the x(date) axis.  In other words, if this is true, 
        /// neither data set has an x value that the other does not.
        /// </summary>
        private bool groupOneAndGroupTwoDataHaveExactDateMatching;

        /// <summary>
        /// Tracks whether we're still loading data and keeps some actions from continually updating
        /// </summary>
        private bool hasFinishedInitialization;

        /// <summary>
        /// Indicates if group 1 data is being displayed
        /// </summary>
        private bool group1IsEnabled = false;
        /// <summary>
        /// Indicates if group 2 data is being displayed
        /// </summary>
        private bool group2IsEnabled = false;

        /// <summary>
        /// The ID of the monitor whose data we are examining
        /// </summary> 
        private Guid monitorID;

        /// <summary>
        /// The earliest date of data in the current data set
        /// </summary>
        private DateTime minimumDateOfDataSet;
        /// <summary>
        /// The range of the dates in the data set, in seconds
        /// </summary>
        private double dateRange;

        // The vertical range of the chart for vertical scrolling
        private double maxValue;
        private double minValue;

        /// <summary>
        /// The current visible duration of the view port in seconds
        /// </summary>
        private double currentDuration = 120 * 86400;
        /// <summary>
        /// The minimum visible duration in the graph view in seconds
        /// </summary>
        private double minDuration = .04 * 86400;

        // IQueryable<BHM_DataReadings> bhm_DataReadingsQuery;
        //MonitorInterfaceDB dbConnection;
        //BindingSource bhm_DataReadingsBindingSource;

        DateTime dataCutoffDate = ConversionMethods.MinimumDateTime();

        bool trendChartNeedsUpdating = false;

        bool polarChartNeedsUpdating = false;

        bool trendChartHasMouseFocus;

        bool polarChartHasMouseFocus;

        /// <summary>
        /// The minimum width of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumWidth = 710;

        /// <summary>
        /// The minimum height of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumHeight = 624;

        /// <summary>
        /// The minimum width of the trend graph
        /// </summary>
        int trendGraphWidthMinimum = 630;

        /// <summary>
        /// The minimum height of the trend graph
        /// </summary>
        int trendGraphHeightMinimum = 474;

        /// <summary>
        /// The minimum amount the trend graph is offset from the left border of the trendWinChartViewer object
        /// </summary>
        int trendGraphOffsetX = 55;

        /// <summary>
        /// The minimum amount the trend graph is offset from the top border of the trendWinChartViewer object
        /// </summary>
        int trendGraphtOffsetY = 100;

        int polarChartMinimumWidth = 700;

        int polarChartMinimumHeight = 660;

        int startingFormWidth = 1024;
        int startingFormHeight = 768;

        int trendPhasesSwappedInitialX = 760;
        int trendPhasesSwappedInitialY = 110;

        int polarPhasesSwappedInitialX = 760;
        int polarPhasesSwappedInitialY = 25;

        /// Stuff for cursor
        ///
        double[] allDateTimesAsDouble;

        int cursorDateTimeAsDoubleIndex = -1;
        double oldCursorXval;
        double cursorXval = -1.0;
        XYChart trendXYChart;

        int trendGraphCurrentOffsetX;
        int trendGraphXStart;
        int trendGraphYStart;
        int trendGraphHeight;
        int trendGraphWidth;

        int polarGraphCurrentCenterX;
        int polarGraphCurrentCenterY;
        int polarGraphCurrentRadius;
        int polarGraphXStart;
        int polarGraphYStart;
        int polarGraphHeight;
        int polarGraphWidth;

        bool cursorIsEnabled = true;

        private double humidityScaleFactor = 0.0;

        private double moistureScaleFactor = 0.0;

        private double temp1ScaleFactor = 0;
        private double temp2ScaleFactor = 0;
        private double temp3ScaleFactor = 0;
        private double temp4ScaleFactor = 0;

        private double loadCurrent1ScaleFactor = 1;
        private double loadCurrent2ScaleFactor = 1;
        private double loadCurrent3ScaleFactor = 1;

        private double voltage1ScaleFactor = 1;
        private double voltage2ScaleFactor = 1;
        private double voltage3ScaleFactor = 1;

        private string humidityOperation = addText;

        private string moistureOperation = addText;

        private string temp1Operation = addText;
        private string temp2Operation = addText;
        private string temp3Operation = addText;
        private string temp4Operation = addText;

        private string loadCurrent1Operation = multiplyText;
        private string loadCurrent2Operation = multiplyText;
        private string loadCurrent3Operation = multiplyText;

        private string voltage1Operation = multiplyText;
        private string voltage2Operation = multiplyText;
        private string voltage3Operation = multiplyText;

        PolarChart polarChartPointer;

        // end stuff for cursor

        #region SQL Query Stuff

        SqlConnection sqlDbConnection;
        MonitorInterfaceDBDataSet1 monitorInterfaceDBDataSet;

        // Query Strings
        public static string insulationParametersQueryString;
        public static string transformerSideParametersQueryString;

        // Data Tables
        DataTable insulationParametersDataTable;
        DataTable transformerSideParametersDataTable;

        // Data Adapters
        SqlDataAdapter insulationParametersDataAdapter;
        SqlDataAdapter transformerSideParametersDataAdapter;

        // Command Builders
        SqlCommandBuilder insulationParametersCommandBuilder;
        SqlCommandBuilder transformerSideParametersCommandBuilder;

        // Binding sources
        BindingSource insulationParametersBindingSource;
        BindingSource transformerSideParametersBindingSource;

        #endregion


        #region Grid View Stuff

        bool gridDataIsBeingEdited;

        bool graphDataNeedsUpdating;

        int[] insulatationParametersGridViewColumnWidths;

        string[] insulationParametersGridViewColumnHeaderText;

        int[] transformerSideParametersGridViewColumnWidths;

        string[] transformerSideParametersGridViewColumnText;

        int insulationParametersGridViewColumnCount = 11;
        int transformerSideParametersGridViewColumnCount = 35;

        #endregion

        private string dbConnectionString;
        private string fullPathToXmlLanguageFile;

        private string applicationDataPath;

        private double group1GammaWarningThreshold;
        private double group2GammaWarningThreshold;
        private double group1GammaAlarmThreshold;
        private double group2GammaAlarmThreshold;

        //private double group1TangentWarningThreshold;
        //private double group2TangentWarningThreshold;
        //private double group1TangentAlarmThreshold;
        //private double group2TangentAlarmThreshold;

        private bool humidityDataIsSelected = false;
        private bool moistureDataIsSelected = false;
        private bool temp1DataIsSelected = false;
        private bool temp2DataIsSelected = false;
        private bool temp3DataIsSelected = false;
        private bool temp4DataIsSelected = false;
        private bool loadCurrent1DataIsSelected = false;
        private bool loadCurrent2DataIsSelected = false;
        private bool loadCurrent3DataIsSelected = false;
        private bool voltage1DataIsSelected = false;
        private bool voltage2DataIsSelected = false;
        private bool voltage3DataIsSelected = false;

        private bool gammaDataIsSelected = false;
        private bool gammaPhaseDataIsSelected = false;
        private bool gammaTrendDataIsSelected = false;
        private bool temperatureCoefficientDataIsSelected = false;
        private bool phaseTemperatureCoefficientDataIsSelected = false;
        private bool bushingCurrentADataIsSelected = false;
        private bool bushingCurrentBDataIsSelected = false;
        private bool bushingCurrentCDataIsSelected = false;
        private bool bushingCurrentPhaseAADataIsSelected = false;
        private bool bushingCurrentPhaseABDataIsSelected = false;
        private bool bushingCurrentPhaseACDataIsSelected = false;
        private bool capacitanceADataIsSelected = false;
        private bool capacitanceBDataIsSelected = false;
        private bool capacitanceCDataIsSelected = false;
        private bool tangentADataIsSelected = false;
        private bool tangentBDataIsSelected = false;
        private bool tangentCDataIsSelected = false;
        private bool frequencyDataIsSelected = false;

        ProgramBrand programBrand;
        ProgramType programType;

        MonitorHierarchy monitorHierarchy;

        private bool setOnePhaseIsSwapped = false;
        private bool setTwoPhaseIsSwapped = false;

        public BHM_DataViewer(ProgramBrand inputProgramBrand, ProgramType inputProgramType, Guid bhmMonitorID, string inputPathToXmlLanguageFile, string inputDbConnectionString, string inputApplicationDataPath)
        {
            try
            {
                hasFinishedInitialization = false;
                InitializeComponent();

                this.programBrand = inputProgramBrand;
                this.programType = inputProgramType;

                this.monitorID = bhmMonitorID;

                this.fullPathToXmlLanguageFile = inputPathToXmlLanguageFile;
                this.dbConnectionString = inputDbConnectionString;

                this.applicationDataPath = inputApplicationDataPath;

                monitorInterfaceDBDataSet = new MonitorInterfaceDBDataSet1();

                // query strings
                insulationParametersQueryString = "SELECT ID, ReadingDateTime, RPN_0, RPN_1, Current_0, Current_1, Temperature_0, Temperature_1, Temperature_2, Temperature_3, CurrentR_0, CurrentR_1, Humidity FROM BHM_Data_InsulationParameters WHERE MonitorID == " + this.monitorID.ToString() + " ORDER BY ReadingDateTime";
                // transformerSideParametersQueryString = "SELECT ID, ReadingDateTime, SideNumber, 

                // Horizontal zooming and scrolling only for trend
                trendWinChartViewer.ZoomDirection = WinChartDirection.Horizontal;
                trendWinChartViewer.ScrollDirection = WinChartDirection.Horizontal;

                // Viewport is always unzoomed as y-axis is auto-scaled
                trendWinChartViewer.ViewPortTop = 0;
                trendWinChartViewer.ViewPortHeight = 1;

                // Horizontal and vertical zooming and scrolling for polar
                polarWinChartViewer.ZoomDirection = WinChartDirection.HorizontalVertical;
                polarWinChartViewer.ScrollDirection = WinChartDirection.HorizontalVertical;

                trendPointerRadioButton.Checked = true;
                polarPointerRadioButton.Checked = true;

                trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                exponentialTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                trendManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                gammaPolarRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                this.trendCursorLabel.Visible = false;
                this.polarHcursorGroup1Label.Visible = false;
                this.polarVcursorGroup1Label.Visible = false;
                this.polarHcursorGroup2Label.Visible = false;
                this.polarVcursorGroup2Label.Visible = false;

                trendRadHScrollBar.Maximum = 5000;
                polarRadHScrollBar.Maximum = 2000;
                polarRadVScrollBar.Maximum = 2000;

                gridDataIsBeingEdited = false;
                graphDataNeedsUpdating = false;
                trendChartHasMouseFocus = false;
                polarChartHasMouseFocus = false;

                AssignStringValuesToInterfaceObjects();

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.BHMDataViewer(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Gets the datbase data, sets preferences, invokes an initial display of the data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BHM_DataViewer_Load(object sender, EventArgs e)
        {
            try
            {
                BHM_Configuration currentConfiguration;

                Cursor.Current = Cursors.WaitCursor;
                /// Tripping this radio button causes all the data do load and sets up the graph limits
                this.trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                /// If you set this for trend, the polar will follow suit.
                // this.trendDisableCursorRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                //// If the grid view is going to be opened first, select the grid view control
                //// so that it will trap the arrow key events, rather than allowing the arrow key
                //// to be used to switch tab views.
                //if (dataDisplayRadPageView.SelectedPage == gridRadPageViewPage)
                //{
                //    bhm_DataReadingsRadGridView.Select();
                //}

                // try to force a cycling so the button is properly enabled, the cursor wasn't working
                // correctly until the user selected another button and then selected this one again.
                this.trendZoomInRadioButton.Checked = true;
                this.trendPointerRadioButton.Checked = true;

                InitializeInsulationParametersGridViewColumnHeaderText();
                InitializeTransformerSideParametersGridViewColumnHeaderText();
                InitializeGridViewColumnWidthVariables();

                PreferencesLoad();

                // Can update chart now
                hasFinishedInitialization = true;

                ResetDatabaseConnectionAndReloadAndDisplayData();

                using (MonitorInterfaceDB loadDB = new MonitorInterfaceDB(dbConnectionString))
                {
                    LoadWarningAndAlarmThresholdValues(loadDB);
                    this.monitorHierarchy = SetFormTitleToReflectDataSource(loadDB);
                    currentConfiguration = BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(this.monitorID, loadDB);
                }

                if((currentConfiguration!=null) && (currentConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    WriteInitialBalanceDataTabObjectValues(currentConfiguration);
                }

                trendWinChartViewer.updateViewPort(true, true);
                polarWinChartViewer.updateViewPort(true, true);

                DynamicsRadGroupBoxAssignContextMenuItems();
                SetupTrendContextMenu();
                SetupPolarContextMenu();
                Cursor.Current = Cursors.Default;
                SetToolTips();

                if (programBrand != ProgramBrand.DevelopmentVersion)
                {
                    createReportRadButton.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.BHMDataViewer_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Saves the current state of the BHM_DataViewer to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BHM_DataViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            GetAllGridViewColumnWidths();
            PreferencesSave();
        }

        private void SetToolTips()
        {
            toolTip1.AutoPopDelay = 3000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            toolTip1.SetToolTip(this.trendDataAgeRadGroupBox, loadDataByAgeToolTipText);
            toolTip1.SetToolTip(this.trendDataGroupSelectionRadGroupBox, selectTransformerSideToolTipText);
            toolTip1.SetToolTip(this.dynamicsRadGroupBox, useRightMouseToolTipText);
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                trendPointerRadioButtonText = pointerRadioButtonText;
                trendZoomInRadioButtonText = zoomInRadioButtonText;
                trendZoomOutRadioButtonText = zoomOutRadioButtonText;
                trendDataAgeRadGroupBoxText = dataAgeRadGroupBoxText;
                trendThreeMonthsRadRadioButtonText = threeMonthsRadRadioButtonText;
                trendSixMonthsRadRadioButtonText = sixMonthsRadRadioButtonText;
                trendOneYearRadRadioButtonText = oneYearRadRadioButtonText;
                trendAllDataRadRadioButtonText = allDataRadRadioButtonText;
                trendDataGroupSelectionRadGroupBoxText = dataGroupSelectionRadGroupBoxText;
                trendGroup1RadRadioButtonText = group1RadRadioButtonText;
                trendGroup2RadRadioButtonText = group2RadRadioButtonText;
                trendAllGroupsRadRadioButtonText = allGroupsRadRadioButtonText;
                trendAutoDrawRadRadioButtonText = autoDrawRadRadioButtonText;
                trendManualDrawRadRadioButtonText = manualDrawRadRadioButtonText;
                trendDrawRadGroupBoxText = drawRadGroupBoxText;
                trendDrawGraphsRadButtonText = drawGraphsRadButtonText;

                polarPointerRadioButtonText = pointerRadioButtonText;
                polarZoomInRadioButtonText = zoomInRadioButtonText;
                polarZoomOutRadioButtonText = zoomOutRadioButtonText;
                polarDataAgeRadGroupBoxText = dataAgeRadGroupBoxText;
                polarThreeMonthsRadRadioButtonText = threeMonthsRadRadioButtonText;
                polarSixMonthsRadRadioButtonText = sixMonthsRadRadioButtonText;
                polarOneYearRadRadioButtonText = oneYearRadRadioButtonText;
                polarAllDataRadRadioButtonText = allDataRadRadioButtonText;
                polarDataGroupSelectionRadGroupBoxText = dataGroupSelectionRadGroupBoxText;
                polarGroup1RadRadioButtonText = group1RadRadioButtonText;
                polarGroup2RadRadioButtonText = group2RadRadioButtonText;
                polarAllGroupsRadRadioButtonText = allGroupsRadRadioButtonText;
                polarDrawRadGroupBoxText = drawRadGroupBoxText;
                polarAutoDrawRadRadioButtonText = autoDrawRadRadioButtonText;
                polarManualDrawRadRadioButtonText = manualDrawRadRadioButtonText;
                polarDrawGraphsRadButtonText = drawGraphsRadButtonText;

                this.Text = bhmDataViewerInterfaceTitleText;

                trendRadPageViewPage.Text = trendRadPageViewPageText;
                trendPointerRadioButton.Text = trendPointerRadioButtonText;
                trendZoomInRadioButton.Text = trendZoomInRadioButtonText;
                trendZoomOutRadioButton.Text = trendZoomOutRadioButtonText;
                gammaRadCheckBox.Text = gammaRadCheckBoxText;
                gammaPhaseRadCheckBox.Text = gammaPhaseRadCheckBoxText;
                capacitanceARadCheckBox.Text = capacitanceARadCheckBoxText;
                capacitanceBRadCheckBox.Text = capacitanceBRadCheckBoxText;
                capacitanceCRadCheckBox.Text = capacitanceCRadCheckBoxText;
                tangentARadCheckBox.Text = tangentARadCheckBoxText;
                tangentBRadCheckBox.Text = tangentBRadCheckBoxText;
                tangentCRadCheckBox.Text = tangentCRadCheckBoxText;
                temperatureCoefficientRadCheckBox.Text = temperatureCoefficientRadCheckBoxText;
                gammaTrendRadCheckBox.Text = gammaTrendRadCheckBoxText;
                frequencyRadCheckBox.Text = frequencyRadCheckBoxText;
                bushingCurrentPhaseAARadCheckBox.Text = bushingCurrentPhaseAARadCheckBoxText;
                bushingCurrentPhaseABRadCheckBox.Text = bushingCurrentPhaseABRadCheckBoxText;
                bushingCurrentPhaseACRadCheckBox.Text = bushingCurrentPhaseACRadCheckBoxText;
                bushingCurrentARadCheckBox.Text = bushingCurrentARadCheckBoxText;
                bushingCurrentBRadCheckBox.Text = bushingCurrentBRadCheckBoxText;
                bushingCurrentCRadCheckBox.Text = bushingCurrentCRadCheckBoxText;
                phaseTemperatureCoefficientRadCheckBox.Text = phaseTemperatureCoefficientRadCheckBoxText;
                trendDataAgeRadGroupBox.Text = trendDataAgeRadGroupBoxText;
                trendThreeMonthsRadRadioButton.Text = trendThreeMonthsRadRadioButtonText;
                trendSixMonthsRadRadioButton.Text = trendSixMonthsRadRadioButtonText;
                trendOneYearRadRadioButton.Text = trendOneYearRadRadioButtonText;
                trendAllDataRadRadioButton.Text = trendAllDataRadRadioButtonText;
                trendDataGroupSelectionRadGroupBox.Text = trendDataGroupSelectionRadGroupBoxText;
                trendGroup1RadRadioButton.Text = trendGroup1RadRadioButtonText;
                trendGroup2RadRadioButton.Text = trendGroup2RadRadioButtonText;
                trendAllGroupsRadRadioButton.Text = trendAllGroupsRadRadioButtonText;
                trendDisplayOptionsRadGroupBox.Text = trendDisplayOptionsRadGroupBoxText;
                trendLineRadCheckBox.Text = trendLineRadCheckBoxText;
                linearTrendRadRadioButton.Text = linearTrendRadRadioButtonText;
                exponentialTrendRadRadioButton.Text = exponentialTrendRadRadioButtonText;
                normalizeDataRadCheckBox.Text = normalizeDataRadCheckBoxText;
                movingAverageRadCheckBox.Text = movingAverageRadCheckBoxText;
                dataItemsRadLabel.Text = dataItemsRadLabelText;
                trendDrawRadGroupBox.Text = trendDrawRadGroupBoxText;
                trendAutoDrawRadRadioButton.Text = trendAutoDrawRadRadioButtonText;
                trendManualDrawRadRadioButton.Text = trendManualDrawRadRadioButtonText;
                trendDrawGraphsRadButton.Text = trendDrawGraphsRadButtonText;
                dynamicsRadGroupBox.Text = dynamicsRadGroupBoxText;
                humidityRadCheckBox.Text = humidityRadCheckBoxText;
                moistureContentRadCheckBox.Text = moistureContentRadCheckBoxText;
                temp1RadCheckBox.Text = temp1RadCheckBoxText;
                temp2RadCheckBox.Text = temp2RadCheckBoxText;
                temp3RadCheckBox.Text = temp3RadCheckBoxText;
                temp4RadCheckBox.Text = temp4RadCheckBoxText;
                loadCurrent1RadCheckBox.Text = loadCurrent1RadCheckBoxText;
                loadCurrent2RadCheckBox.Text = loadCurrent2RadCheckBoxText;
                loadCurrent3RadCheckBox.Text = loadCurrent3RadCheckBoxText;
                voltage1RadCheckBox.Text = voltage1RadCheckBoxText;
                voltage2RadCheckBox.Text = voltage2RadCheckBoxText;
                voltage3RadCheckBox.Text = voltage3RadCheckBoxText;

                polarRadPageViewPage.Text = polarRadPageViewPageText;
                polarPointerRadioButton.Text = polarPointerRadioButtonText;
                polarZoomInRadioButton.Text = polarZoomInRadioButtonText;
                polarZoomOutRadioButton.Text = polarZoomOutRadioButtonText;
                showRadialLabelsRadCheckBox.Text = showRadialLabelsRadCheckBoxText;
                polarDataAgeRadGroupBox.Text = polarDataAgeRadGroupBoxText;
                polarThreeMonthsRadRadioButton.Text = polarThreeMonthsRadRadioButtonText;
                polarSixMonthsRadRadioButton.Text = polarSixMonthsRadRadioButtonText;
                polarOneYearRadRadioButton.Text = polarOneYearRadRadioButtonText;
                polarAllDataRadRadioButton.Text = polarAllDataRadRadioButtonText;
                polarDataGroupSelectionRadGroupBox.Text = polarDataGroupSelectionRadGroupBoxText;
                polarGroup1RadRadioButton.Text = polarGroup1RadRadioButtonText;
                polarGroup2RadRadioButton.Text = polarGroup2RadRadioButtonText;
                polarAllGroupsRadRadioButton.Text = polarAllGroupsRadRadioButtonText;
                polarDataDisplayChoiceRadGroupBox.Text = polarDataDisplayChoiceRadGroupBoxText;
                gammaPolarRadRadioButton.Text = gammaPolarRadRadioButtonText;
                temperatureCoefficientPolarRadRadioButton.Text = temperatureCoefficientPolarRadRadioButtonText;
                phaseSegmentPolarRadGroupBox.Text = phaseSegmentPolarRadGroupBoxText;
                capacitanceAPolarRadCheckBox.Text = capacitanceAPolarRadCheckBoxText;
                capacitanceBPolarRadCheckBox.Text = capacitanceBPolarRadCheckBoxText;
                capacitanceCPolarRadCheckBox.Text = capacitanceCPolarRadCheckBoxText;
                tangentAPolarRadCheckBox.Text = tangentAPolarRadCheckBoxText;
                tangentBPolarRadCheckBox.Text = tangentBPolarRadCheckBoxText;
                tangentCPolarRadCheckBox.Text = tangentCPolarRadCheckBoxText;
                polarDrawRadGroupBox.Text = polarDrawRadGroupBoxText;
                polarAutoDrawRadRadioButton.Text = polarAutoDrawRadRadioButtonText;
                polarManualDrawRadRadioButton.Text = polarManualDrawRadRadioButtonText;
                polarDrawGraphsRadButton.Text = polarDrawGraphsRadButtonText;

                diagnosticsRadPageViewPage.Text = diagnosticsRadPageViewPageText;
                groupOneDiagnosticsRadGroupBox.Text = groupOneDiagnosticsRadGroupBoxText;
                groupTwoDiagnosticsRadGroupBox.Text = groupTwoDiagnosticsRadGroupBoxText;
                diagnosticGroupOneTextRadLabel.Text = diagnosticRadLabelText;
                diagnosticGroupTwoTextRadLabel.Text = diagnosticRadLabelText;
                healthIndexGroupOneTextRadLabel.Text = healthIndexRadLabelText;
                healthIndexGroupTwoTextRadLabel.Text = healthIndexRadLabelText;

                gridRadPageViewPage.Text = gridRadPageViewPageText;
                gridExpandAllRadButton.Text = gridExpandAllRadButtonText;
                gridHideAllRadButton.Text = gridHideAllRadButtonText;
                gridDataEditRadButton.Text = gridDataEditRadButtonText;
                gridDataSaveChangesRadButton.Text = gridDataSaveChangesRadButtonText;
                gridDataCancelChangesRadButton.Text = gridDataCancelChangesRadButtonText;
                gridDataDeleteSelectedRadButton.Text = gridDataDeleteSelectedRadButtonText;

                createCorrelationGraphRadButton.Text = createCorrelationGraphRadButtonText;
                createReportRadButton.Text = createReportRadButtonText;
                saveDataToCsvRadButton.Text = saveDataToCsvRadButtonText;

                initialBalanceDataRadPageViewPage.Text = initialBalanceDataRadPageViewPageText;
                initialBalanceDataTabMeasurementDateRadLabel.Text = initialBalanceDataTabMeasurementDateRadLabelText;
                initialBalanceDataTabMeasurementDateValueRadLabel.Text = initialBalanceDataTabMeasurementDateValueRadLabelText;
                initialBalanceDataTabBushingSet1RadLabel.Text = initialBalanceDataTabBushingSet1RadLabelText;
                initialBalanceDataTabBushingSet2RadLabel.Text = initialBalanceDataTabBushingSet2RadLabelText;
                initialBalanceDataTabImbalanceRadLabel.Text = initialBalanceDataTabImbalanceRadLabelText;
                initialBalanceDataTabImbalancePhaseRadLabel.Text = initialBalanceDataTabImbalancePhaseRadLabelText;
                initialBalanceDataTabTemperatureRadLabel.Text = initialBalanceDataTabTemperatureRadLabelText;
                initialBalanceDataTabAmplitudePhaseARadLabel.Text = initialBalanceDataTabAmplitudePhaseARadLabelText;
                initialBalanceDataTabAmplitudePhaseBRadLabel.Text = initialBalanceDataTabAmplitudePhaseBRadLabelText;
                initialBalanceDataTabAmplitudePhaseCRadLabel.Text = initialBalanceDataTabAmplitudePhaseCRadLabelText;
                initialBalanceDataTabShiftPhaseARadLabel.Text = initialBalanceDataTabShiftPhaseARadLabelText;
                initialBalanceDataTabShiftPhaseBRadLabel.Text = initialBalanceDataTabShiftPhaseBRadLabelText;
                initialBalanceDataTabShiftPhaseCRadLabel.Text = initialBalanceDataTabShiftPhaseCRadLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                // inteface objects text strings
                bhmDataViewerInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTitleText", bhmDataViewerInterfaceTitleText, "", "", "");

                warningAndAlarmThresholdsNotAvailableText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerWarningAndAlarmThresholdsNotAvailableText", warningAndAlarmThresholdsNotAvailableText, htmlFontType, htmlStandardFontSize, "");

                gammaWarningThresholdLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaWarningThresholdLegendText", gammaWarningThresholdLegendText, "", "", "");
                gammaAlarmThresholdLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaAlarmThresholdLegendText", gammaAlarmThresholdLegendText, "", "", "");

                setOneText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSetOneText", setOneText, "", "", "");
                setTwoText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSetTwoText", setTwoText, "", "", "");

                // common strings
                pointerRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePointerRadioButtonText", pointerRadioButtonText, "", "", "");
                zoomInRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceZoomInRadioButtonText", zoomInRadioButtonText, "", "", "").Trim();
                zoomOutRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceZoomOutRadioButtonText", zoomOutRadioButtonText, "", "", "").Trim();
                dataAgeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataAgeRadGroupText", dataAgeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                threeMonthsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataAgeThreeMonthsRadRadioButtonText", threeMonthsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                sixMonthsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataAgeSixMonthsRadRadioButtonText", sixMonthsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                oneYearRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataAgeOneYearRadRadioButtonText", oneYearRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                allDataRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataAgeAllDataRadRadioButtonText", allDataRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                dataGroupSelectionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataGroupSelectionRadGroupBoxText", dataGroupSelectionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                group1RadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataGroupShownGroup1RadRadioButtonText", group1RadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                group2RadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataGroupShownGroup2RadRadioButtonText", group2RadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                allGroupsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataGroupShownAllGroupsRadRadioButtonText", allGroupsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                drawRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDrawRadGroupBoxText", drawRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                autoDrawRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceGraphMethodAutomaticDrawRadRadioButtonText", autoDrawRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                manualDrawRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceGraphMethodManualDrawRadRadioButtonText", manualDrawRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                drawGraphsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceGraphMethodDrawGraphsRadButtonText", drawGraphsRadButtonText, htmlFontType, htmlStandardFontSize, "");

                // individual object strings

                // trend tab
                trendRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTabTrendText", trendRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                gammaRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabGammaRadCheckBoxText", gammaRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                gammaPhaseRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabGammaPhaseRadCheckBoxText", gammaPhaseRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceARadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabCapacitanceARadCheckBoxText", capacitanceARadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceBRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabCapacitanceBRadCheckBoxText", capacitanceBRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceCRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabCapacitanceCRadCheckBoxText", capacitanceCRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentARadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabTangentARadCheckBoxText", tangentARadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentBRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabTangentBRadCheckBoxText", tangentBRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentCRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabTangentCRadCheckBoxText", tangentCRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                temperatureCoefficientRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabTemperatureCoefficientRadCheckBoxText", temperatureCoefficientRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                gammaTrendRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabGammaTrendRadCheckBoxText", gammaTrendRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                frequencyRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabFrequencyRadCheckBoxText", frequencyRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentPhaseAARadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentPhaseAARadCheckBoxText", bushingCurrentPhaseAARadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentPhaseABRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentPhaseABRadCheckBoxText", bushingCurrentPhaseABRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentPhaseACRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentPhaseACRadCheckBoxText", bushingCurrentPhaseACRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentARadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentARadCheckBoxText", bushingCurrentARadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentBRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentBRadCheckBoxText", bushingCurrentBRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                bushingCurrentCRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabBushingCurrentCRadCheckBoxText", bushingCurrentCRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                phaseTemperatureCoefficientRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTrendTabPhaseTemperatureCoefficientRadCheckBoxText", phaseTemperatureCoefficientRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                trendDisplayOptionsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTreandDisplayOptionsRadGroupBoxText", trendDisplayOptionsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                trendLineRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsTrendLineRadCheckBoxText", trendLineRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                linearTrendRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsLinearTrendRadRadioButtonText", linearTrendRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                exponentialTrendRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsExponentialTrendRadRadioButtonText", exponentialTrendRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                normalizeDataRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsNormalizeDataRadCheckBoxText", normalizeDataRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                movingAverageRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsMovingAverageRadCheckBoxText", movingAverageRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                dataItemsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDisplayOptionsDataItemsRadLabelText", dataItemsRadLabelText, htmlFontType, htmlStandardFontSize, "");

                // dynamics
                dynamicsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsRadGroupBoxText", dynamicsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                humidityRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsHumidityRadCheckBoxText", humidityRadCheckBoxText, "", "", "");
                moistureContentRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsMoistureContentRadCheckBoxText", moistureContentRadCheckBoxText, "", "", "");
                temp1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsCorrelatingTempRadCheckBoxText", temp1RadCheckBoxText, "", "", "");
                temp2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsTemp2RadCheckBoxText", temp2RadCheckBoxText, "", "", "");
                temp3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsTemp3RadCheckBoxText", temp3RadCheckBoxText, "", "", "");
                temp4RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsTemp4RadCheckBoxText", temp4RadCheckBoxText, "", "", "");
                loadCurrent1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsLoadCurrent1RadCheckBoxText", loadCurrent1RadCheckBoxText, "", "", "");
                loadCurrent2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsLoadCurrent2RadCheckBoxText", loadCurrent2RadCheckBoxText, "", "", "");
                loadCurrent3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsLoadCurrent3RadCheckBoxText", loadCurrent3RadCheckBoxText, "", "", "");
                voltage1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsVoltage1RadCheckBoxText", voltage1RadCheckBoxText, "", "", "");
                voltage2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsVoltage2RadCheckBoxText", voltage2RadCheckBoxText, "", "", "");
                voltage3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDynamicsVoltage3RadCheckBoxText", voltage3RadCheckBoxText, "", "", "");

                // polar tab
                polarRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTabPolarDiagramTabText", polarRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                showRadialLabelsRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabShowRadialLabelsRadCheckBoxText", showRadialLabelsRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                polarDataDisplayChoiceRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabPolarDataDisplayChoiceRadGroupBoxText", polarDataDisplayChoiceRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                gammaPolarRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabGammaPolarRadRadioButtonText", gammaPolarRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                temperatureCoefficientPolarRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabTemperatureCoefficientPolarRadRadioButtonText", temperatureCoefficientPolarRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                phaseSegmentPolarRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabPhaseSegmentPolarRadGroupBoxText", phaseSegmentPolarRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceAPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabCapacitanceAPolarRadCheckBoxText", capacitanceAPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceBPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabCapacitanceBPolarRadCheckBoxText", capacitanceBPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                capacitanceCPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabCapacitanceCPolarRadCheckBoxText", capacitanceCPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentAPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabTangentAPolarRadCheckBoxText", tangentAPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentBPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabTangentBPolarRadCheckBoxText", tangentBPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                tangentCPolarRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfacePolarTabTangentCPolarRadCheckBoxText", tangentCPolarRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");

                // diagnostics tab
                diagnosticsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDiagnosticsTabTitleText", diagnosticsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                groupOneDiagnosticsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceGroup1DiagnosticsRadGroupBoxText", groupOneDiagnosticsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                groupTwoDiagnosticsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceGroup2DiagnosticsRadGroupBoxText", groupTwoDiagnosticsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                diagnosticRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDiagnosticRadLabelText", diagnosticRadLabelText, htmlFontType, htmlStandardFontSize, "");
                healthIndexRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceHealthIndexRadLabelText", healthIndexRadLabelText, htmlFontType, htmlStandardFontSize, "");

                // grid tab
                gridRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceTabDataGridText", gridRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                gridExpandAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridExpandAllRadButtonText", gridExpandAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridHideAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridHideAllRadButtonText", gridHideAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataEditRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridDataEditRadButtonText", gridDataEditRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataSaveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridDataSaveChangesRadButtonText", gridDataSaveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataCancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridDataCancelChangesRadButtonText", gridDataCancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataDeleteSelectedRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceDataTabGridDataDeleteSelectedRadButtonText", gridDataDeleteSelectedRadButtonText, htmlFontType, htmlStandardFontSize, "");

                // internal strings
                noDataForSelectedDataAgeText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNoDataForSelectedDataAgeText", noDataForSelectedDataAgeText, htmlFontType, htmlStandardFontSize, "");
                saveChangesOrCancelBeforeSwitchingTabsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSaveChangesBeforeSwitchingTabsText", saveChangesOrCancelBeforeSwitchingTabsText, htmlFontType, htmlStandardFontSize, "");
                addText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerAddText", addText, "", "", "");
                multiplyText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerMultiplyText", multiplyText, "", "", "");
                copyGraphText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerContextMenuCopyGraphText", copyGraphText, htmlFontType, htmlStandardFontSize, "");
                loadDataByAgeToolTipText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerLoadDataByAgeToolTipText", loadDataByAgeToolTipText, htmlFontType, htmlStandardFontSize, "");
                selectTransformerSideToolTipText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSelectTransformerSideToolTipText", selectTransformerSideToolTipText, htmlFontType, htmlStandardFontSize, "");
                useRightMouseToolTipText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerUseRightMouseToolTipText", useRightMouseToolTipText, htmlFontType, htmlStandardFontSize, "");
                readingDateInsulationParametersGridText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerReadingDateInsulationParametersGridText", readingDateInsulationParametersGridText, htmlFontType, htmlStandardFontSize, "");


                percentUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPercentUnitsText", percentUnitsText, "", "", "");
                degreesAngularUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDegreesAngularUnitsText", degreesAngularUnitsText, "", "", "");
                // unitlessUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerUnitlessUnitsText", unitlessUnitsText, "", "", "");
                percentPerYearUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPercentPerYearUnitsText", percentPerYearUnitsText, "", "", "");
                degreesCentigradeUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDegreesCentigradeUnitsText", degreesCentigradeUnitsText, "", "", "");
                milliAmpsUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerMilliAmpsUnitsText", milliAmpsUnitsText, "", "", "");
                picoFaradsUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPicoFaradsUnitsText", picoFaradsUnitsText, "", "", "");
                hertzUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerHertzUnitsText", hertzUnitsText, "", "", "");
                kiloVoltsUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerKiloVoltsUnitsText", kiloVoltsUnitsText, "", "", "");
                percentFLUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPercentFLUnitsText", percentFLUnitsText, "", "", "");
                ampsUnitsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerAmpsUnitsText", ampsUnitsText, "", "", "");

                gammaLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaLegendText", gammaLegendText, "", "", "");
                gammaPhaseLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaPhaseLegendText", gammaPhaseLegendText, "", "", "");
                gammaTrendLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaTrendLegendText", gammaTrendLegendText, "", "", "");
                tempCoeffLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTempCoeffLegendText", tempCoeffLegendText, "", "", "");
                phaseTempCoeffLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhaseTempCoeffLegendText", phaseTempCoeffLegendText, "", "", "");
                bushingCur1LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCur1LegendText", bushingCur1LegendText, "", "", "");
                bushingCur2LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCur2LegendText", bushingCur2LegendText, "", "", "");
                bushingCur3LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCur3LegendText", bushingCur3LegendText, "", "", "");
                bushingCurPhase11LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCurPhase11LegendText", bushingCurPhase11LegendText, "", "", "");
                bushingCurPhase12LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCurPhase12LegendText", bushingCurPhase12LegendText, "", "", "");
                bushingCurPhase13LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerBushingCurPhase13LegendText", bushingCurPhase13LegendText, "", "", "");
                capacitance1LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerCapacitance1LegendText", capacitance1LegendText, "", "", "");
                capacitance2LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerCapacitance2LegendText", capacitance2LegendText, "", "", "");
                capacitance3LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerCapacitance3LegendText", capacitance3LegendText, "", "", "");
                tangent1LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTangent1LegendText", tangent1LegendText, "", "", "");
                tangent2LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTangent2LegendText", tangent2LegendText, "", "", "");
                tangent3LegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTangent3LegendText", tangent3LegendText, "", "", "");
                frequencyLegendText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerFrequencyLegendText", frequencyLegendText, "", "", "");

                idGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerIdGridLabelText", idGridLabelText, htmlFontType, htmlStandardFontSize, "");
                dataRootIdGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDataRootIdGridLabelText", dataRootIdGridLabelText, htmlFontType, htmlStandardFontSize, "");
                sideNumberGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSideNumberGridLabelText", sideNumberGridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourceAmplitude0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSourceAmplitude0GridLabelText", sourceAmplitude0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourceAmplitude1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSourceAmplitude1GridLabelText", sourceAmplitude1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourceAmplitude2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewersourceAmplitude2GridLabelText", sourceAmplitude2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                phaseAmplitude0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhaseAmplitude0GridLabelText", phaseAmplitude0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                phaseAmplitude1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhaseAmplitude1GridLabelText", phaseAmplitude1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                phaseAmplitude2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhaseAmplitude2GridLabelText", phaseAmplitude2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                gammaGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaGridLabelText", gammaGridLabelText, htmlFontType, htmlStandardFontSize, "");
                gammaPhaseGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerGammaPhaseGridLabelText", gammaPhaseGridLabelText, htmlFontType, htmlStandardFontSize, "");
                ktGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerKtGridLabelText", ktGridLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmStatusGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerAlarmStatusGridLabelText", alarmStatusGridLabelText, htmlFontType, htmlStandardFontSize, "");
                remainingLife0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerRemainingLife0GridLabelText", remainingLife0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                remainingLife1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerRemainingLife1GridLabelText", remainingLife1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                remainingLife2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerRemainingLife2GridLabelText", remainingLife2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                defectCodeGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDefectCodeGridLabelText", defectCodeGridLabelText, htmlFontType, htmlStandardFontSize, "");
                chPhaseShiftGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerChPhaseShiftGridLabelText", chPhaseShiftGridLabelText, htmlFontType, htmlStandardFontSize, "");
                trendGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTrendGridLabelText", trendGridLabelText, htmlFontType, htmlStandardFontSize, "");
                ktPhaseGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerKtPhaseGridLabelText", ktPhaseGridLabelText, htmlFontType, htmlStandardFontSize, "");
                signalPhase0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSignalPhase0GridLabelText", signalPhase0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                signalPhase1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSignalPhase1GridLabelText", signalPhase1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                signalPhase2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSignalPhase2GridLabelText", signalPhase2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourcePhase0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSourcePhase0GridLabelText", sourcePhase0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourcePhase1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSourcePhase1GridLabelText", sourcePhase1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                sourcePhase2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerSourcePhase2GridLabelText", sourcePhase2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                frequencyGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerFrequencyGridLabelText", frequencyGridLabelText, htmlFontType, htmlStandardFontSize, "");
                temperatureGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTemperatureGridLabelText", temperatureGridLabelText, htmlFontType, htmlStandardFontSize, "");
                c0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerC0GridLabelText", c0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                c1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerC1GridLabelText", c1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                c2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerC2GridLabelText", c2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                tg0GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTg0GridLabelText", tg0GridLabelText, htmlFontType, htmlStandardFontSize, "");
                tg1GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTg1GridLabelText", tg1GridLabelText, htmlFontType, htmlStandardFontSize, "");
                tg2GridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTg2GridLabelText", tg2GridLabelText, htmlFontType, htmlStandardFontSize, "");
                externalSyncShiftGridLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerExternalSyncShiftGridLabelText", externalSyncShiftGridLabelText, htmlFontType, htmlStandardFontSize, "");

                shortingOfCondenserFoilsPhaseAText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerShortingOfCondenserFoilsPhaseAText", shortingOfCondenserFoilsPhaseAText, htmlFontType, htmlStandardFontSize, "");
                shortingOfCondenserFoilsPhaseBText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerShortingOfCondenserFoilsPhaseBText", shortingOfCondenserFoilsPhaseBText, htmlFontType, htmlStandardFontSize, "");
                shortingOfCondenserFoilsPhaseCText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerShortingOfCondenserFoilsPhaseCText", shortingOfCondenserFoilsPhaseCText, htmlFontType, htmlStandardFontSize, "");
                powerFactorIncreasingPhaseAText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPowerFactorIncreasingPhaseAText", powerFactorIncreasingPhaseAText, htmlFontType, htmlStandardFontSize, "");
                powerFactorIncreasingPhaseBText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPowerFactorIncreasingPhaseBText", powerFactorIncreasingPhaseBText, htmlFontType, htmlStandardFontSize, "");
                powerFactorIncreasingPhaseCText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPowerFactorIncreasingPhaseCText", powerFactorIncreasingPhaseCText, htmlFontType, htmlStandardFontSize, "");
                negativeTemperatureDependencyOrPowerFactorPhaseAText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNegativeTemperatureDependencyOrPowerFactorPhaseAText", negativeTemperatureDependencyOrPowerFactorPhaseAText, htmlFontType, htmlStandardFontSize, "");
                negativeTemperatureDependencyOrPowerFactorPhaseBText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNegativeTemperatureDependencyOrPowerFactorPhaseBText", negativeTemperatureDependencyOrPowerFactorPhaseBText, htmlFontType, htmlStandardFontSize, "");
                negativeTemperatureDependencyOrPowerFactorPhaseCText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNegativeTemperatureDependencyOrPowerFactorPhaseCText", negativeTemperatureDependencyOrPowerFactorPhaseCText, htmlFontType, htmlStandardFontSize, "");
                twoBushingOrMultipleDefectsOccurringText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerTwoBushingOrMultipleDefectsOccurringText", twoBushingOrMultipleDefectsOccurringText, htmlFontType, htmlStandardFontSize, "");
                noDiagnosticsAvailableText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNoDiagnosticsAvailableText", noDiagnosticsAvailableText, htmlFontType, htmlStandardFontSize, "");
                noDataAvailableText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerNoDataAvailableText", noDataAvailableText, htmlFontType, htmlStandardFontSize, "");
                diagnosticsAreNormalText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDiagnosticsAreNormalText", diagnosticsAreNormalText, htmlFontType, htmlStandardFontSize, "");

                failedToReadAnyDataToSaveToCsvFile = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerFailedToReadAnyDataToSaveToCsvFileText", failedToReadAnyDataToSaveToCsvFile, htmlFontType, htmlStandardFontSize, "");

                dynamicsCorrelatedWithText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDynamicsCorrelatedWithText", dynamicsCorrelatedWithText, "", "", "");
                dataComparisonForReadingTakenOnText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerDataComparisonForReadingTakenOnText", dataComparisonForReadingTakenOnText, "", "", "");

                phasesSwappedSetOneText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhasesSwappedSetOneText", phasesSwappedSetOneText, "", "", "");
                phasesSwappedSetTwoText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhasesSwappedSetTwoText", phasesSwappedSetTwoText, "", "", "");
                phasesSwappedBothSetsText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerPhasesSwappedBothSetsText", phasesSwappedBothSetsText, "", "", "");

                createCorrelationGraphRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceCreateCorrelationGraphRadButtonText", createCorrelationGraphRadButtonText, "", "", "");
                createReportRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceCreateReportRadButtonText", createReportRadButtonText, "", "", "");

                saveDataToCsvRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMDataviewerInterfaceDataGridTabSaveDataToCsvRadButtonText", saveDataToCsvRadButtonText, "", "", "");

                initialBalanceDataRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMDataviewerInterfaceInitialBalanceDataRadPageViewPageText", initialBalanceDataRadPageViewPageText, "", "", "");
                initialBalanceDataTabMeasurementDateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabMeasurementDateRadLabelText", initialBalanceDataTabMeasurementDateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabBushingSet1RadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabBushingSet1RadLabelText", initialBalanceDataTabBushingSet1RadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabBushingSet2RadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabBushingSet2RadLabelText", initialBalanceDataTabBushingSet2RadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabImbalanceRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabImbalanceRadLabelText", initialBalanceDataTabImbalanceRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabImbalancePhaseRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabImbalancePhaseRadLabelText", initialBalanceDataTabImbalancePhaseRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabTemperatureRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabTemperatureRadLabelText", initialBalanceDataTabTemperatureRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabAmplitudePhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabAmplitudePhaseARadLabelText", initialBalanceDataTabAmplitudePhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabAmplitudePhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabAmplitudePhaseBRadLabelText", initialBalanceDataTabAmplitudePhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabAmplitudePhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabAmplitudePhaseCRadLabelText", initialBalanceDataTabAmplitudePhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabShiftPhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabShiftPhaseARadLabelText", initialBalanceDataTabShiftPhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabShiftPhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabShiftPhaseBRadLabelText", initialBalanceDataTabShiftPhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
                initialBalanceDataTabShiftPhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMDataViewerInterfaceInitialBalanceDataTabShiftPhaseCRadLabelText", initialBalanceDataTabShiftPhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        ///  Capture the arrow keys to use for cursor movement in the graphs
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            try
            {
                if ((this.cursorIsEnabled) &&
                    (dataDisplayRadPageView.SelectedPage != gridRadPageViewPage))
                {
                    const int WM_KEYDOWN = 0x100;
                    const int WM_SYSKEYDOWN = 0x104;

                    if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
                    {

                        //if ((dataDisplayRadPageView.SelectedPage == trendRadPageViewPage) ||
                        //    (dataDisplayRadPageView.SelectedPage == polarRadPageViewPage))
                        //{
                        switch (keyData)
                        {
                            case Keys.Left:
                                MoveXYcursorLeft();
                                return true;

                            case Keys.Right:
                                MoveXYcursorRight();
                                return true;
                        }
                        //}
                        //else
                        //{
                        //    /// not sure anything is necessary here, as I have set up events so that
                        //    /// when the user selects the grid view tab, focus is set to the grid
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.ProcessCmdKey(ref Message, Keys)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// Makes sure the cursor can move left on the xy chart, then moves all cursors to point
        /// to the data associated with the DateTime immediately earlier than the current cursor 
        /// position
        /// </summary>
        private void MoveXYcursorLeft()
        {
            try
            {
                //int newScrollValue;
                int newChartXval;
                //int scrollLength;
                if (this.cursorDateTimeAsDoubleIndex > 0)
                {
                    this.oldCursorXval = this.cursorXval;
                    this.cursorDateTimeAsDoubleIndex--;
                    this.cursorXval = this.allDateTimesAsDouble[this.cursorDateTimeAsDoubleIndex];
                    newChartXval = trendXYChart.getXCoor(this.cursorXval);
                    //if (newChartXval < this.trendGraphOffsetX)
                    if (newChartXval < this.trendGraphCurrentOffsetX)
                    {
                        ScrollXYcursorIntoView(newChartXval);
                        //// scrollLength = (((this.trendGraphXStart - newChartXval + this.trendGraphOffsetX) / this.trendGraphWidth) * trendRadHScrollBar.LargeChange) + (trendRadHScrollBar.LargeChange / 2) ;
                        // scrollLength = (((this.trendGraphOffsetX - newChartXval) / this.trendGraphWidth) * trendRadHScrollBar.LargeChange) + (trendRadHScrollBar.LargeChange / 2);
                        // newScrollValue = trendRadHScrollBar.Value - scrollLength;
                        // //newScrollValue = trendRadHScrollBar.Value - trendRadHScrollBar.SmallChange;
                        // trendRadHScrollBar.Value = Math.Max(newScrollValue, trendRadHScrollBar.Minimum);
                    }
                    else
                    {
                        UpdateAllCursorPositions();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.MoveXYcursorLeft()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Makes sure the cursor can move right on the xy chart, then moves all cursors to point
        /// to the data associated with the DateTime immediately after the current cursor 
        /// position
        /// </summary>
        private void MoveXYcursorRight()
        {
            try
            {
                int newChartXval;
                if (this.cursorDateTimeAsDoubleIndex < (allDateTimesAsDouble.Length - 1))
                {
                    this.oldCursorXval = this.cursorXval;
                    this.cursorDateTimeAsDoubleIndex++;
                    this.cursorXval = this.allDateTimesAsDouble[this.cursorDateTimeAsDoubleIndex];
                    newChartXval = trendXYChart.getXCoor(this.cursorXval);

                    if (newChartXval > (this.trendGraphCurrentOffsetX + this.trendGraphWidth))
                    {
                        ScrollXYcursorIntoView(newChartXval);
                        //newScrollValue = trendRadHScrollBar.Value + trendRadHScrollBar.SmallChange;
                        //trendRadHScrollBar.Value = Math.Min(newScrollValue, trendRadHScrollBar.Maximum);
                    }
                    else
                    {
                        UpdateAllCursorPositions();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.MoveXYcursorRight()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
      
        /// <summary>
        /// Scrolls the trend chart so that the cursor is in view if the cursor has moved off the screen.
        /// </summary>
        /// <param name="newChartXval"></param>
        private void ScrollXYcursorIntoView(int newChartXval)
        {
            try
            {
                int newScrollValue;
                int scrollLength;
                double distanceToScrollInTermsOfChartWidth;
                if (this.cursorDateTimeAsDoubleIndex > -1)
                {
                    {
                        if (newChartXval < this.trendGraphCurrentOffsetX)
                        {
                            // this gets the fraction of the total current screen width spanned by the interval betwee the old and new cursor positions
                            distanceToScrollInTermsOfChartWidth = (this.oldCursorXval - this.cursorXval) / this.dateRange / this.trendWinChartViewer.ViewPortWidth;

                            scrollLength = ((int)Math.Ceiling(distanceToScrollInTermsOfChartWidth * 2.0)) * (trendRadHScrollBar.LargeChange / 2);

                            newScrollValue = trendRadHScrollBar.Value - scrollLength;
                            trendRadHScrollBar.Value = Math.Max(newScrollValue, trendRadHScrollBar.Minimum);
                            UpdateTrendImageMap(this.trendWinChartViewer);
                        }

                        if (newChartXval > (this.trendGraphWidth + this.trendGraphCurrentOffsetX))
                        {
                            // this gets the fraction of the total current screen width spanned by the interval betwee the old and new cursor positions
                            distanceToScrollInTermsOfChartWidth = (this.cursorXval - this.oldCursorXval) / this.dateRange / this.trendWinChartViewer.ViewPortWidth;

                            scrollLength = ((int)Math.Ceiling(distanceToScrollInTermsOfChartWidth * 2.0)) * (trendRadHScrollBar.LargeChange / 2);

                            newScrollValue = trendRadHScrollBar.Value + scrollLength;
                            trendRadHScrollBar.Value = Math.Min(newScrollValue, trendRadHScrollBar.Maximum);
                            UpdateTrendImageMap(this.trendWinChartViewer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.ScrollXYcursorIntoView(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void TrendUpdateCursorPosition()
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    if (this.trendXYChart != null)
                    {
                        int xCoordinateInPixels = this.trendXYChart.getXCoor(this.cursorXval);
                        if (TrendCursorIsInRange(xCoordinateInPixels))
                        {
                            this.trendCursorLabel.Visible = true;
                            this.trendCursorLabel.Location = new Point(this.trendGraphXStart + xCoordinateInPixels, this.trendGraphYStart);
                            this.trendCursorLabel.Height = trendGraphHeight;
                            this.trendCursorLabel.Width = 1;
                        }
                        else
                        {
                            this.trendCursorLabel.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.TrendUpdateCursorPosition()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool TrendCursorIsInRange(int proposedNewXcoordinateInPixels)
        {
            return !((proposedNewXcoordinateInPixels < this.trendGraphCurrentOffsetX) ||
                (proposedNewXcoordinateInPixels > (this.trendGraphWidth + this.trendGraphCurrentOffsetX)));
        }

        private void UpdateAllCursorPositions()
        {
            try
            {
                TrendUpdateCursorPosition();
                PolarUpdateCursorPosition();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.UpdateAllCursorPositions()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        private void HidePhaseSwapText()
        {
            try
            {
                this.trendPhasesSwappedRadLabel.Visible = false;
                this.polarPhasesSwappedRadLabel.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.HidePhaseSwapText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ShowPhaseSwapText()
        {
            try
            {
                this.trendPhasesSwappedRadLabel.Visible = true;
                this.polarPhasesSwappedRadLabel.Visible = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.ShowPhaseSwapText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// this uses the "new" database tables as a data source
        /// </summary>
        private void LoadData(DateTime cutoffDateTime)
        {
            try
            {
                int sideOneCount = 0;
                int sideTwoCount = 0;
                string bitValuesAsString = string.Empty;
                Dictionary<int, bool> setOneDefectCodeAsZeroIndexedBitEncodedDictionary = null;
                Dictionary<int, bool> setTwoDefectCodeAsZeroIndexedBitEncodedDictionary = null;

                bool nonZeroGammaSet1Found=false;
                bool nonZeroGammaSet2Found=false;

                HidePhaseSwapText();

                /// Ignore the download date for now, just get everything
                if ((this.monitorID != null) && (this.monitorID != Guid.Empty))
                {
                    List<BHM_SingleDataReading> dataReadings;
                    using (MonitorInterfaceDB localConnection = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        dataReadings = DataConversion.BHM_GetAllDataForOneMonitor(this.monitorID, cutoffDateTime, localConnection);
                    }

                    if (dataReadings.Count > 0)
                    {
                        /// we need to be sure whether both groups always have a data element at the same place on the x axis.  If they don't, we 
                        /// cannot display both groups of data at the same time.
                        this.groupOneAndGroupTwoDataHaveExactDateMatching = true;

                        /// The old loading code used a method that compared the total counts for each data group to determine
                        /// whether each date had a data point for each group.  That was a minor hack, and we can do better now
                        /// by actually noting if either group has a point missing for a given date.
                        foreach (BHM_SingleDataReading singleDataReading in dataReadings)
                        {
                            if ((singleDataReading.transformerSideParametersSideOne != null) && (singleDataReading.transformerSideParametersSideTwo != null))
                            {
                                sideOneCount++;
                                sideTwoCount++;
                            }
                            else
                            {
                                this.groupOneAndGroupTwoDataHaveExactDateMatching = false;
                                if (singleDataReading.transformerSideParametersSideOne != null)
                                {
                                    sideOneCount++;
                                }
                                if (singleDataReading.transformerSideParametersSideTwo != null)
                                {
                                    sideTwoCount++;
                                }
                            }
                        }

                        this.totalReadingsCommonData = dataReadings.Count;
                        this.totalReadingsGroup1 = sideOneCount;
                        this.totalReadingsGroup2 = sideTwoCount;

                        if (dataReadings[0].transformerSideParametersSideOne != null)
                        {
                            setOneDefectCodeAsZeroIndexedBitEncodedDictionary = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(ConversionMethods.Int32ToByte(dataReadings[0].transformerSideParametersSideOne.DefectCode));
                        }
                        if (dataReadings[0].transformerSideParametersSideTwo != null)
                        {
                            setTwoDefectCodeAsZeroIndexedBitEncodedDictionary = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(ConversionMethods.Int32ToByte(dataReadings[0].transformerSideParametersSideTwo.DefectCode));
                        }

                        if ((setOneDefectCodeAsZeroIndexedBitEncodedDictionary != null) && (setOneDefectCodeAsZeroIndexedBitEncodedDictionary[0]))
                        {
                            this.setOnePhaseIsSwapped = true;
                        }
                        else
                        {
                            this.setOnePhaseIsSwapped = false;
                        }
                        if ((setTwoDefectCodeAsZeroIndexedBitEncodedDictionary != null) && (setTwoDefectCodeAsZeroIndexedBitEncodedDictionary[0]))
                        {
                            this.setTwoPhaseIsSwapped = true;
                        }
                        else
                        {
                            this.setTwoPhaseIsSwapped = false;
                        }

                        InitializeLocalDataArrays();

                        foreach (BHM_SingleDataReading singleDataReading in dataReadings)
                        {
                            this.commonData.AddDataReading(singleDataReading);
                            if (singleDataReading.transformerSideParametersSideOne != null)
                            {
                                if (singleDataReading.transformerSideParametersSideOne.Gamma > 1.0e-6)
                                {
                                    nonZeroGammaSet1Found = true;
                                }
                                this.dbDataGroup1.AddDataReading(singleDataReading, 1);
                            }
                            if (singleDataReading.transformerSideParametersSideTwo != null)
                            {
                                if (singleDataReading.transformerSideParametersSideTwo.Gamma > 1.0e-6)
                                {
                                    nonZeroGammaSet2Found = true;
                                }
                                this.dbDataGroup2.AddDataReading(singleDataReading, 2);
                            }
                        }

                        /// This should never happen unless one inputs crap data and doesn't configure the monitor correctly
                        if (!nonZeroGammaSet1Found)
                        {
                            this.totalReadingsGroup1 = 0;
                            this.dbDataGroup1 = new BHM_DataStructure();
                        }
                        /// This could happen with older database data using previous versions of Athena, but now I don't save empty data to the db, so this
                        /// shouldn't happen with any new users from here on
                        if (!nonZeroGammaSet2Found)
                        {
                            this.totalReadingsGroup2 = 0;
                            this.dbDataGroup2 = new BHM_DataStructure();
                        }

                        this.trendChartNeedsUpdating = true;
                        this.polarChartNeedsUpdating = true;

                        //CalculateDiagnosticsAndPostTheResults();
                    }
                    else
                    {
                        RadMessageBox.Show(this, noDataForSelectedDataAgeText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.LoadData(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Invokes the class initializer for the in-memory storage of the group 1 and group 2 data
        /// </summary>
        private void InitializeLocalDataArrays()
        {
            try
            {
                this.commonData.AllocateAllArrays(this.totalReadingsCommonData);
                if (this.totalReadingsGroup1 > 0)
                {
                    this.dbDataGroup1.AllocateAllArrays(this.totalReadingsGroup1);
                }
                if (this.totalReadingsGroup2 > 0)
                {
                    this.dbDataGroup2.AllocateAllArrays(this.totalReadingsGroup2);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.InitializeLocalDataArrays()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// The ViewPortChanged event handler. This event occurs when the user changes the 
        /// WinChartViewer view port by dragging scrolling, or by zoom in/out, or the 
        /// WinChartViewer.updateViewPort method is being called.
        /// </summary>
        private void trendWinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                // Compute the view port start date and duration
                DateTime currentStartDate = this.minimumDateOfDataSet.AddSeconds(Math.Round(trendWinChartViewer.ViewPortLeft * dateRange));
                this.currentDuration = Math.Round(trendWinChartViewer.ViewPortWidth * dateRange);

                // Synchronize the horizontal scroll bar with the WinChartViewer
                trendRadHScrollBar.Enabled = trendWinChartViewer.ViewPortWidth < 1;
                trendRadHScrollBar.LargeChange = (int)Math.Ceiling(trendWinChartViewer.ViewPortWidth *
                    (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum));
                trendRadHScrollBar.SmallChange = (int)Math.Ceiling(trendRadHScrollBar.LargeChange * 0.1);
                trendRadHScrollBar.Value = (int)Math.Round(trendWinChartViewer.ViewPortLeft *
                    (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum)) + trendRadHScrollBar.Minimum;

                // Synchronize the vertical scroll bar with the WinChartViewer
                //trendRadVScrollBar.Enabled = trendWinChartViewer.ViewPortHeight < 1;
                //trendRadVScrollBar.LargeChange = (int)Math.Ceiling(trendWinChartViewer.ViewPortHeight *
                //    (trendRadVScrollBar.Maximum - trendRadVScrollBar.Minimum));
                //trendRadVScrollBar.SmallChange = (int)Math.Ceiling(trendRadVScrollBar.LargeChange * 0.1);
                //trendRadVScrollBar.Value = (int)Math.Round(trendWinChartViewer.ViewPortTop *
                //    (trendRadVScrollBar.Maximum - trendRadVScrollBar.Minimum)) + trendRadVScrollBar.Minimum;

                // Update chart and image map if necessary
                if (e.NeedUpdateChart)
                {
                    DrawTrendChart(this.trendWinChartViewer, true);
                }
                if (e.NeedUpdateImageMap)
                {
                    UpdateTrendImageMap(trendWinChartViewer);
                }
                if (this.cursorIsEnabled)
                {
                    TrendUpdateCursorPosition();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendWinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Update the image map used on the chart.
        /// </summary>
        private void UpdateTrendImageMap(WinChartViewer viewer)
        {
            try
            {
                // Include tool tip for the chart
                if ((viewer != null) && (viewer.Chart != null) && (viewer.ImageMap == null))
                {
                    viewer.ImageMap = trendWinChartViewer.Chart.getHTMLImageMap("clickable", "",
                        "title='[{dataSetName}] {x|mmm dd, yyyy hh:nn:ss}  val = {value|4}'");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.UpdateTrendImageMap(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Set the chart mouse mode to pointer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trendPointerRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendPointerRadioButton.Checked)
                {
                    if (!polarPointerRadioButton.Checked)
                    {
                        polarPointerRadioButton.Checked = true;
                    }
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                    trendPointerRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendPointerRadioButton.BackColor = trendPointerRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendPointerRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Set the chart mouse mode to zoom in.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trendZoomInRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendZoomInRadioButton.Checked)
                {
                    if (!polarZoomInRadioButton.Checked)
                    {
                        polarZoomInRadioButton.Checked = true;
                    }
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                    trendZoomInRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendZoomInRadioButton.BackColor = trendZoomInRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.zoomInRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Set the chart mouse mode to zoom out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trendZoomOutRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendZoomOutRadioButton.Checked)
                {
                    if (!polarZoomOutRadioButton.Checked)
                    {
                        polarZoomOutRadioButton.Checked = true;
                    }
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                    trendZoomOutRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendZoomOutRadioButton.BackColor = trendZoomOutRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.zoomOutRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the data load date to three months past
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void threeMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ResetDatabaseConnectionAndReloadAndDisplayData();
                    if (polarThreeMonthsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarThreeMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.threeMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the data load date to six months past
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void sixMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ResetDatabaseConnectionAndReloadAndDisplayData();
                    if (polarSixMonthsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarSixMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.sixMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the data load date to one year past
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void oneYearRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ResetDatabaseConnectionAndReloadAndDisplayData();
                    if (polarOneYearRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarOneYearRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.oneYearRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the data load to get all data in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void allDataRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendAllDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    ResetDatabaseConnectionAndReloadAndDisplayData();
                    if (polarAllDataRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.allDataRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Selects the display of group 1 data only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void group1RadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetGroupSelectDataDisplayAndDrawChart();
                    if (polarGroup1RadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.group1RadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Selects the display of group 2 data only
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void group2RadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetGroupSelectDataDisplayAndDrawChart();
                    if (polarGroup2RadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.group2RadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Selects the display of both groups of data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void allGroupsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetGroupSelectDataDisplayAndDrawChart();
                    if (polarAllGroupsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarAllGroupsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.allGroupsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets up the group selection for the trend chart data and draws the chart if automatic Draw is on
        /// </summary>
        private void SetGroupSelectDataDisplayAndDrawChart()
        {
            try
            {
                SetDataDisplayGroupInformation();
                SetChartDateLimits();
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawTrendChart(this.trendWinChartViewer, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetGroupSelectDataDisplayAndDrawChart()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets the enable status of the radio buttons and tries to duplicate the previous radio
        /// button selection, if possible.  Usually called after loading the data.
        /// </summary>
        private void SetGroupRadioButtonStates()
        {
            try
            {
                if (this.totalReadingsGroup1 > 0)
                {
                    this.trendGroup1RadRadioButton.Enabled = true;
                    this.polarGroup1RadRadioButton.Enabled = true;
                }
                else
                {
                    this.trendGroup1RadRadioButton.Enabled = false;
                    this.polarGroup1RadRadioButton.Enabled = false;
                }

                if (this.totalReadingsGroup2 > 0)
                {
                    this.trendGroup2RadRadioButton.Enabled = true;
                    this.polarGroup2RadRadioButton.Enabled = true;
                }
                else
                {
                    this.trendGroup2RadRadioButton.Enabled = false;
                    this.polarGroup2RadRadioButton.Enabled = false;
                }

                // much better than the old method, the newer way of loading data allows an easy check to see if the data
                // always line up as far as the dates go.
                if (this.groupOneAndGroupTwoDataHaveExactDateMatching)
                {
                    this.trendAllGroupsRadRadioButton.Enabled = true;
                    this.polarAllGroupsRadRadioButton.Enabled = true;
                }
                else
                {
                    this.trendAllGroupsRadRadioButton.Enabled = false;
                    this.polarAllGroupsRadRadioButton.Enabled = false;
                }

                /// Here we will attempt to duplicate the old selection for active groups by using the 
                /// state values set before the last data load, which have not been changed yet, as they
                /// only change state when the ToggleState handler is called
                if (this.trendAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (this.trendGroup1RadRadioButton.Enabled && this.trendGroup2RadRadioButton.Enabled && this.trendAllDataRadRadioButton.Enabled)
                    {
                        this.trendAllGroupsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (this.trendGroup1RadRadioButton.Enabled)
                    {
                        this.trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (this.trendGroup2RadRadioButton.Enabled)
                    {
                        this.trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        /// this should only execute if the data set is empty
                        this.trendAllGroupsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    }
                }
                else if (this.trendGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (this.trendGroup1RadRadioButton.Enabled)
                    {
                        this.trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (this.trendGroup2RadRadioButton.Enabled)
                    {
                        this.trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        /// this should only execute if the data set is empty
                        this.trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    }
                }
                else if (this.trendGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (this.trendGroup2RadRadioButton.Enabled)
                    {
                        this.trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (this.trendGroup1RadRadioButton.Enabled)
                    {
                        this.trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        /// this should only execute if the data set is empty
                        this.trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetGroupRadioButtonStates()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Sets which of Groups 1 and 2 are enabled for display, based on the ToggleState of
        /// the associated radio buttons.  Called by the ToggleState handlers for the radio buttions.
        /// </summary>
        private void SetDataDisplayGroupInformation()
        {
            this.group1IsEnabled = false;
            this.group2IsEnabled = false;

            HidePhaseSwapText();

            if (trendAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.group1IsEnabled = true;
                this.group2IsEnabled = true;

                if (this.setOnePhaseIsSwapped && this.setTwoPhaseIsSwapped)
                {
                    this.trendPhasesSwappedRadLabel.Text = phasesSwappedBothSetsText;
                    this.polarPhasesSwappedRadLabel.Text = phasesSwappedBothSetsText;
                    ShowPhaseSwapText();
                }
                else if (this.setOnePhaseIsSwapped)
                {
                    this.trendPhasesSwappedRadLabel.Text = phasesSwappedSetOneText;
                    this.polarPhasesSwappedRadLabel.Text = phasesSwappedSetOneText;
                    ShowPhaseSwapText();

                }
                else if (this.setTwoPhaseIsSwapped)
                {
                    this.trendPhasesSwappedRadLabel.Text = phasesSwappedSetTwoText;
                    this.polarPhasesSwappedRadLabel.Text = phasesSwappedSetTwoText;
                    ShowPhaseSwapText();
                }
            }
            else if (trendGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.group1IsEnabled = true;
                if (this.setOnePhaseIsSwapped)
                {
                    this.trendPhasesSwappedRadLabel.Text = phasesSwappedSetOneText;
                    this.polarPhasesSwappedRadLabel.Text = phasesSwappedSetOneText;
                    ShowPhaseSwapText();
                }
            }
            else if (trendGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.group2IsEnabled = true;
                if (this.setTwoPhaseIsSwapped)
                {
                    this.trendPhasesSwappedRadLabel.Text = phasesSwappedSetTwoText;
                    this.polarPhasesSwappedRadLabel.Text = phasesSwappedSetTwoText;
                    ShowPhaseSwapText();
                }
            }
        }

        /// <summary>
        /// Sets the lower and upper date limits as well as the initial viewport sizes
        /// </summary>
        private void SetChartDateLimits()
        {
            try
            {
                double dataContentZoomLimit;
                // bool valuesPresent = false;
                // if both groups are enabled we default to using the dates from group 1
                this.minimumDateOfDataSet = ConversionMethods.MinimumDateTime();
                this.dateRange = 0;
                this.allDateTimesAsDouble = new double[] { 0.0 };

                if ((this.totalReadingsGroup1 > 0) || (this.totalReadingsGroup2 > 0))
                {
                    this.minimumDateOfDataSet = this.commonData.dataReadingDate[0];
                    this.dateRange = this.commonData.dataReadingDate[this.commonData.dataReadingDate.Length - 1].Subtract(this.minimumDateOfDataSet).TotalSeconds;
                    this.allDateTimesAsDouble = Chart.CTime(this.commonData.dataReadingDate);

                    this.currentDuration = this.dateRange;

                    //if (this.group1IsEnabled)
                    //{
                    //    if ((this.dbDataGroup1.dataReadingDate != null) && (this.dbDataGroup1.dataReadingDate.Length > 0))
                    //    {
                    //        this.minimumDateOfDataSet = this.dbDataGroup1.dataReadingDate[0];
                    //        this.dateRange = this.dbDataGroup1.dataReadingDate[this.dbDataGroup1.dataReadingDate.Length - 1].Subtract(this.minimumDateOfDataSet).TotalSeconds;
                    //        this.allDateTimesAsDouble = Chart.CTime(this.dbDataGroup1.dataReadingDate);
                    //        valuesPresent = true;
                    //    }
                    //}
                    //else if (this.group2IsEnabled)
                    //{
                    //    if ((this.dbDataGroup2.dataReadingDate != null) && (this.dbDataGroup2.dataReadingDate.Length > 0))
                    //    {
                    //        this.minimumDateOfDataSet = this.dbDataGroup2.dataReadingDate[0];
                    //        this.dateRange = this.dbDataGroup2.dataReadingDate[this.dbDataGroup2.dataReadingDate.Length - 1].Subtract(this.minimumDateOfDataSet).TotalSeconds;
                    //        this.allDateTimesAsDouble = Chart.CTime(this.dbDataGroup2.dataReadingDate);
                    //        valuesPresent = true;
                    //    }
                    //}
                    //if (valuesPresent)
                    //{
                    // Set the winChartViewer to reflect the visible and minimum duration

                    dataContentZoomLimit = this.minDuration / this.dateRange;
                    /// The value .02 means that the view port width will never be set to less than that value.
                    /// For proper scrolling this is a good limit.                    
                    trendWinChartViewer.ZoomInWidthLimit = Math.Max(dataContentZoomLimit, .02);
                    trendWinChartViewer.ViewPortWidth = 1.0;
                    trendWinChartViewer.ViewPortLeft = 0.0;
                }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetChartDateLimits()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int FindAllDateTimeAsDoubleIndex(double dateTimeValueAsDouble)
        {
            int index = -1;
            try
            {
                int arrayLength = this.allDateTimesAsDouble.Length;
                index = Array.BinarySearch(this.allDateTimesAsDouble, dateTimeValueAsDouble);
                if (index < 0)
                {
                    string errorMessage = "Error in BHM_DataViewer.FindAllDateTimeAsDoubleIndex(double)\nCould not find the date in the data.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    return 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.FindAllDateTimeAsDoubleIndex(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return index;
        }

        /// <summary>
        /// Gets the start date index of the graph data, based on which group is enabled.  If both groups are enabled
        /// we use the dates from the group 1 data to graph the group 2 data
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        private int FindGraphStartDateIndex(DateTime startDate, DateTime[] dataReadingDates)
        {
            int startDateIndex = 0;
            try
            {
                if ((dataReadingDates != null) && (dataReadingDates.Length > 0))
                {
                    startDateIndex = Array.BinarySearch(dataReadingDates, startDate);
                    if (startDateIndex < 0)
                    {
                        startDateIndex = (~startDateIndex) - 1;
                    }

                    /// Hack inserted because the above formula failed for me on very tiny data sets.  Since it
                    /// worked fine otherwise, I assume there is an anomoly that needed smoothing over.
                    if (startDateIndex < 0)
                    {
                        startDateIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.FindGraphStartDateIndex(DateTime, DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return startDateIndex;
        }

        /// <summary>
        /// Gets the end date index of the graph data, based on which group is enabled.  If both groups are enabled
        /// we use the dates from the group 1 data to graph the group 2 data
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int FindGraphEndDateIndex(DateTime endDate, DateTime[] dataReadingDates)
        {
            int endDateIndex = 0;
            try
            {
                int arrayLength = 0;

                arrayLength = dataReadingDates.Length;
                endDateIndex = Array.BinarySearch(dataReadingDates, endDate);
                if (endDateIndex < 0)
                {
                    if ((~endDateIndex) < arrayLength)
                    {
                        endDateIndex = ~endDateIndex;
                    }
                    else
                    {
                        endDateIndex = arrayLength - 1;
                    }
                }
                /// this is here just to keep the program from crashing, I don't know if this 
                /// condition is even possible at this point in the method.  however, this above is based
                /// on chartdirector sample code, not my own algorithm.
                if ((endDateIndex < 0) || (endDateIndex > (arrayLength - 1)))
                {
                    endDateIndex = arrayLength - 1;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.FindGraphEndDateIndex(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return endDateIndex;
        }

        /// <summary>
        /// Gets a list of every different scale type being used to draw the trend chart.  Used when laying out the labels in the case when more than one y axis is necessary to
        /// display the data using different scale numbering.
        /// </summary>
        /// <returns></returns>
        private List<ScaleType> GetScalesBeingUsedForTrendChartList()
        {
            List<ScaleType> scaleTypes = new List<ScaleType>();
            try
            {
                if (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    if ((gammaRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (tangentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (tangentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (tangentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.Percent);
                    }
                    if (moistureContentRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.KilogramsPerMeterCubed);
                    }
                    if ((gammaPhaseRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (phaseTemperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (bushingCurrentPhaseAARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (bushingCurrentPhaseABRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (bushingCurrentPhaseACRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.DegreesAngular);
                    }

                    if (temperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.Unitless);
                    }

                    if (gammaTrendRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.PercentPerYear);
                    }

                    if ((temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.DegreesCentigrade);
                    }

                    if ((bushingCurrentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (bushingCurrentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (bushingCurrentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.MilliAmps);
                    }

                    if ((capacitanceARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (capacitanceBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (capacitanceCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.PicoFarads);
                    }

                    if (frequencyRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.Hertz);
                    }
                    if ((loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.Amps);
                    }
                    if ((voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                       (voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                       (voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.KiloVolts);
                    }
                }
                else
                {
                    scaleTypes.Add(ScaleType.Unitless);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.GetScalesBeingUsedForTrendChartList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return scaleTypes;
        }

        private string GetScaleLabelFromScaleType(ScaleType scaleType)
        {
            string scaleLabel = string.Empty;
            try
            {
                switch (scaleType)
                {
                    case ScaleType.Percent:
                        scaleLabel = ScaleLabels.Percent;
                        break;
                    case ScaleType.DegreesAngular:
                        scaleLabel = ScaleLabels.DegreesAngular;
                        break;
                    case ScaleType.Unitless:
                        scaleLabel = ScaleLabels.Unitless;
                        break;
                    case ScaleType.PercentPerYear:
                        scaleLabel = ScaleLabels.PercentPerYear;
                        break;
                    case ScaleType.DegreesCentigrade:
                        scaleLabel = ScaleLabels.DegreesCentigrade;
                        break;
                    case ScaleType.KilogramsPerMeterCubed:
                        scaleLabel = ScaleLabels.KilogramsPerMeterCubed;
                        break;
                    case ScaleType.KiloVolts:
                        scaleLabel = ScaleLabels.KiloVolts;
                        break;
                    case ScaleType.MilliAmps:
                        scaleLabel = ScaleLabels.MilliAmps;
                        break;
                    case ScaleType.PicoFarads:
                        scaleLabel = ScaleLabels.PicoFarads;
                        break;
                    case ScaleType.Hertz:
                        scaleLabel = ScaleLabels.Hertz;
                        break;
                    case ScaleType.Amps:
                        scaleLabel = ScaleLabels.Amps;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.GetScaleLabelFromScaleType(ScaleType)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return scaleLabel;
        }

        private void SetupAdditionalAxis(int axisUseCount, int axisOffsetValue, string scaleLabel, ref Axis axisName, ref XYChart xyChart)
        {
            try
            {
                if (axisUseCount < 1)
                {
                    xyChart.yAxis().setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                }
                else
                {
                    axisName = xyChart.addAxis(Chart.Left, axisUseCount * axisOffsetValue);
                    axisName.setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetupAdditionalAxis(int, int, string, ref Axis, ref XYChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllDataSelectionBooleansToFalse()
        {
            try
            {
                this.humidityDataIsSelected = false;
                this.moistureDataIsSelected = false;
                this.temp1DataIsSelected = false;
                this.temp2DataIsSelected = false;
                this.temp3DataIsSelected = false;
                this.temp4DataIsSelected = false;
                this.loadCurrent1DataIsSelected = false;
                this.loadCurrent2DataIsSelected = false;
                this.loadCurrent3DataIsSelected = false;
                this.voltage1DataIsSelected = false;
                this.voltage2DataIsSelected = false;
                this.voltage3DataIsSelected = false;

                this.gammaDataIsSelected = false;
                this.gammaPhaseDataIsSelected = false;
                this.gammaTrendDataIsSelected = false;
                this.temperatureCoefficientDataIsSelected = false;
                this.phaseTemperatureCoefficientDataIsSelected = false;
                this.bushingCurrentADataIsSelected = false;
                this.bushingCurrentBDataIsSelected = false;
                this.bushingCurrentCDataIsSelected = false;
                this.bushingCurrentPhaseAADataIsSelected = false;
                this.bushingCurrentPhaseABDataIsSelected = false;
                this.bushingCurrentPhaseACDataIsSelected = false;
                this.capacitanceADataIsSelected = false;
                this.capacitanceBDataIsSelected = false;
                this.capacitanceCDataIsSelected = false;
                this.tangentADataIsSelected = false;
                this.tangentBDataIsSelected = false;
                this.tangentCDataIsSelected = false;
                this.frequencyDataIsSelected = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetAllDataSelectionBooleansToFalse()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SelectDataToGraphFromCheckBoxes()
        {
            try
            {
                SetAllDataSelectionBooleansToFalse();

                if (this.humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.humidityDataIsSelected = true;
                }

                if (this.moistureContentRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.moistureDataIsSelected = true;
                }

                if (this.temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.temp1DataIsSelected = true;
                }

                if (this.temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.temp2DataIsSelected = true;
                }

                if (this.temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.temp3DataIsSelected = true;
                }

                if (this.temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.temp4DataIsSelected = true;
                }

                if (this.loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.loadCurrent1DataIsSelected = true;
                }

                if (this.loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.loadCurrent2DataIsSelected = true;
                }

                if (this.loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.loadCurrent3DataIsSelected = true;
                }

                if (this.voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.voltage1DataIsSelected = true;
                }

                if (this.voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.voltage2DataIsSelected = true;
                }

                if (this.voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.voltage3DataIsSelected = true;
                }

                if (this.gammaRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.gammaDataIsSelected = true;
                }

                if (this.gammaPhaseRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.gammaPhaseDataIsSelected = true;
                }

                if (this.gammaTrendRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.gammaTrendDataIsSelected = true;
                }

                if (this.temperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.temperatureCoefficientDataIsSelected = true;
                }

                if (this.phaseTemperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.phaseTemperatureCoefficientDataIsSelected = true;
                }

                if (this.bushingCurrentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentADataIsSelected = true;
                }

                if (this.bushingCurrentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentBDataIsSelected = true;
                }

                if (this.bushingCurrentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentCDataIsSelected = true;
                }

                if (this.bushingCurrentPhaseAARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentPhaseAADataIsSelected = true;
                }

                if (this.bushingCurrentPhaseABRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentPhaseABDataIsSelected = true;
                }

                if (this.bushingCurrentPhaseACRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.bushingCurrentPhaseACDataIsSelected = true;
                }

                if (this.capacitanceARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.capacitanceADataIsSelected = true;
                }

                if (this.capacitanceBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.capacitanceBDataIsSelected = true;
                }

                if (this.capacitanceCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.capacitanceCDataIsSelected = true;
                }

                if (this.tangentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.tangentADataIsSelected = true;
                }

                if (this.tangentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.tangentBDataIsSelected = true;
                }

                if (this.tangentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.tangentCDataIsSelected = true;
                }

                if (this.frequencyRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.frequencyDataIsSelected = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SelectDataToGraphFromCheckBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DrawBothCharts()
        {
            if ((this.trendChartNeedsUpdating) && (dataDisplayRadPageView.SelectedPage == trendRadPageViewPage))
            {
                DrawTrendChart(trendWinChartViewer, true);
                this.trendChartNeedsUpdating = false;
            }
            if ((this.polarChartNeedsUpdating) && (dataDisplayRadPageView.SelectedPage == polarRadPageViewPage))
            {
                DrawPolarChart(polarWinChartViewer);
                this.polarChartNeedsUpdating = false;
            }
        }

        /// <summary>
        /// Creates a chart for the trend data using all data that is associated with a checked checkbox in the "trend" tab view.
        /// </summary>
        /// <param name="chartViewer"></param>
        private void DrawTrendChart(WinChartViewer chartViewer, bool selectItemsToDrawFromTheCheckboxes)
        {
            try
            {
                int widthExtension = this.Width - this.startingFormWidth;
                int heightExtension = this.Height - this.startingFormHeight;

                int chartWidth = this.trendChartMinimumWidth + widthExtension;
                int chartHeight = this.trendChartMinimumHeight + heightExtension;
                int graphWidth = this.trendGraphWidthMinimum + widthExtension;
                int graphHeight = this.trendGraphHeightMinimum + heightExtension;

                if (chartViewer != null)
                {
                    if (((this.totalReadingsGroup1 > 0) && (this.group1IsEnabled)) ||
                        ((this.totalReadingsGroup2 > 0) && (this.group2IsEnabled)))
                    {
                        int yAxisUseCount = 0;
                        int axisOffsetValue = 50;

                        Axis percentAxis = null;
                        Axis degreesAngularAxis = null;
                        Axis unitlessAxis = null;
                        Axis percentPerYearAxis = null;
                        Axis degreesCentigradeAxis = null;
                        Axis kilogramsPerMeterCubedAxis = null;
                        Axis milliAmpsAxis = null;
                        Axis picoFaradsAxis = null;
                        Axis kiloVoltsAxis = null;
                        Axis hertzAxis = null;
                        Axis ampsAxis = null;

                        this.trendPhasesSwappedRadLabel.Location = new Point(trendPhasesSwappedInitialX + widthExtension, trendPhasesSwappedInitialY);

                        /// at this point, we're just interested in getting the size of the graph set up
                        List<ScaleType> yScalesUsed = GetScalesBeingUsedForTrendChartList();
                        int additionalYscalesNeeded = yScalesUsed.Count - 1;
                        int graphXeasementForExtraScales = additionalYscalesNeeded * axisOffsetValue;
                        graphWidth -= graphXeasementForExtraScales;

                        /// save some values for the cursor location
                        this.trendGraphCurrentOffsetX = this.trendGraphOffsetX + graphXeasementForExtraScales;
                        this.trendGraphXStart = chartViewer.Location.X;
                        this.trendGraphYStart = chartViewer.Location.Y + this.trendGraphtOffsetY;
                        this.trendGraphHeight = graphHeight;
                        this.trendGraphWidth = graphWidth;

                        /// used to aggregate data when the viewport is too small to hold the amount of data in range
                        // ArrayMath aggregator = null;
                        ArrayMath commonDataAggregator = null;
                        ArrayMath group1DataAggregator = null;
                        ArrayMath group2DataAggregator = null;

                        DateTime viewPortStartDate = this.minimumDateOfDataSet.AddSeconds(Math.Round(chartViewer.ViewPortLeft * this.dateRange));
                        DateTime viewPortEndDate = viewPortStartDate.AddSeconds(Math.Round(chartViewer.ViewPortWidth * this.dateRange));

                        double[] commonDataTimeStampsAsDouble;
                        double[] group1TimeStampsAsDouble = null;
                        double[] group2TimeStampsAsDouble = null;

                        int commonDataStartIndex = 0;
                        int group1StartIndex = 0;
                        int group2StartIndex = 0;

                        int commonDataEndIndex = 0;
                        int group1EndIndex = 0;
                        int group2EndIndex = 0;

                        int numberOfCommonDataPointsBeingGraphed = 0;
                        int numberOfGroup1DataPointsBeingGraphed = 0;
                        int numberOfGroup2DataPointsBeingGraphed = 0;

                        DateTime[] commonDataTimeStamps = null;
                        DateTime[] group1TimeStamps = null;
                        DateTime[] group2TimeStamps = null;

                        commonDataStartIndex = FindGraphStartDateIndex(viewPortStartDate, this.commonData.dataReadingDate);
                        commonDataEndIndex = FindGraphEndDateIndex(viewPortEndDate, this.commonData.dataReadingDate);
                        numberOfCommonDataPointsBeingGraphed = commonDataEndIndex - commonDataStartIndex + 1;
                        commonDataTimeStamps = new DateTime[numberOfCommonDataPointsBeingGraphed];
                        commonDataTimeStampsAsDouble = Chart.CTime(commonDataTimeStamps);
                        Array.Copy(this.commonData.dataReadingDate, commonDataStartIndex, commonDataTimeStamps, 0, numberOfCommonDataPointsBeingGraphed);

                        if (group1IsEnabled)
                        {
                            group1StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup1.dataReadingDate);
                            group1EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup1.dataReadingDate);
                            numberOfGroup1DataPointsBeingGraphed = group1EndIndex - group1StartIndex + 1;
                            group1TimeStamps = new DateTime[numberOfGroup1DataPointsBeingGraphed];
                            group1TimeStampsAsDouble = Chart.CTime(group1TimeStamps);
                            Array.Copy(this.dbDataGroup1.dataReadingDate, group1StartIndex, group1TimeStamps, 0, numberOfGroup1DataPointsBeingGraphed);
                        }
                        if (group2IsEnabled)
                        {
                            group2StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup2.dataReadingDate);
                            group2EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup2.dataReadingDate);
                            numberOfGroup2DataPointsBeingGraphed = group2EndIndex - group2StartIndex + 1;
                            group2TimeStamps = new DateTime[numberOfGroup2DataPointsBeingGraphed];
                            group2TimeStampsAsDouble = Chart.CTime(group2TimeStamps);
                            Array.Copy(this.dbDataGroup2.dataReadingDate, group2StartIndex, group2TimeStamps, 0, numberOfGroup2DataPointsBeingGraphed);
                        }                       

                        //if ((commonDataTimeStamps.Length >= 600) && (this.cursorIsEnabled))
                        //{
                        //    //
                        //    // Zoomable chart with high zooming ratios often need to plot many thousands of 
                        //    // points when fully zoomed out. However, it is usually not needed to plot more
                        //    // data points than the resolution of the chart. Plotting too many points may cause
                        //    // the points and the lines to overlap. So rather than increasing resolution, this 
                        //    // reduces the clarity of the chart. So it is better to aggregate the data first if
                        //    // there are too many points.
                        //    //
                        //    // In our current example, the chart only has 520 pixels in width and is using a 2
                        //    // pixel line width. So if there are more than 520 data points, we aggregate the 
                        //    // data using the ChartDirector aggregation utility method.
                        //    //
                        //    // If in your real application, you do not have too many data points, you may 
                        //    // remove the following code altogether.
                        //    //

                        //    // Set up an aggregator to aggregate the data based on regular sized slots
                        //    //aggregator = new ArrayMath(viewPortTimeStamps);

                        //    commonDataAggregator = new ArrayMath(commonDataTimeStamps);
                        //    commonDataAggregator.selectRegularSpacing(commonDataTimeStamps.Length / 300);
                        //    commonDataTimeStamps = commonDataAggregator.aggregate(commonDataTimeStamps, Chart.AggregateFirst);

                        //    if (this.group1IsEnabled)
                        //    {
                        //        group1DataAggregator = new ArrayMath(group1TimeStamps);
                        //        group1DataAggregator.selectRegularSpacing(group1TimeStamps.Length / 300);
                        //        group1TimeStamps = group1DataAggregator.aggregate(group1TimeStamps, Chart.AggregateFirst);
                        //    }

                        //    if (this.group2IsEnabled)
                        //    {
                        //        group2DataAggregator = new ArrayMath(group2TimeStamps);
                        //        group2DataAggregator.selectRegularSpacing(group2TimeStamps.Length / 300);
                        //        group2TimeStamps = group1DataAggregator.aggregate(group2TimeStamps, Chart.AggregateFirst);
                        //    }
                        //    //aggregator.selectRegularSpacing(viewPortTimeStamps.Length / 300);
                        //    //commonDataAggregator.selectRegularSpacing(commonData.dataReadingDate.Length / 300);

                        //    //// For the timestamps, take the first timestamp on each slot
                        //    //viewPortTimeStamps = aggregator.aggregate(viewPortTimeStamps, Chart.AggregateFirst);
                        //}

                        /// get the double version of the time stamps in case we're adding points to the scatter
                        /// layer or computing some trend lines, both of which require a double for the X argument
                        //viewPortTimeStampsAsDouble = Chart.CTime(viewPortTimeStamps);
                                              
                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 1 - Configure overall chart appearance. 
                        ///////////////////////////////////////////////////////////////////////////////////////

                        // Create an XYChart object 680 x 450 pixels in size, with pale blue (0xf0f0ff) 
                        // background, black (000000) border, 1 pixel raised effect, and with a rounded frame.
                        XYChart xyChart = new XYChart(chartWidth, chartHeight, 0xf0f0ff, 0, 1);
                        xyChart.setRoundedFrame(Chart.CColor(BackColor));

                        // Set the plotarea at (55, 100) and of size 600 x 300 pixels. Use white (ffffff) 
                        // background. Enable both horizontal and vertical grids by setting their colors to 
                        // grey (cccccc). Set clipping mode to clip the data lines to the plot area.
                        xyChart.setPlotArea(this.trendGraphCurrentOffsetX, this.trendGraphtOffsetY,
                                            graphWidth, graphHeight, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
                        xyChart.setClipping();

                        // Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                        // light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                        xyChart.addTitle("Trend", "Times New Roman Bold Italic", 15).setBackground(0xccccff, 0x0, Chart.glassEffect());

                        // Add a bottom title to the chart to show the date range of the axis, with a light blue 
                        // (ccccff) background.
                        xyChart.addTitle2(Chart.Bottom, "From <*font=Arial Bold Italic*>"
                            + xyChart.formatValue(viewPortStartDate, "{value|mmm dd, yyyy}")
                            + "<*/font*> to <*font=Arial Bold Italic*>"
                            + xyChart.formatValue(viewPortEndDate, "{value|mmm dd, yyyy}")
                            + "<*/font*> (Duration <*font=Arial Bold Italic*>"
                            + Math.Round(viewPortEndDate.Subtract(viewPortStartDate).TotalSeconds / 86400.0)
                            + "<*/font*> days)", "Arial Italic", 10).setBackground(0xccccff);

                        // Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                        xyChart.addLegend(50, 33, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

                        // Set axes width to 2 pixels
                        xyChart.yAxis().setWidth(2);
                        xyChart.xAxis().setWidth(2);

                        /// Now set up any addition y-axis needed

                        if (yScalesUsed.Contains(ScaleType.Percent))
                        {
                            // this one is different from all the following ones because by default if the
                            // axis is present, it will be the "standard" y axis.
                            xyChart.yAxis().setTitle(ScaleLabels.Percent).setAlignment(Chart.TopLeft2);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.DegreesAngular))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.DegreesAngular, ref degreesAngularAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Unitless))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Unitless, ref unitlessAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.PercentPerYear))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.PercentPerYear, ref percentPerYearAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.DegreesCentigrade))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.DegreesCentigrade, ref degreesCentigradeAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.KilogramsPerMeterCubed))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.KilogramsPerMeterCubed, ref kilogramsPerMeterCubedAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.MilliAmps))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.MilliAmps, ref milliAmpsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.PicoFarads))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.PicoFarads, ref picoFaradsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Hertz))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Hertz, ref hertzAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.KiloVolts))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.KiloVolts, ref kiloVoltsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Amps))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Amps, ref ampsAxis, ref xyChart);
                            yAxisUseCount++;
                        }

                        //// Add a title to the y-axis
                        //xyChart.yAxis().setTitle("Values", "Arial Bold", 9);

                        //Layer lineLayer = xyChart.addLineLayer2();
                        //lineLayer.setLineWidth(1);

                        //lineLayer.setXData(viewPortTimeStamps);

                        // add data

                        #region Checkboxes - check the toggle state and add data if they are toggled on

                        if (selectItemsToDrawFromTheCheckboxes)
                        {
                            SelectDataToGraphFromCheckBoxes();
                        }

                        #region Dynamics

                        if (this.humidityDataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(commonData.humidity, this.humidityScaleFactor, this.humidityOperation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref percentAxis,
                                                                      humidityColor, humidityRadCheckBox.Text.Trim(), humiditySymbol,false);
                        }

                        if (this.moistureDataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(commonData.moisture, this.moistureScaleFactor, this.moistureOperation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref kilogramsPerMeterCubedAxis,
                                                                      moistureColor, this.moistureContentRadCheckBox.Text.Trim(), moistureSymbol, false);
                        }

                        if (this.temp1DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.temp1, this.temp1ScaleFactor, this.temp1Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis,
                                                                      temp1Color, temp1RadCheckBox.Text.Trim(), temp1Symbol, false);
                        }

                        if (this.temp2DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.temp2, this.temp2ScaleFactor, this.temp2Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis,
                                                                      temp2Color, temp2RadCheckBox.Text.Trim(), temp2Symbol, false);
                        }

                        if (this.temp3DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.temp3, this.temp3ScaleFactor, this.temp3Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis,
                                                                      temp3Color, temp3RadCheckBox.Text.Trim(), temp3Symbol, false);
                        }

                        if (this.temp4DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.temp4, this.temp4ScaleFactor, this.temp4Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis,
                                                                      temp4Color, temp4RadCheckBox.Text.Trim(), temp4Symbol, false);
                        }

                        if (this.loadCurrent1DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.loadCurrent1, this.loadCurrent1ScaleFactor, this.loadCurrent1Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref ampsAxis,
                                                                      loadCurrent1Color, loadCurrent1RadCheckBox.Text.Trim(), loadCurrent1Symbol, false);
                        }

                        if (this.loadCurrent2DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.loadCurrent2, this.loadCurrent2ScaleFactor, this.loadCurrent2Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref ampsAxis,
                                                                      loadCurrent2Color, loadCurrent2RadCheckBox.Text, loadCurrent2Symbol, false);
                        }

                        if (this.loadCurrent3DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.loadCurrent3, this.loadCurrent3ScaleFactor, this.loadCurrent3Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref ampsAxis,
                                                                      loadCurrent3Color, loadCurrent3RadCheckBox.Text, loadCurrent3Symbol, false);
                        }

                        if (this.voltage1DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.voltage1, this.voltage1ScaleFactor, this.voltage1Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis,
                                                                      voltage1Color, voltage1RadCheckBox.Text, voltage1Symbol, false);
                        }

                        if (this.voltage2DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.voltage2, this.voltage2ScaleFactor, this.voltage2Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis,
                                                                      voltage2Color, voltage2RadCheckBox.Text, voltage2Symbol, false);
                        }

                        if (this.voltage3DataIsSelected)
                        {
                            PrepareOneDataSetForGraphingAndAddToGraph(ScaleDynamicsValues(this.commonData.voltage3, this.voltage3ScaleFactor, this.voltage3Operation),
                                                                      commonDataTimeStamps, commonDataTimeStampsAsDouble, commonDataAggregator, commonDataStartIndex,
                                                                      numberOfCommonDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis,
                                                                      voltage3Color, voltage3RadCheckBox.Text, voltage3Symbol, false);
                        }

                        #endregion

                        if (this.gammaDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.gamma, this.dbDataGroup2.gamma, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref percentAxis,
                                                                gammaColor, gammaColor, gammaLegendText, gammaSymbol);
                        }

                        if (this.gammaPhaseDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.gammaPhase, this.dbDataGroup2.gammaPhase, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref degreesAngularAxis,
                                                                gammaPhaseColor, gammaPhaseColor,
                                                                gammaPhaseLegendText, gammaPhaseSymbol);
                        }

                        if (this.gammaTrendDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.gammaTrend, this.dbDataGroup2.gammaTrend, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref percentPerYearAxis,
                                                                gammaTrendColor, gammaTrendColor,
                                                                gammaTrendLegendText, gammaTrendSymbol);
                        }

                        if (this.temperatureCoefficientDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.temperatureCoefficient, this.dbDataGroup2.temperatureCoefficient, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref unitlessAxis,
                                                                temperatureCoefficientColor, temperatureCoefficientColor,
                                                                tempCoeffLegendText, temperatureCoefficientSymbol);
                        }

                        if (this.phaseTemperatureCoefficientDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.phaseTemperatureCoefficient,
                                                                this.dbDataGroup2.phaseTemperatureCoefficient, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref degreesAngularAxis,
                                                                phaseTemperatureCoefficientColor, phaseTemperatureCoefficientColor,
                                                                phaseTempCoeffLegendText, phaseTemperatureCoefficientSymbol);
                        }

                        if (this.bushingCurrentADataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentA,
                                                                this.dbDataGroup2.bushingCurrentA, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref milliAmpsAxis,
                                                                bushingCurrentAColor, bushingCurrentAColor,
                                                                bushingCur1LegendText, bushingCurrentASymbol);
                        }

                        if (this.bushingCurrentBDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentB,
                                                                this.dbDataGroup2.bushingCurrentB, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref milliAmpsAxis,
                                                                bushingCurrentBColor, bushingCurrentBColor,
                                                                bushingCur2LegendText, bushingCurrentBSymbol);
                        }

                        if (this.bushingCurrentCDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentC,
                                                                this.dbDataGroup2.bushingCurrentC, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref milliAmpsAxis,
                                                                bushingCurrentCColor, bushingCurrentCColor,
                                                                bushingCur3LegendText, bushingCurrentCSymbol);
                        }

                        if (this.bushingCurrentPhaseAADataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentPhaseAA,
                                                                this.dbDataGroup2.bushingCurrentPhaseAA, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref degreesAngularAxis,
                                                                bushingCurrentPhaseAAColor, bushingCurrentPhaseAAColor,
                                                               bushingCurPhase11LegendText, bushingCurrentPhaseAASymbol);
                        }

                        if (this.bushingCurrentPhaseABDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentPhaseAB,
                                                                this.dbDataGroup2.bushingCurrentPhaseAB, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref degreesAngularAxis,
                                                                bushingCurrentPhaseABColor, bushingCurrentPhaseABColor,
                                                                bushingCurPhase12LegendText, bushingCurrentPhaseABSymbol);
                        }

                        if (this.bushingCurrentPhaseACDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.bushingCurrentPhaseAC,
                                                                this.dbDataGroup2.bushingCurrentPhaseAC, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref degreesAngularAxis,
                                                                bushingCurrentPhaseACColor, bushingCurrentPhaseACColor,
                                                               bushingCurPhase13LegendText, bushingCurrentPhaseACSymbol);
                        }

                        if (this.capacitanceADataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.capacitanceA,
                                                                this.dbDataGroup2.capacitanceA, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref picoFaradsAxis,
                                                                capacitanceAColor, capacitanceAColor,
                                                                capacitance1LegendText, capacitanceASymbol);
                        }

                        if (this.capacitanceBDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.capacitanceB,
                                                                this.dbDataGroup2.capacitanceB, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref picoFaradsAxis,
                                                                capacitanceBColor, capacitanceBColor,
                                                                capacitance2LegendText, capacitanceBSymbol);
                        }

                        if (this.capacitanceCDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.capacitanceC,
                                                                this.dbDataGroup2.capacitanceC, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref picoFaradsAxis,
                                                                capacitanceCColor, capacitanceCColor,
                                                                capacitance3LegendText, capacitanceCSymbol);
                        }

                        if (this.tangentADataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.tangentA,
                                                                this.dbDataGroup2.tangentA, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref percentAxis,
                                                                tangentAColor, tangentAColor,
                                                                tangent1LegendText, tangentASymbol);
                        }

                        if (this.tangentBDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.tangentB,
                                                                this.dbDataGroup2.tangentB, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref percentAxis,
                                                                tangentBColor, tangentBColor,
                                                                tangent2LegendText, tangentBSymbol);
                        }

                        if (this.tangentCDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.tangentC,
                                                                this.dbDataGroup2.tangentC, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref percentAxis,
                                                                tangentCColor, tangentCColor,
                                                                tangent3LegendText, tangentCSymbol);
                        }

                        if (this.frequencyDataIsSelected)
                        {
                            PrepareDataForGraphingAndAddToGraph(this.dbDataGroup1.frequency,
                                                                this.dbDataGroup2.frequency, group1TimeStamps, group2TimeStamps, group1TimeStampsAsDouble, group2TimeStampsAsDouble,
                                                                group1DataAggregator, group2DataAggregator, group1StartIndex, group2StartIndex, numberOfGroup1DataPointsBeingGraphed, numberOfGroup2DataPointsBeingGraphed,
                                                                ref xyChart, ref hertzAxis,
                                                                frequencyColor, frequencyColor,
                                                                frequencyLegendText, frequencySymbol);
                        }

                        #endregion

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 3 - Set up x-axis scale
                        ///////////////////////////////////////////////////////////////////////////////////////

                        // Set x-axis date scale to the view port date range. 
                        xyChart.xAxis().setDateScale(viewPortStartDate, viewPortEndDate);

                        //
                        // In the current demo, the x-axis range can be from a few years to a few days. We can 
                        // let ChartDirector auto-determine the date/time format. However, for more beautiful 
                        // formatting, we set up several label formats to be applied at different conditions. 
                        //

                        // If all ticks are yearly aligned, then we use "yyyy" as the label format.
                        xyChart.xAxis().setFormatCondition("align", 360 * 86400);
                        xyChart.xAxis().setLabelFormat("{value|yyyy}");

                        // If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
                        // label of a year, and "mmm" for other labels.
                        xyChart.xAxis().setFormatCondition("align", 30 * 86400);
                        xyChart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                            Chart.AllPassFilter(), "{value|mmm}");

                        // If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
                        // first label of a year, and "mmm dd" in bold font as the first label of a month, and
                        // "dd" for other labels.
                        xyChart.xAxis().setFormatCondition("align", 86400);
                        xyChart.xAxis().setMultiFormat(
                            Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                            Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
                        xyChart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

                        // For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
                        // a day, and "hh:nn" for other labels.
                        xyChart.xAxis().setFormatCondition("else");
                        xyChart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                            Chart.AllPassFilter(), "{value|hh:nn}");

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 4 - Set up y-axis scale
                        ///////////////////////////////////////////////////////////////////////////////////////

                        if ((chartViewer.ZoomDirection == WinChartDirection.Horizontal) || (minValue == maxValue))
                        {
                            // y-axis is auto-scaled - save the chosen y-axis scaled to support xy-zoom mode
                            xyChart.layout();
                            minValue = xyChart.yAxis().getMinValue();
                            maxValue = xyChart.yAxis().getMaxValue();
                        }
                        else
                        {
                            // xy-zoom mode - compute the actual axis scale in the view port 
                            double axisLowerLimit = maxValue - (maxValue - minValue) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                            double axisUpperLimit = maxValue - (maxValue - minValue) * chartViewer.ViewPortTop;
                            // *** use the following formula if you are using a log scale axis ***
                            // double axisLowerLimit = maxValue * Math.Pow(minValue / maxValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                            // double axisUpperLimit = maxValue * Math.Pow(minValue / maxValue, chartViewer.ViewPortTop);

                            // use the zoomed-in scale
                            xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                            xyChart.yAxis().setRounding(false, false);
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 5 - Display the chart
                        ///////////////////////////////////////////////////////////////////////////////////////


                        if (selectItemsToDrawFromTheCheckboxes)
                        {
                            trendXYChart = null;

                            trendXYChart = xyChart;

                            chartViewer.Chart = xyChart;
                        }
                        else
                        {


                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.DrawTrendChart(WinChartViewer)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.DrawTrendChart(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private static double[] ScaleDynamicsValues(double[] inputArray, double scaleFactor, string operation)
        {
            double[] result = null;
            try
            {
                ArrayMath scaledValues = new ArrayMath(inputArray);
                string trimmedOperation = operation.Trim();
                if (trimmedOperation.CompareTo("add") == 0)
                {
                    scaledValues.add2(scaleFactor);
                }
                else if (trimmedOperation.CompareTo("multiply") == 0)
                {
                    scaledValues.mul2(scaleFactor);
                }
                else
                {
                    string errorMessage = "Error in BHM_DataViewer.ScaleDynamicsValues(double[], double, string)\nInput string did not indicate a defined operation.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                result = scaledValues.result();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.ScaleDynamicsValues(double[], double, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return result;
        }

        /// <summary>
        /// Handles the copying and aggregation of all data and adds it to the graph.  Also handles setting the trend type, if desired,
        /// and adds it to the graph.
        /// </summary>
        /// <param name="sourceArrayGroup1"></param>
        /// <param name="destinationArrayGroup1"></param>
        /// <param name="sourceArrayGroup2"></param>
        /// <param name="destinationArrayGroup2"></param>
        /// <param name="dateTimeAsDouble"></param>
        /// <param name="aggregator"></param>
        /// <param name="startIndex"></param>
        /// <param name="numberOfPointsToCopy"></param>
        /// <param name="lineLayer"></param>
        /// <param name="xyChart"></param>
        /// <param name="colorGroup1"></param>
        /// <param name="colorGroup2"></param>
        /// <param name="name"></param>
        /// <param name="symbol"></param>
        public void PrepareDataForGraphingAndAddToGraph(double[] sourceArrayGroup1, double[] sourceArrayGroup2, DateTime[] dateTimeGroup1, DateTime[] dateTimeGroup2, double[] dateTimeAsDoubleGroup1,
                                                        double[] dateTimeAsDoubleGroup2, ArrayMath aggregatorGroup1, ArrayMath aggregatorGroup2, int startIndexGroup1, int startIndexGroup2,
                                                        int numberOfPointsToCopyGroup1, int numberOfPointsToCopyGroup2, ref XYChart xyChart, ref Axis displayAxis,
                                                        int colorGroup1, int colorGroup2, string name, int symbol)
        {
            try
            {
                string readingName = name;
                if (this.group1IsEnabled)
                {
                    if (this.group2IsEnabled)
                    {
                        readingName = name + " Set 1";
                    }

                    PrepareOneDataSetForGraphingAndAddToGraph(sourceArrayGroup1, dateTimeGroup1, dateTimeAsDoubleGroup1, aggregatorGroup1, startIndexGroup1,
                                                              numberOfPointsToCopyGroup1, ref xyChart, ref displayAxis,
                                                              colorGroup1, readingName, symbol, false);
                }
                if (this.group2IsEnabled)
                {
                    if (this.group1IsEnabled)
                    {
                        readingName = name + " Set 2";
                    }

                    PrepareOneDataSetForGraphingAndAddToGraph(sourceArrayGroup2, dateTimeGroup2, dateTimeAsDoubleGroup2, aggregatorGroup2, startIndexGroup2,
                                                              numberOfPointsToCopyGroup2, ref xyChart, ref displayAxis,
                                                              colorGroup2, readingName, symbol,true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PrepareDataForGraphingAndAddToGraph(double[], double[], DateTime[], DateTime[], double[], double[], ArrayMath, ArrayMath, int, int, int, int, ref XYChart, ref Axis, int, int, string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Handles the copying and aggregation of one data set and adds it to the graph.  Also handles setting the trend type, if desired,
        /// and adds it to the graph.
        /// </summary>
        /// <param name="sourceArray"></param>
        /// <param name="destinationArray"></param>
        /// <param name="dateTimeAsDouble"></param>
        /// <param name="aggregator"></param>
        /// <param name="startIndex"></param>
        /// <param name="numberOfPointsToCopy"></param>
        /// <param name="lineLayer"></param>
        /// <param name="xyChart"></param>
        /// <param name="color"></param>
        /// <param name="name"></param>
        /// <param name="symbol"></param>
        private void PrepareOneDataSetForGraphingAndAddToGraph(double[] sourceArray, DateTime[] dateTime, double[] dateTimeAsDouble, ArrayMath aggregator,
                                                               int startIndex, int numberOfPointsToCopy, ref XYChart xyChart,
                                                               ref Axis displayAxis, int color, string name, int symbol, bool useDashedLine)
        {
            try
            {
                int lineWidth = 2;
                double[] graphingDataArray = new double[numberOfPointsToCopy];
                if (sourceArray != null)
                {
                    if (sourceArray.Length >= (startIndex + numberOfPointsToCopy))
                    {
                        if (dateTime != null)
                        {
                            if (dateTimeAsDouble != null)
                            {
                                Array.Copy(sourceArray, startIndex, graphingDataArray, 0, numberOfPointsToCopy);

                                /// we use a null test to determine whether or not an aggregator was created, since we only
                                /// create one if we are going to aggregate.  this saves passing in a boolean or other flag.
                                if (aggregator != null)
                                {
                                    graphingDataArray = aggregator.aggregate(graphingDataArray, Chart.AggregateAvg);
                                }

                                if (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                {
                                    graphingDataArray = NormalizeData(graphingDataArray);
                                }

                                if ((this.trendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off) &&
                                    (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                {
                                    LineLayer lineLayer = xyChart.addLineLayer();
                                    lineLayer.setLineWidth(lineWidth);
                                    lineLayer.setXData(dateTime);
                                    if (useDashedLine)
                                    {
                                        lineLayer.addDataSet(graphingDataArray, xyChart.dashLineColor(color, Chart.DashLine), name).setDataSymbol(symbol, 8, color, color);
                                    }
                                    else
                                    {
                                        lineLayer.addDataSet(graphingDataArray, color, name).setDataSymbol(symbol, 8, color, color);
                                    }
                                    if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                    {
                                        lineLayer.setUseYAxis(displayAxis);
                                    }
                                }
                                else
                                {
                                    if (this.trendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        ScatterLayer scatterLayer = xyChart.addScatterLayer(dateTimeAsDouble, graphingDataArray, name, symbol, 8, color, color);
                                        if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                        {
                                            scatterLayer.setUseYAxis(displayAxis);
                                        }
                                        if (this.linearTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        {
                                            TrendLayer linearTrendLayer = null;
                                            if (useDashedLine)
                                            {
                                                linearTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, xyChart.dashLineColor(color, Chart.DashLine));
                                            }
                                            else
                                            {
                                                linearTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, color);
                                            }
                                            if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                            {
                                                linearTrendLayer.setUseYAxis(displayAxis);
                                            }
                                        }
                                        else if (this.exponentialTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        {
                                            TrendLayer exponentialTrendLayer = null;
                                            if (useDashedLine)
                                            {
                                                exponentialTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, xyChart.dashLineColor(color, Chart.DashLine));
                                            }
                                            else
                                            {
                                                exponentialTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, color);
                                            }
                                            exponentialTrendLayer.setRegressionType(Chart.ExponentialRegression);
                                            exponentialTrendLayer.setHTMLImageMap("{disable}");
                                            if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                            {
                                                exponentialTrendLayer.setUseYAxis(displayAxis);
                                            }
                                        }
                                    }
                                    else if (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        double[] movingAverageData = new ArrayMath(graphingDataArray).movAvg((int)this.movingAverageRadSpinEditor.Value).result();
                                        LineLayer movingAverageLayer = xyChart.addLineLayer();
                                        movingAverageLayer.setLineWidth(lineWidth);
                                        if (useDashedLine)
                                        {
                                            movingAverageLayer.addDataSet(movingAverageData, xyChart.dashLineColor(color, Chart.DashLine), name + " Mov Avg");
                                        }
                                        else
                                        {
                                            movingAverageLayer.addDataSet(movingAverageData, color, name + " Mov Avg");
                                        }
                                        movingAverageLayer.setXData(dateTime);
                                        if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                        {
                                            movingAverageLayer.setUseYAxis(displayAxis);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in BHMDataViewer.PrepareOneDataSetForGraphingAndAddToGraph(double[], DateTime[], double[], ArrayMath, int, int, ref XYChart, ref Axis, int, string, int)\nSecond input double[] was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHMDataViewer.PrepareOneDataSetForGraphingAndAddToGraph(double[], DateTime[], double[], ArrayMath, int, int, ref XYChart, ref Axis, int, string, int)\nInput DateTime[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHMDataViewer.PrepareOneDataSetForGraphingAndAddToGraph(double[], DateTime[], double[], ArrayMath, int, int, ref XYChart, ref Axis, int, string, int)\nFirst input double[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.PrepareOneDataSetForGraphingAndAddToGraph(double[], DateTime[], double[], ArrayMath, int, int, ref XYChart, ref Axis, int, string, int)\nFirst input double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PrepareOneDataSetForGraphingAndAddToGraph(double[], DateTime[], double[], ArrayMath, int, int, ref XYChart, ref Axis, int, string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private double[] NormalizeData(double[] inputData)
        {
            double[] normalizedData = null;
            try
            {
                ArrayMath arrayMath;
                double scalefactor;
                if (inputData != null)
                {
                    arrayMath = new ArrayMath(inputData);

                    scalefactor = arrayMath.max();
                    if (scalefactor == 0) // avoid divide by 0 error
                    {
                        scalefactor = 1;
                    }
                    arrayMath.div2(scalefactor);

                    normalizedData = arrayMath.result();
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.NormalizeData(double[])\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.NormalizeData(double[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return normalizedData;
        }

        private void drawGraphsRadButton_Click(object sender, EventArgs e)
        {
            DrawTrendChart(this.trendWinChartViewer, true);
        }

        private void trendSaveGraphRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string dateTimeString = ConvertDateTimeToString(DateTime.Now);
                string filename = "Trend " + dateTimeString + ".png";
                trendSaveFileDialog.FileName = "Trend " + dateTimeString;
                trendSaveFileDialog.Filter = "PNG file (*.png)|*.png";
                trendSaveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                trendSaveFileDialog.AddExtension = true;
                trendSaveFileDialog.CheckPathExists = true;
                if (System.Windows.Forms.DialogResult.OK == trendSaveFileDialog.ShowDialog())
                {
                    filename = System.IO.Path.GetFullPath(trendSaveFileDialog.FileName);
                    System.Drawing.Image trendImage = trendWinChartViewer.Image;
                    trendImage.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendSaveGraphRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Adjusts the width and scrolling range of the horizontal scrollbar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trendRadHScrollBar_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (hasFinishedInitialization)
                {
                    // Set the view port based on the scroll bar
                    this.trendWinChartViewer.ViewPortLeft = ((double)(trendRadHScrollBar.Value - trendRadHScrollBar.Minimum))
                        / (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum);

                    // Update the chart display without updating the image maps. (We can delay updating
                    // the image map until scrolling is completed and the chart display is stable.)
                    trendWinChartViewer.updateViewPort(true, false);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendRadHScrollBar_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void trendWinChartViewer_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                trendChartHasMouseFocus = true;
                UpdateTrendImageMap(this.trendWinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendWinChartViewer_MouseEnter(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void trendWinChartViewer_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                trendChartHasMouseFocus = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendWinChartViewer_MouseLeave(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void trendWinChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            try
            {
                if (trendWinChartViewer.MouseUsage == WinChartMouseUsage.ScrollOnDrag)
                {
                    if (this.cursorIsEnabled)
                    {
                        string attrValueX = e.AttrValues["x"].ToString();
                        this.cursorXval = Double.Parse(attrValueX);
                        this.cursorDateTimeAsDoubleIndex = FindAllDateTimeAsDoubleIndex(this.cursorXval);
                        UpdateAllCursorPositions();
                    }
                    else
                    {
                        new ParamViewer().Display(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendWinChartViewer_ClickHotSpot(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Handles all toggle-state changed events for data selection and trend line selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void dataSelection_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawTrendChart(this.trendWinChartViewer, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.dataSelection_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void trendRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        movingAverageRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    }
                }
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawTrendChart(this.trendWinChartViewer, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void movingAverageRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendLineRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                    }
                }
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawTrendChart(this.trendWinChartViewer, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.movingAverageRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void autoDrawRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    //this.drawGraphsRadButton.Enabled = false;
                    DrawTrendChart(this.trendWinChartViewer, true);
                    if (polarAutoDrawRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarAutoDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.autoDrawRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void manualDrawRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (this.trendManualDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    //this.drawGraphsRadButton.Enabled = true;
                    if (polarManualDrawRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        polarManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.manualDrawRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void linearTrendRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (linearTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer, true);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.linearTrendRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void exponentialTrendRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (exponentialTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer, true);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.exponentialTrendRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        // #region Polar event handlers

        // Many of the following handlers are tied to their equivalent in the trend panel, so there is consistency when you tab
        // to different view types

        private void polarGroup1RadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendGroup1RadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                    if (this.cursorIsEnabled)
                    {
                        UpdateAllCursorPositions();
                    }
                    polarHcursorGroup2Label.Visible = false;
                    polarVcursorGroup2Label.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarGroup1RadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarGroup2RadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendGroup2RadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                    if (this.cursorIsEnabled)
                    {
                        UpdateAllCursorPositions();
                    }
                    polarHcursorGroup1Label.Visible = false;
                    polarVcursorGroup1Label.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarGroup2RadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarAllGroupsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendAllGroupsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendAllGroupsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarAllGroupsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarAutoDrawRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    //polarDrawGraphsRadButton.Enabled = false;
                    DrawPolarChart(this.polarWinChartViewer);
                    if (trendAutoDrawRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendAutoDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (this.cursorIsEnabled)
                    {
                        UpdateAllCursorPositions();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarAutoDrawRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarManualDrawRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarManualDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    //polarDrawGraphsRadButton.Enabled = true;
                    if (trendManualDrawRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarManualDrawRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarThreeMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendThreeMonthsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendThreeMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarThreeMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarSixMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendSixMonthsRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendSixMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarSixMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarOneYearRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendOneYearRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendOneYearRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarOneYearRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarAllDataRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAllDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (trendAllDataRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarAllDataRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarPointerRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (polarPointerRadioButton.Checked)
                {
                    if (!trendPointerRadioButton.Checked)
                    {
                        trendPointerRadioButton.Checked = true;
                    }
                    polarWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                    polarPointerRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    polarPointerRadioButton.BackColor = trendPointerRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarPointerRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarZoomInRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (polarZoomInRadioButton.Checked)
                {
                    if (!trendZoomInRadioButton.Checked)
                    {
                        trendZoomInRadioButton.Checked = true;
                    }
                    polarWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                    polarZoomInRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    polarZoomInRadioButton.BackColor = trendZoomInRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarZoomInRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarZoomOutRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (polarZoomOutRadioButton.Checked)
                {
                    if (!trendZoomOutRadioButton.Checked)
                    {
                        trendZoomOutRadioButton.Checked = true;
                    }
                    polarWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                    polarZoomOutRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    polarZoomOutRadioButton.BackColor = trendZoomInRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarZoomOutRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarDrawGraphsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DrawPolarChart(this.polarWinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarDrawGraphsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarSaveGraphRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string dateTimeString = ConvertDateTimeToString(DateTime.Now);
                string filename = "Polar " + dateTimeString + ".png";
                polarSaveFileDialog.FileName = "Polar " + dateTimeString;
                polarSaveFileDialog.Filter = "PNG file (*.png)|*.png";
                polarSaveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                polarSaveFileDialog.AddExtension = true;
                polarSaveFileDialog.CheckPathExists = true;
                if (System.Windows.Forms.DialogResult.OK == polarSaveFileDialog.ShowDialog())
                {
                    filename = System.IO.Path.GetFullPath(polarSaveFileDialog.FileName);
                    System.Drawing.Image polarImage = polarWinChartViewer.Image;
                    polarImage.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarSaveGraphRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataSelectionPolar_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.dataSelectionPolar_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gammaPolarRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gammaPolarRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void temperatureCoefficientPolarRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (temperatureCoefficientPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawPolarChart(this.polarWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.temperatureCoefficientPolarRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void capacitanceAPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.capacitanceAPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void capacitanceBPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.capacitanceBPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void capacitanceCPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.capacitanceCPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void tangentAPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.tangentAPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void tangentBPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.tangentBPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void tangentCPolarRadCheckBox_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (polarAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawPolarChart(this.polarWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.tangentCPolarRadCheckBox_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Creates a chart for the trend data using all data that is associated with a checked checkbox in the "trend" tab view.
        /// </summary>
        /// <param name="chartViewer"></param>
        private void DrawPolarChart(WinChartViewer chartViewer)
        {
            try
            {
                int centerX;
                int centerY;
                int radius;

                int widthExtension = this.Width - this.startingFormWidth;
                int heightExtension = this.Height - this.startingFormHeight;

                int polarChartWidth = this.polarChartMinimumWidth + widthExtension;
                int polarChartHeight = this.polarChartMinimumHeight + heightExtension;

                this.polarGraphWidth = polarChartWidth;
                this.polarGraphHeight = polarChartHeight;

                int polarChartDiameter = Math.Min(polarChartHeight, polarChartWidth);

                int centerXoffset = 10;
                int centerYoffset = 0;

                if (chartViewer != null)
                {
                    this.polarGraphXStart = chartViewer.Location.X;
                    this.polarGraphYStart = chartViewer.Location.Y;

                    if (((this.totalReadingsGroup1 > 0) && (this.group1IsEnabled)) ||
                        ((this.totalReadingsGroup2 > 0) && (this.group2IsEnabled)))
                    {

                        this.polarPhasesSwappedRadLabel.Location = new Point(polarPhasesSwappedInitialX + widthExtension, polarPhasesSwappedInitialY);

                        /// Get active Y-axis information

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 1 - Configure overall chart appearance. 
                        ///////////////////////////////////////////////////////////////////////////////////////

                        // Create an polarChart object 680 x 450 pixels in size, with pale blue (0xf0f0ff) 
                        // background, black (000000) border, 1 pixel raised effect, and with a rounded frame.
                        PolarChart polarChart = new PolarChart(polarChartWidth, polarChartHeight, 0xf0f0ff, 0, 1);
                        polarChart.setRoundedFrame(Chart.CColor(BackColor));

                        centerX = (int)((0.5 - chartViewer.ViewPortLeft) * polarChartWidth / chartViewer.ViewPortWidth);
                        centerY = (int)((0.5 - chartViewer.ViewPortTop) * polarChartHeight / chartViewer.ViewPortHeight) + 25;

                        centerX += centerXoffset;
                        centerY += centerYoffset;

                       // radius = (int)((((double)polarChartDiameter * 0.87) / 2.0) / chartViewer.ViewPortWidth);

                        radius = (int)((((double)polarChartDiameter) / 2.0) / chartViewer.ViewPortHeight) - 60;

                        this.polarGraphCurrentCenterX = centerX;
                        this.polarGraphCurrentCenterY = centerY;
                        this.polarGraphCurrentRadius = radius;

                        polarChart.setPlotArea(centerX, centerY, radius, 0xffffff);

                        if (this.showRadialLabelsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                        {
                            polarChart.radialAxis().setLabelStyle("", 0, Chart.CColor(Color.Transparent));
                        }

                        // Set the plotarea at (55, 100) and of size 600 x 300 pixels. Use white (ffffff) 
                        // background. Enable both horizontal and vertical grids by setting their colors to 
                        // grey (cccccc). Set clipping mode to clip the data lines to the plot area.
                        //polarChart.setPlotArea(340, 265, 230, 0xffffff);
                        // polarChart.setClipping();

                        polarChart.setGridStyle(false, false);
                        polarChart.angularAxis().setLinearScale(0, 360, 30);

                        // Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                        // light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                        // polarChart.addTitle("Polar Chart", "Times New Roman Bold Italic", 15).setBackground(0xccccff, 0x0, Chart.glassEffect());

                        // Add a bottom title to the chart to show the date range of the axis, with a light blue 
                        // (ccccff) background.
                        //polarChart.addTitle2(Chart.Bottom, "From <*font=Arial Bold Italic*>"
                        //    + polarChart.formatValue(viewPortStartDate, "{value|mmm dd, yyyy}")
                        //    + "<*/font*> to <*font=Arial Bold Italic*>"
                        //    + polarChart.formatValue(viewPortEndDate, "{value|mmm dd, yyyy}")
                        //    + "<*/font*> (Duration <*font=Arial Bold Italic*>"
                        //    + Math.Round(viewPortEndDate.Subtract(viewPortStartDate).TotalSeconds / 86400.0)
                        //    + "<*/font*> days)", "Arial Italic", 10).setBackground(0xccccff);

                        // Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                        polarChart.addLegend(30, 10, false, "Arial Bold", 9).setBackground(Chart.CColor(Color.AntiqueWhite), Chart.Transparent);

                        //// Set axes width to 2 pixels
                        //polarChart.yAxis().setWidth(2);
                        //polarChart.xAxis().setWidth(2);

                        //// Add a title to the y-axis
                        //polarChart.yAxis().setTitle("Values", "Arial Bold", 9)

                        //lineLayer.setLineWidth(1);

                        //lineLayer.setXData(viewPortTimeStamps);

                        // add data

                        if ((this.trendGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                            (this.trendAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                        {
                            polarChart.addExtraField(dbDataGroup1.dataReadingDate);
                        }
                        else if (this.trendGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.addExtraField(dbDataGroup2.dataReadingDate);
                        }

                        #region Checkboxes - check the toggle state and add data if they are toggled on

                        if (this.gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            AddDataToPolarChart(dbDataGroup1.gamma, dbDataGroup1.gammaPhase, dbDataGroup2.gamma, dbDataGroup2.gammaPhase,
                                                "Gamma", gammaSymbol, Chart.CColor(Color.Blue), Chart.CColor(Color.Brown), polarChart);
                            if (group1IsEnabled)
                            {
                                if (group1GammaWarningThreshold > 0.0)
                                {
                                    polarChart.addLineLayer(new double[] { group1GammaWarningThreshold }, polarChart.dashLineColor(Chart.CColor(Color.Yellow), Chart.DashLine), gammaWarningThresholdLegendText + " " + setOneText);
                                    polarChart.radialAxis().addMark(group1GammaWarningThreshold, polarChart.dashLineColor(Chart.CColor(Color.Yellow), Chart.DashLine));
                                }
                                if (group1GammaAlarmThreshold > 0.0)
                                {
                                    polarChart.addLineLayer(new double[] { group1GammaAlarmThreshold }, polarChart.dashLineColor(Chart.CColor(Color.Red), Chart.DashLine), gammaAlarmThresholdLegendText + " " + setOneText);
                                    polarChart.radialAxis().addMark(group1GammaAlarmThreshold, polarChart.dashLineColor(Chart.CColor(Color.Red), Chart.DashLine));
                                }
                            }
                            if (group2IsEnabled)
                            {
                                if (group2GammaWarningThreshold > 0.0)
                                {
                                    polarChart.addLineLayer(new double[] { group2GammaWarningThreshold }, polarChart.dashLineColor(Chart.CColor(Color.Yellow), Chart.DotLine), gammaWarningThresholdLegendText + " " + setTwoText);
                                    polarChart.radialAxis().addMark(group2GammaWarningThreshold, polarChart.dashLineColor(Chart.CColor(Color.Yellow), Chart.DotLine));
                                }
                                if (group2GammaAlarmThreshold > 0.0)
                                {
                                    polarChart.addLineLayer(new double[] { group2GammaAlarmThreshold }, polarChart.dashLineColor(Chart.CColor(Color.Red), Chart.DotLine), gammaAlarmThresholdLegendText + " " + setTwoText);
                                    polarChart.radialAxis().addMark(group2GammaAlarmThreshold, polarChart.dashLineColor(Chart.CColor(Color.Red), Chart.DotLine));
                                }
                            }
                        }

                        if (this.temperatureCoefficientPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            AddDataToPolarChart(dbDataGroup1.temperatureCoefficient, dbDataGroup1.gammaPhase, dbDataGroup2.temperatureCoefficient, dbDataGroup2.gammaPhase,
                                                "Temp Coeff", temperatureCoefficientSymbol, Chart.CColor(Color.Blue), Chart.CColor(Color.Brown), polarChart);
                        }


                        if ((this.capacitanceAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                            (this.tangentAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                        {
                            polarChart.angularAxis().addZone(30, 60, Chart.CColor(Color.Yellow));
                        }
                        else if (this.capacitanceAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(345, 15, Chart.CColor(Color.Yellow));
                        }
                        else if (this.tangentAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(75, 105, Chart.CColor(Color.Yellow));
                        }

                        if ((this.capacitanceBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                            (this.tangentBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                        {
                            polarChart.angularAxis().addZone(150, 180, Chart.CColor(Color.Green));
                        }
                        else if (this.capacitanceBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(105, 135, Chart.CColor(Color.Green));
                        }
                        else if (this.tangentBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(195, 225, Chart.CColor(Color.Green));
                        }

                        if ((this.capacitanceCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) &&
                            (this.tangentCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                        {
                            polarChart.angularAxis().addZone(270, 300, Chart.CColor(Color.Red));
                        }
                        else if (this.capacitanceCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(225, 255, Chart.CColor(Color.Red));
                        }
                        else if (this.tangentCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            polarChart.angularAxis().addZone(315, 345, Chart.CColor(Color.Red));
                        }


                        #endregion

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 3 - Set up radial scale
                        ///////////////////////////////////////////////////////////////////////////////////////

                        //if (this.polarMinimumRadial == this.polarMaximumRadial)
                        //{
                        //    polarMinimumRadial = polarChart.radialAxis().getMinValue();
                        //    polarMaximumRadial = polarChart.radialAxis().getMaxValue();
                        //}
                        //else
                        //{
                        //    polarChart.radialAxis().setLinearScale(polarMinimumRadial, polarMaximumRadial);
                        //    polarChart.radialAxis().setRounding(false, false);
                        //}


                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 5 - Display the chart
                        ///////////////////////////////////////////////////////////////////////////////////////

                        this.polarChartPointer = null;

                        this.polarChartPointer = polarChart;

                        chartViewer.Chart = polarChart;
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.DrawPolarChart(WinChartViewer)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.DrawPolarChart(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToPolarChart(double[] radialDataGroup1, double[] angularDataGroup1,
                                         double[] radialDataGroup2, double[] angularDataGroup2,
                                         string name, int symbol, int colorGroup1, int colorGroup2,
                                         PolarChart polarChart)
        {
            try
            {
                string readingName = name;
                if (this.group1IsEnabled)
                {
                    //if (this.group2IsEnabled)
                    //{
                    readingName = name + " Set 1";
                    //}
                    AddSingleDataSetToPolarChart(radialDataGroup1, angularDataGroup1, readingName, symbol, colorGroup1, polarChart);
                }
                if (this.group2IsEnabled)
                {
                    //if (this.group1IsEnabled)
                    //{
                    readingName = name + " Set 2";
                    //}
                    AddSingleDataSetToPolarChart(radialDataGroup2, angularDataGroup2, readingName, symbol, colorGroup2, polarChart);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.AddDataToPolarChart(double[], double[], double[] double[], string, int, int, int, PolarChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddSingleDataSetToPolarChart(double[] radialData, double[] angularData, string name, int symbol, int color, PolarChart polarChart)
        {
            try
            {
                if (radialData != null)
                {
                    if (angularData != null)
                    {
                        if (polarChart != null)
                        {
                            PolarLineLayer polarLineLayer = polarChart.addLineLayer(radialData, color, name);
                            polarLineLayer.setAngles(angularData);
                            polarLineLayer.setDataSymbol(symbol, 8);
                            polarLineLayer.setLineWidth(0);
                        }
                        else
                        {
                            string errorMessage = "Error in BHMDataViewer.AddDataToPolarChart(double[], double[], string, int, int, PolarChart)\nInput PolarChart was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHMDataViewer.AddDataToPolarChart(double[], double[], string, int, int, PolarChart)\nSecond input double[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.AddDataToPolarChart(double[], double[], string, int, int, PolarChart)\nFirst input double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.AddDataToPolarChart(double[], double[], string, int, int, PolarChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Adjusts the width and scrolling range of the horizontal scrollbar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void polarRadHScrollBar_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // double scrollBarPosition = 0.0;
                double viewPortWidth = this.polarWinChartViewer.ViewPortWidth;
                if (hasFinishedInitialization)
                {
                    //// Set the view port based on the scroll bar
                    this.polarWinChartViewer.ViewPortLeft = ((double)(polarRadHScrollBar.Value - polarRadHScrollBar.Minimum))
                        / (polarRadHScrollBar.Maximum - polarRadHScrollBar.Minimum);

                    ///// I've had some experience with doing scroll bar handling, and you can get into
                    ///// weird states if you aren't careful.  The first two tests will hopefully alleviate
                    ///// that problem.
                    //if (polarRadHScrollBar.Value == polarRadHScrollBar.Maximum)
                    //{
                    //    this.polarWinChartViewer.ViewPortLeft = 1.0 - viewPortWidth;
                    //}
                    //else if (polarRadHScrollBar.Value == polarRadHScrollBar.Minimum)
                    //{
                    //    this.polarWinChartViewer.ViewPortLeft = 0.0;
                    //}
                    //else
                    //{
                    //    scrollBarPosition = ((double)(polarRadHScrollBar.Value - polarRadHScrollBar.Minimum)) /
                    //                        ((double)(polarRadHScrollBar.Maximum - polarRadHScrollBar.Minimum));
                    //    if ((scrollBarPosition + viewPortWidth) > 1.0)
                    //    {
                    //        scrollBarPosition = 1.0 - viewPortWidth;
                    //        polarRadHScrollBar.Value = polarRadHScrollBar.Maximum;
                    //    }
                    //    else if (scrollBarPosition < 0.0)
                    //    {
                    //        scrollBarPosition = 0.0;
                    //        polarRadHScrollBar.Value = polarRadHScrollBar.Minimum;
                    //    }

                    //    // if change in position is less than zero, the scroll bar has been moved to the right,
                    //    // otherwise it has been moved to the left
                    //    this.polarWinChartViewer.ViewPortLeft = scrollBarPosition;
                    //}

                    // Update the chart display without updating the image maps. (We can delay updating
                    // the image map until scrolling is completed and the chart display is stable.)
                    polarWinChartViewer.updateViewPort(true, false);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarRadHScrollBar_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarRadVScrollBar_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (hasFinishedInitialization)
                {
                    // Set the view port based on the scroll bar
                    polarWinChartViewer.ViewPortTop = ((double)(polarRadVScrollBar.Value - polarRadVScrollBar.Minimum))
                        / (polarRadVScrollBar.Maximum - polarRadVScrollBar.Minimum);

                    // Update the chart display without updating the image maps. (We can delay updating
                    // the image map until scrolling is completed and the chart display is stable.)
                    polarWinChartViewer.updateViewPort(true, false);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarRadVScrollBar_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// The ViewPortChanged event handler. This event occurs when the user changes the 
        /// WinChartViewer view port by dragging scrolling, or by zoom in/out, or the 
        /// WinChartViewer.updateViewPort method is being called.
        /// </summary>
        private void polarWinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {

                //double newWidth;
                //double newHeight;
                //double almostOne = 1.0;
                //double almostZero = 0.0;
                // First, settle the aspect ratio, choosing the larger of the two possible windows depending
                // on the values.
                //double localWidthToHeightAspectRatio = polarWinChartViewer.ViewPortWidth / polarWinChartViewer.ViewPortHeight;
                //if (localWidthToHeightAspectRatio < this.polarChartWidthToHeightAspectRatio)
                //{
                //    // zooming has induced a distortion to the width, so we will use the viewport height to compute a width 
                //    // in line with our aspect ratio
                //    newWidth = polarWinChartViewer.ViewPortHeight / polarChartWidthToHeightAspectRatio;
                //    // we have a new width, but we need to change the position of the left pointer
                //    polarWinChartViewer.ViewPortLeft -= newWidth / 2.0;
                //    polarWinChartViewer.ViewPortWidth = newWidth;
                //    //if ((polarWinChartViewer.ViewPortLeft + polarWinChartViewer.ViewPortWidth) > almostOne)
                //    //{
                //    //    polarWinChartViewer.ViewPortLeft = 1.0 - polarWinChartViewer.ViewPortWidth;
                //    //}
                //    //else if (polarWinChartViewer.ViewPortLeft < almostZero)
                //    //{
                //    //    polarWinChartViewer.ViewPortLeft = 0.0;
                //    //}
                //}
                //else
                //{
                //    newHeight = polarWinChartViewer.ViewPortHeight * polarChartWidthToHeightAspectRatio;
                //    polarWinChartViewer.ViewPortHeight = newHeight;
                //    polarWinChartViewer.ViewPortTop -= newHeight / 2.0;
                //    //if ((polarWinChartViewer.ViewPortTop + polarWinChartViewer.ViewPortHeight) > almostOne)
                //    //{
                //    //    polarWinChartViewer.ViewPortTop = 1.0 - polarWinChartViewer.Height;
                //    //}
                //    //else if (polarWinChartViewer.ViewPortTop < almostZero)
                //    //{
                //    //    polarWinChartViewer.ViewPortTop = 0.0;
                //    //}
                //}


                // Synchronize the horizontal scroll bar with the WinChartViewer
                polarRadHScrollBar.Enabled = polarWinChartViewer.ViewPortWidth < 1;
                polarRadHScrollBar.LargeChange = (int)Math.Ceiling(polarWinChartViewer.ViewPortWidth *
                    (polarRadHScrollBar.Maximum - polarRadHScrollBar.Minimum));
                polarRadHScrollBar.SmallChange = (int)Math.Ceiling(polarRadHScrollBar.LargeChange * 0.1);
                polarRadHScrollBar.Value = (int)Math.Round(polarWinChartViewer.ViewPortLeft *
                    (polarRadHScrollBar.Maximum - polarRadHScrollBar.Minimum)) + polarRadHScrollBar.Minimum;

                //// Synchronize the vertical scroll bar with the WinChartViewer
                polarRadVScrollBar.Enabled = polarWinChartViewer.ViewPortHeight < 1;
                polarRadVScrollBar.LargeChange = (int)Math.Ceiling(polarWinChartViewer.ViewPortHeight *
                    (polarRadVScrollBar.Maximum - polarRadVScrollBar.Minimum));
                polarRadVScrollBar.SmallChange = (int)Math.Ceiling(polarRadVScrollBar.LargeChange * 0.1);
                polarRadVScrollBar.Value = (int)Math.Round(polarWinChartViewer.ViewPortTop *
                    (polarRadVScrollBar.Maximum - polarRadVScrollBar.Minimum)) + polarRadVScrollBar.Minimum;

                //double centerX = polarWinChartViewer.ViewPortLeft + polarWinChartViewer.ViewPortWidth / 2;
                //double centerY = polarWinChartViewer.ViewPortTop + polarWinChartViewer.ViewPortHeight / 2;

                ////Aspect ratio and zoom factor
                //double aspectRatio = polarWinChartViewer.ViewPortWidth / polarWinChartViewer.ViewPortHeight;
                //// double zoomTo = ((double)zoomBar.Value) / zoomBar.Maximum;

                ////Zoom while respecting the aspect ratio
                ////polarWinChartViewer.ViewPortWidth = zoomTo * Math.Max(1, aspectRatio);
                ////polarWinChartViewer.ViewPortHeight = zoomTo * Math.Max(1, 1 / aspectRatio);

                ////Adjust ViewPortLeft and ViewPortTop to keep center point unchanged
                //polarWinChartViewer.ViewPortLeft = centerX - polarWinChartViewer.ViewPortWidth / 2;
                //polarWinChartViewer.ViewPortTop = centerY - polarWinChartViewer.ViewPortHeight / 2;

                // Update chart and image map if necessary
                if (e.NeedUpdateChart)
                {
                    DrawPolarChart(polarWinChartViewer);
                }
                if (e.NeedUpdateImageMap)
                {
                    UpdatePolarImageMap(polarWinChartViewer);
                }
                if (this.cursorIsEnabled)
                {
                    PolarUpdateCursorPosition();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarWinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Update the image map used on the chart.
        /// </summary>
        private void UpdatePolarImageMap(WinChartViewer viewer)
        {
            try
            {
                // Include tool tip for the chart
                if ((viewer != null) && (viewer.Chart != null) && (viewer.ImageMap == null))
                {
                    viewer.ImageMap = polarWinChartViewer.Chart.getHTMLImageMap("clickable", "",
                        "title='[{dataSetName}] {field0|mmm dd, yyyy hh:nn:ss}  deg = {x|2} val = {value|4}'");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.UpdatePolarImageMap(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gridDataEditRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    GridDataEnableEditing();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gridDataEditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deleteSelectedRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    BHM_Data_DataRoot foundDataRootEntry;
                    List<BHM_Data_DataRoot> selectedDataRootEntries = new List<BHM_Data_DataRoot>();
                    List<DateTime> selectedEntryDates = new List<DateTime>();
                    Dictionary<Guid, int> selectedEntries = GetAllSelectedGridEntries();
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        foreach (KeyValuePair<Guid, int> entry in selectedEntries)
                        {
                            foundDataRootEntry = BHM_DatabaseMethods.BHM_Data_GetDataRootTableEntry(entry.Key, localDB);
                            if (foundDataRootEntry != null)
                            {
                                selectedDataRootEntries.Add(foundDataRootEntry);
                                selectedEntryDates.Add(foundDataRootEntry.ReadingDateTime);
                            }
                        }
                        selectedEntryDates.Sort();
                        using (DisplayItemsToDelete deleteForm = new DisplayItemsToDelete(selectedEntryDates))
                        {
                            deleteForm.ShowDialog();
                            deleteForm.Hide();
                            if (deleteForm.DeleteSelectedData)
                            {
                                GetAllGridViewColumnWidths();
                                BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntries(selectedDataRootEntries, localDB);
                                // there may be another way rather than reloading the data, but it wouldn't be easier.
                                // one might be able to go through the binding source and remove items one by one, but 
                                // it could get sticky.
                                SetNewSQLConnectionAndGetCurrentData();
                                SetBindingSourceForGridView();
                                bhm_DataReadingsRadGridView.Refresh();
                                GridDataDisableEditing();
                                graphDataNeedsUpdating = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.deleteSelectedRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gridDataSaveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.insulationParametersBindingSource.EndEdit();
                this.insulationParametersDataAdapter.Update(this.insulationParametersDataTable);

                this.transformerSideParametersBindingSource.EndEdit();
                this.transformerSideParametersDataAdapter.Update(this.transformerSideParametersDataTable);

                GetAllGridViewColumnWidths();

                //this.bhm_DataReadingsBindingSource.EndEdit();
                //dbConnection.SubmitChanges();
                // SetBindingSourceAndSetSomeColumnsToReadOnly();
                // See comment for the cancel changes button on the topic of reloading data    
                SetNewSQLConnectionAndGetCurrentData();
                SetBindingSourceForGridView();
                bhm_DataReadingsRadGridView.Refresh();
                GridDataDisableEditing();
                graphDataNeedsUpdating = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gridDataSaveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gridDataCancelChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.insulationParametersBindingSource.CancelEdit();
                this.transformerSideParametersBindingSource.CancelEdit();

                GetAllGridViewColumnWidths();

                /// The only way to refresh the data is to reload it along a new connection.
                /// All other methods resulted in changes to the data in the gridview being
                /// "saved" in the connection, even though they were not saved in the database.
                SetNewSQLConnectionAndGetCurrentData();
                SetBindingSourceForGridView();
                bhm_DataReadingsRadGridView.Refresh();
                GridDataDisableEditing();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gridDataCancelChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

      


        //        private void radTabStrip1_TabSelecting(object sender, Telerik.WinControls.UI.TabCancelEventArgs args)
        //        {
        //            try
        //            {
        //                if (gridDataIsBeingEdited)
        //                {
        //                    RadMessageBox.Show(this, saveChangesOrCancelBeforeSwitchingTabsText);
        //                    args.Cancel = true;
        //                }
        //                else if (graphDataNeedsUpdating)
        //                {
        //                    ResetDatabaseConnectionAndReloadAndDisplayData();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHMDataViewer.radTabStrip1_TabSelecting(object, Telerik.WinControls.UI.TabCancelEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //private void clearGridDataRadButton_Click(object sender, EventArgs e)
        //{
        //    bhm_DataReadingsRadGridView.DataSource = null;
        //    bhm_DataReadingsBindingSource.DataSource = null;
        //}

        //private void assignGridDataRadButton_Click(object sender, EventArgs e)
        //{
        //    // LoadDatabaseData(DatabaseInterface.MinimumDateTime());
        //    SetNewSQLConnectionAndGetCurrentData();
        //    SetBindingSourceForGridView();
        //    //SetBindingSourceAndSetSomeColumnsToReadOnly();
        //}


        private string ConvertDateTimeToString(DateTime inputDateTime)
        {
            string targetDateTime = string.Empty;
            try
            {
                targetDateTime = inputDateTime.Year.ToString() + "-" +
                                 inputDateTime.Month.ToString() + "-" +
                                 inputDateTime.Day.ToString() + " " +
                                 inputDateTime.Hour.ToString() + "-" +
                                 inputDateTime.Minute.ToString() + "-" +
                                 inputDateTime.Second.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.ConvertDateTimeToString(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return targetDateTime;
        }

        private void BringAllPolarCursorsToFront()
        {
            try
            {
                polarHcursorGroup1Label.BringToFront();
                polarVcursorGroup1Label.BringToFront();
                polarHcursorGroup2Label.BringToFront();
                polarVcursorGroup2Label.BringToFront();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.BringAllPolarCursorsToFront()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarWinChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            try
            {
                if (polarWinChartViewer.MouseUsage == WinChartMouseUsage.ScrollOnDrag)
                {
                    if (this.cursorIsEnabled)
                    {
                        string angularValueAsString = e.AttrValues["x"].ToString();
                        string radialValueAsString = e.AttrValues["value"].ToString();

                        double angularValue = Double.Parse(angularValueAsString);
                        double radialValue = Double.Parse(radialValueAsString);

                        int candidateIndex = FindDateTimeIndexOfPolarCoordinates(angularValue, radialValue);

                        if (candidateIndex > -1)
                        {
                            this.cursorDateTimeAsDoubleIndex = candidateIndex;
                            this.cursorXval = this.allDateTimesAsDouble[this.cursorDateTimeAsDoubleIndex];
                            UpdateAllCursorPositions();
                        }

                        //if (polarEnableCursorRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        //{
                        //    string attrValueX = e.AttrValues["x"].ToString();
                        //    this.cursorXval = Double.Parse(attrValueX);
                        //    this.cursorDateTimeAsDoubleIndex = FindAllDateTimeAsDoubleIndex(this.cursorXval);
                        //    TrendUpdateCursorPosition();
                        //}
                    }
                    else
                    {
                        // new ParamViewer().Display(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarWinChartViewer_ClickHotSpot(object, WinHotSpotEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int FindDateTimeIndexOfPolarCoordinates(double angularValue, double radialValue)
        {
            int foundIndex = -1;
            try
            {
                int group1Index = -1;
                int group2Index = -1;

                if (this.group1IsEnabled)
                {
                    if (this.gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        group1Index = FindDateTimeIndexOfPolarCoordinatesForOneDataSet(angularValue, this.dbDataGroup1.gammaPhase, radialValue, this.dbDataGroup1.gamma);
                    }
                    else if (this.temperatureCoefficientPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        group1Index = FindDateTimeIndexOfPolarCoordinatesForOneDataSet(angularValue, this.dbDataGroup1.gammaPhase, radialValue, this.dbDataGroup1.temperatureCoefficient);
                    }
                }
                if (this.group2IsEnabled)
                {
                    if (this.gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        group2Index = FindDateTimeIndexOfPolarCoordinatesForOneDataSet(angularValue, this.dbDataGroup2.gammaPhase, radialValue, this.dbDataGroup2.gamma);
                    }
                    else if (this.temperatureCoefficientPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        group2Index = FindDateTimeIndexOfPolarCoordinatesForOneDataSet(angularValue, this.dbDataGroup2.gammaPhase, radialValue, this.dbDataGroup2.temperatureCoefficient);
                    }
                }

                if (group1Index > -1)
                {
                    foundIndex = group1Index;
                }
                else if (group2Index > -1)
                {
                    foundIndex = group2Index;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.FindDateTimeIndexOfPolarCoordinates(double, double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return foundIndex;
        }

        private int FindDateTimeIndexOfPolarCoordinatesForOneDataSet(double angularValue, double[] angularValueData, double radialValue, double[] radialValueData)
        {
            int foundIndex = -1;
            try
            {
                int arrayIndex = 0;
                int length;
                double epsilon = 0.015;
                bool searching = true;

                if (angularValueData != null)
                {
                    if (radialValueData != null)
                    {
                        length = angularValueData.Length;

                        // The input arrays are not in any kind of order, so one must use a direct search.  We
                        // assume that if the pair of points are both within epsilon for a given index, we have 
                        // found the correct point.  If the data are piled up on top of each other, we will find
                        // the first data point satisfying the condition.  With this kind of data, there really 
                        // isn't any way to differentiate between the data from a click.
                        while (searching & (arrayIndex < length))
                        {
                            if (Math.Abs(angularValueData[arrayIndex] - angularValue) < epsilon)
                            {
                                if (Math.Abs(radialValueData[arrayIndex] - radialValue) < epsilon)
                                {
                                    foundIndex = arrayIndex;
                                    searching = false;
                                }
                            }
                            arrayIndex++;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHMDataViewer.FindDateTimeIndexOfPolarCoordinatesForOneDataSet(double, double[], double, double[])\nSecond input double[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.FindDateTimeIndexOfPolarCoordinatesForOneDataSet(double, double[], double, double[])\nFirst input double[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.FindDateTimeIndexOfPolarCoordinatesForOneDataSet(double, double[], double, double[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return foundIndex;
        }

        private void polarWinChartViewer_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                UpdatePolarImageMap(this.polarWinChartViewer);
                polarChartHasMouseFocus = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarWinChartViewer_MouseEnter(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarWinChartViewer_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                polarChartHasMouseFocus = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.polarWinChartViewer_MouseLeave(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PolarUpdateCursorPosition()
        {
            try
            {
                double angularCoordinate = 0;
                double radialCoordinate = 0;

                if (hasFinishedInitialization)
                {
                    if ((this.cursorDateTimeAsDoubleIndex > -1) && (this.cursorDateTimeAsDoubleIndex < allDateTimesAsDouble.Length))
                    {
                        if (this.group1IsEnabled)
                        {
                            angularCoordinate = dbDataGroup1.gammaPhase[this.cursorDateTimeAsDoubleIndex];
                            if (gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                radialCoordinate = dbDataGroup1.gamma[this.cursorDateTimeAsDoubleIndex];
                            }
                            else
                            {
                                radialCoordinate = dbDataGroup1.temperatureCoefficient[this.cursorDateTimeAsDoubleIndex];
                            }
                            PolarUpdateCursorPostionForOneDataGroup(angularCoordinate, radialCoordinate, ref this.polarHcursorGroup1Label, ref this.polarVcursorGroup1Label);
                        }
                        if (this.group2IsEnabled)
                        {
                            angularCoordinate = dbDataGroup2.gammaPhase[this.cursorDateTimeAsDoubleIndex];
                            if (gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                radialCoordinate = dbDataGroup2.gamma[this.cursorDateTimeAsDoubleIndex];
                            }
                            else
                            {
                                radialCoordinate = dbDataGroup2.temperatureCoefficient[this.cursorDateTimeAsDoubleIndex];
                            }
                            PolarUpdateCursorPostionForOneDataGroup(angularCoordinate, radialCoordinate, ref this.polarHcursorGroup2Label, ref this.polarVcursorGroup2Label);
                        }
                    }
                    else
                    {
                        polarHcursorGroup1Label.Visible = false;
                        polarVcursorGroup1Label.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PolarUpdateCursorPosition()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void PolarUpdateCursorPostionForOneDataGroup(double angularCoordinate, double radialCoordinate, ref Label horizontalLabel, ref Label verticalLabel)
        {
            try
            {
                int polarChartX;
                int polarChartY;
                int size = 40;
                int halfSize = size / 2;
                int centerX;
                int centerY;

                int horizontalCursorStart;
                int verticalCursorStart;
                int cursorWidth = size;
                int cursorHeight = size;

                //int minXValue = this.polarGraphCurrentCenterX - this.polarGraphCurrentRadius;
                int maxXValue = this.polarGraphCurrentCenterX + this.polarGraphCurrentRadius;

                //int minYValue = this.polarGraphCurrentCenterY - this.polarGraphCurrentRadius;
                int maxYValue = this.polarGraphCurrentCenterY + this.polarGraphCurrentRadius;

                int chartWidth = this.polarGraphWidth;
                int chartHeight = this.polarGraphHeight;

                polarChartX = this.polarChartPointer.getXCoor(radialCoordinate, angularCoordinate);
                polarChartY = this.polarChartPointer.getYCoor(radialCoordinate, angularCoordinate);


                //if ((polarChartX > 0) && (polarChartX < this.trendChartMinimumWidth) &&
                //  (polarChartY > 0) && (polarChartY < this.trendChartMinimumHeight))
                if ((polarChartX > 0) && (polarChartX < this.polarGraphWidth) &&
                    (polarChartY > 0) && (polarChartY < this.polarGraphHeight))
                {
                    centerX = this.polarGraphXStart + polarChartX;
                    centerY = this.polarGraphYStart + polarChartY;

                    horizontalCursorStart = centerX - halfSize;
                    verticalCursorStart = centerY - halfSize;

                    if (polarChartX < halfSize)
                    {
                        cursorWidth = halfSize + polarChartX;
                        horizontalCursorStart += (halfSize - polarChartX);
                    }
                    else if ((polarChartX + halfSize) > this.polarGraphWidth)
                    {
                        cursorWidth = this.polarGraphWidth - polarChartX + halfSize;
                    }

                    if (polarChartY < halfSize)
                    {
                        cursorHeight = halfSize + polarChartY;
                        verticalCursorStart += (halfSize - polarChartY);
                    }
                    else if ((polarChartY + halfSize) > this.polarGraphHeight)
                    {
                        cursorHeight = this.polarGraphHeight - polarChartY + halfSize;
                    }


                    horizontalLabel.Location = new Point(horizontalCursorStart, centerY);
                    horizontalLabel.Size = new Size(cursorWidth, 1);

                    verticalLabel.Location = new Point(centerX, verticalCursorStart);
                    verticalLabel.Size = new Size(1, cursorHeight);

                    horizontalLabel.Visible = true;
                    verticalLabel.Visible = true;
                }
                else
                {
                    horizontalLabel.Visible = false;
                    verticalLabel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PolarUpdateCursorPostionForOneDataGroup(double, double, ref Label, ref Label)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataDisplayRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataDisplayRadPageView.SelectedPage == gridRadPageViewPage)
                {
                    bhm_DataReadingsRadGridView.Select();
                }
                else
                {
                    if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawBothCharts();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.dataDisplayRadPageView_SelectedPageChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataDisplayRadPageView_SelectedPageChanging(object sender, Telerik.WinControls.UI.RadPageViewCancelEventArgs e)
        {
            try
            {
                if (gridDataIsBeingEdited)
                {
                    RadMessageBox.Show(this, saveChangesOrCancelBeforeSwitchingTabsText);
                    e.Cancel = true;
                }
                else if (graphDataNeedsUpdating)
                {
                    this.trendChartNeedsUpdating = true;
                    this.polarChartNeedsUpdating = true;
                    this.graphDataNeedsUpdating = false;
                    ResetDatabaseConnectionAndReloadAndDisplayData();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.dataDisplayRadPageView_SelectedPageChanging(object, Telerik.WinControls.UI.RadPageViewCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_DataViewerPreferences CloneBHM_DataViewerPreferences(BHM_DataViewerPreferences otherMonitorPreferences)
        {
            BHM_DataViewerPreferences preferences = null;
            try
            {
                if (otherMonitorPreferences != null)
                {
                    preferences = new BHM_DataViewerPreferences();
                    preferences.Key = Guid.NewGuid();
                    preferences.MonitorID = this.monitorID;

                    preferences.PointerSelect = otherMonitorPreferences.PointerSelect;
                    preferences.DataAgeInMonths = otherMonitorPreferences.DataAgeInMonths;
                    preferences.DataGroup = otherMonitorPreferences.DataGroup;
                    preferences.CursorIsEnabled = otherMonitorPreferences.CursorIsEnabled;
                    preferences.AutoDrawIsEnabled = otherMonitorPreferences.AutoDrawIsEnabled;
                    preferences.ShowTrendLine = otherMonitorPreferences.ShowTrendLine;
                    preferences.ShowMovingAverage = otherMonitorPreferences.ShowMovingAverage;
                    preferences.ShowExponentialTrend = otherMonitorPreferences.ShowExponentialTrend;
                    preferences.MovingAverageItemCount = otherMonitorPreferences.MovingAverageItemCount;
                    preferences.TrendGammaSelected = otherMonitorPreferences.TrendGammaSelected;
                    preferences.TrendGammaPhaseSelected = otherMonitorPreferences.TrendGammaPhaseSelected;
                    preferences.TrendGammaTrendSelected = otherMonitorPreferences.TrendGammaTrendSelected;
                    preferences.TrendFrequencySelected = otherMonitorPreferences.TrendFrequencySelected;
                    preferences.TrendTemperatureSelected = otherMonitorPreferences.TrendTemperatureSelected;
                    preferences.TrendBushingCurrentASelected = otherMonitorPreferences.TrendBushingCurrentASelected;
                    preferences.TrendBushingCurrentBSelected = otherMonitorPreferences.TrendBushingCurrentBSelected;
                    preferences.TrendBushingCurrentCSelected = otherMonitorPreferences.TrendBushingCurrentCSelected;
                    preferences.TrendBushingCurrentPhaseAASelected = otherMonitorPreferences.TrendBushingCurrentPhaseAASelected;
                    preferences.TrendBushingCurrentPhaseABSelected = otherMonitorPreferences.TrendBushingCurrentPhaseABSelected;
                    preferences.TrendBushingCurrentPhaseACSelected = otherMonitorPreferences.TrendBushingCurrentPhaseACSelected;
                    preferences.TrendCapacitanceASelected = otherMonitorPreferences.TrendCapacitanceASelected;
                    preferences.TrendCapacitanceBSelected = otherMonitorPreferences.TrendCapacitanceBSelected;
                    preferences.TrendCapacitanceCSelected = otherMonitorPreferences.TrendCapacitanceCSelected;
                    preferences.TrendTangentASelected = otherMonitorPreferences.TrendTangentASelected;
                    preferences.TrendTangentBSelected = otherMonitorPreferences.TrendTangentBSelected;
                    preferences.TrendTangentCSelected = otherMonitorPreferences.TrendTangentCSelected;
                    preferences.TrendTemperatureCoefficientSelected = otherMonitorPreferences.TrendTemperatureCoefficientSelected;
                    preferences.TrendPhaseTemperatureCoefficientSelected = otherMonitorPreferences.TrendPhaseTemperatureCoefficientSelected;
                    preferences.TrendLTCPositionNumberSelected = otherMonitorPreferences.TrendLTCPositionNumberSelected;
                    preferences.TrendAlarmStatusSelected = otherMonitorPreferences.TrendAlarmStatusSelected;
                    preferences.TrendHumiditySelected = otherMonitorPreferences.TrendHumiditySelected;
                    preferences.TrendTemp1Selected = otherMonitorPreferences.TrendTemp1Selected;
                    preferences.TrendTemp2Selected = otherMonitorPreferences.TrendTemp2Selected;
                    preferences.TrendTemp3Selected = otherMonitorPreferences.TrendTemp3Selected;
                    preferences.TrendTemp4Selected = otherMonitorPreferences.TrendTemp4Selected;
                    preferences.TrendExtLoadActiveSelected = otherMonitorPreferences.TrendExtLoadActiveSelected;
                    preferences.TrendExtLoadReactiveSelected = otherMonitorPreferences.TrendExtLoadReactiveSelected;
                    preferences.PolarGammaSelected = otherMonitorPreferences.PolarGammaSelected;
                    preferences.PolarTemperatureCoefficientSelected = otherMonitorPreferences.PolarTemperatureCoefficientSelected;
                    preferences.PolarCapacitanceASelected = otherMonitorPreferences.PolarCapacitanceASelected;
                    preferences.PolarCapacitanceBSelected = otherMonitorPreferences.PolarCapacitanceBSelected;
                    preferences.PolarCapacitanceCSelected = otherMonitorPreferences.PolarCapacitanceCSelected;
                    preferences.PolarTangentASelected = otherMonitorPreferences.PolarTangentASelected;
                    preferences.PolarTangentBSelected = otherMonitorPreferences.PolarTangentBSelected;
                    preferences.PolarTangentCSelected = otherMonitorPreferences.PolarTangentCSelected;
                    preferences.PolarShowRadialLabelsSelected = otherMonitorPreferences.PolarShowRadialLabelsSelected;
                    preferences.DynamicsVariableNames = otherMonitorPreferences.DynamicsVariableNames;
                    preferences.DynamicsScaleFactors = otherMonitorPreferences.DynamicsScaleFactors;
                    preferences.DynamicsOperations = otherMonitorPreferences.DynamicsOperations;
                    preferences.TabSelected = otherMonitorPreferences.TabSelected;
                    preferences.DateSaved = otherMonitorPreferences.DateSaved;
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.CloneBHM_DataViewerPreferences(BHM_DataViewerPreferences)\nInput BHM_DataViewerPreferences was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.CloneBHM_DataViewerPreferences(BHM_DataViewerPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return preferences;
        }

        private void PreferencesLoad()
        {
            try
            {
                
                BHM_DataViewerPreferences preferences = null;
                BHM_DataViewerPreferences preferencesFromAnotherMonitor = null;
                using (MonitorInterfaceDB db = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    preferences = General_DatabaseMethods.BHM_DataViewerPreferences_Load(this.monitorID, db);
                    if (preferences == null)
                    {
                        preferencesFromAnotherMonitor = General_DatabaseMethods.BHM_DataViewerPreferences_LoadMostRecentlySavedPreferences(db);
                    }
                }
                if (preferencesFromAnotherMonitor != null)
                {
                    preferences = CloneBHM_DataViewerPreferences(preferencesFromAnotherMonitor);
                }

                if (preferences != null)
                {
                    // pointer selection
                    if (preferences.PointerSelect == 0)
                    {
                        trendPointerRadioButton.Checked = true;
                    }
                    else if (preferences.PointerSelect == 1)
                    {
                        trendZoomInRadioButton.Checked = true;
                    }
                    else if (preferences.PointerSelect == 2)
                    {
                        trendZoomOutRadioButton.Checked = true;
                    }

                    // data age selection
                    if (preferences.DataAgeInMonths == 3)
                    {
                        trendThreeMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataAgeInMonths == 6)
                    {
                        trendSixMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataAgeInMonths == 12)
                    {
                        trendOneYearRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataAgeInMonths == 0)
                    {
                        trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    // data group selection
                    if (preferences.DataGroup == 1)
                    {
                        trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataGroup == 2)
                    {
                        trendGroup2RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataGroup == 0)
                    {
                        trendAllGroupsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    //if (preferences.CursorIsEnabled)
                    //{
                    //    trendEnableCursorRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    //}
                    //else
                    //{
                    //    trendDisableCursorRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    //}

                    if (preferences.AutoDrawIsEnabled)
                    {
                        trendAutoDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        trendManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.ShowTrendLine)
                    {
                        trendLineRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.ShowMovingAverage)
                    {
                        movingAverageRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.ShowExponentialTrend)
                    {
                        exponentialTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        linearTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    movingAverageRadSpinEditor.Value = preferences.MovingAverageItemCount;

                    if (preferences.TrendGammaSelected)
                    {
                        gammaRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendGammaPhaseSelected)
                    {
                        gammaPhaseRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendGammaTrendSelected)
                    {
                        gammaTrendRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendFrequencySelected)
                    {
                        frequencyRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemperatureSelected)
                    {
                        temp2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentASelected)
                    {
                        bushingCurrentARadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentBSelected)
                    {
                        bushingCurrentBRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentCSelected)
                    {
                        bushingCurrentCRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentPhaseAASelected)
                    {
                        bushingCurrentPhaseAARadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentPhaseABSelected)
                    {
                        bushingCurrentPhaseABRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendBushingCurrentPhaseACSelected)
                    {
                        bushingCurrentPhaseACRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendCapacitanceASelected)
                    {
                        capacitanceARadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendCapacitanceBSelected)
                    {
                        capacitanceBRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendCapacitanceCSelected)
                    {
                        capacitanceCRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTangentASelected)
                    {
                        tangentARadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTangentBSelected)
                    {
                        tangentBRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTangentCSelected)
                    {
                        tangentCRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemperatureCoefficientSelected)
                    {
                        temperatureCoefficientRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendPhaseTemperatureCoefficientSelected)
                    {
                        phaseTemperatureCoefficientRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendHumiditySelected)
                    {
                        humidityRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp1Selected)
                    {
                        temp2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp2Selected)
                    {
                        temp1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp3Selected)
                    {
                        temp3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp4Selected)
                    {
                        temp4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }


                    if (preferences.PolarGammaSelected)
                    {
                        gammaPolarRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarTemperatureCoefficientSelected)
                    {
                        temperatureCoefficientPolarRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarCapacitanceASelected)
                    {
                        capacitanceAPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarCapacitanceBSelected)
                    {
                        capacitanceBPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarCapacitanceCSelected)
                    {
                        capacitanceCPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarTangentASelected)
                    {
                        tangentAPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarTangentBSelected)
                    {
                        tangentBPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarTangentCSelected)
                    {
                        tangentCPolarRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.PolarShowRadialLabelsSelected)
                    {
                        showRadialLabelsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    
                    if (preferences.TabSelected == 0)
                    {
                        dataDisplayRadPageView.SelectedPage = trendRadPageViewPage;
                    }
                    else if (preferences.TabSelected == 1)
                    {
                        dataDisplayRadPageView.SelectedPage = polarRadPageViewPage;
                    }
                    else if (preferences.TabSelected == 2)
                    {
                        dataDisplayRadPageView.SelectedPage = gridRadPageViewPage;
                    }

                    this.insulatationParametersGridViewColumnWidths = GetCorrectedColumnWidths(preferences.InsulationGridViewColumnWidths, this.insulatationParametersGridViewColumnWidths);
                    this.transformerSideParametersGridViewColumnWidths = GetCorrectedColumnWidths(preferences.TransformerGridViewColumnWidths, this.transformerSideParametersGridViewColumnWidths);

                    string[] variableNames = preferences.DynamicsVariableNames.Split(',');
                    if (variableNames.Length == 12)
                    {
                        this.humidityRadCheckBox.Text = variableNames[0];
                        this.temp1RadCheckBox.Text = variableNames[1];
                        this.temp2RadCheckBox.Text = variableNames[2];
                        this.temp3RadCheckBox.Text = variableNames[3];
                        this.temp4RadCheckBox.Text = variableNames[4];
                        this.loadCurrent1RadCheckBox.Text = variableNames[5];
                        this.loadCurrent2RadCheckBox.Text = variableNames[6];
                        this.loadCurrent3RadCheckBox.Text = variableNames[7];
                        this.voltage1RadCheckBox.Text = variableNames[8];
                        this.voltage2RadCheckBox.Text = variableNames[9];
                        this.voltage3RadCheckBox.Text = variableNames[10];
                        this.moistureContentRadCheckBox.Text = variableNames[11];
                    }

                    string[] scaleFactors = preferences.DynamicsScaleFactors.Split(',');
                    if (variableNames.Length == 12)
                    {
                        this.humidityScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[0]);
                        this.temp1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[1]);
                        this.temp2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[2]);
                        this.temp3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[3]);
                        this.temp4ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[4]);
                        this.loadCurrent1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[5]);
                        this.loadCurrent2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[6]);
                        this.loadCurrent3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[7]);
                        this.voltage1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[8]);
                        this.voltage2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[9]);
                        this.voltage3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[10]);
                        this.moistureScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[11]);
                    }

                    string[] operations = preferences.DynamicsOperations.Split(',');
                    if (variableNames.Length == 7)
                    {
                        this.humidityOperation = operations[0];
                        this.temp1Operation = operations[1];
                        this.temp2Operation = operations[2];
                        this.temp3Operation = operations[3];
                        this.temp4Operation = operations[4];
                        this.loadCurrent1Operation = operations[5];
                        this.loadCurrent2Operation = operations[6];
                        this.loadCurrent3Operation = operations[7];
                        this.voltage1Operation = operations[8];
                        this.voltage2Operation = operations[9];
                        this.voltage3Operation = operations[10];
                        this.moistureOperation = operations[11];
                    }
                   // System.Diagnostics.Debugger.Break();
                    string[] miscellaneous = preferences.MiscellaneousProperties.Split(';');
                    string[] pieces;
                    int colorValue;
                    this.miscellaneousProperties = new Dictionary<string, string>();
                    if ((miscellaneous != null) && (miscellaneous.Length > 0))
                    {
                        foreach (string entry in miscellaneous)
                        {
                            pieces = entry.Split(',');
                            if (pieces.Length == 2)
                            {
                                if (!this.miscellaneousProperties.ContainsKey(pieces[0].Trim()))
                                {
                                    this.miscellaneousProperties.Add(pieces[0].Trim(), pieces[1].Trim());
                                }
                            }
                        }

                        // moisture
                        if (this.miscellaneousProperties.ContainsKey(moistureRadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[moistureRadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.moistureContentRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.moistureContentRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // loadCurrent 1
                        if (this.miscellaneousProperties.ContainsKey(loadCurrent1RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[loadCurrent1RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.loadCurrent1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.loadCurrent1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        //loadCurrent 2
                        if (this.miscellaneousProperties.ContainsKey(loadCurrent2RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[loadCurrent2RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.loadCurrent2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.loadCurrent2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // loadCurrent 3
                        if (this.miscellaneousProperties.ContainsKey(loadCurrent3RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[loadCurrent3RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.loadCurrent3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.loadCurrent3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // voltage 1
                        if (this.miscellaneousProperties.ContainsKey(voltage1RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[voltage1RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.voltage1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.voltage1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        //voltage 2
                        if (this.miscellaneousProperties.ContainsKey(voltage2RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[voltage2RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.voltage2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.voltage2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // voltage 3
                        if (this.miscellaneousProperties.ContainsKey(voltage3RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[voltage3RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.voltage3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.voltage3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Potential error in BHMDataViewer.PreferencesLoad()\nDid not find any preferences in the DB.  That may not be an error.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PreferencesLoad()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int[] GetCorrectedColumnWidths(string columnWidthsAsString, int[] currentColumnWidthsAsInts)
        {
            int[] columnWidthsAsInt = null;
            try
            {
                columnWidthsAsInt = ConversionMethods.ConvertCommaSeparatedStringOfIntsToIntegerArray(columnWidthsAsString);
                if (columnWidthsAsInt == null)
                {
                    columnWidthsAsInt = currentColumnWidthsAsInts;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetCorrectedColumnWidths(string, int[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return columnWidthsAsInt;
        }

        private void PreferencesSave()
        {
            try
            {
               
                BHM_DataViewerPreferences preferences = new BHM_DataViewerPreferences();
                preferences.Key = Guid.NewGuid();
                preferences.MonitorID = this.monitorID;

                if (trendPointerRadioButton.Checked)
                {
                    preferences.PointerSelect = 0;
                }
                else if (trendZoomInRadioButton.Checked)
                {
                    preferences.PointerSelect = 1;
                }
                else if (trendZoomOutRadioButton.Checked)
                {
                    preferences.PointerSelect = 2;
                }

                if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 3;
                }
                else if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 6;
                }
                else if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 12;
                }
                else if (trendAllDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 0;
                }

                if (trendGroup1RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataGroup = 1;
                }
                else if (trendGroup2RadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataGroup = 2;
                }
                else if (trendAllGroupsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataGroup = 0;
                }

                preferences.CursorIsEnabled = this.cursorIsEnabled;

                preferences.AutoDrawIsEnabled = (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);

                preferences.ShowTrendLine = (trendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);

                preferences.ShowMovingAverage = (movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);

                preferences.ShowExponentialTrend = (exponentialTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);

                preferences.MovingAverageItemCount = (int)movingAverageRadSpinEditor.Value;

                /// trend data checkboxes
                preferences.TrendGammaSelected = (gammaRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendGammaPhaseSelected = (gammaPhaseRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendGammaTrendSelected = (gammaTrendRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendFrequencySelected = (frequencyRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemperatureSelected = (temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentASelected = (bushingCurrentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentBSelected = (bushingCurrentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentCSelected = (bushingCurrentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentPhaseAASelected = (bushingCurrentPhaseAARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentPhaseABSelected = (bushingCurrentPhaseABRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendBushingCurrentPhaseACSelected = (bushingCurrentPhaseACRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendCapacitanceASelected = (capacitanceARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendCapacitanceBSelected = (capacitanceBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendCapacitanceCSelected = (capacitanceCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTangentASelected = (tangentARadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTangentBSelected = (tangentBRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTangentCSelected = (tangentCRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemperatureCoefficientSelected = (temperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendPhaseTemperatureCoefficientSelected = (phaseTemperatureCoefficientRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendLTCPositionNumberSelected = false;
                preferences.TrendAlarmStatusSelected = false;
                preferences.TrendHumiditySelected = (humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemp1Selected = (temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemp2Selected = (temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemp3Selected = (temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendTemp4Selected = (temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.TrendExtLoadActiveSelected = false;
                preferences.TrendExtLoadReactiveSelected = false;

                /// polar data checkboxes
                preferences.PolarGammaSelected = (gammaPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarTemperatureCoefficientSelected = (temperatureCoefficientPolarRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarCapacitanceASelected = (capacitanceAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarCapacitanceBSelected = (capacitanceBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarCapacitanceCSelected = (capacitanceCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarTangentASelected = (tangentAPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarTangentBSelected = (tangentBPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarTangentCSelected = (tangentCPolarRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);
                preferences.PolarShowRadialLabelsSelected = (showRadialLabelsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On);

                if (dataDisplayRadPageView.SelectedPage == trendRadPageViewPage)
                {
                    preferences.TabSelected = 0;
                }
                else if (dataDisplayRadPageView.SelectedPage == polarRadPageViewPage)
                {
                    preferences.TabSelected = 1;
                }
                else if (dataDisplayRadPageView.SelectedPage == gridRadPageViewPage)
                {
                    preferences.TabSelected = 2;
                }

                preferences.InsulationGridViewColumnWidths = ConversionMethods.ConvertIntegerArrayToStringOfCommaSeparatedInts(this.insulatationParametersGridViewColumnWidths);
                preferences.TransformerGridViewColumnWidths = ConversionMethods.ConvertIntegerArrayToStringOfCommaSeparatedInts(this.transformerSideParametersGridViewColumnWidths);

                StringBuilder dynamicsVariableNames = new StringBuilder();
                dynamicsVariableNames.Append(this.humidityRadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp4RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.moistureContentRadCheckBox.Text);

                preferences.DynamicsVariableNames = dynamicsVariableNames.ToString();

                StringBuilder dynamicsScaleFactors = new StringBuilder();
                dynamicsScaleFactors.Append(this.humidityScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp4ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.moistureScaleFactor.ToString());

                preferences.DynamicsScaleFactors = dynamicsScaleFactors.ToString();

                StringBuilder dynamicsOperations = new StringBuilder();
                dynamicsOperations.Append(this.humidityOperation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp4Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage3Operation);
                dynamicsOperations.Append(",");

                dynamicsOperations.Append(this.moistureOperation);

                preferences.DynamicsOperations = dynamicsOperations.ToString();

                StringBuilder miscellaneousPropertiesEntries = new StringBuilder();
                // moisture
                miscellaneousPropertiesEntries.Append(moistureRadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.moistureContentRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // load Current 1
                miscellaneousPropertiesEntries.Append(loadCurrent1RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // load Current 2
                miscellaneousPropertiesEntries.Append(loadCurrent2RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // load Current 3
                miscellaneousPropertiesEntries.Append(loadCurrent3RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // voltage 1
                miscellaneousPropertiesEntries.Append(voltage1RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // voltage 2
                miscellaneousPropertiesEntries.Append(voltage2RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // voltage 3
                miscellaneousPropertiesEntries.Append(voltage3RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
              
                preferences.MiscellaneousProperties = miscellaneousPropertiesEntries.ToString();
                preferences.DateSaved = DateTime.Now;

                using (MonitorInterfaceDB db = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    General_DatabaseMethods.BHM_DataViewerPreferences_Save(preferences, db);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.PreferencesSave()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void movingAverageRadSpinEditor_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    if (this.trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer, true);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.movingAverageRadSpinEditor_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gridExpandAllRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.bhm_DataReadingsRadGridView.MasterTemplate.Templates != null) &&
                    (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0))
                {
                    foreach (GridViewRowInfo row in this.bhm_DataReadingsRadGridView.Rows)
                    {
                        row.IsExpanded = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gridExpandAllRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void gridHideAllRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.bhm_DataReadingsRadGridView.MasterTemplate.Templates != null) &&
                   (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0))
                {
                    foreach (GridViewRowInfo row in this.bhm_DataReadingsRadGridView.Rows)
                    {
                        row.IsExpanded = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.gridHideAllRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void BHM_DataViewer_SizeChanged(object sender, EventArgs e)
        {
            this.trendChartNeedsUpdating = true;
            this.polarChartNeedsUpdating = true;
            UpdateAllCursorPositions();
            if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                DrawBothCharts();
            }
        }

        #region WinChartViewer Context Menus

        private void trendWinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref trendRadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.trendWinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void polarWinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref polarRadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.polarWinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetupTrendContextMenu()
        {
            RadMenuItem menuItem;
            // we can eventually switch on the monitorTypeString if we need to customize
            // the context menu for specific monitor types
            trendRadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += trendCopyGraph_Click;
            menuItem.Text = copyGraphText;
            trendRadContextMenu.Items.Add(menuItem);
        }

        private void trendCopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (trendWinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(trendWinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.trendCopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetupPolarContextMenu()
        {
            RadMenuItem menuItem;
            // we can eventually switch on the monitorTypeString if we need to customize
            // the context menu for specific monitor types
            polarRadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += polarCopyGraph_Click;
            menuItem.Text = copyGraphText;
            polarRadContextMenu.Items.Add(menuItem);
        }

        private void polarCopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (polarWinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(polarWinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.polarCopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    graphRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DynamicsRadGroupBoxAssignContextMenuItems()
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                this.dynamicsRadGroupBoxRadContextMenuStrip.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += dyamicsRadGroupBoxOpenEditMenu_Click;
                menuItem.Text = "Open dynamics editor";
                this.dynamicsRadGroupBoxRadContextMenuStrip.Items.Add(menuItem);

                //menuItem = new RadMenuItem();
                //menuItem.Click += channelSelectRadGroupBoxUnselectActive_Click;
                //menuItem.Text = "Unselect all active channels";
                //channelSelectRadGroupBoxRadContextMenuStrip.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.DynamicsRadGroupBoxAssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dyamicsRadGroupBoxOpenEditMenu_Click(object sender, EventArgs e)
        {
            try
            {
                using (DynamicsNamesEditor namesEditor = new DynamicsNamesEditor(this.programBrand, this.programType, DataViewerType.BHM, this.humidityRadCheckBox.Text, this.moistureContentRadCheckBox.Text, this.temp1RadCheckBox.Text, this.temp2RadCheckBox.Text,
                   this.temp3RadCheckBox.Text, this.temp4RadCheckBox.Text, this.loadCurrent1RadCheckBox.Text, this.loadCurrent2RadCheckBox.Text, this.loadCurrent3RadCheckBox.Text,
                   this.voltage1RadCheckBox.Text, this.voltage2RadCheckBox.Text, this.voltage3RadCheckBox.Text,
                   this.humidityScaleFactor, this.moistureScaleFactor, this.temp1ScaleFactor, this.temp2ScaleFactor, this.temp3ScaleFactor, this.temp4ScaleFactor,
                   this.loadCurrent1ScaleFactor, this.loadCurrent2ScaleFactor, this.loadCurrent3ScaleFactor,
                   this.voltage1ScaleFactor, this.voltage2ScaleFactor, this.voltage3ScaleFactor,
                   this.humidityOperation, this.moistureOperation, this.temp1Operation, this.temp2Operation, this.temp3Operation, this.temp4Operation,
                   this.loadCurrent1Operation, this.loadCurrent2Operation, this.loadCurrent3Operation,
                   this.voltage1Operation, this.voltage2Operation, this.voltage3Operation))
                {
                    namesEditor.ShowDialog();
                    namesEditor.Hide();
                    if (namesEditor.ResetValues)
                    {
                        this.humidityRadCheckBox.Text = namesEditor.HumidityNewText;
                        this.moistureContentRadCheckBox.Text = namesEditor.MoistureNewText;
                        this.temp1RadCheckBox.Text = namesEditor.Temp1NewText;
                        this.temp2RadCheckBox.Text = namesEditor.Temp2NewText;
                        this.temp3RadCheckBox.Text = namesEditor.Temp3NewText;
                        this.temp4RadCheckBox.Text = namesEditor.Temp4NewText;
                        this.loadCurrent1RadCheckBox.Text = namesEditor.LoadCurrent1NewText;
                        this.loadCurrent2RadCheckBox.Text = namesEditor.LoadCurrent2NewText;
                        this.loadCurrent3RadCheckBox.Text = namesEditor.LoadCurrent3NewText;
                        this.voltage1RadCheckBox.Text = namesEditor.Voltage1NewText;
                        this.voltage2RadCheckBox.Text = namesEditor.Voltage2NewText;
                        this.voltage3RadCheckBox.Text = namesEditor.Voltage3NewText;

                        this.humidityScaleFactor = namesEditor.HumidityNewScaleFactor;
                        this.moistureScaleFactor = namesEditor.MoistureNewScaleFactor;
                        this.temp1ScaleFactor = namesEditor.Temp1NewScaleFactor;
                        this.temp2ScaleFactor = namesEditor.Temp2NewScaleFactor;
                        this.temp3ScaleFactor = namesEditor.Temp3NewScaleFactor;
                        this.temp4ScaleFactor = namesEditor.Temp4NewScaleFactor;
                        this.loadCurrent1ScaleFactor = namesEditor.LoadCurrent1NewScaleFactor;
                        this.loadCurrent2ScaleFactor = namesEditor.LoadCurrent2NewScaleFactor;
                        this.loadCurrent3ScaleFactor = namesEditor.LoadCurrent3NewScaleFactor;
                        this.voltage1ScaleFactor = namesEditor.Voltage1NewScaleFactor;
                        this.voltage2ScaleFactor = namesEditor.Voltage2NewScaleFactor;
                        this.voltage3ScaleFactor = namesEditor.Voltage3NewScaleFactor;

                        this.humidityOperation = namesEditor.HumidityNewOperation;
                        this.moistureOperation = namesEditor.MoistureNewOperation;
                        this.temp1Operation = namesEditor.Temp1NewOperation;
                        this.temp2Operation = namesEditor.Temp2NewOperation;
                        this.temp3Operation = namesEditor.Temp3NewOperation;
                        this.temp4Operation = namesEditor.Temp4NewOperation;
                        this.loadCurrent1Operation = namesEditor.LoadCurrent1NewOperation;
                        this.loadCurrent2Operation = namesEditor.LoadCurrent2NewOperation;
                        this.loadCurrent3Operation = namesEditor.LoadCurrent3NewOperation;
                        this.voltage1Operation = namesEditor.Voltage1NewOperation;
                        this.voltage2Operation = namesEditor.Voltage2NewOperation;
                        this.voltage3Operation = namesEditor.Voltage3NewOperation;

                        //InitializeInsulationParametersGridViewColumnHeaderText();
                        //InitializeTransformerSideParametersGridViewColumnHeaderText();
                        SetInsulationParametersGridViewColumnHeaderText();
                        SetTransformerSideParametersGridViewColumnHeaderText();

                        this.trendChartNeedsUpdating = true;
                        DrawTrendChart(this.trendWinChartViewer, true);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.dyamicsRadGroupBoxOpenEditMenu_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dynamicsRadGroupBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    dynamicsRadGroupBoxRadContextMenuStrip.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.dynamicsRadGroupBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        private void SaveSelectedDataToCSVFile(DateTime oldestDataDate)
        {
            try
            {
               // List<BHM_Data_TransformerSideParameters> dataList;
                List<BHM_SingleDataReading> dataReadingsList;
                List<string> selectedData;
                StringBuilder outputLine;
                string header;
                string fileNamePrefix = string.Empty;

                ErrorCode errorCode = ErrorCode.None;
                MonitorHierarchy monitorHierarchy;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    dataReadingsList = DataConversion.BHM_GetAllDataForOneMonitor(this.monitorID, oldestDataDate, localDB);
                    //dataList = BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(this.monitorID, oldestDataDate, localDB);

                    monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(this.monitorID, localDB);

                    if ((monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                    {
                        fileNamePrefix = monitorHierarchy.company.Name + "-" + monitorHierarchy.plant.Name + "-" + monitorHierarchy.equipment.Name + "-BHM-Data-";
                    }
                }

                if((dataReadingsList != null) &&(dataReadingsList.Count> 0) && (monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                {
                //if ((dataList != null) && (dataList.Count > 0) && (monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                //{
                    selectedData = new List<string>();

                    header = "Reading DateTime, Side Number, Humidity, Temperature_0, Temperature_1, Temperature_2, Temperature_3, Current_0, Current_1, Current_2, Gamma, Gamma Phase, C_0, C_1, C_2, Tg_0, Tg_1, Tg_2, Source Amplitude 0, Source Amplitude 1, Source Amplitude 2, Source Phase 0, Source Phase 1, Source Phase 2";

                    foreach(BHM_SingleDataReading dataReading in dataReadingsList)
                    {
                    //foreach (BHM_Data_TransformerSideParameters entry in dataList)
                    //{
                          selectedData.Add(GetOneLineOfData(dataReading, 0));

                        //outputLine = new StringBuilder();

                      

                        //outputLine.Append(entry.ReadingDateTime);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SideNumber + 1);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.Gamma);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.GammaPhase);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.C_0);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.C_1);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.C_2);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.Tg_0);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.Tg_1);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.Tg_2);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourceAmplitude_0);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourceAmplitude_1);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourceAmplitude_2);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourcePhase_0);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourcePhase_1);
                        //outputLine.Append(", ");
                        //outputLine.Append(entry.SourcePhase_2);

                        //selectedData.Add(outputLine.ToString());
                    }

                    foreach (BHM_SingleDataReading dataReading in dataReadingsList)
                    {
                        selectedData.Add(GetOneLineOfData(dataReading, 1));
                    }

                    FileUtilities.SaveCSVFileToDatedString(fileNamePrefix, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), selectedData, header);
                }
                else if ((monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                {
                    RadMessageBox.Show(this, failedToReadAnyDataToSaveToCsvFile);
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SaveSelectedDataToCSVFile(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private string GetOneLineOfData(BHM_SingleDataReading dataReading, int sideNumber)
        {
            string oneLineOfData = string.Empty;
            try
            {
                BHM_DataComponent_TransformerSideParameters transformerSideParameters = null;
                StringBuilder oneLineOfDataStringBuilder;
                if ((dataReading != null) && (((dataReading.transformerSideParametersSideOne != null) && (sideNumber == 0)) || 
                                              ((dataReading.transformerSideParametersSideTwo != null) && (sideNumber == 1))))
                {
                    transformerSideParameters = null;
                    if (sideNumber == 0)
                    {
                        transformerSideParameters = dataReading.transformerSideParametersSideOne;
                    }
                    else if (sideNumber == 1)
                    {
                        transformerSideParameters = dataReading.transformerSideParametersSideTwo;
                    }
                    else
                    {
                        string errorMessage = "Error in BHMDataViewer.GetOneLineOfData(BHM_SingleDataReading, int)\nParameter sideNumber has a bad value";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                    if (transformerSideParameters != null)
                    {
                        oneLineOfDataStringBuilder = new StringBuilder();

                        oneLineOfDataStringBuilder.Append(dataReading.readingDateTime);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(sideNumber);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Humidity);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Temperature_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Temperature_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Temperature_2);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Temperature_3);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Current_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.Current_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(dataReading.insulationParameters.CurrentR_1);
                        oneLineOfDataStringBuilder.Append(", ");

                        //Gamma, Gamma Phase, C_0, C_1, C_2, Tg_0, Tg_1, Tg_2, Source Amplitude 0, Source Amplitude 1, Source Amplitude 2, Source Phase 0, Source Phase 1, Source Phase 2";

                        oneLineOfDataStringBuilder.Append(transformerSideParameters.Gamma);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.GammaPhase);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.C_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.C_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.C_2);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.Tg_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.Tg_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.Tg_2);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourceAmplitude_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourceAmplitude_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourceAmplitude_2);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourcePhase_0);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourcePhase_1);
                        oneLineOfDataStringBuilder.Append(", ");
                        oneLineOfDataStringBuilder.Append(transformerSideParameters.SourcePhase_2);




                        oneLineOfData = oneLineOfDataStringBuilder.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.GetOneLineOfData(BHM_SingleDataReading, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return oneLineOfData;
        }


        private void saveDataToCsvRadButton_Click(object sender, EventArgs e)
        {
            SaveSelectedDataToCSVFile(this.dataCutoffDate);
        }

        private MonitorHierarchy SetFormTitleToReflectDataSource(MonitorInterfaceDB localDB)
        {
            MonitorHierarchy hierarchy = null;
            try
            {
                string titleString = bhmDataViewerInterfaceTitleText;
                hierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(this.monitorID, localDB);
                List<string> companyPlantAndEquipment = General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy(hierarchy);
                if (companyPlantAndEquipment.Count == 3)
                {
                    foreach (string entry in companyPlantAndEquipment)
                    {
                        titleString += " - " + entry;
                    }
                }                
                this.Text = titleString;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetFormTitleToReflectDataSource(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchy;
        }

        private void LoadWarningAndAlarmThresholdValues(MonitorInterfaceDB localDB)
        {
            try
            {
                Guid desiredConfigurationRootID = Guid.Empty;
                List<BHM_Config_ConfigurationRoot> configurationRootEntries;
                List<BHM_Config_GammaSideSetup> gammaSideSetups;
                //using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionString))
                //{
                configurationRootEntries = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(this.monitorID, localDB);
                foreach (BHM_Config_ConfigurationRoot entry in configurationRootEntries)
                {
                    if (entry.Description.Trim().CompareTo(BHM_MonitorConfiguration.CurrentConfigName.Trim()) == 0)
                    {
                        desiredConfigurationRootID = entry.ID;
                        break;
                    }
                }

                if (desiredConfigurationRootID != Guid.Empty)
                {
                    gammaSideSetups = BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupEntriesForOneConfiguration(desiredConfigurationRootID, localDB);
                    foreach (BHM_Config_GammaSideSetup entry in gammaSideSetups)
                    {
                        if (entry.SideNumber == 0)
                        {
                            group1GammaWarningThreshold = entry.GammaYellowThreshold / 20.0;
                            group1GammaAlarmThreshold = entry.GammaRedThreshold / 20.0;
                        }
                        else
                        {
                            group2GammaWarningThreshold = entry.GammaYellowThreshold / 20.0;
                            group2GammaAlarmThreshold = entry.GammaRedThreshold / 20.0;
                        }
                    }
                }
                else
                {
                    RadMessageBox.Show(this, warningAndAlarmThresholdsNotAvailableText);
                }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.LoadWarningAndAlarmThresholdValues(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void createCorrelationGraphRadButton_Click(object sender, EventArgs e)
        {
           // RadMessageBox.Show(this, "Feature not finished");
            CreateCorrelationsGraphs(this.trendWinChartViewer);
        }

        private void CreateCorrelationsGraphs(WinChartViewer chartViewer)
        {
            try
            {
                /// This code creates correlations for specific measured data that is currently visible
                /// in the viewport.  Note that when the viewport changes, the graphs created will change as
                /// well.


                DateTime viewPortStartDate = minimumDateOfDataSet.AddSeconds(Math.Round(chartViewer.ViewPortLeft * dateRange));
                DateTime viewPortEndDate = viewPortStartDate.AddSeconds(Math.Round(chartViewer.ViewPortWidth * dateRange));

                string dataDateInterval = viewPortStartDate.ToString() + " - " + viewPortEndDate.ToString();

                // Get the starting index of the array using the start date
                int commonDataStartIndex = FindGraphStartDateIndex(viewPortStartDate, this.commonData.dataReadingDate);
                int group1StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup1.dataReadingDate);
                int group2StartIndex = FindGraphStartDateIndex(viewPortStartDate, this.dbDataGroup2.dataReadingDate);

                // Get the ending index of the array using the end date
                int commonDataEndIndex = FindGraphEndDateIndex(viewPortEndDate, this.commonData.dataReadingDate);
                int group1EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup1.dataReadingDate);
                int group2EndIndex = FindGraphEndDateIndex(viewPortEndDate, this.dbDataGroup2.dataReadingDate);

                // Get the length
                int numberOfCommonDataPointsBeingGraphed = commonDataEndIndex - commonDataStartIndex + 1;
                int numberOfGroup1DataPointsBeingGraphed = group1EndIndex - group1StartIndex + 1;
                int numberOfGroup2DataPointsBeingGraphed = group2EndIndex - group2StartIndex + 1;

                double[] humidityDataScaled = ScaleDynamicsValues(commonData.humidity, this.humidityScaleFactor, this.humidityOperation);
                double[] moistureDataScaled = ScaleDynamicsValues(commonData.moisture, this.moistureScaleFactor, this.moistureOperation);
                double[] temp1DataScaled = ScaleDynamicsValues(commonData.temp1, this.temp1ScaleFactor, this.temp1Operation);
                double[] loadCurrent1Scaled = ScaleDynamicsValues(commonData.loadCurrent1, this.loadCurrent1ScaleFactor, this.loadCurrent1Operation);

                double[] humidityDataScaledGroup1Dates = new double[numberOfGroup1DataPointsBeingGraphed];
                double[] humidityDataScaledGroup2Dates = new double[numberOfGroup2DataPointsBeingGraphed];
                double[] moistureDataScaledGroup1Dates = new double[numberOfGroup1DataPointsBeingGraphed];
                double[] moistureDataScaledGroup2Dates = new double[numberOfGroup2DataPointsBeingGraphed];
                double[] temp1DataScaledGroup1Dates = new double[numberOfGroup1DataPointsBeingGraphed];
                double[] temp1DataScaledGroup2Dates = new double[numberOfGroup2DataPointsBeingGraphed];
                double[] loadCurrent1ScaledGroup1Dates = new double[numberOfGroup1DataPointsBeingGraphed];
                double[] loadCurrent1ScaledGroup2Dates = new double[numberOfGroup2DataPointsBeingGraphed];

                double[] gammaDataGroup1 = new double[numberOfGroup1DataPointsBeingGraphed];
                double[] gammaDataGroup2 = new double[numberOfGroup2DataPointsBeingGraphed];

                double correlation;
                double[] humidityAndMagnitudeMvCorrelationData = new double[2];
                double[] moistureAndMagnitudeMvCorrelationData = new double[2];
                double[] temp1AndMagnitudeMvCorrelationData = new double[2];
                double[] loadCurrent1AndMagnitudeMvCorrelationData = new double[2];

                List<string> correlationNames = new List<string>();
                List<double[]> magnitudeMvCorrelations = new List<double[]>();
                List<string> setLabels = new List<string>();

                Array.Copy(this.dbDataGroup1.gamma, group1StartIndex, gammaDataGroup1, 0, numberOfGroup1DataPointsBeingGraphed);
                Array.Copy(this.dbDataGroup2.gamma, group2StartIndex, gammaDataGroup2, 0, numberOfGroup2DataPointsBeingGraphed);

                /// This code is here to handle potential missing data in either set 1 or set 2.  I don't know if that can happen the
                /// way I have set the code up, but it's possible future changes could make it happen.

                if (numberOfCommonDataPointsBeingGraphed == numberOfGroup1DataPointsBeingGraphed)
                {
                    Array.Copy(humidityDataScaled, group1StartIndex, humidityDataScaledGroup1Dates, 0, numberOfGroup1DataPointsBeingGraphed);
                    Array.Copy(moistureDataScaled, group1StartIndex, moistureDataScaledGroup1Dates, 0, numberOfGroup1DataPointsBeingGraphed);
                    Array.Copy(temp1DataScaled, group1StartIndex, temp1DataScaledGroup1Dates, 0, numberOfGroup1DataPointsBeingGraphed);
                    Array.Copy(loadCurrent1Scaled, group1StartIndex, loadCurrent1ScaledGroup1Dates, 0, numberOfGroup1DataPointsBeingGraphed);
                }
                else
                {
                    int j = commonDataStartIndex;
                    int k = 0;
                    for (int i = group1StartIndex; i <= group1EndIndex; i++)
                    {
                        while (commonData.dataReadingDate[j].CompareTo(this.dbDataGroup1.dataReadingDate[i]) < 0)
                        {
                            j++;
                        }

                        humidityDataScaledGroup1Dates[k] = humidityDataScaled[j];
                        moistureDataScaledGroup1Dates[k] = moistureDataScaled[j];
                        temp1DataScaledGroup1Dates[k] = temp1DataScaled[j];
                        loadCurrent1ScaledGroup1Dates[k] = loadCurrent1Scaled[j];
                        k++;
                    }
                }

                if (numberOfCommonDataPointsBeingGraphed == numberOfGroup2DataPointsBeingGraphed)
                {
                    Array.Copy(humidityDataScaled, group2StartIndex, humidityDataScaledGroup2Dates, 0, numberOfGroup2DataPointsBeingGraphed);
                    Array.Copy(moistureDataScaled, group2StartIndex, moistureDataScaledGroup2Dates, 0, numberOfGroup2DataPointsBeingGraphed);
                    Array.Copy(temp1DataScaled, group2StartIndex, temp1DataScaledGroup2Dates, 0, numberOfGroup2DataPointsBeingGraphed);
                    Array.Copy(loadCurrent1Scaled, group2StartIndex, loadCurrent1ScaledGroup2Dates, 0, numberOfGroup2DataPointsBeingGraphed);
                }
                else
                {
                    int j = commonDataStartIndex;
                    int k = 0;
                    for (int i = group2StartIndex; i <= group2EndIndex; i++)
                    {
                        while (commonData.dataReadingDate[j].CompareTo(this.dbDataGroup2.dataReadingDate[i]) < 0)
                        {
                            j++;
                        }

                        humidityDataScaledGroup2Dates[k] = humidityDataScaled[j];
                        moistureDataScaledGroup2Dates[k] = moistureDataScaled[j];
                        temp1DataScaledGroup2Dates[k] = temp1DataScaled[j];
                        loadCurrent1ScaledGroup2Dates[k] = loadCurrent1Scaled[j];
                        k++;
                    }
                }

                correlation = Statistics.ComputePearsonCorrelation(humidityDataScaledGroup1Dates, 0.0, gammaDataGroup1, 0.0);
                humidityAndMagnitudeMvCorrelationData[0] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(moistureDataScaledGroup1Dates, 0.0, gammaDataGroup1, 0.0);
                moistureAndMagnitudeMvCorrelationData[0] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(temp1DataScaledGroup1Dates, -70.0, gammaDataGroup1, 0.0);
                temp1AndMagnitudeMvCorrelationData[0] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(loadCurrent1ScaledGroup1Dates, 0.0, gammaDataGroup1, 0.0);
                loadCurrent1AndMagnitudeMvCorrelationData[0] = correlation;


                correlation = Statistics.ComputePearsonCorrelation(humidityDataScaledGroup2Dates, 0.0, gammaDataGroup2, 0.0);
                humidityAndMagnitudeMvCorrelationData[1] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(moistureDataScaledGroup2Dates, 0.0, gammaDataGroup2, 0.0);
                moistureAndMagnitudeMvCorrelationData[1] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(temp1DataScaledGroup2Dates, -70.0, gammaDataGroup2, 0.0);
                temp1AndMagnitudeMvCorrelationData[1] = correlation;

                correlation = Statistics.ComputePearsonCorrelation(loadCurrent1ScaledGroup2Dates, 0.0, gammaDataGroup2, 0.0);
                loadCurrent1AndMagnitudeMvCorrelationData[1] = correlation;

                correlationNames.Add(humidityRadCheckBox.Text);
                correlationNames.Add(moistureContentRadCheckBox.Text);
                correlationNames.Add(temp1RadCheckBox.Text);
                correlationNames.Add(loadCurrent1RadCheckBox.Text);

                magnitudeMvCorrelations.Add(humidityAndMagnitudeMvCorrelationData);
                magnitudeMvCorrelations.Add(moistureAndMagnitudeMvCorrelationData);
                magnitudeMvCorrelations.Add(temp1AndMagnitudeMvCorrelationData);
                magnitudeMvCorrelations.Add(loadCurrent1AndMagnitudeMvCorrelationData);

                setLabels.Add(setOneText);
                setLabels.Add(setTwoText);

                PearsonCorrelation correlationGraph = new PearsonCorrelation(DataViewerType.BHM, magnitudeMvCorrelations, correlationNames, setLabels, dynamicsCorrelatedWithText + " " + gammaLegendText + "   " + dataDateInterval, true);
                correlationGraph.Show();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.CreateCorrelationsGraphs(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void createReportRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (programBrand == ProgramBrand.DevelopmentVersion)
                {
                    CreateReport();
                }
                else
                {
                    RadMessageBox.Show("Work in progress");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.createReportRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


    }

    public class BHM_CommonDataStructure
    {
        public double[] humidity;
        public double[] moisture;
        public double[] temp1;
        public double[] temp2;
        public double[] temp3;
        public double[] temp4;
        public double[] loadCurrent1;
        public double[] loadCurrent2;
        public double[] loadCurrent3;
        public double[] voltage1;
        public double[] voltage2;
        public double[] voltage3;


        public DateTime[] dataReadingDate;

        private int globalIndex = 0;

        public void AllocateAllArrays(int numberOfElements)
        {
            try
            {
                humidity = new double[numberOfElements];
                moisture = new double[numberOfElements];
                temp1 = new double[numberOfElements];
                temp2 = new double[numberOfElements];
                temp3 = new double[numberOfElements];
                temp4 = new double[numberOfElements];
                //extLoadActive = new double[numberOfElements];
                //extLoadReactive = new double[numberOfElements];
                loadCurrent1 = new double[numberOfElements];
                loadCurrent2 = new double[numberOfElements];
                loadCurrent3 = new double[numberOfElements];
                voltage1 = new double[numberOfElements];
                voltage2 = new double[numberOfElements];
                voltage3 = new double[numberOfElements];

                dataReadingDate = new DateTime[numberOfElements];

                globalIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_CommonDataStructure.AllocateAllArrays(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AddDataReading(BHM_SingleDataReading singleDataReading)
        {
            this.humidity[globalIndex] = singleDataReading.insulationParameters.Humidity;

            this.temp1[globalIndex] = singleDataReading.insulationParameters.Temperature_0;
            this.temp2[globalIndex] = singleDataReading.insulationParameters.Temperature_1;
            this.temp3[globalIndex] = singleDataReading.insulationParameters.Temperature_2;
            this.temp4[globalIndex] = singleDataReading.insulationParameters.Temperature_3;

            this.loadCurrent1[globalIndex] = singleDataReading.insulationParameters.Current_0;
            this.loadCurrent2[globalIndex] = singleDataReading.insulationParameters.Current_1;
            this.loadCurrent3[globalIndex] = singleDataReading.insulationParameters.CurrentR_1;
            //this.extLoadActive[globalIndex] = singleDataReading.insulationParameters.Current_0;
            //this.extLoadReactive[globalIndex] = singleDataReading.insulationParameters.CurrentR_0;
            this.dataReadingDate[globalIndex] = singleDataReading.readingDateTime;
            this.voltage1[globalIndex] = singleDataReading.insulationParameters.Reserved_0 / 10.0;
            this.voltage2[globalIndex] = singleDataReading.insulationParameters.Reserved_1 / 10.0;
            this.voltage3[globalIndex] = singleDataReading.insulationParameters.Reserved_2 / 10.0;

            moisture[globalIndex] = DynamicsCalculations.GetMoisture(temp1[globalIndex], humidity[globalIndex]);

            globalIndex++;
        }

    }

    /// <summary>
    /// This class exists simply to aggregate a bunch of arrays that hold both the copy
    /// of the data from the database and the local copy of the data used to create the 
    /// graphs.  It is not safe from exteral meddling with internal data, and is not
    /// supposed to be.
    /// </summary>
    public class BHM_DataStructure
    {
        public double[] gamma;
        public double[] gammaPhase;
        public double[] gammaTrend;
        public double[] temperatureCoefficient;
        public double[] phaseTemperatureCoefficient;
        public double[] bushingCurrentA;
        public double[] bushingCurrentB;
        public double[] bushingCurrentC;
        public double[] bushingCurrentPhaseAA;
        public double[] bushingCurrentPhaseAB;
        public double[] bushingCurrentPhaseAC;
        public double[] capacitanceA;
        public double[] capacitanceB;
        public double[] capacitanceC;
        public double[] tangentA;
        public double[] tangentB;
        public double[] tangentC;
        public double[] frequency;
        public double[] temperature;
        public double[] ltcPositionNumber;
        public double[] alarmStatus;

        public DateTime[] dataReadingDate;

        private int globalIndex = 0;

        public void AllocateAllArrays(int numberOfElements)
        {
            try
            {
                gamma = new double[numberOfElements];
                gammaPhase = new double[numberOfElements];
                gammaTrend = new double[numberOfElements];
                temperatureCoefficient = new double[numberOfElements];
                phaseTemperatureCoefficient = new double[numberOfElements];
                bushingCurrentA = new double[numberOfElements];
                bushingCurrentB = new double[numberOfElements];
                bushingCurrentC = new double[numberOfElements];
                bushingCurrentPhaseAA = new double[numberOfElements];
                bushingCurrentPhaseAB = new double[numberOfElements];
                bushingCurrentPhaseAC = new double[numberOfElements];
                capacitanceA = new double[numberOfElements];
                capacitanceB = new double[numberOfElements];
                capacitanceC = new double[numberOfElements];
                tangentA = new double[numberOfElements];
                tangentB = new double[numberOfElements];
                tangentC = new double[numberOfElements];
                frequency = new double[numberOfElements];
                temperature = new double[numberOfElements];
                ltcPositionNumber = new double[numberOfElements];
                alarmStatus = new double[numberOfElements];

                dataReadingDate = new DateTime[numberOfElements];

                globalIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataStructure.AllocateAllArrays(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        public void AddDataReading(BHM_DataReadings dataItem)
        //        {
        //            try
        //            {
        //                this.gamma[globalIndex] = (double)dataItem.Gamma;
        //                this.gammaPhase[globalIndex] = (double)dataItem.GammaPhase;
        //                this.gammaTrend[globalIndex] = (double)dataItem.GammaTrend;
        //                this.temperatureCoefficient[globalIndex] = (double)dataItem.TemperatureCoefficient;
        //                this.phaseTemperatureCoefficient[globalIndex] = (double)dataItem.PhaseTemperatureCoefficient;
        //                this.bushingCurrentA[globalIndex] = (double)dataItem.BushingCurrentA;
        //                this.bushingCurrentB[globalIndex] = (double)dataItem.BushingCurrentB;
        //                this.bushingCurrentC[globalIndex] = (double)dataItem.BushingCurrentC;
        //                this.bushingCurrentPhaseAA[globalIndex] = (double)dataItem.BushingCurrentPhaseAA;
        //                this.bushingCurrentPhaseAB[globalIndex] = (double)dataItem.BushingCurrentPhaseAB;
        //                this.bushingCurrentPhaseAC[globalIndex] = (double)dataItem.BushingCurrentPhaseAC;
        //                this.capacitanceA[globalIndex] = (double)dataItem.CapacitanceA;
        //                this.capacitanceB[globalIndex] = (double)dataItem.CapacitanceB;
        //                this.capacitanceC[globalIndex] = (double)dataItem.CapacitanceC;
        //                this.tangentA[globalIndex] = (double)dataItem.TangentA;
        //                this.tangentB[globalIndex] = (double)dataItem.TangentB;
        //                this.tangentC[globalIndex] = (double)dataItem.TangentC;
        //                this.frequency[globalIndex] = (double)dataItem.Frequency;
        //                this.temperature[globalIndex] = dataItem.Temperature;
        //                this.loadActive[globalIndex] = dataItem.LoadActive;
        //                this.loadReactive[globalIndex] = dataItem.LoadReactive;
        //                this.humidity[globalIndex] = dataItem.Humidity;
        //                this.ltcPositionNumber[globalIndex] = dataItem.LTCPositionNumber;
        //                this.alarmStatus[globalIndex] = dataItem.AlarmStatus;

        //                this.dataReadingDate[globalIndex] = dataItem.ReadingDateTime;

        //                globalIndex++;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DataStructure.AddDataReading(BHM_DataReadings)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        public void AddDataReading(BHM_SingleDataReading singleDataReading, int sideNumber)
        {
            try
            {
                BHM_DataComponent_TransformerSideParameters transformerSideParameters = null;
                if ((sideNumber == 1) && (singleDataReading.transformerSideParametersSideOne != null))
                {
                    transformerSideParameters = singleDataReading.transformerSideParametersSideOne;
                }
                else if ((sideNumber == 2) && (singleDataReading.transformerSideParametersSideTwo != null))
                {
                    transformerSideParameters = singleDataReading.transformerSideParametersSideTwo;
                }
                else
                {
                    string errorMessage = "Error in BHM_DataStructure.AddDataReading(BHM_SingleDataReading, int)\nBad input value, either side number does not exist or entry for that side was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (transformerSideParameters != null)
                {
                    // it does not appear that there are other common parameters, don't know

                    this.gamma[globalIndex] = (double)transformerSideParameters.Gamma;
                    this.gammaPhase[globalIndex] = (double)transformerSideParameters.GammaPhase;
                    this.gammaTrend[globalIndex] = (double)transformerSideParameters.Trend;

                    // many of these are guesses... 

                    // this SEEMS like a reasonable guess
                    this.temperatureCoefficient[globalIndex] = (double)transformerSideParameters.KT;
                    this.phaseTemperatureCoefficient[globalIndex] = (double)transformerSideParameters.KTPhase;

                    // these are slightly less reasonable guesses, but not completely off base
                    this.bushingCurrentA[globalIndex] = (double)transformerSideParameters.SourceAmplitude_0;
                    this.bushingCurrentB[globalIndex] = (double)transformerSideParameters.SourceAmplitude_1;
                    this.bushingCurrentC[globalIndex] = (double)transformerSideParameters.SourceAmplitude_2;
                    this.bushingCurrentPhaseAA[globalIndex] = (double)transformerSideParameters.SourcePhase_0;
                    this.bushingCurrentPhaseAB[globalIndex] = (double)transformerSideParameters.SourcePhase_1;
                    this.bushingCurrentPhaseAC[globalIndex] = (double)transformerSideParameters.SourcePhase_2;

                    // I'm a bit more sure about these guys, just from their names
                    this.capacitanceA[globalIndex] = (double)transformerSideParameters.C_0;
                    this.capacitanceB[globalIndex] = (double)transformerSideParameters.C_1;
                    this.capacitanceC[globalIndex] = (double)transformerSideParameters.C_2;
                    this.tangentA[globalIndex] = (double)transformerSideParameters.Tg_0;
                    this.tangentB[globalIndex] = (double)transformerSideParameters.Tg_1;
                    this.tangentC[globalIndex] = (double)transformerSideParameters.Tg_2;

                    /// these are likely to be spot on
                    this.frequency[globalIndex] = (double)transformerSideParameters.Frequency;
                    this.temperature[globalIndex] = transformerSideParameters.Temperature;

                    // no clue on these, but I will guess in the dark
                    if ((sideNumber == 1) && (singleDataReading.transformerSideParametersSideOne != null))
                    {
                        this.ltcPositionNumber[globalIndex] = singleDataReading.insulationParameters.RPN_0;
                    }
                    else if ((sideNumber == 2) && (singleDataReading.transformerSideParametersSideTwo != null))
                    {
                        this.ltcPositionNumber[globalIndex] = singleDataReading.insulationParameters.RPN_1;
                    }

                    // again, spot on probably
                    this.alarmStatus[globalIndex] = transformerSideParameters.AlarmStatus;
                    this.dataReadingDate[globalIndex] = singleDataReading.readingDateTime;

                    globalIndex++;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataStructure.AddDataReading(BHM_SingleDataReading, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }

    /// <summary>
    /// This code was lifted from an online posting.  It puts up a bunch of junk when you click on a data point.
    /// It will probably be useful in the future if the message is customized.  I'm pretty sure it's sample code
    /// from the chartDirector people.
    /// </summary>
    public class ParamViewer : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button OKPB;
        private System.Windows.Forms.ColumnHeader Key;
        private System.Windows.Forms.ColumnHeader Value;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ListView listView;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public ParamViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView = new System.Windows.Forms.ListView();
            this.OKPB = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.Key = new System.Windows.Forms.ColumnHeader();
            this.Value = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.Key,
																					   this.Value});
            this.listView.GridLines = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.Location = new System.Drawing.Point(8, 56);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(304, 168);
            this.listView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView.TabIndex = 0;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // OKPB
            // 
            this.OKPB.Location = new System.Drawing.Point(120, 232);
            this.OKPB.Name = "OKPB";
            this.OKPB.Size = new System.Drawing.Size(72, 24);
            this.OKPB.TabIndex = 1;
            this.OKPB.Text = "OK";
            this.OKPB.Click += new System.EventHandler(this.OKPB_Click);
            // 
            // label
            // 
            this.label.Location = new System.Drawing.Point(8, 8);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(304, 48);
            this.label.TabIndex = 2;
            this.label.Text = "This is to demonstrate that ChartDirector charts are clickable. In this demo prog" +
                "ram, we just display the information provided to the ClickHotSpot event handler." +
                " ";
            // 
            // Key
            // 
            this.Key.Text = "Key";
            this.Key.Width = 80;
            // 
            // Value
            // 
            this.Value.Text = "Value";
            this.Value.Width = 220;
            // 
            // ParamViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
            this.ClientSize = new System.Drawing.Size(320, 261);
            this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label,
																		  this.OKPB,
																		  this.listView});
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ParamViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hot Spot Parameters";
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public void Display(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            // Add the name of the ChartViewer control that is being clicked
            listView.Items.Add(new ListViewItem(new string[] {"source", 
				((ChartDirector.WinChartViewer)sender).Name}));

            // List out the parameters of the hot spot
            foreach (DictionaryEntry key in e.GetAttrValues())
            {
                listView.Items.Add(new ListViewItem(
                    new string[] { (string)key.Key, (string)key.Value }));
            }

            // Display the form
            ShowDialog();
        }

        /// <summary>
        /// Handler for the OK button
        /// </summary>
        private void OKPB_Click(object sender, System.EventArgs e)
        {
            // Just close the Form
            Close();
        }
    }

}
