﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace BHMonitorUtilities
{
    public partial class BHM_DataViewer : Telerik.WinControls.UI.RadForm
    {
        private static string shortingOfCondenserFoilsPhaseAText = "Shorting of Condenser Foils on Phase A";
        private static string shortingOfCondenserFoilsPhaseBText = "Shorting of Condenser Foils on Phase B";
        private static string shortingOfCondenserFoilsPhaseCText = "Shorting of Condenser Foils on Phase C";
        private static string powerFactorIncreasingPhaseAText = "Power Factor Increasing on Phase A";
        private static string powerFactorIncreasingPhaseBText = "Power Factor Increasing on Phase B";
        private static string powerFactorIncreasingPhaseCText = "Power Factor Increasing on Phase C";
        private static string negativeTemperatureDependencyOrPowerFactorPhaseAText = "A Phase negative temperature dependency or negative Power Factor";
        private static string negativeTemperatureDependencyOrPowerFactorPhaseBText = "B Phase negative temperature dependency or negative Power Factor";
        private static string negativeTemperatureDependencyOrPowerFactorPhaseCText = "C Phase negative temperature dependency or negative Power Factor";
        private static string twoBushingOrMultipleDefectsOccurringText = "Likely two bushing or multiple defects are occurring";
        private static string noDiagnosticsAvailableText = "No Diagnostics Available";
        private static string diagnosticsAreNormalText = "Normal";

        private static string diagnosticsRadPageViewPageText = "Diagnostics";
        private static string groupOneDiagnosticsRadGroupBoxText = "Group 1 Diagnostics";
        private static string groupTwoDiagnosticsRadGroupBoxText = "Group 2 Diagnostics";
        private static string diagnosticRadLabelText = "Diagnostic";
        private static string healthIndexRadLabelText = "<html>Health index<br>10 = good<br>0 = poor</html>";
        private static string noDataAvailableText = "No data available";

        private string GetBushingAngleErrorMessage(double bAngle)
        {
            string bushingAngleErrorMessage = noDiagnosticsAvailableText;

            try
            {

                if (((bAngle >= 345) && (bAngle <= 360)) || ((bAngle >= 0) && (bAngle <= 15)))
                {
                    bushingAngleErrorMessage = shortingOfCondenserFoilsPhaseAText;
                }

                if ((bAngle >= 15) && (bAngle <= 45))
                {
                    bushingAngleErrorMessage = negativeTemperatureDependencyOrPowerFactorPhaseBText;
                }

                if ((bAngle >= 45) && (bAngle <= 75))
                {
                    bushingAngleErrorMessage = twoBushingOrMultipleDefectsOccurringText;
                }

                if ((bAngle >= 75) && (bAngle <= 105))
                {
                    bushingAngleErrorMessage = powerFactorIncreasingPhaseAText;
                }

                if ((bAngle >= 105) && (bAngle <= 135))
                {
                    bushingAngleErrorMessage = shortingOfCondenserFoilsPhaseBText;
                }

                if ((bAngle >= 135) && (bAngle <= 165))
                {
                    bushingAngleErrorMessage = negativeTemperatureDependencyOrPowerFactorPhaseCText;
                }

                if ((bAngle >= 165) && (bAngle <= 195))
                {
                    bushingAngleErrorMessage = twoBushingOrMultipleDefectsOccurringText;
                }

                if ((bAngle >= 195) && (bAngle <= 225))
                {
                    bushingAngleErrorMessage = powerFactorIncreasingPhaseBText;
                }

                if ((bAngle >= 225) && (bAngle <= 255))
                {
                    bushingAngleErrorMessage = shortingOfCondenserFoilsPhaseCText;
                }

                if ((bAngle >= 255) && (bAngle <= 285))
                {
                    bushingAngleErrorMessage = negativeTemperatureDependencyOrPowerFactorPhaseAText;
                }

                if ((bAngle >= 285) && (bAngle <= 315))
                {
                    bushingAngleErrorMessage = twoBushingOrMultipleDefectsOccurringText;
                }

                if ((bAngle >= 315) && (bAngle <= 345))
                {
                    bushingAngleErrorMessage = powerFactorIncreasingPhaseCText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetBushingAngleErrorMessage()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }


            return bushingAngleErrorMessage;
        }

        private void CalculateDiagnosticsAndPostTheResults()
        {
            try
            {
                double gamma;
                double gammaPhase;
                int lastValueIndex;
                string diagnostic;
                double healthIndex;


                if ((dbDataGroup1 != null) && (dbDataGroup1.gamma.Length > 0) && (dbDataGroup1.gammaPhase.Length == dbDataGroup1.gamma.Length))
                {
                    lastValueIndex = dbDataGroup1.gamma.Length - 1;
                    gamma = dbDataGroup1.gamma[lastValueIndex];
                    gammaPhase = dbDataGroup1.gammaPhase[lastValueIndex];

                    if (gamma < 1)
                    {
                        diagnostic = diagnosticsAreNormalText;
                    }
                    else
                    {
                        diagnostic = GetBushingAngleErrorMessage(gammaPhase);
                    }

                    healthIndex = DynamicsCalculations.GetHealthIndexScore(8.0, 2.5, 1.0, gamma);

                    healthIndexGroupOneValueRadLabel.Text = Math.Round(healthIndex, 1).ToString();
                    diagnosticGroupOneValueRadLabel.Text = diagnostic;
                }
                else
                {
                    diagnosticGroupOneValueRadLabel.Text = noDataAvailableText;
                    healthIndexGroupOneValueRadLabel.Text = noDataAvailableText;
                }

                if ((dbDataGroup2 != null) && (dbDataGroup2.gamma.Length > 0) && (dbDataGroup2.gammaPhase.Length == dbDataGroup2.gamma.Length))
                {
                    lastValueIndex = dbDataGroup2.gamma.Length - 1;
                    gamma = dbDataGroup2.gamma[lastValueIndex];
                    gammaPhase = dbDataGroup2.gammaPhase[lastValueIndex];

                    if (gamma < 1)
                    {
                        diagnostic = diagnosticsAreNormalText;
                    }
                    else
                    {
                        diagnostic = GetBushingAngleErrorMessage(gammaPhase);
                    }

                    healthIndex = DynamicsCalculations.GetHealthIndexScore(8.0, 2.5, 1.0, gamma);

                    healthIndexGroupTwoValueRadLabel.Text = Math.Round(healthIndex, 1).ToString();
                    diagnosticGroupTwoValueRadLabel.Text = diagnostic;
                }
                else
                {
                    diagnosticGroupTwoValueRadLabel.Text = noDataAvailableText;
                    healthIndexGroupTwoValueRadLabel.Text = noDataAvailableText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.CalculateDiagnosticsAndPostTheResults()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
