namespace BHMonitorUtilities
{
    public partial class BHM_DataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BHM_DataViewer));
            this.dataDisplayRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.trendRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.createReportRadButton = new Telerik.WinControls.UI.RadButton();
            this.createCorrelationGraphRadButton = new Telerik.WinControls.UI.RadButton();
            this.trendPhasesSwappedRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dynamicsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.voltage3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.moistureContentRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.voltage2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp4RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.voltage1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.humidityRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendDataGroupSelectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendAllGroupsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendGroup2RadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendGroup1RadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendCursorLabel = new System.Windows.Forms.Label();
            this.temperatureCoefficientRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendWinChartViewer = new ChartDirector.WinChartViewer();
            this.trendDrawRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendAutoDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendManualDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendDrawGraphsRadButton = new Telerik.WinControls.UI.RadButton();
            this.trendDisplayOptionsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.normalizeDataRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.dataItemsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.movingAverageRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.movingAverageRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.exponentialTrendRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.linearTrendRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendLineRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendDataAgeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendAllDataRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendOneYearRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendSixMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendThreeMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.frequencyRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentCRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentBRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentARadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.capacitanceCRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.capacitanceBRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.capacitanceARadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentPhaseACRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentPhaseABRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentPhaseAARadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentCRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentBRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.bushingCurrentARadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.phaseTemperatureCoefficientRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.gammaTrendRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.gammaPhaseRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.gammaRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendZoomButtonsRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.trendPointerRadioButton = new System.Windows.Forms.RadioButton();
            this.trendZoomInRadioButton = new System.Windows.Forms.RadioButton();
            this.trendZoomOutRadioButton = new System.Windows.Forms.RadioButton();
            this.trendRadHScrollBar = new Telerik.WinControls.UI.RadHScrollBar();
            this.polarRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.polarPhasesSwappedRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.showRadialLabelsRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.polarVcursorGroup2Label = new System.Windows.Forms.Label();
            this.polarHcursorGroup2Label = new System.Windows.Forms.Label();
            this.polarHcursorGroup1Label = new System.Windows.Forms.Label();
            this.polarVcursorGroup1Label = new System.Windows.Forms.Label();
            this.phaseSegmentPolarRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.capacitanceAPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentCPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.capacitanceBPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentBPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.capacitanceCPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.tangentAPolarRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.polarDataDisplayChoiceRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.temperatureCoefficientPolarRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.gammaPolarRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarRadVScrollBar = new Telerik.WinControls.UI.RadVScrollBar();
            this.polarRadHScrollBar = new Telerik.WinControls.UI.RadHScrollBar();
            this.polarWinChartViewer = new ChartDirector.WinChartViewer();
            this.polarZoomButtonsRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.polarPointerRadioButton = new System.Windows.Forms.RadioButton();
            this.polarZoomInRadioButton = new System.Windows.Forms.RadioButton();
            this.polarZoomOutRadioButton = new System.Windows.Forms.RadioButton();
            this.polarDrawRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.polarManualDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarAutoDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarDrawGraphsRadButton = new Telerik.WinControls.UI.RadButton();
            this.polarDataGroupSelectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.polarAllGroupsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarGroup2RadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarGroup1RadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarDataAgeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.polarOneYearRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarAllDataRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarSixMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.polarThreeMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.initialBalanceDataRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.initialBalanceDataTabMeasurementDateValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabMeasurementDateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabTemperatureRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabImbalanceRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabImbalancePhaseRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.gridRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.saveDataToCsvRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridHideAllRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridExpandAllRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataDeleteSelectedRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataCancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataSaveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataEditRadButton = new Telerik.WinControls.UI.RadButton();
            this.bhm_DataReadingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.diagnosticsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.groupTwoDiagnosticsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.healthIndexGroupTwoValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.healthIndexGroupTwoTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.diagnosticGroupTwoValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.diagnosticGroupTwoTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.groupOneDiagnosticsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.healthIndexGroupOneValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.healthIndexGroupOneTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.diagnosticGroupOneValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.diagnosticGroupOneTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.polarSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.trendRadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.polarRadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.dynamicsRadGroupBoxRadContextMenuStrip = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.dataDisplayRadPageView)).BeginInit();
            this.dataDisplayRadPageView.SuspendLayout();
            this.trendRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createReportRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createCorrelationGraphRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendPhasesSwappedRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGroupBox)).BeginInit();
            this.dynamicsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.voltage3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moistureContentRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp4RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDataGroupSelectionRadGroupBox)).BeginInit();
            this.trendDataGroupSelectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendAllGroupsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGroup2RadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGroup1RadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawRadGroupBox)).BeginInit();
            this.trendDrawRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendAutoDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendManualDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawGraphsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDisplayOptionsRadGroupBox)).BeginInit();
            this.trendDisplayOptionsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialTrendRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearTrendRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendLineRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDataAgeRadGroupBox)).BeginInit();
            this.trendDataAgeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendAllDataRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendOneYearRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendSixMonthsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendThreeMonthsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentCRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentBRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentARadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceCRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceBRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceARadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseACRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseABRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseAARadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentCRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentBRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentARadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseTemperatureCoefficientRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaTrendRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaPhaseRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).BeginInit();
            this.trendZoomButtonsRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendRadHScrollBar)).BeginInit();
            this.polarRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarPhasesSwappedRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showRadialLabelsRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseSegmentPolarRadGroupBox)).BeginInit();
            this.phaseSegmentPolarRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceAPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentCPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceBPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentBPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceCPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentAPolarRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataDisplayChoiceRadGroupBox)).BeginInit();
            this.polarDataDisplayChoiceRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientPolarRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaPolarRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarRadVScrollBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarRadHScrollBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarWinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarZoomButtonsRadPanel)).BeginInit();
            this.polarZoomButtonsRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarDrawRadGroupBox)).BeginInit();
            this.polarDrawRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarManualDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarAutoDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDrawGraphsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataGroupSelectionRadGroupBox)).BeginInit();
            this.polarDataGroupSelectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarAllGroupsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarGroup2RadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarGroup1RadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataAgeRadGroupBox)).BeginInit();
            this.polarDataAgeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarOneYearRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarAllDataRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarSixMonthsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarThreeMonthsRadRadioButton)).BeginInit();
            this.initialBalanceDataRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabTemperatureRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalanceRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalancePhaseRadLabel)).BeginInit();
            this.gridRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveDataToCsvRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHideAllRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridExpandAllRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDeleteSelectedRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataCancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataEditRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhm_DataReadingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhm_DataReadingsRadGridView.MasterTemplate)).BeginInit();
            this.diagnosticsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupTwoDiagnosticsRadGroupBox)).BeginInit();
            this.groupTwoDiagnosticsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupTwoValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupTwoTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupTwoValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupTwoTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOneDiagnosticsRadGroupBox)).BeginInit();
            this.groupOneDiagnosticsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupOneValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupOneTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupOneValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupOneTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataDisplayRadPageView
            // 
            this.dataDisplayRadPageView.Controls.Add(this.trendRadPageViewPage);
            this.dataDisplayRadPageView.Controls.Add(this.polarRadPageViewPage);
            this.dataDisplayRadPageView.Controls.Add(this.initialBalanceDataRadPageViewPage);
            this.dataDisplayRadPageView.Controls.Add(this.gridRadPageViewPage);
            this.dataDisplayRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataDisplayRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDisplayRadPageView.Location = new System.Drawing.Point(0, 0);
            this.dataDisplayRadPageView.Name = "dataDisplayRadPageView";
            this.dataDisplayRadPageView.SelectedPage = this.trendRadPageViewPage;
            this.dataDisplayRadPageView.Size = new System.Drawing.Size(1016, 736);
            this.dataDisplayRadPageView.TabIndex = 0;
            this.dataDisplayRadPageView.Text = "radPageView1";
            this.dataDisplayRadPageView.ThemeName = "Office2007Black";
            this.dataDisplayRadPageView.SelectedPageChanging += new System.EventHandler<Telerik.WinControls.UI.RadPageViewCancelEventArgs>(this.dataDisplayRadPageView_SelectedPageChanging);
            this.dataDisplayRadPageView.SelectedPageChanged += new System.EventHandler(this.dataDisplayRadPageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.dataDisplayRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // trendRadPageViewPage
            // 
            this.trendRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendRadPageViewPage.Controls.Add(this.createReportRadButton);
            this.trendRadPageViewPage.Controls.Add(this.createCorrelationGraphRadButton);
            this.trendRadPageViewPage.Controls.Add(this.trendPhasesSwappedRadLabel);
            this.trendRadPageViewPage.Controls.Add(this.dynamicsRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendDataGroupSelectionRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendCursorLabel);
            this.trendRadPageViewPage.Controls.Add(this.temperatureCoefficientRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.trendWinChartViewer);
            this.trendRadPageViewPage.Controls.Add(this.trendDrawRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendDisplayOptionsRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendDataAgeRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.frequencyRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.tangentCRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.tangentBRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.tangentARadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.capacitanceCRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.capacitanceBRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.capacitanceARadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentPhaseACRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentPhaseABRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentPhaseAARadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentCRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentBRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.bushingCurrentARadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.phaseTemperatureCoefficientRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.gammaTrendRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.gammaPhaseRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.gammaRadCheckBox);
            this.trendRadPageViewPage.Controls.Add(this.trendZoomButtonsRadPanel);
            this.trendRadPageViewPage.Controls.Add(this.trendRadHScrollBar);
            this.trendRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.trendRadPageViewPage.Name = "trendRadPageViewPage";
            this.trendRadPageViewPage.Size = new System.Drawing.Size(995, 690);
            this.trendRadPageViewPage.Text = "Trend";
            // 
            // createReportRadButton
            // 
            this.createReportRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createReportRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createReportRadButton.Location = new System.Drawing.Point(886, 651);
            this.createReportRadButton.Name = "createReportRadButton";
            this.createReportRadButton.Size = new System.Drawing.Size(105, 34);
            this.createReportRadButton.TabIndex = 90;
            this.createReportRadButton.Text = "Create Report";
            this.createReportRadButton.ThemeName = "Office2007Black";
            this.createReportRadButton.Click += new System.EventHandler(this.createReportRadButton_Click);
            // 
            // createCorrelationGraphRadButton
            // 
            this.createCorrelationGraphRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.createCorrelationGraphRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createCorrelationGraphRadButton.Location = new System.Drawing.Point(284, 650);
            this.createCorrelationGraphRadButton.Name = "createCorrelationGraphRadButton";
            this.createCorrelationGraphRadButton.Size = new System.Drawing.Size(105, 34);
            this.createCorrelationGraphRadButton.TabIndex = 89;
            this.createCorrelationGraphRadButton.Text = "Show Correlations";
            this.createCorrelationGraphRadButton.ThemeName = "Office2007Black";
            this.createCorrelationGraphRadButton.Click += new System.EventHandler(this.createCorrelationGraphRadButton_Click);
            // 
            // trendPhasesSwappedRadLabel
            // 
            this.trendPhasesSwappedRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendPhasesSwappedRadLabel.Location = new System.Drawing.Point(770, 110);
            this.trendPhasesSwappedRadLabel.Name = "trendPhasesSwappedRadLabel";
            this.trendPhasesSwappedRadLabel.Size = new System.Drawing.Size(189, 16);
            this.trendPhasesSwappedRadLabel.TabIndex = 88;
            this.trendPhasesSwappedRadLabel.Text = "Phases 2 and 3 have been swapped";
            this.trendPhasesSwappedRadLabel.ThemeName = "ControlDefault";
            // 
            // dynamicsRadGroupBox
            // 
            this.dynamicsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dynamicsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dynamicsRadGroupBox.Controls.Add(this.voltage3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.moistureContentRadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.voltage2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp4RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.voltage1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.humidityRadCheckBox);
            this.dynamicsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dynamicsRadGroupBox.FooterImageIndex = -1;
            this.dynamicsRadGroupBox.FooterImageKey = "";
            this.dynamicsRadGroupBox.HeaderImageIndex = -1;
            this.dynamicsRadGroupBox.HeaderImageKey = "";
            this.dynamicsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dynamicsRadGroupBox.HeaderText = "Dynamics";
            this.dynamicsRadGroupBox.Location = new System.Drawing.Point(133, 384);
            this.dynamicsRadGroupBox.Name = "dynamicsRadGroupBox";
            this.dynamicsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dynamicsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dynamicsRadGroupBox.Size = new System.Drawing.Size(142, 301);
            this.dynamicsRadGroupBox.TabIndex = 87;
            this.dynamicsRadGroupBox.Text = "Dynamics";
            this.dynamicsRadGroupBox.ThemeName = "Office2007Black";
            this.dynamicsRadGroupBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // voltage3RadCheckBox
            // 
            this.voltage3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage3RadCheckBox.Location = new System.Drawing.Point(5, 266);
            this.voltage3RadCheckBox.Name = "voltage3RadCheckBox";
            this.voltage3RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage3RadCheckBox.TabIndex = 13;
            this.voltage3RadCheckBox.Text = "Voltage 3 (kV)";
            this.voltage3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // moistureContentRadCheckBox
            // 
            this.moistureContentRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moistureContentRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.moistureContentRadCheckBox.Location = new System.Drawing.Point(5, 45);
            this.moistureContentRadCheckBox.Name = "moistureContentRadCheckBox";
            this.moistureContentRadCheckBox.Size = new System.Drawing.Size(105, 16);
            this.moistureContentRadCheckBox.TabIndex = 11;
            this.moistureContentRadCheckBox.Text = "Moisture (kg/M3)";
            this.moistureContentRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // loadCurrent1RadCheckBox
            // 
            this.loadCurrent1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent1RadCheckBox.Location = new System.Drawing.Point(5, 156);
            this.loadCurrent1RadCheckBox.Name = "loadCurrent1RadCheckBox";
            this.loadCurrent1RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent1RadCheckBox.TabIndex = 3;
            this.loadCurrent1RadCheckBox.Text = "Load Current 1 (A)";
            this.loadCurrent1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // voltage2RadCheckBox
            // 
            this.voltage2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage2RadCheckBox.Location = new System.Drawing.Point(5, 244);
            this.voltage2RadCheckBox.Name = "voltage2RadCheckBox";
            this.voltage2RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage2RadCheckBox.TabIndex = 16;
            this.voltage2RadCheckBox.Text = "Voltage 2 (kV)";
            this.voltage2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // temp4RadCheckBox
            // 
            this.temp4RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp4RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp4RadCheckBox.Location = new System.Drawing.Point(5, 133);
            this.temp4RadCheckBox.Name = "temp4RadCheckBox";
            this.temp4RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp4RadCheckBox.TabIndex = 2;
            this.temp4RadCheckBox.Text = "Temp 4 (�C)";
            this.temp4RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // temp3RadCheckBox
            // 
            this.temp3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp3RadCheckBox.Location = new System.Drawing.Point(5, 111);
            this.temp3RadCheckBox.Name = "temp3RadCheckBox";
            this.temp3RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp3RadCheckBox.TabIndex = 2;
            this.temp3RadCheckBox.Text = "Temp 3 (�C)";
            this.temp3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // loadCurrent3RadCheckBox
            // 
            this.loadCurrent3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent3RadCheckBox.Location = new System.Drawing.Point(5, 200);
            this.loadCurrent3RadCheckBox.Name = "loadCurrent3RadCheckBox";
            this.loadCurrent3RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent3RadCheckBox.TabIndex = 14;
            this.loadCurrent3RadCheckBox.Text = "Load Current 3 (A)";
            this.loadCurrent3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // temp1RadCheckBox
            // 
            this.temp1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp1RadCheckBox.Location = new System.Drawing.Point(5, 67);
            this.temp1RadCheckBox.Name = "temp1RadCheckBox";
            this.temp1RadCheckBox.Size = new System.Drawing.Size(132, 16);
            this.temp1RadCheckBox.TabIndex = 2;
            this.temp1RadCheckBox.Text = "Correlating Temp (�C)";
            this.temp1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // temp2RadCheckBox
            // 
            this.temp2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp2RadCheckBox.Location = new System.Drawing.Point(5, 89);
            this.temp2RadCheckBox.Name = "temp2RadCheckBox";
            this.temp2RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp2RadCheckBox.TabIndex = 1;
            this.temp2RadCheckBox.Text = "Temp 2 (�C)";
            this.temp2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // loadCurrent2RadCheckBox
            // 
            this.loadCurrent2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent2RadCheckBox.Location = new System.Drawing.Point(5, 178);
            this.loadCurrent2RadCheckBox.Name = "loadCurrent2RadCheckBox";
            this.loadCurrent2RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent2RadCheckBox.TabIndex = 15;
            this.loadCurrent2RadCheckBox.Text = "Load Current 2 (A)";
            this.loadCurrent2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // voltage1RadCheckBox
            // 
            this.voltage1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage1RadCheckBox.Location = new System.Drawing.Point(5, 222);
            this.voltage1RadCheckBox.Name = "voltage1RadCheckBox";
            this.voltage1RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage1RadCheckBox.TabIndex = 12;
            this.voltage1RadCheckBox.Text = "Voltage 1 (kV)";
            this.voltage1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // humidityRadCheckBox
            // 
            this.humidityRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.humidityRadCheckBox.Location = new System.Drawing.Point(5, 23);
            this.humidityRadCheckBox.Name = "humidityRadCheckBox";
            this.humidityRadCheckBox.Size = new System.Drawing.Size(85, 16);
            this.humidityRadCheckBox.TabIndex = 0;
            this.humidityRadCheckBox.Text = "Humidity (%)";
            this.humidityRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // trendDataGroupSelectionRadGroupBox
            // 
            this.trendDataGroupSelectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendDataGroupSelectionRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendDataGroupSelectionRadGroupBox.Controls.Add(this.trendAllGroupsRadRadioButton);
            this.trendDataGroupSelectionRadGroupBox.Controls.Add(this.trendGroup2RadRadioButton);
            this.trendDataGroupSelectionRadGroupBox.Controls.Add(this.trendGroup1RadRadioButton);
            this.trendDataGroupSelectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDataGroupSelectionRadGroupBox.FooterImageIndex = -1;
            this.trendDataGroupSelectionRadGroupBox.FooterImageKey = "";
            this.trendDataGroupSelectionRadGroupBox.HeaderImageIndex = -1;
            this.trendDataGroupSelectionRadGroupBox.HeaderImageKey = "";
            this.trendDataGroupSelectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendDataGroupSelectionRadGroupBox.HeaderText = "Data Group Shown";
            this.trendDataGroupSelectionRadGroupBox.Location = new System.Drawing.Point(133, 277);
            this.trendDataGroupSelectionRadGroupBox.Name = "trendDataGroupSelectionRadGroupBox";
            this.trendDataGroupSelectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendDataGroupSelectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendDataGroupSelectionRadGroupBox.Size = new System.Drawing.Size(142, 101);
            this.trendDataGroupSelectionRadGroupBox.TabIndex = 76;
            this.trendDataGroupSelectionRadGroupBox.Text = "Data Group Shown";
            this.trendDataGroupSelectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendAllGroupsRadRadioButton
            // 
            this.trendAllGroupsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAllGroupsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAllGroupsRadRadioButton.Location = new System.Drawing.Point(14, 72);
            this.trendAllGroupsRadRadioButton.Name = "trendAllGroupsRadRadioButton";
            this.trendAllGroupsRadRadioButton.Size = new System.Drawing.Size(83, 18);
            this.trendAllGroupsRadRadioButton.TabIndex = 2;
            this.trendAllGroupsRadRadioButton.Text = "Both Groups";
            this.trendAllGroupsRadRadioButton.ThemeName = "Office2007Black";
            this.trendAllGroupsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.allGroupsRadRadioButton_ToggleStateChanged);
            // 
            // trendGroup2RadRadioButton
            // 
            this.trendGroup2RadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendGroup2RadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendGroup2RadRadioButton.Location = new System.Drawing.Point(14, 48);
            this.trendGroup2RadRadioButton.Name = "trendGroup2RadRadioButton";
            this.trendGroup2RadRadioButton.Size = new System.Drawing.Size(61, 18);
            this.trendGroup2RadRadioButton.TabIndex = 1;
            this.trendGroup2RadRadioButton.Text = "Group 2";
            this.trendGroup2RadRadioButton.ThemeName = "Office2007Black";
            this.trendGroup2RadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.group2RadRadioButton_ToggleStateChanged);
            // 
            // trendGroup1RadRadioButton
            // 
            this.trendGroup1RadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendGroup1RadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendGroup1RadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.trendGroup1RadRadioButton.Name = "trendGroup1RadRadioButton";
            this.trendGroup1RadRadioButton.Size = new System.Drawing.Size(61, 18);
            this.trendGroup1RadRadioButton.TabIndex = 0;
            this.trendGroup1RadRadioButton.TabStop = true;
            this.trendGroup1RadRadioButton.Text = "Group 1";
            this.trendGroup1RadRadioButton.ThemeName = "Office2007Black";
            this.trendGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.trendGroup1RadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.group1RadRadioButton_ToggleStateChanged);
            // 
            // trendCursorLabel
            // 
            this.trendCursorLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.trendCursorLabel.Location = new System.Drawing.Point(491, 88);
            this.trendCursorLabel.Name = "trendCursorLabel";
            this.trendCursorLabel.Size = new System.Drawing.Size(12, 436);
            this.trendCursorLabel.TabIndex = 86;
            // 
            // temperatureCoefficientRadCheckBox
            // 
            this.temperatureCoefficientRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.temperatureCoefficientRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureCoefficientRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temperatureCoefficientRadCheckBox.Location = new System.Drawing.Point(1, 255);
            this.temperatureCoefficientRadCheckBox.Name = "temperatureCoefficientRadCheckBox";
            this.temperatureCoefficientRadCheckBox.Size = new System.Drawing.Size(115, 16);
            this.temperatureCoefficientRadCheckBox.TabIndex = 57;
            this.temperatureCoefficientRadCheckBox.Text = "Temperature Coeff";
            this.temperatureCoefficientRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // trendWinChartViewer
            // 
            this.trendWinChartViewer.Location = new System.Drawing.Point(281, 3);
            this.trendWinChartViewer.Name = "trendWinChartViewer";
            this.trendWinChartViewer.Size = new System.Drawing.Size(710, 624);
            this.trendWinChartViewer.TabIndex = 80;
            this.trendWinChartViewer.TabStop = false;
            this.trendWinChartViewer.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.trendWinChartViewer_ClickHotSpot);
            this.trendWinChartViewer.ViewPortChanged += new ChartDirector.WinViewPortEventHandler(this.trendWinChartViewer_ViewPortChanged);
            this.trendWinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trendWinChartViewer_MouseClick);
            this.trendWinChartViewer.MouseEnter += new System.EventHandler(this.trendWinChartViewer_MouseEnter);
            this.trendWinChartViewer.MouseLeave += new System.EventHandler(this.trendWinChartViewer_MouseLeave);
            // 
            // trendDrawRadGroupBox
            // 
            this.trendDrawRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendDrawRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendDrawRadGroupBox.Controls.Add(this.trendAutoDrawRadRadioButton);
            this.trendDrawRadGroupBox.Controls.Add(this.trendManualDrawRadRadioButton);
            this.trendDrawRadGroupBox.Controls.Add(this.trendDrawGraphsRadButton);
            this.trendDrawRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDrawRadGroupBox.FooterImageIndex = -1;
            this.trendDrawRadGroupBox.FooterImageKey = "";
            this.trendDrawRadGroupBox.HeaderImageIndex = -1;
            this.trendDrawRadGroupBox.HeaderImageKey = "";
            this.trendDrawRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendDrawRadGroupBox.HeaderText = "Graph Method";
            this.trendDrawRadGroupBox.Location = new System.Drawing.Point(2, 567);
            this.trendDrawRadGroupBox.Name = "trendDrawRadGroupBox";
            this.trendDrawRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendDrawRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendDrawRadGroupBox.Size = new System.Drawing.Size(129, 118);
            this.trendDrawRadGroupBox.TabIndex = 78;
            this.trendDrawRadGroupBox.Text = "Graph Method";
            this.trendDrawRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendAutoDrawRadRadioButton
            // 
            this.trendAutoDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAutoDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAutoDrawRadRadioButton.Location = new System.Drawing.Point(13, 22);
            this.trendAutoDrawRadRadioButton.Name = "trendAutoDrawRadRadioButton";
            this.trendAutoDrawRadRadioButton.Size = new System.Drawing.Size(101, 18);
            this.trendAutoDrawRadRadioButton.TabIndex = 29;
            this.trendAutoDrawRadRadioButton.TabStop = true;
            this.trendAutoDrawRadRadioButton.Text = "Automatic Draw";
            this.trendAutoDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.trendAutoDrawRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.autoDrawRadRadioButton_ToggleStateChanged);
            // 
            // trendManualDrawRadRadioButton
            // 
            this.trendManualDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendManualDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendManualDrawRadRadioButton.Location = new System.Drawing.Point(13, 42);
            this.trendManualDrawRadRadioButton.Name = "trendManualDrawRadRadioButton";
            this.trendManualDrawRadRadioButton.Size = new System.Drawing.Size(101, 18);
            this.trendManualDrawRadRadioButton.TabIndex = 1;
            this.trendManualDrawRadRadioButton.Text = "Manual Draw";
            this.trendManualDrawRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.manualDrawRadioButton_ToggleStateChanged);
            // 
            // trendDrawGraphsRadButton
            // 
            this.trendDrawGraphsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDrawGraphsRadButton.Location = new System.Drawing.Point(13, 71);
            this.trendDrawGraphsRadButton.Name = "trendDrawGraphsRadButton";
            this.trendDrawGraphsRadButton.Size = new System.Drawing.Size(105, 34);
            this.trendDrawGraphsRadButton.TabIndex = 28;
            this.trendDrawGraphsRadButton.Text = "Draw Graph";
            this.trendDrawGraphsRadButton.ThemeName = "Office2007Black";
            this.trendDrawGraphsRadButton.Click += new System.EventHandler(this.drawGraphsRadButton_Click);
            // 
            // trendDisplayOptionsRadGroupBox
            // 
            this.trendDisplayOptionsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendDisplayOptionsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.normalizeDataRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.dataItemsRadLabel);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.movingAverageRadSpinEditor);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.movingAverageRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.exponentialTrendRadRadioButton);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.linearTrendRadRadioButton);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.trendLineRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDisplayOptionsRadGroupBox.FooterImageIndex = -1;
            this.trendDisplayOptionsRadGroupBox.FooterImageKey = "";
            this.trendDisplayOptionsRadGroupBox.HeaderImageIndex = -1;
            this.trendDisplayOptionsRadGroupBox.HeaderImageKey = "";
            this.trendDisplayOptionsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendDisplayOptionsRadGroupBox.HeaderText = "Display Options";
            this.trendDisplayOptionsRadGroupBox.Location = new System.Drawing.Point(3, 411);
            this.trendDisplayOptionsRadGroupBox.Name = "trendDisplayOptionsRadGroupBox";
            this.trendDisplayOptionsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendDisplayOptionsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendDisplayOptionsRadGroupBox.Size = new System.Drawing.Size(128, 150);
            this.trendDisplayOptionsRadGroupBox.TabIndex = 77;
            this.trendDisplayOptionsRadGroupBox.Text = "Display Options";
            this.trendDisplayOptionsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // normalizeDataRadCheckBox
            // 
            this.normalizeDataRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalizeDataRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalizeDataRadCheckBox.Location = new System.Drawing.Point(8, 81);
            this.normalizeDataRadCheckBox.Name = "normalizeDataRadCheckBox";
            this.normalizeDataRadCheckBox.Size = new System.Drawing.Size(98, 16);
            this.normalizeDataRadCheckBox.TabIndex = 35;
            this.normalizeDataRadCheckBox.Text = "Normalize Data";
            this.normalizeDataRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // dataItemsRadLabel
            // 
            this.dataItemsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataItemsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataItemsRadLabel.Location = new System.Drawing.Point(60, 125);
            this.dataItemsRadLabel.Name = "dataItemsRadLabel";
            this.dataItemsRadLabel.Size = new System.Drawing.Size(58, 16);
            this.dataItemsRadLabel.TabIndex = 34;
            this.dataItemsRadLabel.Text = "data items";
            // 
            // movingAverageRadSpinEditor
            // 
            this.movingAverageRadSpinEditor.InterceptArrowKeys = false;
            this.movingAverageRadSpinEditor.Location = new System.Drawing.Point(8, 125);
            this.movingAverageRadSpinEditor.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.movingAverageRadSpinEditor.Name = "movingAverageRadSpinEditor";
            // 
            // 
            // 
            this.movingAverageRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.movingAverageRadSpinEditor.ShowBorder = true;
            this.movingAverageRadSpinEditor.Size = new System.Drawing.Size(39, 20);
            this.movingAverageRadSpinEditor.TabIndex = 33;
            this.movingAverageRadSpinEditor.TabStop = false;
            this.movingAverageRadSpinEditor.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.movingAverageRadSpinEditor.ValueChanged += new System.EventHandler(this.movingAverageRadSpinEditor_ValueChanged);
            // 
            // movingAverageRadCheckBox
            // 
            this.movingAverageRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.movingAverageRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.movingAverageRadCheckBox.Location = new System.Drawing.Point(8, 103);
            this.movingAverageRadCheckBox.Name = "movingAverageRadCheckBox";
            this.movingAverageRadCheckBox.Size = new System.Drawing.Size(111, 16);
            this.movingAverageRadCheckBox.TabIndex = 32;
            this.movingAverageRadCheckBox.Text = "Show Moving Avg";
            this.movingAverageRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.movingAverageRadCheckBox_ToggleStateChanged);
            // 
            // exponentialTrendRadRadioButton
            // 
            this.exponentialTrendRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exponentialTrendRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.exponentialTrendRadRadioButton.Location = new System.Drawing.Point(8, 58);
            this.exponentialTrendRadRadioButton.Name = "exponentialTrendRadRadioButton";
            this.exponentialTrendRadRadioButton.Size = new System.Drawing.Size(90, 18);
            this.exponentialTrendRadRadioButton.TabIndex = 31;
            this.exponentialTrendRadRadioButton.TabStop = true;
            this.exponentialTrendRadRadioButton.Text = "Exponential";
            this.exponentialTrendRadRadioButton.ThemeName = "Office2007Black";
            this.exponentialTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.exponentialTrendRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.exponentialTrendRadRadioButton_ToggleStateChanged);
            // 
            // linearTrendRadRadioButton
            // 
            this.linearTrendRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linearTrendRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.linearTrendRadRadioButton.Location = new System.Drawing.Point(8, 40);
            this.linearTrendRadRadioButton.Name = "linearTrendRadRadioButton";
            this.linearTrendRadRadioButton.Size = new System.Drawing.Size(53, 18);
            this.linearTrendRadRadioButton.TabIndex = 30;
            this.linearTrendRadRadioButton.Text = "Linear";
            this.linearTrendRadRadioButton.ThemeName = "Office2007Black";
            this.linearTrendRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.linearTrendRadRadioButton_ToggleStateChanged);
            // 
            // trendLineRadCheckBox
            // 
            this.trendLineRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendLineRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendLineRadCheckBox.Location = new System.Drawing.Point(8, 23);
            this.trendLineRadCheckBox.Name = "trendLineRadCheckBox";
            this.trendLineRadCheckBox.Size = new System.Drawing.Size(106, 16);
            this.trendLineRadCheckBox.TabIndex = 29;
            this.trendLineRadCheckBox.Text = "Show Trend Line";
            this.trendLineRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.trendRadCheckBox_ToggleStateChanged);
            // 
            // trendDataAgeRadGroupBox
            // 
            this.trendDataAgeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendDataAgeRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendDataAgeRadGroupBox.Controls.Add(this.trendAllDataRadRadioButton);
            this.trendDataAgeRadGroupBox.Controls.Add(this.trendOneYearRadRadioButton);
            this.trendDataAgeRadGroupBox.Controls.Add(this.trendSixMonthsRadRadioButton);
            this.trendDataAgeRadGroupBox.Controls.Add(this.trendThreeMonthsRadRadioButton);
            this.trendDataAgeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDataAgeRadGroupBox.FooterImageIndex = -1;
            this.trendDataAgeRadGroupBox.FooterImageKey = "";
            this.trendDataAgeRadGroupBox.HeaderImageIndex = -1;
            this.trendDataAgeRadGroupBox.HeaderImageKey = "";
            this.trendDataAgeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendDataAgeRadGroupBox.HeaderText = "Data Age";
            this.trendDataAgeRadGroupBox.Location = new System.Drawing.Point(128, 3);
            this.trendDataAgeRadGroupBox.Name = "trendDataAgeRadGroupBox";
            this.trendDataAgeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendDataAgeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendDataAgeRadGroupBox.Size = new System.Drawing.Size(147, 78);
            this.trendDataAgeRadGroupBox.TabIndex = 75;
            this.trendDataAgeRadGroupBox.Text = "Data Age";
            this.trendDataAgeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendAllDataRadRadioButton
            // 
            this.trendAllDataRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAllDataRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAllDataRadRadioButton.Location = new System.Drawing.Point(84, 47);
            this.trendAllDataRadRadioButton.Name = "trendAllDataRadRadioButton";
            this.trendAllDataRadRadioButton.Size = new System.Drawing.Size(54, 18);
            this.trendAllDataRadRadioButton.TabIndex = 3;
            this.trendAllDataRadRadioButton.TabStop = true;
            this.trendAllDataRadRadioButton.Text = "All";
            this.trendAllDataRadRadioButton.ThemeName = "Office2007Black";
            this.trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.trendAllDataRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.allDataRadRadioButton_ToggleStateChanged);
            // 
            // trendOneYearRadRadioButton
            // 
            this.trendOneYearRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendOneYearRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendOneYearRadRadioButton.Location = new System.Drawing.Point(84, 24);
            this.trendOneYearRadRadioButton.Name = "trendOneYearRadRadioButton";
            this.trendOneYearRadRadioButton.Size = new System.Drawing.Size(54, 18);
            this.trendOneYearRadRadioButton.TabIndex = 2;
            this.trendOneYearRadRadioButton.Text = "1 Year";
            this.trendOneYearRadRadioButton.ThemeName = "Office2007Black";
            this.trendOneYearRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.oneYearRadRadioButton_ToggleStateChanged);
            // 
            // trendSixMonthsRadRadioButton
            // 
            this.trendSixMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendSixMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendSixMonthsRadRadioButton.Location = new System.Drawing.Point(13, 47);
            this.trendSixMonthsRadRadioButton.Name = "trendSixMonthsRadRadioButton";
            this.trendSixMonthsRadRadioButton.Size = new System.Drawing.Size(74, 18);
            this.trendSixMonthsRadRadioButton.TabIndex = 1;
            this.trendSixMonthsRadRadioButton.Text = "6 Months";
            this.trendSixMonthsRadRadioButton.ThemeName = "Office2007Black";
            this.trendSixMonthsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.sixMonthsRadRadioButton_ToggleStateChanged);
            // 
            // trendThreeMonthsRadRadioButton
            // 
            this.trendThreeMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendThreeMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendThreeMonthsRadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.trendThreeMonthsRadRadioButton.Name = "trendThreeMonthsRadRadioButton";
            this.trendThreeMonthsRadRadioButton.Size = new System.Drawing.Size(73, 18);
            this.trendThreeMonthsRadRadioButton.TabIndex = 0;
            this.trendThreeMonthsRadRadioButton.Text = "3 Months";
            this.trendThreeMonthsRadRadioButton.ThemeName = "Office2007Black";
            this.trendThreeMonthsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.threeMonthsRadRadioButton_ToggleStateChanged);
            // 
            // frequencyRadCheckBox
            // 
            this.frequencyRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.frequencyRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frequencyRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.frequencyRadCheckBox.Location = new System.Drawing.Point(133, 108);
            this.frequencyRadCheckBox.Name = "frequencyRadCheckBox";
            this.frequencyRadCheckBox.Size = new System.Drawing.Size(98, 16);
            this.frequencyRadCheckBox.TabIndex = 58;
            this.frequencyRadCheckBox.Text = "Frequency (Hz)";
            this.frequencyRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // tangentCRadCheckBox
            // 
            this.tangentCRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.tangentCRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentCRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentCRadCheckBox.Location = new System.Drawing.Point(1, 234);
            this.tangentCRadCheckBox.Name = "tangentCRadCheckBox";
            this.tangentCRadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.tangentCRadCheckBox.TabIndex = 67;
            this.tangentCRadCheckBox.Text = "Tangent 3 (%)";
            this.tangentCRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // tangentBRadCheckBox
            // 
            this.tangentBRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.tangentBRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentBRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentBRadCheckBox.Location = new System.Drawing.Point(1, 213);
            this.tangentBRadCheckBox.Name = "tangentBRadCheckBox";
            this.tangentBRadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.tangentBRadCheckBox.TabIndex = 68;
            this.tangentBRadCheckBox.Text = "Tangent 2 (%)";
            this.tangentBRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // tangentARadCheckBox
            // 
            this.tangentARadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.tangentARadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentARadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentARadCheckBox.Location = new System.Drawing.Point(1, 192);
            this.tangentARadCheckBox.Name = "tangentARadCheckBox";
            this.tangentARadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.tangentARadCheckBox.TabIndex = 69;
            this.tangentARadCheckBox.Text = "Tangent 1 (%)";
            this.tangentARadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // capacitanceCRadCheckBox
            // 
            this.capacitanceCRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.capacitanceCRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceCRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceCRadCheckBox.Location = new System.Drawing.Point(1, 171);
            this.capacitanceCRadCheckBox.Name = "capacitanceCRadCheckBox";
            this.capacitanceCRadCheckBox.Size = new System.Drawing.Size(116, 16);
            this.capacitanceCRadCheckBox.TabIndex = 66;
            this.capacitanceCRadCheckBox.Text = "Capacitance 3 (pF)";
            this.capacitanceCRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // capacitanceBRadCheckBox
            // 
            this.capacitanceBRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.capacitanceBRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceBRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceBRadCheckBox.Location = new System.Drawing.Point(1, 150);
            this.capacitanceBRadCheckBox.Name = "capacitanceBRadCheckBox";
            this.capacitanceBRadCheckBox.Size = new System.Drawing.Size(116, 16);
            this.capacitanceBRadCheckBox.TabIndex = 65;
            this.capacitanceBRadCheckBox.Text = "Capacitance 2 (pF)";
            this.capacitanceBRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // capacitanceARadCheckBox
            // 
            this.capacitanceARadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.capacitanceARadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceARadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceARadCheckBox.Location = new System.Drawing.Point(1, 129);
            this.capacitanceARadCheckBox.Name = "capacitanceARadCheckBox";
            this.capacitanceARadCheckBox.Size = new System.Drawing.Size(116, 16);
            this.capacitanceARadCheckBox.TabIndex = 64;
            this.capacitanceARadCheckBox.Text = "Capacitance 1 (pF)";
            this.capacitanceARadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentPhaseACRadCheckBox
            // 
            this.bushingCurrentPhaseACRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentPhaseACRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentPhaseACRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentPhaseACRadCheckBox.Location = new System.Drawing.Point(133, 171);
            this.bushingCurrentPhaseACRadCheckBox.Name = "bushingCurrentPhaseACRadCheckBox";
            this.bushingCurrentPhaseACRadCheckBox.Size = new System.Drawing.Size(129, 16);
            this.bushingCurrentPhaseACRadCheckBox.TabIndex = 63;
            this.bushingCurrentPhaseACRadCheckBox.Text = "Phase Angle 13 (deg)";
            this.bushingCurrentPhaseACRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentPhaseABRadCheckBox
            // 
            this.bushingCurrentPhaseABRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentPhaseABRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentPhaseABRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentPhaseABRadCheckBox.Location = new System.Drawing.Point(133, 150);
            this.bushingCurrentPhaseABRadCheckBox.Name = "bushingCurrentPhaseABRadCheckBox";
            this.bushingCurrentPhaseABRadCheckBox.Size = new System.Drawing.Size(129, 16);
            this.bushingCurrentPhaseABRadCheckBox.TabIndex = 62;
            this.bushingCurrentPhaseABRadCheckBox.Text = "Phase Angle 12 (deg)";
            this.bushingCurrentPhaseABRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentPhaseAARadCheckBox
            // 
            this.bushingCurrentPhaseAARadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentPhaseAARadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentPhaseAARadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentPhaseAARadCheckBox.Location = new System.Drawing.Point(133, 129);
            this.bushingCurrentPhaseAARadCheckBox.Name = "bushingCurrentPhaseAARadCheckBox";
            this.bushingCurrentPhaseAARadCheckBox.Size = new System.Drawing.Size(129, 16);
            this.bushingCurrentPhaseAARadCheckBox.TabIndex = 60;
            this.bushingCurrentPhaseAARadCheckBox.Text = "Phase Angle 11 (deg)";
            this.bushingCurrentPhaseAARadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentCRadCheckBox
            // 
            this.bushingCurrentCRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentCRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentCRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentCRadCheckBox.Location = new System.Drawing.Point(133, 234);
            this.bushingCurrentCRadCheckBox.Name = "bushingCurrentCRadCheckBox";
            this.bushingCurrentCRadCheckBox.Size = new System.Drawing.Size(139, 16);
            this.bushingCurrentCRadCheckBox.TabIndex = 59;
            this.bushingCurrentCRadCheckBox.Text = "Bushing Current 3 (mA)";
            this.bushingCurrentCRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentBRadCheckBox
            // 
            this.bushingCurrentBRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentBRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentBRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentBRadCheckBox.Location = new System.Drawing.Point(133, 213);
            this.bushingCurrentBRadCheckBox.Name = "bushingCurrentBRadCheckBox";
            this.bushingCurrentBRadCheckBox.Size = new System.Drawing.Size(139, 16);
            this.bushingCurrentBRadCheckBox.TabIndex = 54;
            this.bushingCurrentBRadCheckBox.Text = "Bushing Current 2 (mA)";
            this.bushingCurrentBRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // bushingCurrentARadCheckBox
            // 
            this.bushingCurrentARadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.bushingCurrentARadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bushingCurrentARadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bushingCurrentARadCheckBox.Location = new System.Drawing.Point(133, 192);
            this.bushingCurrentARadCheckBox.Name = "bushingCurrentARadCheckBox";
            this.bushingCurrentARadCheckBox.Size = new System.Drawing.Size(139, 16);
            this.bushingCurrentARadCheckBox.TabIndex = 53;
            this.bushingCurrentARadCheckBox.Text = "Bushing Current 1 (mA)";
            this.bushingCurrentARadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // phaseTemperatureCoefficientRadCheckBox
            // 
            this.phaseTemperatureCoefficientRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phaseTemperatureCoefficientRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseTemperatureCoefficientRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phaseTemperatureCoefficientRadCheckBox.Location = new System.Drawing.Point(133, 255);
            this.phaseTemperatureCoefficientRadCheckBox.Name = "phaseTemperatureCoefficientRadCheckBox";
            this.phaseTemperatureCoefficientRadCheckBox.Size = new System.Drawing.Size(144, 16);
            this.phaseTemperatureCoefficientRadCheckBox.TabIndex = 52;
            this.phaseTemperatureCoefficientRadCheckBox.Text = "Phase Temp Coeff (deg)";
            this.phaseTemperatureCoefficientRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // gammaTrendRadCheckBox
            // 
            this.gammaTrendRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.gammaTrendRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gammaTrendRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gammaTrendRadCheckBox.Location = new System.Drawing.Point(133, 87);
            this.gammaTrendRadCheckBox.Name = "gammaTrendRadCheckBox";
            this.gammaTrendRadCheckBox.Size = new System.Drawing.Size(139, 16);
            this.gammaTrendRadCheckBox.TabIndex = 56;
            this.gammaTrendRadCheckBox.Text = "Gamma Trend (%/year)";
            this.gammaTrendRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // gammaPhaseRadCheckBox
            // 
            this.gammaPhaseRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.gammaPhaseRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gammaPhaseRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gammaPhaseRadCheckBox.Location = new System.Drawing.Point(1, 108);
            this.gammaPhaseRadCheckBox.Name = "gammaPhaseRadCheckBox";
            this.gammaPhaseRadCheckBox.Size = new System.Drawing.Size(125, 16);
            this.gammaPhaseRadCheckBox.TabIndex = 55;
            this.gammaPhaseRadCheckBox.Text = "Gamma Phase (deg)";
            this.gammaPhaseRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // gammaRadCheckBox
            // 
            this.gammaRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.gammaRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gammaRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gammaRadCheckBox.Location = new System.Drawing.Point(1, 87);
            this.gammaRadCheckBox.Name = "gammaRadCheckBox";
            this.gammaRadCheckBox.Size = new System.Drawing.Size(81, 16);
            this.gammaRadCheckBox.TabIndex = 51;
            this.gammaRadCheckBox.Text = "Gamma (%)";
            this.gammaRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // trendZoomButtonsRadPanel
            // 
            this.trendZoomButtonsRadPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendPointerRadioButton);
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendZoomInRadioButton);
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendZoomOutRadioButton);
            this.trendZoomButtonsRadPanel.Location = new System.Drawing.Point(3, 3);
            this.trendZoomButtonsRadPanel.Name = "trendZoomButtonsRadPanel";
            this.trendZoomButtonsRadPanel.Size = new System.Drawing.Size(96, 78);
            this.trendZoomButtonsRadPanel.TabIndex = 50;
            this.trendZoomButtonsRadPanel.ThemeName = "ControlDefault";
            // 
            // trendPointerRadioButton
            // 
            this.trendPointerRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendPointerRadioButton.Checked = true;
            this.trendPointerRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendPointerRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendPointerRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendPointerRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendPointerRadioButton.Image")));
            this.trendPointerRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendPointerRadioButton.Location = new System.Drawing.Point(3, 3);
            this.trendPointerRadioButton.Name = "trendPointerRadioButton";
            this.trendPointerRadioButton.Size = new System.Drawing.Size(90, 25);
            this.trendPointerRadioButton.TabIndex = 9;
            this.trendPointerRadioButton.TabStop = true;
            this.trendPointerRadioButton.Text = "      Pointer";
            this.trendPointerRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.trendPointerRadioButton.CheckedChanged += new System.EventHandler(this.trendPointerRadioButton_CheckedChanged);
            // 
            // trendZoomInRadioButton
            // 
            this.trendZoomInRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendZoomInRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendZoomInRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendZoomInRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendZoomInRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendZoomInRadioButton.Image")));
            this.trendZoomInRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendZoomInRadioButton.Location = new System.Drawing.Point(3, 27);
            this.trendZoomInRadioButton.Name = "trendZoomInRadioButton";
            this.trendZoomInRadioButton.Size = new System.Drawing.Size(90, 25);
            this.trendZoomInRadioButton.TabIndex = 10;
            this.trendZoomInRadioButton.Text = "      Zoom In";
            this.trendZoomInRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.trendZoomInRadioButton.CheckedChanged += new System.EventHandler(this.trendZoomInRadioButton_CheckedChanged);
            // 
            // trendZoomOutRadioButton
            // 
            this.trendZoomOutRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendZoomOutRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendZoomOutRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendZoomOutRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendZoomOutRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendZoomOutRadioButton.Image")));
            this.trendZoomOutRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendZoomOutRadioButton.Location = new System.Drawing.Point(3, 51);
            this.trendZoomOutRadioButton.Name = "trendZoomOutRadioButton";
            this.trendZoomOutRadioButton.Size = new System.Drawing.Size(90, 24);
            this.trendZoomOutRadioButton.TabIndex = 11;
            this.trendZoomOutRadioButton.Text = "      Zoom Out";
            this.trendZoomOutRadioButton.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.trendZoomOutRadioButton.CheckedChanged += new System.EventHandler(this.trendZoomOutRadioButton_CheckedChanged);
            // 
            // trendRadHScrollBar
            // 
            this.trendRadHScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trendRadHScrollBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendRadHScrollBar.Location = new System.Drawing.Point(284, 628);
            this.trendRadHScrollBar.Name = "trendRadHScrollBar";
            this.trendRadHScrollBar.Size = new System.Drawing.Size(708, 19);
            this.trendRadHScrollBar.TabIndex = 48;
            this.trendRadHScrollBar.Text = "radHScrollBar1";
            this.trendRadHScrollBar.ThemeName = "Office2007Black";
            this.trendRadHScrollBar.ValueChanged += new System.EventHandler(this.trendRadHScrollBar_ValueChanged);
            // 
            // polarRadPageViewPage
            // 
            this.polarRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.polarRadPageViewPage.Controls.Add(this.polarPhasesSwappedRadLabel);
            this.polarRadPageViewPage.Controls.Add(this.showRadialLabelsRadCheckBox);
            this.polarRadPageViewPage.Controls.Add(this.polarVcursorGroup2Label);
            this.polarRadPageViewPage.Controls.Add(this.polarHcursorGroup2Label);
            this.polarRadPageViewPage.Controls.Add(this.polarHcursorGroup1Label);
            this.polarRadPageViewPage.Controls.Add(this.polarVcursorGroup1Label);
            this.polarRadPageViewPage.Controls.Add(this.phaseSegmentPolarRadGroupBox);
            this.polarRadPageViewPage.Controls.Add(this.polarDataDisplayChoiceRadGroupBox);
            this.polarRadPageViewPage.Controls.Add(this.polarRadVScrollBar);
            this.polarRadPageViewPage.Controls.Add(this.polarRadHScrollBar);
            this.polarRadPageViewPage.Controls.Add(this.polarWinChartViewer);
            this.polarRadPageViewPage.Controls.Add(this.polarZoomButtonsRadPanel);
            this.polarRadPageViewPage.Controls.Add(this.polarDrawRadGroupBox);
            this.polarRadPageViewPage.Controls.Add(this.polarDataGroupSelectionRadGroupBox);
            this.polarRadPageViewPage.Controls.Add(this.polarDataAgeRadGroupBox);
            this.polarRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarRadPageViewPage.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.polarRadPageViewPage.Name = "polarRadPageViewPage";
            this.polarRadPageViewPage.Size = new System.Drawing.Size(995, 690);
            this.polarRadPageViewPage.Text = "Polar Diagram";
            // 
            // polarPhasesSwappedRadLabel
            // 
            this.polarPhasesSwappedRadLabel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarPhasesSwappedRadLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.polarPhasesSwappedRadLabel.Location = new System.Drawing.Point(770, 25);
            this.polarPhasesSwappedRadLabel.Name = "polarPhasesSwappedRadLabel";
            this.polarPhasesSwappedRadLabel.Size = new System.Drawing.Size(189, 16);
            this.polarPhasesSwappedRadLabel.TabIndex = 95;
            this.polarPhasesSwappedRadLabel.Text = "Phases 2 and 3 have been swapped";
            this.polarPhasesSwappedRadLabel.ThemeName = "ControlDefault";
            // 
            // showRadialLabelsRadCheckBox
            // 
            this.showRadialLabelsRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showRadialLabelsRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.showRadialLabelsRadCheckBox.Location = new System.Drawing.Point(16, 409);
            this.showRadialLabelsRadCheckBox.Name = "showRadialLabelsRadCheckBox";
            this.showRadialLabelsRadCheckBox.Size = new System.Drawing.Size(120, 16);
            this.showRadialLabelsRadCheckBox.TabIndex = 94;
            this.showRadialLabelsRadCheckBox.Text = "Show Radial Labels";
            this.showRadialLabelsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.showRadialLabelsRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelectionPolar_ToggleStateChanged);
            // 
            // polarVcursorGroup2Label
            // 
            this.polarVcursorGroup2Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.polarVcursorGroup2Label.Location = new System.Drawing.Point(442, 389);
            this.polarVcursorGroup2Label.Name = "polarVcursorGroup2Label";
            this.polarVcursorGroup2Label.Size = new System.Drawing.Size(1, 40);
            this.polarVcursorGroup2Label.TabIndex = 88;
            // 
            // polarHcursorGroup2Label
            // 
            this.polarHcursorGroup2Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.polarHcursorGroup2Label.Location = new System.Drawing.Point(485, 409);
            this.polarHcursorGroup2Label.Name = "polarHcursorGroup2Label";
            this.polarHcursorGroup2Label.Size = new System.Drawing.Size(40, 1);
            this.polarHcursorGroup2Label.TabIndex = 87;
            // 
            // polarHcursorGroup1Label
            // 
            this.polarHcursorGroup1Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.polarHcursorGroup1Label.Location = new System.Drawing.Point(512, 200);
            this.polarHcursorGroup1Label.Name = "polarHcursorGroup1Label";
            this.polarHcursorGroup1Label.Size = new System.Drawing.Size(40, 1);
            this.polarHcursorGroup1Label.TabIndex = 86;
            // 
            // polarVcursorGroup1Label
            // 
            this.polarVcursorGroup1Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.polarVcursorGroup1Label.Location = new System.Drawing.Point(505, 183);
            this.polarVcursorGroup1Label.Name = "polarVcursorGroup1Label";
            this.polarVcursorGroup1Label.Size = new System.Drawing.Size(1, 40);
            this.polarVcursorGroup1Label.TabIndex = 85;
            // 
            // phaseSegmentPolarRadGroupBox
            // 
            this.phaseSegmentPolarRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phaseSegmentPolarRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.capacitanceAPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.tangentCPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.capacitanceBPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.tangentBPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.capacitanceCPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.Controls.Add(this.tangentAPolarRadCheckBox);
            this.phaseSegmentPolarRadGroupBox.FooterImageIndex = -1;
            this.phaseSegmentPolarRadGroupBox.FooterImageKey = "";
            this.phaseSegmentPolarRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phaseSegmentPolarRadGroupBox.HeaderImageIndex = -1;
            this.phaseSegmentPolarRadGroupBox.HeaderImageKey = "";
            this.phaseSegmentPolarRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phaseSegmentPolarRadGroupBox.HeaderText = "Phase Segment";
            this.phaseSegmentPolarRadGroupBox.Location = new System.Drawing.Point(3, 258);
            this.phaseSegmentPolarRadGroupBox.Name = "phaseSegmentPolarRadGroupBox";
            this.phaseSegmentPolarRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phaseSegmentPolarRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phaseSegmentPolarRadGroupBox.Size = new System.Drawing.Size(262, 114);
            this.phaseSegmentPolarRadGroupBox.TabIndex = 54;
            this.phaseSegmentPolarRadGroupBox.Text = "Phase Segment";
            this.phaseSegmentPolarRadGroupBox.ThemeName = "Office2007Black";
            // 
            // capacitanceAPolarRadCheckBox
            // 
            this.capacitanceAPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceAPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceAPolarRadCheckBox.Location = new System.Drawing.Point(14, 29);
            this.capacitanceAPolarRadCheckBox.Name = "capacitanceAPolarRadCheckBox";
            this.capacitanceAPolarRadCheckBox.Size = new System.Drawing.Size(93, 16);
            this.capacitanceAPolarRadCheckBox.TabIndex = 38;
            this.capacitanceAPolarRadCheckBox.Text = "Capacitance 1";
            this.capacitanceAPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.capacitanceAPolarRadCheckBox_ToggleStateChanged);
            // 
            // tangentCPolarRadCheckBox
            // 
            this.tangentCPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentCPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentCPolarRadCheckBox.Location = new System.Drawing.Point(134, 77);
            this.tangentCPolarRadCheckBox.Name = "tangentCPolarRadCheckBox";
            this.tangentCPolarRadCheckBox.Size = new System.Drawing.Size(71, 16);
            this.tangentCPolarRadCheckBox.TabIndex = 41;
            this.tangentCPolarRadCheckBox.Text = "Tangent 3";
            this.tangentCPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.tangentCPolarRadCheckBox_ToggleStateChanged);
            // 
            // capacitanceBPolarRadCheckBox
            // 
            this.capacitanceBPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceBPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceBPolarRadCheckBox.Location = new System.Drawing.Point(14, 53);
            this.capacitanceBPolarRadCheckBox.Name = "capacitanceBPolarRadCheckBox";
            this.capacitanceBPolarRadCheckBox.Size = new System.Drawing.Size(93, 16);
            this.capacitanceBPolarRadCheckBox.TabIndex = 39;
            this.capacitanceBPolarRadCheckBox.Text = "Capacitance 2";
            this.capacitanceBPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.capacitanceBPolarRadCheckBox_ToggleStateChanged);
            // 
            // tangentBPolarRadCheckBox
            // 
            this.tangentBPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentBPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentBPolarRadCheckBox.Location = new System.Drawing.Point(134, 53);
            this.tangentBPolarRadCheckBox.Name = "tangentBPolarRadCheckBox";
            this.tangentBPolarRadCheckBox.Size = new System.Drawing.Size(71, 16);
            this.tangentBPolarRadCheckBox.TabIndex = 42;
            this.tangentBPolarRadCheckBox.Text = "Tangent 2";
            this.tangentBPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.tangentBPolarRadCheckBox_ToggleStateChanged);
            // 
            // capacitanceCPolarRadCheckBox
            // 
            this.capacitanceCPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capacitanceCPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.capacitanceCPolarRadCheckBox.Location = new System.Drawing.Point(14, 77);
            this.capacitanceCPolarRadCheckBox.Name = "capacitanceCPolarRadCheckBox";
            this.capacitanceCPolarRadCheckBox.Size = new System.Drawing.Size(93, 16);
            this.capacitanceCPolarRadCheckBox.TabIndex = 40;
            this.capacitanceCPolarRadCheckBox.Text = "Capacitance 3";
            this.capacitanceCPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.capacitanceCPolarRadCheckBox_ToggleStateChanged);
            // 
            // tangentAPolarRadCheckBox
            // 
            this.tangentAPolarRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentAPolarRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentAPolarRadCheckBox.Location = new System.Drawing.Point(134, 29);
            this.tangentAPolarRadCheckBox.Name = "tangentAPolarRadCheckBox";
            this.tangentAPolarRadCheckBox.Size = new System.Drawing.Size(71, 16);
            this.tangentAPolarRadCheckBox.TabIndex = 43;
            this.tangentAPolarRadCheckBox.Text = "Tangent 1";
            this.tangentAPolarRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.tangentAPolarRadCheckBox_ToggleStateChanged);
            // 
            // polarDataDisplayChoiceRadGroupBox
            // 
            this.polarDataDisplayChoiceRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.polarDataDisplayChoiceRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.polarDataDisplayChoiceRadGroupBox.Controls.Add(this.temperatureCoefficientPolarRadRadioButton);
            this.polarDataDisplayChoiceRadGroupBox.Controls.Add(this.gammaPolarRadRadioButton);
            this.polarDataDisplayChoiceRadGroupBox.FooterImageIndex = -1;
            this.polarDataDisplayChoiceRadGroupBox.FooterImageKey = "";
            this.polarDataDisplayChoiceRadGroupBox.HeaderImageIndex = -1;
            this.polarDataDisplayChoiceRadGroupBox.HeaderImageKey = "";
            this.polarDataDisplayChoiceRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.polarDataDisplayChoiceRadGroupBox.HeaderText = "Choose Data to Display";
            this.polarDataDisplayChoiceRadGroupBox.Location = new System.Drawing.Point(3, 194);
            this.polarDataDisplayChoiceRadGroupBox.Name = "polarDataDisplayChoiceRadGroupBox";
            this.polarDataDisplayChoiceRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.polarDataDisplayChoiceRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.polarDataDisplayChoiceRadGroupBox.Size = new System.Drawing.Size(262, 57);
            this.polarDataDisplayChoiceRadGroupBox.TabIndex = 53;
            this.polarDataDisplayChoiceRadGroupBox.Text = "Choose Data to Display";
            this.polarDataDisplayChoiceRadGroupBox.ThemeName = "Office2007Black";
            // 
            // temperatureCoefficientPolarRadRadioButton
            // 
            this.temperatureCoefficientPolarRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureCoefficientPolarRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temperatureCoefficientPolarRadRadioButton.Location = new System.Drawing.Point(103, 23);
            this.temperatureCoefficientPolarRadRadioButton.Name = "temperatureCoefficientPolarRadRadioButton";
            this.temperatureCoefficientPolarRadRadioButton.Size = new System.Drawing.Size(148, 18);
            this.temperatureCoefficientPolarRadRadioButton.TabIndex = 1;
            this.temperatureCoefficientPolarRadRadioButton.Text = "Temperature Coefficient";
            this.temperatureCoefficientPolarRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.temperatureCoefficientPolarRadRadioButton_ToggleStateChanged);
            // 
            // gammaPolarRadRadioButton
            // 
            this.gammaPolarRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gammaPolarRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gammaPolarRadRadioButton.Location = new System.Drawing.Point(13, 23);
            this.gammaPolarRadRadioButton.Name = "gammaPolarRadRadioButton";
            this.gammaPolarRadRadioButton.Size = new System.Drawing.Size(61, 18);
            this.gammaPolarRadRadioButton.TabIndex = 0;
            this.gammaPolarRadRadioButton.TabStop = true;
            this.gammaPolarRadRadioButton.Text = "Gamma";
            this.gammaPolarRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.gammaPolarRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.gammaPolarRadRadioButton_ToggleStateChanged);
            // 
            // polarRadVScrollBar
            // 
            this.polarRadVScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.polarRadVScrollBar.Location = new System.Drawing.Point(975, 5);
            this.polarRadVScrollBar.Name = "polarRadVScrollBar";
            this.polarRadVScrollBar.Size = new System.Drawing.Size(17, 657);
            this.polarRadVScrollBar.TabIndex = 52;
            this.polarRadVScrollBar.Text = "radVScrollBar1";
            this.polarRadVScrollBar.ThemeName = "Office2007Black";
            this.polarRadVScrollBar.ValueChanged += new System.EventHandler(this.polarRadVScrollBar_ValueChanged);
            // 
            // polarRadHScrollBar
            // 
            this.polarRadHScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.polarRadHScrollBar.Location = new System.Drawing.Point(271, 668);
            this.polarRadHScrollBar.Name = "polarRadHScrollBar";
            this.polarRadHScrollBar.Size = new System.Drawing.Size(700, 17);
            this.polarRadHScrollBar.TabIndex = 51;
            this.polarRadHScrollBar.Text = "radHScrollBar1";
            this.polarRadHScrollBar.ThemeName = "Office2007Black";
            this.polarRadHScrollBar.ValueChanged += new System.EventHandler(this.polarRadHScrollBar_ValueChanged);
            // 
            // polarWinChartViewer
            // 
            this.polarWinChartViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.polarWinChartViewer.Location = new System.Drawing.Point(271, 5);
            this.polarWinChartViewer.Name = "polarWinChartViewer";
            this.polarWinChartViewer.Size = new System.Drawing.Size(700, 660);
            this.polarWinChartViewer.TabIndex = 50;
            this.polarWinChartViewer.TabStop = false;
            this.polarWinChartViewer.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.polarWinChartViewer_ClickHotSpot);
            this.polarWinChartViewer.ViewPortChanged += new ChartDirector.WinViewPortEventHandler(this.polarWinChartViewer_ViewPortChanged);
            this.polarWinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.polarWinChartViewer_MouseClick);
            this.polarWinChartViewer.MouseEnter += new System.EventHandler(this.polarWinChartViewer_MouseEnter);
            this.polarWinChartViewer.MouseLeave += new System.EventHandler(this.polarWinChartViewer_MouseLeave);
            // 
            // polarZoomButtonsRadPanel
            // 
            this.polarZoomButtonsRadPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.polarZoomButtonsRadPanel.Controls.Add(this.polarPointerRadioButton);
            this.polarZoomButtonsRadPanel.Controls.Add(this.polarZoomInRadioButton);
            this.polarZoomButtonsRadPanel.Controls.Add(this.polarZoomOutRadioButton);
            this.polarZoomButtonsRadPanel.Location = new System.Drawing.Point(3, 3);
            this.polarZoomButtonsRadPanel.Name = "polarZoomButtonsRadPanel";
            this.polarZoomButtonsRadPanel.Size = new System.Drawing.Size(96, 78);
            this.polarZoomButtonsRadPanel.TabIndex = 49;
            this.polarZoomButtonsRadPanel.ThemeName = "ControlDefault";
            // 
            // polarPointerRadioButton
            // 
            this.polarPointerRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.polarPointerRadioButton.Checked = true;
            this.polarPointerRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.polarPointerRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarPointerRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarPointerRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("polarPointerRadioButton.Image")));
            this.polarPointerRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.polarPointerRadioButton.Location = new System.Drawing.Point(3, 3);
            this.polarPointerRadioButton.Name = "polarPointerRadioButton";
            this.polarPointerRadioButton.Size = new System.Drawing.Size(90, 25);
            this.polarPointerRadioButton.TabIndex = 9;
            this.polarPointerRadioButton.TabStop = true;
            this.polarPointerRadioButton.Text = "      Pointer";
            this.polarPointerRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.polarPointerRadioButton.CheckedChanged += new System.EventHandler(this.polarPointerRadioButton_CheckedChanged);
            // 
            // polarZoomInRadioButton
            // 
            this.polarZoomInRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.polarZoomInRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.polarZoomInRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarZoomInRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarZoomInRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("polarZoomInRadioButton.Image")));
            this.polarZoomInRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.polarZoomInRadioButton.Location = new System.Drawing.Point(3, 27);
            this.polarZoomInRadioButton.Name = "polarZoomInRadioButton";
            this.polarZoomInRadioButton.Size = new System.Drawing.Size(90, 25);
            this.polarZoomInRadioButton.TabIndex = 10;
            this.polarZoomInRadioButton.Text = "      Zoom In";
            this.polarZoomInRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.polarZoomInRadioButton.CheckedChanged += new System.EventHandler(this.polarZoomInRadioButton_CheckedChanged);
            // 
            // polarZoomOutRadioButton
            // 
            this.polarZoomOutRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.polarZoomOutRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.polarZoomOutRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarZoomOutRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarZoomOutRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("polarZoomOutRadioButton.Image")));
            this.polarZoomOutRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.polarZoomOutRadioButton.Location = new System.Drawing.Point(3, 51);
            this.polarZoomOutRadioButton.Name = "polarZoomOutRadioButton";
            this.polarZoomOutRadioButton.Size = new System.Drawing.Size(90, 24);
            this.polarZoomOutRadioButton.TabIndex = 11;
            this.polarZoomOutRadioButton.Text = "      Zoom Out";
            this.polarZoomOutRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.polarZoomOutRadioButton.CheckedChanged += new System.EventHandler(this.polarZoomOutRadioButton_CheckedChanged);
            // 
            // polarDrawRadGroupBox
            // 
            this.polarDrawRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.polarDrawRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.polarDrawRadGroupBox.Controls.Add(this.polarManualDrawRadRadioButton);
            this.polarDrawRadGroupBox.Controls.Add(this.polarAutoDrawRadRadioButton);
            this.polarDrawRadGroupBox.Controls.Add(this.polarDrawGraphsRadButton);
            this.polarDrawRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarDrawRadGroupBox.FooterImageIndex = -1;
            this.polarDrawRadGroupBox.FooterImageKey = "";
            this.polarDrawRadGroupBox.HeaderImageIndex = -1;
            this.polarDrawRadGroupBox.HeaderImageKey = "";
            this.polarDrawRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.polarDrawRadGroupBox.HeaderText = "Graph Method";
            this.polarDrawRadGroupBox.Location = new System.Drawing.Point(3, 622);
            this.polarDrawRadGroupBox.Name = "polarDrawRadGroupBox";
            this.polarDrawRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.polarDrawRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.polarDrawRadGroupBox.Size = new System.Drawing.Size(262, 65);
            this.polarDrawRadGroupBox.TabIndex = 48;
            this.polarDrawRadGroupBox.Text = "Graph Method";
            this.polarDrawRadGroupBox.ThemeName = "Office2007Black";
            // 
            // polarManualDrawRadRadioButton
            // 
            this.polarManualDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarManualDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarManualDrawRadRadioButton.Location = new System.Drawing.Point(13, 42);
            this.polarManualDrawRadRadioButton.Name = "polarManualDrawRadRadioButton";
            this.polarManualDrawRadRadioButton.Size = new System.Drawing.Size(101, 18);
            this.polarManualDrawRadRadioButton.TabIndex = 1;
            this.polarManualDrawRadRadioButton.Text = "Manual Draw";
            this.polarManualDrawRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarManualDrawRadRadioButton_ToggleStateChanged);
            // 
            // polarAutoDrawRadRadioButton
            // 
            this.polarAutoDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarAutoDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarAutoDrawRadRadioButton.Location = new System.Drawing.Point(13, 22);
            this.polarAutoDrawRadRadioButton.Name = "polarAutoDrawRadRadioButton";
            this.polarAutoDrawRadRadioButton.Size = new System.Drawing.Size(110, 18);
            this.polarAutoDrawRadRadioButton.TabIndex = 0;
            this.polarAutoDrawRadRadioButton.TabStop = true;
            this.polarAutoDrawRadRadioButton.Text = "Automatic Draw";
            this.polarAutoDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.polarAutoDrawRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarAutoDrawRadRadioButton_ToggleStateChanged);
            // 
            // polarDrawGraphsRadButton
            // 
            this.polarDrawGraphsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarDrawGraphsRadButton.Location = new System.Drawing.Point(130, 22);
            this.polarDrawGraphsRadButton.Name = "polarDrawGraphsRadButton";
            this.polarDrawGraphsRadButton.Size = new System.Drawing.Size(120, 34);
            this.polarDrawGraphsRadButton.TabIndex = 28;
            this.polarDrawGraphsRadButton.Text = "Draw Graph";
            this.polarDrawGraphsRadButton.ThemeName = "Office2007Black";
            this.polarDrawGraphsRadButton.Click += new System.EventHandler(this.polarDrawGraphsRadButton_Click);
            // 
            // polarDataGroupSelectionRadGroupBox
            // 
            this.polarDataGroupSelectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.polarDataGroupSelectionRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.polarDataGroupSelectionRadGroupBox.Controls.Add(this.polarAllGroupsRadRadioButton);
            this.polarDataGroupSelectionRadGroupBox.Controls.Add(this.polarGroup2RadRadioButton);
            this.polarDataGroupSelectionRadGroupBox.Controls.Add(this.polarGroup1RadRadioButton);
            this.polarDataGroupSelectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarDataGroupSelectionRadGroupBox.FooterImageIndex = -1;
            this.polarDataGroupSelectionRadGroupBox.FooterImageKey = "";
            this.polarDataGroupSelectionRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarDataGroupSelectionRadGroupBox.HeaderImageIndex = -1;
            this.polarDataGroupSelectionRadGroupBox.HeaderImageKey = "";
            this.polarDataGroupSelectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.polarDataGroupSelectionRadGroupBox.HeaderText = "Data Group Shown";
            this.polarDataGroupSelectionRadGroupBox.Location = new System.Drawing.Point(118, 87);
            this.polarDataGroupSelectionRadGroupBox.Name = "polarDataGroupSelectionRadGroupBox";
            this.polarDataGroupSelectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.polarDataGroupSelectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.polarDataGroupSelectionRadGroupBox.Size = new System.Drawing.Size(123, 101);
            this.polarDataGroupSelectionRadGroupBox.TabIndex = 47;
            this.polarDataGroupSelectionRadGroupBox.Text = "Data Group Shown";
            this.polarDataGroupSelectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // polarAllGroupsRadRadioButton
            // 
            this.polarAllGroupsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarAllGroupsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarAllGroupsRadRadioButton.Location = new System.Drawing.Point(14, 72);
            this.polarAllGroupsRadRadioButton.Name = "polarAllGroupsRadRadioButton";
            this.polarAllGroupsRadRadioButton.Size = new System.Drawing.Size(83, 18);
            this.polarAllGroupsRadRadioButton.TabIndex = 2;
            this.polarAllGroupsRadRadioButton.Text = "Both Groups";
            this.polarAllGroupsRadRadioButton.ThemeName = "Office2007Black";
            this.polarAllGroupsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarAllGroupsRadRadioButton_ToggleStateChanged);
            // 
            // polarGroup2RadRadioButton
            // 
            this.polarGroup2RadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarGroup2RadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarGroup2RadRadioButton.Location = new System.Drawing.Point(14, 48);
            this.polarGroup2RadRadioButton.Name = "polarGroup2RadRadioButton";
            this.polarGroup2RadRadioButton.Size = new System.Drawing.Size(61, 18);
            this.polarGroup2RadRadioButton.TabIndex = 1;
            this.polarGroup2RadRadioButton.Text = "Group 2";
            this.polarGroup2RadRadioButton.ThemeName = "Office2007Black";
            this.polarGroup2RadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarGroup2RadRadioButton_ToggleStateChanged);
            // 
            // polarGroup1RadRadioButton
            // 
            this.polarGroup1RadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarGroup1RadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarGroup1RadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.polarGroup1RadRadioButton.Name = "polarGroup1RadRadioButton";
            this.polarGroup1RadRadioButton.Size = new System.Drawing.Size(61, 18);
            this.polarGroup1RadRadioButton.TabIndex = 0;
            this.polarGroup1RadRadioButton.TabStop = true;
            this.polarGroup1RadRadioButton.Text = "Group 1";
            this.polarGroup1RadRadioButton.ThemeName = "Office2007Black";
            this.polarGroup1RadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.polarGroup1RadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarGroup1RadRadioButton_ToggleStateChanged);
            // 
            // polarDataAgeRadGroupBox
            // 
            this.polarDataAgeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.polarDataAgeRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.polarDataAgeRadGroupBox.Controls.Add(this.polarOneYearRadRadioButton);
            this.polarDataAgeRadGroupBox.Controls.Add(this.polarAllDataRadRadioButton);
            this.polarDataAgeRadGroupBox.Controls.Add(this.polarSixMonthsRadRadioButton);
            this.polarDataAgeRadGroupBox.Controls.Add(this.polarThreeMonthsRadRadioButton);
            this.polarDataAgeRadGroupBox.FooterImageIndex = -1;
            this.polarDataAgeRadGroupBox.FooterImageKey = "";
            this.polarDataAgeRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarDataAgeRadGroupBox.HeaderImageIndex = -1;
            this.polarDataAgeRadGroupBox.HeaderImageKey = "";
            this.polarDataAgeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.polarDataAgeRadGroupBox.HeaderText = "Data Age";
            this.polarDataAgeRadGroupBox.Location = new System.Drawing.Point(118, 3);
            this.polarDataAgeRadGroupBox.Name = "polarDataAgeRadGroupBox";
            this.polarDataAgeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.polarDataAgeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.polarDataAgeRadGroupBox.Size = new System.Drawing.Size(147, 78);
            this.polarDataAgeRadGroupBox.TabIndex = 46;
            this.polarDataAgeRadGroupBox.Text = "Data Age";
            this.polarDataAgeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // polarOneYearRadRadioButton
            // 
            this.polarOneYearRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarOneYearRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarOneYearRadRadioButton.Location = new System.Drawing.Point(84, 24);
            this.polarOneYearRadRadioButton.Name = "polarOneYearRadRadioButton";
            this.polarOneYearRadRadioButton.Size = new System.Drawing.Size(54, 18);
            this.polarOneYearRadRadioButton.TabIndex = 2;
            this.polarOneYearRadRadioButton.Text = "1 Year";
            this.polarOneYearRadRadioButton.ThemeName = "Office2007Black";
            this.polarOneYearRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarOneYearRadRadioButton_ToggleStateChanged);
            // 
            // polarAllDataRadRadioButton
            // 
            this.polarAllDataRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarAllDataRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarAllDataRadRadioButton.Location = new System.Drawing.Point(84, 47);
            this.polarAllDataRadRadioButton.Name = "polarAllDataRadRadioButton";
            this.polarAllDataRadRadioButton.Size = new System.Drawing.Size(50, 18);
            this.polarAllDataRadRadioButton.TabIndex = 3;
            this.polarAllDataRadRadioButton.TabStop = true;
            this.polarAllDataRadRadioButton.Text = "All";
            this.polarAllDataRadRadioButton.ThemeName = "Office2007Black";
            this.polarAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.polarAllDataRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarAllDataRadRadioButton_ToggleStateChanged);
            // 
            // polarSixMonthsRadRadioButton
            // 
            this.polarSixMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarSixMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarSixMonthsRadRadioButton.Location = new System.Drawing.Point(13, 47);
            this.polarSixMonthsRadRadioButton.Name = "polarSixMonthsRadRadioButton";
            this.polarSixMonthsRadRadioButton.Size = new System.Drawing.Size(74, 18);
            this.polarSixMonthsRadRadioButton.TabIndex = 1;
            this.polarSixMonthsRadRadioButton.Text = "6 Months";
            this.polarSixMonthsRadRadioButton.ThemeName = "Office2007Black";
            this.polarSixMonthsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarSixMonthsRadRadioButton_ToggleStateChanged);
            // 
            // polarThreeMonthsRadRadioButton
            // 
            this.polarThreeMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.polarThreeMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.polarThreeMonthsRadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.polarThreeMonthsRadRadioButton.Name = "polarThreeMonthsRadRadioButton";
            this.polarThreeMonthsRadRadioButton.Size = new System.Drawing.Size(73, 18);
            this.polarThreeMonthsRadRadioButton.TabIndex = 0;
            this.polarThreeMonthsRadRadioButton.Text = "3 Months";
            this.polarThreeMonthsRadRadioButton.ThemeName = "Office2007Black";
            this.polarThreeMonthsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.polarThreeMonthsRadRadioButton_ToggleStateChanged);
            // 
            // initialBalanceDataRadPageViewPage
            // 
            this.initialBalanceDataRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabMeasurementDateValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabMeasurementDateRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseCRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseCRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseARadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1RadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseARadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2RadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabTemperatureRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabImbalanceRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabImbalancePhaseRadLabel);
            this.initialBalanceDataRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.initialBalanceDataRadPageViewPage.Name = "initialBalanceDataRadPageViewPage";
            this.initialBalanceDataRadPageViewPage.Size = new System.Drawing.Size(995, 690);
            this.initialBalanceDataRadPageViewPage.Text = "Initial Balance Data";
            // 
            // initialBalanceDataTabMeasurementDateValueRadLabel
            // 
            this.initialBalanceDataTabMeasurementDateValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Location = new System.Drawing.Point(169, 54);
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Name = "initialBalanceDataTabMeasurementDateValueRadLabel";
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Size = new System.Drawing.Size(110, 16);
            this.initialBalanceDataTabMeasurementDateValueRadLabel.TabIndex = 53;
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Text = "01/01/2000 00:00:00";
            // 
            // initialBalanceDataTabMeasurementDateRadLabel
            // 
            this.initialBalanceDataTabMeasurementDateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabMeasurementDateRadLabel.Location = new System.Drawing.Point(54, 54);
            this.initialBalanceDataTabMeasurementDateRadLabel.Name = "initialBalanceDataTabMeasurementDateRadLabel";
            this.initialBalanceDataTabMeasurementDateRadLabel.Size = new System.Drawing.Size(109, 16);
            this.initialBalanceDataTabMeasurementDateRadLabel.TabIndex = 52;
            this.initialBalanceDataTabMeasurementDateRadLabel.Text = "Measurement Date: ";
            // 
            // initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Location = new System.Drawing.Point(565, 340);
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.TabIndex = 46;
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Location = new System.Drawing.Point(565, 250);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.TabIndex = 48;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBushingDataSet2InputSignalShiftPhaseBRadLabel
            // 
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Location = new System.Drawing.Point(565, 310);
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Name = "initialBushingDataSet2InputSignalShiftPhaseBRadLabel";
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.TabIndex = 49;
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Location = new System.Drawing.Point(565, 220);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.TabIndex = 50;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Location = new System.Drawing.Point(565, 280);
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.TabIndex = 47;
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Location = new System.Drawing.Point(565, 190);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.TabIndex = 44;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2TemperatureValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Location = new System.Drawing.Point(565, 160);
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Name = "initialBalanceDataTabBushingSet2TemperatureValueRadLabel";
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.TabIndex = 45;
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Location = new System.Drawing.Point(565, 130);
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Name = "initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel";
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.TabIndex = 51;
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ImbalanceValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Location = new System.Drawing.Point(565, 100);
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Name = "initialBalanceDataTabBushingSet2ImbalanceValueRadLabel";
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.TabIndex = 43;
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Location = new System.Drawing.Point(403, 340);
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.TabIndex = 39;
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Location = new System.Drawing.Point(403, 250);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.TabIndex = 38;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Location = new System.Drawing.Point(403, 310);
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.TabIndex = 41;
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Location = new System.Drawing.Point(403, 220);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.TabIndex = 40;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Location = new System.Drawing.Point(403, 280);
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.TabIndex = 35;
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Location = new System.Drawing.Point(403, 190);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.TabIndex = 36;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1TemperatureValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Location = new System.Drawing.Point(403, 160);
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Name = "initialBalanceDataTabBushingSet1TemperatureValueRadLabel";
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.TabIndex = 37;
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Location = new System.Drawing.Point(403, 130);
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Name = "initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel";
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.TabIndex = 42;
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ImbalanceValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Location = new System.Drawing.Point(403, 100);
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Name = "initialBalanceDataTabBushingSet1ImbalanceValueRadLabel";
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.TabIndex = 34;
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabShiftPhaseCRadLabel
            // 
            this.initialBalanceDataTabShiftPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseCRadLabel.Location = new System.Drawing.Point(53, 340);
            this.initialBalanceDataTabShiftPhaseCRadLabel.Name = "initialBalanceDataTabShiftPhaseCRadLabel";
            this.initialBalanceDataTabShiftPhaseCRadLabel.Size = new System.Drawing.Size(164, 16);
            this.initialBalanceDataTabShiftPhaseCRadLabel.TabIndex = 32;
            this.initialBalanceDataTabShiftPhaseCRadLabel.Text = "Input signal shift Phase C (deg)";
            // 
            // initialBalanceDataTabShiftPhaseBRadLabel
            // 
            this.initialBalanceDataTabShiftPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseBRadLabel.Location = new System.Drawing.Point(53, 310);
            this.initialBalanceDataTabShiftPhaseBRadLabel.Name = "initialBalanceDataTabShiftPhaseBRadLabel";
            this.initialBalanceDataTabShiftPhaseBRadLabel.Size = new System.Drawing.Size(163, 16);
            this.initialBalanceDataTabShiftPhaseBRadLabel.TabIndex = 33;
            this.initialBalanceDataTabShiftPhaseBRadLabel.Text = "Input signal shift Phase B (deg)";
            // 
            // initialBalanceDataTabAmplitudePhaseCRadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Location = new System.Drawing.Point(53, 250);
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Name = "initialBalanceDataTabAmplitudePhaseCRadLabel";
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.TabIndex = 30;
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Text = "Input signal amplitude Phase C (mV)";
            // 
            // initialBalanceDataTabAmplitudePhaseBRadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Location = new System.Drawing.Point(53, 220);
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Name = "initialBalanceDataTabAmplitudePhaseBRadLabel";
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.TabIndex = 31;
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Text = "Input signal amplitude Phase B (mV)";
            // 
            // initialBalanceDataTabShiftPhaseARadLabel
            // 
            this.initialBalanceDataTabShiftPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseARadLabel.Location = new System.Drawing.Point(53, 280);
            this.initialBalanceDataTabShiftPhaseARadLabel.Name = "initialBalanceDataTabShiftPhaseARadLabel";
            this.initialBalanceDataTabShiftPhaseARadLabel.Size = new System.Drawing.Size(163, 16);
            this.initialBalanceDataTabShiftPhaseARadLabel.TabIndex = 29;
            this.initialBalanceDataTabShiftPhaseARadLabel.Text = "Input signal shift Phase A (deg)";
            // 
            // initialBalanceDataTabBushingSet1RadLabel
            // 
            this.initialBalanceDataTabBushingSet1RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1RadLabel.Location = new System.Drawing.Point(387, 54);
            this.initialBalanceDataTabBushingSet1RadLabel.Name = "initialBalanceDataTabBushingSet1RadLabel";
            this.initialBalanceDataTabBushingSet1RadLabel.Size = new System.Drawing.Size(76, 16);
            this.initialBalanceDataTabBushingSet1RadLabel.TabIndex = 26;
            this.initialBalanceDataTabBushingSet1RadLabel.Text = "Bushing Set 1";
            // 
            // initialBalanceDataTabAmplitudePhaseARadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Location = new System.Drawing.Point(53, 190);
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Name = "initialBalanceDataTabAmplitudePhaseARadLabel";
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseARadLabel.TabIndex = 25;
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Text = "Input signal amplitude Phase A (mV)";
            // 
            // initialBalanceDataTabBushingSet2RadLabel
            // 
            this.initialBalanceDataTabBushingSet2RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2RadLabel.Location = new System.Drawing.Point(547, 54);
            this.initialBalanceDataTabBushingSet2RadLabel.Name = "initialBalanceDataTabBushingSet2RadLabel";
            this.initialBalanceDataTabBushingSet2RadLabel.Size = new System.Drawing.Size(76, 16);
            this.initialBalanceDataTabBushingSet2RadLabel.TabIndex = 28;
            this.initialBalanceDataTabBushingSet2RadLabel.Text = "Bushing Set 2";
            // 
            // initialBalanceDataTabTemperatureRadLabel
            // 
            this.initialBalanceDataTabTemperatureRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabTemperatureRadLabel.Location = new System.Drawing.Point(53, 160);
            this.initialBalanceDataTabTemperatureRadLabel.Name = "initialBalanceDataTabTemperatureRadLabel";
            this.initialBalanceDataTabTemperatureRadLabel.Size = new System.Drawing.Size(95, 16);
            this.initialBalanceDataTabTemperatureRadLabel.TabIndex = 27;
            this.initialBalanceDataTabTemperatureRadLabel.Text = "Temperature (�C)";
            // 
            // initialBalanceDataTabImbalanceRadLabel
            // 
            this.initialBalanceDataTabImbalanceRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabImbalanceRadLabel.Location = new System.Drawing.Point(53, 100);
            this.initialBalanceDataTabImbalanceRadLabel.Name = "initialBalanceDataTabImbalanceRadLabel";
            this.initialBalanceDataTabImbalanceRadLabel.Size = new System.Drawing.Size(79, 16);
            this.initialBalanceDataTabImbalanceRadLabel.TabIndex = 24;
            this.initialBalanceDataTabImbalanceRadLabel.Text = "Imbalance (%)";
            // 
            // initialBalanceDataTabImbalancePhaseRadLabel
            // 
            this.initialBalanceDataTabImbalancePhaseRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabImbalancePhaseRadLabel.Location = new System.Drawing.Point(53, 130);
            this.initialBalanceDataTabImbalancePhaseRadLabel.Name = "initialBalanceDataTabImbalancePhaseRadLabel";
            this.initialBalanceDataTabImbalancePhaseRadLabel.Size = new System.Drawing.Size(144, 16);
            this.initialBalanceDataTabImbalancePhaseRadLabel.TabIndex = 23;
            this.initialBalanceDataTabImbalancePhaseRadLabel.Text = "Imbalance Phase, gradient:";
            // 
            // gridRadPageViewPage
            // 
            this.gridRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.gridRadPageViewPage.Controls.Add(this.saveDataToCsvRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridHideAllRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridExpandAllRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridDataDeleteSelectedRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridDataCancelChangesRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridDataSaveChangesRadButton);
            this.gridRadPageViewPage.Controls.Add(this.gridDataEditRadButton);
            this.gridRadPageViewPage.Controls.Add(this.bhm_DataReadingsRadGridView);
            this.gridRadPageViewPage.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gridRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.gridRadPageViewPage.Name = "gridRadPageViewPage";
            this.gridRadPageViewPage.Size = new System.Drawing.Size(995, 690);
            this.gridRadPageViewPage.Text = "Data Grid";
            // 
            // saveDataToCsvRadButton
            // 
            this.saveDataToCsvRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveDataToCsvRadButton.Location = new System.Drawing.Point(281, 663);
            this.saveDataToCsvRadButton.Name = "saveDataToCsvRadButton";
            this.saveDataToCsvRadButton.Size = new System.Drawing.Size(117, 24);
            this.saveDataToCsvRadButton.TabIndex = 14;
            this.saveDataToCsvRadButton.Text = "Save Data to CSV";
            this.saveDataToCsvRadButton.ThemeName = "Office2007Black";
            this.saveDataToCsvRadButton.Click += new System.EventHandler(this.saveDataToCsvRadButton_Click);
            // 
            // gridHideAllRadButton
            // 
            this.gridHideAllRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridHideAllRadButton.Location = new System.Drawing.Point(107, 663);
            this.gridHideAllRadButton.Name = "gridHideAllRadButton";
            this.gridHideAllRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridHideAllRadButton.TabIndex = 13;
            this.gridHideAllRadButton.Text = "Hide All";
            this.gridHideAllRadButton.ThemeName = "Office2007Black";
            this.gridHideAllRadButton.Click += new System.EventHandler(this.gridHideAllRadButton_Click);
            // 
            // gridExpandAllRadButton
            // 
            this.gridExpandAllRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridExpandAllRadButton.Location = new System.Drawing.Point(3, 663);
            this.gridExpandAllRadButton.Name = "gridExpandAllRadButton";
            this.gridExpandAllRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridExpandAllRadButton.TabIndex = 12;
            this.gridExpandAllRadButton.Text = "Expand All";
            this.gridExpandAllRadButton.ThemeName = "Office2007Black";
            this.gridExpandAllRadButton.Click += new System.EventHandler(this.gridExpandAllRadButton_Click);
            // 
            // gridDataDeleteSelectedRadButton
            // 
            this.gridDataDeleteSelectedRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataDeleteSelectedRadButton.Location = new System.Drawing.Point(814, 663);
            this.gridDataDeleteSelectedRadButton.Name = "gridDataDeleteSelectedRadButton";
            this.gridDataDeleteSelectedRadButton.Size = new System.Drawing.Size(178, 24);
            this.gridDataDeleteSelectedRadButton.TabIndex = 11;
            this.gridDataDeleteSelectedRadButton.Text = "Delete Selected Items";
            this.gridDataDeleteSelectedRadButton.ThemeName = "Office2007Black";
            this.gridDataDeleteSelectedRadButton.Click += new System.EventHandler(this.deleteSelectedRadButton_Click);
            // 
            // gridDataCancelChangesRadButton
            // 
            this.gridDataCancelChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataCancelChangesRadButton.Location = new System.Drawing.Point(679, 663);
            this.gridDataCancelChangesRadButton.Name = "gridDataCancelChangesRadButton";
            this.gridDataCancelChangesRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataCancelChangesRadButton.TabIndex = 10;
            this.gridDataCancelChangesRadButton.Text = "Cancel Changes";
            this.gridDataCancelChangesRadButton.ThemeName = "Office2007Black";
            this.gridDataCancelChangesRadButton.Click += new System.EventHandler(this.gridDataCancelChangesRadButton_Click);
            // 
            // gridDataSaveChangesRadButton
            // 
            this.gridDataSaveChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataSaveChangesRadButton.Location = new System.Drawing.Point(575, 663);
            this.gridDataSaveChangesRadButton.Name = "gridDataSaveChangesRadButton";
            this.gridDataSaveChangesRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataSaveChangesRadButton.TabIndex = 9;
            this.gridDataSaveChangesRadButton.Text = "Save Changes";
            this.gridDataSaveChangesRadButton.ThemeName = "Office2007Black";
            this.gridDataSaveChangesRadButton.Click += new System.EventHandler(this.gridDataSaveChangesRadButton_Click);
            // 
            // gridDataEditRadButton
            // 
            this.gridDataEditRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataEditRadButton.Location = new System.Drawing.Point(471, 663);
            this.gridDataEditRadButton.Name = "gridDataEditRadButton";
            this.gridDataEditRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataEditRadButton.TabIndex = 8;
            this.gridDataEditRadButton.Text = "Edit Data";
            this.gridDataEditRadButton.ThemeName = "Office2007Black";
            this.gridDataEditRadButton.Click += new System.EventHandler(this.gridDataEditRadButton_Click);
            // 
            // bhm_DataReadingsRadGridView
            // 
            this.bhm_DataReadingsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bhm_DataReadingsRadGridView.BackColor = System.Drawing.Color.DarkSlateGray;
            this.bhm_DataReadingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bhm_DataReadingsRadGridView.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bhm_DataReadingsRadGridView.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.bhm_DataReadingsRadGridView.MasterTemplate.AllowAddNewRow = false;
            this.bhm_DataReadingsRadGridView.MasterTemplate.MultiSelect = true;
            this.bhm_DataReadingsRadGridView.Name = "bhm_DataReadingsRadGridView";
            this.bhm_DataReadingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.bhm_DataReadingsRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.bhm_DataReadingsRadGridView.Size = new System.Drawing.Size(995, 657);
            this.bhm_DataReadingsRadGridView.TabIndex = 0;
            this.bhm_DataReadingsRadGridView.Text = "radGridView1";
            this.bhm_DataReadingsRadGridView.ThemeName = "Office2007Black";
            // 
            // diagnosticsRadPageViewPage
            // 
            this.diagnosticsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.diagnosticsRadPageViewPage.Controls.Add(this.groupTwoDiagnosticsRadGroupBox);
            this.diagnosticsRadPageViewPage.Controls.Add(this.groupOneDiagnosticsRadGroupBox);
            this.diagnosticsRadPageViewPage.Enabled = false;
            this.diagnosticsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.diagnosticsRadPageViewPage.Name = "diagnosticsRadPageViewPage";
            this.diagnosticsRadPageViewPage.Size = new System.Drawing.Size(995, 690);
            this.diagnosticsRadPageViewPage.Text = "Diagnostics";
            // 
            // groupTwoDiagnosticsRadGroupBox
            // 
            this.groupTwoDiagnosticsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupTwoDiagnosticsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.groupTwoDiagnosticsRadGroupBox.Controls.Add(this.healthIndexGroupTwoValueRadLabel);
            this.groupTwoDiagnosticsRadGroupBox.Controls.Add(this.healthIndexGroupTwoTextRadLabel);
            this.groupTwoDiagnosticsRadGroupBox.Controls.Add(this.diagnosticGroupTwoValueRadLabel);
            this.groupTwoDiagnosticsRadGroupBox.Controls.Add(this.diagnosticGroupTwoTextRadLabel);
            this.groupTwoDiagnosticsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTwoDiagnosticsRadGroupBox.FooterImageIndex = -1;
            this.groupTwoDiagnosticsRadGroupBox.FooterImageKey = "";
            this.groupTwoDiagnosticsRadGroupBox.HeaderImageIndex = -1;
            this.groupTwoDiagnosticsRadGroupBox.HeaderImageKey = "";
            this.groupTwoDiagnosticsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.groupTwoDiagnosticsRadGroupBox.HeaderText = "Group 2 Diagnostics";
            this.groupTwoDiagnosticsRadGroupBox.Location = new System.Drawing.Point(574, 126);
            this.groupTwoDiagnosticsRadGroupBox.Name = "groupTwoDiagnosticsRadGroupBox";
            this.groupTwoDiagnosticsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.groupTwoDiagnosticsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.groupTwoDiagnosticsRadGroupBox.Size = new System.Drawing.Size(332, 164);
            this.groupTwoDiagnosticsRadGroupBox.TabIndex = 42;
            this.groupTwoDiagnosticsRadGroupBox.Text = "Group 2 Diagnostics";
            this.groupTwoDiagnosticsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // healthIndexGroupTwoValueRadLabel
            // 
            this.healthIndexGroupTwoValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.healthIndexGroupTwoValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.healthIndexGroupTwoValueRadLabel.Location = new System.Drawing.Point(93, 79);
            this.healthIndexGroupTwoValueRadLabel.Name = "healthIndexGroupTwoValueRadLabel";
            this.healthIndexGroupTwoValueRadLabel.Size = new System.Drawing.Size(94, 16);
            this.healthIndexGroupTwoValueRadLabel.TabIndex = 38;
            this.healthIndexGroupTwoValueRadLabel.Text = "No data available";
            // 
            // healthIndexGroupTwoTextRadLabel
            // 
            this.healthIndexGroupTwoTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.healthIndexGroupTwoTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.healthIndexGroupTwoTextRadLabel.Location = new System.Drawing.Point(13, 79);
            this.healthIndexGroupTwoTextRadLabel.Name = "healthIndexGroupTwoTextRadLabel";
            this.healthIndexGroupTwoTextRadLabel.Size = new System.Drawing.Size(66, 40);
            this.healthIndexGroupTwoTextRadLabel.TabIndex = 37;
            this.healthIndexGroupTwoTextRadLabel.Text = "<html>Health index<br>10 = good<br>0 = poor</html>";
            // 
            // diagnosticGroupTwoValueRadLabel
            // 
            this.diagnosticGroupTwoValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diagnosticGroupTwoValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.diagnosticGroupTwoValueRadLabel.Location = new System.Drawing.Point(93, 34);
            this.diagnosticGroupTwoValueRadLabel.Name = "diagnosticGroupTwoValueRadLabel";
            this.diagnosticGroupTwoValueRadLabel.Size = new System.Drawing.Size(94, 16);
            this.diagnosticGroupTwoValueRadLabel.TabIndex = 36;
            this.diagnosticGroupTwoValueRadLabel.Text = "No data available";
            // 
            // diagnosticGroupTwoTextRadLabel
            // 
            this.diagnosticGroupTwoTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diagnosticGroupTwoTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.diagnosticGroupTwoTextRadLabel.Location = new System.Drawing.Point(13, 34);
            this.diagnosticGroupTwoTextRadLabel.Name = "diagnosticGroupTwoTextRadLabel";
            this.diagnosticGroupTwoTextRadLabel.Size = new System.Drawing.Size(59, 16);
            this.diagnosticGroupTwoTextRadLabel.TabIndex = 35;
            this.diagnosticGroupTwoTextRadLabel.Text = "Diagnostic";
            // 
            // groupOneDiagnosticsRadGroupBox
            // 
            this.groupOneDiagnosticsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupOneDiagnosticsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.groupOneDiagnosticsRadGroupBox.Controls.Add(this.healthIndexGroupOneValueRadLabel);
            this.groupOneDiagnosticsRadGroupBox.Controls.Add(this.healthIndexGroupOneTextRadLabel);
            this.groupOneDiagnosticsRadGroupBox.Controls.Add(this.diagnosticGroupOneValueRadLabel);
            this.groupOneDiagnosticsRadGroupBox.Controls.Add(this.diagnosticGroupOneTextRadLabel);
            this.groupOneDiagnosticsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupOneDiagnosticsRadGroupBox.FooterImageIndex = -1;
            this.groupOneDiagnosticsRadGroupBox.FooterImageKey = "";
            this.groupOneDiagnosticsRadGroupBox.HeaderImageIndex = -1;
            this.groupOneDiagnosticsRadGroupBox.HeaderImageKey = "";
            this.groupOneDiagnosticsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.groupOneDiagnosticsRadGroupBox.HeaderText = "Group 1 Diagnostics";
            this.groupOneDiagnosticsRadGroupBox.Location = new System.Drawing.Point(85, 126);
            this.groupOneDiagnosticsRadGroupBox.Name = "groupOneDiagnosticsRadGroupBox";
            this.groupOneDiagnosticsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.groupOneDiagnosticsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.groupOneDiagnosticsRadGroupBox.Size = new System.Drawing.Size(332, 164);
            this.groupOneDiagnosticsRadGroupBox.TabIndex = 41;
            this.groupOneDiagnosticsRadGroupBox.Text = "Group 1 Diagnostics";
            this.groupOneDiagnosticsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // healthIndexGroupOneValueRadLabel
            // 
            this.healthIndexGroupOneValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.healthIndexGroupOneValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.healthIndexGroupOneValueRadLabel.Location = new System.Drawing.Point(93, 79);
            this.healthIndexGroupOneValueRadLabel.Name = "healthIndexGroupOneValueRadLabel";
            this.healthIndexGroupOneValueRadLabel.Size = new System.Drawing.Size(94, 16);
            this.healthIndexGroupOneValueRadLabel.TabIndex = 38;
            this.healthIndexGroupOneValueRadLabel.Text = "No data available";
            // 
            // healthIndexGroupOneTextRadLabel
            // 
            this.healthIndexGroupOneTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.healthIndexGroupOneTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.healthIndexGroupOneTextRadLabel.Location = new System.Drawing.Point(13, 79);
            this.healthIndexGroupOneTextRadLabel.Name = "healthIndexGroupOneTextRadLabel";
            this.healthIndexGroupOneTextRadLabel.Size = new System.Drawing.Size(66, 40);
            this.healthIndexGroupOneTextRadLabel.TabIndex = 37;
            this.healthIndexGroupOneTextRadLabel.Text = "<html>Health index<br>10 = good<br>0 = poor</html>";
            // 
            // diagnosticGroupOneValueRadLabel
            // 
            this.diagnosticGroupOneValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diagnosticGroupOneValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.diagnosticGroupOneValueRadLabel.Location = new System.Drawing.Point(93, 34);
            this.diagnosticGroupOneValueRadLabel.Name = "diagnosticGroupOneValueRadLabel";
            this.diagnosticGroupOneValueRadLabel.Size = new System.Drawing.Size(94, 16);
            this.diagnosticGroupOneValueRadLabel.TabIndex = 36;
            this.diagnosticGroupOneValueRadLabel.Text = "No data available";
            // 
            // diagnosticGroupOneTextRadLabel
            // 
            this.diagnosticGroupOneTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diagnosticGroupOneTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.diagnosticGroupOneTextRadLabel.Location = new System.Drawing.Point(13, 34);
            this.diagnosticGroupOneTextRadLabel.Name = "diagnosticGroupOneTextRadLabel";
            this.diagnosticGroupOneTextRadLabel.Size = new System.Drawing.Size(59, 16);
            this.diagnosticGroupOneTextRadLabel.TabIndex = 35;
            this.diagnosticGroupOneTextRadLabel.Text = "Diagnostic";
            // 
            // BHM_DataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 736);
            this.Controls.Add(this.dataDisplayRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1024, 766);
            this.Name = "BHM_DataViewer";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bushing Health Monitor Data Viewer";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BHM_DataViewer_FormClosing);
            this.Load += new System.EventHandler(this.BHM_DataViewer_Load);
            this.SizeChanged += new System.EventHandler(this.BHM_DataViewer_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataDisplayRadPageView)).EndInit();
            this.dataDisplayRadPageView.ResumeLayout(false);
            this.trendRadPageViewPage.ResumeLayout(false);
            this.trendRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.createReportRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createCorrelationGraphRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendPhasesSwappedRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGroupBox)).EndInit();
            this.dynamicsRadGroupBox.ResumeLayout(false);
            this.dynamicsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.voltage3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moistureContentRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp4RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDataGroupSelectionRadGroupBox)).EndInit();
            this.trendDataGroupSelectionRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendAllGroupsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGroup2RadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGroup1RadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawRadGroupBox)).EndInit();
            this.trendDrawRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendAutoDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendManualDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawGraphsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDisplayOptionsRadGroupBox)).EndInit();
            this.trendDisplayOptionsRadGroupBox.ResumeLayout(false);
            this.trendDisplayOptionsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialTrendRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearTrendRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendLineRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDataAgeRadGroupBox)).EndInit();
            this.trendDataAgeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendAllDataRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendOneYearRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendSixMonthsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendThreeMonthsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentCRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentBRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentARadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceCRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceBRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceARadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseACRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseABRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentPhaseAARadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentCRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentBRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bushingCurrentARadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseTemperatureCoefficientRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaTrendRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaPhaseRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).EndInit();
            this.trendZoomButtonsRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendRadHScrollBar)).EndInit();
            this.polarRadPageViewPage.ResumeLayout(false);
            this.polarRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polarPhasesSwappedRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showRadialLabelsRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseSegmentPolarRadGroupBox)).EndInit();
            this.phaseSegmentPolarRadGroupBox.ResumeLayout(false);
            this.phaseSegmentPolarRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceAPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentCPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceBPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentBPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacitanceCPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentAPolarRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataDisplayChoiceRadGroupBox)).EndInit();
            this.polarDataDisplayChoiceRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientPolarRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gammaPolarRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarRadVScrollBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarRadHScrollBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarWinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarZoomButtonsRadPanel)).EndInit();
            this.polarZoomButtonsRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.polarDrawRadGroupBox)).EndInit();
            this.polarDrawRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.polarManualDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarAutoDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDrawGraphsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataGroupSelectionRadGroupBox)).EndInit();
            this.polarDataGroupSelectionRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.polarAllGroupsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarGroup2RadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarGroup1RadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarDataAgeRadGroupBox)).EndInit();
            this.polarDataAgeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.polarOneYearRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarAllDataRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarSixMonthsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polarThreeMonthsRadRadioButton)).EndInit();
            this.initialBalanceDataRadPageViewPage.ResumeLayout(false);
            this.initialBalanceDataRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabTemperatureRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalanceRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalancePhaseRadLabel)).EndInit();
            this.gridRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saveDataToCsvRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridHideAllRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridExpandAllRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDeleteSelectedRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataCancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataEditRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhm_DataReadingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bhm_DataReadingsRadGridView)).EndInit();
            this.diagnosticsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupTwoDiagnosticsRadGroupBox)).EndInit();
            this.groupTwoDiagnosticsRadGroupBox.ResumeLayout(false);
            this.groupTwoDiagnosticsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupTwoValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupTwoTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupTwoValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupTwoTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupOneDiagnosticsRadGroupBox)).EndInit();
            this.groupOneDiagnosticsRadGroupBox.ResumeLayout(false);
            this.groupOneDiagnosticsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupOneValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthIndexGroupOneTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupOneValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagnosticGroupOneTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView dataDisplayRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage trendRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage polarRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage gridRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox trendDrawRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton trendAutoDrawRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendManualDrawRadRadioButton;
        private Telerik.WinControls.UI.RadButton trendDrawGraphsRadButton;
        private Telerik.WinControls.UI.RadGroupBox trendDisplayOptionsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel dataItemsRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor movingAverageRadSpinEditor;
        private Telerik.WinControls.UI.RadCheckBox movingAverageRadCheckBox;
        private Telerik.WinControls.UI.RadRadioButton exponentialTrendRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton linearTrendRadRadioButton;
        private Telerik.WinControls.UI.RadCheckBox trendLineRadCheckBox;
        private Telerik.WinControls.UI.RadGroupBox trendDataGroupSelectionRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton trendAllGroupsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendGroup2RadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendGroup1RadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox trendDataAgeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton trendAllDataRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendOneYearRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendSixMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendThreeMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadCheckBox frequencyRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentCRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentBRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentARadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceCRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceBRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceARadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentPhaseACRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentPhaseABRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentPhaseAARadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentCRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentBRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox bushingCurrentARadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox phaseTemperatureCoefficientRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temperatureCoefficientRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox gammaTrendRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox gammaPhaseRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox gammaRadCheckBox;
        private Telerik.WinControls.UI.RadPanel trendZoomButtonsRadPanel;
        private System.Windows.Forms.RadioButton trendPointerRadioButton;
        private System.Windows.Forms.RadioButton trendZoomInRadioButton;
        private System.Windows.Forms.RadioButton trendZoomOutRadioButton;
        private Telerik.WinControls.UI.RadHScrollBar trendRadHScrollBar;
        private Telerik.WinControls.UI.RadGroupBox phaseSegmentPolarRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceAPolarRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentCPolarRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceBPolarRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentBPolarRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox capacitanceCPolarRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox tangentAPolarRadCheckBox;
        private Telerik.WinControls.UI.RadGroupBox polarDataDisplayChoiceRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton temperatureCoefficientPolarRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton gammaPolarRadRadioButton;
        private Telerik.WinControls.UI.RadVScrollBar polarRadVScrollBar;
        private Telerik.WinControls.UI.RadHScrollBar polarRadHScrollBar;
        private Telerik.WinControls.UI.RadPanel polarZoomButtonsRadPanel;
        private System.Windows.Forms.RadioButton polarPointerRadioButton;
        private System.Windows.Forms.RadioButton polarZoomInRadioButton;
        private System.Windows.Forms.RadioButton polarZoomOutRadioButton;
        private Telerik.WinControls.UI.RadGroupBox polarDrawRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton polarManualDrawRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarAutoDrawRadRadioButton;
        private Telerik.WinControls.UI.RadButton polarDrawGraphsRadButton;
        private Telerik.WinControls.UI.RadGroupBox polarDataGroupSelectionRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton polarAllGroupsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarGroup2RadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarGroup1RadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox polarDataAgeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton polarOneYearRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarAllDataRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarSixMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton polarThreeMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadButton gridDataCancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton gridDataSaveChangesRadButton;
        private Telerik.WinControls.UI.RadButton gridDataEditRadButton;
        private Telerik.WinControls.UI.RadGridView bhm_DataReadingsRadGridView;
        private System.Windows.Forms.SaveFileDialog trendSaveFileDialog;
        private System.Windows.Forms.SaveFileDialog polarSaveFileDialog;
        private Telerik.WinControls.UI.RadCheckBox normalizeDataRadCheckBox;
        private ChartDirector.WinChartViewer trendWinChartViewer;
        private System.Windows.Forms.Label trendCursorLabel;
        private System.Windows.Forms.Label polarVcursorGroup2Label;
        private System.Windows.Forms.Label polarHcursorGroup2Label;
        private System.Windows.Forms.Label polarHcursorGroup1Label;
        private System.Windows.Forms.Label polarVcursorGroup1Label;
        private ChartDirector.WinChartViewer polarWinChartViewer;
        private Telerik.WinControls.UI.RadButton gridDataDeleteSelectedRadButton;
        private Telerik.WinControls.UI.RadButton gridHideAllRadButton;
        private Telerik.WinControls.UI.RadButton gridExpandAllRadButton;
        private Telerik.WinControls.UI.RadContextMenu trendRadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu polarRadContextMenu;
        private Telerik.WinControls.UI.RadGroupBox dynamicsRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox temp4RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox humidityRadCheckBox;
        private Telerik.WinControls.UI.RadContextMenu dynamicsRadGroupBoxRadContextMenuStrip;
        private Telerik.WinControls.UI.RadCheckBox showRadialLabelsRadCheckBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent1RadCheckBox;
        private Telerik.WinControls.UI.RadButton saveDataToCsvRadButton;
        private Telerik.WinControls.UI.RadPageViewPage diagnosticsRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox groupTwoDiagnosticsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel healthIndexGroupTwoValueRadLabel;
        private Telerik.WinControls.UI.RadLabel healthIndexGroupTwoTextRadLabel;
        private Telerik.WinControls.UI.RadLabel diagnosticGroupTwoValueRadLabel;
        private Telerik.WinControls.UI.RadLabel diagnosticGroupTwoTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox groupOneDiagnosticsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel healthIndexGroupOneValueRadLabel;
        private Telerik.WinControls.UI.RadLabel healthIndexGroupOneTextRadLabel;
        private Telerik.WinControls.UI.RadLabel diagnosticGroupOneValueRadLabel;
        private Telerik.WinControls.UI.RadLabel diagnosticGroupOneTextRadLabel;
        private Telerik.WinControls.UI.RadCheckBox moistureContentRadCheckBox;
        private Telerik.WinControls.UI.RadLabel trendPhasesSwappedRadLabel;
        private Telerik.WinControls.UI.RadLabel polarPhasesSwappedRadLabel;
        private Telerik.WinControls.UI.RadCheckBox voltage3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox voltage2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox voltage1RadCheckBox;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadButton createCorrelationGraphRadButton;
        private Telerik.WinControls.UI.RadButton createReportRadButton;
        private Telerik.WinControls.UI.RadPageViewPage initialBalanceDataRadPageViewPage;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabMeasurementDateValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabMeasurementDateRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBushingDataSet2InputSignalShiftPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2TemperatureValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ImbalanceValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1TemperatureValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ImbalanceValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1RadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2RadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabTemperatureRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabImbalanceRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabImbalancePhaseRadLabel;

    }
}

