﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BHMonitorUtilities
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {


        //        private void InitializeCalibrationCoefficientsTabObjects()
        //        {
        //            try
        //            {
        //                if (currentConfiguration != null)
        //                {
        //                    if (currentConfiguration.calibrationCoeff != null)
        //                    {
        //                        double temperature1Mulitplier = workingConfiguration.calibrationCoeff.TemperatureK_0;
        //                        double temperature1Offset = workingConfiguration.calibrationCoeff.TemperatureB_0;
        //                        double temperature2Mulitplier = workingConfiguration.calibrationCoeff.TemperatureK_1;
        //                        double temperature2Offset = workingConfiguration.calibrationCoeff.TemperatureB_1;
        //                        double temperature3Mulitplier = workingConfiguration.calibrationCoeff.TemperatureK_2;
        //                        double temperature3Offset = workingConfiguration.calibrationCoeff.TemperatureB_2;
        //                        double temperature4Mulitplier = workingConfiguration.calibrationCoeff.TemperatureK_3;
        //                        double temperature4Offset = workingConfiguration.calibrationCoeff.TemperatureB_3;

        //                        double currentSensor1Multiplier = workingConfiguration.calibrationCoeff.CurrentK_0;
        //                        double currentSensor1Offset = workingConfiguration.calibrationCoeff.CurrentB_0;
        //                        int currentSensor1Connection = 0;
        //                        double currentSensor2Multiplier = workingConfiguration.calibrationCoeff.CurrentK_1;
        //                        double currentSensor2Offset = workingConfiguration.calibrationCoeff.CurrentB_1;
        //                        int currentSensor2Connection = 0;
        //                        double currentSensor3Multiplier = workingConfiguration.calibrationCoeff.CurrentK_2;
        //                        double currentSensor3Offset = workingConfiguration.calibrationCoeff.CurrentB_2;
        //                        int currentSensor3Connection = 0;

        //                        double humidityMulitplier = workingConfiguration.calibrationCoeff.HumiditySlope_0;
        //                        double humidityOffset = workingConfiguration.calibrationCoeff.HumidityOffset_0;

        //                        double ltcPositionMultiplier = 0.0;
        //                        double ltcPositionOffset = 0.0;

        //                        int dataFromScada = 0;

        //                        temperature1MultiplierRadMaskedEditBox.Text = Math.Round(temperature1Mulitplier, 3).ToString();
        //                        temperature1OffsetRadTextBox.Text = Math.Round(temperature1Offset, 3).ToString();
        //                        temperature2MultiplierRadMaskedEditBox.Text = Math.Round(temperature2Mulitplier, 3).ToString();
        //                        temperature2OffsetRadTextBox.Text = Math.Round(temperature2Offset, 3).ToString();
        //                        temperature3MultiplierRadMaskedEditBox.Text = Math.Round(temperature3Mulitplier, 3).ToString();
        //                        temperature3OffsetRadTextBox.Text = Math.Round(temperature3Offset, 3).ToString();
        //                        temperature4MultiplierRadMaskedEditBox.Text = Math.Round(temperature4Mulitplier, 3).ToString();
        //                        temperature4OffsetRadTextBox.Text = Math.Round(temperature4Offset, 3).ToString();

        //                        currentSensor1MultiplierRadMaskedEditBox.Text = Math.Round(currentSensor1Multiplier, 3).ToString();
        //                        currentSensor1OffsetRadTextBox.Text = Math.Round(currentSensor1Offset, 3).ToString();
        //                        currentSensor1ConnectionRadDropDownList.SelectedIndex = currentSensor1Connection;
        //                        currentSensor2MultiplierRadMaskedEditBox.Text = Math.Round(currentSensor2Multiplier, 3).ToString();
        //                        currentSensor2OffsetRadTextBox.Text = Math.Round(currentSensor2Offset, 3).ToString();
        //                        currentSensor2ConnectionRadDropDownList.SelectedIndex = currentSensor2Connection;
        //                        currentSensor3MultiplierRadMaskedEditBox.Text = Math.Round(currentSensor3Multiplier, 3).ToString();
        //                        currentSensor3OffsetRadTextBox.Text = Math.Round(currentSensor3Offset, 3).ToString();
        //                        currentSensor3ConnectionRadDropDownList.SelectedIndex = currentSensor3Connection;

        //                        humidityMultiplierRadMaskedEditBox.Text = Math.Round(humidityMulitplier, 3).ToString();
        //                        humidityOffsetRadTextBox.Text = Math.Round(humidityOffset, 3).ToString();

        //                        ltcPositionMultiplierRadMaskedEditBox.Text = Math.Round(ltcPositionMultiplier, 3).ToString();
        //                        ltcPositionOffsetRadTextBox.Text = Math.Round(ltcPositionOffset, 3).ToString();

        //                        if (dataFromScada == 0)
        //                        {
        //                            dataFromScadaRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
        //                        }
        //                        else
        //                        {
        //                            dataFromScadaRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in BHM_MonitorConfiguration.InitializeCalibrationCoefficientsTabObjects()\nPrivate data this.workingConfiguration.calibrationCoeff was null.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in BHM_MonitorConfiguration.InitializeCalibrationCoefficientsTabObjects()\nPrivate data this.workingConfiguration was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeCalibrationCoefficientsTabObjects()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void TranslateCalibrationCoefficientsTabObjectsToConfigurationData()
        //        {
        //            try
        //            {
        //                if (currentConfiguration != null)
        //                {
        //                    if (currentConfiguration.calibrationCoeff != null)
        //                    {
        //                        double temperature1Mulitplier = 0.0;
        //                        double temperature1Offset = 0.0;
        //                        double temperature2Mulitplier = 0.0;
        //                        double temperature2Offset = 0.0;
        //                        double temperature3Mulitplier = 0.0;
        //                        double temperature3Offset = 0.0;
        //                        double temperature4Mulitplier = 0.0;
        //                        double temperature4Offset = 0.0;

        //                        double currentSensor1Multiplier = 0.0;
        //                        double currentSensor1Offset = 0.0;
        //                        int currentSensor1Connection = 0;
        //                        double currentSensor2Multiplier = 0.0;
        //                        double currentSensor2Offset = 0.0;
        //                        int currentSensor2Connection = 0;
        //                        double currentSensor3Multiplier = 0.0;
        //                        double currentSensor3Offset = 0.0;
        //                        int currentSensor3Connection = 0;

        //                        double humidityMulitplier = 0.0;
        //                        double humidityOffset = 0.0;

        //                        double ltcPositionMultiplier = 0.0;
        //                        double ltcPositionOffset = 0.0;

        //                        int dataFromScada = 0;

        //                        Double.TryParse(temperature1MultiplierRadMaskedEditBox.Text, out temperature1Mulitplier);
        //                        Double.TryParse(temperature1OffsetRadTextBox.Text, out temperature1Offset);
        //                        Double.TryParse(temperature2MultiplierRadMaskedEditBox.Text, out temperature2Mulitplier);
        //                        Double.TryParse(temperature2OffsetRadTextBox.Text, out temperature2Offset);
        //                        Double.TryParse(temperature3MultiplierRadMaskedEditBox.Text, out temperature3Mulitplier);
        //                        Double.TryParse(temperature3OffsetRadTextBox.Text, out temperature3Offset);
        //                        Double.TryParse(temperature4MultiplierRadMaskedEditBox.Text, out temperature4Mulitplier);
        //                        Double.TryParse(temperature4OffsetRadTextBox.Text, out temperature4Offset);

        //                        Double.TryParse(currentSensor1MultiplierRadMaskedEditBox.Text, out currentSensor1Multiplier);
        //                        Double.TryParse(currentSensor1OffsetRadTextBox.Text, out currentSensor1Offset);
        //                        currentSensor1Connection = currentSensor1ConnectionRadDropDownList.SelectedIndex;
        //                        Double.TryParse(currentSensor2MultiplierRadMaskedEditBox.Text, out currentSensor2Multiplier);
        //                        Double.TryParse(currentSensor2OffsetRadTextBox.Text, out currentSensor2Offset);
        //                        currentSensor2Connection = currentSensor2ConnectionRadDropDownList.SelectedIndex;
        //                        Double.TryParse(currentSensor3MultiplierRadMaskedEditBox.Text, out currentSensor3Multiplier);
        //                        Double.TryParse(currentSensor3OffsetRadTextBox.Text, out currentSensor3Offset);
        //                        currentSensor3Connection = currentSensor3ConnectionRadDropDownList.SelectedIndex;

        //                        Double.TryParse(humidityMultiplierRadMaskedEditBox.Text, out humidityMulitplier);
        //                        Double.TryParse(humidityOffsetRadTextBox.Text, out humidityOffset);

        //                        Double.TryParse(ltcPositionMultiplierRadMaskedEditBox.Text, out ltcPositionMultiplier);
        //                        Double.TryParse(ltcPositionOffsetRadTextBox.Text, out ltcPositionOffset);

        //                        if (dataFromScada == 0)
        //                        {
        //                            dataFromScadaRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
        //                        }
        //                        else
        //                        {
        //                            dataFromScadaRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }

        //                        workingConfiguration.calibrationCoeff.TemperatureK_0 = temperature1Mulitplier;
        //                        workingConfiguration.calibrationCoeff.TemperatureB_0 = temperature1Offset;
        //                        workingConfiguration.calibrationCoeff.TemperatureK_1 = temperature2Mulitplier;
        //                        workingConfiguration.calibrationCoeff.TemperatureB_1 = temperature2Offset;
        //                        workingConfiguration.calibrationCoeff.TemperatureK_2 = temperature3Mulitplier;
        //                        workingConfiguration.calibrationCoeff.TemperatureB_2 = temperature3Offset;
        //                        workingConfiguration.calibrationCoeff.TemperatureK_3 = temperature4Mulitplier;
        //                        workingConfiguration.calibrationCoeff.TemperatureB_3 = temperature4Offset;

        //                        workingConfiguration.calibrationCoeff.CurrentK_0 = currentSensor1Multiplier;
        //                        workingConfiguration.calibrationCoeff.CurrentB_0 = currentSensor1Offset;
        //                        workingConfiguration.calibrationCoeff.CurrentK_1 = currentSensor2Multiplier;
        //                        workingConfiguration.calibrationCoeff.CurrentB_1 = currentSensor2Offset;
        //                        workingConfiguration.calibrationCoeff.CurrentK_2 = currentSensor3Multiplier;
        //                        workingConfiguration.calibrationCoeff.CurrentB_2 = currentSensor3Offset;

        //                        workingConfiguration.calibrationCoeff.HumiditySlope_0 = humidityMulitplier;
        //                        workingConfiguration.calibrationCoeff.HumidityOffset_0 = humidityOffset;
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in BHM_MonitorConfiguration.TranslateCalibrationCoefficientsTabObjectsToConfigurationData()\nPrivate data this.workingConfiguration.calibrationCoeff was null.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in BHM_MonitorConfiguration.TranslateCalibrationCoefficientsTabObjectsToConfigurationData()\nPrivate data this.workingConfiguration was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.TranslateCalibrationCoefficientsTabObjectsToConfigurationData()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }



        //        private void availableConfigurationsCalibrationCoefficientsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        //        {
        //            try
        //            {
        //                int selectedIndex = availableConfigurationsCalibrationCoefficientsTabRadListControl.SelectedIndex;
        //                if (selectedIndex != -1)
        //                {
        //                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
        //                    {
        //                        this.availableConfigurationsSelectedIndex = selectedIndex;
        //                        SetAvailableConfigurationsSelectedIndex();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.availableConfigurationsCalibrationCoefficientsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }
    }
}
