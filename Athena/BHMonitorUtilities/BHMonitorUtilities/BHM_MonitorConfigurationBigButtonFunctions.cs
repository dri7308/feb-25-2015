﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;
using DatabaseInterface;
using FormatConversion;
using MonitorCommunication;
using MonitorUtilities;

namespace BHMonitorUtilities
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
       // private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
        private static string noCurrentConfigurationDefinedText = "There is no current configuration defined.";
       // private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
       // private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
       // private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        private static string deleteAsQuestionText = "Delete?";
        private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
      //  private static string cannotDeleteTemplateConfigurationWarningText = "You are not allowed to delete a template configuration from the database.";      
        private static string noConfigurationSelectedText = "No configuration selected.";
      //  private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
       // private static string commandToReadDeviceErrorStateFailedText = "Command to read the device errror state failed.";
      //  private static string lostDeviceConnectionText = "Lost the connection to the device.";
      //  private static string notConnectedToBHMWarningText = "You are not connected to a BHM.  Cannot configure using this interface.";
        private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
      //  private static string failedToWriteTheConfigurationToTheDevice = "Failed to write the configuration to the device.";
     //   private static string configurationWasWrittenToTheDevice = "The configuration was written to the device.";
      //  private static string configurationWasReadFromTheDevice = "The configuration was read from the device.";
        private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        private static string saveDeviceConfigurationQuestionText = "Save device configuration?";

     //   private static string workingConfigAlreadyPresentInDatabaseText = "There is already a working configuration saved in the database.  Overwrite it?";
      //  private static string failedToDeleteWorkingConfigurationText = "Failed to delete the working configuration from the database";
        private static string workingConfigurationSavedToDatabaseText = "The working configuration was saved to the database";
        private static string workingConfigurationNotSavedToDatabaseText = "Error: the working configuration was not saved to the database";
         private static string configurationWasReadFromTheDatabaseText = "The configuration was successfully read from the database";

        private static string changesToWorkingConfigurationNotSavedOverwriteWarningText = "You have made changes to the configuration that have not been saved.  Discard those changes?";
        private static string changesToWorkingConfigurationNotSavedExitWarningText = "You have made changes to the configuration that have not been saved.  Exit anyway?";
        private static string discardChangesAsQuestionText = "Discard changes?";

        private static string configurationLoadCancelledText = "Configuration load cancelled";

        private static string failedToDeleteCurrentConfigurationText = "Failed to delete the current configuration from the database";
        private static string failedtoDeleteConfigurationText = "Failed to delete the configuration";

        private static string replaceExistingConfigurationsWithTheSameNameQuestionText = "Would you like to replace all existing configurations having the same description with this one?";

        private void SaveWorkingConfigurationToDatabase(MonitorInterfaceDB localDB)
        {
            try
            {
                string defaultConfigurationName = string.Empty;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.workingConfiguration != null)
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        if (BHM_MonitorConfiguration.monitor != null)
                        {
                            if (BHM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                            {
                                defaultConfigurationName = this.nameOfLastConfigurationLoadedFromDatabase;
                            }
                            using (ConfigurationDescription description = new ConfigurationDescription(BHM_MonitorConfiguration.CurrentConfigName, defaultConfigurationName))
                            {
                                description.ShowDialog();
                                description.Hide();

                                if (description.SaveConfiguration)
                                {
                                    if ((BHM_MonitorConfiguration.programType == ProgramType.TemplateEditor) && (NamedConfigurationAlreadySavedInDatabase(BHM_MonitorConfiguration.monitor, description.ConfigurationName, localDB)))
                                    {
                                        if (RadMessageBox.Show(this, replaceExistingConfigurationsWithTheSameNameQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            if (!DeleteNamedConfigurationFromDatabase(BHM_MonitorConfiguration.monitor, description.ConfigurationName, localDB))
                                            {
                                                RadMessageBox.Show(this, failedtoDeleteConfigurationText);
                                            }
                                        }
                                    }
                                    //WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                                    if (ConfigurationConversion.SaveBHM_ConfigurationToDatabase(this.workingConfiguration, BHM_MonitorConfiguration.monitor.ID, DateTime.Now, description.ConfigurationName, localDB))
                                    {
                                        // RadMessageBox.Show(this, workingConfigurationSavedToDatabaseText);
                                        this.uneditedWorkingConfiguration = BHM_Configuration.CopyConfiguration(this.workingConfiguration);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, workingConfigurationNotSavedToDatabaseText);
                                    }
                                    LoadAvailableConfigurations(localDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nBHM_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
                // }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool SafeToLoadConfiguration()
        {
            bool safeToLoad = true;
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();
                if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                {
                    if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedOverwriteWarningText, discardChangesAsQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        safeToLoad = false;
                    }
                }             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.SafeToLoadConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return safeToLoad;
        }

        private static bool NamedConfigurationAlreadySavedInDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool configurationIsPresent = false;
            try
            {
                List<BHM_Config_ConfigurationRoot> configRootList = null;
                if (BHM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        // this list will be, at most, two entries long
                        foreach (BHM_Config_ConfigurationRoot entry in configRootList)
                        {
                            if (entry.Description.Contains(configurationName))
                            {
                                configurationIsPresent = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.NamedConfigurationAlreadySavedInDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationIsPresent;
        }

        private static bool DeleteNamedConfigurationFromDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                List<BHM_Config_ConfigurationRoot> configRootList = null;
                if (BHM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        for (int i = 0; i < configRootList.Count; i++)
                        {
                            if (configRootList[i].Description.Contains(configurationName))
                            {
                                success = BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(configRootList[i].ID, localDB);
                                // break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.DeleteNamedConfigurationFromDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

//        private void SaveDeviceConfigurationToDatabase()
//        {
//            try
//            {
//                if (this.configurationFromDevice != null)
//                {
//                    using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
//                    {
//                        SaveDeviceConfigurationToDatabase(this.configurationFromDevice, true, saveDB);
//                        LoadAvailableConfigurations(saveDB);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationLoadedFromDeviceText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public static bool SaveDeviceConfigurationToDatabase(Monitor monitor, BHM_Configuration configurationBeingSaved, bool interactive, MonitorInterfaceDB localDB, IWin32Window parentWindow)
        {
            bool success = false;
            try
            {
                Guid configToDeleteConfigurationRootID = Guid.Empty;
                bool saveConfiguration = false;
                if (configurationBeingSaved != null)
                {
                    if (interactive)
                    {
                        if (RadMessageBox.Show(parentWindow, configurationBeingSavedIsFromDeviceText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            saveConfiguration = true;
                        }
                    }
                    else
                    {
                        saveConfiguration = true;
                    }
                    if (saveConfiguration)
                    {
                        if (NamedConfigurationAlreadySavedInDatabase(monitor, BHM_MonitorConfiguration.CurrentConfigName, localDB))
                        {
                            if (!DeleteNamedConfigurationFromDatabase(monitor, BHM_MonitorConfiguration.CurrentConfigName, localDB))
                            {
                                if (interactive)
                                {
                                    RadMessageBox.Show(parentWindow, failedToDeleteCurrentConfigurationText);
                                }
                                string errorMessage = "Error in BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to delete the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                            else
                            {
                                string errorMessage = "In BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nDeleted the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
                            }
                        }

                        if (ConfigurationConversion.SaveBHM_ConfigurationToDatabase(configurationBeingSaved, monitor.ID, DateTime.Now, BHM_MonitorConfiguration.currentConfigName, localDB))
                        {
                            success = true;
                            //string errorMessage = "In BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nSaved a new Current Device Configuration to the database.";
                            //LogMessage.LogError(errorMessage);
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationSavedToDatabaseText);
                            }
                        }
                        else
                        {
                            //string errorMessage = "Error in BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to save a new Current Device Configuration to the database.";
                            //LogMessage.LogError(errorMessage);
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationNotSavedToDatabaseText);
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nInput BHM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

//        private void LoadSelectedConfigurationFromDatabase()
//        {
//            try
//            {
//                if (this.availableConfigurationsSelectedIndex > 0)
//                {
//                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
//                    {
//                        Guid configurationRootID = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1].ID;
//                        LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(configurationRootID);
//                        //EnableFromDatabaseRadRadioButtons();
//                        //SetFromDatabaseRadRadioButtonState();
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationSelectedText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public static BHM_Configuration LoadConfigurationFromDatabase(Guid configurationRootID, string dbConnectionSTring)
        {
            BHM_Configuration configurationFromDB = null;
            try
            {
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionSTring))
                {
                    configurationFromDB = ConfigurationConversion.GetBHM_ConfigurationFromDatabase(configurationRootID, localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationFromDB;
        }

        private void LoadSelectedConfigurationFromDatabase()
        {
            try
            {
                BHM_Configuration configFromDB = null;
                BHM_Config_ConfigurationRoot configurationRoot;

                if (this.availableConfigurationsSelectedIndex > 0)
                {
                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                    {
                        configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                        if (configurationRoot.ID.CompareTo(Guid.Empty) != 0)
                        {
                            if (SafeToLoadConfiguration())
                            {
                                configFromDB = LoadConfigurationFromDatabase(configurationRoot.ID, this.dbConnectionString);
                                if (configFromDB != null)
                                {
                                    if (configFromDB.AllConfigurationMembersAreNonNull())
                                    {
                                        this.workingConfiguration = configFromDB;
                                        this.uneditedWorkingConfiguration = BHM_Configuration.CopyConfiguration(this.workingConfiguration);
                                        AddDataToAllInterfaceObjects(this.workingConfiguration);

                                        if (configurationRoot.Description.Trim().CompareTo(BHM_MonitorConfiguration.currentConfigName) != 0)
                                        {
                                            nameOfLastConfigurationLoadedFromDatabase = configurationRoot.Description;
                                        }

                                        //RadMessageBox.Show(this, configurationWasReadFromTheDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noConfigurationSelectedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        public static BHM_Configuration LoadConfigurationFromDatabase(Guid configurationRootID, string dbConnectionSTring)
//        {
//            BHM_Configuration configurationFromDB = null;
//            try
//            {
//                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionSTring))
//                {
//                    configurationFromDB = ConfigurationConversion.GetBHM_ConfigurationFromDatabase(configurationRootID, localDB);
//                    if (configurationFromDB == null)
//                    {
//                        string errorMessage = "Error in BHM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nFailed to load the configuration from the database";
//                        LogMessage.LogError(errorMessage);
//                    }
//                    else if (!configurationFromDB.AllConfigurationMembersAreNonNull())
//                    {
//                        string errorMessage = "Error in BHM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nFailed to load the configuration from the database";
//                        LogMessage.LogError(errorMessage);
//                        /// I don't know if this is necessary, I think I do all the checks anyway.
//                        configurationFromDB = null;
//                    }
//                    else
//                    {
//                        string errorMessage = "In BHM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nLoaded the configuration from the database.";
//                        LogMessage.LogError(errorMessage);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return configurationFromDB;
//        }

        public static Guid GetConfigurationRootIDForCurrentDeviceConfiguration(Guid monitorID, MonitorInterfaceDB configDB)
        {
            Guid configurationRootID = Guid.Empty;
            try
            {
                List<BHM_Config_ConfigurationRoot> availableConfigurations = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitorID, configDB);
                foreach (BHM_Config_ConfigurationRoot entry in availableConfigurations)
                {
                    if (entry.Description.Trim().CompareTo(BHM_MonitorConfiguration.currentConfigName) == 0)
                    {
                        configurationRootID = entry.ID;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.GetConfigurationRootIDForCurrentDeviceConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootID;
        }

        public static BHM_Configuration LoadCurrentDeviceConfigurationFromDatabase(Guid monitorID, MonitorInterfaceDB configDB)
        {
            BHM_Configuration currentDeviceConfiguration = null;
            try
            {
                Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(monitorID, configDB);
                if (configurationRootID != Guid.Empty)
                {
                    currentDeviceConfiguration = ConfigurationConversion.GetBHM_ConfigurationFromDatabase(configurationRootID, configDB);
                    if (currentDeviceConfiguration != null)
                    {
                        if (!currentDeviceConfiguration.AllConfigurationMembersAreNonNull())
                        {
                            currentDeviceConfiguration = null;
                            string errorMessage = "Error in BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nThe Current Device Configuration was incomplete in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        else
                        {
                           // string errorMessage = "In BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nThe Current Device Configuration was correctly loaded from the database";
                          //  LogMessage.LogError(errorMessage);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nFailed to get the Current Device Configuration from the database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nNo Current Device Configuration has been defined for the device.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return currentDeviceConfiguration;
        }

        private void DeleteConfigurationFromDatabase()
        {
            try
            {
                BHM_Config_ConfigurationRoot configurationRoot;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.availableConfigurationsSelectedIndex > 0)
                    {
                        if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                        {
                            configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                            if (configurationRoot.Description.Trim().CompareTo(BHM_MonitorConfiguration.currentConfigName) != 0)
                            {
                                if (RadMessageBox.Show(this, configurationDeleteFromDatabaseWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                    {
                                        BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, localDB);
                                        LoadAvailableConfigurations(localDB);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, cannotDeleteCurrentConfigurationWarningText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.DeleteConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noConfigurationSelectedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.DeleteConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                long deviceError = 0;
                int deviceType;
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                BHM_Configuration configFromDevice = null;
                if (SafeToLoadConfiguration())
                {
                    if (!downloadWasInProgress)
                    {
                        if (BHM_MonitorConfiguration.monitor != null)
                        {
                            modBusAddressAsString = monitor.ModbusAddress.Trim();
                            if (modBusAddressAsString.Length > 0)
                            {
                                modBusAddress = Int32.Parse(modBusAddressAsString);

                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                {
                                    errorCode = MonitorConnection.OpenMonitorConnection(BHM_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                }
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    DeviceCommunication.EnableDataDownload();
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                    if (deviceType > 0)
                                    {
                                        // deviceType = DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, this.readDelayInMicroseconds);
                                        if (deviceType == 15002)
                                        {
                                            deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceError > -1)
                                            {
                                                Application.DoEvents();
                                                configFromDevice = LoadConfigurationFromDevice(modBusAddress, this.readDelayInMicroseconds);
                                                if (configFromDevice != null)
                                                {
                                                    if (configFromDevice.AllConfigurationMembersAreNonNull())
                                                    {
                                                        errorCode = ErrorCode.ConfigurationDownloadSucceeded;

                                                        this.workingConfiguration = configFromDevice;
                                                        this.uneditedWorkingConfiguration =BHM_Configuration.CopyConfiguration(this.workingConfiguration);

                                                        // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                                        // Testing code

                                                        //this.previousConfigurationAsArray = this.workingConfigurationAsArray;
                                                        //this.workingConfigurationAsArray = registerData;

                                                        //CheckLatestConfigurationAgainstPreviousConfiguration(registerData);

                                                        // more testing code, show some values

                                                        // ShowSomeValuesThatArePissingMeOff(registerData);

                                                        // AddDataToAllTransientInterfaceObjects(configurationFromDevice);
                                                        //EnableCopyDeviceConfigurationRadButtons();
                                                        //EnableFromDeviceRadRadioButtons();
                                                        //SetFromDeviceRadRadioButtonState();

                                                        AddDataToAllInterfaceObjects(this.workingConfiguration);
                                                       
                                                        /// test code
                                                        //Int16[] registerValues = configurationFromDevice.GetShortValuesFromAllContributors();
                                                        //Main_Configuration testConfiguration = new Main_Configuration(registerValues);
                                                        //if (!configurationFromDevice.ConfigurationIsTheSame(testConfiguration))
                                                        //{
                                                        //    MessageBox.Show("configuration mismatch from a copy");
                                                        //}
                                                        //else
                                                        //{
                                                        //    MessageBox.Show("configuration matches from a copy");
                                                        //}
                                                        /// end test code

                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(BHM_MonitorConfiguration.monitor.ID, localDB);
                                                            if (configurationRootID.CompareTo(Guid.Empty) != 0)
                                                            {
                                                                BHM_Configuration deviceConfigFromDb = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);
                                                                if (!deviceConfigFromDb.ConfigurationIsTheSame(this.workingConfiguration))
                                                                {
                                                                    RadMessageBox.Show(this, deviceConfigurationDoesNotMatchDatabaseConfigurationText);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (RadMessageBox.Show(this, deviceCommunicationNotSavedInDatabaseYetText, saveDeviceConfigurationQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                {
                                                                    SaveDeviceConfigurationToDatabase(BHM_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                                    LoadAvailableConfigurations(localDB);
                                                                }
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationDownloadFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToBHM));
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                    }
                                }
                                else
                                {
                                    if (errorCode == ErrorCode.SerialPortNotSpecified)
                                    {
                                        RadMessageBox.Show(this, serialPortNotSetWarningText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                    }
                                }                             
                            }
                            else
                            {
                                RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.LoadConfigurationFromDevice()\nBHM_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, configurationLoadCancelledText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                MonitorConnection.CloseConnection();
            }
        }

        public BHM_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        {
            BHM_Configuration configuration = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                Int16[] registerValues = null;
                int offset = 1;
                int totalNumberOfBytesSavedSoFar = 0;
                int numberOfBytesReturnedByLastFunctionCall = 0;
                int numberOfBytesPerRequest = 200;
                int numberOfInitializationBytes = 300;
                bool receivedAllData = false;
                double firmwareVersion = 0;

                int iterations = 0;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                DisableAllControls();
                SetProgressBarsToDownloadState();
                EnableProgressBars();
                SetProgressBarProgress(0, BHM_Configuration.expectedLengthOfBHMConfigurationArray);


                DeviceCommunication.EnableDataDownload();
                allByteValues = new Byte[2000];
                currentByteValues = new Byte[numberOfInitializationBytes];
               
                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled())
                {
                    currentByteValues = InteractiveDeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, numberOfBytesPerRequest, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    if (currentByteValues != null)
                    {
                        numberOfBytesReturnedByLastFunctionCall = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, totalNumberOfBytesSavedSoFar, numberOfBytesReturnedByLastFunctionCall);
                        totalNumberOfBytesSavedSoFar += numberOfBytesReturnedByLastFunctionCall;
                        offset += numberOfBytesReturnedByLastFunctionCall;
                        //if (numberOfBytesReturnedByLastFunctionCall < numberOfBytesPerRequest)
                        //{
                        //    receivedAllData = true;
                        //}
                        if (totalNumberOfBytesSavedSoFar >= BHM_Configuration.expectedLengthOfBHMConfigurationArray)
                        {
                            receivedAllData = true;
                        }
                    }
                    else
                    {
                        receivedAllData = true;
                        allByteValues = null;
                        break;
                    }

                    iterations++;
                    SetProgressBarProgress(totalNumberOfBytesSavedSoFar, BHM_Configuration.expectedLengthOfBHMConfigurationArray);
                }
                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                if (registerValues != null)
                {
                    firmwareVersion = registerValues[0] / 100.0;
                }
                if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled())
                {
                    byteValuesToCreateConfiguration = new Byte[totalNumberOfBytesSavedSoFar];
                    Array.Copy(allByteValues, byteValuesToCreateConfiguration, totalNumberOfBytesSavedSoFar);
                    configuration = new BHM_Configuration(byteValuesToCreateConfiguration, firmwareVersion);                   
                } 
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.BHM_LoadConfigurationFromDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                MonitorConnection.CloseConnection();
            }
            return configuration;
        }

        private void ProgramDevice()
        {
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                int deviceType;
                int readDelayInMicroseconds = 1000;
                Byte[] byteData = null;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                Monitor monitorFromDB;
                bool deviceWasProgrammedSuccessfully = true;

                int offset = 1;
                Byte[] bytesBeingSentToDevice;
                int numberOfBytesPerFullSend = 200;
                int numberOfBytesBeingSent;
                int totalNumberOfBytesToWrite;
                int numberOfBytesSentSoFar = 0;

                int iteration = 0;

                DisableAllControls();
                SetProgressBarsToUploadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 5);

                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (!downloadWasInProgress)
                    {
                        if (this.workingConfiguration != null)
                        {
                            if (!ErrorIsPresentInSomeTabObject())
                            {
                                WriteAllInterfaceDataToWorkingConfiguration();
                                if (BHM_MonitorConfiguration.monitor != null)
                                {
                                    modBusAddressAsString = monitor.ModbusAddress.Trim();
                                    if (modBusAddressAsString.Length > 0)
                                    {
                                        modBusAddress = Int32.Parse(modBusAddressAsString);

                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                        {
                                            errorCode = MonitorConnection.OpenMonitorConnection(BHM_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                        }
                                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                        {
                                            DeviceCommunication.EnableDataDownload();
                                            deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceType > 0)
                                            {
                                                if (deviceType == 15002)
                                                {
                                                    Application.DoEvents();
                                                    byteData = workingConfiguration.ConvertConfigurationToByteArray();
                                                    totalNumberOfBytesToWrite = byteData.Length;
                                                    bytesBeingSentToDevice = new byte[numberOfBytesPerFullSend];

                                                    while ((numberOfBytesSentSoFar < totalNumberOfBytesToWrite) && DeviceCommunication.DownloadIsEnabled() && deviceWasProgrammedSuccessfully)
                                                    {
                                                        if ((totalNumberOfBytesToWrite - numberOfBytesSentSoFar) >= numberOfBytesPerFullSend)
                                                        {
                                                            numberOfBytesBeingSent = numberOfBytesPerFullSend;
                                                        }
                                                        else
                                                        {
                                                            numberOfBytesBeingSent = totalNumberOfBytesToWrite - numberOfBytesSentSoFar;
                                                        }
                                                        Array.Copy(byteData, numberOfBytesSentSoFar, bytesBeingSentToDevice, 0, numberOfBytesBeingSent);

                                                        deviceWasProgrammedSuccessfully = InteractiveDeviceCommunication.BHM_SetDeviceSetup(modBusAddress, offset, bytesBeingSentToDevice, numberOfBytesBeingSent, readDelayInMicroseconds,
                                                                                                                                            MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                        offset += numberOfBytesBeingSent;
                                                        numberOfBytesSentSoFar += numberOfBytesBeingSent;
                                                        Application.DoEvents();

                                                        iteration++;
                                                        SetProgressBarProgress(iteration, 5);
                                                    }
                                                    if (deviceWasProgrammedSuccessfully)
                                                    {
                                                        errorCode = ErrorCode.ConfigurationWriteSucceeded;

                                                        if (modBusAddress.ToString().CompareTo(BHM_MonitorConfiguration.monitor.ModbusAddress) != 0)

                                                            using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                            {
                                                                if (modBusAddress.ToString().CompareTo(BHM_MonitorConfiguration.monitor.ModbusAddress) != 0)
                                                                {
                                                                    monitorFromDB = General_DatabaseMethods.GetOneMonitor(BHM_MonitorConfiguration.monitor.ID, localDB);
                                                                    if (monitorFromDB != null)
                                                                    {
                                                                        monitorFromDB.ModbusAddress = modBusAddress.ToString();
                                                                        localDB.SubmitChanges();
                                                                    }
                                                                    BHM_MonitorConfiguration.monitor.ModbusAddress = modBusAddress.ToString();
                                                                }
                                                                // now, delete the old device config from the DB, and save the new one.
                                                                SaveDeviceConfigurationToDatabase(BHM_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                                LoadAvailableConfigurations(localDB);
                                                            }
                                                        /// since the configuraiton has been "saved" by using it to program the device, we update the unedited configuration
                                                        /// to match what we just wrote to the device
                                                        this.uneditedWorkingConfiguration = BHM_Configuration.CopyConfiguration(this.workingConfiguration);
                                                        // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteSucceeded));
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteFailed));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToBHM));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            if (errorCode == ErrorCode.SerialPortNotSpecified)
                                            {
                                                RadMessageBox.Show(this, serialPortNotSetWarningText);
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in BHM_MonitorConfiguration.ProgramDevice()\nBHM_MonitorConfiguration.monitor was null.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, noCurrentConfigurationDefinedText);
                        }

                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ProgramDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                MonitorConnection.CloseConnection();
            }
        }
    }
}
