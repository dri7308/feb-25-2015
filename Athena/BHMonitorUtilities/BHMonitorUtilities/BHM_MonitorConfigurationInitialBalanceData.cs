﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;
using DatabaseInterface;
using FormatConversion;

namespace BHMonitorUtilities
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private void WriteInitialBalanceDataTabObjectValues(BHM_Configuration inputConfiguration)
        {
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Text = ConversionMethods.ConvertIntegerValuesToDateTime(inputConfiguration.initialParameters.Month, inputConfiguration.initialParameters.Day,
                                                                                                 inputConfiguration.initialParameters.Year + 2000, inputConfiguration.initialParameters.Hour,
                                                                                                 inputConfiguration.initialParameters.Min).ToString();

            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.GammaAmplitude / 100.0).ToString();
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.GammaAmplitude / 100.0).ToString();

            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.GammaPhase / 100.0).ToString();
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.GammaPhase / 100.0).ToString();

            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.Temperature - 70).ToString();
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.Temperature - 70).ToString();

            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.PhaseAmplitude_0 / 10.0).ToString();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.PhaseAmplitude_0 / 10.0).ToString();

            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.PhaseAmplitude_1 / 10.0).ToString();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.PhaseAmplitude_1 / 10.0).ToString();

            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.PhaseAmplitude_2 / 10.0).ToString();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.PhaseAmplitude_2 / 10.0).ToString();


            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.SignalPhase_0 / 100.0).ToString();
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.SignalPhase_0 / 100.0).ToString();

            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.SignalPhase_1 / 100.0).ToString();
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Text = (inputConfiguration.trSideParamSideTwo.SignalPhase_1 / 100.0).ToString();

            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Text = (inputConfiguration.trSideParamSideOne.SignalPhase_2 / 100.0).ToString();
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Text = (inputConfiguration.trSideParamSideTwo.SignalPhase_2 / 100.0).ToString();

        }

        private void EnableInitialBalanceDataTabObjects()
        {

        }

        private void DisableInitialBalanceDataTabObjects()
        {

        }
    }
}
