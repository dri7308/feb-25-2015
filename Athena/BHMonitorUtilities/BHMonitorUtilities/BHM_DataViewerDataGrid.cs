﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Linq;
using System.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.IO;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.RichTextBox;

using ChartDirector;

using GeneralUtilities;
using ConfigurationObjects;
using DataObjects;
using PasswordManagement;
using FormatConversion;
using DatabaseInterface;
using DatabaseFileInteraction;
using Dynamics;
using MonitorUtilities;
using StatisticsDisplay;

namespace BHMonitorUtilities
{
    public partial class BHM_DataViewer : Telerik.WinControls.UI.RadForm
    {

        private void GridDataEnableEditing()
        {
            try
            {
                gridDataSaveChangesRadButton.Enabled = true;
                gridDataCancelChangesRadButton.Enabled = true;
                gridDataEditRadButton.Enabled = false;
                gridDataDeleteSelectedRadButton.Enabled = false;
                gridDataIsBeingEdited = true;
                bhm_DataReadingsRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.GridDataEnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GridDataDisableEditing()
        {
            try
            {
                gridDataSaveChangesRadButton.Enabled = false;
                gridDataCancelChangesRadButton.Enabled = false;
                gridDataEditRadButton.Enabled = true;
                gridDataDeleteSelectedRadButton.Enabled = true;
                gridDataIsBeingEdited = false;
                bhm_DataReadingsRadGridView.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.GridDataDisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Dictionary<Guid, int> GetAllSelectedGridEntries()
        {
            Dictionary<Guid, int> selectedEntries = null;
            try
            {
                selectedEntries = GetSelectedInsulationParametersEntries();
                selectedEntries = MergeDictionaries(selectedEntries, GetSelectedTransformerSideParametersEntries());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetAllSelectedGridEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }

        private Dictionary<Guid, int> MergeDictionaries(Dictionary<Guid, int> dictOne, Dictionary<Guid, int> dictTwo)
        {
            Dictionary<Guid, int> selectedEntries = dictOne;
            try
            {
                foreach (KeyValuePair<Guid, int> entry in dictTwo)
                {
                    if (!selectedEntries.ContainsKey(entry.Key))
                    {
                        selectedEntries.Add(entry.Key, entry.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.MergeDictionaries(Dictionary<Guid, int>, Dictionary<Guid, int>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }

        private Dictionary<Guid, int> GetSelectedInsulationParametersEntries()
        {
            Dictionary<Guid, int> selectedEntries = new Dictionary<Guid, int>();
            try
            {
                Guid dataRootID;
                if (this.bhm_DataReadingsRadGridView.Rows != null)
                {
                    int rowCount = this.bhm_DataReadingsRadGridView.Rows.Count;
                    for (int i = 0; i < rowCount; i++)
                    {
                        if (this.bhm_DataReadingsRadGridView.Rows[i].IsSelected)
                        {
                            dataRootID = Guid.Parse(this.bhm_DataReadingsRadGridView.Rows[i].Cells[1].Value.ToString());
                            if (!selectedEntries.ContainsKey(dataRootID))
                            {
                                selectedEntries.Add(dataRootID, 1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetSelectedParametersEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }

        private Dictionary<Guid, int> GetSelectedTransformerSideParametersEntries()
        {
            Dictionary<Guid, int> selectedEntries = new Dictionary<Guid, int>();
            try
            {
                Guid dataRootID;
                if ((this.bhm_DataReadingsRadGridView.Rows != null) &&
                    (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0) &&
                    (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Rows != null) &&
                    (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Rows.Count > 0))
                {
                    int rowCount = this.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Rows.Count;
                    for (int i = 0; i < rowCount; i++)
                    {
                        if (this.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Rows[i].IsSelected)
                        {
                            dataRootID = Guid.Parse(this.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Rows[i].Cells[1].Value.ToString());
                            if (!selectedEntries.ContainsKey(dataRootID))
                            {
                                selectedEntries.Add(dataRootID, 1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetSelectedPDParametersEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }


        private void SetAllGridViewColumnWidths()
        {
            try
            {
                SetParametersGridViewColumnWidths();
                SetPDParametersGridViewColumnWidths();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetAllGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetInsulationParametersGridViewColumnWidths()
        {
            try
            {
                int count;
                if (bhm_DataReadingsRadGridView.Columns != null)
                {
                    count = bhm_DataReadingsRadGridView.Columns.Count;
                    if (count > 0)
                    {
                        if ((this.insulatationParametersGridViewColumnWidths == null) || (count != this.insulatationParametersGridViewColumnWidths.Count()))
                        {
                            this.insulatationParametersGridViewColumnWidths = new int[count];
                        }

                        for (int i = 0; i < count; i++)
                        {
                            this.insulatationParametersGridViewColumnWidths[i] =
                                bhm_DataReadingsRadGridView.Columns[i].Width;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetInsulationParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetTransformerSideParametersGridViewColumnWidths()
        {
            try
            {
                int count;

                if ((bhm_DataReadingsRadGridView.MasterTemplate.Templates != null) &&
                    (bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0) &&
                    (bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns != null))
                {
                    count = bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns.Count;
                    if (count > 0)
                    {
                        if ((this.transformerSideParametersGridViewColumnWidths == null) || (count != this.transformerSideParametersGridViewColumnWidths.Count()))
                        {
                            this.transformerSideParametersGridViewColumnWidths = new int[count];
                        }
                        for (int i = 0; i < count; i++)
                        {
                            this.transformerSideParametersGridViewColumnWidths[i] =
                                bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[i].Width;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetTransformerSideParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetParametersGridViewColumnWidths()
        {
            try
            {
                int count;

                if (bhm_DataReadingsRadGridView.Columns != null)
                {
                    count = bhm_DataReadingsRadGridView.Columns.Count;
                    if (this.insulatationParametersGridViewColumnWidths != null)
                    {
                        if (this.insulatationParametersGridViewColumnWidths.Count() < count)
                        {
                            count = this.insulatationParametersGridViewColumnWidths.Count();
                        }
                        bhm_DataReadingsRadGridView.Columns[0].MinWidth = 1;
                        bhm_DataReadingsRadGridView.Columns[0].Width = 1;
                        bhm_DataReadingsRadGridView.Columns[1].MinWidth = 1;
                        bhm_DataReadingsRadGridView.Columns[1].Width = 1;
                        for (int i = 2; i < count; i++)
                        {
                            bhm_DataReadingsRadGridView.Columns[i].Width = this.insulatationParametersGridViewColumnWidths[i];

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetPDParametersGridViewColumnWidths()
        {
            try
            {
                int count;

                if (bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns != null)
                {
                    count = bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns.Count;
                    if (this.transformerSideParametersGridViewColumnWidths != null)
                    {
                        if (this.transformerSideParametersGridViewColumnWidths.Count() < count)
                        {
                            count = this.transformerSideParametersGridViewColumnWidths.Count();
                        }
                        bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].MinWidth = 1;
                        bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].Width = 1;
                        bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].MinWidth = 1;
                        bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].Width = 1;
                        for (int i = 2; i < count; i++)
                        {
                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[i].Width = this.transformerSideParametersGridViewColumnWidths[i];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetPDParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetBindingSourceForGridView()
        {
            try
            {
                //if (dataGridInsulationParametersRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //{
                //    bhm_DataReadingsRadGridView.DataSource = null;
                //    bhm_DataReadingsRadGridView.DataSource = insulationParametersBindingSource;
                //    bhm_DataReadingsRadGridView.Columns[0].ReadOnly = true;
                //    bhm_DataReadingsRadGridView.Columns[1].ReadOnly = true;
                //}
                //else if (dataGridTransformerSideParametersRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //{
                //    bhm_DataReadingsRadGridView.DataSource = null;
                //    bhm_DataReadingsRadGridView.DataSource = transformerSideParametersBindingSource;
                //    bhm_DataReadingsRadGridView.Columns[0].ReadOnly = true;
                //    bhm_DataReadingsRadGridView.Columns[1].ReadOnly = true;
                //    bhm_DataReadingsRadGridView.Columns[2].ReadOnly = true;
                //}

                bhm_DataReadingsRadGridView.DataSource = null;
                bhm_DataReadingsRadGridView.Relations.Clear();
                bhm_DataReadingsRadGridView.MasterTemplate.Templates.Clear();
                bhm_DataReadingsRadGridView.DataSource = insulationParametersBindingSource;

                GridViewTemplate childTemplate = new GridViewTemplate();
                childTemplate.DataSource = transformerSideParametersBindingSource;
                childTemplate.AllowAddNewRow = false;
                childTemplate.AllowDeleteRow = false;

                bhm_DataReadingsRadGridView.MasterTemplate.Templates.Add(childTemplate);

                GridViewRelation relation = new GridViewRelation(bhm_DataReadingsRadGridView.MasterTemplate);
                relation.ChildTemplate = childTemplate;
                relation.RelationName = "parentChild";
                relation.ParentColumnNames.Add("DataRootID");
                relation.ChildColumnNames.Add("DataRootID");
                bhm_DataReadingsRadGridView.Relations.Add(relation);

                ResizeGridViewColumns();

                GridDataDisableEditing();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.SetBindingSourceForGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ResizeGridViewColumns()
        {
            try
            {
                int i;
                if (bhm_DataReadingsRadGridView.Columns != null)
                {
                    if ((bhm_DataReadingsRadGridView.Columns.Count > 1) && (bhm_DataReadingsRadGridView.Columns.Count == insulationParametersGridViewColumnCount))
                    {
                        if (bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0)
                        {
                            if (bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns != null)
                            {
                                if (bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns.Count == transformerSideParametersGridViewColumnCount)
                                {
                                    int insulationColumnsCount = bhm_DataReadingsRadGridView.Columns.Count;
                                    int transformerColumnsCount = bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns.Count;

                                    // Do NOT take out the MinWidth commands or you will be sorry, because then you will expose the hidden stuff no matter what else you do
                                    bhm_DataReadingsRadGridView.Columns[0].MinWidth = 1;
                                    bhm_DataReadingsRadGridView.Columns[0].Width = 1;
                                    bhm_DataReadingsRadGridView.Columns[1].MinWidth = 1;
                                    bhm_DataReadingsRadGridView.Columns[1].Width = 1;

                                    bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].MinWidth = 1;
                                    bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].Width = 1;
                                  //  bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].AllowReorder = false;
                                    bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].MinWidth = 1;
                                    bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].Width = 1;
                                  //  bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].AllowReorder = false;

                                    /// test to see if renaming columns is possible after binding, it is, and doesn't seem to affect parent-child relations
                                    // bhm_DataReadingsRadGridView.Columns[0].HeaderText = "Fred Rulz";

                                    for (i = 2; i < insulationParametersGridViewColumnCount; i++)
                                    {
                                        bhm_DataReadingsRadGridView.Columns[i].Width = insulatationParametersGridViewColumnWidths[i];
                                    }

                                    for (i = 2; i < transformerSideParametersGridViewColumnCount; i++)
                                    {
                                        bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[i].Width = transformerSideParametersGridViewColumnWidths[i];
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in BHMDataViewer.ResizeGridViewColumns()\nthis.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns had an incorrect number of entries.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in BHMDataViewer.ResizeGridViewColumns()\nthis.bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHMDataViewer.ResizeGridViewColumns()\nthis.bhm_DataReadingsRadGridView.MasterTemplate.Templates.Count was 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHMDataViewer.ResizeGridViewColumns()\nthis.bhm_DataReadingsRadGridView.Columns had an incorrect number of entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHMDataViewer.ResizeGridViewColumns()\nthis.bhm_DataReadingsRadGridView.Columns was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.ResizeGridViewColumns()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        /// <summary>
        /// Creates a new database connection and re-reads all the data, meant to be called after the
        /// data have been edited by the user in the grid view.
        /// </summary>
        private void ResetDatabaseConnectionAndReloadAndDisplayData()
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    DateTime dataLimitDateTime = General_DatabaseMethods.MinimumDateTime();
                    if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 0, 3);
                    }
                    else if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 0, 6);
                    }
                    else if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 1, 0);
                    }

                    this.dataCutoffDate = dataLimitDateTime;

                    // LoadDatabaseData(dataLimitDateTime);
                    // SetBindingSourceAndSetSomeColumnsToReadOnly();

                    SetNewSQLConnectionAndGetCurrentData();
                    SetBindingSourceForGridView();
                    SetInsulationParametersGridViewColumnHeaderText();
                    SetTransformerSideParametersGridViewColumnHeaderText();
                    SetParametersGridViewColumnWidths();
                    SetPDParametersGridViewColumnWidths();

                    LoadData(dataLimitDateTime);
                    SetDataDisplayGroupInformation();
                    // LoadDatabaseDataIntoLocalArrays();
                    SetGroupRadioButtonStates();
                    SetChartDateLimits();
                    if (trendAutoDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawBothCharts();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.ResetDatabaseConnectionAndReloadAndDisplayData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        /// <summary>
        /// Establishes a new connection to the database and gets all the current data.  This
        /// method will be called every time the database information associated with the 
        /// binding sources has been updated using a different database connection.
        /// </summary>
        private void SetNewSQLConnectionAndGetCurrentData()
        {
            try
            {
                if (this.sqlDbConnection != null)
                {
                    this.sqlDbConnection.Close();
                    this.sqlDbConnection = null;
                }

                this.sqlDbConnection = new SqlConnection(this.dbConnectionString);

                /// build the query strings, you need to do this here because the limiting times can change
                StringBuilder ipQueryString = new StringBuilder();
                ipQueryString.Append("SELECT ID, DataRootID, ReadingDateTime, Humidity, Temperature_0, Temperature_1, Temperature_2,  Temperature_3");
                ipQueryString.Append(", Current_0, Current_1, CurrentR_1");
                //ipQueryString.Append(", Reserved_0, Reserved_1, Reserved_2");
                //ipQueryString.Append(", Reserved_10, Reserved_11, Reserved_12, Reserved_13, Reserved_14, Reserved_15, Reserved_16, Reserved_17, Reserved_18, Reserved_19"); 
                ipQueryString.Append(" FROM BHM_Data_InsulationParameters WHERE MonitorID = '");
                ipQueryString.Append(this.monitorID.ToString());
                ipQueryString.Append("' AND ReadingDateTime >= '");
                ipQueryString.Append(this.dataCutoffDate.ToString());
                ipQueryString.Append("' ORDER BY ReadingDateTime ASC");

                insulationParametersQueryString = ipQueryString.ToString();

                StringBuilder tspQueryString = new StringBuilder();
                tspQueryString.Append("SELECT ID, DataRootID, SideNumber, SourceAmplitude_0, SourceAmplitude_1, SourceAmplitude_2, PhaseAmplitude_0, PhaseAmplitude_1, PhaseAmplitude_2, Gamma, GammaPhase");
                tspQueryString.Append(", KT, AlarmStatus, RemainingLife_0, RemainingLife_1, RemainingLife_2, DefectCode, ChPhaseShift, Trend, KTPhase, SignalPhase_0, SignalPhase_1, SignalPhase_2");
                tspQueryString.Append(", SourcePhase_0, SourcePhase_1, SourcePhase_2, Frequency, Temperature, C_0, C_1, C_2, Tg_0, Tg_1, Tg_2, ExternalSyncShift");
                tspQueryString.Append(" FROM BHM_Data_TransformerSideParameters WHERE MonitorID = '");
                tspQueryString.Append(this.monitorID.ToString());
                tspQueryString.Append("' AND ReadingDateTime >= '");
                tspQueryString.Append(this.dataCutoffDate.ToString());
                tspQueryString.Append("' ORDER BY ReadingDateTime ASC, SideNumber ASC");

                transformerSideParametersQueryString = tspQueryString.ToString();

                insulationParametersDataAdapter = new SqlDataAdapter(insulationParametersQueryString, this.sqlDbConnection);
                transformerSideParametersDataAdapter = new SqlDataAdapter(transformerSideParametersQueryString, this.sqlDbConnection);

                insulationParametersCommandBuilder = new SqlCommandBuilder(insulationParametersDataAdapter);
                transformerSideParametersCommandBuilder = new SqlCommandBuilder(transformerSideParametersDataAdapter);

                this.insulationParametersDataTable = new DataTable();
                FillInsulationParametersDataTable();
                this.insulationParametersGridViewColumnCount = this.insulationParametersDataTable.Columns.Count;

                this.transformerSideParametersDataTable = new DataTable();
                FillTransformerSideParametersDataTable();
                this.transformerSideParametersGridViewColumnCount = this.transformerSideParametersDataTable.Columns.Count;

                insulationParametersBindingSource = new BindingSource();
                insulationParametersBindingSource.DataSource = insulationParametersDataTable;

                transformerSideParametersBindingSource = new BindingSource();
                transformerSideParametersBindingSource.DataSource = transformerSideParametersDataTable;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetNewConnectionAndGetCurrentData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void InitializeInsulationParametersGridViewColumnHeaderText()
        {
            try
            {
                int index = 0;
                this.insulationParametersGridViewColumnHeaderText = new string[this.insulationParametersGridViewColumnCount];

                this.insulationParametersGridViewColumnHeaderText[index++] = idGridLabelText;
                this.insulationParametersGridViewColumnHeaderText[index++] = dataRootIdGridLabelText;
                this.insulationParametersGridViewColumnHeaderText[index++] = readingDateInsulationParametersGridText;
                this.insulationParametersGridViewColumnHeaderText[index++] = this.humidityRadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.temp1RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.temp2RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.temp3RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.temp4RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.loadCurrent1RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index++] = this.loadCurrent2RadCheckBox.Text.Trim();
                this.insulationParametersGridViewColumnHeaderText[index] = this.loadCurrent3RadCheckBox.Text.Trim();
                //this.insulationParametersGridViewColumnHeaderText[index++] = this.voltage1RadCheckBox.Text.Trim();
                //this.insulationParametersGridViewColumnHeaderText[index++] = this.voltage2RadCheckBox.Text.Trim();
                //this.insulationParametersGridViewColumnHeaderText[index] = this.voltage3RadCheckBox.Text.Trim();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.InitializeInsulationParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeTransformerSideParametersGridViewColumnHeaderText()
        {
            try
            {
                this.transformerSideParametersGridViewColumnText = new string[transformerSideParametersGridViewColumnCount];

                this.transformerSideParametersGridViewColumnText[0] = idGridLabelText;
                this.transformerSideParametersGridViewColumnText[1] = dataRootIdGridLabelText;
                this.transformerSideParametersGridViewColumnText[2] = sideNumberGridLabelText;
                this.transformerSideParametersGridViewColumnText[3] = sourceAmplitude0GridLabelText;
                this.transformerSideParametersGridViewColumnText[4] = sourceAmplitude1GridLabelText;
                this.transformerSideParametersGridViewColumnText[5] = sourceAmplitude2GridLabelText;
                this.transformerSideParametersGridViewColumnText[6] = phaseAmplitude0GridLabelText;
                this.transformerSideParametersGridViewColumnText[7] = phaseAmplitude1GridLabelText;
                this.transformerSideParametersGridViewColumnText[8] = phaseAmplitude2GridLabelText;
                this.transformerSideParametersGridViewColumnText[9] = gammaGridLabelText;
                this.transformerSideParametersGridViewColumnText[10] = gammaPhaseGridLabelText;
                this.transformerSideParametersGridViewColumnText[11] = ktGridLabelText;
                this.transformerSideParametersGridViewColumnText[12] = alarmStatusGridLabelText;
                this.transformerSideParametersGridViewColumnText[13] = remainingLife0GridLabelText;
                this.transformerSideParametersGridViewColumnText[14] = remainingLife1GridLabelText;
                this.transformerSideParametersGridViewColumnText[15] = remainingLife2GridLabelText;
                this.transformerSideParametersGridViewColumnText[16] = defectCodeGridLabelText;
                this.transformerSideParametersGridViewColumnText[17] = chPhaseShiftGridLabelText;
                this.transformerSideParametersGridViewColumnText[18] = trendGridLabelText;
                this.transformerSideParametersGridViewColumnText[19] = ktPhaseGridLabelText;
                this.transformerSideParametersGridViewColumnText[20] = signalPhase0GridLabelText;
                this.transformerSideParametersGridViewColumnText[21] = signalPhase1GridLabelText;
                this.transformerSideParametersGridViewColumnText[22] = signalPhase2GridLabelText;
                this.transformerSideParametersGridViewColumnText[23] = sourcePhase0GridLabelText;
                this.transformerSideParametersGridViewColumnText[24] = sourcePhase1GridLabelText;
                this.transformerSideParametersGridViewColumnText[25] = sourcePhase2GridLabelText;
                this.transformerSideParametersGridViewColumnText[26] = frequencyGridLabelText;
                this.transformerSideParametersGridViewColumnText[27] = temperatureGridLabelText;
                this.transformerSideParametersGridViewColumnText[28] = c0GridLabelText;
                this.transformerSideParametersGridViewColumnText[29] = c1GridLabelText;
                this.transformerSideParametersGridViewColumnText[30] = c2GridLabelText;
                this.transformerSideParametersGridViewColumnText[31] = tg0GridLabelText;
                this.transformerSideParametersGridViewColumnText[32] = tg1GridLabelText;
                this.transformerSideParametersGridViewColumnText[33] = tg2GridLabelText;
                this.transformerSideParametersGridViewColumnText[34] = externalSyncShiftGridLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.InitializeTransformerSideParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void SetInsulationParametersGridViewColumnHeaderText()
        {
            try
            {
                int count;
                if (bhm_DataReadingsRadGridView.Columns != null)
                {
                    count = bhm_DataReadingsRadGridView.Columns.Count;
                    if (count > 10)
                    {
                        if (this.insulationParametersGridViewColumnHeaderText != null)
                        {
                            if (this.insulationParametersGridViewColumnHeaderText.Count() < count)
                            {
                                count = this.insulationParametersGridViewColumnHeaderText.Count();
                            }
                            bhm_DataReadingsRadGridView.Columns[0].IsPinned = true;
                            bhm_DataReadingsRadGridView.Columns[0].AllowResize = false;
                            bhm_DataReadingsRadGridView.Columns[1].IsPinned = true;
                            bhm_DataReadingsRadGridView.Columns[1].AllowResize = false;
                            bhm_DataReadingsRadGridView.Columns[2].IsPinned = true;
                            for (int i = 0; i < count; i++)
                            {
                                bhm_DataReadingsRadGridView.Columns[i].DisableHTMLRendering = false;
                                bhm_DataReadingsRadGridView.Columns[i].HeaderText = this.insulationParametersGridViewColumnHeaderText[i];
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_DataViewer.SetInsulationParametersGridViewColumnHeaderText()\nthis.bhm_DataReadingsRadGridView had too few columns.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DataViewer.SetInsulationParametersGridViewColumnHeaderText()\nthis.bhm_DataReadingsRadGridView was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetInsulationParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetTransformerSideParametersGridViewColumnHeaderText()
        {
            try
            {
                int count;
                if (bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns != null)
                {
                    count = bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns.Count;
                    if (count > 34)
                    {
                        if (this.transformerSideParametersGridViewColumnText != null)
                        {
                            if (this.transformerSideParametersGridViewColumnText.Count() < count)
                            {
                                count = this.transformerSideParametersGridViewColumnText.Count();
                            }

                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].IsPinned = true;
                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].AllowResize = false;
                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[0].AllowReorder = false;

                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].IsPinned = true;
                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].AllowResize = false;
                            bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[1].AllowReorder = false;

                            for (int i = 0; i < count; i++)
                            {
                                bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[i].DisableHTMLRendering = false;
                                bhm_DataReadingsRadGridView.MasterTemplate.Templates[0].Columns[i].HeaderText = this.transformerSideParametersGridViewColumnText[i];
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_DataViewer.SetTransformerSideParametersGridViewColumnHeaderText()\nthis.bhm_DataReadingsRadGridView had too few columns.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DataViewer.SetTransformerSideParametersGridViewColumnHeaderText()\nthis.bhm_DataReadingsRadGridView was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.SetTransformerSideParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the company data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillInsulationParametersDataTable()
        {
            try
            {
                this.insulationParametersDataAdapter.Fill(this.insulationParametersDataTable);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.FillInsulationParametersDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the company data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillTransformerSideParametersDataTable()
        {
            try
            {
                this.transformerSideParametersDataAdapter.Fill(this.transformerSideParametersDataTable);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.FillTransformerSideParametersDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeGridViewColumnWidthVariables()
        {
            try
            {
               // System.Diagnostics.Debugger.Break();
                int i;
                int defaultWidth = 105;

                if (this.insulationParametersGridViewColumnCount > 0)
                {
                    if (this.transformerSideParametersGridViewColumnCount > 0)
                    {
                        insulatationParametersGridViewColumnWidths = new int[insulationParametersGridViewColumnCount];
                        transformerSideParametersGridViewColumnWidths = new int[transformerSideParametersGridViewColumnCount];

                        for (i = 0; i < insulationParametersGridViewColumnCount; i++)
                        {
                            insulatationParametersGridViewColumnWidths[i] = defaultWidth;
                        }

                        for (i = 0; i < transformerSideParametersGridViewColumnCount; i++)
                        {
                            transformerSideParametersGridViewColumnWidths[i] = defaultWidth;
                        }

                        insulatationParametersGridViewColumnWidths[0] = 1;
                        insulatationParametersGridViewColumnWidths[1] = 1;
                        insulatationParametersGridViewColumnWidths[2] = 125;

                        int val1 = 55;
                        insulatationParametersGridViewColumnWidths[3] = val1;

                        insulatationParametersGridViewColumnWidths[4] = 125;

                        int val2 = 80;
                        insulatationParametersGridViewColumnWidths[5] = val2;
                        insulatationParametersGridViewColumnWidths[6] = val2;
                        insulatationParametersGridViewColumnWidths[7] = val2;

                        int val3 = 100;
                        insulatationParametersGridViewColumnWidths[8] = val3;
                        insulatationParametersGridViewColumnWidths[9] = val3;
                        insulatationParametersGridViewColumnWidths[10] = val3;

                       // int val4 = 90;
                     //   insulatationParametersGridViewColumnWidths[11] = val4;
                       // insulatationParametersGridViewColumnWidths[12] = val4;
                     //  insulatationParametersGridViewColumnWidths[13] = val4;



                        transformerSideParametersGridViewColumnWidths[0] = 1;
                        transformerSideParametersGridViewColumnWidths[1] = 1;

                        transformerSideParametersGridViewColumnWidths[2] = 70;

                        int val5 = 105;
                        transformerSideParametersGridViewColumnWidths[3] = val5;
                        transformerSideParametersGridViewColumnWidths[4] = val5;
                        transformerSideParametersGridViewColumnWidths[5] = val5;
                        transformerSideParametersGridViewColumnWidths[6] = val5;
                        transformerSideParametersGridViewColumnWidths[7] = val5;
                        transformerSideParametersGridViewColumnWidths[8] = val5;

                        transformerSideParametersGridViewColumnWidths[9] = 50;

                        transformerSideParametersGridViewColumnWidths[10] = 90;

                        transformerSideParametersGridViewColumnWidths[11] = 40;

                        transformerSideParametersGridViewColumnWidths[12] = 70;

                        int val6 = 100;
                        transformerSideParametersGridViewColumnWidths[13] = val6;
                        transformerSideParametersGridViewColumnWidths[14] = val6;
                        transformerSideParametersGridViewColumnWidths[15] = val6;

                        transformerSideParametersGridViewColumnWidths[16] = 70;

                        transformerSideParametersGridViewColumnWidths[17] = 90;

                        transformerSideParametersGridViewColumnWidths[18] = 60;

                        transformerSideParametersGridViewColumnWidths[19] = 70;

                        int val7 = 95;
                        transformerSideParametersGridViewColumnWidths[20] = val7;
                        transformerSideParametersGridViewColumnWidths[21] = val7;
                        transformerSideParametersGridViewColumnWidths[22] = val7;
                        transformerSideParametersGridViewColumnWidths[23] = val7;
                        transformerSideParametersGridViewColumnWidths[24] = val7;
                        transformerSideParametersGridViewColumnWidths[25] = val7;

                        transformerSideParametersGridViewColumnWidths[26] = 80;

                        transformerSideParametersGridViewColumnWidths[27] = 85;

                        int val8 = 40;
                        transformerSideParametersGridViewColumnWidths[28] = val8;
                        transformerSideParametersGridViewColumnWidths[29] = val8;
                        transformerSideParametersGridViewColumnWidths[30] = val8;
                        transformerSideParametersGridViewColumnWidths[31] = val8;
                        transformerSideParametersGridViewColumnWidths[32] = val8;
                        transformerSideParametersGridViewColumnWidths[33] = val8;

                        transformerSideParametersGridViewColumnWidths[34] = 100;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.InitializeGridViewColumnWidthVariables()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetAllGridViewColumnWidths()
        {
            try
            {
                GetInsulationParametersGridViewColumnWidths();
                GetTransformerSideParametersGridViewColumnWidths();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.GetAllGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        } 
    }
}
