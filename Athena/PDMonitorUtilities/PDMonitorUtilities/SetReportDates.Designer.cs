﻿namespace PDMonitorUtilities
{
    partial class SetReportDates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetReportDates));
            this.phaseResolvedDataDatesRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phaseResolvedDataDatesRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.availableConfigurationsCommonSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.dataDateSelectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dataBeforePhaseResolvedDataRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.mostRecentDataRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.selectedDatesRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dateEndDateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dataEndDateRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.dataStartDateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dataStartDateRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.phaseResolvedDataDateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.phaseResolvedDataDateRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.useDefaultDatesRadButton = new Telerik.WinControls.UI.RadButton();
            this.useSelectedDatesRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDatesRadGroupBox)).BeginInit();
            this.phaseResolvedDataDatesRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDatesRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDateSelectionRadGroupBox)).BeginInit();
            this.dataDateSelectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBeforePhaseResolvedDataRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostRecentDataRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatesRadGroupBox)).BeginInit();
            this.selectedDatesRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEndDateRadGroupBox)).BeginInit();
            this.dateEndDateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataEndDateRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataStartDateRadGroupBox)).BeginInit();
            this.dataStartDateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataStartDateRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDateRadGroupBox)).BeginInit();
            this.phaseResolvedDataDateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDateRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useDefaultDatesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedDatesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // phaseResolvedDataDatesRadGroupBox
            // 
            this.phaseResolvedDataDatesRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phaseResolvedDataDatesRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.phaseResolvedDataDatesRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phaseResolvedDataDatesRadGroupBox.Controls.Add(this.phaseResolvedDataDatesRadListControl);
            this.phaseResolvedDataDatesRadGroupBox.Controls.Add(this.availableConfigurationsCommonSettingsTabRadListControl);
            this.phaseResolvedDataDatesRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseResolvedDataDatesRadGroupBox.FooterImageIndex = -1;
            this.phaseResolvedDataDatesRadGroupBox.FooterImageKey = "";
            this.phaseResolvedDataDatesRadGroupBox.HeaderImageIndex = -1;
            this.phaseResolvedDataDatesRadGroupBox.HeaderImageKey = "";
            this.phaseResolvedDataDatesRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phaseResolvedDataDatesRadGroupBox.HeaderText = "<html>Phase Resolved Data Dates</html>";
            this.phaseResolvedDataDatesRadGroupBox.Location = new System.Drawing.Point(12, 77);
            this.phaseResolvedDataDatesRadGroupBox.Name = "phaseResolvedDataDatesRadGroupBox";
            this.phaseResolvedDataDatesRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phaseResolvedDataDatesRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phaseResolvedDataDatesRadGroupBox.Size = new System.Drawing.Size(193, 327);
            this.phaseResolvedDataDatesRadGroupBox.TabIndex = 44;
            this.phaseResolvedDataDatesRadGroupBox.Text = "<html>Phase Resolved Data Dates</html>";
            this.phaseResolvedDataDatesRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phaseResolvedDataDatesRadListControl
            // 
            this.phaseResolvedDataDatesRadListControl.AutoScroll = true;
            this.phaseResolvedDataDatesRadListControl.CaseSensitiveSort = true;
            this.phaseResolvedDataDatesRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.phaseResolvedDataDatesRadListControl.ItemHeight = 18;
            this.phaseResolvedDataDatesRadListControl.Location = new System.Drawing.Point(10, 20);
            this.phaseResolvedDataDatesRadListControl.Name = "phaseResolvedDataDatesRadListControl";
            this.phaseResolvedDataDatesRadListControl.Size = new System.Drawing.Size(173, 297);
            this.phaseResolvedDataDatesRadListControl.TabIndex = 10;
            this.phaseResolvedDataDatesRadListControl.Text = "radListControl2";
            this.phaseResolvedDataDatesRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.phaseResolvedDataDatesRadListControl_SelectedIndexChanged);
            // 
            // availableConfigurationsCommonSettingsTabRadListControl
            // 
            this.availableConfigurationsCommonSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsCommonSettingsTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsCommonSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsCommonSettingsTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsCommonSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsCommonSettingsTabRadListControl.Name = "availableConfigurationsCommonSettingsTabRadListControl";
            this.availableConfigurationsCommonSettingsTabRadListControl.Size = new System.Drawing.Size(173, 297);
            this.availableConfigurationsCommonSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsCommonSettingsTabRadListControl.Text = "radListControl2";
            // 
            // dataDateSelectionRadGroupBox
            // 
            this.dataDateSelectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dataDateSelectionRadGroupBox.Controls.Add(this.dataBeforePhaseResolvedDataRadRadioButton);
            this.dataDateSelectionRadGroupBox.Controls.Add(this.mostRecentDataRadRadioButton);
            this.dataDateSelectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDateSelectionRadGroupBox.FooterImageIndex = -1;
            this.dataDateSelectionRadGroupBox.FooterImageKey = "";
            this.dataDateSelectionRadGroupBox.HeaderImageIndex = -1;
            this.dataDateSelectionRadGroupBox.HeaderImageKey = "";
            this.dataDateSelectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dataDateSelectionRadGroupBox.HeaderText = "Data selection";
            this.dataDateSelectionRadGroupBox.Location = new System.Drawing.Point(211, 12);
            this.dataDateSelectionRadGroupBox.Name = "dataDateSelectionRadGroupBox";
            this.dataDateSelectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dataDateSelectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dataDateSelectionRadGroupBox.Size = new System.Drawing.Size(188, 105);
            this.dataDateSelectionRadGroupBox.TabIndex = 45;
            this.dataDateSelectionRadGroupBox.Text = "Data selection";
            this.dataDateSelectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dataBeforePhaseResolvedDataRadRadioButton
            // 
            this.dataBeforePhaseResolvedDataRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataBeforePhaseResolvedDataRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataBeforePhaseResolvedDataRadRadioButton.Location = new System.Drawing.Point(13, 56);
            this.dataBeforePhaseResolvedDataRadRadioButton.Name = "dataBeforePhaseResolvedDataRadRadioButton";
            this.dataBeforePhaseResolvedDataRadRadioButton.Size = new System.Drawing.Size(110, 31);
            this.dataBeforePhaseResolvedDataRadRadioButton.TabIndex = 1;
            this.dataBeforePhaseResolvedDataRadRadioButton.Text = "<html>Data up to phase<br>resolved data date</html>";
            this.dataBeforePhaseResolvedDataRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataBeforePhaseResolvedDataRadRadioButton_ToggleStateChanged);
            // 
            // mostRecentDataRadRadioButton
            // 
            this.mostRecentDataRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostRecentDataRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.mostRecentDataRadRadioButton.Location = new System.Drawing.Point(13, 32);
            this.mostRecentDataRadRadioButton.Name = "mostRecentDataRadRadioButton";
            this.mostRecentDataRadRadioButton.Size = new System.Drawing.Size(110, 18);
            this.mostRecentDataRadRadioButton.TabIndex = 0;
            this.mostRecentDataRadRadioButton.Text = "Most recent data";
            this.mostRecentDataRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.mostRecentDataRadRadioButton_ToggleStateChanged);
            // 
            // selectedDatesRadGroupBox
            // 
            this.selectedDatesRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.selectedDatesRadGroupBox.Controls.Add(this.dateEndDateRadGroupBox);
            this.selectedDatesRadGroupBox.Controls.Add(this.dataStartDateRadGroupBox);
            this.selectedDatesRadGroupBox.Controls.Add(this.phaseResolvedDataDateRadGroupBox);
            this.selectedDatesRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedDatesRadGroupBox.FooterImageIndex = -1;
            this.selectedDatesRadGroupBox.FooterImageKey = "";
            this.selectedDatesRadGroupBox.HeaderImageIndex = -1;
            this.selectedDatesRadGroupBox.HeaderImageKey = "";
            this.selectedDatesRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.selectedDatesRadGroupBox.HeaderText = "Selected dates";
            this.selectedDatesRadGroupBox.Location = new System.Drawing.Point(211, 123);
            this.selectedDatesRadGroupBox.Name = "selectedDatesRadGroupBox";
            this.selectedDatesRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.selectedDatesRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.selectedDatesRadGroupBox.Size = new System.Drawing.Size(188, 216);
            this.selectedDatesRadGroupBox.TabIndex = 46;
            this.selectedDatesRadGroupBox.Text = "Selected dates";
            this.selectedDatesRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dateEndDateRadGroupBox
            // 
            this.dateEndDateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dateEndDateRadGroupBox.Controls.Add(this.dataEndDateRadTextBox);
            this.dateEndDateRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEndDateRadGroupBox.FooterImageIndex = -1;
            this.dateEndDateRadGroupBox.FooterImageKey = "";
            this.dateEndDateRadGroupBox.HeaderImageIndex = -1;
            this.dateEndDateRadGroupBox.HeaderImageKey = "";
            this.dateEndDateRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dateEndDateRadGroupBox.HeaderText = "Data end date";
            this.dateEndDateRadGroupBox.Location = new System.Drawing.Point(13, 148);
            this.dateEndDateRadGroupBox.Name = "dateEndDateRadGroupBox";
            this.dateEndDateRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dateEndDateRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dateEndDateRadGroupBox.Size = new System.Drawing.Size(162, 54);
            this.dateEndDateRadGroupBox.TabIndex = 49;
            this.dateEndDateRadGroupBox.Text = "Data end date";
            this.dateEndDateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dataEndDateRadTextBox
            // 
            this.dataEndDateRadTextBox.Location = new System.Drawing.Point(13, 23);
            this.dataEndDateRadTextBox.Name = "dataEndDateRadTextBox";
            this.dataEndDateRadTextBox.ReadOnly = true;
            this.dataEndDateRadTextBox.Size = new System.Drawing.Size(136, 20);
            this.dataEndDateRadTextBox.TabIndex = 0;
            this.dataEndDateRadTextBox.TabStop = false;
            this.dataEndDateRadTextBox.Text = "radTextBox1";
            // 
            // dataStartDateRadGroupBox
            // 
            this.dataStartDateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dataStartDateRadGroupBox.Controls.Add(this.dataStartDateRadTextBox);
            this.dataStartDateRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataStartDateRadGroupBox.FooterImageIndex = -1;
            this.dataStartDateRadGroupBox.FooterImageKey = "";
            this.dataStartDateRadGroupBox.HeaderImageIndex = -1;
            this.dataStartDateRadGroupBox.HeaderImageKey = "";
            this.dataStartDateRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dataStartDateRadGroupBox.HeaderText = "Data start date";
            this.dataStartDateRadGroupBox.Location = new System.Drawing.Point(13, 88);
            this.dataStartDateRadGroupBox.Name = "dataStartDateRadGroupBox";
            this.dataStartDateRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dataStartDateRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dataStartDateRadGroupBox.Size = new System.Drawing.Size(162, 54);
            this.dataStartDateRadGroupBox.TabIndex = 48;
            this.dataStartDateRadGroupBox.Text = "Data start date";
            this.dataStartDateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dataStartDateRadTextBox
            // 
            this.dataStartDateRadTextBox.Location = new System.Drawing.Point(13, 23);
            this.dataStartDateRadTextBox.Name = "dataStartDateRadTextBox";
            this.dataStartDateRadTextBox.ReadOnly = true;
            this.dataStartDateRadTextBox.Size = new System.Drawing.Size(136, 20);
            this.dataStartDateRadTextBox.TabIndex = 1;
            this.dataStartDateRadTextBox.TabStop = false;
            this.dataStartDateRadTextBox.Text = "radTextBox1";
            // 
            // phaseResolvedDataDateRadGroupBox
            // 
            this.phaseResolvedDataDateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.phaseResolvedDataDateRadGroupBox.Controls.Add(this.phaseResolvedDataDateRadTextBox);
            this.phaseResolvedDataDateRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseResolvedDataDateRadGroupBox.FooterImageIndex = -1;
            this.phaseResolvedDataDateRadGroupBox.FooterImageKey = "";
            this.phaseResolvedDataDateRadGroupBox.HeaderImageIndex = -1;
            this.phaseResolvedDataDateRadGroupBox.HeaderImageKey = "";
            this.phaseResolvedDataDateRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.phaseResolvedDataDateRadGroupBox.HeaderText = "Phase resolved data date";
            this.phaseResolvedDataDateRadGroupBox.Location = new System.Drawing.Point(13, 28);
            this.phaseResolvedDataDateRadGroupBox.Name = "phaseResolvedDataDateRadGroupBox";
            this.phaseResolvedDataDateRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.phaseResolvedDataDateRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.phaseResolvedDataDateRadGroupBox.Size = new System.Drawing.Size(162, 54);
            this.phaseResolvedDataDateRadGroupBox.TabIndex = 47;
            this.phaseResolvedDataDateRadGroupBox.Text = "Phase resolved data date";
            this.phaseResolvedDataDateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // phaseResolvedDataDateRadTextBox
            // 
            this.phaseResolvedDataDateRadTextBox.Location = new System.Drawing.Point(13, 23);
            this.phaseResolvedDataDateRadTextBox.Name = "phaseResolvedDataDateRadTextBox";
            this.phaseResolvedDataDateRadTextBox.ReadOnly = true;
            this.phaseResolvedDataDateRadTextBox.Size = new System.Drawing.Size(136, 20);
            this.phaseResolvedDataDateRadTextBox.TabIndex = 2;
            this.phaseResolvedDataDateRadTextBox.TabStop = false;
            this.phaseResolvedDataDateRadTextBox.Text = "radTextBox2";
            // 
            // useDefaultDatesRadButton
            // 
            this.useDefaultDatesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.useDefaultDatesRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useDefaultDatesRadButton.Location = new System.Drawing.Point(12, 410);
            this.useDefaultDatesRadButton.Name = "useDefaultDatesRadButton";
            this.useDefaultDatesRadButton.Size = new System.Drawing.Size(125, 50);
            this.useDefaultDatesRadButton.TabIndex = 47;
            this.useDefaultDatesRadButton.Text = "<html>Use default<br>dates</html>";
            this.useDefaultDatesRadButton.ThemeName = "Office2007Black";
            this.useDefaultDatesRadButton.Click += new System.EventHandler(this.useDefaultDatesRadButton_Click);
            // 
            // useSelectedDatesRadButton
            // 
            this.useSelectedDatesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.useSelectedDatesRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useSelectedDatesRadButton.Location = new System.Drawing.Point(143, 410);
            this.useSelectedDatesRadButton.Name = "useSelectedDatesRadButton";
            this.useSelectedDatesRadButton.Size = new System.Drawing.Size(125, 50);
            this.useSelectedDatesRadButton.TabIndex = 45;
            this.useSelectedDatesRadButton.Text = "<html>Use selected<br>dates</html>";
            this.useSelectedDatesRadButton.ThemeName = "Office2007Black";
            this.useSelectedDatesRadButton.Click += new System.EventHandler(this.useSelectedDatesRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(274, 410);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(125, 50);
            this.cancelRadButton.TabIndex = 45;
            this.cancelRadButton.Text = "<html>Cancel</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // SetReportDates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(411, 462);
            this.ControlBox = false;
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.useSelectedDatesRadButton);
            this.Controls.Add(this.useDefaultDatesRadButton);
            this.Controls.Add(this.selectedDatesRadGroupBox);
            this.Controls.Add(this.dataDateSelectionRadGroupBox);
            this.Controls.Add(this.phaseResolvedDataDatesRadGroupBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetReportDates";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Set Report Dates";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SetReportDates_Load);
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDatesRadGroupBox)).EndInit();
            this.phaseResolvedDataDatesRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDatesRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDateSelectionRadGroupBox)).EndInit();
            this.dataDateSelectionRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataBeforePhaseResolvedDataRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mostRecentDataRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatesRadGroupBox)).EndInit();
            this.selectedDatesRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEndDateRadGroupBox)).EndInit();
            this.dateEndDateRadGroupBox.ResumeLayout(false);
            this.dateEndDateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataEndDateRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataStartDateRadGroupBox)).EndInit();
            this.dataStartDateRadGroupBox.ResumeLayout(false);
            this.dataStartDateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataStartDateRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDateRadGroupBox)).EndInit();
            this.phaseResolvedDataDateRadGroupBox.ResumeLayout(false);
            this.phaseResolvedDataDateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phaseResolvedDataDateRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useDefaultDatesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedDatesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox phaseResolvedDataDatesRadGroupBox;
        private Telerik.WinControls.UI.RadListControl phaseResolvedDataDatesRadListControl;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsCommonSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadGroupBox dataDateSelectionRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton dataBeforePhaseResolvedDataRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton mostRecentDataRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox selectedDatesRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox dateEndDateRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox dataEndDateRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox dataStartDateRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox dataStartDateRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox phaseResolvedDataDateRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox phaseResolvedDataDateRadTextBox;
        private Telerik.WinControls.UI.RadButton useDefaultDatesRadButton;
        private Telerik.WinControls.UI.RadButton useSelectedDatesRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
    }
}
