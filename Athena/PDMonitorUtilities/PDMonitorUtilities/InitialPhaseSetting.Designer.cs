namespace PDMonitorUtilities
{
    partial class InitialPhaseSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InitialPhaseSetting));
            this.showVoltageSinewaveRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.sineWaveCColorPanel = new System.Windows.Forms.Panel();
            this.sineWaveBColorPanel = new System.Windows.Forms.Panel();
            this.sineWaveAColorPanel = new System.Windows.Forms.Panel();
            this.colorTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.phaseTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.changeSineWaveCColorRadButton = new Telerik.WinControls.UI.RadButton();
            this.changeSineWaveBColorRadButton = new Telerik.WinControls.UI.RadButton();
            this.changeSineWaveAColorRadButton = new Telerik.WinControls.UI.RadButton();
            this.twoFortyDegreeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.oneTwentyDegreeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.zeroDegreeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.phaseShiftInformationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.okayRadButton = new Telerik.WinControls.UI.RadButton();
            this.phaseShiftRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.showVoltageSinewaveRadGroupBox)).BeginInit();
            this.showVoltageSinewaveRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveCColorRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveBColorRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveAColorRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoFortyDegreeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oneTwentyDegreeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zeroDegreeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseShiftInformationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseShiftRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // showVoltageSinewaveRadGroupBox
            // 
            this.showVoltageSinewaveRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.showVoltageSinewaveRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.sineWaveCColorPanel);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.sineWaveBColorPanel);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.sineWaveAColorPanel);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.colorTextRadLabel);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.phaseTextRadLabel);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.changeSineWaveCColorRadButton);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.changeSineWaveBColorRadButton);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.changeSineWaveAColorRadButton);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.twoFortyDegreeRadCheckBox);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.oneTwentyDegreeRadCheckBox);
            this.showVoltageSinewaveRadGroupBox.Controls.Add(this.zeroDegreeRadCheckBox);
            this.showVoltageSinewaveRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showVoltageSinewaveRadGroupBox.FooterImageIndex = -1;
            this.showVoltageSinewaveRadGroupBox.FooterImageKey = "";
            this.showVoltageSinewaveRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.showVoltageSinewaveRadGroupBox.HeaderImageIndex = -1;
            this.showVoltageSinewaveRadGroupBox.HeaderImageKey = "";
            this.showVoltageSinewaveRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.showVoltageSinewaveRadGroupBox.HeaderText = "Show voltage sine wave";
            this.showVoltageSinewaveRadGroupBox.Location = new System.Drawing.Point(12, 45);
            this.showVoltageSinewaveRadGroupBox.Name = "showVoltageSinewaveRadGroupBox";
            this.showVoltageSinewaveRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.showVoltageSinewaveRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.showVoltageSinewaveRadGroupBox.Size = new System.Drawing.Size(234, 133);
            this.showVoltageSinewaveRadGroupBox.TabIndex = 0;
            this.showVoltageSinewaveRadGroupBox.Text = "Show voltage sine wave";
            this.showVoltageSinewaveRadGroupBox.ThemeName = "Office2007Black";
            // 
            // sineWaveCColorPanel
            // 
            this.sineWaveCColorPanel.Location = new System.Drawing.Point(85, 98);
            this.sineWaveCColorPanel.Name = "sineWaveCColorPanel";
            this.sineWaveCColorPanel.Size = new System.Drawing.Size(20, 20);
            this.sineWaveCColorPanel.TabIndex = 10;
            // 
            // sineWaveBColorPanel
            // 
            this.sineWaveBColorPanel.Location = new System.Drawing.Point(85, 72);
            this.sineWaveBColorPanel.Name = "sineWaveBColorPanel";
            this.sineWaveBColorPanel.Size = new System.Drawing.Size(20, 20);
            this.sineWaveBColorPanel.TabIndex = 9;
            // 
            // sineWaveAColorPanel
            // 
            this.sineWaveAColorPanel.Location = new System.Drawing.Point(85, 46);
            this.sineWaveAColorPanel.Name = "sineWaveAColorPanel";
            this.sineWaveAColorPanel.Size = new System.Drawing.Size(20, 20);
            this.sineWaveAColorPanel.TabIndex = 8;
            // 
            // colorTextRadLabel
            // 
            this.colorTextRadLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.colorTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colorTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.colorTextRadLabel.Location = new System.Drawing.Point(82, 23);
            this.colorTextRadLabel.Name = "colorTextRadLabel";
            this.colorTextRadLabel.Size = new System.Drawing.Size(33, 15);
            this.colorTextRadLabel.TabIndex = 7;
            this.colorTextRadLabel.Text = "<html>Color</html>";
            this.colorTextRadLabel.ThemeName = "Office2007Black";
            // 
            // phaseTextRadLabel
            // 
            this.phaseTextRadLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phaseTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phaseTextRadLabel.Location = new System.Drawing.Point(20, 23);
            this.phaseTextRadLabel.Name = "phaseTextRadLabel";
            this.phaseTextRadLabel.Size = new System.Drawing.Size(38, 15);
            this.phaseTextRadLabel.TabIndex = 6;
            this.phaseTextRadLabel.Text = "<html>Phase</html>";
            this.phaseTextRadLabel.ThemeName = "Office2007Black";
            // 
            // changeSineWaveCColorRadButton
            // 
            this.changeSineWaveCColorRadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.changeSineWaveCColorRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeSineWaveCColorRadButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeSineWaveCColorRadButton.Location = new System.Drawing.Point(126, 98);
            this.changeSineWaveCColorRadButton.Name = "changeSineWaveCColorRadButton";
            this.changeSineWaveCColorRadButton.Size = new System.Drawing.Size(92, 20);
            this.changeSineWaveCColorRadButton.TabIndex = 5;
            this.changeSineWaveCColorRadButton.Text = "Change color";
            this.changeSineWaveCColorRadButton.ThemeName = "Office2007Black";
            this.changeSineWaveCColorRadButton.Click += new System.EventHandler(this.changeSineWaveCColorRadButton_Click);
            // 
            // changeSineWaveBColorRadButton
            // 
            this.changeSineWaveBColorRadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.changeSineWaveBColorRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeSineWaveBColorRadButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeSineWaveBColorRadButton.Location = new System.Drawing.Point(126, 72);
            this.changeSineWaveBColorRadButton.Name = "changeSineWaveBColorRadButton";
            this.changeSineWaveBColorRadButton.Size = new System.Drawing.Size(92, 20);
            this.changeSineWaveBColorRadButton.TabIndex = 5;
            this.changeSineWaveBColorRadButton.Text = "Change color";
            this.changeSineWaveBColorRadButton.ThemeName = "Office2007Black";
            this.changeSineWaveBColorRadButton.Click += new System.EventHandler(this.changeSineWaveBColorRadButton_Click);
            // 
            // changeSineWaveAColorRadButton
            // 
            this.changeSineWaveAColorRadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.changeSineWaveAColorRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeSineWaveAColorRadButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeSineWaveAColorRadButton.Location = new System.Drawing.Point(126, 46);
            this.changeSineWaveAColorRadButton.Name = "changeSineWaveAColorRadButton";
            this.changeSineWaveAColorRadButton.Size = new System.Drawing.Size(92, 20);
            this.changeSineWaveAColorRadButton.TabIndex = 4;
            this.changeSineWaveAColorRadButton.Text = "Change color";
            this.changeSineWaveAColorRadButton.ThemeName = "Office2007Black";
            this.changeSineWaveAColorRadButton.Click += new System.EventHandler(this.changeSineWaveAColorRadButton_Click);
            // 
            // twoFortyDegreeRadCheckBox
            // 
            this.twoFortyDegreeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoFortyDegreeRadCheckBox.Location = new System.Drawing.Point(13, 100);
            this.twoFortyDegreeRadCheckBox.Name = "twoFortyDegreeRadCheckBox";
            this.twoFortyDegreeRadCheckBox.Size = new System.Drawing.Size(56, 16);
            this.twoFortyDegreeRadCheckBox.TabIndex = 2;
            this.twoFortyDegreeRadCheckBox.Text = "3 (240)";
            // 
            // oneTwentyDegreeRadCheckBox
            // 
            this.oneTwentyDegreeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneTwentyDegreeRadCheckBox.Location = new System.Drawing.Point(13, 74);
            this.oneTwentyDegreeRadCheckBox.Name = "oneTwentyDegreeRadCheckBox";
            this.oneTwentyDegreeRadCheckBox.Size = new System.Drawing.Size(56, 16);
            this.oneTwentyDegreeRadCheckBox.TabIndex = 1;
            this.oneTwentyDegreeRadCheckBox.Text = "2 (120)";
            // 
            // zeroDegreeRadCheckBox
            // 
            this.zeroDegreeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zeroDegreeRadCheckBox.Location = new System.Drawing.Point(13, 48);
            this.zeroDegreeRadCheckBox.Name = "zeroDegreeRadCheckBox";
            this.zeroDegreeRadCheckBox.Size = new System.Drawing.Size(43, 16);
            this.zeroDegreeRadCheckBox.TabIndex = 0;
            this.zeroDegreeRadCheckBox.Text = "1 (0)";
            // 
            // phaseShiftInformationRadLabel
            // 
            this.phaseShiftInformationRadLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.phaseShiftInformationRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseShiftInformationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.phaseShiftInformationRadLabel.Location = new System.Drawing.Point(12, 12);
            this.phaseShiftInformationRadLabel.Name = "phaseShiftInformationRadLabel";
            this.phaseShiftInformationRadLabel.Size = new System.Drawing.Size(148, 27);
            this.phaseShiftInformationRadLabel.TabIndex = 0;
            this.phaseShiftInformationRadLabel.Text = "<html>Phase shift between phase 1<br> and phase reference signal:</html>";
            this.phaseShiftInformationRadLabel.ThemeName = "Office2007Black";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cancelRadButton.Location = new System.Drawing.Point(141, 184);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(105, 24);
            this.cancelRadButton.TabIndex = 2;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // okayRadButton
            // 
            this.okayRadButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.okayRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okayRadButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.okayRadButton.Location = new System.Drawing.Point(12, 184);
            this.okayRadButton.Name = "okayRadButton";
            this.okayRadButton.Size = new System.Drawing.Size(105, 24);
            this.okayRadButton.TabIndex = 3;
            this.okayRadButton.Text = "OK";
            this.okayRadButton.ThemeName = "Office2007Black";
            this.okayRadButton.Click += new System.EventHandler(this.okayRadButton_Click);
            // 
            // phaseShiftRadSpinEditor
            // 
            this.phaseShiftRadSpinEditor.DecimalPlaces = 1;
            this.phaseShiftRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseShiftRadSpinEditor.Increment = new decimal(new int[] {
            75,
            0,
            0,
            65536});
            this.phaseShiftRadSpinEditor.Location = new System.Drawing.Point(193, 18);
            this.phaseShiftRadSpinEditor.Maximum = new decimal(new int[] {
            3525,
            0,
            0,
            65536});
            this.phaseShiftRadSpinEditor.Name = "phaseShiftRadSpinEditor";
            // 
            // 
            // 
            this.phaseShiftRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.phaseShiftRadSpinEditor.ShowBorder = true;
            this.phaseShiftRadSpinEditor.Size = new System.Drawing.Size(53, 19);
            this.phaseShiftRadSpinEditor.TabIndex = 4;
            this.phaseShiftRadSpinEditor.TabStop = false;
            this.phaseShiftRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // InitialPhaseSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(259, 222);
            this.Controls.Add(this.phaseShiftRadSpinEditor);
            this.Controls.Add(this.okayRadButton);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.phaseShiftInformationRadLabel);
            this.Controls.Add(this.showVoltageSinewaveRadGroupBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(250, 212);
            this.Name = "InitialPhaseSetting";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Common Phase Shift";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.InitialPhaseSetting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.showVoltageSinewaveRadGroupBox)).EndInit();
            this.showVoltageSinewaveRadGroupBox.ResumeLayout(false);
            this.showVoltageSinewaveRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveCColorRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveBColorRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeSineWaveAColorRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.twoFortyDegreeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oneTwentyDegreeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zeroDegreeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseShiftInformationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phaseShiftRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox showVoltageSinewaveRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox twoFortyDegreeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox oneTwentyDegreeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox zeroDegreeRadCheckBox;
        private Telerik.WinControls.UI.RadLabel phaseShiftInformationRadLabel;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton okayRadButton;
        private Telerik.WinControls.UI.RadSpinEditor phaseShiftRadSpinEditor;
        private Telerik.WinControls.UI.RadButton changeSineWaveCColorRadButton;
        private Telerik.WinControls.UI.RadButton changeSineWaveBColorRadButton;
        private Telerik.WinControls.UI.RadButton changeSineWaveAColorRadButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel sineWaveCColorPanel;
        private System.Windows.Forms.Panel sineWaveBColorPanel;
        private System.Windows.Forms.Panel sineWaveAColorPanel;
        private Telerik.WinControls.UI.RadLabel colorTextRadLabel;
        private Telerik.WinControls.UI.RadLabel phaseTextRadLabel;
    }
}

