﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using MonitorInterface;
using ConfigurationObjects;
using PasswordManagement;
using FormatConversion;
using DatabaseInterface;
using MonitorCommunication;
using MonitorUtilities;

namespace PDMonitorUtilities
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        //private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
        private static string noCurrentConfigurationDefinedText = "There is no current configuration defined.";
        //private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
        //private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
        //private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        private static string deleteAsQuestionText = "Delete?";
        private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
        //private static string cannotDeleteTemplateConfigurationWarningText = "You are not allowed to delete a template configuration from the database.";      
        private static string noConfigurationSelectedText = "No configuration selected.";
        //private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
        //private static string commandToReadDeviceErrorStateFailedText = "Command to read the device error state failed.";
        //private static string lostDeviceConnectionText = "Lost the connection to the device.";
        //private static string notConnectedToPDMWarningText = "You are not connected to a PDM.  Cannot configure using this interface.";
        private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
        //private static string failedToWriteTheConfigurationToTheDeviceText = "Failed to write the configuration to the device.";
        //private static string configurationWasReadFromTheDeviceText = "The configuration was read from the device.";
        //private static string configurationWasWrittenToTheDeviceText = "The configuration was written to the device.";
        private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        private static string saveDeviceConfigurationQuestionText = "Save device configuration?";

        private static string workingConfigurationSavedToDatabaseText = "The configuration was saved to the database";
        private static string workingConfigurationNotSavedToDatabaseText = "Error: the working configuration was not saved to the database";
        private static string configurationWasReadFromTheDatabaseText = "The configuration was successfully read from the database";

        private static string changesToWorkingConfigurationNotSavedOverwriteWarningText = "You have made changes to the configuration that have not been saved.  Discard those changes?";
        private static string changesToWorkingConfigurationNotSavedExitWarningText = "You have made changes to the configuration that have not been saved.  Exit anyway?";
        private static string discardChangesAsQuestionText = "Discard changes?";

        private static string configurationLoadCancelledText = "Configuration load cancelled";

        private static string failedToDeleteCurrentConfigurationText = "Failed to delete the current configuration from the database";
        private static string failedtoDeleteConfigurationText = "Failed to delete the configuration";

        private static string replaceExistingConfigurationsWithTheSameNameQuestionText = "Would you like to replace all existing configurations having the same description with this one?";

        private static string currentDeviceConfigurationAlreadyExistsOverwriteWarningText = "A current device configuration is already saved to the database: replace it with this one?";
        
        private static string areYouSureYouWantToDeleteCurrentConfigQuestionText = "Are you sure you want to delete the current configuration?\nA current configuration is necessary to complete all data readings for this PDM";

        private void SaveWorkingConfigurationToDatabase(MonitorInterfaceDB localDB)
        {
            try
            {
                string defaultConfigurationName = string.Empty;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.workingConfiguration != null)
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        if (PDM_MonitorConfiguration.monitor != null)
                        {
                            if (PDM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                            {
                                defaultConfigurationName = this.nameOfLastConfigurationLoadedFromDatabase;
                            }
                            using (ConfigurationDescription description = new ConfigurationDescription(PDM_MonitorConfiguration.CurrentConfigName, defaultConfigurationName))
                            {
                                description.ShowDialog();
                                description.Hide();

                                if (description.SaveConfiguration)
                                {
                                    if ((PDM_MonitorConfiguration.programType == ProgramType.TemplateEditor) && (NamedConfigurationAlreadySavedInDatabase(PDM_MonitorConfiguration.monitor, description.ConfigurationName, localDB)))
                                    {
                                        if (RadMessageBox.Show(this, replaceExistingConfigurationsWithTheSameNameQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            if (!DeleteNamedConfigurationFromDatabase(PDM_MonitorConfiguration.monitor, description.ConfigurationName, localDB))
                                            {
                                                RadMessageBox.Show(this, failedtoDeleteConfigurationText);
                                            }
                                        }
                                    }
                                    //WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                                    if (ConfigurationConversion.SavePDM_ConfigurationToDatabase(this.workingConfiguration, PDM_MonitorConfiguration.monitor.ID, DateTime.Now, description.ConfigurationName, localDB))
                                    {
                                       // RadMessageBox.Show(this, workingConfigurationSavedToDatabaseText);
                                        this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, workingConfigurationNotSavedToDatabaseText);
                                    }
                                    LoadAvailableConfigurations(localDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nPDM_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
                // }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Saves the configuration to the database as the current device configuration; only available for ProductName DRPD-15 
        /// </summary>
        /// <param name="localDB"></param>
        private void SaveWorkingConfigurationToDatabaseAsCurrentDeviceConfiguration(MonitorInterfaceDB localDB)
        {
            try
            {
                bool goAheadAndSaveTheConfiguration = true;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.workingConfiguration != null)
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        if (PDM_MonitorConfiguration.monitor != null)
                        {
                            if (NamedConfigurationAlreadySavedInDatabase(PDM_MonitorConfiguration.monitor, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                            {
                                if (RadMessageBox.Show(this, currentDeviceConfigurationAlreadyExistsOverwriteWarningText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    if (!DeleteNamedConfigurationFromDatabase(PDM_MonitorConfiguration.monitor, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                                    {
                                        RadMessageBox.Show(this, failedToDeleteCurrentConfigurationText);
                                    }
                                }
                                else
                                {
                                    goAheadAndSaveTheConfiguration = false;
                                }
                            }

                            if (goAheadAndSaveTheConfiguration)
                            {
                                if (ConfigurationConversion.SavePDM_ConfigurationToDatabase(this.workingConfiguration, PDM_MonitorConfiguration.monitor.ID, DateTime.Now, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                                {
                                    // RadMessageBox.Show(this, workingConfigurationSavedToDatabaseText);
                                    this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                                }
                                else
                                {
                                    RadMessageBox.Show(this, workingConfigurationNotSavedToDatabaseText);
                                }
                                LoadAvailableConfigurations(localDB);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.SaveWorkingConfigurationToDatabaseAsCurrentDeviceConfiguration(MonitorInterfaceDB)\nPDM_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.SaveWorkingConfigurationToDatabaseAsCurrentDeviceConfiguration(MonitorInterfaceDB)\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
                // }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SaveWorkingConfigurationToDatabaseAsCurrentDeviceConfiguration(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool SafeToLoadConfiguration()
        {
            bool safeToLoad = true;
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();
                if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                {
                    if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedOverwriteWarningText, discardChangesAsQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        safeToLoad = false;
                    }
                }                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SafeToLoadConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return safeToLoad;
        }

        private static bool NamedConfigurationAlreadySavedInDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool configurationIsPresent = false;
            try
            {
                List<PDM_Config_ConfigurationRoot> configRootList = null;
                if (PDM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        // this list will be, at most, two entries long
                        foreach (PDM_Config_ConfigurationRoot entry in configRootList)
                        {
                            if (entry.Description.Contains(configurationName))
                            {
                                configurationIsPresent = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.NamedConfigurationAlreadySavedInDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationIsPresent;
        }

        private static bool DeleteNamedConfigurationFromDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                List<PDM_Config_ConfigurationRoot> configRootList = null;
                if (PDM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        for (int i = 0; i < configRootList.Count; i++)
                        {
                            if (configRootList[i].Description.Contains(configurationName))
                            {
                                success = PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(configRootList[i].ID, localDB);
                                // break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DeleteNamedConfigurationFromDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


//        private void SaveDeviceConfigurationToDatabase()
//        {
//            try
//            {
//                if (this.configurationFromDevice != null)
//                {
//                    using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
//                    {
//                        SaveDeviceConfigurationToDatabase(this.configurationFromDevice, true, saveDB);
//                        LoadAvailableConfigurations(saveDB);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationLoadedFromDeviceText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public static bool SaveDeviceConfigurationToDatabase(Monitor monitor, PDM_Configuration configurationBeingSaved, bool interactive, MonitorInterfaceDB localDB, IWin32Window parentWindow)
        {
            bool success = false;
            try
            {
                Guid configToDeleteConfigurationRootID = Guid.Empty;
                bool saveConfiguration = false;
                if (configurationBeingSaved != null)
                {
                    if (interactive)
                    {
                        if (RadMessageBox.Show(parentWindow, configurationBeingSavedIsFromDeviceText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            saveConfiguration = true;
                        }
                    }
                    else
                    {
                        saveConfiguration = true;
                    }
                    if (saveConfiguration)
                    {
                        if (NamedConfigurationAlreadySavedInDatabase(monitor, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                        {
                            if (NamedConfigurationAlreadySavedInDatabase(monitor, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                                if (!DeleteNamedConfigurationFromDatabase(monitor, PDM_MonitorConfiguration.CurrentConfigName, localDB))
                                {
                                    if (interactive)
                                    {
                                        RadMessageBox.Show(parentWindow, failedToDeleteCurrentConfigurationText);
                                    }
                                    string errorMessage = "Error in PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to delete the existing Current Device Configuration from the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                }
                                else
                                {
                                    string errorMessage = "In PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nDeleted the existing Current Device Configuration from the database.";
                                    LogMessage.LogError(errorMessage);
                                }
                        }
                        if (ConfigurationConversion.SavePDM_ConfigurationToDatabase(configurationBeingSaved, monitor.ID, DateTime.Now, PDM_MonitorConfiguration.currentConfigName, localDB))
                        {
                            success = true;
                            //string errorMessage = "In PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nSaved a new Current Device Configuration to the database.";
                            //LogMessage.LogError(errorMessage);
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationSavedToDatabaseText);
                            }
                        }
                        else
                        {
                            //string errorMessage = "Error in PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to save a new Current Device Configuration to the database.";
                            //LogMessage.LogError(errorMessage);
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationNotSavedToDatabaseText);
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nInput PDM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Configuration LoadConfigurationFromDatabase(Guid configurationRootID, string dbConnectionSTring)
        {
            PDM_Configuration configurationFromDB = null;
            try
            {
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionSTring))
                {
                    configurationFromDB = ConfigurationConversion.GetPDM_ConfigurationFromDatabase(configurationRootID, localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationFromDB;
        }

        private void LoadSelectedConfigurationFromDatabase()
        {
            try
            {
                PDM_Configuration configFromDB = null;
                PDM_Config_ConfigurationRoot configurationRoot;

                if (this.availableConfigurationsSelectedIndex > 0)
                {
                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                    {
                        configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                        if (configurationRoot.ID.CompareTo(Guid.Empty) != 0)
                        {
                            if (SafeToLoadConfiguration())
                            {
                                configFromDB = LoadConfigurationFromDatabase(configurationRoot.ID, this.dbConnectionString);
                                if (configFromDB != null)
                                {
                                    if (configFromDB.AllMembersAreNonNull())
                                    {
                                        this.workingConfiguration = configFromDB;
                                        this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                                        AddDataToAllInterfaceObjects(this.workingConfiguration);

                                        if (configurationRoot.Description.Trim().CompareTo(PDM_MonitorConfiguration.currentConfigName) != 0)
                                        {
                                            nameOfLastConfigurationLoadedFromDatabase = configurationRoot.Description;
                                        }

                                       // RadMessageBox.Show(this, configurationWasReadFromTheDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noConfigurationSelectedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void LoadSelectedConfigurationFromDatabase()
//        {
//            try
//            {
//                if (this.availableConfigurationsSelectedIndex > 0)
//                {
//                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
//                    {
//                        Guid configurationRootID = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1].ID;
//                        LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(configurationRootID);
//                        EnableFromDatabaseRadRadioButtons();
//                        SetFromDatabaseRadRadioButtonState();
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationSelectedText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(Guid configurationRootID)
//        {
//            try
//            {
//                configurationFromDatabase = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);

//                if (configurationFromDatabase != null)
//                {
//                    if (configurationFromDatabase.AllMembersAreNonNull())
//                    {
//                        EnableFromDatabaseRadRadioButtons();
//                        AddDataToAllInterfaceObjects(configurationFromDatabase);
//                    }
//                    else
//                    {
//                        RadMessageBox.Show(this, errorInConfigurationLoadedFromDatabaseText);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, configurationCouldNotBeLoadedFromDatabaseText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(Guid)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

    

        public static Guid GetConfigurationRootIDForCurrentDeviceConfiguration(Guid monitorID, MonitorInterfaceDB configDB)
        {
            Guid configurationRootID = Guid.Empty;
            try
            {
                List<PDM_Config_ConfigurationRoot> availableConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitorID, configDB);
                foreach (PDM_Config_ConfigurationRoot entry in availableConfigurations)
                {
                    if (entry.Description.Trim().CompareTo(PDM_MonitorConfiguration.currentConfigName) == 0)
                    {
                        configurationRootID = entry.ID;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetConfigurationRootIDForCurrentDeviceConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootID;
        }

        public static PDM_Configuration LoadCurrentDeviceConfigurationFromDatabase(Guid monitorID, MonitorInterfaceDB configDB)
        {
            PDM_Configuration currentDeviceConfiguration = null;
            try
            {
                Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(monitorID, configDB);
                currentDeviceConfiguration = ConfigurationConversion.GetPDM_ConfigurationFromDatabase(configurationRootID, configDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return currentDeviceConfiguration;
        }

        private void DeleteConfigurationFromDatabase()
        {
            try
            {
                bool goAheadAndDeleteConfiguration = false;
                PDM_Config_ConfigurationRoot configurationRoot;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.availableConfigurationsSelectedIndex > 0)
                    {
                        if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                        {
                            configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                            if (configurationRoot.Description.Trim().CompareTo(PDM_MonitorConfiguration.currentConfigName) != 0)
                            {
                                if (RadMessageBox.Show(this, configurationDeleteFromDatabaseWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    goAheadAndDeleteConfiguration = true;
                                }
                            }
                            else
                            {
                                if (PDM_MonitorConfiguration.programType == ProgramType.PD15)
                                {
                                    if (RadMessageBox.Show(this, areYouSureYouWantToDeleteCurrentConfigQuestionText, deleteAsQuestionText, MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        goAheadAndDeleteConfiguration = true;
                                    }
                                }
                                else if (PDM_MonitorConfiguration.programType == ProgramType.IHM2)
                                {
                                    RadMessageBox.Show(this, cannotDeleteCurrentConfigurationWarningText);
                                }
                                else
                                {
                                    string errorMessage = "Error in PDM_MonitorConfiguration.DeleteConfigurationFromDatabase()\nUnknown program type.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            if (goAheadAndDeleteConfiguration)
                            {
                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                {
                                    PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, localDB);
                                    LoadAvailableConfigurations(localDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.DeleteConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noConfigurationSelectedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DeleteConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                long deviceError = 0;
                int deviceType;
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                PDM_Configuration configFromDevice = null;
                if (SafeToLoadConfiguration())
                {
                    if (!downloadWasInProgress)
                    {
                        if (PDM_MonitorConfiguration.monitor != null)
                        {
                            modBusAddressAsString = monitor.ModbusAddress.Trim();
                            if (modBusAddressAsString.Length > 0)
                            {
                                modBusAddress = Int32.Parse(modBusAddressAsString);

                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                {
                                    errorCode = MonitorConnection.OpenMonitorConnection(PDM_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                }
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    DeviceCommunication.EnableDataDownload();
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                    if (deviceType > 0)
                                    {
                                        // deviceType = DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, this.readDelayInMicroseconds);
                                        if (deviceType == 505)
                                        {
                                            deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceError > -1)
                                            {
                                                Application.DoEvents();
                                                configFromDevice = LoadConfigurationFromDevice(modBusAddress, this.readDelayInMicroseconds);
                                                if (configFromDevice != null)
                                                {
                                                    if (configFromDevice.AllMembersAreNonNull())
                                                    {
                                                        errorCode = ErrorCode.ConfigurationDownloadSucceeded;

                                                        this.workingConfiguration = configFromDevice;
                                                        this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);

                                                        // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                                        // Testing code

                                                        //this.previousConfigurationAsArray = this.workingConfigurationAsArray;
                                                        //this.workingConfigurationAsArray = registerData;

                                                        //CheckLatestConfigurationAgainstPreviousConfiguration(registerData);

                                                        // more testing code, show some values

                                                        // ShowSomeValuesThatArePissingMeOff(registerData);

                                                        // AddDataToAllTransientInterfaceObjects(configurationFromDevice);
                                                        //EnableCopyDeviceConfigurationRadButtons();
                                                        //EnableFromDeviceRadRadioButtons();
                                                        //SetFromDeviceRadRadioButtonState();

                                                        AddDataToAllInterfaceObjects(this.workingConfiguration);

                                                        /// test code
                                                        //Int16[] registerValues = configurationFromDevice.GetShortValuesFromAllContributors();
                                                        //Main_Configuration testConfiguration = new Main_Configuration(registerValues);
                                                        //if (!configurationFromDevice.ConfigurationIsTheSame(testConfiguration))
                                                        //{
                                                        //    MessageBox.Show("configuration mismatch from a copy");
                                                        //}
                                                        //else
                                                        //{
                                                        //    MessageBox.Show("configuration matches from a copy");
                                                        //}
                                                        /// end test code

                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(PDM_MonitorConfiguration.monitor.ID, localDB);
                                                            if (configurationRootID.CompareTo(Guid.Empty) != 0)
                                                            {
                                                                PDM_Configuration deviceConfigFromDb = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);
                                                                if (!deviceConfigFromDb.ConfigurationIsTheSame(this.workingConfiguration))
                                                                {
                                                                    RadMessageBox.Show(this, deviceConfigurationDoesNotMatchDatabaseConfigurationText);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (RadMessageBox.Show(this, deviceCommunicationNotSavedInDatabaseYetText, saveDeviceConfigurationQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                {
                                                                    SaveDeviceConfigurationToDatabase(PDM_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                                    LoadAvailableConfigurations(localDB);
                                                                }
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationDownloadFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToPDM));
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                    }
                                }
                                else
                                {
                                    if (errorCode == ErrorCode.SerialPortNotSpecified)
                                    {
                                        RadMessageBox.Show(this, serialPortNotSetWarningText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.LoadConfigurationFromDevice()\nPDM_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, configurationLoadCancelledText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                MonitorConnection.CloseConnection();
            }
        }

        public PDM_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        {
            PDM_Configuration configuration = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                Int16[] registerValues;
                int offset = 0;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int numberOfInitializationBytes = 1100;
                bool receivedAllData = false;

                double firmwareVersion = 0;
                int cpldVersion = 0;
                int iterations = 0;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                DisableAllControls();
                SetProgressBarsToDownloadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 2);

                allByteValues = new Byte[2000];
                currentByteValues = new Byte[numberOfInitializationBytes];

                receivedAllData = false;
                while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled())
                {
                    currentByteValues = InteractiveDeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    if (currentByteValues != null)
                    {
                        bytesReturned = currentByteValues.Length;
                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                        bytesSaved += bytesReturned;
                        offset += bytesReturned;
                        if (bytesReturned < 1000)
                        {
                            receivedAllData = true;
                        }
                    }
                    else
                    {
                        receivedAllData = true;
                        allByteValues = null;
                        break;
                    }
                    iterations++;
                    SetProgressBarProgress(iterations, 2);
                }
                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                if (registerValues != null)
                {
                    firmwareVersion = registerValues[0] / 100.0;
                }

                registerValues = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 100, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                if (registerValues != null)
                {
                    cpldVersion = registerValues[0];
                }

                if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled())
                {
                    byteValuesToCreateConfiguration = new Byte[bytesSaved];
                    Array.Copy(allByteValues, byteValuesToCreateConfiguration, bytesSaved);
                    configuration = new PDM_Configuration(byteValuesToCreateConfiguration, firmwareVersion);
                    if ((configuration != null) && (configuration.AllMembersAreNonNull()))
                    {
                        configuration.setupInfo.Reserved_59 = cpldVersion;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.PDM_LoadConfigurationFromDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                MonitorConnection.CloseConnection();
            }
            return configuration;
        }

        private void ProgramDevice()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                int deviceType;
                int readDelayInMicroseconds = 1000;
                Byte[] byteData = null;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                Monitor monitorFromDB;
                bool deviceWasProgrammedSuccessfully = true;

                int offset = 0;
                Byte[] bytesBeingSentToDevice;
                int numberOfBytesPerFullSend = 200;
                int numberOfBytesBeingSent;
                int totalNumberOfBytesToWrite;
                int numberOfBytesSentSoFar = 0;

                int iterations = 0;

                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                DisableAllControls();
                SetProgressBarsToUploadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 8);

                if (!downloadWasInProgress)
                {
                    if (this.workingConfiguration != null)
                    {
                        if (!ErrorIsPresentInSomeTabObject())
                        {
                            WriteAllInterfaceDataToWorkingConfiguration();
                            if (PDM_MonitorConfiguration.monitor != null)
                            {
                                modBusAddressAsString = monitor.ModbusAddress.Trim();
                                if (modBusAddressAsString.Length > 0)
                                {
                                    modBusAddress = Int32.Parse(modBusAddressAsString);

                                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                    {
                                        errorCode = MonitorConnection.OpenMonitorConnection(PDM_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                    }
                                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                    {
                                        DeviceCommunication.EnableDataDownload();
                                        deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                        if (deviceType > 0)
                                        {
                                            if (deviceType == 505)
                                            {
                                                Application.DoEvents();
                                                byteData = this.workingConfiguration.ConvertConfigurationToByteArray();
                                                totalNumberOfBytesToWrite = byteData.Length;
                                                bytesBeingSentToDevice = new byte[numberOfBytesPerFullSend];

                                                while ((numberOfBytesSentSoFar < totalNumberOfBytesToWrite) && DeviceCommunication.DownloadIsEnabled() && deviceWasProgrammedSuccessfully)
                                                {
                                                    if ((totalNumberOfBytesToWrite - numberOfBytesSentSoFar) >= numberOfBytesPerFullSend)
                                                    {
                                                        numberOfBytesBeingSent = numberOfBytesPerFullSend;
                                                    }
                                                    else
                                                    {
                                                        numberOfBytesBeingSent = totalNumberOfBytesToWrite - numberOfBytesSentSoFar;
                                                    }
                                                    Array.Copy(byteData, numberOfBytesSentSoFar, bytesBeingSentToDevice, 0, numberOfBytesBeingSent);

                                                    deviceWasProgrammedSuccessfully = InteractiveDeviceCommunication.PDM_SetDeviceSetup(modBusAddress, offset, bytesBeingSentToDevice, numberOfBytesBeingSent, readDelayInMicroseconds,
                                                                                                                                        MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                    offset += numberOfBytesBeingSent;
                                                    numberOfBytesSentSoFar += numberOfBytesBeingSent;
                                                    Application.DoEvents();

                                                    iterations++;
                                                    SetProgressBarProgress(iterations, 8);
                                                }

                                                if (deviceWasProgrammedSuccessfully)
                                                {
                                                    deviceWasProgrammedSuccessfully = InteractiveDeviceCommunication.PDM_ResetSetupParameters(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt,
                                                                                                                                              MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                }

                                                if (deviceWasProgrammedSuccessfully)
                                                {
                                                    errorCode = ErrorCode.ConfigurationWriteSucceeded;

                                                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                    {
                                                        if (modBusAddress.ToString().CompareTo(PDM_MonitorConfiguration.monitor.ModbusAddress) != 0)
                                                        {
                                                            monitorFromDB = General_DatabaseMethods.GetOneMonitor(PDM_MonitorConfiguration.monitor.ID, localDB);
                                                            if (monitorFromDB != null)
                                                            {
                                                                monitorFromDB.ModbusAddress = modBusAddress.ToString();
                                                                localDB.SubmitChanges();
                                                            }
                                                            PDM_MonitorConfiguration.monitor.ModbusAddress = modBusAddress.ToString();
                                                        }
                                                        // now, delete the old device config from the DB, and save the new one.
                                                        SaveDeviceConfigurationToDatabase(PDM_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                        LoadAvailableConfigurations(localDB);
                                                    }
                                                    /// since the configuraiton has been "saved" by using it to program the device, we update the unedited configuration
                                                    /// to match what we just wrote to the device
                                                    this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                                                    // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteSucceeded));
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToPDM));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                        }
                                    }
                                    else
                                    {
                                        if (errorCode == ErrorCode.SerialPortNotSpecified)
                                        {
                                            RadMessageBox.Show(this, serialPortNotSetWarningText);
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                        }
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_MonitorConfiguration.ProgramDevice()\nPDM_MonitorConfiguration.monitor was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noCurrentConfigurationDefinedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ProgramDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                MonitorConnection.CloseConnection();
            }
        }
    } 
}
