using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Data.Linq;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;
using DataObjects;
using DatabaseInterface;

namespace PDMonitorUtilities
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static Monitor monitor;
        private bool downloadWasInProgress;

        private bool configurationError;

        private String[] pdiCalculationLimits; 

        private PDM_Configuration workingConfiguration;
        private PDM_Configuration uneditedWorkingConfiguration;

        private List<PDM_Config_ConfigurationRoot> availableConfigurations;
        private List<PDM_Config_ConfigurationRoot> templateConfigurations;

        private int availableConfigurationsSelectedIndex = -1;
        private int templateConfigurationsSelectedIndex = -1;

        private int readDelayInMicroseconds;

        private string dbConnectionString;

        private string serialPort;
        private int baudRate;
    
        private static string currentConfigName = "Current Device Configuration";
        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }      

        //private ConfigurationDisplayed configurationBeingDisplayed = ConfigurationDisplayed.None;

    
        private static string htmlPrefix = "<html>";
        private static string htmlSuffix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string emptyCellErrorMessage = "There is a value missing from the grid.";
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        //private static string currentDeviceConfigurationNotInDatabaseText = "Could not find the current device configuration, displaying most recent configuration saved.";
        //private static string noDeviceConfigurationInDatabaseText = "Could not find any device configurations in the database.";
        //private static string changesMadeToCurrentConfigurationNotSavedWarningText = "You have made changes to the working copy that have not been saved.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";
        //private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
        //private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";

        //private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";

        // display objects text strings
        private static string pdmMonitorConfigurationInterfaceTitleText = "Partial Discharge Monitor Configuration";
       
        // common display object texts
        private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Configuration<br>to<br>Database</html>";
        //private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device</html>";
        private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
        private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
        //private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        //private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        //private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        //private static string currentRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        //private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        //private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";
        //private static string initializeWorkingConfigurationRadButtonText = "Initialize Working Configuration";


        // individual display object text strings, one for each display object
        private static string aSensorPhaseText = "1";
        private static string bSensorPhaseText = "2";
        private static string cSensorPhaseText = "3";
        private static string abSensorPhaseText = "1-2";
        private static string bcSensorPhaseText = "2-3";
        private static string caSensorPhaseText = "3-1";

        private static string commonSettingsRadPageViewPageText = "Common Settings";
        private static string monitoringRadGroupBoxText = "Monitoring";
        private static string enableMonitoringRadRadioButtonText = "Enable";
        private static string disableMonitoringRadRadioButtonText = "Disable";

        private static string ratedVoltageRadGroupBoxText = "Rated Voltage";
        private static string channels1to3RadLabelText = "Channels Ch.A-Ch.C";
        private static string channels4to6RadLabelText = "Channels Ch.a-Ch.c";
        private static string channels7to12RadLabelText = "Channels PD1-PD6";
        private static string channels13to15RadLabelText = "Channels N1-N3";
        private static string kV1RadLabelText = "kV";
        private static string kV2RadLabelText = "kV";
        private static string kV3RadLabelText = "kV";
        private static string kV4RadLabelText = "kV";
        private static string connectionSettingsRadGroupBoxText = "Connection Settings";
        private static string modbusAddressRadLabelText = "ModBus Address";
        private static string baudRateRadLabelText = "Baud Rate";
        private static string saveRadGroupBoxText = "Save";
        private static string saveModeRadLabelText = "Save Mode";
        private static string normalSaveModeRadRadioButtonText = "Normal";
        private static string testSaveModeRadRadioButtonText = "Test";
        private static string savePRPDDRadLabelText = "Save PRPDD";
        private static string measurementsRadLabelText = "measurements on";
        private static string dayRadLabelText = "day";
        private static string measurementScheduleRadGroupBoxText = "Measurement Schedule";
        private static string stepMeasurementScheduleRadRadioButtonText = "Every";
        private static string byScheduledMeasurementScheduleRadRadioButtonText = "By Schedule";
        private static string hourRadLabelText = "Hour";
        private static string minuteRadLabelText = "Minute";
     
        private static string pdSettingsRadPageViewPageText = "PD Settings";
        private static string synchronizationRadLabelText = "Synchronization";
        private static string hzRadLabelText = "Hz";
        private static string rereadOnAlarmRadCheckBoxText = "Reread on alarm";
        private static string cyclesPerAcquisitionRadLabelText = "Cycles per Acquisition";
        private static string commonPhaseShiftRadLabelText = "Common Phase Shift";
        private static string degreesRadLabelText = "degrees";
        //private static string neutralRadLabelText = "Neutral";
        //private static string commonNeutralRadRadioButtonText = "Common";
        //private static string insulatedNeutralRadRadioButtonText = "Insulated";
      
        private static string alarmSettingsRadPageViewPageText = "Alarm Settings";
        private static string relayModeRadGroupBoxText = "Relay Mode";
        private static string onRelayModeRadRadioButtonText = "On";
        private static string offRelayModeRadRadioButtonText = "Off";
        private static string alarmEnableRadGroupBoxText = "Enable Alarms";
        private static string alarmOnPDILevelRadCheckBoxText = "Alarm on PDI Level";
        private static string alarmOnPDITrendRadCheckBoxText = "Alarm on PDI Trend";
        private static string alarmOnPDIChangeRadCheckBoxText = "Alarm on PDI Change";
        private static string alarmOnQmaxLevelRadCheckBoxText = "Alarm on Qmax Level";
        private static string alarmOnQmaxTrendRadCheckBoxText = "Alarm on Qmax Trend";
        private static string alarmOnQmaxChangeRadCheckBoxText = "Alarm on Qmax Change";
        private static string commonAlarmSettingsRadGroupBoxText = "Common Alarm Settings";
        private static string trendAlarmsTitleRadLabelText = "Trend Alarms";
        private static string trendWarningRadLabelText = "Trend Warning";
        private static string trendAlarmRadLabelText = "Trend Alarm";
        private static string calcTrendOnRadLabelText = "Calc trend on";
        private static string timesPerYearTrendWarningRadLabelText = "times/year";
        private static string timesPerYearTrendAlarmRadLabelText = "times/year";
        private static string weeksRadLabelText = "weeks";
        private static string alarmOnChangeRadLabelText = "Alarm on Change";
        private static string changeWarningRadLabelText = "Change Warning";
        private static string changeAlarmRadLabelText = "Change Alarm";
        private static string percentChangeWarningRadLabelText = "%";
        private static string percentChangeAlarmRadLabelText = "%";

        private static string showAdvancedSettingsRadButtonText = "Show Advanced Settings";
        private static string hideAdvancedSettingsRadButtonText = "Hide Advanced Settings";
        private static string advancedSettingsWarningText = "Do not change advanced settings unless instructed";

        private static string deviceInteractionGroupBoxText = "Device Interaction";
        private static string databaseInteractionGroupBoxText = "Database Interaction";

        private static string noTemplateConfigurationsAvailable = "No template configurations are available";

        private static string couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = "Could not find the current device configuration, displaying an empty configuration.";

        private static string templateConfigurationsRadGroupBoxText = "Template Configurations";
        private static string copySelectedConfigurationRadButtonText = "<html>Copy Selected<br>Configuration to<br>Database</html>";

        private static string availableConfigurationsHeaderText = "Date Added                        Description";
        private static string noConfigurationsSavedInDbText = "None Saved in DB";

        private static string firmwareVersionTextRadLabelText = "Firmware Version";
        private static string cpldVersionTextRadLabelText = "CPLD Version";

        private bool showAdvancedSettings = false;

        private ProgramBrand programBrand;
        private static ProgramType programType;

        private string nameOfLastConfigurationLoadedFromDatabase = string.Empty;

        private RadMenuItem alarmSettingsCopyAllRowRadMenuItem;
        private RadMenuItem channelConfigurationCopyAllRadMenuItem;
        private RadMenuItem channelConfigurationCopyEnabledRadMenuItem;

        private string templateDbConnectionString;

        private bool oppositePolarityFeatureExists = false;

        // private MonitorInterfaceDB internalDbConnection;

        private int athenaTemplateConfigurationGroupBoxLocationX = 3;
        private int athenaTemplateConfigurationGroupBoxLocationY = 295;
        private int athenaTemplateConfigurationGroupBoxWidth = 537;
        private int athenaTemplateConfigurationGroupBoxHeight = 65;

        private int athenaConfigurationDisplayGroupBoxLocationX = 156;
        private int athenaConfigurationDisplayGroupBoxLocationY = 365;
        private int athenaConfigurationDisplayGroupBoxWidth = 529;
        private int athenaConfigurationDisplayGroupBoxHeight = 192;

        private int athenaDatabaseInteractionGroupBoxLocationX = 6;
        private int athenaDatabaseInteractionGroupBoxLocationY = 365;
        private int athenaDatabaseInteractionGroupBoxWidth= 160;
        private int athenaDatabaseInteractionGroupBoxHeight = 192;

        private int athenaDeviceInteractionGroupBoxLocationX= 697;
        private int athenaDeviceInteractionGroupBoxLocationY = 365;
        private int athenaDeviceInteractionGroupBoxWidth = 151;
        private int athenaDeviceInteractionGroupBoxHeight = 192;

        private int drpdTemplateConfigurationGroupBoxLocationX = 3;
        private int drpdTemplateConfigurationGroupBoxLocationY = 295;
        private int drpdTemplateConfigurationGroupBoxWidth = 559;
        private int drpdTemplateConfigurationGroupBoxHeight = 65;

        private int drpdConfigurationDisplayGroupBoxLocationX = 3;
        private int drpdConfigurationDisplayGroupBoxLocationY = 366;
        private int drpdConfigurationDisplayGroupBoxWidth = 559;
        private int drpdConfigurationDisplayGroupBoxHeight = 192;

        private int drpdDatabaseInteractionGroupBoxLocationX = 568;
        private int drpdDatabaseInteractionGroupBoxLocationY = 365;
        private int drpdDatabaseInteractionGroupBoxWidth = 280;
        private int drpdDatabaseInteractionGroupBoxHeight = 192;

        private int templatesConfigurationDisplayGroupBoxLocationX = 3;
        private int templatesConfigurationDisplayGroupBoxLocationY = 366;
        private int templatesConfigurationDisplayGroupBoxWidth = 690;
        private int templatesConfigurationDisplayGroupBoxHeight = 192;

        private int templatesDatabaseInteractionGroupBoxLocationX = 697;
        private int templatesDatabaseInteractionGroupBoxLocationY = 365;
        private int templatesDatabaseInteractionGroupBoxWidth = 280;
        private int templatesDatabaseInteractionGroupBoxHeight = 192;
      
        public PDM_MonitorConfiguration()
        {
            InitializeComponent();
        }

        public PDM_MonitorConfiguration(ProgramBrand inputProgramBrand, ProgramType inputProgramType, Monitor inputMonitor, string inputSerialPort, int inputBaudRate, bool inputdownloadWasInProgress, string inputDbConnectionString, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();
                this.programBrand = inputProgramBrand;
                PDM_MonitorConfiguration.programType = inputProgramType;
                PDM_MonitorConfiguration.monitor = inputMonitor;
                this.serialPort = inputSerialPort;
                this.baudRate = inputBaudRate;
                this.downloadWasInProgress = inputdownloadWasInProgress;
                this.dbConnectionString = inputDbConnectionString;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                AssignStringValuesToInterfaceObjects();
                this.StartPosition = FormStartPosition.CenterParent;
                // internalDbConnection = new MonitorInterfaceDB(this.dbConnectionString);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.PDM_MonitorConfiguration(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PDM_MonitorConfiguration_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.UseWaitCursor = true;
            this.UseWaitCursor = true;

            //DisableCurrentRadRadioButton();
            //DisableFromDatabaseRadRadioButtons();
            //DisableFromDeviceRadRadioButtons();

            DisableProgressBars();

            IntialializePDICalculationLimitValuesList();

            //InitializeChannelConfigurationContextMenu();
            //InitializeAlarmSettingsContextMenu();

            //SetupChannelConfigurationContextMenu();
            //SetupAlarmSettingsContextMenu();

            InitializeAllGridViews();

            // disable program device buttons
            programDeviceAlarmSettingsTabRadButton.Enabled = false;
            programDeviceCommonSettingsTabRadButton.Enabled = false;
            programDevicePDSettingsTabRadButton.Enabled = false;

            pdiCalculationLimits = new String[] { "6894", "5351", "4154", "3225", "2503", "1943", "1508", "1171", "909", "705", "548", "425", "330", 
                   "256", "199", "154", "120", "93", "72", "56", "43", "34", "26", "20", "16", "12", "10", "7", "6", "4", "3", "0" };

            using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
            {
                LoadAvailableConfigurations(localDB);
                if (PDM_MonitorConfiguration.monitor != null)
                {
                    this.workingConfiguration = LoadCurrentDeviceConfigurationFromDatabase(PDM_MonitorConfiguration.monitor.ID, localDB);
                    if (this.workingConfiguration != null)
                    {
                        this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                        //EnableFromDatabaseRadRadioButtons();
                        //SetFromDatabaseRadRadioButtonState();
                        AddDataToAllInterfaceObjects(this.workingConfiguration);
                    }
                    else
                    {
                        if (programType != ProgramType.TemplateEditor)
                        {
                            RadMessageBox.Show(this, couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText);
                        }
                        this.workingConfiguration = new PDM_Configuration();
                        this.workingConfiguration.InitializeConfigurationToZeroes();
                        this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
                        AddDataToAllInterfaceObjects(this.workingConfiguration);
                    }
                }
            }

            if (PDM_MonitorConfiguration.programType != ProgramType.TemplateEditor)
            {
                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                {
                    LoadTemplateConfigurations(templateDB);
                }
            }
         
            baudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            synchronizationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

            this.configurationRadPageView.SelectedPage = commonSettingsRadPageViewPage;

            this.UseWaitCursor = false;
            Application.UseWaitCursor = false;
            Cursor.Current = Cursors.Default;

            ArrangeInterfaceBasedOnProgramType();
        }

        private void ArrangeInterfaceBasedOnProgramType()
        {
            Point location;
            Size size;
/*
            if (PDM_MonitorConfiguration.programType == ProgramType.IHM2)
            {
                this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Visible = false;
                this.saveAsCurrentConfigurationPDSettingsTabRadButton.Visible = false;
                this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Visible = false;

                size = new Size(athenaTemplateConfigurationGroupBoxWidth, athenaTemplateConfigurationGroupBoxHeight);
                this.templateConfigurationsCommonSettingsTabRadGroupBox.Size = size;
                this.templateConfigurationsPDSettingsTabRadGroupBox.Size = size;
                this.templateConfigurationsAlarmSettingsTabRadGroupBox.Size = size;

                size = new Size(athenaConfigurationDisplayGroupBoxWidth, athenaConfigurationDisplayGroupBoxHeight);
                this.availableConfigurationsCommonSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsPDSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsAlarmSettingsTabRadGroupBox.Size = size;

                location = new Point(athenaDatabaseInteractionGroupBoxLocationX, athenaDatabaseInteractionGroupBoxLocationY);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionPDSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Location = location;

                size = new Size(athenaDatabaseInteractionGroupBoxWidth, athenaDatabaseInteractionGroupBoxHeight);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionPDSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Size = size;

                location = new Point(athenaDeviceInteractionGroupBoxLocationX, athenaDeviceInteractionGroupBoxLocationY);
                this.deviceInteractionCommonSettingsTabRadGroupBox.Location = location;
                this.deviceInteractionPDSettingsTabRadGroupBox.Location = location;
                this.deviceInteractionAlarmSettingsTabRadGroupBox.Location = location;

                size = new Size(athenaDeviceInteractionGroupBoxWidth, athenaDeviceInteractionGroupBoxHeight);
                this.deviceInteractionCommonSettingsTabRadGroupBox.Size = size;
                this.deviceInteractionPDSettingsTabRadGroupBox.Size = size;
                this.deviceInteractionAlarmSettingsTabRadGroupBox.Size = size;
            }
            else if (programType == ProgramType.PD15)
            {
                this.connectionSettingsRadGroupBox.Visible = false;
                this.saveRadGroupBox.Visible = false;
                this.measurementScheduleRadGroupBox.Visible = false;

                this.deviceInteractionCommonSettingsTabRadGroupBox.Visible = false;
                this.deviceInteractionPDSettingsTabRadGroupBox.Visible = false;
                this.deviceInteractionAlarmSettingsTabRadGroupBox.Visible = false;

                size = new Size(drpdTemplateConfigurationGroupBoxWidth, drpdTemplateConfigurationGroupBoxHeight);
                this.templateConfigurationsCommonSettingsTabRadGroupBox.Size = size;
                this.templateConfigurationsPDSettingsTabRadGroupBox.Size = size;
                this.templateConfigurationsAlarmSettingsTabRadGroupBox.Size = size;

                size = new Size(drpdConfigurationDisplayGroupBoxWidth, drpdConfigurationDisplayGroupBoxHeight);
                this.availableConfigurationsCommonSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsPDSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsAlarmSettingsTabRadGroupBox.Size = size;

                location = new Point(drpdDatabaseInteractionGroupBoxLocationX, drpdDatabaseInteractionGroupBoxLocationY);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionPDSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Location = location;

                size = new Size(drpdDatabaseInteractionGroupBoxWidth, drpdDatabaseInteractionGroupBoxHeight);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionPDSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Size = size;
            }
            else if (programType == ProgramType.TemplateEditor)
            {
                this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Visible = false;
                this.saveAsCurrentConfigurationPDSettingsTabRadButton.Visible = false;
                this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Visible = false;

                this.templateConfigurationsCommonSettingsTabRadGroupBox.Visible = false;
                this.templateConfigurationsPDSettingsTabRadGroupBox.Visible = false;
                this.templateConfigurationsAlarmSettingsTabRadGroupBox.Visible = false;

                this.deviceInteractionCommonSettingsTabRadGroupBox.Visible = false;
                this.deviceInteractionPDSettingsTabRadGroupBox.Visible = false;
                this.deviceInteractionAlarmSettingsTabRadGroupBox.Visible = false;

                size = new Size(templatesConfigurationDisplayGroupBoxWidth, templatesConfigurationDisplayGroupBoxHeight);
                this.availableConfigurationsCommonSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsPDSettingsTabRadGroupBox.Size = size;
                this.availableConfigurationsAlarmSettingsTabRadGroupBox.Size = size;

                location = new Point(templatesDatabaseInteractionGroupBoxLocationX, templatesDatabaseInteractionGroupBoxLocationY);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionPDSettingsTabRadGroupBox.Location = location;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Location = location;

                size = new Size(templatesDatabaseInteractionGroupBoxWidth, templatesDatabaseInteractionGroupBoxHeight);
                this.databaseInteractionCommonSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionPDSettingsTabRadGroupBox.Size = size;
                this.databaseInteractionAlarmSettingsTabRadGroupBox.Size = size;
            }

            */
        }


        private void InitializeAllGridViews()
        {
            try
            {
                InitializeChannelConfigurationRadGridView();
                InitializeAlarmSettingsRadGridView();
                InitializeMeasurementSettingsRadGridView();

                AddEmptyRowsToMeasurementSettingsGridView();
                AddEmptyRowsToChannelConfigurationGridView();
                AddEmptyRowsToAlarmSettingsRadGridView();

                HideSelectedPDSettingsColumns();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeAllGridViews()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = pdmMonitorConfigurationInterfaceTitleText;

                commonSettingsRadPageViewPage.Text = commonSettingsRadPageViewPageText;
                monitoringRadGroupBox.Text = monitoringRadGroupBoxText;
                monitoringRadGroupBox.Text = monitoringRadGroupBoxText;
                enableMonitoringRadRadioButton.Text = enableMonitoringRadRadioButtonText;
                disableMonitoringRadRadioButton.Text = disableMonitoringRadRadioButtonText;

                ratedVoltageRadGroupBox.Text = ratedVoltageRadGroupBoxText;
                channels1to3RadLabel.Text = channels1to3RadLabelText;
                channels4to6RadLabel.Text = channels4to6RadLabelText;
                channels7to12RadLabel.Text = channels7to12RadLabelText;
                channels13to15RadLabel.Text = channels13to15RadLabelText;
                kV1RadLabel.Text = kV1RadLabelText;
                kV2RadLabel.Text = kV2RadLabelText;
                kV3RadLabel.Text = kV3RadLabelText;
                kV4RadLabel.Text = kV4RadLabelText;
                connectionSettingsRadGroupBox.Text = connectionSettingsRadGroupBoxText;
                modbusAddressRadLabel.Text = modbusAddressRadLabelText;
                baudRateRadLabel.Text = baudRateRadLabelText;
                saveRadGroupBox.Text = saveRadGroupBoxText;
                saveModeRadLabel.Text = saveModeRadLabelText;
                normalSaveModeRadRadioButton.Text = normalSaveModeRadRadioButtonText;
                testSaveModeRadRadioButton.Text = testSaveModeRadRadioButtonText;
                savePRPDDRadLabel.Text = savePRPDDRadLabelText;
                measurementsRadLabel.Text = measurementsRadLabelText;
                dayRadLabel.Text = dayRadLabelText;
                measurementScheduleRadGroupBox.Text = measurementScheduleRadGroupBoxText;
                stepMeasurementScheduleRadRadioButton.Text = stepMeasurementScheduleRadRadioButtonText;
                byScheduledMeasurementScheduleRadRadioButton.Text = byScheduledMeasurementScheduleRadRadioButtonText;
                hourRadLabel.Text = hourRadLabelText;
                minuteRadLabel.Text = minuteRadLabelText;
                availableConfigurationsCommonSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveCurrentConfigurationCommonSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
                //saveDeviceConfigurationCommonSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseCommonSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceCommonSettingsTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceCommonSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                deleteSelectedConfigurationCommonSettingsTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectCommonSettingsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceCommonSettingsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseCommonSettingsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //currentCommonSettingsTabRadRadioButton.Text = currentRadRadioButtonText;
                //copyDeviceConfigurationCommonSettingsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationCommonSettingsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationCommonSettingsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                pdSettingsRadPageViewPage.Text = pdSettingsRadPageViewPageText;
                synchronizationRadLabel.Text = synchronizationRadLabelText;
                hzRadLabel.Text = hzRadLabelText;
                rereadOnAlarmRadCheckBox.Text = rereadOnAlarmRadCheckBoxText;
                cyclesPerAcquisitionRadLabel.Text = cyclesPerAcquisitionRadLabelText;
                commonPhaseShiftRadLabel.Text = commonPhaseShiftRadLabelText;
                degreesRadLabel.Text = degreesRadLabelText;
                //neutralRadLabel.Text = neutralRadLabelText;
                //commonNeutralRadRadioButton.Text = commonNeutralRadRadioButtonText;
                //insulatedNeutralRadRadioButton.Text = insulatedNeutralRadRadioButtonText;
                availableConfigurationsPDSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveCurrentConfigurationPDSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
               // saveDeviceConfigurationPDSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabasePDSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDevicePDSettingsTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDevicePDSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                deleteSelectedConfigurationPDSettingsTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectPDSettingsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDevicePDSettingsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabasePDSettingsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //currentPDSettingsTabRadRadioButton.Text = currentRadRadioButtonText;
                //copyDeviceConfigurationPDSettingsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationPDSettingsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationPDSettingsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                alarmSettingsRadPageViewPage.Text = alarmSettingsRadPageViewPageText;
                relayModeRadGroupBox.Text = relayModeRadGroupBoxText;
                onRelayModeRadRadioButton.Text = onRelayModeRadRadioButtonText;
                offRelayModeRadRadioButton.Text = offRelayModeRadRadioButtonText;
                alarmEnableRadGroupBox.Text = alarmEnableRadGroupBoxText;
                alarmOnPDILevelRadCheckBox.Text = alarmOnPDILevelRadCheckBoxText;
                alarmOnPDITrendRadCheckBox.Text = alarmOnPDITrendRadCheckBoxText;
                alarmOnPDIChangeRadCheckBox.Text = alarmOnPDIChangeRadCheckBoxText;
                alarmOnQmaxLevelRadCheckBox.Text = alarmOnQmaxLevelRadCheckBoxText;
                alarmOnQmaxTrendRadCheckBox.Text = alarmOnQmaxTrendRadCheckBoxText;
                alarmOnQmaxChangeRadCheckBox.Text = alarmOnQmaxChangeRadCheckBoxText;
                commonAlarmSettingsRadGroupBox.Text = commonAlarmSettingsRadGroupBoxText;
                trendAlarmsTitleRadLabel.Text = trendAlarmsTitleRadLabelText;
                trendWarningRadLabel.Text = trendWarningRadLabelText;
                trendAlarmRadLabel.Text = trendAlarmRadLabelText;
                calcTrendOnRadLabel.Text = calcTrendOnRadLabelText;
                timesPerYearTrendWarningRadLabel.Text = timesPerYearTrendWarningRadLabelText;
                timesPerYearTrendAlarmRadLabel.Text = timesPerYearTrendAlarmRadLabelText;
                weeksRadLabel.Text = weeksRadLabelText;
                alarmOnChangeRadLabel.Text = alarmOnChangeRadLabelText;
                changeWarningRadLabel.Text = changeWarningRadLabelText;
                changeAlarmRadLabel.Text = changeAlarmRadLabelText;
                percentChangeWarningRadLabel.Text = percentChangeWarningRadLabelText;
                percentChangeAlarmRadLabel.Text = percentChangeAlarmRadLabelText;
                availableConfigurationsAlarmSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveCurrentConfigurationAlarmSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
                //saveDeviceConfigurationAlarmSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceAlarmSettingsTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceAlarmSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                deleteSelectedConfigurationAlarmSettingsTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectAlarmSettingsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceAlarmSettingsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseAlarmSettingsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //currentAlarmSettingsTabRadRadioButton.Text = currentRadRadioButtonText;
                //copyDeviceConfigurationAlarmSettingsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationAlarmSettingsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationAlarmSettingsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                databaseInteractionCommonSettingsTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionPDSettingsTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionAlarmSettingsTabRadGroupBox.Text = databaseInteractionGroupBoxText;

                deviceInteractionCommonSettingsTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionPDSettingsTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionAlarmSettingsTabRadGroupBox.Text = deviceInteractionGroupBoxText;

                advancedSettingsWarningRadLabel.Text = advancedSettingsWarningText;

                templateConfigurationsCommonSettingsTabRadDropDownList.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsPDSettingsTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsAlarmSettingsTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;

                copySelectedConfigurationCommonSettingsTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationPDSettingsTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationAlarmSettingsTabRadButton.Text = copySelectedConfigurationRadButtonText;

                firmwareVersionTextRadLabel.Text = firmwareVersionTextRadLabelText;
                cpldVersionTextRadLabel.Text = cpldVersionTextRadLabelText;

                if (showAdvancedSettings)
                {
                    advancedSettingsRadButton.Text = showAdvancedSettingsRadButtonText;
                }
                else
                {
                    advancedSettingsRadButton.Text = hideAdvancedSettingsRadButtonText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                currentConfigName = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCurrentConfigNameText", currentConfigName, "", "", "");
                emptyCellErrorMessage = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationEmptyCellErrorMessage", emptyCellErrorMessage, htmlFontType, htmlStandardFontSize, "");
                uploadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationUploadingConfigurationText", uploadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                downloadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDownloadingConfigurationText", downloadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                //currentDeviceConfigurationNotInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCurrentDeviceConfigurationNotInDatabaseText", currentDeviceConfigurationNotInDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //noDeviceConfigurationInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoDeviceConfigurationInDatabaseText", noDeviceConfigurationInDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //changesMadeToCurrentConfigurationNotSavedWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationChangesMadeToCurrentConfigurationNotSavedWarningText", changesMadeToCurrentConfigurationNotSavedWarningText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationExitWithoutSavingQuestionText", exitWithoutSavingQuestionText, htmlFontType, htmlStandardFontSize, "");
                //noDatabaseConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoDatabaseConfigurationLoadedText", noDatabaseConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
                //noDeviceConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoDeviceConfigurationLoadedText", noDeviceConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
                //overwriteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationOverwriteCurrentConfigurationText", overwriteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");

                // display objects text strings
                pdmMonitorConfigurationInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceTitleText", pdmMonitorConfigurationInterfaceTitleText, "", "", "");
                //pdmMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText", pdmMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText, "", "", "");
                //pdmMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText", pdmMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText, "", "", "");
                //pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText", pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText, "", "", "");
                //pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText", pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText, "", "", "");
                //pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText", pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText, "", "", "");

                // common display object texts
                availableConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAvailableConfigurationsRadGroupBoxText", availableConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                saveCurrentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceSaveCurrentConfigurationRadButtonText", saveCurrentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //saveDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceSaveDeviceConfigurationRadButtonText", saveDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceLoadConfigFromDatabaseRadButtonText", loadConfigurationFromDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                programDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceProgramDeviceRadButtonText", programDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceLoadConfigurationFromDeviceRadButtonText", loadConfigurationFromDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                deleteConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceDeleteConfigurationRadButtonText", deleteConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //configurationViewSelectRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceConfigurationViewSelectRadGroupBoxText", configurationViewSelectRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                //fromDeviceRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceFromDeviceRadioButtonText", fromDeviceRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //fromDatabaseRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceFromDatabaseRadioButtonText", fromDatabaseRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //currentRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCurrentRadioButtonText", currentRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCopyDeviceConfigurationRadButtonText", copyDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyDatabaseConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCopyDatabaseConfigurationRadButtonText", copyDatabaseConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                // individual display object text strings, one for each display object
                aSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationASensorPhaseText", aSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                bSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationBSensorPhaseText", bSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                cSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCSensorPhaseText", cSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                abSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationAbSensorPhaseText", abSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                bcSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationBcSensorPhaseText", bcSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                caSensorPhaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCaSensorPhaseText", caSensorPhaseText, htmlFontType, htmlStandardFontSize, "");
                commonSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabText", commonSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                monitoringRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMonitoringRadGroupBoxText", monitoringRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMonitoringEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMonitoringDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                ratedVoltageRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageRadGroupBoxText", ratedVoltageRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                channels1to3RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageChannels1to3RadLabelText", channels1to3RadLabelText, htmlFontType, htmlStandardFontSize, "");
                channels4to6RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageChannels4to6RadLabelText", channels4to6RadLabelText, htmlFontType, htmlStandardFontSize, "");
                channels7to12RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageChannels7to12RadLabelText", channels7to12RadLabelText, htmlFontType, htmlStandardFontSize, "");
                channels13to15RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageChannels13to15RadLabelText", channels13to15RadLabelText, htmlFontType, htmlStandardFontSize, "");
                kV1RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageKV1RadLabelText", kV1RadLabelText, htmlFontType, htmlStandardFontSize, "");
                kV2RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageKV2RadLabelText", kV2RadLabelText, htmlFontType, htmlStandardFontSize, "");
                kV3RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageKV3RadLabelText", kV3RadLabelText, htmlFontType, htmlStandardFontSize, "");
                kV4RadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabRatedVoltageKV4RadLabelText", kV4RadLabelText, htmlFontType, htmlStandardFontSize, "");
                connectionSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabConnectionSettingsRadGroupBoxText", connectionSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabConnectionSettingsModbusAddressRadLabelText", modbusAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                baudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabConnectionSettingsBaudRateRadLabelText", baudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                saveRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveRadGroupBoxText", saveRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                saveModeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveSaveModRadLabelText", saveModeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                normalSaveModeRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveNormalSaveModeRadRadioButtonText", normalSaveModeRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                testSaveModeRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveTestSaveModeRadRadioButtonText", testSaveModeRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                savePRPDDRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveSavePRPDDRadLabelText", savePRPDDRadLabelText, htmlFontType, htmlStandardFontSize, "");
                measurementsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveMeasurementsRadLabelText", measurementsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                dayRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabSaveDayRadLabel", dayRadLabelText, htmlFontType, htmlStandardFontSize, "");
                measurementScheduleRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMeasurementScheduleRadGroupBoxText", measurementScheduleRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                stepMeasurementScheduleRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMeasurementScheduleStepMeasurementScheduleRadRadioButtonText", stepMeasurementScheduleRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                byScheduledMeasurementScheduleRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMeasurementScheduleByScheduleMeasurementScheduleRadRadioButtonText", byScheduledMeasurementScheduleRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                hourRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMeasurementScheduleHourRadLabelText", hourRadLabelText, htmlFontType, htmlStandardFontSize, "");
                minuteRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCommonSettingsTabMeasurementScheduleMinuteRadLabelText", minuteRadLabelText, htmlFontType, htmlStandardFontSize, "");

                pdSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabText", pdSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                synchronizationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabSynchronizationRadLabelText", synchronizationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                hzRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabHzRadLabelText", hzRadLabelText, htmlFontType, htmlStandardFontSize, "");
                rereadOnAlarmRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabRereadOnAlarmRadCheckBoxText", rereadOnAlarmRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                cyclesPerAcquisitionRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabCyclesPerAcquisitionRadLabelText", cyclesPerAcquisitionRadLabelText, htmlFontType, htmlStandardFontSize, "");
                commonPhaseShiftRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabCommonPhaseShiftRadLabelText", commonPhaseShiftRadLabelText, htmlFontType, htmlStandardFontSize, "");
                degreesRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabDegreesRadLabelText", degreesRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //neutralRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabNeutralRadLabelText", neutralRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //commonNeutralRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabCommonNeutralRadRadioButtonText", commonNeutralRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //insulatedNeutralRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfacePDSettingsTabInsulatedNeutralRadRadioButtonText", insulatedNeutralRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");

                alarmSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabText", alarmSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                relayModeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabRelayModeRadGroupBoxText", relayModeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                onRelayModeRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabRelayModeOnRelayModeRadRadioButtonText", onRelayModeRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                offRelayModeRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabRelayModeOffRelayModeRadRadioButtonText", offRelayModeRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                alarmEnableRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsRadGroupBoxText", alarmEnableRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnPDILevelRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnPDILevelRadCheckBoxText", alarmOnPDILevelRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnPDITrendRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnPDITrendRadCheckBoxText", alarmOnPDITrendRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnPDIChangeRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnPDIChangeRadCheckBoxText", alarmOnPDIChangeRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnQmaxLevelRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnQmaxLevelRadCheckBoxText", alarmOnQmaxLevelRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnQmaxTrendRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnQmaxTrendRadCheckBoxText", alarmOnQmaxTrendRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                alarmOnQmaxChangeRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabEnableAlarmsAlarmOnQmaxChangeRadCheckBoxText", alarmOnQmaxChangeRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                commonAlarmSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabCommonAlarmSettingsRadGroupBoxText", commonAlarmSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                trendAlarmsTitleRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabTrendAlarmsTitleRadLabelText", trendAlarmsTitleRadLabelText, htmlFontType, htmlStandardFontSize, "");
                trendWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabTrendWarningRadLabel", trendWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
                trendAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabTrendAlarmRadLabelText", trendAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
                calcTrendOnRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabCalcTrendOnRadLabelText", calcTrendOnRadLabelText, htmlFontType, htmlStandardFontSize, "");
                timesPerYearTrendWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabTimesPerYearTrendWarningRadLabel", timesPerYearTrendWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
                timesPerYearTrendAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabTimesPerYearTrendAlarmRadLabel", timesPerYearTrendAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
                weeksRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabWeeksRadLabelText", weeksRadLabelText, htmlFontType, htmlStandardFontSize, "");
                alarmOnChangeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabAlarmOnChangeRadLabelText", alarmOnChangeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                changeWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabChangeWarningRadLabelText", changeWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
                changeAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabChangeAlarmRadLabelText", changeAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
                percentChangeWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabPercentChangeWarningRadLabelText", percentChangeWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
                percentChangeAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAlarmSettingsTabPercentChangeAlarmRadLabelText", percentChangeAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");

                errorInReferenceChannelValueInPDSettingsText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationErrorInReferenceChannelValueInPDSettingsText", errorInReferenceChannelValueInPDSettingsText, htmlFontType, htmlStandardFontSize, "");
                copySettingsToAllOtherRowsText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCopySettingsToAllOtherRowsText", copySettingsToAllOtherRowsText, htmlFontType, htmlStandardFontSize, "");
                copySettingsToAllEnabledRowsText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCopySettingsToAllEnabledRowsText", copySettingsToAllEnabledRowsText, htmlFontType, htmlStandardFontSize, "");
                channelNumberChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationChannelNumberChannelConfigurationGridViewText", channelNumberChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationEnableChannelChannelConfigurationGridViewText", enableChannelChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                pulseWidthChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationPulseWidthChannelConfigurationGridViewText", pulseWidthChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                deadTimeChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeadTimeChannelConfigurationGridViewText", deadTimeChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                pdiCalculationLimitChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationPdiCalculationLimitChannelConfigurationGridViewText", pdiCalculationLimitChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensitivityChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationSensitivityChannelConfigurationGridViewText", sensitivityChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorPhaseChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationSensorPhaseChannelConfigurationGridViewText", sensorPhaseChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                enablePolarityChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationEnablePolarityChannelConfigurationGridViewText", enablePolarityChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableOppositePolarityChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationEnableOppositePolarityChannelConfigurationGridViewText", enableOppositePolarityChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableTimeOfArrivalChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationEnableTimeOfArrivalChannelConfigurationGridViewText", enableTimeOfArrivalChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                referenceChannelChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationReferenceChannelChannelConfigurationGridViewText", referenceChannelChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                referenceChannelShiftChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationReferenceChannelShiftChannelConfigurationGridViewText", referenceChannelShiftChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");

                group1AmplitudeFilterEnableChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup1AmplitudeFilterEnableChannelConfigurationGridViewText", group1AmplitudeFilterEnableChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group1AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup1AmplitudeFilterThresholdTypeChannelConfigurationGridViewText", group1AmplitudeFilterThresholdTypeChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group1AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup1AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText", group1AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group1AmplitudeFilterThresholdStopChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup1AmplitudeFilterThresholdStopChannelConfigurationGridViewText", group1AmplitudeFilterThresholdStopChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");

                group2AmplitudeFilterEnableChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup2AmplitudeFilterEnableChannelConfigurationGridViewText", group2AmplitudeFilterEnableChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group2AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup2AmplitudeFilterThresholdTypeChannelConfigurationGridViewText", group2AmplitudeFilterThresholdTypeChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group2AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup2AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText", group2AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group2AmplitudeFilterThresholdStopChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup2AmplitudeFilterThresholdStopChannelConfigurationGridViewText", group2AmplitudeFilterThresholdStopChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");

                group3AmplitudeFilterEnableChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup3AmplitudeFilterEnableChannelConfigurationGridViewText", group3AmplitudeFilterEnableChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group3AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup3AmplitudeFilterThresholdTypeChannelConfigurationGridViewText", group3AmplitudeFilterThresholdTypeChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group3AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup3AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText", group3AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                group3AmplitudeFilterThresholdStopChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationGroup3AmplitudeFilterThresholdStopChannelConfigurationGridViewText", group3AmplitudeFilterThresholdStopChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");

                slidingChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationSlidingChannelConfigurationGridViewText", slidingChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                constantChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConstantChannelConfigurationGridViewText", constantChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                noneChannelConfigurationGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoneChannelConfigurationGridViewText", noneChannelConfigurationGridViewText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressContainsNonNumericalValueText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationModbusAddressContainsNonNumericalValueText", modbusAddressContainsNonNumericalValueText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationModbusAddressOutOfRange", modbusAddressOutOfRangeText, htmlFontType, htmlStandardFontSize, "");

                measurementNumberMeasurementSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationMeasurementNumberMeasurementSettingsGridViewText", measurementNumberMeasurementSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");
                hourMeasurementSettingsRadGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationHourMeasurementSettingsRadGridViewText", hourMeasurementSettingsRadGridViewText, htmlFontType, htmlStandardFontSize, "");
                minuteMeasurementSettingsRadGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationMinuteMeasurementSettingsRadGridViewText", minuteMeasurementSettingsRadGridViewText, htmlFontType, htmlStandardFontSize, "");

               // failedToSaveConfigurationToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToSaveConfigurationToDatabaseText", failedToSaveConfigurationToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                noCurrentConfigurationDefinedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoCurrentConfigurationDefinedText", noCurrentConfigurationDefinedText, htmlFontType, htmlStandardFontSize, "");
                //noConfigurationLoadedFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoConfigurationLoadedFromDeviceText", noConfigurationLoadedFromDeviceText, htmlFontType, htmlStandardFontSize, "");
                configurationBeingSavedIsFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConfigurationBeingSavedIsFromDeviceText", configurationBeingSavedIsFromDeviceText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeviceConfigurationSavedToDatabaseText", deviceConfigurationSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeviceConfigurationNotSavedToDatabaseText", deviceConfigurationNotSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
               // errorInConfigurationLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationErrorInConfigurationLoadedFromDatabaseText", errorInConfigurationLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
               // configurationCouldNotBeLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCouldNotLoadedFromDatabaseText", configurationCouldNotBeLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
                configurationDeleteFromDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeleteFromDatabaseWarningText", configurationDeleteFromDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
                cannotDeleteCurrentConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCannotDeleteCurrentConfigurationWarningText", cannotDeleteCurrentConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");
                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoConfigurationSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");
                //failedToDownloadDeviceConfigurationDataText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToDownloadDeviceConfigurationDataText", failedToDownloadDeviceConfigurationDataText, htmlFontType, htmlStandardFontSize, "");
                //commandToReadDeviceErrorStateFailedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCommandToReadDeviceErrorStateFailedText", commandToReadDeviceErrorStateFailedText, htmlFontType, htmlStandardFontSize, "");
                //lostDeviceConnectionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationLostDeviceConnectionText", lostDeviceConnectionText, htmlFontType, htmlStandardFontSize, "");
                //notConnectedToPDMWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNotConnectedToPDMWarningText", notConnectedToPDMWarningText, htmlFontType, htmlStandardFontSize, "");
                serialPortNotSetWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationSerialPortNotSetWarningText", serialPortNotSetWarningText, htmlFontType, htmlStandardFontSize, "");
                failedToOpenMonitorConnectionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToOpenMonitorConnectionText", failedToOpenMonitorConnectionText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotProperlyConfiguredText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorCommunicationDeviceCommunicationNotProperlyConfiguredText", deviceCommunicationNotProperlyConfiguredText, htmlFontType, htmlStandardFontSize, "");
                downloadWasInProgressWhenInterfaceWasOpenedText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDownloadWasInProgressWhenInterfaceWasOpenedWarningText", downloadWasInProgressWhenInterfaceWasOpenedText, htmlFontType, htmlStandardFontSize, "");
                //failedToWriteTheConfigurationToTheDeviceText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToWriteTheConfigurationToTheDeviceText", failedToWriteTheConfigurationToTheDeviceText, htmlFontType, htmlStandardFontSize, "");
                //configurationWasReadFromTheDeviceText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConfigurationWasReadFromTheDeviceText", configurationWasReadFromTheDeviceText, htmlFontType, htmlStandardFontSize, "");
                //configurationWasWrittenToTheDeviceText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConfigurationWasWrittenToTheDeviceText", configurationWasWrittenToTheDeviceText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeviceConfigurationDoesNotMatchDatabaseConfigurationText", deviceConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotSavedInDatabaseYetText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDeviceCommunicationNotSavedInDatabaseYetText", deviceCommunicationNotSavedInDatabaseYetText, htmlFontType, htmlStandardFontSize, "");
                saveDeviceConfigurationQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationSaveDeviceConfigurationQuestionText", saveDeviceConfigurationQuestionText, htmlFontType, htmlStandardFontSize, "");

                copyRowsErrorMessage = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCopyRowsErrorMessage", copyRowsErrorMessage, htmlFontType, htmlStandardFontSize, "");
                nonNumericalValuePresentText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNonNumericalValuePresentText", nonNumericalValuePresentText, htmlFontType, htmlStandardFontSize, "");
                channelNumberAlarmSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationChannelNumberAlarmSettingsGridViewText", channelNumberAlarmSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");
                pdiWarningAlarmSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationPdiWarningAlarmSettingsGridViewText", pdiWarningAlarmSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");
                pdiAlarmAlarmSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationPdiAlarmAlarmSettingsGridViewText", pdiAlarmAlarmSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");
                qmaxWarningAlarmSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationQmaxWarningAlarmSettingsGridViewText", qmaxWarningAlarmSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");
                qmaxAlarmAlarmSettingsGridViewText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationQmaxAlarmAlarmSettingsGridViewText", qmaxAlarmAlarmSettingsGridViewText, htmlFontType, htmlStandardFontSize, "");

                firmwareVersionNotAvailableText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFirmwareVersionNotAvailableText", firmwareVersionNotAvailableText, "", "", "");

               // initializeWorkingConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceInitializeWorkingConfigurationRadButtonText", initializeWorkingConfigurationRadButtonText, "", "", "");

                //workingConfigAlreadyPresentInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationWorkingConfigAlreadyPresentInDatabaseText", workingConfigAlreadyPresentInDatabaseText, "", "", "");
                //failedToDeleteWorkingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToDeleteWorkingConfigurationText", failedToDeleteWorkingConfigurationText, "", "", "");
                workingConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationWorkingConfigurationSavedToDatabaseText", workingConfigurationSavedToDatabaseText, "", "", "");
                workingConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationWorkingConfigurationNotSavedToDatabaseText", workingConfigurationNotSavedToDatabaseText, "", "", "");

                configurationWasReadFromTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConfigurationWasReadFromTheDatabaseText", configurationWasReadFromTheDatabaseText, "", "", "");

                changesToWorkingConfigurationNotSavedOverwriteWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationChangesToWorkingConfigurationNotSavedOverwriteWarningText", changesToWorkingConfigurationNotSavedOverwriteWarningText, "", "", "");
                changesToWorkingConfigurationNotSavedExitWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationChangesToWorkingConfigurationNotSavedExitWarningText", changesToWorkingConfigurationNotSavedExitWarningText, "", "", "");
                discardChangesAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationDiscardChangesAsQuestionText", discardChangesAsQuestionText, "", "", "");

                couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCouldNotFindWorkingConfigurationDisplayingEmptyConfigurationText", couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText, htmlFontType, htmlStandardFontSize, "");

                templateConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationTemplateConfigurationsRadGroupBoxText", templateConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                copySelectedConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCopySelectedConfigurationRadButtonText", copySelectedConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                availableConfigurationsHeaderText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationAvailableConfigurationsHeaderText", availableConfigurationsHeaderText, "", "", "");
                noConfigurationsSavedInDbText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoConfigurationsSavedInDbText", noConfigurationsSavedInDbText, "", "", "");

                //
                showAdvancedSettingsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceShowAdvancedSettingsRadButtonText", showAdvancedSettingsRadButtonText, "", "", "");
                hideAdvancedSettingsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceHideAdvancedSettingsRadButtonText", hideAdvancedSettingsRadButtonText, "", "", "");
                advancedSettingsWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceAdvancedSettingsWarningText", advancedSettingsWarningText, "", "", "");
                


                deviceInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceDeviceInteractionGroupBoxText", deviceInteractionGroupBoxText, "", "", "");
                databaseInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceDatabaseInteractionGroupBoxText", databaseInteractionGroupBoxText, "", "", "");

                configurationLoadCancelledText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationConfigurationLoadCancelledText", configurationLoadCancelledText, htmlFontType, htmlStandardFontSize, "");

                failedToDeleteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToDeleteCurrentConfigurationText", failedToDeleteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");

                //cannotDeleteTemplateConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCannotDeleteTemplateConfigurationWarningText", cannotDeleteTemplateConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");

                noTemplateConfigurationsAvailable = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationNoTemplateConfigurationsAvailable", noTemplateConfigurationsAvailable, "", "", "");

                failedtoDeleteConfigurationText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationFailedToDeleteConfigurationText", failedtoDeleteConfigurationText, htmlFontType, htmlStandardFontSize, "");
                replaceExistingConfigurationsWithTheSameNameQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationReplaceExistingConfigurationsWithTheSameNameQuestionText", replaceExistingConfigurationsWithTheSameNameQuestionText, htmlFontType, htmlStandardFontSize, "");
                currentDeviceConfigurationAlreadyExistsOverwriteWarningText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationCurrentDeviceConfigurationAlreadyExistsOverwriteWarningText", currentDeviceConfigurationAlreadyExistsOverwriteWarningText, htmlFontType, htmlStandardFontSize, "");
                areYouSureYouWantToDeleteCurrentConfigQuestionText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationAreYouSureYouWantToDeleteCurrentConfigQuestionText", areYouSureYouWantToDeleteCurrentConfigQuestionText, htmlFontType, htmlStandardFontSize, "");

                firmwareVersionTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceFirmwareVersionTextRadLabelText", firmwareVersionTextRadLabelText, "", "", "");
                cpldVersionTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("PDMMonitorConfigurationInterfaceCpldVersionTextRadLabelTextRadLabelText", cpldVersionTextRadLabelText, "", "", "");
              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void AddDataToAllInterfaceObjects(PDM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    AddDataToCommonSettingsTabObjects(inputConfiguration);
                    AddDataToMeasurementSettingsGridView(inputConfiguration.measurementsInfoList);
                    AddDataToPDSettingsTabObjects(inputConfiguration);
                    AddDataToTheChannelConfigurationGridView(inputConfiguration.channelInfoList);
                    AddDataToAlarmSettingsTabObjects(inputConfiguration);
                    AddDataToAlarmSettingsRadGridView(inputConfiguration.channelInfoList);
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToAllInterfaceObjects()\nInput PDM_MonitorConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToAllInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

     

        private int GetBaudRateFromSelectedIndex(int selectedIndex)
        {
            int baudRate = 0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        baudRate = 9600;
                        break;
                    case 1:
                        baudRate = 38400;
                        break;
                    case 2:
                        baudRate = 57600;
                        break;
                    case 3:
                        baudRate = 115200;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetBaudRateFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return baudRate;
        }

        private int GetSelectedIndexFromBaudRate(int baudRate)
        {
            int selectedIndex = -1;
            try
            {
                switch (baudRate)
                {
                    case 9600:
                        selectedIndex = 0;
                        break;
                    case 38400:
                        selectedIndex = 1;
                        break;
                    case 57600:
                        selectedIndex = 2;
                        break;
                    case 115200:
                        selectedIndex = 3;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private string GetSensorPhaseAsString(int sensorPhase)
        {
            string sensorPhaseAsString = string.Empty;
            try
            {
                switch (sensorPhase)
                {
                    case 0:
                        sensorPhaseAsString = aSensorPhaseText;
                        break;
                    case 1:
                        sensorPhaseAsString = bSensorPhaseText;
                        break;
                    case 2:
                        sensorPhaseAsString = cSensorPhaseText;
                        break;
                    case 3:
                        sensorPhaseAsString = abSensorPhaseText;
                        break;
                    case 4:
                        sensorPhaseAsString = bcSensorPhaseText;
                        break;
                    case 5:
                        sensorPhaseAsString = caSensorPhaseText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSensorPhaseAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sensorPhaseAsString;
        }

        private int GetSelectedIndexFromSensorPhase(string sensorPhase)
        {
            int selectedIndex = -1;
            try
            {
                if (sensorPhase != null)
                {
                    if (sensorPhase.CompareTo(aSensorPhaseText) == 0)
                    {
                        selectedIndex = 0;
                    }
                    else if (sensorPhase.CompareTo(bSensorPhaseText) == 0)
                    {
                        selectedIndex = 1;
                    }
                    else if (sensorPhase.CompareTo(cSensorPhaseText) == 0)
                    {
                        selectedIndex = 2;
                    }
                    else if (sensorPhase.CompareTo(abSensorPhaseText) == 0)
                    {
                        selectedIndex = 3;
                    }
                    else if (sensorPhase.CompareTo(bcSensorPhaseText) == 0)
                    {
                        selectedIndex = 4;
                    }
                    else if (sensorPhase.CompareTo(caSensorPhaseText) == 0)
                    {
                        selectedIndex = 5;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetSelectedIndexFromSensorPhase(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromSensorPhase(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private int GetSelectedIndexFromPDILimitEntry(string pdiEntry)
        {
            int selectedIndex = 0;
            try
            {
                if (pdiEntry != null)
                {
                    switch (pdiEntry)
                    {
                        case "6894":
                            selectedIndex = 0;
                            break;
                        case "5351":
                            selectedIndex = 1;
                            break;
                        case "4154":
                            selectedIndex = 2;
                            break;
                        case "3225":
                            selectedIndex = 3;
                            break;
                        case "2503":
                            selectedIndex = 4;
                            break;
                        case "1943":
                            selectedIndex = 5;
                            break;
                        case "1508":
                            selectedIndex = 6;
                            break;
                        case "1171":
                            selectedIndex = 7;
                            break;
                        case "909":
                            selectedIndex = 8;
                            break;
                        case "705":
                            selectedIndex = 9;
                            break;
                        case "548":
                            selectedIndex = 10;
                            break;
                        case "425":
                            selectedIndex = 11;
                            break;
                        case "330":
                            selectedIndex = 12;
                            break;
                        case "256":
                            selectedIndex = 13;
                            break;
                        case "199":
                            selectedIndex = 14;
                            break;
                        case "154":
                            selectedIndex = 15;
                            break;
                        case "120":
                            selectedIndex = 16;
                            break;
                        case "93":
                            selectedIndex = 17;
                            break;
                        case "72":
                            selectedIndex = 18;
                            break;
                        case "56":
                            selectedIndex = 19;
                            break;
                        case "43":
                            selectedIndex = 20;
                            break;
                        case "34":
                            selectedIndex = 21;
                            break;
                        case "26":
                            selectedIndex = 22;
                            break;
                        case "20":
                            selectedIndex = 23;
                            break;
                        case "16":
                            selectedIndex = 24;
                            break;
                        case "12":
                            selectedIndex = 25;
                            break;
                        case "10":
                            selectedIndex = 26;
                            break;
                        case "7":
                            selectedIndex = 27;
                            break;
                        case "6":
                            selectedIndex = 28;
                            break;
                        case "4":
                            selectedIndex = 29;
                            break;
                        case "3":
                            selectedIndex = 30;
                            break;
                        case "0":
                            selectedIndex = 31;
                            break;
                    }
                    selectedIndex++;
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetSelectedIndexFromPDILimitEntry(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetSelectedIndexFromPDILimitEntry(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        //private void FillChannelConfigurationWithEmptyValues()
        //{
        //    for (int i = 0; i < 15; i++)
        //    {

        //    }
        //}

//        private void InitializeEntries()
//        {
//            try
//            {
//                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
//                {
//                    this.availableConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(PDM_MonitorConfiguration.monitorID, localDB);
//                    if (availableConfigurations.Count > 0)
//                    {

//                        LoadConfigurationFromDB(availableConfigurations[0].ID);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeEntries()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void LoadConfigurationFromDatabase()
//        {
//            try
//            {
//                if (this.availableConfigurationsSelectedIndex > 0)
//                {
//                    Guid configurationRootID = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1].ID;
//                    this.workingConfiguration = null;
                    
//                }
//                else
//                {
//                    RadMessageBox.Show(this, "No configuration selected");
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadConfigurationFromDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


        private void SelectCommonSettingsTab()
        {
            configurationRadPageView.SelectedPage = commonSettingsRadPageViewPage;
        }

        private void SelectPDSettingsTab()
        {
            configurationRadPageView.SelectedPage = pdSettingsRadPageViewPage;
        }

        private void SelectAlarmSettingsTab()
        {
            configurationRadPageView.SelectedPage = alarmSettingsRadPageViewPage;
        }
       
        private void normalSaveModeRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (normalSaveModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementsSavePRPDDRadMaskedEditBox.Enabled = true;
                    daySavePRPDDRadMaskedEditBox.Enabled = true;
                    savePRPDDRadLabel.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.normalSaveModeRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void testSaveModeRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (testSaveModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementsSavePRPDDRadMaskedEditBox.Enabled = false;
                    daySavePRPDDRadMaskedEditBox.Enabled = false;
                    savePRPDDRadLabel.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.testSaveModeRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (stepMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    hourMeasurementScheduleRadMaskedEditBox.Enabled = true;
                    minuteMeasurementScheduleRadMaskedEditBox.Enabled = true;
                    measurementSettingsRadGridView.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (byScheduledMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    measurementSettingsRadGridView.Enabled = true;
                    hourMeasurementScheduleRadMaskedEditBox.Enabled = false;
                    minuteMeasurementScheduleRadMaskedEditBox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void synchronizationRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = synchronizationRadDropDownList.SelectedIndex;
                if (selectedIndex == 0)
                {
                    synchronizationFrequencyRadMaskedEditBox.Visible = true;
                    hzRadLabel.Visible = true;
                }
                else
                {
                    synchronizationFrequencyRadMaskedEditBox.Visible = false;
                    hzRadLabel.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.synchronizationRadDropDownList_SelectedIndexChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInSomeTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                if ((!errorIsPresent) && ErrorIsPresentInACommonSettingsTabObject())
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && ErrorIsPresentInAPDSettingsTabObject())
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && ErrorIsPresentInAnAlarmSettingsTabObject())
                {
                    errorIsPresent = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.UpdateConfigurationWithCurrentInterfaceValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void LoadAvailableConfigurations(MonitorInterfaceDB localDB)
        {
            try
            {
                string messageString;
                Guid templateConfigurationMonitorID = PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();

                if (PDM_MonitorConfiguration.monitor != null)
                {
                    if (PDM_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                    {
                        this.availableConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(localDB);
                    }
                    else
                    {
                        this.availableConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(PDM_MonitorConfiguration.monitor.ID, localDB);
                    }

                    availableConfigurationsAlarmSettingsTabRadListControl.Items.Clear();
                    availableConfigurationsCommonSettingsTabRadListControl.Items.Clear();
                    availableConfigurationsPDSettingsTabRadListControl.Items.Clear();

                    if ((this.availableConfigurations != null) && (this.availableConfigurations.Count > 0))
                    {
                        messageString = availableConfigurationsHeaderText;
                        availableConfigurationsAlarmSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCommonSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsPDSettingsTabRadListControl.Items.Add(messageString);

                        foreach (PDM_Config_ConfigurationRoot entry in this.availableConfigurations)
                        {
                            messageString = entry.DateAdded.ToString() + "       " + entry.Description;
                            availableConfigurationsAlarmSettingsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsCommonSettingsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsPDSettingsTabRadListControl.Items.Add(messageString);
                        }
                    }
                    else
                    {
                        messageString = noConfigurationsSavedInDbText;
                        availableConfigurationsAlarmSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCommonSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsPDSettingsTabRadListControl.Items.Add(messageString);
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.LoadAvailableConfigurations()\nPrivate data PDM_MonitorConfiguration.monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {                
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsCommonSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsPDSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsAlarmSettingsTabRadDropDownListDataSource;
                string description;

                this.templateConfigurations = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource= new string[templateCount];
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsCommonSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsPDSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAlarmSettingsTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsCommonSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsPDSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAlarmSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsCommonSettingsTabRadDropDownList.DataSource = templateConfigurationsCommonSettingsTabRadDropDownListDataSource;
                templateConfigurationsPDSettingsTabRadDropDownList.DataSource = templateConfigurationsPDSettingsTabRadDropDownListDataSource;
                templateConfigurationsAlarmSettingsTabRadDropDownList.DataSource = templateConfigurationsAlarmSettingsTabRadDropDownListDataSource;

                templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private bool DisplayMostRecentConfigurationInTheDatabase()
//        {
//            bool configFound = false;
//            try
//            {
//                Guid configurationRootID;
//                if (this.availableConfigurations != null)
//                {
//                    if (this.availableConfigurations.Count > 0)
//                    {
//                        configurationRootID = this.availableConfigurations[0].ID;
//                        configurationFromDatabase = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);
//                        AddDataToAllInterfaceObjects(configurationFromDatabase);
//                        EnableFromDatabaseRadRadioButtons();
//                        SetFromDatabaseRadRadioButtonState();
//                        configFound = true;
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nthis.availableConfigurations was null";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return configFound;
//        }

        private void WriteAllInterfaceDataToWorkingConfiguration()
        {
            try
            {
                WriteCommonSettingsTabObjectsToConfigurationData();
                WriteMeasurementSettingsToConfigurationData();
                WritePDSettingsTabObjectsToConfigurationData();
                WritePDSettingsChannelInfoGridEntriesToConfigurationData();
                WriteAlarmSettingsTabObjectsToConfigurationData();
                WriteAlarmSettingsChannelInfoGridEntriesToConfigurationData();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.programDeviceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveCurrentConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    SaveWorkingConfigurationToDatabase(saveDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.saveCurrentConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveConfigurationAsCurrentDeviceConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    SaveWorkingConfigurationToDatabaseAsCurrentDeviceConfiguration(saveDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.saveCurrentConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        //private void saveDeviceConfigurationRadButton_Click(object sender, EventArgs e)
        //{
        //    SaveDeviceConfigurationToDatabase();
        //}

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
                programDeviceAlarmSettingsTabRadButton.Enabled = true;
                programDeviceCommonSettingsTabRadButton.Enabled = true;
                programDevicePDSettingsTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadConfigurationFromDeviceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadConfigurationFromDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadSelectedConfigurationFromDatabase();
                programDeviceAlarmSettingsTabRadButton.Enabled = true;
                programDeviceCommonSettingsTabRadButton.Enabled = true;
                programDevicePDSettingsTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadConfigurationFromDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deleteConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteConfigurationFromDatabase();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.deleteConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private void copyDeviceConfigurationToCurrentRadButton_Click(object sender, EventArgs e)
        //{
        //    CopyConfigurationFromDeviceToCurrent();
        //}

        //private void copyDatabaseConfigurationToCurrentRadButton_Click(object sender, EventArgs e)
        //{
        //    CopyConfigurationFromDatabaseToCurrent();
        //}

        //private void initializeWorkingConfigurationRadButton_Click(object sender, EventArgs e)
        //{
        //    InitializeWorkingConfiguration();
        //}


        private void SetAvailableConfigurationsSelectedIndex()
        {
            try
            {
                availableConfigurationsCommonSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsPDSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsAlarmSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsCommonSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsCommonSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.availableConfigurationsCommonSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsPDSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsPDSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.availableConfigurationsPDSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsAlarmSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsAlarmSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex > -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.availableConfigurationsAlarmSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void PDM_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();

                if (this.workingConfiguration != null)
                {
                    //if (this.lastConfigurationLoadedWasTheWorkingConfiguration)
                    //{
                    //    if (RadMessageBox.Show(this, saveWorkingConfigurationToDatabaseQuestionText, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    //    {
                    //        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    //        {
                    //            SaveWorkingConfigurationToDatabase(localDB);
                    //        }
                    //    }
                    //}
                    //else 
                    if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                    {
                        if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedExitWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.PDM_MonitorConfiguration_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void SetFromDeviceRadRadioButtonState()
//        {
//            try
//            {
//                if (this.configurationFromDevice != null)
//                {
//                    this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText;

//                    if (this.fromDeviceCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceCommonSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDevicePDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDevicePDSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDeviceAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceAlarmSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }                   
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDevice)
//                    {
//                        if (this.configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                        {
//                            WriteAllInterfaceDataToWorkingConfiguration();
//                        }
//                        AddDataToAllInterfaceObjects(this.configurationFromDevice);
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.FromDevice;
//                        DisableCommonSettingsTabObjects();
//                        DisablePDSettingsTabObjects();
//                        DisableAlarmSettingsTabObjects();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetFromDeviceRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void SetFromDatabaseRadRadioButtonState()
//        {
//            try
//            {
//                if (this.configurationFromDatabase != null)
//                {
//                    this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText;

//                    if (this.fromDatabaseCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseCommonSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabasePDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabasePDSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabaseAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseAlarmSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }                 
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDatabase)
//                    {
//                        if (this.configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                        {
//                            WriteAllInterfaceDataToWorkingConfiguration();
//                        }
//                        AddDataToAllInterfaceObjects(this.configurationFromDatabase);
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.FromDatabase;
//                        DisableCommonSettingsTabObjects();
//                        DisablePDSettingsTabObjects();
//                        DisableAlarmSettingsTabObjects();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetFromDatabaseRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void SetCurrentRadRadioButtonState()
//        {
//            try
//            {
//                if (this.workingConfiguration != null)
//                {
//                    if (workingCopyIsFromDevice)
//                    {
//                        this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
//                    }
//                    else if (workingCopyIsFromDatabase)
//                    {
//                        this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
//                    }
//                    else if (workingCopyIsFromInitialization)
//                    {
//                        this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
//                    }

//                    if (this.currentCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.currentCommonSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.currentPDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.currentPDSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.currentAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.currentAlarmSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.Current)
//                    {
//                        AddDataToAllInterfaceObjects(this.workingConfiguration);
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.Current;
//                        EnableCommonSettingsTabObjects();
//                        EnablePDSettingsTabObjects();
//                        EnableAlarmSettingsTabObjects();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetCurrentRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void DisableFromDeviceRadRadioButtons()
//        {
//            this.fromDeviceCommonSettingsTabRadRadioButton.Enabled = false;
//            this.fromDevicePDSettingsTabRadRadioButton.Enabled = false;
//            this.fromDeviceAlarmSettingsTabRadRadioButton.Enabled = false;
//        }

//        private void EnableFromDeviceRadRadioButtons()
//        {
//            if (this.configurationFromDevice != null)
//            {
//                this.fromDeviceCommonSettingsTabRadRadioButton.Enabled = true;
//                this.fromDevicePDSettingsTabRadRadioButton.Enabled = true;
//                this.fromDeviceAlarmSettingsTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void DisableFromDatabaseRadRadioButtons()
//        {
//            this.fromDatabaseCommonSettingsTabRadRadioButton.Enabled = false;
//            this.fromDatabasePDSettingsTabRadRadioButton.Enabled = false;
//            this.fromDatabaseAlarmSettingsTabRadRadioButton.Enabled = false;
//        }

//        private void EnableFromDatabaseRadRadioButtons()
//        {
//            if (this.configurationFromDatabase != null)
//            {
//                this.fromDatabaseCommonSettingsTabRadRadioButton.Enabled = true;
//                this.fromDatabasePDSettingsTabRadRadioButton.Enabled = true;
//                this.fromDatabaseAlarmSettingsTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void DisableCurrentRadRadioButton()
//        {
//            this.currentCommonSettingsTabRadRadioButton.Enabled = false;
//            this.currentPDSettingsTabRadRadioButton.Enabled = false;
//            this.currentAlarmSettingsTabRadRadioButton.Enabled = false;
//        }

//        private void EnableCurrentRadRadioButton()
//        {
//            if (this.workingConfiguration != null)
//            {
//                this.currentCommonSettingsTabRadRadioButton.Enabled = true;
//                this.currentPDSettingsTabRadRadioButton.Enabled = true;
//                this.currentAlarmSettingsTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void CopyConfigurationFromDatabaseToCurrent()
//        {
//            bool copyConfiguration = true;
//            if (this.configurationFromDatabase != null)
//            {
//                if (this.workingConfiguration != null)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                    if ((this.uneditedWorkingConfiguration != null) && (!ConfigurationsAreEquivalent(this.workingConfiguration, this.uneditedWorkingConfiguration)))
//                    {
//                        if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                        {
//                            copyConfiguration = false;
//                        }
//                    }
//                }
//                if (copyConfiguration)
//                {
//                    this.workingConfiguration = PDM_Configuration.CopyConfiguration(this.configurationFromDatabase);
//                    this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
//                    EnableCurrentRadRadioButton();
//                    workingCopyIsFromDatabase = true;
//                    workingCopyIsFromDevice = false;
//                    workingConfigurationIsFromInitialization = false;
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                    {
//                        AddDataToAllInterfaceObjects(this.workingConfiguration);
//                        this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
//                    }
//                    else
//                    {
//                        SetCurrentRadRadioButtonState();
//                    }
//                }
//            }
//            else
//            {
//                RadMessageBox.Show(this, noDatabaseConfigurationLoadedText);
//            }
//        }

//        private void CopyConfigurationFromDeviceToCurrent()
//        {
//            bool copyConfiguration = true;
//            if (this.configurationFromDevice != null)
//            {
//                if (this.workingConfiguration != null)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                    if ((this.uneditedWorkingConfiguration != null) && (!ConfigurationsAreEquivalent(this.workingConfiguration, this.uneditedWorkingConfiguration)))
//                    {
//                        if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                        {
//                            copyConfiguration = false;
//                        }
//                    }
//                }
//                if (copyConfiguration)
//                {
//                    this.workingConfiguration = PDM_Configuration.CopyConfiguration(this.configurationFromDevice);
//                    this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
//                    EnableCurrentRadRadioButton();
//                    workingCopyIsFromDevice = true;
//                    workingCopyIsFromInitialization = false;
//                    workingCopyIsFromDatabase = false;
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                    {
//                        AddDataToAllInterfaceObjects(this.workingConfiguration);
//                        this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
//                    }
//                    else
//                    {
//                        SetCurrentRadRadioButtonState();
//                    }
//                }
//            }
//            else
//            {
//                RadMessageBox.Show(this, noDeviceConfigurationLoadedText);
//            }
//        }

//        private void InitializeWorkingConfiguration()
//        {
//            bool copyConfiguration = true;

//            if (this.workingConfiguration != null)
//            {
//                WriteAllInterfaceDataToWorkingConfiguration();
//                if ((this.uneditedWorkingConfiguration != null) && (!ConfigurationsAreEquivalent(this.workingConfiguration, this.uneditedWorkingConfiguration)))
//                {
//                    if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                    {
//                        copyConfiguration = false;
//                    }
//                }
//            }
//            if (copyConfiguration)
//            {
//                this.workingConfiguration = new PDM_Configuration();
//                this.uneditedWorkingConfiguration = PDM_Configuration.CopyConfiguration(this.workingConfiguration);
//                EnableCurrentRadRadioButton();
//                workingCopyIsFromDevice = false;
//                workingCopyIsFromDatabase = false;
//                workingCopyIsFromInitialization = true;
//                if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                {
//                    AddDataToAllInterfaceObjects(this.workingConfiguration);
//                    this.Text = pdmMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
//                }
//                else
//                {
//                    SetCurrentRadRadioButtonState();
//                }
//            }
//        }


//        private void fromDeviceCommonSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDeviceCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDeviceCommonSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseCommonSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDatabaseCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDatabaseCommonSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentCommonSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (currentCommonSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetCurrentRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.currentCommonSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDevicePDSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDevicePDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDevicePDSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabasePDSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDatabasePDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDatabasePDSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentPDSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (currentPDSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetCurrentRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.currentPDSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDeviceAlarmSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDeviceAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDeviceAlarmSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseAlarmSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (fromDatabaseAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.fromDatabaseAlarmSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentAlarmSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (currentAlarmSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetCurrentRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.currentAlarmSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
//        {
//            try
//            {
//                if (e.Button == MouseButtons.Right)
//                {
//                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
//                    graphRadContextMenu.Show(menuLocation);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DataViewer.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void channelConfigurationRadGridView_MouseClick(object sender, MouseEventArgs e)
        {
//            try
//            {
//                if (e.Button == MouseButtons.Right)
//                {
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                    {
//                        Point menuLocation = (sender as Control).PointToScreen(e.Location);
//                        alarmSettingsContextMenu.Show(menuLocation);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DataViewer.channelConfigurationRadGridView_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
        }

//        private void alarmSettingsRadGridView_MouseClick(object sender, MouseEventArgs e)
//        {
//            try
//            {
//                if (e.Button == MouseButtons.Right)
//                {
//                    //if (configurationBeingDisplayed == ConfigurationDisplayed.Current)
//                    //{
//                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
//                    alarmSettingsContextMenu.Show(menuLocation);
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DataViewer.alarmSettingsRadGridView_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void DisableAllControls()
        {
            saveCurrentConfigurationCommonSettingsTabRadButton.Enabled = false;
           // saveDeviceConfigurationCommonSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseCommonSettingsTabRadButton.Enabled = false;
            programDeviceCommonSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceCommonSettingsTabRadButton.Enabled = false;
            //deleteConfigurationCommonSettingsTabRadButton.Enabled = false;

            saveCurrentConfigurationPDSettingsTabRadButton.Enabled = false;
            //saveDeviceConfigurationPDSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDatabasePDSettingsTabRadButton.Enabled = false;
            programDevicePDSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDevicePDSettingsTabRadButton.Enabled = false;
            //deleteConfigurationPDSettingsTabRadButton.Enabled = false;

            saveCurrentConfigurationAlarmSettingsTabRadButton.Enabled = false;
            //saveDeviceConfigurationAlarmSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Enabled = false;
            programDeviceAlarmSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceAlarmSettingsTabRadButton.Enabled = false;
            //deleteConfigurationAlarmSettingsTabRadButton.Enabled = false;
         
            //DisableFromDatabaseRadRadioButtons();
            //DisableFromDeviceRadRadioButtons();
            //DisableCurrentRadRadioButton();

            //copyDatabaseConfigurationCommonSettingsTabRadButton.Enabled = false;
            //copyDeviceConfigurationCommonSettingsTabRadButton.Enabled = false;

            //copyDatabaseConfigurationPDSettingsTabRadButton.Enabled = false;
            //copyDeviceConfigurationPDSettingsTabRadButton.Enabled = false;

            //copyDatabaseConfigurationAlarmSettingsTabRadButton.Enabled = false;
            //copyDeviceConfigurationAlarmSettingsTabRadButton.Enabled = false;

          
            DisableCommonSettingsTabObjects();
            DisablePDSettingsTabObjects();
            DisableAlarmSettingsTabObjects();
        }

        private void EnableAllControls()
        {
            saveCurrentConfigurationCommonSettingsTabRadButton.Enabled = true;
            //saveDeviceConfigurationCommonSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseCommonSettingsTabRadButton.Enabled = true;
            programDeviceCommonSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceCommonSettingsTabRadButton.Enabled = true;
            //deleteConfigurationCommonSettingsTabRadButton.Enabled = true;

            saveCurrentConfigurationPDSettingsTabRadButton.Enabled = true;
            //saveDeviceConfigurationPDSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDatabasePDSettingsTabRadButton.Enabled = true;
            programDevicePDSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDevicePDSettingsTabRadButton.Enabled = true;
            //deleteConfigurationPDSettingsTabRadButton.Enabled = true;

            saveCurrentConfigurationAlarmSettingsTabRadButton.Enabled = true;
            //saveDeviceConfigurationAlarmSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Enabled = true;
            programDeviceAlarmSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceAlarmSettingsTabRadButton.Enabled = true;
            //deleteConfigurationAlarmSettingsTabRadButton.Enabled = true;

  
            //EnableFromDatabaseRadRadioButtons();
            //EnableFromDeviceRadRadioButtons();
            //EnableCurrentRadRadioButton();

            //copyDatabaseConfigurationCommonSettingsTabRadButton.Enabled = true;
            //copyDeviceConfigurationCommonSettingsTabRadButton.Enabled = true;

            //copyDatabaseConfigurationPDSettingsTabRadButton.Enabled = true;
            //copyDeviceConfigurationPDSettingsTabRadButton.Enabled = true;

            //copyDatabaseConfigurationAlarmSettingsTabRadButton.Enabled = true;
            //copyDeviceConfigurationAlarmSettingsTabRadButton.Enabled = true;

            EnableCommonSettingsTabObjects();
            EnablePDSettingsTabObjects();
            EnableAlarmSettingsTabObjects();
        }

        private void EnableProgressBars()
        {
            commonSettingsRadProgressBar.Visible = true;
            pdSettingsRadProgressBar.Visible = true;
            alarmSettingsRadProgressBar.Visible = true;
        }

        private void DisableProgressBars()
        {
            commonSettingsRadProgressBar.Visible = false;
            pdSettingsRadProgressBar.Visible = false;
            alarmSettingsRadProgressBar.Visible = false;
        }

        private void SetProgressBarsToUploadState()
        {
            commonSettingsRadProgressBar.Text = uploadingConfigurationText;
            pdSettingsRadProgressBar.Text = uploadingConfigurationText;
            alarmSettingsRadProgressBar.Text = uploadingConfigurationText;
        }

        private void SetProgressBarsToDownloadState()
        {
            commonSettingsRadProgressBar.Text = downloadingConfigurationText;
            pdSettingsRadProgressBar.Text = downloadingConfigurationText;
            alarmSettingsRadProgressBar.Text = downloadingConfigurationText;
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    commonSettingsRadProgressBar.Value1 = currentProgress;
                    pdSettingsRadProgressBar.Value1 = currentProgress;
                    alarmSettingsRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void advancedSettingsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!showAdvancedSettings)
                {
                    showAdvancedSettings = true;
                    advancedSettingsRadButton.Text = hideAdvancedSettingsRadButtonText;
                    advancedSettingsWarningRadLabel.Visible = true;
                    UnHideSelectedPDSettingsColumns();
                }
                else
                {
                    showAdvancedSettings = false;
                    advancedSettingsRadButton.Text = showAdvancedSettingsRadButtonText;
                    advancedSettingsWarningRadLabel.Visible = false;
                    HideSelectedPDSettingsColumns();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.advancedSettingsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ConfigurationsAreEquivalent(PDM_Configuration configurationOne, PDM_Configuration configurationTwo)
        {
            bool isTheSame=false;
            try
            {
                if (PDM_MonitorConfiguration.programType == ProgramType.IHM2)
                {
                    isTheSame = configurationOne.ConfigurationIsTheSame(configurationTwo);
                }
                else if (PDM_MonitorConfiguration.programType == ProgramType.PD15)
                {
                    isTheSame = configurationOne.ConfigurationIsTheSameExceptForMeasurementsSchedule(configurationTwo);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ConfigurationIsTheSame(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private void alarmSettingsRadGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            try
            {
                /// This stuff refuses to work when the debugger is running.  So, don't bother testing it under that circumstance.
                if (e.ContextMenu != null)
                {
                    for (int i = 0; i < e.ContextMenu.Items.Count; i++)
                    {
                        // this code is per http://www.telerik.com/community/forums/winforms/gridview/remove-items-from-default-context-menu.aspx
                        e.ContextMenu.Items[i].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                        // or you could say
                        // e.ContextMenu.Items[i].Enabled = false; 
                    }
                    // now we add our own context menu items, per http://www.telerik.com/community/forums/winforms/gridview/adding-items-to-default-contect-menu.aspx
                    // though that code is in Visual Basic.  bleh.
                    if (this.alarmSettingsCopyAllRowRadMenuItem == null)
                    {
                        this.alarmSettingsCopyAllRowRadMenuItem = new RadMenuItem();
                        this.alarmSettingsCopyAllRowRadMenuItem.Click += alarmSettingsCopyAll_Click;
                        this.alarmSettingsCopyAllRowRadMenuItem.Text = copySettingsToAllOtherRowsText;
                    }

                    e.ContextMenu.Items.Add(this.alarmSettingsCopyAllRowRadMenuItem);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.channelConfigurationRadGridView_ContextMenuOpening(object, ContextMenuOpeningEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void alarmSettingsCopyAll_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewSelectedRowsCollection selectedRows;
                GridViewSelectedCellsCollection selectedCells;

                selectedRows = alarmSettingsRadGridView.SelectedRows;
                selectedCells = alarmSettingsRadGridView.SelectedCells;
                if (selectedRows.Count == 1)
                {
                    CopyAlarmSettingsToAllRows(selectedRows.First());
                }
                else if (selectedCells.Count == 1)
                {
                    CopyAlarmSettingsToAllRows(selectedCells.First().RowInfo);
                }
                else
                {
                    RadMessageBox.Show(this, copyRowsErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.alarmSettingsCopyAll_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
      
         private void channelConfigurationRadGridView_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
         {
             try
             {
                 /// This stuff refuses to work when the debugger is running.  So, don't bother testing it under that circumstance.
                 if (e.ContextMenu != null)
                 {
                     for (int i = 0; i < e.ContextMenu.Items.Count; i++)
                     {
                         // this code is per http://www.telerik.com/community/forums/winforms/gridview/remove-items-from-default-context-menu.aspx
                         e.ContextMenu.Items[i].Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
                         // or you could say
                         // e.ContextMenu.Items[i].Enabled = false; 
                     }
                     // now we add our own context menu items, per http://www.telerik.com/community/forums/winforms/gridview/adding-items-to-default-contect-menu.aspx
                     // though that code is in Visual Basic.  bleh.
                     if (this.channelConfigurationCopyAllRadMenuItem == null)
                     {
                         this.channelConfigurationCopyAllRadMenuItem = new RadMenuItem();
                         this.channelConfigurationCopyAllRadMenuItem.Click += pdSettingsCopyAll_Click;
                         this.channelConfigurationCopyAllRadMenuItem.Text = copySettingsToAllOtherRowsText;
                     }
                     if (this.channelConfigurationCopyEnabledRadMenuItem == null)
                     {
                         this.channelConfigurationCopyEnabledRadMenuItem = new RadMenuItem();
                         this.channelConfigurationCopyEnabledRadMenuItem.Click += pdSettingsCopyEnabled_Click;
                         this.channelConfigurationCopyEnabledRadMenuItem.Text = copySettingsToAllEnabledRowsText;
                     }

                     e.ContextMenu.Items.Add(this.channelConfigurationCopyAllRadMenuItem);
                     e.ContextMenu.Items.Add(this.channelConfigurationCopyEnabledRadMenuItem);
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.channelConfigurationRadGridView_ContextMenuOpening(object, ContextMenuOpeningEventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void pdSettingsCopyAll_Click(object sender, EventArgs e)
         {
             try
             {
                 GridViewSelectedRowsCollection selectedRows;
                 GridViewSelectedCellsCollection selectedCells;
                 selectedRows = channelConfigurationRadGridView.SelectedRows;
                 selectedCells = channelConfigurationRadGridView.SelectedCells;
                 if (selectedRows.Count == 1)
                 {
                     CopyPDSettingsToAllRows(selectedRows.First());
                 }
                 else if (selectedCells.Count == 1)
                 {
                     CopyPDSettingsToAllRows(selectedCells.First().RowInfo);
                 }
                 else
                 {
                     RadMessageBox.Show(this, copyRowsErrorMessage);
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.pdSettingsCopyAll_Click(object, EventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void pdSettingsCopyEnabled_Click(object sender, EventArgs e)
         {
             try
             {
                 GridViewSelectedRowsCollection selectedRows;
                 GridViewSelectedCellsCollection selectedCells;
                 selectedRows = channelConfigurationRadGridView.SelectedRows;
                 selectedCells = channelConfigurationRadGridView.SelectedCells;
                 if (selectedRows.Count == 1)
                 {
                     CopyPDSettingsToAllEnabledRows(selectedRows.First());
                 }
                 else if (selectedCells.Count == 1)
                 {
                     CopyPDSettingsToAllEnabledRows(selectedCells.First().RowInfo);
                 }
                 else
                 {
                     RadMessageBox.Show(this, copyRowsErrorMessage);
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.pdSettingsCopyEnabled_Click(object, EventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void SetTemplateConfigurationsSelectedIndex()
         {
             try
             {
                 if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                 {
                     string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                     templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                     templateConfigurationsCommonSettingsTabRadDropDownList.Text = description;
                     templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                     templateConfigurationsPDSettingsTabRadDropDownList.Text = description;
                     templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                     templateConfigurationsAlarmSettingsTabRadDropDownList.Text = description;
                 }
                 else
                 {
                     string errorMessage = "Error in PDM_MonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                     LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }


         private void templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
         {
             try
             {
                 int selectedIndex = templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndex;
                 if (selectedIndex != -1)
                 {
                     if (selectedIndex != this.templateConfigurationsSelectedIndex)
                     {
                         this.templateConfigurationsSelectedIndex = selectedIndex;
                         SetTemplateConfigurationsSelectedIndex();
                     }
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
         {
             try
             {
                 int selectedIndex = templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndex;
                 if (selectedIndex != -1)
                 {
                     if (selectedIndex != this.templateConfigurationsSelectedIndex)
                     {
                         this.templateConfigurationsSelectedIndex = selectedIndex;
                         SetTemplateConfigurationsSelectedIndex();
                     }
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
         {
             try
             {
                 int selectedIndex = templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndex;
                 if (selectedIndex != -1)
                 {
                     if (selectedIndex != this.templateConfigurationsSelectedIndex)
                     {
                         this.templateConfigurationsSelectedIndex = selectedIndex;
                         SetTemplateConfigurationsSelectedIndex();
                     }
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void copySelectedConfigurationRadButton_Click(object sender, EventArgs e)
         {
             try
             {
                 PDM_Config_ConfigurationRoot templateConfigurationRoot;
                 Guid destinationConfigurationRootID;
                 if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                 {
                     if (this.templateConfigurations != null)
                     {
                         if (this.templateConfigurations.Count > 0)
                         {
                             if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                             {
                                 templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                                 if (templateConfigurationRoot != null)
                                 {
                                     destinationConfigurationRootID = Guid.NewGuid();
                                     using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.dbConnectionString))
                                     using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                     {
                                         if (PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(templateConfigurationRoot.ID, templateDB, destinationConfigurationRootID, PDM_MonitorConfiguration.monitor.ID, destinationDB))
                                         {
                                            // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopySucceeded));
                                         }
                                         else
                                         {
                                             RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopyFailed));
                                         }
                                         LoadAvailableConfigurations(destinationDB);
                                     }
                                 }
                                 else
                                 {
                                     RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                                 }
                             }
                             else
                             {
                                 RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                             }
                         }
                         else
                         {
                             RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                         }
                     }
                     else
                     {
                         RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                         string errorMessage = "Error in PDM_MonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                         LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                     }
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_MonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
         }

         private void alarmSettingsRadPageViewPage_Paint(object sender, PaintEventArgs e)
         {

         }

         private void synchronizationRadDropDownList_SelectedValueChanged(object sender, EventArgs e)
         {
             // this will set the visibility of the frequency textbox and label to only show when slection = internal


         }
    }
}
