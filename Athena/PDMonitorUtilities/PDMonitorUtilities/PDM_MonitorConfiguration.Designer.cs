namespace PDMonitorUtilities
{
    partial class PDM_MonitorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PDM_MonitorConfiguration));
            this.configurationRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.commonSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.cpldVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cpldVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.templateConfigurationsCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCommonSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveCurrentConfigurationCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.firmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firmwareVersionTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.commonSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsCommonSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsCommonSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.saveRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.dayRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.daySavePRPDDRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.measurementsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.measurementsSavePRPDDRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.savePRPDDRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.saveModeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.normalSaveModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.testSaveModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.connectionSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.modbusAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.baudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.baudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ratedVoltageRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.channels1to3RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.kV4RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV3RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV2RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.kV1RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels13to15RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels7to12RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels4to6RatedVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.channels13to15RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels7to12RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels4to6RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channels1to3RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitoringRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.disableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.measurementScheduleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.measurementSettingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.byScheduledMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.minuteRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.hourRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.minuteMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hourMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.stepMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.pdSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.saveAsCurrentConfigurationPDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsPDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationPDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsPDSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionPDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDevicePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDevicePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.advancedSettingsWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.databaseInteractionPDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.saveCurrentConfigurationPDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deleteSelectedConfigurationPDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabasePDSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.advancedSettingsRadButton = new Telerik.WinControls.UI.RadButton();
            this.pdSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsPDSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsPDSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.rereadOnAlarmRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.commonPhaseShiftRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.degreesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cyclesPerAcquisitionRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hzRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.synchronizationFrequencyRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.synchronizationRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.commonPhaseShiftRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cyclesPerAcquisitionRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.synchronizationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.channelConfigurationRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.alarmSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.availableConfigurationsAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsAlarmSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveCurrentConfigurationAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAlarmSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionAlarmSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.alarmSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.commonAlarmSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendWarningRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.trendAlarmRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.calcTrendRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.weeksRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timesPerYearTrendAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timesPerYearTrendWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.percentChangeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.percentChangeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.changeAlarmRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.changeWarningRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.changeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.changeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmOnChangeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.calcTrendOnRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendAlarmsTitleRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.relayModeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.offRelayModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.onRelayModeRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.alarmSettingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.alarmEnableRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.alarmOnQmaxChangeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnQmaxTrendRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnQmaxLevelRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDIChangeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDITrendRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.alarmOnPDILevelRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).BeginInit();
            this.configurationRadPageView.SuspendLayout();
            this.commonSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCommonSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCommonSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsCommonSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadGroupBox)).BeginInit();
            this.saveRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dayRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daySavePRPDDRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsSavePRPDDRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.savePRPDDRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveModeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalSaveModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testSaveModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionSettingsRadGroupBox)).BeginInit();
            this.connectionSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratedVoltageRadGroupBox)).BeginInit();
            this.ratedVoltageRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV4RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV3RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV2RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV1RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RatedVoltageRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).BeginInit();
            this.monitoringRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).BeginInit();
            this.measurementScheduleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduledMeasurementScheduleRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).BeginInit();
            this.pdSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationPDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsPDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationPDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionPDSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionPDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDevicePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDevicePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionPDSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionPDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationPDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationPDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabasePDSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsPDSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsPDSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsPDSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rereadOnAlarmRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hzRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationFrequencyRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView.MasterTemplate)).BeginInit();
            this.alarmSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAlarmSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAlarmSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionAlarmSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAlarmSettingsRadGroupBox)).BeginInit();
            this.commonAlarmSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weeksRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnChangeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendOnRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmsTitleRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayModeRadGroupBox)).BeginInit();
            this.relayModeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offRelayModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.onRelayModeRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmEnableRadGroupBox)).BeginInit();
            this.alarmEnableRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxChangeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxTrendRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxLevelRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDIChangeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDITrendRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDILevelRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationRadPageView
            // 
            this.configurationRadPageView.Controls.Add(this.commonSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.pdSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.alarmSettingsRadPageViewPage);
            this.configurationRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configurationRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationRadPageView.Location = new System.Drawing.Point(0, 0);
            this.configurationRadPageView.Name = "configurationRadPageView";
            this.configurationRadPageView.SelectedPage = this.pdSettingsRadPageViewPage;
            this.configurationRadPageView.Size = new System.Drawing.Size(872, 606);
            this.configurationRadPageView.TabIndex = 0;
            this.configurationRadPageView.Text = "radPageView1";
            this.configurationRadPageView.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.configurationRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // commonSettingsRadPageViewPage
            // 
            this.commonSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.commonSettingsRadPageViewPage.Controls.Add(this.saveAsCurrentConfigurationCommonSettingsTabRadButton);
            this.commonSettingsRadPageViewPage.Controls.Add(this.cpldVersionValueRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.cpldVersionTextRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.databaseInteractionCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.deviceInteractionCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.firmwareVersionValueRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.firmwareVersionTextRadLabel);
            this.commonSettingsRadPageViewPage.Controls.Add(this.commonSettingsRadProgressBar);
            this.commonSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsCommonSettingsTabRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.saveRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.connectionSettingsRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.ratedVoltageRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.monitoringRadGroupBox);
            this.commonSettingsRadPageViewPage.Controls.Add(this.measurementScheduleRadGroupBox);
            //this.commonSettingsRadPageViewPage.ItemSize = new System.Drawing.SizeF(106F, 26F);
            this.commonSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.commonSettingsRadPageViewPage.Name = "commonSettingsRadPageViewPage";
            this.commonSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 560);
            this.commonSettingsRadPageViewPage.Text = "Common Settings";
            // 
            // saveAsCurrentConfigurationCommonSettingsTabRadButton
            // 
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Location = new System.Drawing.Point(449, 214);
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Name = "saveAsCurrentConfigurationCommonSettingsTabRadButton";
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.TabIndex = 43;
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Text = "<html>Save Configuration<br>as<br>Current Configuration</html>";
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Visible = false;
            this.saveAsCurrentConfigurationCommonSettingsTabRadButton.Click += new System.EventHandler(this.saveConfigurationAsCurrentDeviceConfigurationRadButton_Click);
            // 
            // cpldVersionValueRadLabel
            // 
            this.cpldVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpldVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cpldVersionValueRadLabel.Location = new System.Drawing.Point(546, 35);
            this.cpldVersionValueRadLabel.Name = "cpldVersionValueRadLabel";
            this.cpldVersionValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.cpldVersionValueRadLabel.TabIndex = 52;
            this.cpldVersionValueRadLabel.Text = "0.00";
            // 
            // cpldVersionTextRadLabel
            // 
            this.cpldVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpldVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cpldVersionTextRadLabel.Location = new System.Drawing.Point(445, 35);
            this.cpldVersionTextRadLabel.Name = "cpldVersionTextRadLabel";
            this.cpldVersionTextRadLabel.Size = new System.Drawing.Size(78, 16);
            this.cpldVersionTextRadLabel.TabIndex = 51;
            this.cpldVersionTextRadLabel.Text = "CPLD Version";
            // 
            // templateConfigurationsCommonSettingsTabRadGroupBox
            // 
            this.templateConfigurationsCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationCommonSettingsTabRadButton);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsCommonSettingsTabRadDropDownList);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.radButton3);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCommonSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 295);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Name = "templateConfigurationsCommonSettingsTabRadGroupBox";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(402, 65);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.TabIndex = 53;
            this.templateConfigurationsCommonSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationCommonSettingsTabRadButton
            // 
            this.copySelectedConfigurationCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationCommonSettingsTabRadButton.Location = new System.Drawing.Point(274, 11);
            this.copySelectedConfigurationCommonSettingsTabRadButton.Name = "copySelectedConfigurationCommonSettingsTabRadButton";
            this.copySelectedConfigurationCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationCommonSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationCommonSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationCommonSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsCommonSettingsTabRadDropDownList
            // 
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Name = "templateConfigurationsCommonSettingsTabRadDropDownList";
            this.templateConfigurationsCommonSettingsTabRadDropDownList.Size = new System.Drawing.Size(254, 18);
            this.templateConfigurationsCommonSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsCommonSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsCommonSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsCommonSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton3
            // 
            this.radButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Location = new System.Drawing.Point(0, 232);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 70);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton3.ThemeName = "Office2007Black";
            // 
            // databaseInteractionCommonSettingsTabRadGroupBox
            // 
            this.databaseInteractionCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionCommonSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationCommonSettingsTabRadButton);
            this.databaseInteractionCommonSettingsTabRadGroupBox.Controls.Add(this.saveCurrentConfigurationCommonSettingsTabRadButton);
            this.databaseInteractionCommonSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseCommonSettingsTabRadButton);
            this.databaseInteractionCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionCommonSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(6, 365);
            this.databaseInteractionCommonSettingsTabRadGroupBox.Name = "databaseInteractionCommonSettingsTabRadGroupBox";
            this.databaseInteractionCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(158, 192);
            this.databaseInteractionCommonSettingsTabRadGroupBox.TabIndex = 52;
            this.databaseInteractionCommonSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationCommonSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Name = "deleteSelectedConfigurationCommonSettingsTabRadButton";
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.TabIndex = 45;
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationCommonSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveCurrentConfigurationCommonSettingsTabRadButton
            // 
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Name = "saveCurrentConfigurationCommonSettingsTabRadButton";
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveCurrentConfigurationCommonSettingsTabRadButton.TabIndex = 42;
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveCurrentConfigurationCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveCurrentConfigurationCommonSettingsTabRadButton.Click += new System.EventHandler(this.saveCurrentConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseCommonSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Name = "loadConfigurationFromDatabaseCommonSettingsTabRadButton";
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.TabIndex = 44;
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseCommonSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // deviceInteractionCommonSettingsTabRadGroupBox
            // 
            this.deviceInteractionCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionCommonSettingsTabRadGroupBox.Controls.Add(this.programDeviceCommonSettingsTabRadButton);
            this.deviceInteractionCommonSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceCommonSettingsTabRadButton);
            this.deviceInteractionCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionCommonSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(697, 365);
            this.deviceInteractionCommonSettingsTabRadGroupBox.Name = "deviceInteractionCommonSettingsTabRadGroupBox";
            this.deviceInteractionCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionCommonSettingsTabRadGroupBox.TabIndex = 52;
            this.deviceInteractionCommonSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceCommonSettingsTabRadButton
            // 
            this.programDeviceCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceCommonSettingsTabRadButton.Name = "programDeviceCommonSettingsTabRadButton";
            this.programDeviceCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceCommonSettingsTabRadButton.TabIndex = 41;
            this.programDeviceCommonSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceCommonSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceCommonSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Name = "loadConfigurationFromDeviceCommonSettingsTabRadButton";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceCommonSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // firmwareVersionValueRadLabel
            // 
            this.firmwareVersionValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareVersionValueRadLabel.Location = new System.Drawing.Point(546, 13);
            this.firmwareVersionValueRadLabel.Name = "firmwareVersionValueRadLabel";
            this.firmwareVersionValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.firmwareVersionValueRadLabel.TabIndex = 51;
            this.firmwareVersionValueRadLabel.Text = "0.00";
            // 
            // firmwareVersionTextRadLabel
            // 
            this.firmwareVersionTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareVersionTextRadLabel.Location = new System.Drawing.Point(445, 13);
            this.firmwareVersionTextRadLabel.Name = "firmwareVersionTextRadLabel";
            this.firmwareVersionTextRadLabel.Size = new System.Drawing.Size(95, 16);
            this.firmwareVersionTextRadLabel.TabIndex = 50;
            this.firmwareVersionTextRadLabel.Text = "Firmware Version";
            // 
            // commonSettingsRadProgressBar
            // 
            this.commonSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.commonSettingsRadProgressBar.Location = new System.Drawing.Point(570, 329);
            this.commonSettingsRadProgressBar.Name = "commonSettingsRadProgressBar";
            this.commonSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.commonSettingsRadProgressBar.TabIndex = 49;
            this.commonSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsCommonSettingsTabRadGroupBox
            // 
            this.availableConfigurationsCommonSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsCommonSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsCommonSettingsTabRadListControl);
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Controls.Add(this.radButton1);
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsCommonSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Location = new System.Drawing.Point(170, 365);
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Name = "availableConfigurationsCommonSettingsTabRadGroupBox";
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Size = new System.Drawing.Size(523, 192);
            this.availableConfigurationsCommonSettingsTabRadGroupBox.TabIndex = 43;
            this.availableConfigurationsCommonSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCommonSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsCommonSettingsTabRadListControl
            // 
            this.availableConfigurationsCommonSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsCommonSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsCommonSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsCommonSettingsTabRadListControl.Name = "availableConfigurationsCommonSettingsTabRadListControl";
            this.availableConfigurationsCommonSettingsTabRadListControl.Size = new System.Drawing.Size(503, 162);
            this.availableConfigurationsCommonSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsCommonSettingsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsCommonSettingsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsCommonSettingsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // saveRadGroupBox
            // 
            this.saveRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.saveRadGroupBox.Controls.Add(this.dayRadLabel);
            this.saveRadGroupBox.Controls.Add(this.daySavePRPDDRadMaskedEditBox);
            this.saveRadGroupBox.Controls.Add(this.measurementsRadLabel);
            this.saveRadGroupBox.Controls.Add(this.measurementsSavePRPDDRadMaskedEditBox);
            this.saveRadGroupBox.Controls.Add(this.savePRPDDRadLabel);
            this.saveRadGroupBox.Controls.Add(this.saveModeRadLabel);
            this.saveRadGroupBox.Controls.Add(this.normalSaveModeRadRadioButton);
            this.saveRadGroupBox.Controls.Add(this.testSaveModeRadRadioButton);
            this.saveRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveRadGroupBox.HeaderText = "Save";
            this.saveRadGroupBox.Location = new System.Drawing.Point(226, 87);
            this.saveRadGroupBox.Name = "saveRadGroupBox";
            this.saveRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.saveRadGroupBox.Size = new System.Drawing.Size(207, 108);
            this.saveRadGroupBox.TabIndex = 24;
            this.saveRadGroupBox.Text = "Save";
            this.saveRadGroupBox.ThemeName = "Office2007Black";
            // 
            // dayRadLabel
            // 
            this.dayRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dayRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dayRadLabel.Location = new System.Drawing.Point(180, 83);
            this.dayRadLabel.Name = "dayRadLabel";
            this.dayRadLabel.Size = new System.Drawing.Size(21, 15);
            this.dayRadLabel.TabIndex = 11;
            this.dayRadLabel.Text = "<html>day</html>";
            // 
            // daySavePRPDDRadMaskedEditBox
            // 
            this.daySavePRPDDRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daySavePRPDDRadMaskedEditBox.Location = new System.Drawing.Point(144, 80);
            this.daySavePRPDDRadMaskedEditBox.Name = "daySavePRPDDRadMaskedEditBox";
            this.daySavePRPDDRadMaskedEditBox.Size = new System.Drawing.Size(30, 18);
            this.daySavePRPDDRadMaskedEditBox.TabIndex = 10;
            this.daySavePRPDDRadMaskedEditBox.TabStop = false;
            this.daySavePRPDDRadMaskedEditBox.Text = "0";
            // 
            // measurementsRadLabel
            // 
            this.measurementsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.measurementsRadLabel.Location = new System.Drawing.Point(45, 83);
            this.measurementsRadLabel.Name = "measurementsRadLabel";
            this.measurementsRadLabel.Size = new System.Drawing.Size(92, 15);
            this.measurementsRadLabel.TabIndex = 9;
            this.measurementsRadLabel.Text = "<html>measurements on</html>";
            // 
            // measurementsSavePRPDDRadMaskedEditBox
            // 
            this.measurementsSavePRPDDRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementsSavePRPDDRadMaskedEditBox.Location = new System.Drawing.Point(9, 80);
            this.measurementsSavePRPDDRadMaskedEditBox.Name = "measurementsSavePRPDDRadMaskedEditBox";
            this.measurementsSavePRPDDRadMaskedEditBox.Size = new System.Drawing.Size(30, 18);
            this.measurementsSavePRPDDRadMaskedEditBox.TabIndex = 8;
            this.measurementsSavePRPDDRadMaskedEditBox.TabStop = false;
            this.measurementsSavePRPDDRadMaskedEditBox.Text = "0";
            // 
            // savePRPDDRadLabel
            // 
            this.savePRPDDRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savePRPDDRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.savePRPDDRadLabel.Location = new System.Drawing.Point(58, 59);
            this.savePRPDDRadLabel.Name = "savePRPDDRadLabel";
            this.savePRPDDRadLabel.Size = new System.Drawing.Size(71, 15);
            this.savePRPDDRadLabel.TabIndex = 7;
            this.savePRPDDRadLabel.Text = "<html>Save PRPDD</html>";
            // 
            // saveModeRadLabel
            // 
            this.saveModeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveModeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveModeRadLabel.Location = new System.Drawing.Point(24, 29);
            this.saveModeRadLabel.Name = "saveModeRadLabel";
            this.saveModeRadLabel.Size = new System.Drawing.Size(60, 15);
            this.saveModeRadLabel.TabIndex = 6;
            this.saveModeRadLabel.Text = "<html>Save Mode</html>";
            // 
            // normalSaveModeRadRadioButton
            // 
            this.normalSaveModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalSaveModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalSaveModeRadRadioButton.Location = new System.Drawing.Point(123, 17);
            this.normalSaveModeRadRadioButton.Name = "normalSaveModeRadRadioButton";
            this.normalSaveModeRadRadioButton.Size = new System.Drawing.Size(57, 16);
            this.normalSaveModeRadRadioButton.TabIndex = 1;
            this.normalSaveModeRadRadioButton.Text = "Normal";
            this.normalSaveModeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.normalSaveModeRadRadioButton_ToggleStateChanged);
            // 
            // testSaveModeRadRadioButton
            // 
            this.testSaveModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testSaveModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.testSaveModeRadRadioButton.Location = new System.Drawing.Point(123, 35);
            this.testSaveModeRadRadioButton.Name = "testSaveModeRadRadioButton";
            this.testSaveModeRadRadioButton.Size = new System.Drawing.Size(42, 16);
            this.testSaveModeRadRadioButton.TabIndex = 0;
            this.testSaveModeRadRadioButton.Text = "Test";
            this.testSaveModeRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.testSaveModeRadRadioButton_ToggleStateChanged);
            // 
            // connectionSettingsRadGroupBox
            // 
            this.connectionSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.connectionSettingsRadGroupBox.Controls.Add(this.modbusAddressRadLabel);
            this.connectionSettingsRadGroupBox.Controls.Add(this.modBusAddressRadMaskedEditBox);
            this.connectionSettingsRadGroupBox.Controls.Add(this.baudRateRadLabel);
            this.connectionSettingsRadGroupBox.Controls.Add(this.baudRateRadDropDownList);
            this.connectionSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectionSettingsRadGroupBox.HeaderText = "Connection Settings";
            this.connectionSettingsRadGroupBox.Location = new System.Drawing.Point(226, 3);
            this.connectionSettingsRadGroupBox.Name = "connectionSettingsRadGroupBox";
            this.connectionSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.connectionSettingsRadGroupBox.Size = new System.Drawing.Size(207, 78);
            this.connectionSettingsRadGroupBox.TabIndex = 23;
            this.connectionSettingsRadGroupBox.Text = "Connection Settings";
            this.connectionSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // modbusAddressRadLabel
            // 
            this.modbusAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressRadLabel.Location = new System.Drawing.Point(13, 26);
            this.modbusAddressRadLabel.Name = "modbusAddressRadLabel";
            this.modbusAddressRadLabel.Size = new System.Drawing.Size(88, 15);
            this.modbusAddressRadLabel.TabIndex = 7;
            this.modbusAddressRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressRadMaskedEditBox
            // 
            this.modBusAddressRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressRadMaskedEditBox.Location = new System.Drawing.Point(113, 26);
            this.modBusAddressRadMaskedEditBox.Name = "modBusAddressRadMaskedEditBox";
            this.modBusAddressRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressRadMaskedEditBox.TabIndex = 6;
            this.modBusAddressRadMaskedEditBox.TabStop = false;
            this.modBusAddressRadMaskedEditBox.Text = "0";
            // 
            // baudRateRadLabel
            // 
            this.baudRateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.baudRateRadLabel.Location = new System.Drawing.Point(14, 55);
            this.baudRateRadLabel.Name = "baudRateRadLabel";
            this.baudRateRadLabel.Size = new System.Drawing.Size(56, 15);
            this.baudRateRadLabel.TabIndex = 5;
            this.baudRateRadLabel.Text = "<html>Baud Rate</html>";
            // 
            // baudRateRadDropDownList
            // 
            this.baudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem2.Text = "38400";
            radListDataItem3.Text = "57600";
            radListDataItem4.Text = "115200";
            this.baudRateRadDropDownList.Items.Add(radListDataItem1);
            this.baudRateRadDropDownList.Items.Add(radListDataItem2);
            this.baudRateRadDropDownList.Items.Add(radListDataItem3);
            this.baudRateRadDropDownList.Items.Add(radListDataItem4);
            this.baudRateRadDropDownList.Location = new System.Drawing.Point(113, 50);
            this.baudRateRadDropDownList.Name = "baudRateRadDropDownList";
            this.baudRateRadDropDownList.Size = new System.Drawing.Size(86, 18);
            this.baudRateRadDropDownList.TabIndex = 4;
            this.baudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // ratedVoltageRadGroupBox
            // 
            this.ratedVoltageRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels1to3RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV4RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV3RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV2RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.kV1RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels13to15RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels7to12RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels4to6RatedVoltageRadMaskedEditBox);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels13to15RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels7to12RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels4to6RadLabel);
            this.ratedVoltageRadGroupBox.Controls.Add(this.channels1to3RadLabel);
            this.ratedVoltageRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ratedVoltageRadGroupBox.HeaderText = "Rated Voltage";
            this.ratedVoltageRadGroupBox.Location = new System.Drawing.Point(3, 60);
            this.ratedVoltageRadGroupBox.Name = "ratedVoltageRadGroupBox";
            this.ratedVoltageRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.ratedVoltageRadGroupBox.Size = new System.Drawing.Size(217, 135);
            this.ratedVoltageRadGroupBox.TabIndex = 22;
            this.ratedVoltageRadGroupBox.Text = "Rated Voltage";
            this.ratedVoltageRadGroupBox.ThemeName = "Office2007Black";
            // 
            // channels1to3RatedVoltageRadMaskedEditBox
            // 
            this.channels1to3RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels1to3RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 29);
            this.channels1to3RatedVoltageRadMaskedEditBox.Name = "channels1to3RatedVoltageRadMaskedEditBox";
            this.channels1to3RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels1to3RatedVoltageRadMaskedEditBox.TabIndex = 32;
            this.channels1to3RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels1to3RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // kV4RadLabel
            // 
            this.kV4RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV4RadLabel.Location = new System.Drawing.Point(190, 101);
            this.kV4RadLabel.Name = "kV4RadLabel";
            this.kV4RadLabel.Size = new System.Drawing.Size(19, 18);
            this.kV4RadLabel.TabIndex = 31;
            this.kV4RadLabel.Text = "kV";
            // 
            // kV3RadLabel
            // 
            this.kV3RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV3RadLabel.Location = new System.Drawing.Point(190, 77);
            this.kV3RadLabel.Name = "kV3RadLabel";
            this.kV3RadLabel.Size = new System.Drawing.Size(19, 18);
            this.kV3RadLabel.TabIndex = 31;
            this.kV3RadLabel.Text = "kV";
            // 
            // kV2RadLabel
            // 
            this.kV2RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV2RadLabel.Location = new System.Drawing.Point(190, 55);
            this.kV2RadLabel.Name = "kV2RadLabel";
            this.kV2RadLabel.Size = new System.Drawing.Size(19, 18);
            this.kV2RadLabel.TabIndex = 31;
            this.kV2RadLabel.Text = "kV";
            // 
            // kV1RadLabel
            // 
            this.kV1RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.kV1RadLabel.Location = new System.Drawing.Point(190, 31);
            this.kV1RadLabel.Name = "kV1RadLabel";
            this.kV1RadLabel.Size = new System.Drawing.Size(19, 18);
            this.kV1RadLabel.TabIndex = 30;
            this.kV1RadLabel.Text = "kV";
            // 
            // channels13to15RatedVoltageRadMaskedEditBox
            // 
            this.channels13to15RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels13to15RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 101);
            this.channels13to15RatedVoltageRadMaskedEditBox.Name = "channels13to15RatedVoltageRadMaskedEditBox";
            this.channels13to15RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels13to15RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels13to15RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels13to15RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels7to12RatedVoltageRadMaskedEditBox
            // 
            this.channels7to12RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels7to12RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 77);
            this.channels7to12RatedVoltageRadMaskedEditBox.Name = "channels7to12RatedVoltageRadMaskedEditBox";
            this.channels7to12RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels7to12RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels7to12RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels7to12RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels4to6RatedVoltageRadMaskedEditBox
            // 
            this.channels4to6RatedVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.channels4to6RatedVoltageRadMaskedEditBox.Location = new System.Drawing.Point(131, 53);
            this.channels4to6RatedVoltageRadMaskedEditBox.Name = "channels4to6RatedVoltageRadMaskedEditBox";
            this.channels4to6RatedVoltageRadMaskedEditBox.Size = new System.Drawing.Size(53, 18);
            this.channels4to6RatedVoltageRadMaskedEditBox.TabIndex = 29;
            this.channels4to6RatedVoltageRadMaskedEditBox.TabStop = false;
            this.channels4to6RatedVoltageRadMaskedEditBox.Text = "0";
            // 
            // channels13to15RadLabel
            // 
            this.channels13to15RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels13to15RadLabel.Location = new System.Drawing.Point(13, 101);
            this.channels13to15RadLabel.Name = "channels13to15RadLabel";
            this.channels13to15RadLabel.Size = new System.Drawing.Size(88, 18);
            this.channels13to15RadLabel.TabIndex = 27;
            this.channels13to15RadLabel.Text = "Channels N1-N3";
            // 
            // channels7to12RadLabel
            // 
            this.channels7to12RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels7to12RadLabel.Location = new System.Drawing.Point(13, 77);
            this.channels7to12RadLabel.Name = "channels7to12RadLabel";
            this.channels7to12RadLabel.Size = new System.Drawing.Size(100, 18);
            this.channels7to12RadLabel.TabIndex = 26;
            this.channels7to12RadLabel.Text = "Channels PD1-PD6";
            // 
            // channels4to6RadLabel
            // 
            this.channels4to6RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels4to6RadLabel.Location = new System.Drawing.Point(13, 55);
            this.channels4to6RadLabel.Name = "channels4to6RadLabel";
            this.channels4to6RadLabel.Size = new System.Drawing.Size(102, 18);
            this.channels4to6RadLabel.TabIndex = 25;
            this.channels4to6RadLabel.Text = "Channels Ch.a-Ch.c";
            // 
            // channels1to3RadLabel
            // 
            this.channels1to3RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.channels1to3RadLabel.Location = new System.Drawing.Point(13, 31);
            this.channels1to3RadLabel.Name = "channels1to3RadLabel";
            this.channels1to3RadLabel.Size = new System.Drawing.Size(105, 18);
            this.channels1to3RadLabel.TabIndex = 24;
            this.channels1to3RadLabel.Text = "Channels Ch.A-Ch.C";
            // 
            // monitoringRadGroupBox
            // 
            this.monitoringRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.monitoringRadGroupBox.Controls.Add(this.disableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Controls.Add(this.enableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitoringRadGroupBox.HeaderText = "Monitoring";
            this.monitoringRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.monitoringRadGroupBox.Name = "monitoringRadGroupBox";
            this.monitoringRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.monitoringRadGroupBox.Size = new System.Drawing.Size(148, 51);
            this.monitoringRadGroupBox.TabIndex = 21;
            this.monitoringRadGroupBox.Text = "Monitoring";
            this.monitoringRadGroupBox.ThemeName = "Office2007Black";
            // 
            // disableMonitoringRadRadioButton
            // 
            this.disableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableMonitoringRadRadioButton.Location = new System.Drawing.Point(78, 23);
            this.disableMonitoringRadRadioButton.Name = "disableMonitoringRadRadioButton";
            this.disableMonitoringRadRadioButton.Size = new System.Drawing.Size(58, 16);
            this.disableMonitoringRadRadioButton.TabIndex = 1;
            this.disableMonitoringRadRadioButton.Text = "Disable";
            // 
            // enableMonitoringRadRadioButton
            // 
            this.enableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableMonitoringRadRadioButton.Location = new System.Drawing.Point(13, 23);
            this.enableMonitoringRadRadioButton.Name = "enableMonitoringRadRadioButton";
            this.enableMonitoringRadRadioButton.Size = new System.Drawing.Size(56, 16);
            this.enableMonitoringRadRadioButton.TabIndex = 0;
            this.enableMonitoringRadRadioButton.Text = "Enable";
            // 
            // measurementScheduleRadGroupBox
            // 
            this.measurementScheduleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.measurementScheduleRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.measurementScheduleRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.measurementScheduleRadGroupBox.Controls.Add(this.measurementSettingsRadGridView);
            this.measurementScheduleRadGroupBox.Controls.Add(this.byScheduledMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Controls.Add(this.minuteRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.hourRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.minuteMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.hourMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.stepMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementScheduleRadGroupBox.HeaderText = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.Location = new System.Drawing.Point(631, 3);
            this.measurementScheduleRadGroupBox.Name = "measurementScheduleRadGroupBox";
            this.measurementScheduleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.measurementScheduleRadGroupBox.Size = new System.Drawing.Size(217, 306);
            this.measurementScheduleRadGroupBox.TabIndex = 5;
            this.measurementScheduleRadGroupBox.Text = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // measurementSettingsRadGridView
            // 
            this.measurementSettingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementSettingsRadGridView.Location = new System.Drawing.Point(13, 83);
            this.measurementSettingsRadGridView.Name = "measurementSettingsRadGridView";
            this.measurementSettingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.measurementSettingsRadGridView.Size = new System.Drawing.Size(191, 210);
            this.measurementSettingsRadGridView.TabIndex = 25;
            this.measurementSettingsRadGridView.Text = "radGridView1";
            this.measurementSettingsRadGridView.ThemeName = "Office2007Black";
            // 
            // byScheduledMeasurementScheduleRadRadioButton
            // 
            this.byScheduledMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.byScheduledMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.byScheduledMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 58);
            this.byScheduledMeasurementScheduleRadRadioButton.Name = "byScheduledMeasurementScheduleRadRadioButton";
            this.byScheduledMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(84, 16);
            this.byScheduledMeasurementScheduleRadRadioButton.TabIndex = 24;
            this.byScheduledMeasurementScheduleRadRadioButton.Text = "By Schedule";
            this.byScheduledMeasurementScheduleRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.byScheduledMeasurementScheduleRadRadioButton_ToggleStateChanged);
            // 
            // minuteRadLabel
            // 
            this.minuteRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.minuteRadLabel.Location = new System.Drawing.Point(170, 35);
            this.minuteRadLabel.Name = "minuteRadLabel";
            this.minuteRadLabel.Size = new System.Drawing.Size(42, 18);
            this.minuteRadLabel.TabIndex = 23;
            this.minuteRadLabel.Text = "Minute";
            // 
            // hourRadLabel
            // 
            this.hourRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hourRadLabel.Location = new System.Drawing.Point(101, 35);
            this.hourRadLabel.Name = "hourRadLabel";
            this.hourRadLabel.Size = new System.Drawing.Size(31, 18);
            this.hourRadLabel.TabIndex = 22;
            this.hourRadLabel.Text = "Hour";
            // 
            // minuteMeasurementScheduleRadMaskedEditBox
            // 
            this.minuteMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minuteMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(138, 33);
            this.minuteMeasurementScheduleRadMaskedEditBox.Name = "minuteMeasurementScheduleRadMaskedEditBox";
            this.minuteMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.minuteMeasurementScheduleRadMaskedEditBox.TabIndex = 21;
            this.minuteMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.minuteMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // hourMeasurementScheduleRadMaskedEditBox
            // 
            this.hourMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(69, 33);
            this.hourMeasurementScheduleRadMaskedEditBox.Name = "hourMeasurementScheduleRadMaskedEditBox";
            this.hourMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.hourMeasurementScheduleRadMaskedEditBox.TabIndex = 20;
            this.hourMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.hourMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // stepMeasurementScheduleRadRadioButton
            // 
            this.stepMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stepMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 33);
            this.stepMeasurementScheduleRadRadioButton.Name = "stepMeasurementScheduleRadRadioButton";
            this.stepMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(49, 16);
            this.stepMeasurementScheduleRadRadioButton.TabIndex = 0;
            this.stepMeasurementScheduleRadRadioButton.Text = "Every";
            this.stepMeasurementScheduleRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.stepMeasurementScheduleRadRadioButton_ToggleStateChanged);
            // 
            // pdSettingsRadPageViewPage
            // 
            this.pdSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.pdSettingsRadPageViewPage.Controls.Add(this.saveAsCurrentConfigurationPDSettingsTabRadButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsPDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.deviceInteractionPDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.advancedSettingsWarningRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.databaseInteractionPDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.advancedSettingsRadButton);
            this.pdSettingsRadPageViewPage.Controls.Add(this.pdSettingsRadProgressBar);
            this.pdSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsPDSettingsTabRadGroupBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.rereadOnAlarmRadCheckBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.commonPhaseShiftRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.degreesRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.cyclesPerAcquisitionRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.hzRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationFrequencyRadMaskedEditBox);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationRadDropDownList);
            this.pdSettingsRadPageViewPage.Controls.Add(this.commonPhaseShiftRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.cyclesPerAcquisitionRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.synchronizationRadLabel);
            this.pdSettingsRadPageViewPage.Controls.Add(this.channelConfigurationRadGridView);
            this.pdSettingsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
           // this.pdSettingsRadPageViewPage.ItemSize = new System.Drawing.SizeF(76F, 26F);
            this.pdSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.pdSettingsRadPageViewPage.Name = "pdSettingsRadPageViewPage";
            this.pdSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 560);
            this.pdSettingsRadPageViewPage.Text = "PD Settings";
            // 
            // saveAsCurrentConfigurationPDSettingsTabRadButton
            // 
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Location = new System.Drawing.Point(424, 306);
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Name = "saveAsCurrentConfigurationPDSettingsTabRadButton";
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.TabIndex = 46;
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Text = "<html>Save Configuration<br>as<br>Current Configuration</html>";
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Visible = false;
            this.saveAsCurrentConfigurationPDSettingsTabRadButton.Click += new System.EventHandler(this.saveConfigurationAsCurrentDeviceConfigurationRadButton_Click);
            // 
            // templateConfigurationsPDSettingsTabRadGroupBox
            // 
            this.templateConfigurationsPDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsPDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsPDSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationPDSettingsTabRadButton);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsPDSettingsTabRadDropDownList);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.radButton7);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsPDSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsPDSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 295);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Name = "templateConfigurationsPDSettingsTabRadGroupBox";
            this.templateConfigurationsPDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsPDSettingsTabRadGroupBox.Size = new System.Drawing.Size(402, 65);
            this.templateConfigurationsPDSettingsTabRadGroupBox.TabIndex = 53;
            this.templateConfigurationsPDSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsPDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationPDSettingsTabRadButton
            // 
            this.copySelectedConfigurationPDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationPDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationPDSettingsTabRadButton.Location = new System.Drawing.Point(274, 11);
            this.copySelectedConfigurationPDSettingsTabRadButton.Name = "copySelectedConfigurationPDSettingsTabRadButton";
            this.copySelectedConfigurationPDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationPDSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationPDSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationPDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationPDSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsPDSettingsTabRadDropDownList
            // 
            this.templateConfigurationsPDSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsPDSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsPDSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsPDSettingsTabRadDropDownList.Name = "templateConfigurationsPDSettingsTabRadDropDownList";
            this.templateConfigurationsPDSettingsTabRadDropDownList.Size = new System.Drawing.Size(254, 18);
            this.templateConfigurationsPDSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsPDSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsPDSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsPDSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton7
            // 
            this.radButton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton7.Location = new System.Drawing.Point(0, 232);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(130, 70);
            this.radButton7.TabIndex = 8;
            this.radButton7.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton7.ThemeName = "Office2007Black";
            // 
            // deviceInteractionPDSettingsTabRadGroupBox
            // 
            this.deviceInteractionPDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionPDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionPDSettingsTabRadGroupBox.Controls.Add(this.programDevicePDSettingsTabRadButton);
            this.deviceInteractionPDSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDevicePDSettingsTabRadButton);
            this.deviceInteractionPDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionPDSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionPDSettingsTabRadGroupBox.Location = new System.Drawing.Point(697, 365);
            this.deviceInteractionPDSettingsTabRadGroupBox.Name = "deviceInteractionPDSettingsTabRadGroupBox";
            this.deviceInteractionPDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionPDSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionPDSettingsTabRadGroupBox.TabIndex = 52;
            this.deviceInteractionPDSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionPDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDevicePDSettingsTabRadButton
            // 
            this.programDevicePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDevicePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDevicePDSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDevicePDSettingsTabRadButton.Name = "programDevicePDSettingsTabRadButton";
            this.programDevicePDSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDevicePDSettingsTabRadButton.TabIndex = 41;
            this.programDevicePDSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDevicePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDevicePDSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDevicePDSettingsTabRadButton
            // 
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Name = "loadConfigurationFromDevicePDSettingsTabRadButton";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDevicePDSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDevicePDSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // advancedSettingsWarningRadLabel
            // 
            this.advancedSettingsWarningRadLabel.BackColor = System.Drawing.Color.White;
            this.advancedSettingsWarningRadLabel.ForeColor = System.Drawing.Color.Red;
            this.advancedSettingsWarningRadLabel.Location = new System.Drawing.Point(573, 10);
            this.advancedSettingsWarningRadLabel.Name = "advancedSettingsWarningRadLabel";
            this.advancedSettingsWarningRadLabel.Size = new System.Drawing.Size(261, 18);
            this.advancedSettingsWarningRadLabel.TabIndex = 51;
            this.advancedSettingsWarningRadLabel.Text = "Do not change advanced settings unless instructed";
            this.advancedSettingsWarningRadLabel.Visible = false;
            // 
            // databaseInteractionPDSettingsTabRadGroupBox
            // 
            this.databaseInteractionPDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionPDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionPDSettingsTabRadGroupBox.Controls.Add(this.saveCurrentConfigurationPDSettingsTabRadButton);
            this.databaseInteractionPDSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationPDSettingsTabRadButton);
            this.databaseInteractionPDSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabasePDSettingsTabRadButton);
            this.databaseInteractionPDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionPDSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionPDSettingsTabRadGroupBox.Location = new System.Drawing.Point(6, 365);
            this.databaseInteractionPDSettingsTabRadGroupBox.Name = "databaseInteractionPDSettingsTabRadGroupBox";
            this.databaseInteractionPDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionPDSettingsTabRadGroupBox.Size = new System.Drawing.Size(157, 192);
            this.databaseInteractionPDSettingsTabRadGroupBox.TabIndex = 51;
            this.databaseInteractionPDSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionPDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // saveCurrentConfigurationPDSettingsTabRadButton
            // 
            this.saveCurrentConfigurationPDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveCurrentConfigurationPDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveCurrentConfigurationPDSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveCurrentConfigurationPDSettingsTabRadButton.Name = "saveCurrentConfigurationPDSettingsTabRadButton";
            this.saveCurrentConfigurationPDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveCurrentConfigurationPDSettingsTabRadButton.TabIndex = 42;
            this.saveCurrentConfigurationPDSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveCurrentConfigurationPDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveCurrentConfigurationPDSettingsTabRadButton.Click += new System.EventHandler(this.saveCurrentConfigurationRadButton_Click);
            // 
            // deleteSelectedConfigurationPDSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Name = "deleteSelectedConfigurationPDSettingsTabRadButton";
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationPDSettingsTabRadButton.TabIndex = 45;
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationPDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationPDSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabasePDSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Name = "loadConfigurationFromDatabasePDSettingsTabRadButton";
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.TabIndex = 44;
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabasePDSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // advancedSettingsRadButton
            // 
            this.advancedSettingsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advancedSettingsRadButton.Location = new System.Drawing.Point(621, 32);
            this.advancedSettingsRadButton.Name = "advancedSettingsRadButton";
            this.advancedSettingsRadButton.Size = new System.Drawing.Size(182, 22);
            this.advancedSettingsRadButton.TabIndex = 50;
            this.advancedSettingsRadButton.Text = "Show Advanced Settings";
            this.advancedSettingsRadButton.ThemeName = "Office2007Black";
            this.advancedSettingsRadButton.Click += new System.EventHandler(this.advancedSettingsRadButton_Click);
            // 
            // pdSettingsRadProgressBar
            // 
            this.pdSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pdSettingsRadProgressBar.Location = new System.Drawing.Point(570, 322);
            this.pdSettingsRadProgressBar.Name = "pdSettingsRadProgressBar";
            this.pdSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.pdSettingsRadProgressBar.TabIndex = 49;
            this.pdSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsPDSettingsTabRadGroupBox
            // 
            this.availableConfigurationsPDSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsPDSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsPDSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsPDSettingsTabRadListControl);
            this.availableConfigurationsPDSettingsTabRadGroupBox.Controls.Add(this.radButton5);
            this.availableConfigurationsPDSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsPDSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsPDSettingsTabRadGroupBox.Location = new System.Drawing.Point(170, 365);
            this.availableConfigurationsPDSettingsTabRadGroupBox.Name = "availableConfigurationsPDSettingsTabRadGroupBox";
            this.availableConfigurationsPDSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsPDSettingsTabRadGroupBox.Size = new System.Drawing.Size(523, 192);
            this.availableConfigurationsPDSettingsTabRadGroupBox.TabIndex = 43;
            this.availableConfigurationsPDSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsPDSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsPDSettingsTabRadListControl
            // 
            this.availableConfigurationsPDSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsPDSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsPDSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsPDSettingsTabRadListControl.Name = "availableConfigurationsPDSettingsTabRadListControl";
            this.availableConfigurationsPDSettingsTabRadListControl.Size = new System.Drawing.Size(503, 162);
            this.availableConfigurationsPDSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsPDSettingsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsPDSettingsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsPDSettingsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton5
            // 
            this.radButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(0, 232);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(130, 70);
            this.radButton5.TabIndex = 8;
            this.radButton5.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton5.ThemeName = "Office2007Black";
            // 
            // rereadOnAlarmRadCheckBox
            // 
            this.rereadOnAlarmRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rereadOnAlarmRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rereadOnAlarmRadCheckBox.Location = new System.Drawing.Point(14, 38);
            this.rereadOnAlarmRadCheckBox.Name = "rereadOnAlarmRadCheckBox";
            this.rereadOnAlarmRadCheckBox.Size = new System.Drawing.Size(104, 16);
            this.rereadOnAlarmRadCheckBox.TabIndex = 13;
            this.rereadOnAlarmRadCheckBox.Text = "Reread on alarm";
            // 
            // commonPhaseShiftRadMaskedEditBox
            // 
            this.commonPhaseShiftRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonPhaseShiftRadMaskedEditBox.Location = new System.Drawing.Point(435, 38);
            this.commonPhaseShiftRadMaskedEditBox.Name = "commonPhaseShiftRadMaskedEditBox";
            this.commonPhaseShiftRadMaskedEditBox.Size = new System.Drawing.Size(42, 18);
            this.commonPhaseShiftRadMaskedEditBox.TabIndex = 12;
            this.commonPhaseShiftRadMaskedEditBox.TabStop = false;
            this.commonPhaseShiftRadMaskedEditBox.Text = "0";
            this.commonPhaseShiftRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // degreesRadLabel
            // 
            this.degreesRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.degreesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.degreesRadLabel.Location = new System.Drawing.Point(483, 38);
            this.degreesRadLabel.Name = "degreesRadLabel";
            this.degreesRadLabel.Size = new System.Drawing.Size(47, 16);
            this.degreesRadLabel.TabIndex = 11;
            this.degreesRadLabel.Text = "degrees";
            this.degreesRadLabel.ThemeName = "Office2007Black";
            // 
            // cyclesPerAcquisitionRadMaskedEditBox
            // 
            this.cyclesPerAcquisitionRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cyclesPerAcquisitionRadMaskedEditBox.Location = new System.Drawing.Point(435, 10);
            this.cyclesPerAcquisitionRadMaskedEditBox.Name = "cyclesPerAcquisitionRadMaskedEditBox";
            this.cyclesPerAcquisitionRadMaskedEditBox.Size = new System.Drawing.Size(42, 18);
            this.cyclesPerAcquisitionRadMaskedEditBox.TabIndex = 10;
            this.cyclesPerAcquisitionRadMaskedEditBox.TabStop = false;
            this.cyclesPerAcquisitionRadMaskedEditBox.Text = "0";
            this.cyclesPerAcquisitionRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // hzRadLabel
            // 
            this.hzRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hzRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hzRadLabel.Location = new System.Drawing.Point(272, 10);
            this.hzRadLabel.Name = "hzRadLabel";
            this.hzRadLabel.Size = new System.Drawing.Size(20, 16);
            this.hzRadLabel.TabIndex = 9;
            this.hzRadLabel.Text = "Hz";
            this.hzRadLabel.ThemeName = "Office2007Black";
            // 
            // synchronizationFrequencyRadMaskedEditBox
            // 
            this.synchronizationFrequencyRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synchronizationFrequencyRadMaskedEditBox.Location = new System.Drawing.Point(224, 10);
            this.synchronizationFrequencyRadMaskedEditBox.Name = "synchronizationFrequencyRadMaskedEditBox";
            this.synchronizationFrequencyRadMaskedEditBox.Size = new System.Drawing.Size(42, 18);
            this.synchronizationFrequencyRadMaskedEditBox.TabIndex = 8;
            this.synchronizationFrequencyRadMaskedEditBox.TabStop = false;
            this.synchronizationFrequencyRadMaskedEditBox.Text = "0";
            this.synchronizationFrequencyRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // synchronizationRadDropDownList
            // 
            this.synchronizationRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem5.Text = "internal";
            radListDataItem6.Text = "From PD Ch A";
            radListDataItem7.Text = "From Bus";
            radListDataItem8.Text = "external (Ref)";
            this.synchronizationRadDropDownList.Items.Add(radListDataItem5);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem6);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem7);
            this.synchronizationRadDropDownList.Items.Add(radListDataItem8);
            this.synchronizationRadDropDownList.Location = new System.Drawing.Point(107, 10);
            this.synchronizationRadDropDownList.Name = "synchronizationRadDropDownList";
            this.synchronizationRadDropDownList.Size = new System.Drawing.Size(111, 18);
            this.synchronizationRadDropDownList.TabIndex = 7;
            this.synchronizationRadDropDownList.ThemeName = "Office2007Black";
            this.synchronizationRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.synchronizationRadDropDownList_SelectedIndexChanged);
            this.synchronizationRadDropDownList.SelectedValueChanged += new System.EventHandler(this.synchronizationRadDropDownList_SelectedValueChanged);
            // 
            // commonPhaseShiftRadLabel
            // 
            this.commonPhaseShiftRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonPhaseShiftRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commonPhaseShiftRadLabel.Location = new System.Drawing.Point(319, 38);
            this.commonPhaseShiftRadLabel.Name = "commonPhaseShiftRadLabel";
            this.commonPhaseShiftRadLabel.Size = new System.Drawing.Size(113, 16);
            this.commonPhaseShiftRadLabel.TabIndex = 3;
            this.commonPhaseShiftRadLabel.Text = "Common Phase Shift";
            this.commonPhaseShiftRadLabel.ThemeName = "Office2007Black";
            // 
            // cyclesPerAcquisitionRadLabel
            // 
            this.cyclesPerAcquisitionRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cyclesPerAcquisitionRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cyclesPerAcquisitionRadLabel.Location = new System.Drawing.Point(319, 10);
            this.cyclesPerAcquisitionRadLabel.Name = "cyclesPerAcquisitionRadLabel";
            this.cyclesPerAcquisitionRadLabel.Size = new System.Drawing.Size(118, 16);
            this.cyclesPerAcquisitionRadLabel.TabIndex = 2;
            this.cyclesPerAcquisitionRadLabel.Text = "Cycles per Acquisition";
            this.cyclesPerAcquisitionRadLabel.ThemeName = "Office2007Black";
            // 
            // synchronizationRadLabel
            // 
            this.synchronizationRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.synchronizationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.synchronizationRadLabel.Location = new System.Drawing.Point(14, 16);
            this.synchronizationRadLabel.Name = "synchronizationRadLabel";
            this.synchronizationRadLabel.Size = new System.Drawing.Size(87, 16);
            this.synchronizationRadLabel.TabIndex = 1;
            this.synchronizationRadLabel.Text = "Synchronization";
            this.synchronizationRadLabel.ThemeName = "Office2007Black";
            // 
            // channelConfigurationRadGridView
            // 
            this.channelConfigurationRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.channelConfigurationRadGridView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.channelConfigurationRadGridView.Cursor = System.Windows.Forms.Cursors.Default;
            this.channelConfigurationRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.channelConfigurationRadGridView.ForeColor = System.Drawing.SystemColors.ControlText;
            this.channelConfigurationRadGridView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.channelConfigurationRadGridView.Location = new System.Drawing.Point(0, 60);
            this.channelConfigurationRadGridView.Name = "channelConfigurationRadGridView";
            this.channelConfigurationRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.channelConfigurationRadGridView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.channelConfigurationRadGridView.Size = new System.Drawing.Size(851, 229);
            this.channelConfigurationRadGridView.TabIndex = 0;
            this.channelConfigurationRadGridView.Text = "radGridView1";
            this.channelConfigurationRadGridView.ThemeName = "Office2007Black";
            this.channelConfigurationRadGridView.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.channelConfigurationRadGridView_CellEditorInitialized);
            this.channelConfigurationRadGridView.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.channelConfigurationRadGridView_ContextMenuOpening);
            this.channelConfigurationRadGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.channelConfigurationRadGridView_MouseClick);
            // 
            // alarmSettingsRadPageViewPage
            // 
            this.alarmSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.alarmSettingsRadPageViewPage.Controls.Add(this.saveAsCurrentConfigurationAlarmSettingsTabRadButton);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.databaseInteractionAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.deviceInteractionAlarmSettingsTabRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmSettingsRadProgressBar);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.commonAlarmSettingsRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.relayModeRadGroupBox);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmSettingsRadGridView);
            this.alarmSettingsRadPageViewPage.Controls.Add(this.alarmEnableRadGroupBox);
          //  this.alarmSettingsRadPageViewPage.ItemSize = new System.Drawing.SizeF(90F, 26F);
            this.alarmSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.alarmSettingsRadPageViewPage.Name = "alarmSettingsRadPageViewPage";
            this.alarmSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 560);
            this.alarmSettingsRadPageViewPage.Text = "Alarm Settings";
            this.alarmSettingsRadPageViewPage.Paint += new System.Windows.Forms.PaintEventHandler(this.alarmSettingsRadPageViewPage_Paint);
            // 
            // saveAsCurrentConfigurationAlarmSettingsTabRadButton
            // 
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Location = new System.Drawing.Point(421, 270);
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Name = "saveAsCurrentConfigurationAlarmSettingsTabRadButton";
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.TabIndex = 45;
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Text = "<html>Save Configuration<br>as<br>Current Configuration</html>";
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveAsCurrentConfigurationAlarmSettingsTabRadButton.Visible = false;
            // 
            // availableConfigurationsAlarmSettingsTabRadGroupBox
            // 
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsAlarmSettingsTabRadListControl);
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.radButton4);
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(170, 365);
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Name = "availableConfigurationsAlarmSettingsTabRadGroupBox";
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(523, 192);
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.TabIndex = 44;
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsAlarmSettingsTabRadListControl
            // 
            this.availableConfigurationsAlarmSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsAlarmSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsAlarmSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsAlarmSettingsTabRadListControl.Name = "availableConfigurationsAlarmSettingsTabRadListControl";
            this.availableConfigurationsAlarmSettingsTabRadListControl.Size = new System.Drawing.Size(503, 162);
            this.availableConfigurationsAlarmSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsAlarmSettingsTabRadListControl.Text = "radListControl2";
            // 
            // radButton4
            // 
            this.radButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton4.Location = new System.Drawing.Point(0, 232);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(130, 70);
            this.radButton4.TabIndex = 8;
            this.radButton4.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton4.ThemeName = "Office2007Black";
            // 
            // databaseInteractionAlarmSettingsTabRadGroupBox
            // 
            this.databaseInteractionAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationAlarmSettingsTabRadButton);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Controls.Add(this.saveCurrentConfigurationAlarmSettingsTabRadButton);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionAlarmSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(6, 365);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Name = "databaseInteractionAlarmSettingsTabRadGroupBox";
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(154, 192);
            this.databaseInteractionAlarmSettingsTabRadGroupBox.TabIndex = 53;
            this.databaseInteractionAlarmSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationAlarmSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Name = "deleteSelectedConfigurationAlarmSettingsTabRadButton";
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.TabIndex = 43;
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationAlarmSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveCurrentConfigurationAlarmSettingsTabRadButton
            // 
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Name = "saveCurrentConfigurationAlarmSettingsTabRadButton";
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.TabIndex = 42;
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveCurrentConfigurationAlarmSettingsTabRadButton.Click += new System.EventHandler(this.saveCurrentConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseAlarmSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Name = "loadConfigurationFromDatabaseAlarmSettingsTabRadButton";
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.TabIndex = 44;
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // templateConfigurationsAlarmSettingsTabRadGroupBox
            // 
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationAlarmSettingsTabRadButton);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsAlarmSettingsTabRadDropDownList);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Controls.Add(this.radButton9);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 275);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Name = "templateConfigurationsAlarmSettingsTabRadGroupBox";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(402, 65);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.TabIndex = 52;
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationAlarmSettingsTabRadButton
            // 
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Location = new System.Drawing.Point(274, 11);
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Name = "copySelectedConfigurationAlarmSettingsTabRadButton";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationAlarmSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationAlarmSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsAlarmSettingsTabRadDropDownList
            // 
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Name = "templateConfigurationsAlarmSettingsTabRadDropDownList";
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.Size = new System.Drawing.Size(254, 18);
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsAlarmSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsAlarmSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton9
            // 
            this.radButton9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton9.Location = new System.Drawing.Point(0, 232);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(130, 70);
            this.radButton9.TabIndex = 8;
            this.radButton9.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton9.ThemeName = "Office2007Black";
            // 
            // deviceInteractionAlarmSettingsTabRadGroupBox
            // 
            this.deviceInteractionAlarmSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Controls.Add(this.programDeviceAlarmSettingsTabRadButton);
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton);
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionAlarmSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Location = new System.Drawing.Point(697, 365);
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Name = "deviceInteractionAlarmSettingsTabRadGroupBox";
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionAlarmSettingsTabRadGroupBox.TabIndex = 50;
            this.deviceInteractionAlarmSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionAlarmSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceAlarmSettingsTabRadButton
            // 
            this.programDeviceAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceAlarmSettingsTabRadButton.Name = "programDeviceAlarmSettingsTabRadButton";
            this.programDeviceAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceAlarmSettingsTabRadButton.TabIndex = 41;
            this.programDeviceAlarmSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceAlarmSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceAlarmSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Name = "loadConfigurationFromDeviceAlarmSettingsTabRadButton";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceAlarmSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // alarmSettingsRadProgressBar
            // 
            this.alarmSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.alarmSettingsRadProgressBar.Location = new System.Drawing.Point(579, 300);
            this.alarmSettingsRadProgressBar.Name = "alarmSettingsRadProgressBar";
            this.alarmSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.alarmSettingsRadProgressBar.TabIndex = 49;
            this.alarmSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // commonAlarmSettingsRadGroupBox
            // 
            this.commonAlarmSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendWarningRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.calcTrendRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.weeksRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.timesPerYearTrendAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.timesPerYearTrendWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.percentChangeAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.percentChangeWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeAlarmRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeWarningRadMaskedEditBox);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.changeWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.alarmOnChangeRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.calcTrendOnRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendWarningRadLabel);
            this.commonAlarmSettingsRadGroupBox.Controls.Add(this.trendAlarmsTitleRadLabel);
            this.commonAlarmSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commonAlarmSettingsRadGroupBox.HeaderText = "Common Alarm Settings";
            this.commonAlarmSettingsRadGroupBox.Location = new System.Drawing.Point(176, 4);
            this.commonAlarmSettingsRadGroupBox.Name = "commonAlarmSettingsRadGroupBox";
            this.commonAlarmSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.commonAlarmSettingsRadGroupBox.Size = new System.Drawing.Size(202, 235);
            this.commonAlarmSettingsRadGroupBox.TabIndex = 22;
            this.commonAlarmSettingsRadGroupBox.Text = "Common Alarm Settings";
            this.commonAlarmSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendWarningRadMaskedEditBox
            // 
            this.trendWarningRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendWarningRadMaskedEditBox.Location = new System.Drawing.Point(97, 61);
            this.trendWarningRadMaskedEditBox.Name = "trendWarningRadMaskedEditBox";
            this.trendWarningRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.trendWarningRadMaskedEditBox.TabIndex = 15;
            this.trendWarningRadMaskedEditBox.TabStop = false;
            // 
            // trendAlarmRadMaskedEditBox
            // 
            this.trendAlarmRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmRadMaskedEditBox.Location = new System.Drawing.Point(97, 85);
            this.trendAlarmRadMaskedEditBox.Name = "trendAlarmRadMaskedEditBox";
            this.trendAlarmRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.trendAlarmRadMaskedEditBox.TabIndex = 15;
            this.trendAlarmRadMaskedEditBox.TabStop = false;
            // 
            // calcTrendRadMaskedEditBox
            // 
            this.calcTrendRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcTrendRadMaskedEditBox.Location = new System.Drawing.Point(97, 109);
            this.calcTrendRadMaskedEditBox.Name = "calcTrendRadMaskedEditBox";
            this.calcTrendRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.calcTrendRadMaskedEditBox.TabIndex = 14;
            this.calcTrendRadMaskedEditBox.TabStop = false;
            // 
            // weeksRadLabel
            // 
            this.weeksRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weeksRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.weeksRadLabel.Location = new System.Drawing.Point(140, 107);
            this.weeksRadLabel.Name = "weeksRadLabel";
            this.weeksRadLabel.Size = new System.Drawing.Size(38, 16);
            this.weeksRadLabel.TabIndex = 13;
            this.weeksRadLabel.Text = "weeks";
            this.weeksRadLabel.ThemeName = "Office2007Black";
            // 
            // timesPerYearTrendAlarmRadLabel
            // 
            this.timesPerYearTrendAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPerYearTrendAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timesPerYearTrendAlarmRadLabel.Location = new System.Drawing.Point(140, 85);
            this.timesPerYearTrendAlarmRadLabel.Name = "timesPerYearTrendAlarmRadLabel";
            this.timesPerYearTrendAlarmRadLabel.Size = new System.Drawing.Size(58, 16);
            this.timesPerYearTrendAlarmRadLabel.TabIndex = 12;
            this.timesPerYearTrendAlarmRadLabel.Text = "times/year";
            this.timesPerYearTrendAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // timesPerYearTrendWarningRadLabel
            // 
            this.timesPerYearTrendWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timesPerYearTrendWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timesPerYearTrendWarningRadLabel.Location = new System.Drawing.Point(140, 63);
            this.timesPerYearTrendWarningRadLabel.Name = "timesPerYearTrendWarningRadLabel";
            this.timesPerYearTrendWarningRadLabel.Size = new System.Drawing.Size(58, 16);
            this.timesPerYearTrendWarningRadLabel.TabIndex = 11;
            this.timesPerYearTrendWarningRadLabel.Text = "times/year";
            this.timesPerYearTrendWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // percentChangeAlarmRadLabel
            // 
            this.percentChangeAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentChangeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.percentChangeAlarmRadLabel.Location = new System.Drawing.Point(140, 204);
            this.percentChangeAlarmRadLabel.Name = "percentChangeAlarmRadLabel";
            this.percentChangeAlarmRadLabel.Size = new System.Drawing.Size(16, 16);
            this.percentChangeAlarmRadLabel.TabIndex = 10;
            this.percentChangeAlarmRadLabel.Text = "%";
            this.percentChangeAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // percentChangeWarningRadLabel
            // 
            this.percentChangeWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentChangeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.percentChangeWarningRadLabel.Location = new System.Drawing.Point(140, 182);
            this.percentChangeWarningRadLabel.Name = "percentChangeWarningRadLabel";
            this.percentChangeWarningRadLabel.Size = new System.Drawing.Size(16, 16);
            this.percentChangeWarningRadLabel.TabIndex = 9;
            this.percentChangeWarningRadLabel.Text = "%";
            this.percentChangeWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // changeAlarmRadMaskedEditBox
            // 
            this.changeAlarmRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAlarmRadMaskedEditBox.Location = new System.Drawing.Point(97, 204);
            this.changeAlarmRadMaskedEditBox.Name = "changeAlarmRadMaskedEditBox";
            this.changeAlarmRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.changeAlarmRadMaskedEditBox.TabIndex = 8;
            this.changeAlarmRadMaskedEditBox.TabStop = false;
            // 
            // changeWarningRadMaskedEditBox
            // 
            this.changeWarningRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeWarningRadMaskedEditBox.Location = new System.Drawing.Point(97, 180);
            this.changeWarningRadMaskedEditBox.Name = "changeWarningRadMaskedEditBox";
            this.changeWarningRadMaskedEditBox.Size = new System.Drawing.Size(37, 18);
            this.changeWarningRadMaskedEditBox.TabIndex = 7;
            this.changeWarningRadMaskedEditBox.TabStop = false;
            // 
            // changeAlarmRadLabel
            // 
            this.changeAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeAlarmRadLabel.Location = new System.Drawing.Point(7, 206);
            this.changeAlarmRadLabel.Name = "changeAlarmRadLabel";
            this.changeAlarmRadLabel.Size = new System.Drawing.Size(79, 16);
            this.changeAlarmRadLabel.TabIndex = 6;
            this.changeAlarmRadLabel.Text = "Change Alarm";
            this.changeAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // changeWarningRadLabel
            // 
            this.changeWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.changeWarningRadLabel.Location = new System.Drawing.Point(5, 182);
            this.changeWarningRadLabel.Name = "changeWarningRadLabel";
            this.changeWarningRadLabel.Size = new System.Drawing.Size(91, 16);
            this.changeWarningRadLabel.TabIndex = 5;
            this.changeWarningRadLabel.Text = "Change Warning";
            this.changeWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // alarmOnChangeRadLabel
            // 
            this.alarmOnChangeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnChangeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnChangeRadLabel.Location = new System.Drawing.Point(70, 146);
            this.alarmOnChangeRadLabel.Name = "alarmOnChangeRadLabel";
            this.alarmOnChangeRadLabel.Size = new System.Drawing.Size(94, 16);
            this.alarmOnChangeRadLabel.TabIndex = 4;
            this.alarmOnChangeRadLabel.Text = "Alarm on Change";
            this.alarmOnChangeRadLabel.ThemeName = "Office2007Black";
            // 
            // calcTrendOnRadLabel
            // 
            this.calcTrendOnRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcTrendOnRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.calcTrendOnRadLabel.Location = new System.Drawing.Point(7, 109);
            this.calcTrendOnRadLabel.Name = "calcTrendOnRadLabel";
            this.calcTrendOnRadLabel.Size = new System.Drawing.Size(73, 16);
            this.calcTrendOnRadLabel.TabIndex = 3;
            this.calcTrendOnRadLabel.Text = "Calc trend on";
            this.calcTrendOnRadLabel.ThemeName = "Office2007Black";
            // 
            // trendAlarmRadLabel
            // 
            this.trendAlarmRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAlarmRadLabel.Location = new System.Drawing.Point(7, 87);
            this.trendAlarmRadLabel.Name = "trendAlarmRadLabel";
            this.trendAlarmRadLabel.Size = new System.Drawing.Size(68, 16);
            this.trendAlarmRadLabel.TabIndex = 2;
            this.trendAlarmRadLabel.Text = "Trend Alarm";
            this.trendAlarmRadLabel.ThemeName = "Office2007Black";
            // 
            // trendWarningRadLabel
            // 
            this.trendWarningRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendWarningRadLabel.Location = new System.Drawing.Point(7, 65);
            this.trendWarningRadLabel.Name = "trendWarningRadLabel";
            this.trendWarningRadLabel.Size = new System.Drawing.Size(81, 16);
            this.trendWarningRadLabel.TabIndex = 1;
            this.trendWarningRadLabel.Text = "Trend Warning";
            this.trendWarningRadLabel.ThemeName = "Office2007Black";
            // 
            // trendAlarmsTitleRadLabel
            // 
            this.trendAlarmsTitleRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAlarmsTitleRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAlarmsTitleRadLabel.Location = new System.Drawing.Point(75, 30);
            this.trendAlarmsTitleRadLabel.Name = "trendAlarmsTitleRadLabel";
            this.trendAlarmsTitleRadLabel.Size = new System.Drawing.Size(74, 16);
            this.trendAlarmsTitleRadLabel.TabIndex = 0;
            this.trendAlarmsTitleRadLabel.Text = "Trend Alarms";
            this.trendAlarmsTitleRadLabel.ThemeName = "Office2007Black";
            // 
            // relayModeRadGroupBox
            // 
            this.relayModeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.relayModeRadGroupBox.Controls.Add(this.offRelayModeRadRadioButton);
            this.relayModeRadGroupBox.Controls.Add(this.onRelayModeRadRadioButton);
            this.relayModeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayModeRadGroupBox.HeaderText = "Relay Mode";
            this.relayModeRadGroupBox.Location = new System.Drawing.Point(3, 4);
            this.relayModeRadGroupBox.Name = "relayModeRadGroupBox";
            this.relayModeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.relayModeRadGroupBox.Size = new System.Drawing.Size(167, 48);
            this.relayModeRadGroupBox.TabIndex = 19;
            this.relayModeRadGroupBox.Text = "Relay Mode";
            this.relayModeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // offRelayModeRadRadioButton
            // 
            this.offRelayModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.offRelayModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.offRelayModeRadRadioButton.Location = new System.Drawing.Point(65, 24);
            this.offRelayModeRadRadioButton.Name = "offRelayModeRadRadioButton";
            this.offRelayModeRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.offRelayModeRadRadioButton.TabIndex = 1;
            this.offRelayModeRadRadioButton.Text = "Off";
            // 
            // onRelayModeRadRadioButton
            // 
            this.onRelayModeRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onRelayModeRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.onRelayModeRadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.onRelayModeRadRadioButton.Name = "onRelayModeRadRadioButton";
            this.onRelayModeRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.onRelayModeRadRadioButton.TabIndex = 0;
            this.onRelayModeRadRadioButton.Text = "On";
            // 
            // alarmSettingsRadGridView
            // 
            this.alarmSettingsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.alarmSettingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmSettingsRadGridView.Location = new System.Drawing.Point(384, 4);
            this.alarmSettingsRadGridView.Name = "alarmSettingsRadGridView";
            this.alarmSettingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.alarmSettingsRadGridView.Size = new System.Drawing.Size(464, 235);
            this.alarmSettingsRadGridView.TabIndex = 18;
            this.alarmSettingsRadGridView.Text = "radGridView1";
            this.alarmSettingsRadGridView.ThemeName = "Office2007Black";
            this.alarmSettingsRadGridView.ContextMenuOpening += new Telerik.WinControls.UI.ContextMenuOpeningEventHandler(this.alarmSettingsRadGridView_ContextMenuOpening);
            // 
            // alarmEnableRadGroupBox
            // 
            this.alarmEnableRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.alarmEnableRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxChangeRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxTrendRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnQmaxLevelRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDIChangeRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDITrendRadCheckBox);
            this.alarmEnableRadGroupBox.Controls.Add(this.alarmOnPDILevelRadCheckBox);
            this.alarmEnableRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmEnableRadGroupBox.HeaderText = "Enable Alarms";
            this.alarmEnableRadGroupBox.Location = new System.Drawing.Point(3, 63);
            this.alarmEnableRadGroupBox.Name = "alarmEnableRadGroupBox";
            this.alarmEnableRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.alarmEnableRadGroupBox.Size = new System.Drawing.Size(167, 176);
            this.alarmEnableRadGroupBox.TabIndex = 17;
            this.alarmEnableRadGroupBox.Text = "Enable Alarms";
            this.alarmEnableRadGroupBox.ThemeName = "Office2007Black";
            // 
            // alarmOnQmaxChangeRadCheckBox
            // 
            this.alarmOnQmaxChangeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxChangeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxChangeRadCheckBox.Location = new System.Drawing.Point(13, 154);
            this.alarmOnQmaxChangeRadCheckBox.Name = "alarmOnQmaxChangeRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxChangeRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxChangeRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxChangeRadCheckBox.Size = new System.Drawing.Size(144, 15);
            this.alarmOnQmaxChangeRadCheckBox.TabIndex = 21;
            this.alarmOnQmaxChangeRadCheckBox.Text = "Alarm on Qmax Change";
            // 
            // alarmOnQmaxTrendRadCheckBox
            // 
            this.alarmOnQmaxTrendRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxTrendRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxTrendRadCheckBox.Location = new System.Drawing.Point(13, 132);
            this.alarmOnQmaxTrendRadCheckBox.Name = "alarmOnQmaxTrendRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxTrendRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxTrendRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxTrendRadCheckBox.Size = new System.Drawing.Size(135, 16);
            this.alarmOnQmaxTrendRadCheckBox.TabIndex = 20;
            this.alarmOnQmaxTrendRadCheckBox.Text = "Alarm on Qmax Trend";
            // 
            // alarmOnQmaxLevelRadCheckBox
            // 
            this.alarmOnQmaxLevelRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnQmaxLevelRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnQmaxLevelRadCheckBox.Location = new System.Drawing.Point(13, 107);
            this.alarmOnQmaxLevelRadCheckBox.Name = "alarmOnQmaxLevelRadCheckBox";
            // 
            // 
            // 
            this.alarmOnQmaxLevelRadCheckBox.RootElement.StretchHorizontally = true;
            this.alarmOnQmaxLevelRadCheckBox.RootElement.StretchVertically = true;
            this.alarmOnQmaxLevelRadCheckBox.Size = new System.Drawing.Size(135, 16);
            this.alarmOnQmaxLevelRadCheckBox.TabIndex = 19;
            this.alarmOnQmaxLevelRadCheckBox.Text = "Alarm on Qmax Level";
            // 
            // alarmOnPDIChangeRadCheckBox
            // 
            this.alarmOnPDIChangeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDIChangeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDIChangeRadCheckBox.Location = new System.Drawing.Point(13, 78);
            this.alarmOnPDIChangeRadCheckBox.Name = "alarmOnPDIChangeRadCheckBox";
            this.alarmOnPDIChangeRadCheckBox.Size = new System.Drawing.Size(130, 16);
            this.alarmOnPDIChangeRadCheckBox.TabIndex = 18;
            this.alarmOnPDIChangeRadCheckBox.Text = "Alarm on PDI Change";
            // 
            // alarmOnPDITrendRadCheckBox
            // 
            this.alarmOnPDITrendRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDITrendRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDITrendRadCheckBox.Location = new System.Drawing.Point(13, 56);
            this.alarmOnPDITrendRadCheckBox.Name = "alarmOnPDITrendRadCheckBox";
            this.alarmOnPDITrendRadCheckBox.Size = new System.Drawing.Size(120, 16);
            this.alarmOnPDITrendRadCheckBox.TabIndex = 2;
            this.alarmOnPDITrendRadCheckBox.Text = "Alarm on PDI Trend";
            // 
            // alarmOnPDILevelRadCheckBox
            // 
            this.alarmOnPDILevelRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmOnPDILevelRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmOnPDILevelRadCheckBox.Location = new System.Drawing.Point(13, 31);
            this.alarmOnPDILevelRadCheckBox.Name = "alarmOnPDILevelRadCheckBox";
            this.alarmOnPDILevelRadCheckBox.Size = new System.Drawing.Size(117, 16);
            this.alarmOnPDILevelRadCheckBox.TabIndex = 1;
            this.alarmOnPDILevelRadCheckBox.Text = "Alarm on PDI Level";
            // 
            // PDM_MonitorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(872, 606);
            this.Controls.Add(this.configurationRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PDM_MonitorConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Partial Discharge Monitor Configuration";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PDM_MonitorConfiguration_FormClosing);
            this.Load += new System.EventHandler(this.PDM_MonitorConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).EndInit();
            this.configurationRadPageView.ResumeLayout(false);
            this.commonSettingsRadPageViewPage.ResumeLayout(false);
            this.commonSettingsRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpldVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsCommonSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsCommonSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCommonSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionCommonSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCommonSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionCommonSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsCommonSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCommonSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadGroupBox)).EndInit();
            this.saveRadGroupBox.ResumeLayout(false);
            this.saveRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dayRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daySavePRPDDRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementsSavePRPDDRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.savePRPDDRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveModeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalSaveModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testSaveModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionSettingsRadGroupBox)).EndInit();
            this.connectionSettingsRadGroupBox.ResumeLayout(false);
            this.connectionSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratedVoltageRadGroupBox)).EndInit();
            this.ratedVoltageRadGroupBox.ResumeLayout(false);
            this.ratedVoltageRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV4RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV3RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV2RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kV1RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RatedVoltageRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels13to15RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels7to12RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels4to6RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channels1to3RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).EndInit();
            this.monitoringRadGroupBox.ResumeLayout(false);
            this.monitoringRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).EndInit();
            this.measurementScheduleRadGroupBox.ResumeLayout(false);
            this.measurementScheduleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduledMeasurementScheduleRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).EndInit();
            this.pdSettingsRadPageViewPage.ResumeLayout(false);
            this.pdSettingsRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationPDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsPDSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsPDSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationPDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsPDSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionPDSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionPDSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDevicePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDevicePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionPDSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionPDSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationPDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationPDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabasePDSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advancedSettingsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsPDSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsPDSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsPDSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rereadOnAlarmRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hzRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationFrequencyRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonPhaseShiftRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyclesPerAcquisitionRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.synchronizationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.channelConfigurationRadGridView)).EndInit();
            this.alarmSettingsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saveAsCurrentConfigurationAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAlarmSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentConfigurationAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsAlarmSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAlarmSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionAlarmSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonAlarmSettingsRadGroupBox)).EndInit();
            this.commonAlarmSettingsRadGroupBox.ResumeLayout(false);
            this.commonAlarmSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weeksRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timesPerYearTrendWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.percentChangeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.changeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnChangeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcTrendOnRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendAlarmsTitleRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayModeRadGroupBox)).EndInit();
            this.relayModeRadGroupBox.ResumeLayout(false);
            this.relayModeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offRelayModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.onRelayModeRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmEnableRadGroupBox)).EndInit();
            this.alarmEnableRadGroupBox.ResumeLayout(false);
            this.alarmEnableRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxChangeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxTrendRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnQmaxLevelRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDIChangeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDITrendRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmOnPDILevelRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView configurationRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage commonSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage pdSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage alarmSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox measurementScheduleRadGroupBox;
        private Telerik.WinControls.UI.RadGridView measurementSettingsRadGridView;
        private Telerik.WinControls.UI.RadRadioButton byScheduledMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadLabel minuteRadLabel;
        private Telerik.WinControls.UI.RadLabel hourRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox minuteMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox hourMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadRadioButton stepMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox monitoringRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton disableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox ratedVoltageRadGroupBox;
        private Telerik.WinControls.UI.RadLabel kV4RadLabel;
        private Telerik.WinControls.UI.RadLabel kV3RadLabel;
        private Telerik.WinControls.UI.RadLabel kV2RadLabel;
        private Telerik.WinControls.UI.RadLabel kV1RadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox channels13to15RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox channels7to12RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox channels4to6RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel channels13to15RadLabel;
        private Telerik.WinControls.UI.RadLabel channels7to12RadLabel;
        private Telerik.WinControls.UI.RadLabel channels4to6RadLabel;
        private Telerik.WinControls.UI.RadLabel channels1to3RadLabel;
        private Telerik.WinControls.UI.RadGroupBox saveRadGroupBox;
        private Telerik.WinControls.UI.RadLabel dayRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox daySavePRPDDRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel measurementsRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox measurementsSavePRPDDRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel savePRPDDRadLabel;
        private Telerik.WinControls.UI.RadLabel saveModeRadLabel;
        private Telerik.WinControls.UI.RadRadioButton normalSaveModeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton testSaveModeRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox alarmEnableRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxChangeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxTrendRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnQmaxLevelRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDIChangeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDITrendRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox alarmOnPDILevelRadCheckBox;
        private Telerik.WinControls.UI.RadGridView channelConfigurationRadGridView;
        private Telerik.WinControls.UI.RadGridView alarmSettingsRadGridView;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadMaskedEditBox cyclesPerAcquisitionRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox synchronizationFrequencyRadMaskedEditBox;
        private Telerik.WinControls.UI.RadDropDownList synchronizationRadDropDownList;
        private Telerik.WinControls.UI.RadLabel commonPhaseShiftRadLabel;
        private Telerik.WinControls.UI.RadLabel cyclesPerAcquisitionRadLabel;
        private Telerik.WinControls.UI.RadLabel synchronizationRadLabel;
        private Telerik.WinControls.UI.RadCheckBox rereadOnAlarmRadCheckBox;
        private Telerik.WinControls.UI.RadMaskedEditBox commonPhaseShiftRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel degreesRadLabel;
        private Telerik.WinControls.UI.RadGroupBox relayModeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton offRelayModeRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton onRelayModeRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox commonAlarmSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox trendWarningRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox trendAlarmRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox calcTrendRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel weeksRadLabel;
        private Telerik.WinControls.UI.RadLabel timesPerYearTrendAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel timesPerYearTrendWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel percentChangeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel percentChangeWarningRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox changeAlarmRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox changeWarningRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel changeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel changeWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel alarmOnChangeRadLabel;
        private Telerik.WinControls.UI.RadLabel calcTrendOnRadLabel;
        private Telerik.WinControls.UI.RadLabel trendAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel trendWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel trendAlarmsTitleRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox channels1to3RatedVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel hzRadLabel;
        private Telerik.WinControls.UI.RadGroupBox connectionSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel modbusAddressRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel baudRateRadLabel;
        private Telerik.WinControls.UI.RadDropDownList baudRateRadDropDownList;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsCommonSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton saveCurrentConfigurationCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDevicePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabasePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsPDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsPDSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton saveCurrentConfigurationPDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDevicePDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadProgressBar commonSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar pdSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar alarmSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadLabel firmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel firmwareVersionTextRadLabel;
        private Telerik.WinControls.UI.RadButton advancedSettingsRadButton;
        private Telerik.WinControls.UI.RadLabel advancedSettingsWarningRadLabel;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionPDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionPDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationPDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsCommonSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsCommonSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsPDSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationPDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsPDSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsAlarmSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadLabel cpldVersionValueRadLabel;
        private Telerik.WinControls.UI.RadLabel cpldVersionTextRadLabel;
        private Telerik.WinControls.UI.RadButton saveAsCurrentConfigurationCommonSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton saveAsCurrentConfigurationPDSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton saveAsCurrentConfigurationAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton saveCurrentConfigurationAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseAlarmSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsAlarmSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsAlarmSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton4;
    }
}

