using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;
using Dynamics;

namespace PDMonitorUtilities
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private List<string> pdiCalculationLimitValues;

        private static string errorInReferenceChannelValueInPDSettingsText = "Error in Reference Channel value in PD settings tag:\nThe reference channel is the same as the channel number";

        private static string copySettingsToAllOtherRowsText = "Copy settings to all other rows";
        private static string copySettingsToAllEnabledRowsText = "Copy settings to all enabled rows";

        private static string channelNumberChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Channel<br>Number</html>";
        private static string enableChannelChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable Channel</html>";
        private static string pulseWidthChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Pulse Width (nS)</html>";
        private static string deadTimeChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Dead Time (nS)</html>";
        private static string pdiCalculationLimitChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Calculate PDI<br>up to (mV)</html>";
        private static string sensitivityChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensitivity</html>";
        private static string sensorPhaseChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor Phase</html>";
        private static string enablePolarityChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable Polarity</html>";
        private static string enableOppositePolarityChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable Opposite<br>Polarity</html>";
        private static string enableTimeOfArrivalChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable Time<br>of Arrival</html>";
        private static string referenceChannelChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Reference Channel</html>";
        private static string referenceChannelShiftChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Reference Channel<br>Shift (dB)</html>";
        private static string group1AmplitudeFilterEnableChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 1<br><br>Enable</html>";
        private static string group1AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 1<br><br>Threshold type</html>";
        private static string group1AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 1<br><br>Threshold offset</html>";
        private static string group1AmplitudeFilterThresholdStopChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 1<br><br>Threshold stop @ (mV)</html>";
        private static string group2AmplitudeFilterEnableChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 2<br><br>Enable</html>";
        private static string group2AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 2<br><br>Threshold type</html>";
        private static string group2AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 2<br><br>Threshold offset</html>";
        private static string group2AmplitudeFilterThresholdStopChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 2<br><br>Threshold stop @ (mV)</html>";
        private static string group3AmplitudeFilterEnableChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 3<br><br>Enable</html>";
        private static string group3AmplitudeFilterThresholdTypeChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 3<br><br>Threshold type</html>";
        private static string group3AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 3<br><br>Threshold offset</html>";
        private static string group3AmplitudeFilterThresholdStopChannelConfigurationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Amp Filter Set 3<br><br>Threshold stop @ (mV)</html>";
        private static string slidingChannelConfigurationGridViewText = "sliding";
        private static string constantChannelConfigurationGridViewText = "constant";
        private static string noneChannelConfigurationGridViewText = "None";
       
        private void CopyPDSettingsToAllRows(GridViewRowInfo inputRowInfo)
        {
            CopyPDSettingsToSpecifiedRows(inputRowInfo, false);
        }

        private void CopyPDSettingsToSpecifiedRows(GridViewRowInfo inputRowInfo, bool enabledOnly)
        {
            try
            {
                PDM_ConfigComponent_ChannelInfo channelInfo;
                GridViewRowInfo rowInfo;
                bool copyRow;
                bool enabled;
                if (inputRowInfo != null)
                {
                    if (inputRowInfo.Cells.Count > 22)
                    {
                        if (this.channelConfigurationRadGridView.RowCount == 15)
                        {
                            // we had better get the data from the grid and into the data structure before we do anything else, right?  Right?
                            // or maybe not.
                           // WritePDSettingsChannelInfoGridEntriesToConfigurationData();
                            channelInfo = GetChannelInfoGridEntriesForOneRow(inputRowInfo);
                            for (int i = 0; i < 15; i++)
                            {
                                copyRow = false;
                                rowInfo = this.channelConfigurationRadGridView.Rows[i];
                                enabled = ((bool)(rowInfo.Cells[1].Value) == true);
                                if (enabledOnly)
                                {
                                    if (enabled)
                                    {
                                        copyRow = true;
                                    }
                                }
                                else
                                {
                                    copyRow = true;
                                }

                                if (copyRow)
                                {
                                    AddDataToOneRowOfTheChannelConfigurationGridView(channelInfo, ref rowInfo, false, false);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.CopyPDSettingsToAllRows(GridViewRowInfo, bool)\nthis.channelConfigurationRadGridView.RowCount was not 15.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.CopyPDSettingsToAllRows(GridViewRowInfo, bool)\nInput GridViewRowInfo needs to have at least 23 cells.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.CopyPDSettingsToAllRows(GridViewRowInfo, bool)\nInput GridViewRowInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.CopyPDSettingsToAllRows(GridViewRowInfo, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CopyPDSettingsToAllEnabledRows(GridViewRowInfo inputRowInfo)
        {
            CopyPDSettingsToSpecifiedRows(inputRowInfo, true);
        }

        private void IntialializePDICalculationLimitValuesList()
        {
            this.pdiCalculationLimitValues = new List<string>();
            this.pdiCalculationLimitValues.Add("6894");
            this.pdiCalculationLimitValues.Add("5351");
            this.pdiCalculationLimitValues.Add("4154");
            this.pdiCalculationLimitValues.Add("3225");
            this.pdiCalculationLimitValues.Add("2503");
            this.pdiCalculationLimitValues.Add("1943");
            this.pdiCalculationLimitValues.Add("1508");
            this.pdiCalculationLimitValues.Add("1171");
            this.pdiCalculationLimitValues.Add("909");
            this.pdiCalculationLimitValues.Add("705");
            this.pdiCalculationLimitValues.Add("548");
            this.pdiCalculationLimitValues.Add("425");
            this.pdiCalculationLimitValues.Add("330");
            this.pdiCalculationLimitValues.Add("256");
            this.pdiCalculationLimitValues.Add("199");
            this.pdiCalculationLimitValues.Add("154");
            this.pdiCalculationLimitValues.Add("120");
            this.pdiCalculationLimitValues.Add("93");
            this.pdiCalculationLimitValues.Add("72");
            this.pdiCalculationLimitValues.Add("56");
            this.pdiCalculationLimitValues.Add("43");
            this.pdiCalculationLimitValues.Add("34");
            this.pdiCalculationLimitValues.Add("26");
            this.pdiCalculationLimitValues.Add("20");
            this.pdiCalculationLimitValues.Add("16");
            this.pdiCalculationLimitValues.Add("12");
            this.pdiCalculationLimitValues.Add("10");
            this.pdiCalculationLimitValues.Add("7");
            this.pdiCalculationLimitValues.Add("6");
            this.pdiCalculationLimitValues.Add("4");
            this.pdiCalculationLimitValues.Add("3");
            this.pdiCalculationLimitValues.Add("0");
        }

        private void InitializeChannelConfigurationRadGridView()
        {
            try
            {
                GridViewTextBoxColumn channelNumberGridViewTextBoxColumn = new GridViewTextBoxColumn();
                channelNumberGridViewTextBoxColumn.Name = "ChannelNumber";
                channelNumberGridViewTextBoxColumn.HeaderText = channelNumberChannelConfigurationGridViewText;
                channelNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                channelNumberGridViewTextBoxColumn.Width = 50;
                channelNumberGridViewTextBoxColumn.AllowSort = false;
                channelNumberGridViewTextBoxColumn.IsPinned = true;
                channelNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableChannelGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                enableChannelGridViewCheckBoxColumn.Name = "EnableChannel";
                enableChannelGridViewCheckBoxColumn.HeaderText = enableChannelChannelConfigurationGridViewText;
                enableChannelGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableChannelGridViewCheckBoxColumn.Width = 90;
                enableChannelGridViewCheckBoxColumn.AllowSort = false;

                GridViewComboBoxColumn pulseWidthGridViewComboBoxColumn = new GridViewComboBoxColumn();
                pulseWidthGridViewComboBoxColumn.Name = "PulseWidth";
                pulseWidthGridViewComboBoxColumn.HeaderText = pulseWidthChannelConfigurationGridViewText;
                pulseWidthGridViewComboBoxColumn.DataSource = new String[] { "640", "1280" };
                pulseWidthGridViewComboBoxColumn.DisableHTMLRendering = false;
                pulseWidthGridViewComboBoxColumn.Width = 90;
                pulseWidthGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn deadTimeGridViewComboBoxColumn = new GridViewComboBoxColumn();
                deadTimeGridViewComboBoxColumn.Name = "DeadTime";
                deadTimeGridViewComboBoxColumn.HeaderText = deadTimeChannelConfigurationGridViewText;
                deadTimeGridViewComboBoxColumn.DataSource = new String[] { "1280", "2560" };
                deadTimeGridViewComboBoxColumn.DisableHTMLRendering = false;
                deadTimeGridViewComboBoxColumn.Width = 90;
                deadTimeGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn pdiCalculationLimitGridViewComboBoxColumn = new GridViewComboBoxColumn();
                pdiCalculationLimitGridViewComboBoxColumn.Name = "PdiCalculationLimit";
                pdiCalculationLimitGridViewComboBoxColumn.HeaderText = pdiCalculationLimitChannelConfigurationGridViewText;
                //pdiCalculationLimitGridViewComboBoxColumn.DataSource =
                //    new String[] { "6894", "5351", "4154", "3225", "2503", "1943", "1508", "1171", "909", "705", "548", "425", "330", 
                //       "256", "199", "154", "120", "93", "72", "56", "43", "34", "26", "20", "16", "12", "10", "7", "6", "4", "3", "0" };
                pdiCalculationLimitGridViewComboBoxColumn.DataSource = pdiCalculationLimits;
                pdiCalculationLimitGridViewComboBoxColumn.DisableHTMLRendering = false;
                pdiCalculationLimitGridViewComboBoxColumn.Width = 80;
                pdiCalculationLimitGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensitivityGridViewDecimalColumn = new GridViewDecimalColumn();
                sensitivityGridViewDecimalColumn.Name = "Sensitivity";
                sensitivityGridViewDecimalColumn.HeaderText = sensitivityChannelConfigurationGridViewText;
                sensitivityGridViewDecimalColumn.DecimalPlaces = 2;
                sensitivityGridViewDecimalColumn.DisableHTMLRendering = false;
                sensitivityGridViewDecimalColumn.Width = 65;
                sensitivityGridViewDecimalColumn.AllowSort = false;

                GridViewComboBoxColumn sensorPhaseGridViewComboBoxColumn = new GridViewComboBoxColumn();
                sensorPhaseGridViewComboBoxColumn.Name = "SensorPhase";
                sensorPhaseGridViewComboBoxColumn.HeaderText = sensorPhaseChannelConfigurationGridViewText;
                sensorPhaseGridViewComboBoxColumn.DataSource = new String[] { aSensorPhaseText, bSensorPhaseText, cSensorPhaseText, abSensorPhaseText, bcSensorPhaseText, caSensorPhaseText };
                sensorPhaseGridViewComboBoxColumn.DisableHTMLRendering = false;
                sensorPhaseGridViewComboBoxColumn.Width = 76;
                sensorPhaseGridViewComboBoxColumn.AllowSort = false;

                GridViewCheckBoxColumn enablePolarityGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                enablePolarityGridViewCheckBoxColumn.Name = "EnablePolarity";
                enablePolarityGridViewCheckBoxColumn.HeaderText = enablePolarityChannelConfigurationGridViewText;
                enablePolarityGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enablePolarityGridViewCheckBoxColumn.Width = 90;
                enablePolarityGridViewCheckBoxColumn.AllowSort = false;

                GridViewCheckBoxColumn enableOppositePolarityGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                enableOppositePolarityGridViewCheckBoxColumn.Name = "EnableOppositePolarity";
                enableOppositePolarityGridViewCheckBoxColumn.HeaderText = enableOppositePolarityChannelConfigurationGridViewText;
                enableOppositePolarityGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableOppositePolarityGridViewCheckBoxColumn.Width = 100;
                enableOppositePolarityGridViewCheckBoxColumn.AllowSort = false;

                GridViewCheckBoxColumn enableTimeOfArrivalGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                enableTimeOfArrivalGridViewCheckBoxColumn.Name = "EnableTimeOfArrival";
                enableTimeOfArrivalGridViewCheckBoxColumn.HeaderText = enableTimeOfArrivalChannelConfigurationGridViewText;
                enableTimeOfArrivalGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableTimeOfArrivalGridViewCheckBoxColumn.Width = 100;
                enableTimeOfArrivalGridViewCheckBoxColumn.AllowSort = false;

                GridViewComboBoxColumn referenceChannelGridViewComboBoxColumn = new GridViewComboBoxColumn();
                referenceChannelGridViewComboBoxColumn.Name = "ReferenceChannel";
                referenceChannelGridViewComboBoxColumn.HeaderText = referenceChannelChannelConfigurationGridViewText;
                referenceChannelGridViewComboBoxColumn.DataSource = new String[] { noneChannelConfigurationGridViewText, "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15" };
                referenceChannelGridViewComboBoxColumn.DisableHTMLRendering = false;
                referenceChannelGridViewComboBoxColumn.Width = 100;
                referenceChannelGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn referenceChannelShiftGridViewDecimalColumn = new GridViewDecimalColumn();
                referenceChannelShiftGridViewDecimalColumn.Name = "ReferenceChannelShift";
                referenceChannelShiftGridViewDecimalColumn.HeaderText = referenceChannelShiftChannelConfigurationGridViewText;
                referenceChannelShiftGridViewDecimalColumn.DecimalPlaces = 0;
                referenceChannelShiftGridViewDecimalColumn.DisableHTMLRendering = false;
                referenceChannelShiftGridViewDecimalColumn.Width = 100;
                referenceChannelShiftGridViewDecimalColumn.AllowSort = false;

                GridViewCheckBoxColumn group1AmplitudeFilterEnableGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                group1AmplitudeFilterEnableGridViewCheckBoxColumn.Name = "Group1AmplitudeFilterEnable";
                group1AmplitudeFilterEnableGridViewCheckBoxColumn.HeaderText = group1AmplitudeFilterEnableChannelConfigurationGridViewText;
                group1AmplitudeFilterEnableGridViewCheckBoxColumn.DisableHTMLRendering = false;
                group1AmplitudeFilterEnableGridViewCheckBoxColumn.Width = 80;
                group1AmplitudeFilterEnableGridViewCheckBoxColumn.AllowSort = false;

                GridViewComboBoxColumn group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Name = "Group1AmplitudeFilterThresholdType";
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.HeaderText = group1AmplitudeFilterThresholdTypeChannelConfigurationGridViewText;
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DataSource = new String[] { slidingChannelConfigurationGridViewText, constantChannelConfigurationGridViewText };
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DisableHTMLRendering = false;
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Width = 80;
                group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn.AllowSort = false;

                //GridViewDecimalColumn group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn = new GridViewDecimalColumn();
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Name = "Group1AmplitudeFilterThresholdOffset";
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.HeaderText = Amplitude Filter<br>by Group 1<br><br>Threshold offset;
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DecimalPlaces = 0;
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DisableHTMLRendering = false;
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Width = 85;
                //group1AmplitudeFilterThresoldOffsetGridViewDecimalColumn.AllowSort = false;

                GridViewComboBoxColumn group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Name = "Group1AmplitudeFilterThresholdOffset";
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.HeaderText = group1AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText;
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DataSource = new string[] { "5", "4", "3", "2", "1", "0", "-1", "-2", "-3", "-4", "-5" };
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DisableHTMLRendering = false;
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Width = 85;
                group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn group1AmplitudeFilterThresholdStopGridViewDecimalColumn = new GridViewDecimalColumn();
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.Name = "Group1AmplitudeFilterThresholdStop";
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.HeaderText = group1AmplitudeFilterThresholdStopChannelConfigurationGridViewText;
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.DecimalPlaces = 0;
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.DisableHTMLRendering = false;
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.Width = 115;
                group1AmplitudeFilterThresholdStopGridViewDecimalColumn.AllowSort = false;

                GridViewCheckBoxColumn group2AmplitudeFilterEnableGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                group2AmplitudeFilterEnableGridViewCheckBoxColumn.Name = "Group2AmplitudeFilterEnable";
                group2AmplitudeFilterEnableGridViewCheckBoxColumn.HeaderText = group2AmplitudeFilterEnableChannelConfigurationGridViewText;
                group2AmplitudeFilterEnableGridViewCheckBoxColumn.DisableHTMLRendering = false;
                group2AmplitudeFilterEnableGridViewCheckBoxColumn.Width = 80;
                group2AmplitudeFilterEnableGridViewCheckBoxColumn.AllowSort = false;

                GridViewComboBoxColumn group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Name = "Group2AmplitudeFilterThresholdType";
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.HeaderText = group2AmplitudeFilterThresholdTypeChannelConfigurationGridViewText;
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DataSource = new String[] { slidingChannelConfigurationGridViewText, constantChannelConfigurationGridViewText };
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DisableHTMLRendering = false;
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Width = 80;
                group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn.AllowSort = false;

                //GridViewDecimalColumn group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn = new GridViewDecimalColumn();
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Name = "Group2AmplitudeFilterThresholdOffset";
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.HeaderText = Amplitude Filter<br>by Group 2<br><br>Threshold offset;
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DecimalPlaces = 0;
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DisableHTMLRendering = false;
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Width = 85;
                //group2AmplitudeFilterThresoldOffsetGridViewDecimalColumn.AllowSort = false;

                GridViewComboBoxColumn group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Name = "Group2AmplitudeFilterThresholdOffset";
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.HeaderText = group2AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText;
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DataSource = new string[] { "5", "4", "3", "2", "1", "0", "-1", "-2", "-3", "-4", "-5" };
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DisableHTMLRendering = false;
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Width = 85;
                group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn group2AmplitudeFilterThresholdStopGridViewDecimalColumn = new GridViewDecimalColumn();
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.Name = "Group2AmplitudeFilterThresholdStop";
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.HeaderText = group2AmplitudeFilterThresholdStopChannelConfigurationGridViewText;
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.DecimalPlaces = 0;
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.DisableHTMLRendering = false;
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.Width = 115;
                group2AmplitudeFilterThresholdStopGridViewDecimalColumn.AllowSort = false;

                GridViewCheckBoxColumn group3AmplitudeFilterEnableGridViewCheckBoxColumn = new GridViewCheckBoxColumn();
                group3AmplitudeFilterEnableGridViewCheckBoxColumn.Name = "Group3AmplitudeFilterEnable";
                group3AmplitudeFilterEnableGridViewCheckBoxColumn.HeaderText = group3AmplitudeFilterEnableChannelConfigurationGridViewText;
                group3AmplitudeFilterEnableGridViewCheckBoxColumn.DisableHTMLRendering = false;
                group3AmplitudeFilterEnableGridViewCheckBoxColumn.Width = 80;
                group3AmplitudeFilterEnableGridViewCheckBoxColumn.AllowSort = false;

                GridViewComboBoxColumn group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Name = "Group3AmplitudeFilterThresholdType";
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.HeaderText = group3AmplitudeFilterThresholdTypeChannelConfigurationGridViewText;
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DataSource = new String[] { slidingChannelConfigurationGridViewText, constantChannelConfigurationGridViewText };
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.DisableHTMLRendering = false;
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.Width = 80;
                group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn.AllowSort = false;

                //GridViewDecimalColumn group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn = new GridViewDecimalColumn();
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Name = "Group3AmplitudeFilterThresholdOffset";
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.HeaderText = Amplitude Filter<br>by Group 3<br><br>Threshold offset;
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DecimalPlaces = 0;
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.DisableHTMLRendering = false;
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.Width = 85;
                //group3AmplitudeFilterThresoldOffsetGridViewDecimalColumn.AllowSort = false;

                GridViewComboBoxColumn group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn = new GridViewComboBoxColumn();
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Name = "Group3AmplitudeFilterThresholdOffset";
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.HeaderText = group3AmplitudeFilterThresoldOffsetChannelConfigurationGridViewText;
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DataSource = new string[] { "5", "4", "3", "2", "1", "0", "-1", "-2", "-3", "-4", "-5" };
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.DisableHTMLRendering = false;
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.Width = 85;
                group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn group3AmplitudeFilterThresholdStopGridViewDecimalColumn = new GridViewDecimalColumn();
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.Name = "Group3AmplitudeFilterThresholdStop";
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.HeaderText = group3AmplitudeFilterThresholdStopChannelConfigurationGridViewText;
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.DecimalPlaces = 0;
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.DisableHTMLRendering = false;
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.Width = 115;
                group3AmplitudeFilterThresholdStopGridViewDecimalColumn.AllowSort = false;

                this.channelConfigurationRadGridView.Columns.Add(channelNumberGridViewTextBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(enableChannelGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(pulseWidthGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(deadTimeGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(pdiCalculationLimitGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(sensitivityGridViewDecimalColumn);
                this.channelConfigurationRadGridView.Columns.Add(sensorPhaseGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(enablePolarityGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(enableOppositePolarityGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(enableTimeOfArrivalGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(referenceChannelGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(referenceChannelShiftGridViewDecimalColumn);
                this.channelConfigurationRadGridView.Columns.Add(group1AmplitudeFilterEnableGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group1AmplitudeFilterThresholdTypeGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group1AmplitudeFilterThresoldOffsetGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group1AmplitudeFilterThresholdStopGridViewDecimalColumn);
                this.channelConfigurationRadGridView.Columns.Add(group2AmplitudeFilterEnableGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group2AmplitudeFilterThresholdTypeGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group2AmplitudeFilterThresoldOffsetGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group2AmplitudeFilterThresholdStopGridViewDecimalColumn);
                this.channelConfigurationRadGridView.Columns.Add(group3AmplitudeFilterEnableGridViewCheckBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group3AmplitudeFilterThresholdTypeGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group3AmplitudeFilterThresoldOffsetGridViewComboBoxColumn);
                this.channelConfigurationRadGridView.Columns.Add(group3AmplitudeFilterThresholdStopGridViewDecimalColumn);

                this.channelConfigurationRadGridView.TableElement.TableHeaderHeight = 50;
                this.channelConfigurationRadGridView.AllowColumnReorder = false;
                this.channelConfigurationRadGridView.AllowColumnChooser = false;
                this.channelConfigurationRadGridView.ShowGroupPanel = false;
                this.channelConfigurationRadGridView.EnableGrouping = false;
                this.channelConfigurationRadGridView.AllowAddNewRow = false;
                this.channelConfigurationRadGridView.AllowDeleteRow = false;
                this.channelConfigurationRadGridView.AllowColumnHeaderContextMenu = false;
                //this.channelConfigurationRadGridView.AllowEditRow = false;
            
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeChannelConfigurationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private List<string> CreateReferenceChannelList(int zeroIndexedChannelNotIncluded)
        {
            List<string> referenceChannelList = new List<string>();
            try
            {
                int index = 0;

                referenceChannelList.Add("None");

                for (index = 0; index < zeroIndexedChannelNotIncluded; index++)
                {
                    referenceChannelList.Add((index + 1).ToString());
                }

                for (index = (zeroIndexedChannelNotIncluded + 1); index < 15; index++)
                {
                    referenceChannelList.Add((index + 1).ToString());
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.CreateReferenceChannelList(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return referenceChannelList;
        }

        private void channelConfigurationRadGridView_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            try
            {
                List<string> referenceChannelList;
                //string uniqueMonitorName;
                //int monitorType = 0;
                {
                    var editor = e.ActiveEditor as RadDropDownListEditor;
                    if (editor != null)
                    {
                        var editorElement = editor.EditorElement as RadDropDownListEditorElement;
                        if (editorElement != null)
                        {
                            
                            editorElement.DefaultItemsCountInDropDown = 15;

                            if (e.ColumnIndex == 4)
                            {
                                editorElement.MaxDropDownItems = this.pdiCalculationLimitValues.Count;
                                //editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.pdiCalculationLimitValues);
                                editorElement.DataSource = pdiCalculationLimits;
                                editorElement.SelectedIndex = this.pdiCalculationLimitValues.IndexOf(e.Row.Cells[4].Value.ToString());
                            }
                            else if(e.ColumnIndex == 10)
                            {
                                referenceChannelList = CreateReferenceChannelList(e.RowIndex);
                                editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(referenceChannelList);
                                editorElement.SelectedIndex = referenceChannelList.IndexOf(e.Row.Cells[10].Value.ToString());
                                editorElement.SelectionLength = 15;
                            }
//                            editorElement.SelectedIndexChanged -= new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
//                            // we're only interested in changing the slave device register entries dynamically
//                            uniqueMonitorName = e.Row.Cells[3].Value.ToString().Trim();

//                            if (this.monitorsByUniqueMonitorName.ContainsKey(uniqueMonitorName))
//                            {
//                                monitorType = this.monitorsByUniqueMonitorName[uniqueMonitorName].monitorTypeAsInteger;
//                                if (e.ColumnIndex == 5)
//                                {
//                                    if (monitorType == 6001)
//                                    {
//                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.admDynamicsSlaveRegisterNames);
//                                        editorElement.SelectedIndex = this.admDynamicsSlaveRegisterNames.IndexOf(e.Row.Cells[5].Value.ToString()); ;
//                                    }
//                                    else if (monitorType == 15002)
//                                    {
//                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.bhmDynamicsSlaveRegisterNames);
//                                        e.Row.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.bhmDynamicsSlaveRegisterNames, e.Row.Cells[5].Value.ToString());
//                                        editorElement.SelectedIndex = this.bhmDynamicsSlaveRegisterNames.IndexOf(e.Row.Cells[5].Value.ToString());
//                                    }
//                                    else if (monitorType == 505)
//                                    {
//                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.pdmDynamicsSlaveRegisterNames);
//                                        editorElement.SelectedIndex = this.pdmDynamicsSlaveRegisterNames.IndexOf(e.Row.Cells[5].Value.ToString()); ;
//                                    }
//                                    else
//                                    {
//                                        editorElement.DataSource = new string[] { "No Value Selected" };
//                                    }
//                                }
//                                else if (e.ColumnIndex == 3)
//                                {
//                                    editorElement.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
//                                    editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.monitorUniqueNames);
//                                    editorElement.SelectedIndex = this.monitorUniqueNames.IndexOf(uniqueMonitorName);
//                                }
//                            }
//                            else
//                            {
//                                string errorMessage = "Error in Main_MonitorConfiguration.dataTransferRadGridView_CellEditorInitialized(object, GridViewCellEventArgs)\nCould not find monitor name from grid in the unique names list.";
//                                LogMessage.LogError(errorMessage);
//#if DEBUG
//                                MessageBox.Show(errorMessage);
//#endif
//                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.channelConfigurationRadGridView_CellEditorInitialized(object, GridViewCellEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToChannelConfigurationGridView()
        {
            try
            {
                this.channelConfigurationRadGridView.Rows.Clear();
                for (int i = 0; i < 15; i++)
                {
                    this.channelConfigurationRadGridView.Rows.Add((i + 1), false, "640", "1280", "6894", 0.0, aSensorPhaseText, false, false, false, noneChannelConfigurationGridViewText, 0,
                                                                  false, slidingChannelConfigurationGridViewText, 0, 0, false, slidingChannelConfigurationGridViewText, 0, 0, false, 
                                                                  slidingChannelConfigurationGridViewText, 0, 0);
                }
                this.channelConfigurationRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddEmptyRowsToChannelConfigurationGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToOneRowOfTheChannelConfigurationGridView(PDM_ConfigComponent_ChannelInfo channelInfo, ref GridViewRowInfo rowInfo, bool copyEnableState, bool copyReferenceChannel)
        {
            try
            {
                Dictionary<int, bool> polarityAsZeroIndexedBitEncodedDictionary = null;
                if (channelInfo != null)
                {
                    if (rowInfo != null)
                    {
                        if (rowInfo.Cells.Count > 23)
                        {
                            if (copyEnableState)
                            {
                                if (channelInfo.ChannelIsOn == 1)
                                {
                                    rowInfo.Cells[1].Value = true;
                                }
                                else
                                {
                                    rowInfo.Cells[1].Value = false;
                                }
                            }
                            if (channelInfo.PDMaxWidth == 1)
                            {
                                rowInfo.Cells[2].Value = "640";
                            }
                            else
                            {
                                rowInfo.Cells[2].Value = "1280";
                            }

                            if (channelInfo.PDIntervalTime == 1)
                            {
                                rowInfo.Cells[3].Value = "1280";
                            }
                            else
                            {
                                rowInfo.Cells[3].Value = "2560";
                            }
                            rowInfo.Cells[4].Value = this.pdiCalculationLimits[channelInfo.CalcPDILimit - 1];
                            rowInfo.Cells[5].Value = Math.Round(channelInfo.ChannelSensitivity, 2);
                            rowInfo.Cells[6].Value = GetSensorPhaseAsString(channelInfo.CHPhase);
                            polarityAsZeroIndexedBitEncodedDictionary = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(ConversionMethods.Int16BytesToUInt16Value(ConversionMethods.Int32ToFakeUnsignedShort(channelInfo.Polarity_0)));
                            if (polarityAsZeroIndexedBitEncodedDictionary[0])
                            {
                                rowInfo.Cells[7].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[7].Value = false;
                            }

                            /// here is where the opposing polarity has to go
                            if (polarityAsZeroIndexedBitEncodedDictionary[4])
                            {
                                rowInfo.Cells[8].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[8].Value = false;
                            }
                            /// finally, we switch this capability on and off
                            if (this.oppositePolarityFeatureExists)
                            {
                                rowInfo.Cells[8].ReadOnly = false;
                            }
                            else
                            {
                                rowInfo.Cells[8].ReadOnly = true;
                            }

                            if (channelInfo.TimeOfArrival_0 == 1)
                            {
                                rowInfo.Cells[9].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[9].Value = false;
                            }

                            if (copyReferenceChannel)
                            {
                                if (channelInfo.Ref_0 == 0)
                                {
                                    rowInfo.Cells[10].Value = noneChannelConfigurationGridViewText;
                                }
                                else
                                {
                                    rowInfo.Cells[10].Value = channelInfo.Ref_0.ToString();
                                }
                            }

                            rowInfo.Cells[11].Value = -channelInfo.RefShift_0;

                            /// group 1 stuff
                            if (channelInfo.NoiseOn_0 == 1)
                            {
                                rowInfo.Cells[12].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[12].Value = false;
                            }
                            if (channelInfo.NoiseType_0 == 0)
                            {
                                rowInfo.Cells[13].Value = slidingChannelConfigurationGridViewText;
                            }
                            else
                            {
                                rowInfo.Cells[13].Value = constantChannelConfigurationGridViewText;
                            }
                            rowInfo.Cells[14].Value = channelInfo.NoiseShift_0 - 6;
                            rowInfo.Cells[15].Value = channelInfo.MinNoiseLevel_0;

                            /// group 2 stuff
                            if (channelInfo.NoiseOn_1 == 1)
                            {
                                rowInfo.Cells[16].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[16].Value = false;
                            }
                            if (channelInfo.NoiseType_1 == 0)
                            {
                                rowInfo.Cells[17].Value = slidingChannelConfigurationGridViewText;
                            }
                            else
                            {
                                rowInfo.Cells[17].Value = constantChannelConfigurationGridViewText;
                            }
                            rowInfo.Cells[18].Value = channelInfo.NoiseShift_1 - 6;
                            rowInfo.Cells[19].Value = channelInfo.MinNoiseLevel_1;

                            /// group 3 stuff
                            if (channelInfo.NoiseOn_2 == 1)
                            {
                                rowInfo.Cells[20].Value = true;
                            }
                            else
                            {
                                rowInfo.Cells[20].Value = false;
                            }
                            if (channelInfo.NoiseType_2 == 0)
                            {
                                rowInfo.Cells[21].Value = slidingChannelConfigurationGridViewText;
                            }
                            else
                            {
                                rowInfo.Cells[21].Value = constantChannelConfigurationGridViewText;
                            }
                            rowInfo.Cells[22].Value = channelInfo.NoiseShift_2 - 6;
                            rowInfo.Cells[23].Value = channelInfo.MinNoiseLevel_2;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheChannelConfigurationGridView(PDM_ConfigComponent_ChannelInfo, ref GridViewRowInfo, bool, bool)\nInput GridViewRowInfo had too few cells.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheChannelConfigurationGridView(PDM_ConfigComponent_ChannelInfo, ref GridViewRowInfo, bool, bool)\nInput GridViewRowInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToOneRowOfTheChannelConfigurationGridView(PDM_ConfigComponent_ChannelInfo, ref GridViewRowInfo, bool, bool)\nInput PDM_ConfigComponent_ChannelInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToOneRowOfTheChannelConfigurationGridView(PDM_ConfigComponent_ChannelInfo, ref GridViewRowInfo, bool, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo> channelInfoList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                PDM_ConfigComponent_ChannelInfo channelInfo;
                if (channelInfoList != null)
                {
                    if (channelInfoList.Count == 15)
                    {
                        if (this.channelConfigurationRadGridView.RowCount == 15)
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                rowInfo = this.channelConfigurationRadGridView.Rows[i];
                                channelInfo = channelInfoList[i];

                                AddDataToOneRowOfTheChannelConfigurationGridView(channelInfo, ref rowInfo, true, true);                               
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddAllDataToTheChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo>)\nthis.channelConfigurationRadGridView did not have 15 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddAllDataToTheChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo>)\nInput List<PDM_ConfigComponent_ChannelInfo> did not have 15 entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddAllDataToTheChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo>)\nInput List<PDM_ConfigComponent_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddAllDataToTheChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



//        private void AddDataToChannelConfigurationGridView(List<PDM_ConfigComponent_ChannelInfo> channelInfoList)
//        {
//            try
//            {
//                string channelNumber;
//                int enableChannel;
//                int pulseWidth;
//                int deadTime;
//                int pdiCalculationLimit;
//                double sensitivity;
//                int sensorPhase;
//                int enablePolarity;
//                int enableTimeOfArr;
//                int referenceChannel;
//                int referenceChannelShift;
//                int group1Enable;
//                int group1ThresholdType;
//                int group1ThresholdOffset;
//                int group1ThresholdStop;
//                int group2Enable;
//                int group2ThresholdType;
//                int group2ThresholdOffset;
//                int group2ThresholdStop;
//                int group3Enable;
//                int group3ThresholdType;
//                int group3ThresholdOffset;
//                int group3ThresholdStop;

//                if (this.workingConfiguration != null)
//                {
//                    if (this.workingConfiguration.channelInfoList != null)
//                    {
//                        this.channelConfigurationRadGridView.Rows.Clear();

//                        foreach (PDM_ConfigComponent_ChannelInfo channelInfo in channelInfoList)
//                        {
//                            channelNumber = (channelInfo.ChannelNumber + 1).ToString();
//                            enableChannel = channelInfo.ChannelIsOn;
//                            pulseWidth = channelInfo.PDMaxWidth;
//                            deadTime = channelInfo.PDIntervalTime;
//                            pdiCalculationLimit = channelInfo.CalcPDILimit;
//                            sensitivity = channelInfo.ChannelSensitivity;
//                            sensorPhase = channelInfo.CHPhase;
//                            enablePolarity = channelInfo.Polarity_0;
//                            enableTimeOfArr = channelInfo.TimeOfArrival_0;
//                            referenceChannel = channelInfo.Ref_0;
//                            referenceChannelShift = channelInfo.RefShift_0;

//                            group1Enable = channelInfo.NoiseOn_0;
//                            group1ThresholdType = channelInfo.NoiseType_0;
//                            group1ThresholdOffset = channelInfo.NoiseShift_0 - 6;
//                            group1ThresholdStop = channelInfo.MinNoiseLevel_0;

//                            group2Enable = channelInfo.NoiseOn_1;
//                            group2ThresholdType = channelInfo.NoiseType_1;
//                            group2ThresholdOffset = channelInfo.NoiseShift_1 - 6;
//                            group2ThresholdStop = channelInfo.MinNoiseLevel_1;

//                            group3Enable = channelInfo.NoiseOn_2;
//                            group3ThresholdType = channelInfo.NoiseType_2;
//                            group3ThresholdOffset = channelInfo.NoiseShift_2 - 6;
//                            group3ThresholdStop = channelInfo.MinNoiseLevel_2;

//                            AddRowEntryToChannelConfiguration(channelNumber, enableChannel, pulseWidth, deadTime, pdiCalculationLimit, sensitivity, sensorPhase,
//                                                              enablePolarity, enableTimeOfArr, referenceChannel, referenceChannelShift, group1Enable, group1ThresholdType,
//                                                              group1ThresholdOffset, group1ThresholdStop, group2Enable, group2ThresholdType, group2ThresholdOffset,
//                                                              group2ThresholdStop, group3Enable, group3ThresholdType, group3ThresholdOffset, group3ThresholdStop);
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.FillChannelConfigurationGridView()\nthis.currentConfigration.channelInfoList was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.FillChannelConfigurationGridView()\nthis.currentConfigration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.FillChannelConfigurationGridView()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


 

//        private void AddRowEntryToChannelConfiguration(string channelNumber, int enableChannel, int pulseWidth, int deadTime, int pdiCalculationLimit, double sensitivity,
//                                                 int sensorPhase, int enablePolarity, int enableTimeOfArr, int referenceChannel, int referenceChannelShift,
//                                                 int group1Enable, int group1ThresholdType, int group1ThresholdOffset, int group1ThresholdStop,
//                                                 int group2Enable, int group2ThresholdType, int group2ThresholdOffset, int group2ThresholdStop,
//                                                 int group3Enable, int group3ThresholdType, int group3ThresholdOffset, int group3ThresholdStop)
//        {
//            try
//            {                
//                GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(info);

//                if (channelNumber != null)
//                {
//                    rowInfo.Cells[0].Value = channelNumber;
//                    if (enableChannel == 1)
//                    {
//                        rowInfo.Cells[1].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[1].Value = false;
//                    }
//                    if (pulseWidth == 1)
//                    {
//                        rowInfo.Cells[2].Value = "640";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[2].Value = "1280";
//                    }
//                    if (deadTime == 1)
//                    {
//                        rowInfo.Cells[3].Value = "1280";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[3].Value = "2560";
//                    }

//                    rowInfo.Cells[4].Value = this.pdiCalculationLimits[pdiCalculationLimit - 1];
//                    rowInfo.Cells[5].Value = Math.Round(sensitivity, 2);
//                    rowInfo.Cells[6].Value = GetSensorPhaseAsString(sensorPhase);
//                    if (enablePolarity == 1)
//                    {
//                        rowInfo.Cells[7].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[7].Value = false;
//                    }
//                    if (enableTimeOfArr == 1)
//                    {
//                        rowInfo.Cells[8].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[8].Value = false;
//                    }
//                    if (referenceChannel == 0)
//                    {
//                        rowInfo.Cells[9].Value = "none";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[9].Value = referenceChannel.ToString();
//                    }
//                    rowInfo.Cells[10].Value = -referenceChannelShift;

//                    /// group 1 stuff
//                    if (group1Enable == 1)
//                    {
//                        rowInfo.Cells[11].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[11].Value = false;
//                    }
//                    if (group1ThresholdType == 0)
//                    {
//                        rowInfo.Cells[12].Value = "sliding";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[12].Value = "constant";
//                    }
//                    rowInfo.Cells[13].Value = group1ThresholdOffset;
//                    rowInfo.Cells[14].Value = group1ThresholdStop;

//                    /// group 2 stuff
//                    if (group2Enable == 1)
//                    {
//                        rowInfo.Cells[15].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[15].Value = false;
//                    }
//                    if (group2ThresholdType == 0)
//                    {
//                        rowInfo.Cells[16].Value = "sliding";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[16].Value = "constant";
//                    }
//                    rowInfo.Cells[17].Value = group2ThresholdOffset;
//                    rowInfo.Cells[18].Value = group2ThresholdStop;

//                    /// group 3 stuff
//                    if (group3Enable == 1)
//                    {
//                        rowInfo.Cells[19].Value = true;
//                    }
//                    else
//                    {
//                        rowInfo.Cells[19].Value = false;
//                    }
//                    if (group3ThresholdType == 0)
//                    {
//                        rowInfo.Cells[20].Value = "sliding";
//                    }
//                    else
//                    {
//                        rowInfo.Cells[20].Value = "constant";
//                    }
//                    rowInfo.Cells[21].Value = group3ThresholdOffset;
//                    rowInfo.Cells[22].Value = group3ThresholdStop;

//                    this.channelConfigurationRadGridView.Rows.Add(rowInfo);
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.AddRowEntryToChannelConfiguration(string, int, int, int, int, double, int, int, int, int, int,\n" +
//                                                       " int, int, int, int,  int, int, int, int, int, int, int, int)\nInput string was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddRowEntryToChannelConfiguration(string, int, int, int, int, double, int, int, int, int, int,\n" +
//                                                       " int, int, int, int,  int, int, int, int, int, int, int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


        private PDM_ConfigComponent_ChannelInfo GetChannelInfoGridEntriesForOneRow(GridViewRowInfo rowInfo)
        {
            PDM_ConfigComponent_ChannelInfo channelInfo = new PDM_ConfigComponent_ChannelInfo();
            try
            {
                
                if (rowInfo.Cells.Count > 22)
                {
                    /// Many of these are DropDownLists with only a few entries, so I just use the fact that we have already set the value
                    /// to zero, so there is no need to check the value corresponding to 0.

                    if ((bool)(rowInfo.Cells[1].Value) == true)
                    {
                        channelInfo.ChannelIsOn = 1;
                    }

                    channelInfo.PDMaxWidth = 1;

                    if (rowInfo.Cells[2].Value.ToString() == "1280")
                    {
                        channelInfo.PDMaxWidth = 2;
                    }

                    channelInfo.PDIntervalTime = 1;

                    if (rowInfo.Cells[3].Value.ToString() == "2560")
                    {
                        channelInfo.PDIntervalTime = 2;
                    }

                    channelInfo.CalcPDILimit = GetSelectedIndexFromPDILimitEntry(rowInfo.Cells[4].Value.ToString());

                    channelInfo.ChannelSensitivity = 0;
                    if (rowInfo.Cells[5].Value != null)
                    {
                        Double.TryParse(rowInfo.Cells[5].Value.ToString(), out channelInfo.ChannelSensitivity);
                    }

                    channelInfo.CHPhase = GetSelectedIndexFromSensorPhase(rowInfo.Cells[6].Value.ToString());

                    // we are now saving two values in this variable
                    channelInfo.Polarity_0 = 0;
                    if ((bool)(rowInfo.Cells[7].Value) == true)
                    {
                        channelInfo.Polarity_0 += 1;
                    }

                    if ((bool)(rowInfo.Cells[8].Value) == true)
                    {
                        channelInfo.Polarity_0 += 16;
                    }

                    if ((bool)(rowInfo.Cells[9].Value) == true)
                    {
                        channelInfo.TimeOfArrival_0 = 1;
                    }

                    if (rowInfo.Cells[10].Value.ToString().CompareTo("none") != 0)
                    {
                        Int32.TryParse(rowInfo.Cells[10].Value.ToString(), out channelInfo.Ref_0);
                        //if (referenceChannel == (i + 1))
                        //{
                        //    RadMessageBox.Show(this, "Error in Reference Channel value in PD settings tag:\nThe reference channel is the same as the channel number");
                        //    this.configurationError = true;
                        //}
                    }

                    channelInfo.RefShift_0 = 0;
                    if (rowInfo.Cells[11].Value != null)
                    {
                        Int32.TryParse(rowInfo.Cells[11].Value.ToString(), out channelInfo.RefShift_0);
                    }
                    channelInfo.RefShift_0 = -channelInfo.RefShift_0;

                    if ((bool)(rowInfo.Cells[12].Value) == true)
                    {
                        channelInfo.NoiseOn_0 = 1;
                    }
                    if (rowInfo.Cells[13].Value.ToString() != slidingChannelConfigurationGridViewText)
                    {
                        channelInfo.NoiseType_0 = 1;
                    }

                    Int32.TryParse(rowInfo.Cells[14].Value.ToString(), out channelInfo.NoiseShift_0);
                    channelInfo.NoiseShift_0 += 6;

                    channelInfo.MinNoiseLevel_0 = 0;
                    if (rowInfo.Cells[15].Value != null)
                    {
                        Int32.TryParse(rowInfo.Cells[15].Value.ToString(), out channelInfo.MinNoiseLevel_0);
                    }

                    if ((bool)(rowInfo.Cells[16].Value) == true)
                    {
                        channelInfo.NoiseOn_1 = 1;
                    }
                    if (rowInfo.Cells[17].Value.ToString() != slidingChannelConfigurationGridViewText)
                    {
                        channelInfo.NoiseType_1 = 1;
                    }

                    Int32.TryParse(rowInfo.Cells[18].Value.ToString(), out channelInfo.NoiseShift_1);
                    channelInfo.NoiseShift_1 += 6;

                    channelInfo.MinNoiseLevel_1 = 0;
                    if (rowInfo.Cells[19].Value != null)
                    {
                        Int32.TryParse(rowInfo.Cells[19].Value.ToString(), out channelInfo.MinNoiseLevel_1);
                    }

                    if ((bool)(rowInfo.Cells[20].Value) == true)
                    {
                        channelInfo.NoiseOn_2 = 1;
                    }

                    if (rowInfo.Cells[21].Value.ToString() !=slidingChannelConfigurationGridViewText)
                    {
                        channelInfo.NoiseType_2 = 1;
                    }

                    Int32.TryParse(rowInfo.Cells[22].Value.ToString(), out channelInfo.NoiseShift_2);
                    channelInfo.NoiseShift_2 += 6;

                    channelInfo.MinNoiseLevel_2 = 0;
                    if (rowInfo.Cells[23].Value != null)
                    {
                        Int32.TryParse(rowInfo.Cells[23].Value.ToString(), out channelInfo.MinNoiseLevel_2);
                    }

                    /// Assign the values to the object

                    // channelInfo.ChannelNumber = zeroIndexChannelNumber + 1;
                    //channelInfo.ChannelIsOn = enableChannel;
                    //channelInfo.PDMaxWidth = pulseWidth;
                    //channelInfo.PDIntervalTime = deadTime;
                    //channelInfo.CalcPDILimit = pdiCalculationLimit;
                    //channelInfo.ChannelSensitivity = sensitivity;
                    //channelInfo.CHPhase = sensorPhase;
                    //channelInfo.Polarity_0 = enablePolarity;
                    //channelInfo.TimeOfArrival_0 = enableTimeOfArr;
                    //channelInfo.Ref_0 = referenceChannel;
                    //channelInfo.RefShift_0 = referenceChannelShift;

                    //channelInfo.NoiseOn_0 = group1Enable;
                    //channelInfo.NoiseType_0 = group1ThresholdType;
                    //channelInfo.NoiseShift_0 = group1ThresholdOffset;
                    //channelInfo.MinNoiseLevel_0 = group1ThresholdStop;

                    //channelInfo.NoiseOn_1 = group2Enable;
                    //channelInfo.NoiseType_1 = group2ThresholdType;
                    //channelInfo.NoiseShift_1 = group2ThresholdOffset;
                    //channelInfo.MinNoiseLevel_1 = group2ThresholdStop;

                    //channelInfo.NoiseOn_2 = group3Enable;
                    //channelInfo.NoiseType_2 = group3ThresholdType;
                    //channelInfo.NoiseShift_2 = group3ThresholdOffset;
                    //channelInfo.MinNoiseLevel_2 = group3ThresholdStop;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetChannelInfoGridEntriesForOneRow(GridViewRowInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfo;
        }

        private void WritePDSettingsChannelInfoGridEntriesToConfigurationData()
        {
            try
            {
                GridViewRowInfo rowInfo;
                PDM_ConfigComponent_ChannelInfo channelInfoInCurrent;
                PDM_ConfigComponent_ChannelInfo channelInfoFromRow;
               
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.channelInfoList != null)
                    {
                        if (this.workingConfiguration.channelInfoList.Count == 15)
                        {
                            if (this.channelConfigurationRadGridView.Rows.Count == 15)
                            {
                                for (int i = 0; i < 15; i++)
                                {
                                    channelInfoInCurrent = this.workingConfiguration.channelInfoList[i];
                                    rowInfo = this.channelConfigurationRadGridView.Rows[i];
                                    if (rowInfo.Cells.Count > 22)
                                    {
                                        channelInfoFromRow = GetChannelInfoGridEntriesForOneRow(rowInfo);

                                        /// Assign the values to the object
                                       
                                        // channelInfo.ChannelNumber = channelInfoFromRow.zeroIndexChannelNumber + 1;
                                        channelInfoInCurrent.ChannelIsOn = channelInfoFromRow.ChannelIsOn;
                                        channelInfoInCurrent.PDMaxWidth = channelInfoFromRow.PDMaxWidth;
                                        channelInfoInCurrent.PDIntervalTime = channelInfoFromRow.PDIntervalTime;
                                        channelInfoInCurrent.CalcPDILimit = channelInfoFromRow.CalcPDILimit;
                                        channelInfoInCurrent.ChannelSensitivity = channelInfoFromRow.ChannelSensitivity;
                                        channelInfoInCurrent.CHPhase = channelInfoFromRow.CHPhase;
                                        channelInfoInCurrent.Polarity_0 = channelInfoFromRow.Polarity_0;
                                        channelInfoInCurrent.TimeOfArrival_0 = channelInfoFromRow.TimeOfArrival_0;
                                        channelInfoInCurrent.Ref_0 = channelInfoFromRow.Ref_0;
                                        channelInfoInCurrent.RefShift_0 = channelInfoFromRow.RefShift_0;
                                        channelInfoInCurrent.NoiseOn_0 = channelInfoFromRow.NoiseOn_0;
                                        channelInfoInCurrent.NoiseType_0 = channelInfoFromRow.NoiseType_0;
                                        channelInfoInCurrent.NoiseShift_0 = channelInfoFromRow.NoiseShift_0;
                                        channelInfoInCurrent.MinNoiseLevel_0 = channelInfoFromRow.MinNoiseLevel_0;

                                        channelInfoInCurrent.NoiseOn_1 = channelInfoFromRow.NoiseOn_1;
                                        channelInfoInCurrent.NoiseType_1 = channelInfoFromRow.NoiseType_1;
                                        channelInfoInCurrent.NoiseShift_1 = channelInfoFromRow.NoiseShift_1;
                                        channelInfoInCurrent.MinNoiseLevel_1 = channelInfoFromRow.MinNoiseLevel_1;

                                        channelInfoInCurrent.NoiseOn_2 = channelInfoFromRow.NoiseOn_2;
                                        channelInfoInCurrent.NoiseType_2 = channelInfoFromRow.NoiseType_2;
                                        channelInfoInCurrent.NoiseShift_2 = channelInfoFromRow.NoiseShift_2;
                                        channelInfoInCurrent.MinNoiseLevel_2 = channelInfoFromRow.MinNoiseLevel_2;
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nthis.alarmSettingsRadGridView.Rows[" + i.ToString() + "].Cells has too few elements.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nthis.alarmSettingsRadGridView did not have 15 rows.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nthis.workingConfiguration.channelInfoList did not have 15 entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nthis.workingConfiguration.channelInfoList was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.WritePDSettingsChannelInfoGridEntriesToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void GetChannelConfigurationFromRowEntry(ref PDM_ConfigComponent_ChannelInfo channelInfo)
//        {
//            try
//            {
//                int zeroIndexChannelNumber;
//                GridViewRowInfo rowInfo = null;

//                int enableChannel = 0;
//                int pulseWidth = 1;
//                int deadTime = 1;
//                int pdiCalculationLimit = 0;
//                double sensitivity = 0.0;
//                int sensorPhase = 0;
//                int enablePolarity = 0;
//                int enableTimeOfArr = 0;
//                int referenceChannel = 0;
//                int referenceChannelShift = 0;
//                int group1Enable = 0;
//                int group1ThresholdType = 0;
//                int group1ThresholdOffset = 0;
//                int group1ThresholdStop = 0;
//                int group2Enable = 0;
//                int group2ThresholdType = 0;
//                int group2ThresholdOffset = 0;
//                int group2ThresholdStop = 0;
//                int group3Enable = 0;
//                int group3ThresholdType = 0;
//                int group3ThresholdOffset = 0;
//                int group3ThresholdStop = 0;

//                if (channelInfo != null)
//                {
//                    zeroIndexChannelNumber = channelInfo.ChannelNumber;
//                    if (this.channelConfigurationRadGridView.Rows.Count > zeroIndexChannelNumber)
//                    {
//                        rowInfo = this.channelConfigurationRadGridView.Rows[zeroIndexChannelNumber];
//                        if (rowInfo.Cells.Count > 22)
//                        {

//                            /// Many of these are DropDownLists with only a few entries, so I just use the fact that we have already set the value
//                            /// to zero, so there is no need to check the value corresponding to 0.

//                            if (rowInfo.Cells[1].Value.ToString().CompareTo("True") == 0)
//                            {
//                                enableChannel = 1;
//                            }

//                            if (rowInfo.Cells[2].Value.ToString() == "1280")
//                            {
//                                pulseWidth = 2;
//                            }

//                            if (rowInfo.Cells[3].Value.ToString() == "2560")
//                            {
//                                deadTime = 2;
//                            }

//                            pdiCalculationLimit = GetSelectedIndexFromPDILimitEntry(rowInfo.Cells[4].Value.ToString());

//                            Double.TryParse(rowInfo.Cells[5].Value.ToString(), out sensitivity);

//                            sensorPhase = GetSelectedIndexFromSensorPhase(rowInfo.Cells[6].Value.ToString());

//                            if (rowInfo.Cells[7].Value.ToString().CompareTo("True") == 0)
//                            {
//                                enablePolarity = 1;
//                            }

//                            if (rowInfo.Cells[8].Value.ToString().CompareTo("True") == 0)
//                            {
//                                enableTimeOfArr = 1;
//                            }

//                            if (rowInfo.Cells[9].Value.ToString().CompareTo("none") != 0)
//                            {
//                                Int32.TryParse(rowInfo.Cells[9].Value.ToString(), out referenceChannel);
//                                if (referenceChannel == (zeroIndexChannelNumber + 1))
//                                {
//                                    RadMessageBox.Show(this, "Error in Reference Channel value in PD settings tag:\nThe reference channel is the same as the channel number");
//                                    this.configurationError = true;
//                                }
//                            }

//                            Int32.TryParse(rowInfo.Cells[10].Value.ToString(), out referenceChannelShift);
//                            referenceChannelShift = -referenceChannelShift;

//                            if (rowInfo.Cells[11].Value.ToString().CompareTo("True") == 0)
//                            {
//                                group1Enable = 1;
//                            }
//                            if (rowInfo.Cells[12].Value.ToString() != "sliding")
//                            {
//                                group1ThresholdType = 1;
//                            }

//                            Int32.TryParse(rowInfo.Cells[13].Value.ToString(), out group1ThresholdOffset);
//                            group1ThresholdOffset += 6;

//                            Int32.TryParse(rowInfo.Cells[14].Value.ToString(), out group1ThresholdStop);

//                            if (rowInfo.Cells[15].Value.ToString().CompareTo("True") == 0)
//                            {
//                                group2Enable = 1;
//                            }
//                            if (rowInfo.Cells[16].Value.ToString() != "sliding")
//                            {
//                                group2ThresholdType = 1;
//                            }

//                            Int32.TryParse(rowInfo.Cells[17].Value.ToString(), out group2ThresholdOffset);
//                            group2ThresholdOffset += 6;

//                            Int32.TryParse(rowInfo.Cells[18].Value.ToString(), out group2ThresholdStop);

//                            if (rowInfo.Cells[19].Value.ToString().CompareTo("True") == 0)
//                            {
//                                group3Enable = 1;
//                            }
//                            if (rowInfo.Cells[20].Value.ToString() != "sliding")
//                            {
//                                group3ThresholdType = 1;
//                            }

//                            Int32.TryParse(rowInfo.Cells[21].Value.ToString(), out group3ThresholdOffset);
//                            group3ThresholdOffset += 6;

//                            Int32.TryParse(rowInfo.Cells[22].Value.ToString(), out group3ThresholdStop);

//                            /// Assign the values to the object

//                            // channelInfo.ChannelNumber = zeroIndexChannelNumber + 1;
//                            channelInfo.ChannelIsOn = enableChannel;
//                            channelInfo.PDMaxWidth = pulseWidth;
//                            channelInfo.PDIntervalTime = deadTime;
//                            channelInfo.CalcPDILimit = pdiCalculationLimit;
//                            channelInfo.ChannelSensitivity = sensitivity;
//                            channelInfo.CHPhase = sensorPhase;
//                            channelInfo.Polarity_0 = enablePolarity;
//                            channelInfo.TimeOfArrival_0 = enableTimeOfArr;
//                            channelInfo.Ref_0 = referenceChannel;
//                            channelInfo.RefShift_0 = referenceChannelShift;

//                            channelInfo.NoiseOn_0 = group1Enable;
//                            channelInfo.NoiseType_0 = group1ThresholdType;
//                            channelInfo.NoiseShift_0 = group1ThresholdOffset;
//                            channelInfo.MinNoiseLevel_0 = group1ThresholdStop;

//                            channelInfo.NoiseOn_1 = group2Enable;
//                            channelInfo.NoiseType_1 = group2ThresholdType;
//                            channelInfo.NoiseShift_1 = group2ThresholdOffset;
//                            channelInfo.MinNoiseLevel_1 = group2ThresholdStop;

//                            channelInfo.NoiseOn_2 = group3Enable;
//                            channelInfo.NoiseType_2 = group3ThresholdType;
//                            channelInfo.NoiseShift_2 = group3ThresholdOffset;
//                            channelInfo.MinNoiseLevel_2 = group3ThresholdStop;
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in PDM_MonitorConfiguration.GetChannelConfigurationFromRowEntry(ref PDM_Config_ChannelInfo)\nthis.channelConfigurationRadGridView.Rows[" + zeroIndexChannelNumber.ToString() + "].Cells had too few elements.";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.GetChannelConfigurationFromRowEntry(ref PDM_Config_ChannelInfo)\nthis.channelConfigurationRadGridView.Rows had too few elements.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.GetChannelConfigurationFromRowEntry(ref PDM_Config_ChannelInfo)\nInput PDM_Config_ChannelInfo was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetChannelConfigurationFromRowEntry(ref PDM_Config_ChannelInfo)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void AddDataToPDSettingsTabObjects(PDM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.setupInfo != null)
                    {
                        if (inputConfiguration.pdSetupInfo != null)
                        {
                            int synchronizationType = inputConfiguration.pdSetupInfo.SyncType;
                            double synchronizationFrequency = inputConfiguration.pdSetupInfo.InternalSyncFrequency;
                            int cyclesPerAcquisition = inputConfiguration.pdSetupInfo.ReadingSinPeriods;
                            int commonPhaseShift = inputConfiguration.pdSetupInfo.PhShift;
                            // int neutralType = inputConfiguration.setupInfo.ObjectType;
                            int rereadOnAlarm = inputConfiguration.setupInfo.ReReadOnAlarm;

                            synchronizationRadDropDownList.SelectedIndex = synchronizationType;
                            synchronizationFrequencyRadMaskedEditBox.Text = Math.Round(synchronizationFrequency, 2).ToString();
                            cyclesPerAcquisitionRadMaskedEditBox.Text = cyclesPerAcquisition.ToString();
                            commonPhaseShiftRadMaskedEditBox.Text = commonPhaseShift.ToString();

                            //if (neutralType == 1)
                            //{
                            //    commonNeutralRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            //}
                            //else
                            //{
                            //    insulatedNeutralRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            //}

                            if (rereadOnAlarm == 0)
                            {
                                rereadOnAlarmRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                            else
                            {
                                rereadOnAlarmRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToPDSettingsTabObjects(PDM_Configuration)\ninputConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToPDSettingsTabObjects(PDM_Configuration)\ninputConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToPDSettingsTabObjects(PDM_Configuration)\ninputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializePDSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MissingValueInChannelConfigurationRadGridView()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.channelConfigurationRadGridView.RowCount;
                int i, j;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.channelConfigurationRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    for (j = 0; j < cellCount; j++)
                    {
                        if (rowInfo.Cells[j].Value == null)
                        {
                            SelectPDSettingsTab();
                            rowInfo.Cells[j].IsSelected = true;
                            RadMessageBox.Show(this, emptyCellErrorMessage);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.MissingValueInChannelConfigurationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private bool ErrorIsPresentInAPDSettingsTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;

                //int synchronizationType = 0;
                double synchronizationFrequency = 0.0;
                int cyclesPerAcquisition = 0;
                int commonPhaseShift = 0;
                int referenceChannel;
                bool enabled;
                //int neutralType = 0;
                //int rereadOnAlarm = 0;

                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.setupInfo != null)
                    {
                        if (this.workingConfiguration.pdSetupInfo != null)
                        {
                            errorIsPresent = MissingValueInChannelConfigurationRadGridView();

                            if ((!errorIsPresent) && (!Double.TryParse(synchronizationFrequencyRadMaskedEditBox.Text, out synchronizationFrequency)))
                            {
                                configurationError = true;
                                SelectPDSettingsTab();
                                synchronizationFrequencyRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(cyclesPerAcquisitionRadMaskedEditBox.Text, out cyclesPerAcquisition)))
                            {
                                configurationError = true;
                                SelectPDSettingsTab();
                                cyclesPerAcquisitionRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(commonPhaseShiftRadMaskedEditBox.Text, out commonPhaseShift)))
                            {
                                configurationError = true;
                                SelectPDSettingsTab();
                                commonPhaseShiftRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if (this.channelConfigurationRadGridView.Rows.Count == 15)
                            {
                                for (int i = 0; i < 15; i++)
                                {
                                    rowInfo = this.channelConfigurationRadGridView.Rows[i];
                                    enabled = (Boolean)rowInfo.Cells[1].Value;
                                    if (enabled)
                                    {
                                        if (rowInfo.Cells[9].Value.ToString().CompareTo("none") != 0)
                                        {
                                            Int32.TryParse(rowInfo.Cells[9].Value.ToString(), out referenceChannel);
                                            if (referenceChannel == (i + 1))
                                            {
                                                RadMessageBox.Show(this, errorInReferenceChannelValueInPDSettingsText);
                                                errorIsPresent = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAPDSettingsTabObject()\nthis.workingConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAPDSettingsTabObject()\nthis.workingConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInAPDSettingsTabObject()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ErrorIsPresentInAPDSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WritePDSettingsTabObjectsToConfigurationData()
        {
            try
            {
                int synchronizationType = 0;
                double synchronizationFrequency = 0.0;
                int cyclesPerAcquisition = 0;
                int commonPhaseShift = 0;
                //int neutralType = 0;
                int rereadOnAlarm = 0;

                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.setupInfo != null)
                    {
                        if (this.workingConfiguration.pdSetupInfo != null)
                        {
                            synchronizationType = synchronizationRadDropDownList.SelectedIndex;
                            Double.TryParse(synchronizationFrequencyRadMaskedEditBox.Text, out synchronizationFrequency);
                            Int32.TryParse(cyclesPerAcquisitionRadMaskedEditBox.Text, out cyclesPerAcquisition);
                            Int32.TryParse(commonPhaseShiftRadMaskedEditBox.Text, out commonPhaseShift);

                            //if (commonNeutralRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            //{
                            //    neutralType = 1;
                            //}

                            if (rereadOnAlarmRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                rereadOnAlarm = 1;
                            }

                            this.workingConfiguration.pdSetupInfo.SyncType = synchronizationType;
                            this.workingConfiguration.pdSetupInfo.InternalSyncFrequency = synchronizationFrequency;
                            this.workingConfiguration.pdSetupInfo.ReadingSinPeriods = cyclesPerAcquisition;
                            this.workingConfiguration.pdSetupInfo.PhShift = commonPhaseShift;
                            // this.workingConfiguration.setupInfo.ObjectType = neutralType;
                            this.workingConfiguration.setupInfo.ReReadOnAlarm = rereadOnAlarm;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.WritePDSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.WritePDSettingsTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisablePDSettingsTabObjects()
        {
            try
            {
                synchronizationRadDropDownList.Enabled = false;
                synchronizationFrequencyRadMaskedEditBox.ReadOnly = true;
                rereadOnAlarmRadCheckBox.Enabled = false;
                cyclesPerAcquisitionRadMaskedEditBox.ReadOnly = true;
                commonPhaseShiftRadMaskedEditBox.ReadOnly = true;
                //commonNeutralRadRadioButton.Enabled = false;
                //insulatedNeutralRadRadioButton.Enabled = false;
                channelConfigurationRadGridView.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DisablePDSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnablePDSettingsTabObjects()
        {
            try
            {
                synchronizationRadDropDownList.Enabled = true;
                synchronizationFrequencyRadMaskedEditBox.ReadOnly = false;
                rereadOnAlarmRadCheckBox.Enabled = true;
                cyclesPerAcquisitionRadMaskedEditBox.ReadOnly = false;
                commonPhaseShiftRadMaskedEditBox.ReadOnly = false;
                //commonNeutralRadRadioButton.Enabled = true;
                //insulatedNeutralRadRadioButton.Enabled = true;
                channelConfigurationRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.EnablePDSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void HideSelectedPDSettingsColumns()
        {
            try
            {
                if (this.channelConfigurationRadGridView.ColumnCount > 22)
                {
                    HideChannelConfigurationColumn(2);
                    HideChannelConfigurationColumn(3);
                    HideChannelConfigurationColumn(4);
                    HideChannelConfigurationColumn(13);
                    HideChannelConfigurationColumn(14);
                    HideChannelConfigurationColumn(15);
                    HideChannelConfigurationColumn(17);
                    HideChannelConfigurationColumn(18);
                    HideChannelConfigurationColumn(19);
                    HideChannelConfigurationColumn(21);
                    HideChannelConfigurationColumn(22);
                    HideChannelConfigurationColumn(23);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.HideSelectedPDSettingsColumns()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UnHideSelectedPDSettingsColumns()
        {
            try
            {
                if (this.channelConfigurationRadGridView.ColumnCount > 22)
                {
                    ShowChannelConfigurationColumn(2, 90);
                    ShowChannelConfigurationColumn(3, 90);
                    ShowChannelConfigurationColumn(4, 80);
                    ShowChannelConfigurationColumn(13, 80);
                    ShowChannelConfigurationColumn(14, 85);
                    ShowChannelConfigurationColumn(15, 115);
                    ShowChannelConfigurationColumn(17, 80);
                    ShowChannelConfigurationColumn(18, 85);
                    ShowChannelConfigurationColumn(19, 115);
                    ShowChannelConfigurationColumn(21, 80);
                    ShowChannelConfigurationColumn(22, 85);
                    ShowChannelConfigurationColumn(23, 115);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.UnHideSelectedPDSettingsColumns()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void HideChannelConfigurationColumn(int columnIndex)
        {
            try
            {
                this.channelConfigurationRadGridView.Columns[columnIndex].AllowResize = true;
                this.channelConfigurationRadGridView.Columns[columnIndex].MinWidth = 1;
                this.channelConfigurationRadGridView.Columns[columnIndex].Width = 1;
                this.channelConfigurationRadGridView.Columns[columnIndex].AllowResize = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.HideChannelConfigurationColumn(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ShowChannelConfigurationColumn(int columnIndex, int width)
        {
            try
            {
                this.channelConfigurationRadGridView.Columns[columnIndex].AllowResize = true;
                this.channelConfigurationRadGridView.Columns[columnIndex].Width = width;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.HideChannelConfigurationColumn(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    } 
}
