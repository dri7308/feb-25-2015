using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using PasswordManagement;

namespace PDMonitorUtilities
{
    public partial class PDM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string modbusAddressContainsNonNumericalValueText = "Error in ModBus address, non-numerical value is present";
        private static string modbusAddressOutOfRangeText = "Error in ModBus address, value must be between 1 and 255 inclusive.";

        private static string measurementNumberMeasurementSettingsGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Measurement<br>Number</html>";
        private static string hourMeasurementSettingsRadGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Hour</html>";
        private static string minuteMeasurementSettingsRadGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Minute</html>";

        private static string firmwareVersionNotAvailableText = "not available";

        private void InitializeMeasurementSettingsRadGridView()
        {
            try
            {
                GridViewTextBoxColumn measurementNumberGridViewTextBoxColumn = new GridViewTextBoxColumn();
                measurementNumberGridViewTextBoxColumn.Name = "MeasurementNumber";
                measurementNumberGridViewTextBoxColumn.HeaderText =measurementNumberMeasurementSettingsGridViewText;
                measurementNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                measurementNumberGridViewTextBoxColumn.Width = 74;
                measurementNumberGridViewTextBoxColumn.AllowSort = false;
                measurementNumberGridViewTextBoxColumn.IsPinned = true;
                measurementNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewDecimalColumn hourEntryGridViewDecimalColumn = new GridViewDecimalColumn();
                hourEntryGridViewDecimalColumn.Name = "HourEntry";
                hourEntryGridViewDecimalColumn.HeaderText = hourMeasurementSettingsRadGridViewText;
                hourEntryGridViewDecimalColumn.DecimalPlaces = 0;
                hourEntryGridViewDecimalColumn.DisableHTMLRendering = false;
                hourEntryGridViewDecimalColumn.Width = 35;
                hourEntryGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn minuteEntryGridViewDecimalColumn = new GridViewDecimalColumn();
                minuteEntryGridViewDecimalColumn.Name = "MinuteEntry";
                minuteEntryGridViewDecimalColumn.HeaderText = minuteMeasurementSettingsRadGridViewText;
                minuteEntryGridViewDecimalColumn.DecimalPlaces = 0;
                minuteEntryGridViewDecimalColumn.DisableHTMLRendering = false;
                minuteEntryGridViewDecimalColumn.Width = 40;
                minuteEntryGridViewDecimalColumn.AllowSort = false;

                this.measurementSettingsRadGridView.Columns.Add(measurementNumberGridViewTextBoxColumn);
                this.measurementSettingsRadGridView.Columns.Add(hourEntryGridViewDecimalColumn);
                this.measurementSettingsRadGridView.Columns.Add(minuteEntryGridViewDecimalColumn);

                this.measurementSettingsRadGridView.TableElement.TableHeaderHeight = 30;
                this.measurementSettingsRadGridView.AllowColumnReorder = false;
                this.measurementSettingsRadGridView.AllowColumnChooser = false;
                this.measurementSettingsRadGridView.ShowGroupPanel = false;
                this.measurementSettingsRadGridView.EnableGrouping = false;
                this.measurementSettingsRadGridView.AllowAddNewRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeMeasurementSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToCommonSettingsTabObjects(PDM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.setupInfo != null)
                    {
                        if (inputConfiguration.pdSetupInfo != null)
                        {
                            int disableMonitoring = inputConfiguration.setupInfo.Stopped;
                            int saveMode = inputConfiguration.pdSetupInfo.SaveMode;
                            int prpddSaveMeasurements = inputConfiguration.pdSetupInfo.SaveNum;
                            int prpddSaveDays = inputConfiguration.pdSetupInfo.SaveDays;
                            int modBusAddress = inputConfiguration.setupInfo.DeviceNumber;
                            double channelsAtoCRatedVoltage = inputConfiguration.setupInfo.RatedVoltage_0;
                            double channelsatocRatedVoltage = inputConfiguration.setupInfo.RatedVoltage_1;
                            double channelsPD1toPD6 = inputConfiguration.setupInfo.RatedVoltage_2;
                            double channelsN1toN3 = inputConfiguration.setupInfo.RatedVoltage_3;
                            int baudRate = inputConfiguration.setupInfo.BaudRate;
                            int hours = inputConfiguration.setupInfo.DTime_Hour;
                            int minutes = inputConfiguration.setupInfo.DTime_Minute;
                            int readingsBySchedule = inputConfiguration.setupInfo.ScheduleType;
                           
                            double firmwareVersion = inputConfiguration.setupInfo.FirmwareVersion;
                            
                            double cpldVersion = Math.Round(inputConfiguration.setupInfo.Reserved_59 / 100.0, 2);

                            if (firmwareVersion > 1.0)
                            {
                                firmwareVersionValueRadLabel.Text = (Math.Round(firmwareVersion, 2)).ToString();
                            }
                            else
                            {
                                firmwareVersionValueRadLabel.Text = firmwareVersionNotAvailableText;
                            }
                            if (cpldVersion > 1.0)
                            {
                                cpldVersionValueRadLabel.Text = (Math.Round(cpldVersion, 2)).ToString();
                            }
                            else
                            {
                                cpldVersionValueRadLabel.Text = firmwareVersionNotAvailableText;
                            }

                            if ((firmwareVersion > 2.01))
                            {
                                this.oppositePolarityFeatureExists = true;
                            }
                            else
                            {
                                this.oppositePolarityFeatureExists = false;
                            } 

                            /// Monitoring group box
                            if (disableMonitoring == 0)
                            {
                                enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                disableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            /// Rated Voltage group box
                            channels1to3RatedVoltageRadMaskedEditBox.Text = Math.Round(channelsAtoCRatedVoltage, 1).ToString();
                            channels4to6RatedVoltageRadMaskedEditBox.Text = Math.Round(channelsatocRatedVoltage, 1).ToString();
                            channels7to12RatedVoltageRadMaskedEditBox.Text = Math.Round(channelsPD1toPD6, 1).ToString();
                            channels13to15RatedVoltageRadMaskedEditBox.Text = Math.Round(channelsN1toN3, 1).ToString();

                            /// Measurement Schedule group box
                            if (readingsBySchedule == 1)
                            {
                                stepMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                byScheduledMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            hourMeasurementScheduleRadMaskedEditBox.Text = hours.ToString();
                            minuteMeasurementScheduleRadMaskedEditBox.Text = minutes.ToString();

                            /// Connection Settings group box
                            modBusAddressRadMaskedEditBox.Text = modBusAddress.ToString();
                            baudRateRadDropDownList.Text = GetBaudRateFromSelectedIndex(baudRate).ToString();

                            /// Save group box
                            if (saveMode == 0)
                            {
                                normalSaveModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                testSaveModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            measurementsSavePRPDDRadMaskedEditBox.Text = prpddSaveMeasurements.ToString();
                            daySavePRPDDRadMaskedEditBox.Text = prpddSaveDays.ToString();

                            /// Measurement Schedule
                            if (readingsBySchedule == 1)
                            {
                                byScheduledMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                stepMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            hourMeasurementScheduleRadMaskedEditBox.Text = hours.ToString();
                            minuteMeasurementScheduleRadMaskedEditBox.Text = minutes.ToString();

                            /// Connection Settings group box
                            modBusAddressRadMaskedEditBox.Text = modBusAddress.ToString();
                            baudRateRadDropDownList.SelectedIndex = baudRate;

                            /// Save group box
                            if (saveMode == 0)
                            {
                                normalSaveModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                testSaveModeRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            measurementsSavePRPDDRadMaskedEditBox.Text = prpddSaveMeasurements.ToString();
                            daySavePRPDDRadMaskedEditBox.Text = prpddSaveDays.ToString();
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.InitializeCommonSettingsTabObjects()\ninputConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.InitializeCommonSettingsTabObjects()\ninputConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.InitializeCommonSettingsTabObjects()\ninputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.InitializeCommonSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInACommonSettingsTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                //int disableMonitoring = 0;
                //int saveMode = 0;
                int prpddSaveMeasurements = 0;
                int prpddSaveDays = 0;
                int modBusAddress = 0;
                double channelsAtoCRatedVoltage = 0.0;
                double channelsatocRatedVoltage = 0.0;
                double channelsPD1toPD6 = 0.0;
                double channelsN1toN3 = 0.0;
                //int baudRate = 0;
                int hours = 0;
                int minutes = 0;
                //int readingsBySchedule = 0;

                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.setupInfo != null)
                    {
                        if (this.workingConfiguration.pdSetupInfo != null)
                        {
                            errorIsPresent = MissingValueInMeasurementSettingsRadGridView();

                            if ((!errorIsPresent) && (!Int32.TryParse(modBusAddressRadMaskedEditBox.Text, out modBusAddress)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                modBusAddressRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, modbusAddressContainsNonNumericalValueText);
                            }
                            if ((modBusAddress < 1) || (modBusAddress > 255))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                modBusAddressRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, modbusAddressOutOfRangeText);
                            }

                            if ((!errorIsPresent) && (!Double.TryParse(channels1to3RatedVoltageRadMaskedEditBox.Text, out channelsAtoCRatedVoltage)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                channels1to3RatedVoltageRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Double.TryParse(channels4to6RatedVoltageRadMaskedEditBox.Text, out channelsatocRatedVoltage)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                channels4to6RatedVoltageRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Double.TryParse(channels7to12RatedVoltageRadMaskedEditBox.Text, out channelsPD1toPD6)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                channels7to12RatedVoltageRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Double.TryParse(channels13to15RatedVoltageRadMaskedEditBox.Text, out channelsN1toN3)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                channels13to15RatedVoltageRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(hourMeasurementScheduleRadMaskedEditBox.Text, out hours)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                hourMeasurementScheduleRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(minuteMeasurementScheduleRadMaskedEditBox.Text, out minutes)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                minuteMeasurementScheduleRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(measurementsSavePRPDDRadMaskedEditBox.Text, out prpddSaveMeasurements)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                measurementsSavePRPDDRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(daySavePRPDDRadMaskedEditBox.Text, out prpddSaveDays)))
                            {
                                errorIsPresent = true;
                                SelectCommonSettingsTab();
                                daySavePRPDDRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, nonNumericalValuePresentText);
                            }                           
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInACommonSettingsTabObject()\nthis.workingConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.CheckCommonSettingsTabObjectsForError()\nthis.workingConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.ErrorIsPresentInACommonSettingsTabObject()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.ErrorIsPresentInACommonSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteCommonSettingsTabObjectsToConfigurationData()
        {
            try
            {
                int disableMonitoring = 0;
                int saveMode = 0;
                int prpddSaveMeasurements = 0;
                int prpddSaveDays = 0;
                int modBusAddress = 0;
                double channelsAtoCRatedVoltage = 0.0;
                double channelsatocRatedVoltage = 0.0;
                double channelsPD1toPD6 = 0.0;
                double channelsN1toN3 = 0.0;
                int baudRate = 0;
                int hours = 0;
                int minutes = 0;
                int readingsBySchedule = 0;

                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.setupInfo != null)
                    {
                        if (this.workingConfiguration.pdSetupInfo != null)
                        {
                            if (disableMonitoringRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                disableMonitoring = 1;
                            }
                            Int32.TryParse(modBusAddressRadMaskedEditBox.Text, out modBusAddress);                           
                            baudRate = baudRateRadDropDownList.SelectedIndex;
                            Double.TryParse(channels1to3RatedVoltageRadMaskedEditBox.Text, out channelsAtoCRatedVoltage);
                            Double.TryParse(channels4to6RatedVoltageRadMaskedEditBox.Text, out channelsatocRatedVoltage);
                            Double.TryParse(channels7to12RatedVoltageRadMaskedEditBox.Text, out channelsPD1toPD6);
                            Double.TryParse(channels13to15RatedVoltageRadMaskedEditBox.Text, out channelsN1toN3);

                            if (byScheduledMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                readingsBySchedule = 1;
                            }

                            Int32.TryParse(hourMeasurementScheduleRadMaskedEditBox.Text, out hours);
                            Int32.TryParse(minuteMeasurementScheduleRadMaskedEditBox.Text, out minutes);

                            if (testSaveModeRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                saveMode = 1;
                            }

                            Int32.TryParse(measurementsSavePRPDDRadMaskedEditBox.Text, out prpddSaveMeasurements);
                            Int32.TryParse(daySavePRPDDRadMaskedEditBox.Text, out prpddSaveDays);

                            this.workingConfiguration.setupInfo.Stopped = disableMonitoring;
                            this.workingConfiguration.pdSetupInfo.SaveMode = saveMode;
                            this.workingConfiguration.pdSetupInfo.SaveNum = prpddSaveMeasurements;
                            this.workingConfiguration.pdSetupInfo.SaveDays = prpddSaveDays;
                            this.workingConfiguration.setupInfo.DeviceNumber = modBusAddress;

                            this.workingConfiguration.setupInfo.RatedVoltage_0 = channelsAtoCRatedVoltage;
                            this.workingConfiguration.setupInfo.RatedVoltage_1 = channelsatocRatedVoltage;
                            this.workingConfiguration.setupInfo.RatedVoltage_2 = channelsPD1toPD6;
                            this.workingConfiguration.setupInfo.RatedVoltage_3 = channelsN1toN3;

                            this.workingConfiguration.setupInfo.BaudRate = baudRate;
                            this.workingConfiguration.setupInfo.DTime_Hour = hours;
                            this.workingConfiguration.setupInfo.DTime_Minute = minutes;
                            this.workingConfiguration.setupInfo.ScheduleType = readingsBySchedule;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.WriteCommonSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration.pdSetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.WriteCommonSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration.setupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.WriteCommonSettingsTabObjectsToConfigurationData()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.WriteCommonSettingsTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToMeasurementSettingsGridView()
        {
            try
            {
                this.measurementSettingsRadGridView.Rows.Clear();
                for (int i = 0; i < 50; i++)
                {
                    this.measurementSettingsRadGridView.Rows.Add((i + 1), 0, 0);
                }
                this.measurementSettingsRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.FillMeasurementSettingsGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo> measurementsInfoList)
        {
            try
            {
                int count;
                int index = 0;
                GridViewRowInfo rowInfo;
                PDM_ConfigComponent_MeasurementsInfo measurementsInfo;
                if (measurementsInfoList != null)
                {
                    if (this.measurementSettingsRadGridView.RowCount == 50)
                    {
                        count = measurementsInfoList.Count;
                        if (count > 50)
                        {
                            count = 50;
                            string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nInput List<PDM_ConfigComponent_MeasurementsInfo> had more than 50 entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        else
                        {
                            for (index = 0; index < count; index++)
                            {
                                measurementsInfo = measurementsInfoList[index];
                                rowInfo = this.measurementSettingsRadGridView.Rows[index];
                                if (rowInfo.Cells.Count > 2)
                                {
                                    rowInfo.Cells[1].Value = measurementsInfo.Hour;
                                    rowInfo.Cells[2].Value = measurementsInfo.Minute;
                                }
                                else
                                {
                                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            for (; index < 50; index++)
                            {                                
                                rowInfo = this.measurementSettingsRadGridView.Rows[index];
                                if (rowInfo.Cells.Count > 2)
                                {
                                    rowInfo.Cells[1].Value = 0;
                                    rowInfo.Cells[2].Value = 0;
                                }
                                else
                                {
                                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have 50 rows.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nInput List<PDM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<PDM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void FillMeasurementSettingsGridView()
//        {
//            try
//            {
//                if (this.workingConfiguration != null)
//                {
//                    string entryNumber = "0";
//                    int hour = 0;
//                    int minute = 0;
//                    int numberOfNonEmptyEntries;
//                    int index;

//                    if (this.workingConfiguration.measurementsInfoList != null)
//                    {
//                        numberOfNonEmptyEntries = this.workingConfiguration.measurementsInfoList.Count;

//                        this.measurementSettingsRadGridView.Rows.Clear();

//                        for (index = 0; index < numberOfNonEmptyEntries; index++)
//                        {
//                            hour = this.workingConfiguration.measurementsInfoList[index].Hour;
//                            minute = this.workingConfiguration.measurementsInfoList[index].Minute;
//                            entryNumber = (index + 1).ToString();
//                            AddRowEntryToMeasurementSettings(entryNumber, hour, minute);
//                        }
//                        hour = 0;
//                        minute = 0;
//                        for (; index < 50; index++)
//                        {
//                            entryNumber = (index + 1).ToString();
//                            AddRowEntryToMeasurementSettings(entryNumber, hour, minute);
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in PDM_MonitorConfiguration.FillMeasurementSettingsGridView()\nthis.workingConfiguration.measurementsInfoList was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.FillMeasurementSettingsGridView()\nthis.workingConfiguration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.FillMeasurementSettingsGridView()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


        private bool MissingValueInMeasurementSettingsRadGridView()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.measurementSettingsRadGridView.RowCount;
                int i,j;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.measurementSettingsRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    for (j = 0; j < cellCount; j++)
                    {
                        if (rowInfo.Cells[j].Value == null)
                        {
                            SelectCommonSettingsTab();
                            rowInfo.Cells[j].IsSelected = true;
                            RadMessageBox.Show(this, emptyCellErrorMessage);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.MissingValueInMeasurementSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteMeasurementSettingsToConfigurationData()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    List<PDM_ConfigComponent_MeasurementsInfo> allMeasurementsInfo = new List<PDM_ConfigComponent_MeasurementsInfo>();
                    PDM_ConfigComponent_MeasurementsInfo singleMeasurementInfo;
                    List<PDM_ConfigComponent_MeasurementsInfo> sortedMeasurementsInfo;
                    int totalMeasurements;

                    for (int i = 0; i < 50; i++)
                    {
                        singleMeasurementInfo = new PDM_ConfigComponent_MeasurementsInfo();
                        singleMeasurementInfo.ItemNumber = i;
                        GetMeasurementSettingsFromRowEntry(ref singleMeasurementInfo, i);
                        if ((singleMeasurementInfo.Hour > 0) || (singleMeasurementInfo.Minute > 0))
                        {
                            allMeasurementsInfo.Add(singleMeasurementInfo);
                        }
                    }

                    var measurementsQuery =
                          from item in allMeasurementsInfo
                          orderby item.Hour ascending, item.Minute ascending
                          select item;

                    sortedMeasurementsInfo = measurementsQuery.ToList();
                    totalMeasurements = sortedMeasurementsInfo.Count;
                    for (int i = 0; i < totalMeasurements; i++)
                    {
                        sortedMeasurementsInfo[i].ItemNumber = i + 1;
                    }

                    this.workingConfiguration.measurementsInfoList = sortedMeasurementsInfo;
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.TranslateMeasurementSettingsToConfigurationData()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.TranslateMeasurementSettingsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void AddRowEntryToMeasurementSettings(string entryNumber, int hour, int minute)
//        {
//            try
//            {
//                GridViewInfo info;
//                GridViewDataRowInfo rowInfo;
//                if (entryNumber != null)
//                {
//                    info = new GridViewInfo(this.measurementSettingsRadGridView.MasterTemplate);
//                    rowInfo = new GridViewDataRowInfo(info);

//                    rowInfo.Cells[0].Value = entryNumber;
//                    rowInfo.Cells[1].Value = hour;
//                    rowInfo.Cells[2].Value = minute;

//                    this.measurementSettingsRadGridView.Rows.Add(rowInfo);
//                }
//                else
//                {
//                    string errorMessage = "Error in PDM_MonitorConfiguration.AddRowEntryToMeasurementSettings(string, int, int)\nInput string was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }

//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.AddRowEntryToMeasurementSettings(string, int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void GetMeasurementSettingsFromRowEntry(ref PDM_ConfigComponent_MeasurementsInfo measurementsInfo, int zeroIndexMeasurementNumber)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int hour=0;
                int minute=0;

                if (measurementsInfo != null)
                {
                    if (this.measurementSettingsRadGridView.Rows.Count > zeroIndexMeasurementNumber)
                    {
                        rowInfo = this.measurementSettingsRadGridView.Rows[zeroIndexMeasurementNumber];
                        if (rowInfo.Cells.Count > 2)
                        {
                            if (rowInfo.Cells[1].Value != null)
                            {
                                Int32.TryParse(rowInfo.Cells[1].Value.ToString(), out hour);
                            }
                            if (rowInfo.Cells[2].Value != null)
                            {
                                Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out minute);
                            }

                            measurementsInfo.ItemNumber = zeroIndexMeasurementNumber;
                            measurementsInfo.Hour = hour;
                            measurementsInfo.Minute = minute;
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref PDM_ConfigComponent_MeasurementsInfo, int)\nthis.measurementSettingsRadGridView.Rows[" + zeroIndexMeasurementNumber.ToString() + "].Cells had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref PDM_ConfigComponent_MeasurementsInfo, int)\nthis.measurementSettingsRadGridView.Rows had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref PDM_ConfigComponent_MeasurementsInfo, int)\nInput PDM_ConfigComponent_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref PDM_ConfigComponent_MeasurementsInfo, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableCommonSettingsTabObjects()
        {
            try
            {
                enableMonitoringRadRadioButton.Enabled = false;
                disableMonitoringRadRadioButton.Enabled = false;
                channels1to3RatedVoltageRadMaskedEditBox.ReadOnly = true;
                channels4to6RatedVoltageRadMaskedEditBox.ReadOnly = true;
                channels7to12RatedVoltageRadMaskedEditBox.ReadOnly = true;
                channels13to15RatedVoltageRadMaskedEditBox.ReadOnly = true;
                modBusAddressRadMaskedEditBox.ReadOnly = true;
                baudRateRadDropDownList.Enabled = false;
                normalSaveModeRadRadioButton.Enabled = false;
                testSaveModeRadRadioButton.Enabled = false;
                measurementsSavePRPDDRadMaskedEditBox.ReadOnly = true;
                daySavePRPDDRadMaskedEditBox.ReadOnly = true;
                stepMeasurementScheduleRadRadioButton.Enabled = false;
                byScheduledMeasurementScheduleRadRadioButton.Enabled = false;
                hourMeasurementScheduleRadMaskedEditBox.ReadOnly = true;
                minuteMeasurementScheduleRadMaskedEditBox.ReadOnly = true;
                measurementSettingsRadGridView.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.DisableCommonSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableCommonSettingsTabObjects()
        {
            try
            {
                enableMonitoringRadRadioButton.Enabled = true;
                disableMonitoringRadRadioButton.Enabled = true;
                channels1to3RatedVoltageRadMaskedEditBox.ReadOnly = false;
                channels4to6RatedVoltageRadMaskedEditBox.ReadOnly = false;
                channels7to12RatedVoltageRadMaskedEditBox.ReadOnly = false;
                channels13to15RatedVoltageRadMaskedEditBox.ReadOnly = false;
                modBusAddressRadMaskedEditBox.ReadOnly = false;
                baudRateRadDropDownList.Enabled = true;
                normalSaveModeRadRadioButton.Enabled = true;
                testSaveModeRadRadioButton.Enabled = true;
                measurementsSavePRPDDRadMaskedEditBox.ReadOnly = false;
                daySavePRPDDRadMaskedEditBox.ReadOnly = false;
                stepMeasurementScheduleRadRadioButton.Enabled = true;
                byScheduledMeasurementScheduleRadRadioButton.Enabled = true;
                hourMeasurementScheduleRadMaskedEditBox.ReadOnly = false;
                minuteMeasurementScheduleRadMaskedEditBox.ReadOnly = false;
                measurementSettingsRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.EnableCommonSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        //private void FillAlarmSettingsWithEmptyValues()
        //{

        //}

  

    }
}
