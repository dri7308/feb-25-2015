﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using GeneralUtilities;

namespace PDMonitorUtilities
{ 
    public partial class SetReportDates : Telerik.WinControls.UI.RadForm
    {
        private DateTime phaseResolvedDataDate = ConversionMethods.MinimumDateTime();
        public DateTime PhaseResolvedDataDate
        {
            get
            {
                return phaseResolvedDataDate;
            }
        }

        private DateTime startDataDateTime = ConversionMethods.MinimumDateTime();
        public DateTime StartDataDateTime
        {
            get
            {
                return startDataDateTime;
            }
        }

        private DateTime endDataDateTime = ConversionMethods.MinimumDateTime();
        public DateTime EndDataDateTime
        {
            get
            {
                return endDataDateTime;
            }
        }

        private bool dataDatesSet = false;
        public bool DataDatesSet
        {
            get
            {
                return dataDatesSet;
            }
        }
        
        DateTime[] phaseResolvedDataDates;
        DateTime[] dataDates;
        int phaseResolvedDataDatesCount;

        public SetReportDates(DateTime[] inputPhaseResolvedDataDates, DateTime[] inputDataDates)
        {
            InitializeComponent();

            this.phaseResolvedDataDates = inputPhaseResolvedDataDates;
            this.dataDates = inputDataDates;
        }

        private void SetReportDates_Load(object sender, EventArgs e)
        {
            try
            {
                if ((phaseResolvedDataDates != null) && (dataDates != null))
                {
                    this.phaseResolvedDataDatesCount = phaseResolvedDataDates.Length;

                    for (int i = 0; i < this.phaseResolvedDataDatesCount; i++)
                    {
                        this.phaseResolvedDataDatesRadListControl.Items.Add(phaseResolvedDataDates[i].ToString());
                    }
                    SetMostRecentDataDates();
                    this.phaseResolvedDataDatesRadListControl.SelectedIndex = this.phaseResolvedDataDatesCount - 1;
                }
                else
                {
                    string errorMessage = "Error in SetReportDates.SetReportDates_Load(object, EventArgs)\nAt least one input argument was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetReportDates.SetReportDates_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void useDefaultDatesRadButton_Click(object sender, EventArgs e)
        {
            SetMostRecentDataDates();
            dataDatesSet = true;
            this.Close();
        }

        private void useSelectedDatesRadButton_Click(object sender, EventArgs e)
        {
            dataDatesSet = true;
            this.Close();
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            dataDatesSet = false;
            this.Close();
        }

        private void mostRecentDataRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (mostRecentDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                endDataDateTime = dataDates[dataDates.Length - 1];
                startDataDateTime = dataDates[ArrayUtilities.FindGraphStartDateIndex(endDataDateTime.AddDays(-30), dataDates)];
                RefreshDataDateTextBoxes();
            }
        }

        private void dataBeforePhaseResolvedDataRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (dataBeforePhaseResolvedDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                endDataDateTime = phaseResolvedDataDate;
                startDataDateTime = dataDates[ArrayUtilities.FindGraphStartDateIndex(endDataDateTime.AddDays(-30), dataDates)];
                RefreshDataDateTextBoxes();
            }
        }

        private void phaseResolvedDataDatesRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            int selectedIndex = phaseResolvedDataDatesRadListControl.SelectedIndex;
            if (selectedIndex > -1)
            {
                if (selectedIndex != (this.phaseResolvedDataDatesCount - 1))
                {
                    dataBeforePhaseResolvedDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    mostRecentDataRadRadioButton.Enabled = false;
                }
                else
                {
                    mostRecentDataRadRadioButton.Enabled = true;
                }
                phaseResolvedDataDate = phaseResolvedDataDates[selectedIndex];
               
                if (mostRecentDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    endDataDateTime = dataDates[dataDates.Length - 1];
                }
                else if(dataBeforePhaseResolvedDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    endDataDateTime = phaseResolvedDataDate;
                }
                startDataDateTime = dataDates[ArrayUtilities.FindGraphStartDateIndex(endDataDateTime.AddDays(-30), dataDates)];
                RefreshDataDateTextBoxes();
            }
        }

        private void SetMostRecentDataDates()
        {
            try
            {
                if (mostRecentDataRadRadioButton.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    mostRecentDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                this.phaseResolvedDataDate = phaseResolvedDataDates[phaseResolvedDataDates.Length-1];
                this.endDataDateTime = dataDates[dataDates.Length - 1];
                this.startDataDateTime = dataDates[ArrayUtilities.FindGraphStartDateIndex(endDataDateTime.AddDays(-30), dataDates)];
                RefreshDataDateTextBoxes();
            }            
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetReportDates.SetMostRecentDataDates()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void RefreshDataDateTextBoxes()
        {
            try
            {
                this.phaseResolvedDataDateRadTextBox.Text = this.phaseResolvedDataDate.ToString();
                this.dataStartDateRadTextBox.Text = this.startDataDateTime.ToString();
                this.dataEndDateRadTextBox.Text = this.endDataDateTime.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetReportDates.RefreshDataDateTextBoxes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
       

       
    }
}
