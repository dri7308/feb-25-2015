﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using System.Data.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using Microsoft.Win32;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.RichTextBox;
using Telerik.WinControls.RichTextBox.Model;
using Telerik.WinControls.RichTextBox.FileFormats.Rtf; 

using ChartDirector;

using ConfigurationObjects;
using GeneralUtilities;
using DataObjects;
using PasswordManagement;
using DatabaseInterface;
using Dynamics;
using FormatConversion;
using MonitorUtilities;
using StatisticsDisplay;

namespace PDMonitorUtilities
{
    public partial class PDM_DataViewer : Telerik.WinControls.UI.RadForm
    {
        private int rowNumberChartWidth = 40;
        private int columnHeaderChartHeight = 40;

        private int chartWidth = 190;
        private int chartHeight = 80;

        private int plotStartX = 5;
        private int plotStartY = 5;

        private int prpddReportChartWidth = 205;
        private int prpddReportChartHeight = 200;
        private int prpddReportPlotStartX = 52;
        private int prpddReportPlotStartY = 28;

        private static List<string> paragraphTextList = null;

        private Bitmap CreateSparkChartRowNumber(string channelNumber)
        {
            Bitmap image = null;
            try
            {
                int localChartHeight = chartHeight;

                if (channelNumber == string.Empty)
                {
                    localChartHeight = columnHeaderChartHeight;
                }

                int plotWidth = rowNumberChartWidth - (2 * plotStartX);
                int plotHeight = localChartHeight - (2 * plotStartY);

                XYChart chart = new XYChart(rowNumberChartWidth, localChartHeight);

                chart.xAxis().setRounding(false, false);
                chart.yAxis().setRounding(false, false);
                chart.setTransparentColor(0xffffff);
                chart.xAxis().setColors(0xffffff, 0xffffff, 0xffffff, 0xffffff);
                chart.yAxis().setColors(0xffffff, 0xffffff, 0xffffff, 0xffffff);
                chart.setPlotArea(plotStartX, plotStartY, plotWidth, plotHeight, 0xffffff, 0xffffff, 0xffffff, 0xffffff, 0xffffff);

                chart.addText(plotWidth / 2, plotHeight / 2, channelNumber, "Times New Roman", 11, Chart.CColor(Color.Black)).setAlignment(Chart.Center);

                image = new Bitmap(chart.makeImage(), rowNumberChartWidth, localChartHeight);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateSparkChartRowNumber(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return image;
        }

        private Bitmap CreateSparkChartHeader(string plotTitle)
        {
            Bitmap image = null;
            try
            {
                int plotWidth = chartWidth - (2 * plotStartX);
                int plotHeight = columnHeaderChartHeight - (2 * plotStartY);

                XYChart chart = new XYChart(chartWidth, columnHeaderChartHeight);

                chart.xAxis().setRounding(false, false);
                chart.yAxis().setRounding(false, false);
                chart.setTransparentColor(0xffffff);
                chart.xAxis().setColors(0xffffff, 0xffffff, 0xffffff, 0xffffff);
                chart.yAxis().setColors(0xffffff, 0xffffff, 0xffffff, 0xffffff);
                chart.setPlotArea(plotStartX, plotStartY, plotWidth, plotHeight, 0xffffff, 0xffffff, 0xffffff, 0xffffff, 0xffffff);

                chart.addText(plotWidth / 2, plotHeight / 2, plotTitle, "Times New Roman", 11, Chart.CColor(Color.Black)).setAlignment(Chart.Center);

                image = new Bitmap(chart.makeImage(), chartWidth, columnHeaderChartHeight);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateSparkChartHeader(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return image;
        }

        private Paragraph CreateParagraphOfSparkChartHeaders()
        {
            Paragraph paragraph = null;
            try
            {
                Bitmap rowNumberHeader = null;
                Bitmap magnitudeHeader = null;
                Bitmap intensityHeader = null;
                Bitmap pulseCountHeader = null;

                SizeF headerSize = new SizeF(chartWidth, columnHeaderChartHeight);

                rowNumberHeader = CreateSparkChartRowNumber(string.Empty);
                magnitudeHeader = CreateSparkChartHeader("Magnitude");
                intensityHeader = CreateSparkChartHeader("Intensity");
                pulseCountHeader = CreateSparkChartHeader("Pulse Count");

                if ((magnitudeHeader != null) && (intensityHeader != null) && (pulseCountHeader != null))
                {
                    paragraph = new Paragraph();
                    paragraph.Inlines.Add(new ImageInline(rowNumberHeader, new SizeF(rowNumberChartWidth, columnHeaderChartHeight)));
                    paragraph.Inlines.Add(new ImageInline(magnitudeHeader, headerSize));
                    paragraph.Inlines.Add(new ImageInline(intensityHeader, headerSize));
                    paragraph.Inlines.Add(new ImageInline(pulseCountHeader, headerSize));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateParagraphOfSparkChartHeaders()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private Bitmap CreateSparkChart(double[] data, DateTime[] dataDateTimes)
        {
            Bitmap image = null;
            try
            {
                int plotWidth = chartWidth - (2 * plotStartX);
                int plotHeight = chartHeight - (2 * plotStartY);

                XYChart chart = new XYChart(chartWidth, chartHeight, Chart.CColor(Color.LightBlue), Chart.CColor(Color.Black));

                chart.xAxis().setAutoScale(0, 0, 0);
                chart.yAxis().setAutoScale(0, 0, 0);

                chart.xAxis().setRounding(false, false);
                chart.yAxis().setRounding(false, false);
                // chart.setTransparentColor(0xffffff);

                chart.setPlotArea(plotStartX, plotStartY, plotWidth, plotHeight, Chart.Transparent, Chart.Transparent, Chart.Transparent, Chart.Transparent, Chart.Transparent);

                LineLayer lineLayer = chart.addLineLayer2();
                lineLayer.setLineWidth(1);
                lineLayer.addDataSet(data);
                lineLayer.setXData(dataDateTimes);

                chart.xAxis().setColors(Chart.CColor(Color.Black), Chart.Transparent, Chart.Transparent, Chart.Transparent);
                chart.yAxis().setColors(Chart.CColor(Color.Black), Chart.Transparent, Chart.Transparent, Chart.Transparent);

                image = new Bitmap(chart.makeImage(), chartWidth, chartHeight);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateSparkChart(double[], DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return image;
        }

        private Paragraph CreateParagraphOfOneChannelOfSparkCharts(int channelNumber, double[] magnitude, double[] intensity, double[] pulseCount, DateTime[] dataReadingDates)
        {
            Paragraph paragraph = null;
            try
            {
                Bitmap rowNumberImage = null;
                Bitmap magnitudeImage = null;
                Bitmap intensityImage = null;
                Bitmap pulseCountImage = null;

                rowNumberImage = CreateSparkChartRowNumber(channelNumber.ToString());
                magnitudeImage = CreateSparkChart(magnitude, dataReadingDates);
                intensityImage = CreateSparkChart(intensity, dataReadingDates);
                pulseCountImage = CreateSparkChart(pulseCount, dataReadingDates);

                if ((magnitudeImage != null) && (intensityImage != null) && (pulseCountImage != null))
                {
                    paragraph = new Paragraph(); ;
                    paragraph.Inlines.Add(new ImageInline(rowNumberImage, new SizeF(rowNumberChartWidth, chartHeight)));
                    paragraph.Inlines.Add(new ImageInline(magnitudeImage));
                    paragraph.Inlines.Add(new ImageInline(intensityImage));
                    paragraph.Inlines.Add(new ImageInline(pulseCountImage));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateParagraphOfOneChannelOfSparkCharts(double[], double[], double[], DateTime[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraph;
        }

        private bool InitializeParagraphTextList()
        {
            bool success = false;
            try
            {
                StreamReader reader;
                List<string> paragraphSourceFileNameList;

                paragraphSourceFileNameList = new List<string>();

                if ((PDM_DataViewer.paragraphTextList == null) || (PDM_DataViewer.paragraphTextList.Count != 3))
                {
                    PDM_DataViewer.paragraphTextList = new List<string>();

                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "pd_report_paragraph1.txt"));
                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "pd_report_paragraph2.txt"));
                    paragraphSourceFileNameList.Add(Path.Combine(this.applicationDataPath, "pd_report_paragraph3.txt"));
                    paragraphTextList = new List<string>();

                    foreach (string fileName in paragraphSourceFileNameList)
                    {
                        if (File.Exists(fileName))
                        {
                            using (reader = new StreamReader(fileName))
                            {
                                if (reader != null)
                                {
                                    paragraphTextList.Add(reader.ReadToEnd());
                                }
                            }
                        }
                    }
                }
                if (PDM_DataViewer.paragraphTextList.Count == 3)
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.InitializeParagraphTextList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void CreateReport(DateTime startDate, DateTime endDate, DateTime phaseResolvedDataDate)
        {
            try
            {
                RtfFormatProvider provider = new RtfFormatProvider();

                RadDocument document = new RadDocument();
                Section section;

                string saveFileName = string.Empty;

                //Table table;
                //TableRow tableRow;
                //TableCell tableCell;

                Paragraph paragraph;
                Span span;
                List<Paragraph> paragraphList = new List<Paragraph>();

                List<string> companyPlantAndEquipmentNames;

                Bitmap logoImage = null;

                double[] magnitude;
                double[] intensity;
                double[] pulseCount;
                DateTime[] dataDateTimes;

                // RadMessageBox.Show(this, "At some point this will create a report.  Not right now, though.");

                // RadRichTextBox reportRadRichTextBox = new RadRichTextBox();

                int startIndex = ArrayUtilities.FindGraphStartDateIndex(startDate, this.commonData.readingDateTime);
                int endIndex = ArrayUtilities.FindGraphEndDateIndex(endDate, this.commonData.readingDateTime);
                int numberOfDataReadingsBeingDisplayed = endIndex - startIndex + 1;

                int numberOfActiveChannels = this.activeChannelNumberList.Count;
                int channelBeingGraphed;

                magnitude = new double[numberOfDataReadingsBeingDisplayed];
                intensity = new double[numberOfDataReadingsBeingDisplayed];
                pulseCount = new double[numberOfDataReadingsBeingDisplayed];
                dataDateTimes = new DateTime[numberOfDataReadingsBeingDisplayed];

                Array.Copy(this.commonData.readingDateTime, startIndex, dataDateTimes, 0, numberOfDataReadingsBeingDisplayed);

                // DR Logo
                section = new Section();
                logoImage = (Bitmap)Image.FromFile(Path.Combine(this.applicationDataPath, "DR_Logo_DocumentSize.bmp"));
                paragraph = new Paragraph();
                paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Right;
                paragraph.Inlines.Add(new ImageInline(logoImage));
                section.Blocks.Add(paragraph);
                document.Sections.Add(section);
                //paragraphList.Add(paragraph);

                // Title
                section = new Section();
                paragraph = new Paragraph();
                span = new Span("PD Report");
                span.FontSize = 20;
                span.FontStyle = TextStyle.Bold;
                paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                paragraph.Inlines.Add(span);
                section.Blocks.Add(paragraph);
                document.Sections.Add(section);
                // paragraphList.Add(paragraph);

                //// Company Plant Equipment info
                companyPlantAndEquipmentNames = General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy(this.monitorHierarchy);

                paragraph = new Paragraph();
                span = new Span("Company: " + companyPlantAndEquipmentNames[0] + "\t Plant: " + companyPlantAndEquipmentNames[1] + "\t Equipment: " + companyPlantAndEquipmentNames[2]);
                span.FontSize = 16;
                // span.FontStyle = TextStyle.Bold;
                paragraph.TextAlignment = Telerik.WinControls.RichTextBox.Layout.RadTextAlignment.Center;
                paragraph.Inlines.Add(span);

                section = new Section();
                section.Blocks.Add(paragraph);
                document.Sections.Add(section);


                if (InitializeParagraphTextList())
                {
                    paragraph = new Paragraph();
                    span = new Span("Sparklines for Channel Data from " + startDate.ToString() + " to " + endDate.ToString());
                    span.FontSize = 14;
                    paragraph.Inlines.Add(span);
                    paragraphList.Add(paragraph);

                    paragraph = CreateParagraphOfSparkChartHeaders();
                    if (paragraph != null)
                    {
                        paragraphList.Add(paragraph);
                    }

                    /// Add sparklines
                    for (int i = 0; i < numberOfActiveChannels; i++)
                    {
                        channelBeingGraphed = this.activeChannelNumberList[i];

                        Array.Copy(this.channelDataList[channelBeingGraphed].magnitudeMV, startIndex, magnitude, 0, numberOfDataReadingsBeingDisplayed);
                        Array.Copy(this.channelDataList[channelBeingGraphed].pdi, startIndex, intensity, 0, numberOfDataReadingsBeingDisplayed);
                        Array.Copy(this.channelDataList[channelBeingGraphed].pulseCount, startIndex, pulseCount, 0, numberOfDataReadingsBeingDisplayed);

                        paragraph = CreateParagraphOfOneChannelOfSparkCharts(channelBeingGraphed + 1, magnitude, intensity, pulseCount, dataDateTimes);
                        if (paragraph != null)
                        {
                            paragraphList.Add(paragraph);
                        }
                    }

                    paragraph = new Paragraph();
                    span = new Span("Phase Resolved Data for " + phaseResolvedDataDate.ToString());
                    span.FontSize = 14;
                    paragraph.Inlines.Add(span);
                    paragraphList.Add(paragraph);

                    paragraphList.AddRange(CreateAllPRPDDGraphParagraphs(phaseResolvedDataDate));

                    section = new Section();

                    paragraph = new Paragraph();
                    span = new Span("This is the final paragraph.  It is really short.");
                    paragraph.Inlines.Add(span);
                    paragraphList.Add(paragraph);

                    foreach (Paragraph entry in paragraphList)
                    {
                        section.Blocks.Add(entry);
                    }

                    document.Sections.Add(section);

                    saveFileName = "PD_Report_" + FileUtilities.GetCurrentDateTimeAsPartialFileNameString() + ".rtf";

                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.DefaultExt = ".rtf";
                    saveDialog.Filter = "Documents|*.rtf";
                    saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    saveDialog.FileName = saveFileName;
                    DialogResult dialogResult = saveDialog.ShowDialog();
                    using (Stream output = saveDialog.OpenFile())
                    {
                        provider.Export(document, output);
                    }

                    int waitcount = 0;
                    Thread.Sleep(500);
                    while(!File.Exists(saveDialog.FileName))
                    {
                        Thread.Sleep(200);
                        waitcount++;
                        if (waitcount > 19)
                        {
                            break;
                        }
                    }

                    // Check whether Microsoft Word is installed on this computer,
                    // by searching the HKEY_CLASSES_ROOT\Word.Application key.
                    using (var regWord = Registry.ClassesRoot.OpenSubKey("Word.Application"))
                    {
                        if (regWord == null)
                        {
                            RadMessageBox.Show("Word is not installed, cannot automatically open report file");
                        }
                        else
                        {
                            System.Diagnostics.Process p = new System.Diagnostics.Process();
                            p.StartInfo.FileName = "winword.exe";
                            p.StartInfo.Arguments = "\"" + saveDialog.FileName + "\"";
                            p.Start();
                        }
                    }

                    //System.Diagnostics.Process p = new System.Diagnostics.Process();
                    //p.StartInfo.FileName = "wordpad.exe";
                    //p.StartInfo.Arguments = "\"" + saveDialog.FileName + "\"";
                    //p.Start();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateReport()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Bitmap CreatePRPDDChart(List<MatrixEntry> matrixDataPlusList, List<MatrixEntry> matrixDataMinusList, double phaseShift, double localMaximumVoltage, int channelNumber, double pdi)
        {
            Bitmap image = null;
            try
            {
                XYChart xyChart = new XYChart(prpddReportChartWidth, prpddReportChartHeight, Chart.CColor(Color.White), Chart.CColor(Color.Black));

                double[] phaseData;
                double[] magnitudeData;
                double[] pulseCounts;

                double[] localMagnitudeSteps;

                int[] rowIndices;

                double pixelsPerMagnitudeUnit;
                double pixelsPerPhaseUnit;

                double viewPortStartDegrees;
                double viewPortEndDegrees;
                //int xStartIndex;
                //int xEndIndex;

             //   double scaleMax = ConvertOneVoltageToSelectedUnits(localMaximumVoltage, (channelNumber - 1));

                double scaleMax = localMaximumVoltage;

                double scaleMin = -scaleMax;

                double dbCorrectionConstant = 0.0;

                int index = 0;
                int pointCount = 0;

                int pixelsPerPhasePoint;
                // int pixelsForCurrentMagnitudePoint;
                //int widthExtension = this.Width - 1024;
                //int heightExtension = this.Height - 768;

                int prpddReportPlotWidth = this.prpddReportChartWidth - prpddReportPlotStartX - 18;
                int prpddReportPlotHeight = this.prpddReportChartHeight - prpddReportPlotStartY - 64;

                bool amplitudeScaleWasCorrectlySelected = true;
                string amplitudeScaleString = string.Empty;

                // double actualMagnitudeInDb = 0.0;

                if (phaseShift > 360.0)
                {
                    phaseShift -= 360.0;
                }
                if (matrixDataPlusList != null)
                {
                    if (matrixDataMinusList != null)
                    {
                        //localMagnitudeSteps = GetLocalMagnitudeStepsInSelectedUnits(channelNumber - 1);
                        localMagnitudeSteps = this.magnitudeSteps;
                        if (localMagnitudeSteps != null)
                        {
                            //if (this.matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            //{
                            //amplitudeScaleString = "Amplitude (dB)";
                            //dbCorrectionConstant = 80.0;
                            //scaleMax += dbCorrectionConstant;
                            //scaleMax = RoundUpDbScaleMaxValue(scaleMax);
                            //scaleMin = -scaleMax;
                            //                                }
                            //                                else if (this.matrixVRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            //                                {
                                                                amplitudeScaleString = "Amplitude (V)";
                            //                                }
                            //                                else if (this.matrixPcRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            //                                {
                            //                                    amplitudeScaleString = "Amplitude (pC)";
                            //                                }
                            //                                else
                            //                                {
                            //                                    amplitudeScaleWasCorrectlySelected = false;
                            //                                    string errorMessage = "Error in PDM_DataViewerReportGeneration.CreatePRPDDChart(MatrixData, MatrixData, double, double, int)\nAmplitude scale was not correctly selected.";
                            //                                    LogMessage.LogError(errorMessage);
                            //#if DEBUG
                            //                                    MessageBox.Show(errorMessage);
                            //#endif
                            //                                }
                            if (amplitudeScaleWasCorrectlySelected)
                            {
                                if (this.allMatrixDateTimeSelectedIndex > -1)
                                {

                                    // figure out the scaling needed to graph each point

                                    pixelsPerMagnitudeUnit = prpddReportPlotHeight / (2 * scaleMax);
                                    pixelsPerPhaseUnit = prpddReportPlotWidth / 360.0;

                                    pixelsPerPhasePoint = GetPointDimensionInPixels(pixelsPerPhaseUnit * 7.5);

                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 1 - Configure overall chart appearance. 
                                    ///////////////////////////////////////////////////////////////////////////////////////
   
                                    xyChart.setPlotArea(prpddReportPlotStartX, prpddReportPlotStartY, prpddReportPlotWidth, prpddReportPlotHeight);
                                    xyChart.setClipping();

                                    // Set axes width to 2 pixels
                                    xyChart.yAxis().setWidth(1);
                                    xyChart.xAxis().setWidth(1);

                                    xyChart.xAxis().setRounding(false, false);
                                    xyChart.yAxis().setRounding(false, false);

                                    xyChart.addTitle("Channel " + channelNumber.ToString(), "Times New Roman", 10);

                                    xyChart.addTitle2(Chart.Bottom, "PDI = " + Math.Round(pdi, 2).ToString() + "  Phase Shift = " + Math.Round(phaseShift, 1).ToString() + " deg.", "Times New Roman", 10);

                                    // Add a title to the y-axis
                                    xyChart.yAxis().setTitle(amplitudeScaleString, "Arial", 9);

                                    xyChart.xAxis().setTitle("Phase (deg)", "Arial", 9);

                                    // add data

                                    ///// if we have zoomed, we need to figure out how man points to display
                                    ///// the 360.0 refers to the total possible degrees displayed on the x axis
                                    viewPortStartDegrees = 0.0;
                                    viewPortEndDegrees = 360.0;

                                    // now adjust the viewport limits
                                    viewPortStartDegrees -= 1.0e-7;
                                    viewPortEndDegrees += 1.0e-7;

                                    /// add the sine waves to the chart
                                    /// 
                                    /// We only add the first wave if no other waves are present
                                    if ((!this.showZeroDegreeSineWave) && (!this.showOneTwentyDegreeSineWave) && (!this.showTwoFortyDegreeSineWave))
                                    {
                                        AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(Color.Red));
                                    }
                                    else
                                    {
                                        if (this.showZeroDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 0.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveAColor));
                                        }
                                        if (this.showOneTwentyDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 120.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveBColor));
                                        }
                                        if (this.showTwoFortyDegreeSineWave)
                                        {
                                            AddSineWaveToPRPDDChart(ref xyChart, scaleMax, 240.0, viewPortStartDegrees, viewPortEndDegrees, Chart.CColor(sineWaveCColor));
                                        }
                                    }

                                    // set up the data to be graphed, if there is in fact any data to graph

                                    index = 0;
                                    pointCount = matrixDataPlusList.Count + matrixDataMinusList.Count;
                                    if (pointCount > 0)
                                    {
                                        // convert the matrix data into scatter array data
                                        phaseData = new double[pointCount];
                                        magnitudeData = new double[pointCount];
                                        pulseCounts = new double[pointCount];

                                        rowIndices = new int[pointCount];

                                        int colorBeingUsed;

                                        // Note that the matricies are saved in 1-index form, but C# uses 0-index form, so we
                                        // need to convert it.
                                        foreach (MatrixEntry matrixEntry in matrixDataPlusList)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            magnitudeData[index] = localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant;
                                            rowIndices[index] = matrixEntry.rowNumber - 1;
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        foreach (MatrixEntry matrixEntry in matrixDataMinusList)
                                        {
                                            phaseData[index] = this.phaseSteps[matrixEntry.columnNumber - 1];
                                            // since this is minus matrix data we need to change the sign for this value
                                            magnitudeData[index] = -(localMagnitudeSteps[matrixEntry.rowNumber - 1] + dbCorrectionConstant);
                                            pulseCounts[index] = matrixEntry.value;
                                            index++;
                                        }

                                        if (phaseShift > 1e-7)
                                        {
                                            phaseData = ShiftPhaseData(phaseData, phaseShift);
                                        }

                                        double[] singlePhaseData = new double[1];
                                        double[] singleMagnitudeData = new double[1];
                                        double[] singlePulseCount = new double[1];

                                        double[] singlePointWidth = new double[1];
                                        double[] singlePointHeight = new double[1];
                                        double[] pixelsPerPhaseWidthAsDouble = new double[1];

                                        double[] actualMagnitudeInDbAsArray = new double[1];

                                        double currentPhase;
                                        double currentMagnitude;

                                        pixelsPerPhaseWidthAsDouble[0] = pixelsPerPhaseUnit * 7.5 / 1.1;

                                        singlePointWidth[0] = 7.5;
                                        double[] currentPointHeightInPixelsAsDouble = new double[1];

                                        // add a 0 point to the graph at -80 db
                                        //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        //{

                                        //}

                                        for (int i = 0; i < pointCount; i++)
                                        {
                                            //if ((Math.Abs(magnitudeData[i]) < scaleMax)||
                                            //    ((matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)&&(Math.Abs(magnitudeData[i]) < Math.Abs(80 + scaleMax))))
                                            if (Math.Abs(magnitudeData[i]) < scaleMax)
                                            {
                                                currentPhase = phaseData[i];
                                                currentMagnitude = magnitudeData[i];
                                                //if ((currentPhase > viewPortStartDegrees) && (currentPhase < viewPortEndDegrees))
                                                //{
                                                singlePhaseData[0] = currentPhase;
                                                singleMagnitudeData[0] = currentMagnitude;
                                                singlePulseCount[0] = pulseCounts[i];

                                                colorBeingUsed = GetPointColorBasedOnPulseCount((int)(Math.Round(pulseCounts[i])));

                                                ScatterLayer layer = xyChart.addScatterLayer(singlePhaseData, singleMagnitudeData, "", Chart.CircleSymbol, 1, colorBeingUsed, colorBeingUsed);
                                                layer.addExtraField(singlePulseCount);
                                                //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                                //{
                                                //    if (currentMagnitude > 0)
                                                //    {
                                                //        actualMagnitudeInDb = currentMagnitude - dbCorrectionConstant;
                                                //    }
                                                //    else
                                                //    {
                                                //        actualMagnitudeInDb = (-currentMagnitude) - dbCorrectionConstant;
                                                //    }
                                                //    actualMagnitudeInDbAsArray[0] = actualMagnitudeInDb;
                                                //    layer.addExtraField(actualMagnitudeInDbAsArray);
                                                //}
                                                //layer.setSymbolScale(singlePointWidth, Chart.XAxisScale, singlePointWidth, Chart.XAxisScale);
                                                // layer.setSymbolScale(new double[] { pixelsPerPhasePoint }, Chart.PixelScale);
                                                layer.setSymbolScale(pixelsPerPhaseWidthAsDouble, Chart.PixelScale);
                                                //}
                                                //}
                                            }
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 3 - Set up x-axis scale
                                    //////////////////////////////////////////////////////////////////////////////////////

                                    if (prpddReportChartWidth < 400.0)
                                    {
                                        xyChart.xAxis().setLinearScale(0.0, 360.0, 90.0, 15.0);
                                    }
                                    else if (prpddReportChartWidth < 600.0)
                                    {
                                        xyChart.xAxis().setLinearScale(0.0, 360.0, 45.0, 5.0);
                                    }
                                    else
                                    {
                                        xyChart.xAxis().setLinearScale(0.0, 360.0, 15.0, 5.0);
                                    }

                                    //if (matrixDbRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    //{
                                    //    ReplaceCurrentAxisLabelsWithDbLabels(ref xyChart);
                                    //}

                                    ///////////////////////////////////////////////////////////////////////////////////////
                                    // Step 5 - Display the chart
                                    ///////////////////////////////////////////////////////////////////////////////////////

                                    image = new Bitmap(xyChart.makeImage(), prpddReportChartWidth, prpddReportChartHeight);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_DataViewerReportGeneration.CreatePRPDDChart(MatrixData, MatrixData, double, double, int)\nFailed to compute the local magnitude steps.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_DataViewerReportGeneration.CreatePRPDDChart(MatrixData, MatrixData, double, double, int)\nSecond input MatrixData was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DataViewerReportGeneration.CreatePRPDDChart(MatrixData, MatrixData, double, double, int)\nFirst input MatrixData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreatePRPDDChart(MatrixData, MatrixData, double, double, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return image;
        }

        private List<Paragraph> CreateAllPRPDDGraphParagraphs(DateTime prDataDate)
        {
            List<Paragraph> paragraphList = new List<Paragraph>();
            try
            {
                int zeroIndexedChannelNumber;
                int count;
                int limit;
                Paragraph paragraph;
                Bitmap image;
                SizeF imageSize = new SizeF(prpddReportChartWidth, prpddReportChartHeight);
                int channelDataDateIndex = ArrayUtilities.FindGraphStartDateIndex(prDataDate, this.commonData.readingDateTime);

                bool[] activeChannelsForPrDataDate = GetActiveChannels(this.commonData.registeredChannels[channelDataDateIndex]);
                List<int> activeChannelNumberListForPrDataDate = GetActiveChannelNumberList(activeChannelsForPrDataDate);

                if (this.hasFinishedInitialization && (this.totalDataReadings > 0))
                {
                    if (this.activeChannelNumberListForSelectedDate != null)
                    {
                        count = this.activeChannelNumberListForSelectedDate.Count;
                        {
                            for (int i = 0; i < count; i += 3)
                            {
                                paragraph = new Paragraph();
                                limit = i + 3;
                                if (limit > count)
                                {
                                    limit = count;
                                }

                                for (int j = i; j < limit; j++)
                                {
                                    image = null;
                                    zeroIndexedChannelNumber = activeChannelNumberListForPrDataDate[j];
                                    image = SetupAndCreateOnePRPDDGraphImage(zeroIndexedChannelNumber, channelDataDateIndex, prDataDate);
                                    if (image != null)
                                    {
                                        paragraph.Inlines.Add(new ImageInline(image, imageSize));
                                    }
                                }
                                paragraphList.Add(paragraph);
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_DataViewerReportGeneration.CreateAllPRPDDGraphParagraphs()\nthis.activeChannelNumberListForSelectedDate was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.CreateAllPRPDDGraphParagraphs(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paragraphList;
        }

        /// <summary>
        /// Sets up the data to draw one PRPDD graph for a given channel.
        /// </summary>
        /// <param name="zeroIndexChartNumber"></param>
        /// <param name="zeroIndexedChannelNumber"></param>
        private Bitmap SetupAndCreateOnePRPDDGraphImage(int zeroIndexedChannelNumber, int channelDataDateIndex, DateTime prDataDate)
        {
            Bitmap image = null;
            try
            {
                if ((zeroIndexedChannelNumber > -1) && (zeroIndexedChannelNumber < 15))
                {

                    int prDataDateIndex = ArrayUtilities.FindGraphStartDateIndex(prDataDate, this.allMatrixDateTimes);
                    double phaseShift = GetPhaseShiftValueFromConfigurationChPhaseEntry(this.currentMonitorConfiguration.channelInfoList[zeroIndexedChannelNumber].CHPhase);
                    double yScaleMax = this.maxVoltageOverAllChannels[prDataDateIndex];
                    yScaleMax = AdjustYscaleSetting(yScaleMax);
                    //if ((this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0) ||
                    //           (this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[this.allMatrixDateTimeSelectedIndex].Count > 0))
                    {
                        image = CreatePRPDDChart(this.channelPlusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[prDataDateIndex],
                                                 this.channelMinusMatrixDataList[zeroIndexedChannelNumber].matrixEntries[prDataDateIndex],
                                                 phaseShift, yScaleMax, zeroIndexedChannelNumber + 1, this.channelDataList[zeroIndexedChannelNumber].pdi[channelDataDateIndex]);
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DataViewerReportGeneration.SetupAndCreateOnePRPDDGraphImage(int, DateTime)\nSecond input int needs to be between 0 and 14 inclusive";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.SetupAndCreateOnePRPDDGraphImage(int, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return image;
        }

        private double GetPhaseShiftValueFromConfigurationChPhaseEntry(int chPhase)
        {
            double phaseShift = 0.0;
            try
            {
                switch (chPhase)
                {
                    case 0:
                        phaseShift = 0.0;
                        break;
                    case 1:
                        phaseShift = 120.0;
                        break;
                    case 2:
                        phaseShift = 240.0;
                        break;
                    case 3:
                        phaseShift = 60.0;
                        break;
                    case 4:
                        phaseShift = 180.0;
                        break;
                    case 5:
                        phaseShift = 300.0;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewerReportGeneration.GetPhaseShiftValueFromConfigurationChPhaseEntry(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseShift;
        }
    }
}
