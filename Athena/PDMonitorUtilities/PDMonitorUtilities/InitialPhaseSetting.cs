using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;

using ChartDirector;

namespace PDMonitorUtilities
{
    public partial class InitialPhaseSetting : Telerik.WinControls.UI.RadForm
    {
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string initialPhaseSettingTitleText = "Common Phase Shift";

        private static string phaseShiftInformationRadLabelText = "Phase shift between phase 1<br>and phase reference signal:";
        private static string showVoltageSinewaveRadGroupBoxText = "Show voltage sinewave";
        private static string zeroDegreeRadCheckBoxText = "1 (0)";
        private static string oneTwentyDegreeRadCheckBoxText = "2 (120)";
        private static string twoFortyDegreeRadCheckBoxText = "3 (240)";
        private static string okayRadButtonText = "OK";
        private static string cancelRadButtonText = "Cancel";

        private static string phaseText = "Phase";
        private static string colorText = "Color";
        private static string changeColorRadButtonText = "Change Color";

        private Color sineWaveAColor;
        public Color SineWaveAColor
        {
            get
            {
                return sineWaveAColor;
            }
        }

        private Color sineWaveBColor;
        public Color SineWaveBColor
        {
            get
            {
                return sineWaveBColor;
            }
        }

        private Color sineWaveCColor;
        public Color SineWaveCColor
        {
            get
            {
                return sineWaveCColor;
            }
        }

        private double phaseShift;
        public double PhaseShift
        {
            get
            {
                return phaseShift;
            }
        }

        private bool showZeroDegreeSineWave;
        public bool ShowZeroDegreeSineWave
        {
            get
            {
                return showZeroDegreeSineWave;
            }
        }

        private bool showOneTwentyDegreeSineWave;
        public bool ShowOneTwentyDegreeSineWave
        {
            get
            {
                return showOneTwentyDegreeSineWave;
            }
        }

        private bool showTwoFortyDegreeSineWave;
        public bool ShowTwoFortyDegreeSineWave
        {
            get
            {
                return showTwoFortyDegreeSineWave;
            }
        }

        private bool updatesAreRequired;
        public bool UpdatesAreRequired
        {
            get
            {
                return updatesAreRequired;
            }
        }

        private double initialPhaseShift;
        private bool initialShowZeroDegreeSineWave;
        private bool initialShowOneTwentyDegreeSineWave;
        private bool initialShowTwoFortyDegreeSineWave;

        private Color initialSineWaveAColor;
        private Color initialSineWaveBColor;
        private Color initialSineWaveCColor;

        public InitialPhaseSetting(double inputPhaseShift, bool inputShowZeroDegreeSineWave, bool inputShowOneTwentyDegreeSineWave, bool inputShowTwoFortyDegreeSineWave, Color inputSineWaveAColor, Color inputSineWaveBColor, Color inputSineWaveCColor)
        {
            try
            {
                InitializeComponent();

                this.initialPhaseShift = inputPhaseShift;

                this.initialShowZeroDegreeSineWave = inputShowZeroDegreeSineWave;
                this.initialShowOneTwentyDegreeSineWave = inputShowOneTwentyDegreeSineWave;
                this.initialShowTwoFortyDegreeSineWave = inputShowTwoFortyDegreeSineWave;

                this.sineWaveAColor = inputSineWaveAColor;
                this.sineWaveBColor = inputSineWaveBColor;
                this.sineWaveCColor = inputSineWaveCColor;

                this.initialSineWaveAColor = inputSineWaveAColor;
                this.initialSineWaveBColor = inputSineWaveBColor;
                this.initialSineWaveCColor = inputSineWaveCColor;

                if (this.initialShowZeroDegreeSineWave)
                {
                    this.zeroDegreeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (this.initialShowOneTwentyDegreeSineWave)
                {
                    this.oneTwentyDegreeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                if (this.initialShowTwoFortyDegreeSineWave)
                {
                    this.twoFortyDegreeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }

                this.phaseShiftRadSpinEditor.Value = GetDecimalEquivalentOfPhaseShift(this.initialPhaseShift);

                this.updatesAreRequired = false;

                //this.sineWaveARadColorBox.ColorDialog.SelectedColor = Color.Yellow;
                //this.sineWaveARadColorBox.ForeColor = Color.Yellow;
                //this.sineWaveARadColorBox
                AssignStringValuesToInterfaceObjects();
                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.InitialPhaseSetting(double, bool, bool, bool, Color, Color, Color)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitialPhaseSetting_Load(object sender, EventArgs e)
        {
            try
            {
                sineWaveAColorPanel.BackColor = sineWaveAColor;
                sineWaveBColorPanel.BackColor = sineWaveBColor;
                sineWaveCColorPanel.BackColor = sineWaveCColor;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.InitialPhaseSetting_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void okayRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                showZeroDegreeSineWave = false;
                showOneTwentyDegreeSineWave = false;
                showTwoFortyDegreeSineWave = false;

                if (this.zeroDegreeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    showZeroDegreeSineWave = true;
                }

                if (this.oneTwentyDegreeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    showOneTwentyDegreeSineWave = true;
                }

                if (this.twoFortyDegreeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    showTwoFortyDegreeSineWave = true;
                }

                phaseShift = (Double)this.phaseShiftRadSpinEditor.Value;

                this.updatesAreRequired = true;

                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.okayRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.showZeroDegreeSineWave = this.initialShowZeroDegreeSineWave;
                this.showOneTwentyDegreeSineWave = this.initialShowOneTwentyDegreeSineWave;
                this.showTwoFortyDegreeSineWave = this.initialShowTwoFortyDegreeSineWave;

                this.sineWaveAColor = this.initialSineWaveAColor;
                this.sineWaveBColor = this.initialSineWaveBColor;
                this.sineWaveCColor = this.initialSineWaveCColor;

                this.phaseShift = this.initialPhaseShift;

                this.updatesAreRequired = false;

                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.cancelRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Decimal GetDecimalEquivalentOfPhaseShift(double phaseShift)
        {
            Decimal decimalPhaseShift = 0.0M;
            try
            {
                decimalPhaseShift = (Decimal)Math.Round(phaseShift, 2);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.GetDecimalEquivalentOfPhaseShift(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return decimalPhaseShift;
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = initialPhaseSettingTitleText;

                phaseShiftInformationRadLabel.Text = phaseShiftInformationRadLabelText;
                showVoltageSinewaveRadGroupBox.Text = showVoltageSinewaveRadGroupBoxText;
                zeroDegreeRadCheckBox.Text = zeroDegreeRadCheckBoxText;
                oneTwentyDegreeRadCheckBox.Text = oneTwentyDegreeRadCheckBoxText;
                twoFortyDegreeRadCheckBox.Text = twoFortyDegreeRadCheckBoxText;
                okayRadButton.Text = okayRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, htmlFontType, htmlStandardFontSize, "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, htmlFontType, htmlStandardFontSize, "");

                initialPhaseSettingTitleText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceText", initialPhaseSettingTitleText, htmlFontType, htmlStandardFontSize, "");
                phaseShiftInformationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfacePhaseShiftInformationRadLabelText", phaseShiftInformationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                showVoltageSinewaveRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceShowVoltageSinewaveRadGroupBoxText", showVoltageSinewaveRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                zeroDegreeRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceZeroDegreeRadCheckBoxText", zeroDegreeRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                oneTwentyDegreeRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceOneTwentyDegreeRadCheckBoxText", oneTwentyDegreeRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                twoFortyDegreeRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceTwoFortyDegreeRadCheckBoxText", twoFortyDegreeRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                okayRadButtonText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceOkayRadButtonText", okayRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");

                phaseText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfacePhaseText", phaseText, htmlFontType, htmlStandardFontSize, "");
                colorText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceColorText", colorText, htmlFontType, htmlStandardFontSize, "");
                changeColorRadButtonText = LanguageConversion.GetStringAssociatedWithTag("InitialPhaseSettingInterfaceChangeColorRadButtonText", changeColorRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void changeSineWaveAColorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //ColorDialog colorDlg = new ColorDialog();
                //colorDlg.AllowFullOpen = false;
                //colorDlg.AnyColor = false;
                //colorDlg.SolidColorOnly = false;

                //colorDlg.Color = Color.Red;

                colorDialog1.AllowFullOpen = false;
                colorDialog1.AnyColor = false;
                colorDialog1.SolidColorOnly = true;
                colorDialog1.Color = sineWaveAColor;

                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    sineWaveAColor = colorDialog1.Color;
                    sineWaveAColorPanel.BackColor = sineWaveAColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.changeSineWaveAColorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void changeSineWaveBColorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                colorDialog1.AllowFullOpen = false;
                colorDialog1.AnyColor = false;
                colorDialog1.SolidColorOnly = true;
                colorDialog1.Color = sineWaveBColor;

                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    sineWaveBColor = colorDialog1.Color;
                    sineWaveBColorPanel.BackColor = sineWaveBColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.changeSineWaveBColorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void changeSineWaveCColorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                colorDialog1.AllowFullOpen = false;
                colorDialog1.AnyColor = false;
                colorDialog1.SolidColorOnly = true;
                colorDialog1.Color = sineWaveCColor;

                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    sineWaveCColor = colorDialog1.Color;
                    sineWaveCColorPanel.BackColor = sineWaveCColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InitialPhaseSetting.changeSineWaveCColorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }      
    }
}
