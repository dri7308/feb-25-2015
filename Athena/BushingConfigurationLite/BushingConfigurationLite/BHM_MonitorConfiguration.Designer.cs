namespace BushingConfigurationLite
{
    partial class BHM_MonitorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BHM_MonitorConfiguration));
            this.configurationRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.commonParametersRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileCommonParametersTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileCommonParametersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileCommonParametersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationCommonParametersTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadSelectedConfigurationCommonParametersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCommonParametersTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.commonParametersRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceCommonParametersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceCommonParametersTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.measurementScheduleRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.measurementSettingsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.byScheduleMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.measurementScheduleMinuteRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.measurementScheduleHourRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.minuteMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hourMeasurementScheduleRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.stepMeasurementScheduleRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.basicSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.monitoringRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.disableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.allowablePhaseDifferenceUnitsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.allowablePhaseDifferenceRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.daysToComputeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.rateOfChangeRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tangentRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.temperatureCoefficientRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.tangentRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.temperatureCoefficientRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.trendRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.allowablePhaseDifferenceRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.averagingUnnRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.averagingUnnRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.confirmationMeasurementUnitsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.confirmationMeasurementRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.confirmationMeasurementRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.hysterisisUnitsradLabel = new Telerik.WinControls.UI.RadLabel();
            this.hysterisisRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.hysterisisRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.baudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.baudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modbusAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modBusAddressRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileFirstBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileFirstBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsFirstBushingSetTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadSelectedConfigurationFirstBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsFirstBushingSetTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.firstBushingSetRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.firstBushingSetTransformerNameplateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetTemperatureAtMeasurementRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetTangentPhaseCRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetTangentPhaseBRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetTangentPhaseARadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetCapacitancePhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetCapacitancePhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetCapacitancePhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTangentPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTangentPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTangentPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.programDeviceFirstBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.firstBushingSetMeasurementsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetDisableMeasurementsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetEnableMeasurementsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetTemperatureSensorRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetTemperatureSensorRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.firstBushingSetInputImpedenceRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetPhaseCRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.firstBushingSetPhaseBRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.firstBushingSetPhaseARadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.firstBushingSetPhaseCOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetPhaseBOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetPhaseAOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetThresholdsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetRateOfChangeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetRateOfChangeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetTemperatureCoeffRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.firstBushingSetAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetAmplitudeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetAmplitudeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firstBushingSetkVRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetAmpsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetCurrentRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetVoltageRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.firstBushingSetCurrentRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.firstBushingSetVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileSecondBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileSecondBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsSecondBushingSetTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadSelectedConfigurationSecondBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsSecondBushingSetTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.secondBushingSetRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceSecondBushingSetTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.secondBushingSetMeasurementsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetDisableMeasurementsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetEnableMeasurementsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetTemperatureSensorRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetTemperatureSensorRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.secondBushingSetTransformerNameplateRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetTemperatureAtMeasurementRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetTangentPhaseCRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetTangentPhaseBRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetTangentPhaseARadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetCapacitancePhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetCapacitancePhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetCapacitancePhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTangentPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTangentPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTangentPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetInputImpedenceRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetPhaseCRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.secondBushingSetPhaseBRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.secondBushingSetPhaseARadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.secondBushingSetPhaseCOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetPhaseBOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetPhaseAOhmsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetThresholdsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetRateOfChangeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetRateOfChangeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetTemperatureCoeffRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTemperatureCoeffAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.secondBushingSetAmplitudeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetAmplitudeAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetAmplitudeWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.secondBushingSetkVRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetAmpsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetCurrentRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetVoltageRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.secondBushingSetCurrentRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.secondBushingSetVoltageRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.initialBalanceDataRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadFromFileInitialBalanceDataTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveToFileInitialBalanceDataTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            this.initialBalanceDataRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceInitialBalanceDataTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.initialBalanceDataTabMeasurementDateValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabMeasurementDateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseCRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseBRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabShiftPhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet1RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabAmplitudePhaseARadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabBushingSet2RadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabTemperatureRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabImbalanceRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.initialBalanceDataTabImbalancePhaseRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).BeginInit();
            this.configurationRadPageView.SuspendLayout();
            this.commonParametersRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileCommonParametersTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileCommonParametersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileCommonParametersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationCommonParametersTabRadGroupBox)).BeginInit();
            this.templateConfigurationCommonParametersTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationCommonParametersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonParametersTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonParametersRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonParametersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonParametersTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).BeginInit();
            this.measurementScheduleRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduleMeasurementScheduleRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleMinuteRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleHourRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.basicSettingsRadGroupBox)).BeginInit();
            this.basicSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).BeginInit();
            this.monitoringRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceUnitsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysToComputeRadGroupBox)).BeginInit();
            this.daysToComputeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateOfChangeRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.averagingUnnRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.averagingUnnRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementUnitsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisUnitsradLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).BeginInit();
            this.firstBushingSetRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileFirstBushingSetTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileFirstBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileFirstBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsFirstBushingSetTabRadGroupBox)).BeginInit();
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationFirstBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsFirstBushingSetTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceFirstBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTransformerNameplateRadGroupBox)).BeginInit();
            this.firstBushingSetTransformerNameplateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureAtMeasurementRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseCRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseBRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseARadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseCRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseBRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseARadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceFirstBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetMeasurementsRadGroupBox)).BeginInit();
            this.firstBushingSetMeasurementsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetDisableMeasurementsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetEnableMeasurementsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureSensorRadGroupBox)).BeginInit();
            this.firstBushingSetTemperatureSensorRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureSensorRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetInputImpedenceRadGroupBox)).BeginInit();
            this.firstBushingSetInputImpedenceRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseARadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseAOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetThresholdsRadGroupBox)).BeginInit();
            this.firstBushingSetThresholdsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeRadGroupBox)).BeginInit();
            this.firstBushingSetRateOfChangeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmThresholdRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmOffRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmOnRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffRadGroupBox)).BeginInit();
            this.firstBushingSetTemperatureCoeffRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatrreCoeffAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeRadGroupBox)).BeginInit();
            this.firstBushingSetAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetSettingsRadGroupBox)).BeginInit();
            this.firstBushingSetSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetkVRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmpsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCurrentRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetVoltageRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCurrentRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetVoltageRadMaskedEditBox)).BeginInit();
            this.secondBushingSetRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileSecondBushingSetTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileSecondBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileSecondBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSecondBushingSetTabRadGroupBox)).BeginInit();
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationSecondBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSecondBushingSetTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceSecondBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceSecondBushingSetTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetMeasurementsRadGroupBox)).BeginInit();
            this.secondBushingSetMeasurementsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetDisableMeasurementsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetEnableMeasurementsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureSensorRadGroupBox)).BeginInit();
            this.secondBushingSetTemperatureSensorRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureSensorRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTransformerNameplateRadGroupBox)).BeginInit();
            this.secondBushingSetTransformerNameplateRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureAtMeasurementRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseCRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseBRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseARadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseCRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseBRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseARadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetInputImpedenceRadGroupBox)).BeginInit();
            this.secondBushingSetInputImpedenceRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseARadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseAOhmsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetThresholdsRadGroupBox)).BeginInit();
            this.secondBushingSetThresholdsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeRadGroupBox)).BeginInit();
            this.secondBushingSetRateOfChangeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmThresholdRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmOffRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmOnRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffRadGroupBox)).BeginInit();
            this.secondBushingSetTemperatureCoeffRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeRadGroupBox)).BeginInit();
            this.secondBushingSetAmplitudeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetSettingsRadGroupBox)).BeginInit();
            this.secondBushingSetSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetkVRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmpsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCurrentRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetVoltageRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCurrentRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetVoltageRadMaskedEditBox)).BeginInit();
            this.initialBalanceDataRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox)).BeginInit();
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileInitialBalanceDataTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileInitialBalanceDataTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsInitialBalanceDataTabRadGroupBox)).BeginInit();
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationInitialBalanceDataTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsInitialBalanceDataTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceInitialBalanceDataTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseCRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseBRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseARadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2RadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabTemperatureRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalanceRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalancePhaseRadLabel)).BeginInit();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationRadPageView
            // 
            this.configurationRadPageView.Controls.Add(this.commonParametersRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.firstBushingSetRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.secondBushingSetRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.initialBalanceDataRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.radPageViewPage1);
            this.configurationRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configurationRadPageView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationRadPageView.Location = new System.Drawing.Point(0, 0);
            this.configurationRadPageView.Name = "configurationRadPageView";
            this.configurationRadPageView.SelectedPage = this.commonParametersRadPageViewPage;
            this.configurationRadPageView.Size = new System.Drawing.Size(872, 480);
            this.configurationRadPageView.TabIndex = 0;
            this.configurationRadPageView.Text = "radPageView1";
            this.configurationRadPageView.ThemeName = "Office2007Black";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.configurationRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // commonParametersRadPageViewPage
            // 
            this.commonParametersRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.commonParametersRadPageViewPage.Controls.Add(this.xmlConfigurationFileCommonParametersTabRadGroupBox);
            this.commonParametersRadPageViewPage.Controls.Add(this.templateConfigurationCommonParametersTabRadGroupBox);
            this.commonParametersRadPageViewPage.Controls.Add(this.commonParametersRadProgressBar);
            this.commonParametersRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceCommonParametersTabRadButton);
            this.commonParametersRadPageViewPage.Controls.Add(this.programDeviceCommonParametersTabRadButton);
            this.commonParametersRadPageViewPage.Controls.Add(this.measurementScheduleRadGroupBox);
            this.commonParametersRadPageViewPage.Controls.Add(this.basicSettingsRadGroupBox);
           // this.commonParametersRadPageViewPage.ItemSize = new System.Drawing.SizeF(124F, 26F);
            this.commonParametersRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.commonParametersRadPageViewPage.Name = "commonParametersRadPageViewPage";
            this.commonParametersRadPageViewPage.Size = new System.Drawing.Size(851, 434);
            this.commonParametersRadPageViewPage.Text = "Common Parameters";
            // 
            // xmlConfigurationFileCommonParametersTabRadGroupBox
            // 
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Controls.Add(this.loadFromFileCommonParametersTabRadButton);
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Controls.Add(this.saveToFileCommonParametersTabRadButton);
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Location = new System.Drawing.Point(3, 357);
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Name = "xmlConfigurationFileCommonParametersTabRadGroupBox";
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Size = new System.Drawing.Size(258, 75);
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.TabIndex = 56;
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileCommonParametersTabRadButton
            // 
            this.loadFromFileCommonParametersTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileCommonParametersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileCommonParametersTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadFromFileCommonParametersTabRadButton.Name = "loadFromFileCommonParametersTabRadButton";
            this.loadFromFileCommonParametersTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.loadFromFileCommonParametersTabRadButton.TabIndex = 36;
            this.loadFromFileCommonParametersTabRadButton.Text = "Load  File";
            this.loadFromFileCommonParametersTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileCommonParametersTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileCommonParametersTabRadButton
            // 
            this.saveToFileCommonParametersTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileCommonParametersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileCommonParametersTabRadButton.Location = new System.Drawing.Point(136, 23);
            this.saveToFileCommonParametersTabRadButton.Name = "saveToFileCommonParametersTabRadButton";
            this.saveToFileCommonParametersTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.saveToFileCommonParametersTabRadButton.TabIndex = 35;
            this.saveToFileCommonParametersTabRadButton.Text = "Save File";
            this.saveToFileCommonParametersTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileCommonParametersTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationCommonParametersTabRadGroupBox
            // 
            this.templateConfigurationCommonParametersTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationCommonParametersTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationCommonParametersTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationCommonParametersTabRadGroupBox.Controls.Add(this.loadSelectedConfigurationCommonParametersTabRadButton);
            this.templateConfigurationCommonParametersTabRadGroupBox.Controls.Add(this.templateConfigurationsCommonParametersTabRadDropDownList);
            this.templateConfigurationCommonParametersTabRadGroupBox.Controls.Add(this.radButton4);
            this.templateConfigurationCommonParametersTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationCommonParametersTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationCommonParametersTabRadGroupBox.Location = new System.Drawing.Point(3, 279);
            this.templateConfigurationCommonParametersTabRadGroupBox.Name = "templateConfigurationCommonParametersTabRadGroupBox";
            this.templateConfigurationCommonParametersTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationCommonParametersTabRadGroupBox.Size = new System.Drawing.Size(572, 65);
            this.templateConfigurationCommonParametersTabRadGroupBox.TabIndex = 55;
            this.templateConfigurationCommonParametersTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationCommonParametersTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadSelectedConfigurationCommonParametersTabRadButton
            // 
            this.loadSelectedConfigurationCommonParametersTabRadButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.loadSelectedConfigurationCommonParametersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadSelectedConfigurationCommonParametersTabRadButton.Location = new System.Drawing.Point(444, 11);
            this.loadSelectedConfigurationCommonParametersTabRadButton.Name = "loadSelectedConfigurationCommonParametersTabRadButton";
            this.loadSelectedConfigurationCommonParametersTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadSelectedConfigurationCommonParametersTabRadButton.TabIndex = 34;
            this.loadSelectedConfigurationCommonParametersTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadSelectedConfigurationCommonParametersTabRadButton.ThemeName = "Office2007Black";
            this.loadSelectedConfigurationCommonParametersTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsCommonParametersTabRadDropDownList
            // 
            this.templateConfigurationsCommonParametersTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCommonParametersTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCommonParametersTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsCommonParametersTabRadDropDownList.Name = "templateConfigurationsCommonParametersTabRadDropDownList";
            this.templateConfigurationsCommonParametersTabRadDropDownList.Size = new System.Drawing.Size(424, 18);
            this.templateConfigurationsCommonParametersTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsCommonParametersTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsCommonParametersTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsCommonParametersTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton4
            // 
            this.radButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton4.Location = new System.Drawing.Point(0, 232);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(130, 70);
            this.radButton4.TabIndex = 8;
            this.radButton4.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton4.ThemeName = "Office2007Black";
            // 
            // commonParametersRadProgressBar
            // 
            this.commonParametersRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commonParametersRadProgressBar.Location = new System.Drawing.Point(552, 357);
            this.commonParametersRadProgressBar.Name = "commonParametersRadProgressBar";
            this.commonParametersRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.commonParametersRadProgressBar.TabIndex = 43;
            this.commonParametersRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceCommonParametersTabRadButton
            // 
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Location = new System.Drawing.Point(552, 393);
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Name = "loadConfigurationFromDeviceCommonParametersTabRadButton";
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.TabIndex = 13;
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceCommonParametersTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceCommonParametersTabRadButton
            // 
            this.programDeviceCommonParametersTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceCommonParametersTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceCommonParametersTabRadButton.Location = new System.Drawing.Point(683, 393);
            this.programDeviceCommonParametersTabRadButton.Name = "programDeviceCommonParametersTabRadButton";
            this.programDeviceCommonParametersTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceCommonParametersTabRadButton.TabIndex = 9;
            this.programDeviceCommonParametersTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceCommonParametersTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceCommonParametersTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // measurementScheduleRadGroupBox
            // 
            this.measurementScheduleRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.measurementScheduleRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measurementScheduleRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.measurementScheduleRadGroupBox.Controls.Add(this.measurementSettingsRadGridView);
            this.measurementScheduleRadGroupBox.Controls.Add(this.byScheduleMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Controls.Add(this.measurementScheduleMinuteRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.measurementScheduleHourRadLabel);
            this.measurementScheduleRadGroupBox.Controls.Add(this.minuteMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.hourMeasurementScheduleRadMaskedEditBox);
            this.measurementScheduleRadGroupBox.Controls.Add(this.stepMeasurementScheduleRadRadioButton);
            this.measurementScheduleRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementScheduleRadGroupBox.HeaderText = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.Location = new System.Drawing.Point(631, 3);
            this.measurementScheduleRadGroupBox.Name = "measurementScheduleRadGroupBox";
            this.measurementScheduleRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.measurementScheduleRadGroupBox.Size = new System.Drawing.Size(217, 283);
            this.measurementScheduleRadGroupBox.TabIndex = 3;
            this.measurementScheduleRadGroupBox.Text = "Measurement Schedule";
            this.measurementScheduleRadGroupBox.ThemeName = "Office2007Black";
            // 
            // measurementSettingsRadGridView
            // 
            this.measurementSettingsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.measurementSettingsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementSettingsRadGridView.Location = new System.Drawing.Point(13, 83);
            this.measurementSettingsRadGridView.Name = "measurementSettingsRadGridView";
            this.measurementSettingsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.measurementSettingsRadGridView.Size = new System.Drawing.Size(191, 187);
            this.measurementSettingsRadGridView.TabIndex = 25;
            this.measurementSettingsRadGridView.Text = "radGridView1";
            this.measurementSettingsRadGridView.ThemeName = "Office2007Black";
            // 
            // byScheduleMeasurementScheduleRadRadioButton
            // 
           // this.byScheduleMeasurementScheduleRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.byScheduleMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.byScheduleMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.byScheduleMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 58);
            this.byScheduleMeasurementScheduleRadRadioButton.Name = "byScheduleMeasurementScheduleRadRadioButton";
            this.byScheduleMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(84, 16);
            this.byScheduleMeasurementScheduleRadRadioButton.TabIndex = 24;
            this.byScheduleMeasurementScheduleRadRadioButton.Text = "By Schedule";
            this.byScheduleMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // measurementScheduleMinuteRadLabel
            // 
            this.measurementScheduleMinuteRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.measurementScheduleMinuteRadLabel.Location = new System.Drawing.Point(170, 35);
            this.measurementScheduleMinuteRadLabel.Name = "measurementScheduleMinuteRadLabel";
            this.measurementScheduleMinuteRadLabel.Size = new System.Drawing.Size(42, 18);
            this.measurementScheduleMinuteRadLabel.TabIndex = 23;
            this.measurementScheduleMinuteRadLabel.Text = "Minute";
            // 
            // measurementScheduleHourRadLabel
            // 
            this.measurementScheduleHourRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.measurementScheduleHourRadLabel.Location = new System.Drawing.Point(101, 35);
            this.measurementScheduleHourRadLabel.Name = "measurementScheduleHourRadLabel";
            this.measurementScheduleHourRadLabel.Size = new System.Drawing.Size(31, 18);
            this.measurementScheduleHourRadLabel.TabIndex = 22;
            this.measurementScheduleHourRadLabel.Text = "Hour";
            // 
            // minuteMeasurementScheduleRadMaskedEditBox
            // 
            this.minuteMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minuteMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(138, 33);
            this.minuteMeasurementScheduleRadMaskedEditBox.Name = "minuteMeasurementScheduleRadMaskedEditBox";
            this.minuteMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.minuteMeasurementScheduleRadMaskedEditBox.TabIndex = 21;
            this.minuteMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.minuteMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // hourMeasurementScheduleRadMaskedEditBox
            // 
            this.hourMeasurementScheduleRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourMeasurementScheduleRadMaskedEditBox.Location = new System.Drawing.Point(69, 33);
            this.hourMeasurementScheduleRadMaskedEditBox.Name = "hourMeasurementScheduleRadMaskedEditBox";
            this.hourMeasurementScheduleRadMaskedEditBox.Size = new System.Drawing.Size(26, 18);
            this.hourMeasurementScheduleRadMaskedEditBox.TabIndex = 20;
            this.hourMeasurementScheduleRadMaskedEditBox.TabStop = false;
            this.hourMeasurementScheduleRadMaskedEditBox.Text = "0";
            // 
            // stepMeasurementScheduleRadRadioButton
            // 
            this.stepMeasurementScheduleRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepMeasurementScheduleRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stepMeasurementScheduleRadRadioButton.Location = new System.Drawing.Point(13, 33);
            this.stepMeasurementScheduleRadRadioButton.Name = "stepMeasurementScheduleRadRadioButton";
            this.stepMeasurementScheduleRadRadioButton.Size = new System.Drawing.Size(49, 16);
            this.stepMeasurementScheduleRadRadioButton.TabIndex = 0;
            this.stepMeasurementScheduleRadRadioButton.Text = "Every";
            // 
            // basicSettingsRadGroupBox
            // 
            this.basicSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.basicSettingsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.basicSettingsRadGroupBox.Controls.Add(this.monitoringRadGroupBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.allowablePhaseDifferenceUnitsRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.allowablePhaseDifferenceRadMaskedEditBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.daysToComputeRadGroupBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.allowablePhaseDifferenceRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.averagingUnnRadMaskedEditBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.averagingUnnRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.confirmationMeasurementUnitsRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.confirmationMeasurementRadMaskedEditBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.confirmationMeasurementRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.hysterisisUnitsradLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.hysterisisRadMaskedEditBox);
            this.basicSettingsRadGroupBox.Controls.Add(this.hysterisisRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.baudRateRadDropDownList);
            this.basicSettingsRadGroupBox.Controls.Add(this.baudRateRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.modbusAddressRadLabel);
            this.basicSettingsRadGroupBox.Controls.Add(this.modBusAddressRadMaskedEditBox);
            this.basicSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.basicSettingsRadGroupBox.HeaderText = "Basic Settings";
            this.basicSettingsRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.basicSettingsRadGroupBox.Name = "basicSettingsRadGroupBox";
            this.basicSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.basicSettingsRadGroupBox.Size = new System.Drawing.Size(491, 238);
            this.basicSettingsRadGroupBox.TabIndex = 0;
            this.basicSettingsRadGroupBox.Text = "Basic Settings";
            this.basicSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // monitoringRadGroupBox
            // 
            this.monitoringRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.monitoringRadGroupBox.Controls.Add(this.disableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Controls.Add(this.enableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitoringRadGroupBox.HeaderText = "Monitoring";
            this.monitoringRadGroupBox.Location = new System.Drawing.Point(14, 174);
            this.monitoringRadGroupBox.Name = "monitoringRadGroupBox";
            this.monitoringRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.monitoringRadGroupBox.Size = new System.Drawing.Size(143, 51);
            this.monitoringRadGroupBox.TabIndex = 5;
            this.monitoringRadGroupBox.Text = "Monitoring";
            this.monitoringRadGroupBox.ThemeName = "Office2007Black";
            // 
            // disableMonitoringRadRadioButton
            // 
            this.disableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableMonitoringRadRadioButton.Location = new System.Drawing.Point(78, 23);
            this.disableMonitoringRadRadioButton.Name = "disableMonitoringRadRadioButton";
            this.disableMonitoringRadRadioButton.Size = new System.Drawing.Size(58, 16);
            this.disableMonitoringRadRadioButton.TabIndex = 1;
            this.disableMonitoringRadRadioButton.Text = "Disable";
            // 
            // enableMonitoringRadRadioButton
            // 
          //  this.enableMonitoringRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableMonitoringRadRadioButton.Location = new System.Drawing.Point(13, 23);
            this.enableMonitoringRadRadioButton.Name = "enableMonitoringRadRadioButton";
            this.enableMonitoringRadRadioButton.Size = new System.Drawing.Size(56, 16);
            this.enableMonitoringRadRadioButton.TabIndex = 0;
            this.enableMonitoringRadRadioButton.Text = "Enable";
            this.enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // allowablePhaseDifferenceUnitsRadLabel
            // 
            this.allowablePhaseDifferenceUnitsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allowablePhaseDifferenceUnitsRadLabel.Location = new System.Drawing.Point(166, 144);
            this.allowablePhaseDifferenceUnitsRadLabel.Name = "allowablePhaseDifferenceUnitsRadLabel";
            this.allowablePhaseDifferenceUnitsRadLabel.Size = new System.Drawing.Size(47, 18);
            this.allowablePhaseDifferenceUnitsRadLabel.TabIndex = 19;
            this.allowablePhaseDifferenceUnitsRadLabel.Text = "Degrees";
            // 
            // allowablePhaseDifferenceRadMaskedEditBox
            // 
            this.allowablePhaseDifferenceRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allowablePhaseDifferenceRadMaskedEditBox.Location = new System.Drawing.Point(109, 142);
            this.allowablePhaseDifferenceRadMaskedEditBox.Name = "allowablePhaseDifferenceRadMaskedEditBox";
            this.allowablePhaseDifferenceRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.allowablePhaseDifferenceRadMaskedEditBox.TabIndex = 18;
            this.allowablePhaseDifferenceRadMaskedEditBox.TabStop = false;
            this.allowablePhaseDifferenceRadMaskedEditBox.Text = "0";
            // 
            // daysToComputeRadGroupBox
            // 
            this.daysToComputeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.daysToComputeRadGroupBox.Controls.Add(this.rateOfChangeRadMaskedEditBox);
            this.daysToComputeRadGroupBox.Controls.Add(this.tangentRadMaskedEditBox);
            this.daysToComputeRadGroupBox.Controls.Add(this.temperatureCoefficientRadMaskedEditBox);
            this.daysToComputeRadGroupBox.Controls.Add(this.tangentRadLabel);
            this.daysToComputeRadGroupBox.Controls.Add(this.temperatureCoefficientRadLabel);
            this.daysToComputeRadGroupBox.Controls.Add(this.trendRadLabel);
            this.daysToComputeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.daysToComputeRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.daysToComputeRadGroupBox.HeaderText = "Days to calculate";
            this.daysToComputeRadGroupBox.Location = new System.Drawing.Point(307, 102);
            this.daysToComputeRadGroupBox.Name = "daysToComputeRadGroupBox";
            this.daysToComputeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.daysToComputeRadGroupBox.Size = new System.Drawing.Size(169, 122);
            this.daysToComputeRadGroupBox.TabIndex = 1;
            this.daysToComputeRadGroupBox.Text = "Days to calculate";
            this.daysToComputeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // rateOfChangeRadMaskedEditBox
            // 
            this.rateOfChangeRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rateOfChangeRadMaskedEditBox.Location = new System.Drawing.Point(100, 33);
            this.rateOfChangeRadMaskedEditBox.Name = "rateOfChangeRadMaskedEditBox";
            this.rateOfChangeRadMaskedEditBox.Size = new System.Drawing.Size(52, 18);
            this.rateOfChangeRadMaskedEditBox.TabIndex = 19;
            this.rateOfChangeRadMaskedEditBox.TabStop = false;
            this.rateOfChangeRadMaskedEditBox.Text = "0";
            // 
            // tangentRadMaskedEditBox
            // 
            this.tangentRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tangentRadMaskedEditBox.Location = new System.Drawing.Point(100, 88);
            this.tangentRadMaskedEditBox.Name = "tangentRadMaskedEditBox";
            this.tangentRadMaskedEditBox.Size = new System.Drawing.Size(52, 18);
            this.tangentRadMaskedEditBox.TabIndex = 19;
            this.tangentRadMaskedEditBox.TabStop = false;
            this.tangentRadMaskedEditBox.Text = "0";
            // 
            // temperatureCoefficientRadMaskedEditBox
            // 
            this.temperatureCoefficientRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureCoefficientRadMaskedEditBox.Location = new System.Drawing.Point(100, 61);
            this.temperatureCoefficientRadMaskedEditBox.Name = "temperatureCoefficientRadMaskedEditBox";
            this.temperatureCoefficientRadMaskedEditBox.Size = new System.Drawing.Size(52, 18);
            this.temperatureCoefficientRadMaskedEditBox.TabIndex = 19;
            this.temperatureCoefficientRadMaskedEditBox.TabStop = false;
            this.temperatureCoefficientRadMaskedEditBox.Text = "0";
            // 
            // tangentRadLabel
            // 
            this.tangentRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.tangentRadLabel.Location = new System.Drawing.Point(14, 91);
            this.tangentRadLabel.Name = "tangentRadLabel";
            this.tangentRadLabel.Size = new System.Drawing.Size(47, 18);
            this.tangentRadLabel.TabIndex = 2;
            this.tangentRadLabel.Text = "Tangent";
            // 
            // temperatureCoefficientRadLabel
            // 
            this.temperatureCoefficientRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temperatureCoefficientRadLabel.Location = new System.Drawing.Point(14, 58);
            this.temperatureCoefficientRadLabel.Name = "temperatureCoefficientRadLabel";
            this.temperatureCoefficientRadLabel.Size = new System.Drawing.Size(66, 32);
            this.temperatureCoefficientRadLabel.TabIndex = 1;
            this.temperatureCoefficientRadLabel.Text = "<html>Temperature<br>Coefficient</html>";
            // 
            // trendRadLabel
            // 
            this.trendRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendRadLabel.Location = new System.Drawing.Point(14, 36);
            this.trendRadLabel.Name = "trendRadLabel";
            this.trendRadLabel.Size = new System.Drawing.Size(32, 17);
            this.trendRadLabel.TabIndex = 0;
            this.trendRadLabel.Text = "<html>Trend</html>";
            // 
            // allowablePhaseDifferenceRadLabel
            // 
            this.allowablePhaseDifferenceRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allowablePhaseDifferenceRadLabel.Location = new System.Drawing.Point(14, 137);
            this.allowablePhaseDifferenceRadLabel.Name = "allowablePhaseDifferenceRadLabel";
            this.allowablePhaseDifferenceRadLabel.Size = new System.Drawing.Size(83, 32);
            this.allowablePhaseDifferenceRadLabel.TabIndex = 17;
            this.allowablePhaseDifferenceRadLabel.Text = "<html>Allowable Phase<br>Difference</html>";
            // 
            // averagingUnnRadMaskedEditBox
            // 
            this.averagingUnnRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.averagingUnnRadMaskedEditBox.Location = new System.Drawing.Point(109, 113);
            this.averagingUnnRadMaskedEditBox.Name = "averagingUnnRadMaskedEditBox";
            this.averagingUnnRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.averagingUnnRadMaskedEditBox.TabIndex = 16;
            this.averagingUnnRadMaskedEditBox.TabStop = false;
            this.averagingUnnRadMaskedEditBox.Text = "0";
            // 
            // averagingUnnRadLabel
            // 
            this.averagingUnnRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.averagingUnnRadLabel.Location = new System.Drawing.Point(14, 114);
            this.averagingUnnRadLabel.Name = "averagingUnnRadLabel";
            this.averagingUnnRadLabel.Size = new System.Drawing.Size(81, 18);
            this.averagingUnnRadLabel.TabIndex = 15;
            this.averagingUnnRadLabel.Text = "Averaging Unn";
            // 
            // confirmationMeasurementUnitsRadLabel
            // 
            this.confirmationMeasurementUnitsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.confirmationMeasurementUnitsRadLabel.Location = new System.Drawing.Point(166, 89);
            this.confirmationMeasurementUnitsRadLabel.Name = "confirmationMeasurementUnitsRadLabel";
            this.confirmationMeasurementUnitsRadLabel.Size = new System.Drawing.Size(46, 18);
            this.confirmationMeasurementUnitsRadLabel.TabIndex = 14;
            this.confirmationMeasurementUnitsRadLabel.Text = "Minutes";
            // 
            // confirmationMeasurementRadMaskedEditBox
            // 
            this.confirmationMeasurementRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmationMeasurementRadMaskedEditBox.Location = new System.Drawing.Point(109, 87);
            this.confirmationMeasurementRadMaskedEditBox.Name = "confirmationMeasurementRadMaskedEditBox";
            this.confirmationMeasurementRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.confirmationMeasurementRadMaskedEditBox.TabIndex = 13;
            this.confirmationMeasurementRadMaskedEditBox.TabStop = false;
            this.confirmationMeasurementRadMaskedEditBox.Text = "0";
            // 
            // confirmationMeasurementRadLabel
            // 
            this.confirmationMeasurementRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.confirmationMeasurementRadLabel.Location = new System.Drawing.Point(13, 82);
            this.confirmationMeasurementRadLabel.Name = "confirmationMeasurementRadLabel";
            this.confirmationMeasurementRadLabel.Size = new System.Drawing.Size(70, 32);
            this.confirmationMeasurementRadLabel.TabIndex = 12;
            this.confirmationMeasurementRadLabel.Text = "<html>Confirmation<br>Measurement</html>";
            // 
            // hysterisisUnitsradLabel
            // 
            this.hysterisisUnitsradLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hysterisisUnitsradLabel.Location = new System.Drawing.Point(162, 64);
            this.hysterisisUnitsradLabel.Name = "hysterisisUnitsradLabel";
            this.hysterisisUnitsradLabel.Size = new System.Drawing.Size(15, 18);
            this.hysterisisUnitsradLabel.TabIndex = 11;
            this.hysterisisUnitsradLabel.Text = "%";
            // 
            // hysterisisRadMaskedEditBox
            // 
            this.hysterisisRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hysterisisRadMaskedEditBox.Location = new System.Drawing.Point(109, 62);
            this.hysterisisRadMaskedEditBox.Name = "hysterisisRadMaskedEditBox";
            this.hysterisisRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.hysterisisRadMaskedEditBox.TabIndex = 10;
            this.hysterisisRadMaskedEditBox.TabStop = false;
            this.hysterisisRadMaskedEditBox.Text = "0";
            // 
            // hysterisisRadLabel
            // 
            this.hysterisisRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hysterisisRadLabel.Location = new System.Drawing.Point(14, 61);
            this.hysterisisRadLabel.Name = "hysterisisRadLabel";
            this.hysterisisRadLabel.Size = new System.Drawing.Size(49, 17);
            this.hysterisisRadLabel.TabIndex = 9;
            this.hysterisisRadLabel.Text = "<html>Hysterisis</html>";
            // 
            // baudRateRadDropDownList
            // 
            this.baudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem2.Text = "38400";
            radListDataItem3.Text = "57600";
            radListDataItem4.Text = "115200";
            this.baudRateRadDropDownList.Items.Add(radListDataItem1);
            this.baudRateRadDropDownList.Items.Add(radListDataItem2);
            this.baudRateRadDropDownList.Items.Add(radListDataItem3);
            this.baudRateRadDropDownList.Items.Add(radListDataItem4);
            this.baudRateRadDropDownList.Location = new System.Drawing.Point(370, 22);
            this.baudRateRadDropDownList.Name = "baudRateRadDropDownList";
            this.baudRateRadDropDownList.Size = new System.Drawing.Size(106, 18);
            this.baudRateRadDropDownList.TabIndex = 3;
            this.baudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // baudRateRadLabel
            // 
            this.baudRateRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.baudRateRadLabel.Location = new System.Drawing.Point(279, 27);
            this.baudRateRadLabel.Name = "baudRateRadLabel";
            this.baudRateRadLabel.Size = new System.Drawing.Size(56, 15);
            this.baudRateRadLabel.TabIndex = 2;
            this.baudRateRadLabel.Text = "<html>Baud Rate</html>";
            // 
            // modbusAddressRadLabel
            // 
            this.modbusAddressRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressRadLabel.Location = new System.Drawing.Point(14, 27);
            this.modbusAddressRadLabel.Name = "modbusAddressRadLabel";
            this.modbusAddressRadLabel.Size = new System.Drawing.Size(88, 15);
            this.modbusAddressRadLabel.TabIndex = 1;
            this.modbusAddressRadLabel.Text = "<html>ModBus Address</html>";
            this.modbusAddressRadLabel.ThemeName = "Office2007Black";
            // 
            // modBusAddressRadMaskedEditBox
            // 
            this.modBusAddressRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modBusAddressRadMaskedEditBox.Location = new System.Drawing.Point(109, 27);
            this.modBusAddressRadMaskedEditBox.Name = "modBusAddressRadMaskedEditBox";
            this.modBusAddressRadMaskedEditBox.Size = new System.Drawing.Size(47, 18);
            this.modBusAddressRadMaskedEditBox.TabIndex = 0;
            this.modBusAddressRadMaskedEditBox.TabStop = false;
            this.modBusAddressRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetRadPageViewPage
            // 
            this.firstBushingSetRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetRadPageViewPage.Controls.Add(this.xmlConfigurationFileFirstBushingSetTabRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.templateConfigurationsFirstBushingSetTabRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetRadProgressBar);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceFirstBushingSetTabRadButton);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetTransformerNameplateRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.programDeviceFirstBushingSetTabRadButton);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetMeasurementsRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetTemperatureSensorRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetInputImpedenceRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetThresholdsRadGroupBox);
            this.firstBushingSetRadPageViewPage.Controls.Add(this.firstBushingSetSettingsRadGroupBox);
          //  this.firstBushingSetRadPageViewPage.ItemSize = new System.Drawing.SizeF(102F, 26F);
            this.firstBushingSetRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.firstBushingSetRadPageViewPage.Name = "firstBushingSetRadPageViewPage";
            this.firstBushingSetRadPageViewPage.Size = new System.Drawing.Size(851, 434);
            this.firstBushingSetRadPageViewPage.Text = "First Bushing Set";
            // 
            // xmlConfigurationFileFirstBushingSetTabRadGroupBox
            // 
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Controls.Add(this.loadFromFileFirstBushingSetTabRadButton);
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Controls.Add(this.saveToFileFirstBushingSetTabRadButton);
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Location = new System.Drawing.Point(3, 357);
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Name = "xmlConfigurationFileFirstBushingSetTabRadGroupBox";
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Size = new System.Drawing.Size(258, 75);
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.TabIndex = 56;
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileFirstBushingSetTabRadButton
            // 
            this.loadFromFileFirstBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileFirstBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileFirstBushingSetTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadFromFileFirstBushingSetTabRadButton.Name = "loadFromFileFirstBushingSetTabRadButton";
            this.loadFromFileFirstBushingSetTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.loadFromFileFirstBushingSetTabRadButton.TabIndex = 36;
            this.loadFromFileFirstBushingSetTabRadButton.Text = "Load File";
            this.loadFromFileFirstBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileFirstBushingSetTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileFirstBushingSetTabRadButton
            // 
            this.saveToFileFirstBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileFirstBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileFirstBushingSetTabRadButton.Location = new System.Drawing.Point(136, 23);
            this.saveToFileFirstBushingSetTabRadButton.Name = "saveToFileFirstBushingSetTabRadButton";
            this.saveToFileFirstBushingSetTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.saveToFileFirstBushingSetTabRadButton.TabIndex = 35;
            this.saveToFileFirstBushingSetTabRadButton.Text = "Save File";
            this.saveToFileFirstBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileFirstBushingSetTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsFirstBushingSetTabRadGroupBox
            // 
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Controls.Add(this.loadSelectedConfigurationFirstBushingSetTabRadButton);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Controls.Add(this.templateConfigurationsFirstBushingSetTabRadDropDownList);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Controls.Add(this.radButton2);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Location = new System.Drawing.Point(3, 279);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Name = "templateConfigurationsFirstBushingSetTabRadGroupBox";
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Size = new System.Drawing.Size(572, 65);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.TabIndex = 54;
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadSelectedConfigurationFirstBushingSetTabRadButton
            // 
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Location = new System.Drawing.Point(444, 11);
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Name = "loadSelectedConfigurationFirstBushingSetTabRadButton";
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.TabIndex = 34;
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadSelectedConfigurationFirstBushingSetTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsFirstBushingSetTabRadDropDownList
            // 
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.Name = "templateConfigurationsFirstBushingSetTabRadDropDownList";
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.Size = new System.Drawing.Size(424, 20);
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsFirstBushingSetTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsFirstBushingSetTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton2
            // 
            this.radButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton2.Location = new System.Drawing.Point(0, 232);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(130, 70);
            this.radButton2.TabIndex = 8;
            this.radButton2.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton2.ThemeName = "Office2007Black";
            // 
            // firstBushingSetRadProgressBar
            // 
            this.firstBushingSetRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.firstBushingSetRadProgressBar.Location = new System.Drawing.Point(552, 357);
            this.firstBushingSetRadProgressBar.Name = "firstBushingSetRadProgressBar";
            this.firstBushingSetRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.firstBushingSetRadProgressBar.TabIndex = 43;
            this.firstBushingSetRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceFirstBushingSetTabRadButton
            // 
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Location = new System.Drawing.Point(552, 393);
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Name = "loadConfigurationFromDeviceFirstBushingSetTabRadButton";
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.TabIndex = 10;
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceFirstBushingSetTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // firstBushingSetTransformerNameplateRadGroupBox
            // 
            this.firstBushingSetTransformerNameplateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetTransformerNameplateRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTemperatureAtMeasurementRadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseCRadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseBRadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseARadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseCRadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseBRadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseARadMaskedEditBox);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseCRadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseBRadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetCapacitancePhaseARadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseCRadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseBRadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.firstBushingSetTangentPhaseARadLabel);
            this.firstBushingSetTransformerNameplateRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTransformerNameplateRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTransformerNameplateRadGroupBox.HeaderText = "Transformer Nameplate Data";
            this.firstBushingSetTransformerNameplateRadGroupBox.Location = new System.Drawing.Point(505, 3);
            this.firstBushingSetTransformerNameplateRadGroupBox.Name = "firstBushingSetTransformerNameplateRadGroupBox";
            this.firstBushingSetTransformerNameplateRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetTransformerNameplateRadGroupBox.Size = new System.Drawing.Size(284, 212);
            this.firstBushingSetTransformerNameplateRadGroupBox.TabIndex = 2;
            this.firstBushingSetTransformerNameplateRadGroupBox.Text = "Transformer Nameplate Data";
            this.firstBushingSetTransformerNameplateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetTemperatureAtMeasurementRadLabel
            // 
            this.firstBushingSetTemperatureAtMeasurementRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTemperatureAtMeasurementRadLabel.Location = new System.Drawing.Point(14, 184);
            this.firstBushingSetTemperatureAtMeasurementRadLabel.Name = "firstBushingSetTemperatureAtMeasurementRadLabel";
            this.firstBushingSetTemperatureAtMeasurementRadLabel.Size = new System.Drawing.Size(156, 16);
            this.firstBushingSetTemperatureAtMeasurementRadLabel.TabIndex = 23;
            this.firstBushingSetTemperatureAtMeasurementRadLabel.Text = "Temperature at Measurement";
            // 
            // firstBushingSetTemperatureAtMeasurementRadMaskedEditBox
            // 
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Location = new System.Drawing.Point(192, 182);
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Name = "firstBushingSetTemperatureAtMeasurementRadMaskedEditBox";
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Size = new System.Drawing.Size(29, 18);
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.TabIndex = 22;
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.TabStop = false;
            this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetCapacitancePhaseCRadMaskedEditBox
            // 
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.Location = new System.Drawing.Point(155, 152);
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.Name = "firstBushingSetCapacitancePhaseCRadMaskedEditBox";
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.TabIndex = 11;
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.TabStop = false;
            this.firstBushingSetCapacitancePhaseCRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetCapacitancePhaseBRadMaskedEditBox
            // 
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.Location = new System.Drawing.Point(155, 128);
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.Name = "firstBushingSetCapacitancePhaseBRadMaskedEditBox";
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.TabIndex = 11;
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.TabStop = false;
            this.firstBushingSetCapacitancePhaseBRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetCapacitancePhaseARadMaskedEditBox
            // 
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.Location = new System.Drawing.Point(155, 104);
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.Name = "firstBushingSetCapacitancePhaseARadMaskedEditBox";
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.TabIndex = 13;
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.TabStop = false;
            this.firstBushingSetCapacitancePhaseARadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetTangentPhaseCRadMaskedEditBox
            // 
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.Location = new System.Drawing.Point(155, 78);
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.Name = "firstBushingSetTangentPhaseCRadMaskedEditBox";
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.TabIndex = 12;
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.TabStop = false;
            this.firstBushingSetTangentPhaseCRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetTangentPhaseBRadMaskedEditBox
            // 
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.Location = new System.Drawing.Point(155, 54);
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.Name = "firstBushingSetTangentPhaseBRadMaskedEditBox";
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.TabIndex = 11;
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.TabStop = false;
            this.firstBushingSetTangentPhaseBRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetTangentPhaseARadMaskedEditBox
            // 
            this.firstBushingSetTangentPhaseARadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTangentPhaseARadMaskedEditBox.Location = new System.Drawing.Point(155, 30);
            this.firstBushingSetTangentPhaseARadMaskedEditBox.Name = "firstBushingSetTangentPhaseARadMaskedEditBox";
            this.firstBushingSetTangentPhaseARadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetTangentPhaseARadMaskedEditBox.TabIndex = 10;
            this.firstBushingSetTangentPhaseARadMaskedEditBox.TabStop = false;
            this.firstBushingSetTangentPhaseARadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetCapacitancePhaseCRadLabel
            // 
            this.firstBushingSetCapacitancePhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetCapacitancePhaseCRadLabel.Location = new System.Drawing.Point(14, 153);
            this.firstBushingSetCapacitancePhaseCRadLabel.Name = "firstBushingSetCapacitancePhaseCRadLabel";
            this.firstBushingSetCapacitancePhaseCRadLabel.Size = new System.Drawing.Size(116, 16);
            this.firstBushingSetCapacitancePhaseCRadLabel.TabIndex = 9;
            this.firstBushingSetCapacitancePhaseCRadLabel.Text = "Capacitance Phase C";
            // 
            // firstBushingSetCapacitancePhaseBRadLabel
            // 
            this.firstBushingSetCapacitancePhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetCapacitancePhaseBRadLabel.Location = new System.Drawing.Point(14, 130);
            this.firstBushingSetCapacitancePhaseBRadLabel.Name = "firstBushingSetCapacitancePhaseBRadLabel";
            this.firstBushingSetCapacitancePhaseBRadLabel.Size = new System.Drawing.Size(115, 16);
            this.firstBushingSetCapacitancePhaseBRadLabel.TabIndex = 9;
            this.firstBushingSetCapacitancePhaseBRadLabel.Text = "Capacitance Phase B";
            // 
            // firstBushingSetCapacitancePhaseARadLabel
            // 
            this.firstBushingSetCapacitancePhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetCapacitancePhaseARadLabel.Location = new System.Drawing.Point(14, 107);
            this.firstBushingSetCapacitancePhaseARadLabel.Name = "firstBushingSetCapacitancePhaseARadLabel";
            this.firstBushingSetCapacitancePhaseARadLabel.Size = new System.Drawing.Size(115, 16);
            this.firstBushingSetCapacitancePhaseARadLabel.TabIndex = 8;
            this.firstBushingSetCapacitancePhaseARadLabel.Text = "Capacitance Phase A";
            // 
            // firstBushingSetTangentPhaseCRadLabel
            // 
            this.firstBushingSetTangentPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTangentPhaseCRadLabel.Location = new System.Drawing.Point(14, 78);
            this.firstBushingSetTangentPhaseCRadLabel.Name = "firstBushingSetTangentPhaseCRadLabel";
            this.firstBushingSetTangentPhaseCRadLabel.Size = new System.Drawing.Size(94, 16);
            this.firstBushingSetTangentPhaseCRadLabel.TabIndex = 7;
            this.firstBushingSetTangentPhaseCRadLabel.Text = "Tangent Phase C";
            // 
            // firstBushingSetTangentPhaseBRadLabel
            // 
            this.firstBushingSetTangentPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTangentPhaseBRadLabel.Location = new System.Drawing.Point(14, 55);
            this.firstBushingSetTangentPhaseBRadLabel.Name = "firstBushingSetTangentPhaseBRadLabel";
            this.firstBushingSetTangentPhaseBRadLabel.Size = new System.Drawing.Size(93, 16);
            this.firstBushingSetTangentPhaseBRadLabel.TabIndex = 6;
            this.firstBushingSetTangentPhaseBRadLabel.Text = "Tangent Phase B";
            // 
            // firstBushingSetTangentPhaseARadLabel
            // 
            this.firstBushingSetTangentPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTangentPhaseARadLabel.Location = new System.Drawing.Point(14, 32);
            this.firstBushingSetTangentPhaseARadLabel.Name = "firstBushingSetTangentPhaseARadLabel";
            this.firstBushingSetTangentPhaseARadLabel.Size = new System.Drawing.Size(93, 16);
            this.firstBushingSetTangentPhaseARadLabel.TabIndex = 5;
            this.firstBushingSetTangentPhaseARadLabel.Text = "Tangent Phase A";
            // 
            // programDeviceFirstBushingSetTabRadButton
            // 
            this.programDeviceFirstBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceFirstBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceFirstBushingSetTabRadButton.Location = new System.Drawing.Point(683, 393);
            this.programDeviceFirstBushingSetTabRadButton.Name = "programDeviceFirstBushingSetTabRadButton";
            this.programDeviceFirstBushingSetTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceFirstBushingSetTabRadButton.TabIndex = 5;
            this.programDeviceFirstBushingSetTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceFirstBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceFirstBushingSetTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // firstBushingSetMeasurementsRadGroupBox
            // 
            this.firstBushingSetMeasurementsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetMeasurementsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetMeasurementsRadGroupBox.Controls.Add(this.firstBushingSetDisableMeasurementsRadRadioButton);
            this.firstBushingSetMeasurementsRadGroupBox.Controls.Add(this.firstBushingSetEnableMeasurementsRadRadioButton);
            this.firstBushingSetMeasurementsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetMeasurementsRadGroupBox.HeaderText = "Measurements";
            this.firstBushingSetMeasurementsRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.firstBushingSetMeasurementsRadGroupBox.Name = "firstBushingSetMeasurementsRadGroupBox";
            this.firstBushingSetMeasurementsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetMeasurementsRadGroupBox.Size = new System.Drawing.Size(200, 73);
            this.firstBushingSetMeasurementsRadGroupBox.TabIndex = 4;
            this.firstBushingSetMeasurementsRadGroupBox.Text = "Measurements";
            this.firstBushingSetMeasurementsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetDisableMeasurementsRadRadioButton
            // 
            this.firstBushingSetDisableMeasurementsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetDisableMeasurementsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetDisableMeasurementsRadRadioButton.Location = new System.Drawing.Point(83, 33);
            this.firstBushingSetDisableMeasurementsRadRadioButton.Name = "firstBushingSetDisableMeasurementsRadRadioButton";
            this.firstBushingSetDisableMeasurementsRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.firstBushingSetDisableMeasurementsRadRadioButton.TabIndex = 1;
            this.firstBushingSetDisableMeasurementsRadRadioButton.Text = "Disable";
            // 
            // firstBushingSetEnableMeasurementsRadRadioButton
            // 
          //  this.firstBushingSetEnableMeasurementsRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.firstBushingSetEnableMeasurementsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetEnableMeasurementsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetEnableMeasurementsRadRadioButton.Location = new System.Drawing.Point(14, 33);
            this.firstBushingSetEnableMeasurementsRadRadioButton.Name = "firstBushingSetEnableMeasurementsRadRadioButton";
            this.firstBushingSetEnableMeasurementsRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.firstBushingSetEnableMeasurementsRadRadioButton.TabIndex = 0;
            this.firstBushingSetEnableMeasurementsRadRadioButton.Text = "Enable";
            this.firstBushingSetEnableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // firstBushingSetTemperatureSensorRadGroupBox
            // 
            this.firstBushingSetTemperatureSensorRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetTemperatureSensorRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetTemperatureSensorRadGroupBox.Controls.Add(this.firstBushingSetTemperatureSensorRadDropDownList);
            this.firstBushingSetTemperatureSensorRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureSensorRadGroupBox.HeaderText = "Temperature Sensor";
            this.firstBushingSetTemperatureSensorRadGroupBox.Location = new System.Drawing.Point(505, 221);
            this.firstBushingSetTemperatureSensorRadGroupBox.Name = "firstBushingSetTemperatureSensorRadGroupBox";
            this.firstBushingSetTemperatureSensorRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetTemperatureSensorRadGroupBox.Size = new System.Drawing.Size(335, 52);
            this.firstBushingSetTemperatureSensorRadGroupBox.TabIndex = 3;
            this.firstBushingSetTemperatureSensorRadGroupBox.Text = "Temperature Sensor";
            this.firstBushingSetTemperatureSensorRadGroupBox.ThemeName = "Office2007Black";
            this.firstBushingSetTemperatureSensorRadGroupBox.Visible = false;
            // 
            // firstBushingSetTemperatureSensorRadDropDownList
            // 
            this.firstBushingSetTemperatureSensorRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem5.Text = "Not Connected / Data from SCADA";
            radListDataItem6.Text = "Connected to Channel 1";
            radListDataItem7.Text = "Connected to Channel 2";
            radListDataItem8.Text = "Connected to Channel 3";
            this.firstBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem5);
            this.firstBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem6);
            this.firstBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem7);
            this.firstBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem8);
            this.firstBushingSetTemperatureSensorRadDropDownList.Location = new System.Drawing.Point(13, 24);
            this.firstBushingSetTemperatureSensorRadDropDownList.Name = "firstBushingSetTemperatureSensorRadDropDownList";
            this.firstBushingSetTemperatureSensorRadDropDownList.Size = new System.Drawing.Size(309, 20);
            this.firstBushingSetTemperatureSensorRadDropDownList.TabIndex = 5;
            this.firstBushingSetTemperatureSensorRadDropDownList.ThemeName = "Office2007Black";
            // 
            // firstBushingSetInputImpedenceRadGroupBox
            // 
            this.firstBushingSetInputImpedenceRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetInputImpedenceRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseCRadSpinEditor);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseBRadSpinEditor);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseARadSpinEditor);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseCOhmsRadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseBOhmsRadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseAOhmsRadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseCRadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseBRadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Controls.Add(this.firstBushingSetPhaseARadLabel);
            this.firstBushingSetInputImpedenceRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetInputImpedenceRadGroupBox.HeaderText = "Input Impedence";
            this.firstBushingSetInputImpedenceRadGroupBox.Location = new System.Drawing.Point(3, 164);
            this.firstBushingSetInputImpedenceRadGroupBox.Name = "firstBushingSetInputImpedenceRadGroupBox";
            this.firstBushingSetInputImpedenceRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetInputImpedenceRadGroupBox.Size = new System.Drawing.Size(200, 109);
            this.firstBushingSetInputImpedenceRadGroupBox.TabIndex = 1;
            this.firstBushingSetInputImpedenceRadGroupBox.Text = "Input Impedence";
            this.firstBushingSetInputImpedenceRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetPhaseCRadSpinEditor
            // 
            this.firstBushingSetPhaseCRadSpinEditor.DecimalPlaces = 2;
            this.firstBushingSetPhaseCRadSpinEditor.Location = new System.Drawing.Point(76, 79);
            this.firstBushingSetPhaseCRadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.firstBushingSetPhaseCRadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.firstBushingSetPhaseCRadSpinEditor.Name = "firstBushingSetPhaseCRadSpinEditor";
            // 
            // 
            // 
            this.firstBushingSetPhaseCRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.firstBushingSetPhaseCRadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.firstBushingSetPhaseCRadSpinEditor.TabIndex = 46;
            this.firstBushingSetPhaseCRadSpinEditor.TabStop = false;
            this.firstBushingSetPhaseCRadSpinEditor.ThemeName = "Office2007Black";
            this.firstBushingSetPhaseCRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // firstBushingSetPhaseBRadSpinEditor
            // 
            this.firstBushingSetPhaseBRadSpinEditor.DecimalPlaces = 2;
            this.firstBushingSetPhaseBRadSpinEditor.Location = new System.Drawing.Point(76, 55);
            this.firstBushingSetPhaseBRadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.firstBushingSetPhaseBRadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.firstBushingSetPhaseBRadSpinEditor.Name = "firstBushingSetPhaseBRadSpinEditor";
            // 
            // 
            // 
            this.firstBushingSetPhaseBRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.firstBushingSetPhaseBRadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.firstBushingSetPhaseBRadSpinEditor.TabIndex = 45;
            this.firstBushingSetPhaseBRadSpinEditor.TabStop = false;
            this.firstBushingSetPhaseBRadSpinEditor.ThemeName = "Office2007Black";
            this.firstBushingSetPhaseBRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // firstBushingSetPhaseARadSpinEditor
            // 
            this.firstBushingSetPhaseARadSpinEditor.DecimalPlaces = 2;
            this.firstBushingSetPhaseARadSpinEditor.Location = new System.Drawing.Point(76, 31);
            this.firstBushingSetPhaseARadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.firstBushingSetPhaseARadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.firstBushingSetPhaseARadSpinEditor.Name = "firstBushingSetPhaseARadSpinEditor";
            // 
            // 
            // 
            this.firstBushingSetPhaseARadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.firstBushingSetPhaseARadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.firstBushingSetPhaseARadSpinEditor.TabIndex = 44;
            this.firstBushingSetPhaseARadSpinEditor.TabStop = false;
            this.firstBushingSetPhaseARadSpinEditor.ThemeName = "Office2007Black";
            this.firstBushingSetPhaseARadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // firstBushingSetPhaseCOhmsRadLabel
            // 
            this.firstBushingSetPhaseCOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseCOhmsRadLabel.Location = new System.Drawing.Point(148, 83);
            this.firstBushingSetPhaseCOhmsRadLabel.Name = "firstBushingSetPhaseCOhmsRadLabel";
            this.firstBushingSetPhaseCOhmsRadLabel.Size = new System.Drawing.Size(36, 16);
            this.firstBushingSetPhaseCOhmsRadLabel.TabIndex = 8;
            this.firstBushingSetPhaseCOhmsRadLabel.Text = "Ohms";
            // 
            // firstBushingSetPhaseBOhmsRadLabel
            // 
            this.firstBushingSetPhaseBOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseBOhmsRadLabel.Location = new System.Drawing.Point(148, 59);
            this.firstBushingSetPhaseBOhmsRadLabel.Name = "firstBushingSetPhaseBOhmsRadLabel";
            this.firstBushingSetPhaseBOhmsRadLabel.Size = new System.Drawing.Size(36, 16);
            this.firstBushingSetPhaseBOhmsRadLabel.TabIndex = 8;
            this.firstBushingSetPhaseBOhmsRadLabel.Text = "Ohms";
            // 
            // firstBushingSetPhaseAOhmsRadLabel
            // 
            this.firstBushingSetPhaseAOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseAOhmsRadLabel.Location = new System.Drawing.Point(148, 35);
            this.firstBushingSetPhaseAOhmsRadLabel.Name = "firstBushingSetPhaseAOhmsRadLabel";
            this.firstBushingSetPhaseAOhmsRadLabel.Size = new System.Drawing.Size(36, 16);
            this.firstBushingSetPhaseAOhmsRadLabel.TabIndex = 7;
            this.firstBushingSetPhaseAOhmsRadLabel.Text = "Ohms";
            // 
            // firstBushingSetPhaseCRadLabel
            // 
            this.firstBushingSetPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseCRadLabel.Location = new System.Drawing.Point(14, 83);
            this.firstBushingSetPhaseCRadLabel.Name = "firstBushingSetPhaseCRadLabel";
            this.firstBushingSetPhaseCRadLabel.Size = new System.Drawing.Size(49, 16);
            this.firstBushingSetPhaseCRadLabel.TabIndex = 6;
            this.firstBushingSetPhaseCRadLabel.Text = "Phase C";
            // 
            // firstBushingSetPhaseBRadLabel
            // 
            this.firstBushingSetPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseBRadLabel.Location = new System.Drawing.Point(14, 59);
            this.firstBushingSetPhaseBRadLabel.Name = "firstBushingSetPhaseBRadLabel";
            this.firstBushingSetPhaseBRadLabel.Size = new System.Drawing.Size(49, 16);
            this.firstBushingSetPhaseBRadLabel.TabIndex = 5;
            this.firstBushingSetPhaseBRadLabel.Text = "Phase B";
            // 
            // firstBushingSetPhaseARadLabel
            // 
            this.firstBushingSetPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetPhaseARadLabel.Location = new System.Drawing.Point(14, 35);
            this.firstBushingSetPhaseARadLabel.Name = "firstBushingSetPhaseARadLabel";
            this.firstBushingSetPhaseARadLabel.Size = new System.Drawing.Size(49, 16);
            this.firstBushingSetPhaseARadLabel.TabIndex = 4;
            this.firstBushingSetPhaseARadLabel.Text = "Phase A";
            // 
            // firstBushingSetThresholdsRadGroupBox
            // 
            this.firstBushingSetThresholdsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetThresholdsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetThresholdsRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeRadGroupBox);
            this.firstBushingSetThresholdsRadGroupBox.Controls.Add(this.firstBushingSetTemperatureCoeffRadGroupBox);
            this.firstBushingSetThresholdsRadGroupBox.Controls.Add(this.firstBushingSetAmplitudeRadGroupBox);
            this.firstBushingSetThresholdsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetThresholdsRadGroupBox.HeaderText = "Thresholds";
            this.firstBushingSetThresholdsRadGroupBox.Location = new System.Drawing.Point(209, 3);
            this.firstBushingSetThresholdsRadGroupBox.Name = "firstBushingSetThresholdsRadGroupBox";
            this.firstBushingSetThresholdsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetThresholdsRadGroupBox.Size = new System.Drawing.Size(285, 270);
            this.firstBushingSetThresholdsRadGroupBox.TabIndex = 1;
            this.firstBushingSetThresholdsRadGroupBox.Text = "Thresholds";
            this.firstBushingSetThresholdsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetRateOfChangeRadGroupBox
            // 
            this.firstBushingSetRateOfChangeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetRateOfChangeRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox);
            this.firstBushingSetRateOfChangeRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeAlarmThresholdRadLabel);
            this.firstBushingSetRateOfChangeRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeAlarmRadLabel);
            this.firstBushingSetRateOfChangeRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeAlarmOffRadRadioButton);
            this.firstBushingSetRateOfChangeRadGroupBox.Controls.Add(this.firstBushingSetRateOfChangeAlarmOnRadRadioButton);
            this.firstBushingSetRateOfChangeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetRateOfChangeRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.firstBushingSetRateOfChangeRadGroupBox.HeaderText = "Rate of Gamma Change (%/Year)";
            this.firstBushingSetRateOfChangeRadGroupBox.Location = new System.Drawing.Point(13, 185);
            this.firstBushingSetRateOfChangeRadGroupBox.Name = "firstBushingSetRateOfChangeRadGroupBox";
            this.firstBushingSetRateOfChangeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetRateOfChangeRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.firstBushingSetRateOfChangeRadGroupBox.TabIndex = 3;
            this.firstBushingSetRateOfChangeRadGroupBox.Text = "Rate of Gamma Change (%/Year)";
            this.firstBushingSetRateOfChangeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox
            // 
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(171, 36);
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Name = "firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox";
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.TabIndex = 9;
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetRateOfChangeAlarmThresholdRadLabel
            // 
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.Location = new System.Drawing.Point(114, 25);
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.Name = "firstBushingSetRateOfChangeAlarmThresholdRadLabel";
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.Size = new System.Drawing.Size(52, 40);
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.TabIndex = 8;
            this.firstBushingSetRateOfChangeAlarmThresholdRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // firstBushingSetRateOfChangeAlarmRadLabel
            // 
            this.firstBushingSetRateOfChangeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetRateOfChangeAlarmRadLabel.Location = new System.Drawing.Point(14, 37);
            this.firstBushingSetRateOfChangeAlarmRadLabel.Name = "firstBushingSetRateOfChangeAlarmRadLabel";
            this.firstBushingSetRateOfChangeAlarmRadLabel.Size = new System.Drawing.Size(36, 16);
            this.firstBushingSetRateOfChangeAlarmRadLabel.TabIndex = 7;
            this.firstBushingSetRateOfChangeAlarmRadLabel.Text = "Alarm";
            // 
            // firstBushingSetRateOfChangeAlarmOffRadRadioButton
            // 
          //  this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.Location = new System.Drawing.Point(56, 47);
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.Name = "firstBushingSetRateOfChangeAlarmOffRadRadioButton";
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.Size = new System.Drawing.Size(38, 18);
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.TabIndex = 6;
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.Text = "Off";
            this.firstBushingSetRateOfChangeAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // firstBushingSetRateOfChangeAlarmOnRadRadioButton
            // 
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.Location = new System.Drawing.Point(56, 25);
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.Name = "firstBushingSetRateOfChangeAlarmOnRadRadioButton";
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.Size = new System.Drawing.Size(38, 18);
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.TabIndex = 5;
            this.firstBushingSetRateOfChangeAlarmOnRadRadioButton.Text = "On";
            // 
            // firstBushingSetTemperatureCoeffRadGroupBox
            // 
            this.firstBushingSetTemperatureCoeffRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.firstBushingSetTemperatrreCoeffAlarmRadLabel);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureCoeffRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.firstBushingSetTemperatureCoeffRadGroupBox.HeaderText = "Temperature Coefficient (%/C)";
            this.firstBushingSetTemperatureCoeffRadGroupBox.Location = new System.Drawing.Point(13, 104);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Name = "firstBushingSetTemperatureCoeffRadGroupBox";
            this.firstBushingSetTemperatureCoeffRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetTemperatureCoeffRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.firstBushingSetTemperatureCoeffRadGroupBox.TabIndex = 2;
            this.firstBushingSetTemperatureCoeffRadGroupBox.Text = "Temperature Coefficient (%/C)";
            this.firstBushingSetTemperatureCoeffRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox
            // 
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(171, 36);
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Name = "firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox";
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.TabIndex = 4;
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetTemperatureCoeffAlarmThresholdRadLabel
            // 
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.Location = new System.Drawing.Point(114, 25);
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.Name = "firstBushingSetTemperatureCoeffAlarmThresholdRadLabel";
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.Size = new System.Drawing.Size(52, 40);
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.TabIndex = 3;
            this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // firstBushingSetTemperatrreCoeffAlarmRadLabel
            // 
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.Location = new System.Drawing.Point(14, 37);
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.Name = "firstBushingSetTemperatrreCoeffAlarmRadLabel";
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.Size = new System.Drawing.Size(36, 16);
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.TabIndex = 2;
            this.firstBushingSetTemperatrreCoeffAlarmRadLabel.Text = "Alarm";
            // 
            // firstBushingSetTemperatureCoeffAlarmOffRadRadioButton
            // 
           // this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Location = new System.Drawing.Point(56, 47);
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Name = "firstBushingSetTemperatureCoeffAlarmOffRadRadioButton";
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Size = new System.Drawing.Size(38, 18);
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.TabIndex = 1;
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Text = "Off";
            this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // firstBushingSetTemperatureCoeffAlarmOnRadRadioButton
            // 
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Location = new System.Drawing.Point(56, 25);
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Name = "firstBushingSetTemperatureCoeffAlarmOnRadRadioButton";
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Size = new System.Drawing.Size(38, 18);
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.TabIndex = 0;
            this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Text = "On";
            // 
            // firstBushingSetAmplitudeRadGroupBox
            // 
            this.firstBushingSetAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetAmplitudeRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetAmplitudeRadGroupBox.Controls.Add(this.firstBushingSetAmplitudeAlarmRadLabel);
            this.firstBushingSetAmplitudeRadGroupBox.Controls.Add(this.firstBushingSetAmplitudeWarningRadLabel);
            this.firstBushingSetAmplitudeRadGroupBox.Controls.Add(this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox);
            this.firstBushingSetAmplitudeRadGroupBox.Controls.Add(this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox);
            this.firstBushingSetAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetAmplitudeRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.firstBushingSetAmplitudeRadGroupBox.HeaderText = "Amplitude (Gamma %)";
            this.firstBushingSetAmplitudeRadGroupBox.Location = new System.Drawing.Point(13, 23);
            this.firstBushingSetAmplitudeRadGroupBox.Name = "firstBushingSetAmplitudeRadGroupBox";
            this.firstBushingSetAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetAmplitudeRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.firstBushingSetAmplitudeRadGroupBox.TabIndex = 0;
            this.firstBushingSetAmplitudeRadGroupBox.Text = "Amplitude (Gamma %)";
            this.firstBushingSetAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetAmplitudeAlarmRadLabel
            // 
            this.firstBushingSetAmplitudeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetAmplitudeAlarmRadLabel.Location = new System.Drawing.Point(137, 25);
            this.firstBushingSetAmplitudeAlarmRadLabel.Name = "firstBushingSetAmplitudeAlarmRadLabel";
            this.firstBushingSetAmplitudeAlarmRadLabel.Size = new System.Drawing.Size(52, 40);
            this.firstBushingSetAmplitudeAlarmRadLabel.TabIndex = 5;
            this.firstBushingSetAmplitudeAlarmRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // firstBushingSetAmplitudeWarningRadLabel
            // 
            this.firstBushingSetAmplitudeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetAmplitudeWarningRadLabel.Location = new System.Drawing.Point(13, 25);
            this.firstBushingSetAmplitudeWarningRadLabel.Name = "firstBushingSetAmplitudeWarningRadLabel";
            this.firstBushingSetAmplitudeWarningRadLabel.Size = new System.Drawing.Size(52, 40);
            this.firstBushingSetAmplitudeWarningRadLabel.TabIndex = 4;
            this.firstBushingSetAmplitudeWarningRadLabel.Text = "<html>Warning<br>Threshold<br>Value</html>";
            // 
            // firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox
            // 
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(194, 35);
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Name = "firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox";
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.TabIndex = 2;
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox
            // 
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Location = new System.Drawing.Point(70, 35);
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Name = "firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox";
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Size = new System.Drawing.Size(52, 18);
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.TabIndex = 1;
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.TabStop = false;
            this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text = "0";
            // 
            // firstBushingSetSettingsRadGroupBox
            // 
            this.firstBushingSetSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firstBushingSetSettingsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetkVRadLabel);
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetAmpsRadLabel);
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetCurrentRadLabel);
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetVoltageRadLabel);
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetCurrentRadMaskedEditBox);
            this.firstBushingSetSettingsRadGroupBox.Controls.Add(this.firstBushingSetVoltageRadMaskedEditBox);
            this.firstBushingSetSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetSettingsRadGroupBox.HeaderText = "Settings";
            this.firstBushingSetSettingsRadGroupBox.Location = new System.Drawing.Point(3, 82);
            this.firstBushingSetSettingsRadGroupBox.Name = "firstBushingSetSettingsRadGroupBox";
            this.firstBushingSetSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firstBushingSetSettingsRadGroupBox.Size = new System.Drawing.Size(200, 76);
            this.firstBushingSetSettingsRadGroupBox.TabIndex = 0;
            this.firstBushingSetSettingsRadGroupBox.Text = "Settings";
            this.firstBushingSetSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firstBushingSetkVRadLabel
            // 
            this.firstBushingSetkVRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetkVRadLabel.Location = new System.Drawing.Point(148, 24);
            this.firstBushingSetkVRadLabel.Name = "firstBushingSetkVRadLabel";
            this.firstBushingSetkVRadLabel.Size = new System.Drawing.Size(19, 16);
            this.firstBushingSetkVRadLabel.TabIndex = 5;
            this.firstBushingSetkVRadLabel.Text = "kV";
            // 
            // firstBushingSetAmpsRadLabel
            // 
            this.firstBushingSetAmpsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetAmpsRadLabel.Location = new System.Drawing.Point(148, 46);
            this.firstBushingSetAmpsRadLabel.Name = "firstBushingSetAmpsRadLabel";
            this.firstBushingSetAmpsRadLabel.Size = new System.Drawing.Size(35, 16);
            this.firstBushingSetAmpsRadLabel.TabIndex = 4;
            this.firstBushingSetAmpsRadLabel.Text = "Amps";
            this.firstBushingSetAmpsRadLabel.Visible = false;
            // 
            // firstBushingSetCurrentRadLabel
            // 
            this.firstBushingSetCurrentRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetCurrentRadLabel.Location = new System.Drawing.Point(14, 48);
            this.firstBushingSetCurrentRadLabel.Name = "firstBushingSetCurrentRadLabel";
            this.firstBushingSetCurrentRadLabel.Size = new System.Drawing.Size(44, 16);
            this.firstBushingSetCurrentRadLabel.TabIndex = 3;
            this.firstBushingSetCurrentRadLabel.Text = "Current";
            this.firstBushingSetCurrentRadLabel.Visible = false;
            // 
            // firstBushingSetVoltageRadLabel
            // 
            this.firstBushingSetVoltageRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firstBushingSetVoltageRadLabel.Location = new System.Drawing.Point(14, 24);
            this.firstBushingSetVoltageRadLabel.Name = "firstBushingSetVoltageRadLabel";
            this.firstBushingSetVoltageRadLabel.Size = new System.Drawing.Size(45, 16);
            this.firstBushingSetVoltageRadLabel.TabIndex = 2;
            this.firstBushingSetVoltageRadLabel.Text = "Voltage";
            // 
            // firstBushingSetCurrentRadMaskedEditBox
            // 
            this.firstBushingSetCurrentRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetCurrentRadMaskedEditBox.Location = new System.Drawing.Point(76, 46);
            this.firstBushingSetCurrentRadMaskedEditBox.Name = "firstBushingSetCurrentRadMaskedEditBox";
            this.firstBushingSetCurrentRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetCurrentRadMaskedEditBox.TabIndex = 1;
            this.firstBushingSetCurrentRadMaskedEditBox.TabStop = false;
            this.firstBushingSetCurrentRadMaskedEditBox.Text = "0";
            this.firstBushingSetCurrentRadMaskedEditBox.Visible = false;
            // 
            // firstBushingSetVoltageRadMaskedEditBox
            // 
            this.firstBushingSetVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstBushingSetVoltageRadMaskedEditBox.Location = new System.Drawing.Point(76, 22);
            this.firstBushingSetVoltageRadMaskedEditBox.Name = "firstBushingSetVoltageRadMaskedEditBox";
            this.firstBushingSetVoltageRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.firstBushingSetVoltageRadMaskedEditBox.TabIndex = 0;
            this.firstBushingSetVoltageRadMaskedEditBox.TabStop = false;
            this.firstBushingSetVoltageRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetRadPageViewPage
            // 
            this.secondBushingSetRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetRadPageViewPage.Controls.Add(this.xmlConfigurationFileSecondBushingSetTabRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.templateConfigurationsSecondBushingSetTabRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetRadProgressBar);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceSecondBushingSetTabRadButton);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.programDeviceSecondBushingSetTabRadButton);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetMeasurementsRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetTemperatureSensorRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetTransformerNameplateRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetInputImpedenceRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetThresholdsRadGroupBox);
            this.secondBushingSetRadPageViewPage.Controls.Add(this.secondBushingSetSettingsRadGroupBox);
           // this.secondBushingSetRadPageViewPage.ItemSize = new System.Drawing.SizeF(119F, 26F);
            this.secondBushingSetRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.secondBushingSetRadPageViewPage.Name = "secondBushingSetRadPageViewPage";
            this.secondBushingSetRadPageViewPage.Size = new System.Drawing.Size(851, 434);
            this.secondBushingSetRadPageViewPage.Text = "Second Bushing Set";
            // 
            // xmlConfigurationFileSecondBushingSetTabRadGroupBox
            // 
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Controls.Add(this.loadFromFileSecondBushingSetTabRadButton);
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Controls.Add(this.saveToFileSecondBushingSetTabRadButton);
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Location = new System.Drawing.Point(3, 357);
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Name = "xmlConfigurationFileSecondBushingSetTabRadGroupBox";
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Size = new System.Drawing.Size(258, 75);
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.TabIndex = 56;
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileSecondBushingSetTabRadButton
            // 
            this.loadFromFileSecondBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileSecondBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileSecondBushingSetTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadFromFileSecondBushingSetTabRadButton.Name = "loadFromFileSecondBushingSetTabRadButton";
            this.loadFromFileSecondBushingSetTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.loadFromFileSecondBushingSetTabRadButton.TabIndex = 36;
            this.loadFromFileSecondBushingSetTabRadButton.Text = "Load File";
            this.loadFromFileSecondBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileSecondBushingSetTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileSecondBushingSetTabRadButton
            // 
            this.saveToFileSecondBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileSecondBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileSecondBushingSetTabRadButton.Location = new System.Drawing.Point(136, 23);
            this.saveToFileSecondBushingSetTabRadButton.Name = "saveToFileSecondBushingSetTabRadButton";
            this.saveToFileSecondBushingSetTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.saveToFileSecondBushingSetTabRadButton.TabIndex = 35;
            this.saveToFileSecondBushingSetTabRadButton.Text = "Save File";
            this.saveToFileSecondBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileSecondBushingSetTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsSecondBushingSetTabRadGroupBox
            // 
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Controls.Add(this.loadSelectedConfigurationSecondBushingSetTabRadButton);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Controls.Add(this.templateConfigurationsSecondBushingSetTabRadDropDownList);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Controls.Add(this.radButton6);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Location = new System.Drawing.Point(3, 279);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Name = "templateConfigurationsSecondBushingSetTabRadGroupBox";
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Size = new System.Drawing.Size(572, 65);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.TabIndex = 55;
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadSelectedConfigurationSecondBushingSetTabRadButton
            // 
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Location = new System.Drawing.Point(444, 11);
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Name = "loadSelectedConfigurationSecondBushingSetTabRadButton";
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.TabIndex = 34;
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadSelectedConfigurationSecondBushingSetTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsSecondBushingSetTabRadDropDownList
            // 
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.Name = "templateConfigurationsSecondBushingSetTabRadDropDownList";
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.Size = new System.Drawing.Size(424, 18);
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsSecondBushingSetTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsSecondBushingSetTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton6
            // 
            this.radButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton6.Location = new System.Drawing.Point(0, 232);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(130, 70);
            this.radButton6.TabIndex = 8;
            this.radButton6.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton6.ThemeName = "Office2007Black";
            // 
            // secondBushingSetRadProgressBar
            // 
            this.secondBushingSetRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.secondBushingSetRadProgressBar.Location = new System.Drawing.Point(552, 357);
            this.secondBushingSetRadProgressBar.Name = "secondBushingSetRadProgressBar";
            this.secondBushingSetRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.secondBushingSetRadProgressBar.TabIndex = 43;
            this.secondBushingSetRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceSecondBushingSetTabRadButton
            // 
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Location = new System.Drawing.Point(552, 393);
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Name = "loadConfigurationFromDeviceSecondBushingSetTabRadButton";
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.TabIndex = 19;
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceSecondBushingSetTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceSecondBushingSetTabRadButton
            // 
            this.programDeviceSecondBushingSetTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceSecondBushingSetTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceSecondBushingSetTabRadButton.Location = new System.Drawing.Point(683, 393);
            this.programDeviceSecondBushingSetTabRadButton.Name = "programDeviceSecondBushingSetTabRadButton";
            this.programDeviceSecondBushingSetTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceSecondBushingSetTabRadButton.TabIndex = 15;
            this.programDeviceSecondBushingSetTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceSecondBushingSetTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceSecondBushingSetTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // secondBushingSetMeasurementsRadGroupBox
            // 
            this.secondBushingSetMeasurementsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetMeasurementsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetMeasurementsRadGroupBox.Controls.Add(this.secondBushingSetDisableMeasurementsRadRadioButton);
            this.secondBushingSetMeasurementsRadGroupBox.Controls.Add(this.secondBushingSetEnableMeasurementsRadRadioButton);
            this.secondBushingSetMeasurementsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetMeasurementsRadGroupBox.HeaderText = "Measurements";
            this.secondBushingSetMeasurementsRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.secondBushingSetMeasurementsRadGroupBox.Name = "secondBushingSetMeasurementsRadGroupBox";
            this.secondBushingSetMeasurementsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetMeasurementsRadGroupBox.Size = new System.Drawing.Size(200, 73);
            this.secondBushingSetMeasurementsRadGroupBox.TabIndex = 14;
            this.secondBushingSetMeasurementsRadGroupBox.Text = "Measurements";
            this.secondBushingSetMeasurementsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetDisableMeasurementsRadRadioButton
            // 
            this.secondBushingSetDisableMeasurementsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetDisableMeasurementsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetDisableMeasurementsRadRadioButton.Location = new System.Drawing.Point(83, 33);
            this.secondBushingSetDisableMeasurementsRadRadioButton.Name = "secondBushingSetDisableMeasurementsRadRadioButton";
            this.secondBushingSetDisableMeasurementsRadRadioButton.Size = new System.Drawing.Size(58, 16);
            this.secondBushingSetDisableMeasurementsRadRadioButton.TabIndex = 1;
            this.secondBushingSetDisableMeasurementsRadRadioButton.Text = "Disable";
            // 
            // secondBushingSetEnableMeasurementsRadRadioButton
            // 
           // this.secondBushingSetEnableMeasurementsRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.secondBushingSetEnableMeasurementsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetEnableMeasurementsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetEnableMeasurementsRadRadioButton.Location = new System.Drawing.Point(14, 33);
            this.secondBushingSetEnableMeasurementsRadRadioButton.Name = "secondBushingSetEnableMeasurementsRadRadioButton";
            this.secondBushingSetEnableMeasurementsRadRadioButton.Size = new System.Drawing.Size(56, 16);
            this.secondBushingSetEnableMeasurementsRadRadioButton.TabIndex = 0;
            this.secondBushingSetEnableMeasurementsRadRadioButton.Text = "Enable";
            this.secondBushingSetEnableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // secondBushingSetTemperatureSensorRadGroupBox
            // 
            this.secondBushingSetTemperatureSensorRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetTemperatureSensorRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetTemperatureSensorRadGroupBox.Controls.Add(this.secondBushingSetTemperatureSensorRadDropDownList);
            this.secondBushingSetTemperatureSensorRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureSensorRadGroupBox.HeaderText = "Temperature Sensor";
            this.secondBushingSetTemperatureSensorRadGroupBox.Location = new System.Drawing.Point(505, 221);
            this.secondBushingSetTemperatureSensorRadGroupBox.Name = "secondBushingSetTemperatureSensorRadGroupBox";
            this.secondBushingSetTemperatureSensorRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetTemperatureSensorRadGroupBox.Size = new System.Drawing.Size(335, 52);
            this.secondBushingSetTemperatureSensorRadGroupBox.TabIndex = 13;
            this.secondBushingSetTemperatureSensorRadGroupBox.Text = "Temperature Sensor";
            this.secondBushingSetTemperatureSensorRadGroupBox.ThemeName = "Office2007Black";
            this.secondBushingSetTemperatureSensorRadGroupBox.Visible = false;
            // 
            // secondBushingSetTemperatureSensorRadDropDownList
            // 
            this.secondBushingSetTemperatureSensorRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem9.Text = "Not Connected / Data from SCADA";
            radListDataItem10.Text = "Connected to Channel 1";
            radListDataItem11.Text = "Connected to Channel 2";
            radListDataItem12.Text = "Connected to Channel 3";
            this.secondBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem9);
            this.secondBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem10);
            this.secondBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem11);
            this.secondBushingSetTemperatureSensorRadDropDownList.Items.Add(radListDataItem12);
            this.secondBushingSetTemperatureSensorRadDropDownList.Location = new System.Drawing.Point(13, 24);
            this.secondBushingSetTemperatureSensorRadDropDownList.Name = "secondBushingSetTemperatureSensorRadDropDownList";
            this.secondBushingSetTemperatureSensorRadDropDownList.Size = new System.Drawing.Size(309, 18);
            this.secondBushingSetTemperatureSensorRadDropDownList.TabIndex = 5;
            this.secondBushingSetTemperatureSensorRadDropDownList.ThemeName = "Office2007Black";
            // 
            // secondBushingSetTransformerNameplateRadGroupBox
            // 
            this.secondBushingSetTransformerNameplateRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetTransformerNameplateRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTemperatureAtMeasurementRadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseCRadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseBRadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseARadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseCRadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseBRadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseARadMaskedEditBox);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseCRadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseBRadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetCapacitancePhaseARadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseCRadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseBRadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Controls.Add(this.secondBushingSetTangentPhaseARadLabel);
            this.secondBushingSetTransformerNameplateRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTransformerNameplateRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTransformerNameplateRadGroupBox.HeaderText = "Transformer Nameplate Data";
            this.secondBushingSetTransformerNameplateRadGroupBox.Location = new System.Drawing.Point(505, 3);
            this.secondBushingSetTransformerNameplateRadGroupBox.Name = "secondBushingSetTransformerNameplateRadGroupBox";
            this.secondBushingSetTransformerNameplateRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetTransformerNameplateRadGroupBox.Size = new System.Drawing.Size(284, 212);
            this.secondBushingSetTransformerNameplateRadGroupBox.TabIndex = 12;
            this.secondBushingSetTransformerNameplateRadGroupBox.Text = "Transformer Nameplate Data";
            this.secondBushingSetTransformerNameplateRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetTemperatureAtMeasurementRadLabel
            // 
            this.secondBushingSetTemperatureAtMeasurementRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTemperatureAtMeasurementRadLabel.Location = new System.Drawing.Point(14, 184);
            this.secondBushingSetTemperatureAtMeasurementRadLabel.Name = "secondBushingSetTemperatureAtMeasurementRadLabel";
            this.secondBushingSetTemperatureAtMeasurementRadLabel.Size = new System.Drawing.Size(155, 18);
            this.secondBushingSetTemperatureAtMeasurementRadLabel.TabIndex = 25;
            this.secondBushingSetTemperatureAtMeasurementRadLabel.Text = "Temperature at Measurement";
            // 
            // secondBushingSetTemperatureAtMeasurementRadMaskedEditBox
            // 
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Location = new System.Drawing.Point(192, 182);
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Name = "secondBushingSetTemperatureAtMeasurementRadMaskedEditBox";
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Size = new System.Drawing.Size(29, 18);
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.TabIndex = 24;
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.TabStop = false;
            this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetCapacitancePhaseCRadMaskedEditBox
            // 
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.Location = new System.Drawing.Point(155, 152);
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.Name = "secondBushingSetCapacitancePhaseCRadMaskedEditBox";
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.TabIndex = 11;
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.TabStop = false;
            this.secondBushingSetCapacitancePhaseCRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetCapacitancePhaseBRadMaskedEditBox
            // 
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.Location = new System.Drawing.Point(155, 128);
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.Name = "secondBushingSetCapacitancePhaseBRadMaskedEditBox";
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.TabIndex = 11;
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.TabStop = false;
            this.secondBushingSetCapacitancePhaseBRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetCapacitancePhaseARadMaskedEditBox
            // 
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.Location = new System.Drawing.Point(155, 104);
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.Name = "secondBushingSetCapacitancePhaseARadMaskedEditBox";
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.TabIndex = 13;
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.TabStop = false;
            this.secondBushingSetCapacitancePhaseARadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetTangentPhaseCRadMaskedEditBox
            // 
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.Location = new System.Drawing.Point(155, 78);
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.Name = "secondBushingSetTangentPhaseCRadMaskedEditBox";
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.TabIndex = 12;
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.TabStop = false;
            this.secondBushingSetTangentPhaseCRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetTangentPhaseBRadMaskedEditBox
            // 
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.Location = new System.Drawing.Point(155, 54);
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.Name = "secondBushingSetTangentPhaseBRadMaskedEditBox";
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.TabIndex = 11;
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.TabStop = false;
            this.secondBushingSetTangentPhaseBRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetTangentPhaseARadMaskedEditBox
            // 
            this.secondBushingSetTangentPhaseARadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTangentPhaseARadMaskedEditBox.Location = new System.Drawing.Point(155, 30);
            this.secondBushingSetTangentPhaseARadMaskedEditBox.Name = "secondBushingSetTangentPhaseARadMaskedEditBox";
            this.secondBushingSetTangentPhaseARadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetTangentPhaseARadMaskedEditBox.TabIndex = 10;
            this.secondBushingSetTangentPhaseARadMaskedEditBox.TabStop = false;
            this.secondBushingSetTangentPhaseARadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetCapacitancePhaseCRadLabel
            // 
            this.secondBushingSetCapacitancePhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetCapacitancePhaseCRadLabel.Location = new System.Drawing.Point(14, 153);
            this.secondBushingSetCapacitancePhaseCRadLabel.Name = "secondBushingSetCapacitancePhaseCRadLabel";
            this.secondBushingSetCapacitancePhaseCRadLabel.Size = new System.Drawing.Size(109, 18);
            this.secondBushingSetCapacitancePhaseCRadLabel.TabIndex = 9;
            this.secondBushingSetCapacitancePhaseCRadLabel.Text = "Capacitance Phase C";
            // 
            // secondBushingSetCapacitancePhaseBRadLabel
            // 
            this.secondBushingSetCapacitancePhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetCapacitancePhaseBRadLabel.Location = new System.Drawing.Point(14, 130);
            this.secondBushingSetCapacitancePhaseBRadLabel.Name = "secondBushingSetCapacitancePhaseBRadLabel";
            this.secondBushingSetCapacitancePhaseBRadLabel.Size = new System.Drawing.Size(108, 18);
            this.secondBushingSetCapacitancePhaseBRadLabel.TabIndex = 9;
            this.secondBushingSetCapacitancePhaseBRadLabel.Text = "Capacitance Phase B";
            // 
            // secondBushingSetCapacitancePhaseARadLabel
            // 
            this.secondBushingSetCapacitancePhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetCapacitancePhaseARadLabel.Location = new System.Drawing.Point(14, 107);
            this.secondBushingSetCapacitancePhaseARadLabel.Name = "secondBushingSetCapacitancePhaseARadLabel";
            this.secondBushingSetCapacitancePhaseARadLabel.Size = new System.Drawing.Size(109, 18);
            this.secondBushingSetCapacitancePhaseARadLabel.TabIndex = 8;
            this.secondBushingSetCapacitancePhaseARadLabel.Text = "Capacitance Phase A";
            // 
            // secondBushingSetTangentPhaseCRadLabel
            // 
            this.secondBushingSetTangentPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTangentPhaseCRadLabel.Location = new System.Drawing.Point(14, 78);
            this.secondBushingSetTangentPhaseCRadLabel.Name = "secondBushingSetTangentPhaseCRadLabel";
            this.secondBushingSetTangentPhaseCRadLabel.Size = new System.Drawing.Size(90, 18);
            this.secondBushingSetTangentPhaseCRadLabel.TabIndex = 7;
            this.secondBushingSetTangentPhaseCRadLabel.Text = "Tangent Phase C";
            // 
            // secondBushingSetTangentPhaseBRadLabel
            // 
            this.secondBushingSetTangentPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTangentPhaseBRadLabel.Location = new System.Drawing.Point(14, 55);
            this.secondBushingSetTangentPhaseBRadLabel.Name = "secondBushingSetTangentPhaseBRadLabel";
            this.secondBushingSetTangentPhaseBRadLabel.Size = new System.Drawing.Size(89, 18);
            this.secondBushingSetTangentPhaseBRadLabel.TabIndex = 6;
            this.secondBushingSetTangentPhaseBRadLabel.Text = "Tangent Phase B";
            // 
            // secondBushingSetTangentPhaseARadLabel
            // 
            this.secondBushingSetTangentPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTangentPhaseARadLabel.Location = new System.Drawing.Point(14, 32);
            this.secondBushingSetTangentPhaseARadLabel.Name = "secondBushingSetTangentPhaseARadLabel";
            this.secondBushingSetTangentPhaseARadLabel.Size = new System.Drawing.Size(90, 18);
            this.secondBushingSetTangentPhaseARadLabel.TabIndex = 5;
            this.secondBushingSetTangentPhaseARadLabel.Text = "Tangent Phase A";
            // 
            // secondBushingSetInputImpedenceRadGroupBox
            // 
            this.secondBushingSetInputImpedenceRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetInputImpedenceRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseCRadSpinEditor);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseBRadSpinEditor);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseARadSpinEditor);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseCOhmsRadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseBOhmsRadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseAOhmsRadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseCRadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseBRadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Controls.Add(this.secondBushingSetPhaseARadLabel);
            this.secondBushingSetInputImpedenceRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetInputImpedenceRadGroupBox.HeaderText = "Input Impedence";
            this.secondBushingSetInputImpedenceRadGroupBox.Location = new System.Drawing.Point(3, 164);
            this.secondBushingSetInputImpedenceRadGroupBox.Name = "secondBushingSetInputImpedenceRadGroupBox";
            this.secondBushingSetInputImpedenceRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetInputImpedenceRadGroupBox.Size = new System.Drawing.Size(200, 109);
            this.secondBushingSetInputImpedenceRadGroupBox.TabIndex = 10;
            this.secondBushingSetInputImpedenceRadGroupBox.Text = "Input Impedence";
            this.secondBushingSetInputImpedenceRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetPhaseCRadSpinEditor
            // 
            this.secondBushingSetPhaseCRadSpinEditor.DecimalPlaces = 2;
            this.secondBushingSetPhaseCRadSpinEditor.Location = new System.Drawing.Point(76, 79);
            this.secondBushingSetPhaseCRadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.secondBushingSetPhaseCRadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.secondBushingSetPhaseCRadSpinEditor.Name = "secondBushingSetPhaseCRadSpinEditor";
            // 
            // 
            // 
            this.secondBushingSetPhaseCRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.secondBushingSetPhaseCRadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.secondBushingSetPhaseCRadSpinEditor.TabIndex = 47;
            this.secondBushingSetPhaseCRadSpinEditor.TabStop = false;
            this.secondBushingSetPhaseCRadSpinEditor.ThemeName = "Office2007Black";
            this.secondBushingSetPhaseCRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // secondBushingSetPhaseBRadSpinEditor
            // 
            this.secondBushingSetPhaseBRadSpinEditor.DecimalPlaces = 2;
            this.secondBushingSetPhaseBRadSpinEditor.Location = new System.Drawing.Point(76, 55);
            this.secondBushingSetPhaseBRadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.secondBushingSetPhaseBRadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.secondBushingSetPhaseBRadSpinEditor.Name = "secondBushingSetPhaseBRadSpinEditor";
            // 
            // 
            // 
            this.secondBushingSetPhaseBRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.secondBushingSetPhaseBRadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.secondBushingSetPhaseBRadSpinEditor.TabIndex = 46;
            this.secondBushingSetPhaseBRadSpinEditor.TabStop = false;
            this.secondBushingSetPhaseBRadSpinEditor.ThemeName = "Office2007Black";
            this.secondBushingSetPhaseBRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // secondBushingSetPhaseARadSpinEditor
            // 
            this.secondBushingSetPhaseARadSpinEditor.DecimalPlaces = 2;
            this.secondBushingSetPhaseARadSpinEditor.Location = new System.Drawing.Point(76, 31);
            this.secondBushingSetPhaseARadSpinEditor.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.secondBushingSetPhaseARadSpinEditor.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.secondBushingSetPhaseARadSpinEditor.Name = "secondBushingSetPhaseARadSpinEditor";
            // 
            // 
            // 
            this.secondBushingSetPhaseARadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.secondBushingSetPhaseARadSpinEditor.Size = new System.Drawing.Size(66, 20);
            this.secondBushingSetPhaseARadSpinEditor.TabIndex = 45;
            this.secondBushingSetPhaseARadSpinEditor.TabStop = false;
            this.secondBushingSetPhaseARadSpinEditor.ThemeName = "Office2007Black";
            this.secondBushingSetPhaseARadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // secondBushingSetPhaseCOhmsRadLabel
            // 
            this.secondBushingSetPhaseCOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseCOhmsRadLabel.Location = new System.Drawing.Point(148, 83);
            this.secondBushingSetPhaseCOhmsRadLabel.Name = "secondBushingSetPhaseCOhmsRadLabel";
            this.secondBushingSetPhaseCOhmsRadLabel.Size = new System.Drawing.Size(36, 18);
            this.secondBushingSetPhaseCOhmsRadLabel.TabIndex = 8;
            this.secondBushingSetPhaseCOhmsRadLabel.Text = "Ohms";
            // 
            // secondBushingSetPhaseBOhmsRadLabel
            // 
            this.secondBushingSetPhaseBOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseBOhmsRadLabel.Location = new System.Drawing.Point(148, 59);
            this.secondBushingSetPhaseBOhmsRadLabel.Name = "secondBushingSetPhaseBOhmsRadLabel";
            this.secondBushingSetPhaseBOhmsRadLabel.Size = new System.Drawing.Size(36, 18);
            this.secondBushingSetPhaseBOhmsRadLabel.TabIndex = 8;
            this.secondBushingSetPhaseBOhmsRadLabel.Text = "Ohms";
            // 
            // secondBushingSetPhaseAOhmsRadLabel
            // 
            this.secondBushingSetPhaseAOhmsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseAOhmsRadLabel.Location = new System.Drawing.Point(148, 35);
            this.secondBushingSetPhaseAOhmsRadLabel.Name = "secondBushingSetPhaseAOhmsRadLabel";
            this.secondBushingSetPhaseAOhmsRadLabel.Size = new System.Drawing.Size(36, 18);
            this.secondBushingSetPhaseAOhmsRadLabel.TabIndex = 7;
            this.secondBushingSetPhaseAOhmsRadLabel.Text = "Ohms";
            // 
            // secondBushingSetPhaseCRadLabel
            // 
            this.secondBushingSetPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseCRadLabel.Location = new System.Drawing.Point(14, 83);
            this.secondBushingSetPhaseCRadLabel.Name = "secondBushingSetPhaseCRadLabel";
            this.secondBushingSetPhaseCRadLabel.Size = new System.Drawing.Size(46, 18);
            this.secondBushingSetPhaseCRadLabel.TabIndex = 6;
            this.secondBushingSetPhaseCRadLabel.Text = "Phase C";
            // 
            // secondBushingSetPhaseBRadLabel
            // 
            this.secondBushingSetPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseBRadLabel.Location = new System.Drawing.Point(14, 59);
            this.secondBushingSetPhaseBRadLabel.Name = "secondBushingSetPhaseBRadLabel";
            this.secondBushingSetPhaseBRadLabel.Size = new System.Drawing.Size(45, 18);
            this.secondBushingSetPhaseBRadLabel.TabIndex = 5;
            this.secondBushingSetPhaseBRadLabel.Text = "Phase B";
            // 
            // secondBushingSetPhaseARadLabel
            // 
            this.secondBushingSetPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetPhaseARadLabel.Location = new System.Drawing.Point(14, 35);
            this.secondBushingSetPhaseARadLabel.Name = "secondBushingSetPhaseARadLabel";
            this.secondBushingSetPhaseARadLabel.Size = new System.Drawing.Size(46, 18);
            this.secondBushingSetPhaseARadLabel.TabIndex = 4;
            this.secondBushingSetPhaseARadLabel.Text = "Phase A";
            // 
            // secondBushingSetThresholdsRadGroupBox
            // 
            this.secondBushingSetThresholdsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetThresholdsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetThresholdsRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeRadGroupBox);
            this.secondBushingSetThresholdsRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoeffRadGroupBox);
            this.secondBushingSetThresholdsRadGroupBox.Controls.Add(this.secondBushingSetAmplitudeRadGroupBox);
            this.secondBushingSetThresholdsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetThresholdsRadGroupBox.HeaderText = "Thresholds";
            this.secondBushingSetThresholdsRadGroupBox.Location = new System.Drawing.Point(209, 3);
            this.secondBushingSetThresholdsRadGroupBox.Name = "secondBushingSetThresholdsRadGroupBox";
            this.secondBushingSetThresholdsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetThresholdsRadGroupBox.Size = new System.Drawing.Size(285, 270);
            this.secondBushingSetThresholdsRadGroupBox.TabIndex = 11;
            this.secondBushingSetThresholdsRadGroupBox.Text = "Thresholds";
            this.secondBushingSetThresholdsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetRateOfChangeRadGroupBox
            // 
            this.secondBushingSetRateOfChangeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetRateOfChangeRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox);
            this.secondBushingSetRateOfChangeRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeAlarmThresholdRadLabel);
            this.secondBushingSetRateOfChangeRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeAlarmRadLabel);
            this.secondBushingSetRateOfChangeRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeAlarmOffRadRadioButton);
            this.secondBushingSetRateOfChangeRadGroupBox.Controls.Add(this.secondBushingSetRateOfChangeAlarmOnRadRadioButton);
            this.secondBushingSetRateOfChangeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetRateOfChangeRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.secondBushingSetRateOfChangeRadGroupBox.HeaderText = "Rate of Gamma Change (%/Year)";
            this.secondBushingSetRateOfChangeRadGroupBox.Location = new System.Drawing.Point(13, 185);
            this.secondBushingSetRateOfChangeRadGroupBox.Name = "secondBushingSetRateOfChangeRadGroupBox";
            this.secondBushingSetRateOfChangeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetRateOfChangeRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.secondBushingSetRateOfChangeRadGroupBox.TabIndex = 3;
            this.secondBushingSetRateOfChangeRadGroupBox.Text = "Rate of Gamma Change (%/Year)";
            this.secondBushingSetRateOfChangeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox
            // 
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(171, 36);
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Name = "secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox";
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.TabIndex = 9;
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetRateOfChangeAlarmThresholdRadLabel
            // 
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.Location = new System.Drawing.Point(114, 25);
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.Name = "secondBushingSetRateOfChangeAlarmThresholdRadLabel";
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.Size = new System.Drawing.Size(52, 46);
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.TabIndex = 8;
            this.secondBushingSetRateOfChangeAlarmThresholdRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // secondBushingSetRateOfChangeAlarmRadLabel
            // 
            this.secondBushingSetRateOfChangeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetRateOfChangeAlarmRadLabel.Location = new System.Drawing.Point(14, 37);
            this.secondBushingSetRateOfChangeAlarmRadLabel.Name = "secondBushingSetRateOfChangeAlarmRadLabel";
            this.secondBushingSetRateOfChangeAlarmRadLabel.Size = new System.Drawing.Size(36, 18);
            this.secondBushingSetRateOfChangeAlarmRadLabel.TabIndex = 7;
            this.secondBushingSetRateOfChangeAlarmRadLabel.Text = "Alarm";
            // 
            // secondBushingSetRateOfChangeAlarmOffRadRadioButton
            // 
           // this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.Location = new System.Drawing.Point(56, 47);
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.Name = "secondBushingSetRateOfChangeAlarmOffRadRadioButton";
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.TabIndex = 6;
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.Text = "Off";
            this.secondBushingSetRateOfChangeAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // secondBushingSetRateOfChangeAlarmOnRadRadioButton
            // 
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.Location = new System.Drawing.Point(56, 25);
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.Name = "secondBushingSetRateOfChangeAlarmOnRadRadioButton";
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.TabIndex = 5;
            this.secondBushingSetRateOfChangeAlarmOnRadRadioButton.Text = "On";
            // 
            // secondBushingSetTemperatureCoeffRadGroupBox
            // 
            this.secondBushingSetTemperatureCoeffRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoeffAlarmRadLabel);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Controls.Add(this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureCoeffRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.secondBushingSetTemperatureCoeffRadGroupBox.HeaderText = "Temperature Coefficient (%/C)";
            this.secondBushingSetTemperatureCoeffRadGroupBox.Location = new System.Drawing.Point(13, 104);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Name = "secondBushingSetTemperatureCoeffRadGroupBox";
            this.secondBushingSetTemperatureCoeffRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetTemperatureCoeffRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.secondBushingSetTemperatureCoeffRadGroupBox.TabIndex = 2;
            this.secondBushingSetTemperatureCoeffRadGroupBox.Text = "Temperature Coefficient (%/C)";
            this.secondBushingSetTemperatureCoeffRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox
            // 
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(171, 36);
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Name = "secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox";
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.TabIndex = 4;
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetTemperatureCoeffAlarmThresholdRadLabel
            // 
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.Location = new System.Drawing.Point(114, 25);
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.Name = "secondBushingSetTemperatureCoeffAlarmThresholdRadLabel";
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.Size = new System.Drawing.Size(52, 46);
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.TabIndex = 3;
            this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // secondBushingSetTemperatureCoeffAlarmRadLabel
            // 
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.Location = new System.Drawing.Point(14, 37);
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.Name = "secondBushingSetTemperatureCoeffAlarmRadLabel";
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.Size = new System.Drawing.Size(36, 18);
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.TabIndex = 2;
            this.secondBushingSetTemperatureCoeffAlarmRadLabel.Text = "Alarm";
            // 
            // secondBushingSetTemperatureCoeffAlarmOffRadRadioButton
            // 
         //   this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Location = new System.Drawing.Point(56, 47);
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Name = "secondBushingSetTemperatureCoeffAlarmOffRadRadioButton";
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.TabIndex = 1;
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Text = "Off";
            this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // secondBushingSetTemperatureCoeffAlarmOnRadRadioButton
            // 
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Location = new System.Drawing.Point(56, 25);
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Name = "secondBushingSetTemperatureCoeffAlarmOnRadRadioButton";
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Size = new System.Drawing.Size(35, 16);
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.TabIndex = 0;
            this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Text = "On";
            // 
            // secondBushingSetAmplitudeRadGroupBox
            // 
            this.secondBushingSetAmplitudeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetAmplitudeRadGroupBox.Controls.Add(this.secondBushingSetAmplitudeAlarmRadLabel);
            this.secondBushingSetAmplitudeRadGroupBox.Controls.Add(this.secondBushingSetAmplitudeWarningRadLabel);
            this.secondBushingSetAmplitudeRadGroupBox.Controls.Add(this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox);
            this.secondBushingSetAmplitudeRadGroupBox.Controls.Add(this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox);
            this.secondBushingSetAmplitudeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetAmplitudeRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.secondBushingSetAmplitudeRadGroupBox.HeaderText = "Amplitude (Gamma %)";
            this.secondBushingSetAmplitudeRadGroupBox.Location = new System.Drawing.Point(13, 23);
            this.secondBushingSetAmplitudeRadGroupBox.Name = "secondBushingSetAmplitudeRadGroupBox";
            this.secondBushingSetAmplitudeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetAmplitudeRadGroupBox.Size = new System.Drawing.Size(260, 75);
            this.secondBushingSetAmplitudeRadGroupBox.TabIndex = 0;
            this.secondBushingSetAmplitudeRadGroupBox.Text = "Amplitude (Gamma %)";
            this.secondBushingSetAmplitudeRadGroupBox.ThemeName = "Office2007Black";
            this.secondBushingSetAmplitudeRadGroupBox.Click += new System.EventHandler(this.secondBushingSetAmplitudeRadGroupBox_Click);
            // 
            // secondBushingSetAmplitudeAlarmRadLabel
            // 
            this.secondBushingSetAmplitudeAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetAmplitudeAlarmRadLabel.Location = new System.Drawing.Point(137, 25);
            this.secondBushingSetAmplitudeAlarmRadLabel.Name = "secondBushingSetAmplitudeAlarmRadLabel";
            this.secondBushingSetAmplitudeAlarmRadLabel.Size = new System.Drawing.Size(52, 46);
            this.secondBushingSetAmplitudeAlarmRadLabel.TabIndex = 5;
            this.secondBushingSetAmplitudeAlarmRadLabel.Text = "<html>Alarm<br>Threshold<br>Value</html>";
            // 
            // secondBushingSetAmplitudeWarningRadLabel
            // 
            this.secondBushingSetAmplitudeWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetAmplitudeWarningRadLabel.Location = new System.Drawing.Point(13, 25);
            this.secondBushingSetAmplitudeWarningRadLabel.Name = "secondBushingSetAmplitudeWarningRadLabel";
            this.secondBushingSetAmplitudeWarningRadLabel.Size = new System.Drawing.Size(52, 46);
            this.secondBushingSetAmplitudeWarningRadLabel.TabIndex = 4;
            this.secondBushingSetAmplitudeWarningRadLabel.Text = "<html>Warning<br>Threshold<br>Value</html>";
            // 
            // secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox
            // 
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Location = new System.Drawing.Point(194, 35);
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Name = "secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox";
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Size = new System.Drawing.Size(49, 18);
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.TabIndex = 2;
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.TabStop = false;
            this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox
            // 
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Location = new System.Drawing.Point(70, 35);
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Name = "secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox";
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Size = new System.Drawing.Size(52, 18);
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.TabIndex = 1;
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.TabStop = false;
            this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text = "0";
            // 
            // secondBushingSetSettingsRadGroupBox
            // 
            this.secondBushingSetSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.secondBushingSetSettingsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetkVRadLabel);
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetAmpsRadLabel);
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetCurrentRadLabel);
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetVoltageRadLabel);
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetCurrentRadMaskedEditBox);
            this.secondBushingSetSettingsRadGroupBox.Controls.Add(this.secondBushingSetVoltageRadMaskedEditBox);
            this.secondBushingSetSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetSettingsRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetSettingsRadGroupBox.HeaderText = "Settings";
            this.secondBushingSetSettingsRadGroupBox.Location = new System.Drawing.Point(3, 82);
            this.secondBushingSetSettingsRadGroupBox.Name = "secondBushingSetSettingsRadGroupBox";
            this.secondBushingSetSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.secondBushingSetSettingsRadGroupBox.Size = new System.Drawing.Size(200, 76);
            this.secondBushingSetSettingsRadGroupBox.TabIndex = 9;
            this.secondBushingSetSettingsRadGroupBox.Text = "Settings";
            this.secondBushingSetSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // secondBushingSetkVRadLabel
            // 
            this.secondBushingSetkVRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetkVRadLabel.Location = new System.Drawing.Point(148, 24);
            this.secondBushingSetkVRadLabel.Name = "secondBushingSetkVRadLabel";
            this.secondBushingSetkVRadLabel.Size = new System.Drawing.Size(19, 18);
            this.secondBushingSetkVRadLabel.TabIndex = 5;
            this.secondBushingSetkVRadLabel.Text = "kV";
            // 
            // secondBushingSetAmpsRadLabel
            // 
            this.secondBushingSetAmpsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.secondBushingSetAmpsRadLabel.Location = new System.Drawing.Point(148, 46);
            this.secondBushingSetAmpsRadLabel.Name = "secondBushingSetAmpsRadLabel";
            this.secondBushingSetAmpsRadLabel.Size = new System.Drawing.Size(35, 18);
            this.secondBushingSetAmpsRadLabel.TabIndex = 4;
            this.secondBushingSetAmpsRadLabel.Text = "Amps";
            this.secondBushingSetAmpsRadLabel.Visible = false;
            // 
            // secondBushingSetCurrentRadLabel
            // 
            this.secondBushingSetCurrentRadLabel.Location = new System.Drawing.Point(14, 48);
            this.secondBushingSetCurrentRadLabel.Name = "secondBushingSetCurrentRadLabel";
            this.secondBushingSetCurrentRadLabel.Size = new System.Drawing.Size(44, 18);
            this.secondBushingSetCurrentRadLabel.TabIndex = 3;
            this.secondBushingSetCurrentRadLabel.Text = "Current";
            this.secondBushingSetCurrentRadLabel.Visible = false;
            // 
            // secondBushingSetVoltageRadLabel
            // 
            this.secondBushingSetVoltageRadLabel.Location = new System.Drawing.Point(14, 24);
            this.secondBushingSetVoltageRadLabel.Name = "secondBushingSetVoltageRadLabel";
            this.secondBushingSetVoltageRadLabel.Size = new System.Drawing.Size(45, 18);
            this.secondBushingSetVoltageRadLabel.TabIndex = 2;
            this.secondBushingSetVoltageRadLabel.Text = "Voltage";
            // 
            // secondBushingSetCurrentRadMaskedEditBox
            // 
            this.secondBushingSetCurrentRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetCurrentRadMaskedEditBox.Location = new System.Drawing.Point(76, 46);
            this.secondBushingSetCurrentRadMaskedEditBox.Name = "secondBushingSetCurrentRadMaskedEditBox";
            this.secondBushingSetCurrentRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetCurrentRadMaskedEditBox.TabIndex = 1;
            this.secondBushingSetCurrentRadMaskedEditBox.TabStop = false;
            this.secondBushingSetCurrentRadMaskedEditBox.Text = "0";
            this.secondBushingSetCurrentRadMaskedEditBox.Visible = false;
            // 
            // secondBushingSetVoltageRadMaskedEditBox
            // 
            this.secondBushingSetVoltageRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondBushingSetVoltageRadMaskedEditBox.Location = new System.Drawing.Point(76, 22);
            this.secondBushingSetVoltageRadMaskedEditBox.Name = "secondBushingSetVoltageRadMaskedEditBox";
            this.secondBushingSetVoltageRadMaskedEditBox.Size = new System.Drawing.Size(66, 18);
            this.secondBushingSetVoltageRadMaskedEditBox.TabIndex = 0;
            this.secondBushingSetVoltageRadMaskedEditBox.TabStop = false;
            this.secondBushingSetVoltageRadMaskedEditBox.Text = "0";
            // 
            // initialBalanceDataRadPageViewPage
            // 
            this.initialBalanceDataRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.templateConfigurationsInitialBalanceDataTabRadGroupBox);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataRadProgressBar);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.programDeviceInitialBalanceDataTabRadButton);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabMeasurementDateValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabMeasurementDateRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseCRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseCRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseBRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabShiftPhaseARadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet1RadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabAmplitudePhaseARadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabBushingSet2RadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabTemperatureRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabImbalanceRadLabel);
            this.initialBalanceDataRadPageViewPage.Controls.Add(this.initialBalanceDataTabImbalancePhaseRadLabel);
          //  this.initialBalanceDataRadPageViewPage.ItemSize = new System.Drawing.SizeF(114F, 26F);
            this.initialBalanceDataRadPageViewPage.Location = new System.Drawing.Point(10, 35);
            this.initialBalanceDataRadPageViewPage.Name = "initialBalanceDataRadPageViewPage";
            this.initialBalanceDataRadPageViewPage.Size = new System.Drawing.Size(851, 434);
            this.initialBalanceDataRadPageViewPage.Text = "Initial Balance Data";
            // 
            // xmlConfigurationFileInitialBalanceDataTabRadGroupBox
            // 
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Controls.Add(this.loadFromFileInitialBalanceDataTabRadButton);
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Controls.Add(this.saveToFileInitialBalanceDataTabRadButton);
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.HeaderText = "XML Configuration File";
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Location = new System.Drawing.Point(3, 350);
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Name = "xmlConfigurationFileInitialBalanceDataTabRadGroupBox";
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Size = new System.Drawing.Size(258, 75);
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.TabIndex = 55;
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.Text = "XML Configuration File";
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadFromFileInitialBalanceDataTabRadButton
            // 
            this.loadFromFileInitialBalanceDataTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFromFileInitialBalanceDataTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFromFileInitialBalanceDataTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadFromFileInitialBalanceDataTabRadButton.Name = "loadFromFileInitialBalanceDataTabRadButton";
            this.loadFromFileInitialBalanceDataTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.loadFromFileInitialBalanceDataTabRadButton.TabIndex = 36;
            this.loadFromFileInitialBalanceDataTabRadButton.Text = "Load File";
            this.loadFromFileInitialBalanceDataTabRadButton.ThemeName = "Office2007Black";
            this.loadFromFileInitialBalanceDataTabRadButton.Click += new System.EventHandler(this.loadFromFileRadButton_Click);
            // 
            // saveToFileInitialBalanceDataTabRadButton
            // 
            this.saveToFileInitialBalanceDataTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveToFileInitialBalanceDataTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveToFileInitialBalanceDataTabRadButton.Location = new System.Drawing.Point(136, 23);
            this.saveToFileInitialBalanceDataTabRadButton.Name = "saveToFileInitialBalanceDataTabRadButton";
            this.saveToFileInitialBalanceDataTabRadButton.Size = new System.Drawing.Size(110, 39);
            this.saveToFileInitialBalanceDataTabRadButton.TabIndex = 35;
            this.saveToFileInitialBalanceDataTabRadButton.Text = "Save File";
            this.saveToFileInitialBalanceDataTabRadButton.ThemeName = "Office2007Black";
            this.saveToFileInitialBalanceDataTabRadButton.Click += new System.EventHandler(this.saveToFileRadButton_Click);
            // 
            // templateConfigurationsInitialBalanceDataTabRadGroupBox
            // 
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Controls.Add(this.loadSelectedConfigurationInitialBalanceDataTabRadButton);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Controls.Add(this.templateConfigurationsInitialBalanceDataTabRadDropDownList);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Controls.Add(this.radButton11);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Location = new System.Drawing.Point(3, 279);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Name = "templateConfigurationsInitialBalanceDataTabRadGroupBox";
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Size = new System.Drawing.Size(572, 65);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.TabIndex = 53;
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadSelectedConfigurationInitialBalanceDataTabRadButton
            // 
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Location = new System.Drawing.Point(444, 11);
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Name = "loadSelectedConfigurationInitialBalanceDataTabRadButton";
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.TabIndex = 34;
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.ThemeName = "Office2007Black";
            this.loadSelectedConfigurationInitialBalanceDataTabRadButton.Click += new System.EventHandler(this.loadSelectedTemplateConfigurationRadButton_Click);
            // 
            // templateConfigurationsInitialBalanceDataTabRadDropDownList
            // 
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.Name = "templateConfigurationsInitialBalanceDataTabRadDropDownList";
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.Size = new System.Drawing.Size(424, 20);
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsInitialBalanceDataTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsInitialBalanceDataTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton11
            // 
            this.radButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton11.Location = new System.Drawing.Point(0, 232);
            this.radButton11.Name = "radButton11";
            this.radButton11.Size = new System.Drawing.Size(130, 70);
            this.radButton11.TabIndex = 8;
            this.radButton11.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton11.ThemeName = "Office2007Black";
            // 
            // initialBalanceDataRadProgressBar
            // 
            this.initialBalanceDataRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.initialBalanceDataRadProgressBar.Location = new System.Drawing.Point(552, 357);
            this.initialBalanceDataRadProgressBar.Name = "initialBalanceDataRadProgressBar";
            this.initialBalanceDataRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.initialBalanceDataRadProgressBar.TabIndex = 50;
            this.initialBalanceDataRadProgressBar.Text = "radProgressBar1";
            // 
            // loadConfigurationFromDeviceInitialBalanceDataTabRadButton
            // 
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Location = new System.Drawing.Point(552, 393);
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Name = "loadConfigurationFromDeviceInitialBalanceDataTabRadButton";
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.TabIndex = 45;
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Text = "<html>Load Configuration</html>";
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceInitialBalanceDataTabRadButton
            // 
            this.programDeviceInitialBalanceDataTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceInitialBalanceDataTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceInitialBalanceDataTabRadButton.Location = new System.Drawing.Point(683, 393);
            this.programDeviceInitialBalanceDataTabRadButton.Name = "programDeviceInitialBalanceDataTabRadButton";
            this.programDeviceInitialBalanceDataTabRadButton.Size = new System.Drawing.Size(125, 30);
            this.programDeviceInitialBalanceDataTabRadButton.TabIndex = 41;
            this.programDeviceInitialBalanceDataTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceInitialBalanceDataTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceInitialBalanceDataTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // initialBalanceDataTabMeasurementDateValueRadLabel
            // 
            this.initialBalanceDataTabMeasurementDateValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Location = new System.Drawing.Point(156, 19);
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Name = "initialBalanceDataTabMeasurementDateValueRadLabel";
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Size = new System.Drawing.Size(110, 16);
            this.initialBalanceDataTabMeasurementDateValueRadLabel.TabIndex = 22;
            this.initialBalanceDataTabMeasurementDateValueRadLabel.Text = "01/01/2000 00:00:00";
            // 
            // initialBalanceDataTabMeasurementDateRadLabel
            // 
            this.initialBalanceDataTabMeasurementDateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabMeasurementDateRadLabel.Location = new System.Drawing.Point(41, 19);
            this.initialBalanceDataTabMeasurementDateRadLabel.Name = "initialBalanceDataTabMeasurementDateRadLabel";
            this.initialBalanceDataTabMeasurementDateRadLabel.Size = new System.Drawing.Size(109, 16);
            this.initialBalanceDataTabMeasurementDateRadLabel.TabIndex = 21;
            this.initialBalanceDataTabMeasurementDateRadLabel.Text = "Measurement Date: ";
            // 
            // initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Location = new System.Drawing.Point(552, 241);
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.TabIndex = 15;
            this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Location = new System.Drawing.Point(552, 175);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.TabIndex = 17;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBushingDataSet2InputSignalShiftPhaseBRadLabel
            // 
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Location = new System.Drawing.Point(552, 219);
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Name = "initialBushingDataSet2InputSignalShiftPhaseBRadLabel";
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.TabIndex = 18;
            this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Location = new System.Drawing.Point(552, 153);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.TabIndex = 19;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Location = new System.Drawing.Point(552, 197);
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.TabIndex = 16;
            this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Location = new System.Drawing.Point(552, 131);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.TabIndex = 13;
            this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2TemperatureValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Location = new System.Drawing.Point(552, 109);
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Name = "initialBalanceDataTabBushingSet2TemperatureValueRadLabel";
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.TabIndex = 14;
            this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Location = new System.Drawing.Point(552, 87);
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Name = "initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel";
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.TabIndex = 20;
            this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet2ImbalanceValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Location = new System.Drawing.Point(552, 65);
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Name = "initialBalanceDataTabBushingSet2ImbalanceValueRadLabel";
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.TabIndex = 12;
            this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Location = new System.Drawing.Point(390, 241);
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Location = new System.Drawing.Point(390, 175);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Location = new System.Drawing.Point(390, 219);
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Location = new System.Drawing.Point(390, 153);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Location = new System.Drawing.Point(390, 197);
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Location = new System.Drawing.Point(390, 131);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Name = "initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel";
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1TemperatureValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Location = new System.Drawing.Point(390, 109);
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Name = "initialBalanceDataTabBushingSet1TemperatureValueRadLabel";
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.TabIndex = 10;
            this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Location = new System.Drawing.Point(390, 87);
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Name = "initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel";
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.TabIndex = 11;
            this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabBushingSet1ImbalanceValueRadLabel
            // 
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Location = new System.Drawing.Point(390, 65);
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Name = "initialBalanceDataTabBushingSet1ImbalanceValueRadLabel";
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Size = new System.Drawing.Size(28, 16);
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.TabIndex = 9;
            this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel.Text = "0.00";
            // 
            // initialBalanceDataTabShiftPhaseCRadLabel
            // 
            this.initialBalanceDataTabShiftPhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseCRadLabel.Location = new System.Drawing.Point(41, 241);
            this.initialBalanceDataTabShiftPhaseCRadLabel.Name = "initialBalanceDataTabShiftPhaseCRadLabel";
            this.initialBalanceDataTabShiftPhaseCRadLabel.Size = new System.Drawing.Size(164, 16);
            this.initialBalanceDataTabShiftPhaseCRadLabel.TabIndex = 8;
            this.initialBalanceDataTabShiftPhaseCRadLabel.Text = "Input signal shift Phase C (deg)";
            // 
            // initialBalanceDataTabShiftPhaseBRadLabel
            // 
            this.initialBalanceDataTabShiftPhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseBRadLabel.Location = new System.Drawing.Point(41, 219);
            this.initialBalanceDataTabShiftPhaseBRadLabel.Name = "initialBalanceDataTabShiftPhaseBRadLabel";
            this.initialBalanceDataTabShiftPhaseBRadLabel.Size = new System.Drawing.Size(163, 16);
            this.initialBalanceDataTabShiftPhaseBRadLabel.TabIndex = 8;
            this.initialBalanceDataTabShiftPhaseBRadLabel.Text = "Input signal shift Phase B (deg)";
            // 
            // initialBalanceDataTabAmplitudePhaseCRadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Location = new System.Drawing.Point(40, 175);
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Name = "initialBalanceDataTabAmplitudePhaseCRadLabel";
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.TabIndex = 7;
            this.initialBalanceDataTabAmplitudePhaseCRadLabel.Text = "Input signal amplitude Phase C (mV)";
            // 
            // initialBalanceDataTabAmplitudePhaseBRadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Location = new System.Drawing.Point(41, 153);
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Name = "initialBalanceDataTabAmplitudePhaseBRadLabel";
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.TabIndex = 7;
            this.initialBalanceDataTabAmplitudePhaseBRadLabel.Text = "Input signal amplitude Phase B (mV)";
            // 
            // initialBalanceDataTabShiftPhaseARadLabel
            // 
            this.initialBalanceDataTabShiftPhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabShiftPhaseARadLabel.Location = new System.Drawing.Point(41, 197);
            this.initialBalanceDataTabShiftPhaseARadLabel.Name = "initialBalanceDataTabShiftPhaseARadLabel";
            this.initialBalanceDataTabShiftPhaseARadLabel.Size = new System.Drawing.Size(163, 16);
            this.initialBalanceDataTabShiftPhaseARadLabel.TabIndex = 6;
            this.initialBalanceDataTabShiftPhaseARadLabel.Text = "Input signal shift Phase A (deg)";
            // 
            // initialBalanceDataTabBushingSet1RadLabel
            // 
            this.initialBalanceDataTabBushingSet1RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet1RadLabel.Location = new System.Drawing.Point(374, 19);
            this.initialBalanceDataTabBushingSet1RadLabel.Name = "initialBalanceDataTabBushingSet1RadLabel";
            this.initialBalanceDataTabBushingSet1RadLabel.Size = new System.Drawing.Size(76, 16);
            this.initialBalanceDataTabBushingSet1RadLabel.TabIndex = 5;
            this.initialBalanceDataTabBushingSet1RadLabel.Text = "Bushing Set 1";
            // 
            // initialBalanceDataTabAmplitudePhaseARadLabel
            // 
            this.initialBalanceDataTabAmplitudePhaseARadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Location = new System.Drawing.Point(41, 131);
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Name = "initialBalanceDataTabAmplitudePhaseARadLabel";
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Size = new System.Drawing.Size(190, 16);
            this.initialBalanceDataTabAmplitudePhaseARadLabel.TabIndex = 5;
            this.initialBalanceDataTabAmplitudePhaseARadLabel.Text = "Input signal amplitude Phase A (mV)";
            // 
            // initialBalanceDataTabBushingSet2RadLabel
            // 
            this.initialBalanceDataTabBushingSet2RadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabBushingSet2RadLabel.Location = new System.Drawing.Point(534, 19);
            this.initialBalanceDataTabBushingSet2RadLabel.Name = "initialBalanceDataTabBushingSet2RadLabel";
            this.initialBalanceDataTabBushingSet2RadLabel.Size = new System.Drawing.Size(76, 16);
            this.initialBalanceDataTabBushingSet2RadLabel.TabIndex = 5;
            this.initialBalanceDataTabBushingSet2RadLabel.Text = "Bushing Set 2";
            // 
            // initialBalanceDataTabTemperatureRadLabel
            // 
            this.initialBalanceDataTabTemperatureRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabTemperatureRadLabel.Location = new System.Drawing.Point(41, 109);
            this.initialBalanceDataTabTemperatureRadLabel.Name = "initialBalanceDataTabTemperatureRadLabel";
            this.initialBalanceDataTabTemperatureRadLabel.Size = new System.Drawing.Size(95, 16);
            this.initialBalanceDataTabTemperatureRadLabel.TabIndex = 5;
            this.initialBalanceDataTabTemperatureRadLabel.Text = "Temperature (�C)";
            // 
            // initialBalanceDataTabImbalanceRadLabel
            // 
            this.initialBalanceDataTabImbalanceRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabImbalanceRadLabel.Location = new System.Drawing.Point(40, 65);
            this.initialBalanceDataTabImbalanceRadLabel.Name = "initialBalanceDataTabImbalanceRadLabel";
            this.initialBalanceDataTabImbalanceRadLabel.Size = new System.Drawing.Size(79, 16);
            this.initialBalanceDataTabImbalanceRadLabel.TabIndex = 4;
            this.initialBalanceDataTabImbalanceRadLabel.Text = "Imbalance (%)";
            // 
            // initialBalanceDataTabImbalancePhaseRadLabel
            // 
            this.initialBalanceDataTabImbalancePhaseRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.initialBalanceDataTabImbalancePhaseRadLabel.Location = new System.Drawing.Point(41, 87);
            this.initialBalanceDataTabImbalancePhaseRadLabel.Name = "initialBalanceDataTabImbalancePhaseRadLabel";
            this.initialBalanceDataTabImbalancePhaseRadLabel.Size = new System.Drawing.Size(144, 16);
            this.initialBalanceDataTabImbalancePhaseRadLabel.TabIndex = 3;
            this.initialBalanceDataTabImbalancePhaseRadLabel.Text = "Imbalance Phase, gradient:";
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.radPageViewPage1.Controls.Add(this.radButton3);
            this.radPageViewPage1.Controls.Add(this.radButton1);
            this.radPageViewPage1.Controls.Add(this.radTextBox2);
            this.radPageViewPage1.Controls.Add(this.radLabel12);
            this.radPageViewPage1.Controls.Add(this.radLabel9);
            this.radPageViewPage1.Controls.Add(this.radLabel4);
            this.radPageViewPage1.Controls.Add(this.radLabel5);
            this.radPageViewPage1.Controls.Add(this.radLabel6);
            this.radPageViewPage1.Controls.Add(this.radLabel7);
            this.radPageViewPage1.Controls.Add(this.radLabel8);
            this.radPageViewPage1.Controls.Add(this.radLabel3);
            this.radPageViewPage1.Controls.Add(this.radLabel2);
            this.radPageViewPage1.Controls.Add(this.radGridView2);
            this.radPageViewPage1.Controls.Add(this.radGridView1);
            this.radPageViewPage1.Controls.Add(this.radTextBox1);
            this.radPageViewPage1.Controls.Add(this.radLabel1);
          //  this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(111F, 26F);
            this.radPageViewPage1.Location = new System.Drawing.Point(10, 35);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(851, 434);
            this.radPageViewPage1.Text = "Bushing Calculator";
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(283, 314);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 24);
            this.radButton3.TabIndex = 18;
            this.radButton3.Text = "Calculate";
            this.radButton3.ThemeName = "Office2007Black";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(550, 24);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 24);
            this.radButton1.TabIndex = 17;
            this.radButton1.Text = "Windows Calculator";
            this.radButton1.ThemeName = "Office2007Black";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(105, 59);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(100, 20);
            this.radTextBox2.TabIndex = 12;
            this.radTextBox2.TabStop = false;
            this.radTextBox2.Text = " 60";
            // 
            // radLabel12
            // 
            this.radLabel12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel12.Location = new System.Drawing.Point(592, 104);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(116, 16);
            this.radLabel12.TabIndex = 16;
            this.radLabel12.Text = "Impedance Calculator";
            // 
            // radLabel9
            // 
            this.radLabel9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel9.Location = new System.Drawing.Point(50, 104);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(82, 16);
            this.radLabel9.TabIndex = 15;
            this.radLabel9.Text = "Balance Check";
            // 
            // radLabel4
            // 
            this.radLabel4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel4.Location = new System.Drawing.Point(149, 335);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(24, 16);
            this.radLabel4.TabIndex = 11;
            this.radLabel4.Text = "Min";
            // 
            // radLabel5
            // 
            this.radLabel5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel5.Location = new System.Drawing.Point(149, 314);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(28, 16);
            this.radLabel5.TabIndex = 11;
            this.radLabel5.Text = "Max";
            // 
            // radLabel6
            // 
            this.radLabel6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel6.Location = new System.Drawing.Point(31, 356);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(74, 16);
            this.radLabel6.TabIndex = 11;
            this.radLabel6.Text = "% Difference:";
            // 
            // radLabel7
            // 
            this.radLabel7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel7.Location = new System.Drawing.Point(31, 335);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(56, 16);
            this.radLabel7.TabIndex = 11;
            this.radLabel7.Text = "Min Volts:";
            // 
            // radLabel8
            // 
            this.radLabel8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel8.Location = new System.Drawing.Point(31, 314);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(59, 16);
            this.radLabel8.TabIndex = 11;
            this.radLabel8.Text = "Max Volts:";
            // 
            // radLabel3
            // 
            this.radLabel3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radLabel3.Location = new System.Drawing.Point(149, 356);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(69, 16);
            this.radLabel3.TabIndex = 14;
            this.radLabel3.Text = "Will Balance";
            // 
            // radLabel2
            // 
            this.radLabel2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel2.Location = new System.Drawing.Point(17, 59);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(82, 16);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "Frequency (hz)";
            // 
            // radGridView2
            // 
            this.radGridView2.Location = new System.Drawing.Point(580, 125);
            // 
            // 
            // 
            this.radGridView2.MasterTemplate.AllowAddNewRow = false;
            this.radGridView2.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnChooser = false;
            this.radGridView2.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.radGridView2.MasterTemplate.AllowColumnReorder = false;
            this.radGridView2.MasterTemplate.AllowColumnResize = false;
            this.radGridView2.MasterTemplate.AllowDeleteRow = false;
            this.radGridView2.MasterTemplate.AllowDragToGroup = false;
            this.radGridView2.MasterTemplate.AllowEditRow = false;
            this.radGridView2.MasterTemplate.AllowRowResize = false;
            this.radGridView2.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.HeaderText = "Phase";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn2.HeaderText = "Desired R";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.HeaderText = "Required Parallel R";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn3.WrapText = true;
            this.radGridView2.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView2.ReadOnly = true;
            this.radGridView2.Size = new System.Drawing.Size(245, 154);
            this.radGridView2.TabIndex = 13;
            this.radGridView2.Text = "radGridView2";
            this.radGridView2.ThemeName = "Office2007Black";
            // 
            // radGridView1
            // 
            this.radGridView1.Location = new System.Drawing.Point(50, 125);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowCellContextMenu = false;
            this.radGridView1.MasterTemplate.AllowColumnChooser = false;
            this.radGridView1.MasterTemplate.AllowColumnReorder = false;
            this.radGridView1.MasterTemplate.AllowColumnResize = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowDragToGroup = false;
            this.radGridView1.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn4.HeaderText = "Phase";
            gridViewTextBoxColumn4.Name = "column1";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn5.HeaderText = "C1 (pF)";
            gridViewTextBoxColumn5.Name = "column2";
            gridViewTextBoxColumn6.HeaderText = "Burden  R (Ohms)";
            gridViewTextBoxColumn6.Name = "column3";
            gridViewTextBoxColumn6.Width = 80;
            gridViewTextBoxColumn6.WrapText = true;
            gridViewTextBoxColumn7.HeaderText = "Current (ma)";
            gridViewTextBoxColumn7.Name = "column4";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 80;
            gridViewTextBoxColumn8.HeaderText = "Voltage (V)";
            gridViewTextBoxColumn8.Name = "column5";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.HeaderText = "Range Check";
            gridViewTextBoxColumn9.Name = "column6";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 80;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridView1.Size = new System.Drawing.Size(467, 154);
            this.radGridView1.TabIndex = 12;
            this.radGridView1.Text = "radGridView1";
            this.radGridView1.ThemeName = "Office2007Black";
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(105, 28);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(100, 20);
            this.radTextBox1.TabIndex = 11;
            this.radTextBox1.TabStop = false;
            this.radTextBox1.Text = "500";
            // 
            // radLabel1
            // 
            this.radLabel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Location = new System.Drawing.Point(17, 29);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(68, 16);
            this.radLabel1.TabIndex = 10;
            this.radLabel1.Text = "Voltage (kV)";
            // 
            // BHM_MonitorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(872, 480);
            this.Controls.Add(this.configurationRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BHM_MonitorConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bushing Monitor Configuration";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BHM_MonitorConfiguration_FormClosing);
            this.Load += new System.EventHandler(this.BHM_MonitorConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).EndInit();
            this.configurationRadPageView.ResumeLayout(false);
            this.commonParametersRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileCommonParametersTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileCommonParametersTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileCommonParametersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileCommonParametersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationCommonParametersTabRadGroupBox)).EndInit();
            this.templateConfigurationCommonParametersTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationCommonParametersTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationCommonParametersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCommonParametersTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commonParametersRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCommonParametersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCommonParametersTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleRadGroupBox)).EndInit();
            this.measurementScheduleRadGroupBox.ResumeLayout(false);
            this.measurementScheduleRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementSettingsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.byScheduleMeasurementScheduleRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleMinuteRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measurementScheduleHourRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourMeasurementScheduleRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepMeasurementScheduleRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.basicSettingsRadGroupBox)).EndInit();
            this.basicSettingsRadGroupBox.ResumeLayout(false);
            this.basicSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).EndInit();
            this.monitoringRadGroupBox.ResumeLayout(false);
            this.monitoringRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceUnitsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysToComputeRadGroupBox)).EndInit();
            this.daysToComputeRadGroupBox.ResumeLayout(false);
            this.daysToComputeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateOfChangeRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tangentRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureCoefficientRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowablePhaseDifferenceRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.averagingUnnRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.averagingUnnRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementUnitsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmationMeasurementRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisUnitsradLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hysterisisRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modBusAddressRadMaskedEditBox)).EndInit();
            this.firstBushingSetRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileFirstBushingSetTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileFirstBushingSetTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileFirstBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileFirstBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsFirstBushingSetTabRadGroupBox)).EndInit();
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsFirstBushingSetTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationFirstBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsFirstBushingSetTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceFirstBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTransformerNameplateRadGroupBox)).EndInit();
            this.firstBushingSetTransformerNameplateRadGroupBox.ResumeLayout(false);
            this.firstBushingSetTransformerNameplateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureAtMeasurementRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureAtMeasurementRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseCRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseBRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseARadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseCRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseBRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseARadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCapacitancePhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTangentPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceFirstBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetMeasurementsRadGroupBox)).EndInit();
            this.firstBushingSetMeasurementsRadGroupBox.ResumeLayout(false);
            this.firstBushingSetMeasurementsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetDisableMeasurementsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetEnableMeasurementsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureSensorRadGroupBox)).EndInit();
            this.firstBushingSetTemperatureSensorRadGroupBox.ResumeLayout(false);
            this.firstBushingSetTemperatureSensorRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureSensorRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetInputImpedenceRadGroupBox)).EndInit();
            this.firstBushingSetInputImpedenceRadGroupBox.ResumeLayout(false);
            this.firstBushingSetInputImpedenceRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseARadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseAOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetThresholdsRadGroupBox)).EndInit();
            this.firstBushingSetThresholdsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeRadGroupBox)).EndInit();
            this.firstBushingSetRateOfChangeRadGroupBox.ResumeLayout(false);
            this.firstBushingSetRateOfChangeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmThresholdRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmOffRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetRateOfChangeAlarmOnRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffRadGroupBox)).EndInit();
            this.firstBushingSetTemperatureCoeffRadGroupBox.ResumeLayout(false);
            this.firstBushingSetTemperatureCoeffRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmThresholdRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatrreCoeffAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmOffRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetTemperatureCoeffAlarmOnRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeRadGroupBox)).EndInit();
            this.firstBushingSetAmplitudeRadGroupBox.ResumeLayout(false);
            this.firstBushingSetAmplitudeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetSettingsRadGroupBox)).EndInit();
            this.firstBushingSetSettingsRadGroupBox.ResumeLayout(false);
            this.firstBushingSetSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetkVRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetAmpsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCurrentRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetVoltageRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetCurrentRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstBushingSetVoltageRadMaskedEditBox)).EndInit();
            this.secondBushingSetRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileSecondBushingSetTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileSecondBushingSetTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileSecondBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileSecondBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSecondBushingSetTabRadGroupBox)).EndInit();
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsSecondBushingSetTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationSecondBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSecondBushingSetTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceSecondBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceSecondBushingSetTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetMeasurementsRadGroupBox)).EndInit();
            this.secondBushingSetMeasurementsRadGroupBox.ResumeLayout(false);
            this.secondBushingSetMeasurementsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetDisableMeasurementsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetEnableMeasurementsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureSensorRadGroupBox)).EndInit();
            this.secondBushingSetTemperatureSensorRadGroupBox.ResumeLayout(false);
            this.secondBushingSetTemperatureSensorRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureSensorRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTransformerNameplateRadGroupBox)).EndInit();
            this.secondBushingSetTransformerNameplateRadGroupBox.ResumeLayout(false);
            this.secondBushingSetTransformerNameplateRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureAtMeasurementRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureAtMeasurementRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseCRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseBRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseARadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseCRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseBRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseARadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCapacitancePhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTangentPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetInputImpedenceRadGroupBox)).EndInit();
            this.secondBushingSetInputImpedenceRadGroupBox.ResumeLayout(false);
            this.secondBushingSetInputImpedenceRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseARadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseAOhmsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetThresholdsRadGroupBox)).EndInit();
            this.secondBushingSetThresholdsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeRadGroupBox)).EndInit();
            this.secondBushingSetRateOfChangeRadGroupBox.ResumeLayout(false);
            this.secondBushingSetRateOfChangeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmThresholdRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmOffRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetRateOfChangeAlarmOnRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffRadGroupBox)).EndInit();
            this.secondBushingSetTemperatureCoeffRadGroupBox.ResumeLayout(false);
            this.secondBushingSetTemperatureCoeffRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmThresholdRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmOffRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetTemperatureCoeffAlarmOnRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeRadGroupBox)).EndInit();
            this.secondBushingSetAmplitudeRadGroupBox.ResumeLayout(false);
            this.secondBushingSetAmplitudeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetSettingsRadGroupBox)).EndInit();
            this.secondBushingSetSettingsRadGroupBox.ResumeLayout(false);
            this.secondBushingSetSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetkVRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetAmpsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCurrentRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetVoltageRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetCurrentRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondBushingSetVoltageRadMaskedEditBox)).EndInit();
            this.initialBalanceDataRadPageViewPage.ResumeLayout(false);
            this.initialBalanceDataRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox)).EndInit();
            this.xmlConfigurationFileInitialBalanceDataTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadFromFileInitialBalanceDataTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveToFileInitialBalanceDataTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsInitialBalanceDataTabRadGroupBox)).EndInit();
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsInitialBalanceDataTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadSelectedConfigurationInitialBalanceDataTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsInitialBalanceDataTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceInitialBalanceDataTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceInitialBalanceDataTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabMeasurementDateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBushingDataSet2InputSignalShiftPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2TemperatureValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2ImbalanceValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1TemperatureValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1ImbalanceValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseCRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseBRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabShiftPhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet1RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabAmplitudePhaseARadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabBushingSet2RadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabTemperatureRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalanceRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialBalanceDataTabImbalancePhaseRadLabel)).EndInit();
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView configurationRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage commonParametersRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage firstBushingSetRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage secondBushingSetRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox basicSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel baudRateRadLabel;
        private Telerik.WinControls.UI.RadLabel modbusAddressRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox modBusAddressRadMaskedEditBox;
        private Telerik.WinControls.UI.RadGroupBox daysToComputeRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox rateOfChangeRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox tangentRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox temperatureCoefficientRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel tangentRadLabel;
        private Telerik.WinControls.UI.RadLabel temperatureCoefficientRadLabel;
        private Telerik.WinControls.UI.RadLabel trendRadLabel;
        private Telerik.WinControls.UI.RadLabel allowablePhaseDifferenceUnitsRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox allowablePhaseDifferenceRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel allowablePhaseDifferenceRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox averagingUnnRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel averagingUnnRadLabel;
        private Telerik.WinControls.UI.RadLabel confirmationMeasurementUnitsRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox confirmationMeasurementRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel confirmationMeasurementRadLabel;
        private Telerik.WinControls.UI.RadLabel hysterisisUnitsradLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox hysterisisRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel hysterisisRadLabel;
        private Telerik.WinControls.UI.RadDropDownList baudRateRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox measurementScheduleRadGroupBox;
        private Telerik.WinControls.UI.RadGridView measurementSettingsRadGridView;
        private Telerik.WinControls.UI.RadRadioButton byScheduleMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadLabel measurementScheduleMinuteRadLabel;
        private Telerik.WinControls.UI.RadLabel measurementScheduleHourRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox minuteMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox hourMeasurementScheduleRadMaskedEditBox;
        private Telerik.WinControls.UI.RadRadioButton stepMeasurementScheduleRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetTemperatureCoeffRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTemperatureCoeffAlarmThresholdRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTemperatrreCoeffAlarmRadLabel;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetTemperatureCoeffAlarmOffRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetTemperatureCoeffAlarmOnRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetTemperatureSensorRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList firstBushingSetTemperatureSensorRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetTransformerNameplateRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetCapacitancePhaseCRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetCapacitancePhaseBRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetCapacitancePhaseARadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetTangentPhaseCRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetTangentPhaseBRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetTangentPhaseARadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetCapacitancePhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetCapacitancePhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetCapacitancePhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTangentPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTangentPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTangentPhaseARadLabel;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetInputImpedenceRadGroupBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseCOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseBOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseAOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetPhaseARadLabel;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetThresholdsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetRateOfChangeRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetRateOfChangeAlarmThresholdRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetRateOfChangeAlarmRadLabel;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetRateOfChangeAlarmOffRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetRateOfChangeAlarmOnRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetAmplitudeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetAmplitudeWarningRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel firstBushingSetkVRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetAmpsRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetCurrentRadLabel;
        private Telerik.WinControls.UI.RadLabel firstBushingSetVoltageRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetCurrentRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadGroupBox monitoringRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton disableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadButton programDeviceFirstBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox firstBushingSetMeasurementsRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetDisableMeasurementsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton firstBushingSetEnableMeasurementsRadRadioButton;
        private Telerik.WinControls.UI.RadPageViewPage initialBalanceDataRadPageViewPage;
        private Telerik.WinControls.UI.RadButton programDeviceCommonParametersTabRadButton;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetDisableMeasurementsRadRadioButton;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetCapacitancePhaseCRadMaskedEditBox;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetEnableMeasurementsRadRadioButton;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetCapacitancePhaseBRadMaskedEditBox;
        private Telerik.WinControls.UI.RadDropDownList secondBushingSetTemperatureSensorRadDropDownList;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetCapacitancePhaseARadMaskedEditBox;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetMeasurementsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetTemperatureSensorRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetTransformerNameplateRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetTangentPhaseCRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetTangentPhaseBRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetTangentPhaseARadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetCapacitancePhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetCapacitancePhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetCapacitancePhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTangentPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTangentPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTangentPhaseARadLabel;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetInputImpedenceRadGroupBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseCOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseBOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseAOhmsRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetPhaseARadLabel;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetThresholdsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetRateOfChangeRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetRateOfChangeAlarmThresholdRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetRateOfChangeAlarmRadLabel;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetRateOfChangeAlarmOffRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetRateOfChangeAlarmOnRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetTemperatureCoeffRadGroupBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTemperatureCoeffAlarmThresholdRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTemperatureCoeffAlarmRadLabel;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetTemperatureCoeffAlarmOffRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton secondBushingSetTemperatureCoeffAlarmOnRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetAmplitudeRadGroupBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetAmplitudeAlarmRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetAmplitudeWarningRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetAmplitudeAlarmThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetAmplitudeWarningThresholdRadMaskedEditBox;
        private Telerik.WinControls.UI.RadGroupBox secondBushingSetSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetkVRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetAmpsRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetCurrentRadLabel;
        private Telerik.WinControls.UI.RadLabel secondBushingSetVoltageRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetCurrentRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetVoltageRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceCommonParametersTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceFirstBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceSecondBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceSecondBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadLabel firstBushingSetTemperatureAtMeasurementRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox firstBushingSetTemperatureAtMeasurementRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel secondBushingSetTemperatureAtMeasurementRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox secondBushingSetTemperatureAtMeasurementRadMaskedEditBox;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabTemperatureRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabImbalanceRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabImbalancePhaseRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ShiftPhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1AmplitudePhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1TemperatureValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ImbalancePhaseValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1ImbalanceValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseCRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabAmplitudePhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabShiftPhaseARadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet1RadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2RadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ShiftPhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseCValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBushingDataSet2InputSignalShiftPhaseBRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseBValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ShiftPhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2AmplitudePhaseAValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2TemperatureValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ImbalancePhaseValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabBushingSet2ImbalanceValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabMeasurementDateValueRadLabel;
        private Telerik.WinControls.UI.RadLabel initialBalanceDataTabMeasurementDateRadLabel;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceInitialBalanceDataTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceInitialBalanceDataTabRadButton;
        private Telerik.WinControls.UI.RadProgressBar commonParametersRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar firstBushingSetRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar secondBushingSetRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar initialBalanceDataRadProgressBar;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadSpinEditor firstBushingSetPhaseCRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor firstBushingSetPhaseBRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor firstBushingSetPhaseARadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor secondBushingSetPhaseCRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor secondBushingSetPhaseBRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor secondBushingSetPhaseARadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationCommonParametersTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadSelectedConfigurationCommonParametersTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsCommonParametersTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsFirstBushingSetTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadSelectedConfigurationFirstBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsFirstBushingSetTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsSecondBushingSetTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadSelectedConfigurationSecondBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsSecondBushingSetTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsInitialBalanceDataTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadSelectedConfigurationInitialBalanceDataTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsInitialBalanceDataTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton11;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileCommonParametersTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileCommonParametersTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileCommonParametersTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileFirstBushingSetTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileFirstBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileFirstBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileSecondBushingSetTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileSecondBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileSecondBushingSetTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox xmlConfigurationFileInitialBalanceDataTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton loadFromFileInitialBalanceDataTabRadButton;
        private Telerik.WinControls.UI.RadButton saveToFileInitialBalanceDataTabRadButton;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}

