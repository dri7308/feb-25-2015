﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;

namespace BushingConfigurationLite
{
    public partial class BHM_MonitorConfiguration
    {
        private static string errorInModbusAddressNonNumericalValuePresentText = "Error in ModBus address, non-numerical value is present";
        private static string errorModbusAddressValueIncorrect = "Error in ModBus address, value must be between 1 and 255 inclusive.";
        private static string errorNonNumericalValuePresentText = "Error: non-numerical value is present";

        private static string measurementNumberChannelNumberGridText = "<html><font=Microsoft Sans Serif><size=8.25>Measurement<br>Number</html>";
        private static string hourChannelNumberGridText = "<html><font=Microsoft Sans Serif><size=8.25>Hour</html>";
        private static string minuteChannelNumberGridText = "<html><font=Microsoft Sans Serif><size=8.25>Minute</html>";

        private void InitializeMeasurementSettingsRadGridView()
        {
            try
            {
                GridViewTextBoxColumn channelNumberGridViewTextBoxColumn = new GridViewTextBoxColumn();
                channelNumberGridViewTextBoxColumn.Name = "MeasurementNumber";
                channelNumberGridViewTextBoxColumn.HeaderText = measurementNumberChannelNumberGridText;
                channelNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                channelNumberGridViewTextBoxColumn.Width = 74;
                channelNumberGridViewTextBoxColumn.AllowSort = false;
                channelNumberGridViewTextBoxColumn.IsPinned = true;
                channelNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewDecimalColumn hourEntryGridViewDecimalColumn = new GridViewDecimalColumn();
                hourEntryGridViewDecimalColumn.Name = "HourEntry";
                hourEntryGridViewDecimalColumn.HeaderText = hourChannelNumberGridText;
                hourEntryGridViewDecimalColumn.DecimalPlaces = 0;
                hourEntryGridViewDecimalColumn.DisableHTMLRendering = false;
                hourEntryGridViewDecimalColumn.Width = 35;
                hourEntryGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn minuteEntryGridViewDecimalColumn = new GridViewDecimalColumn();
                minuteEntryGridViewDecimalColumn.Name = "MinuteEntry";
                minuteEntryGridViewDecimalColumn.HeaderText = minuteChannelNumberGridText;
                minuteEntryGridViewDecimalColumn.DecimalPlaces = 0;
                minuteEntryGridViewDecimalColumn.DisableHTMLRendering = false;
                minuteEntryGridViewDecimalColumn.Width = 40;
                minuteEntryGridViewDecimalColumn.AllowSort = false;

                this.measurementSettingsRadGridView.Columns.Add(channelNumberGridViewTextBoxColumn);
                this.measurementSettingsRadGridView.Columns.Add(hourEntryGridViewDecimalColumn);
                this.measurementSettingsRadGridView.Columns.Add(minuteEntryGridViewDecimalColumn);

                this.measurementSettingsRadGridView.TableElement.TableHeaderHeight = 30;
                this.measurementSettingsRadGridView.AllowColumnReorder = false;
                this.measurementSettingsRadGridView.AllowColumnChooser = false;
                this.measurementSettingsRadGridView.ShowGroupPanel = false;
                this.measurementSettingsRadGridView.EnableGrouping = false;
                this.measurementSettingsRadGridView.AllowAddNewRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeMeasurementSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToMeasurementSettingsGridView()
        {
            try
            {
                this.measurementSettingsRadGridView.Rows.Clear();
                for (int i = 0; i < 50; i++)
                {
                    this.measurementSettingsRadGridView.Rows.Add((i + 1), 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.FillMeasurementSettingsGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo> measurementsInfoList)
        {
            try
            {
                int count;
                int index = 0;
                GridViewRowInfo rowInfo;
                BHM_ConfigComponent_MeasurementsInfo measurementsInfo;
                if (measurementsInfoList != null)
                {
                    if (this.measurementSettingsRadGridView.RowCount == 50)
                    {
                        count = measurementsInfoList.Count;
                        if (count > 50)
                        {
                            count = 50;
                            string errorMessage = "Error in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> had more than 50 entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        else
                        {
                            for (index = 0; index < count; index++)
                            {
                                measurementsInfo = measurementsInfoList[index];
                                rowInfo = this.measurementSettingsRadGridView.Rows[index];
                                if (rowInfo.Cells.Count > 2)
                                {
                                    rowInfo.Cells[1].Value = measurementsInfo.Hour;
                                    rowInfo.Cells[2].Value = measurementsInfo.Minute;
                                }
                                else
                                {
                                    string errorMessage = "Error in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            for (; index < 50; index++)
                            {
                                rowInfo = this.measurementSettingsRadGridView.Rows[index];
                                if (rowInfo.Cells.Count > 2)
                                {
                                    rowInfo.Cells[1].Value = 0;
                                    rowInfo.Cells[2].Value = 0;
                                }
                                else
                                {
                                    string errorMessage = "Error in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nthis.measurementSettingsRadGridView did not have 50 rows.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.AddDataToMeasurementSettingsGridView(List<BHM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void FillMeasurementSettingsGridView()
//        {
//            try
//            {
//                string entryNumber = "0";
//                int hour = 0;
//                int minute = 0;
//                int numberOfNonEmptyEntries;
//                int index;
//                if (this.currentConfiguration != null)
//                {
//                    if (this.currentConfiguration.measurementsInfoList != null)
//                    {
//                        numberOfNonEmptyEntries = this.currentConfiguration.measurementsInfoList.Count;

//                        this.measurementSettingsRadGridView.Rows.Clear();

//                        for (index = 0; index < numberOfNonEmptyEntries; index++)
//                        {
//                            hour = this.currentConfiguration.measurementsInfoList[index].Hour;
//                            minute = this.currentConfiguration.measurementsInfoList[index].Minute;
//                            entryNumber = (index + 1).ToString();
//                            AddRowEntryToMeasurementSettings(entryNumber, hour, minute);
//                        }

//                        hour = 0;
//                        minute = 0;
//                        for (; index < 50; index++)
//                        {
//                            entryNumber = (index + 1).ToString();
//                            AddRowEntryToMeasurementSettings(entryNumber, hour, minute);
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.FillMeasurementSettingsGridView()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void AddRowEntryToMeasurementSettings(string entryNumber, int hour, int minute)
//        {
//            try
//            {
//                GridViewInfo info = new GridViewInfo(this.measurementSettingsRadGridView.MasterTemplate);
//                GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(info);

//                rowInfo.Cells[0].Value = entryNumber;
//                rowInfo.Cells[1].Value = hour;
//                rowInfo.Cells[2].Value = minute;

//                this.measurementSettingsRadGridView.Rows.Add(rowInfo);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.AddRowEntryToMeasurementSettings(string, int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void GetMeasurementSettingsFromRowEntry(ref BHM_ConfigComponent_MeasurementsInfo measurementsInfo, int zeroIndexMeasurementNumber)
        {
            try
            {
                GridViewInfo info = new GridViewInfo(this.measurementSettingsRadGridView.MasterTemplate);
                GridViewRowInfo rowInfo = null;
                int hour=0;
                int minute=0;

                if (measurementsInfo != null)
                {
                    if (this.measurementSettingsRadGridView.Rows.Count > zeroIndexMeasurementNumber)
                    {
                        rowInfo = this.measurementSettingsRadGridView.Rows[zeroIndexMeasurementNumber];
                        if (rowInfo.Cells.Count > 2)
                        {
                            if (rowInfo.Cells[1].Value != null)
                            {
                                Int32.TryParse(rowInfo.Cells[1].Value.ToString(), out hour);
                            }
                            if (rowInfo.Cells[2].Value != null)
                            {
                                Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out minute);
                            }

                            measurementsInfo.ItemNumber = zeroIndexMeasurementNumber;
                            measurementsInfo.Hour = hour;
                            measurementsInfo.Minute = minute;
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref BHM_Config_MeasurementsInfo, int)\nPrivate data this.measurementSettingsRadGridView had too cells for the given row.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref BHM_Config_MeasurementsInfo, int)\nPrivate data this.measurementSettingsRadGridView had too few rows.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref BHM_Config_MeasurementsInfo, int)\nInput BHM_Config_MeasurementsInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.GetMeasurementSettingsFromRowEntry(ref BHM_Config_MeasurementsInfo, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteMeasurementSettingsToConfigurationData()
        {
            try
            {
                List<BHM_ConfigComponent_MeasurementsInfo> allMeasurementsInfo = new List<BHM_ConfigComponent_MeasurementsInfo>();
                BHM_ConfigComponent_MeasurementsInfo singleMeasurementInfo;
                List<BHM_ConfigComponent_MeasurementsInfo> sortedMeasurementsInfo;
                int totalMeasurements;
                if (this.currentConfiguration != null)
                {

                    for (int i = 0; i < 50; i++)
                    {
                        singleMeasurementInfo = new BHM_ConfigComponent_MeasurementsInfo();
                        singleMeasurementInfo.ItemNumber = i;
                        GetMeasurementSettingsFromRowEntry(ref singleMeasurementInfo, i);
                        if ((singleMeasurementInfo.Hour > 0) || (singleMeasurementInfo.Minute > 0))
                        {
                            allMeasurementsInfo.Add(singleMeasurementInfo);
                        }
                    }

                    var measurementsQuery =
                          from item in allMeasurementsInfo
                          orderby item.Hour, item.Minute
                          select item;

                    sortedMeasurementsInfo = measurementsQuery.ToList();
                    totalMeasurements = sortedMeasurementsInfo.Count;
                    for (int i = 0; i < totalMeasurements; i++)
                    {
                        sortedMeasurementsInfo[i].ItemNumber = i + 1;
                    }

                    currentConfiguration.measurementsInfoList = sortedMeasurementsInfo;
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.TranslateMeasurementSettingsToConfigurationData()\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.TranslateMeasurementSettingsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToCommonParametersTabObjects(BHM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.gammaSetupInfo != null)
                    {
                        if (inputConfiguration.setupInfo != null)
                        {
                            int modBusAddress = inputConfiguration.setupInfo.DeviceNumber;
                            int hysterisis = inputConfiguration.gammaSetupInfo.AlarmHysteresis;
                            int confirmationMeasurement = inputConfiguration.gammaSetupInfo.ReReadOnAlarm;
                            int averagingUnn = inputConfiguration.gammaSetupInfo.AveragingForGamma;
                            int allowablePhaseDifference = inputConfiguration.gammaSetupInfo.AllowedPhaseDispersion;
                            int baudRateSelectedIndex = inputConfiguration.setupInfo.BaudRate;
                            int disableMonitoring = inputConfiguration.setupInfo.Stopped;

                            int rateOfChange = inputConfiguration.gammaSetupInfo.DaysToCalculateTrend;
                            int temperatureCoefficient = inputConfiguration.gammaSetupInfo.DaysToCalculateTCoefficient;
                            int tangent = inputConfiguration.gammaSetupInfo.MinDiagGamma;

                            int scheduleType = inputConfiguration.setupInfo.ScheduleType;
                            int hours = inputConfiguration.setupInfo.DTime_Hour;
                            int minutes = inputConfiguration.setupInfo.DTime_Min;

                            /// Basic Settings group box
                            modBusAddressRadMaskedEditBox.Text = modBusAddress.ToString();

                            hysterisisRadMaskedEditBox.Text = hysterisis.ToString();
                            confirmationMeasurementRadMaskedEditBox.Text = confirmationMeasurement.ToString();
                            averagingUnnRadMaskedEditBox.Text = averagingUnn.ToString();
                            allowablePhaseDifferenceRadMaskedEditBox.Text = allowablePhaseDifference.ToString();
                            baudRateRadDropDownList.SelectedIndex = baudRateSelectedIndex;

                            /// Days to calculate group box
                            rateOfChangeRadMaskedEditBox.Text = rateOfChange.ToString();
                            temperatureCoefficientRadMaskedEditBox.Text = temperatureCoefficient.ToString();
                            tangentRadMaskedEditBox.Text = tangent.ToString();

                            /// Monitoring group box
                            if (disableMonitoring == 0)
                            {
                                enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                disableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }

                            /// Measurement Schedule group box
                            if (scheduleType == 0)
                            {
                                stepMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                byScheduleMeasurementScheduleRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            hourMeasurementScheduleRadMaskedEditBox.Text = hours.ToString();
                            minuteMeasurementScheduleRadMaskedEditBox.Text = minutes.ToString();
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.InitializeCommonParametersTabObjects()\nPrivate data inputConfiguration.setupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.InitializeCommonParametersTabObjects()\nPrivate data inputConfiguration.gammaSetupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.InitializeCommonParametersTabObjects()\nPrivate data inputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeCommonParametersTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MissingValueInMeasurementSettingsRadGridView()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.measurementSettingsRadGridView.RowCount;
                int i, j;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.measurementSettingsRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    for (j = 0; j < cellCount; j++)
                    {
                        if (rowInfo.Cells[j].Value == null)
                        {
                            SelectCommonParametersTab();
                            rowInfo.Cells[j].IsSelected = true;
                            RadMessageBox.Show(this, emptyCellErrorMessageText);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.MissingValueInMeasurementSettingsRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private bool ErrorIsPresentInACommonParametersTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                int modBusAddress = 0;

                int hysterisis = 0;
                int confirmationMeasurement = 0;
                int averagingUnn = 0;
                int allowablePhaseDifference = 0;
                
                int rateOfChange = 0;
                int temperatureCoefficient = 0;
                int tangent = 0;

                int hours = 0;
                int minutes = 0;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.gammaSetupInfo != null)
                    {
                        if (this.currentConfiguration.setupInfo != null)
                        {
                            errorIsPresent = MissingValueInMeasurementSettingsRadGridView();

                            /// Basic Settings group box

                            if ((!errorIsPresent) && (!Int32.TryParse(modBusAddressRadMaskedEditBox.Text, out modBusAddress)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                modBusAddressRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorInModbusAddressNonNumericalValuePresentText);
                            }

                            if ((modBusAddress < 1) || (modBusAddress > 255))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                modBusAddressRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorModbusAddressValueIncorrect);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(hysterisisRadMaskedEditBox.Text, out hysterisis)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                hysterisisRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(confirmationMeasurementRadMaskedEditBox.Text, out confirmationMeasurement)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                confirmationMeasurementRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(averagingUnnRadMaskedEditBox.Text, out averagingUnn)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                averagingUnnRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(allowablePhaseDifferenceRadMaskedEditBox.Text, out allowablePhaseDifference)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                allowablePhaseDifferenceRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            /// Days to calculate group box

                            if ((!errorIsPresent) && (!Int32.TryParse(rateOfChangeRadMaskedEditBox.Text, out rateOfChange)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                rateOfChangeRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(temperatureCoefficientRadMaskedEditBox.Text, out temperatureCoefficient)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                temperatureCoefficientRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(tangentRadMaskedEditBox.Text, out tangent)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                tangentRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(hourMeasurementScheduleRadMaskedEditBox.Text, out hours)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                hourMeasurementScheduleRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }

                            if ((!errorIsPresent) && (!Int32.TryParse(minuteMeasurementScheduleRadMaskedEditBox.Text, out minutes)))
                            {
                                configurationError = true;
                                SelectCommonParametersTab();
                                minuteMeasurementScheduleRadMaskedEditBox.Select();
                                RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                            }
                           
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInACommonParametersTabObject()\nPrivate data this.currentConfiguration.setupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInACommonParametersTabObject()\nPrivate data this.currentConfiguration.gammaSetupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInACommonParametersTabObject()\nPrivate data this.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ErrorIsPresentInACommonParametersTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteCommonParametersTabObjectsToConfigurationData()
        {
            try
            {
                int modBusAddress = 0;

                int hysterisis = 0;
                int confirmationMeasurement = 0;
                int averagingUnn = 0;
                int allowablePhaseDifference = 0;
                int baudRateSelectedIndex = 0;

                int disableMonitoring = 0;

                int rateOfChange = 0;
                int temperatureCoefficient = 0;
                int tangent = 0;

                int scheduleType = 0;
                int hours = 0;
                int minutes = 0;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.gammaSetupInfo != null)
                    {
                        if (this.currentConfiguration.setupInfo != null)
                        {
                            /// Basic Settings group box

                            Int32.TryParse(modBusAddressRadMaskedEditBox.Text, out modBusAddress);
                            Int32.TryParse(hysterisisRadMaskedEditBox.Text, out hysterisis);
                            Int32.TryParse(confirmationMeasurementRadMaskedEditBox.Text, out confirmationMeasurement);
                            Int32.TryParse(averagingUnnRadMaskedEditBox.Text, out averagingUnn);
                            Int32.TryParse(allowablePhaseDifferenceRadMaskedEditBox.Text, out allowablePhaseDifference);

                            baudRateSelectedIndex = baudRateRadDropDownList.SelectedIndex;

                            /// Days to calculate group box
                            Int32.TryParse(rateOfChangeRadMaskedEditBox.Text, out rateOfChange);
                            Int32.TryParse(temperatureCoefficientRadMaskedEditBox.Text, out temperatureCoefficient);
                            Int32.TryParse(tangentRadMaskedEditBox.Text, out tangent);

                            /// Monitoring group box                            
                            if (disableMonitoringRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                disableMonitoring = 1;
                            }

                            /// Measurement Schedule group box
                            if (byScheduleMeasurementScheduleRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                scheduleType = 1;
                            }

                            Int32.TryParse(hourMeasurementScheduleRadMaskedEditBox.Text, out hours);
                            Int32.TryParse(minuteMeasurementScheduleRadMaskedEditBox.Text, out minutes);

                            currentConfiguration.setupInfo.DeviceNumber = modBusAddress;
                            currentConfiguration.gammaSetupInfo.AlarmHysteresis = hysterisis;
                            currentConfiguration.gammaSetupInfo.ReReadOnAlarm = confirmationMeasurement;
                            currentConfiguration.gammaSetupInfo.AveragingForGamma = averagingUnn;
                            currentConfiguration.gammaSetupInfo.AllowedPhaseDispersion = allowablePhaseDifference;
                            currentConfiguration.setupInfo.BaudRate = baudRateSelectedIndex;
                            currentConfiguration.setupInfo.Stopped = disableMonitoring;

                            currentConfiguration.gammaSetupInfo.DaysToCalculateTrend = rateOfChange;
                            currentConfiguration.gammaSetupInfo.DaysToCalculateTCoefficient = temperatureCoefficient;
                            currentConfiguration.gammaSetupInfo.MinDiagGamma = tangent;

                            currentConfiguration.setupInfo.ScheduleType = scheduleType;
                            currentConfiguration.setupInfo.DTime_Hour = hours;
                            currentConfiguration.setupInfo.DTime_Min = minutes;
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_MonitorConfiguration.WriteCommonParametersTabObjectsToConfigurationData()\nPrivate data this.currentConfiguration.setupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteCommonParametersTabObjectsToConfigurationData()\nPrivate data this.currentConfiguration.gammaSetupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteCommonParametersTabObjectsToConfigurationData()\nPrivate data this.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteCommonParametersTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableCommonParametersTabObjects()
        {
            modBusAddressRadMaskedEditBox.ReadOnly = true;
            hysterisisRadMaskedEditBox.ReadOnly = true;
            confirmationMeasurementRadMaskedEditBox.ReadOnly = true;
            averagingUnnRadMaskedEditBox.ReadOnly = true;
            allowablePhaseDifferenceRadMaskedEditBox.ReadOnly = true;
            baudRateRadDropDownList.Enabled = false;
            enableMonitoringRadRadioButton.Enabled = false;
            disableMonitoringRadRadioButton.Enabled = false;
            rateOfChangeRadMaskedEditBox.ReadOnly = true;
            temperatureCoefficientRadMaskedEditBox.ReadOnly = true;
            tangentRadMaskedEditBox.ReadOnly = true;
            stepMeasurementScheduleRadRadioButton.Enabled = false;
            byScheduleMeasurementScheduleRadRadioButton.Enabled = false;
            hourMeasurementScheduleRadMaskedEditBox.ReadOnly = true;
            minuteMeasurementScheduleRadMaskedEditBox.ReadOnly = true;
            measurementSettingsRadGridView.ReadOnly = true;
        }

        private void EnableCommonParametersTabObjects()
        {
            modBusAddressRadMaskedEditBox.ReadOnly = false;
            hysterisisRadMaskedEditBox.ReadOnly = false;
            confirmationMeasurementRadMaskedEditBox.ReadOnly = false;
            averagingUnnRadMaskedEditBox.ReadOnly = false;
            allowablePhaseDifferenceRadMaskedEditBox.ReadOnly = false;
            baudRateRadDropDownList.Enabled = true;
            enableMonitoringRadRadioButton.Enabled = true;
            disableMonitoringRadRadioButton.Enabled = true;
            rateOfChangeRadMaskedEditBox.ReadOnly = false;
            temperatureCoefficientRadMaskedEditBox.ReadOnly = false;
            tangentRadMaskedEditBox.ReadOnly = false;
            stepMeasurementScheduleRadRadioButton.Enabled = true;
            byScheduleMeasurementScheduleRadRadioButton.Enabled = true;
            hourMeasurementScheduleRadMaskedEditBox.ReadOnly = false;
            minuteMeasurementScheduleRadMaskedEditBox.ReadOnly = false;
            measurementSettingsRadGridView.ReadOnly = false;
        }

        private void SelectCommonParametersTab()
        {
            configurationRadPageView.SelectedPage = commonParametersRadPageViewPage;
        }

        private void stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                SetStepMeasurementScheduleEnableState();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.stepMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void byScheduleMeasurementScheduleRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                SetByScheduleMeasurementScheduleEnableState();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.byScheduleMeasurementScheduleRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetStepMeasurementScheduleEnableState()
        {
            hourMeasurementScheduleRadMaskedEditBox.Enabled = true;
            minuteMeasurementScheduleRadMaskedEditBox.Enabled = true;
            measurementScheduleHourRadLabel.Enabled = true;
            measurementScheduleMinuteRadLabel.Enabled = true;
            measurementSettingsRadGridView.Enabled = false;
        }

        private void SetByScheduleMeasurementScheduleEnableState()
        {
            measurementSettingsRadGridView.Enabled = true;
            hourMeasurementScheduleRadMaskedEditBox.Enabled = false;
            minuteMeasurementScheduleRadMaskedEditBox.Enabled = false;
            measurementScheduleHourRadLabel.Enabled = false;
            measurementScheduleMinuteRadLabel.Enabled = false;
        }
    }
}
