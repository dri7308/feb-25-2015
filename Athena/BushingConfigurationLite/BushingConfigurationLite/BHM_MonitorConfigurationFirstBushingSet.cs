﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;

namespace BushingConfigurationLite
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {

        private void WriteDataToFirstBushingSetTabObjects(BHM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.gammaSideSetupSideOne != null)
                    {
                        /// derived from other variables
                        int enableMeasurements = 0;
                        int temperatureCoefficientAlarmEnable = 0;
                        int rateOfUnnChangealarmEnable = 0;

                        double voltage = inputConfiguration.gammaSideSetupSideOne.RatedVoltage;
                        double current = inputConfiguration.gammaSideSetupSideOne.RatedCurrent;
                        double phaseA = inputConfiguration.gammaSideSetupSideOne.InputImpedance_0 / 100.0;
                        double phaseB = inputConfiguration.gammaSideSetupSideOne.InputImpedance_1 / 100.0;
                        double phaseC = inputConfiguration.gammaSideSetupSideOne.InputImpedance_2 / 100.0;

                        double amplitudeUnnWarningThresholdValue = inputConfiguration.gammaSideSetupSideOne.GammaYellowThreshold / 20.0;
                        double amplitudeUnnAlarmThresholdValue = inputConfiguration.gammaSideSetupSideOne.GammaRedThreshold / 20.0;

                        double temperatureCoefficientAlarmThresholdValue = inputConfiguration.gammaSideSetupSideOne.TempCoefficient / 500.0;
                        double rateOfUnnChangeAlarmThresholdValue = inputConfiguration.gammaSideSetupSideOne.TrendAlarm / 5.0;

                        double tangentPhaseA = inputConfiguration.gammaSideSetupSideOne.Tg0_0 / 100.0;
                        double tangentPhaseB = inputConfiguration.gammaSideSetupSideOne.Tg0_1 / 100.0;
                        double tangentPhaseC = inputConfiguration.gammaSideSetupSideOne.Tg0_2 / 100.0;
                        double capacitancePhaseA = inputConfiguration.gammaSideSetupSideOne.C0_0 / 10.0;
                        double capacitancePhaseB = inputConfiguration.gammaSideSetupSideOne.C0_1 / 10.0;
                        double capacitancePhaseC = inputConfiguration.gammaSideSetupSideOne.C0_2 / 10.0;

                        int temperatureAtMeasurement = inputConfiguration.gammaSideSetupSideOne.Temperature0 - 70;
                        int temperatureSensor = inputConfiguration.gammaSideSetupSideOne.TemperatureConfig;

                        int alarmEnable = inputConfiguration.gammaSideSetupSideOne.AlarmEnable;
                        int readOnSide = inputConfiguration.gammaSetupInfo.ReadOnSide;

                        if ((readOnSide == 1) || (readOnSide == 3))
                        {
                            enableMeasurements = 1;
                        }

                        if (alarmEnable == 3)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                        }
                        else if (alarmEnable == 5)
                        {
                            rateOfUnnChangealarmEnable = 1;
                        }
                        else if (alarmEnable == 7)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                            rateOfUnnChangealarmEnable = 1;
                        }

                        if (enableMeasurements == 0)
                        {
                            firstBushingSetDisableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            firstBushingSetEnableMeasurementsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        firstBushingSetVoltageRadMaskedEditBox.Text = Math.Round(voltage, 2).ToString();
                        firstBushingSetCurrentRadMaskedEditBox.Text = Math.Round(current, 2).ToString();
                        firstBushingSetPhaseARadSpinEditor.Value = (Decimal)Math.Round(phaseA, 2);
                        firstBushingSetPhaseBRadSpinEditor.Value = (Decimal)Math.Round(phaseB, 2);
                        firstBushingSetPhaseCRadSpinEditor.Value = (Decimal)Math.Round(phaseC, 2);

                        firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text = Math.Round(amplitudeUnnWarningThresholdValue, 1).ToString();
                        firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text = Math.Round(amplitudeUnnAlarmThresholdValue, 1).ToString();

                        if (temperatureCoefficientAlarmEnable == 0)
                        {
                            firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text = Math.Round(temperatureCoefficientAlarmThresholdValue, 3).ToString();

                        if (rateOfUnnChangealarmEnable == 0)
                        {
                            firstBushingSetRateOfChangeAlarmOffRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        else
                        {
                            firstBushingSetRateOfChangeAlarmOnRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                        }
                        firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text = rateOfUnnChangeAlarmThresholdValue.ToString();

                        firstBushingSetTangentPhaseARadMaskedEditBox.Text = Math.Round(tangentPhaseA, 2).ToString();
                        firstBushingSetTangentPhaseBRadMaskedEditBox.Text = Math.Round(tangentPhaseB, 2).ToString();
                        firstBushingSetTangentPhaseCRadMaskedEditBox.Text = Math.Round(tangentPhaseC, 2).ToString();

                        firstBushingSetCapacitancePhaseARadMaskedEditBox.Text = Math.Round(capacitancePhaseA, 1).ToString();
                        firstBushingSetCapacitancePhaseBRadMaskedEditBox.Text = Math.Round(capacitancePhaseB, 1).ToString();
                        firstBushingSetCapacitancePhaseCRadMaskedEditBox.Text = Math.Round(capacitancePhaseC, 1).ToString();

                        firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text = temperatureAtMeasurement.ToString();

                        firstBushingSetTemperatureSensorRadDropDownList.SelectedIndex = temperatureSensor;
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.InitializeFirstBushingSetTabObjects()\nPrivate data this.inputConfiguration.gammaSideSetupSideOne was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.InitializeFirstBushingSetTabObjects()\nPrivate data this.inputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeFirstBushingSetTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInFirstBushingSetTabObjects()
        {
            bool errorIsPresent = false;
            try
            {
                if (currentConfiguration != null)
                {
                    if (currentConfiguration.gammaSideSetupSideOne != null)
                    {
                        double voltage = 0.0;
                        double current = 0.0;

                        double amplitudeUnnWarningThresholdValue = 0.0;
                        double amplitudeUnnAlarmThresholdValue = 0.0;

                        double temperatureCoefficientAlarmThresholdValue = 0.0;
                        double rateOfUnnChangeAlarmThresholdValue = 0.0;

                        double tangentPhaseA = 0.0;
                        double tangentPhaseB = 0.0;
                        double tangentPhaseC = 0.0;
                        double capacitancePhaseA = 0.0;
                        double capacitancePhaseB = 0.0;
                        double capacitancePhaseC = 0.0;
                        int temperatureAtMeasurement = 0;

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetVoltageRadMaskedEditBox.Text, out voltage)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetVoltageRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetCurrentRadMaskedEditBox.Text, out current)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetCurrentRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }                    

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text, out amplitudeUnnWarningThresholdValue)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text, out amplitudeUnnAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text, out temperatureCoefficientAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text, out rateOfUnnChangeAlarmThresholdValue)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetTangentPhaseARadMaskedEditBox.Text, out tangentPhaseA)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetTangentPhaseARadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetTangentPhaseBRadMaskedEditBox.Text, out tangentPhaseB)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetTangentPhaseBRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetTangentPhaseCRadMaskedEditBox.Text, out tangentPhaseC)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetTangentPhaseCRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetCapacitancePhaseARadMaskedEditBox.Text, out capacitancePhaseA)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetCapacitancePhaseARadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetCapacitancePhaseBRadMaskedEditBox.Text, out capacitancePhaseB)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetCapacitancePhaseBRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Double.TryParse(firstBushingSetCapacitancePhaseCRadMaskedEditBox.Text, out capacitancePhaseC)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetCapacitancePhaseCRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }

                        if ((!errorIsPresent) && (!Int32.TryParse(firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text, out temperatureAtMeasurement)))
                        {
                            configurationError = true;
                            SelectFirstBushingSetTab();
                            firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Select();
                            RadMessageBox.Show(this, errorNonNumericalValuePresentText);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInFirstBushingSetTabObjects()\nPrivate data this.currentConfiguration.gammaSideSetupSideOne was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.ErrorIsPresentInFirstBushingSetTabObjects()\nPrivate data this.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ErrorIsPresentInFirstBushingSetTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteFirstBushingSetTabObjectsToConfigurationData()
        {
            try
            {
                if (currentConfiguration != null)
                {
                    if (currentConfiguration.gammaSideSetupSideOne != null)
                    {
                        int alarmEnable = 1;
                        int temperatureCoefficientAlarmEnable = 0;
                        int rateOfUnnChangeEnable = 0;

                        double voltage = 0.0;
                        double current = 0.0;
                        double phaseA = 0.0;
                        double phaseB = 0.0;
                        double phaseC = 0.0;

                        double amplitudeUnnWarningThresholdValue = 0.0;
                        double amplitudeUnnAlarmThresholdValue = 0.0;

                        double temperatureCoefficientAlarmThresholdValue = 0.0;
                        double rateOfUnnChangeAlarmThresholdValue = 0.0;

                        double tangentPhaseA = 0.0;
                        double tangentPhaseB = 0.0;
                        double tangentPhaseC = 0.0;
                        double capacitancePhaseA = 0.0;
                        double capacitancePhaseB = 0.0;
                        double capacitancePhaseC = 0.0;
                        int temperatureAtMeasurement = 0;
                        int temperatureSensor = 0;

                        Double.TryParse(firstBushingSetVoltageRadMaskedEditBox.Text, out voltage);
                        Double.TryParse(firstBushingSetCurrentRadMaskedEditBox.Text, out current);
                        phaseA = (Double)firstBushingSetPhaseARadSpinEditor.Value;
                        phaseB = (Double)firstBushingSetPhaseBRadSpinEditor.Value;
                        phaseC = (Double)firstBushingSetPhaseCRadSpinEditor.Value;
                        Double.TryParse(firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.Text, out amplitudeUnnWarningThresholdValue);
                        Double.TryParse(firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.Text, out amplitudeUnnAlarmThresholdValue);

                        if (firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            temperatureCoefficientAlarmEnable = 1;
                        }

                        if (firstBushingSetRateOfChangeAlarmOnRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            rateOfUnnChangeEnable = 1;
                        }

                        Double.TryParse(firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.Text, out temperatureCoefficientAlarmThresholdValue);
                        Double.TryParse(firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.Text, out rateOfUnnChangeAlarmThresholdValue);
                        Double.TryParse(firstBushingSetTangentPhaseARadMaskedEditBox.Text, out tangentPhaseA);
                        Double.TryParse(firstBushingSetTangentPhaseBRadMaskedEditBox.Text, out tangentPhaseB);
                        Double.TryParse(firstBushingSetTangentPhaseCRadMaskedEditBox.Text, out tangentPhaseC);
                        Double.TryParse(firstBushingSetCapacitancePhaseARadMaskedEditBox.Text, out capacitancePhaseA);
                        Double.TryParse(firstBushingSetCapacitancePhaseBRadMaskedEditBox.Text, out capacitancePhaseB);
                        Double.TryParse(firstBushingSetCapacitancePhaseCRadMaskedEditBox.Text, out capacitancePhaseC);

                        Int32.TryParse(firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.Text, out temperatureAtMeasurement);

                        temperatureSensor = firstBushingSetTemperatureSensorRadDropDownList.SelectedIndex;

                        /// assign the values to the database object

                        if ((temperatureCoefficientAlarmEnable == 1) && (rateOfUnnChangeEnable == 1))
                        {
                            alarmEnable = 7;
                        }
                        else if (temperatureCoefficientAlarmEnable == 1)
                        {
                            alarmEnable = 3;
                        }
                        else if (rateOfUnnChangeEnable == 1)
                        {
                            alarmEnable = 5;
                        }

                        currentConfiguration.gammaSideSetupSideOne.AlarmEnable = alarmEnable;

                        currentConfiguration.gammaSideSetupSideOne.RatedVoltage = voltage;
                        currentConfiguration.gammaSideSetupSideOne.RatedCurrent = current;
                        currentConfiguration.gammaSideSetupSideOne.InputImpedance_0 = (Int32)Math.Round((phaseA * 100), 0);
                        currentConfiguration.gammaSideSetupSideOne.InputImpedance_1 = (Int32)Math.Round((phaseB * 100), 0);
                        currentConfiguration.gammaSideSetupSideOne.InputImpedance_2 = (Int32)Math.Round((phaseC * 100), 0);

                        currentConfiguration.gammaSideSetupSideOne.GammaYellowThreshold = (Int32)Math.Round((amplitudeUnnWarningThresholdValue * 20.0), 0);
                        currentConfiguration.gammaSideSetupSideOne.GammaRedThreshold = (Int32)Math.Round((amplitudeUnnAlarmThresholdValue * 20.0), 0);

                        currentConfiguration.gammaSideSetupSideOne.TempCoefficient = (Int32)Math.Round((temperatureCoefficientAlarmThresholdValue * 500.0), 0);
                        currentConfiguration.gammaSideSetupSideOne.TrendAlarm = (Int32)Math.Round((rateOfUnnChangeAlarmThresholdValue * 5.0), 0);

                        currentConfiguration.gammaSideSetupSideOne.Tg0_0 = (Int32)Math.Round((tangentPhaseA * 100), 0);
                        currentConfiguration.gammaSideSetupSideOne.Tg0_1 = (Int32)Math.Round((tangentPhaseB * 100), 0);
                        currentConfiguration.gammaSideSetupSideOne.Tg0_2 = (Int32)Math.Round((tangentPhaseC * 100), 0);

                        currentConfiguration.gammaSideSetupSideOne.C0_0 = (Int32)Math.Round((capacitancePhaseA * 10), 0);
                        currentConfiguration.gammaSideSetupSideOne.C0_1 = (Int32)Math.Round((capacitancePhaseB * 10), 0);
                        currentConfiguration.gammaSideSetupSideOne.C0_2 = (Int32)Math.Round((capacitancePhaseC * 10), 0);

                        currentConfiguration.gammaSideSetupSideOne.Temperature0 = temperatureAtMeasurement + 70;
                        currentConfiguration.gammaSideSetupSideOne.TemperatureConfig = firstBushingSetTemperatureSensorRadDropDownList.SelectedIndex;
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteFirstBushingSetTabObjectsToConfigurationData()\nPrivate data this.currentConfiguration.gammaSideSetupSideOne was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteFirstBushingSetTabObjectsToConfigurationData()\nPrivate data this.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteFirstBushingSetTabObjectsToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableFirstBushingSetTabObjects()
        {
            firstBushingSetEnableMeasurementsRadRadioButton.Enabled = false;
            firstBushingSetDisableMeasurementsRadRadioButton.Enabled = false;
            firstBushingSetVoltageRadMaskedEditBox.ReadOnly = true;
            firstBushingSetCurrentRadMaskedEditBox.ReadOnly = true;
            firstBushingSetPhaseARadSpinEditor.ReadOnly = true;
            firstBushingSetPhaseBRadSpinEditor.ReadOnly = true;
            firstBushingSetPhaseCRadSpinEditor.ReadOnly = true;
            firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.ReadOnly = true;
            firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Enabled = false;
            firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Enabled = false;
            firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            firstBushingSetRateOfChangeAlarmOnRadRadioButton.Enabled = false;
            firstBushingSetRateOfChangeAlarmOffRadRadioButton.Enabled = false;
            firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.ReadOnly = true;
            firstBushingSetTangentPhaseARadMaskedEditBox.ReadOnly = true;
            firstBushingSetTangentPhaseBRadMaskedEditBox.ReadOnly = true;
            firstBushingSetTangentPhaseCRadMaskedEditBox.ReadOnly = true;
            firstBushingSetCapacitancePhaseARadMaskedEditBox.ReadOnly = true;
            firstBushingSetCapacitancePhaseBRadMaskedEditBox.ReadOnly = true;
            firstBushingSetCapacitancePhaseCRadMaskedEditBox.ReadOnly = true;
            firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.ReadOnly = true;
            firstBushingSetTemperatureSensorRadDropDownList.Enabled = false;
        }

        private void EnableFirstBushingSetTabObjects()
        {
            firstBushingSetEnableMeasurementsRadRadioButton.Enabled = true;
            firstBushingSetDisableMeasurementsRadRadioButton.Enabled = true;
            firstBushingSetVoltageRadMaskedEditBox.ReadOnly = false;
            firstBushingSetCurrentRadMaskedEditBox.ReadOnly = false;
            firstBushingSetPhaseARadSpinEditor.ReadOnly = false;
            firstBushingSetPhaseBRadSpinEditor.ReadOnly = false;
            firstBushingSetPhaseCRadSpinEditor.ReadOnly = false;
            firstBushingSetAmplitudeWarningThresholdRadMaskedEditBox.ReadOnly = false;
            firstBushingSetAmplitudeAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Enabled = true;
            firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Enabled = true;
            firstBushingSetTemperatureCoefficientAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            firstBushingSetRateOfChangeAlarmOnRadRadioButton.Enabled = true;
            firstBushingSetRateOfChangeAlarmOffRadRadioButton.Enabled = true;
            firstBushingSetRateOfChangeAlarmThresholdRadMaskedEditBox.ReadOnly = false;
            firstBushingSetTangentPhaseARadMaskedEditBox.ReadOnly = false;
            firstBushingSetTangentPhaseBRadMaskedEditBox.ReadOnly = false;
            firstBushingSetTangentPhaseCRadMaskedEditBox.ReadOnly = false;
            firstBushingSetCapacitancePhaseARadMaskedEditBox.ReadOnly = false;
            firstBushingSetCapacitancePhaseBRadMaskedEditBox.ReadOnly = false;
            firstBushingSetCapacitancePhaseCRadMaskedEditBox.ReadOnly = false;
            firstBushingSetTemperatureAtMeasurementRadMaskedEditBox.ReadOnly = false;
            firstBushingSetTemperatureSensorRadDropDownList.Enabled = true;
        }

    }
}
