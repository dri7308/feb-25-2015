using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using DatabaseInterface;
using FormatConversion;
using MonitorInterface;
using ConfigurationObjects;

namespace BushingConfigurationLite
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private BHM_Configuration currentConfiguration;
        private BHM_Configuration uneditedCurrentConfiguration;

        bool configurationError;

        private int modBusAddress;

        private static string emptyCellErrorMessageText = "There is a value missing from the grid.";
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        //private static string couldNotFindCurrentDeviceConfigurationDisplayingMostRecentText = "Could not find the current device configuration, displaying most recent configuration saved.";
        //private static string noDeviceConfigurationsInDatabaseText = "Could not find any device configurations in the database.";
        //private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
        //private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";
        private static string changesToCurrentConfigurationNotSavedWarningText = "You have made changes to the configuration but haven't loaded it onto the device.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";

        //private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";


        // strings holding text for interface objects
        //private static string htmlPrefix = "<html>";
        //private static string htmlSuffis = "</html>";
        //private static string htmlFontType = "<font=Microsoft Sans Serif>";
        //private static string htmlStandardFontSize = "<size=8.25>";

        //private static string bhmMonitorConfigurationInterfaceTitleText = "Bushing Health Monitor Configuration";

        //private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        //private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Working Copy<br>of Configuration<br>to Database</html>";
        //private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        //private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        //private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device<br>with Working Copy<br>of Configuration</html>";
        //private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
        //private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
        //private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        //private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        //private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        //private static string currentRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        //private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        //private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";

        //private static string commonParametersRadPageViewPageText = "<html><font=Microsoft Sans Serif>Common Parameters</html>";
        //private static string basicSettingsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Basic Settings</html>";
        //private static string modbusAddressRadLabelText = "<html><font=Microsoft Sans Serif>ModBus Address</html>";
        //private static string hysterisisRadLabelText = "<html><font=Microsoft Sans Serif>Hysterisis</html>";
        //private static string hysterisisUnitsradLabelText = "<html><font=Microsoft Sans Serif>%</html>";
        //private static string confirmationMeasurementRadLabelText = "<html><font=Microsoft Sans Serif>Confirmation<br>Measurement</html>";
        //private static string confirmationMeasurementUnitsRadLabelText = "<html><font=Microsoft Sans Serif>Minutes</html>";
        //private static string averagingUnnRadLabelText = "<html><font=Microsoft Sans Serif>Averaging Unn</html>";
        //private static string allowablePhaseDifferenceRadLabelText = "<html><font=Microsoft Sans Serif>Allowable Phase<br>Difference</html>";
        //private static string allowablePhaseDifferenceUnitsRadLabelText = "<html><font=Microsoft Sans Serif>Degrees</html>";
        //private static string baudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        //private static string monitoringRadGroupBoxText = "<html><font=Microsoft Sans Serif>Monitoring</html>";
        //private static string enableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Enable</html>";
        //private static string disableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Disable</html>";
        //private static string daysToComputeRadGroupBoxText = "<html><font=Microsoft Sans Serif>Days to calculate</html>";
        //private static string trendRadLabelText = "<html><font=Microsoft Sans Serif>Trend</html>";
        //private static string temperatureCoefficientRadLabelText = "<html><font=Microsoft Sans Serif>Temperature</html>";
        //private static string tangentRadLabelText = "<html><font=Microsoft Sans Serif>Tangent</html>";
        //private static string measurementScheduleRadGroupBoxText = "<html><font=Microsoft Sans Serif>Measurement Schedule</html>";
        //private static string stepMeasurementScheduleRadRadioButtonText = "<html><font=Microsoft Sans Serif>Every</html>";
        //private static string measurementScheduleHourRadLabelText = "<html><font=Microsoft Sans Serif></html>";
        //private static string measurementScheduleMinuteRadLabelText = "<html><font=Microsoft Sans Serif></html>";
        //private static string byScheduleMeasurementScheduleRadRadioButtonText = "<html><font=Microsoft Sans Serif>By Schedule</html>";

        //private static string firstBushingSetRadPageViewPageText = "First Bushing Set";
        //private static string secondBushingSetRadPageViewPageText = "Second Bushing Set";

        // strings common to both bushing sets
        //private static string bushingSetMeasurementsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Measurements</html>";
        //private static string bushingSetEnableMeasurementsRadRadioButtonText = "<html><font=Microsoft Sans Serif>Enable</html>";
        //private static string bushingSetDisableMeasurementsRadRadioButtonText = "<html><font=Microsoft Sans Serif>Disable</html>";
        //private static string bushingSetSettingsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Settings</html>";
        //private static string bushingSetVoltageRadLabelText = "<html><font=Microsoft Sans Serif>Voltage</html>";
        //private static string bushingSetkVRadLabelText = "<html><font=Microsoft Sans Serif>kV</html>";
        //private static string bushingSetCurrentRadLabelText = "<html><font=Microsoft Sans Serif>Current</html>";
        //private static string bushingSetAmpsRadLabelText = "<html><font=Microsoft Sans Serif>Amps</html>";
        //private static string bushingSetInputImpedenceRadGroupBoxText = "<html><font=Microsoft Sans Serif>Input Impedence</html>";
        //private static string bushingSetPhaseARadLabelText = "<html><font=Microsoft Sans Serif>Phase A</html>";
        //private static string bushingSetPhaseAOhmsRadLabelText = "<html><font=Microsoft Sans Serif>Ohms</html>";
        //private static string bushingSetPhaseBRadLabelText = "<html><font=Microsoft Sans Serif>Phase B</html>";
        //private static string bushingSetPhaseBOhmsRadLabelText = "<html><font=Microsoft Sans Serif>Ohms</html>";
        //private static string bushingSetPhaseCRadLabelText = "<html><font=Microsoft Sans Serif>Phase C</html>";
        //private static string bushingSetPhaseCOhmsRadLabelText = "<html><font=Microsoft Sans Serif>Ohms</html>";
        //private static string bushingSetThresholdsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Thresholds</html>";
        //private static string bushingSetAmplitudeRadGroupBoxText = "<html><font=Microsoft Sans Serif>Amplitude Unn</html>";
        //private static string bushingSetAmplitudeWarningRadLabelText = "<html><font=Microsoft Sans Serif>Warning<br>Threshold<br>Value</html>";
        //private static string bushingSetAmplitudeAlarmRadLabelText = "<html><font=Microsoft Sans Serif>Alarm<br>Threshold<br>Value</html>";
        //private static string bushingSetTemperatureCoeffRadGroupBoxText = "<html><font=Microsoft Sans Serif>Temperature Coefficient</html>";
        //private static string bushingSetTemperatureCoeffAlarmRadLabelText = "<html><font=Microsoft Sans Serif>Alarm</html>";
        //private static string bushingSetTemperatureCoeffAlarmOnRadRadioButtonText = "<html><font=Microsoft Sans Serif>On</html>";
        //private static string bushingSetTemperatureCoeffAlarmOffRadRadioButtonText = "<html><font=Microsoft Sans Serif>Off</html>";
        //private static string bushingSetTemperatureCoeffAlarmThresholdRadLabelText = "<html><font=Microsoft Sans Serif>Alarm<br>Threshold<br>Value";
        //private static string bushingSetRateOfChangeRadGroupBoxText = "<html><font=Microsoft Sans Serif>Rate of Unn Change";
        //private static string bushingSetRateOfChangeAlarmRadLabelText = "<html><font=Microsoft Sans Serif>Alarm";
        //private static string bushingSetRateOfChangeAlarmOnRadRadioButtonText = "<html><font=Microsoft Sans Serif>On";
        //private static string bushingSetRateOfChangeAlarmOffRadRadioButtonText = "<html><font=Microsoft Sans Serif>Off";
        //private static string bushingSetRateOfChangeAlarmThresholdRadLabelText = "<html><font=Microsoft Sans Serif>Alarm<br>Threshold<br>Value";
        //private static string bushingSetTransformerNameplateRadGroupBoxText = "<html><font=Microsoft Sans Serif>Transformer Nameplate Data";
        //private static string bushingSetTangentPhaseARadLabelText = "<html><font=Microsoft Sans Serif>Tangent Phase A";
        //private static string bushingSetTangentPhaseBRadLabelText = "<html><font=Microsoft Sans Serif>Tangent Phase B";
        //private static string bushingSetTangentPhaseCRadLabelText = "<html><font=Microsoft Sans Serif>Tangent Phase C";
        //private static string bushingSetCapacitancePhaseARadLabelText = "<html><font=Microsoft Sans Serif>Capacitance Phase A";
        //private static string bushingSetCapacitancePhaseBRadLabelText = "<html><font=Microsoft Sans Serif>Capacitance Phase B";
        //private static string bushingSetCapacitancePhaseCRadLabelText = "<html><font=Microsoft Sans Serif>Capacitance Phase C";
        //private static string bushingSetTemperatureAtMeasurementRadLabelText = "<html><font=Microsoft Sans Serif>Temperature at Measurement";
        //private static string bushingSetTemperatureSensorRadGroupBoxText = "<html><font=Microsoft Sans Serif>Temperature Sensor";

        //private static string initialBalanceDataRadPageViewPageText = "Initial Balance Data";
        //private static string initialBalanceDataTabMeasurementDateRadLabelText = "Measurement Date:";
        //private static string initialBalanceDataTabMeasurementDateValueRadLabelText = "";
        //private static string initialBalanceDataTabBushingSet1RadLabelText = "Bushing Set 1";
        //private static string initialBalanceDataTabBushingSet2RadLabelText = "Bushing Set 2";
        //private static string initialBalanceDataTabImbalanceRadLabelText = "Imbalance (%)";
        //private static string initialBalanceDataTabImbalancePhaseRadLabelText = "Imbalance Phase, gradient";
        //private static string initialBalanceDataTabTemperatureRadLabelText = "Temperature (�C)";
        //private static string initialBalanceDataTabAmplitudePhaseARadLabelText = "Input signal amplitude Phase A (mV)";
        //private static string initialBalanceDataTabAmplitudePhaseBRadLabelText = "Input signal amplitude Phase B (mV)";
        //private static string initialBalanceDataTabAmplitudePhaseCRadLabelText = "Input signal amplitude Phase C (mV)";
        //private static string initialBalanceDataTabShiftPhaseARadLabelText = "Input signal Shift Phase A (deg)";
        //private static string initialBalanceDataTabShiftPhaseBRadLabelText = "Input signal Shift Phase B (deg)";
        //private static string initialBalanceDataTabShiftPhaseCRadLabelText = "Input signal Shift Phase C (deg)";

        private int numberOfNonInteractiveDeviceCommunicationTries;
        private int totalNumberOfDeviceCommunicationTries;

        private List<BHM_Config_ConfigurationRoot> templateConfigurations;
        private int templateConfigurationsSelectedIndex = -1;
        private static string noTemplateConfigurationsAvailable = "No template configurations are available";

        private string templateDbConnectionString = string.Empty;

        private static string currentConfigName = "Current Device Configuration";
        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }

        public BHM_MonitorConfiguration(int inputModBusAddress, int inputNumberOfNonInteractiveDeviceCommunicationTries, int inputTotalNumberOfDeviceCommunicationTries, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();

                this.modBusAddress = inputModBusAddress;
                this.numberOfNonInteractiveDeviceCommunicationTries = inputNumberOfNonInteractiveDeviceCommunicationTries;
                this.totalNumberOfDeviceCommunicationTries = inputTotalNumberOfDeviceCommunicationTries;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;
                // AssignStringValuesToInterfaceObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.BHM_MonitorConfiguration(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void BHM_MonitorConfiguration_Load(object sender, EventArgs e)
        {
            RadMessageBox.ThemeName = "Office2007Black";
            Cursor.Current = Cursors.WaitCursor;
            InitializeMeasurementSettingsRadGridView();
            AddEmptyRowsToMeasurementSettingsGridView();
            InitializeCalculator();
            DisableProgressBars();

            // baudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            Cursor.Current = Cursors.Default;
            configurationRadPageView.SelectedPage = commonParametersRadPageViewPage;

            using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
            {
                LoadTemplateConfigurations(templateDB);
            }
            LoadConfigurationFromDeviceAndAssignItToConfigObject();  // load config on load cfk
        }

        private void AddDataToAllInterfaceObjects(BHM_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    AddDataToCommonParametersTabObjects(inputConfiguration);
                    AddDataToMeasurementSettingsGridView(inputConfiguration.measurementsInfoList);
                    WriteDataToFirstBushingSetTabObjects(inputConfiguration);
                    WriteDataToSecondBushingSetTabObjects(inputConfiguration);
                    WriteInitialBalanceDataTabObjectValues(inputConfiguration);
                    // InitializeCalibrationCoefficientsTabObjects();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.InitializeAllTransientInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        public void AssignStringValuesToInterfaceObjects()
        //        {
        //            try
        //            {
        //                this.Text = bhmMonitorConfigurationInterfaceTitleText;

        //                /// assign strings which go accross tabs

        //                /// now assign the text to the objects
        //                commonParametersRadPageViewPage.Text = commonParametersRadPageViewPageText;
        //                basicSettingsRadGroupBox.Text = basicSettingsRadGroupBoxText;
        //                modbusAddressRadLabel.Text = modbusAddressRadLabelText;
        //                hysterisisRadLabel.Text = hysterisisRadLabelText;
        //                hysterisisUnitsradLabel.Text = hysterisisUnitsradLabelText;
        //                confirmationMeasurementRadLabel.Text = confirmationMeasurementRadLabelText;
        //                confirmationMeasurementUnitsRadLabel.Text = confirmationMeasurementUnitsRadLabelText;
        //                averagingUnnRadLabel.Text = averagingUnnRadLabelText;
        //                allowablePhaseDifferenceRadLabel.Text = allowablePhaseDifferenceRadLabelText;
        //                allowablePhaseDifferenceUnitsRadLabel.Text = allowablePhaseDifferenceUnitsRadLabelText;
        //                baudRateRadLabel.Text = baudRateRadLabelText;
        //                monitoringRadGroupBox.Text = monitoringRadGroupBoxText;
        //                enableMonitoringRadRadioButton.Text = enableMonitoringRadRadioButtonText;
        //                disableMonitoringRadRadioButton.Text = disableMonitoringRadRadioButtonText;
        //                daysToComputeRadGroupBox.Text = daysToComputeRadGroupBoxText;
        //                trendRadLabel.Text = trendRadLabelText;
        //                temperatureCoefficientRadLabel.Text = temperatureCoefficientRadLabelText;
        //                tangentRadLabel.Text = tangentRadLabelText;
        //                measurementScheduleRadGroupBox.Text = measurementScheduleRadGroupBoxText;
        //                stepMeasurementScheduleRadRadioButton.Text = stepMeasurementScheduleRadRadioButtonText;
        //                measurementScheduleHourRadLabel.Text = measurementScheduleHourRadLabelText;
        //                measurementScheduleMinuteRadLabel.Text = measurementScheduleMinuteRadLabelText;
        //                byScheduleMeasurementScheduleRadRadioButton.Text = byScheduleMeasurementScheduleRadRadioButtonText;

        //                availableConfigurationsCommonParametersTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
        //                saveCurrentConfigurationCommonParametersTabRadButton.Text = saveCurrentConfigurationRadButtonText;
        //                saveDeviceConfigurationCommonParametersTabRadButton.Text = saveDeviceConfigurationRadButtonText;
        //                loadConfigurationFromDatabaseCommonParametersTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
        //                programDeviceCommonParametersTabRadButton.Text = programDeviceRadButtonText;
        //                loadConfigurationFromDeviceCommonParametersTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
        //                deleteConfigurationCommonParametersTabRadButton.Text = deleteConfigurationRadButtonText;
        //                configurationViewSelectCommonParametersTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
        //                fromDeviceCommonParametersTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
        //                fromDatabaseCommonParametersTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
        //                currentCommonParametersTabRadRadioButton.Text = currentRadRadioButtonText;
        //                copyDeviceConfigurationCommonParametersTabRadButton.Text = copyDeviceConfigurationRadButtonText;
        //                copyDatabaseConfigurationCommonParametersTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

        //                firstBushingSetRadPageViewPage.Text= firstBushingSetRadPageViewPageText;
        //                firstBushingSetMeasurementsRadGroupBox.Text= bushingSetMeasurementsRadGroupBoxText;
        //                firstBushingSetEnableMeasurementsRadRadioButton.Text= bushingSetEnableMeasurementsRadRadioButtonText;
        //                firstBushingSetDisableMeasurementsRadRadioButton.Text= bushingSetDisableMeasurementsRadRadioButtonText;
        //                firstBushingSetSettingsRadGroupBox.Text= bushingSetSettingsRadGroupBoxText;
        //                firstBushingSetVoltageRadLabel.Text= bushingSetVoltageRadLabelText;
        //                firstBushingSetkVRadLabel.Text= bushingSetkVRadLabelText;
        //                firstBushingSetCurrentRadLabel.Text= bushingSetCurrentRadLabelText;
        //                firstBushingSetAmpsRadLabel.Text= bushingSetAmpsRadLabelText;
        //                firstBushingSetInputImpedenceRadGroupBox.Text= bushingSetInputImpedenceRadGroupBoxText;
        //                firstBushingSetPhaseARadLabel.Text= bushingSetPhaseARadLabelText;
        //                firstBushingSetPhaseAOhmsRadLabel.Text= bushingSetPhaseAOhmsRadLabelText;
        //                firstBushingSetPhaseBRadLabel.Text= bushingSetPhaseBRadLabelText;
        //                firstBushingSetPhaseBOhmsRadLabel.Text= bushingSetPhaseBOhmsRadLabelText;
        //                firstBushingSetPhaseCRadLabel.Text= bushingSetPhaseCRadLabelText;
        //                firstBushingSetPhaseCOhmsRadLabel.Text= bushingSetPhaseCOhmsRadLabelText;
        //                firstBushingSetThresholdsRadGroupBox.Text= bushingSetThresholdsRadGroupBoxText;
        //                firstBushingSetAmplitudeRadGroupBox.Text= bushingSetAmplitudeRadGroupBoxText;
        //                firstBushingSetAmplitudeWarningRadLabel.Text= bushingSetAmplitudeWarningRadLabelText;
        //                firstBushingSetAmplitudeAlarmRadLabel.Text= bushingSetAmplitudeAlarmRadLabelText;
        //                firstBushingSetTemperatureCoeffRadGroupBox.Text= bushingSetTemperatureCoeffRadGroupBoxText;
        //                firstBushingSetTemperatrreCoeffAlarmRadLabel.Text= bushingSetTemperatureCoeffAlarmRadLabelText;
        //                firstBushingSetTemperatureCoeffAlarmOnRadRadioButton.Text= bushingSetTemperatureCoeffAlarmOnRadRadioButtonText;
        //                firstBushingSetTemperatureCoeffAlarmOffRadRadioButton.Text= bushingSetTemperatureCoeffAlarmOffRadRadioButtonText;
        //                firstBushingSetTemperatureCoeffAlarmThresholdRadLabel.Text= bushingSetTemperatureCoeffAlarmThresholdRadLabelText;
        //                firstBushingSetRateOfChangeRadGroupBox.Text= bushingSetRateOfChangeRadGroupBoxText;
        //                firstBushingSetRateOfChangeAlarmRadLabel.Text= bushingSetRateOfChangeAlarmRadLabelText;
        //                firstBushingSetRateOfChangeAlarmOnRadRadioButton.Text= bushingSetRateOfChangeAlarmOnRadRadioButtonText;
        //                firstBushingSetRateOfChangeAlarmOffRadRadioButton.Text= bushingSetRateOfChangeAlarmOffRadRadioButtonText;
        //                firstBushingSetRateOfChangeAlarmThresholdRadLabel.Text= bushingSetRateOfChangeAlarmThresholdRadLabelText;
        //                firstBushingSetTransformerNameplateRadGroupBox.Text= bushingSetTransformerNameplateRadGroupBoxText;
        //                firstBushingSetTangentPhaseARadLabel.Text= bushingSetTangentPhaseARadLabelText;
        //                firstBushingSetTangentPhaseBRadLabel.Text= bushingSetTangentPhaseBRadLabelText;
        //                firstBushingSetTangentPhaseCRadLabel.Text= bushingSetTangentPhaseCRadLabelText;
        //                firstBushingSetCapacitancePhaseARadLabel.Text= bushingSetCapacitancePhaseARadLabelText;
        //                firstBushingSetCapacitancePhaseBRadLabel.Text= bushingSetCapacitancePhaseBRadLabelText;
        //                firstBushingSetCapacitancePhaseCRadLabel.Text= bushingSetCapacitancePhaseCRadLabelText;
        //                firstBushingSetTemperatureAtMeasurementRadLabel.Text= bushingSetTemperatureAtMeasurementRadLabelText;
        //                firstBushingSetTemperatureSensorRadGroupBox.Text = bushingSetTemperatureSensorRadGroupBoxText;

        //                availableConfigurationsFirstBushingSetTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
        //                saveCurrentConfigurationFirstBushingSetTabRadButton.Text = saveCurrentConfigurationRadButtonText;
        //                saveDeviceConfigurationFirstBushingSetTabRadButton.Text = saveDeviceConfigurationRadButtonText;
        //                loadConfigurationFromDatabaseFirstBushingSetTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
        //                programDeviceFirstBushingSetTabRadButton.Text = programDeviceRadButtonText;
        //                loadConfigurationFromDeviceFirstBushingSetTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
        //                deleteConfigurationFirstBushingSetTabRadButton.Text = deleteConfigurationRadButtonText;
        //                configurationViewSelectFirstBushingSetTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
        //                fromDeviceFirstBushingSetTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
        //                fromDatabaseFirstBushingSetTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
        //                currentFirstBushingSetTabRadRadioButton.Text = currentRadRadioButtonText;
        //                copyDeviceConfigurationFirstBushingSetTabRadButton.Text = copyDeviceConfigurationRadButtonText;
        //                copyDatabaseConfigurationFirstBushingSetTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

        //                secondBushingSetRadPageViewPage.Text= secondBushingSetRadPageViewPageText;

        //                secondBushingSetMeasurementsRadGroupBox.Text= bushingSetMeasurementsRadGroupBoxText;
        //                secondBushingSetEnableMeasurementsRadRadioButton.Text= bushingSetEnableMeasurementsRadRadioButtonText;
        //                secondBushingSetDisableMeasurementsRadRadioButton.Text= bushingSetDisableMeasurementsRadRadioButtonText;
        //                secondBushingSetSettingsRadGroupBox.Text= bushingSetSettingsRadGroupBoxText;
        //                secondBushingSetVoltageRadLabel.Text= bushingSetVoltageRadLabelText;
        //                secondBushingSetkVRadLabel.Text= bushingSetkVRadLabelText;
        //                secondBushingSetCurrentRadLabel.Text= bushingSetCurrentRadLabelText;
        //                secondBushingSetAmpsRadLabel.Text= bushingSetAmpsRadLabelText;
        //                secondBushingSetInputImpedenceRadGroupBox.Text= bushingSetInputImpedenceRadGroupBoxText;
        //                secondBushingSetPhaseARadLabel.Text= bushingSetPhaseARadLabelText;
        //                secondBushingSetPhaseAOhmsRadLabel.Text= bushingSetPhaseAOhmsRadLabelText;
        //                secondBushingSetPhaseBRadLabel.Text= bushingSetPhaseBRadLabelText;
        //                secondBushingSetPhaseBOhmsRadLabel.Text= bushingSetPhaseBOhmsRadLabelText;
        //                secondBushingSetPhaseCRadLabel.Text= bushingSetPhaseCRadLabelText;
        //                secondBushingSetPhaseCOhmsRadLabel.Text= bushingSetPhaseCOhmsRadLabelText;
        //                secondBushingSetThresholdsRadGroupBox.Text= bushingSetThresholdsRadGroupBoxText;
        //                secondBushingSetAmplitudeRadGroupBox.Text= bushingSetAmplitudeRadGroupBoxText;
        //                secondBushingSetAmplitudeWarningRadLabel.Text= bushingSetAmplitudeWarningRadLabelText;
        //                secondBushingSetAmplitudeAlarmRadLabel.Text= bushingSetAmplitudeAlarmRadLabelText;
        //                secondBushingSetTemperatureCoeffRadGroupBox.Text= bushingSetTemperatureCoeffRadGroupBoxText;
        //                secondBushingSetTemperatureCoeffAlarmRadLabel.Text= bushingSetTemperatureCoeffAlarmRadLabelText;
        //                secondBushingSetTemperatureCoeffAlarmOnRadRadioButton.Text= bushingSetTemperatureCoeffAlarmOnRadRadioButtonText;
        //                secondBushingSetTemperatureCoeffAlarmOffRadRadioButton.Text= bushingSetTemperatureCoeffAlarmOffRadRadioButtonText;
        //                secondBushingSetTemperatureCoeffAlarmThresholdRadLabel.Text= bushingSetTemperatureCoeffAlarmThresholdRadLabelText;
        //                secondBushingSetRateOfChangeRadGroupBox.Text= bushingSetRateOfChangeRadGroupBoxText;
        //                secondBushingSetRateOfChangeAlarmRadLabel.Text= bushingSetRateOfChangeAlarmRadLabelText;
        //                secondBushingSetRateOfChangeAlarmOnRadRadioButton.Text= bushingSetRateOfChangeAlarmOnRadRadioButtonText;
        //                secondBushingSetRateOfChangeAlarmOffRadRadioButton.Text= bushingSetRateOfChangeAlarmOffRadRadioButtonText;
        //                secondBushingSetRateOfChangeAlarmThresholdRadLabel.Text= bushingSetRateOfChangeAlarmThresholdRadLabelText;
        //                secondBushingSetTransformerNameplateRadGroupBox.Text= bushingSetTransformerNameplateRadGroupBoxText;
        //                secondBushingSetTangentPhaseARadLabel.Text= bushingSetTangentPhaseARadLabelText;
        //                secondBushingSetTangentPhaseBRadLabel.Text= bushingSetTangentPhaseBRadLabelText;
        //                secondBushingSetTangentPhaseCRadLabel.Text= bushingSetTangentPhaseCRadLabelText;
        //                secondBushingSetCapacitancePhaseARadLabel.Text= bushingSetCapacitancePhaseARadLabelText;
        //                secondBushingSetCapacitancePhaseBRadLabel.Text= bushingSetCapacitancePhaseBRadLabelText;
        //                secondBushingSetCapacitancePhaseCRadLabel.Text= bushingSetCapacitancePhaseCRadLabelText;
        //                secondBushingSetTemperatureAtMeasurementRadLabel.Text= bushingSetTemperatureAtMeasurementRadLabelText;
        //                secondBushingSetTemperatureSensorRadGroupBox.Text = bushingSetTemperatureSensorRadGroupBoxText;

        //                availableConfigurationsSecondBushingSetTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
        //                saveCurrentConfigurationSecondBushingSetTabRadButton.Text = saveCurrentConfigurationRadButtonText;
        //                saveDeviceConfigurationSecondBushingSetTabRadButton.Text = saveDeviceConfigurationRadButtonText;
        //                loadConfigurationFromDatabaseSecondBushingSetTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
        //                programDeviceSecondBushingSetTabRadButton.Text = programDeviceRadButtonText;
        //                loadConfigurationFromDeviceSecondBushingSetTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
        //                deleteConfigurationSecondBushingSetTabRadButton.Text = deleteConfigurationRadButtonText;
        //                configurationViewSelectSecondBushingSetTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
        //                fromDeviceSecondBushingSetTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
        //                fromDatabaseSecondBushingSetTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
        //                currentSecondBushingSetTabRadRadioButton.Text = currentRadRadioButtonText;
        //                copyDeviceConfigurationSecondBushingSetTabRadButton.Text = copyDeviceConfigurationRadButtonText;
        //                copyDatabaseConfigurationSecondBushingSetTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

        //                initialBalanceDataTabMeasurementDateRadLabel.Text = initialBalanceDataTabMeasurementDateRadLabelText;
        //                initialBalanceDataTabMeasurementDateValueRadLabel.Text = initialBalanceDataTabMeasurementDateValueRadLabelText;
        //                initialBalanceDataTabBushingSet1RadLabel.Text = initialBalanceDataTabBushingSet1RadLabelText;
        //                initialBalanceDataTabBushingSet2RadLabel.Text = initialBalanceDataTabBushingSet2RadLabelText;
        //                initialBalanceDataTabImbalanceRadLabel.Text = initialBalanceDataTabImbalanceRadLabelText;
        //                initialBalanceDataTabImbalancePhaseRadLabel.Text = initialBalanceDataTabImbalancePhaseRadLabelText;
        //                initialBalanceDataTabTemperatureRadLabel.Text = initialBalanceDataTabTemperatureRadLabelText;
        //                initialBalanceDataTabAmplitudePhaseARadLabel.Text = initialBalanceDataTabAmplitudePhaseARadLabelText;
        //                initialBalanceDataTabAmplitudePhaseBRadLabel.Text = initialBalanceDataTabAmplitudePhaseBRadLabelText;
        //                initialBalanceDataTabAmplitudePhaseCRadLabel.Text = initialBalanceDataTabAmplitudePhaseCRadLabelText;
        //                initialBalanceDataTabShiftPhaseARadLabel.Text = initialBalanceDataTabShiftPhaseARadLabelText;
        //                initialBalanceDataTabShiftPhaseBRadLabel.Text = initialBalanceDataTabShiftPhaseBRadLabelText;
        //                initialBalanceDataTabShiftPhaseCRadLabel.Text = initialBalanceDataTabShiftPhaseCRadLabelText;

        //                availableConfigurationsInitialBalanceDataTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
        //                saveCurrentConfigurationInitialBalanceDataTabRadButton.Text = saveCurrentConfigurationRadButtonText;
        //                saveDeviceConfigurationInitialBalanceDataTabRadButton.Text = saveDeviceConfigurationRadButtonText;
        //                loadConfigurationFromDatabaseInitialBalanceDataTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
        //                programDeviceInitialBalanceDataTabRadButton.Text = programDeviceRadButtonText;
        //                loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
        //                deleteConfigurationInitialBalanceDataTabRadButton.Text = deleteConfigurationRadButtonText;
        //                configurationViewSelectInitialBalanceDataTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
        //                fromDeviceInitialBalanceDataTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
        //                fromDatabaseInitialBalanceDataTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
        //                currentInitialBalanceDataTabRadRadioButton.Text = currentRadRadioButtonText;
        //                copyDeviceConfigurationInitialBalanceDataTabRadButton.Text = copyDeviceConfigurationRadButtonText;
        //                copyDatabaseConfigurationInitialBalanceDataTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        public static void AssignValuesToInternalStaticStrings()
        //        {
        //            try
        //            {
        //                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
        //                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

        //                availableConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceAvailableConfigurationsRadGroupBoxText", availableConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                saveCurrentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceSaveCurrentConfigurationRadButtonText", saveCurrentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                saveDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceSaveDeviceConfigurationRadButtonText", saveDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                loadConfigurationFromDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceLoadConfigFromDatabaseRadButtonText", loadConfigurationFromDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                programDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceProgramDeviceRadButtonText", programDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                loadConfigurationFromDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceLoadConfigurationFromDeviceRadButtonText", loadConfigurationFromDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                deleteConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceDeleteConfigurationRadButtonText", deleteConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                configurationViewSelectRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceConfigurationViewSelectRadGroupBoxText", configurationViewSelectRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                fromDeviceRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceFromDeviceRadioButtonText", fromDeviceRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                fromDatabaseRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceFromDatabaseRadioButtonText", fromDatabaseRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                currentRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCurrentRadioButtonText", currentRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                copyDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCopyDeviceConfigurationRadButtonText", copyDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
        //                copyDatabaseConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCopyDatabaseConfigurationRadButtonText", copyDatabaseConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");

        //                bhmMonitorConfigurationInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceTitleText", bhmMonitorConfigurationInterfaceTitleText, htmlFontType, htmlStandardFontSize, "");

        //                commonParametersRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabText", commonParametersRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
        //                basicSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsRadGroupBoxText", basicSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                modbusAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsModBusAddressRadLabelText", modbusAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                hysterisisRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsHysterisisRadLabelText", hysterisisRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                hysterisisUnitsradLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsHysterisisUnitsRadLabelText", hysterisisUnitsradLabelText, htmlFontType, htmlStandardFontSize, "");
        //                confirmationMeasurementRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsConfirmationMeasurementRadLabelText", confirmationMeasurementRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                confirmationMeasurementUnitsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsConfirmationMeasurementUnitsRadLabelText", confirmationMeasurementUnitsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                averagingUnnRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsAveragingUnnRadLabelText", averagingUnnRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                allowablePhaseDifferenceRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsAllowablePhaseDifferenceRadLabelText", allowablePhaseDifferenceRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                allowablePhaseDifferenceUnitsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsAllowablePhaseDifferenceUnitsRadLabelText", allowablePhaseDifferenceUnitsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                baudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabBasicSettingsBaudRateRadLabelText", baudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                monitoringRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMonitoringRadGroupBoxText", monitoringRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMonitoringEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMonitoringDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                daysToComputeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabDaysToComputeRadGroupBoxText", daysToComputeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                trendRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabDaysToCalculateTrendRadLabelText", trendRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                temperatureCoefficientRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabDaysToCalculateTemperatureCoefficientRadLabelText", temperatureCoefficientRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                tangentRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabDaysToCalculateTangentRadLabelText", tangentRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                measurementScheduleRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMeasurementScheduleRadGroupBoxText", measurementScheduleRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                stepMeasurementScheduleRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMeasurementScheduleStepMeasurementScheduleRadRadioButtonText", stepMeasurementScheduleRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                measurementScheduleHourRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMeasurementScheduleHourRadLabelText", measurementScheduleHourRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                measurementScheduleMinuteRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMeasurementScheduleMinuteRadLabelText", measurementScheduleMinuteRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                byScheduleMeasurementScheduleRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceCommonTabMeasurementScheduleByScheduleMeasurementScheduleRadRadioButtonText", byScheduleMeasurementScheduleRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");

        //                firstBushingSetRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceFirstBushingTabText", firstBushingSetRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
        //                secondBushingSetRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceSecondBushingTabText", firstBushingSetRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");

        //                bushingSetMeasurementsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabMeasurementsRadGroupBoxText", bushingSetMeasurementsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetEnableMeasurementsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabEnableMeasurementsRadRadioButtonText", bushingSetEnableMeasurementsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetDisableMeasurementsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabDisableMeasurementsRadRadioButtonText", bushingSetDisableMeasurementsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabSettingsRadGroupBoxText", bushingSetSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetVoltageRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabSettingsVoltageRadLabelText", bushingSetVoltageRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetkVRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabSettingskVRadLabelText", bushingSetkVRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetCurrentRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabSettingsCurrentRadLabelText", bushingSetCurrentRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetAmpsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabSettingsAmpsRadLabelText", bushingSetAmpsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetInputImpedenceRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedenceRadGroupBoxText", bushingSetInputImpedenceRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedencePhaseARadLabelText", bushingSetPhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseAOhmsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedenceOhmsRadLabelText", bushingSetPhaseAOhmsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedencePhaseBRadLabelText", bushingSetPhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseBOhmsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedenceOhmsRadLabelText", bushingSetPhaseBOhmsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedencePhaseCRadLabelText", bushingSetPhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetPhaseCOhmsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabInputImpedenceOhmsRadLabelText", bushingSetPhaseCOhmsRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetThresholdsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabThresholdsRadGroupBoxText", bushingSetThresholdsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetAmplitudeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabAmplitudeRadGroupBoxText", bushingSetAmplitudeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetAmplitudeWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabAmplitudeUnnAmplitudeWarningRadLabelText", bushingSetAmplitudeWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetAmplitudeAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabAmplitudeUnnAmplitudeAlarmRadLabelText", bushingSetAmplitudeAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureCoeffRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureCoeffRadGroupBoxText", bushingSetTemperatureCoeffRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureCoeffAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureCoeffAlarmRadLabelText", bushingSetTemperatureCoeffAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureCoeffAlarmOnRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureCoefficientAlarmOnRadRadioButtonText", bushingSetTemperatureCoeffAlarmOnRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureCoeffAlarmOffRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureCoefficientAlarmOffRadRadioButtonText", bushingSetTemperatureCoeffAlarmOffRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureCoeffAlarmThresholdRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureCoeffAlarmThresholdRadLabelText", bushingSetTemperatureCoeffAlarmThresholdRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetRateOfChangeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabRateOfChangeRadGroupBoxText", bushingSetRateOfChangeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetRateOfChangeAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabRateOfChangeAlarmRadLabelText", bushingSetRateOfChangeAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetRateOfChangeAlarmOnRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabRateOfChangeAlarmOnRadRadioButtonText", bushingSetRateOfChangeAlarmOnRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetRateOfChangeAlarmOffRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabRateOfChangeAlarmOffRadRadioButtonText", bushingSetRateOfChangeAlarmOffRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetRateOfChangeAlarmThresholdRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabRateOfChangeAlarmThresholdRadLabelText", bushingSetRateOfChangeAlarmThresholdRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTransformerNameplateRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateRadGroupBoxText", bushingSetTransformerNameplateRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTangentPhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataTangentPhaseARadLabelText", bushingSetTangentPhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTangentPhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataTangentPhaseBRadLabelText", bushingSetTangentPhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTangentPhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataTangentPhaseCRadLabelText", bushingSetTangentPhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetCapacitancePhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataCapacitancePhaseARadLabelText", bushingSetCapacitancePhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetCapacitancePhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataCapacitancePhaseBRadLabelText", bushingSetCapacitancePhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetCapacitancePhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataCapacitancePhaseCRadLabelText", bushingSetCapacitancePhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureAtMeasurementRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTransformerNameplateDataTemperatureAtMeasurementRadLabelText", bushingSetTemperatureAtMeasurementRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                bushingSetTemperatureSensorRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBushingTabTemperatureSensorRadGroupBoxText", bushingSetTemperatureSensorRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");

        //                initialBalanceDataRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceInitialBalanceDataRadPageViewPageText", initialBalanceDataRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabMeasurementDateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabMeasurementDateRadLabelText", initialBalanceDataTabMeasurementDateRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabBushingSet1RadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabBushingSet1RadLabelText", initialBalanceDataTabBushingSet1RadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabBushingSet2RadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabBushingSet2RadLabelText", initialBalanceDataTabBushingSet2RadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabImbalanceRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabImbalanceRadLabelText", initialBalanceDataTabImbalanceRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabImbalancePhaseRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabImbalancePhaseRadLabelText", initialBalanceDataTabImbalancePhaseRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabTemperatureRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabTemperatureRadLabelText", initialBalanceDataTabTemperatureRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabAmplitudePhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabAmplitudePhaseARadLabelText", initialBalanceDataTabAmplitudePhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabAmplitudePhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabAmplitudePhaseBRadLabelText", initialBalanceDataTabAmplitudePhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabAmplitudePhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabAmplitudePhaseCRadLabelText", initialBalanceDataTabAmplitudePhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabShiftPhaseARadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabShiftPhaseARadLabelText", initialBalanceDataTabShiftPhaseARadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabShiftPhaseBRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabShiftPhaseBRadLabelText", initialBalanceDataTabShiftPhaseBRadLabelText, htmlFontType, htmlStandardFontSize, "");
        //                initialBalanceDataTabShiftPhaseCRadLabelText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationInterfaceBalanceTabShiftPhaseCRadLabelText", initialBalanceDataTabShiftPhaseCRadLabelText, htmlFontType, htmlStandardFontSize, "");

        //                // internal text
        //                currentConfigName = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationCurrentConfigName", currentConfigName, "", "", "");
        //                emptyCellErrorMessageText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationEmptyCellErrorMessageText", emptyCellErrorMessageText, htmlFontType, htmlStandardFontSize, "");
        //                uploadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationUploadingConfigurationText", uploadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
        //                downloadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDownloadingConfigurationText", downloadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
        //                couldNotFindCurrentDeviceConfigurationDisplayingMostRecentText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationCouldNotFindCurrentDeviceConfigurationDisplayingMostRecentText", couldNotFindCurrentDeviceConfigurationDisplayingMostRecentText, htmlFontType, htmlStandardFontSize, "");
        //                noDeviceConfigurationsInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoDeviceConfigurationsInDatabaseText", noDeviceConfigurationsInDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                noDatabaseConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoDatabaseConfigurationLoadedText", noDatabaseConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
        //                noDeviceConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoDeviceConfigurationLoadedText", noDeviceConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
        //                changesToCurrentConfigurationNotSavedWarningText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationChangesToCurrentConfigurationNotSavedWarningText", changesToCurrentConfigurationNotSavedWarningText, htmlFontType, htmlStandardFontSize, "");
        //                exitWithoutSavingQuestionText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationExitWithoutSavingQuestionText", exitWithoutSavingQuestionText, htmlFontType, htmlStandardFontSize, "");
        //                overwriteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationOverwriteCurrentConfigurationText", overwriteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");
        //                failedToSaveConfigurationToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationFailedToSaveConfigurationToDatabaseText", failedToSaveConfigurationToDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                noCurrentConfigurationDefinedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoCurrentConfigurationDefinedText", noCurrentConfigurationDefinedText, htmlFontType, htmlStandardFontSize, "");
        //                noConfigurationLoadedFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoConfigurationLoadedFromDeviceText", noConfigurationLoadedFromDeviceText, htmlFontType, htmlStandardFontSize, "");
        //                configurationBeingSavedIsFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationConfigurationBeingSavedIsFromDeviceText", configurationBeingSavedIsFromDeviceText, htmlFontType, htmlStandardFontSize, "");
        //                deviceConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeviceConfigurationSavedToDatabaseText", deviceConfigurationSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                deviceConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeviceConfigurationNotSavedToDatabaseText", deviceConfigurationNotSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                errorInConfigurationLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationErrorInConfigurationLoadedFromDatabaseText", errorInConfigurationLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                configurationCouldNotBeLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationConfigurationCouldNotBeLoadedFromDatabaseText", configurationCouldNotBeLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
        //                configurationDeleteFromDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationConfigurationDeleteFromDatabaseWarningText", configurationDeleteFromDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
        //                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
        //                cannotDeleteCurrentConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationCannotDeleteCurrentConfigurationWarningText", cannotDeleteCurrentConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");
        //                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoConfigurationSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");
        //                failedToDownloadDeviceConfigurationDataText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationFailedToDownloadDeviceConfigurationDataText", failedToDownloadDeviceConfigurationDataText, htmlFontType, htmlStandardFontSize, "");
        //                commandToReadDeviceErrorStateFailedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationCommandToReadDeviceErrorStateFailedText", commandToReadDeviceErrorStateFailedText, htmlFontType, htmlStandardFontSize, "");
        //                lostDeviceConnectionText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationLostDeviceConnectionText", lostDeviceConnectionText, htmlFontType, htmlStandardFontSize, "");
        //                notConnectedToBHMWarningText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNotConnectedToBHMWarningText", notConnectedToBHMWarningText, htmlFontType, htmlStandardFontSize, "");
        //                serialPortNotSetWarningText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationSerialPortNotSetWarningText", serialPortNotSetWarningText, htmlFontType, htmlStandardFontSize, "");
        //                failedToOpenMonitorConnectionText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationFailedToOpenMonitorConnectionText", failedToOpenMonitorConnectionText, htmlFontType, htmlStandardFontSize, "");
        //                deviceCommunicationNotProperlyConfiguredText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeviceCommunicationNotProperlyConfiguredText", deviceCommunicationNotProperlyConfiguredText, htmlFontType, htmlStandardFontSize, "");
        //                downloadWasInProgressWhenInterfaceWasOpenedText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDownloadWasInProgressWhenInterfaceWasOpenedText", downloadWasInProgressWhenInterfaceWasOpenedText, htmlFontType, htmlStandardFontSize, "");
        //                failedToWriteTheConfigurationToTheDevice = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationFailedToWriteTheConfigurationToTheDevice", failedToWriteTheConfigurationToTheDevice, htmlFontType, htmlStandardFontSize, "");
        //                configurationWasWrittenToTheDevice = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationConfigurationWasWrittenToTheDevice", configurationWasWrittenToTheDevice, htmlFontType, htmlStandardFontSize, "");
        //                configurationWasReadFromTheDevice = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationConfigurationWasReadFromTheDevice", configurationWasReadFromTheDevice, htmlFontType, htmlStandardFontSize, "");
        //                deviceConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeviceConfigurationDoesNotMatchDatabaseConfigurationText", deviceConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
        //                deviceCommunicationNotSavedInDatabaseYetText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDeviceCommunicationNotSavedInDatabaseYetText", deviceCommunicationNotSavedInDatabaseYetText, htmlFontType, htmlStandardFontSize, "");
        //                saveDeviceConfigurationQuestionText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationSaveDeviceConfigurationQuestionText", saveDeviceConfigurationQuestionText, htmlFontType, htmlStandardFontSize, "");
        //                errorInModbusAddressNonNumericalValuePresentText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationErrorInModbusAddressNonNumericalValuePresentText", errorInModbusAddressNonNumericalValuePresentText, htmlFontType, htmlStandardFontSize, "");
        //                errorModbusAddressValueIncorrect = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationErrorModbusAddressValueIncorrect", errorModbusAddressValueIncorrect, htmlFontType, htmlStandardFontSize, "");
        //                errorNonNumericalValuePresentText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationErrorNonNumericalValuePresentText", errorNonNumericalValuePresentText, htmlFontType, htmlStandardFontSize, "");
        //                measurementNumberChannelNumberGridText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationMeasurementNumberGridText", measurementNumberChannelNumberGridText, htmlFontType, htmlStandardFontSize, "");
        //                hourChannelNumberGridText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationHourGridText", hourChannelNumberGridText, htmlFontType, htmlStandardFontSize, "");
        //                minuteChannelNumberGridText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationMinuteGridText", minuteChannelNumberGridText, htmlFontType, htmlStandardFontSize, "");
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private bool ErrorIsPresentInATabObject()
        {
            bool errorIsPresent = false;
            try
            {
                if ((!errorIsPresent) && (ErrorIsPresentInACommonParametersTabObject()))
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && (ErrorIsPresentInFirstBushingSetTabObjects()))
                {
                    errorIsPresent = true;
                }
                if ((!errorIsPresent) && (ErrorIsPresentInFirstBushingSetTabObjects()))
                {
                    errorIsPresent = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ErrorIsPresentInATabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void SelectFirstBushingSetTab()
        {
            configurationRadPageView.SelectedPage = firstBushingSetRadPageViewPage;
        }

        private void SelectSecondBushingSetTab()
        {
            configurationRadPageView.SelectedPage = secondBushingSetRadPageViewPage;
        }

        private void WriteReadOnSideValueToConfigurationData()
        {
            try
            {
                int sideOneEnabled = 0;
                int sideTwoEnabled = 0;
                int readOnSideValue = 0;

                if (currentConfiguration != null)
                {
                    if (currentConfiguration.gammaSetupInfo != null)
                    {
                        if (firstBushingSetEnableMeasurementsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            sideOneEnabled = 1;
                        }

                        if (secondBushingSetEnableMeasurementsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            sideTwoEnabled = 1;
                        }

                        if ((sideOneEnabled == 1) && (sideTwoEnabled == 1))
                        {
                            readOnSideValue = 3;
                        }
                        else if (sideOneEnabled == 1)
                        {
                            readOnSideValue = 1;
                        }
                        else if (sideTwoEnabled == 1)
                        {
                            readOnSideValue = 2;
                        }

                        currentConfiguration.gammaSetupInfo.ReadOnSide = readOnSideValue;
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteReadOnSideValueToConfigurationData()\nPrivate data this.currentConfiguration.gammaSetupInfo was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteReadOnSideValueToConfigurationData()\nPrivate data this.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteReadOnSideValueToConfigurationData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteUserInterfaceViewedConfigurationToCurrentConfiguration()
        {
            try
            {
                WriteCommonParametersTabObjectsToConfigurationData();
                WriteMeasurementSettingsToConfigurationData();
                WriteFirstBushingSetTabObjectsToConfigurationData();
                WriteSecondBushingSetTabObjectsToConfigurationData();
                WriteReadOnSideValueToConfigurationData();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetBaudRateFromSelectedIndex(int selectedIndex)
        {
            int baudRate = 0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        baudRate = 9600;
                        break;
                    case 1:
                        baudRate = 38400;
                        break;
                    case 2:
                        baudRate = 57600;
                        break;
                    case 3:
                        baudRate = 115200;
                        break;
                    case 4:
                        baudRate = 230400;
                        break;
                    case 5:
                        baudRate = 500000;
                        break;
                    case 6:
                        baudRate = 1000000;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.GetBaudRateFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return baudRate;
        }

        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.programDeviceCommonParametersTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToConfigObject();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.loadConfigurationFromDeviceCommonParametersTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void BHM_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.currentConfiguration != null)
            {
                WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                if (!this.currentConfiguration.ConfigurationIsTheSame(this.uneditedCurrentConfiguration))
                {
                    if (RadMessageBox.Show(this, changesToCurrentConfigurationNotSavedWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void DisableAllControls()
        {
            try
            {
                programDeviceCommonParametersTabRadButton.Enabled = false;
                loadConfigurationFromDeviceCommonParametersTabRadButton.Enabled = false;
                programDeviceFirstBushingSetTabRadButton.Enabled = false;
                loadConfigurationFromDeviceFirstBushingSetTabRadButton.Enabled = false;
                programDeviceSecondBushingSetTabRadButton.Enabled = false;
                loadConfigurationFromDeviceSecondBushingSetTabRadButton.Enabled = false;
                programDeviceInitialBalanceDataTabRadButton.Enabled = false;
                loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Enabled = false;

                DisableCommonParametersTabObjects();
                DisableFirstBushingSetTabObjects();
                DisableSecondBushingSetTabObjects();
                DisableInitialBalanceDataTabObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.DisableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableAllControls()
        {
            try
            {
                programDeviceCommonParametersTabRadButton.Enabled = true;
                loadConfigurationFromDeviceCommonParametersTabRadButton.Enabled = true;
                programDeviceFirstBushingSetTabRadButton.Enabled = true;
                loadConfigurationFromDeviceFirstBushingSetTabRadButton.Enabled = true;
                programDeviceSecondBushingSetTabRadButton.Enabled = true;
                loadConfigurationFromDeviceSecondBushingSetTabRadButton.Enabled = true;
                programDeviceInitialBalanceDataTabRadButton.Enabled = true;
                loadConfigurationFromDeviceInitialBalanceDataTabRadButton.Enabled = true;

                EnableCommonParametersTabObjects();
                EnableFirstBushingSetTabObjects();
                EnableSecondBushingSetTabObjects();
                EnableInitialBalanceDataTabObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.EnableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void EnableProgressBars()
        {
            try
            {
                commonParametersRadProgressBar.Visible = true;
                firstBushingSetRadProgressBar.Visible = true;
                secondBushingSetRadProgressBar.Visible = true;
                initialBalanceDataRadProgressBar.Visible = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.EnableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DisableProgressBars()
        {
            try
            {
                commonParametersRadProgressBar.Visible = false;
                firstBushingSetRadProgressBar.Visible = false;
                secondBushingSetRadProgressBar.Visible = false;
                initialBalanceDataRadProgressBar.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.DisableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetProgressBarsToUploadState()
        {
            try
            {
                commonParametersRadProgressBar.Text = uploadingConfigurationText;
                firstBushingSetRadProgressBar.Text = uploadingConfigurationText;
                secondBushingSetRadProgressBar.Text = uploadingConfigurationText;
                initialBalanceDataRadProgressBar.Text = uploadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarsToUploadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetProgressBarsToDownloadState()
        {
            try
            {
                commonParametersRadProgressBar.Text = downloadingConfigurationText;
                firstBushingSetRadProgressBar.Text = downloadingConfigurationText;
                secondBushingSetRadProgressBar.Text = downloadingConfigurationText;
                initialBalanceDataRadProgressBar.Text = downloadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarsToDownloadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    commonParametersRadProgressBar.Value1 = currentProgress;
                    firstBushingSetRadProgressBar.Value1 = currentProgress;
                    secondBushingSetRadProgressBar.Value1 = currentProgress;
                    initialBalanceDataRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsCommonParametersTabRadDropDownListDataSource;
                string[] templateConfigurationsFirstBushingSetTabRadDropDownListDataSource;
                string[] templateConfigurationsSecondBushingSetTabRadDropDownListDataSource;
                string[] templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource;
                string description;

                this.templateConfigurations = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsCommonParametersTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsFirstBushingSetTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsSecondBushingSetTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsCommonParametersTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsFirstBushingSetTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsSecondBushingSetTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsCommonParametersTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsFirstBushingSetTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsSecondBushingSetTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsCommonParametersTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsFirstBushingSetTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsSecondBushingSetTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsCommonParametersTabRadDropDownList.DataSource = templateConfigurationsCommonParametersTabRadDropDownListDataSource;
                templateConfigurationsFirstBushingSetTabRadDropDownList.DataSource = templateConfigurationsFirstBushingSetTabRadDropDownListDataSource;
                templateConfigurationsSecondBushingSetTabRadDropDownList.DataSource = templateConfigurationsSecondBushingSetTabRadDropDownListDataSource;
                templateConfigurationsInitialBalanceDataTabRadDropDownList.DataSource = templateConfigurationsInitialBalanceDataTabRadDropDownListDataSource;

                templateConfigurationsCommonParametersTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsCommonParametersTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCommonParametersTabRadDropDownList.Text = description;
                    templateConfigurationsFirstBushingSetTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsFirstBushingSetTabRadDropDownList.Text = description;
                    templateConfigurationsSecondBushingSetTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsSecondBushingSetTabRadDropDownList.Text = description;
                    templateConfigurationsInitialBalanceDataTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsInitialBalanceDataTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsCommonParametersTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCommonParametersTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.templateConfigurationsCommonParametersTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsFirstBushingSetTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsFirstBushingSetTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.templateConfigurationsFirstBushingSetTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsSecondBushingSetTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsSecondBushingSetTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.templateConfigurationsSecondBushingSetTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsInitialBalanceDataTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsInitialBalanceDataTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.templateConfigurationsInitialBalanceDataTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadSelectedTemplateConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                BHM_Config_ConfigurationRoot templateConfigurationRoot;
                BHM_Configuration templateConfiguration;

                if (this.templateConfigurations != null)
                {
                    if (this.templateConfigurations.Count > 0)
                    {
                        if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                        {
                            templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                            if (templateConfigurationRoot != null)
                            {
                                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                {
                                    templateConfiguration = ConfigurationConversion.GetBHM_ConfigurationFromDatabase(templateConfigurationRoot.ID, templateDB);
                                    if (templateConfiguration.AllConfigurationMembersAreNonNull())
                                    {
                                        this.currentConfiguration = templateConfiguration;
                                        this.uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(this.currentConfiguration);
                                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                                        RadMessageBox.Show("Loaded template configuration");
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Failed to load the template configuration");
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    string errorMessage = "Error in BHM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadFromFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceFileName;
                XmlSerializer serializer;
                bool loadFailed = false;

                sourceFileName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml");
                if ((sourceFileName != null) && (sourceFileName != string.Empty))
                {
                    this.currentConfiguration = null;
                    serializer = new XmlSerializer(typeof(BHM_Configuration));
                    try
                    {
                        using (Stream s = File.OpenRead(sourceFileName))
                        {
                            this.currentConfiguration = (BHM_Configuration)serializer.Deserialize(s);
                            this.uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(this.currentConfiguration);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to deserialize configuration from file, exception message was: " + ex.Message);
                        this.currentConfiguration = new BHM_Configuration();
                        this.currentConfiguration.InitializeConfigurationToZeroes();
                        this.uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if ((!loadFailed) && (!this.currentConfiguration.AllConfigurationMembersAreNonNull()))
                    {
                        MessageBox.Show("Configuration loaded from file was incomplete or contained errors");
                        this.currentConfiguration = new BHM_Configuration();
                        this.currentConfiguration.InitializeConfigurationToZeroes();
                        this.uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if (!loadFailed)
                    {
                        AddDataToAllInterfaceObjects(this.currentConfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.loadFromFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string destinationFileName;
                XmlSerializer serializer;

                LoadConfigurationFromDeviceAndAssignItToConfigObject();
                if ((this.currentConfiguration != null) && (this.currentConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    destinationFileName = FileUtilities.GetSaveFileNameWithFullPath("BHM_MonitorConfiguration", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml", true, false);
                    if ((destinationFileName != null) && (destinationFileName != string.Empty))
                    {
                        serializer = new XmlSerializer(typeof(BHM_Configuration));
                        using (Stream s = File.Create(destinationFileName))
                        {
                            serializer.Serialize(s, this.currentConfiguration);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.saveToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void secondBushingSetAmplitudeRadGroupBox_Click(object sender, EventArgs e)
        {

        }

       

    }
}
