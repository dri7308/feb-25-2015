﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;
using MonitorUtilities;

namespace BushingConfigurationLite
{
    public partial class BHM_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
        private static string noCurrentConfigurationDefinedText = "There is no current configuration defined.";
        private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
        private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
        private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        private static string deleteAsQuestionText = "Delete?";
        private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
        private static string noConfigurationSelectedText = "No configuration selected.";
        private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
        private static string commandToReadDeviceErrorStateFailedText = "Command to read the device errror state failed.";
        private static string lostDeviceConnectionText = "Lost the connection to the device.";
        private static string notConnectedToBHMWarningText = "You are not connected to a BHM.  Cannot configure using this interface.";
        private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
        private static string failedToWriteTheConfigurationToTheDevice = "Failed to write the configuration to the device.";
        private static string configurationWasWrittenToTheDevice = "The configuration was written to the device.";
        private static string configurationWasReadFromTheDevice = "The configuration was read from the device.";
        private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        private static string saveDeviceConfigurationQuestionText = "Save device configuration?";

        private void LoadConfigurationFromDeviceAndAssignItToConfigObject()
        {
            try
            {
                currentConfiguration = LoadConfigurationFromDevice();
                uneditedCurrentConfiguration = null;
                if ((currentConfiguration != null) && (currentConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(currentConfiguration);
                    RadMessageBox.Show(this, configurationWasReadFromTheDevice);
                    AddDataToAllInterfaceObjects(currentConfiguration);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        public BHM_Configuration LoadConfigurationFromDevice()
        {
            BHM_Configuration configuration = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                Byte[] byteValuesToCreateConfiguration = null;
                int offset = 1;
                long deviceError;
                int totalNumberOfBytesSavedSoFar = 0;
                int numberOfBytesReturnedByLastFunctionCall = 0;
                int numberOfBytesPerRequest = 200;
                int numberOfInitializationBytes = 300;
                bool receivedAllData = false;

                int iterations = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                ErrorCode errorCode = ErrorCode.ConfigurationDownloadFailed;

                DisableAllControls();

                SetProgressBarsToDownloadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 5);

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                    if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                    {
                        allByteValues = new Byte[2000];
                        currentByteValues = new Byte[numberOfInitializationBytes];

                        deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                        if (deviceError > -1)
                        {
                            receivedAllData = false;
                            while ((!receivedAllData) && DeviceCommunication.DownloadIsEnabled())
                            {
                                currentByteValues = InteractiveDeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, numberOfBytesPerRequest, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                                if (currentByteValues != null)
                                {
                                    numberOfBytesReturnedByLastFunctionCall = currentByteValues.Length;
                                    Array.Copy(currentByteValues, 0, allByteValues, totalNumberOfBytesSavedSoFar, numberOfBytesReturnedByLastFunctionCall);
                                    totalNumberOfBytesSavedSoFar += numberOfBytesReturnedByLastFunctionCall;
                                    offset += numberOfBytesReturnedByLastFunctionCall;
                                    if (numberOfBytesReturnedByLastFunctionCall < numberOfBytesPerRequest)
                                    {
                                        receivedAllData = true;
                                    }
                                }
                                else
                                {
                                    receivedAllData = true;
                                    errorCode = ErrorCode.DownloadFailed;
                                    allByteValues = null;
                                    break;
                                }

                                iterations++;
                                SetProgressBarProgress(iterations, 5);
                            }
                            if ((allByteValues != null) && DeviceCommunication.DownloadIsEnabled())
                            {
                                byteValuesToCreateConfiguration = new Byte[totalNumberOfBytesSavedSoFar];
                                Array.Copy(allByteValues, byteValuesToCreateConfiguration, totalNumberOfBytesSavedSoFar);
                                configuration = new BHM_Configuration(byteValuesToCreateConfiguration);
                                errorCode = ErrorCode.ConfigurationDownloadSucceeded;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.DeviceErrorReadFailed;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_MonitorConfiguration.BHM_LoadConfigurationFromDevice(int, int, int, int)\nModbus address needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (errorCode != ErrorCode.ConfigurationDownloadSucceeded)
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.BHM_LoadConfigurationFromDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                DeviceCommunication.CloseConnection();
            }
            return configuration;
        }

        private void ProgramDevice()
        {
            try
            {
                Byte[] byteData = null;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                bool deviceWasProgrammedSuccessfully = true;

                int offset = 1;
                Byte[] bytesBeingSentToDevice;
                int numberOfBytesPerFullSend = 200;
                int numberOfBytesBeingSent;
                int totalNumberOfBytesToWrite;
                int numberOfBytesSentSoFar = 0;

                int iteration = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                DisableAllControls();
                SetProgressBarsToUploadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 5);

                if (this.currentConfiguration != null)
                {
                    if (!ErrorIsPresentInATabObject())
                    {
                        WriteUserInterfaceViewedConfigurationToCurrentConfiguration();

                        errorCode = SimplifiedCommunication.OpenUsbConnectionToABhm(modBusAddress, 20, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                        {
                            Application.DoEvents();
                            byteData = currentConfiguration.ConvertConfigurationToByteArray();
                            totalNumberOfBytesToWrite = byteData.Length;
                            bytesBeingSentToDevice = new byte[numberOfBytesPerFullSend];

                            while ((numberOfBytesSentSoFar < totalNumberOfBytesToWrite) && DeviceCommunication.DownloadIsEnabled() && deviceWasProgrammedSuccessfully)
                            {
                                if ((totalNumberOfBytesToWrite - numberOfBytesSentSoFar) >= numberOfBytesPerFullSend)
                                {
                                    numberOfBytesBeingSent = numberOfBytesPerFullSend;
                                }
                                else
                                {
                                    numberOfBytesBeingSent = totalNumberOfBytesToWrite - numberOfBytesSentSoFar;
                                }
                                Array.Copy(byteData, numberOfBytesSentSoFar, bytesBeingSentToDevice, 0, numberOfBytesBeingSent);

                                deviceWasProgrammedSuccessfully = InteractiveDeviceCommunication.BHM_SetDeviceSetup(modBusAddress, offset, bytesBeingSentToDevice, numberOfBytesBeingSent, 200, this.numberOfNonInteractiveDeviceCommunicationTries, this.totalNumberOfDeviceCommunicationTries,parentWindowInformation);
                                offset += numberOfBytesBeingSent;
                                numberOfBytesSentSoFar += numberOfBytesBeingSent;

                                uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(currentConfiguration);

                                Application.DoEvents();

                                iteration++;
                                SetProgressBarProgress(iteration, 5);
                            }
                            if (deviceWasProgrammedSuccessfully)
                            {
                                errorCode = ErrorCode.ConfigurationWriteSucceeded;
                                uneditedCurrentConfiguration = BHM_Configuration.CopyConfiguration(currentConfiguration);
                            }
                            else
                            {
                                errorCode = ErrorCode.ConfigurationWriteFailed;
                            }
                        }
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noCurrentConfigurationDefinedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.ProgramDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
                EnableAllControls();
                DeviceCommunication.CloseConnection();
            }
        }
    }
}
