﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using MonitorInterface;
using System.Linq;
using ConfigurationObjects;

namespace BushingConfigurationLite
{
    public partial class BHM_MonitorConfiguration
    {
         private void InitializeCalculator()
        {
            string[] grid11 = new string[] { "1", "500", "33" };
            string[] grid21 = new string[] { "2", "500", "33" };
            string[] grid31 = new string[] { "3", "500", "33" };

            object[] grid1 = new object[] { grid11, grid21, grid31 };
            foreach (string[] rowArray1 in grid1)
            {
                radGridView1.Rows.Add(rowArray1);
            }
            // add rows to grid 2
            string[] grid12 = new string[] { "1" };
            string[] grid22 = new string[] { "2" };
            string[] grid32 = new string[] { "3" };

            object[] grid2 = new object[] { grid12, grid22, grid32};
            foreach (string[] rowArray2 in grid2)
            {
                radGridView2.Rows.Add(rowArray2);
            }


            doCalcs();
        }

        

        private void doCalcs()
        {
            double ma, z, maxVd, minVd, dbR, dbRp, vtemp;
            double[] vDrop = new double[3];

            //calculate ma and voltage drop
            for (int x = 0; x < 3; x++)
            { 
                z = 1 / (2 * Math.PI * Convert.ToDouble(radTextBox2.Text) * Convert.ToDouble(radGridView1.Rows[x].Cells[1].Value) * Math.Pow(10, -12));
                ma = Convert.ToDouble(radTextBox1.Text) / Math.Sqrt(3) * 1000000 / z;  // milliamps
                radGridView1.Rows[x].Cells[3].Value = ma.ToString("F");
                vDrop[x] = (ma * Convert.ToDouble(radGridView1.Rows[x].Cells[2].Value) / 1000); // voltage drop     
                this.radGridView1.Rows[x].Cells[4].Value = vDrop[x].ToString("F");
                
                this.radGridView1.Rows[x].Cells[5].Style.CustomizeFill = true; 
                this.radGridView1.Rows[x].Cells[5].Style.DrawFill = true; 

                // do range check and set notices
                radGridView1.Rows[x].Cells[5].Value = "Within Range";
                this.radGridView1.Rows[x].Cells[5].Style.BackColor = Color.LightGreen;
                if (vDrop[x] > 3.5)
                {
                    radGridView1.Rows[x].Cells[5].Value = "Above Range";
                    this.radGridView1.Rows[x].Cells[5].Style.BackColor = Color.LightYellow;
                }

                if (vDrop[x] < .25)
                {
                    radGridView1.Rows[x].Cells[5].Value = "Below Range";
                    this.radGridView1.Rows[x].Cells[5].Style.BackColor = Color.LightYellow;
                }
            }

            //find min and max of the three voltage drops
            maxVd = vDrop[0];
            for (int x = 0; x < 3; x++)  // find max value
            {
                if (maxVd < vDrop[x])
                {
                    maxVd = vDrop[x];
                }
            }
           
            minVd = vDrop[0];
            for (int x = 0; x < 3; x++) // find min value     
            {
                if (minVd > vDrop[x])
                {
                    minVd = vDrop[x];
                }
            }
            radLabel5.Text = maxVd.ToString("F");
            radLabel4.Text = minVd.ToString("F");

            // determine if system will balance
            if ((maxVd - minVd) / maxVd > .115)// will not balance
            {
                radLabel3.Text = ((maxVd - minVd) / maxVd * 100).ToString("F") + "% - Will Not Balance";
                radLabel3.BackColor = Color.LightYellow;
                for (int x = 0; x < 3; x++)
                {
                    vtemp = Convert.ToDouble(radGridView1.Rows[x].Cells[4].Value);
                    if ( vtemp < minVd*.92 || vtemp > minVd*1.08)
                    {
                        dbR = (minVd / Convert.ToDouble(radGridView1.Rows[x].Cells[3].Value) * 1000);
                        radGridView2.Rows[x].Cells[1].Value = dbR.ToString("F0");
                        dbRp = (dbR * Convert.ToDouble(radGridView1.Rows[x].Cells[2].Value)) / (Convert.ToDouble(radGridView1.Rows[x].Cells[2].Value) - dbR);
                        radGridView2.Rows[x].Cells[2].Value = dbRp.ToString("F0");
                    }
                    else // balance
                    {
                        radGridView2.Rows[x].Cells[1].Value = radGridView1.Rows[x].Cells[2].Value;
                        radGridView2.Rows[x].Cells[2].Value = "None";
                    }
                }
            }
            else
            {
                radLabel3.Text = ((maxVd - minVd) / maxVd * 100).ToString("F") + "% - Will Balance";
                radLabel3.BackColor = Color.LightGreen;
                for (int x = 0; x < 3; x++)
                {
                    radGridView2.Rows[x].Cells[1].Value = "None";
                    radGridView2.Rows[x].Cells[2].Value = "None";
                }
            }
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            doCalcs();
        }
    }
}
