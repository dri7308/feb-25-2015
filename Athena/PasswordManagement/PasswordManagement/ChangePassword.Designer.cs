namespace PasswordManagement
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.retypeNewPasswordRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.newPasswordRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.oldPasswordRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.userLevelRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.retypeNewPasswordRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.newPasswordRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.oldPasswordRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.okayRadButton = new Telerik.WinControls.UI.RadButton();
            this.userLevelRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.retypeNewPasswordRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPasswordRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPasswordRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retypeNewPasswordRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPasswordRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPasswordRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // retypeNewPasswordRadLabel
            // 
            this.retypeNewPasswordRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.retypeNewPasswordRadLabel.Location = new System.Drawing.Point(12, 115);
            this.retypeNewPasswordRadLabel.Name = "retypeNewPasswordRadLabel";
            this.retypeNewPasswordRadLabel.Size = new System.Drawing.Size(116, 17);
            this.retypeNewPasswordRadLabel.TabIndex = 38;
            this.retypeNewPasswordRadLabel.Text = "<html>Retype New Password</html>";
            // 
            // newPasswordRadLabel
            // 
            this.newPasswordRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.newPasswordRadLabel.Location = new System.Drawing.Point(12, 84);
            this.newPasswordRadLabel.Name = "newPasswordRadLabel";
            this.newPasswordRadLabel.Size = new System.Drawing.Size(79, 18);
            this.newPasswordRadLabel.TabIndex = 39;
            this.newPasswordRadLabel.Text = "New password";
            // 
            // oldPasswordRadLabel
            // 
            this.oldPasswordRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.oldPasswordRadLabel.Location = new System.Drawing.Point(12, 52);
            this.oldPasswordRadLabel.Name = "oldPasswordRadLabel";
            this.oldPasswordRadLabel.Size = new System.Drawing.Size(75, 18);
            this.oldPasswordRadLabel.TabIndex = 40;
            this.oldPasswordRadLabel.Text = "Old password";
            // 
            // userLevelRadLabel
            // 
            this.userLevelRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.userLevelRadLabel.Location = new System.Drawing.Point(12, 20);
            this.userLevelRadLabel.Name = "userLevelRadLabel";
            this.userLevelRadLabel.Size = new System.Drawing.Size(57, 18);
            this.userLevelRadLabel.TabIndex = 37;
            this.userLevelRadLabel.Text = "User Level";
            // 
            // retypeNewPasswordRadMaskedEditBox
            // 
            this.retypeNewPasswordRadMaskedEditBox.AutoSize = true;
            this.retypeNewPasswordRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retypeNewPasswordRadMaskedEditBox.Location = new System.Drawing.Point(133, 115);
            this.retypeNewPasswordRadMaskedEditBox.Name = "retypeNewPasswordRadMaskedEditBox";
            this.retypeNewPasswordRadMaskedEditBox.PasswordChar = '*';
            this.retypeNewPasswordRadMaskedEditBox.Size = new System.Drawing.Size(166, 18);
            this.retypeNewPasswordRadMaskedEditBox.TabIndex = 36;
            this.retypeNewPasswordRadMaskedEditBox.TabStop = false;
            this.retypeNewPasswordRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // newPasswordRadMaskedEditBox
            // 
            this.newPasswordRadMaskedEditBox.AutoSize = true;
            this.newPasswordRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPasswordRadMaskedEditBox.Location = new System.Drawing.Point(133, 84);
            this.newPasswordRadMaskedEditBox.Name = "newPasswordRadMaskedEditBox";
            this.newPasswordRadMaskedEditBox.PasswordChar = '*';
            this.newPasswordRadMaskedEditBox.Size = new System.Drawing.Size(166, 18);
            this.newPasswordRadMaskedEditBox.TabIndex = 35;
            this.newPasswordRadMaskedEditBox.TabStop = false;
            this.newPasswordRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // oldPasswordRadMaskedEditBox
            // 
            this.oldPasswordRadMaskedEditBox.AutoSize = true;
            this.oldPasswordRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oldPasswordRadMaskedEditBox.Location = new System.Drawing.Point(133, 52);
            this.oldPasswordRadMaskedEditBox.Name = "oldPasswordRadMaskedEditBox";
            this.oldPasswordRadMaskedEditBox.PasswordChar = '*';
            this.oldPasswordRadMaskedEditBox.Size = new System.Drawing.Size(166, 18);
            this.oldPasswordRadMaskedEditBox.TabIndex = 34;
            this.oldPasswordRadMaskedEditBox.TabStop = false;
            this.oldPasswordRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(163, 154);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(136, 34);
            this.cancelRadButton.TabIndex = 33;
            this.cancelRadButton.Text = "Exit";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // okayRadButton
            // 
            this.okayRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okayRadButton.Location = new System.Drawing.Point(12, 154);
            this.okayRadButton.Name = "okayRadButton";
            this.okayRadButton.Size = new System.Drawing.Size(130, 34);
            this.okayRadButton.TabIndex = 32;
            this.okayRadButton.Text = "Save Changes";
            this.okayRadButton.ThemeName = "Office2007Black";
            this.okayRadButton.Click += new System.EventHandler(this.okayRadButton_Click);
            // 
            // userLevelRadDropDownList
            // 
            this.userLevelRadDropDownList.DropDownAnimationEnabled = true;
            this.userLevelRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "38400";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "57600";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "115200";
            radListDataItem4.TextWrap = true;
            this.userLevelRadDropDownList.Items.Add(radListDataItem1);
            this.userLevelRadDropDownList.Items.Add(radListDataItem2);
            this.userLevelRadDropDownList.Items.Add(radListDataItem3);
            this.userLevelRadDropDownList.Items.Add(radListDataItem4);
            this.userLevelRadDropDownList.Location = new System.Drawing.Point(133, 16);
            this.userLevelRadDropDownList.Name = "userLevelRadDropDownList";
            this.userLevelRadDropDownList.ShowImageInEditorArea = true;
            this.userLevelRadDropDownList.Size = new System.Drawing.Size(166, 20);
            this.userLevelRadDropDownList.TabIndex = 31;
            this.userLevelRadDropDownList.ThemeName = "Office2007Black";
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(311, 204);
            this.ControlBox = false;
            this.Controls.Add(this.retypeNewPasswordRadLabel);
            this.Controls.Add(this.newPasswordRadLabel);
            this.Controls.Add(this.oldPasswordRadLabel);
            this.Controls.Add(this.userLevelRadLabel);
            this.Controls.Add(this.retypeNewPasswordRadMaskedEditBox);
            this.Controls.Add(this.newPasswordRadMaskedEditBox);
            this.Controls.Add(this.oldPasswordRadMaskedEditBox);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.okayRadButton);
            this.Controls.Add(this.userLevelRadDropDownList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(319, 236);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(319, 236);
            this.Name = "ChangePassword";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(319, 236);
            this.Text = "Change Password";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ChangePassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.retypeNewPasswordRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPasswordRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPasswordRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retypeNewPasswordRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPasswordRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oldPasswordRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadLabel retypeNewPasswordRadLabel;
        private Telerik.WinControls.UI.RadLabel newPasswordRadLabel;
        private Telerik.WinControls.UI.RadLabel oldPasswordRadLabel;
        private Telerik.WinControls.UI.RadLabel userLevelRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox retypeNewPasswordRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox newPasswordRadMaskedEditBox;
        private Telerik.WinControls.UI.RadMaskedEditBox oldPasswordRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton okayRadButton;
        private Telerik.WinControls.UI.RadDropDownList userLevelRadDropDownList;
    }
}

