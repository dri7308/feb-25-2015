﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using System.IO;

namespace PasswordManagement
{
    public class PasswordUtilities
    {
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string managerText = "Manager";
        private static string operatorText = "Operator";
        private static string viewerText = "Viewer";

        private static string doYouWantToIncreaseUserLevelQuestionText = "You are not logged in with a sufficient user level for this function\nDo you want to increase your user level?";
        private static string insufficientUserLevelText = "Insufficient User Level";
        private static string userLevelInsufficientOperationCancelledText = "User level selected was insufficient, selected operation cancelled.";
        private static string incorrectPasswordOperationCancelledText = "Incorrect password, selected operation cancelled";

        private static UserLevel currentUserLevel = UserLevel.Viewer;
        public static UserLevel CurrentUserLevel
        {
            get
            {
                return currentUserLevel;
            }
        }

        private static string managerPassword = "admin";
        public static string ManagerPassword
        {
            get
            {
                return managerPassword;
            }
        }

        private static string operatorPassword = "admin";
        public static string OperatorPassword
        {
            get
            {
                return operatorPassword;
            }
        }

        private static string viewerPassword = "admin";
        public static string ViewerPassword
        {
            get
            {
                return viewerPassword;
            }
        }

        private static bool passwordWasChanged = false;
        public static bool PasswordWasChanged
        {
            get
            {
                return passwordWasChanged;
            }
        }

        private static bool userLevelChanged = false;
        public static bool UserLevelChanged
        {
            get
            {
                return userLevelChanged;
            }
        }

        public static void ResetPasswordChangedToFalse()
        {
            passwordWasChanged = false;
        }

        public static bool SetPassword(UserLevel userLevel, string password)
        {
            bool success = true;
            try
            {
                if (userLevel == UserLevel.Manager)
                {
                    managerPassword = password;
                }
                else if (userLevel == UserLevel.Operator)
                {
                    operatorPassword = password;
                }
                else
                {
                    viewerPassword = password;
                }
                // save to file
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";
                string encrypt;
                StringBuilder passwords = new StringBuilder();
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Manager));
                passwords.Append(";");
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Operator));
                passwords.Append(";");
                passwords.Append(PasswordUtilities.GetPassword(UserLevel.Viewer));

                encrypt = FileUtilities.EncryptStringToBytes_Aes(passwords.ToString());
                StreamWriter writer = new StreamWriter(sFolder);
                writer.WriteLine(encrypt);
                writer.Close();
                //FileUtilities.WriteBytesToNewBinaryFile(ConversionMethods.ConvertStringToByteArray(passwords.ToString()), fullFilePath);
                PasswordUtilities.ResetPasswordChangedToFalse();

                passwordWasChanged = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.SetPassword(UserLevel, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static string GetPassword(UserLevel userLevel)
        {
            string password = string.Empty;
            if (userLevel == UserLevel.Manager)
            {
                password = managerPassword;
            }
            else if (userLevel == UserLevel.Operator)
            {
                password = operatorPassword;
            }
            else
            {
                password = viewerPassword;
            }
            return password;
        }

        public static bool CheckUserLevelAndCorrectItIfNecessaryAndDesired(Telerik.WinControls.UI.RadForm parentWindow, UserLevel inputUserLevel, string questionString, string captionString)
        {
            bool userLevelIsSufficient = false;
            try
            {
                // string userLevelAsString;
                // bool userWishesToChangeUserLevel = true;
                userLevelIsSufficient = (PasswordUtilities.currentUserLevel >= inputUserLevel);

                if (!userLevelIsSufficient)
                {
                    if (RadMessageBox.Show(parentWindow, questionString, captionString, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        using (EnterPassword enterPassword = new EnterPassword(inputUserLevel))
                        {
                            enterPassword.ShowDialog();
                            enterPassword.Hide();
                            if (enterPassword.PasswordForUserSelectedUserLevelWasCorrect)
                            {
                                userLevelChanged = true;
                                if (PasswordUtilities.currentUserLevel >= inputUserLevel)
                                {
                                    userLevelIsSufficient = true;
                                    // RadMessageBox.Show(this, "Selected operation will be performed.");
                                }
                                else
                                {
                                    RadMessageBox.Show(parentWindow, userLevelInsufficientOperationCancelledText);
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(parentWindow, incorrectPasswordOperationCancelledText);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(Telerik.WinControls.UI.RadForm, UserLevel, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return userLevelIsSufficient;
        }
        /// <summary>
        /// Checks the current security level against the value passed in, and if the current level is insufficient ask the user if he wants to enter the appropriate password.  Returns
        /// a boolean indicating if the security level upon leaving the function is sufficient.
        /// </summary>
        /// <returns></returns>
        public static bool CheckUserLevelAndCorrectItIfNecessaryAndDesired(Telerik.WinControls.UI.RadForm parentWindow, UserLevel inputUserLevel)
        {
            bool userLevelIsSufficient = false;
            try
            {
                userLevelIsSufficient = CheckUserLevelAndCorrectItIfNecessaryAndDesired(parentWindow, inputUserLevel, doYouWantToIncreaseUserLevelQuestionText, insufficientUserLevelText);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(Telerik.WinControls.UI.RadForm, UserLevel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return userLevelIsSufficient;
        }

        public static bool SetUserLevel(UserLevel inputUserLevel, string inputPassword)
        {
            bool correctPassword = false;
            try
            {
                string passwordForInputUserLevel;

                if (inputUserLevel == UserLevel.Viewer)
                {
                    passwordForInputUserLevel = viewerPassword;
                }
                else if (inputUserLevel == UserLevel.Operator)
                {
                    passwordForInputUserLevel = operatorPassword;
                }
                else
                {
                    passwordForInputUserLevel = managerPassword;
                }

                if (passwordForInputUserLevel.CompareTo(inputPassword) == 0)
                {
                    currentUserLevel = inputUserLevel;
                    correctPassword = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.SetUserLevel(UserLevel, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return correctPassword;
        }

        public static string GetStringEquivalentOfUserLevel(UserLevel inputUserLevel)
        {
            string userLevelAsString = string.Empty;
            try
            {
                if (inputUserLevel == UserLevel.Manager)
                {
                    userLevelAsString = managerText;
                }
                else if (inputUserLevel == UserLevel.Operator)
                {
                    userLevelAsString = operatorText;
                }
                else
                {
                    userLevelAsString = viewerText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return userLevelAsString;
        }

        public static UserLevel GetUserLevelEquivalentOfString(string inputUserLevelAsString)
        {
            UserLevel inputUserLevel = UserLevel.Viewer;
            try
            {
                if (inputUserLevelAsString.CompareTo(GetStringEquivalentOfUserLevel(UserLevel.Manager)) == 0)
                {
                    inputUserLevel = UserLevel.Manager;
                }
                else if (inputUserLevelAsString.CompareTo(GetStringEquivalentOfUserLevel(UserLevel.Operator)) == 0)
                {
                    inputUserLevel = UserLevel.Operator;
                }
                else if (inputUserLevelAsString.CompareTo(GetStringEquivalentOfUserLevel(UserLevel.Viewer)) == 0)
                {
                    inputUserLevel = UserLevel.Viewer;
                }
                else
                {
                    string errorMessage = "Error in PasswordUtilities.GetUserLevelEquivalentOfString(string)\nInput string had no equivalent UserType.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordUtilities.GetUserLevelEquivalentOfString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return inputUserLevel;
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                managerText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesManagerText", managerText, "", "", "");
                operatorText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesOperatorText", operatorText, "", "", "");
                viewerText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesViewerText", viewerText, "", "", "");
                doYouWantToIncreaseUserLevelQuestionText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesDoYouWantToIncreaseUserLevelQuestionText", doYouWantToIncreaseUserLevelQuestionText, htmlFontType, htmlStandardFontSize, "");
                insufficientUserLevelText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesInsufficientUserLevelText", insufficientUserLevelText, htmlFontType, htmlStandardFontSize, "");
                userLevelInsufficientOperationCancelledText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesUserLevelInsufficientOperationCancelledText", userLevelInsufficientOperationCancelledText, htmlFontType, htmlStandardFontSize, "");
                incorrectPasswordOperationCancelledText = LanguageConversion.GetStringAssociatedWithTag("PasswordUtilitiesIncorrectPasswordOperationCancelledText", incorrectPasswordOperationCancelledText, htmlFontType, htmlStandardFontSize, "");
               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
    public enum UserLevel
    {
        Viewer,
        Operator,
        Manager
    };
}
