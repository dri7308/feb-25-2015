namespace PasswordManagement
{
    partial class EnterPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnterPassword));
            this.passwordRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.userLevelRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.passwordRadMaskedEditBox = new Telerik.WinControls.UI.RadMaskedEditBox();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.okayRadButton = new Telerik.WinControls.UI.RadButton();
            this.userLevelRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadMaskedEditBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // passwordRadLabel
            // 
            this.passwordRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.passwordRadLabel.Location = new System.Drawing.Point(12, 59);
            this.passwordRadLabel.Name = "passwordRadLabel";
            this.passwordRadLabel.Size = new System.Drawing.Size(53, 18);
            this.passwordRadLabel.TabIndex = 38;
            this.passwordRadLabel.Text = "Password";
            // 
            // userLevelRadLabel
            // 
            this.userLevelRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.userLevelRadLabel.Location = new System.Drawing.Point(12, 21);
            this.userLevelRadLabel.Name = "userLevelRadLabel";
            this.userLevelRadLabel.Size = new System.Drawing.Size(57, 18);
            this.userLevelRadLabel.TabIndex = 37;
            this.userLevelRadLabel.Text = "User Level";
            // 
            // passwordRadMaskedEditBox
            // 
            this.passwordRadMaskedEditBox.AutoSize = true;
            this.passwordRadMaskedEditBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordRadMaskedEditBox.Location = new System.Drawing.Point(92, 59);
            this.passwordRadMaskedEditBox.Name = "passwordRadMaskedEditBox";
            this.passwordRadMaskedEditBox.PasswordChar = '*';
            this.passwordRadMaskedEditBox.Size = new System.Drawing.Size(166, 18);
            this.passwordRadMaskedEditBox.TabIndex = 36;
            this.passwordRadMaskedEditBox.TabStop = false;
            this.passwordRadMaskedEditBox.ThemeName = "Office2007Black";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(138, 101);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(120, 34);
            this.cancelRadButton.TabIndex = 35;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // okayRadButton
            // 
            this.okayRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okayRadButton.Location = new System.Drawing.Point(12, 101);
            this.okayRadButton.Name = "okayRadButton";
            this.okayRadButton.Size = new System.Drawing.Size(120, 34);
            this.okayRadButton.TabIndex = 34;
            this.okayRadButton.Text = "OK";
            this.okayRadButton.ThemeName = "Office2007Black";
            this.okayRadButton.Click += new System.EventHandler(this.okayRadButton_Click);
            // 
            // userLevelRadDropDownList
            // 
            this.userLevelRadDropDownList.DropDownAnimationEnabled = true;
            this.userLevelRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "38400";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "57600";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "115200";
            radListDataItem4.TextWrap = true;
            this.userLevelRadDropDownList.Items.Add(radListDataItem1);
            this.userLevelRadDropDownList.Items.Add(radListDataItem2);
            this.userLevelRadDropDownList.Items.Add(radListDataItem3);
            this.userLevelRadDropDownList.Items.Add(radListDataItem4);
            this.userLevelRadDropDownList.Location = new System.Drawing.Point(92, 17);
            this.userLevelRadDropDownList.Name = "userLevelRadDropDownList";
            this.userLevelRadDropDownList.ShowImageInEditorArea = true;
            this.userLevelRadDropDownList.Size = new System.Drawing.Size(166, 20);
            this.userLevelRadDropDownList.TabIndex = 33;
            this.userLevelRadDropDownList.ThemeName = "Office2007Black";
            // 
            // EnterPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(270, 159);
            this.Controls.Add(this.passwordRadLabel);
            this.Controls.Add(this.userLevelRadLabel);
            this.Controls.Add(this.passwordRadMaskedEditBox);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.okayRadButton);
            this.Controls.Add(this.userLevelRadDropDownList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(278, 182);
            this.MinimumSize = new System.Drawing.Size(278, 182);
            this.Name = "EnterPassword";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.MaxSize = new System.Drawing.Size(278, 182);
            this.Text = "Enter Password";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.EnterPassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadMaskedEditBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okayRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userLevelRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel passwordRadLabel;
        private Telerik.WinControls.UI.RadLabel userLevelRadLabel;
        private Telerik.WinControls.UI.RadMaskedEditBox passwordRadMaskedEditBox;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton okayRadButton;
        private Telerik.WinControls.UI.RadDropDownList userLevelRadDropDownList;
    }
}

