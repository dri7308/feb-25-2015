using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;
using System.IO;
using System.Text;

namespace PasswordManagement
{
    public partial class ChangePassword : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string passwordChangedForUserLevelText = "Password changed for user level";
        private static string newPasswordAndRetypedPasswordDontMatchText = "The new password and your retype of it do not match";
        private static string oldPasswordIsIncorrectText = "Old password is incorrect.";

        private static string changePasswordTitleText = "Change Password";
        private static string userLevelRadLabelText = "User Level";
        private static string oldPasswordRadLabelText = "Old Password";
        private static string newPasswordRadLabelText = "New Password";
        private static string retypeNewPasswordRadLabelText = "Retype New Password";
        private static string okayRadButtonText = "Save Changes";
        private static string cancelRadButtonText = "Exit";
       
        private UserLevel localUserLevel;

        public UserLevel LocalUserLevel
        {
            get
            {
                return localUserLevel;
            }
        }

        private string oldPassword;

        private string newPassword;

        public string NewPassword
        {
            get
            {
                return newPassword;
            }
        }

        private bool passwordChanged = false;

        public bool PasswordChanged
        {
            get
            {
                return passwordChanged;
            }
        }

        public ChangePassword()
        {
            InitializeComponent();
            localUserLevel = UserLevel.Manager;

            userLevelRadDropDownList.DataSource = new string[] { PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Operator), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Viewer) };
            userLevelRadDropDownList.Text = PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager);
            AcceptButton = okayRadButton;
            oldPasswordRadMaskedEditBox.Select();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public ChangePassword(UserLevel userLevel)
        {
            InitializeComponent();
            localUserLevel = userLevel;

            userLevelRadDropDownList.DataSource = new string[] { PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Operator), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Viewer) };
            userLevelRadDropDownList.Text = PasswordUtilities.GetStringEquivalentOfUserLevel(localUserLevel);
            AcceptButton = okayRadButton;
            oldPasswordRadMaskedEditBox.Select();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            AssignStringValuesToInterfaceObjects();
        }

        private void okayRadButton_Click(object sender, EventArgs e)
        {
            string localUserLevelAsString = string.Empty;

            if (userLevelRadDropDownList.Text.CompareTo(PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager)) == 0)
            {
                localUserLevel = UserLevel.Manager;                
            }
            else if (userLevelRadDropDownList.Text.CompareTo(PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Operator)) == 0)
            {
                localUserLevel = UserLevel.Operator;               
            }
            else
            {
                localUserLevel = UserLevel.Viewer;                
            }
            this.oldPassword = PasswordUtilities.GetPassword(localUserLevel);

            localUserLevelAsString = PasswordUtilities.GetStringEquivalentOfUserLevel(localUserLevel);
           
            if (oldPasswordRadMaskedEditBox.Text.CompareTo(oldPassword) == 0)
            {
                if (PasswordPolicy.IsValid(newPasswordRadMaskedEditBox.Text) == true)
                {
                    if (newPasswordRadMaskedEditBox.Text.CompareTo(retypeNewPasswordRadMaskedEditBox.Text) == 0)
                    {
                        newPassword = newPasswordRadMaskedEditBox.Text;

                        PasswordUtilities.SetPassword(localUserLevel, newPassword);

                        RadMessageBox.Show(this, passwordChangedForUserLevelText + " " + localUserLevelAsString);

                        passwordChanged = true;
                       

                    }
                    else
                    {
                        RadMessageBox.Show(this, newPasswordAndRetypedPasswordDontMatchText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, "Password must be a length of at least 8 characters" + Environment.NewLine + "and must have at one of the following:" + Environment.NewLine + "- Upper Case letter" + Environment.NewLine + "- Lower Case letter" + Environment.NewLine + "- Number" + Environment.NewLine + "- Special Character");
                }
            }
            else
            {
                RadMessageBox.Show(this, oldPasswordIsIncorrectText);
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
           


            // need to check if all passwords have been set and meet requirements that are stored in the password file
            try
            {
                //System.Diagnostics.Debugger.Break();
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup2.dll";
                string allPasswordsScrunchedTogether;
                string[] passwords;
                string decrypt;
                if (File.Exists(sFolder))
                {
                    // allPasswordsScrunchedTogether = ConversionMethods.ConvertByteArrayToString(FileUtilities.ReadBinaryFileAsBytes(fullFilePath));
                    StreamReader reader = new StreamReader(sFolder);
                    allPasswordsScrunchedTogether = reader.ReadLine();
                    reader.ReadLine();
                    reader.Close();
                    decrypt = FileUtilities.DecryptStringFromBytes_Aes(allPasswordsScrunchedTogether);
                    if ((allPasswordsScrunchedTogether != null) && (allPasswordsScrunchedTogether.Length > 0))
                    {
                        passwords = decrypt.Split(';');
                        if ((passwords != null) && (passwords.Length == 3))
                        {
                            PasswordUtilities.SetPassword(UserLevel.Manager, passwords[0]);
                            PasswordUtilities.SetPassword(UserLevel.Operator, passwords[1]);
                            PasswordUtilities.SetPassword(UserLevel.Viewer, passwords[2]);
                        }

                        if (PasswordPolicy.IsValid(passwords[0]) == false)
                        {
                            RadMessageBox.Show("Password Not Set for User Level: Manager");
                            return;
                        }

                        if (PasswordPolicy.IsValid(passwords[1]) == false)
                        {
                            RadMessageBox.Show("Password Not Set for User Level: Operator");
                            return;
                        }
                        if (PasswordPolicy.IsValid(passwords[2]) == false)
                        {
                            RadMessageBox.Show("Password Not Set for User Level: Viewer");
                            return;
                        }

                        // sets level to viewer.  to change user level must use menu item enter password

                        PasswordUtilities.SetUserLevel(UserLevel.Viewer, passwords[2]);
                        this.Close();
                        
                    }
                }
                else
                {
                    RadMessageBox.Show("Valid Passwords Must be entered for all user levels", "Warning");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PasswordManagement.cancelRadButton_Click()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }

            
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = changePasswordTitleText;

                userLevelRadLabel.Text = userLevelRadLabelText;
                oldPasswordRadLabel.Text = oldPasswordRadLabelText;
                newPasswordRadLabel.Text = newPasswordRadLabelText;
                retypeNewPasswordRadLabel.Text = retypeNewPasswordRadLabelText;
                okayRadButton.Text = okayRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ChangePassword.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                changePasswordTitleText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordTitleText", changePasswordTitleText, htmlFontType, htmlStandardFontSize, "");
                userLevelRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceUserLevelRadLabelText", userLevelRadLabelText, htmlFontType, htmlStandardFontSize, "");
                oldPasswordRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceOldPasswordRadLabelText", oldPasswordRadLabelText, htmlFontType, htmlStandardFontSize, "");
                newPasswordRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceNewPasswordRadLabelText", newPasswordRadLabelText, htmlFontType, htmlStandardFontSize, "");
                retypeNewPasswordRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceRetypeNewPasswordRadLabelText", retypeNewPasswordRadLabelText, htmlFontType, htmlStandardFontSize, "");
                okayRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceOkayRadButtonText", okayRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");

                passwordChangedForUserLevelText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordPasswordChangedForUserLevelText", passwordChangedForUserLevelText, htmlFontType, htmlStandardFontSize, "");
                newPasswordAndRetypedPasswordDontMatchText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordNewPasswordAndRetypedPasswordDontMatchText", newPasswordAndRetypedPasswordDontMatchText, htmlFontType, htmlStandardFontSize, "");
                oldPasswordIsIncorrectText = LanguageConversion.GetStringAssociatedWithTag("ChangePasswordOldPasswordIsIncorrectText", oldPasswordIsIncorrectText, htmlFontType, htmlStandardFontSize, "");             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ChangePassword.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
    }
}