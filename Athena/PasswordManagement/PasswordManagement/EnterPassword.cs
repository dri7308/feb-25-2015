using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;

namespace PasswordManagement
{
    public partial class EnterPassword : Telerik.WinControls.UI.RadForm
    {
        private UserLevel inputUserLevel;
        private string passwordForSelectedUserLevel;

        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string enterPasswordTitleText = "Enter Password";
        private static string userLevelRadLabelText = "User Level";
        private static string passwordRadLabelText = "Password";
        private static string okayRadButtonText = "OK";
        private static string cancelRadButtonText = "Cancel";

        private static string downgradingUserLevelWarningText = "<html><font=Microsoft Sans Serif>You are either downgrading your user level or not selecting a sufficient user level<br>for the last operation you attempted.  Is this what you want to do?</html>";
        private static string downgradeUserLevelAsQuestionText = "Downgrade user level?";
        private static string passwordAcceptedText = "Password Accepted.  User level is now";
        private static string incorrectPasswordText = "Incorrect password for selected user level.";

        private UserLevel userSelectedUserLevel;
        public UserLevel UserSelectedUserLevel
        {
            get
            {
                return userSelectedUserLevel;
            }
        }

        private bool passwordForUserSelectedUserLevelWasCorrect = false;
        public bool PasswordForUserSelectedUserLevelWasCorrect
        {
            get
            {
                return passwordForUserSelectedUserLevelWasCorrect;
            }
        }

        public EnterPassword(UserLevel userLevel)
        {
            InitializeComponent();
            inputUserLevel = userLevel;

            userLevelRadDropDownList.DataSource = new string[] { PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Operator), PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Viewer) };
            userLevelRadDropDownList.Text = PasswordUtilities.GetStringEquivalentOfUserLevel(inputUserLevel);
            AcceptButton = okayRadButton;
            passwordRadMaskedEditBox.Select();
            this.StartPosition = FormStartPosition.CenterParent;
       }

        private void EnterPassword_Load(object sender, EventArgs e)
        {
            AssignStringValuesToInterfaceObjects();
        }

        //private bool CheckPassword()
        //{
        //    bool correctPassword = false;

        //    if (userLevelRadDropDownList.Text.Trim().CompareTo(PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Viewer)) == 0)
        //    {
        //        passwordForSelectedUserLevel = PasswordUtilities.ViewerPassword;
        //        userSelectedUserLevel = UserLevel.Viewer;
        //    }
        //    else if (userLevelRadDropDownList.Text.Trim().CompareTo(PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Operator)) == 0)
        //    {
        //        passwordForSelectedUserLevel = PasswordUtilities.OperatorPassword;
        //        userSelectedUserLevel = UserLevel.Operator;
        //    }
        //    else if (userLevelRadDropDownList.Text.Trim().CompareTo(PasswordUtilities.GetStringEquivalentOfUserLevel(UserLevel.Manager)) == 0)
        //    {
        //        passwordForSelectedUserLevel = PasswordUtilities.ManagerPassword;
        //        userSelectedUserLevel = UserLevel.Manager;
        //    }

        //    if (passwordForSelectedUserLevel.CompareTo(passwordRadMaskedEditBox.Text) == 0)
        //    {
        //        correctPassword = true;
        //    }

        //    return correctPassword;
        //}

        private void okayRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool continueWithPasswordCheck = false;
                
                UserLevel selectedUserLevel = PasswordUtilities.GetUserLevelEquivalentOfString(this.userLevelRadDropDownList.Text);
                if (selectedUserLevel < inputUserLevel)
                {
                    if (RadMessageBox.Show(this, downgradingUserLevelWarningText, downgradeUserLevelAsQuestionText, MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        continueWithPasswordCheck = true;
                    }
                }
                else
                {
                    continueWithPasswordCheck = true;
                }
                if (continueWithPasswordCheck)
                {
                    if (PasswordUtilities.SetUserLevel(selectedUserLevel, this.passwordRadMaskedEditBox.Text))
                    {
                       // RadMessageBox.Show(this, passwordAcceptedText + " " + this.userLevelRadDropDownList.Text);
                        passwordForUserSelectedUserLevelWasCorrect = true;
                        this.Close();
                    }
                    else
                    {
                        RadMessageBox.Show(this, incorrectPasswordText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in EnterPassword.GetStringEquivalentOfUserLevel(UserLevel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            passwordForUserSelectedUserLevelWasCorrect = false;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = enterPasswordTitleText;

                userLevelRadLabel.Text = userLevelRadLabelText;
                passwordRadLabel.Text = passwordRadLabelText;
                okayRadButton.Text = okayRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in EnterPassword.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                enterPasswordTitleText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordTitleText", enterPasswordTitleText, htmlFontType, htmlStandardFontSize, "");
                userLevelRadLabelText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordInterfaceUserLevelRadLabelText", userLevelRadLabelText, htmlFontType, htmlStandardFontSize, "");
                passwordRadLabelText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordInterfacePasswordRadLabelText", passwordRadLabelText, htmlFontType, htmlStandardFontSize, "");

                okayRadButtonText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordInterfaceOkayRadButtonText", okayRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");

                downgradingUserLevelWarningText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordDowngradingUserLevelWarningText", downgradingUserLevelWarningText, htmlFontType, htmlStandardFontSize, "");
                downgradeUserLevelAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordDowngradeUserLevelAsQuestionText", downgradeUserLevelAsQuestionText, htmlFontType, htmlStandardFontSize, "");
                passwordAcceptedText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordPasswordAcceptedText", passwordAcceptedText, htmlFontType, htmlStandardFontSize, "");
                incorrectPasswordText = LanguageConversion.GetStringAssociatedWithTag("EnterPasswordIncorrectPasswordText", incorrectPasswordText, htmlFontType, htmlStandardFontSize, ""); 
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in EnterPassword.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

       

    }
}
