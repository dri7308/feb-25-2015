using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;

namespace MainMonitorUtilities
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        GridViewTemplate vibrationThresholdGridViewTemplate;
        GridViewTemplate analogInThresholdGridViewTemplate;
        GridViewTemplate currentThresholdGridViewTemplate;
        GridViewTemplate voltageThresholdGridViewTemplate;
        GridViewTemplate humidityThresholdGridViewTemplate;
        GridViewTemplate temperatureThresholdGridViewTemplate;
        GridViewTemplate chassisTemperatureThresholdGridViewTemplate;
        
        GridViewRelation vibrationGridViewRelation;
        GridViewRelation analogInGridViewRelation;
        GridViewRelation currentGridViewRelation;
        GridViewRelation voltageGridViewRelation;
        GridViewRelation humidityGridViewRelation;
        GridViewRelation temperatureGridViewRelation;
        GridViewRelation chassisTemperatureGridViewRelation;
       
        //List<ThresholdData> vibrationThresholdDataList;
        //List<ThresholdData> pressureThresholdDataList;
        //List<ThresholdData> currentThresholdDataList;
        //List<ThresholdData> voltageThresholdDataList;
        //List<ThresholdData> humidityThresholdDataList;
        //List<ThresholdData> temperatureThresholdDataList;
        //List<ThresholdData> chassisTemperatureThresholdDataList;

        //List<VibrationData> vibrationDataList;
        //List<PressureData> pressureDataList;
        //List<CurrentData> currentDataList;
        //List<VoltageData> voltageDataList;
        //HumidityData humidityData;
        //List<TemperatureData> temperatureDataList;
        //ChassisTemperatureData chassisTemperatureData;

        Main_Configuration workingConfiguration;
        Main_Configuration uneditedWorkingConfiguration;
        //Main_Configuration configurationFromDatabase;
        //Main_Configuration configurationFromDevice;

        //ConfigurationDisplayed configurationBeingDisplayed = ConfigurationDisplayed.None;

        List<MainConfigUISystemConfigurationData> systemConfigurationList;

        List<TransferSetupData> transferSetupDataList;

        private static List<Main_Config_ConfigurationRoot> availableConfigurations;
        private List<Main_Config_ConfigurationRoot> templateConfigurations;

        private static Monitor monitor = null;
        private bool downloadWasInProgress;

        private bool configurationError;

        private int tableHeaderHeight = 50;
        private int entryTypeWidth = 120;
        private int sensorNumberWidth = 60;
        private int toSaveWidth = 120;
        private int enableEntryWidth = 65;
        private int sensitivityWidth = 80;

        private int availableConfigurationsSelectedIndex;
        private int templateConfigurationsSelectedIndex;

        int readDelayInMicroseconds;

        /// <summary>
        ///  just for testing to find values I can't figure out in the configuration
        /// </summary>
        private Int16[] previousConfigurationAsArray;
        /// <summary>
        ///  just for testing to find values I can't figure out in the configuration
        /// </summary>
        private Int16[] workingConfigurationAsArray;

        private static string htmlPrefix = "<html>";
        private static string htmlSuffix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string mainMonitorConfigurationTitleText = "Main Monitor Configuration";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText = "Main Monitor Configuration - Displaying Configuration from Database";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText = "Main Monitor Configuration - Displaying Configuration from Device";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText = "Main Monitor Configuration - Displaying Working Copy of Configuration from Device";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText = "Main Monitor Configuration - Displaying Working Copy of Configuration from Database";
        // private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText = "Main Monitor Configuration - Displaying Working Copy of Configuration from Initialization";

        private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Working Copy<br>of Configuration<br>to Database</html>";
       // private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device</html>";
        private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
       // private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
      //  private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        //private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        //private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        //private static string currentConfigurationRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        //private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        //private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";
        //private static string initializeWorkingConfigurationRadButtonText = "Initialize Working Configuration";

        private static string emptyCellErrorMessage = "There is a value missing from the grid.";
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        private static string couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = "Could not find the current device configuration, displaying an empty configuration.";
      //  private static string couldNotFindCurrentConfigurationDisplayingMostRecentText = "Could not find the current device configuration, displaying most recent configuration saved.";
      //  private static string couldNotFindAnyDeviceConfigurationText = "Could not find any device configurations in the database.";
        private static string failedToUploadConfigurationText = "Failed to upload the configuration for some reason.";
        private static string errorSendingPasswordToDeviceText = "Error in sending the password to the device.\nPlease check the connection.";
      //  private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
     //   private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";
      //  private static string currentConfigurationNotSavedWarningText = "You have made changes to the working copy that have not been saved.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";

       // private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";

        private static string offRelayText = "off";
        private static string onRelayText = "on";
        private static string externalDevicesRelayText = "External devices";
        private static string nullDeviceRelayText = "null device";

        private static string generalSettingsRadPageViewPageText = "<html><font=Microsoft Sans Serif>General Settings</html>";
        private static string monitoringRadGroupBoxText = "<html><font=Microsoft Sans Serif>Monitoring</html>";
        private static string enableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Enable</html>";
        private static string disableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Disable</html>";
        private static string frequencyRadGroupBoxText = "<html><font=Microsoft Sans Serif>Frequency</html>";
        private static string hertzRadLabelText = "<html><font=Microsoft Sans Serif>Hz.</html>";
        private static string versionRadGroupBoxText = "<html><font=Microsoft Sans Serif>Firmware Version</html>";
        private static string boardNameRadGroupBoxText = "<html><font=Microsoft Sans Serif>Board Name</html>";
        private static string connectionParametersRadGroupBoxText = "<html><font=Microsoft Sans Serif>Connection Parameters</html>";
        private static string modbusAddressRadLabelText = "<html><font=Microsoft Sans Serif>Modbus Address</html>";
        private static string serialConnectionRadGroupBoxText = "<html><font=Microsoft Sans Serif>RS 485</html>";
        private static string serialBaudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        private static string ethernetRadGroupBoxText = "<html><font=Microsoft Sans Serif>Ethernet</html>";
        private static string ethernetBaudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        private static string ipAddressRadLabelText = "<html><font=Microsoft Sans Serif>I.P. Address</html>";
        private static string relayRadGroupBoxText = "<html><font=Microsoft Sans Serif>Relay</html>";
        private static string generalSettingsAlarmRadLabelText = "<html><font=Microsoft Sans Serif>Alarm</html>";
        private static string generalSettingsWarningRadLabelText = "<html><font=Microsoft Sans Serif>Warning</html>";
        private static string modeSaveRadGroupBoxText = "<html><font=Microsoft Sans Serif>Mode Save</html>";
        private static string stepRadLabelText = "<html><font=Microsoft Sans Serif>Step</html>";
        private static string minutesRadLabelText = "<html><font=Microsoft Sans Serif>minutes</html>";
        private static string useDiagnosticResultsRadCheckBoxText = "<html><font=Microsoft Sans Serif>Take into account<br>results of diagnostic</html>";
        private static string otherOptionsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Other options</html>";
        private static string enableCurrentSignatureAnalysisRadCheckBoxText = "<html><font=Microsoft Sans Serif>Enable current signature analysis</html>";
        private static string allowDirectAccessRadCheckBoxText = "<html><font=Microsoft Sans Serif>Allow direct access to the<br>internal modules over RS 485</html>";

        private static string ethernetProtocolRadLabelText = "Protocol";

        private static string systemConfigurationRadPageViewPageText = "System Configuration";
      
        private static string analogInputsRadPageViewPageText = "Analog Inputs";
        private static string controlParametersRadGroupBoxText = "Control Parameters";
        private static string vibrationRadRadioButtonText = "Vibration";
        private static string analogInRadRadioButtonText = "Analog In";
        private static string currentRadRadioButtonText = "Current";
        private static string voltageRadRadioButtonText = "Voltage";
        private static string humidityRadRadioButtonText = "Humidity";
        private static string temperatureRadRadioButtonText = "Temperature";
        private static string chassisTemperatureRadRadioButtonText = "<html><font=Microsoft Sans Serif>Chassis<br>Temperature</html>";
       
        private static string calibrationRadPageViewPageText = "Calibration";
        private static string dataTransferRadPageViewPageText = "Data Transfer";

       // private static string saveWorkingConfigurationToDatabaseQuestionText = "Do you want to save the working configuration to the database?  You will lose it otherwise.";

        private static string deviceInteractionGroupBoxText = "Device Interaction";
        private static string databaseInteractionGroupBoxText = "Database Interaction";

        private static string noTemplateConfigurationsAvailable = "No template configurations are available";

        private static string templateConfigurationsRadGroupBoxText = "Template Configurations";
        private static string copySelectedConfigurationRadButtonText = "<html>Copy Selected<br>Configuration to<br>Database</html>";

        private static string availableConfigurationsHeaderText = "Date Added                        Description";
        private static string noConfigurationsSavedInDbText = "None Saved in DB";

        private static string deleteSelectedConfigurationRadButtonText = "<html>Delete Selected<br>Configuration from<br>Database</html>";

        private static string currentConfigName = "Current Device Configuration";
        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }

        private string dbConnectionString;

        bool systemConfigurationIsCorrect = false;

        private string serialPort;
        private int baudRate;

        private string templateDbConnectionString;

        private ProgramBrand programBrand;
        private static ProgramType programType;

        private string nameOfLastConfigurationLoadedFromDatabase = string.Empty;

        //private bool workingCopyIsFromDevice = false;
        //private bool workingCopyIsFromDatabase = false;
        //private bool workingCopyIsFromInitialization = false;

        public Main_MonitorConfiguration(ProgramBrand inputProgramBrand, ProgramType inputProgramType, Monitor inputMonitor, string inputSerialPort, int inputBaudRate, bool inputDownloadInProgress, string inputDbConnectionString, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();
                AssignStringValuesToInterfaceObjects();

                this.programBrand = inputProgramBrand;
                Main_MonitorConfiguration.programType = inputProgramType;
                Main_MonitorConfiguration.monitor = inputMonitor;
                this.serialPort = inputSerialPort;
                this.baudRate = inputBaudRate;
                this.downloadWasInProgress = inputDownloadInProgress;
                this.dbConnectionString = inputDbConnectionString;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;

//                if (inputMonitor != null)
//                {
//                    string connectionType = this.monitor.ConnectionType.Trim();
//                    if (connectionType.CompareTo("USB") == 0)
//                    {
//                        this.readDelayInMicroseconds = MainDisplay.directConnectionUSBReadDelay;
//                    }
//                    else if (connectionType.CompareTo("SOE") == 0)
//                    {
//                        this.readDelayInMicroseconds = MainDisplay.directConnectionEthernetReadDelay;
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_MonitorConfiguration.Main_MonitorConfiguration(Monitor, bool)\nInput Monitor was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_MonitorConfiguration(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void Main_MonitorConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                this.UseWaitCursor = true;
                this.systemConfigurationList = new List<MainConfigUISystemConfigurationData>();

                //DisableWorkingRadRadioButtons();
                //DisableFromDatabaseRadRadioButtons();
                //DisableFromDeviceRadRadioButtons();
                DisableProgressBars();

                //DisableCopyDatabaseConfigurationRadButtons();
                //DisableCopyDeviceConfigurationRadButtons();

                InitializationsForDataTransferVariables();

                InitializeDataTransferMasterDeviceRegisterNamesList();
                InitializeDataTransferADMSlaveDeviceRegisterNamesList();
                InitializeDataTransferBHMSlaveDeviceRegisterNamesList();
                InitializeDataTransferPDMSlaveDeviceRegisterNamesList();

                InitializeSystemConfigurationRadGridView();
                InitializeAnalogInputsRadGridViews();
                InitializeAllThresholdRadGridViews();
                InitializeCalibrationRadGridView();
                InitializeDataTransferRadGridView();

                EstablishAllParentChildGridRelations();
                SetAllAnalogInputsGridViewLocationAndSize();

                // GetConfigurationInformation();

                AddEmptyRowsToTheSystemConfigurationGrid();
                AddEmptyRowsToAllAnalogInputsGrids();
                AddEmptyRowsToTheCalibrationGrid();
                AddEmptyRowsToTheDataTransferGrid();

                CollapseGridViewForAllAnalogInputsGrids();

                this.transferSetupDataList = new List<TransferSetupData>();
                //InitializeDataStorageLists();
                //InitializeThresholdDataStorageLists();

                this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                serialBaudRateRadDropDownList.DataSource = new string[] { "9600", "38400", "57600", "115200" };
                ethernetBaudRateRadDropDownList.DataSource = new string[] { "9600", "38400", "57600", "115200", "921600" };
                ethernetProtocolRadDropDownList.DataSource = new string[] { "RTU", "TCP" };
                relayAlarmRadDropDownList.DataSource = new string[] { offRelayText, onRelayText, externalDevicesRelayText, nullDeviceRelayText};
                relayWarningRadDropDownList.DataSource = new string[] { offRelayText, onRelayText, externalDevicesRelayText, nullDeviceRelayText };

                // disable program device buttons
                programDeviceGeneralSettingsTabRadButton.Enabled = false;
                programDeviceAnalogInputsTabRadButton.Enabled = false;
                programDeviceCalibrationTabRadButton.Enabled = false;
                programDeviceDataTransferTabRadButton.Enabled = false;
                programDeviceSystemConfigurationTabRadButton.Enabled = false;
                
                /// In the original code, this was a "using" statement.  However, this was considered an error here, and apparently
                /// the using statement does nothing for the MonitorInterfaceDB type, as it doesn't have a Dispose() method anyway.

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    LoadAvailableConfigurations(localDB);
                    if (Main_MonitorConfiguration.monitor != null)
                    {
                        this.workingConfiguration = LoadCurrentDeviceConfigurationFromDatabase(Main_MonitorConfiguration.monitor.ID, localDB);
                        if (this.workingConfiguration != null)
                        {
                            this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
                            if (!AddDataToAllTransientInterfaceObjects(this.workingConfiguration))
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(this, dataTransferContainsDepricatedDestinationRegister);
                            }
                            //EnableCopyDatabaseConfigurationRadButtons();
                            //EnableFromDatabaseRadRadioButtons();
                            //SetFromDatabaseRadRadioButtonState();
                        }
                        else
                        {
                            if (Main_MonitorConfiguration.programType != ProgramType.TemplateEditor)
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(this, couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText);
                            }
                            this.workingConfiguration = new Main_Configuration();
                            this.workingConfiguration.InitializeDataToZeroes();
                            this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
                            AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
                        }
                    }
                }
                if (Main_MonitorConfiguration.programType != ProgramType.TemplateEditor)
                {
                    using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                    {
                        LoadTemplateConfigurations(templateDB);
                    }
                }

                this.enableCurrentSignatureAnalysisRadCheckBox.Enabled = false;

                this.firmwareVersionRadTextBox.ReadOnly = true;
                // this.objectNameRadTextBox.ReadOnly = true;

                serialBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                ethernetBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                relayAlarmRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                relayWarningRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

                SetAllRadDropDownListsToReadOnly();

                configurationRadPageView.SelectedPage = generalSettingsRadPageViewPage;

                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;

                if (Main_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    templateConfigurationsGeneralSettingsTabRadGroupBox.Visible = false;
                    templateConfigurationsSystemConfigurationTabRadGroupBox.Visible = false;
                    templateConfigurationsAnalogInputsTabRadGroupBox.Visible = false;
                    templateConfigurationsCalibrationTabRadGroupBox.Visible = false;
                    templateConfigurationsDataTransferTabRadGroupBox.Visible = false;

                    deviceInteractionGeneralSettingsTabRadGroupBox.Visible = false;
                    deviceInteractionSystemConfigurationTabRadGroupBox.Visible = false;
                    deviceInteractionAnalogInputsTabRadGroupBox.Visible = false;
                    deviceInteractionCalibrationTabRadGroupBox.Visible = false;
                    deviceInteractionDataTransferTabRadGroupBox.Visible = false;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_MonitorConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void Main_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {

                WriteAllInterfaceDataToWorkingConfiguration();
                
                if (this.workingConfiguration != null)
                {
                    //if (this.lastConfigurationLoadedWasTheWorkingConfiguration)
                    //{
                    //    if (RadMessageBox.Show(this, saveWorkingConfigurationToDatabaseQuestionText, "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    //    {
                    //        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    //        {
                    //            SaveWorkingConfigurationToDatabase(localDB);
                    //        }
                    //    }
                    //}
                    //else 
                    if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                    {
                        RadMessageBox.SetThemeName("office2007BlackTheme");
                        if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedExitWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_MonitorConfiguration_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllRadDropDownListsToReadOnly()
        {
            try
            {
                this.serialBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.ethernetBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.ethernetProtocolRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.relayAlarmRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.relayWarningRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAllRadDropDownListsToReadOnly()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = mainMonitorConfigurationTitleText;

                generalSettingsRadPageViewPage.Text = generalSettingsRadPageViewPageText;
                monitoringRadGroupBox.Text = monitoringRadGroupBoxText;
                enableMonitoringRadRadioButton.Text = enableMonitoringRadRadioButtonText;
                disableMonitoringRadRadioButton.Text = disableMonitoringRadRadioButtonText;
                frequencyRadGroupBox.Text = frequencyRadGroupBoxText;
                hertzRadLabel.Text = hertzRadLabelText;
                versionRadGroupBox.Text = versionRadGroupBoxText;
                boardNameRadGroupBox.Text = boardNameRadGroupBoxText;
                connectionParametersRadGroupBox.Text = connectionParametersRadGroupBoxText;
                modbusAddressRadLabel.Text = modbusAddressRadLabelText;
                serialConnectionRadGroupBox.Text = serialConnectionRadGroupBoxText;
                serialBaudRateRadLabel.Text = serialBaudRateRadLabelText;
                ethernetRadGroupBox.Text = ethernetRadGroupBoxText;
                ethernetBaudRateRadLabel.Text = ethernetBaudRateRadLabelText;
                ipAddressRadLabel.Text = ipAddressRadLabelText;
                relayRadGroupBox.Text = relayRadGroupBoxText;
                generalSettingsAlarmRadLabel.Text = generalSettingsAlarmRadLabelText;
                generalSettingsWarningRadLabel.Text = generalSettingsWarningRadLabelText;
                modeSaveRadGroupBox.Text = modeSaveRadGroupBoxText;
                stepRadLabel.Text = stepRadLabelText;
                minutesRadLabel.Text = minutesRadLabelText;
                useDiagnosticResultsRadCheckBox.Text = useDiagnosticResultsRadCheckBoxText;
                otherOptionsRadGroupBox.Text = otherOptionsRadGroupBoxText;
                enableCurrentSignatureAnalysisRadCheckBox.Text = enableCurrentSignatureAnalysisRadCheckBoxText;
                allowDirectAccessRadCheckBox.Text = allowDirectAccessRadCheckBoxText;
                availableConfigurationsGeneralSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveWorkingConfigurationGeneralSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
                //saveDeviceConfigurationGeneralSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceGeneralSettingsTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceGeneralSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                //deleteConfigurationGeneralSettingsTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectGeneralSettingsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceGeneralSettingsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseGeneralSettingsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //workingGeneralSettingsTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
                //copyDeviceConfigurationGeneralSettingsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationGeneralSettingsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationGeneralSettingsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                ethernetProtocolRadLabel.Text = ethernetProtocolRadLabelText;

                systemConfigurationRadPageViewPage.Text = systemConfigurationRadPageViewPageText;
                availableConfigurationsSystemConfigurationTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveWorkingConfigurationSystemConfigurationTabRadButton.Text = saveCurrentConfigurationRadButtonText;
               // saveDeviceConfigurationSystemConfigurationTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceSystemConfigurationTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceSystemConfigurationTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                //deleteConfigurationSystemConfigurationTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectSystemConfigurationTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceSystemConfigurationTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseSystemConfigurationTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //workingSystemConfigurationTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
                //copyDeviceConfigurationSystemConfigurationTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationSystemConfigurationTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationSystemConfigurationTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                analogInputsRadPageViewPage.Text = analogInputsRadPageViewPageText;
                controlParametersRadGroupBox.Text = controlParametersRadGroupBoxText;
                vibrationRadRadioButton.Text = vibrationRadRadioButtonText;
                analogInRadRadioButton.Text = analogInRadRadioButtonText;
                currentRadRadioButton.Text = currentRadRadioButtonText;
                voltageRadRadioButton.Text = voltageRadRadioButtonText;
                humidityRadRadioButton.Text = humidityRadRadioButtonText;
                temperatureRadRadioButton.Text = temperatureRadRadioButtonText;
                chassisTemperatureRadRadioButton.Text = chassisTemperatureRadRadioButtonText;
                availableConfigurationsAnalogInputsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveWorkingConfigurationAnalogInputsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
                //saveDeviceConfigurationAnalogInputsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseAnalogInputsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceAnalogInputsTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceAnalogInputsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                //deleteConfigurationAnalogInputsTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectAnalogInputsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceAnalogInputsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseAnalogInputsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //workingAnalogInputsTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
                //copyDeviceConfigurationAnalogInputsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationAnalogInputsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationAnalogInputsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                calibrationRadPageViewPage.Text = calibrationRadPageViewPageText;
                availableConfigurationsCalibrationTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveWorkingConfigurationCalibrationTabRadButton.Text = saveCurrentConfigurationRadButtonText;
                //saveDeviceConfigurationCalibrationTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseCalibrationTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceCalibrationTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceCalibrationTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                //deleteConfigurationCalibrationTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectCalibrationTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceCalibrationTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseCalibrationTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //workingCalibrationTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
                //copyDeviceConfigurationCalibrationTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationCalibrationTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationCalibrationTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                dataTransferRadPageViewPage.Text = dataTransferRadPageViewPageText;
                availableConfigurationsDataTransferTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
                saveWorkingConfigurationDataTransferTabRadButton.Text = saveCurrentConfigurationRadButtonText;
               // saveDeviceConfigurationDataTransferTabRadButton.Text = saveDeviceConfigurationRadButtonText;
                loadConfigurationFromDatabaseDataTransferTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
                programDeviceDataTransferTabRadButton.Text = programDeviceRadButtonText;
                loadConfigurationFromDeviceDataTransferTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
                //deleteConfigurationDataTransferTabRadButton.Text = deleteConfigurationRadButtonText;
                //configurationViewSelectDataTransferTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
                //fromDeviceDataTransferTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
                //fromDatabaseDataTransferTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
                //workingDataTransferTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
                //copyDeviceConfigurationDataTransferTabRadButton.Text = copyDeviceConfigurationRadButtonText;
                //copyDatabaseConfigurationDataTransferTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
                //initializeWorkingConfigurationDataTransferTabRadButton.Text = initializeWorkingConfigurationRadButtonText;

                databaseInteractionGeneralSettingsTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionSystemConfigurationTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionAnalogInputsTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionCalibrationTabRadGroupBox.Text = databaseInteractionGroupBoxText;
                databaseInteractionDataTransferTabRadGroupBox.Text = databaseInteractionGroupBoxText;

                deviceInteractionGeneralSettingsTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionSystemConfigurationTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionAnalogInputsTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionCalibrationTabRadGroupBox.Text = deviceInteractionGroupBoxText;
                deviceInteractionDataTransferTabRadGroupBox.Text = deviceInteractionGroupBoxText;

                templateConfigurationsGeneralSettingsTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsSystemConfigurationTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsAnalogInputsTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsCalibrationTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;
                templateConfigurationsDataTransferTabRadGroupBox.Text = templateConfigurationsRadGroupBoxText;

                copySelectedConfigurationGeneralSettingsTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationSystemConfigurationTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationAnalogInputsTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationCalibrationTabRadButton.Text = copySelectedConfigurationRadButtonText;
                copySelectedConfigurationDataTransferTabRadButton.Text = copySelectedConfigurationRadButtonText;

                deleteSelectedConfigurationGeneralSettingsTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
                deleteSelectedConfigurationSystemConfigurationTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
                deleteSelectedConfigurationAnalogInputsTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
                deleteSelectedConfigurationCalibrationTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
                deleteSelectedConfigurationDataTransferTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                duplicateModbusAddressDetectedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDuplicateModbusAddressDetectedText", duplicateModbusAddressDetectedText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModbusAddressOutOfRangeText", modbusAddressOutOfRangeText, htmlFontType, htmlStandardFontSize, "");
                monitorNumberSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorNumberSystemConfigGridViewText", monitorNumberSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
                monitorTypeSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeSystemConfigGridViewText", monitorTypeSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
                monitorTypeNoneSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeNoneSystemConfigGridViewText", monitorTypeNoneSystemConfigGridViewText, "", "", "");
                monitorTypeMainSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeMainSystemConfigGridViewText", monitorTypeMainSystemConfigGridViewText, "", "", "");               
                monitorTypeBushingSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeBushingSystemConfigGridViewText", monitorTypeBushingSystemConfigGridViewText, "", "", "");
                monitorTypePDMonitorSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypePDMonitorSystemConfigGridViewText", monitorTypePDMonitorSystemConfigGridViewText, "", "", "");
                monitorTypeAcousticSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeAcousticSystemConfigGridViewText", monitorTypeAcousticSystemConfigGridViewText, "", "", "");
                modbusAddressSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModbusAddressSystemConfigGridViewText", modbusAddressSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
                baudRateSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationBaudRateSystemConfigGridViewText", baudRateSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
                dataTransferSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDataTransferSystemConfigGridViewText", dataTransferSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");

                //failedToSaveConfigurationToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToSaveConfigurationToDatabaseText", failedToSaveConfigurationToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //noCurrentConfigurationDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoCurrentConfigurationDefinedText", noCurrentConfigurationDefinedText, htmlFontType, htmlStandardFontSize, "");
                //noConfigurationLoadedFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoConfigurationLoadedFromDeviceText", noConfigurationLoadedFromDeviceText, htmlFontType, htmlStandardFontSize, "");
                configurationBeingSavedIsFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationBeingSavedIsFromDeviceText", configurationBeingSavedIsFromDeviceText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationSavedToDatabaseText", deviceConfigurationSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationNotSavedToDatabaseText", deviceConfigurationNotSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //errorInConfigurationLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationErrorInConfigurationLoadedFromDatabaseText", errorInConfigurationLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //configurationCouldNotBeLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationCouldNotBeLoadedFromDatabaseText", configurationCouldNotBeLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
                configurationDeleteFromDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationDeleteFromDatabaseWarningText", configurationDeleteFromDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
                cannotDeleteCurrentConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCannotDeleteCurrentConfigurationWarningText", cannotDeleteCurrentConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");
              //  cannotDeleteTemplateConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCannotDeleteTemplateConfigurationWarningText", cannotDeleteTemplateConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");               
                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoConfigurationSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");
                //failedToDownloadDeviceConfigurationDataText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToDownloadDeviceConfigurationDataText", failedToDownloadDeviceConfigurationDataText, htmlFontType, htmlStandardFontSize, "");
                //commandToReadDeviceErrorStateFailedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCommandToReadDeviceErrorStateFailedText", commandToReadDeviceErrorStateFailedText, htmlFontType, htmlStandardFontSize, "");
                //lostDeviceConnectionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLostDeviceConnectionText", lostDeviceConnectionText, htmlFontType, htmlStandardFontSize, "");
                //notConnectedToMainMonitorWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNotConnectedToMainMonitorWarningText", notConnectedToMainMonitorWarningText, htmlFontType, htmlStandardFontSize, "");
                serialPortNotSetWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSerialPortNotSetWarningText", serialPortNotSetWarningText, htmlFontType, htmlStandardFontSize, "");
                failedToOpenMonitorConnectionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToOpenMonitorConnectionText", failedToOpenMonitorConnectionText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotProperlyConfiguredText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceCommunicationNotProperlyConfiguredText", deviceCommunicationNotProperlyConfiguredText, htmlFontType, htmlStandardFontSize, "");
                downloadWasInProgressWhenInterfaceWasOpenedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDownloadWasInProgressWhenInterfaceWasOpenedText", downloadWasInProgressWhenInterfaceWasOpenedText, htmlFontType, htmlStandardFontSize, "");
                //failedToWriteTheConfigurationToTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToWriteTheConfigurationToTheDevice", failedToWriteTheConfigurationToTheDevice, htmlFontType, htmlStandardFontSize, "");
                //configurationWasReadFromTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationWasReadFromTheDevice", configurationWasReadFromTheDevice, htmlFontType, htmlStandardFontSize, "");
                //configurationWasWrittenToTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationWasWrittenToTheDevice", configurationWasWrittenToTheDevice, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationDoesNotMatchDatabaseConfigurationText", deviceConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotSavedInDatabaseYetText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceCommunicationNotSavedInDatabaseYetText", deviceCommunicationNotSavedInDatabaseYetText, htmlFontType, htmlStandardFontSize, "");
                saveDeviceConfigurationQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSaveDeviceConfigurationQuestionText", saveDeviceConfigurationQuestionText, htmlFontType, htmlStandardFontSize, "");
                currentConfigurationNotDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentConfigurationNotDefinedText", currentConfigurationNotDefinedText, htmlFontType, htmlStandardFontSize, "");
               // commandToReadDeviceTypeFailed = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCommandToReadDeviceTypeFailed", commandToReadDeviceTypeFailed, htmlFontType, htmlStandardFontSize, "");

                formatErrorInIPAddressText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFormatErrorInIPAddressText", formatErrorInIPAddressText, htmlFontType, htmlStandardFontSize, "");
                digitErrorInIPAddressText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDigitErrorInIPAddressText", digitErrorInIPAddressText, htmlFontType, htmlStandardFontSize, "");

                noValuesAvailableDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoValuesAvailableDataTransferText", noValuesAvailableDataTransferText, "", "", "");
                noValueSetDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoValueSetDataTransferText", noValueSetDataTransferText, "", "", "");
                noneDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoneDataTransferText", noneDataTransferText, "", "", "");
                humidityDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityDataTransferText", humidityDataTransferText, "", "", "");
                correlatingTempDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCorrelatingTempDataTransferText", correlatingTempDataTransferText, "", "", "");
                temp1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp1DataTransferText", temp1DataTransferText, "", "", "");
                temp2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp2DataTransferText", temp2DataTransferText, "", "", "");
                temp3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp3DataTransferText", temp3DataTransferText, "", "", "");
                temp4DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp4DataTransferText", temp4DataTransferText, "", "", "");
                temp5DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp5DataTransferText", temp5DataTransferText, "", "", "");
                temp6DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp6DataTransferText", temp6DataTransferText, "", "", "");
                temp7DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp7DataTransferText", temp7DataTransferText, "", "", "");
               
                loadCurrent1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent1DataTransferText", loadCurrent1DataTransferText, "", "", "");
                loadCurrent2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent2DataTransferText", loadCurrent2DataTransferText, "", "", "");
                loadCurrent3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent3DataTransferText", loadCurrent3DataTransferText, "", "", "");
                voltage1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage1DataTransferText", voltage1DataTransferText, "", "", "");
                voltage2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage2DataTransferText", voltage2DataTransferText, "", "", "");
                voltage3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage3DataTransferText", voltage3DataTransferText, "", "", "");

                analogIn1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn1DataTransferText", analogIn1DataTransferText, "", "", "");
                analogIn2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn2DataTransferText", analogIn2DataTransferText, "", "", "");
                analogIn3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn3DataTransferText", analogIn3DataTransferText, "", "", "");
                analogIn4DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn4DataTransferText", analogIn4DataTransferText, "", "", "");
                analogIn5DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn5DataTransferText", analogIn5DataTransferText, "", "", "");
                analogIn6DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogIn6DataTransferText", analogIn6DataTransferText, "", "", "");

                chassisTempDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChassisTempDataTransferText", chassisTempDataTransferText, "", "", "");

                register18DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationRegister18DataTransferText", register18DataTransferText, "", "", "");
                register20DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationRegister20DataTransferText", register20DataTransferText, "", "", "");

                dataTransferContainsDepricatedDestinationRegister = LanguageConversion.GetStringAssociatedWithTag("MainDisplayDataTransferContainsDepricatedDestinationRegister", dataTransferContainsDepricatedDestinationRegister, htmlFontType, htmlStandardFontSize, "");

                transferSetupNumberDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTransferSetupNumberDataTransferGridViewText", transferSetupNumberDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelDataTransferGridViewText", enableChannelDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                masterDeviceRegisterDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMasterDeviceRegisterDataTransferGridViewText", masterDeviceRegisterDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                slaveDeviceNameDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceNameDataTransferGridViewText", slaveDeviceNameDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                slaveDeviceModbusAddressDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceModbusAddressDataTransferGridViewText", slaveDeviceModbusAddressDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                slaveDeviceRegisterDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceRegisterDataTransferGridViewText", slaveDeviceRegisterDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                slopeDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlopeDataTransferGridViewText", slopeDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
                incorrectDataTransferTabEntryMessage = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationIncorrectDataTransferTabEntryMessage", incorrectDataTransferTabEntryMessage, htmlFontType, htmlStandardFontSize, "");

                transferParameterNameCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTransferParameterNameCalibrationGridViewText", transferParameterNameCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                nullLineCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNullLineCalibrationGridViewText", nullLineCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                multiplierCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMultiplierCalibrationGridViewText", multiplierCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                offsetCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffsetCalibrationGridViewText", offsetCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                channelNoiseCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChannelNoiseCalibrationGridViewText", channelNoiseCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                vibrationCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVibrationCalibrationGridViewText", vibrationCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                analogInCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogInCalibrationGridViewText", analogInCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                currentCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentCalibrationGridViewText", currentCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                voltageCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltageCalibrationGridViewText", voltageCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                humidityCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityCalibrationGridViewText", humidityCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                temperatureCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemperatureCalibrationGridViewText", temperatureCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                chassisTemperatureCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChassisTemperatureCalibrationGridViewText", chassisTemperatureCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeVibrationGridViewText", entryTypeVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelVibrationGridViewText", enableChannelVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberVibrationGridViewText", sensorNumberVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensitivityVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityVibrationGridViewText", sensitivityVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                boundaryFrequencyVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationBoundaryFrequencyVibrationGridViewText", boundaryFrequencyVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                numberOfPointsVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNumberOfPointsVibrationGridViewText", numberOfPointsVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeVibrationGridViewText", toSaveAtChangeVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeAnalogInGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeAnalogInGridViewText", entryTypeAnalogInGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelAnalogInGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelAnalogInGridViewText", enableChannelAnalogInGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberAnalogInGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberAnalogInGridViewText", sensorNumberAnalogInGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensitivityAnalogInGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityAnalogInGridViewText", sensitivityAnalogInGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeAnalogInGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeAnalogInGridViewText", toSaveAtChangeAnalogInGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeCurrentGridViewText", entryTypeCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelCurrentGridViewText", enableChannelCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberCurrentGridViewText", sensorNumberCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensitivityCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityCurrentGridViewText", sensitivityCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeCurrentGridViewText", toSaveAtChangeCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeVoltageGridViewText", entryTypeVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelVoltageGridViewText", enableChannelVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberVoltageGridViewText", sensorNumberVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensitivityVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityVoltageGridViewText", sensitivityVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeVoltageGridViewText", toSaveAtChangeVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeHumidityGridViewText", entryTypeHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelHumidityGridViewText", enableChannelHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberHumidityGridViewText", sensorNumberHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeHumidityGridViewText", toSaveAtChangeHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeTemperatureGridViewText", entryTypeTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                enableChannelTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelTemperatureGridViewText", enableChannelTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                sensorNumberTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberTemperatureGridViewText", sensorNumberTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeTemperatureGridViewText", toSaveAtChangeTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                entryTypeChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeChassisTemperatureGridViewText", entryTypeChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");

                enableChannelChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelChassisTemperatureGridViewText", enableChannelChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                inclusionThresholdChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInclusionThresholdChassisTemperatureGridViewText", inclusionThresholdChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
                toSaveAtChangeChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeChassisTemperatureGridViewText", toSaveAtChangeChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");

                onThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOnThresholdText", onThresholdText, htmlFontType, htmlStandardFontSize, "");
                offThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffThresholdText", offThresholdText, htmlFontType, htmlStandardFontSize, "");
                exceedingThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExceedingThresholdText", exceedingThresholdText, htmlFontType, htmlStandardFontSize, "");
                diminishingThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDiminishingThresholdText", diminishingThresholdText, htmlFontType, htmlStandardFontSize, "");
                warningThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationWarningThresholdText", warningThresholdText, htmlFontType, htmlStandardFontSize, "");
                alarmThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAlarmThresholdText", alarmThresholdText, htmlFontType, htmlStandardFontSize, "");
                ledOnlyThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLedOnlyThresholdText", ledOnlyThresholdText, htmlFontType, htmlStandardFontSize, "");
                ledAndRelayThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLedAndRelayThresholdText", ledAndRelayThresholdText, htmlFontType, htmlStandardFontSize, "");
                entryTypeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeThresholdGridViewText", entryTypeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                thresholdNumberThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationThresholdNumberThresholdGridViewText", thresholdNumberThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                statusThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationStatusThresholdGridViewText", statusThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                modeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModeThresholdGridViewText", modeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                typeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTypeThresholdGridViewText", typeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                resultThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationResultThresholdGridViewText", resultThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                timeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTimeThresholdGridViewText", timeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                levelThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLevelThresholdGridViewText", levelThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
                vibrationGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVibrationGridViewEntryText", vibrationGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                analogInGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAnalogInGridViewEntryText", analogInGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                currentGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentGridViewEntryText", currentGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                voltageGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltageGridViewEntryText", voltageGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                humidityGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityGridViewEntryText", humidityGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                temperatureGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemperatureGridViewEntryText", temperatureGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
                chassisTemperatureGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChassisTemperatureGridViewEntryText", chassisTemperatureGridViewEntryText, htmlFontType, htmlStandardFontSize, "");

                availableConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAvailableConfigurationsRadGroupBoxText", availableConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                saveCurrentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSaveCurrentConfigurationRadButtonText", saveCurrentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //saveDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSaveDeviceConfigurationRadButtonText", saveDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceLoadConfigFromDatabaseRadButtonText", loadConfigurationFromDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                programDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceProgramDeviceRadButtonText", programDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceLoadConfigurationFromDeviceRadButtonText", loadConfigurationFromDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //deleteConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDeleteConfigurationRadButtonText", deleteConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //configurationViewSelectRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationViewSelectRadGroupBoxText", configurationViewSelectRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                //fromDeviceRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceFromDeviceRadioButtonText", fromDeviceRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //fromDatabaseRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceFromDatabaseRadioButtonText", fromDatabaseRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //currentConfigurationRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCurrentConfigurationRadioButtonText", currentConfigurationRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCopyDeviceConfigurationRadButtonText", copyDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyDatabaseConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCopyDatabaseConfigurationRadButtonText", copyDatabaseConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");

                emptyCellErrorMessage = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEmptyCellErrorMessageText", emptyCellErrorMessage, htmlFontType, htmlStandardFontSize, "");
                uploadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationUploadingConfigurationText", uploadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                downloadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDownloadingConfigurationText", downloadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                //couldNotFindCurrentConfigurationDisplayingMostRecentText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCouldNotFindCurrentConfigurationDisplayingMostRecentText", couldNotFindCurrentConfigurationDisplayingMostRecentText, htmlFontType, htmlStandardFontSize, "");
                //couldNotFindAnyDeviceConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCouldNotFindAnyDeviceConfigurationText", couldNotFindAnyDeviceConfigurationText, htmlFontType, htmlStandardFontSize, "");

                failedToUploadConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToUploadConfigurationText", failedToUploadConfigurationText, htmlFontType, htmlStandardFontSize, "");
                errorSendingPasswordToDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationErrorSendingPasswordToDeviceText", errorSendingPasswordToDeviceText, htmlFontType, htmlStandardFontSize, "");
                //noDatabaseConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoDatabaseConfigurationLoadedText", noDatabaseConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
                //noDeviceConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoDeviceConfigurationLoadedText", noDeviceConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
                //currentConfigurationNotSavedWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentConfigurationNotSavedWarningText", currentConfigurationNotSavedWarningText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExitWithoutSavingQuestionText", exitWithoutSavingQuestionText, htmlFontType, htmlStandardFontSize, "");
                //overwriteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOverwriteCurrentConfigurationText", overwriteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");
                offRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffRelayText", offRelayText, "", "", "").Trim();
                onRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOnRelayText", onRelayText, "", "", "").Trim();
                externalDevicesRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExternalDevicesText", externalDevicesRelayText, htmlFontType, htmlStandardFontSize, "");
                nullDeviceRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNullDeviceText", nullDeviceRelayText, htmlFontType, htmlStandardFontSize, "");

                mainMonitorConfigurationTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTitleText", mainMonitorConfigurationTitleText, "", "", "");
                //mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText", mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText, "", "", "");
                //mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText", mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText, "", "", "");
                //mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText", mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText, "", "", "");
                //mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText", mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText, "", "", "");

                generalSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabText", generalSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                monitoringRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringRadGroupBoxText", monitoringRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                frequencyRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabFrequencyRadGroupBoxText", frequencyRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                hertzRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabHertzRadLabelText", hertzRadLabelText, htmlFontType, htmlStandardFontSize, "");
                versionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabVersionRadGroupBoxText", versionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                boardNameRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabBoardNameRadGroupBoxText", boardNameRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                connectionParametersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabConnectionParametersRadGroupBoxText", connectionParametersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                modbusAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModbusAddressRadLabelText", modbusAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                serialConnectionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabSerialConnectionRadGroupBoxText", serialConnectionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                serialBaudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabSerialBaudRateRadLabelText", serialBaudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                ethernetRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetRadGroupBoxText", ethernetRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                ethernetBaudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetBaudRateRadLabelText", ethernetBaudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
                ipAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabIPAddressRadLabelText", ipAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
                relayRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabRelayRadGroupBoxText", relayRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                generalSettingsAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabGeneralSettingsAlarmRadLabelText", generalSettingsAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
                generalSettingsWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabGeneralSettingsWarningRadLabelText", generalSettingsWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
                modeSaveRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveRadGroupBoxText", modeSaveRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                stepRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveStepRadLabelText", stepRadLabelText, htmlFontType, htmlStandardFontSize, "");
                minutesRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveMinutesRadLabelText", minutesRadLabelText, htmlFontType, htmlStandardFontSize, "");
                useDiagnosticResultsRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabUseDiagnosticeResultsRadCheckBoxText", useDiagnosticResultsRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                otherOptionsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabOtherOptionsRadGroupBoxText", otherOptionsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                enableCurrentSignatureAnalysisRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEnableCurrentSignatureAnalysisRadCheckBoxText", enableCurrentSignatureAnalysisRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                allowDirectAccessRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabAllowDirectAccessRadCheckBoxText", allowDirectAccessRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                ethernetProtocolRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetProtocolRadLabelText", ethernetProtocolRadLabelText, htmlFontType, htmlStandardFontSize, "");

                systemConfigurationRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSystemConfigurationTabText", systemConfigurationRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");

                analogInputsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabText", analogInputsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                controlParametersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabControlParametersRadGroupBoxText", controlParametersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                vibrationRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabVibrationRadRadioButtonText", vibrationRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                analogInRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabAnalogInRadRadioButtonText", analogInRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                currentRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabCurrentRadRadioButtonText", currentRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                voltageRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabVoltageRadRadioButtonText", voltageRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                humidityRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabHumidityRadRadioButtonText", humidityRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                temperatureRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabTemperatureRadRadioButtonText", temperatureRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                chassisTemperatureRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabChassisTemperatureRadRadioButtonText", chassisTemperatureRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");

                calibrationRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCalibrationTabText", calibrationRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                dataTransferRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDataTransferTabText", dataTransferRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                
                //initializeWorkingConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceInitializeWorkingConfigurationRadButtonText", initializeWorkingConfigurationRadButtonText, "", "", "");

                couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCouldNotFindWorkingConfigurationDisplayingEmptyConfigurationText", couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText, "", "", "");

                //workingConfigAlreadyPresentInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationWorkingConfigAlreadyPresentInDatabaseText", workingConfigAlreadyPresentInDatabaseText, "", "", "");
                //failedToDeleteWorkingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToDeleteWorkingConfigurationText", failedToDeleteWorkingConfigurationText, "", "", "");
                workingConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationWorkingConfigurationSavedToDatabaseText", workingConfigurationSavedToDatabaseText, "", "", "");
                workingConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationWorkingConfigurationNotSavedToDatabaseText", workingConfigurationNotSavedToDatabaseText, "", "", "");

                //loadedWorkingConfigurationAndDeletedItFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadedWorkingConfigurationAndDeletedItFromDatabaseText", loadedWorkingConfigurationAndDeletedItFromDatabaseText, "", "", "");
                //loadedWorkingConfigurationButFailedToDeleteItFromTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadedWorkingConfigurationButFailedToDeleteItFromTheDatabaseText", loadedWorkingConfigurationButFailedToDeleteItFromTheDatabaseText, "", "", "");
                configurationWasReadFromTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationWasReadFromTheDatabaseText", configurationWasReadFromTheDatabaseText, "", "", "");

                changesToWorkingConfigurationNotSavedOverwriteWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChangesToWorkingConfigurationNotSavedOverwriteWarningText", changesToWorkingConfigurationNotSavedOverwriteWarningText, "", "", "");
                changesToWorkingConfigurationNotSavedExitWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChangesToWorkingConfigurationNotSavedExitWarningText", changesToWorkingConfigurationNotSavedExitWarningText, "", "", "");
                discardChangesAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDiscardChangesAsQuestionText", discardChangesAsQuestionText, "", "", "");

                undefinedDeviceNameText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationUndefinedDeviceNameText", undefinedDeviceNameText, "", "", "");
                undefinedRegisterNameText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationUndefinedRegisterNameText", undefinedRegisterNameText, "", "", "");

                undefinedDeviceInDataTransferGridText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationUndefinedDeviceInDataTransferGridText", undefinedDeviceInDataTransferGridText, htmlFontType, htmlStandardFontSize, "");

               // saveWorkingConfigurationToDatabaseQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSaveWorkingConfigurationToDatabaseQuestionText", saveWorkingConfigurationToDatabaseQuestionText, htmlFontType, htmlStandardFontSize, "");

                deviceInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDeviceInteractionGroupBoxText", deviceInteractionGroupBoxText, "", "", "");
                databaseInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDatabaseInteractionGroupBoxText", databaseInteractionGroupBoxText, "", "", "");

                configurationLoadCancelledText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationLoadCancelledText", configurationLoadCancelledText, htmlFontType, htmlStandardFontSize, "");

                failedToDeleteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToDeleteCurrentConfigurationText", failedToDeleteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");
                noTemplateConfigurationsAvailable = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoTemplateConfigurationsAvailable", noTemplateConfigurationsAvailable, "", "", "");
                failedtoDeleteConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToDeleteConfigurationText", failedtoDeleteConfigurationText, htmlFontType, htmlStandardFontSize, "");
                replaceExistingConfigurationsWithTheSameNameQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationReplaceExistingConfigurationsWithTheSameNameQuestionText", replaceExistingConfigurationsWithTheSameNameQuestionText, htmlFontType, htmlStandardFontSize, "");

                templateConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemplateConfigurationsRadGroupBoxText", templateConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                copySelectedConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCopySelectedConfigurationRadButtonText", copySelectedConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                availableConfigurationsHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAvailableConfigurationsHeaderText", availableConfigurationsHeaderText, "", "", "");
                noConfigurationsSavedInDbText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoConfigurationsSavedInDbText", noConfigurationsSavedInDbText, "", "", "");

                deleteSelectedConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeleteSelectedConfigurationRadButtonText", deleteSelectedConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
            
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInSomeTabObject()
        {
            bool errorIsPresent = false;

            errorIsPresent = ErrorIsPresentInAGeneralSettingsTabObject();
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInASystemConfigurationTabObject();
            }
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInAnAnalogInputsTabObject();
            }
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInADataTransferTabObject();
            }
            return errorIsPresent;
        }

        private void WriteAllInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    WriteGeneralSettingsTabValuesToCurrentConfiguration();
                    WriteSystemConfigurationGridDataToCurrentConfiguration();
                    WriteAnalogInputDataToCurrentConfiguration();
                    WriteCalibrationTabDataToCurrentConfiguration();
                    WriteDataTransferTabDataToCurrentConfiguration();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool AddDataToAllTransientInterfaceObjects(Main_Configuration inputConfiguration)
        {
            bool noRegisterErrorsSeen = true;
            try
            {
                if (inputConfiguration != null)
                {
                    SetGeneralSettingsTabValuesFromConfiguration(inputConfiguration);
                    AddDataToSystemConfigurationGrid(inputConfiguration.fullStatusInformationConfigObjects);
                    // FillSystemConfigurationList();
                    // AddRowsToTheSystemConfigurationGrid();

                    AddDataToTheAnalogInputsGrids(inputConfiguration);
                    AddDataToTheCalibrationGrid(inputConfiguration.calibrationDataConfigObjects);

                    this.systemConfigurationIsCorrect = !ErrorIsPresentInASystemConfigurationTabObject();
                    if (this.systemConfigurationIsCorrect)
                    {
                        GetSystemConfigurationForDataTransfer();
                        if (this.dataTransferRadGridView.RowCount == 0)
                        {
                            AddEmptyRowsToTheDataTransferGrid();
                        }
                        noRegisterErrorsSeen = AddDataToTheDataTransferGrid(inputConfiguration.transferSetupConfigObjects);
                    }
                    else
                    {
                        this.dataTransferRadGridView.Rows.Clear();
                    }

                    //FillAllPrimaryAnalogInputDataLists();
                    // FillAllThresholdDataLists();
                    // AddAllDataToTheAnalogInputsGrids();
                    //FillTransferSetupDataList();
                  
                    this.chassisTemperatureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return noRegisterErrorsSeen;
        }

        private void saveWorkingConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                // this used to have a "using" statement but that wasn't acceptable in this instance for some reason I don't yet understand.
                using(MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    SaveWorkingConfigurationToDatabase(localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.saveCurrentConfigurationCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
 
        private void loadConfigurationFromDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadSelectedConfigurationFromDatabase();
                programDeviceGeneralSettingsTabRadButton.Enabled = true;
                programDeviceAnalogInputsTabRadButton.Enabled = true;
                programDeviceCalibrationTabRadButton.Enabled = true;
                programDeviceDataTransferTabRadButton.Enabled = true;
                programDeviceSystemConfigurationTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.loadConfigurationFromDatabaseGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void deleteConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteConfigurationFromDatabase();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.deleteConfigurationGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }




        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.programDeviceCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
                programDeviceGeneralSettingsTabRadButton.Enabled = true;
                programDeviceAnalogInputsTabRadButton.Enabled = true;
                programDeviceCalibrationTabRadButton.Enabled = true;
                programDeviceDataTransferTabRadButton.Enabled = true;
                programDeviceSystemConfigurationTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.loadConfigurationFromDeviceGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private void copyDeviceConfigurationRadButton_Click(object sender, EventArgs e)
        //{
        //    CopyConfigurationFromDeviceToWorking();
        //}

        //private void copyDatabaseConfigurationRadButton_Click(object sender, EventArgs e)
        //{
        //    CopyConfigurationFromDatabaseToWorking();
        //}

        //private void initializeWorkingConfigurationRadButton_Click(object sender, EventArgs e)
        //{
        //    InitializeWorkingConfiguration();
        //}

        private void availableConfigurationsGeneralSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsGeneralSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.availableConfigurationsGeneralSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsSystemConfigurationTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsSystemConfigurationTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.availableConfigurationsSystemConfigurationTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsAnalogInputsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsAnalogInputsTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.availableConfigurationsAnalogInputsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsCalibrationTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsCalibrationTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.availableConfigurationsAnalogInputsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableConfigurationsDataTransferTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsDataTransferTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.availableConfigurationsDataTransferTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAvailableConfigurationsSelectedIndex()
        {
            try
            {
                availableConfigurationsGeneralSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsSystemConfigurationTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsAnalogInputsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsCalibrationTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsDataTransferTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private bool UpdateConfigurationWithCurrentInterfaceValues()
//        {
//            bool errorIsPresent = false;
//            try
//            {
//                //Int16[] registerData;
//                //registerData = currentConfiguration.GetShortValuesFromAllContributors();
//                //currentConfiguration = null;
//                //currentConfiguration = new Main_Configuration(registerData);
//                errorIsPresent = ErrorIsPresentInSomeTabObject();
//                if(!errorIsPresent)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateConfigurationWithCurrentInterfaceValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return errorIsPresent;
//        }

//        private void WriteUserInterfaceViewedConfigurationToCurrentConfiguration()
//        {
//            try
//            {
//                this.configurationError = false;
//                if (this.workingConfiguration != null)
//                {
//                    if (this.workingConfiguration.fullStatusInformationConfigObjects != null)
//                    {
//                        if (this.workingConfiguration.inputChannelConfigObjects != null)
//                        {
//                            if (this.workingConfiguration.inputChannelThresholdConfigObjects != null)
//                            {
//                                if (this.workingConfiguration.transferSetupConfigObjects != null)
//                                {
//                                    TranslateGeneralSettingsTabValuesToConfigurationValues(ref currentConfiguration);

//                                    if (!this.configurationError)
//                                    {
//                                        GetSystemConfigurationDataFromGrid(ref currentConfiguration.fullStatusInformationConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        CopyFullStatusInformationToShortStatusInformation(ref currentConfiguration);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetAllInputChannelDataFromGrids(ref currentConfiguration.inputChannelConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetAllThresholdDataFromGrids(ref currentConfiguration.inputChannelThresholdConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetDataTransferDataFromGrid(ref currentConfiguration.transferSetupConfigObjects);
//                                    }
//                                }
//                                else
//                                {
//                                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.workingConfiguration.transferSetupConfigObjects was null.";
//                                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                                    MessageBox.Show(errorMessage);
//#endif
//                                }
//                            }
//                            else
//                            {
//                                string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.workingConfiguration.inputChannelThresholdConfigObjects was null.";
//                                LogMessage.LogError(errorMessage);
//#if DEBUG
//                                MessageBox.Show(errorMessage);
//#endif
//                            }
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.workingConfiguration.inputChannelConfigObjects was null.";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.workingConfiguration.fullStatusInformationConfigObjects was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }

//                }
//                else
//                {
//                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.workingConfiguration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

       

        /// <summary>
        /// Testing program that shows some specific register values, only for debugging
        /// </summary>
        /// <param name="latestRegisterData"></param>
        private void ShowSomeValuesThatArePissingMeOff(Int16[] latestRegisterData)
        {
            int offset = 300;

            StringBuilder entry = new StringBuilder();

            int tempIndex;

            for (int i = 0; i < 25; i++)
            {
                for (int j = 14; j < 17; j++)
                {
                    tempIndex = offset + j;
                    entry.Append("Register " + tempIndex.ToString() + ":  " + latestRegisterData[tempIndex].ToString());
                    entry.Append("\n");
                }
                offset += 200;
                entry.Append("\n");
            }

            MessageBox.Show(entry.ToString());
        }

        /// <summary>
        /// Compares two configurations using their register data, only for debugging
        /// </summary>
        /// <param name="latestRegisterData"></param>
        private void CheckLatestConfigurationAgainstPreviousConfiguration(Int16[] latestRegisterData)
        {
            try
            {
                List<String> badValues = new List<string>();
                // Int16[] previousRegisterData = currentConfiguration.GetShortValuesFromAllContributors();
                string entry;

                if ((this.previousConfigurationAsArray != null) && (this.workingConfigurationAsArray != null))
                {
                    for (int i = 0; i < 6000; i++)
                    {
                        if (this.previousConfigurationAsArray[i] != this.workingConfigurationAsArray[i])
                        {
                            entry = "Register " + i.ToString() + ": Old Value = " + this.previousConfigurationAsArray[i].ToString() + "   New Value = " + this.workingConfigurationAsArray[i].ToString();
                            badValues.Add(entry);
                        }
                    }

                    if (badValues.Count > 0)
                    {
                        StringBuilder allerrors = new StringBuilder();
                        allerrors.Append("Found mismatches between old and new register values\n\n");
                        foreach (string error in badValues)
                        {
                            allerrors.Append(error);
                            allerrors.Append("\n");
                        }
                        MessageBox.Show(allerrors.ToString());
                    }
                    else
                    {
                        MessageBox.Show("New values loaded equal to old values");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.CheckLatestConfigurationAgainstPreviousConfiguration(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Int16[] Main_GetDeviceSetup(int modBusAddress)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int offset = 0;
                int registersReturned = 0;
                int registersPerDataRequest = 100;
                List<int> errorCodes = new List<int>();
                bool receivedAllData = false;
                int chunkCount = 0;

                allRegisterValues = new Int16[6100];

                DisableAllControls();
                SetProgressBarsToDownloadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 60);
                while (!receivedAllData)
                {
                    readIsIncorrect = true;
                    while (readIsIncorrect && !receivedAllData)
                    {
                        currentRegisterValues = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, registersPerDataRequest, readDelayInMicroseconds,
                                                                                                   MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, new ParentWindowInformation(this.Location, this.Size));
                        if (currentRegisterValues != null)
                        {
                            registersReturned = currentRegisterValues.Length;
                            Array.Copy(currentRegisterValues, 0, allRegisterValues, offset, registersReturned);
                            offset += registersReturned;
                            readIsIncorrect = false;
                            if (offset >= 6000)
                            {
                                receivedAllData = true;
                            }
                            chunkCount++;
                        }
                        else
                        {
                            //RadMessageBox.Show(this, "Failed to download monitor configuration data.\nThe error log may have some clue as to why it failed.");
                            receivedAllData = true;
                            allRegisterValues = null;
                        }
                        Application.DoEvents();
                    }
                    SetProgressBarProgress(chunkCount, 60);
                    // UpdateDownloadChunkCountDisplay(chunkCount);
                    Application.DoEvents();
                }

                if (allRegisterValues != null)
                {
                    returnedRegisterValues = new Int16[offset];
                    Array.Copy(allRegisterValues, 0, returnedRegisterValues, 0, offset);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_GetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return returnedRegisterValues;
        }


        public bool Main_SetDeviceSetup(int modBusAddress)
        {
            bool success = false;
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                Int16[] allRegisterValues = null;
                // Int16[] currentRegisterValues = null;
                //int registersPerDataSend = 100;
                List<int> errorCodes = new List<int>();

                string downloadErrorMessage = string.Empty;
                ErrorCode error = ErrorCode.None;

                allRegisterValues = this.workingConfiguration.GetShortValuesFromAllContributors();
                // currentRegisterValues = new Int16[registersPerDataSend];

                // write the password so we can write the rest of the values

                // DeviceCommunication.Main_SetDeviceSetup(modBusAddress, 

                if (WritePasswordToDeviceAndCheckIfTheWriteWorked(modBusAddress))
                {
                    DisableAllControls();
                    SetProgressBarsToUploadState();
                    SetProgressBarProgress(0, 1000);
                    EnableProgressBars();

                    //Int16[] value = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 121, 1, this.readDelayInMicroseconds, ref downloadErrorMessage);
                    //if (value != null)
                    //{
                    //if (value[0] == 1)
                    //{
                    //success = WriteShortStatusInformationToDevice(modBusAddress, allRegisterValues);

                    success = WriteBoardNameToDevice(modBusAddress, allRegisterValues);

                    Application.DoEvents();

                    if (success)
                    {
                        success = WriteInputChannelDataToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }

                    if (success)
                    {
                        success = WriteTransferSetupToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }

                    if (success)
                    {
                        success = WriteFullStatusInformationToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteCalibrationDataToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteSetupAndCommunicationInformationToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteUnknownValuesAsZeroesToDevice(modBusAddress);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteConfigurationFinalCommandToDevice(modBusAddress);
                        Application.DoEvents();
                    }
                    //if (success)
                    //{
                    //    success = WriteCalibrationDataToDevice(modBusAddress, allRegisterValues);
                    //    Application.DoEvents();
                    //}
                    //if (success)
                    //{
                    //    success = WriteTransferSetupToDevice(modBusAddress, allRegisterValues);
                    //    Application.DoEvents();
                    //}
                    if (!success)
                    {
                        RadMessageBox.SetThemeName("office2007BlackTheme");
                        RadMessageBox.Show(this, failedToUploadConfigurationText);
                    }
                    //// reset the configuration to read only
                    //DeviceCommunication.WriteSingleRegister(modBusAddress, 121, 0, this.readDelayInMicroseconds);

                    //}
                    //else
                    //{
                    //    RadMessageBox.Show(this, "Error in password sent to device.\nPlease check the connection.");
                    //}
                    //}
                    //else
                    //{
                    //    if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadFailedMessage) == 0)
                    //    {
                    //        error = ErrorCode.DownloadFailed;
                    //    }
                    //    else
                    //    {
                    //        error = ErrorCode.DownloadCancelled;
                    //    }
                    //}

                }
                else
                {
                    RadMessageBox.SetThemeName("office2007BlackTheme");
                    RadMessageBox.Show(this, errorSendingPasswordToDeviceText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_SetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return success;
        }

        private bool WritePasswordToDeviceAndCheckIfTheWriteWorked(int modBusAddress)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] password = new Int16[1];
                Int16[] responseMessage=null;
                password[0] = (Int16)5421;

                if (InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, password, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation))
                {
                    responseMessage = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, 120, 1, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                }
                if (responseMessage != null)
                {
                    if(responseMessage[0]==1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteConfigurationFinalCommandToDevice(int modBusAddress)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] outputValue = new Int16[1];          
                outputValue[0] = 0;

                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, outputValue, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);            
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        private bool WriteUnknownValuesAsZeroesToDevice(int modBusAddress)  // cfk. overwrites some whs values 12/23/13
        {
          /*  bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] outputValues = new Int16[48];

                for (int i = 0; i < 48; i++)
                {
                    outputValues[i] = 0;
                }

                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 5500, outputValues, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                SetProgressBarProgress(20 + 2550 + 288 + 120 + 24 + 48, 3050);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }*/
            SetProgressBarProgress(20 + 2550 + 288 + 120 + 24 + 48, 3050);
            return true;
        }

        private bool WriteBoardNameToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[20];
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 40)
                        {
                            Array.Copy(allRegisters, 20, registersToWrite, 0, 20);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 20, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                            SetProgressBarProgress(20, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteShortStatusInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[45];
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 95)
                        {
                            Array.Copy(allRegisters, 50, registersToWrite, 0, 45);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 50, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteSetupAndCommunicationInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[24];
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 138)
                        {
                            Array.Copy(allRegisters, 121, registersToWrite, 0, 24);
                            /// value found in setup write from IHM using usb sniffer, don't know where it comes from
                            registersToWrite[21] = 10;
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 121, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                            SetProgressBarProgress(20 + 2550 + 288 + 120 + 24, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteFullStatusInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[120];
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 270)
                        {
                            Array.Copy(allRegisters, 150, registersToWrite, 0, 120);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 150, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                            SetProgressBarProgress(20 + 2550 + 288 + 120, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteInputChannelDataToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[102];
                bool uploading = true;
                int count = 0;
                int offset = 300;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5300)
                        {
                            while (uploading)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 102);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                offset += 200;
                                count++;
                                SetProgressBarProgress(20 + (count * 102), 3050);
                                if (!success || (count == 25))
                                {
                                    uploading = false;
                                }
                                Application.DoEvents();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteCalibrationDataToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[6];
                int offset = 5300;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5450)
                        {
                            for (int i = 0; i < 25; i++)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 6);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                if (!success)
                                {
                                    break;
                                }
                                Application.DoEvents();
                                offset += 6;
                            }                            
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteTransferSetupToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] registersToWrite = new Int16[9];

                bool uploading = true;
                int count = 0;
                int offset = 5600;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5888)
                        {
                            while (uploading)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 9);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                offset += 9;
                                count++;
                                SetProgressBarProgress(20 + 2550 + (count * 9), 3050);
                                if (!success || (count == 32))
                                {
                                    uploading = false;
                                }
                                Application.DoEvents();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

//        private void UpdateDownloadChunkCountDisplay(int chunkCount)
//        {
//            try
//            {
//                string text = "Downloading Chunk " + chunkCount.ToString() + " of 60";

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateDownloadChunkCountDisplay(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void UpdateUploadChunkCountDisplay(int chunkCount)
//        {
//            try
//            {
//                string text = "Uploading Chunk " + chunkCount.ToString() + " of 60";

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateUploadChunkCountDisplay(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void ClearChunkCountDisplay()
//        {
//            try
//            {
//                string text = string.Empty;

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ClearChunkCountDisplay()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void LoadAvailableConfigurations(MonitorInterfaceDB localDB)
        {
            try
            {
                string messageString;
                if (Main_MonitorConfiguration.monitor != null)
                {
                    if (Main_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                    {
                        Main_MonitorConfiguration.availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(localDB);
                    }
                    else
                    {
                        Main_MonitorConfiguration.availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(Main_MonitorConfiguration.monitor.ID, localDB);
                    }

                    availableConfigurationsGeneralSettingsTabRadListControl.Items.Clear();
                    availableConfigurationsSystemConfigurationTabRadListControl.Items.Clear();
                    availableConfigurationsAnalogInputsTabRadListControl.Items.Clear();
                    availableConfigurationsCalibrationTabRadListControl.Items.Clear();
                    availableConfigurationsDataTransferTabRadListControl.Items.Clear();

                    if ((Main_MonitorConfiguration.availableConfigurations != null) && (Main_MonitorConfiguration.availableConfigurations.Count > 0))
                    {
                        messageString = availableConfigurationsHeaderText;

                        availableConfigurationsGeneralSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsSystemConfigurationTabRadListControl.Items.Add(messageString);
                        availableConfigurationsAnalogInputsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCalibrationTabRadListControl.Items.Add(messageString);
                        availableConfigurationsDataTransferTabRadListControl.Items.Add(messageString);

                        foreach (Main_Config_ConfigurationRoot entry in Main_MonitorConfiguration.availableConfigurations)
                        {
                            messageString = entry.DateAdded.ToString() + "      " + entry.Description;

                            availableConfigurationsGeneralSettingsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsSystemConfigurationTabRadListControl.Items.Add(messageString);
                            availableConfigurationsAnalogInputsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsCalibrationTabRadListControl.Items.Add(messageString);
                            availableConfigurationsDataTransferTabRadListControl.Items.Add(messageString);
                        }
                    }
                    else
                    {
                        messageString = noConfigurationsSavedInDbText;

                        availableConfigurationsGeneralSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsSystemConfigurationTabRadListControl.Items.Add(messageString);
                        availableConfigurationsAnalogInputsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCalibrationTabRadListControl.Items.Add(messageString);
                        availableConfigurationsDataTransferTabRadListControl.Items.Add(messageString);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.LoadAvailableConfigurations()\nMain_MonitorConfiguration.monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsSystemConfigurationTabRadDropDownListDataSource;
                string[] templateConfigurationsAnalogInputsTabRadDropDownListDataSource; 
                string[] templateConfigurationsCalibrationTabRadDropDownListDataSource;
                string[] templateConfigurationsDataTransferTabRadDropDownListDataSource;

                string description;

                this.templateConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsCalibrationTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsDataTransferTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsSystemConfigurationTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAnalogInputsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsCalibrationTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsDataTransferTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource = new string[1]; ;
                    templateConfigurationsCalibrationTabRadDropDownListDataSource = new string[1]; ;
                    templateConfigurationsDataTransferTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsCalibrationTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsDataTransferTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsGeneralSettingsTabRadDropDownList.DataSource = templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                templateConfigurationsSystemConfigurationTabRadDropDownList.DataSource = templateConfigurationsSystemConfigurationTabRadDropDownListDataSource;
                templateConfigurationsAnalogInputsTabRadDropDownList.DataSource = templateConfigurationsAnalogInputsTabRadDropDownListDataSource;
                templateConfigurationsCalibrationTabRadDropDownList.DataSource = templateConfigurationsCalibrationTabRadDropDownListDataSource;
                templateConfigurationsDataTransferTabRadDropDownList.DataSource = templateConfigurationsDataTransferTabRadDropDownListDataSource;

                templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


//        private bool DisplayMostRecentConfigurationInTheDatabase()
//        {
//            bool configFound = false;
//            try
//            {
//                Guid configurationRootID;
//                if (Main_MonitorConfiguration.availableConfigurations != null)
//                {
//                    if (Main_MonitorConfiguration.availableConfigurations.Count > 0)
//                    {
//                        configurationRootID = Main_MonitorConfiguration.availableConfigurations[0].ID;
//                        configurationFromDatabase = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);
//                        if ((this.configurationFromDatabase != null) && (this.configurationFromDatabase.AllConfigurationMembersAreNonNull()))
//                        {
//                            AddDataToAllTransientInterfaceObjects(configurationFromDatabase);
//                            EnableFromDatabaseRadRadioButtons();
//                            SetFromDatabaseRadRadioButtonState();
//                            configFound = true;
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in Main_MonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nConfiguration was not complete in the database";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_MonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nthis.availableConfigurations was null";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return configFound;
//        }


        private void configurationRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {
            try
            {
                //Main_Configuration selectedConfig = null;
                if (configurationRadPageView.SelectedPage == dataTransferRadPageViewPage)
                {
                    //if (!ErrorIsPresentInASystemConfigurationTabObject())
                    //{
                    GetSystemConfigurationForDataTransfer();
                    UpdateDataTransferPageSlaveMonitorInformation();
                    UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                    UpdateDataTransferPageRadGridViewMainRegisterNames();
                    if (this.dataTransferRadGridView.Rows.Count == 0)
                    {
                        AddEmptyRowsToTheDataTransferGrid();
                    }
                    //if (this.fromDatabaseGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    //{
                    //    selectedConfig = configurationFromDatabase;
                    //}
                    //else if (this.fromDeviceGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    //{
                    //    selectedConfig = configurationFromDevice;
                    //}
                    //else if (this.workingGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    //{
                    //    selectedConfig = this.workingConfiguration;
                    //}
                    if (this.workingConfiguration != null)
                    {
                        AddDataToTheDataTransferGrid(this.workingConfiguration.transferSetupConfigObjects);
                    }

                    //}
                    //else
                    //{
                    //    RadMessageBox.Show(this, "The system configuration as defined in the \"System Configuration\" tab is incorrect.\nThe transfer setup will not be displayed until it is corrected.");
                    //    this.dataTransferRadGridView.Rows.Clear();
                    //}
                }
                //else
                //{
                //    /// save the current information in the Data Transfer tab if we could have been editing it.
                //    if (this.workingGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //    {
                //        if (this.workingConfiguration != null)
                //        {

                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.configurationRadPageView_SelectedPageChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void configurationRadPageView_SelectedPageChanging(object sender, RadPageViewCancelEventArgs e)
        {
            try
            {
                //bool cancel = false;
                //if (!CheckSystemConfigurationForCorrectness())
                //{
                //    cancel = true;
                //}
                //else if (!CheckDataTransferRadGridViewElementsForOverallCorrectness())
                //{
                //    //cancel = true;
                //}
                //if (cancel)
                //{
                //    e.Cancel = true;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.configurationRadPageView_SelectedPageChanging(object, RadPageViewCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
      
//        private void fromDeviceGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDeviceGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDeviceGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDatabaseGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDatabaseGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.workingGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }
       
//        private void fromDeviceSystemConfigurationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDeviceSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDeviceSystemConfigurationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseSystemConfigurationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDatabaseSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDatabaseSystemConfigurationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentSystemConfigurationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.workingSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentSystemConfigurationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }
       
//        private void fromDeviceAnalogInputsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDeviceAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDeviceAnalogInputsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseAnalogInputsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDatabaseAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDatabaseAnalogInputsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentAnalogInputsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.workingAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentAnalogInputsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDeviceCalibrationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDeviceCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDeviceCalibrationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseCalibrationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDatabaseCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDatabaseCalibrationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentCalibrationTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.workingCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentCalibrationTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }


//        private void fromDeviceDataTransferTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDeviceDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDeviceRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDeviceDataTransferTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void fromDatabaseDataTransferTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.fromDatabaseDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetFromDatabaseRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.fromDatabaseDataTransferTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void currentDataTransferTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
//        {
//            try
//            {
//                if (this.workingDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentDataTransferTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }
       
//        private void SetFromDeviceRadRadioButtonState()
//        {
//            try
//            {
//                if (this.configurationFromDevice != null)
//                {
//                    this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText;

//                    if (this.fromDeviceGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDeviceSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceSystemConfigurationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDeviceAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceAnalogInputsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDeviceCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceCalibrationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDeviceDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDeviceDataTransferTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDevice)
//                    {
//                        if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                        {
//                            WriteAllInterfaceDataToWorkingConfiguration();
//                        }
//                        AddDataToAllTransientInterfaceObjects(this.configurationFromDevice);
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.FromDevice;
//                        DisableGeneralSettingsTabConfigurationEditObjects();
//                        DisableSystemConfigurationTabConfigurationEditObjects();
//                        DisableAllAnalogInputsTabConfigurationEditObjects();
//                        DisableCalibrationTabConfigurationEditObjects();
//                        DisableDataTransferTabConfigurationEditObjects();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetFromDeviceRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void SetFromDatabaseRadRadioButtonState()
//        {
//            try
//            {
//                if (this.configurationFromDatabase != null)
//                {
//                    this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText;

//                    if (this.fromDatabaseGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabaseSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseSystemConfigurationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabaseAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseAnalogInputsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabaseCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseCalibrationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.fromDatabaseDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.fromDatabaseDataTransferTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDatabase)
//                    {
//                        if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                        {
//                            WriteAllInterfaceDataToWorkingConfiguration();
//                        }
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.FromDatabase;
//                        DisableGeneralSettingsTabConfigurationEditObjects();
//                        DisableSystemConfigurationTabConfigurationEditObjects();
//                        DisableAllAnalogInputsTabConfigurationEditObjects();
//                        DisableCalibrationTabConfigurationEditObjects();
//                        DisableDataTransferTabConfigurationEditObjects();
//                    }
//                    AddDataToAllTransientInterfaceObjects(this.configurationFromDatabase);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetFromDatabaseRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void SetWorkingRadRadioButtonState()
//        {
//            try
//            {
//                if (this.workingConfiguration != null)
//                {
//                    if (workingCopyIsFromDevice)
//                    {
//                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
//                    }
//                    else if(workingCopyIsFromDevice)
//                    {
//                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
//                    }
//                    else if (workingCopyIsFromInitialization)
//                    {
//                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
//                    }

//                    if (this.workingGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.workingGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.workingSystemConfigurationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.workingSystemConfigurationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.workingAnalogInputsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.workingAnalogInputsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.workingCalibrationTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.workingCalibrationTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.workingDataTransferTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
//                    {
//                        this.workingDataTransferTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    }
//                    if (this.configurationBeingDisplayed != ConfigurationDisplayed.Working)
//                    {
//                        AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
//                        this.configurationBeingDisplayed = ConfigurationDisplayed.Working;
//                        EnableGeneralSettingsTabConfigurationEditObjects();
//                        EnableSystemConfigurationTabConfigurationEditObjects();
//                        EnableAllAnalogInputsTabConfigurationEditObjects();
//                        EnableCalibrationTabConfigurationEditObjects();
//                        EnableDataTransferTabConfigurationEditObjects();                       
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetWorkingRadRadioButtonState()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void DisableFromDeviceRadRadioButtons()
//        {
//            this.fromDeviceGeneralSettingsTabRadRadioButton.Enabled = false;
//            this.fromDeviceSystemConfigurationTabRadRadioButton.Enabled = false;
//            this.fromDeviceAnalogInputsTabRadRadioButton.Enabled = false;
//            this.fromDeviceDataTransferTabRadRadioButton.Enabled = false; 
//        }

//        private void EnableFromDeviceRadRadioButtons()
//        {
//            if (this.configurationFromDevice != null)
//            {
//                this.fromDeviceGeneralSettingsTabRadRadioButton.Enabled = true;
//                this.fromDeviceSystemConfigurationTabRadRadioButton.Enabled = true;
//                this.fromDeviceAnalogInputsTabRadRadioButton.Enabled = true;
//                this.fromDeviceDataTransferTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void DisableFromDatabaseRadRadioButtons()
//        {
//            this.fromDatabaseGeneralSettingsTabRadRadioButton.Enabled = false;
//            this.fromDatabaseSystemConfigurationTabRadRadioButton.Enabled = false;
//            this.fromDatabaseAnalogInputsTabRadRadioButton.Enabled = false;
//            this.fromDatabaseDataTransferTabRadRadioButton.Enabled = false;
//        }

//        private void EnableFromDatabaseRadRadioButtons()
//        {
//            if (this.configurationFromDatabase != null)
//            {
//                this.fromDatabaseGeneralSettingsTabRadRadioButton.Enabled = true;
//                this.fromDatabaseSystemConfigurationTabRadRadioButton.Enabled = true;
//                this.fromDatabaseAnalogInputsTabRadRadioButton.Enabled = true;
//                this.fromDatabaseDataTransferTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void DisableWorkingRadRadioButtons()
//        {
//            this.workingGeneralSettingsTabRadRadioButton.Enabled = false;
//            this.workingSystemConfigurationTabRadRadioButton.Enabled = false;
//            this.workingAnalogInputsTabRadRadioButton.Enabled = false;
//            this.workingDataTransferTabRadRadioButton.Enabled = false;
//        }

//        private void EnableWorkingRadRadioButtons()
//        {
//            if (this.workingConfiguration != null)
//            {
//                this.workingGeneralSettingsTabRadRadioButton.Enabled = true;
//                this.workingSystemConfigurationTabRadRadioButton.Enabled = true;
//                this.workingAnalogInputsTabRadRadioButton.Enabled = true;
//                this.workingDataTransferTabRadRadioButton.Enabled = true;
//            }
//        }

//        private void CopyConfigurationFromDatabaseToWorking()
//        {
//            bool copyConfiguration = true;
//            if (this.configurationFromDatabase != null)
//            {
//                if (this.workingConfiguration != null)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                    if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
//                    {
//                        if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                        {
//                            copyConfiguration = false;
//                        }
//                    }
//                }
//                if (copyConfiguration)
//                {
//                    this.workingConfiguration = Main_Configuration.CopyConfiguration(this.configurationFromDatabase);
//                    this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
//                    EnableWorkingRadRadioButtons();
//                    workingCopyIsFromDatabase = true;
//                    workingCopyIsFromDevice = false;
//                    workingCopyIsFromInitialization = false;
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                    {
//                        AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
//                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
//                    }
//                    else
//                    {
//                        SetWorkingRadRadioButtonState();
//                    }
//                }
//            }
//            else
//            {
//                RadMessageBox.Show(this, noDatabaseConfigurationLoadedText);
//            }
//        }

//        private void CopyConfigurationFromDeviceToWorking()
//        {
//            bool copyConfiguration = true;
//            if (this.configurationFromDevice != null)
//            {
//                if (this.workingConfiguration != null)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                    if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
//                    {
//                        if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                        {
//                            copyConfiguration = false;
//                        }
//                    }
//                }
//                if (copyConfiguration)
//                {
//                    this.workingConfiguration = Main_Configuration.CopyConfiguration(this.configurationFromDevice);
//                    this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
//                    EnableWorkingRadRadioButtons();
//                    workingCopyIsFromDevice = true;
//                    workingCopyIsFromDatabase = false;
//                    workingCopyIsFromInitialization = false;
//                    if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                    {
//                        AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
//                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
//                    }
//                    else
//                    {
//                        SetWorkingRadRadioButtonState();
//                    }
//                }
//            }
//            else
//            {
//                RadMessageBox.Show(this, noDeviceConfigurationLoadedText);
//            }
//        }

//        private void InitializeWorkingConfiguration()
//        {
//            bool copyConfiguration = true;
//            if (this.workingConfiguration != null)
//            {
//                WriteAllInterfaceDataToWorkingConfiguration();
//                if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
//                {
//                    if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
//                    {
//                        copyConfiguration = false;
//                    }
//                }
//            }
//            if (copyConfiguration)
//            {
//                this.workingConfiguration = new Main_Configuration();
//                this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);

//                EnableWorkingRadRadioButtons();
//                workingCopyIsFromInitialization = true;
//                workingCopyIsFromDevice = false;
//                workingCopyIsFromDatabase = false;
//                if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                {
//                    AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
//                    this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
//                }
//                else
//                {
//                    SetWorkingRadRadioButtonState();
//                }
//            }
//        }

//        private void Main_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
//        {
//            try
//            {
//                /// If we are about to exit while editing the working configuration, we want to write all 
//                /// changes
//                if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
//                {
//                    WriteAllInterfaceDataToWorkingConfiguration();
//                }
//                if (this.workingConfiguration != null)
//                {
//                    if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
//                    {
//                        if (RadMessageBox.Show(this, currentConfigurationNotSavedWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
//                        {
//                            e.Cancel = true;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_MonitorConfiguration_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void DisableAllControls()
        {
            saveWorkingConfigurationGeneralSettingsTabRadButton.Enabled = false;
            //saveDeviceConfigurationGeneralSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Enabled = false;
            programDeviceGeneralSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceGeneralSettingsTabRadButton.Enabled = false;
            //deleteConfigurationGeneralSettingsTabRadButton.Enabled = false;

            saveWorkingConfigurationSystemConfigurationTabRadButton.Enabled = false;
            //saveDeviceConfigurationSystemConfigurationTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Enabled = false;
            programDeviceSystemConfigurationTabRadButton.Enabled = false;
            loadConfigurationFromDeviceSystemConfigurationTabRadButton.Enabled = false;
            //deleteConfigurationSystemConfigurationTabRadButton.Enabled = false;

            saveWorkingConfigurationAnalogInputsTabRadButton.Enabled = false;
            //saveDeviceConfigurationAnalogInputsTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseAnalogInputsTabRadButton.Enabled = false;
            programDeviceAnalogInputsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceAnalogInputsTabRadButton.Enabled = false;
            //deleteConfigurationAnalogInputsTabRadButton.Enabled = false;

            saveWorkingConfigurationCalibrationTabRadButton.Enabled = false;
            //saveDeviceConfigurationCalibrationTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseCalibrationTabRadButton.Enabled = false;
            programDeviceCalibrationTabRadButton.Enabled = false;
            loadConfigurationFromDeviceCalibrationTabRadButton.Enabled = false;
            //deleteConfigurationCalibrationTabRadButton.Enabled = false;

            saveWorkingConfigurationDataTransferTabRadButton.Enabled = false;
            //saveDeviceConfigurationDataTransferTabRadButton.Enabled = false;
            loadConfigurationFromDatabaseDataTransferTabRadButton.Enabled = false;
            programDeviceDataTransferTabRadButton.Enabled = false;
            loadConfigurationFromDeviceDataTransferTabRadButton.Enabled = false;
            //deleteConfigurationDataTransferTabRadButton.Enabled = false;

            //DisableFromDatabaseRadRadioButtons();
            //DisableFromDeviceRadRadioButtons();
            //DisableWorkingRadRadioButtons();

            //DisableCopyDatabaseConfigurationRadButtons();
            //DisableCopyDeviceConfigurationRadButtons();
            //DisableInitializeWorkingConfigurationRadButtons();

            DisableGeneralSettingsTabConfigurationEditObjects();
            DisableSystemConfigurationTabConfigurationEditObjects();
            DisableAllAnalogInputsTabConfigurationEditObjects();
            DisableCalibrationTabConfigurationEditObjects();
            DisableDataTransferTabConfigurationEditObjects(); 
        }

        //private void DisableCopyDatabaseConfigurationRadButtons()
        //{
        //    copyDatabaseConfigurationGeneralSettingsTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationSystemConfigurationTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationAnalogInputsTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationCalibrationTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationDataTransferTabRadButton.Enabled = false;
        //}

        //private void DisableCopyDeviceConfigurationRadButtons()
        //{
        //    copyDeviceConfigurationGeneralSettingsTabRadButton.Enabled = false;
        //    copyDeviceConfigurationSystemConfigurationTabRadButton.Enabled = false;
        //    copyDeviceConfigurationAnalogInputsTabRadButton.Enabled = false;
        //    copyDeviceConfigurationCalibrationTabRadButton.Enabled = false;
        //    copyDeviceConfigurationDataTransferTabRadButton.Enabled = false;
        //}

        //private void DisableInitializeWorkingConfigurationRadButtons()
        //{
        //    initializeWorkingConfigurationGeneralSettingsTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationSystemConfigurationTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationAnalogInputsTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationCalibrationTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationDataTransferTabRadButton.Enabled = false;
        //}

        private void EnableAllControls()
        {
            saveWorkingConfigurationGeneralSettingsTabRadButton.Enabled = true;
            //saveDeviceConfigurationGeneralSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Enabled = true;
            programDeviceGeneralSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceGeneralSettingsTabRadButton.Enabled = true;
            //deleteConfigurationGeneralSettingsTabRadButton.Enabled = true;

            saveWorkingConfigurationSystemConfigurationTabRadButton.Enabled = true;
            //saveDeviceConfigurationSystemConfigurationTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Enabled = true;
            programDeviceSystemConfigurationTabRadButton.Enabled = true;
            loadConfigurationFromDeviceSystemConfigurationTabRadButton.Enabled = true;
            //deleteConfigurationSystemConfigurationTabRadButton.Enabled = true;

            saveWorkingConfigurationAnalogInputsTabRadButton.Enabled = true;
           // saveDeviceConfigurationAnalogInputsTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseAnalogInputsTabRadButton.Enabled = true;
            programDeviceAnalogInputsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceAnalogInputsTabRadButton.Enabled = true;
            //deleteConfigurationAnalogInputsTabRadButton.Enabled = true;

            saveWorkingConfigurationCalibrationTabRadButton.Enabled = true;
            //saveDeviceConfigurationCalibrationTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseCalibrationTabRadButton.Enabled = true;
            programDeviceCalibrationTabRadButton.Enabled = true;
            loadConfigurationFromDeviceCalibrationTabRadButton.Enabled = true;
            //deleteConfigurationCalibrationTabRadButton.Enabled = true;

            saveWorkingConfigurationDataTransferTabRadButton.Enabled = true;
            //saveDeviceConfigurationDataTransferTabRadButton.Enabled = true;
            loadConfigurationFromDatabaseDataTransferTabRadButton.Enabled = true;
            programDeviceDataTransferTabRadButton.Enabled = true;
            loadConfigurationFromDeviceDataTransferTabRadButton.Enabled = true;
            //deleteConfigurationDataTransferTabRadButton.Enabled = true;

            //EnableFromDatabaseRadRadioButtons();
            //EnableFromDeviceRadRadioButtons();
            //EnableWorkingRadRadioButtons();

            //EnableCopyDatabaseConfigurationRadButtons();
            //EnableCopyDeviceConfigurationRadButtons();
            //EnableInitializeWorkingConfigurationRadButtons();

            EnableGeneralSettingsTabConfigurationEditObjects();
            EnableSystemConfigurationTabConfigurationEditObjects();
            EnableAllAnalogInputsTabConfigurationEditObjects();
            EnableCalibrationTabConfigurationEditObjects();
            EnableDataTransferTabConfigurationEditObjects(); 
        }

        //private void EnableCopyDatabaseConfigurationRadButtons()
        //{
        //    if (this.configurationFromDatabase != null)
        //    {
        //        copyDatabaseConfigurationGeneralSettingsTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationSystemConfigurationTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationAnalogInputsTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationCalibrationTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationDataTransferTabRadButton.Enabled = true;
        //    }
        //}

        //private void EnableCopyDeviceConfigurationRadButtons()
        //{
        //    if (this.configurationFromDevice != null)
        //    {
        //        copyDeviceConfigurationGeneralSettingsTabRadButton.Enabled = true;
        //        copyDeviceConfigurationSystemConfigurationTabRadButton.Enabled = true;
        //        copyDeviceConfigurationAnalogInputsTabRadButton.Enabled = true;
        //        copyDeviceConfigurationCalibrationTabRadButton.Enabled = true;
        //        copyDeviceConfigurationDataTransferTabRadButton.Enabled = true;
        //    }
        //}

        //private void EnableInitializeWorkingConfigurationRadButtons()
        //{
        //    initializeWorkingConfigurationGeneralSettingsTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationSystemConfigurationTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationAnalogInputsTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationCalibrationTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationDataTransferTabRadButton.Enabled = true;
        //}

        private void EnableProgressBars()
        {
            generalSettingsRadProgressBar.Visible = true;
            systemConfigurationRadProgressBar.Visible = true;
            analogInputsRadProgressBar.Visible = true;
            calibrationRadProgressBar.Visible = true;
            dataTransferRadProgressBar.Visible = true;
        }

        private void DisableProgressBars()
        {
            generalSettingsRadProgressBar.Visible = false;
            systemConfigurationRadProgressBar.Visible = false;
            analogInputsRadProgressBar.Visible = false;
            calibrationRadProgressBar.Visible = false;
            dataTransferRadProgressBar.Visible = false;
        }

        private void SetProgressBarsToUploadState()
        {
            generalSettingsRadProgressBar.Text = uploadingConfigurationText;
            systemConfigurationRadProgressBar.Text = uploadingConfigurationText;
            analogInputsRadProgressBar.Text = uploadingConfigurationText;
            calibrationRadProgressBar.Text = uploadingConfigurationText;
            dataTransferRadProgressBar.Text = uploadingConfigurationText;
        }

        private void SetProgressBarsToDownloadState()
        {
            generalSettingsRadProgressBar.Text = downloadingConfigurationText;
            systemConfigurationRadProgressBar.Text = downloadingConfigurationText;
            analogInputsRadProgressBar.Text = downloadingConfigurationText;
            calibrationRadProgressBar.Text = downloadingConfigurationText;
            dataTransferRadProgressBar.Text = downloadingConfigurationText;
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    generalSettingsRadProgressBar.Value1 = currentProgress;
                    systemConfigurationRadProgressBar.Value1 = currentProgress;
                    analogInputsRadProgressBar.Value1 = currentProgress;
                    calibrationRadProgressBar.Value1 = currentProgress;
                    dataTransferRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsSystemConfigurationTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsSystemConfigurationTabRadDropDownList.Text = description;
                    templateConfigurationsAnalogInputsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsAnalogInputsTabRadDropDownList.Text = description;
                    templateConfigurationsCalibrationTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCalibrationTabRadDropDownList.Text = description;
                    templateConfigurationsDataTransferTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsDataTransferTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsSystemConfigurationTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsSystemConfigurationTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsSystemConfigurationTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsAnalogInputsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsAnalogInputsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsAnalogInputsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsCalibrationTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCalibrationTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsCalibrationTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsDataTransferTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsDataTransferTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsDataTransferTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void copySelectedConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Main_Config_ConfigurationRoot templateConfigurationRoot;
                Guid destinationConfigurationRootID;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.templateConfigurations != null)
                    {
                        if (this.templateConfigurations.Count > 0)
                        {
                            if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                            {
                                templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                                if (templateConfigurationRoot != null)
                                {
                                    destinationConfigurationRootID = Guid.NewGuid();
                                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.dbConnectionString))
                                    using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                    {
                                        if (MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(templateConfigurationRoot.ID, templateDB, destinationConfigurationRootID, Main_MonitorConfiguration.monitor.ID, destinationDB))
                                        {
                                            // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopySucceeded));
                                        }
                                        else
                                        {
                                            RadMessageBox.SetThemeName("office2007BlackTheme");
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopyFailed));
                                        }
                                        LoadAvailableConfigurations(destinationDB);
                                    }
                                }
                                else
                                {
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                                }
                            }
                            else
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                            }
                        }
                        else
                        {
                            RadMessageBox.SetThemeName("office2007BlackTheme");
                            RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                        string errorMessage = "Error in Main_MonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        } 
    }

    public class TransferSetupData
    {
        public int transferSetupNumber;
        public bool enabled;
        public int readFrequency;
        public int readPoints;
        public int averages;
        public double saveData;
        // transfer setup number : enable measurement : master dev register : slave dev name : slave dev modbus : slave dev register : slope
    }


    public class MainConfigUISystemConfigurationData
    {
        public int moduleNumber;
        public string deviceType;
        public int modBusAddress;
        public int baudRate;
        public int dataTransferInterval;
    }

    public class InputChannelData
    {
        public int inputChannelNumber;
        public int readStatus;
        public int sensorType;
        public int unit;
        public int sensorID;
        public int sensorNumber;
        public double sensitivity;
        public int highPassFilter;
        public int lowPassFilter;
        public int masterDeviceModBusAddress;
        public int masterDeviceRegister;
        public int slaveDeviceModBusAddress;
        public int slaveDeviceRegister;
        public double slope;
        public int reserved;
        public int working;
    }

    public enum ConfigurationDisplayed
    {
        FromDatabase,
        FromDevice,
        Working, 
        None
    }
}
