namespace MainMonitorUtilities
{
    partial class Main_MonitorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_MonitorConfiguration));
            this.configurationRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.generalSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsGeneralSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.generalSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsGeneralSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.otherOptionsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.enableCurrentSignatureAnalysisRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.allowDirectAccessRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.modeSaveRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.useDiagnosticResultsRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.minutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.stepRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modeSaveIntervalRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.frequencyRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.hertzRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.frequencyRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.relayRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.generalSettingsWarningRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.generalSettingsAlarmRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.relayWarningRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.relayAlarmRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.versionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firmwareVersionRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.connectionParametersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.ethernetRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.ethernetProtocolRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.ethernetProtocolRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ipAddressRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.ipAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.ethernetBaudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ethernetBaudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serialConnectionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.serialBaudRateRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.serialBaudRateRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.modbusAddressRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.modbusAddressRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.boardNameRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.objectNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.monitoringRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.disableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.systemConfigurationRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.databaseInteractionSystemConfigurationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsSystemConfigurationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsSystemConfigurationTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionSystemConfigurationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.systemConfigurationRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsSystemConfigurationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsSystemConfigurationTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.systemConfigurationRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.analogInputsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.templateConfigurationsAnalogInputsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAnalogInputsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionAnalogInputsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionAnalogInputsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.analogInputsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsAnalogInputsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsAnalogInputsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            this.chassisTemperatureRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.temperatureRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.humidityRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.voltageRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.currentRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.analogInRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.vibrationRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.controlParametersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.chassisTemperatureRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.currentRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.analogInRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.temperatureRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.humidityRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.voltageRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.vibrationRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.calibrationRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.templateConfigurationsCalibrationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCalibrationTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton10 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionCalibrationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionCalibrationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseCalibrationTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.calibrationRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.calibrationRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsCalibrationTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsCalibrationTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton15 = new Telerik.WinControls.UI.RadButton();
            this.dataTransferRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.templateConfigurationsDataTransferTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsDataTransferTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton13 = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionDataTransferTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionDataTransferTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceDataTransferTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.dataTransferRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsDataTransferTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsDataTransferTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton17 = new Telerik.WinControls.UI.RadButton();
            this.dataTransferRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).BeginInit();
            this.configurationRadPageView.SuspendLayout();
            this.generalSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionGeneralSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionGeneralSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsGeneralSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.otherOptionsRadGroupBox)).BeginInit();
            this.otherOptionsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enableCurrentSignatureAnalysisRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowDirectAccessRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeSaveRadGroupBox)).BeginInit();
            this.modeSaveRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.useDiagnosticResultsRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeSaveIntervalRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadGroupBox)).BeginInit();
            this.frequencyRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hertzRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayRadGroupBox)).BeginInit();
            this.relayRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsWarningRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsAlarmRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayWarningRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayAlarmRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.versionRadGroupBox)).BeginInit();
            this.versionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionParametersRadGroupBox)).BeginInit();
            this.connectionParametersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetRadGroupBox)).BeginInit();
            this.ethernetRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetProtocolRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetProtocolRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetBaudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetBaudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialConnectionRadGroupBox)).BeginInit();
            this.serialConnectionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialBaudRateRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialBaudRateRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardNameRadGroupBox)).BeginInit();
            this.boardNameRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).BeginInit();
            this.monitoringRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).BeginInit();
            this.systemConfigurationRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionSystemConfigurationTabRadGroupBox)).BeginInit();
            this.databaseInteractionSystemConfigurationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSystemConfigurationTabRadGroupBox)).BeginInit();
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSystemConfigurationTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionSystemConfigurationTabRadGroupBox)).BeginInit();
            this.deviceInteractionSystemConfigurationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceSystemConfigurationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsSystemConfigurationTabRadGroupBox)).BeginInit();
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsSystemConfigurationTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadGridView.MasterTemplate)).BeginInit();
            this.analogInputsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAnalogInputsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsAnalogInputsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAnalogInputsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAnalogInputsTabRadGroupBox)).BeginInit();
            this.databaseInteractionAnalogInputsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAnalogInputsTabRadGroupBox)).BeginInit();
            this.deviceInteractionAnalogInputsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAnalogInputsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInputsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAnalogInputsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsAnalogInputsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAnalogInputsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlParametersRadGroupBox)).BeginInit();
            this.controlParametersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadRadioButton)).BeginInit();
            this.calibrationRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalibrationTabRadGroupBox)).BeginInit();
            this.templateConfigurationsCalibrationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalibrationTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCalibrationTabRadGroupBox)).BeginInit();
            this.deviceInteractionCalibrationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCalibrationTabRadGroupBox)).BeginInit();
            this.databaseInteractionCalibrationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCalibrationTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalibrationTabRadGroupBox)).BeginInit();
            this.availableConfigurationsCalibrationTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalibrationTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).BeginInit();
            this.dataTransferRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsDataTransferTabRadGroupBox)).BeginInit();
            this.templateConfigurationsDataTransferTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsDataTransferTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionDataTransferTabRadGroupBox)).BeginInit();
            this.databaseInteractionDataTransferTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionDataTransferTabRadGroupBox)).BeginInit();
            this.deviceInteractionDataTransferTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceDataTransferTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsDataTransferTabRadGroupBox)).BeginInit();
            this.availableConfigurationsDataTransferTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsDataTransferTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationRadPageView
            // 
            this.configurationRadPageView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.configurationRadPageView.Controls.Add(this.generalSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.systemConfigurationRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.analogInputsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.calibrationRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.dataTransferRadPageViewPage);
            this.configurationRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configurationRadPageView.Location = new System.Drawing.Point(0, 0);
            this.configurationRadPageView.Name = "configurationRadPageView";
            this.configurationRadPageView.SelectedPage = this.generalSettingsRadPageViewPage;
            this.configurationRadPageView.Size = new System.Drawing.Size(872, 677);
            this.configurationRadPageView.TabIndex = 0;
            this.configurationRadPageView.Text = "radPageView1";
            this.configurationRadPageView.ThemeName = "Office2007Black";
            this.configurationRadPageView.SelectedPageChanging += new System.EventHandler<Telerik.WinControls.UI.RadPageViewCancelEventArgs>(this.configurationRadPageView_SelectedPageChanging);
            this.configurationRadPageView.SelectedPageChanged += new System.EventHandler(this.configurationRadPageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.configurationRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // generalSettingsRadPageViewPage
            // 
            this.generalSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.generalSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsGeneralSettingsTabRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.databaseInteractionGeneralSettingsTabRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.deviceInteractionGeneralSettingsTabRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.generalSettingsRadProgressBar);
            this.generalSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsGeneralSettingsTabRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.otherOptionsRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.modeSaveRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.frequencyRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.relayRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.versionRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.connectionParametersRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.boardNameRadGroupBox);
            this.generalSettingsRadPageViewPage.Controls.Add(this.monitoringRadGroupBox);
            this.generalSettingsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generalSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.generalSettingsRadPageViewPage.Name = "generalSettingsRadPageViewPage";
            this.generalSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 629);
            this.generalSettingsRadPageViewPage.Text = "General Settings";
            // 
            // templateConfigurationsGeneralSettingsTabRadGroupBox
            // 
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationGeneralSettingsTabRadButton);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsGeneralSettingsTabRadDropDownList);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.radButton1);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 364);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Name = "templateConfigurationsGeneralSettingsTabRadGroupBox";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.TabIndex = 44;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationGeneralSettingsTabRadButton
            // 
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Name = "copySelectedConfigurationGeneralSettingsTabRadButton";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationGeneralSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsGeneralSettingsTabRadDropDownList
            // 
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Name = "templateConfigurationsGeneralSettingsTabRadDropDownList";
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // databaseInteractionGeneralSettingsTabRadGroupBox
            // 
            this.databaseInteractionGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationGeneralSettingsTabRadButton);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationGeneralSettingsTabRadButton);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(2, 434);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Name = "databaseInteractionGeneralSettingsTabRadGroupBox";
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionGeneralSettingsTabRadGroupBox.TabIndex = 43;
            this.databaseInteractionGeneralSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationGeneralSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Name = "deleteSelectedConfigurationGeneralSettingsTabRadButton";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.TabIndex = 32;
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseGeneralSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Name = "loadConfigurationFromDatabaseGeneralSettingsTabRadButton";
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.TabIndex = 33;
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // saveWorkingConfigurationGeneralSettingsTabRadButton
            // 
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Name = "saveWorkingConfigurationGeneralSettingsTabRadButton";
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.TabIndex = 31;
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationGeneralSettingsTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // deviceInteractionGeneralSettingsTabRadGroupBox
            // 
            this.deviceInteractionGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Controls.Add(this.programDeviceGeneralSettingsTabRadButton);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceGeneralSettingsTabRadButton);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(697, 434);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Name = "deviceInteractionGeneralSettingsTabRadGroupBox";
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionGeneralSettingsTabRadGroupBox.TabIndex = 32;
            this.deviceInteractionGeneralSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceGeneralSettingsTabRadButton
            // 
            this.programDeviceGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceGeneralSettingsTabRadButton.Name = "programDeviceGeneralSettingsTabRadButton";
            this.programDeviceGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceGeneralSettingsTabRadButton.TabIndex = 30;
            this.programDeviceGeneralSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceGeneralSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceGeneralSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Name = "loadConfigurationFromDeviceGeneralSettingsTabRadButton";
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.TabIndex = 34;
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceGeneralSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // generalSettingsRadProgressBar
            // 
            this.generalSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.generalSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.generalSettingsRadProgressBar.ImageIndex = -1;
            this.generalSettingsRadProgressBar.ImageKey = "";
            this.generalSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.generalSettingsRadProgressBar.Location = new System.Drawing.Point(567, 398);
            this.generalSettingsRadProgressBar.Name = "generalSettingsRadProgressBar";
            this.generalSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.generalSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.generalSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.generalSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.generalSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.generalSettingsRadProgressBar.TabIndex = 42;
            this.generalSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsGeneralSettingsTabRadGroupBox
            // 
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsGeneralSettingsTabRadListControl);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.radButton5);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(159, 434);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Name = "availableConfigurationsGeneralSettingsTabRadGroupBox";
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(532, 192);
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.TabIndex = 32;
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsGeneralSettingsTabRadListControl
            // 
            this.availableConfigurationsGeneralSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsGeneralSettingsTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsGeneralSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsGeneralSettingsTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsGeneralSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsGeneralSettingsTabRadListControl.Name = "availableConfigurationsGeneralSettingsTabRadListControl";
            this.availableConfigurationsGeneralSettingsTabRadListControl.Size = new System.Drawing.Size(512, 162);
            this.availableConfigurationsGeneralSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsGeneralSettingsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsGeneralSettingsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsGeneralSettingsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton5
            // 
            this.radButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(0, 232);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(130, 70);
            this.radButton5.TabIndex = 8;
            this.radButton5.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton5.ThemeName = "Office2007Black";
            // 
            // otherOptionsRadGroupBox
            // 
            this.otherOptionsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.otherOptionsRadGroupBox.Controls.Add(this.enableCurrentSignatureAnalysisRadCheckBox);
            this.otherOptionsRadGroupBox.Controls.Add(this.allowDirectAccessRadCheckBox);
            this.otherOptionsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.otherOptionsRadGroupBox.FooterImageIndex = -1;
            this.otherOptionsRadGroupBox.FooterImageKey = "";
            this.otherOptionsRadGroupBox.HeaderImageIndex = -1;
            this.otherOptionsRadGroupBox.HeaderImageKey = "";
            this.otherOptionsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.otherOptionsRadGroupBox.HeaderText = "Other options";
            this.otherOptionsRadGroupBox.Location = new System.Drawing.Point(428, 3);
            this.otherOptionsRadGroupBox.Name = "otherOptionsRadGroupBox";
            this.otherOptionsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.otherOptionsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.otherOptionsRadGroupBox.Size = new System.Drawing.Size(195, 81);
            this.otherOptionsRadGroupBox.TabIndex = 29;
            this.otherOptionsRadGroupBox.Text = "Other options";
            this.otherOptionsRadGroupBox.ThemeName = "Office2007Black";
            this.otherOptionsRadGroupBox.Visible = false;
            // 
            // enableCurrentSignatureAnalysisRadCheckBox
            // 
            this.enableCurrentSignatureAnalysisRadCheckBox.Enabled = false;
            this.enableCurrentSignatureAnalysisRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableCurrentSignatureAnalysisRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableCurrentSignatureAnalysisRadCheckBox.Location = new System.Drawing.Point(13, 57);
            this.enableCurrentSignatureAnalysisRadCheckBox.Name = "enableCurrentSignatureAnalysisRadCheckBox";
            this.enableCurrentSignatureAnalysisRadCheckBox.Size = new System.Drawing.Size(180, 15);
            this.enableCurrentSignatureAnalysisRadCheckBox.TabIndex = 31;
            this.enableCurrentSignatureAnalysisRadCheckBox.Text = "<html>Enable current signature analysis</html>";
            this.enableCurrentSignatureAnalysisRadCheckBox.Visible = false;
            // 
            // allowDirectAccessRadCheckBox
            // 
            this.allowDirectAccessRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allowDirectAccessRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allowDirectAccessRadCheckBox.Location = new System.Drawing.Point(13, 23);
            this.allowDirectAccessRadCheckBox.Name = "allowDirectAccessRadCheckBox";
            this.allowDirectAccessRadCheckBox.Size = new System.Drawing.Size(162, 27);
            this.allowDirectAccessRadCheckBox.TabIndex = 30;
            this.allowDirectAccessRadCheckBox.Text = "<html>Allow direct access to the<br>internal modules over RS 485</html>";
            // 
            // modeSaveRadGroupBox
            // 
            this.modeSaveRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.modeSaveRadGroupBox.Controls.Add(this.useDiagnosticResultsRadCheckBox);
            this.modeSaveRadGroupBox.Controls.Add(this.minutesRadLabel);
            this.modeSaveRadGroupBox.Controls.Add(this.stepRadLabel);
            this.modeSaveRadGroupBox.Controls.Add(this.modeSaveIntervalRadSpinEditor);
            this.modeSaveRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeSaveRadGroupBox.FooterImageIndex = -1;
            this.modeSaveRadGroupBox.FooterImageKey = "";
            this.modeSaveRadGroupBox.HeaderImageIndex = -1;
            this.modeSaveRadGroupBox.HeaderImageKey = "";
            this.modeSaveRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.modeSaveRadGroupBox.HeaderText = "Mode Save";
            this.modeSaveRadGroupBox.Location = new System.Drawing.Point(204, 252);
            this.modeSaveRadGroupBox.Name = "modeSaveRadGroupBox";
            this.modeSaveRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.modeSaveRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.modeSaveRadGroupBox.Size = new System.Drawing.Size(176, 86);
            this.modeSaveRadGroupBox.TabIndex = 28;
            this.modeSaveRadGroupBox.Text = "Mode Save";
            this.modeSaveRadGroupBox.ThemeName = "Office2007Black";
            // 
            // useDiagnosticResultsRadCheckBox
            // 
            this.useDiagnosticResultsRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useDiagnosticResultsRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.useDiagnosticResultsRadCheckBox.Location = new System.Drawing.Point(22, 48);
            this.useDiagnosticResultsRadCheckBox.Name = "useDiagnosticResultsRadCheckBox";
            this.useDiagnosticResultsRadCheckBox.Size = new System.Drawing.Size(115, 27);
            this.useDiagnosticResultsRadCheckBox.TabIndex = 29;
            this.useDiagnosticResultsRadCheckBox.Text = "<html>Take into account<br>results of diagnostic</html>";
            // 
            // minutesRadLabel
            // 
            this.minutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.minutesRadLabel.Location = new System.Drawing.Point(104, 29);
            this.minutesRadLabel.Name = "minutesRadLabel";
            this.minutesRadLabel.Size = new System.Drawing.Size(46, 16);
            this.minutesRadLabel.TabIndex = 30;
            this.minutesRadLabel.Text = "minutes";
            // 
            // stepRadLabel
            // 
            this.stepRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stepRadLabel.Location = new System.Drawing.Point(13, 29);
            this.stepRadLabel.Name = "stepRadLabel";
            this.stepRadLabel.Size = new System.Drawing.Size(29, 16);
            this.stepRadLabel.TabIndex = 29;
            this.stepRadLabel.Text = "Step";
            // 
            // modeSaveIntervalRadSpinEditor
            // 
            this.modeSaveIntervalRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeSaveIntervalRadSpinEditor.Location = new System.Drawing.Point(48, 25);
            this.modeSaveIntervalRadSpinEditor.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.modeSaveIntervalRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.modeSaveIntervalRadSpinEditor.Name = "modeSaveIntervalRadSpinEditor";
            // 
            // 
            // 
            this.modeSaveIntervalRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.modeSaveIntervalRadSpinEditor.ShowBorder = true;
            this.modeSaveIntervalRadSpinEditor.Size = new System.Drawing.Size(50, 19);
            this.modeSaveIntervalRadSpinEditor.TabIndex = 28;
            this.modeSaveIntervalRadSpinEditor.TabStop = false;
            this.modeSaveIntervalRadSpinEditor.ThemeName = "Office2007Black";
            this.modeSaveIntervalRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // frequencyRadGroupBox
            // 
            this.frequencyRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.frequencyRadGroupBox.Controls.Add(this.hertzRadLabel);
            this.frequencyRadGroupBox.Controls.Add(this.frequencyRadSpinEditor);
            this.frequencyRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frequencyRadGroupBox.FooterImageIndex = -1;
            this.frequencyRadGroupBox.FooterImageKey = "";
            this.frequencyRadGroupBox.HeaderImageIndex = -1;
            this.frequencyRadGroupBox.HeaderImageKey = "";
            this.frequencyRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.frequencyRadGroupBox.HeaderText = "Frequency";
            this.frequencyRadGroupBox.Location = new System.Drawing.Point(147, 3);
            this.frequencyRadGroupBox.Name = "frequencyRadGroupBox";
            this.frequencyRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.frequencyRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.frequencyRadGroupBox.Size = new System.Drawing.Size(106, 51);
            this.frequencyRadGroupBox.TabIndex = 26;
            this.frequencyRadGroupBox.Text = "Frequency";
            this.frequencyRadGroupBox.ThemeName = "Office2007Black";
            // 
            // hertzRadLabel
            // 
            this.hertzRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hertzRadLabel.Location = new System.Drawing.Point(69, 27);
            this.hertzRadLabel.Name = "hertzRadLabel";
            this.hertzRadLabel.Size = new System.Drawing.Size(23, 16);
            this.hertzRadLabel.TabIndex = 29;
            this.hertzRadLabel.Text = "Hz.";
            // 
            // frequencyRadSpinEditor
            // 
            this.frequencyRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frequencyRadSpinEditor.Location = new System.Drawing.Point(13, 23);
            this.frequencyRadSpinEditor.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.frequencyRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.frequencyRadSpinEditor.Name = "frequencyRadSpinEditor";
            // 
            // 
            // 
            this.frequencyRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.frequencyRadSpinEditor.ShowBorder = true;
            this.frequencyRadSpinEditor.Size = new System.Drawing.Size(50, 19);
            this.frequencyRadSpinEditor.TabIndex = 27;
            this.frequencyRadSpinEditor.TabStop = false;
            this.frequencyRadSpinEditor.ThemeName = "Office2007Black";
            this.frequencyRadSpinEditor.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // relayRadGroupBox
            // 
            this.relayRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.relayRadGroupBox.Controls.Add(this.generalSettingsWarningRadLabel);
            this.relayRadGroupBox.Controls.Add(this.generalSettingsAlarmRadLabel);
            this.relayRadGroupBox.Controls.Add(this.relayWarningRadDropDownList);
            this.relayRadGroupBox.Controls.Add(this.relayAlarmRadDropDownList);
            this.relayRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayRadGroupBox.FooterImageIndex = -1;
            this.relayRadGroupBox.FooterImageKey = "";
            this.relayRadGroupBox.HeaderImageIndex = -1;
            this.relayRadGroupBox.HeaderImageKey = "";
            this.relayRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.relayRadGroupBox.HeaderText = "Relay";
            this.relayRadGroupBox.Location = new System.Drawing.Point(3, 252);
            this.relayRadGroupBox.Name = "relayRadGroupBox";
            this.relayRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.relayRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.relayRadGroupBox.Size = new System.Drawing.Size(195, 86);
            this.relayRadGroupBox.TabIndex = 27;
            this.relayRadGroupBox.Text = "Relay";
            this.relayRadGroupBox.ThemeName = "Office2007Black";
            // 
            // generalSettingsWarningRadLabel
            // 
            this.generalSettingsWarningRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.generalSettingsWarningRadLabel.Location = new System.Drawing.Point(8, 57);
            this.generalSettingsWarningRadLabel.Name = "generalSettingsWarningRadLabel";
            this.generalSettingsWarningRadLabel.Size = new System.Drawing.Size(48, 16);
            this.generalSettingsWarningRadLabel.TabIndex = 33;
            this.generalSettingsWarningRadLabel.Text = "Warning";
            // 
            // generalSettingsAlarmRadLabel
            // 
            this.generalSettingsAlarmRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.generalSettingsAlarmRadLabel.Location = new System.Drawing.Point(20, 29);
            this.generalSettingsAlarmRadLabel.Name = "generalSettingsAlarmRadLabel";
            this.generalSettingsAlarmRadLabel.Size = new System.Drawing.Size(36, 16);
            this.generalSettingsAlarmRadLabel.TabIndex = 32;
            this.generalSettingsAlarmRadLabel.Text = "Alarm";
            // 
            // relayWarningRadDropDownList
            // 
            this.relayWarningRadDropDownList.DropDownAnimationEnabled = true;
            this.relayWarningRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayWarningRadDropDownList.Location = new System.Drawing.Point(78, 51);
            this.relayWarningRadDropDownList.Name = "relayWarningRadDropDownList";
            this.relayWarningRadDropDownList.ShowImageInEditorArea = true;
            this.relayWarningRadDropDownList.Size = new System.Drawing.Size(107, 20);
            this.relayWarningRadDropDownList.TabIndex = 31;
            this.relayWarningRadDropDownList.Text = "External Devices";
            this.relayWarningRadDropDownList.ThemeName = "Office2007Black";
            // 
            // relayAlarmRadDropDownList
            // 
            this.relayAlarmRadDropDownList.DropDownAnimationEnabled = true;
            this.relayAlarmRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relayAlarmRadDropDownList.Location = new System.Drawing.Point(78, 23);
            this.relayAlarmRadDropDownList.Name = "relayAlarmRadDropDownList";
            this.relayAlarmRadDropDownList.ShowImageInEditorArea = true;
            this.relayAlarmRadDropDownList.Size = new System.Drawing.Size(107, 20);
            this.relayAlarmRadDropDownList.TabIndex = 30;
            this.relayAlarmRadDropDownList.Text = "External Devices";
            this.relayAlarmRadDropDownList.ThemeName = "Office2007Black";
            // 
            // versionRadGroupBox
            // 
            this.versionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.versionRadGroupBox.Controls.Add(this.firmwareVersionRadTextBox);
            this.versionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionRadGroupBox.FooterImageIndex = -1;
            this.versionRadGroupBox.FooterImageKey = "";
            this.versionRadGroupBox.HeaderImageIndex = -1;
            this.versionRadGroupBox.HeaderImageKey = "";
            this.versionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.versionRadGroupBox.HeaderText = "Firmware Version";
            this.versionRadGroupBox.Location = new System.Drawing.Point(259, 3);
            this.versionRadGroupBox.Name = "versionRadGroupBox";
            this.versionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.versionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.versionRadGroupBox.Size = new System.Drawing.Size(121, 51);
            this.versionRadGroupBox.TabIndex = 26;
            this.versionRadGroupBox.Text = "Firmware Version";
            this.versionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firmwareVersionRadTextBox
            // 
            this.firmwareVersionRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionRadTextBox.Location = new System.Drawing.Point(46, 25);
            this.firmwareVersionRadTextBox.Name = "firmwareVersionRadTextBox";
            this.firmwareVersionRadTextBox.Size = new System.Drawing.Size(43, 18);
            this.firmwareVersionRadTextBox.TabIndex = 0;
            this.firmwareVersionRadTextBox.TabStop = false;
            this.firmwareVersionRadTextBox.Text = "0.00";
            // 
            // connectionParametersRadGroupBox
            // 
            this.connectionParametersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.connectionParametersRadGroupBox.Controls.Add(this.ethernetRadGroupBox);
            this.connectionParametersRadGroupBox.Controls.Add(this.serialConnectionRadGroupBox);
            this.connectionParametersRadGroupBox.Controls.Add(this.modbusAddressRadSpinEditor);
            this.connectionParametersRadGroupBox.Controls.Add(this.modbusAddressRadLabel);
            this.connectionParametersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectionParametersRadGroupBox.FooterImageIndex = -1;
            this.connectionParametersRadGroupBox.FooterImageKey = "";
            this.connectionParametersRadGroupBox.HeaderImageIndex = -1;
            this.connectionParametersRadGroupBox.HeaderImageKey = "";
            this.connectionParametersRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.connectionParametersRadGroupBox.HeaderText = "Connection Parameters";
            this.connectionParametersRadGroupBox.Location = new System.Drawing.Point(3, 117);
            this.connectionParametersRadGroupBox.Name = "connectionParametersRadGroupBox";
            this.connectionParametersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.connectionParametersRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.connectionParametersRadGroupBox.Size = new System.Drawing.Size(377, 129);
            this.connectionParametersRadGroupBox.TabIndex = 24;
            this.connectionParametersRadGroupBox.Text = "Connection Parameters";
            this.connectionParametersRadGroupBox.ThemeName = "Office2007Black";
            // 
            // ethernetRadGroupBox
            // 
            this.ethernetRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.ethernetRadGroupBox.Controls.Add(this.ethernetProtocolRadLabel);
            this.ethernetRadGroupBox.Controls.Add(this.ethernetProtocolRadDropDownList);
            this.ethernetRadGroupBox.Controls.Add(this.ipAddressRadTextBox);
            this.ethernetRadGroupBox.Controls.Add(this.ipAddressRadLabel);
            this.ethernetRadGroupBox.Controls.Add(this.ethernetBaudRateRadDropDownList);
            this.ethernetRadGroupBox.Controls.Add(this.ethernetBaudRateRadLabel);
            this.ethernetRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ethernetRadGroupBox.FooterImageIndex = -1;
            this.ethernetRadGroupBox.FooterImageKey = "";
            this.ethernetRadGroupBox.HeaderImageIndex = -1;
            this.ethernetRadGroupBox.HeaderImageKey = "";
            this.ethernetRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.ethernetRadGroupBox.HeaderText = "Ethernet";
            this.ethernetRadGroupBox.Location = new System.Drawing.Point(177, 14);
            this.ethernetRadGroupBox.Name = "ethernetRadGroupBox";
            this.ethernetRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.ethernetRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.ethernetRadGroupBox.Size = new System.Drawing.Size(188, 102);
            this.ethernetRadGroupBox.TabIndex = 28;
            this.ethernetRadGroupBox.Text = "Ethernet";
            this.ethernetRadGroupBox.ThemeName = "Office2007Black";
            // 
            // ethernetProtocolRadLabel
            // 
            this.ethernetProtocolRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ethernetProtocolRadLabel.Location = new System.Drawing.Point(11, 54);
            this.ethernetProtocolRadLabel.Name = "ethernetProtocolRadLabel";
            this.ethernetProtocolRadLabel.Size = new System.Drawing.Size(48, 16);
            this.ethernetProtocolRadLabel.TabIndex = 33;
            this.ethernetProtocolRadLabel.Text = "Protocol";
            // 
            // ethernetProtocolRadDropDownList
            // 
            this.ethernetProtocolRadDropDownList.DropDownAnimationEnabled = true;
            this.ethernetProtocolRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ethernetProtocolRadDropDownList.Location = new System.Drawing.Point(85, 50);
            this.ethernetProtocolRadDropDownList.Name = "ethernetProtocolRadDropDownList";
            this.ethernetProtocolRadDropDownList.ShowImageInEditorArea = true;
            this.ethernetProtocolRadDropDownList.Size = new System.Drawing.Size(93, 20);
            this.ethernetProtocolRadDropDownList.TabIndex = 32;
            this.ethernetProtocolRadDropDownList.Text = "RTU";
            this.ethernetProtocolRadDropDownList.ThemeName = "Office2007Black";
            // 
            // ipAddressRadTextBox
            // 
            this.ipAddressRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipAddressRadTextBox.Location = new System.Drawing.Point(85, 78);
            this.ipAddressRadTextBox.Name = "ipAddressRadTextBox";
            this.ipAddressRadTextBox.Size = new System.Drawing.Size(91, 18);
            this.ipAddressRadTextBox.TabIndex = 31;
            this.ipAddressRadTextBox.TabStop = false;
            this.ipAddressRadTextBox.Text = "000.000.000.000";
            // 
            // ipAddressRadLabel
            // 
            this.ipAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ipAddressRadLabel.Location = new System.Drawing.Point(11, 78);
            this.ipAddressRadLabel.Name = "ipAddressRadLabel";
            this.ipAddressRadLabel.Size = new System.Drawing.Size(68, 16);
            this.ipAddressRadLabel.TabIndex = 30;
            this.ipAddressRadLabel.Text = "I.P. Address";
            // 
            // ethernetBaudRateRadDropDownList
            // 
            this.ethernetBaudRateRadDropDownList.DropDownAnimationEnabled = true;
            this.ethernetBaudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ethernetBaudRateRadDropDownList.Location = new System.Drawing.Point(85, 22);
            this.ethernetBaudRateRadDropDownList.Name = "ethernetBaudRateRadDropDownList";
            this.ethernetBaudRateRadDropDownList.ShowImageInEditorArea = true;
            this.ethernetBaudRateRadDropDownList.Size = new System.Drawing.Size(93, 20);
            this.ethernetBaudRateRadDropDownList.TabIndex = 29;
            this.ethernetBaudRateRadDropDownList.Text = "115200";
            this.ethernetBaudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // ethernetBaudRateRadLabel
            // 
            this.ethernetBaudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ethernetBaudRateRadLabel.Location = new System.Drawing.Point(11, 27);
            this.ethernetBaudRateRadLabel.Name = "ethernetBaudRateRadLabel";
            this.ethernetBaudRateRadLabel.Size = new System.Drawing.Size(60, 16);
            this.ethernetBaudRateRadLabel.TabIndex = 28;
            this.ethernetBaudRateRadLabel.Text = "Baud Rate";
            // 
            // serialConnectionRadGroupBox
            // 
            this.serialConnectionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.serialConnectionRadGroupBox.Controls.Add(this.serialBaudRateRadDropDownList);
            this.serialConnectionRadGroupBox.Controls.Add(this.serialBaudRateRadLabel);
            this.serialConnectionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialConnectionRadGroupBox.FooterImageIndex = -1;
            this.serialConnectionRadGroupBox.FooterImageKey = "";
            this.serialConnectionRadGroupBox.HeaderImageIndex = -1;
            this.serialConnectionRadGroupBox.HeaderImageKey = "";
            this.serialConnectionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.serialConnectionRadGroupBox.HeaderText = "RS 485";
            this.serialConnectionRadGroupBox.Location = new System.Drawing.Point(13, 56);
            this.serialConnectionRadGroupBox.Name = "serialConnectionRadGroupBox";
            this.serialConnectionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.serialConnectionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.serialConnectionRadGroupBox.Size = new System.Drawing.Size(158, 60);
            this.serialConnectionRadGroupBox.TabIndex = 27;
            this.serialConnectionRadGroupBox.Text = "RS 485";
            this.serialConnectionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // serialBaudRateRadDropDownList
            // 
            this.serialBaudRateRadDropDownList.DropDownAnimationEnabled = true;
            this.serialBaudRateRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialBaudRateRadDropDownList.Location = new System.Drawing.Point(80, 24);
            this.serialBaudRateRadDropDownList.Name = "serialBaudRateRadDropDownList";
            this.serialBaudRateRadDropDownList.ShowImageInEditorArea = true;
            this.serialBaudRateRadDropDownList.Size = new System.Drawing.Size(67, 20);
            this.serialBaudRateRadDropDownList.TabIndex = 27;
            this.serialBaudRateRadDropDownList.Text = "115200";
            this.serialBaudRateRadDropDownList.ThemeName = "Office2007Black";
            // 
            // serialBaudRateRadLabel
            // 
            this.serialBaudRateRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serialBaudRateRadLabel.Location = new System.Drawing.Point(13, 28);
            this.serialBaudRateRadLabel.Name = "serialBaudRateRadLabel";
            this.serialBaudRateRadLabel.Size = new System.Drawing.Size(60, 16);
            this.serialBaudRateRadLabel.TabIndex = 26;
            this.serialBaudRateRadLabel.Text = "Baud Rate";
            // 
            // modbusAddressRadSpinEditor
            // 
            this.modbusAddressRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modbusAddressRadSpinEditor.Location = new System.Drawing.Point(110, 29);
            this.modbusAddressRadSpinEditor.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.modbusAddressRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.modbusAddressRadSpinEditor.Name = "modbusAddressRadSpinEditor";
            // 
            // 
            // 
            this.modbusAddressRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.modbusAddressRadSpinEditor.ShowBorder = true;
            this.modbusAddressRadSpinEditor.Size = new System.Drawing.Size(50, 19);
            this.modbusAddressRadSpinEditor.TabIndex = 26;
            this.modbusAddressRadSpinEditor.TabStop = false;
            this.modbusAddressRadSpinEditor.ThemeName = "Office2007Black";
            this.modbusAddressRadSpinEditor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // modbusAddressRadLabel
            // 
            this.modbusAddressRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.modbusAddressRadLabel.Location = new System.Drawing.Point(13, 34);
            this.modbusAddressRadLabel.Name = "modbusAddressRadLabel";
            this.modbusAddressRadLabel.Size = new System.Drawing.Size(91, 16);
            this.modbusAddressRadLabel.TabIndex = 25;
            this.modbusAddressRadLabel.Text = "Modbus Address";
            // 
            // boardNameRadGroupBox
            // 
            this.boardNameRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.boardNameRadGroupBox.Controls.Add(this.objectNameRadTextBox);
            this.boardNameRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boardNameRadGroupBox.FooterImageIndex = -1;
            this.boardNameRadGroupBox.FooterImageKey = "";
            this.boardNameRadGroupBox.HeaderImageIndex = -1;
            this.boardNameRadGroupBox.HeaderImageKey = "";
            this.boardNameRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.boardNameRadGroupBox.HeaderText = "Board Name";
            this.boardNameRadGroupBox.Location = new System.Drawing.Point(3, 60);
            this.boardNameRadGroupBox.Name = "boardNameRadGroupBox";
            this.boardNameRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.boardNameRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.boardNameRadGroupBox.Size = new System.Drawing.Size(377, 51);
            this.boardNameRadGroupBox.TabIndex = 23;
            this.boardNameRadGroupBox.Text = "Board Name";
            this.boardNameRadGroupBox.ThemeName = "Office2007Black";
            // 
            // objectNameRadTextBox
            // 
            this.objectNameRadTextBox.Location = new System.Drawing.Point(14, 23);
            this.objectNameRadTextBox.Name = "objectNameRadTextBox";
            this.objectNameRadTextBox.Size = new System.Drawing.Size(351, 20);
            this.objectNameRadTextBox.TabIndex = 0;
            this.objectNameRadTextBox.TabStop = false;
            this.objectNameRadTextBox.Text = "Not Specified";
            // 
            // monitoringRadGroupBox
            // 
            this.monitoringRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.monitoringRadGroupBox.Controls.Add(this.disableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Controls.Add(this.enableMonitoringRadRadioButton);
            this.monitoringRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitoringRadGroupBox.FooterImageIndex = -1;
            this.monitoringRadGroupBox.FooterImageKey = "";
            this.monitoringRadGroupBox.HeaderImageIndex = -1;
            this.monitoringRadGroupBox.HeaderImageKey = "";
            this.monitoringRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.monitoringRadGroupBox.HeaderText = "Monitoring";
            this.monitoringRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.monitoringRadGroupBox.Name = "monitoringRadGroupBox";
            this.monitoringRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.monitoringRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.monitoringRadGroupBox.Size = new System.Drawing.Size(138, 51);
            this.monitoringRadGroupBox.TabIndex = 22;
            this.monitoringRadGroupBox.Text = "Monitoring";
            this.monitoringRadGroupBox.ThemeName = "Office2007Black";
            // 
            // disableMonitoringRadRadioButton
            // 
            this.disableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableMonitoringRadRadioButton.Location = new System.Drawing.Point(75, 23);
            this.disableMonitoringRadRadioButton.Name = "disableMonitoringRadRadioButton";
            this.disableMonitoringRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.disableMonitoringRadRadioButton.TabIndex = 1;
            this.disableMonitoringRadRadioButton.Text = "Disable";
            // 
            // enableMonitoringRadRadioButton
            // 
            this.enableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableMonitoringRadRadioButton.Location = new System.Drawing.Point(10, 23);
            this.enableMonitoringRadRadioButton.Name = "enableMonitoringRadRadioButton";
            this.enableMonitoringRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.enableMonitoringRadRadioButton.TabIndex = 0;
            this.enableMonitoringRadRadioButton.Text = "Enable";
            // 
            // systemConfigurationRadPageViewPage
            // 
            this.systemConfigurationRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.systemConfigurationRadPageViewPage.Controls.Add(this.databaseInteractionSystemConfigurationTabRadGroupBox);
            this.systemConfigurationRadPageViewPage.Controls.Add(this.templateConfigurationsSystemConfigurationTabRadGroupBox);
            this.systemConfigurationRadPageViewPage.Controls.Add(this.deviceInteractionSystemConfigurationTabRadGroupBox);
            this.systemConfigurationRadPageViewPage.Controls.Add(this.systemConfigurationRadProgressBar);
            this.systemConfigurationRadPageViewPage.Controls.Add(this.availableConfigurationsSystemConfigurationTabRadGroupBox);
            this.systemConfigurationRadPageViewPage.Controls.Add(this.systemConfigurationRadGridView);
            this.systemConfigurationRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemConfigurationRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.systemConfigurationRadPageViewPage.Name = "systemConfigurationRadPageViewPage";
            this.systemConfigurationRadPageViewPage.Size = new System.Drawing.Size(851, 629);
            this.systemConfigurationRadPageViewPage.Text = "System Configuration";
            // 
            // databaseInteractionSystemConfigurationTabRadGroupBox
            // 
            this.databaseInteractionSystemConfigurationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationSystemConfigurationTabRadButton);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationSystemConfigurationTabRadButton);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionSystemConfigurationTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionSystemConfigurationTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionSystemConfigurationTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionSystemConfigurationTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionSystemConfigurationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Location = new System.Drawing.Point(2, 434);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Name = "databaseInteractionSystemConfigurationTabRadGroupBox";
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionSystemConfigurationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionSystemConfigurationTabRadGroupBox.TabIndex = 45;
            this.databaseInteractionSystemConfigurationTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionSystemConfigurationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationSystemConfigurationTabRadButton
            // 
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Name = "deleteSelectedConfigurationSystemConfigurationTabRadButton";
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.TabIndex = 24;
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationSystemConfigurationTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationSystemConfigurationTabRadButton
            // 
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Name = "saveWorkingConfigurationSystemConfigurationTabRadButton";
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.TabIndex = 23;
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationSystemConfigurationTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseSystemConfigurationTabRadButton
            // 
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Name = "loadConfigurationFromDatabaseSystemConfigurationTabRadButton";
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.TabIndex = 25;
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // templateConfigurationsSystemConfigurationTabRadGroupBox
            // 
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Controls.Add(this.copySelectedConfigurationSystemConfigurationTabRadButton);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Controls.Add(this.templateConfigurationsSystemConfigurationTabRadDropDownList);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Controls.Add(this.radButton6);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Location = new System.Drawing.Point(3, 364);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Name = "templateConfigurationsSystemConfigurationTabRadGroupBox";
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.TabIndex = 46;
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationSystemConfigurationTabRadButton
            // 
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Name = "copySelectedConfigurationSystemConfigurationTabRadButton";
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationSystemConfigurationTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationSystemConfigurationTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsSystemConfigurationTabRadDropDownList
            // 
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.Name = "templateConfigurationsSystemConfigurationTabRadDropDownList";
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsSystemConfigurationTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsSystemConfigurationTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton6
            // 
            this.radButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton6.Location = new System.Drawing.Point(0, 232);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(130, 70);
            this.radButton6.TabIndex = 8;
            this.radButton6.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton6.ThemeName = "Office2007Black";
            // 
            // deviceInteractionSystemConfigurationTabRadGroupBox
            // 
            this.deviceInteractionSystemConfigurationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Controls.Add(this.programDeviceSystemConfigurationTabRadButton);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceSystemConfigurationTabRadButton);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionSystemConfigurationTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionSystemConfigurationTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionSystemConfigurationTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionSystemConfigurationTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionSystemConfigurationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Location = new System.Drawing.Point(697, 434);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Name = "deviceInteractionSystemConfigurationTabRadGroupBox";
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionSystemConfigurationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionSystemConfigurationTabRadGroupBox.TabIndex = 45;
            this.deviceInteractionSystemConfigurationTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionSystemConfigurationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceSystemConfigurationTabRadButton
            // 
            this.programDeviceSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceSystemConfigurationTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceSystemConfigurationTabRadButton.Name = "programDeviceSystemConfigurationTabRadButton";
            this.programDeviceSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceSystemConfigurationTabRadButton.TabIndex = 22;
            this.programDeviceSystemConfigurationTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceSystemConfigurationTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceSystemConfigurationTabRadButton
            // 
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Name = "loadConfigurationFromDeviceSystemConfigurationTabRadButton";
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.TabIndex = 26;
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceSystemConfigurationTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // systemConfigurationRadProgressBar
            // 
            this.systemConfigurationRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.systemConfigurationRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.systemConfigurationRadProgressBar.ImageIndex = -1;
            this.systemConfigurationRadProgressBar.ImageKey = "";
            this.systemConfigurationRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.systemConfigurationRadProgressBar.Location = new System.Drawing.Point(567, 398);
            this.systemConfigurationRadProgressBar.Name = "systemConfigurationRadProgressBar";
            this.systemConfigurationRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.systemConfigurationRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.systemConfigurationRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.systemConfigurationRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.systemConfigurationRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.systemConfigurationRadProgressBar.TabIndex = 42;
            this.systemConfigurationRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsSystemConfigurationTabRadGroupBox
            // 
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Controls.Add(this.availableConfigurationsSystemConfigurationTabRadListControl);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Controls.Add(this.radButton3);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Location = new System.Drawing.Point(156, 434);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Name = "availableConfigurationsSystemConfigurationTabRadGroupBox";
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Size = new System.Drawing.Size(532, 192);
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.TabIndex = 24;
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsSystemConfigurationTabRadListControl
            // 
            this.availableConfigurationsSystemConfigurationTabRadListControl.AutoScroll = true;
            this.availableConfigurationsSystemConfigurationTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsSystemConfigurationTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsSystemConfigurationTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsSystemConfigurationTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsSystemConfigurationTabRadListControl.Name = "availableConfigurationsSystemConfigurationTabRadListControl";
            this.availableConfigurationsSystemConfigurationTabRadListControl.Size = new System.Drawing.Size(512, 162);
            this.availableConfigurationsSystemConfigurationTabRadListControl.TabIndex = 9;
            this.availableConfigurationsSystemConfigurationTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsSystemConfigurationTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsSystemConfigurationTabRadListControl_SelectedIndexChanged);
            // 
            // radButton3
            // 
            this.radButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Location = new System.Drawing.Point(0, 232);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 70);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton3.ThemeName = "Office2007Black";
            // 
            // systemConfigurationRadGridView
            // 
            this.systemConfigurationRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.systemConfigurationRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.systemConfigurationRadGridView.Location = new System.Drawing.Point(3, 3);
            this.systemConfigurationRadGridView.Name = "systemConfigurationRadGridView";
            this.systemConfigurationRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.systemConfigurationRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.systemConfigurationRadGridView.Size = new System.Drawing.Size(845, 355);
            this.systemConfigurationRadGridView.TabIndex = 0;
            this.systemConfigurationRadGridView.Text = "radGridView1";
            this.systemConfigurationRadGridView.ThemeName = "Office2007Black";
            // 
            // analogInputsRadPageViewPage
            // 
            this.analogInputsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.analogInputsRadPageViewPage.Controls.Add(this.templateConfigurationsAnalogInputsTabRadGroupBox);
            this.analogInputsRadPageViewPage.Controls.Add(this.databaseInteractionAnalogInputsTabRadGroupBox);
            this.analogInputsRadPageViewPage.Controls.Add(this.deviceInteractionAnalogInputsTabRadGroupBox);
            this.analogInputsRadPageViewPage.Controls.Add(this.analogInputsRadProgressBar);
            this.analogInputsRadPageViewPage.Controls.Add(this.availableConfigurationsAnalogInputsTabRadGroupBox);
            this.analogInputsRadPageViewPage.Controls.Add(this.chassisTemperatureRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.temperatureRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.humidityRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.voltageRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.currentRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.analogInRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.vibrationRadGridView);
            this.analogInputsRadPageViewPage.Controls.Add(this.controlParametersRadGroupBox);
            this.analogInputsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analogInputsRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.analogInputsRadPageViewPage.Name = "analogInputsRadPageViewPage";
            this.analogInputsRadPageViewPage.Size = new System.Drawing.Size(851, 629);
            this.analogInputsRadPageViewPage.Text = "Analog Inputs";
            // 
            // templateConfigurationsAnalogInputsTabRadGroupBox
            // 
            this.templateConfigurationsAnalogInputsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAnalogInputsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationAnalogInputsTabRadButton);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Controls.Add(this.templateConfigurationsAnalogInputsTabRadDropDownList);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Controls.Add(this.radButton8);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAnalogInputsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsAnalogInputsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsAnalogInputsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsAnalogInputsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsAnalogInputsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Location = new System.Drawing.Point(3, 364);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Name = "templateConfigurationsAnalogInputsTabRadGroupBox";
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsAnalogInputsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.TabIndex = 46;
            this.templateConfigurationsAnalogInputsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsAnalogInputsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationAnalogInputsTabRadButton
            // 
            this.copySelectedConfigurationAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationAnalogInputsTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationAnalogInputsTabRadButton.Name = "copySelectedConfigurationAnalogInputsTabRadButton";
            this.copySelectedConfigurationAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationAnalogInputsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationAnalogInputsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationAnalogInputsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsAnalogInputsTabRadDropDownList
            // 
            this.templateConfigurationsAnalogInputsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAnalogInputsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsAnalogInputsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAnalogInputsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsAnalogInputsTabRadDropDownList.Name = "templateConfigurationsAnalogInputsTabRadDropDownList";
            this.templateConfigurationsAnalogInputsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsAnalogInputsTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsAnalogInputsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsAnalogInputsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsAnalogInputsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsAnalogInputsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton8
            // 
            this.radButton8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton8.Location = new System.Drawing.Point(0, 232);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(130, 70);
            this.radButton8.TabIndex = 8;
            this.radButton8.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton8.ThemeName = "Office2007Black";
            // 
            // databaseInteractionAnalogInputsTabRadGroupBox
            // 
            this.databaseInteractionAnalogInputsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionAnalogInputsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionAnalogInputsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationAnalogInputsTabRadButton);
            this.databaseInteractionAnalogInputsTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationAnalogInputsTabRadButton);
            this.databaseInteractionAnalogInputsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseAnalogInputsTabRadButton);
            this.databaseInteractionAnalogInputsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionAnalogInputsTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionAnalogInputsTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionAnalogInputsTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionAnalogInputsTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionAnalogInputsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionAnalogInputsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionAnalogInputsTabRadGroupBox.Location = new System.Drawing.Point(2, 434);
            this.databaseInteractionAnalogInputsTabRadGroupBox.Name = "databaseInteractionAnalogInputsTabRadGroupBox";
            this.databaseInteractionAnalogInputsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionAnalogInputsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionAnalogInputsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionAnalogInputsTabRadGroupBox.TabIndex = 45;
            this.databaseInteractionAnalogInputsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionAnalogInputsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationAnalogInputsTabRadButton
            // 
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Name = "deleteSelectedConfigurationAnalogInputsTabRadButton";
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.TabIndex = 30;
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationAnalogInputsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationAnalogInputsTabRadButton
            // 
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Name = "saveWorkingConfigurationAnalogInputsTabRadButton";
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationAnalogInputsTabRadButton.TabIndex = 29;
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationAnalogInputsTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseAnalogInputsTabRadButton
            // 
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Name = "loadConfigurationFromDatabaseAnalogInputsTabRadButton";
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.TabIndex = 31;
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseAnalogInputsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // deviceInteractionAnalogInputsTabRadGroupBox
            // 
            this.deviceInteractionAnalogInputsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionAnalogInputsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionAnalogInputsTabRadGroupBox.Controls.Add(this.programDeviceAnalogInputsTabRadButton);
            this.deviceInteractionAnalogInputsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceAnalogInputsTabRadButton);
            this.deviceInteractionAnalogInputsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionAnalogInputsTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionAnalogInputsTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionAnalogInputsTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionAnalogInputsTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionAnalogInputsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionAnalogInputsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionAnalogInputsTabRadGroupBox.Location = new System.Drawing.Point(697, 434);
            this.deviceInteractionAnalogInputsTabRadGroupBox.Name = "deviceInteractionAnalogInputsTabRadGroupBox";
            this.deviceInteractionAnalogInputsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionAnalogInputsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionAnalogInputsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionAnalogInputsTabRadGroupBox.TabIndex = 45;
            this.deviceInteractionAnalogInputsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionAnalogInputsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceAnalogInputsTabRadButton
            // 
            this.programDeviceAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceAnalogInputsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceAnalogInputsTabRadButton.Name = "programDeviceAnalogInputsTabRadButton";
            this.programDeviceAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceAnalogInputsTabRadButton.TabIndex = 28;
            this.programDeviceAnalogInputsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceAnalogInputsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceAnalogInputsTabRadButton
            // 
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Name = "loadConfigurationFromDeviceAnalogInputsTabRadButton";
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.TabIndex = 32;
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceAnalogInputsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // analogInputsRadProgressBar
            // 
            this.analogInputsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.analogInputsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.analogInputsRadProgressBar.ImageIndex = -1;
            this.analogInputsRadProgressBar.ImageKey = "";
            this.analogInputsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.analogInputsRadProgressBar.Location = new System.Drawing.Point(567, 398);
            this.analogInputsRadProgressBar.Name = "analogInputsRadProgressBar";
            this.analogInputsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.analogInputsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.analogInputsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.analogInputsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.analogInputsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.analogInputsRadProgressBar.TabIndex = 42;
            this.analogInputsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsAnalogInputsTabRadGroupBox
            // 
            this.availableConfigurationsAnalogInputsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsAnalogInputsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Controls.Add(this.availableConfigurationsAnalogInputsTabRadListControl);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Controls.Add(this.radButton11);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsAnalogInputsTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsAnalogInputsTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsAnalogInputsTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsAnalogInputsTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsAnalogInputsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Location = new System.Drawing.Point(156, 435);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Name = "availableConfigurationsAnalogInputsTabRadGroupBox";
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsAnalogInputsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Size = new System.Drawing.Size(532, 192);
            this.availableConfigurationsAnalogInputsTabRadGroupBox.TabIndex = 30;
            this.availableConfigurationsAnalogInputsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAnalogInputsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsAnalogInputsTabRadListControl
            // 
            this.availableConfigurationsAnalogInputsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsAnalogInputsTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsAnalogInputsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsAnalogInputsTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsAnalogInputsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsAnalogInputsTabRadListControl.Name = "availableConfigurationsAnalogInputsTabRadListControl";
            this.availableConfigurationsAnalogInputsTabRadListControl.Size = new System.Drawing.Size(512, 162);
            this.availableConfigurationsAnalogInputsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsAnalogInputsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsAnalogInputsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsAnalogInputsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton11
            // 
            this.radButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton11.Location = new System.Drawing.Point(0, 232);
            this.radButton11.Name = "radButton11";
            this.radButton11.Size = new System.Drawing.Size(130, 70);
            this.radButton11.TabIndex = 8;
            this.radButton11.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton11.ThemeName = "Office2007Black";
            // 
            // chassisTemperatureRadGridView
            // 
            this.chassisTemperatureRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chassisTemperatureRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chassisTemperatureRadGridView.Location = new System.Drawing.Point(534, 247);
            this.chassisTemperatureRadGridView.Name = "chassisTemperatureRadGridView";
            this.chassisTemperatureRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.chassisTemperatureRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.chassisTemperatureRadGridView.Size = new System.Drawing.Size(240, 111);
            this.chassisTemperatureRadGridView.TabIndex = 3;
            this.chassisTemperatureRadGridView.Text = "radGridView1";
            this.chassisTemperatureRadGridView.ThemeName = "Office2007Black";
            // 
            // temperatureRadGridView
            // 
            this.temperatureRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temperatureRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureRadGridView.Location = new System.Drawing.Point(534, 134);
            this.temperatureRadGridView.Name = "temperatureRadGridView";
            this.temperatureRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.temperatureRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.temperatureRadGridView.Size = new System.Drawing.Size(240, 110);
            this.temperatureRadGridView.TabIndex = 3;
            this.temperatureRadGridView.Text = "radGridView1";
            this.temperatureRadGridView.ThemeName = "Office2007Black";
            // 
            // humidityRadGridView
            // 
            this.humidityRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.humidityRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityRadGridView.Location = new System.Drawing.Point(368, 134);
            this.humidityRadGridView.Name = "humidityRadGridView";
            this.humidityRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.humidityRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.humidityRadGridView.Size = new System.Drawing.Size(160, 110);
            this.humidityRadGridView.TabIndex = 3;
            this.humidityRadGridView.Text = "radGridView1";
            this.humidityRadGridView.ThemeName = "Office2007Black";
            // 
            // voltageRadGridView
            // 
            this.voltageRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.voltageRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltageRadGridView.Location = new System.Drawing.Point(166, 134);
            this.voltageRadGridView.Name = "voltageRadGridView";
            this.voltageRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.voltageRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.voltageRadGridView.Size = new System.Drawing.Size(196, 110);
            this.voltageRadGridView.TabIndex = 3;
            this.voltageRadGridView.Text = "radGridView1";
            this.voltageRadGridView.ThemeName = "Office2007Black";
            // 
            // currentRadGridView
            // 
            this.currentRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.currentRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentRadGridView.Location = new System.Drawing.Point(534, 26);
            this.currentRadGridView.Name = "currentRadGridView";
            this.currentRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.currentRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.currentRadGridView.Size = new System.Drawing.Size(240, 105);
            this.currentRadGridView.TabIndex = 3;
            this.currentRadGridView.Text = "radGridView1";
            this.currentRadGridView.ThemeName = "Office2007Black";
            // 
            // analogInRadGridView
            // 
            this.analogInRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.analogInRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analogInRadGridView.Location = new System.Drawing.Point(368, 26);
            this.analogInRadGridView.Name = "analogInRadGridView";
            this.analogInRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.analogInRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.analogInRadGridView.Size = new System.Drawing.Size(160, 105);
            this.analogInRadGridView.TabIndex = 3;
            this.analogInRadGridView.Text = "radGridView1";
            this.analogInRadGridView.ThemeName = "Office2007Black";
            // 
            // vibrationRadGridView
            // 
            this.vibrationRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vibrationRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vibrationRadGridView.Location = new System.Drawing.Point(166, 26);
            this.vibrationRadGridView.Name = "vibrationRadGridView";
            this.vibrationRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.vibrationRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.vibrationRadGridView.Size = new System.Drawing.Size(196, 105);
            this.vibrationRadGridView.TabIndex = 2;
            this.vibrationRadGridView.Text = "radGridView1";
            this.vibrationRadGridView.ThemeName = "Office2007Black";
            this.vibrationRadGridView.ChildViewExpanded += new Telerik.WinControls.UI.ChildViewExpandedEventHandler(this.vibrationRadGridView_ChildViewExpanded);
            // 
            // controlParametersRadGroupBox
            // 
            this.controlParametersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.controlParametersRadGroupBox.Controls.Add(this.chassisTemperatureRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.currentRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.analogInRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.temperatureRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.humidityRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.voltageRadRadioButton);
            this.controlParametersRadGroupBox.Controls.Add(this.vibrationRadRadioButton);
            this.controlParametersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.controlParametersRadGroupBox.FooterImageIndex = -1;
            this.controlParametersRadGroupBox.FooterImageKey = "";
            this.controlParametersRadGroupBox.HeaderImageIndex = -1;
            this.controlParametersRadGroupBox.HeaderImageKey = "";
            this.controlParametersRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.controlParametersRadGroupBox.HeaderText = "Control Parameters";
            this.controlParametersRadGroupBox.Location = new System.Drawing.Point(3, 3);
            this.controlParametersRadGroupBox.Name = "controlParametersRadGroupBox";
            this.controlParametersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.controlParametersRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.controlParametersRadGroupBox.Size = new System.Drawing.Size(125, 238);
            this.controlParametersRadGroupBox.TabIndex = 1;
            this.controlParametersRadGroupBox.Text = "Control Parameters";
            this.controlParametersRadGroupBox.ThemeName = "Office2007Black";
            // 
            // chassisTemperatureRadRadioButton
            // 
            this.chassisTemperatureRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chassisTemperatureRadRadioButton.Location = new System.Drawing.Point(13, 179);
            this.chassisTemperatureRadRadioButton.Name = "chassisTemperatureRadRadioButton";
            this.chassisTemperatureRadRadioButton.Size = new System.Drawing.Size(99, 38);
            this.chassisTemperatureRadRadioButton.TabIndex = 6;
            this.chassisTemperatureRadRadioButton.Text = "<html>Chassis<br>Temperature</html>";
            this.chassisTemperatureRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chassisTemperatureRadRadioButton_ToggleStateChanged);
            // 
            // currentRadRadioButton
            // 
            this.currentRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentRadRadioButton.Location = new System.Drawing.Point(13, 83);
            this.currentRadRadioButton.Name = "currentRadRadioButton";
            this.currentRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.currentRadRadioButton.TabIndex = 5;
            this.currentRadRadioButton.Text = "Current";
            this.currentRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.currentRadRadioButton_ToggleStateChanged);
            // 
            // analogInRadRadioButton
            // 
            this.analogInRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analogInRadRadioButton.Location = new System.Drawing.Point(13, 59);
            this.analogInRadRadioButton.Name = "analogInRadRadioButton";
            this.analogInRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.analogInRadRadioButton.TabIndex = 4;
            this.analogInRadRadioButton.Text = "Analog In";
            this.analogInRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.analogInRadRadioButton_ToggleStateChanged);
            // 
            // temperatureRadRadioButton
            // 
            this.temperatureRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temperatureRadRadioButton.Location = new System.Drawing.Point(13, 155);
            this.temperatureRadRadioButton.Name = "temperatureRadRadioButton";
            this.temperatureRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.temperatureRadRadioButton.TabIndex = 3;
            this.temperatureRadRadioButton.Text = "Temperature";
            this.temperatureRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.temperatureRadRadioButton_ToggleStateChanged);
            // 
            // humidityRadRadioButton
            // 
            this.humidityRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.humidityRadRadioButton.Location = new System.Drawing.Point(13, 131);
            this.humidityRadRadioButton.Name = "humidityRadRadioButton";
            this.humidityRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.humidityRadRadioButton.TabIndex = 2;
            this.humidityRadRadioButton.Text = "Humidity";
            this.humidityRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.humidityRadRadioButton_ToggleStateChanged);
            // 
            // voltageRadRadioButton
            // 
            this.voltageRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltageRadRadioButton.Location = new System.Drawing.Point(13, 107);
            this.voltageRadRadioButton.Name = "voltageRadRadioButton";
            this.voltageRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.voltageRadRadioButton.TabIndex = 1;
            this.voltageRadRadioButton.Text = "Voltage";
            this.voltageRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.voltageRadRadioButton_ToggleStateChanged);
            // 
            // vibrationRadRadioButton
            // 
            this.vibrationRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.vibrationRadRadioButton.Location = new System.Drawing.Point(13, 35);
            this.vibrationRadRadioButton.Name = "vibrationRadRadioButton";
            this.vibrationRadRadioButton.Size = new System.Drawing.Size(99, 18);
            this.vibrationRadRadioButton.TabIndex = 0;
            this.vibrationRadRadioButton.TabStop = true;
            this.vibrationRadRadioButton.Text = "Vibration";
            this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.vibrationRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.vibrationRadRadioButton_ToggleStateChanged);
            // 
            // calibrationRadPageViewPage
            // 
            this.calibrationRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.calibrationRadPageViewPage.Controls.Add(this.templateConfigurationsCalibrationTabRadGroupBox);
            this.calibrationRadPageViewPage.Controls.Add(this.deviceInteractionCalibrationTabRadGroupBox);
            this.calibrationRadPageViewPage.Controls.Add(this.databaseInteractionCalibrationTabRadGroupBox);
            this.calibrationRadPageViewPage.Controls.Add(this.calibrationRadGridView);
            this.calibrationRadPageViewPage.Controls.Add(this.calibrationRadProgressBar);
            this.calibrationRadPageViewPage.Controls.Add(this.availableConfigurationsCalibrationTabRadGroupBox);
            this.calibrationRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.calibrationRadPageViewPage.Name = "calibrationRadPageViewPage";
            this.calibrationRadPageViewPage.Size = new System.Drawing.Size(851, 629);
            this.calibrationRadPageViewPage.Text = "Calibration";
            // 
            // templateConfigurationsCalibrationTabRadGroupBox
            // 
            this.templateConfigurationsCalibrationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsCalibrationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCalibrationTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsCalibrationTabRadGroupBox.Controls.Add(this.copySelectedConfigurationCalibrationTabRadButton);
            this.templateConfigurationsCalibrationTabRadGroupBox.Controls.Add(this.templateConfigurationsCalibrationTabRadDropDownList);
            this.templateConfigurationsCalibrationTabRadGroupBox.Controls.Add(this.radButton10);
            this.templateConfigurationsCalibrationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCalibrationTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsCalibrationTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsCalibrationTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsCalibrationTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsCalibrationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsCalibrationTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsCalibrationTabRadGroupBox.Location = new System.Drawing.Point(3, 364);
            this.templateConfigurationsCalibrationTabRadGroupBox.Name = "templateConfigurationsCalibrationTabRadGroupBox";
            this.templateConfigurationsCalibrationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsCalibrationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsCalibrationTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsCalibrationTabRadGroupBox.TabIndex = 55;
            this.templateConfigurationsCalibrationTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsCalibrationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationCalibrationTabRadButton
            // 
            this.copySelectedConfigurationCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationCalibrationTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationCalibrationTabRadButton.Name = "copySelectedConfigurationCalibrationTabRadButton";
            this.copySelectedConfigurationCalibrationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationCalibrationTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationCalibrationTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationCalibrationTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsCalibrationTabRadDropDownList
            // 
            this.templateConfigurationsCalibrationTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCalibrationTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsCalibrationTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCalibrationTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsCalibrationTabRadDropDownList.Name = "templateConfigurationsCalibrationTabRadDropDownList";
            this.templateConfigurationsCalibrationTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsCalibrationTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsCalibrationTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsCalibrationTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsCalibrationTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsCalibrationTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton10
            // 
            this.radButton10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton10.Location = new System.Drawing.Point(0, 232);
            this.radButton10.Name = "radButton10";
            this.radButton10.Size = new System.Drawing.Size(130, 70);
            this.radButton10.TabIndex = 8;
            this.radButton10.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton10.ThemeName = "Office2007Black";
            // 
            // deviceInteractionCalibrationTabRadGroupBox
            // 
            this.deviceInteractionCalibrationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionCalibrationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionCalibrationTabRadGroupBox.Controls.Add(this.programDeviceCalibrationTabRadButton);
            this.deviceInteractionCalibrationTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceCalibrationTabRadButton);
            this.deviceInteractionCalibrationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionCalibrationTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionCalibrationTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionCalibrationTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionCalibrationTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionCalibrationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionCalibrationTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionCalibrationTabRadGroupBox.Location = new System.Drawing.Point(697, 434);
            this.deviceInteractionCalibrationTabRadGroupBox.Name = "deviceInteractionCalibrationTabRadGroupBox";
            this.deviceInteractionCalibrationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionCalibrationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionCalibrationTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionCalibrationTabRadGroupBox.TabIndex = 54;
            this.deviceInteractionCalibrationTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionCalibrationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceCalibrationTabRadButton
            // 
            this.programDeviceCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceCalibrationTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceCalibrationTabRadButton.Name = "programDeviceCalibrationTabRadButton";
            this.programDeviceCalibrationTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceCalibrationTabRadButton.TabIndex = 43;
            this.programDeviceCalibrationTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceCalibrationTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceCalibrationTabRadButton
            // 
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Name = "loadConfigurationFromDeviceCalibrationTabRadButton";
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceCalibrationTabRadButton.TabIndex = 47;
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceCalibrationTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // databaseInteractionCalibrationTabRadGroupBox
            // 
            this.databaseInteractionCalibrationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionCalibrationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionCalibrationTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationCalibrationTabRadButton);
            this.databaseInteractionCalibrationTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationCalibrationTabRadButton);
            this.databaseInteractionCalibrationTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseCalibrationTabRadButton);
            this.databaseInteractionCalibrationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionCalibrationTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionCalibrationTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionCalibrationTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionCalibrationTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionCalibrationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionCalibrationTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionCalibrationTabRadGroupBox.Location = new System.Drawing.Point(2, 434);
            this.databaseInteractionCalibrationTabRadGroupBox.Name = "databaseInteractionCalibrationTabRadGroupBox";
            this.databaseInteractionCalibrationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionCalibrationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionCalibrationTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionCalibrationTabRadGroupBox.TabIndex = 53;
            this.databaseInteractionCalibrationTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionCalibrationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationCalibrationTabRadButton
            // 
            this.deleteSelectedConfigurationCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationCalibrationTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationCalibrationTabRadButton.Name = "deleteSelectedConfigurationCalibrationTabRadButton";
            this.deleteSelectedConfigurationCalibrationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationCalibrationTabRadButton.TabIndex = 47;
            this.deleteSelectedConfigurationCalibrationTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationCalibrationTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationCalibrationTabRadButton
            // 
            this.saveWorkingConfigurationCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationCalibrationTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationCalibrationTabRadButton.Name = "saveWorkingConfigurationCalibrationTabRadButton";
            this.saveWorkingConfigurationCalibrationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationCalibrationTabRadButton.TabIndex = 44;
            this.saveWorkingConfigurationCalibrationTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationCalibrationTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseCalibrationTabRadButton
            // 
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Name = "loadConfigurationFromDatabaseCalibrationTabRadButton";
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.TabIndex = 46;
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseCalibrationTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // calibrationRadGridView
            // 
            this.calibrationRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.calibrationRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrationRadGridView.Location = new System.Drawing.Point(3, 3);
            this.calibrationRadGridView.Name = "calibrationRadGridView";
            this.calibrationRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.calibrationRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.calibrationRadGridView.Size = new System.Drawing.Size(845, 355);
            this.calibrationRadGridView.TabIndex = 52;
            this.calibrationRadGridView.Text = "radGridView1";
            this.calibrationRadGridView.ThemeName = "Office2007Black";
            // 
            // calibrationRadProgressBar
            // 
            this.calibrationRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.calibrationRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.calibrationRadProgressBar.ImageIndex = -1;
            this.calibrationRadProgressBar.ImageKey = "";
            this.calibrationRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.calibrationRadProgressBar.Location = new System.Drawing.Point(567, 398);
            this.calibrationRadProgressBar.Name = "calibrationRadProgressBar";
            this.calibrationRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.calibrationRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.calibrationRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.calibrationRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.calibrationRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.calibrationRadProgressBar.TabIndex = 51;
            this.calibrationRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsCalibrationTabRadGroupBox
            // 
            this.availableConfigurationsCalibrationTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsCalibrationTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsCalibrationTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsCalibrationTabRadGroupBox.Controls.Add(this.availableConfigurationsCalibrationTabRadListControl);
            this.availableConfigurationsCalibrationTabRadGroupBox.Controls.Add(this.radButton15);
            this.availableConfigurationsCalibrationTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsCalibrationTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsCalibrationTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsCalibrationTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsCalibrationTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsCalibrationTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsCalibrationTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCalibrationTabRadGroupBox.Location = new System.Drawing.Point(156, 435);
            this.availableConfigurationsCalibrationTabRadGroupBox.Name = "availableConfigurationsCalibrationTabRadGroupBox";
            this.availableConfigurationsCalibrationTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsCalibrationTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsCalibrationTabRadGroupBox.Size = new System.Drawing.Size(532, 192);
            this.availableConfigurationsCalibrationTabRadGroupBox.TabIndex = 45;
            this.availableConfigurationsCalibrationTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCalibrationTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsCalibrationTabRadListControl
            // 
            this.availableConfigurationsCalibrationTabRadListControl.AutoScroll = true;
            this.availableConfigurationsCalibrationTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsCalibrationTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsCalibrationTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsCalibrationTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsCalibrationTabRadListControl.Name = "availableConfigurationsCalibrationTabRadListControl";
            this.availableConfigurationsCalibrationTabRadListControl.Size = new System.Drawing.Size(512, 162);
            this.availableConfigurationsCalibrationTabRadListControl.TabIndex = 9;
            this.availableConfigurationsCalibrationTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsCalibrationTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsCalibrationTabRadListControl_SelectedIndexChanged);
            // 
            // radButton15
            // 
            this.radButton15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton15.Location = new System.Drawing.Point(0, 232);
            this.radButton15.Name = "radButton15";
            this.radButton15.Size = new System.Drawing.Size(130, 70);
            this.radButton15.TabIndex = 8;
            this.radButton15.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton15.ThemeName = "Office2007Black";
            // 
            // dataTransferRadPageViewPage
            // 
            this.dataTransferRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dataTransferRadPageViewPage.Controls.Add(this.templateConfigurationsDataTransferTabRadGroupBox);
            this.dataTransferRadPageViewPage.Controls.Add(this.databaseInteractionDataTransferTabRadGroupBox);
            this.dataTransferRadPageViewPage.Controls.Add(this.deviceInteractionDataTransferTabRadGroupBox);
            this.dataTransferRadPageViewPage.Controls.Add(this.dataTransferRadProgressBar);
            this.dataTransferRadPageViewPage.Controls.Add(this.availableConfigurationsDataTransferTabRadGroupBox);
            this.dataTransferRadPageViewPage.Controls.Add(this.dataTransferRadGridView);
            this.dataTransferRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTransferRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.dataTransferRadPageViewPage.Name = "dataTransferRadPageViewPage";
            this.dataTransferRadPageViewPage.Size = new System.Drawing.Size(851, 629);
            this.dataTransferRadPageViewPage.Text = "Data Transfer";
            // 
            // templateConfigurationsDataTransferTabRadGroupBox
            // 
            this.templateConfigurationsDataTransferTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsDataTransferTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsDataTransferTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsDataTransferTabRadGroupBox.Controls.Add(this.copySelectedConfigurationDataTransferTabRadButton);
            this.templateConfigurationsDataTransferTabRadGroupBox.Controls.Add(this.templateConfigurationsDataTransferTabRadDropDownList);
            this.templateConfigurationsDataTransferTabRadGroupBox.Controls.Add(this.radButton13);
            this.templateConfigurationsDataTransferTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsDataTransferTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsDataTransferTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsDataTransferTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsDataTransferTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsDataTransferTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsDataTransferTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsDataTransferTabRadGroupBox.Location = new System.Drawing.Point(3, 364);
            this.templateConfigurationsDataTransferTabRadGroupBox.Name = "templateConfigurationsDataTransferTabRadGroupBox";
            this.templateConfigurationsDataTransferTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsDataTransferTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsDataTransferTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsDataTransferTabRadGroupBox.TabIndex = 48;
            this.templateConfigurationsDataTransferTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsDataTransferTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationDataTransferTabRadButton
            // 
            this.copySelectedConfigurationDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationDataTransferTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationDataTransferTabRadButton.Name = "copySelectedConfigurationDataTransferTabRadButton";
            this.copySelectedConfigurationDataTransferTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationDataTransferTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationDataTransferTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationDataTransferTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsDataTransferTabRadDropDownList
            // 
            this.templateConfigurationsDataTransferTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsDataTransferTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsDataTransferTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsDataTransferTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsDataTransferTabRadDropDownList.Name = "templateConfigurationsDataTransferTabRadDropDownList";
            this.templateConfigurationsDataTransferTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsDataTransferTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsDataTransferTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsDataTransferTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsDataTransferTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsDataTransferTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton13
            // 
            this.radButton13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton13.Location = new System.Drawing.Point(0, 232);
            this.radButton13.Name = "radButton13";
            this.radButton13.Size = new System.Drawing.Size(130, 70);
            this.radButton13.TabIndex = 8;
            this.radButton13.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton13.ThemeName = "Office2007Black";
            // 
            // databaseInteractionDataTransferTabRadGroupBox
            // 
            this.databaseInteractionDataTransferTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionDataTransferTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionDataTransferTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationDataTransferTabRadButton);
            this.databaseInteractionDataTransferTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationDataTransferTabRadButton);
            this.databaseInteractionDataTransferTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseDataTransferTabRadButton);
            this.databaseInteractionDataTransferTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionDataTransferTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionDataTransferTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionDataTransferTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionDataTransferTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionDataTransferTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionDataTransferTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionDataTransferTabRadGroupBox.Location = new System.Drawing.Point(2, 434);
            this.databaseInteractionDataTransferTabRadGroupBox.Name = "databaseInteractionDataTransferTabRadGroupBox";
            this.databaseInteractionDataTransferTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionDataTransferTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionDataTransferTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionDataTransferTabRadGroupBox.TabIndex = 47;
            this.databaseInteractionDataTransferTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionDataTransferTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationDataTransferTabRadButton
            // 
            this.deleteSelectedConfigurationDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationDataTransferTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationDataTransferTabRadButton.Name = "deleteSelectedConfigurationDataTransferTabRadButton";
            this.deleteSelectedConfigurationDataTransferTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationDataTransferTabRadButton.TabIndex = 32;
            this.deleteSelectedConfigurationDataTransferTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationDataTransferTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationDataTransferTabRadButton
            // 
            this.saveWorkingConfigurationDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationDataTransferTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationDataTransferTabRadButton.Name = "saveWorkingConfigurationDataTransferTabRadButton";
            this.saveWorkingConfigurationDataTransferTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationDataTransferTabRadButton.TabIndex = 29;
            this.saveWorkingConfigurationDataTransferTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationDataTransferTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseDataTransferTabRadButton
            // 
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Name = "loadConfigurationFromDatabaseDataTransferTabRadButton";
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.TabIndex = 31;
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseDataTransferTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // deviceInteractionDataTransferTabRadGroupBox
            // 
            this.deviceInteractionDataTransferTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionDataTransferTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionDataTransferTabRadGroupBox.Controls.Add(this.programDeviceDataTransferTabRadButton);
            this.deviceInteractionDataTransferTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceDataTransferTabRadButton);
            this.deviceInteractionDataTransferTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionDataTransferTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionDataTransferTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionDataTransferTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionDataTransferTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionDataTransferTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionDataTransferTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionDataTransferTabRadGroupBox.Location = new System.Drawing.Point(697, 434);
            this.deviceInteractionDataTransferTabRadGroupBox.Name = "deviceInteractionDataTransferTabRadGroupBox";
            this.deviceInteractionDataTransferTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionDataTransferTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionDataTransferTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionDataTransferTabRadGroupBox.TabIndex = 46;
            this.deviceInteractionDataTransferTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionDataTransferTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceDataTransferTabRadButton
            // 
            this.programDeviceDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceDataTransferTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceDataTransferTabRadButton.Name = "programDeviceDataTransferTabRadButton";
            this.programDeviceDataTransferTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceDataTransferTabRadButton.TabIndex = 28;
            this.programDeviceDataTransferTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceDataTransferTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceDataTransferTabRadButton
            // 
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Name = "loadConfigurationFromDeviceDataTransferTabRadButton";
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceDataTransferTabRadButton.TabIndex = 32;
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceDataTransferTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceDataTransferTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // dataTransferRadProgressBar
            // 
            this.dataTransferRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dataTransferRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataTransferRadProgressBar.ImageIndex = -1;
            this.dataTransferRadProgressBar.ImageKey = "";
            this.dataTransferRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.dataTransferRadProgressBar.Location = new System.Drawing.Point(567, 398);
            this.dataTransferRadProgressBar.Name = "dataTransferRadProgressBar";
            this.dataTransferRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.dataTransferRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.dataTransferRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.dataTransferRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.dataTransferRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.dataTransferRadProgressBar.TabIndex = 42;
            this.dataTransferRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsDataTransferTabRadGroupBox
            // 
            this.availableConfigurationsDataTransferTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsDataTransferTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsDataTransferTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsDataTransferTabRadGroupBox.Controls.Add(this.availableConfigurationsDataTransferTabRadListControl);
            this.availableConfigurationsDataTransferTabRadGroupBox.Controls.Add(this.radButton17);
            this.availableConfigurationsDataTransferTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsDataTransferTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsDataTransferTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsDataTransferTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsDataTransferTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsDataTransferTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsDataTransferTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsDataTransferTabRadGroupBox.Location = new System.Drawing.Point(156, 434);
            this.availableConfigurationsDataTransferTabRadGroupBox.Name = "availableConfigurationsDataTransferTabRadGroupBox";
            this.availableConfigurationsDataTransferTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsDataTransferTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsDataTransferTabRadGroupBox.Size = new System.Drawing.Size(532, 192);
            this.availableConfigurationsDataTransferTabRadGroupBox.TabIndex = 30;
            this.availableConfigurationsDataTransferTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsDataTransferTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsDataTransferTabRadListControl
            // 
            this.availableConfigurationsDataTransferTabRadListControl.AutoScroll = true;
            this.availableConfigurationsDataTransferTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsDataTransferTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsDataTransferTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsDataTransferTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsDataTransferTabRadListControl.Name = "availableConfigurationsDataTransferTabRadListControl";
            this.availableConfigurationsDataTransferTabRadListControl.Size = new System.Drawing.Size(512, 162);
            this.availableConfigurationsDataTransferTabRadListControl.TabIndex = 9;
            this.availableConfigurationsDataTransferTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsDataTransferTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsDataTransferTabRadListControl_SelectedIndexChanged);
            // 
            // radButton17
            // 
            this.radButton17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton17.Location = new System.Drawing.Point(0, 232);
            this.radButton17.Name = "radButton17";
            this.radButton17.Size = new System.Drawing.Size(130, 70);
            this.radButton17.TabIndex = 8;
            this.radButton17.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton17.ThemeName = "Office2007Black";
            // 
            // dataTransferRadGridView
            // 
            this.dataTransferRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataTransferRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTransferRadGridView.Location = new System.Drawing.Point(3, 3);
            this.dataTransferRadGridView.Name = "dataTransferRadGridView";
            this.dataTransferRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.dataTransferRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.dataTransferRadGridView.Size = new System.Drawing.Size(845, 355);
            this.dataTransferRadGridView.TabIndex = 1;
            this.dataTransferRadGridView.Text = "radGridView1";
            this.dataTransferRadGridView.ThemeName = "Office2007Black";
            this.dataTransferRadGridView.CellEditorInitialized += new Telerik.WinControls.UI.GridViewCellEventHandler(this.dataTransferRadGridView_CellEditorInitialized);
            // 
            // Main_MonitorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(872, 677);
            this.Controls.Add(this.configurationRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(880, 709);
            this.Name = "Main_MonitorConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Main Monitor Configuration";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_MonitorConfiguration_FormClosing);
            this.Load += new System.EventHandler(this.Main_MonitorConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).EndInit();
            this.configurationRadPageView.ResumeLayout(false);
            this.generalSettingsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionGeneralSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionGeneralSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsGeneralSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsGeneralSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.otherOptionsRadGroupBox)).EndInit();
            this.otherOptionsRadGroupBox.ResumeLayout(false);
            this.otherOptionsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enableCurrentSignatureAnalysisRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allowDirectAccessRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeSaveRadGroupBox)).EndInit();
            this.modeSaveRadGroupBox.ResumeLayout(false);
            this.modeSaveRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.useDiagnosticResultsRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modeSaveIntervalRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadGroupBox)).EndInit();
            this.frequencyRadGroupBox.ResumeLayout(false);
            this.frequencyRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hertzRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayRadGroupBox)).EndInit();
            this.relayRadGroupBox.ResumeLayout(false);
            this.relayRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsWarningRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalSettingsAlarmRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayWarningRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relayAlarmRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.versionRadGroupBox)).EndInit();
            this.versionRadGroupBox.ResumeLayout(false);
            this.versionRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connectionParametersRadGroupBox)).EndInit();
            this.connectionParametersRadGroupBox.ResumeLayout(false);
            this.connectionParametersRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetRadGroupBox)).EndInit();
            this.ethernetRadGroupBox.ResumeLayout(false);
            this.ethernetRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetProtocolRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetProtocolRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetBaudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ethernetBaudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialConnectionRadGroupBox)).EndInit();
            this.serialConnectionRadGroupBox.ResumeLayout(false);
            this.serialConnectionRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialBaudRateRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serialBaudRateRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modbusAddressRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boardNameRadGroupBox)).EndInit();
            this.boardNameRadGroupBox.ResumeLayout(false);
            this.boardNameRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadGroupBox)).EndInit();
            this.monitoringRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).EndInit();
            this.systemConfigurationRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionSystemConfigurationTabRadGroupBox)).EndInit();
            this.databaseInteractionSystemConfigurationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSystemConfigurationTabRadGroupBox)).EndInit();
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsSystemConfigurationTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsSystemConfigurationTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionSystemConfigurationTabRadGroupBox)).EndInit();
            this.deviceInteractionSystemConfigurationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceSystemConfigurationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsSystemConfigurationTabRadGroupBox)).EndInit();
            this.availableConfigurationsSystemConfigurationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsSystemConfigurationTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.systemConfigurationRadGridView)).EndInit();
            this.analogInputsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAnalogInputsTabRadGroupBox)).EndInit();
            this.templateConfigurationsAnalogInputsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsAnalogInputsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAnalogInputsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAnalogInputsTabRadGroupBox)).EndInit();
            this.databaseInteractionAnalogInputsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAnalogInputsTabRadGroupBox)).EndInit();
            this.deviceInteractionAnalogInputsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAnalogInputsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInputsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAnalogInputsTabRadGroupBox)).EndInit();
            this.availableConfigurationsAnalogInputsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAnalogInputsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.controlParametersRadGroupBox)).EndInit();
            this.controlParametersRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chassisTemperatureRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analogInRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltageRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibrationRadRadioButton)).EndInit();
            this.calibrationRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalibrationTabRadGroupBox)).EndInit();
            this.templateConfigurationsCalibrationTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsCalibrationTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalibrationTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCalibrationTabRadGroupBox)).EndInit();
            this.deviceInteractionCalibrationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCalibrationTabRadGroupBox)).EndInit();
            this.databaseInteractionCalibrationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCalibrationTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calibrationRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalibrationTabRadGroupBox)).EndInit();
            this.availableConfigurationsCalibrationTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalibrationTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).EndInit();
            this.dataTransferRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsDataTransferTabRadGroupBox)).EndInit();
            this.templateConfigurationsDataTransferTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsDataTransferTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsDataTransferTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionDataTransferTabRadGroupBox)).EndInit();
            this.databaseInteractionDataTransferTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionDataTransferTabRadGroupBox)).EndInit();
            this.deviceInteractionDataTransferTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceDataTransferTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsDataTransferTabRadGroupBox)).EndInit();
            this.availableConfigurationsDataTransferTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsDataTransferTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView configurationRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage generalSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage systemConfigurationRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage analogInputsRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage dataTransferRadPageViewPage;
        private Telerik.WinControls.UI.RadGroupBox controlParametersRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton chassisTemperatureRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton currentRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton analogInRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton temperatureRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton humidityRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton voltageRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton vibrationRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox connectionParametersRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox boardNameRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox objectNameRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox monitoringRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton disableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadGroupBox serialConnectionRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList serialBaudRateRadDropDownList;
        private Telerik.WinControls.UI.RadLabel serialBaudRateRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor modbusAddressRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel modbusAddressRadLabel;
        private Telerik.WinControls.UI.RadGroupBox modeSaveRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox frequencyRadGroupBox;
        private Telerik.WinControls.UI.RadLabel hertzRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor frequencyRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox relayRadGroupBox;
        private Telerik.WinControls.UI.RadLabel generalSettingsWarningRadLabel;
        private Telerik.WinControls.UI.RadLabel generalSettingsAlarmRadLabel;
        private Telerik.WinControls.UI.RadDropDownList relayWarningRadDropDownList;
        private Telerik.WinControls.UI.RadDropDownList relayAlarmRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox versionRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox firmwareVersionRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox ethernetRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox ipAddressRadTextBox;
        private Telerik.WinControls.UI.RadLabel ipAddressRadLabel;
        private Telerik.WinControls.UI.RadDropDownList ethernetBaudRateRadDropDownList;
        private Telerik.WinControls.UI.RadLabel ethernetBaudRateRadLabel;
        private Telerik.WinControls.UI.RadGroupBox otherOptionsRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox enableCurrentSignatureAnalysisRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox allowDirectAccessRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox useDiagnosticResultsRadCheckBox;
        private Telerik.WinControls.UI.RadLabel minutesRadLabel;
        private Telerik.WinControls.UI.RadLabel stepRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor modeSaveIntervalRadSpinEditor;
        private Telerik.WinControls.UI.RadGridView chassisTemperatureRadGridView;
        private Telerik.WinControls.UI.RadGridView temperatureRadGridView;
        private Telerik.WinControls.UI.RadGridView humidityRadGridView;
        private Telerik.WinControls.UI.RadGridView voltageRadGridView;
        private Telerik.WinControls.UI.RadGridView currentRadGridView;
        private Telerik.WinControls.UI.RadGridView analogInRadGridView;
        private Telerik.WinControls.UI.RadGridView vibrationRadGridView;
        private Telerik.WinControls.UI.RadGridView systemConfigurationRadGridView;
        private Telerik.WinControls.UI.RadGridView dataTransferRadGridView;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsGeneralSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceSystemConfigurationTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseSystemConfigurationTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsSystemConfigurationTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsSystemConfigurationTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationSystemConfigurationTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceSystemConfigurationTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceAnalogInputsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseAnalogInputsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsAnalogInputsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsAnalogInputsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton11;
        private Telerik.WinControls.UI.RadButton programDeviceAnalogInputsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceDataTransferTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseDataTransferTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsDataTransferTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsDataTransferTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton17;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationDataTransferTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceDataTransferTabRadButton;
        internal Telerik.WinControls.UI.RadButton saveWorkingConfigurationAnalogInputsTabRadButton;
        private Telerik.WinControls.UI.RadProgressBar generalSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar systemConfigurationRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar analogInputsRadProgressBar;
        private Telerik.WinControls.UI.RadProgressBar dataTransferRadProgressBar;
        private Telerik.WinControls.UI.RadPageViewPage calibrationRadPageViewPage;
        private Telerik.WinControls.UI.RadProgressBar calibrationRadProgressBar;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceCalibrationTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseCalibrationTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsCalibrationTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsCalibrationTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton15;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationCalibrationTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceCalibrationTabRadButton;
        private Telerik.WinControls.UI.RadGridView calibrationRadGridView;
        private Telerik.WinControls.UI.RadLabel ethernetProtocolRadLabel;
        private Telerik.WinControls.UI.RadDropDownList ethernetProtocolRadDropDownList;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionSystemConfigurationTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionSystemConfigurationTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionAnalogInputsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionAnalogInputsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionCalibrationTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionCalibrationTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionDataTransferTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionDataTransferTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationSystemConfigurationTabRadButton;
        internal Telerik.WinControls.UI.RadButton deleteSelectedConfigurationAnalogInputsTabRadButton;
        internal Telerik.WinControls.UI.RadButton deleteSelectedConfigurationCalibrationTabRadButton;
        internal Telerik.WinControls.UI.RadButton deleteSelectedConfigurationDataTransferTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsGeneralSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsSystemConfigurationTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsSystemConfigurationTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationSystemConfigurationTabRadButton;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsAnalogInputsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationAnalogInputsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsAnalogInputsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton8;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsCalibrationTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationCalibrationTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsCalibrationTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton10;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsDataTransferTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationDataTransferTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsDataTransferTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton13;
    }
}

