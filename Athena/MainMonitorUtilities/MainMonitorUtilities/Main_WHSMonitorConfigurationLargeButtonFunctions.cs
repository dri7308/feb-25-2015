﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using FormatConversion;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using MonitorUtilities;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;

namespace MainMonitorUtilities
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
       // private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
       // private static string noworkingConfigurationDefinedText = "There is no working configuration defined.";
       // private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
       // private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
       // private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        private static string deleteAsQuestionText = "Delete?";
        private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
      //  private static string cannotDeleteTemplateConfigurationWarningText = "You are not allowed to delete a template configuration from the database.";      
        private static string noConfigurationSelectedText = "No configuration selected";
       // private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
       // private static string commandToReadDeviceErrorStateFailedText = "Command to read the device errror state failed.";
      //  private static string lostDeviceConnectionText = "Lost the connection to the device.";
        private static string notConnectedToMainMonitorWarningText = "You are not connected to a Main monitor.  Cannot configure using this interface.";
        private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
       // private static string failedToWriteTheConfigurationToTheDevice = "Failed to write the configuration to the device.";
      //  private static string configurationWasReadFromTheDevice = "The configuration was read from the device.";
       // private static string configurationWasWrittenToTheDevice = "The configuration was written to the device.";
        private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        private static string saveDeviceConfigurationQuestionText = "Save device configuration?";
        private static string workingConfigurationNotDefinedText = "The current configuration is undefined.";
      //  private static string commandToReadDeviceTypeFailed = "Command to read the device type failed";
        private static string configurationWasIncompleteInDatabaseAndHasBeenRemovedText = "An incomplete configuration was found in the database and has been removed";

      //  private static string workingConfigAlreadyPresentInDatabaseText = "There is already a working configuration saved in the database.  Overwrite it?";
      //  private static string failedToDeleteWorkingConfigurationText = "Failed to delete the working configuration from the database";
        private static string workingConfigurationSavedToDatabaseText = "The working configuration was saved to the database";
        private static string workingConfigurationNotSavedToDatabaseText = "Error: the working configuration was not saved to the database";
        private static string configurationWasReadFromTheDatabaseText = "The configuration was successfully read from the database";

        private static string changesToWorkingConfigurationNotSavedOverwriteWarningText = "You have made changes to the configuration that have not been saved.  Discard those changes?";
        private static string changesToWorkingConfigurationNotSavedExitWarningText = "You have made changes to the configuration that have not been saved.  Exit anyway?";
        private static string discardChangesAsQuestionText = "Discard changes?";

        private static string configurationLoadCancelledText = "Configuration load cancelled";

        private static string failedToDeleteCurrentConfigurationText = "Failed to delete the current configuration from the database";
        private static string failedtoDeleteConfigurationText = "Failed to delete the configuration";
        private static string replaceExistingConfigurationsWithTheSameNameQuestionText = "Would you like to replace all existing configurations having the same description with this one?";

        private void SaveWorkingConfigurationToDatabase(MonitorInterfaceDB localDB)
        {
            try
            {
                string defaultConfigurationName = string.Empty;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.workingConfiguration != null)
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        if (this.monitor != null)
                        {
                            if (Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor)
                            {
                                defaultConfigurationName = this.nameOfLastConfigurationLoadedFromDatabase;
                            }
                            using (ConfigurationDescription description = new ConfigurationDescription(Main_WHSMonitorConfiguration.CurrentConfigName, defaultConfigurationName))
                            {
                                description.ShowDialog();
                                description.Hide();

                                if (description.SaveConfiguration)
                                {
                                    if ((Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor) && (NamedConfigurationAlreadySavedInDatabase(this.monitor, description.ConfigurationName, localDB)))
                                    {
                                        if (RadMessageBox.Show(this, replaceExistingConfigurationsWithTheSameNameQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            if (!DeleteNamedConfigurationFromDatabase(this.monitor, description.ConfigurationName, localDB))
                                            {
                                                RadMessageBox.Show(this, failedtoDeleteConfigurationText);
                                            }
                                        }
                                    }
                                    //WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                                    if (ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(this.workingConfiguration, this.monitor.ID, DateTime.Now, description.ConfigurationName, localDB))
                                    {
                                        RadMessageBox.Show(this, workingConfigurationSavedToDatabaseText);
                                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, workingConfigurationNotSavedToDatabaseText);
                                    }
                                    LoadAvailableConfigurations(localDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMain_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
                // }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool SafeToLoadConfiguration()
        {
            bool safeToLoad = true;
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();
                if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                {
                    if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedOverwriteWarningText, discardChangesAsQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        safeToLoad = false;
                    }
                }               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SafeToLoadConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return safeToLoad;
        }

        private static bool NamedConfigurationAlreadySavedInDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool configurationIsPresent=false;
            try
            {
                List<Main_Config_WHSConfigurationRoot> configRootList = null;
                if (Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        // this list will be, at most, two entries long
                        foreach (Main_Config_WHSConfigurationRoot entry in configRootList)
                        {
                            if (entry.Description.Contains(configurationName))
                            {
                                configurationIsPresent = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.NamedConfigurationAlreadySavedInDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationIsPresent;
        }

        private static bool DeleteNamedConfigurationFromDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_WHSConfigurationRoot> configRootList = null;
                if (Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        for (int i = 0; i < configRootList.Count; i++)
                        {
                            if (configRootList[i].Description.Contains(configurationName))
                            {
                                success = MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(configRootList[i].ID, localDB);
                                //break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DeleteNamedConfigurationFromDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


//        private void SaveDeviceConfigurationToDatabase()
//        {
//            try
//            {
//                if (this.configurationFromDevice != null)
//                {
//                    using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
//                    {
//                        SaveDeviceConfigurationToDatabase(this.configurationFromDevice, true, saveDB);
//                        LoadAvailableConfigurations(saveDB);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationLoadedFromDeviceText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SaveDeviceConfigurationToDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private static bool SaveDeviceConfigurationToDatabase(Monitor monitor, Main_WHSConfiguration configurationBeingSaved, bool interactive, MonitorInterfaceDB localDB, IWin32Window parentWindow)
        {
            bool success = false;
            try
            {
                
                //Guid configToDeleteConfigurationRootID = Guid.Empty;
                bool saveConfiguration = false;
                if (configurationBeingSaved != null)
                {
                    if (interactive)
                    {
                        if (RadMessageBox.Show(parentWindow, configurationBeingSavedIsFromDeviceText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            saveConfiguration = true;
                        }
                    }
                    else
                    {
                        saveConfiguration = true;
                    }
                    if (saveConfiguration)
                    {
                        if (NamedConfigurationAlreadySavedInDatabase(monitor, Main_WHSMonitorConfiguration.CurrentConfigName, localDB))
                        {
                            if (!DeleteNamedConfigurationFromDatabase(monitor, Main_WHSMonitorConfiguration.CurrentConfigName, localDB))
                            {
                                if (interactive)
                                {
                                    RadMessageBox.Show(parentWindow, failedToDeleteCurrentConfigurationText);
                                }
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to delete the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                            else
                            {
                                string errorMessage = "In Main_WHSMonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nDeleted the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
                            }
                        }
                        if (ConfigurationConversion.SaveMain_WHSConfigurationToDatabase(configurationBeingSaved, monitor.ID, DateTime.Now, Main_WHSMonitorConfiguration.currentConfigName, localDB))
                        {
                            success = true;
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationSavedToDatabaseText);
                            }
                        }
                        else
                        {
                            if (interactive)
                            {
                                RadMessageBox.Show(parentWindow, deviceConfigurationNotSavedToDatabaseText);
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void LoadSelectedConfigurationFromDatabase()
        {
            try
            {
                Main_WHSConfiguration configFromDB = null;
                Main_Config_WHSConfigurationRoot configurationRoot;

                if (this.availableConfigurationsSelectedIndex > 0)
                {
                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                    {
                        configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                        if (configurationRoot.ID.CompareTo(Guid.Empty) != 0)
                        {
                            if (SafeToLoadConfiguration())
                            {
                                configFromDB = LoadConfigurationFromDatabase(configurationRoot.ID, this.dbConnectionString, this);
                                if (configFromDB != null)
                                {
                                    if (configFromDB.AllConfigurationMembersAreNonNull())
                                    {
                                        this.workingConfiguration = configFromDB;
                                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);

                                        if (configurationRoot.Description.Trim().CompareTo(Main_WHSMonitorConfiguration.currentConfigName) != 0)
                                        {
                                            nameOfLastConfigurationLoadedFromDatabase = configurationRoot.Description;
                                        }

                                        RadMessageBox.Show(this, configurationWasReadFromTheDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noConfigurationSelectedText);
                }
//                if (this.availableConfigurationsSelectedIndex > 0)
//                {
//                    if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
//                    {
//                        Guid configurationRootID = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1].ID;
//                        LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(configurationRootID);
//                        EnableFromDatabaseConfigurationRadRadioButtons();
//                        SetFromDatabaseRadRadioButtonState();
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationSelectedText);
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(Guid configurationRootID)
//        {
//            try
//            {
//                Main_WHSConfiguration temporaryConfiguration = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString, this);

//                if (temporaryConfiguration != null)
//                {
//                    if (temporaryConfiguration.AllConfigurationMembersAreNonNull())
//                    {
//                        // test new config against old config
//                        //if (workingConfiguration != null)
//                        //{
//                        //    Int16[] newRegisterValues = temporaryConfiguration.GetShortValuesFromAllContributors();
//                        //    // vShowSomeValuesThatArePissingMeOff(newRegisterValues);
//                        //    CheckLatestConfigurationAgainstPreviousConfiguration(newRegisterValues);
//                        //}

//                        configurationFromDatabase = temporaryConfiguration;

//                        EnableFromDatabaseConfigurationRadRadioButtons();
//                        WriteConfigurationDataToAllInterfaceObjects(configurationFromDatabase);
                      
//                        //if (!workingConfiguration.ConfigurationIsTheSame(workingConfiguration))
//                        //{
//                        //    MessageBox.Show("Configuration checking code is no good.");
//                        //}
//                    }
//                    else
//                    {
//                        RadMessageBox.Show(this, errorInConfigurationLoadedFromDatabaseText);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, configurationCouldNotBeLoadedFromDatabaseText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public Main_WHSConfiguration LoadConfigurationFromDatabase(Guid configurationRootID, string dbConnectionSTring, IWin32Window parentWindow)
        {
            Main_WHSConfiguration configurationFromDB = null;
            try
            {
                
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionSTring))
                {
                    configurationFromDB = ConfigurationConversion.GetMain_WHSConfigurationFromDatabase(configurationRootID, localDB);

                    if ((configurationFromDB == null) || ((configurationFromDB != null) & (!configurationFromDB.AllConfigurationMembersAreNonNull())))
                    {
                        /// this should not happen to the user unless the code is frelled up, but we'll put it in anyway
                        if (MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(configurationRootID, localDB))
                        {
                            RadMessageBox.Show(parentWindow, configurationWasIncompleteInDatabaseAndHasBeenRemovedText);
                            LoadAvailableConfigurations(localDB);
                        }
                        configurationFromDB = null;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationFromDB;
        }

        public static Guid GetConfigurationRootIDForCurrentDeviceConfiguration(Guid monitorID, MonitorInterfaceDB configDB)
        {
            Guid configurationRootID = Guid.Empty;
            try
            {
                List<Main_Config_WHSConfigurationRoot> availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitorID, configDB);
                if (availableConfigurations != null)
                {
                    foreach (Main_Config_WHSConfigurationRoot entry in availableConfigurations)
                    {
                        if (entry.Description.Trim().CompareTo(Main_WHSMonitorConfiguration.currentConfigName) == 0)
                        {
                            configurationRootID = entry.ID;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetConfigurationRootIDForCurrentDeviceConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootID;
        }

        List<Main_Config_WHSConfigurationRoot> GetAllConfigurationRootTableEntriesMarkedAsCurrentDeviceConfiguration(Guid monitorID, MonitorInterfaceDB configDB)
        {
            List<Main_Config_WHSConfigurationRoot> configurationRootList = null;
            try
            {
                List<Main_Config_WHSConfigurationRoot> availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitorID, configDB);
                if (availableConfigurations != null)
                {
                    foreach (Main_Config_WHSConfigurationRoot entry in availableConfigurations)
                    {
                        if (entry.Description.Trim().CompareTo(Main_WHSMonitorConfiguration.currentConfigName) == 0)
                        {
                            if (configurationRootList == null)
                            {
                                configurationRootList = new List<Main_Config_WHSConfigurationRoot>();
                            }
                            configurationRootList.Add(entry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetAllConfigurationRootTableEntriesMarkedAsCurrentDeviceConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static Main_WHSConfiguration LoadCurrentDeviceConfigurationFromDatabase(Guid monitorID, MonitorInterfaceDB configDB)
        {
            
            Main_WHSConfiguration currentDeviceConfiguration = null;
            try
            {
                Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(monitorID, configDB);
                currentDeviceConfiguration = ConfigurationConversion.GetMain_WHSConfigurationFromDatabase(configurationRootID, configDB);                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return currentDeviceConfiguration;
        }

        private void DeleteConfigurationFromDatabase()
        {
            try
            {
                Main_Config_WHSConfigurationRoot configurationRoot;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.availableConfigurationsSelectedIndex > 0)
                    {
                        if (this.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                        {
                            configurationRoot = this.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                            if (configurationRoot.Description.Trim().CompareTo(Main_WHSMonitorConfiguration.currentConfigName) != 0)
                            {
                                if (RadMessageBox.Show(this, configurationDeleteFromDatabaseWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                    {
                                        MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(configurationRoot.ID, localDB);
                                        LoadAvailableConfigurations(localDB);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, cannotDeleteCurrentConfigurationWarningText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.DeleteConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noConfigurationSelectedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DeleteConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private static Main_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        //        {
        //            Main_Configuration deviceConfiguration = null;
        //            try
        //            {
        //                Byte[] byteData = null;

        //                if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 15002)
        //                {
        //                    byteData = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, readDelayInMicroseconds);
        //                    if (byteData != null)
        //                    {
        //                        deviceConfiguration = new Main_Configuration(byteData);
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice(int, int)\nMonitor was not a Main.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return deviceConfiguration;
        //        }

        private void LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                long deviceError = 0;
                int deviceType;
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Main_WHSConfiguration configFromDevice = null;
                if (SafeToLoadConfiguration())
                {
                    DisableAllControls();
                    if (!downloadInProgress)
                    {
                        if (this.monitor != null)
                        {
                            modBusAddressAsString = monitor.ModbusAddress.Trim();
                            if (modBusAddressAsString.Length > 0)
                            {
                                modBusAddress = Int32.Parse(modBusAddressAsString);

                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                {
                                    errorCode = MonitorConnection.OpenMonitorConnection(this.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                }
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    DeviceCommunication.EnableDataDownload();
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                    if (deviceType > 0)
                                    {
                                        // deviceType = DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, this.readDelayInMicroseconds);
                                        if (deviceType == 101)
                                        {
                                            deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceError > -1)
                                            {
                                                Application.DoEvents();
                                                configFromDevice = LoadConfigurationFromDevice(modBusAddress, this.readDelayInMicroseconds);
                                                if (configFromDevice != null)
                                                {
                                                    if (configFromDevice.AllConfigurationMembersAreNonNull())
                                                    {
                                                        errorCode = ErrorCode.ConfigurationDownloadSucceeded;

                                                        this.workingConfiguration = configFromDevice;
                                                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);

                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                                        // Testing code

                                                        //this.previousConfigurationAsArray = this.workingConfigurationAsArray;
                                                        //this.workingConfigurationAsArray = registerData;

                                                        //CheckLatestConfigurationAgainstPreviousConfiguration(registerData);

                                                        // more testing code, show some values

                                                        // ShowSomeValuesThatArePissingMeOff(registerData);

                                                        // AddDataToAllTransientInterfaceObjects(configurationFromDevice);
                                                        //EnableCopyDeviceConfigurationRadButtons();
                                                        //EnableFromDeviceRadRadioButtons();
                                                        //SetFromDeviceRadRadioButtonState();

                                                        WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);                                                      

                                                        /// test code
                                                        //Int16[] registerValues = configurationFromDevice.GetShortValuesFromAllContributors();
                                                        //Main_Configuration testConfiguration = new Main_Configuration(registerValues);
                                                        //if (!configurationFromDevice.ConfigurationIsTheSame(testConfiguration))
                                                        //{
                                                        //    MessageBox.Show("configuration mismatch from a copy");
                                                        //}
                                                        //else
                                                        //{
                                                        //    MessageBox.Show("configuration matches from a copy");
                                                        //}
                                                        /// end test code

                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(this.monitor.ID, localDB);
                                                            if (configurationRootID.CompareTo(Guid.Empty) != 0)
                                                            {
                                                                Main_WHSConfiguration deviceConfigFromDb = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString, this);
                                                                if (!deviceConfigFromDb.ConfigurationIsTheSame(this.workingConfiguration))
                                                                {
                                                                    RadMessageBox.Show(this, deviceConfigurationDoesNotMatchDatabaseConfigurationText);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (RadMessageBox.Show(this, deviceCommunicationNotSavedInDatabaseYetText, saveDeviceConfigurationQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                {
                                                                    SaveDeviceConfigurationToDatabase(this.monitor, this.workingConfiguration, false, localDB, this);
                                                                    LoadAvailableConfigurations(localDB);
                                                                }
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationDownloadFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToMainMonitor));
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                    }
                                }
                                else
                                {
                                    if (errorCode == ErrorCode.SerialPortNotSpecified)
                                    {
                                        RadMessageBox.Show(this, serialPortNotSetWarningText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()\nthis.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, configurationLoadCancelledText);
                }

                UpdateAlarmListButton_Click(null, null);
                updateErrorButton_Click(null, null);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                MonitorConnection.CloseConnection();
                EnableAllControls();
            }
        }

        public Main_WHSConfiguration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        {
            Main_WHSConfiguration deviceConfiguration = null;
            try
            {
                Int16[] registerData = null;
                int firmwareVersion=0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                //if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 101)
                //{             
                EnableProgressBars();
                SetProgressBarsToDownloadState();
                SetProgressBarProgress(0, 100);
                registerData = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                if (registerData != null)
                {
                    firmwareVersion = registerData[0];
                }
                SetProgressBarProgress(50, 100);//changed num of regs from 60 to 63  cfk 12/20/13
                registerData = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5451, 60, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                short[] fs = new short[2];
                // read fan status and set colors of buttons
                fs = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5528, 2, 200, 1, 20, parentWindowInformation);

                if (fs[0] == 1)
                {

                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;

                }
                else
                {
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }

                if (fs[1] == 1)
                {

                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;

                }
                else
                {
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }
                SetProgressBarProgress(100, 100);
                if (registerData != null)
                {
                    deviceConfiguration = new Main_WHSConfiguration(registerData, firmwareVersion);
                }
                if (registerData[27] == 0)
                {
                    fanTestButton.Text = "Cooling Off";
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;

                }
                else
                {
                    fanTestButton.Text = "Cooling On";
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    fanTestButton.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                DisableProgressBars();
            }
            return deviceConfiguration;
        }

        private void ProgramDevice()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                int deviceType;
                long deviceError;
                Int16[] registerData = null;
                int firmwareVersion = 0;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Monitor monitorFromDB = null;
                bool deviceProgrammedSuccessfully = false;

                
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (!downloadInProgress)
                    {
                        if (this.workingConfiguration != null)
                        {
                            if (this.monitor != null)
                            {
                                modBusAddressAsString = monitor.ModbusAddress.Trim();
                                if (modBusAddressAsString.Length > 0)
                                {
                                    modBusAddress = Int32.Parse(modBusAddressAsString);
                                    if (!ErrorIsPresentInSomeTabObject())
                                    {
                                        WriteAllInterfaceDataToWorkingConfiguration();

                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                        {
                                            errorCode = MonitorConnection.OpenMonitorConnection(this.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                        }
                                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                        {
                                            DeviceCommunication.EnableDataDownload();
                                            deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceType > 0)
                                            {
                                                if (deviceType == 101)
                                                {
                                                    Application.DoEvents();

                                                    /// snag the firmware version from the device we are about to write to.  that should be correct I think.
                                                    registerData = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 2, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                                    if (registerData != null)
                                                    {
                                                        firmwareVersion = registerData[0];
                                                    }
                                                    this.workingConfiguration.windingHotSpotSetup.FirmwareVersion = firmwareVersion;
                                                    if (firmwareVersion > 0)
                                                    {
                                                        this.firmwareVersionValueRadLabel.Text = (Math.Round((firmwareVersion / 100.0), 2)).ToString();
                                                    }
                                                    else
                                                    {
                                                        this.firmwareVersionValueRadLabel.Text = firmwareVersionUnknownText;
                                                    }

                                                    deviceProgrammedSuccessfully = Main_WriteWindingHotSpotSetupToDevice(modBusAddress);
                                                    Cursor.Current = Cursors.Default;
                                                   
                                                    Application.DoEvents();

                                                    if (deviceProgrammedSuccessfully)
                                                    {
                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            // if the user changed the ModBus address, update that information automatically
                                                            if (modBusAddress.ToString().CompareTo(this.monitor.ModbusAddress) != 0)
                                                            {
                                                                monitorFromDB = General_DatabaseMethods.GetOneMonitor(this.monitor.ID, localDB);
                                                                if (monitorFromDB != null)
                                                                {
                                                                    monitorFromDB.ModbusAddress = modBusAddress.ToString();
                                                                    localDB.SubmitChanges();
                                                                }
                                                                this.monitor.ModbusAddress = modBusAddress.ToString();
                                                            }
                                                            // now, delete the old device config from the DB, and save the new one.
                                                            SaveDeviceConfigurationToDatabase(this.monitor, this.workingConfiguration, false, localDB, this);
                                                            LoadAvailableConfigurations(localDB);
                                                        }
                                                        /// since the configuraiton has been "saved" by using it to program the device, we update the unedited configuration
                                                        /// to match what we just wrote to the device
                                                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteSucceeded));
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteFailed));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToMainMonitor));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            if (errorCode == ErrorCode.SerialPortNotSpecified)
                                            {
                                                RadMessageBox.Show(this, serialPortNotSetWarningText);
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nthis.monitor was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, workingConfigurationNotDefinedText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }

        public bool Main_WriteWindingHotSpotSetupToDevice(int modBusAddress)
        {
            bool success = false;
            try
            {
              
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                Int16[] allRegisterValues = null;
                Int16[] registerValuesToWrite;
                // Int16[] currentRegisterValues = null;
                //int registersPerDataSend = 100;
                List<int> errorCodes = new List<int>();
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                string downloadErrorMessage = string.Empty;
                ErrorCode error = ErrorCode.None;

                allRegisterValues = this.workingConfiguration.GetShortValuesFromAllContributors();
                // currentRegisterValues = new Int16[registersPerDataSend];

                // write the password so we can write the rest of the values

                // DeviceCommunication.Main_SetDeviceSetup(modBusAddress, 

                DisableAllControls();
                SetProgressBarsToUploadState();
                SetProgressBarProgress(0, 100);
                EnableProgressBars();
               
                /// write the first block of data
                registerValuesToWrite = new short[60];
                Array.Copy(allRegisterValues, 0, registerValuesToWrite, 0, 60);
                SendPassword();
                success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5451, registerValuesToWrite, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);

                /// skip the value for previous aging, which is written separately
                SetProgressBarProgress(33, 100);
                /// write the second block of data
                /// 
                /*
                if (success)
                {
                    registerValuesToWrite = new short[29];// cfk 12/20/13  changed from 23 to 29
                    Array.Copy(allRegisterValues, 30, registerValuesToWrite, 0, 29);// cfk 12/20/13  changed from 23 to 29
                    success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5481, registerValuesToWrite, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    SetProgressBarProgress(67, 100);
                }

                /// Skip the values for the date of the last fan exercise and the run times for fans 1 and 2

                /// write last two registers
                if (success)
                {
                    registerValuesToWrite = new short[4];// cfk 12/20/13  changed from 2 to 4
                    Array.Copy(allRegisterValues, 58, registerValuesToWrite, 0, 4);// cfk 12/20/13  changed from 2 to 4
                    success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5509, registerValuesToWrite, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                    SetProgressBarProgress(100, 100);
                }
                if (!success)
                {
                    RadMessageBox.Show(this, failedToUploadConfigurationText);
                }
                */
                //// reset the configuration to read only
                //DeviceCommunication.WriteSingleRegister(modBusAddress, 121, 0, this.readDelayInMicroseconds);

                //}
                //else
                //{
                //    RadMessageBox.Show(this, "Error in password sent to device.\nPlease check the connection.");
                //}
                //}
                //else
                //{
                //    if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadFailedMessage) == 0)
                //    {
                //        error = ErrorCode.DownloadFailed;
                //    }
                //    else
                //    {
                //        error = ErrorCode.DownloadCancelled;
                //    }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_SetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return success;
        }
    }
}
