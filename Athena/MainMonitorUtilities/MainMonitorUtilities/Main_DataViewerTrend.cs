﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Linq;
using System.Data.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using Dynamics;
using ChartDirector;
using GeneralUtilities;
using DataObjects;
using PasswordManagement;
using DatabaseInterface;

namespace MainMonitorUtilities
{
    public partial class Main_DataViewer
    {
        /// <summary>
        /// The minimum width of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumWidth = 720;

        /// <summary>
        /// The minimum height of the trendWinChartViewer object
        /// </summary>
        int trendChartMinimumHeight = 660;

        /// <summary>
        /// The minimum width of the trend graph
        /// </summary>
        int trendGraphWidthMinimum = 640;

        /// <summary>
        /// The minimum height of the trend graph
        /// </summary>
        int trendGraphHeightMinimum = 510;

        /// <summary>
        /// The minimum amount the trend graph is offset from the left border of the trendWinChartViewer object
        /// </summary>
        int trendGraphOffsetX = 55;

        /// <summary>
        /// The minimum amount the trend graph is offset from the top border of the trendWinChartViewer object
        /// </summary>
        int trendGraphtOffsetY = 100;

        /// <summary>
        /// The current amount the trend graph is offset from the left border of the trendWinChartViewer object.  Increased
        /// offset makes room for the different scales possible for the y axis.
        /// </summary>
        int trendGraphCurrentOffsetX;

        /// <summary>
        /// The maximum Y value that can currently be displayed in the trend chart
        /// </summary>
        double trendChartMaxYValue;

        /// <summary>
        /// The minimum Y value that can currently be displayed in the trend chart
        /// </summary>
        double trendChartMinYValue;

        /// <summary>
        /// The index into the dataTimeAsDouble array associated with the date the cursor is currently covering
        /// </summary>
        int cursorDateTimeAsDoubleIndex = -1;

        /// <summary>
        /// The value in the dataTImesAsDouble array assoicated with the current cursor position
        /// </summary>
        double cursorXval = -1.0;

        double oldCursorXval;

        /// <summary>
        /// A pointer to preserve the instance of the trend chart after it is created in DrawTrendChart(), used 
        /// to calculate the postion of the cursor.
        /// </summary>
        XYChart trendXYChart;

        /// <summary>
        /// The position where the display for x values on trend graph starts, used to compute the cursor position
        /// and whether it should be visible.
        /// </summary>
        int trendGraphXStart;

        /// <summary>
        /// The position where the display for y values on trend graph starts, used to compute the cursor position
        /// and whether it should be visible.
        /// </summary>
        int trendGraphYStart;

        /// <summary>
        /// The height of the trend graph in graph units, used to compute the height of the cursor
        /// </summary>
        int trendGraphHeight;

        /// <summary>
        /// The width of the trend graph in graph units, mostly used to determine whether the cursor should be
        /// visible or not.
        /// </summary>
        int trendGraphWidth;

        /// <summary>
        /// Used to determine whether to update the trend chart when switching to it from another tab
        /// </summary>
        bool trendChartNeedsUpdating = false;

        /// <summary>
        /// The minimum data reading in the data set currently loaded in memory
        /// </summary>
        DateTime minimumDateOfDataSet;

        /// <summary>
        /// The total number of seconds spanned by the data set currently loaded in memory
        /// </summary>
        double dateRange;

        /// <summary>
        /// The double equivalent of all the dates of the data readings, with the values given by a ChartDirector
        /// converions routine.
        /// </summary>
        double[] allDateTimesAsDouble;

        private DateTime dataCutoffDate;

        private bool cursorIsEnabled = true;

        private bool gridDataIsBeingEdited;
        private bool graphDataNeedsUpdating;

        private double humidityScaleFactor = 0.0;

        private double moistureScaleFactor = 0.0;

        private double temp1ScaleFactor = 0;
        private double temp2ScaleFactor = 0;
        private double temp3ScaleFactor = 0;
        private double temp4ScaleFactor = 0;

        private double loadCurrent1ScaleFactor = 1.0;
        private double loadCurrent2ScaleFactor = 1.0;
        private double loadCurrent3ScaleFactor = 1.0;

        private double voltage1ScaleFactor = 1.0;
        private double voltage2ScaleFactor = 1.0;
        private double voltage3ScaleFactor = 1.0;

        private double analog1ScaleFactor = 1.0;
        private double analog2ScaleFactor = 1.0;
        private double analog3ScaleFactor = 1.0;
        private double analog4ScaleFactor = 1.0;
        private double analog5ScaleFactor = 1.0;
        private double analog6ScaleFactor = 1.0;

        private double vibration1ScaleFactor = 1.0;
        private double vibration2ScaleFactor = 1.0;
        private double vibration3ScaleFactor = 1.0;
        private double vibration4ScaleFactor = 1.0;

        private string humidityOperation = addText;

        private string moistureOperation = addText;

        private string temp1Operation = addText;
        private string temp2Operation = addText;
        private string temp3Operation = addText;
        private string temp4Operation = addText;

        private string loadCurrent1Operation = multiplyText;
        private string loadCurrent2Operation = multiplyText;
        private string loadCurrent3Operation = multiplyText;

        private string voltage1Operation = multiplyText;
        private string voltage2Operation = multiplyText;
        private string voltage3Operation = multiplyText;

        private string analog1Operation = multiplyText;
        private string analog2Operation = multiplyText;
        private string analog3Operation = multiplyText;
        private string analog4Operation = multiplyText;
        private string analog5Operation = multiplyText;
        private string analog6Operation = multiplyText;

        private string vibration1Operation = multiplyText;
        private string vibration2Operation = multiplyText;
        private string vibration3Operation = multiplyText;
        private string vibration4Operation = multiplyText;

        /// <summary>
        /// The default viewport size for the trend graph, in seconds
        /// </summary>
        private static double trendGraphDefaultDuration = 120 * 86400;

        /// <summary>
        /// The current visible duration of the trend view port in seconds
        /// </summary>
        private double trendGraphCurrentDuration = trendGraphDefaultDuration;

        /// <summary>
        /// The minimum visible duration in the trend graph view in seconds, limits the amount one can zoom in.
        /// </summary>
        private double trendGraphMinDuration = .04 * 86400;

        #region Color settings for graphing the data types

        public int temp1Color = Chart.CColor(Color.Red);
        public int temp2Color = Chart.CColor(Color.Yellow);
        public int temp3Color = Chart.CColor(Color.Indigo);
        public int temp4Color = Chart.CColor(Color.Green);
        public int chassisTempColor = Chart.CColor(Color.Orange);

        public int humidityColor = Chart.CColor(Color.Violet);
        public int moistureColor = Chart.CColor(Color.SlateGray);

        public int loadCurrent1Color = Chart.CColor(Color.Chartreuse);
        public int loadCurrent2Color = Chart.CColor(Color.Teal);
        public int loadCurrent3Color = Chart.CColor(Color.Brown);

        public int voltage1Color = Chart.CColor(Color.OrangeRed);
        public int voltage2Color = Chart.CColor(Color.Blue);
        public int voltage3Color = Chart.CColor(Color.Purple);

        public int analog1Color = Chart.CColor(ColorTranslator.FromHtml("#ff0000"));
        public int analog2Color = Chart.CColor(ColorTranslator.FromHtml("#ff6600"));
        public int analog3Color = Chart.CColor(ColorTranslator.FromHtml("#ff9900"));
        public int analog4Color = Chart.CColor(ColorTranslator.FromHtml("#ace600"));
        public int analog5Color = Chart.CColor(ColorTranslator.FromHtml("#00cc00"));
        public int analog6Color = Chart.CColor(ColorTranslator.FromHtml("#0099cc"));

        public int vibration1Color = Chart.CColor(ColorTranslator.FromHtml("#bb00cc"));
        public int vibration2Color = Chart.CColor(ColorTranslator.FromHtml("#ff4800"));
        public int vibration3Color = Chart.CColor(ColorTranslator.FromHtml("#bf5700"));
        public int vibration4Color = Chart.CColor(ColorTranslator.FromHtml("#bfa300"));

        public int highVoltageTempPhaseAColor = Chart.CColor(Color.LightGreen);
        public int highVoltageTempPhaseBColor = Chart.CColor(Color.OliveDrab);
        public int highVoltageTempPhaseCColor = Chart.CColor(Color.SpringGreen);

        public int tertiaryVoltageTempPhaseAColor = Chart.CColor(Color.DarkSlateGray);
        public int tertiaryVoltageTempPhaseBColor = Chart.CColor(Color.Fuchsia);
        public int tertiaryVoltageTempPhaseCColor = Chart.CColor(Color.Tomato);

        public int temp3PhaseAColor = Chart.CColor(Color.Pink);
        public int temp3PhaseBColor = Chart.CColor(Color.OrangeRed);
        public int temp3PhaseCColor = Chart.CColor(Color.DarkKhaki);

        public int maxWindingTempPhaseAColor = Chart.CColor(Color.YellowGreen);
        public int maxWindingTempPhaseBColor = Chart.CColor(Color.RoyalBlue);
        public int maxWindingTempPhaseCColor = Chart.CColor(Color.LightBlue);

        public int agingFactorColor = Chart.CColor(Color.Gold);
        public int accumulatedAgingColor = Chart.CColor(Color.BurlyWood);

        #endregion

        #region Symbol Definitions for Graphing the Data Types

        public int temp1Symbol = Chart.TriangleShape;
        public int temp2Symbol = Chart.TriangleShape;
        public int temp3Symbol = Chart.TriangleShape;
        public int temp4Symbol = Chart.TriangleShape;
        public int chassisTempSymbol = Chart.TriangleShape;

        public int humiditySymbol = Chart.InvertedTriangleShape;
        public int moistureSymbol = Chart.CircleShape;

        public int loadCurrent1Symbol = Chart.LeftTriangleShape;
        public int loadCurrent2Symbol = Chart.LeftTriangleShape;
        public int loadCurrent3Symbol = Chart.LeftTriangleShape;

        public int voltage1Symbol = Chart.RightTriangleShape;
        public int voltage2Symbol = Chart.RightTriangleShape;
        public int voltage3Symbol = Chart.RightTriangleShape;

        public int analog1Symbol = Chart.StarShape(3);
        public int analog2Symbol = Chart.StarShape(3);
        public int analog3Symbol = Chart.StarShape(3);
        public int analog4Symbol = Chart.StarShape(3);
        public int analog5Symbol = Chart.StarShape(3);
        public int analog6Symbol = Chart.StarShape(3);

        public int vibration1Symbol = Chart.StarShape(5);
        public int vibration2Symbol = Chart.StarShape(5);
        public int vibration3Symbol = Chart.StarShape(5);
        public int vibration4Symbol = Chart.StarShape(5);

        public int highVoltageTempPhaseASymbol = Chart.TriangleShape;
        public int highVoltageTempPhaseBSymbol = Chart.TriangleShape;
        public int highVoltageTempPhaseCSymbol = Chart.TriangleShape;

        public int tertiaryVoltageTempPhaseASymbol = Chart.TriangleShape;
        public int tertiaryVoltageTempPhaseBSymbol = Chart.TriangleShape;
        public int tertiaryVoltageTempPhaseCSymbol = Chart.TriangleShape;

        public int temp3PhaseASymbol = Chart.TriangleShape;
        public int temp3PhaseBSymbol = Chart.TriangleShape;
        public int temp3PhaseCSymbol = Chart.TriangleShape;

        public int maxWindingTempPhaseASymbol = Chart.TriangleShape;
        public int maxWindingTempPhaseBSymbol = Chart.TriangleShape;
        public int maxWindingTempPhaseCSymbol = Chart.TriangleShape;

        public int agingFactorSymbol = Chart.DiamondShape;
        public int accumulatedAgingSymbol = Chart.DiamondShape;

        #endregion

        enum ScaleType
        {
            MilliWatts,
            TimesPerYear,
            MilliVolts,
            NanoCoulombs,
            PulsesPerSecond,
            PulsesPerCycle,
            Percent,
            DegreesCentigrade,
            KilogramsPerMeterCubed,
            Unitless,
            KiloVolts,
            Amps
        };

        public class ScaleLabels
        {
            public static string Percent = "%";
            public static string Unitless = " ";
            public static string TimesPerYear = "times/\nyear";
            public static string DegreesCentigrade = "ºC";
            public static string KilogramsPerMeterCubed = "kg/M3";
            public static string MilliWatts = "mW";
            public static string MilliVolts = "mV";
            public static string NanoCoulombs = "nC";
            public static string PulsesPerSecond = "pulse/\nsec";
            public static string PulsesPerCycle = "pulse/\ncycle";
            public static string KiloVolts = "kV";
            public static string Amps = "Amps";
        }

        /// <summary>
        /// Makes sure the cursor can move left on the trend chart, then moves the cursor to point
        /// to the data associated with the DateTime immediately earlier than the current cursor 
        /// position
        /// </summary>
        private void MoveXYcursorLeft()
        {
            try
            {
                int newChartXval;
                if (this.cursorDateTimeAsDoubleIndex > 0)
                {
                    this.oldCursorXval = this.cursorXval;
                    this.cursorDateTimeAsDoubleIndex--;
                    this.cursorXval = this.allDateTimesAsDouble[this.cursorDateTimeAsDoubleIndex];
                    newChartXval = trendXYChart.getXCoor(this.cursorXval);
                    if (newChartXval < this.trendGraphCurrentOffsetX)
                    {
                        ScrollXYcursorIntoView(newChartXval);
                        //// scrollLength = (((this.trendGraphXStart - newChartXval + this.trendGraphOffsetX) / this.trendGraphWidth) * trendRadHScrollBar.LargeChange) + (trendRadHScrollBar.LargeChange / 2) ;
                        // scrollLength = (((this.trendGraphOffsetX - newChartXval) / this.trendGraphWidth) * trendRadHScrollBar.LargeChange) + (trendRadHScrollBar.LargeChange / 2);
                        // newScrollValue = trendRadHScrollBar.Value - scrollLength;
                        // //newScrollValue = trendRadHScrollBar.Value - trendRadHScrollBar.SmallChange;
                        // trendRadHScrollBar.Value = Math.Max(newScrollValue, trendRadHScrollBar.Minimum);
                    }
                    else
                    {
                        TrendUpdateCursorPosition();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.MoveXYcursorLeft()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Makes sure the cursor can move right on the trend chart, then moves the cursor to point
        /// to the data associated with the DateTime immediately earlier than the current cursor 
        /// position
        /// </summary>
        private void MoveXYcursorRight()
        {
            try
            {
                int newChartXval;
                if (this.cursorDateTimeAsDoubleIndex < (allDateTimesAsDouble.Length - 1))
                {
                    this.oldCursorXval = this.cursorXval;
                    this.cursorDateTimeAsDoubleIndex++;
                    this.cursorXval = this.allDateTimesAsDouble[this.cursorDateTimeAsDoubleIndex];
                    newChartXval = trendXYChart.getXCoor(this.cursorXval);
                    if (newChartXval > (this.trendGraphCurrentOffsetX + this.trendGraphWidth))
                    {
                        ScrollXYcursorIntoView(newChartXval);
                        //newScrollValue = trendRadHScrollBar.Value + trendRadHScrollBar.SmallChange;
                        //trendRadHScrollBar.Value = Math.Min(newScrollValue, trendRadHScrollBar.Maximum);
                    }
                    else
                    {
                        TrendUpdateCursorPosition();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.MoveXYcursorRight()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Scrolls the trend chart so that the cursor is in view if the cursor has moved off the screen.
        /// </summary>
        /// <param name="newChartXval"></param>
        private void ScrollXYcursorIntoView(int newChartXval)
        {
            try
            {
                int newScrollValue;
                int scrollLength;
                double distanceToScrollInTermsOfChartWidth;
                if (this.cursorDateTimeAsDoubleIndex > -1)
                {
                    {
                        if (newChartXval < this.trendGraphCurrentOffsetX)
                        {
                            // this gets the fraction of the total current screen width spanned by the interval betwee the old and new cursor positions
                            distanceToScrollInTermsOfChartWidth = (this.oldCursorXval - this.cursorXval) / this.dateRange / this.trendWinChartViewer.ViewPortWidth;

                            scrollLength = ((int)Math.Ceiling(distanceToScrollInTermsOfChartWidth * 2.0)) * (trendRadHScrollBar.LargeChange / 2);

                            newScrollValue = trendRadHScrollBar.Value - scrollLength;
                            trendRadHScrollBar.Value = Math.Max(newScrollValue, trendRadHScrollBar.Minimum);
                            UpdateTrendImageMap(this.trendWinChartViewer);
                        }

                        if (newChartXval > (this.trendGraphWidth + this.trendGraphCurrentOffsetX))
                        {
                            // this gets the fraction of the total current screen width spanned by the interval betwee the old and new cursor positions
                            distanceToScrollInTermsOfChartWidth = (this.cursorXval - this.oldCursorXval) / this.dateRange / this.trendWinChartViewer.ViewPortWidth;

                            scrollLength = ((int)Math.Ceiling(distanceToScrollInTermsOfChartWidth * 2.0)) * (trendRadHScrollBar.LargeChange / 2);

                            newScrollValue = trendRadHScrollBar.Value + scrollLength;
                            trendRadHScrollBar.Value = Math.Min(newScrollValue, trendRadHScrollBar.Maximum);
                            UpdateTrendImageMap(this.trendWinChartViewer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.ScrollXYcursorIntoView(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Moves the cursor to the latest selected position, and turns the display on or off depending on if it's currently
        /// in the chart view window.  Note that the cursor is an overlay on the chart, not part of the chart drawing routine.
        /// </summary>
        private void TrendUpdateCursorPosition()
        {
            try
            {
                if (hasFinishedInitialization)
                {
                    if (this.trendXYChart != null)
                    {
                        int xCoordinateInPixels = this.trendXYChart.getXCoor(this.cursorXval);
                        if (TrendCursorIsInRange(xCoordinateInPixels))
                        {
                            this.trendCursorLabel.Visible = true;
                            this.trendCursorLabel.Location = new Point(this.trendGraphXStart + xCoordinateInPixels, this.trendGraphYStart);
                            this.trendCursorLabel.Height = trendGraphHeight;
                            this.trendCursorLabel.Width = 1;
                        }
                        else
                        {
                            this.trendCursorLabel.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.TrendUpdateCursorPosition()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Returns true if the cursor position is on the currenty viewable chart
        /// </summary>
        /// <param name="proposedNewXcoordinateInPixels"></param>
        /// <returns></returns>
        private bool TrendCursorIsInRange(int proposedNewXcoordinateInPixels)
        {
            return !((proposedNewXcoordinateInPixels < this.trendGraphCurrentOffsetX) ||
                     (proposedNewXcoordinateInPixels > (this.trendGraphWidth + this.trendGraphCurrentOffsetX)));
        }

        /// <summary>
        /// Returns a list of all scale types that are being used for the current trend chart display.  This is used to help
        /// set up the display size and scales displayed.
        /// </summary>
        /// <returns></returns>
        private List<ScaleType> GetScalesBeingUsedForTrendChartList()
        {
            List<ScaleType> scaleTypes = new List<ScaleType>();
            try
            {
                if (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    if (humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.Percent);
                    }
                    if (moistureContentRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        scaleTypes.Add(ScaleType.KilogramsPerMeterCubed);
                    }
                    if ((WHS1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (WHS2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (WHS3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (TopOilTempRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.DegreesCentigrade);
                    }
                    

                    if ((loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.Amps);
                    }
                    if ((voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.KiloVolts);
                    }
                    if ((agingFac1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (AgingFac2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (AgingFac3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (AccumAge1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (AccumAge2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (AccumAge3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (Fan1_StartsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (Fan2_StartsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (Fan1_RunTimeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (Fan2_RunTimeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog5RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (analog6RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (vibration1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (vibration2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (vibration3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On) ||
                        (vibration4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On))
                    {
                        scaleTypes.Add(ScaleType.Unitless);
                    }
                }
                else
                {
                    scaleTypes.Add(ScaleType.Unitless);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetScalesBeingUsedForTrendChartList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return scaleTypes;
        }

        private static double[] ScaleDynamicsValues(double[] inputArray, double scaleFactor, string operation)
        {
            double[] result = null;
            try
            {
                ArrayMath scaledValues = new ArrayMath(inputArray);
                string trimmedOperation = operation.Trim();
                if (operation.Trim() == "none")
                {
                    scaledValues.mul2(1);
                }
                else
                {
                    if (trimmedOperation.CompareTo(addText) == 0)
                    {
                        scaledValues.add2(scaleFactor);
                    }
                    else if (trimmedOperation.CompareTo(multiplyText) == 0)
                    {
                        scaledValues.mul2(scaleFactor);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_DataViewer.ScaleDynamicsValues(double[], double, string)\nInput string did not indicate a defined operation.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                    MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                result = scaledValues.result();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.ScaleDynamicsValues(double[], double, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return result;
        }

        /// <summary>
        /// Adds an additional axis type to the chart.  Each scale type (e.g. percentage, milliwatt, pulse count) requires a different axis.  The input Axis is set to point to the new
        /// axis instance.
        /// </summary>
        /// <param name="axisUseCount"></param>
        /// <param name="axisOffsetValue"></param>
        /// <param name="scaleLabel"></param>
        /// <param name="axisName"></param>
        /// <param name="xyChart"></param>
        private void SetupAdditionalAxis(int axisUseCount, int axisOffsetValue, string scaleLabel, ref Axis axisName, ref XYChart xyChart)
        {
            try
            {
                if (xyChart != null)
                {
                    if (axisUseCount < 1)
                    {
                        xyChart.yAxis().setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                        xyChart.yAxis().setAutoScale(.1,.1,1);
                    }
                    else
                    {
                        axisName = xyChart.addAxis(Chart.Left, axisUseCount * axisOffsetValue);
                        axisName.setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                        xyChart.yAxis().setAutoScale(.1, .1, 1);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.SetupAdditionalAxis(int, int string, ref Axis, ref XYChart)\nInput ref XYChart was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetupAdditionalAxis(int, int string, ref Axis, ref XYChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Returns the index in the data associated with a specific input date for the trend data
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        private int FindGraphStartDateIndex(DateTime startDate, DateTime[] readingDateTimes)
        {
            int startDateIndex = 0;
            try
            {
                if (readingDateTimes != null)
                {
                    startDateIndex = Array.BinarySearch(readingDateTimes, startDate);
                    if (startDateIndex < 0)
                    {
                        startDateIndex = (~startDateIndex) - 1;
                    }
                    /// Hack inserted because the above formula failed for me on very tiny data sets.  Since it
                    /// worked fine otherwise, I assume there is an anomoly that needed smoothing over.
                    if (startDateIndex < 0)
                    {
                        startDateIndex = 0;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.FindGraphStartDateIndex(DateTime)\nthis.monitorData.ReadingDateTime was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.FindGraphStartDateIndex(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return startDateIndex;
        }

        /// <summary>
        /// Gets the end date index of the trend graph data
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int FindGraphEndDateIndex(DateTime endDate, DateTime[] readingDateTimes)
        {
            int endDateIndex = 0;
            try
            {
                int arrayLength;

                if (readingDateTimes != null)
                {
                    arrayLength = this.monitorData.readingDateTime.Length;
                    endDateIndex = Array.BinarySearch(readingDateTimes, endDate);
                    if (endDateIndex < 0)
                    {
                        if ((~endDateIndex) < arrayLength)
                        {
                            endDateIndex = ~endDateIndex;
                        }
                        else
                        {
                            endDateIndex = arrayLength - 1;
                        }
                    }
                    /// this is here just to keep the program from crashing, I don't know if this 
                    /// condition is even possible at this point in the method.  however, this above is based
                    /// on chartdirector sample code, not my own algorithm.
                    if ((endDateIndex < 0) || (endDateIndex > (arrayLength - 1)))
                    {
                        endDateIndex = arrayLength - 1;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.FindGraphEndDateIndex(DateTime)\nthis.monitorData.ReadingDateTime was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.FindGraphEndDateIndex(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return endDateIndex;
        }

        /// <summary>
        /// Sets the x-axis date limits for the trend chart
        /// </summary>
        private void SetTrendChartDateLimits()
        {
            try
            {
                int numberOfDates;
                int index = 0;
                double dataContentZoomLimit;
                List<DateTime> matrixSavedDates = new List<DateTime>();
                if (this.monitorData != null)
                {
                    if (this.monitorData.readingDateTime != null)
                    {
                        numberOfDates = this.monitorData.readingDateTime.Length;

                        if (numberOfDates > 0)
                        {
                            this.minimumDateOfDataSet = this.monitorData.readingDateTime[0];
                            this.dateRange = this.monitorData.readingDateTime[numberOfDates - 1].Subtract(this.minimumDateOfDataSet).TotalSeconds;

                            if (this.dateRange < Main_DataViewer.trendGraphDefaultDuration)
                            {
                                this.trendGraphCurrentDuration = this.dateRange;
                            }
                            else
                            {
                                this.trendGraphCurrentDuration = Main_DataViewer.trendGraphDefaultDuration;
                            }

                            this.allDateTimesAsDouble = Chart.CTime(this.monitorData.readingDateTime);

                            // Set the winChartViewer to reflect the visible and minimum duration

                            dataContentZoomLimit = this.trendGraphMinDuration / this.dateRange;
                            /// The value .02 means that the view port width will never be set to less than that value.
                            /// For proper scrolling this is a good limit.
                            trendWinChartViewer.ZoomInWidthLimit = Math.Max(dataContentZoomLimit, .02);
                            trendWinChartViewer.ViewPortWidth = 1.0;
                            trendWinChartViewer.ViewPortLeft = 0.0;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_DataViewer.SetTrendChartDateLimits()\nthis.monitorData.readingDateTime was null.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.SetTrendChartDateLimits()\nthis.monitorData was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetTrendChartDateLimits()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DrawTrendChart(WinChartViewer chartViewer)
        {
            try
            {
                if (chartViewer != null)
                {
                    int widthExtension = this.Width - 1024;
                    int heightExtension = this.Height - 768;

                    int chartWidth = this.trendChartMinimumWidth + widthExtension;
                    int chartHeight = this.trendChartMinimumHeight + heightExtension;
                    int graphWidth = this.trendGraphWidthMinimum + widthExtension;
                    int graphHeight = this.trendGraphHeightMinimum + heightExtension;

                    ///// save some values for the cursor location
                    //this.trendGraphXStart = chartViewer.Location.X;
                    //this.trendGraphYStart = chartViewer.Location.Y + this.trendGraphtOffsetY;
                    //this.trendGraphHeight = graphHeight;
                    //this.trendGraphWidth = graphWidth;
                    
                    if (this.totalDynamicsDataReadings > 0)
                    {
                        int yAxisUseCount = 0;
                        int axisOffsetValue = 50;

                        Axis milliWattsAxis = null;
                        Axis timesPerYearAxis = null;
                        Axis milliVoltsAxis = null;
                        Axis nanoCoulombsAxis = null;
                        Axis pulsesAxis = null;
                        Axis percentAxis = null;
                        Axis degreesCentigradeAxis = null;
                        Axis kilogramsPerMeterCubedAxis = null;
                        Axis kiloVoltsAxis = null;
                        Axis ampsAxis = null;
                        Axis unitlessAxis = null;
                        
                        /// at this point, we're just interested in getting the size of the graph set up
                        List<ScaleType> yScalesUsed = GetScalesBeingUsedForTrendChartList();
                        int additionalYscalesNeeded = yScalesUsed.Count - 1;
                        //int additionalYscalesNeeded = yScalesUsed.Count;
                        int graphXeasementForExtraScales = additionalYscalesNeeded * axisOffsetValue;
                        graphWidth -= graphXeasementForExtraScales;

                        /// save some values for the cursor location
                        this.trendGraphCurrentOffsetX = this.trendGraphOffsetX + graphXeasementForExtraScales;
                        this.trendGraphXStart = chartViewer.Location.X;
                        this.trendGraphYStart = chartViewer.Location.Y + this.trendGraphtOffsetY;
                        this.trendGraphHeight = graphHeight;
                        this.trendGraphWidth = graphWidth;

                        /// used to aggregate data when the viewport is too small to hold the amount of data in range
                        ArrayMath aggregator = null;

                        DateTime viewPortStartDate = minimumDateOfDataSet.AddSeconds(Math.Round(chartViewer.ViewPortLeft * dateRange));
                        DateTime viewPortEndDate = viewPortStartDate.AddSeconds(Math.Round(chartViewer.ViewPortWidth * dateRange));

                        double[] dynamicsDataViewPortTimeStampsAsDouble;

                        // Get the starting index of the array using the start date
                        int dynamicsDataStartIndex = FindGraphStartDateIndex(viewPortStartDate, this.monitorData.readingDateTime);

                        // Get the ending index of the array using the end date
                        int dynamicsDataEndIndex = FindGraphEndDateIndex(viewPortEndDate, this.monitorData.readingDateTime);

                        // Get the length
                        int numberOfDynamicsDataPointsBeingGraphed = dynamicsDataEndIndex - dynamicsDataStartIndex + 1;

                        // Now, we can just copy the visible data we need into the view port data series
                        DateTime[] dynamicsDataViewPortTimeStamps = new DateTime[numberOfDynamicsDataPointsBeingGraphed];
                        Array.Copy(this.monitorData.readingDateTime, dynamicsDataStartIndex, dynamicsDataViewPortTimeStamps, 0, numberOfDynamicsDataPointsBeingGraphed);

                        //if ((dynamicsDataViewPortTimeStamps.Length >= 600) && (this.cursorIsEnabled))
                        //{
                        //    //
                        //    // Zoomable chart with high zooming ratios often need to plot many thousands of 
                        //    // points when fully zoomed out. However, it is usually not needed to plot more
                        //    // data points than the resolution of the chart. Plotting too many points may cause
                        //    // the points and the lines to overlap. So rather than increasing resolution, this 
                        //    // reduces the clarity of the chart. So it is better to aggregate the data first if
                        //    // there are too many points.
                        //    //
                        //    // In our current example, the chart only has 520 pixels in width and is using a 2
                        //    // pixel line width. So if there are more than 520 data points, we aggregate the 
                        //    // data using the ChartDirector aggregation utility method.
                        //    //
                        //    // If in your real application, you do not have too many data points, you may 
                        //    // remove the following code altogether.
                        //    //

                        //    // Set up an aggregator to aggregate the data based on regular sized slots
                        //    aggregator = new ArrayMath(dynamicsDataViewPortTimeStamps);

                        //    aggregator.selectRegularSpacing(dynamicsDataViewPortTimeStamps.Length / 300);

                        //    // For the timestamps, take the first timestamp on each slot
                        //    dynamicsDataViewPortTimeStamps = aggregator.aggregate(dynamicsDataViewPortTimeStamps, Chart.AggregateFirst);
                        //}

                        /// get the double version of the time stamps in case we're adding points to the scatter
                        /// layer or computing some trend lines, both of which require a double for the X argument
                        dynamicsDataViewPortTimeStampsAsDouble = Chart.CTime(dynamicsDataViewPortTimeStamps);

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 1 - Configure overall chart appearance. 
                        ///////////////////////////////////////////////////////////////////////////////////////

                        // Create an XYChart object 680 x 450 pixels in size, with pale blue (0xf0f0ff) 
                        // background, black (000000) border, 1 pixel raised effect, and with a rounded frame.
                        XYChart xyChart = new XYChart(chartWidth, chartHeight, 0xf0f0ff, 0, 1);
                        xyChart.setRoundedFrame(Chart.CColor(BackColor));

                        // Set the plotarea at (55, 100) and of size 600 x 300 pixels. Use white (ffffff) 
                        // background. Enable both horizontal and vertical grids by setting their colors to 
                        // grey (cccccc). Set clipping mode to clip the data lines to the plot area.
                        xyChart.setPlotArea(this.trendGraphCurrentOffsetX, this.trendGraphtOffsetY,
                            graphWidth, graphHeight, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
                        xyChart.setClipping();

                        // Add a top title to the chart using 15 pts Times New Roman Bold Italic font, with a 
                        // light blue (ccccff) background, black (000000) border, and a glass like raised effect.
                        xyChart.addTitle("Trend", "Times New Roman Bold Italic", 15).setBackground(0xccccff, 0x0, Chart.glassEffect());

                        // Add a bottom title to the chart to show the date range of the axis, with a light blue 
                        // (ccccff) background.
                        xyChart.addTitle2(Chart.Bottom, "From <*font=Arial Bold Italic*>" +
                                                        xyChart.formatValue(viewPortStartDate, "{value|mmm dd, yyyy}") +
                                                        "<*/font*> to <*font=Arial Bold Italic*>" +
                                                        xyChart.formatValue(viewPortEndDate, "{value|mmm dd, yyyy}") +
                                                        "<*/font*> (Duration <*font=Arial Bold Italic*>" +
                                                        Math.Round(viewPortEndDate.Subtract(viewPortStartDate).TotalSeconds / 86400.0) +
                                                        "<*/font*> days)", "Arial Italic", 10).setBackground(0xccccff);

                        // Add a legend box at the top of the plot area with 9pts Arial Bold font with flow layout. 
                        xyChart.addLegend(50, 33, false, "Arial Bold", 9).setBackground(Chart.Transparent, Chart.Transparent);

                        // Set axes width to 2 pixels
                        xyChart.yAxis().setWidth(2);
                        xyChart.xAxis().setWidth(2);

                        /// Now set up any addition y-axis needed
                        
                        if (yScalesUsed.Contains(ScaleType.MilliWatts))
                        {
                            // this one is different from all the following ones because by default if the
                            // axis is present, it will be the "standard" y axis.
                            xyChart.yAxis().setTitle(ScaleLabels.MilliWatts).setAlignment(Chart.TopLeft2);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.TimesPerYear))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.TimesPerYear, ref timesPerYearAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.MilliVolts))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.MilliVolts, ref milliVoltsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.NanoCoulombs))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.NanoCoulombs, ref nanoCoulombsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.PulsesPerCycle))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.PulsesPerCycle, ref pulsesAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        else if (yScalesUsed.Contains(ScaleType.PulsesPerSecond))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.PulsesPerSecond, ref pulsesAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Percent))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Percent, ref percentAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.DegreesCentigrade))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.DegreesCentigrade, ref degreesCentigradeAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.KilogramsPerMeterCubed))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.KilogramsPerMeterCubed, ref kilogramsPerMeterCubedAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.KiloVolts))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.KiloVolts, ref kiloVoltsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Amps))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Amps, ref ampsAxis, ref xyChart);
                            yAxisUseCount++;
                        }
                        if (yScalesUsed.Contains(ScaleType.Unitless))
                        {
                            SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, ScaleLabels.Unitless, ref unitlessAxis, ref xyChart);
                            yAxisUseCount++;
                        }

                        // Add a title to the y-axis
                        // xyChart.yAxis().setTitle("Values", "Arial Bold", 9);

                        //Layer lineLayer = xyChart.addLineLayer2();
                        //lineLayer.setLineWidth(1);

                        //lineLayer.setXData(dynamicsDataViewPortTimeStamps);

                        // add data

                        #region Checkboxes - check the toggle state and add data if they are toggled on

                        #region Dynamics

                        if (this.humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.humidity, this.humidityScaleFactor, this.humidityOperation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.humidityRadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref percentAxis, humidityColor, humiditySymbol);
                        }

                        if (this.moistureContentRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.moisture, this.moistureScaleFactor, this.moistureOperation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.moistureContentRadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref kilogramsPerMeterCubedAxis, moistureColor, moistureSymbol);
                        }

                        if (this.temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.temp1, this.temp1ScaleFactor, this.temp1Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.temp1RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis, temp1Color, temp1Symbol);
                        }

                        if (this.temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.temp2, this.temp2ScaleFactor, this.temp2Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.temp2RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis, temp2Color, temp2Symbol);
                        }

                        if (this.temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.temp3, this.temp3ScaleFactor, this.temp3Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.temp3RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis, temp3Color, temp3Symbol);
                        }

                        if (this.temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.temp4, this.temp4ScaleFactor, this.temp4Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.temp4RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref degreesCentigradeAxis, temp4Color, temp4Symbol);
                        }

                        if (this.loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.loadCurrent1, this.loadCurrent1ScaleFactor, this.loadCurrent1Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.loadCurrent1RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref ampsAxis, loadCurrent1Color, loadCurrent1Symbol);
                        }

                        if (this.loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.loadCurrent2, this.loadCurrent2ScaleFactor, this.loadCurrent2Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.loadCurrent2RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref ampsAxis, loadCurrent2Color, loadCurrent2Symbol);
                        }

                        if (this.loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.loadCurrent3, this.loadCurrent3ScaleFactor, this.loadCurrent3Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.loadCurrent3RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref ampsAxis, loadCurrent3Color, loadCurrent3Symbol);
                        }

                        if (this.voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.voltage1, this.voltage1ScaleFactor, this.voltage1Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.voltage1RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis, voltage1Color, voltage1Symbol);
                        }

                        if (this.voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.voltage2, this.voltage2ScaleFactor, this.voltage2Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.voltage2RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis, voltage2Color, voltage2Symbol);
                        }

                        if (this.voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.voltage3, this.voltage3ScaleFactor, this.voltage3Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.voltage3RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref kiloVoltsAxis, voltage3Color, voltage3Symbol);
                        }

                        if (this.analog1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog1, this.analog1ScaleFactor, this.analog1Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog1RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog1Color, analog1Symbol);
                        }

                        if (this.analog2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog2, this.analog2ScaleFactor, this.analog2Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog2RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog2Color, analog2Symbol);
                        }

                        if (this.analog3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog3, this.analog3ScaleFactor, this.analog3Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog3RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog3Color, analog3Symbol);
                        }

                        if (this.analog4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog4, this.analog4ScaleFactor, this.analog4Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog4RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog4Color, analog4Symbol);
                        }

                        if (this.analog5RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog5, this.analog5ScaleFactor, this.analog5Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog5RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog5Color, analog5Symbol);
                        }

                        if (this.analog6RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.analog6, this.analog6ScaleFactor, this.analog6Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.analog6RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, analog6Color, analog6Symbol);
                        }

                        if (this.vibration1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.vibration1, this.vibration1ScaleFactor, this.vibration1Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.vibration1RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, vibration1Color, vibration1Symbol);
                        }

                        if (this.vibration2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.vibration2, this.vibration2ScaleFactor, this.vibration2Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.vibration2RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, vibration2Color, vibration2Symbol);
                        }

                        if (this.vibration3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.vibration3, this.vibration3ScaleFactor, this.vibration3Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.vibration3RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, vibration3Color, vibration3Symbol);
                        }

                        if (this.vibration4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            GraphDataForOneDataType(ScaleDynamicsValues(monitorData.vibration4, this.vibration4ScaleFactor, this.vibration4Operation), dynamicsDataViewPortTimeStamps, dynamicsDataViewPortTimeStampsAsDouble,
                                this.vibration4RadCheckBox.Text.Trim(), aggregator, dynamicsDataStartIndex, numberOfDynamicsDataPointsBeingGraphed, ref xyChart, ref unitlessAxis, vibration4Color, vibration4Symbol);
                        }

                        /// winding hotspot data

                        if (this.totalWindingHotSpotDataReadings > 0)
                        {
                            int windingHotSpotDataStartIndex = FindGraphStartDateIndex(viewPortStartDate, this.windingHotSpotData.readingDateTime);
                            int windingHotSpotDataEndIndex = FindGraphEndDateIndex(viewPortEndDate, this.windingHotSpotData.readingDateTime);
                            int numberOfWindingHotSpotDataPointsBeingGraphed = windingHotSpotDataEndIndex - windingHotSpotDataStartIndex + 1;

                            DateTime[] windingHotSpotDataViewPortTimeStamps = new DateTime[numberOfWindingHotSpotDataPointsBeingGraphed];
                            Array.Copy(this.monitorData.readingDateTime, windingHotSpotDataStartIndex, windingHotSpotDataViewPortTimeStamps, 0, numberOfWindingHotSpotDataPointsBeingGraphed);

                            double[] windingHotSpotDataViewPortTimeStampsAsDouble = Chart.CTime(windingHotSpotDataViewPortTimeStamps);

                            if (this.WHS1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Whs1TempData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.WHS1RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref degreesCentigradeAxis, highVoltageTempPhaseAColor, highVoltageTempPhaseASymbol);
                            }

                            if (this.WHS2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Whs2TempData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.WHS2RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref degreesCentigradeAxis, highVoltageTempPhaseBColor, highVoltageTempPhaseBSymbol);
                            }

                            if (this.WHS3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Whs3TempData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.WHS3RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref degreesCentigradeAxis, highVoltageTempPhaseCColor, highVoltageTempPhaseCSymbol);
                            }

                            if (this.agingFac1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AgingFactor1Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.agingFac1RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, tertiaryVoltageTempPhaseAColor, tertiaryVoltageTempPhaseASymbol);
                            }

                            if (this.AgingFac2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AgingFactor2Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.AgingFac2RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, tertiaryVoltageTempPhaseBColor, tertiaryVoltageTempPhaseBSymbol);
                            }

                            if (this.AgingFac3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AgingFactor3Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.AgingFac3RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, tertiaryVoltageTempPhaseCColor, tertiaryVoltageTempPhaseCSymbol);
                            }

                            if (this.AccumAge1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AccumAge1Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.AccumAge1RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, temp3PhaseAColor, temp3PhaseASymbol);
                            }

                            if (this.AccumAge2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AccumAge2Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.AccumAge2RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, temp3PhaseBColor, temp3PhaseBSymbol);
                            }

                            if (this.AccumAge3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.AccumAge3Data, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.AccumAge3RadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, temp3PhaseCColor, temp3PhaseCSymbol);
                            }

                            if (this.TopOilTempRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.TopOilTempData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.TopOilTempRadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref degreesCentigradeAxis, maxWindingTempPhaseAColor, maxWindingTempPhaseASymbol);
                            }

                            if (this.Fan1_StartsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Fan1StartsData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.Fan1_StartsRadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, maxWindingTempPhaseBColor, maxWindingTempPhaseBSymbol);
                            }

                            if (this.Fan2_StartsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Fan2StartsData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.Fan2_StartsRadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, maxWindingTempPhaseCColor, maxWindingTempPhaseCSymbol);
                            }

                            if (this.Fan1_RunTimeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Fan1RunTimeData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.Fan1_RunTimeRadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, agingFactorColor, agingFactorSymbol);
                            }

                            if (this.Fan2_RunTimeRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                GraphDataForOneDataType(ScaleDynamicsValues(windingHotSpotData.Fan2RunTimeData, 0, "none"), windingHotSpotDataViewPortTimeStamps, windingHotSpotDataViewPortTimeStampsAsDouble,
                                    this.Fan2_RunTimeRadCheckBox.Text.Trim(), aggregator, windingHotSpotDataStartIndex, numberOfWindingHotSpotDataPointsBeingGraphed, ref xyChart,
                                    ref unitlessAxis, accumulatedAgingColor, accumulatedAgingSymbol);
                            }
                        }

                        #endregion

                        #endregion

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 3 - Set up x-axis scale
                        ///////////////////////////////////////////////////////////////////////////////////////

                        // Set x-axis date scale to the view port date range. 
                        xyChart.xAxis().setDateScale(viewPortStartDate, viewPortEndDate);

                        //
                        // In the current demo, the x-axis range can be from a few years to a few days. We can 
                        // let ChartDirector auto-determine the date/time format. However, for more beautiful 
                        // formatting, we set up several label formats to be applied at different conditions. 
                        //

                        // If all ticks are yearly aligned, then we use "yyyy" as the label format.
                        xyChart.xAxis().setFormatCondition("align", 360 * 86400);
                        xyChart.xAxis().setLabelFormat("{value|yyyy}");

                        // If all ticks are monthly aligned, then we use "mmm yyyy" in bold font as the first 
                        // label of a year, and "mmm" for other labels.
                        xyChart.xAxis().setFormatCondition("align", 30 * 86400);
                        xyChart.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
                            Chart.AllPassFilter(), "{value|mmm}");

                        // If all ticks are daily algined, then we use "mmm dd<*br*>yyyy" in bold font as the 
                        // first label of a year, and "mmm dd" in bold font as the first label of a month, and
                        // "dd" for other labels.
                        xyChart.xAxis().setFormatCondition("align", 86400);
                        xyChart.xAxis().setMultiFormat(
                            Chart.StartOfYearFilter(), "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}",
                            Chart.StartOfMonthFilter(), "<*font=bold*>{value|mmm dd}");
                        xyChart.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}");

                        // For all other cases (sub-daily ticks), use "hh:nn<*br*>mmm dd" for the first label of
                        // a day, and "hh:nn" for other labels.
                        xyChart.xAxis().setFormatCondition("else");
                        xyChart.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
                            Chart.AllPassFilter(), "{value|hh:nn}");

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 4 - Set up y-axis scale
                        ///////////////////////////////////////////////////////////////////////////////////////

                        if ((chartViewer.ZoomDirection == WinChartDirection.Horizontal) || (trendChartMinYValue == trendChartMaxYValue))
                        {
                            // y-axis is auto-scaled - save the chosen y-axis scaled to support xy-zoom mode
                            xyChart.layout();
                            trendChartMinYValue = xyChart.yAxis().getMinValue();
                            trendChartMaxYValue = xyChart.yAxis().getMaxValue();
                        }
                        else
                        {
                            // xy-zoom mode - compute the actual axis scale in the view port 
                            double axisLowerLimit = trendChartMaxYValue - (trendChartMaxYValue - trendChartMinYValue) * (chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                            double axisUpperLimit = trendChartMaxYValue - (trendChartMaxYValue - trendChartMinYValue) * chartViewer.ViewPortTop;
                            // *** use the following formula if you are using a log scale axis ***
                            // double axisLowerLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop + chartViewer.ViewPortHeight);
                            // double axisUpperLimit = trendChartMaxYValue * Math.Pow(trendChartMinYValue / trendChartMaxYValue, chartViewer.ViewPortTop);

                            // use the zoomed-in scale
                            xyChart.yAxis().setLinearScale(axisLowerLimit, axisUpperLimit);
                            xyChart.yAxis().setRounding(false, false);
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////
                        // Step 5 - Display the chart
                        ///////////////////////////////////////////////////////////////////////////////////////

                        trendXYChart = null;

                        trendXYChart = xyChart;

                        chartViewer.Chart = xyChart;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.DrawTrendChart(WinChartViewer)\nInput WinChartViewer was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.DrawTrendChart(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Adds one data set for one channel to the trend graph.  Draws the data to it's own layer, and assoicates that layer with the displayAxis passed in.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dateTime"></param>
        /// <param name="dateTimeAsDouble"></param>
        /// <param name="dataLabel"></param>
        /// <param name="aggregator"></param>
        /// <param name="startIndex"></param>
        /// <param name="numberOfPointsToCopy"></param>
        /// <param name="xyChart"></param>
        /// <param name="displayAxis"></param>
        /// <param name="color"></param>
        /// <param name="symbol"></param>
        private void GraphDataForOneDataType(double[] data, DateTime[] dateTime, double[] dateTimeAsDouble, string dataLabel, ArrayMath aggregator,
            int startIndex, int numberOfPointsToCopy,
            ref XYChart xyChart, ref Axis displayAxis, int color, int symbol)
        {
            try
            {
                // We will not test the ArrayMath input for a null value, because we use a null value for that input to determine if we are going to 
                /// graph the moving average.  We also use a null value for the Axis input to test if we are going to declare a different x axis.
                int lineWidth = 2;
                double[] graphingDataArray = new double[numberOfPointsToCopy];

                if (data != null)
                {
                    if (data.Length >= (startIndex + numberOfPointsToCopy))
                    {
                        if (dateTime != null)
                        {
                            if (dateTimeAsDouble != null)
                            {
                                if (xyChart != null)
                                {
                                    Array.Copy(data, startIndex, graphingDataArray, 0, numberOfPointsToCopy);

                                    /// we use a null test to determine whether or not an aggregator was created, since we only
                                    /// create one if we are going to aggregate.  this saves passing in a boolean or other flag.
                                    if (aggregator != null)
                                    {
                                        graphingDataArray = aggregator.aggregate(graphingDataArray, Chart.AggregateAvg);
                                    }

                                    if (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                    {
                                        graphingDataArray = NormalizeData(graphingDataArray);
                                    }

                                    if ((this.showTrendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off) &&
                                        (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                    {
                                        LineLayer lineLayer = xyChart.addLineLayer();
                                        lineLayer.setLineWidth(lineWidth);
                                        lineLayer.setXData(dateTime);
                                        lineLayer.addDataSet(graphingDataArray, color, dataLabel).setDataSymbol(symbol, 8, color, color);
                                        if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                        {
                                            lineLayer.setUseYAxis(displayAxis);
                                        }
                                    }
                                    else
                                    {
                                        if (this.showTrendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        {
                                            ScatterLayer scatterLayer = xyChart.addScatterLayer(dateTimeAsDouble, graphingDataArray, dataLabel, symbol, 8, color, color);
                                            if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                            {
                                                scatterLayer.setUseYAxis(displayAxis);
                                            }
                                            if (this.linearTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                            {
                                                TrendLayer linearTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, color);
                                                if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                                {
                                                    linearTrendLayer.setUseYAxis(displayAxis);
                                                }
                                            }
                                            else if (this.exponentialTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                            {
                                                TrendLayer exponentialTrendLayer = xyChart.addTrendLayer(dateTimeAsDouble, graphingDataArray, color);
                                                exponentialTrendLayer.setRegressionType(Chart.ExponentialRegression);
                                                exponentialTrendLayer.setHTMLImageMap("{disable}");
                                                if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                                {
                                                    exponentialTrendLayer.setUseYAxis(displayAxis);
                                                }
                                            }
                                        }
                                        else if (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                                        {
                                            double[] movingAverageData = new ArrayMath(graphingDataArray).movAvg((int)this.movingAverageRadSpinEditor.Value).result();
                                            LineLayer movingAverageLayer = xyChart.addLineLayer();
                                            movingAverageLayer.setLineWidth(lineWidth);
                                            movingAverageLayer.addDataSet(movingAverageData, color, dataLabel + " Mov Avg");
                                            movingAverageLayer.setXData(dateTime);
                                            if ((displayAxis != null) && (this.normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off))
                                            {
                                                movingAverageLayer.setUseYAxis(displayAxis);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nInput XYChart was null.";
                                    LogMessage.LogError(errorMessage);
                                    #if DEBUG
                                    MessageBox.Show(errorMessage);
                                    #endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nInput double[] was null.";
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nInput DateTime[] was null.";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nInput double[] had too few elements.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GraphDataForOneDataType(double[], DateTime[], double[], string, ArrayMath, int, int, ref XYChart, ref Axis, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Returns a normalized version of the inputData
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private double[] NormalizeData(double[] inputData)
        {
            double[] normalizedData = null;
            try
            {
                if (inputData != null)
                {
                    ArrayMath arrayMath = new ArrayMath(inputData);
                    double scalefactor = 1;
                    double currentValue = 0;
                    int pointCount = inputData.Length;

                    /// I first used the ArrayMath.max() function call to do this, but I just realized that his will fail
                    /// for an array with negative entries which have a high absolute value.
                    for (int i = 0; i < pointCount; i++)
                    {
                        currentValue = Math.Abs(inputData[i]);
                        if (scalefactor < currentValue)
                        {
                            scalefactor = currentValue;
                        }
                    }

                    if (scalefactor == 0) // avoid divide by 0 error
                    {
                        scalefactor = 1;
                    }
                    arrayMath.div2(scalefactor);
                    normalizedData = arrayMath.result();
                }
                else
                {
                    string errorMessage = "Error in Main_DataViewer.NormalizeData(double[])\nInput double[] was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.NormalizeData(double[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return normalizedData;
        }

        /// <summary>
        /// Update the image map used on the chart.
        /// </summary>
        private void UpdateTrendImageMap(WinChartViewer viewer)
        {
            try
            {
                // Include tool tip for the chart
                if (viewer != null)
                {
                    if (viewer.Chart != null)
                    {
                        if (viewer.ImageMap == null)
                        {
                            viewer.ImageMap = viewer.Chart.getHTMLImageMap("clickable", "",
                                "title='[{dataSetName}] {x|mmm dd, yyyy hh:nn:ss}  val = {value|4}'");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.UpdateTrendImageMap(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Returns the index into the allDateTimesAsDouble array for the dateTimeValueAsDouble input
        /// </summary>
        /// <param name="dateTimeValueAsDouble"></param>
        /// <returns></returns>
        private int FindAllDateTimeAsDoubleIndex(double dateTimeValueAsDouble)
        {
            int index = -1;
            try
            {
                int arrayLength = this.allDateTimesAsDouble.Length;
                index = Array.BinarySearch(this.allDateTimesAsDouble, dateTimeValueAsDouble);
                if (index < 0)
                {
                    string errorMessage = "Error in PDM_DataViewer.FindAllDateTimeAsDoubleIndex(double)\nCould not find the date in the data.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                    index = 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.FindAllDateTimeAsDoubleIndex(double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return index;
        }

        private void DynamicsRadGroupBoxAssignContextMenuItems()
        {
            try
            {
                RadMenuItem menuItem;
                // we can eventually switch on the monitorTypeString if we need to customize
                // the context menu for specific monitor types
                this.dynamicsRadGroupBoxRadContextMenu.Items.Clear();

                menuItem = new RadMenuItem();
                menuItem.Click += dyamicsRadGroupBoxOpenEditMenu_Click;
                menuItem.Text = openDynamicsEditorContextMenuText;
                this.dynamicsRadGroupBoxRadContextMenu.Items.Add(menuItem);
                //menuItem = new RadMenuItem();
                //menuItem.Click += channelSelectRadGroupBoxUnselectActive_Click;
                //menuItem.Text = "Unselect all active channels";
                //channelSelectRadGroupBoxRadContextMenuStrip.Items.Add(menuItem);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.DynamicsRadGroupBoxAssignContextMenuItems()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void dyamicsRadGroupBoxOpenEditMenu_Click(object sender, EventArgs e)
        {
            try
            {
                using (DynamicsNamesEditor namesEditor = new DynamicsNamesEditor(this.programBrand, this.programType, DataViewerType.Main, this.humidityRadCheckBox.Text, this.moistureContentRadCheckBox.Text,
                    this.temp1RadCheckBox.Text, this.temp2RadCheckBox.Text, this.temp3RadCheckBox.Text, this.temp4RadCheckBox.Text,
                    this.loadCurrent1RadCheckBox.Text, this.loadCurrent2RadCheckBox.Text, this.loadCurrent3RadCheckBox.Text,
                    this.voltage1RadCheckBox.Text, this.voltage2RadCheckBox.Text, this.voltage3RadCheckBox.Text,
                    this.analog1RadCheckBox.Text, this.analog2RadCheckBox.Text, this.analog3RadCheckBox.Text, this.analog4RadCheckBox.Text, this.analog5RadCheckBox.Text, this.analog6RadCheckBox.Text,
                    this.vibration1RadCheckBox.Text, this.vibration2RadCheckBox.Text, this.vibration3RadCheckBox.Text, this.vibration3RadCheckBox.Text,
                    this.humidityScaleFactor, this.moistureScaleFactor,
                    this.temp1ScaleFactor, this.temp2ScaleFactor, this.temp3ScaleFactor, this.temp4ScaleFactor,
                    this.loadCurrent1ScaleFactor, this.loadCurrent2ScaleFactor, this.loadCurrent3ScaleFactor,
                    this.voltage1ScaleFactor, this.voltage2ScaleFactor, this.voltage3ScaleFactor,
                    this.analog1ScaleFactor, this.analog2ScaleFactor, this.analog3ScaleFactor, this.analog4ScaleFactor, this.analog5ScaleFactor, this.analog6ScaleFactor,
                    this.vibration1ScaleFactor, this.vibration2ScaleFactor, this.vibration2ScaleFactor, this.vibration2ScaleFactor,
                    this.humidityOperation, this.moistureOperation,
                    this.temp1Operation, this.temp2Operation, this.temp3Operation, this.temp4Operation,
                    this.loadCurrent1Operation, this.loadCurrent2Operation, this.loadCurrent3Operation,
                    this.voltage1Operation, this.voltage2Operation, this.voltage3Operation,
                    this.analog1Operation, this.analog2Operation, this.analog3Operation, this.analog4Operation, this.analog5Operation, this.analog6Operation,
                    this.vibration1Operation, this.vibration2Operation, this.vibration3Operation, this.vibration4Operation)) 
                {
                    namesEditor.ShowDialog();
                    namesEditor.Hide();
                    if (namesEditor.ResetValues)
                    {
                        this.humidityRadCheckBox.Text = namesEditor.HumidityNewText;
                        this.moistureContentRadCheckBox.Text = namesEditor.MoistureNewText;
                        this.temp1RadCheckBox.Text = namesEditor.Temp1NewText;
                        this.temp2RadCheckBox.Text = namesEditor.Temp2NewText;
                        this.temp3RadCheckBox.Text = namesEditor.Temp3NewText;
                        this.temp4RadCheckBox.Text = namesEditor.Temp4NewText;
                        this.loadCurrent1RadCheckBox.Text = namesEditor.LoadCurrent1NewText;
                        this.loadCurrent2RadCheckBox.Text = namesEditor.LoadCurrent2NewText;
                        this.loadCurrent3RadCheckBox.Text = namesEditor.LoadCurrent3NewText;
                        this.voltage1RadCheckBox.Text = namesEditor.Voltage1NewText;
                        this.voltage2RadCheckBox.Text = namesEditor.Voltage2NewText;
                        this.voltage3RadCheckBox.Text = namesEditor.Voltage3NewText;
                        this.analog1RadCheckBox.Text = namesEditor.Analog1NewText;
                        this.analog2RadCheckBox.Text = namesEditor.Analog2NewText;
                        this.analog3RadCheckBox.Text = namesEditor.Analog3NewText;
                        this.analog4RadCheckBox.Text = namesEditor.Analog4NewText;
                        this.analog5RadCheckBox.Text = namesEditor.Analog5NewText;
                        this.analog6RadCheckBox.Text = namesEditor.Analog6NewText;
                        this.vibration1RadCheckBox.Text = namesEditor.Vibration1NewText;
                        this.vibration2RadCheckBox.Text = namesEditor.Vibration2NewText;
                        this.vibration3RadCheckBox.Text = namesEditor.Vibration3NewText;

                        this.humidityScaleFactor = namesEditor.HumidityNewScaleFactor;
                        this.moistureScaleFactor = namesEditor.MoistureNewScaleFactor;
                        this.temp1ScaleFactor = namesEditor.Temp1NewScaleFactor;
                        this.temp2ScaleFactor = namesEditor.Temp2NewScaleFactor;
                        this.temp3ScaleFactor = namesEditor.Temp3NewScaleFactor;
                        this.temp4ScaleFactor = namesEditor.Temp4NewScaleFactor;
                        this.loadCurrent1ScaleFactor = namesEditor.LoadCurrent1NewScaleFactor;
                        this.loadCurrent2ScaleFactor = namesEditor.LoadCurrent2NewScaleFactor;
                        this.loadCurrent3ScaleFactor = namesEditor.LoadCurrent3NewScaleFactor;
                        this.voltage1ScaleFactor = namesEditor.Voltage1NewScaleFactor;
                        this.voltage2ScaleFactor = namesEditor.Voltage2NewScaleFactor;
                        this.voltage3ScaleFactor = namesEditor.Voltage3NewScaleFactor;
                        this.analog1ScaleFactor = namesEditor.Analog1NewScaleFactor;
                        this.analog2ScaleFactor = namesEditor.Analog2NewScaleFactor;
                        this.analog3ScaleFactor = namesEditor.Analog3NewScaleFactor;
                        this.analog4ScaleFactor = namesEditor.Analog4NewScaleFactor;
                        this.analog5ScaleFactor = namesEditor.Analog5NewScaleFactor;
                        this.analog6ScaleFactor = namesEditor.Analog6NewScaleFactor;
                        this.vibration1ScaleFactor = namesEditor.Vibration1NewScaleFactor;
                        this.vibration2ScaleFactor = namesEditor.Vibration2NewScaleFactor;
                        this.vibration3ScaleFactor = namesEditor.Vibration3NewScaleFactor;
                        this.vibration4ScaleFactor = namesEditor.Vibration4NewScaleFactor;

                        this.humidityOperation = namesEditor.HumidityNewOperation;
                        this.moistureOperation = namesEditor.MoistureNewOperation;
                        this.temp2Operation = namesEditor.Temp1NewOperation;
                        this.temp2Operation = namesEditor.Temp2NewOperation;
                        this.temp3Operation = namesEditor.Temp3NewOperation;
                        this.temp4Operation = namesEditor.Temp4NewOperation;
                        this.loadCurrent1Operation = namesEditor.LoadCurrent1NewOperation;
                        this.loadCurrent2Operation = namesEditor.LoadCurrent2NewOperation;
                        this.loadCurrent3Operation = namesEditor.LoadCurrent3NewOperation;
                        this.voltage1Operation = namesEditor.Voltage1NewOperation;
                        this.voltage2Operation = namesEditor.Voltage2NewOperation;
                        this.voltage3Operation = namesEditor.Voltage3NewOperation;
                        this.analog1Operation = namesEditor.Analog1NewOperation;
                        this.analog2Operation = namesEditor.Analog2NewOperation;
                        this.analog3Operation = namesEditor.Analog3NewOperation;
                        this.analog4Operation = namesEditor.Analog4NewOperation;
                        this.analog5Operation = namesEditor.Analog5NewOperation;
                        this.analog6Operation = namesEditor.Analog6NewOperation;
                        this.vibration1Operation = namesEditor.Vibration1NewOperation;
                        this.vibration2Operation = namesEditor.Vibration2NewOperation;
                        this.vibration3Operation = namesEditor.Vibration3NewOperation;
                        this.vibration4Operation = namesEditor.Vibration4NewOperation;

                        InitializeDynamicsGridViewColumnHeaderText();
                        SetDynamicsGridViewColumnHeaderText();

                        this.trendChartNeedsUpdating = true;
                        DrawTrendChart(this.trendWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.dyamicsRadGroupBoxOpenEditMenu_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
    }
}