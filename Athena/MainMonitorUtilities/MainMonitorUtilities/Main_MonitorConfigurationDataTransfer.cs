﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;

namespace MainMonitorUtilities
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {

        private List<string> dataTransferMasterDeviceRegisterNames;

        // private SortedDictionary<string, Monitor> monitorsByUniqueMonitorName;

        private SortedDictionary<string, MonitorInfo> monitorsByUniqueMonitorName;

        private List<string> monitorUniqueNames;

        // private List<MonitorInfo> monitorsWithUniqueAssignedNames;

        private List<string> admDynamicsSlaveRegisterNames;
        private List<string> bhmDynamicsSlaveRegisterNames;
        private List<string> pdmDynamicsSlaveRegisterNames;

        // private List<TransferSetupEntry> currentTransferSetupEntries;

        private static string noValuesAvailableDataTransferText = "No Values Available";
        private static string noValueSetDataTransferText = "No Value Set";
        private static string noneDataTransferText = "None";

        private static string register18DataTransferText = "Incorrect 1";
        private static string register20DataTransferText = "Incorrect 2";

        private static string vibration1DataTransferText = "Vibration 1";
        private static string vibration2DataTransferText = "Vibration 2";
        private static string vibration3DataTransferText = "Vibration 3";
        private static string vibration4DataTransferText = "Vibration 4";
        private static string analogIn1DataTransferText = "Analog In 1";
        private static string analogIn2DataTransferText = "Analog In 2";
        private static string analogIn3DataTransferText = "Analog In 3";
        private static string analogIn4DataTransferText = "Analog In 4";
        private static string analogIn5DataTransferText = "Analog In 5";
        private static string analogIn6DataTransferText = "Analog In 6";
        private static string loadCurrent1DataTransferText = "Load Current 1";
        private static string loadCurrent2DataTransferText = "Load Current 2";
        private static string loadCurrent3DataTransferText = "Load Current 3";
        private static string voltage1DataTransferText = "Voltage 1";
        private static string voltage2DataTransferText = "Voltage 2";
        private static string voltage3DataTransferText = "Voltage 3";
        private static string humidityDataTransferText = "Humidity";
        private static string temp1DataTransferText = "Temp 1";
        private static string correlatingTempDataTransferText = "Correlating Temp";
        private static string temp2DataTransferText = "Temp 2";
        private static string temp3DataTransferText = "Temp 3";
        private static string temp4DataTransferText = "Temp 4";
        private static string temp5DataTransferText = "Temp 5";
        private static string temp6DataTransferText = "Temp 6";
        private static string temp7DataTransferText = "Temp 7";
        private static string chassisTempDataTransferText = "Chassis Temp";

        private static string transferSetupNumberDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Transfer<br>Setup Number</html>";
        private static string enableChannelDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string masterDeviceRegisterDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Master Device<br>Register</html>";
        private static string slaveDeviceNameDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Slave Device<br>Name</html>";
        private static string slaveDeviceModbusAddressDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Slave Device<br>Modbus Address<br>(read only)</html>";
        private static string slaveDeviceRegisterDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Slave Device<br>Register</html>";
        private static string slopeDataTransferGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Slope</html>";

        private static string incorrectDataTransferTabEntryMessage = "Incorrect entry in Data Transfer tab:\nSince transfer is enabled, all values must be set.\nPlease correct the entry or disable the transfer.";

        private static string dataTransferContainsDepricatedDestinationRegister = "The data transfer contains at least one incorrect destination register\nThe value can be changed, but cannot be selected by the user";

        private static string undefinedDeviceNameText = "Undefined";
        private static string undefinedRegisterNameText = "Register";

        private static string undefinedDeviceInDataTransferGridText = "A device that was not defined in the system configuration tab is enabled in the data transfer tab\nPlease correct the entry or disable the transfer.";

        private int slaveDeviceTypeWhenUnknown = 11111;

        // All of the data objects starting with "undefined" have been created to handle the special
        // case in which the configuration loaded from the device has slave devices referenced in the 
        // data transfer tab which are not specified in the system configuration tab.  Such configs
        // cannot be created in Athena and written to the device.
        //private Dictionary<string, int> undefinedMonitorsKeyedByName;
        //private Dictionary<int, string> undefinedMonitorsKeyedByModbusAddress;
        private Dictionary<string, int> undefinedDestinationRegistersKeyedByName;
        private Dictionary<int, string> undefinedDestinationRegistersKeyedByRegisterNumber;
        //private List<string> undefinedMonitorsNames;

        private List<string> undefinedMonitorsDynamicsSlaveRegisterNames;

        private void SelectDataTransferTab()
        {
            configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
        }

        private void EnableDataTransferTabConfigurationEditObjects()
        {
            dataTransferRadGridView.ReadOnly = false;
        }

        private void DisableDataTransferTabConfigurationEditObjects()
        {
            dataTransferRadGridView.ReadOnly = true;
        }

        private void InitializationsForDataTransferVariables()
        {
            //monitorsByUniqueMonitorName = new SortedDictionary<string, Monitor>();
            monitorsByUniqueMonitorName = new SortedDictionary<string, MonitorInfo>();

            monitorUniqueNames = new List<string>();

            //this.undefinedMonitorsKeyedByName = new Dictionary<string, int>();
            //this.undefinedMonitorsKeyedByModbusAddress = new Dictionary<int, string>();
            this.undefinedDestinationRegistersKeyedByName = new Dictionary<string, int>();
            this.undefinedDestinationRegistersKeyedByRegisterNumber = new Dictionary<int, string>();
            //this.undefinedMonitorsNames = new List<string>();

            this.undefinedMonitorsDynamicsSlaveRegisterNames = new List<string>();
        }

        private void InitializeDataTransferMasterDeviceRegisterNamesList()
        {
            this.dataTransferMasterDeviceRegisterNames = new List<string>();
            this.dataTransferMasterDeviceRegisterNames.Add(noValueSetDataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(vibration1DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(vibration2DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(vibration3DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(vibration4DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn1DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn2DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn3DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn4DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn5DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(analogIn6DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent1DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent2DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent3DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(voltage1DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(voltage2DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(voltage3DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(humidityDataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp1DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp2DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp3DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp4DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp5DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp6DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(temp7DataTransferText);
            this.dataTransferMasterDeviceRegisterNames.Add(chassisTempDataTransferText);
        }

        private void InitializeDataTransferADMSlaveDeviceRegisterNamesList()
        {
            this.admDynamicsSlaveRegisterNames = new List<string>();
            this.admDynamicsSlaveRegisterNames.Add(noValueSetDataTransferText);
            this.admDynamicsSlaveRegisterNames.Add(humidityDataTransferText);
            this.admDynamicsSlaveRegisterNames.Add(temp2DataTransferText);
        }

        private void InitializeDataTransferBHMSlaveDeviceRegisterNamesList()
        {
            this.bhmDynamicsSlaveRegisterNames = new List<string>();
            this.bhmDynamicsSlaveRegisterNames.Add(noValueSetDataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(humidityDataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(correlatingTempDataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(temp2DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(temp3DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(temp4DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(loadCurrent1DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(loadCurrent2DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(loadCurrent3DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(voltage1DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(voltage2DataTransferText);
            this.bhmDynamicsSlaveRegisterNames.Add(voltage3DataTransferText);
        }

        private void InitializeDataTransferPDMSlaveDeviceRegisterNamesList()
        {
            this.pdmDynamicsSlaveRegisterNames = new List<string>();
            this.pdmDynamicsSlaveRegisterNames.Add(noValueSetDataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(humidityDataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(correlatingTempDataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(temp2DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(temp3DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(temp4DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(loadCurrent1DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(loadCurrent2DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(loadCurrent3DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(voltage1DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(voltage2DataTransferText);
            this.pdmDynamicsSlaveRegisterNames.Add(voltage3DataTransferText);
        }

        private void GetSystemConfigurationForDataTransfer()
        {
            try
            {
                SortedDictionary<int, int> monitorTypesCount;

                /// what we are getting here is the system configuration as defined in the System Configuration tab, keyed by the ModBus address
                /// and having the integer monitor type as the value
                List<MonitorInfo> systemConfigurationInOrderEnteredByUser = GetModulesFromSystemConfigurationByEntryOrder();

                // these guys are here in case we have multiple monitors of the same type, and need to append a number to
                // the end of the monitor names
                int admCount = 0;
                int bhmCount = 0;
                int pdmCount = 0;

                int monitorTypeAsInteger;
                string uniqueName;

                MonitorInfo monitorInfo = null;

                if (this.monitorsByUniqueMonitorName != null)
                {
                    if (this.monitorUniqueNames != null)
                    {
                        if (systemConfigurationInOrderEnteredByUser != null)
                        {
                            monitorTypesCount = new SortedDictionary<int, int>();
                            this.monitorsByUniqueMonitorName.Clear();
                            this.monitorUniqueNames.Clear();

                            /// First we need to get a count of each monitor type.  We'll let the dictionary
                            /// data structure do most of our work for us by tracking what types are already
                            /// present
                            foreach (MonitorInfo entry in systemConfigurationInOrderEnteredByUser)
                            {
                                if (!monitorTypesCount.ContainsKey(entry.monitorTypeAsInteger))
                                {
                                    monitorTypesCount.Add(entry.monitorTypeAsInteger, 1);
                                }
                                else
                                {
                                    monitorTypesCount[entry.monitorTypeAsInteger]++;
                                }
                            }

                            /// now that we have the monitor counts, we can define the unique monitor names
                            
                            // First, we'll add an entry for no specified monitor
                            monitorInfo = new MonitorInfo();
                            monitorInfo.modBusAddress = 0;
                            monitorInfo.monitorTypeAsInteger = 0;
                            monitorsByUniqueMonitorName.Add(noneDataTransferText, monitorInfo);

                            this.monitorUniqueNames.Add(noneDataTransferText);

                            // now we'll add an entry for every unique monitor defined in the grid
                            foreach (MonitorInfo entry in systemConfigurationInOrderEnteredByUser)
                            {
                                monitorTypeAsInteger = entry.monitorTypeAsInteger;
                                uniqueName = GetModuleNameFromIntegerModuleType(monitorTypeAsInteger);

                                if (monitorTypesCount.ContainsKey(monitorTypeAsInteger))
                                {
                                    if (monitorTypesCount[monitorTypeAsInteger] > 1)
                                    {
                                        if (monitorTypeAsInteger == 6001)
                                        {
                                            admCount += 1;
                                            uniqueName = uniqueName + " " + admCount.ToString();
                                        }
                                        else if (monitorTypeAsInteger == 15002)
                                        {
                                            bhmCount += 1;
                                            uniqueName = uniqueName + " " + bhmCount.ToString();
                                        }
                                        else if (monitorTypeAsInteger == 505)
                                        {
                                            pdmCount += 1;
                                            uniqueName = uniqueName + " " + pdmCount.ToString();
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nThe system configuration contained an undefined monitor.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    /// now we can actually add the unique name to the list
                                    this.monitorUniqueNames.Add(uniqueName);
                                    /// we also add the monitor info to a dictionary keyed on this unique name we just created
                                    monitorInfo = new MonitorInfo();
                                    monitorInfo.modBusAddress = entry.modBusAddress;
                                    monitorInfo.monitorTypeAsInteger = monitorTypeAsInteger;
                                    monitorsByUniqueMonitorName.Add(uniqueName, monitorInfo);
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nThe local Dictionary<int,int> monitorTypesCount is missing a monitor type that should be present.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }

                            
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nThe system configuration was incorrectly defined in the System Configuration tab.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nthis.monitorUniqueNames was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nthis.monitorsByUniqueMonitorName was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetConfigurationInformation()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// Original version of the code

        //        private void GetConfigurationInformation()
        //        {

        //            //       private SortedDictionary<string, Monitor> monitorsByUniqueMonitorName;
        //            //private Dictionary<string, int> monitorTypesCount;
        //            try
        //            {
        //                SortedDictionary<string, int> monitorTypesCount;
        //                List<Monitor> equipmentList;
        //                List<Monitor> allMonitorsWithSameType;
        //                string monitorType;
        //                string monitorName;
        //                int count;

        //                if (this.monitorsByUniqueMonitorName != null)
        //                {
        //                    if (this.monitorUniqueNames != null)
        //                    {
        //                        monitorTypesCount = new SortedDictionary<string, int>();
        //                        this.monitorsByUniqueMonitorName.Clear();
        //                        this.monitorUniqueNames.Clear();

        //                        this.monitorUniqueNames.Add("None");

        //                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
        //                        {
        //                            equipmentList = DatabaseInterface.GetAllMonitorsForOneEquipment(this.monitor.EquipmentID, localDB);
        //                        }
        //                        if (equipmentList != null)
        //                        {
        //                            foreach (Monitor entry in equipmentList)
        //                            {
        //                                monitorType = entry.MonitorType.Trim();
        //                                if (monitorType.CompareTo("BHM") == 0)
        //                                {
        //                                    AddStringToSortedDictionary("BHM", ref monitorTypesCount);
        //                                }
        //                                else if (monitorType.CompareTo("PDM") == 0)
        //                                {
        //                                    AddStringToSortedDictionary("PDM", ref monitorTypesCount);
        //                                }
        //                                else if (monitorType.CompareTo("ADM") == 0)
        //                                {

        //                                }
        //                            }

        //                            foreach (KeyValuePair<string, int> kv in monitorTypesCount)
        //                            {
        //                                allMonitorsWithSameType = FindAllMonitorsOfTheGivenType(kv.Key, equipmentList);
        //                                count = kv.Value;
        //                                if (count > 1)
        //                                {
        //                                    for (int i = 0; i < count; i++)
        //                                    {
        //                                        monitorName = kv.Key + (i + 1).ToString();
        //                                        monitorUniqueNames.Add(monitorName);
        //                                        monitorsByUniqueMonitorName.Add(monitorName, allMonitorsWithSameType[i]);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    monitorName = kv.Key;
        //                                    monitorUniqueNames.Add(monitorName);
        //                                    monitorsByUniqueMonitorName.Add(monitorName, allMonitorsWithSameType[0]);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nthis.monitorUniqueNames was null.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.GetConfigurationInformation()\nthis.monitorsByUniqueMonitorName was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetConfigurationInformation()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private List<Monitor> FindAllMonitorsOfTheGivenType(string monitorType, List<Monitor> monitorList)
        {
            List<Monitor> outputList = new List<Monitor>();

            foreach (Monitor entry in monitorList)
            {
                if (entry.MonitorType.Trim().CompareTo(monitorType) == 0)
                {
                    outputList.Add(entry);
                }
            }
            return outputList;
        }

        private void AddStringToSortedDictionary(string monitorName, ref SortedDictionary<string, int> sortedDictionary)
        {
            if (sortedDictionary.ContainsKey(monitorName))
            {
                sortedDictionary[monitorName]++;
            }
            else
            {
                sortedDictionary.Add(monitorName, 1);
            }
        }

        private void InitializeDataTransferRadGridView()
        {
            try
            {
                this.dataTransferRadGridView.MasterTemplate.Columns.Clear();

                GridViewTextBoxColumn transferSetupNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("TransferSetupNumber");
                transferSetupNumberGridViewTextBoxColumn.Name = "TransferSetupNumber";
                transferSetupNumberGridViewTextBoxColumn.HeaderText = transferSetupNumberDataTransferGridViewText;
                transferSetupNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                transferSetupNumberGridViewTextBoxColumn.Width = 90;
                transferSetupNumberGridViewTextBoxColumn.AllowSort = false;
                transferSetupNumberGridViewTextBoxColumn.IsPinned = true;
                transferSetupNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelDataTransferGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = 60;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                //GridViewTextBoxColumn masterDeviceModbusAddressGridViewTextBoxColumn = new GridViewTextBoxColumn("MasterDeviceModbusAddress");
                //masterDeviceModbusAddressGridViewTextBoxColumn.Name = "MasterDeviceModbusAddress";
                //masterDeviceModbusAddressGridViewTextBoxColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Master Device<br>Modbus Address</html>";
                //masterDeviceModbusAddressGridViewTextBoxColumn.DisableHTMLRendering = false;
                //masterDeviceModbusAddressGridViewTextBoxColumn.Width = 101;
                //masterDeviceModbusAddressGridViewTextBoxColumn.AllowSort = false;
                //masterDeviceModbusAddressGridViewTextBoxColumn.ReadOnly = true;

                GridViewComboBoxColumn masterDeviceRegisterGridViewComboBoxColumn = new GridViewComboBoxColumn("MasterDeviceRegister");
                masterDeviceRegisterGridViewComboBoxColumn.Name = "MasterDeviceRegister";
                masterDeviceRegisterGridViewComboBoxColumn.HeaderText = masterDeviceRegisterDataTransferGridViewText;
                masterDeviceRegisterGridViewComboBoxColumn.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.dataTransferMasterDeviceRegisterNames);
                masterDeviceRegisterGridViewComboBoxColumn.DisableHTMLRendering = false;
                masterDeviceRegisterGridViewComboBoxColumn.Width = 125;
                masterDeviceRegisterGridViewComboBoxColumn.AllowSort = false;

                //GridViewDecimalColumn masterDeviceRegisterGridViewDecimalColumn = new GridViewDecimalColumn("MasterDeviceRegister");
                //masterDeviceRegisterGridViewDecimalColumn.Name = "MasterDeviceRegister";
                //masterDeviceRegisterGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Master Device<br>Register</html>";
                //masterDeviceRegisterGridViewDecimalColumn.DecimalPlaces = 0;
                //masterDeviceRegisterGridViewDecimalColumn.DisableHTMLRendering = false;
                //masterDeviceRegisterGridViewDecimalColumn.Width = 101;
                //masterDeviceRegisterGridViewDecimalColumn.AllowSort = false;
                //masterDeviceRegisterGridViewDecimalColumn.Minimum = 1;
                //masterDeviceRegisterGridViewDecimalColumn.Maximum = 10000;

                GridViewComboBoxColumn slaveDeviceNameGridViewComboBoxColumn = new GridViewComboBoxColumn("SlaveDeviceName");
                slaveDeviceNameGridViewComboBoxColumn.Name = "SlaveDeviceName";
                slaveDeviceNameGridViewComboBoxColumn.HeaderText = slaveDeviceNameDataTransferGridViewText;
                //if (this.monitorUniqueNames != null)
                //{
                //    slaveDeviceNameGridViewComboBoxColumn.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.monitorUniqueNames);
                //}
                //else
                //{
                //    slaveDeviceNameGridViewComboBoxColumn.DataSource = new string[] { "No Values Available" };
                //}
                slaveDeviceNameGridViewComboBoxColumn.DisableHTMLRendering = false;
                slaveDeviceNameGridViewComboBoxColumn.Width = 110;
                slaveDeviceNameGridViewComboBoxColumn.AllowSort = false;

                GridViewTextBoxColumn slaveDeviceModbusAddressGridViewTextBoxColumn = new GridViewTextBoxColumn("SlaveDeviceModbusAddress");
                slaveDeviceModbusAddressGridViewTextBoxColumn.Name = "SlaveDeviceModbusAddress";
                slaveDeviceModbusAddressGridViewTextBoxColumn.HeaderText = slaveDeviceModbusAddressDataTransferGridViewText;
                // slaveDeviceModbusAddressGridViewDecimalColumn.DecimalPlaces = 0;
                slaveDeviceModbusAddressGridViewTextBoxColumn.DisableHTMLRendering = false;
                slaveDeviceModbusAddressGridViewTextBoxColumn.Width = 105;
                slaveDeviceModbusAddressGridViewTextBoxColumn.AllowSort = false;
                //slaveDeviceModbusAddressGridViewTextBoxColumn.Minimum = 1;
                //slaveDeviceModbusAddressGridViewTextBoxColumn.Maximum = 255;
                slaveDeviceModbusAddressGridViewTextBoxColumn.ReadOnly = true;

                GridViewComboBoxColumn slaveDeviceRegisterGridViewComboBoxColumn = new GridViewComboBoxColumn("SlaveDeviceRegister");
                slaveDeviceRegisterGridViewComboBoxColumn.Name = "SlaveDeviceRegister";
                slaveDeviceRegisterGridViewComboBoxColumn.HeaderText = slaveDeviceRegisterDataTransferGridViewText;
                slaveDeviceRegisterGridViewComboBoxColumn.DataSource = new string[] { noValuesAvailableDataTransferText };
                slaveDeviceRegisterGridViewComboBoxColumn.DisableHTMLRendering = false;
                slaveDeviceRegisterGridViewComboBoxColumn.Width = 110;
                slaveDeviceRegisterGridViewComboBoxColumn.AllowSort = false;

                //GridViewDecimalColumn slaveDeviceRegisterGridViewDecimalColumn = new GridViewDecimalColumn("SlaveDeviceRegister");
                //slaveDeviceRegisterGridViewDecimalColumn.Name = "SlaveDeviceRegister";
                //slaveDeviceRegisterGridViewDecimalColumn.HeaderText = "<html><font=Microsoft Sans Serif><size=8.25>Slave Device<br>Register</html>";
                //slaveDeviceRegisterGridViewDecimalColumn.DecimalPlaces = 0;
                //slaveDeviceRegisterGridViewDecimalColumn.DisableHTMLRendering = false;
                //slaveDeviceRegisterGridViewDecimalColumn.Width = 101;
                //slaveDeviceRegisterGridViewDecimalColumn.AllowSort = false;
                //slaveDeviceRegisterGridViewDecimalColumn.Minimum = 1;
                //slaveDeviceRegisterGridViewDecimalColumn.Maximum = 10000;

                GridViewDecimalColumn slopeGridViewDecimalColumn = new GridViewDecimalColumn("Slope");
                slopeGridViewDecimalColumn.Name = "Slope";
                slopeGridViewDecimalColumn.HeaderText = slopeDataTransferGridViewText;
                slopeGridViewDecimalColumn.DecimalPlaces = 6;
                slopeGridViewDecimalColumn.Step = (decimal)0.1;
                slopeGridViewDecimalColumn.DisableHTMLRendering = false;
                slopeGridViewDecimalColumn.Width = 85;
                slopeGridViewDecimalColumn.AllowSort = false;
                //slopeGridViewDecimalColumn.Minimum = 0;
                //slopeGridViewDecimalColumn.Maximum = 10;

                this.dataTransferRadGridView.MasterTemplate.Columns.Add(transferSetupNumberGridViewTextBoxColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                //this.dataTransferRadGridView.MasterTemplate.Columns.Add(masterDeviceModbusAddressGridViewDecimalColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(masterDeviceRegisterGridViewComboBoxColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(slaveDeviceNameGridViewComboBoxColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(slaveDeviceModbusAddressGridViewTextBoxColumn);
                //this.dataTransferRadGridView.MasterTemplate.Columns.Add(slaveDeviceModbusAddressGridViewDecimalColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(slaveDeviceRegisterGridViewComboBoxColumn);
                this.dataTransferRadGridView.MasterTemplate.Columns.Add(slopeGridViewDecimalColumn);

                this.dataTransferRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.dataTransferRadGridView.AllowColumnReorder = false;
                this.dataTransferRadGridView.AllowColumnChooser = false;
                this.dataTransferRadGridView.ShowGroupPanel = false;
                this.dataTransferRadGridView.EnableGrouping = false;
                this.dataTransferRadGridView.AllowAddNewRow = false;
                this.dataTransferRadGridView.AllowDeleteRow = false;
                this.dataTransferRadGridView.MultiSelect = true;
                this.dataTransferRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeDataTransferRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private void FillTransferSetupDataList()
        //        {
        //            try
        //            {
        //                transferSetupDataList.Clear();
        //                for (int i = 0; i < 10; i++)
        //                {
        //                    transferSetupDataList.Add(GetTransferSetupDataForOneTransferNumber(i));
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.FillTransferSetupDataList()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private TransferSetupData GetTransferSetupDataForOneTransferNumber(int transferNumber)
        //        {
        //            TransferSetupData transferData = new TransferSetupData();
        //            try
        //            {
        //                transferData.transferSetupNumber = transferNumber + 1;
        //                transferData.enabled = false;
        //                transferData.masterDeviceModBusAddress = 1;
        //                transferData.masterDeviceRegister = 0;
        //                transferData.slaveDeviceModBusAddress = 0;
        //                transferData.slaveDeviceRegister = 0;
        //                transferData.slope = 0.00;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetTransferSetupDataForOneTransferNumber(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return transferData;
        //        }

        private void AddEmptyRowsToTheDataTransferGrid()
        {
            this.dataTransferRadGridView.Rows.Clear();
            for (int i = 0; i < 10; i++)
            {
                this.dataTransferRadGridView.Rows.Add(i + 1, false, noValueSetDataTransferText, noneDataTransferText, 0, noValueSetDataTransferText, 0);
            }
            this.dataTransferRadGridView.TableElement.ScrollToRow(0);
        }

        private bool AddDataToTheDataTransferGrid(List<Main_ConfigComponent_TransferSetup> inputTransferSetupList)
        {
            bool noRegisterErrorsSeen = true;
            try
            {
                GridViewRowInfo rowInfo;
                bool enabled;
                string masterDeviceRegisterName;
                int slaveDeviceModBusAddress;
                string slaveDeviceUniqueName = string.Empty;
                int slaveDeviceType = 0;
                string slaveDeviceRegisterName = string.Empty;
                double slope = 0.0;
                Main_ConfigComponent_TransferSetup transferSetup;
                int undefinedMonitorCount = 0;
                string generatedMonitorName;
                string generatedRegisterName;
                int destinationRegisterNumber;
                MonitorInfo monitorInfo;
                if (inputTransferSetupList != null)
                {
                    if (inputTransferSetupList.Count > 9)
                    {
                        if (this.dataTransferRadGridView.Rows.Count == 10)
                        {
                            //this.undefinedMonitorsKeyedByName.Clear();
                            //this.undefinedMonitorsKeyedByModbusAddress.Clear();
                       
                          //  this.undefinedMonitorsNames.Clear();

                            // we'll save destination registers for undefined monitors here, just to handle the case when a bad
                            // configuration is loaded.  this is used when displaying values in the drop-down list
                            this.undefinedMonitorsDynamicsSlaveRegisterNames.Clear();
                            this.undefinedMonitorsDynamicsSlaveRegisterNames.Add(noValueSetDataTransferText);
                            this.undefinedDestinationRegistersKeyedByName.Clear();
                            this.undefinedDestinationRegistersKeyedByRegisterNumber.Clear();

                            for (int i = 0; i < 10; i++)
                            {
                                transferSetup = inputTransferSetupList[i];
                                rowInfo = this.dataTransferRadGridView.Rows[i];

                                if (transferSetup.Working == 0)
                                {
                                    enabled = false;
                                }
                                else
                                {
                                    enabled = true;
                                }

                                masterDeviceRegisterName = ConvertMasterDeviceRegisterToMasterDeviceRegisterName(transferSetup.SourceRegisterNumber);

                                slaveDeviceModBusAddress = transferSetup.DestinationModuleNumber;
                                if (slaveDeviceModBusAddress != 0)
                                {
                                    slaveDeviceUniqueName = GetUniqueMonitorNameFromModbusAddress(slaveDeviceModBusAddress);
                                    if (this.monitorsByUniqueMonitorName.ContainsKey(slaveDeviceUniqueName))
                                    {
                                        slaveDeviceType = this.monitorsByUniqueMonitorName[slaveDeviceUniqueName].monitorTypeAsInteger;
                                    }
                                    else
                                    {
                                        /// add a new entry to the monitor list, even though it has not been defined in the system configuration tab, 
                                        /// because we need to handle configurations that are incorrect (not created in athena or related software)
                                        monitorInfo = new MonitorInfo();
                                        monitorInfo.modBusAddress = slaveDeviceModBusAddress;
                                        monitorInfo.monitorTypeAsInteger = slaveDeviceTypeWhenUnknown;
                                        undefinedMonitorCount++;
                                        generatedMonitorName = undefinedDeviceNameText + " " + undefinedMonitorCount.ToString();

                                        this.monitorsByUniqueMonitorName.Add(generatedMonitorName, monitorInfo);
                                        this.monitorUniqueNames.Add(generatedMonitorName);

                                        //if (!this.undefinedMonitorsKeyedByModbusAddress.ContainsKey(slaveDeviceModBusAddress))
                                        //{
                                        //    undefinedMonitorCount++;
                                        //    generatedMonitorName = undefinedDeviceNameText + " " + undefinedMonitorCount.ToString();
                                        //    this.undefinedMonitorsKeyedByModbusAddress.Add(slaveDeviceModBusAddress, generatedMonitorName);
                                        //    this.undefinedMonitorsKeyedByName.Add(generatedMonitorName, slaveDeviceModBusAddress);
                                        //    this.undefinedMonitorsNames.Add(generatedMonitorName);
                                        //}
                                        slaveDeviceUniqueName = generatedMonitorName;
                                        slaveDeviceType = slaveDeviceTypeWhenUnknown;
                                    }
                                }
                                else
                                {
                                    slaveDeviceUniqueName = noneDataTransferText;
                                    slaveDeviceType = 0;
                                }

                                destinationRegisterNumber = transferSetup.DestinationRegisterNumber;

                                if (slaveDeviceType == 6001)
                                {
                                    slaveDeviceRegisterName = ConvertADMSlaveDeviceRegisiterToSlaveDeviceRegisterName(destinationRegisterNumber);
                                }
                                else if (slaveDeviceType == 15002)
                                {
                                    slaveDeviceRegisterName = ConvertBHMSlaveDeviceRegisiterToSlaveDeviceRegisterName(destinationRegisterNumber);
                                }
                                else if (slaveDeviceType == 505)
                                {
                                    slaveDeviceRegisterName = ConvertPDMSlaveDeviceRegisiterToSlaveDeviceRegisterName(destinationRegisterNumber);
                                }
                                else if (slaveDeviceType == slaveDeviceTypeWhenUnknown)
                                {
                                    if (!this.undefinedDestinationRegistersKeyedByRegisterNumber.ContainsKey(destinationRegisterNumber))
                                    {
                                        generatedRegisterName = undefinedRegisterNameText + " " +  destinationRegisterNumber.ToString();
                                        this.undefinedDestinationRegistersKeyedByRegisterNumber.Add(destinationRegisterNumber, generatedRegisterName);
                                        this.undefinedDestinationRegistersKeyedByName.Add(generatedRegisterName, destinationRegisterNumber);
                                        this.undefinedMonitorsDynamicsSlaveRegisterNames.Add(generatedRegisterName);
                                    }
                                    slaveDeviceRegisterName = this.undefinedDestinationRegistersKeyedByRegisterNumber[destinationRegisterNumber];
                                }
                                else
                                {
                                    slaveDeviceRegisterName = noValueSetDataTransferText;
                                }

                                if ((slaveDeviceRegisterName.CompareTo(register18DataTransferText) == 0) || (slaveDeviceRegisterName.CompareTo(register20DataTransferText) == 0))
                                {
                                    noRegisterErrorsSeen = false;
                                }

                                slope = Math.Round(transferSetup.Multiplier, 6);

                                rowInfo.Cells[1].Value = enabled;
                                rowInfo.Cells[2].Value = masterDeviceRegisterName;
                                rowInfo.Cells[3].Value = slaveDeviceUniqueName;
                                rowInfo.Cells[4].Value = slaveDeviceModBusAddress;
                                rowInfo.Cells[5].Value = slaveDeviceRegisterName;
                                rowInfo.Cells[6].Value = slope;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheDataTransferGrid(Main_ConfigComponent_TransferSetup)\nthis.dataTransferRadGridView did not have 10 entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheDataTransferGrid(Main_ConfigComponent_TransferSetup)\nInput List<Main_ConfigComponent_TransferSetup> had too few entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheDataTransferGrid(Main_ConfigComponent_TransferSetup)\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheDataTransferGrid(Main_ConfigComponent_TransferSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return noRegisterErrorsSeen;
        }


        //        private void AddRowsToTheDataTransferGrid()
        //        {
        //            try
        //            {
        //                int transferSetupNumber = 1;
        //                bool enabled;
        //                int masterDeviceModBusAddress;
        //                string masterDeviceRegisterName;
        //                int slaveDeviceModBusAddress;
        //                string slaveDeviceUniqueName = string.Empty;
        //                int slaveDeviceType = 0;
        //                string slaveDeviceRegisterName = string.Empty;
        //                double slope = 0.0;

        //                this.dataTransferRadGridView.Rows.Clear();
        //                foreach (Main_ConfigComponent_TransferSetup transferSetup in currentConfiguration.transferSetupConfigObjects)
        //                {
        //                    if (transferSetup.Working == 0)
        //                    {
        //                        enabled = false;
        //                    }
        //                    else
        //                    {
        //                        enabled = true;
        //                    }
        //                    masterDeviceModBusAddress = transferSetup.SourceModuleNumber;
        //                    masterDeviceRegisterName = ConvertMasterDeviceRegisterToMasterDeviceRegisterName(transferSetup.SourceRegisterNumber);

        //                    slaveDeviceModBusAddress = transferSetup.DestinationModuleNumber;
        //                    if (slaveDeviceModBusAddress != 0)
        //                    {
        //                        slaveDeviceUniqueName = GetUniqueMonitorNameFromModbusAddress(slaveDeviceModBusAddress);
        //                        if (this.monitorsByUniqueMonitorName.ContainsKey(slaveDeviceUniqueName))
        //                        {
        //                            slaveDeviceType = this.monitorsByUniqueMonitorName[slaveDeviceUniqueName].monitorTypeAsInteger;
        //                        }
        //                        else
        //                        {
        //                            slaveDeviceUniqueName = "None";
        //                            slaveDeviceType = 0;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        slaveDeviceUniqueName = "None";
        //                        slaveDeviceType = 0;
        //                    }

        //                    if (slaveDeviceType == 6001)
        //                    {
        //                        slaveDeviceRegisterName = ConvertADMSlaveDeviceRegisiterToSlaveDeviceRegisterName(transferSetup.DestinationRegisterNumber);
        //                    }
        //                    else if (slaveDeviceType == 15002)
        //                    {
        //                        slaveDeviceRegisterName = ConvertBHMSlaveDeviceRegisiterToSlaveDeviceRegisterName(transferSetup.DestinationRegisterNumber);
        //                    }
        //                    else if (slaveDeviceType == 505)
        //                    {
        //                        slaveDeviceRegisterName = ConvertPDMSlaveDeviceRegisiterToSlaveDeviceRegisterName(transferSetup.DestinationRegisterNumber);
        //                    }
        //                    else
        //                    {
        //                        slaveDeviceRegisterName = "No Value Set";
        //                    }

        //                    slope = Math.Round(transferSetup.Multiplier, 6);

        //                    this.dataTransferRadGridView.Rows.Add(transferSetup.TransferSetupNumber + 1, enabled, masterDeviceRegisterName, slaveDeviceUniqueName,
        //                                                          slaveDeviceModBusAddress, slaveDeviceRegisterName, slope);

        //                    transferSetupNumber++;

        //                    if (transferSetupNumber > 10)
        //                    {
        //                        break;
        //                    }
        //                }
        //                this.dataTransferRadGridView.TableElement.ScrollToRow(0);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheDataTransferGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private string GetUniqueMonitorNameFromModbusAddress(int modbusAddress)
        {
            string uniqueMonitorName = string.Empty;
            foreach (KeyValuePair<string, MonitorInfo> kv in this.monitorsByUniqueMonitorName)
            {
                if (kv.Value.modBusAddress == modbusAddress)
                {
                    uniqueMonitorName = kv.Key;
                    break;
                }
            }
            return uniqueMonitorName;
        }

        private string ConvertMasterDeviceRegisterToMasterDeviceRegisterName(int masterDeviceRegister)
        {
            string masterDeviceRegisterName = string.Empty;
            try
            {
                switch (masterDeviceRegister)
                {
                    case 6011:
                        masterDeviceRegisterName = vibration1DataTransferText;
                        break;
                    case 6013:
                        masterDeviceRegisterName = vibration2DataTransferText;
                        break;
                    case 6015:
                        masterDeviceRegisterName = vibration3DataTransferText;
                        break;
                    case 6017:
                        masterDeviceRegisterName = vibration4DataTransferText;
                        break;
                    case 6019:
                        masterDeviceRegisterName = analogIn1DataTransferText;
                        break;
                    case 6021:
                        masterDeviceRegisterName = analogIn2DataTransferText;
                        break;
                    case 6023:
                        masterDeviceRegisterName = analogIn3DataTransferText;
                        break;
                    case 6025:
                        masterDeviceRegisterName = analogIn4DataTransferText;
                        break;
                    case 6027:
                        masterDeviceRegisterName = analogIn5DataTransferText;
                        break;
                    case 6029:
                        masterDeviceRegisterName = analogIn6DataTransferText;
                        break;
                    case 6031:
                        masterDeviceRegisterName = loadCurrent1DataTransferText;
                        break;
                    case 6033:
                        masterDeviceRegisterName = loadCurrent2DataTransferText;
                        break;
                    case 6035:
                        masterDeviceRegisterName = loadCurrent3DataTransferText;
                        break;
                    case 6037:
                        masterDeviceRegisterName = voltage1DataTransferText;
                        break;
                    case 6039:
                        masterDeviceRegisterName = voltage2DataTransferText;
                        break;
                    case 6041:
                        masterDeviceRegisterName = voltage3DataTransferText;
                        break;
                    case 6043:
                        masterDeviceRegisterName = humidityDataTransferText;
                        break;
                    case 6045:
                        masterDeviceRegisterName = temp1DataTransferText;
                        break;
                    case 6047:
                        masterDeviceRegisterName = temp2DataTransferText;
                        break;
                    case 6049:
                        masterDeviceRegisterName = temp3DataTransferText;
                        break;
                    case 6051:
                        masterDeviceRegisterName = temp4DataTransferText;
                        break;
                    case 6053:
                        masterDeviceRegisterName = temp5DataTransferText;
                        break;
                    case 6055:
                        masterDeviceRegisterName = temp6DataTransferText;
                        break;
                    case 6057:
                        masterDeviceRegisterName = temp7DataTransferText;
                        break;
                    case 6059:
                        masterDeviceRegisterName = chassisTempDataTransferText;
                        break;
                    default:
                        masterDeviceRegisterName = "No Value Set";
                        //                        string errorMessage = "Error in Main_MonitorConfiguration.ConvertMasterDeviceRegisterToMasterDeviceRegisterName(string)\nInput master device register number not handled.";
                        //                        LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                        MessageBox.Show(errorMessage);
                        //#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertMasterDeviceRegisterToMasterDeviceRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return masterDeviceRegisterName;
        }

        private int ConvertMasterDeviceRegisiterNameToMasterDeviceRegister(string masterDeviceRegisterName)
        {
            int masterDeviceRegister = 0;
            try
            {
                if (masterDeviceRegisterName.CompareTo(vibration1DataTransferText) == 0)
                {
                    masterDeviceRegister = 6011;
                }
                else if (masterDeviceRegisterName.CompareTo(vibration2DataTransferText) == 0)
                {
                    masterDeviceRegister = 6013;
                }
                else if (masterDeviceRegisterName.CompareTo(vibration3DataTransferText) == 0)
                {
                    masterDeviceRegister = 6015;
                }
                else if (masterDeviceRegisterName.CompareTo(vibration4DataTransferText) == 0)
                {
                    masterDeviceRegister = 6017;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn1DataTransferText) == 0)
                {
                    masterDeviceRegister = 6019;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn2DataTransferText) == 0)
                {
                    masterDeviceRegister = 6021;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn3DataTransferText) == 0)
                {
                    masterDeviceRegister = 6023;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn4DataTransferText) == 0)
                {
                    masterDeviceRegister = 6025;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn5DataTransferText) == 0)
                {
                    masterDeviceRegister = 6027;
                }
                else if (masterDeviceRegisterName.CompareTo(analogIn6DataTransferText) == 0)
                {
                    masterDeviceRegister = 6029;
                }
                else if (masterDeviceRegisterName.CompareTo(loadCurrent1DataTransferText) == 0)
                {
                    masterDeviceRegister = 6031;
                }
                else if (masterDeviceRegisterName.CompareTo(loadCurrent2DataTransferText) == 0)
                {
                    masterDeviceRegister = 6033;
                }
                else if (masterDeviceRegisterName.CompareTo(loadCurrent3DataTransferText) == 0)
                {
                    masterDeviceRegister = 6035;
                }
                else if (masterDeviceRegisterName.CompareTo(voltage1DataTransferText) == 0)
                {
                    masterDeviceRegister = 6037;
                }
                else if (masterDeviceRegisterName.CompareTo(voltage2DataTransferText) == 0)
                {
                    masterDeviceRegister = 6039;
                }
                else if (masterDeviceRegisterName.CompareTo(voltage3DataTransferText) == 0)
                {
                    masterDeviceRegister = 6041;
                }
                else if (masterDeviceRegisterName.CompareTo(humidityDataTransferText) == 0)
                {
                    masterDeviceRegister = 6043;
                }
                else if (masterDeviceRegisterName.CompareTo(temp1DataTransferText) == 0)
                {
                    masterDeviceRegister = 6045;
                }
                else if (masterDeviceRegisterName.CompareTo(temp2DataTransferText) == 0)
                {
                    masterDeviceRegister = 6047;
                }
                else if (masterDeviceRegisterName.CompareTo(temp3DataTransferText) == 0)
                {
                    masterDeviceRegister = 6049;
                }
                else if (masterDeviceRegisterName.CompareTo(temp4DataTransferText) == 0)
                {
                    masterDeviceRegister = 6051;
                }
                else if (masterDeviceRegisterName.CompareTo(temp5DataTransferText) == 0)
                {
                    masterDeviceRegister = 6053;
                }
                else if (masterDeviceRegisterName.CompareTo(temp6DataTransferText) == 0)
                {
                    masterDeviceRegister = 6055;
                }
                else if (masterDeviceRegisterName.CompareTo(temp7DataTransferText) == 0)
                {
                    masterDeviceRegister = 6057;
                }
                else if (masterDeviceRegisterName.CompareTo(chassisTempDataTransferText) == 0)
                {
                    masterDeviceRegister = 6059;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertMasterDeviceRegisiterNameToMasterDeviceRegister(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return masterDeviceRegister;
        }

        private string ConvertADMSlaveDeviceRegisiterToSlaveDeviceRegisterName(int slaveDeviceRegister)
        {
            string slaveDeviceRegisterName = string.Empty;
            try
            {
                switch (slaveDeviceRegister)
                {
                    case 19:
                        slaveDeviceRegisterName = humidityDataTransferText;
                        break;
                    case 16:
                        slaveDeviceRegisterName = temp2DataTransferText;
                        break;
                    default:
                        slaveDeviceRegisterName = noValueSetDataTransferText;
                        string errorMessage = "Error in Main_MonitorConfiguration.ConvertADMSlaveDeviceRegisiterToSlaveDeviceRegisterName(string)\nInput slave device register number not handled.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertADMSlaveDeviceRegisiterToSlaveDeviceRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegisterName;
        }



        //BHM

        //16 temp 1

        //17 load 1

        //19 Humidity

        //21 Temp 2

        //22 Temp 3

        //23 temp 4
        private string ConvertBHMSlaveDeviceRegisiterToSlaveDeviceRegisterName(int slaveDeviceRegister)
        {
            string slaveDeviceRegisterName = string.Empty;
            try
            {
                switch (slaveDeviceRegister)
                {
                    case 16:
                        slaveDeviceRegisterName = correlatingTempDataTransferText;
                        break;
                    case 17:
                        slaveDeviceRegisterName = loadCurrent1DataTransferText;
                        break;
                    case 18:
                        slaveDeviceRegisterName = register18DataTransferText;
                        break;
                    case 19:
                        slaveDeviceRegisterName = humidityDataTransferText;
                        break;
                    case 20:
                        slaveDeviceRegisterName = register20DataTransferText;
                        break;
                    case 21:
                        slaveDeviceRegisterName = temp2DataTransferText;
                        break;
                    case 22:
                        slaveDeviceRegisterName = temp3DataTransferText;
                        break;
                    case 23:
                        slaveDeviceRegisterName = temp4DataTransferText;
                        break;
                    case 24:
                        slaveDeviceRegisterName = loadCurrent2DataTransferText;
                        break;
                    case 25:
                        slaveDeviceRegisterName = loadCurrent3DataTransferText;
                        break;
                    case 26:
                        slaveDeviceRegisterName = voltage1DataTransferText;
                        break;
                    case 27:
                        slaveDeviceRegisterName = voltage2DataTransferText;
                        break;
                    case 28:
                        slaveDeviceRegisterName = voltage3DataTransferText;
                        break;
                    default:
                        slaveDeviceRegisterName = noValueSetDataTransferText;
                        string errorMessage = "Error in Main_MonitorConfiguration.ConvertBHMSlaveDeviceRegisiterToSlaveDeviceRegisterName(string)\nInput slave device register number not handled.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertslaveDeviceRegisterToslaveDeviceRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegisterName;
        }

        //16 Temp1

        //17 load 1

        //19 Humidity

        //20 load 2

        //21 load 3

        //22  Temp2

        //23  temp3

        //24 temp4

        //25 volt1

        //26 volt2

        //27 volt3
        private string ConvertPDMSlaveDeviceRegisiterToSlaveDeviceRegisterName(int slaveDeviceRegister)
        {
            string slaveDeviceRegisterName = string.Empty;
            try
            {
                switch (slaveDeviceRegister)
                {
                    case 16:
                        slaveDeviceRegisterName = correlatingTempDataTransferText;
                        break;
                    case 17:
                        slaveDeviceRegisterName = loadCurrent1DataTransferText;
                        break;
                    case 18:
                        slaveDeviceRegisterName = register18DataTransferText;
                        break;
                    case 19:
                        slaveDeviceRegisterName = humidityDataTransferText;
                        break;
                    case 20:
                        slaveDeviceRegisterName = register20DataTransferText;
                        break;
                    case 21:
                        slaveDeviceRegisterName = temp2DataTransferText;
                        break;
                    case 22:
                        slaveDeviceRegisterName = temp3DataTransferText;
                        break;
                    case 23:
                        slaveDeviceRegisterName = temp4DataTransferText;
                        break;
                    case 24:
                        slaveDeviceRegisterName = loadCurrent2DataTransferText;
                        break;
                    case 25:
                        slaveDeviceRegisterName = loadCurrent3DataTransferText;
                        break;
                    case 26:
                        slaveDeviceRegisterName = voltage1DataTransferText;
                        break;
                    case 27:
                        slaveDeviceRegisterName = voltage2DataTransferText;
                        break;
                    case 28:
                        slaveDeviceRegisterName = voltage3DataTransferText;
                        break;
                    default:
                        slaveDeviceRegisterName = noValueSetDataTransferText;
                        //                        string errorMessage = "Error in Main_MonitorConfiguration.ConvertPDMSlaveDeviceRegisiterToSlaveDeviceRegisterName(string)\nInput slave device register number not handled.";
                        //                        LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                        MessageBox.Show(errorMessage);
                        //#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertslaveDeviceRegisterToslaveDeviceRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegisterName;
        }

        private int ConvertADMSlaveDeviceRegisiterNameToSlaveDeviceRegister(string slaveDeviceRegisterName)
        {
            int slaveDeviceRegister = 0;
            try
            {

                if (slaveDeviceRegisterName.CompareTo(humidityDataTransferText) == 0)
                {
                    slaveDeviceRegister = 19;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 16;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertADMSlaveDeviceRegisiterNameToSlaveDeviceRegister(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegister;
        }

        private int ConvertBHMSlaveDeviceRegisiterNameToSlaveDeviceRegister(string slaveDeviceRegisterName)
        {
            int slaveDeviceRegister = 0;
            try
            {
                if (slaveDeviceRegisterName.CompareTo(correlatingTempDataTransferText) == 0)
                {
                    slaveDeviceRegister = 16;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent1DataTransferText) == 0)
                {
                    slaveDeviceRegister = 17;
                }
                else if (slaveDeviceRegisterName.CompareTo(register18DataTransferText) == 0)
                {
                    slaveDeviceRegister = 18;
                }
                else if (slaveDeviceRegisterName.CompareTo(humidityDataTransferText) == 0)
                {
                    slaveDeviceRegister = 19;
                }
                else if (slaveDeviceRegisterName.CompareTo(register20DataTransferText) == 0)
                {
                    slaveDeviceRegister = 20;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 21;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 22;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp4DataTransferText) == 0)
                {
                    slaveDeviceRegister = 23;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 24;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 25;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage1DataTransferText) == 0)
                {
                    slaveDeviceRegister = 26;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 27;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 28;
                }


            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertBHMSlaveDeviceRegisiterNameToSlaveDeviceRegister(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegister;
        }

        private int ConvertPDMSlaveDeviceRegisiterNameToSlaveDeviceRegister(string slaveDeviceRegisterName)
        {
            int slaveDeviceRegister = 0;
            try
            {
                if (slaveDeviceRegisterName.CompareTo(correlatingTempDataTransferText) == 0)
                {
                    slaveDeviceRegister = 16;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent1DataTransferText) == 0)
                {
                    slaveDeviceRegister = 17;
                }
                else if (slaveDeviceRegisterName.CompareTo(register18DataTransferText) == 0)
                {
                    slaveDeviceRegister = 18;
                }
                else if (slaveDeviceRegisterName.CompareTo(humidityDataTransferText) == 0)
                {
                    slaveDeviceRegister = 19;
                }
                else if (slaveDeviceRegisterName.CompareTo(register20DataTransferText) == 0)
                {
                    slaveDeviceRegister = 20;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 21;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 22;
                }
                else if (slaveDeviceRegisterName.CompareTo(temp4DataTransferText) == 0)
                {
                    slaveDeviceRegister = 23;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 24;
                }
                else if (slaveDeviceRegisterName.CompareTo(loadCurrent3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 25;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage1DataTransferText) == 0)
                {
                    slaveDeviceRegister = 26;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage2DataTransferText) == 0)
                {
                    slaveDeviceRegister = 27;
                }
                else if (slaveDeviceRegisterName.CompareTo(voltage3DataTransferText) == 0)
                {
                    slaveDeviceRegister = 28;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertslaveDeviceRegisiterNameToslaveDeviceRegister(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return slaveDeviceRegister;
        }

        private void WriteDataTransferTabDataToCurrentConfiguration()
        {
            try
            {
                GridViewRowInfo rowInfo;
                //string enabledAsString;
                int enabled;
                int masterDeviceModBusAddress;
                int masterDeviceRegister;
                string slaveDeviceUniqueName;
                int slaveDeviceType;
                int slaveDeviceModBusAddress;
                int slaveDeviceRegister;
                string slaveDeviceRegisterName;
                double slope = 0.0;

                int count = this.dataTransferRadGridView.Rows.Count;

                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.transferSetupConfigObjects != null)
                    {
                        if (this.workingConfiguration.transferSetupConfigObjects.Count >= count)
                        {
                            if (count > 0)
                            {
                                if (!Int32.TryParse(Main_MonitorConfiguration.monitor.ModbusAddress, out masterDeviceModBusAddress))
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nMain_MonitorConfiguration.monitor.ModBusAddress failed to parse properly.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                else
                                {
                                    for (int i = 0; i < count; i++)
                                    {
                                        enabled = 0;
                                        masterDeviceModBusAddress = 0;
                                        masterDeviceRegister = 0;
                                        slaveDeviceType = 0;
                                        slaveDeviceModBusAddress = 0;
                                        slaveDeviceRegister = 0;
                                        slope = 0.0;

                                        rowInfo = dataTransferRadGridView.Rows[i];
                                        if (rowInfo.Cells.Count > 6)
                                        {
                                            if ((bool)(rowInfo.Cells[1].Value) == true)
                                            {
                                                enabled = 1;
                                            }

                                            masterDeviceRegister = ConvertMasterDeviceRegisiterNameToMasterDeviceRegister(rowInfo.Cells[2].Value.ToString().Trim());
                                            if (masterDeviceRegister != 0)
                                            {
                                                masterDeviceModBusAddress = 1;
                                            }

                                            // Get slave device modbus address, takes some indirection to get it.  The name keys into a dictionary that has
                                            // all the info.
                                            slaveDeviceUniqueName = rowInfo.Cells[3].Value.ToString();

                                            if (this.monitorsByUniqueMonitorName.ContainsKey(slaveDeviceUniqueName))
                                            {
                                                slaveDeviceType = this.monitorsByUniqueMonitorName[slaveDeviceUniqueName].monitorTypeAsInteger;
                                                slaveDeviceModBusAddress = this.monitorsByUniqueMonitorName[slaveDeviceUniqueName].modBusAddress;
                                            }
                                            //else if (this.undefinedMonitorsKeyedByName.ContainsKey(slaveDeviceUniqueName))
                                            //{
                                            //    slaveDeviceType = slaveDeviceTypeWhenUnknown;
                                            //}
                                            else
                                            {
                                                string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\n" + slaveDeviceUniqueName + " was missing from this.monitorsByUniqueMonitorName";
                                                LogMessage.LogError(errorMessage);
#if DEBUG
                                                MessageBox.Show(errorMessage);
#endif
                                            }
                                          
                                            // skip a cell for the modbus address, which we will put in as a read-only item. it's in cells[4].

                                            slaveDeviceRegisterName = rowInfo.Cells[5].Value.ToString().Trim();

                                            if (slaveDeviceType == 6001)
                                            {
                                                slaveDeviceRegister = ConvertADMSlaveDeviceRegisiterNameToSlaveDeviceRegister(slaveDeviceRegisterName);
                                            }
                                            else if (slaveDeviceType == 15002)
                                            {
                                                slaveDeviceRegister = ConvertBHMSlaveDeviceRegisiterNameToSlaveDeviceRegister(slaveDeviceRegisterName);
                                            }
                                            else if (slaveDeviceType == 505)
                                            {
                                                slaveDeviceRegister = ConvertPDMSlaveDeviceRegisiterNameToSlaveDeviceRegister(slaveDeviceRegisterName);
                                            }
                                            else if (slaveDeviceType == slaveDeviceTypeWhenUnknown)
                                            {
                                                if (this.undefinedDestinationRegistersKeyedByName.ContainsKey(slaveDeviceRegisterName))
                                                {
                                                    slaveDeviceRegister = this.undefinedDestinationRegistersKeyedByName[slaveDeviceRegisterName];
                                                }
                                                else
                                                {
                                                    string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nFailed to save a unique name for a register on an unknown device in the AddDataToTheDataTransferGrid() function.";
                                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                                    MessageBox.Show(errorMessage);
#endif
                                                }
                                            }
                                            else if(slaveDeviceType != 0)
                                            {
                                                string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nUndefined device type.";
                                                LogMessage.LogError(errorMessage);
#if DEBUG
                                                MessageBox.Show(errorMessage);
#endif
                                            }

                                            if (rowInfo.Cells[6].Value != null)
                                            {
                                                if (!Double.TryParse(rowInfo.Cells[6].Value.ToString(), out  slope))
                                                {
                                                    string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nCould not parse the slope.";
                                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                                    MessageBox.Show(errorMessage);
#endif
                                                    slope = 0;
                                                }
                                            }

                                            this.workingConfiguration.transferSetupConfigObjects[i].Working = enabled;
                                            this.workingConfiguration.transferSetupConfigObjects[i].SourceModuleNumber = masterDeviceModBusAddress;
                                            this.workingConfiguration.transferSetupConfigObjects[i].SourceRegisterNumber = masterDeviceRegister;
                                            this.workingConfiguration.transferSetupConfigObjects[i].DestinationModuleNumber = slaveDeviceModBusAddress;
                                            this.workingConfiguration.transferSetupConfigObjects[i].DestinationRegisterNumber = slaveDeviceRegister;
                                            this.workingConfiguration.transferSetupConfigObjects[i].Multiplier = slope;
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nthis.dataTransferRadGridView.Rows[" + i.ToString() + "] had too few cells.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nthis.dataTransferRadGridView did not have any rows.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nInpug List<Main_ConfigComponent_TransferSetup> did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteDataTransferTabDataToCurrentConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDataTransferPageMainRegisterNameDropDownListEntries()
        {
            try
            {
                if ((this.humidityRadGridView.RowCount > 0) && (this.temperatureRadGridView.RowCount > 6) &&
                   (this.currentRadGridView.RowCount > 2) && (this.voltageRadGridView.RowCount > 2) &&
                    (this.vibrationRadGridView.RowCount > 3) && (this.chassisTemperatureRadGridView.RowCount > 0) &&
                    (this.analogInRadGridView.RowCount > 5))
                {
                    this.dataTransferMasterDeviceRegisterNames.Clear();

                    this.dataTransferMasterDeviceRegisterNames.Add("No Value Set");

                    // vibration
                    if ((bool)(vibrationRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(vibration1DataTransferText);
                    }
                    if ((bool)(vibrationRadGridView.Rows[1].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(vibration2DataTransferText);
                    }
                    if ((bool)(vibrationRadGridView.Rows[2].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(vibration3DataTransferText);
                    }
                    if ((bool)(vibrationRadGridView.Rows[3].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(vibration4DataTransferText);
                    }

                    // analog in
                    if ((bool)(analogInRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn1DataTransferText);
                    }
                    if ((bool)(analogInRadGridView.Rows[1].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn2DataTransferText);
                    }
                    if ((bool)(analogInRadGridView.Rows[2].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn3DataTransferText);
                    }
                    if ((bool)(analogInRadGridView.Rows[3].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn4DataTransferText);
                    }
                    if ((bool)(analogInRadGridView.Rows[4].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn5DataTransferText);
                    }
                    if ((bool)(analogInRadGridView.Rows[5].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(analogIn6DataTransferText);
                    }

                    // current
                    if ((bool)(currentRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent1DataTransferText);
                    }
                    if ((bool)(currentRadGridView.Rows[1].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent2DataTransferText);
                    }
                    if ((bool)(currentRadGridView.Rows[2].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(loadCurrent3DataTransferText);
                    }

                    // voltage
                    if ((bool)(voltageRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(voltage1DataTransferText);
                    }
                    if ((bool)(voltageRadGridView.Rows[1].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(voltage2DataTransferText);
                    }
                    if ((bool)(voltageRadGridView.Rows[2].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(voltage3DataTransferText);
                    }

                    // humidity
                    if ((bool)(humidityRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(humidityDataTransferText);
                    }

                    // temperature
                    if ((bool)(temperatureRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp1DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[1].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp2DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[2].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp3DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[3].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp4DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[4].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp5DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[5].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp6DataTransferText);
                    }
                    if ((bool)(temperatureRadGridView.Rows[6].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(temp7DataTransferText);
                    }

                    // chassis temperature
                    if ((bool)(chassisTemperatureRadGridView.Rows[0].Cells[1].Value) == true)
                    {
                        this.dataTransferMasterDeviceRegisterNames.Add(chassisTempDataTransferText);
                    }

                }
                else
                {
                    string errorMessage = "Potential error in Main_MonitorConfiguration.UpdateDataTransferPageMainRegisterNameDropDownListEntries()\nDid not have expected number of rows in some analog input entries.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateDataTransferPageMainRegisterNameDropDownListEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDataTransferPageRadGridViewMainRegisterNames()
        {
            try
            {
                // have to check to be sure that the current entries are feasible and correct, given which entries are
                // selected on the analog inputs page, and which type of monitor is selected as the slave device.

                int count = this.dataTransferRadGridView.Rows.Count;
                string mainMonitorRegisterName;

                if (count >= 10)
                {
                    for (int i = 0; i < count; i++)
                    {
                        mainMonitorRegisterName = this.dataTransferRadGridView.Rows[i].Cells[2].Value.ToString().Trim();
                        if (this.dataTransferMasterDeviceRegisterNames.IndexOf(mainMonitorRegisterName) == -1)
                        {
                            this.dataTransferRadGridView.Rows[i].Cells[2].Value = this.dataTransferMasterDeviceRegisterNames[0];
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.UpdateDataTransferPageRadGridViewMainRegisterNames()\nthis.dataTransferRadGridView did not have exactly 32 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateDataTransferPageRadGridViewMainRegisterNames()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void UpdateDataTransferPageSlaveMonitorInformation()
        {
            try
            {
                int count;
                string slaveDeviceName;
                int modbusAddress;
                int slaveDeviceType;
                string modBusAddressAsString;
                string slaveDeviceRegister;

                count = this.dataTransferRadGridView.Rows.Count;
                if (count > 0)
                {
                    UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                    UpdateDataTransferPageRadGridViewMainRegisterNames();
                    for (int i = 0; i < count; i++)
                    {
                        modbusAddress = 0;
                        slaveDeviceType = 0;
                        slaveDeviceRegister = string.Empty;

                        modBusAddressAsString = this.dataTransferRadGridView.Rows[i].Cells[4].Value.ToString().Trim();
                        if (Int32.TryParse(modBusAddressAsString, out modbusAddress))
                        {
                            slaveDeviceName = FindUniqueMonitorNameWithInputModBusAddress(modbusAddress);
                            if (slaveDeviceName.CompareTo(string.Empty) == 0)
                            {
                                modbusAddress = 0;
                                slaveDeviceType = 0;
                                slaveDeviceName = noneDataTransferText;
                                slaveDeviceRegister = noValueSetDataTransferText;
                            }
                            else
                            {
                                slaveDeviceType = this.monitorsByUniqueMonitorName[slaveDeviceName].monitorTypeAsInteger;
                                slaveDeviceRegister = this.dataTransferRadGridView.Rows[i].Cells[5].Value.ToString().Trim();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.UpdateDataTransferPageSlaveMonitorInformation()\nCould not parse modbus address in this.dataTransferRadGridView.Rows[" +
                                i.ToString() + "], value was " + modBusAddressAsString;
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            break;
                        }

                        this.dataTransferRadGridView.Rows[i].Cells[3].Value = slaveDeviceName;
                        this.dataTransferRadGridView.Rows[i].Cells[4].Value = modbusAddress;
                        this.dataTransferRadGridView.Rows[i].Cells[5].Value = slaveDeviceRegister;

                        //slaveDeviceName = this.dataTransferRadGridView.Rows[i].Cells[3].Value.ToString().Trim();
                        //if (this.monitorsByUniqueMonitorName.ContainsKey(slaveDeviceName))
                        //{
                        //    modbusAddress = this.monitorsByUniqueMonitorName[slaveDeviceName].modBusAddress;
                        //}
                        //else
                        //{
                        //    this.dataTransferRadGridView.Rows[i].Cells[3].Value = "None";
                        //}
                        //this.dataTransferRadGridView.Rows[i].Cells[4].Value = modbusAddress;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateDataTransferPageSlaveMonitorInformation()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private string FindUniqueMonitorNameWithInputModBusAddress(int modBusAddress)
        {
            string uniqueMonitorName = string.Empty;

            foreach (KeyValuePair<string, MonitorInfo> kvPair in this.monitorsByUniqueMonitorName)
            {
                if (kvPair.Value.modBusAddress == modBusAddress)
                {
                    uniqueMonitorName = kvPair.Key;
                    break;
                }
            }
            return uniqueMonitorName;
        }

        private bool MissingValueInDataTransferRadGridView()
        {
            bool valueIsMissing = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.dataTransferRadGridView.RowCount;
                int i;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.dataTransferRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    if (rowInfo.Cells.Count > 6)
                    {
                        if (rowInfo.Cells[6].Value == null)
                        {
                            SelectDataTransferTab();
                            rowInfo.Cells[6].IsSelected = true;
                            RadMessageBox.SetThemeName("office2007BlackTheme");
                            RadMessageBox.Show(this, emptyCellErrorMessage);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInDataTransferRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return valueIsMissing;
        }

        private bool ErrorIsPresentInADataTransferTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                int count;
                bool isEnabled;
                string mainRegisterName;
                string slaveDeviceName;
                int slaveDeviceType = 0;
                string slaveRegisterName;
                count = this.dataTransferRadGridView.Rows.Count;
                if (count >= 10)
                {
                    errorIsPresent = MissingValueInDataTransferRadGridView();
                    if (!errorIsPresent)
                    {
                        UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                        UpdateDataTransferPageRadGridViewMainRegisterNames();
                        for (int i = 0; i < count; i++)
                        {
                            isEnabled = (bool)(this.dataTransferRadGridView.Rows[i].Cells[1].Value);
                            if (isEnabled)
                            {
                                slaveDeviceType = 0;
                                mainRegisterName = this.dataTransferRadGridView.Rows[i].Cells[2].Value.ToString().Trim();
                                slaveDeviceName = this.dataTransferRadGridView.Rows[i].Cells[3].Value.ToString().Trim();
                                slaveRegisterName = this.dataTransferRadGridView.Rows[i].Cells[5].Value.ToString().Trim();
                                if (mainRegisterName.CompareTo(noValueSetDataTransferText) == 0)
                                {
                                    this.dataTransferRadGridView.Rows[i].Cells[2].IsSelected = true;
                                    configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                    errorIsPresent = true;
                                    break;
                                }

                                /// slave device, which is the destination for the data transfer
                                if (this.monitorsByUniqueMonitorName.ContainsKey(slaveDeviceName))
                                {
                                    // recall we may have added some undefined monitors to the entry, or more likely had some from a bad configuration made outside of athena
                                    if (!slaveDeviceName.Contains(undefinedDeviceNameText))
                                    {
                                        slaveDeviceType = this.monitorsByUniqueMonitorName[slaveDeviceName].monitorTypeAsInteger;
                                    }
                                    else
                                    {
                                        // slave device existed in the data transfer tab, but was never defined in the system configuration
                                        this.dataTransferRadGridView.Rows[i].Cells[3].IsSelected = true;
                                        configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                        RadMessageBox.SetThemeName("office2007BlackTheme");
                                        RadMessageBox.Show(this, undefinedDeviceInDataTransferGridText);
                                        errorIsPresent = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    // slave device completely undefined
                                    this.dataTransferRadGridView.Rows[i].Cells[3].IsSelected = true;
                                    configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                    errorIsPresent = true;
                                    break;
                                }

                                //// slave destination register
                                //if ((slaveRegisterName.Contains(undefinedDeviceNameText) || this.undefinedDestinationRegistersKeyedByName.ContainsKey(slaveRegisterName))
                                //{
                                //    this.dataTransferRadGridView.Rows[i].Cells[3].IsSelected = true;
                                //    configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                //    RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                //    errorIsPresent = true;
                                //    break;
                                //}
                                

                                if (slaveRegisterName.Contains(noValueSetDataTransferText))
                                {
                                    this.dataTransferRadGridView.Rows[i].Cells[5].IsSelected = true;
                                    configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                    errorIsPresent = true;
                                    break;
                                }

                                /// we don't need all this code, since we use the same name when the destination register
                                /// is undefined.
                                
                                //if (slaveDeviceType == 6001)
                                //{
                                //    if (slaveRegisterName.CompareTo(this.admDynamicsSlaveRegisterNames[0]) == 0)
                                //    {
                                //        this.dataTransferRadGridView.Rows[i].Cells[5].IsSelected = true;
                                //        configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                //        RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                //        errorIsPresent = true;
                                //        break;
                                //    }
                                //}
                                //else if (slaveDeviceType == 15002)
                                //{
                                //    if (slaveRegisterName.CompareTo(this.bhmDynamicsSlaveRegisterNames[0]) == 0)
                                //    {
                                //        this.dataTransferRadGridView.Rows[i].Cells[5].IsSelected = true;
                                //        configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                //        RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                //        errorIsPresent = true;
                                //        break;
                                //    }
                                //}
                                //else if (slaveDeviceType == 505)
                                //{
                                //    if (slaveRegisterName.CompareTo(this.pdmDynamicsSlaveRegisterNames[0]) == 0)
                                //    {
                                //        this.dataTransferRadGridView.Rows[i].Cells[5].IsSelected = true;
                                //        configurationRadPageView.SelectedPage = dataTransferRadPageViewPage;
                                //        RadMessageBox.Show(this, incorrectDataTransferTabEntryMessage);
                                //        errorIsPresent = true;
                                //        break;
                                //    }
                                //}
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.CheckDataTransferRadGridViewElementsForCorrectness()\nthis.dataTransferRadGridView did not have exactly 32 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CheckDataTransferRadGridViewElementsForCorrectness()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void dataTransferRadGridView_CellEditorInitialized(object sender, GridViewCellEventArgs e)
        {
            try
            {
                string uniqueMonitorName;
                string slaveRegisterName;
                int monitorType = 0;
                {
                    var editor = e.ActiveEditor as RadDropDownListEditor;
                    if (editor != null)
                    {
                        var editorElement = editor.EditorElement as RadDropDownListEditorElement;
                        if (editorElement != null)
                        {
                            editorElement.SelectedIndexChanged -= new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
                            // we're only interested in changing the slave device register entries dynamically
                            uniqueMonitorName = e.Row.Cells[3].Value.ToString().Trim();

                            if (this.monitorsByUniqueMonitorName.ContainsKey(uniqueMonitorName))// || this.undefinedMonitorsKeyedByName.ContainsKey(uniqueMonitorName))
                            {
                                //if (this.monitorsByUniqueMonitorName.ContainsKey(uniqueMonitorName))
                                //{
                                    monitorType = this.monitorsByUniqueMonitorName[uniqueMonitorName].monitorTypeAsInteger;
                                //}
                               
                                //else if (this.undefinedMonitorsKeyedByName.ContainsKey(uniqueMonitorName))
                                //{
                                //    monitorType = this.slaveDeviceTypeWhenUnknown;
                                //}
                                if (e.ColumnIndex == 5)
                                {
                                    slaveRegisterName = e.Row.Cells[5].Value.ToString().Trim();
                                    if (monitorType == 6001)
                                    {
                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.admDynamicsSlaveRegisterNames);
                                        editorElement.SelectedIndex = this.admDynamicsSlaveRegisterNames.IndexOf(slaveRegisterName); ;
                                    }
                                    else if (monitorType == 15002)
                                    {
                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.bhmDynamicsSlaveRegisterNames);
                                        e.Row.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.bhmDynamicsSlaveRegisterNames, e.Row.Cells[5].Value.ToString());
                                        editorElement.SelectedIndex = this.bhmDynamicsSlaveRegisterNames.IndexOf(slaveRegisterName);
                                    }
                                    else if (monitorType == 505)
                                    {
                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.pdmDynamicsSlaveRegisterNames);
                                        editorElement.SelectedIndex = this.pdmDynamicsSlaveRegisterNames.IndexOf(slaveRegisterName); ;
                                    }
                                    else if (monitorType == slaveDeviceTypeWhenUnknown)
                                    {
                                        editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.undefinedMonitorsDynamicsSlaveRegisterNames);
                                        editorElement.SelectedIndex = this.undefinedMonitorsDynamicsSlaveRegisterNames.IndexOf(slaveRegisterName); ;
                                    }
                                    else
                                    {
                                        editorElement.DataSource = new string[] { noValueSetDataTransferText };
                                    }
                                }
                                else if (e.ColumnIndex == 3)
                                {
                                    editorElement.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
                                    editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.monitorUniqueNames);
                                    if (this.monitorUniqueNames.Contains(uniqueMonitorName))
                                    {
                                        editorElement.SelectedIndex = this.monitorUniqueNames.IndexOf(uniqueMonitorName);
                                    }
                                    //else if (this.undefinedMonitorsKeyedByName.ContainsKey(uniqueMonitorName))
                                    //{
                                    //    editorElement.SelectedIndex = this.monitorUniqueNames.Count + this.undefinedMonitorsNames.IndexOf(uniqueMonitorName) - 1;
                                    //}
                                    else
                                    {
                                        editorElement.SelectedIndex = this.monitorUniqueNames.IndexOf(noneDataTransferText);
                                    }
                                }
                                else if (e.ColumnIndex == 2)
                                {
                                    // editorElement.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
                                    editorElement.DataSource = ConversionMethods.ConvertListOfStringsToArrayOfStrings(this.dataTransferMasterDeviceRegisterNames);
                                    editorElement.SelectedIndex = this.dataTransferMasterDeviceRegisterNames.IndexOf(e.Row.Cells[2].Value.ToString().Trim());
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.dataTransferRadGridView_CellEditorInitialized(object, GridViewCellEventArgs)\nCould not find monitor name from grid in the unique names list.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.dataTransferRadGridView_CellEditorInitialized(object, GridViewCellEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private int FindIndexOfCurrentMonitorNameInSelectedGrid(string monitorName)
        //{
        //    int index = 0;
        //    for (int i = 0; i < this.monitorUniqueNames.Count; i++)
        //    {
        //        if (this.monitorUniqueNames[i].CompareTo(monitorName) == 0)
        //        {
        //            index = i;
        //            break;
        //        }
        //    }
        //    return index;
        //}

//        private void dataTransferRadGridView_CellValueChanged(object sender, GridViewCellEventArgs e)
//        {
//            try
//            {
//                string monitorType;
//                if (e.ColumnIndex == 3)
//                {
//                    monitorType = e.Row.Cells[3].Value.ToString().Trim().Substring(0, 3);
//                    // we only need to check if the new value is a BHM, because the PDM contains
//                    // all possible dynamics variables
//                    if (monitorType.CompareTo("BHM") == 0)
//                    {
//                        e.Row.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.bhmDynamicsSlaveRegisterNames, e.Row.Cells[5].Value.ToString());
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.dataTransferRadGridView_CellValueChanged(object, GridViewCellEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private string CorrectSlaveRegisterNameIfNecessary(List<string> correctRegisterNames, string registerName)
        {
            string newRegisterName = registerName;
            try
            {
                if (!correctRegisterNames.Contains(registerName))
                {
                    newRegisterName = noValueSetDataTransferText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CorrectSlaveRegisterNameIfNecessary(List<string>, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return newRegisterName;
        }

//        private void dataTransferRadGridView_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
//        {
//            try
//            {
//                if (e.ColumnIndex == 3)
//                {
//                    IInputEditor editor = this.dataTransferRadGridView.ActiveEditor;
//                    if (editor != null)
//                    {
//                        if (editor.GetType() == typeof(RadDropDownListEditor))
//                        {
//                            RadDropDownListElement comboElement = (RadDropDownListElement)((RadDropDownListEditor)editor).EditorElement;
//                            comboElement.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.dataTransferRadGridView_CellBeginEdit(object, GridViewCellCancelEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void dataTransferRadGridView_CellEndEdit(object sender, GridViewCellEventArgs e)
//        {
//            try
//            {
//                if (e.ColumnIndex == 3)
//                {
//                    IInputEditor editor = this.dataTransferRadGridView.ActiveEditor;
//                    if (editor != null)
//                    {
//                        if (editor.GetType() == typeof(RadDropDownListEditor))
//                        {
//                            RadDropDownListElement comboElement = (RadDropDownListElement)((RadDropDownListEditor)editor).EditorElement;
//                            comboElement.SelectedIndexChanged -= new Telerik.WinControls.UI.Data.PositionChangedEventHandler(editorElement_SelectedIndexChanged);
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.dataTransferRadGridView_CellEndEdit(object, GridViewCellEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        void editorElement_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int index = (int)(((RadDropDownListElement)sender).SelectedIndex);
                if (index > -1)
                {
                    if (this.dataTransferRadGridView.CurrentColumn.Index == 3)
                    {
                        CorrectSlaveRegistersForCurrentRow(index);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.editorElement_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CorrectSlaveRegistersForCurrentRow(int selectedIndex)
        {
            try
            {
                string monitorUniqueName;
                int monitorType = 0;
                int modBusAddress = 0;
                GridViewRowInfo rowInfo;
                int numberOfMonitorsDefinedInSystemConfigTab = this.monitorUniqueNames.Count;
                string destinationRegisterName;
                if (this.dataTransferRadGridView != null)
                {
                    if (this.dataTransferRadGridView.ColumnCount > 5)
                    {
                        rowInfo = this.dataTransferRadGridView.CurrentRow;
                        // reading the value directly doesn't work at this point, because the text has not yet changed
                        // to the new value.
                        //monitorUniqueName = rowInfo.Cells[3].Value.ToString().Trim();\\
                        //if (selectedIndex < numberOfMonitorsDefinedInSystemConfigTab)
                        //{
                            monitorUniqueName = this.monitorUniqueNames[selectedIndex];
                        //}
                        //else
                        //{
                        //    monitorUniqueName = this.undefinedMonitorsNames[selectedIndex - numberOfMonitorsDefinedInSystemConfigTab + 1];
                        //}

                        if (this.monitorsByUniqueMonitorName.ContainsKey(monitorUniqueName))
                        {
                            modBusAddress = this.monitorsByUniqueMonitorName[monitorUniqueName].modBusAddress;
                            monitorType = this.monitorsByUniqueMonitorName[monitorUniqueName].monitorTypeAsInteger;
                        }
                        //else if (this.undefinedMonitorsKeyedByName.ContainsKey(monitorUniqueName))
                        //{
                        //    modBusAddress = this.undefinedMonitorsKeyedByName[monitorUniqueName];
                        //    monitorType = slaveDeviceTypeWhenUnknown;
                        //}
                        else
                        {
                            modBusAddress = 0;
                            monitorType = 0;
                        }

                        rowInfo.Cells[4].Value = modBusAddress;

                        destinationRegisterName = rowInfo.Cells[5].Value.ToString().Trim();

                        if (monitorType == 6001)
                        {
                            rowInfo.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.admDynamicsSlaveRegisterNames, destinationRegisterName);
                        }
                        else if (monitorType == 15002)
                        {
                            rowInfo.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.bhmDynamicsSlaveRegisterNames, destinationRegisterName);
                        }
                        else if (monitorType == slaveDeviceTypeWhenUnknown)
                        {
                            rowInfo.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.undefinedMonitorsDynamicsSlaveRegisterNames, destinationRegisterName);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.CorrectSlaveRegistersForCurrentRow(int)\nThere were not enough columns declared in this.dataTransferRadGridView.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.CorrectSlaveRegistersForCurrentRow(int)\nthis.dataTransferRadGridView was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CorrectSlaveRegistersForCurrentRow(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CorrectSlaveRegistersForAllRowEntries()
        {
            try
            {
                int rowCount;
                string monitorUniqueName;
                int monitorType;
                int modBusAddress = 0;
                GridViewRowInfo rowInfo;
                if (this.dataTransferRadGridView != null)
                {
                    rowInfo = this.dataTransferRadGridView.CurrentRow;
                    monitorUniqueName = rowInfo.Cells[3].Value.ToString().Trim();

                    if (this.dataTransferRadGridView.ColumnCount > 5)
                    {
                        rowCount = this.dataTransferRadGridView.Rows.Count;
                        for (int row = 0; row < rowCount; row++)
                        {
                            rowInfo = this.dataTransferRadGridView.Rows[row];
                            monitorUniqueName = rowInfo.Cells[3].Value.ToString().Trim();
                            if (this.monitorsByUniqueMonitorName.ContainsKey(monitorUniqueName))
                            {
                                modBusAddress = this.monitorsByUniqueMonitorName[monitorUniqueName].modBusAddress;
                                monitorType = this.monitorsByUniqueMonitorName[monitorUniqueName].monitorTypeAsInteger;
                            }
                            else
                            {
                                modBusAddress = 0;
                                monitorType = 0;
                            }

                            rowInfo.Cells[4].Value = modBusAddress;

                            if (monitorType == 6001)
                            {
                                rowInfo.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.admDynamicsSlaveRegisterNames, rowInfo.Cells[5].Value.ToString());
                            }
                            else if (monitorType == 15002)
                            {
                                rowInfo.Cells[5].Value = CorrectSlaveRegisterNameIfNecessary(this.bhmDynamicsSlaveRegisterNames, rowInfo.Cells[5].Value.ToString());
                            }

                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.CorrectSlaveRegistersForAllRowEntries()\nThere were not enough columns declared in this.dataTransferRadGridView.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.CorrectSlaveRegistersForAllRowEntries()\nthis.dataTransferRadGridView was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CorrectSlaveRegistersForAllRowEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void dataTransferRadGridView_ValueChanged(object sender, EventArgs e)
        {
            CorrectSlaveRegistersForAllRowEntries();
        }

        private void dataTransferRadGridView_TextChanged(object sender, EventArgs e)
        {
            CorrectSlaveRegistersForAllRowEntries();
        }
    }

    public class MonitorInfo
    {
        public int monitorTypeAsInteger;
        public int modBusAddress;
    }

    public class TransferSetupEntry
    {
        public int transferNumber;
        public int masterModbusAddress;
        public int masterSourceRegister;
        public int slaveModbusAddress;
        public int slaveDestinationRegister;
        public double slope;
    }
}
