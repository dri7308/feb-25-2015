﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;
using MonitorUtilities;
using FormatConversion;

namespace MainMonitorUtilities
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string whsGeneralSettingsRadPageViewPageText = "General Settings";
        private static string whsCalculationEnableDisableRadGroupBoxText = "WHS Calculations";
        private static string enableMonitoringRadRadioButtonText = "Enable";
        private static string disableMonitoringRadRadioButtonText = "Disable";
        private static string firmwareVersionRadGroupBoxText = "Firmware Version";
        private static string tempRiseSettingsRadGroupBoxText = "WHS Winding Settings";
        private static string maximumRatedCurrentRadGroupBoxText = "Maximum Rated Current";
        private static string agingCalculationMethodRadGroupBoxText = "Aging calculation method";
        private static string temperatureInputsRadGroupBoxText = "Top Oil Temperature Input";
        private static string hotSpotFactorGroupBoxLabelText = "Hot Spot Factor";
        private static string hotSpotLabelText = "Hot Spot Factor";
        private static string firmwareVersionUnknownText = "unknown";
        private static string temperatureRiseOverTopOilText = "Winding Rise Over Top ";
        private static string alarmListLabelText = "Current Alarm List";
        private static string updateAlarmListButtonText = "Update Alarm List";
        private static string clearAlarmListButtonText = "Clear Alarms";
        private static string errorListLabelText = "Error List";
        private static string updateErrorListButtonText = "Update Error List";

        // for grid Headers and grid
        private static string windingText = "Winding";       
        private static string windingExponentHeaderText = "Exponent";
        private static string windingTimeConstantHeaderText = "Time Constant (Min)";
        private static string inputText = "Current Input";
        private static string maximumRatedCurrentText = "Maximum RatedCurrent (Amps)";     
        private static string windingPreviousAgingHeaderText = "Previous Aging (Days)";
        private static string currentInputNoneText = "None";
        private static string currentInputIAText = "I-A";
        private static string currentInputIBText = "I-B";
        private static string currentInputICText = "I-C";
        private static string highVoltagePhaseAText = "Winding 1";
        private static string highVoltagePhaseBText = "Winding 2";
        private static string highVoltagePhaseCText = "Winding 3";

        //For Oil Temp Combo Box
        private static string topOilTempInputNoneText = "None";
        private static string topOilTempInputRTDOneText = "RTD 01";
        private static string topOilTempInputRTDTwoText = "RTD 02";
        private static string topOilTempInputRTDThreeText = "RTD 03";
        private static string topOilTempInputRTDFourText = "RTD 04";
        private static string topOilTempInputRTDFiveText = "RTD 05";
        private static string topOilTempInputRTDSixText = "RTD 06";
        private static string topOilTempInputRTDSevenText = "RTD 07";

        // Ageing Calculation Method Combo Box
        private static string agingCalculationMethodIECText = "IEC";
        private static string agingCalculationMethodIEEESixtyFiveText = "IEEE 65";
        private static string agingCalculationMethodIEEEFiftyFiveText = "IEEE 55";
        private static string agingCalculationMethodNomexText = "Nomex";

        // for alarm List

        private static string AlarmListBit0 = "None"; 
        private static string AlarmListBit1 = "WHS Max High";
        private static string AlarmListBit2 = "WHS Max High - High";
        private static string AlarmListBit3 = "Top Oil Temp High";
        private static string AlarmListBit4 = "Top Oil Temp High - High";
        private static string AlarmListBit5 = "Group 1 Low Current";
        private static string AlarmListBit6 = "Group 1 High Current";
        private static string AlarmListBit7 = "Group 2 Low Current";
        private static string AlarmListBit8 = "Group 2 High Current";
        private static string AlarmListBit9 = "";
        private static string AlarmListBit10 = "";
        private static string AlarmListBit11 = "";
        private static string AlarmListBit12 = "";
        private static string AlarmListBit13 = "";
        private static string AlarmListBit14 = "";
        private static string AlarmListBit15 = "";
        private static string AlarmListBit16 = "";

        // for Error List

        private static string ErrorListBit0 = "None";
        private static string ErrorListBit1 = "Top Oil Temperature Channel Not Configured";
        private static string ErrorListBit2 = "Top Oil Temperature Channel Not Enabled";
        private static string ErrorListBit3 = "Top Oil Temperature Sensor Failure";
        private static string ErrorListBit4 = "No Current Channel Configured";
        private static string ErrorListBit5 = "Current Channel I-A configured, but not enabled";
        private static string ErrorListBit6 = "Current Channel I-B configured, but not enabled";
        private static string ErrorListBit7 = "Current Channel I-C configured, but not enabled";
        private static string ErrorListBit8 = "Group 1 enabled with Monitoring, but Current Channel I-01 not enabled";
        private static string ErrorListBit9 = "Group 2 enabled with Monitoring, but Current Channel I-02 not enabled";
        private static string ErrorListBit10 = "Group 1 Current Enabled, but not calibrated";
        private static string ErrorListBit11 = "Group 2 Current Enabled, but not calibrated";
        private static string ErrorListBit12 = "Group 1 current outside reasonable limits";
        private static string ErrorListBit13 = "Group 2 current outside reasonable limits";
        private static string ErrorListBit14 = "";
        private static string ErrorListBit15 = "";
        private static string ErrorListBit16 = "";
        private static string highVoltageText = "High Voltage";
        private static string maximumRatedCurrentForHighVoltageOutOfRangeText = "The maximum rated current for the high voltage windings must be between 1 and 5000 Amps";

        private static string PreviousAgingDaysPhaseATextRadLabelText = "Aging Phase A";
        private static string PreviousAgingDaysPhaseBTextRadLabelText = "Aging Phase B";
        private static string PreviousAgingDaysPhaseCTextRadLabelText = "Aging Phase C";

        private int tempRiseSettingsRadGroupBoxNormalHeight = 300;
        private int tempRiseSettingsRadGroupBoxShorterHeight = 156;
        private int maximumRatedCurrentRadGroupBoxNormalHeight = 300;
        private int maximumRatedCurrentRadGroupBoxShorterHeight = 156;

        private void AssignStringValuesToWhsGeneralSettingsInterfaceObjects()
        {
            whsGeneralSettingsRadPageViewPage.Text = whsGeneralSettingsRadPageViewPageText;
            whsCalculationEnableDisableRadGroupBox.Text = whsCalculationEnableDisableRadGroupBoxText;
            enableMonitoringRadRadioButton.Text = enableMonitoringRadRadioButtonText;
            disableMonitoringRadRadioButton.Text = disableMonitoringRadRadioButtonText;
            firmwareVersionRadGroupBox.Text = firmwareVersionRadGroupBoxText;
            agingCalculationMethodRadGroupBox.Text = agingCalculationMethodRadGroupBoxText;
            TopOilGroupBox.Text = temperatureInputsRadGroupBoxText;            
            HotSpotFactorGroupBox.Text = hotSpotFactorGroupBoxLabelText;
            hotSpotFactorLabelText.Text = hotSpotLabelText;
            tempRiseSettingsRadGroupBox.Text = tempRiseSettingsRadGroupBoxText;
            AlarmLabel.Text = alarmListLabelText;
            UpdateAlarmListButton.Text = updateAlarmListButtonText;
            ClearAlarmListButton.Text = clearAlarmListButtonText;
            ErrorListLabel.Text = errorListLabelText;
            updateErrorButton.Text = updateErrorListButtonText;

            databaseInteractionWhsGeneralSettingsTabRadGroupBox.Text = databaseInteractionGroupBoxText;
            deviceInteractionWhsGeneralSettingsTabRadGroupBox.Text = deviceInteractionGroupBoxText;

            /// These use generic strings declared in the main code file
            availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
            saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
            //saveDeviceConfigurationWhsGeneralSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
            loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
            programDeviceWhsGeneralSettingsTabRadButton.Text = programDeviceRadButtonText;
            loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
            maximumRatedCurrentRadGroupBox.Text = maximumRatedCurrentRadGroupBoxText;
            
            //initializeWorkingConfigurationWhsGeneralSettingsTabRadButton.Text = initializeWorkingConfigurationRadButtonText;
            deleteSelectedConfigurationGeneralSettingsTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
        }

        private static void AssignValuesToWhsGeneralSettingsInternalStaticStrings()
        {
            whsGeneralSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsRadPageViewPageText", whsGeneralSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
            whsCalculationEnableDisableRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsCalculationEnableDisableRadGroupBoxText", whsCalculationEnableDisableRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
            disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
            firmwareVersionUnknownText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceFirmwareVersionUnknownText", firmwareVersionUnknownText, htmlFontType, htmlStandardFontSize, "");
            firmwareVersionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceFirmwareVersionRadGroupBoxText", firmwareVersionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            tempRiseSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTempRiseSettingsRadGroupBoxText", tempRiseSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            maximumRatedCurrentRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceMaximumRatedCurrentRadGroupBoxText", maximumRatedCurrentRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            temperatureRiseOverTopOilText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTemperatureRiseOverTopOilText", temperatureRiseOverTopOilText, htmlFontType, htmlStandardFontSize, "");
            maximumRatedCurrentText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceMaximumRatedCurrentText", maximumRatedCurrentText, htmlFontType, htmlStandardFontSize, "");
            windingText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWindingText", windingText, htmlFontType, htmlStandardFontSize, "");
            highVoltageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceHighVoltageText", highVoltageText, htmlFontType, htmlStandardFontSize, "");
            highVoltagePhaseAText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceHighVoltagePhaseAText", highVoltagePhaseAText, htmlFontType, htmlStandardFontSize, "");
            highVoltagePhaseBText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceHighVoltagePhaseBText", highVoltagePhaseBText, htmlFontType, htmlStandardFontSize, "");
            highVoltagePhaseCText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceHighVoltagePhaseCText", highVoltagePhaseCText, htmlFontType, htmlStandardFontSize, "");
            maximumRatedCurrentForHighVoltageOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceMaximumRatedCurrentForHighVoltageOutOfRangeText", maximumRatedCurrentForHighVoltageOutOfRangeText, "", "", "");

            topOilTempInputNoneText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputNoneText", topOilTempInputNoneText, "", "", "");
            topOilTempInputRTDOneText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDOneText", topOilTempInputRTDOneText, "", "", "");
            topOilTempInputRTDTwoText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDTwoText", topOilTempInputRTDTwoText, "", "", "");
            topOilTempInputRTDThreeText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDThreeText", topOilTempInputRTDThreeText, "", "", "");
            topOilTempInputRTDFourText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDFourText", topOilTempInputRTDFourText, "", "", "");
            topOilTempInputRTDFiveText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDFiveText", topOilTempInputRTDFiveText, "", "", "");
            topOilTempInputRTDSixText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDSixText", topOilTempInputRTDSixText, "", "", "");
            topOilTempInputRTDSevenText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceTopOilTempInputRTDSevenText", topOilTempInputRTDSevenText, "", "", "");

            agingCalculationMethodIECText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAgingCalculationMethodIECText", agingCalculationMethodIECText, "", "", "");
            agingCalculationMethodIEEESixtyFiveText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAgingCalculationMethodIEEESixtyFiveText", agingCalculationMethodIEEESixtyFiveText, "", "", "");
            agingCalculationMethodIEEEFiftyFiveText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAgingCalculationMethodIEEEFiftyFiveText", agingCalculationMethodIEEEFiftyFiveText, "", "", "");
            agingCalculationMethodNomexText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAgingCalculationMethodNomexText", agingCalculationMethodNomexText, "", "", "");
            

            currentInputNoneText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceCurrentInputNoneText", currentInputNoneText, htmlFontType, htmlStandardFontSize, "");
            currentInputIAText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceCurrentInputIAText", currentInputIAText, htmlFontType, htmlStandardFontSize, "");
            currentInputIBText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceCurrentInputIBText", currentInputIBText, htmlFontType, htmlStandardFontSize, "");
            currentInputICText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceCurrentInputICText", currentInputICText, htmlFontType, htmlStandardFontSize, "");

            inputText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceInputText", inputText, htmlFontType, htmlStandardFontSize, "");

            temperatureInputsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingstemperatureInputsRadGroupBoxText", temperatureInputsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            hotSpotFactorGroupBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingshotSpotFactorGroupBoxLabelText", hotSpotFactorGroupBoxLabelText, htmlFontType, htmlStandardFontSize, "");
            hotSpotLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingshotSpotLabelText", hotSpotLabelText, htmlFontType, htmlStandardFontSize, "");
            alarmListLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsalarmListLabelText", alarmListLabelText, htmlFontType, htmlStandardFontSize, "");
            updateAlarmListButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsupdateAlarmListButtonText", updateAlarmListButtonText, htmlFontType, htmlStandardFontSize, "");
            clearAlarmListButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsclearAlarmListButtonText", clearAlarmListButtonText, htmlFontType, htmlStandardFontSize, "");
            errorListLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingserrorListLabelText", errorListLabelText, htmlFontType, htmlStandardFontSize, "");
            updateErrorListButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsupdateErrorListButtonText", updateErrorListButtonText, htmlFontType, htmlStandardFontSize, "");
            windingText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingswindingText", windingText, htmlFontType, htmlStandardFontSize, "");
            windingExponentHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingswindingExponentHeaderText", windingExponentHeaderText, htmlFontType, htmlStandardFontSize, "");
            windingTimeConstantHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingswindingTimeConstantHeaderText", windingTimeConstantHeaderText, htmlFontType, htmlStandardFontSize, "");
            inputText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsinputText", inputText, htmlFontType, htmlStandardFontSize, "");
            maximumRatedCurrentText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsmaximumRatedCurrentText", maximumRatedCurrentText, htmlFontType, htmlStandardFontSize, "");
            windingPreviousAgingHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingswindingPreviousAgingHeaderText", windingPreviousAgingHeaderText, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit0 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit0", AlarmListBit0, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit1 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit1", AlarmListBit1, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit2 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit2", AlarmListBit2, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit3 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit3", AlarmListBit3, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit4 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit4", AlarmListBit4, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit5 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit5", AlarmListBit5, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit6 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit6", AlarmListBit6, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit7 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit7", AlarmListBit7, htmlFontType, htmlStandardFontSize, "");
            AlarmListBit8 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsAlarmListBit8", AlarmListBit8, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit0 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit0", ErrorListBit0, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit1 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit1", ErrorListBit1, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit2 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit2", ErrorListBit2, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit3 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit3", ErrorListBit3, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit4 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit4", ErrorListBit4, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit5 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit5", ErrorListBit5, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit6 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit6", ErrorListBit6, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit7 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit7", ErrorListBit7, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit8 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit8", ErrorListBit8, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit9 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit9", ErrorListBit9, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit10 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit10", ErrorListBit10, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit11 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit11", ErrorListBit11, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit12 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit12", ErrorListBit12, htmlFontType, htmlStandardFontSize, "");
            ErrorListBit13 = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettingsErrorListBit13", ErrorListBit13, htmlFontType, htmlStandardFontSize, "");
            //  xxx = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceWhsGeneralSettings", xxx, htmlFontType, htmlStandardFontSize, "");
        }

        private void SelectWhsGeneralSettingsTab()
        {
            configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;
        }

        private void InitializeWhsGeneralSettingsTabObjects()
        {
            try
            {
                InitializeTemperatureRiseOverTopOilRadGridView();
                // InitializeMaximumRatedCurrentRadGridView();
                topOilTemperatureComboBox.DataSource = new string[] {topOilTempInputNoneText, topOilTempInputRTDOneText, topOilTempInputRTDTwoText, topOilTempInputRTDThreeText, topOilTempInputRTDFourText, topOilTempInputRTDFiveText, topOilTempInputRTDSixText, topOilTempInputRTDSevenText };
                this.agingCalculationMethodRadDropDownList.DataSource = new string[] { agingCalculationMethodIECText, agingCalculationMethodIEEESixtyFiveText, agingCalculationMethodIEEEFiftyFiveText, agingCalculationMethodNomexText };
                
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        // enabled/disbaled
                        if (enableMonitoringRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            this.ratingsBitEncodingAsIntegerArray[15] = 1;
                        }
                        else
                        {
                            this.ratingsBitEncodingAsIntegerArray[15] = 0;
                        }

                        this.ratingsBitEncodingAsIntegerArray[14] = 0; // 3 phase txf
                        this.ratingsBitEncodingAsIntegerArray[13] = 0;  //std txf

                       
                       

                        WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration();
                        
                       
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration contains null member objects";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteConfigurationDataToWhsGeneralSettingsInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                string bitString;
                // enabled/disabled
                if (windingHotSpotSetup.FirmwareVersion > 0)
                {
                    this.firmwareVersionValueRadLabel.Text = Math.Round(windingHotSpotSetup.FirmwareVersion / 100.0, 2).ToString();
                }
                else
                {
                    this.firmwareVersionValueRadLabel.Text = firmwareVersionUnknownText;
                }

                bitString = windingHotSpotSetup.RatingAsBitString;
                if (bitString[15] == '1')
                {
                    enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else
                {
                    enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                }

                AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableWhsGeneralSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.enableMonitoringRadRadioButton.Enabled = true;
                this.disableMonitoringRadRadioButton.Enabled = true;

                this.transformerConfigurationRadDropDownList.Enabled = true;
                this.transformerTypeRadDropDownList.Enabled = true;
                this.temperatureRiseOverTopOilRadGridView.ReadOnly = false;
                this.maximumRatedCurrentRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableWhsGeneralSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.enableMonitoringRadRadioButton.Enabled = false;
                this.disableMonitoringRadRadioButton.Enabled = false;

                this.transformerConfigurationRadDropDownList.Enabled = false;
                this.transformerTypeRadDropDownList.Enabled = false;
                this.temperatureRiseOverTopOilRadGridView.ReadOnly = true;
                this.maximumRatedCurrentRadGridView.ReadOnly = true;             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool ErrorIsPresentInAWhsGeneralSettingsTabObject()
        {
            /// The values for the maximum rated current are not consistent for all voltages.  The high voltage case only allows
            /// 5K Amps for the maximum current, while the low voltage case allows for 50K Amps.  This makes sense since the ratio of the 
            /// voltage to current is inversely proportional.
            try
            {

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return false;
        }

        private void InitializeTemperatureRiseOverTopOilRadGridView()
        {
            try
            {
                this.temperatureRiseOverTopOilRadGridView.Columns.Clear();

                GridViewTextBoxColumn windingNameGridViewTextBoxColumn = new GridViewTextBoxColumn("WindingName");
                windingNameGridViewTextBoxColumn.Name = "WindingName";
                windingNameGridViewTextBoxColumn.HeaderText = windingText;
                windingNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                windingNameGridViewTextBoxColumn.Width = 80;
                windingNameGridViewTextBoxColumn.AllowSort = false;
                windingNameGridViewTextBoxColumn.IsPinned = true;
                windingNameGridViewTextBoxColumn.ReadOnly = true;
                windingNameGridViewTextBoxColumn.AllowResize = false;

                GridViewDecimalColumn temperatureRiseGridViewDecimalColumn = new GridViewDecimalColumn("TemperatureRiseOverTopOil");
                temperatureRiseGridViewDecimalColumn.Name = "TemperatureRiseOverTopOil";
                temperatureRiseGridViewDecimalColumn.HeaderText = temperatureRiseOverTopOilText;
                temperatureRiseGridViewDecimalColumn.Minimum = 1;
                temperatureRiseGridViewDecimalColumn.Maximum = 50;
                temperatureRiseGridViewDecimalColumn.DecimalPlaces = 1;
                temperatureRiseGridViewDecimalColumn.Step = .1M;
                temperatureRiseGridViewDecimalColumn.DisableHTMLRendering = false;
                temperatureRiseGridViewDecimalColumn.Width = 80;
                temperatureRiseGridViewDecimalColumn.AllowSort = false;
                temperatureRiseGridViewDecimalColumn.AllowResize = false;
                temperatureRiseGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn maximumRatedCurrentGridViewDecimalColumn = new GridViewDecimalColumn("MaximumRatedCurrent");
                maximumRatedCurrentGridViewDecimalColumn.Name = "MaximumRatedCurrent";
                maximumRatedCurrentGridViewDecimalColumn.HeaderText = maximumRatedCurrentText;
                maximumRatedCurrentGridViewDecimalColumn.DecimalPlaces = 0;
                maximumRatedCurrentGridViewDecimalColumn.Step = 1;
                maximumRatedCurrentGridViewDecimalColumn.Minimum = 1;
                maximumRatedCurrentGridViewDecimalColumn.Maximum = 50000;
                maximumRatedCurrentGridViewDecimalColumn.DisableHTMLRendering = false;
                maximumRatedCurrentGridViewDecimalColumn.Width = 80;
                maximumRatedCurrentGridViewDecimalColumn.AllowSort = false;
                maximumRatedCurrentGridViewDecimalColumn.AllowResize = false;
                maximumRatedCurrentGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn windingExponentGridViewDecimalColumn = new GridViewDecimalColumn("WindingExponent");
                windingExponentGridViewDecimalColumn.Name = "WindingExponent";
                windingExponentGridViewDecimalColumn.HeaderText = windingExponentHeaderText;
                windingExponentGridViewDecimalColumn.DecimalPlaces = 1;
                windingExponentGridViewDecimalColumn.Step = .1M;
                windingExponentGridViewDecimalColumn.Minimum = 0.5M;
                windingExponentGridViewDecimalColumn.Maximum = 2;
                windingExponentGridViewDecimalColumn.DisableHTMLRendering = false;
                windingExponentGridViewDecimalColumn.Width = 80;
                windingExponentGridViewDecimalColumn.AllowSort = false;
                windingExponentGridViewDecimalColumn.AllowResize = false;
                windingExponentGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn windingTimeConstantGridViewDecimalColumn = new GridViewDecimalColumn("TimeConstant");
                windingTimeConstantGridViewDecimalColumn.Name = "TimeConstant";
                windingTimeConstantGridViewDecimalColumn.HeaderText = windingTimeConstantHeaderText;
                windingTimeConstantGridViewDecimalColumn.DecimalPlaces = 1;
                windingTimeConstantGridViewDecimalColumn.Step = .1M;
                windingTimeConstantGridViewDecimalColumn.Minimum = 0.5M;
                windingTimeConstantGridViewDecimalColumn.Maximum = 20;
                windingTimeConstantGridViewDecimalColumn.DisableHTMLRendering = false;
                windingTimeConstantGridViewDecimalColumn.Width = 80;
                windingTimeConstantGridViewDecimalColumn.AllowSort = false;
                windingTimeConstantGridViewDecimalColumn.AllowResize = false;
                windingTimeConstantGridViewDecimalColumn.WrapText = true;

                GridViewComboBoxColumn inputGridViewComboBoxColumn = new GridViewComboBoxColumn("Input");
                inputGridViewComboBoxColumn.Name = "Input";
                inputGridViewComboBoxColumn.HeaderText = inputText;
                inputGridViewComboBoxColumn.DataSource = new string[] { currentInputNoneText, currentInputIAText, currentInputIBText, currentInputICText };
                inputGridViewComboBoxColumn.DisableHTMLRendering = false;
                inputGridViewComboBoxColumn.Width = 80;
                inputGridViewComboBoxColumn.AllowSort = false;
                inputGridViewComboBoxColumn.AllowResize = false;
                inputGridViewComboBoxColumn.WrapText = true;

                GridViewDecimalColumn windingPreviousAgeingDecimalColumn = new GridViewDecimalColumn("PreviousAging");
                windingPreviousAgeingDecimalColumn.Name = "PreviousAging";
                windingPreviousAgeingDecimalColumn.HeaderText = windingPreviousAgingHeaderText;
                windingPreviousAgeingDecimalColumn.DecimalPlaces = 1;
                windingPreviousAgeingDecimalColumn.Step = 1;
                windingPreviousAgeingDecimalColumn.Minimum = 0;
                windingPreviousAgeingDecimalColumn.Maximum = 65000;
                windingPreviousAgeingDecimalColumn.DisableHTMLRendering = false;
                windingPreviousAgeingDecimalColumn.Width = 80;
                windingPreviousAgeingDecimalColumn.AllowSort = false;
                windingPreviousAgeingDecimalColumn.AllowResize = false;
                windingPreviousAgeingDecimalColumn.WrapText = true;

                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingNameGridViewTextBoxColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(temperatureRiseGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(maximumRatedCurrentGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingExponentGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingTimeConstantGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(inputGridViewComboBoxColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingPreviousAgeingDecimalColumn);

                this.temperatureRiseOverTopOilRadGridView.TableElement.TableHeaderHeight = 50;
                this.temperatureRiseOverTopOilRadGridView.AllowColumnReorder = false;
                this.temperatureRiseOverTopOilRadGridView.AllowColumnChooser = false;
                this.temperatureRiseOverTopOilRadGridView.ShowGroupPanel = false;
                this.temperatureRiseOverTopOilRadGridView.EnableGrouping = false;
                this.temperatureRiseOverTopOilRadGridView.AllowAddNewRow = false;
                this.temperatureRiseOverTopOilRadGridView.AllowDeleteRow = false;
                this.temperatureRiseOverTopOilRadGridView.MultiSelect = true;
                this.temperatureRiseOverTopOilRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
                this.temperatureRiseOverTopOilRadGridView.AutoSize = true;
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeTemperatureRiseOverTopOilRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void AddDataToTemperatureRiseOverTopOilRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            string bitString;
         
            try
            {
                this.temperatureRiseOverTopOilRadGridView.Rows.Clear();
                this.temperatureRiseOverTopOilRadGridView.Rows.Clear();

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseAText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseA / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseA),
                    (decimal)(windingHotSpotSetup.Exponent / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseA / 4));

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseBText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseB / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseB),
                    (decimal)(windingHotSpotSetup.Exponent_B / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant_B / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseB / 4));

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseCText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseC / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseC),
                    (decimal)(windingHotSpotSetup.Exponent_C / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant_C / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseC / 4));

                // aging selection
                bitString = windingHotSpotSetup.RatingAsBitString;
                if (bitString[11] == '0')
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 0;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIECText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 1;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEESixtyFiveText;
                    }
                }
                else
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 2;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEEFiftyFiveText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 3;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodNomexText;
                    }
                }

                if (bitString[10] == '1')
                {
                    this.EnableFanCurrentMonitoring.Checked = true;
                }
                else
                {
                    this.EnableFanCurrentMonitoring.Checked = false;

                }

                // hot spot Factor
                this.hotSpotFactorSpinEditor.Value = windingHotSpotSetup.HotSpotFactor / 100M;

                // top oil temp reference
                
                this.topOilTemperatureComboBox.Text = GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber);

                // do fan currents
                this.fanCurrent1SpinEditor.Value = (windingHotSpotSetup.fan_1BaseCurrent / 10M);
                this.fanCurrent2SpinEditor.Value = (windingHotSpotSetup.fan_2BaseCurrent / 10M);
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToTemperatureRiseOverTopOilRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())

                    {
                       
                        //rated hotspot
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[1].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[1].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[1].Value.ToString()) * 10.0, 0));
                    
                        // max current
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[2].Value.ToString()), 0));
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[2].Value.ToString()), 0));
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[2].Value.ToString()), 0));

                        //exponents
                        this.workingConfiguration.windingHotSpotSetup.Exponent = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[3].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.Exponent_B = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[3].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.Exponent_C = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[3].Value.ToString()) * 10.0, 0));

                        // time constants
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[4].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant_B = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[4].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant_C = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[4].Value.ToString()) * 10.0, 0));

                        //current inputs
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[5].Value.ToString());
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[5].Value.ToString());
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[5].Value.ToString());

                        //whs previous aging
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[6].Value.ToString()) * 4.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[6].Value.ToString()) * 4.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[6].Value.ToString()) * 4.0, 0));
                    
                        // hot spot factor
                        this.workingConfiguration.windingHotSpotSetup.HotSpotFactor = (Int16)((hotSpotFactorSpinEditor.Value * 100M));

                        // aging model
                       
                       // this.ratingsBitEncodingAsIntegerArray[6] = 0;
                        if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIECText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEESixtyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEEFiftyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodNomexText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }
                        if (this.EnableFanCurrentMonitoring.Checked)
                        {
                            this.ratingsBitEncodingAsIntegerArray[10] = 1;
                        }
                        else
                        {
                            this.ratingsBitEncodingAsIntegerArray[10] = 0;
                        }
                        //top oil temp input
                        
                        this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.topOilTemperatureComboBox.Text);
                    
                        // write fan currents
                        
                        this.workingConfiguration.windingHotSpotSetup.fan_1BaseCurrent = (Int16)(fanCurrent1SpinEditor.Value * 10M);
                        this.workingConfiguration.windingHotSpotSetup.fan_2BaseCurrent = (Int16)(fanCurrent2SpinEditor.Value * 10M);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration had some null members";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
                                  
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void InitializeMaximumRatedCurrentRadGridView()
        {
            try
            {
                this.maximumRatedCurrentRadGridView.Columns.Clear();

                GridViewTextBoxColumn windingNameGridViewTextBoxColumn = new GridViewTextBoxColumn("WindingName");
                windingNameGridViewTextBoxColumn.Name = "WindingName";
                windingNameGridViewTextBoxColumn.HeaderText = windingText;
                windingNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                windingNameGridViewTextBoxColumn.Width = 120;
                windingNameGridViewTextBoxColumn.AllowSort = false;
                windingNameGridViewTextBoxColumn.IsPinned = true;
                windingNameGridViewTextBoxColumn.ReadOnly = true;
                windingNameGridViewTextBoxColumn.AllowResize = false;

                GridViewDecimalColumn maximumRatedCurrentGridViewDecimalColumn = new GridViewDecimalColumn("MaximumRatedCurrent");
                maximumRatedCurrentGridViewDecimalColumn.Name = "MaximumRatedCurrent";
                maximumRatedCurrentGridViewDecimalColumn.HeaderText = maximumRatedCurrentText;
                maximumRatedCurrentGridViewDecimalColumn.DecimalPlaces = 0;
                maximumRatedCurrentGridViewDecimalColumn.Step = 1;
                maximumRatedCurrentGridViewDecimalColumn.Minimum = 1;
                maximumRatedCurrentGridViewDecimalColumn.Maximum = 50000;
                maximumRatedCurrentGridViewDecimalColumn.DisableHTMLRendering = false;
                maximumRatedCurrentGridViewDecimalColumn.Width = 120;
                maximumRatedCurrentGridViewDecimalColumn.AllowSort = false;
                maximumRatedCurrentGridViewDecimalColumn.AllowResize = false;

                this.maximumRatedCurrentRadGridView.MasterTemplate.Columns.Add(windingNameGridViewTextBoxColumn);
                this.maximumRatedCurrentRadGridView.MasterTemplate.Columns.Add(maximumRatedCurrentGridViewDecimalColumn);

                this.maximumRatedCurrentRadGridView.TableElement.TableHeaderHeight = 50;
                this.maximumRatedCurrentRadGridView.AllowColumnReorder = false;
                this.maximumRatedCurrentRadGridView.AllowColumnChooser = false;
                this.maximumRatedCurrentRadGridView.ShowGroupPanel = false;
                this.maximumRatedCurrentRadGridView.EnableGrouping = false;
                this.maximumRatedCurrentRadGridView.AllowAddNewRow = false;
                this.maximumRatedCurrentRadGridView.AllowDeleteRow = false;
                this.maximumRatedCurrentRadGridView.MultiSelect = true;
                this.maximumRatedCurrentRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeMaximumRatedCurrentRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
        private int GetTopOilTemperatureInputRegisterNumberFromRegisterName(string registerName)
        {
            int registerNumber = 0;
            try
            {
                if (registerName.CompareTo(topOilTempInputNoneText) == 0)
                {
                    registerNumber = 0;
                }
                else if (registerName.CompareTo(topOilTempInputRTDOneText) == 0)
                {
                    registerNumber = 17;
                }
                else if (registerName.CompareTo(topOilTempInputRTDTwoText) == 0)
                {
                    registerNumber = 18;
                }
                else if (registerName.CompareTo(topOilTempInputRTDThreeText) == 0)
                {
                    registerNumber = 19;
                }
                else if (registerName.CompareTo(topOilTempInputRTDFourText) == 0)
                {
                    registerNumber = 20;
                }
                else if (registerName.CompareTo(topOilTempInputRTDFiveText) == 0)
                {
                    registerNumber = 21;
                }
                else if (registerName.CompareTo(topOilTempInputRTDSixText) == 0)
                {
                    registerNumber = 22;
                }
                else if (registerName.CompareTo(topOilTempInputRTDSevenText) == 0)
                {
                    registerNumber = 23;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetTopOilTemperatureInputRegisterNumberFromRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerNumber;
        }

        private string GetTopOilTemperatureInputRegisterNameFromRegisterNumber(int registerNumber)
        {
            string registerName = topOilTempInputNoneText;
            try
            {
                switch (registerNumber)
                {
                    case 0:
                        registerName = topOilTempInputNoneText;
                        break;
                    case 17:
                        registerName = topOilTempInputRTDOneText;
                        break;
                    case 18:
                        registerName = topOilTempInputRTDTwoText;
                        break;
                    case 19:
                        registerName = topOilTempInputRTDThreeText;
                        break;
                    case 20:
                        registerName = topOilTempInputRTDFourText;
                        break;
                    case 21:
                        registerName = topOilTempInputRTDFiveText;
                        break;
                    case 22:
                        registerName = topOilTempInputRTDSixText;
                        break;
                    case 23:
                        registerName = topOilTempInputRTDSevenText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetTopOilTemperatureInputRegisterNameFromRegisterNumber(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerName;
        }

        private int GetCurrentInputRegisterNumberFromRegisterName(string registerName)
        {
            int registerNumber = 0;
            try
            {
                if (registerName.CompareTo(currentInputNoneText) == 0)
                {
                    registerNumber = 0;
                }
                else if (registerName.CompareTo(currentInputIAText) == 0)
                {
                    registerNumber = 10;
                }
                else if (registerName.CompareTo(currentInputIBText) == 0)
                {
                    registerNumber = 11;
                }
                else if (registerName.CompareTo(currentInputICText) == 0)
                {
                    registerNumber = 12;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetCurrentInputRegisterNumberFromRegisterName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerNumber;
        }

        private string GetCurrentInputRegisterNameFromRegisterNumber(int registerNumber)
        {
            string registerName = currentInputNoneText;
            try
            {
                switch (registerNumber)
                {
                    case 0:
                        registerName = currentInputNoneText;
                        break;
                    case 10:
                        registerName = currentInputIAText;
                        break;
                    case 11:
                        registerName = currentInputIBText;
                        break;
                    case 12:
                        registerName = currentInputICText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.GetCurrentInputRegisterNameFromRegisterNumber(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerName;
        }
    }
}