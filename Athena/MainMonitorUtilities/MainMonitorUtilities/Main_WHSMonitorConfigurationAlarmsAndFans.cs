﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;

namespace MainMonitorUtilities
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string alarmsAndFansRadPageViewPageText = "Alarms and Fans";
        private static string deadBandTemperatureRadGroupBoxText = "Dead Band Temperature";
        private static string deadBandTemperatureTextRadLabelText = "Dead band temp (ºC)";
        private static string alarmSettingsRadGroupBoxText = "Alarm settings";
        private static string alarmDelayTextRadLabelText = "Alarm delay (seconds)";
        private static string topOilTempIsHighRadGroupBoxText = "Top Oil Temp Set Point";
        private static string whsIsHighRadGroupBoxText = "WHS Alarm Set Points";
        private static string whsLowAlarmSetPointTextRadLabelText = "High Alarm (ºC)";
        private static string whsHighAlarmSetPointTextRadLabelText = "High - High Alarm (ºC)";        
        private static string topOilTempLowAlarmSetPointTextRadLabelText = "High Alarm (ºC)";
        private static string topOilTempHighAlarmSetPointTextRadLabelText = "High - High Alarm (ºC)";     
        private static string fanSettingsRadGroupBoxText = "Fan Settings";
        private static string numberOfCoolingGroupsLabelText = "Groups Enabled";
        private static string coolingGroupsZero = "None";
        private static string coolingGroupsOne = "1";
        private static string coolingGroupsTwo = "2";
        private static string coolingGroupsThree = " 1 and 2";
        private static string calibrateFanButtonText = "Calibrate Fan Currents";
        private static string group1CurrentLabelText = "Group 1";
        private static string group2CurrentLabelText = "Group 2";
        private static string enableFanGroupCurrentMomitotingLabelText = "Enable Fan Current Monitoring";
        private static string coolingCurrentAlarmGroupBoxText = "Per Unit Current Alarm Set Points";
        private static string lowCoolingAlarmLabelText = "Low Current";
        private static string highCoolingAlarmLabelText = "High Current";
        private static string fanStartTemperaturesRadGroupBoxText = "Fan start temperatures";
        private static string whsTextRadLabelText = "WHS";
        private static string topOilTextRadLabelText = "Top Oil";
        private static string fanBankOneTextRadLabelText = "Group 1 (ºC)";
        private static string fanBankTwoTextRadLabelText = "Group 2 (ºC)";
        private static string fanAutoExerciseRadGroupBoxText = "Fan auto exercise settings";
        private static string fanAutoExerciseEveryTextRadLabelText = "Every";
        private static string fanAutoExerciseDaysTextRadLabelText = "Days";
        private static string fanAutoExerciseForTextRadLabelText = "For";
        private static string fanAutoExerciseMinutesTextRadLabelText = "Minutes";
        private static string fanAutoExerciseSetBothToZeroToDisableTextRadLabelText = "Set days to 0 to disable";
        private static string fanManualExcerciseLabelText = "Manual Excercise";
        private static string fanTestButtonText = "Fan Test";
        private static string minimumFanRunTimeRadGroupBoxText = "Minimum fan run time";
        private static string minimumFanRunTimeMinutesTextRadLabelText = "minutes";
        private static string fanBankSwapRadGroupBoxText = "Fan bank swap";
        private static string fanBankSwapEveryTextRadLabelText = "Every";
        private static string fanBankSwapHoursTextRadLabelText = "hours";
        private static string fanBankSwapSetToZeroToDisableTextRadLabelText = "Set to 0 to disable";
        private static string fanOperationGroupBoxText = "Cooling Group Statistics";
        private static string whsRunTimeHoursTextRadLabelText = "Runtime Hours";
        private static string whsNumOfStartsTextRadLabelText = "Number of Starts";
        private static string whsFan1StatsTextRadLabelText = "Group 1";
        private static string whsFan2StatsTextRadLabelText = "Group 2";
        private static string fanStatusLabelText = "On/Off";
        

        private void AssignStringValuesToAlarmsAndFansInterfaceObjects()
        {
            alarmsAndFansRadPageViewPage.Text = alarmsAndFansRadPageViewPageText;
            deadBandTemperatureRadGroupBox.Text = deadBandTemperatureRadGroupBoxText;
            deadBandTemperatureTextRadLabel.Text = deadBandTemperatureTextRadLabelText;
            alarmSettingsRadGroupBox.Text = alarmSettingsRadGroupBoxText;
            alarmDelayTextRadLabel.Text = alarmDelayTextRadLabelText;
            whsIsHighRadGroupBox.Text = whsIsHighRadGroupBoxText;
            whsLowAlarmSetPointTextRadLabel.Text = whsLowAlarmSetPointTextRadLabelText;
            whsHighAlarmSetPointTextRadLabel.Text = whsHighAlarmSetPointTextRadLabelText;
            topOilTempIsHighRadGroupBox.Text = topOilTempIsHighRadGroupBoxText;
            topOilTempLowAlarmSetPointTextRadLabel.Text = topOilTempLowAlarmSetPointTextRadLabelText;
            topOilTempHighAlarmSetPointTextRadLabel.Text = topOilTempHighAlarmSetPointTextRadLabelText;
            fanSettingsRadGroupBox.Text = fanSettingsRadGroupBoxText;
            NumberofFanGroupsLabel.Text = numberOfCoolingGroupsLabelText;
            EnableFanCurrentMonitoring.Text = enableFanGroupCurrentMomitotingLabelText;
            coolingAlarmGroupBox.Text = coolingCurrentAlarmGroupBoxText;
            lowCoolingAlarmlabel.Text = lowCoolingAlarmLabelText;
            highCoolingAlarmLabel.Text = highCoolingAlarmLabelText;
            CalibrateFanButton.Text = calibrateFanButtonText;
            Group1CurrentLabel.Text = group1CurrentLabelText;
            Group2CurrentLabel.Text = group2CurrentLabelText;
            fanStartTemperaturesRadGroupBox.Text = fanStartTemperaturesRadGroupBoxText;
            whsTextRadLabel.Text = whsTextRadLabelText;
            topOilTextRadLabel.Text = topOilTextRadLabelText;
            fanBankOneTextRadLabel.Text = fanBankOneTextRadLabelText;
            fanBankTwoTextRadLabel.Text = fanBankTwoTextRadLabelText;
            fanAutoExerciseRadGroupBox.Text = fanAutoExerciseRadGroupBoxText;
            fanAutoExerciseEveryTextRadLabel.Text = fanAutoExerciseEveryTextRadLabelText;
            fanAutoExerciseDaysTextRadLabel.Text = fanAutoExerciseDaysTextRadLabelText;
            fanAutoExerciseForTextRadLabel.Text = fanAutoExerciseForTextRadLabelText;
            fanAutoExerciseMinutesTextRadLabel.Text = fanAutoExerciseMinutesTextRadLabelText;
            fanAutoExerciseSetBothToZeroToDisableTextRadLabel.Text = fanAutoExerciseSetBothToZeroToDisableTextRadLabelText;
            ManualExcerciseLabel.Text = fanManualExcerciseLabelText;
            fanTestButton.Text = fanTestButtonText;
            minimumFanRunTimeRadGroupBox.Text = minimumFanRunTimeRadGroupBoxText;
            minimumFanRunTimeMinutesTextRadLabel.Text = minimumFanRunTimeMinutesTextRadLabelText;
            fanBankSwapRadGroupBox.Text = fanBankSwapRadGroupBoxText;
            fanBankSwapEveryTextRadLabel.Text = fanBankSwapEveryTextRadLabelText;
            fanBankSwapHoursTextRadLabel.Text = fanBankSwapHoursTextRadLabelText;
            fanBankSwapSetToZeroToDisableTextRadLabel.Text = fanBankSwapSetToZeroToDisableTextRadLabelText;
            fanOperationalInfoGroupBox.Text = fanOperationGroupBoxText;
            whsRunTimeHoursLabelText.Text = whsRunTimeHoursTextRadLabelText;          
            whsNumOfStartsLabelText.Text = whsNumOfStartsTextRadLabelText;
            whsFan1StatsLabelText.Text = whsFan1StatsTextRadLabelText;
            whsFan2StatsLabelText.Text = whsFan2StatsTextRadLabelText;
            FanStatusLabel.Text = fanStatusLabelText;

            /// These use generic strings declared in the main code file
            availableConfigurationsAlarmsAndFansTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
            saveWorkingConfigurationAlarmsAndFansTabRadButton.Text = saveCurrentConfigurationRadButtonText;
            // saveDeviceConfigurationAlarmsAndFansTabRadButton.Text = saveDeviceConfigurationRadButtonText;
            loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
            programDeviceAlarmsAndFansTabRadButton.Text = programDeviceRadButtonText;
            loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
            databaseInteractionAlarmsAndFansTabRadGroupBox.Text = databaseInteractionGroupBoxText;
            deviceInteractionAlarmsAndFansTabRadGroupBox.Text = deviceInteractionGroupBoxText;
            deleteSelectedConfigurationAlarmsAndFansTabRadButton.Text = deleteSelectedConfigurationRadButtonText;
        }

        private static void AssignValuesToAlarmsAndFansInternalStaticStrings()
        {
            alarmsAndFansRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansRadPageViewPageText", alarmsAndFansRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
            deadBandTemperatureRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansDeadBandTemperatureRadGroupBoxText", deadBandTemperatureRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            deadBandTemperatureTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansDeadBandTemperatureTextRadLabelText", deadBandTemperatureTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            alarmSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansAlarmSettingsRadGroupBoxText", alarmSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            alarmDelayTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansAlarmDelayTextRadLabelText", alarmDelayTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsIsHighRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansWhsIsHighRadGroupBoxText", whsIsHighRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            whsLowAlarmSetPointTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansWhsLowAlarmSetPointTextRadLabelText", whsLowAlarmSetPointTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsHighAlarmSetPointTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansWhsHighAlarmSetPointTextRadLabelText", whsHighAlarmSetPointTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            topOilTempIsHighRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansTopOilTempIsHighRadGroupBoxText", topOilTempIsHighRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            topOilTempLowAlarmSetPointTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansTopOilTempLowAlarmSetPointTextRadLabelText", topOilTempLowAlarmSetPointTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            topOilTempHighAlarmSetPointTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansTopOilTempHighAlarmSetPointTextRadLabelText", topOilTempHighAlarmSetPointTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanSettingsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanSettingsRadGroupBoxText", fanSettingsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            fanStartTemperaturesRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanStartTemperaturesRadGroupBoxText", fanStartTemperaturesRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            whsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansWhsTextRadLabelText", whsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            topOilTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansTopOilTextRadLabelText", topOilTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanBankOneTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankOneTextRadLabelText", fanBankOneTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanBankTwoTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankTwoTextRadLabelText", fanBankTwoTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseRadGroupBoxText", fanAutoExerciseRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseEveryTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseEveryTextRadLabelText", fanAutoExerciseEveryTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseDaysTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseDaysTextRadLabelText", fanAutoExerciseDaysTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseForTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseForTextRadLabelText", fanAutoExerciseForTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseMinutesTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseMinutesTextRadLabelText", fanAutoExerciseMinutesTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanAutoExerciseSetBothToZeroToDisableTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanAutoExerciseSetBothToZeroToDisableTextRadLabelText", fanAutoExerciseSetBothToZeroToDisableTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            minimumFanRunTimeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansMinimumFanRunTimeRadGroupBoxText", minimumFanRunTimeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            minimumFanRunTimeMinutesTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansMinimumFanRunTimeMinutesTextRadLabelText", minimumFanRunTimeMinutesTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanBankSwapRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankSwapRadGroupBoxText", fanBankSwapRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            fanBankSwapEveryTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankSwapEveryTextRadLabelText", fanBankSwapEveryTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanBankSwapHoursTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankSwapHoursTextRadLabelText", fanBankSwapHoursTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            fanBankSwapSetToZeroToDisableTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansFanBankSwapSetToZeroToDisableTextRadLabelText", fanBankSwapSetToZeroToDisableTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsRunTimeHoursTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanswhsRunTimeHoursTextRadLabelText", whsRunTimeHoursTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsNumOfStartsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanswhsNumOfStartswhsStatsTextRadLabelText", whsNumOfStartsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsFan1StatsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanswhsFan1StatuswhsFan1StatsTextRadLabelText", whsFan1StatsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
            whsFan2StatsTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanswhsFan2StatuswhsFan2StatsTextRadLabelText", whsFan2StatsTextRadLabelText, htmlFontType, htmlStandardFontSize, "");

           // xxx = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFans", xxx, htmlFontType, htmlStandardFontSize, ""); 
            fanStatusLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansfanStatusLabelText", fanStatusLabelText, htmlFontType, htmlStandardFontSize, "");
            fanTestButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansfanTestButtonText", fanTestButtonText, htmlFontType, htmlStandardFontSize, "");
            fanManualExcerciseLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansfanManualExcerciseLabelText", fanManualExcerciseLabelText, htmlFontType, htmlStandardFontSize, "");
            numberOfCoolingGroupsLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansnumberOfCoolingGroupsLabelText", numberOfCoolingGroupsLabelText, htmlFontType, htmlStandardFontSize, "");
            enableFanGroupCurrentMomitotingLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansenableFanGroupCurrentMomitotingLabelText", enableFanGroupCurrentMomitotingLabelText, htmlFontType, htmlStandardFontSize, "");
            calibrateFanButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscalibrateFanButtonText", calibrateFanButtonText, htmlFontType, htmlStandardFontSize, "");
            group1CurrentLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansgroup1CurrentLabelText", group1CurrentLabelText, htmlFontType, htmlStandardFontSize, "");
            group2CurrentLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFansgroup2CurrentLabelText", group2CurrentLabelText, htmlFontType, htmlStandardFontSize, "");
            coolingCurrentAlarmGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscoolingCurrentAlarmGroupBoxText", coolingCurrentAlarmGroupBoxText, htmlFontType, htmlStandardFontSize, "");
            lowCoolingAlarmLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanslowCoolingAlarmLabelText", lowCoolingAlarmLabelText, htmlFontType, htmlStandardFontSize, "");
            highCoolingAlarmLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanshighCoolingAlarmLabelText", highCoolingAlarmLabelText, htmlFontType, htmlStandardFontSize, "");
            coolingGroupsZero = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscoolingGroupsZero", coolingGroupsZero, "", "", "");
            coolingGroupsOne = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscoolingGroupsOne", coolingGroupsOne, "", "", "");
            coolingGroupsTwo = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscoolingGroupsTwo", coolingGroupsTwo, "", "", "");
            coolingGroupsThree = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAlarmsAndFanscoolingGroupsThree", coolingGroupsThree, "", "", ""); 
        }

        private void InitializeAlarmsAndFansTabObjects()
        {
            NumberOfCoolingGroupsComboBox.DataSource = new string[] { coolingGroupsZero, coolingGroupsOne, coolingGroupsTwo, coolingGroupsThree };
        }

        private void WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        this.workingConfiguration.windingHotSpotSetup.AlarmDeadBandTemperature = (int)this.deadBandTemperatureRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmDelayInSeconds = (int)this.alarmDelayRadSpinEditor.Value;

                        this.workingConfiguration.windingHotSpotSetup.AlarmMaxSetPointTemperature_H = (int)this.whsLowAlarmSetPointRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmMaxSetPointTemperature_HH = (int)this.whsHighAlarmSetPointRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmTopOilTemperature_H = (int)this.topOilTempLowAlarmSetPointRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmTopOilTemperature_HH = (int)this.topOilTempHighAlarmSetPointRadSpinEditor.Value;

                        this.workingConfiguration.windingHotSpotSetup.FanSetpoint_H = (int)this.fanBankOneStartTempWhsRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.FanSetpoint_HH = (int)this.fanBankTwoStartTempWhsRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TopOilTemperatureFanSetpoint_H = (int)this.fanBankOneStartTempTopOilRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = (int)this.fanBankTwoStartTempTopOilRadSpinEditor.Value;

                        this.workingConfiguration.windingHotSpotSetup.TimeBetweenFanExerciseInDays = (int)this.fanExerciseIntervalInDaysRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TimeToExerciseFanInMinutes = (int)this.fanExerciseTimeInMinutesRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.MinimumFanRunTimeInMinutes = (int)this.minimumfanRunTimeInMinutesRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.FanBankSwitchingTimeInHours = (int)this.fanBankSwapIntervalInHoursRadSpinEditor.Value;
                        //code updated cfk 12/17/31
                        //------------------------------------
                        this.workingConfiguration.windingHotSpotSetup.FanOneRuntimeInHours = Convert.ToInt16(fan1RuntimeHoursSpinEditor.Value / 3.0M);
                        this.workingConfiguration.windingHotSpotSetup.FanTwoRuntimeInHours = Convert.ToInt16(fan2RuntimeHoursSpinEditor.Value / 3.0M);
                        this.workingConfiguration.windingHotSpotSetup.whsFan1NumberOfStarts = Convert.ToInt16(this.fan1NumOfStartsSpinEditor.Value);
                        this.workingConfiguration.windingHotSpotSetup.whsFan2NumberOfStarts = Convert.ToInt16(this.fan2NumOfStartsSpinEdito.Value);
                        this.workingConfiguration.windingHotSpotSetup.NumberOfCoolingGroups = Convert.ToInt16(this.NumberOfCoolingGroupsComboBox.SelectedIndex);
                        this.workingConfiguration.windingHotSpotSetup.fan_lowLimitAlarm = Convert.ToInt16(Convert.ToDouble(this.lowCoolingAlarmSpinEditor.Value) * 100.0);
                        this.workingConfiguration.windingHotSpotSetup.fan_highLimitAlarm = Convert.ToInt16(Convert.ToDouble(this.HighCoolingAlarmSpinEditor.Value) * 100.0);


                        // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseA = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseA.Text) * 4.0);
                        // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseB = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseB.Text) * 4.0);
                        // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseC = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseC.Text) * 4.0);
                        //-------------------------------------------------------

                        return;
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration contained at least on null member";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteConfigurationDataToAlarmsAndFansInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.Value = windingHotSpotSetup.AlarmDeadBandTemperature;
                this.alarmDelayRadSpinEditor.Value = windingHotSpotSetup.AlarmDelayInSeconds;
                this.whsLowAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                this.whsHighAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                this.topOilTempLowAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmTopOilTemperature_H;
                this.topOilTempHighAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmTopOilTemperature_HH;

                this.fanBankOneStartTempWhsRadSpinEditor.Value = windingHotSpotSetup.FanSetpoint_H;
                this.fanBankTwoStartTempWhsRadSpinEditor.Value = windingHotSpotSetup.FanSetpoint_HH;
                this.fanBankOneStartTempTopOilRadSpinEditor.Value = windingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                this.fanBankTwoStartTempTopOilRadSpinEditor.Value = windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;

                this.fanExerciseIntervalInDaysRadSpinEditor.Value = windingHotSpotSetup.TimeBetweenFanExerciseInDays;
                this.fanExerciseTimeInMinutesRadSpinEditor.Value = windingHotSpotSetup.TimeToExerciseFanInMinutes;

                this.minimumfanRunTimeInMinutesRadSpinEditor.Value = windingHotSpotSetup.MinimumFanRunTimeInMinutes;

                this.fanBankSwapIntervalInHoursRadSpinEditor.Value = windingHotSpotSetup.FanBankSwitchingTimeInHours;
                // code added cfk 12/17/13
                this.fan1RuntimeHoursSpinEditor.Value = (windingHotSpotSetup.FanOneRuntimeInHours * 3.0M);
                this.fan2RuntimeHoursSpinEditor.Value = (windingHotSpotSetup.FanTwoRuntimeInHours * 3.0M);
                this.fan1NumOfStartsSpinEditor.Value = windingHotSpotSetup.whsFan1NumberOfStarts;
                this.fan2NumOfStartsSpinEdito.Value = windingHotSpotSetup.whsFan2NumberOfStarts;
                this.fanBankSwapIntervalInHoursRadSpinEditor.Value = windingHotSpotSetup.FanBankSwitchingTimeInHours;
                this.NumberOfCoolingGroupsComboBox.SelectedIndex = windingHotSpotSetup.NumberOfCoolingGroups;
                this.lowCoolingAlarmSpinEditor.Value = windingHotSpotSetup.fan_lowLimitAlarm / 100M;
                this.HighCoolingAlarmSpinEditor.Value = windingHotSpotSetup.fan_highLimitAlarm / 100M;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteConfigurationDataToAlarmsAndFansInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableAlarmsAndFansTabConfigurationEditObjects()
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.ReadOnly = false;
                this.alarmDelayRadSpinEditor.ReadOnly = false;
                this.whsLowAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.whsHighAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ReadOnly = false;

                this.fanBankOneStartTempWhsRadSpinEditor.ReadOnly = false;
                this.fanBankOneStartTempTopOilRadSpinEditor.ReadOnly = false;
                this.fanBankTwoStartTempWhsRadSpinEditor.ReadOnly = false;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ReadOnly = false;
                this.fanExerciseIntervalInDaysRadSpinEditor.ReadOnly = false;
                this.fanExerciseTimeInMinutesRadSpinEditor.ReadOnly = false;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ReadOnly = false;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ReadOnly = false;

                this.deadBandTemperatureRadSpinEditor.ShowUpDownButtons = true;
                this.alarmDelayRadSpinEditor.ShowUpDownButtons = true;
                this.whsLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.whsHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;

                this.fanBankOneStartTempWhsRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankOneStartTempTopOilRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankTwoStartTempWhsRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ShowUpDownButtons = true;
                this.fanExerciseIntervalInDaysRadSpinEditor.ShowUpDownButtons = true;
                this.fanExerciseTimeInMinutesRadSpinEditor.ShowUpDownButtons = true;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ShowUpDownButtons = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableAlarmsAndFansTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableAlarmsAndFansTabConfigurationEditObjects()
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.ReadOnly = true;
                this.alarmDelayRadSpinEditor.ReadOnly = true;
                this.whsLowAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.whsHighAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ReadOnly = true;

                this.fanBankOneStartTempWhsRadSpinEditor.ReadOnly = true;
                this.fanBankOneStartTempTopOilRadSpinEditor.ReadOnly = true;
                this.fanBankTwoStartTempWhsRadSpinEditor.ReadOnly = true;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ReadOnly = true;
                this.fanExerciseIntervalInDaysRadSpinEditor.ReadOnly = true;
                this.fanExerciseTimeInMinutesRadSpinEditor.ReadOnly = true;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ReadOnly = true;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ReadOnly = true;

                this.deadBandTemperatureRadSpinEditor.ShowUpDownButtons = false;
                this.alarmDelayRadSpinEditor.ShowUpDownButtons = false;
                this.whsLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.whsHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;

                this.fanBankOneStartTempWhsRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankOneStartTempTopOilRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankTwoStartTempWhsRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ShowUpDownButtons = false;
                this.fanExerciseIntervalInDaysRadSpinEditor.ShowUpDownButtons = false;
                this.fanExerciseTimeInMinutesRadSpinEditor.ShowUpDownButtons = false;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ShowUpDownButtons = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableAlarmsAndFansTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool ErrorIsPresentInAnAlarmsAndFansTabObject()
        {
            return false;
        }
    }
}