﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;
using FormatConversion;
using MonitorUtilities;

namespace MainMonitorUtilities
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
       // private static string failedToSaveConfigurationToDatabaseText = "Failed to save the configuration to the database.";
      //  private static string noCurrentConfigurationDefinedText = "There is no current configuration defined.";
     //   private static string noConfigurationLoadedFromDeviceText = "No configuration has been loaded from the device.";
        private static string configurationBeingSavedIsFromDeviceText = "This will save the configuration loaded from the device, not the current configuration";
        private static string deviceConfigurationSavedToDatabaseText = "The device configuration was saved to the database";
        private static string deviceConfigurationNotSavedToDatabaseText = "Error: the device configuration was not saved to the database";
       // private static string errorInConfigurationLoadedFromDatabaseText = "Error in configuration loaded from the database, load cancelled";
      //  private static string configurationCouldNotBeLoadedFromDatabaseText = "Configuration could not be loaded from the database";
        private static string configurationDeleteFromDatabaseWarningText = "Are you sure you want to delete this configuration?\nIt cannot be recovered.";
        private static string deleteAsQuestionText = "Delete?";
        private static string cannotDeleteCurrentConfigurationWarningText = "You are not allowed to delete the current device configuration from the database.";
    //    private static string cannotDeleteTemplateConfigurationWarningText = "You are not allowed to delete a template configuration from the database.";      
        private static string noConfigurationSelectedText = "No configuration selected";
      //  private static string failedToDownloadDeviceConfigurationDataText = "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.";
     //   private static string commandToReadDeviceErrorStateFailedText = "Command to read the device errror state failed.";
     //   private static string lostDeviceConnectionText = "Lost the connection to the device.";
      //  private static string notConnectedToMainMonitorWarningText = "You are not connected to a Main monitor.  Cannot configure using this interface.";
        private static string serialPortNotSetWarningText = "You need to set the serial port and baud rate in the Athena main interface.";
        private static string failedToOpenMonitorConnectionText = "Failed to open a connection to the monitor.";
        private static string deviceCommunicationNotProperlyConfiguredText = "Device communication is not properly configured.\nPlease correct the system configuration.";
        private static string downloadWasInProgressWhenInterfaceWasOpenedText = "Some kind of download was in progress when you opened this interface.\nYou cannot access the device unless manual and automatic downloads are inactive.";
       // private static string failedToWriteTheConfigurationToTheDevice = "Failed to write the configuration to the device.";
      //  private static string configurationWasReadFromTheDevice = "The configuration was read from the device.";
       // private static string configurationWasWrittenToTheDevice = "The configuration was written to the device.";
        private static string deviceConfigurationDoesNotMatchDatabaseConfigurationText = "The device configuration does not match the configuration saved in the database.\nYou may wish to load the database version and compare the two.";
        private static string deviceCommunicationNotSavedInDatabaseYetText = "You have not yet saved the device configuration to the database.  Would you like to save it now?";
        private static string saveDeviceConfigurationQuestionText = "Save device configuration?";
        private static string currentConfigurationNotDefinedText = "The current configuration is undefined.";
      //  private static string commandToReadDeviceTypeFailed = "Command to read the device type failed";

     //   private static string workingConfigAlreadyPresentInDatabaseText = "There is already a working configuration saved in the database.  Overwrite it?";
   //     private static string failedToDeleteWorkingConfigurationText = "Failed to delete the working configuration from the database";
        private static string workingConfigurationSavedToDatabaseText = "The working configuration was saved to the database";
        private static string workingConfigurationNotSavedToDatabaseText = "Error: the working configuration was not saved to the database";
       // private static string loadedWorkingConfigurationAndDeletedItFromDatabaseText = "The working configuration was loaded and deleted from the database.";
       // private static string loadedWorkingConfigurationButFailedToDeleteItFromTheDatabaseText = "The working configuration was loaded, but it failed to be deleted from the database.";
        private static string configurationWasReadFromTheDatabaseText = "The configuration was successfully read from the database";

        private static string changesToWorkingConfigurationNotSavedOverwriteWarningText = "You have made changes to the configuration that have not been saved.  Discard those changes?";
        private static string changesToWorkingConfigurationNotSavedExitWarningText = "You have made changes to the configuration that have not been saved.  Exit anyway?";
        private static string discardChangesAsQuestionText = "Discard changes?";

        private static string configurationLoadCancelledText = "Configuration load cancelled";

        private static string failedToDeleteCurrentConfigurationText = "Failed to delete the current configuration from the database";
        private static string failedtoDeleteConfigurationText = "Failed to delete the configuration";

        private static string replaceExistingConfigurationsWithTheSameNameQuestionText = "Would you like to replace all existing configurations having the same description with this one?";



        private void SaveWorkingConfigurationToDatabase(MonitorInterfaceDB localDB)
        {
            try
            {
                string defaultConfigurationName = string.Empty;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.workingConfiguration != null)
                    {
                        WriteAllInterfaceDataToWorkingConfiguration();

                        if (Main_MonitorConfiguration.monitor != null)
                        {
                            if (Main_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                            {
                                defaultConfigurationName = this.nameOfLastConfigurationLoadedFromDatabase;
                            }
                            using (ConfigurationDescription description = new ConfigurationDescription(Main_MonitorConfiguration.CurrentConfigName, defaultConfigurationName))
                            {
                                description.ShowDialog();
                                description.Hide();

                                if (description.SaveConfiguration)
                                {
                                    if ((Main_MonitorConfiguration.programType == ProgramType.TemplateEditor) && (NamedConfigurationAlreadySavedInDatabase(Main_MonitorConfiguration.monitor, description.ConfigurationName.Trim(), localDB)))
                                    {
                                        if (RadMessageBox.Show(this, replaceExistingConfigurationsWithTheSameNameQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                        {
                                            if (!DeleteNamedConfigurationFromDatabase(Main_MonitorConfiguration.monitor, description.ConfigurationName.Trim(), localDB))
                                            {
                                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                                RadMessageBox.Show(this, failedtoDeleteConfigurationText);
                                            }
                                        }
                                    }
                                    //WriteUserInterfaceViewedConfigurationToCurrentConfiguration();
                                    if (ConfigurationConversion.SaveMain_ConfigurationToDatabase(this.workingConfiguration, Main_MonitorConfiguration.monitor.ID, DateTime.Now, description.ConfigurationName, localDB))
                                    {
                                        // RadMessageBox.Show(this, workingConfigurationSavedToDatabaseText);
                                        this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
                                    }
                                    else
                                    {
                                        RadMessageBox.SetThemeName("office2007BlackTheme");
                                        RadMessageBox.Show(this, workingConfigurationNotSavedToDatabaseText);
                                    }
                                    LoadAvailableConfigurations(localDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMain_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nthis.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
                // }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SaveCurrentConfigurationToDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool SafeToLoadConfiguration()
        {
            bool safeToLoad = true;
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();
                if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                {
                    RadMessageBox.SetThemeName("office2007BlackTheme");
                    if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedOverwriteWarningText, discardChangesAsQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        safeToLoad = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SafeToLoadConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return safeToLoad;
        }

        private static bool NamedConfigurationAlreadySavedInDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool configurationIsPresent=false;
            try
            {
                List<Main_Config_ConfigurationRoot> configRootList = null;
                if (Main_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        // this list will be, at most, two entries long
                        foreach (Main_Config_ConfigurationRoot entry in configRootList)
                        {
                            if (entry.Description.Trim().Contains(configurationName))
                            {
                                configurationIsPresent = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.NamedConfigurationAlreadySavedInDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationIsPresent;
        }

        private static bool DeleteNamedConfigurationFromDatabase(Monitor monitor, string configurationName, MonitorInterfaceDB localDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_ConfigurationRoot> configRootList = null;
                if (Main_MonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(localDB);
                }
                else
                {
                    configRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, localDB);
                }
                if (configRootList != null)
                {
                    if (configRootList.Count > 0)
                    {
                        for (int i = 0; i < configRootList.Count; i++)
                        {
                            if (configRootList[i].Description.Contains(configurationName))
                            {
                                success = MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(configRootList[i].ID, localDB);
                                //break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.DeleteNamedConfigurationFromDatabase(string, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


//        private void SaveTheWorkingConfigurationToTheDatabase()
//        {
//            try
//            {
//                if (this.workingConfiguration != null)
//                {
//                    using (MonitorInterfaceDB saveDB = new MonitorInterfaceDB(this.dbConnectionString))
//                    {
//                        SaveDeviceConfigurationToDatabase(this.workingConfiguration, true, saveDB);
//                        LoadAvailableConfigurations(saveDB);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, noConfigurationLoadedFromDeviceText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public static bool SaveDeviceConfigurationToDatabase(Monitor monitor, Main_Configuration configurationBeingSaved, bool interactive, MonitorInterfaceDB localDB, IWin32Window parentWindow)
        {
            bool success = false;
            try
            {
                Guid configToDeleteConfigurationRootID = Guid.Empty;
                bool saveConfiguration = false;
                if (configurationBeingSaved != null)
                {
                    if (interactive)
                    {
                        RadMessageBox.SetThemeName("office2007BlackTheme");
                        if (RadMessageBox.Show(parentWindow, configurationBeingSavedIsFromDeviceText, "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            saveConfiguration = true;
                        }
                    }
                    else
                    {
                        saveConfiguration = true;
                    }
                    if (saveConfiguration)
                    {
                        if (NamedConfigurationAlreadySavedInDatabase(monitor, Main_MonitorConfiguration.CurrentConfigName, localDB))
                        {
                            if (!DeleteNamedConfigurationFromDatabase(monitor, Main_MonitorConfiguration.CurrentConfigName, localDB))
                            {
                                if (interactive)
                                {
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(parentWindow, failedToDeleteCurrentConfigurationText);
                                }
                                string errorMessage = "Error in Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nFailed to delete the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                            else
                            {
                                string errorMessage = "In Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nDeleted the existing Current Device Configuration from the database.";
                                LogMessage.LogError(errorMessage);
                            }
                        }
                        if (ConfigurationConversion.SaveMain_ConfigurationToDatabase(configurationBeingSaved, monitor.ID, DateTime.Now, Main_MonitorConfiguration.currentConfigName, localDB))
                        {
                            success = true;
                            if (interactive)
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(parentWindow, deviceConfigurationSavedToDatabaseText);
                            }
                        }
                        else
                        {
                            if (interactive)
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(parentWindow, deviceConfigurationNotSavedToDatabaseText);
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SaveDeviceConfigurationToDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private void LoadSelectedConfigurationFromDatabase()
        {
            try
            {
                Main_Configuration configFromDB = null;
                Main_Config_ConfigurationRoot configurationRoot;

                if (this.availableConfigurationsSelectedIndex > 0)
                {
                    if (Main_MonitorConfiguration.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                    {
                        configurationRoot = Main_MonitorConfiguration.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                        if (configurationRoot.ID.CompareTo(Guid.Empty) != 0)
                        {
                            if (SafeToLoadConfiguration())
                            {
                                configFromDB = LoadConfigurationFromDatabase(configurationRoot.ID, this.dbConnectionString);
                                if (configFromDB != null)
                                {
                                    if (configFromDB.AllConfigurationMembersAreNonNull())
                                    {
                                        this.workingConfiguration = configFromDB;
                                        this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
                                        AddDataToAllTransientInterfaceObjects(this.workingConfiguration);
                                        if (this.configurationRadPageView.SelectedPage == this.dataTransferRadPageViewPage)
                                        {
                                            GetSystemConfigurationForDataTransfer();
                                            UpdateDataTransferPageSlaveMonitorInformation();
                                            UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                                            UpdateDataTransferPageRadGridViewMainRegisterNames();
                                        }
                                        if (configurationRoot.Description.Trim().CompareTo(Main_MonitorConfiguration.currentConfigName) != 0)
                                        {
                                            nameOfLastConfigurationLoadedFromDatabase = configurationRoot.Description;
                                        }

                                        // RadMessageBox.Show(this, configurationWasReadFromTheDatabaseText);
                                    }
                                    else
                                    {
                                        RadMessageBox.SetThemeName("office2007BlackTheme");
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nCould not find the configuration in the database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    RadMessageBox.SetThemeName("office2007BlackTheme");
                    RadMessageBox.Show(this, noConfigurationSelectedText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadSelectedConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private void LoadConfigurationFromDatabaseAndAssignItToDatabaseConfiguration(Guid configurationRootID)
//        {
//            try
//            {
//                Main_Configuration temporaryConfiguration = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);

//                if (temporaryConfiguration != null)
//                {
//                    if (temporaryConfiguration.AllConfigurationMembersAreNonNull())
//                    {
//                        // test new config against old config
//                        //if (currentConfiguration != null)
//                        //{
//                        //    Int16[] newRegisterValues = temporaryConfiguration.GetShortValuesFromAllContributors();
//                        //    // vShowSomeValuesThatArePissingMeOff(newRegisterValues);
//                        //    CheckLatestConfigurationAgainstPreviousConfiguration(newRegisterValues);
//                        //}

//                        configurationFromDatabase = temporaryConfiguration;

//                        EnableCopyDatabaseConfigurationRadButtons();
//                        EnableFromDatabaseRadRadioButtons();
//                        SetFromDatabaseRadRadioButtonState();
                       
//                        if (!AddDataToAllTransientInterfaceObjects(configurationFromDatabase))
//                        {
//                            RadMessageBox.Show(this, dataTransferContainsDepricatedDestinationRegister);
//                        }

//                        //if (!currentConfiguration.ConfigurationIsTheSame(currentConfiguration))
//                        //{
//                        //    MessageBox.Show("Configuration checking code is no good.");
//                        //}
//                    }
//                    else
//                    {
//                        RadMessageBox.Show(this, errorInConfigurationLoadedFromDatabaseText);
//                    }
//                }
//                else
//                {
//                    RadMessageBox.Show(this, configurationCouldNotBeLoadedFromDatabaseText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public static Main_Configuration LoadConfigurationFromDatabase(Guid configurationRootID, string dbConnectionSTring)
        {
            Main_Configuration configurationFromDB = null;
            try
            {
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(dbConnectionSTring))
                {
                    configurationFromDB = ConfigurationConversion.GetMain_ConfigurationFromDatabase(configurationRootID, localDB);
                }  
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDatabase(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationFromDB;
        }

        public static Guid GetConfigurationRootIDForCurrentDeviceConfiguration(Guid monitorID, MonitorInterfaceDB configDB)
        {
            Guid configurationRootID = Guid.Empty;
            try
            {
                List<Main_Config_ConfigurationRoot> availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitorID, configDB);
                if (availableConfigurations != null)
                {
                    foreach (Main_Config_ConfigurationRoot entry in availableConfigurations)
                    {
                        if (entry.Description.Trim().CompareTo(Main_MonitorConfiguration.currentConfigName) == 0)
                        {
                            configurationRootID = entry.ID;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetConfigurationRootIDForCurrentDeviceConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootID;
        }

        public static Main_Configuration LoadCurrentDeviceConfigurationFromDatabase(Guid monitorID, MonitorInterfaceDB configDB)
        {
            Main_Configuration currentDeviceConfiguration = null;
            try
            {
                Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(monitorID, configDB);
                currentDeviceConfiguration = ConfigurationConversion.GetMain_ConfigurationFromDatabase(configurationRootID, configDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return currentDeviceConfiguration;
        }

        private void DeleteConfigurationFromDatabase()
        {
            try
            {
                Main_Config_ConfigurationRoot configurationRoot;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.availableConfigurationsSelectedIndex > 0)
                    {
                        if (Main_MonitorConfiguration.availableConfigurations.Count > (this.availableConfigurationsSelectedIndex - 1))
                        {
                            configurationRoot = Main_MonitorConfiguration.availableConfigurations[this.availableConfigurationsSelectedIndex - 1];
                            if (configurationRoot.Description.Trim().CompareTo(Main_MonitorConfiguration.currentConfigName) != 0)
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                if (RadMessageBox.Show(this, configurationDeleteFromDatabaseWarningText, deleteAsQuestionText, MessageBoxButtons.OKCancel) == DialogResult.OK)
                                {
                                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                    {
                                        MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, localDB);
                                        LoadAvailableConfigurations(localDB);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                RadMessageBox.Show(this, cannotDeleteCurrentConfigurationWarningText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.DeleteConfigurationFromDatabase()\nthis.availableConfigurations had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.SetThemeName("office2007BlackTheme");
                        RadMessageBox.Show(this, noConfigurationSelectedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.DeleteConfigurationFromDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private static Main_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
//        {
//            Main_Configuration deviceConfiguration = null;
//            try
//            {
//                Byte[] byteData = null;

//                if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 15002)
//                {
//                    byteData = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, readDelayInMicroseconds);
//                    if (byteData != null)
//                    {
//                        deviceConfiguration = new Main_Configuration(byteData);
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_MonitorConfiguration.LoadConfigurationFromDevice(int, int)\nMonitor was not a Main.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }bmn
//            return deviceConfiguration;
//        }

        private void LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                long deviceError = 0;
                int deviceType;
                //bool connectionWasApparentlyOpenedSuccessfully = false;
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Main_Configuration configFromDevice = null;
                if (SafeToLoadConfiguration())
                {
                    if (!downloadWasInProgress)
                    {
                        if (Main_MonitorConfiguration.monitor != null)
                        {
                            modBusAddressAsString = monitor.ModbusAddress.Trim();
                            if (modBusAddressAsString.Length > 0)
                            {
                                modBusAddress = Int32.Parse(modBusAddressAsString);

                                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                {
                                    errorCode = MonitorConnection.OpenMonitorConnection(Main_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                }
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    DeviceCommunication.EnableDataDownload();
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                    if (deviceType > 0)
                                    {
                                        // deviceType = DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, this.readDelayInMicroseconds);
                                        if (deviceType == 101)
                                        {
                                            deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceError > -1)
                                            {
                                                Application.DoEvents();
                                                configFromDevice = LoadConfigurationFromDevice(modBusAddress, this.readDelayInMicroseconds);
                                                if (configFromDevice != null)
                                                {
                                                    if (configFromDevice.AllConfigurationMembersAreNonNull())
                                                    {
                                                        errorCode = ErrorCode.ConfigurationDownloadSucceeded;

                                                        this.workingConfiguration = configFromDevice;
                                                        this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);

                                                        // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                                        // Testing code

                                                        //this.previousConfigurationAsArray = this.workingConfigurationAsArray;
                                                        //this.workingConfigurationAsArray = registerData;

                                                        //CheckLatestConfigurationAgainstPreviousConfiguration(registerData);

                                                        // more testing code, show some values

                                                        // ShowSomeValuesThatArePissingMeOff(registerData);

                                                        // AddDataToAllTransientInterfaceObjects(configurationFromDevice);
                                                        //EnableCopyDeviceConfigurationRadButtons();
                                                        //EnableFromDeviceRadRadioButtons();
                                                        //SetFromDeviceRadRadioButtonState();

                                                        if (!AddDataToAllTransientInterfaceObjects(this.workingConfiguration))
                                                        {
                                                            RadMessageBox.SetThemeName("office2007BlackTheme");
                                                            RadMessageBox.Show(this, dataTransferContainsDepricatedDestinationRegister);
                                                        }
                                                        if (this.configurationRadPageView.SelectedPage == this.dataTransferRadPageViewPage)
                                                        {
                                                            GetSystemConfigurationForDataTransfer();
                                                            UpdateDataTransferPageSlaveMonitorInformation();
                                                            UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                                                            UpdateDataTransferPageRadGridViewMainRegisterNames();
                                                        }

                                                        /// test code
                                                        //Int16[] registerValues = configurationFromDevice.GetShortValuesFromAllContributors();
                                                        //Main_Configuration testConfiguration = new Main_Configuration(registerValues);
                                                        //if (!configurationFromDevice.ConfigurationIsTheSame(testConfiguration))
                                                        //{
                                                        //    MessageBox.Show("configuration mismatch from a copy");
                                                        //}
                                                        //else
                                                        //{
                                                        //    MessageBox.Show("configuration matches from a copy");
                                                        //}
                                                        /// end test code
                                                        RadMessageBox.SetThemeName("office2007BlackTheme");
                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            Guid configurationRootID = GetConfigurationRootIDForCurrentDeviceConfiguration(Main_MonitorConfiguration.monitor.ID, localDB);
                                                            if (configurationRootID.CompareTo(Guid.Empty) != 0)
                                                            {
                                                                Main_Configuration deviceConfigFromDb = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString);
                                                                if (!deviceConfigFromDb.ConfigurationIsTheSame(this.workingConfiguration))
                                                                {
                                                                    RadMessageBox.Show(this, deviceConfigurationDoesNotMatchDatabaseConfigurationText);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (RadMessageBox.Show(this, deviceCommunicationNotSavedInDatabaseYetText, saveDeviceConfigurationQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                {
                                                                    SaveDeviceConfigurationToDatabase(Main_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                                    LoadAvailableConfigurations(localDB);
                                                                }
                                                            }
                                                        }
                                                        Application.DoEvents();
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWasIncomplete));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationDownloadFailed));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToMainMonitor));
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                    }
                                }
                                else
                                {
                                    if (errorCode == ErrorCode.SerialPortNotSpecified)
                                    {
                                        RadMessageBox.Show(this, serialPortNotSetWarningText);
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()\nMain_MonitorConfiguration.monitor was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, configurationLoadCancelledText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                MonitorConnection.CloseConnection();
            }
        }

        public Main_Configuration LoadConfigurationFromDevice(int modBusAddress, int readDelayInMicroseconds)
        {
            Main_Configuration deviceConfiguration = null;
            try
            {
                Int16[] registerData = null;
         
                //if (DeviceCommunication.GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds) == 101)
                //{
                    registerData = Main_GetDeviceSetup(modBusAddress);
                    if (registerData != null)
                    {
                        deviceConfiguration = new Main_Configuration(registerData);
                    }
                //    else
                //    {
                //        RadMessageBox.Show(this, "Failed to download device configuration data.\nPlease check the connection cables and the system configuration.");
                //    }
                //}
                //else
                //{
                //    RadMessageBox.Show(this, "You are not connected to a Main monitor.  Cannot configure using this interface.");
                //}

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
            return deviceConfiguration;
        }

        private void ProgramDevice()
        {
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                int modBusAddress = 0;
                int deviceType;
                long deviceError;
                RadMessageBox.SetThemeName("office2007BlackTheme");
                ErrorCode errorCode = ErrorCode.MonitorWasNull;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Monitor monitorFromDB = null;
                bool deviceProgrammedSuccessfully = false;

                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (!downloadWasInProgress)
                    {
                        if (this.workingConfiguration != null)
                        {
                            if (Main_MonitorConfiguration.monitor != null)
                            {
                                modBusAddressAsString = monitor.ModbusAddress.Trim();
                                if (modBusAddressAsString.Length > 0)
                                {
                                    modBusAddress = Int32.Parse(modBusAddressAsString);
                                    if (!ErrorIsPresentInSomeTabObject())
                                    {
                                        WriteAllInterfaceDataToWorkingConfiguration();

                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                        {
                                            errorCode = MonitorConnection.OpenMonitorConnection(Main_MonitorConfiguration.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                        }
                                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                        {
                                            DeviceCommunication.EnableDataDownload();
                                            deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceType > 0)
                                            {
                                                if (deviceType == 101)
                                                {
                                                    Application.DoEvents();
                                                    deviceProgrammedSuccessfully = Main_SetDeviceSetup(modBusAddress);
                                                    Cursor.Current = Cursors.Default;
                                                   
                                                    Application.DoEvents();

                                                    if (deviceProgrammedSuccessfully)
                                                    {
                                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                                        {
                                                            // if the user changed the ModBus address, update that information automatically
                                                            if (modBusAddress.ToString().CompareTo(Main_MonitorConfiguration.monitor.ModbusAddress) != 0)
                                                            {
                                                                monitorFromDB = General_DatabaseMethods.GetOneMonitor(Main_MonitorConfiguration.monitor.ID, localDB);
                                                                if (monitorFromDB != null)
                                                                {
                                                                    monitorFromDB.ModbusAddress = modBusAddress.ToString();
                                                                    localDB.SubmitChanges();
                                                                }
                                                                Main_MonitorConfiguration.monitor.ModbusAddress = modBusAddress.ToString();
                                                            }
                                                            // now, delete the old device config from the DB, and save the new one.
                                                            SaveDeviceConfigurationToDatabase(Main_MonitorConfiguration.monitor, this.workingConfiguration, false, localDB, this);
                                                            LoadAvailableConfigurations(localDB);
                                                        }
                                                        /// since the configuraiton has been "saved" by using it to program the device, we update the unedited configuration
                                                        /// to match what we just wrote to the device
                                                        this.uneditedWorkingConfiguration = Main_Configuration.CopyConfiguration(this.workingConfiguration);
                                                        // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteSucceeded));
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationWriteFailed));
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.NotConnectedToMainMonitor));
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceTypeReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            if (errorCode == ErrorCode.SerialPortNotSpecified)
                                            {
                                                RadMessageBox.Show(this, serialPortNotSetWarningText);
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMain_MonitorConfiguration.monitor was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, currentConfigurationNotDefinedText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadConfigurationFromDevice()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                MonitorConnection.CloseConnection();
            }
        }
    }
}
