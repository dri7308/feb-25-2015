﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
namespace MainMonitorUtilities
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        #region Analog Inputs Tab

        private static string entryTypeVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string sensitivityVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensitivity<br>(mV/g)</html>";
        private static string boundaryFrequencyVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Boundary frequency<br>in a spectrum (Hz)</html>";
        private static string numberOfPointsVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Number<br>of Points</html>";
        private static string toSaveAtChangeVibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(mm/s)</html>";

        private static string entryTypeAnalogInGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelAnalogInGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberAnalogInGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string sensitivityAnalogInGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensitivity<br>(atm/mA)</html>";
        private static string toSaveAtChangeAnalogInGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(atm)</html>";

        private static string entryTypeCurrentGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelCurrentGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberCurrentGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string sensitivityCurrentGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensitivity<br>(A/mV)</html>";
        private static string toSaveAtChangeCurrentGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(A)</html>";

        private static string entryTypeVoltageGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelVoltageGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberVoltageGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string sensitivityVoltageGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensitivity<br>(A/mV)</html>";
        private static string toSaveAtChangeVoltageGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(V)</html>";

        private static string entryTypeHumidityGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelHumidityGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberHumidityGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string toSaveAtChangeHumidityGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(%)</html>";

        private static string entryTypeTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string sensorNumberTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Sensor<br>Number</html>";
        private static string toSaveAtChangeTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(ºC)</html>";

        private static string entryTypeChassisTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string enableChannelChassisTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Enable</html>";
        private static string inclusionThresholdChassisTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Threshold of inclusion<br>warmed the chassis (ºC)</html>";
        private static string toSaveAtChangeChassisTemperatureGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>To save at change on<br>(ºC)</html>";

        private static string onThresholdText = "on";
        private static string offThresholdText = "off";
        private static string exceedingThresholdText = "Exceeding";
        private static string diminishingThresholdText = "Diminishing";
        private static string warningThresholdText = "Warning";
        private static string alarmThresholdText = "Alarm";
        private static string ledOnlyThresholdText = "LED Only";
        private static string ledAndRelayThresholdText = "LED and Relay";

        private static string entryTypeThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Entry</html>";
        private static string thresholdNumberThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Threshold<br>Number</html>";
        private static string statusThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Status</html>";
        private static string modeThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Mode</html>";
        private static string typeThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Type</html>";
        private static string resultThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Result</html>";
        private static string timeThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Time</html>";
        private static string levelThresholdGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Level</html>";

        private static string vibrationGridViewEntryText = "Vibration";
        private static string analogInGridViewEntryText = "Analog In";
        private static string currentGridViewEntryText = "Current";
        private static string voltageGridViewEntryText = "Voltage";
        private static string humidityGridViewEntryText = "Humidity";
        private static string temperatureGridViewEntryText = "Temperature";
        private static string chassisTemperatureGridViewEntryText = "Chassis Temperature";

        private void SelectAnalogInputsTab()
        {
            configurationRadPageView.SelectedPage = analogInputsRadPageViewPage;
        }

        private void EnableAllAnalogInputsTabConfigurationEditObjects()
        {
            this.vibrationRadGridView.ReadOnly = false;
            this.analogInRadGridView.ReadOnly = false;
            this.currentRadGridView.ReadOnly = false;
            this.voltageRadGridView.ReadOnly = false;
            this.humidityRadGridView.ReadOnly = false;
            this.temperatureRadGridView.ReadOnly = false;
            this.chassisTemperatureRadGridView.ReadOnly = false;

            this.vibrationThresholdGridViewTemplate.ReadOnly = false;
            this.analogInThresholdGridViewTemplate.ReadOnly = false;
            this.currentThresholdGridViewTemplate.ReadOnly = false;
            this.voltageThresholdGridViewTemplate.ReadOnly = false;
            this.humidityThresholdGridViewTemplate.ReadOnly = false;
            this.temperatureThresholdGridViewTemplate.ReadOnly = false;
            this.chassisTemperatureThresholdGridViewTemplate.ReadOnly = false;
        }

        private void DisableAllAnalogInputsTabConfigurationEditObjects()
        {
            this.vibrationRadGridView.ReadOnly = true;
            this.analogInRadGridView.ReadOnly = true;
            this.currentRadGridView.ReadOnly = true;
            this.voltageRadGridView.ReadOnly = true;
            this.humidityRadGridView.ReadOnly = true;
            this.temperatureRadGridView.ReadOnly = true;
            this.chassisTemperatureRadGridView.ReadOnly = true;

            this.vibrationThresholdGridViewTemplate.ReadOnly = true;
            this.analogInThresholdGridViewTemplate.ReadOnly = true;
            this.currentThresholdGridViewTemplate.ReadOnly = true;
            this.voltageThresholdGridViewTemplate.ReadOnly = true;
            this.humidityThresholdGridViewTemplate.ReadOnly = true;
            this.temperatureThresholdGridViewTemplate.ReadOnly = true;
            this.chassisTemperatureThresholdGridViewTemplate.ReadOnly = true;
        }

        private void SetAllAnalogInputsGridViewLocationAndSize()
        {
            try
            {
                Point startPoint = new Point(135, 3);
                int width = 710;

                this.vibrationRadGridView.Location = startPoint;
                this.vibrationRadGridView.Size = new Size(width, 345);

                this.analogInRadGridView.Location = startPoint;
                this.analogInRadGridView.Size = new Size(width, 345);

                this.currentRadGridView.Location = startPoint;
                this.currentRadGridView.Size = new Size(width, 317);

                this.voltageRadGridView.Location = startPoint;
                this.voltageRadGridView.Size = new Size(width, 317);

                this.humidityRadGridView.Location = startPoint;
                this.humidityRadGridView.Size = new Size(width, 270);

                this.temperatureRadGridView.Location = startPoint;
                this.temperatureRadGridView.Size = new Size(width, 345);

                this.chassisTemperatureRadGridView.Location = startPoint;
                this.chassisTemperatureRadGridView.Size = new Size(width, 270);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAllAnalogInputsGridViewLocationAndSize()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CollapseGenericGridViewForAllRows(ref RadGridView inputRadGridView)
        {
            try
            {
                if ((inputRadGridView != null) && (inputRadGridView.RowCount > 0))
                {
                    int rowCount = inputRadGridView.RowCount;
                    for (int i = 0; i < rowCount; i++)
                    {
                        inputRadGridView.Rows[i].IsExpanded = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CollapseGenericGridViewForAllRows(ref RadGridView)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CollapseGridViewForAllAnalogInputsGrids()
        {
            try
            {
                CollapseGenericGridViewForAllRows(ref this.vibrationRadGridView);
                CollapseGenericGridViewForAllRows(ref this.analogInRadGridView);
                CollapseGenericGridViewForAllRows(ref this.currentRadGridView);
                CollapseGenericGridViewForAllRows(ref this.voltageRadGridView);
                CollapseGenericGridViewForAllRows(ref this.humidityRadGridView);
                CollapseGenericGridViewForAllRows(ref this.temperatureRadGridView);
                CollapseGenericGridViewForAllRows(ref this.chassisTemperatureRadGridView);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CollapseGridViewForAllAnalogInputsGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAnalogInputsRadGridViews()
        {
            try
            {
                InitializeVibrationRadGridView();
                InitializeAnalogInRadGridView();
                InitializeCurrentRadGridView();
                InitializeVoltageRadGridView();
                InitializeHumidityRadGridView();
                InitializeTemperatureRadGridView();
                InitializeChassisTemperatureRadGridView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeAnalogInputsRadGridViews()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeVibrationRadGridView()
        {
            try
            {
                GridViewTextBoxColumn vibrationNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                vibrationNumberGridViewTextBoxColumn.Name = "EntryType";
                vibrationNumberGridViewTextBoxColumn.HeaderText = entryTypeVibrationGridViewText;
                vibrationNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                vibrationNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                vibrationNumberGridViewTextBoxColumn.AllowSort = false;
                vibrationNumberGridViewTextBoxColumn.IsPinned = true;
                vibrationNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelVibrationGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberVibrationGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn sensitivityGridViewDecimalColumn = new GridViewDecimalColumn("Sensitivity");
                sensitivityGridViewDecimalColumn.Name = "Sensitivity";
                sensitivityGridViewDecimalColumn.HeaderText = sensitivityVibrationGridViewText;
                sensitivityGridViewDecimalColumn.DecimalPlaces = 3;
                sensitivityGridViewDecimalColumn.DisableHTMLRendering = false;
                sensitivityGridViewDecimalColumn.Width = sensitivityWidth;
                sensitivityGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn boundaryFrequencyGridViewDecimalColumn = new GridViewDecimalColumn("BoundaryFrequency");
                boundaryFrequencyGridViewDecimalColumn.Name = "BoundaryFrequency";
                boundaryFrequencyGridViewDecimalColumn.HeaderText = boundaryFrequencyVibrationGridViewText;
                boundaryFrequencyGridViewDecimalColumn.DecimalPlaces = 0;
                boundaryFrequencyGridViewDecimalColumn.DisableHTMLRendering = false;
                boundaryFrequencyGridViewDecimalColumn.Width = 120;
                boundaryFrequencyGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn numberOfPointsGridViewDecimalColumn = new GridViewDecimalColumn("NumberOfPoints");
                numberOfPointsGridViewDecimalColumn.Name = "NumberOfPoints";
                numberOfPointsGridViewDecimalColumn.HeaderText = numberOfPointsVibrationGridViewText;
                numberOfPointsGridViewDecimalColumn.DecimalPlaces = 0;
                numberOfPointsGridViewDecimalColumn.DisableHTMLRendering = false;
                numberOfPointsGridViewDecimalColumn.Width = 70;
                numberOfPointsGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeVibrationGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 2;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.vibrationRadGridView.MasterTemplate.Columns.Add(vibrationNumberGridViewTextBoxColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(sensitivityGridViewDecimalColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(boundaryFrequencyGridViewDecimalColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(numberOfPointsGridViewDecimalColumn);
                this.vibrationRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);
                //this.vibrationRadGridView.Columns.Add(trendWarningGridViewDecimalColumn);
                //this.vibrationRadGridView.Columns.Add(trendAlarmGridViewDecimalColumn);
                //this.vibrationRadGridView.Columns.Add(calcTrendOnGridViewDecimalColumn);
                //this.vibrationRadGridView.Columns.Add(changeWarningGridViewDecimalColumn);
                //this.vibrationRadGridView.Columns.Add(changeAlarmGridViewDecimalColumn);

                this.vibrationRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.vibrationRadGridView.AllowColumnReorder = false;
                this.vibrationRadGridView.AllowColumnChooser = false;
                this.vibrationRadGridView.ShowGroupPanel = false;
                this.vibrationRadGridView.EnableGrouping = false;
                this.vibrationRadGridView.AllowAddNewRow = false;
                this.vibrationRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeVibrationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAnalogInRadGridView()
        {
            try
            {
                GridViewTextBoxColumn analogInNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                analogInNumberGridViewTextBoxColumn.Name = "EntryType";
                analogInNumberGridViewTextBoxColumn.HeaderText = entryTypeAnalogInGridViewText;
                analogInNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                analogInNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                analogInNumberGridViewTextBoxColumn.AllowSort = false;
                analogInNumberGridViewTextBoxColumn.IsPinned = true;
                analogInNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelAnalogInGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberAnalogInGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn sensitivityGridViewDecimalColumn = new GridViewDecimalColumn("Sensitivity");
                sensitivityGridViewDecimalColumn.Name = "Sensitivity";
                sensitivityGridViewDecimalColumn.HeaderText = sensitivityAnalogInGridViewText;
                sensitivityGridViewDecimalColumn.DecimalPlaces = 3;
                sensitivityGridViewDecimalColumn.DisableHTMLRendering = false;
                sensitivityGridViewDecimalColumn.Width = sensitivityWidth;
                sensitivityGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeAnalogInGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 2;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.analogInRadGridView.MasterTemplate.Columns.Add(analogInNumberGridViewTextBoxColumn);
                this.analogInRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.analogInRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.analogInRadGridView.MasterTemplate.Columns.Add(sensitivityGridViewDecimalColumn);
                this.analogInRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.analogInRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.analogInRadGridView.AllowColumnReorder = false;
                this.analogInRadGridView.AllowColumnChooser = false;
                this.analogInRadGridView.ShowGroupPanel = false;
                this.analogInRadGridView.EnableGrouping = false;
                this.analogInRadGridView.AllowAddNewRow = false;
                this.analogInRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeAnalogInRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCurrentRadGridView()
        {
            try
            {
                GridViewTextBoxColumn currentNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                currentNumberGridViewTextBoxColumn.Name = "EntryType";
                currentNumberGridViewTextBoxColumn.HeaderText = entryTypeCurrentGridViewText;
                currentNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                currentNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                currentNumberGridViewTextBoxColumn.AllowSort = false;
                currentNumberGridViewTextBoxColumn.IsPinned = true;
                currentNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelCurrentGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberCurrentGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn sensitivityGridViewDecimalColumn = new GridViewDecimalColumn("Sensitivity");
                sensitivityGridViewDecimalColumn.Name = "Sensitivity";
                sensitivityGridViewDecimalColumn.HeaderText = sensitivityCurrentGridViewText;
                sensitivityGridViewDecimalColumn.DecimalPlaces = 5;
                sensitivityGridViewDecimalColumn.DisableHTMLRendering = false;
                sensitivityGridViewDecimalColumn.Width = sensitivityWidth;
                sensitivityGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeCurrentGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 1;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.currentRadGridView.MasterTemplate.Columns.Add(currentNumberGridViewTextBoxColumn);
                this.currentRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.currentRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.currentRadGridView.MasterTemplate.Columns.Add(sensitivityGridViewDecimalColumn);
                this.currentRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.currentRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.currentRadGridView.AllowColumnReorder = false;
                this.currentRadGridView.AllowColumnChooser = false;
                this.currentRadGridView.ShowGroupPanel = false;
                this.currentRadGridView.EnableGrouping = false;
                this.currentRadGridView.AllowAddNewRow = false;
                this.currentRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeCurrentRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeVoltageRadGridView()
        {
            try
            {
                GridViewTextBoxColumn voltageNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                voltageNumberGridViewTextBoxColumn.Name = "EntryType";
                voltageNumberGridViewTextBoxColumn.HeaderText = entryTypeVoltageGridViewText;
                voltageNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                voltageNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                voltageNumberGridViewTextBoxColumn.AllowSort = false;
                voltageNumberGridViewTextBoxColumn.IsPinned = true;
                voltageNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelVoltageGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberVoltageGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn sensitivityGridViewDecimalColumn = new GridViewDecimalColumn("Sensitivity");
                sensitivityGridViewDecimalColumn.Name = "Sensitivity";
                sensitivityGridViewDecimalColumn.HeaderText = sensitivityVoltageGridViewText;
                sensitivityGridViewDecimalColumn.DecimalPlaces = 3;
                sensitivityGridViewDecimalColumn.DisableHTMLRendering = false;
                sensitivityGridViewDecimalColumn.Width = sensitivityWidth;
                sensitivityGridViewDecimalColumn.AllowSort = false;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeVoltageGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 1;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.voltageRadGridView.MasterTemplate.Columns.Add(voltageNumberGridViewTextBoxColumn);
                this.voltageRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.voltageRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.voltageRadGridView.MasterTemplate.Columns.Add(sensitivityGridViewDecimalColumn);
                this.voltageRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.voltageRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.voltageRadGridView.AllowColumnReorder = false;
                this.voltageRadGridView.AllowColumnChooser = false;
                this.voltageRadGridView.ShowGroupPanel = false;
                this.voltageRadGridView.EnableGrouping = false;
                this.voltageRadGridView.AllowAddNewRow = false;
                this.voltageRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeVoltageRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeHumidityRadGridView()
        {
            try
            {
                GridViewTextBoxColumn humidityNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                humidityNumberGridViewTextBoxColumn.Name = "EntryType";
                humidityNumberGridViewTextBoxColumn.HeaderText = entryTypeHumidityGridViewText;
                humidityNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                humidityNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                humidityNumberGridViewTextBoxColumn.AllowSort = false;
                humidityNumberGridViewTextBoxColumn.IsPinned = true;
                humidityNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelHumidityGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberHumidityGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeHumidityGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 1;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.humidityRadGridView.MasterTemplate.Columns.Add(humidityNumberGridViewTextBoxColumn);
                this.humidityRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.humidityRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.humidityRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.humidityRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.humidityRadGridView.AllowColumnReorder = false;
                this.humidityRadGridView.AllowColumnChooser = false;
                this.humidityRadGridView.ShowGroupPanel = false;
                this.humidityRadGridView.EnableGrouping = false;
                this.humidityRadGridView.AllowAddNewRow = false;
                this.humidityRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeHumidityRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeTemperatureRadGridView()
        {
            try
            {
                GridViewTextBoxColumn temperatureNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                temperatureNumberGridViewTextBoxColumn.Name = "EntryType";
                temperatureNumberGridViewTextBoxColumn.HeaderText = entryTypeTemperatureGridViewText;
                temperatureNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                temperatureNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                temperatureNumberGridViewTextBoxColumn.AllowSort = false;
                temperatureNumberGridViewTextBoxColumn.IsPinned = true;
                temperatureNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelTemperatureGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn sensorNumberGridViewDecimalColumn = new GridViewDecimalColumn("SensorNumber");
                sensorNumberGridViewDecimalColumn.Name = "SensorNumber";
                sensorNumberGridViewDecimalColumn.HeaderText = sensorNumberTemperatureGridViewText;
                sensorNumberGridViewDecimalColumn.DecimalPlaces = 0;
                sensorNumberGridViewDecimalColumn.DisableHTMLRendering = false;
                sensorNumberGridViewDecimalColumn.Width = this.sensorNumberWidth;
                sensorNumberGridViewDecimalColumn.AllowSort = false;
                //sensorNumberGridViewDecimalColumn.IsPinned = true;
                //sensorNumberGridViewDecimalColumn.ReadOnly = true;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeTemperatureGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 1;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.temperatureRadGridView.MasterTemplate.Columns.Add(temperatureNumberGridViewTextBoxColumn);
                this.temperatureRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.temperatureRadGridView.MasterTemplate.Columns.Add(sensorNumberGridViewDecimalColumn);
                this.temperatureRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.temperatureRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.temperatureRadGridView.AllowColumnReorder = false;
                this.temperatureRadGridView.AllowColumnChooser = false;
                this.temperatureRadGridView.ShowGroupPanel = false;
                this.temperatureRadGridView.EnableGrouping = false;
                this.temperatureRadGridView.AllowAddNewRow = false;
                this.temperatureRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeTemperatureRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeChassisTemperatureRadGridView()
        {
            try
            {
                GridViewTextBoxColumn temperatureNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                temperatureNumberGridViewTextBoxColumn.Name = "EntryType";
                temperatureNumberGridViewTextBoxColumn.HeaderText = entryTypeChassisTemperatureGridViewText;
                temperatureNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                temperatureNumberGridViewTextBoxColumn.Width = this.entryTypeWidth;
                temperatureNumberGridViewTextBoxColumn.AllowSort = false;
                temperatureNumberGridViewTextBoxColumn.IsPinned = true;
                temperatureNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewCheckBoxColumn enableEntryGridViewCheckBoxColumn = new GridViewCheckBoxColumn("EnableChannel");
                enableEntryGridViewCheckBoxColumn.Name = "EnableChannel";
                enableEntryGridViewCheckBoxColumn.HeaderText = enableChannelChassisTemperatureGridViewText;
                enableEntryGridViewCheckBoxColumn.DisableHTMLRendering = false;
                enableEntryGridViewCheckBoxColumn.Width = this.enableEntryWidth;
                enableEntryGridViewCheckBoxColumn.AllowSort = false;

                GridViewDecimalColumn inclusionThresholdGridViewDecimalColumn = new GridViewDecimalColumn("InclusionThreshold");
                inclusionThresholdGridViewDecimalColumn.Name = "InclusionThreshold";
                inclusionThresholdGridViewDecimalColumn.HeaderText = inclusionThresholdChassisTemperatureGridViewText;
                inclusionThresholdGridViewDecimalColumn.DecimalPlaces = 0;
                inclusionThresholdGridViewDecimalColumn.DisableHTMLRendering = false;
                inclusionThresholdGridViewDecimalColumn.Width = 150;
                inclusionThresholdGridViewDecimalColumn.AllowSort = false;
                //inclusionThresholdGridViewTextBoxColumn.IsPinned = true;
                //inclusionThresholdGridViewTextBoxColumn.ReadOnly = true;

                GridViewDecimalColumn toSaveGridViewDecimalColumn = new GridViewDecimalColumn("ToSave");
                toSaveGridViewDecimalColumn.Name = "ToSave";
                toSaveGridViewDecimalColumn.HeaderText = toSaveAtChangeChassisTemperatureGridViewText;
                toSaveGridViewDecimalColumn.DecimalPlaces = 1;
                toSaveGridViewDecimalColumn.DisableHTMLRendering = false;
                toSaveGridViewDecimalColumn.Width = this.toSaveWidth;
                toSaveGridViewDecimalColumn.AllowSort = false;

                this.chassisTemperatureRadGridView.MasterTemplate.Columns.Add(temperatureNumberGridViewTextBoxColumn);
                this.chassisTemperatureRadGridView.MasterTemplate.Columns.Add(enableEntryGridViewCheckBoxColumn);
                this.chassisTemperatureRadGridView.MasterTemplate.Columns.Add(inclusionThresholdGridViewDecimalColumn);
                this.chassisTemperatureRadGridView.MasterTemplate.Columns.Add(toSaveGridViewDecimalColumn);

                this.chassisTemperatureRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.chassisTemperatureRadGridView.AllowColumnReorder = false;
                this.chassisTemperatureRadGridView.AllowColumnChooser = false;
                this.chassisTemperatureRadGridView.ShowGroupPanel = false;
                this.chassisTemperatureRadGridView.EnableGrouping = false;
                this.chassisTemperatureRadGridView.AllowAddNewRow = false;
                this.chassisTemperatureRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeChassisTemperatureRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAllThresholdRadGridViews()
        {
            try
            {
                this.vibrationThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.analogInThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.currentThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.voltageThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.humidityThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.temperatureThresholdGridViewTemplate = InitializeThresholdRadGridView();
                this.chassisTemperatureThresholdGridViewTemplate = InitializeThresholdRadGridView();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeAllThresholdRadGridViews()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private GridViewTemplate InitializeThresholdRadGridView()
        {
            GridViewTemplate genericThresholdGridViewTemplate = new GridViewTemplate();
            try
            {
                GridViewTextBoxColumn entryTypeGridViewTextBoxColumn = new GridViewTextBoxColumn("EntryType");
                entryTypeGridViewTextBoxColumn.Name = "EntryType";
                entryTypeGridViewTextBoxColumn.HeaderText = entryTypeThresholdGridViewText;
                entryTypeGridViewTextBoxColumn.DisableHTMLRendering = false;
                entryTypeGridViewTextBoxColumn.Width = entryTypeWidth;
                entryTypeGridViewTextBoxColumn.AllowSort = false;
                entryTypeGridViewTextBoxColumn.IsPinned = true;
                entryTypeGridViewTextBoxColumn.ReadOnly = true;

                GridViewTextBoxColumn thresholdNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("ThresholdNumber");
                thresholdNumberGridViewTextBoxColumn.Name = "ThresholdNumber";
                thresholdNumberGridViewTextBoxColumn.HeaderText = thresholdNumberThresholdGridViewText;
                thresholdNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                thresholdNumberGridViewTextBoxColumn.Width = 75;
                thresholdNumberGridViewTextBoxColumn.AllowSort = false;
                thresholdNumberGridViewTextBoxColumn.IsPinned = true;
                thresholdNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewComboBoxColumn statusGridViewComboBoxColumn = new GridViewComboBoxColumn("Status");
                statusGridViewComboBoxColumn.Name = "Status";
                statusGridViewComboBoxColumn.HeaderText = statusThresholdGridViewText;
                statusGridViewComboBoxColumn.DataSource = new String[] { offThresholdText, onThresholdText };
                statusGridViewComboBoxColumn.DisableHTMLRendering = false;
                statusGridViewComboBoxColumn.Width = 50;
                statusGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn modeGridViewComboBoxColumn = new GridViewComboBoxColumn("Mode");
                modeGridViewComboBoxColumn.Name = "Mode";
                modeGridViewComboBoxColumn.HeaderText = modeThresholdGridViewText;
                modeGridViewComboBoxColumn.DataSource = new String[] { exceedingThresholdText, diminishingThresholdText };
                modeGridViewComboBoxColumn.DisableHTMLRendering = false;
                modeGridViewComboBoxColumn.Width = 90;
                modeGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn typeGridViewComboBoxColumn = new GridViewComboBoxColumn("Type");
                typeGridViewComboBoxColumn.Name = "Type";
                typeGridViewComboBoxColumn.HeaderText = typeThresholdGridViewText;
                typeGridViewComboBoxColumn.DataSource = new String[] { warningThresholdText, alarmThresholdText };
                typeGridViewComboBoxColumn.DisableHTMLRendering = false;
                typeGridViewComboBoxColumn.Width = 80;
                typeGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn resultGridViewComboBoxColumn = new GridViewComboBoxColumn("Result");
                resultGridViewComboBoxColumn.Name = "Result";
                resultGridViewComboBoxColumn.HeaderText = resultThresholdGridViewText;
                resultGridViewComboBoxColumn.DataSource = new String[] { ledOnlyThresholdText, ledAndRelayThresholdText };
                resultGridViewComboBoxColumn.DisableHTMLRendering = false;
                resultGridViewComboBoxColumn.Width = 90;
                resultGridViewComboBoxColumn.AllowSort = false;

                GridViewComboBoxColumn timeGridViewComboBoxColumn = new GridViewComboBoxColumn("Time");
                timeGridViewComboBoxColumn.Name = "Time";
                timeGridViewComboBoxColumn.HeaderText = timeThresholdGridViewText;
                timeGridViewComboBoxColumn.DataSource = new String[] { "0 s.", "1 s.", "2 s.", "3 s.", "4 s.", "5 s." };
                timeGridViewComboBoxColumn.DisableHTMLRendering = false;
                timeGridViewComboBoxColumn.Width = 60;
                timeGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn levelGridViewDecimalColumn = new GridViewDecimalColumn("Level");
                levelGridViewDecimalColumn.Name = "Level";
                levelGridViewDecimalColumn.HeaderText = levelThresholdGridViewText;
                levelGridViewDecimalColumn.DecimalPlaces = 0;
                levelGridViewDecimalColumn.DisableHTMLRendering = false;
                levelGridViewDecimalColumn.Width = 60;
                levelGridViewDecimalColumn.AllowSort = false;

                genericThresholdGridViewTemplate.Columns.Add(entryTypeGridViewTextBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(thresholdNumberGridViewTextBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(statusGridViewComboBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(modeGridViewComboBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(typeGridViewComboBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(resultGridViewComboBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(timeGridViewComboBoxColumn);
                genericThresholdGridViewTemplate.Columns.Add(levelGridViewDecimalColumn);

                genericThresholdGridViewTemplate.AllowColumnReorder = false;
                genericThresholdGridViewTemplate.AllowColumnChooser = false;
                // genericThresholdGridViewTemplate.ShowGroupPanel = false;
                genericThresholdGridViewTemplate.EnableGrouping = false;
                genericThresholdGridViewTemplate.AllowAddNewRow = false;
                genericThresholdGridViewTemplate.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeThresholdRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return genericThresholdGridViewTemplate;
        }

        private void EstablishAllParentChildGridRelations()
        {
            try
            { /*
                this.vibrationRadGridView.MasterTemplate.Templates.Add(vibrationThresholdGridViewTemplate);

                vibrationGridViewRelation = new GridViewRelation(this.vibrationRadGridView.MasterTemplate);
                vibrationGridViewRelation.ChildTemplate = this.vibrationThresholdGridViewTemplate;
                vibrationGridViewRelation.RelationName = "ParentChild";
                vibrationGridViewRelation.ParentColumnNames.Add("EntryType");
                vibrationGridViewRelation.ChildColumnNames.Add("EntryType");

                this.vibrationRadGridView.Relations.Add(this.vibrationGridViewRelation);


                this.analogInRadGridView.MasterTemplate.Templates.Add(analogInThresholdGridViewTemplate);

                analogInGridViewRelation = new GridViewRelation(this.analogInRadGridView.MasterTemplate);
                analogInGridViewRelation.ChildTemplate = analogInThresholdGridViewTemplate;
                analogInGridViewRelation.RelationName = "ParentChild";
                analogInGridViewRelation.ParentColumnNames.Add("EntryType");
                analogInGridViewRelation.ChildColumnNames.Add("EntryType");

                this.analogInRadGridView.Relations.Add(analogInGridViewRelation);


                this.currentRadGridView.MasterTemplate.Templates.Add(currentThresholdGridViewTemplate);

                currentGridViewRelation = new GridViewRelation(currentRadGridView.MasterTemplate);
                currentGridViewRelation.ChildTemplate = currentThresholdGridViewTemplate;
                currentGridViewRelation.RelationName = "ParentChild";
                currentGridViewRelation.ParentColumnNames.Add("EntryType");
                currentGridViewRelation.ChildColumnNames.Add("EntryType");

                this.currentRadGridView.Relations.Add(currentGridViewRelation);


                this.voltageRadGridView.MasterTemplate.Templates.Add(voltageThresholdGridViewTemplate);

                voltageGridViewRelation = new GridViewRelation(voltageRadGridView.MasterTemplate);
                voltageGridViewRelation.ChildTemplate = voltageThresholdGridViewTemplate;
                voltageGridViewRelation.RelationName = "ParentChild";
                voltageGridViewRelation.ParentColumnNames.Add("EntryType");
                voltageGridViewRelation.ChildColumnNames.Add("EntryType");

                this.voltageRadGridView.Relations.Add(voltageGridViewRelation);


                this.humidityRadGridView.MasterTemplate.Templates.Add(humidityThresholdGridViewTemplate);

                humidityGridViewRelation = new GridViewRelation(humidityRadGridView.MasterTemplate);
                humidityGridViewRelation.ChildTemplate = humidityThresholdGridViewTemplate;
                humidityGridViewRelation.RelationName = "ParentChild";
                humidityGridViewRelation.ParentColumnNames.Add("EntryType");
                humidityGridViewRelation.ChildColumnNames.Add("EntryType");

                this.humidityRadGridView.Relations.Add(humidityGridViewRelation);


                this.temperatureRadGridView.MasterTemplate.Templates.Add(temperatureThresholdGridViewTemplate);

                temperatureGridViewRelation = new GridViewRelation(temperatureRadGridView.MasterTemplate);
                temperatureGridViewRelation.ChildTemplate = temperatureThresholdGridViewTemplate;
                temperatureGridViewRelation.RelationName = "ParentChild";
                temperatureGridViewRelation.ParentColumnNames.Add("EntryType");
                temperatureGridViewRelation.ChildColumnNames.Add("EntryType");

                this.temperatureRadGridView.Relations.Add(temperatureGridViewRelation);


                this.chassisTemperatureRadGridView.MasterTemplate.Templates.Add(chassisTemperatureThresholdGridViewTemplate);

                chassisTemperatureGridViewRelation = new GridViewRelation(chassisTemperatureRadGridView.MasterTemplate);
                chassisTemperatureGridViewRelation.ChildTemplate = chassisTemperatureThresholdGridViewTemplate;
                chassisTemperatureGridViewRelation.RelationName = "ParentChild";
                chassisTemperatureGridViewRelation.ParentColumnNames.Add("EntryType");
                chassisTemperatureGridViewRelation.ChildColumnNames.Add("EntryType");

                this.chassisTemperatureRadGridView.Relations.Add(chassisTemperatureGridViewRelation);*/
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.EstablishAllParentChildGridRelations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheAnalogInputsGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                AddVibrationDataToTheGrids(inputConfiguration);
                AddAnalogInDataToTheGrids(inputConfiguration);
                AddCurrentDataToTheGrids(inputConfiguration);
                AddVoltageDataToTheGrids(inputConfiguration);
                AddHumidityDataToTheGrids(inputConfiguration);
                AddTemperatureDataToTheGrids(inputConfiguration);
                AddChassisTemperatureDataToTheGrids(inputConfiguration);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheAnalogInputsGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddVibrationDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.vibrationRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheVibrationGrid();
                        //AddRowsToTheVibrationThresholdGrid();
                        AddDataToTheVibrationGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheVibrationThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddVibrationDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddVibrationDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddAnalogInDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.analogInRadGridView.DeferRefresh())
                    {
                        //AddRowsToThePressureGrid();
                        //AddRowsToThePressureThresholdGrid();
                        AddDataToTheAnalogInGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheAnalogInThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddAnalogInDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddAnalogInDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddCurrentDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.currentRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheCurrentGrid();
                        //AddRowsToTheCurrentThresholdGrid();
                        AddDataToTheCurrentGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheCurrentThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddCurrentDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddCurrentDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddVoltageDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.voltageRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheVoltageGrid();
                        //AddRowsToTheVoltageThresholdGrid();
                        AddDataToTheVoltageGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheVoltageThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddVoltageDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddVoltageDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddHumidityDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.humidityRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheHumidityGrid();
                        //AddRowsToTheHumidityThresholdGrid();
                        AddDataToTheHumidityGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheHumidityThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddHumidityDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddHumidityDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddTemperatureDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.temperatureRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheTemperatureGrid();
                        //AddRowsToTheTemperatureThresholdGrid();
                        AddDataToTheTemperatureGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheTemperatureThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddTemperatureDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddTemperatureDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddChassisTemperatureDataToTheGrids(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    using (this.chassisTemperatureRadGridView.DeferRefresh())
                    {
                        //AddRowsToTheChassisTemperatureGrid();
                        //AddRowsToTheChassisTemperatureThresholdGrid();
                        AddDataToTheChassisTemperatureGrid(inputConfiguration.inputChannelConfigObjects);
                        AddDataToTheChassisTemperatureThresholdGrid(inputConfiguration.inputChannelThresholdConfigObjects);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddChassisTemperatureDataToTheGrids()\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddChassisTemperatureDataToTheGrids()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddEmptyRowsToAllAnalogInputsGrids()
        {
            AddEmptyRowsToVibrationGrid();
            AddEmptyRowsToVibrationThresholdGrid();
            AddEmptyRowsToAnalogInGrid();
            AddEmptyRowsToAnalogInThresholdGrid();
            AddEmptyRowsToCurrentGrid();
            AddEmptyRowsToCurrentThresholdGrid();
            AddEmptyRowsToVoltageGrid();
            AddEmptyRowsToVoltageThresholdGrid();
            AddEmptyRowsToHumidityGrid();
            AddEmptyRowsToHumidityThresholdGrid();
            AddEmptyRowsToTemperatureGrid();
            AddEmptyRowsToTemperatureThresholdGrid();
            AddEmptyRowsToChassisTemperatureGrid();
            AddEmptyRowsToChassisTemperatureThresholdGrid();
        }

        void AddEmptyRowsToVibrationGrid()
        {
            try
            {
                this.vibrationRadGridView.MasterTemplate.Rows.Clear();
                for (int i = 0; i < 4; i++)
                {
                    this.vibrationRadGridView.MasterTemplate.Rows.Add(vibrationGridViewEntryText + " " + (i + 1).ToString(), false, 0, 0, 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToVibrationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ChannelIsEnabled(int readStatus)
        {
            bool enabled;
            if (readStatus == 0)
            {
                enabled = false;
            }
            else
            {
                enabled = true;
            }
            return enabled;
        }

        void AddDataToTheVibrationGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                // VibrationData vibrationData;
                Main_ConfigComponent_InputChannel configInputChannel;

                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 3)
                    {
                        if (this.vibrationRadGridView.MasterTemplate.RowCount == 4)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                rowInfo = this.vibrationRadGridView.MasterTemplate.Rows[i];
                                configInputChannel = inputChannelList[i];
                                // vibrationData = this.vibrationDataList[i];

                                rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                                rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                                rowInfo.Cells[3].Value = Math.Round(configInputChannel.Sensitivity, 3);
                                rowInfo.Cells[4].Value = configInputChannel.ReadFrequency;
                                rowInfo.Cells[5].Value = configInputChannel.ReadPoints;
                                rowInfo.Cells[6].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 100.0), 2);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVibrationGrid()\nthis.vibrationRadGridView.MasterTemplate did not have 4 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVibrationGrid()\nInput List<Main_ConfigComponent_InputChannel> did not have enough elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVibrationGrid()\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheVibrationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddEmptyRowsToAnalogInGrid()
        {
            try
            {
                this.analogInRadGridView.MasterTemplate.Rows.Clear();
                for (int i = 0; i < 6; i++)
                {
                    this.analogInRadGridView.MasterTemplate.Rows.Add(analogInGridViewEntryText + " " + (i + 1).ToString(), false, 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToAnalogInGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddDataToTheAnalogInGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                // PressureData pressureData;
                Main_ConfigComponent_InputChannel configInputChannel;

                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 9)
                    {
                        if (this.analogInRadGridView.MasterTemplate.RowCount == 6)
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                rowInfo = this.analogInRadGridView.MasterTemplate.Rows[i];
                                configInputChannel = inputChannelList[i + 4];
                                // pressureData = this.pressureDataList[i];

                                rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                                rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                                rowInfo.Cells[3].Value = Math.Round(configInputChannel.Sensitivity, 3);
                                rowInfo.Cells[4].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 100.0), 2);

                                // this.pressureRadGridView.MasterTemplate.Rows.Add(entryType, pressureData.enabled, pressureData.sensorNumber, pressureData.sensitivity, pressureData.toSave);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheAnalogInGrid()\nthis.pressureRadGridView.MasterTemplate did not have 6 elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheAnalogInGrid()\nInput List<Main_ConfigComponent_InputChannel>  did not have enough elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheAnalogInGrid()\nInput List<Main_ConfigComponent_InputChannel>  was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheAnalogInGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddEmptyRowsToCurrentGrid()
        {
            try
            {
                this.currentRadGridView.MasterTemplate.Rows.Clear();
                for (int i = 0; i < 3; i++)
                {
                    this.currentRadGridView.MasterTemplate.Rows.Add(currentGridViewEntryText + " " + (i + 1).ToString(), false, 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToCurrentGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddDataToTheCurrentGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_InputChannel configInputChannel;

                // CurrentData currentData;
                // string entryType;
                //if (this.currentDataList != null)
                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 12)
                    {
                        if (this.currentRadGridView.MasterTemplate.RowCount == 3)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                rowInfo = this.currentRadGridView.MasterTemplate.Rows[i];
                                configInputChannel = inputChannelList[i + 10];

                                rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                                rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                                rowInfo.Cells[3].Value = Math.Round(configInputChannel.Sensitivity, 5);
                                rowInfo.Cells[4].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 10.0), 1);

                                //currentData = this.currentDataList[i];
                                // entryType = "Current " + (i + 1).ToString();
                                //this.currentRadGridView.MasterTemplate.Rows.Add(entryType, currentData.enabled, currentData.sensorNumber, currentData.sensitivity, currentData.toSave);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCurrentGrid()\nthis.currentRadGridView.MasterTemplate did not have 3 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCurrentGrid()\nInput List<Main_ConfigComponent_InputChannel>  had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCurrentGrid()\ntInput List<Main_ConfigComponent_InputChannel>  was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheCurrentGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddEmptyRowsToVoltageGrid()
        {
            try
            {
                this.voltageRadGridView.MasterTemplate.Rows.Clear();
                for (int i = 0; i < 3; i++)
                {
                    this.voltageRadGridView.MasterTemplate.Rows.Add(voltageGridViewEntryText + " " + (i + 1).ToString(), false, 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToVoltageGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddDataToTheVoltageGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_InputChannel configInputChannel;

                // CurrentData currentData;
                // string entryType;
                //if (this.currentDataList != null)
                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 15)
                    {
                        if (this.voltageRadGridView.MasterTemplate.RowCount == 3)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                rowInfo = this.voltageRadGridView.MasterTemplate.Rows[i];
                                configInputChannel = inputChannelList[i + 13];

                                rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                                rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                                rowInfo.Cells[3].Value = Math.Round(configInputChannel.Sensitivity, 3);
                                rowInfo.Cells[4].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 10.0), 1);

                                //currentData = this.currentDataList[i];
                                // entryType = "Current " + (i + 1).ToString();
                                //this.voltageRadGridView.MasterTemplate.Rows.Add(entryType, currentData.enabled, currentData.sensorNumber, currentData.sensitivity, currentData.toSave);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVoltageGrid()\nthis.voltageRadGridView.MasterTemplate did not have 3 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVoltageGrid()\nInput List<Main_ConfigComponent_InputChannel>  had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVoltageGrid()\nthis.voltageRadGridView.MasterTemplate did not have 3 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheVoltageGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        void AddEmptyRowsToHumidityGrid()
        {
            this.humidityRadGridView.MasterTemplate.Rows.Clear();
            this.humidityRadGridView.MasterTemplate.Rows.Add(humidityGridViewEntryText, false, 0, 0);
        }

        void AddDataToTheHumidityGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_InputChannel configInputChannel;

                // CurrentData currentData;
                // string entryType;
                //if (this.currentDataList != null)
                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 16)
                    {
                        if (this.humidityRadGridView.MasterTemplate.RowCount == 1)
                        {
                            rowInfo = this.humidityRadGridView.MasterTemplate.Rows[0];
                            configInputChannel = inputChannelList[16];

                            rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                            rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                            rowInfo.Cells[3].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 10.0), 2);

                            //currentData = this.currentDataList[i];
                            // entryType = "Current " + (i + 1).ToString();
                            //this.voltageRadGridView.MasterTemplate.Rows.Add(entryType, currentData.enabled, currentData.sensorNumber, currentData.sensitivity, currentData.toSave);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidityGrid()\nthis.voltageRadGridView.MasterTemplate did not have 1 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidityGrid()\nInput List<Main_ConfigComponent_InputChannel>  had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidtyGrid()\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheHumidtyGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        //        void AddDataToTheHumidityGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        //        {
        //            try
        //            {
        //                if (this.humidityData != null)
        //                {
        //                    this.humidityRadGridView.MasterTemplate.Rows.Clear();
        //                    string entryType = "Humidity";
        //                    this.humidityRadGridView.MasterTemplate.Rows.Add(entryType, this.humidityData.enabled, this.humidityData.sensorNumber, this.humidityData.toSave);
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidityGrid()\nthis.humidityData was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheHumidityGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToTemperatureGrid()
        {
            try
            {
                this.temperatureRadGridView.MasterTemplate.Rows.Clear();
                for (int i = 0; i < 7; i++)
                {
                    this.temperatureRadGridView.MasterTemplate.Rows.Add(temperatureGridViewEntryText + " " + (i + 1).ToString(), false, 0, 0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToTemperatureGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheTemperatureGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_InputChannel configInputChannel;

                // CurrentData currentData;
                // string entryType;
                //if (this.currentDataList != null)
                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 23)
                    {
                        if (this.temperatureRadGridView.MasterTemplate.RowCount == 7)
                        {
                            for (int i = 0; i < 7; i++)
                            {
                                rowInfo = this.temperatureRadGridView.MasterTemplate.Rows[i];
                                configInputChannel = inputChannelList[i + 17];

                                rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                                rowInfo.Cells[2].Value = configInputChannel.SensorNumber;
                                rowInfo.Cells[3].Value = Math.Round((configInputChannel.SaveDataIfChangedValue / 10.0), 1);

                                //currentData = this.currentDataList[i];
                                // entryType = "Current " + (i + 1).ToString();
                                //this.TemperatureRadGridView.MasterTemplate.Rows.Add(entryType, currentData.enabled, currentData.sensorNumber, currentData.sensitivity, currentData.toSave);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheTemperatureGrid()\nthis.TemperatureRadGridView.MasterTemplate did not have 7 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheTemperatureGrid()\nInput List<Main_ConfigComponent_InputChannel>  had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheTemperatureGrid()\nthis.TemperatureRadGridView.MasterTemplate did not have 3 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheTemperatureGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        //        void AddRowsToTheTemperatureGrid()
        //        {
        //            try
        //            {
        //                TemperatureData temperatureData;
        //                string entryType;
        //                if (this.temperatureDataList != null)
        //                {
        //                    if (this.temperatureDataList.Count > 2)
        //                    {
        //                        this.temperatureRadGridView.MasterTemplate.Rows.Clear();
        //                        for (int i = 0; i < 7; i++)
        //                        {
        //                            temperatureData = this.temperatureDataList[i];
        //                            entryType = "Temperature " + (i + 1).ToString();
        //                            this.temperatureRadGridView.MasterTemplate.Rows.Add(entryType, temperatureData.enabled, temperatureData.sensorNumber, temperatureData.toSave);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheTemperatureGrid()\nthis.temperatureDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheTemperatureGrid()\nthis.temperatureDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheTemperatureGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        void AddEmptyRowsToChassisTemperatureGrid()
        {
            this.chassisTemperatureRadGridView.MasterTemplate.Rows.Clear();
            this.chassisTemperatureRadGridView.MasterTemplate.Rows.Add(chassisTemperatureGridViewEntryText, false, 0, 0);
        }

        void AddDataToTheChassisTemperatureGrid(List<Main_ConfigComponent_InputChannel> inputChannelList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_InputChannel configInputChannel;

                // CurrentData currentData;
                // string entryType;
                //if (this.currentDataList != null)
                if (inputChannelList != null)
                {
                    if (inputChannelList.Count > 23)
                    {
                        if (this.chassisTemperatureRadGridView.MasterTemplate.RowCount == 1)
                        {
                            rowInfo = this.chassisTemperatureRadGridView.MasterTemplate.Rows[0];
                            configInputChannel = inputChannelList[24];

                            rowInfo.Cells[1].Value = ChannelIsEnabled(configInputChannel.ReadStatus);
                            rowInfo.Cells[2].Value = 0;
                            rowInfo.Cells[3].Value = Math.Round(configInputChannel.SaveDataIfChangedValue / 10.0, 1);

                            //currentData = this.currentDataList[i];
                            // entryType = "Current " + (i + 1).ToString();
                            //this.voltageRadGridView.MasterTemplate.Rows.Add(entryType, currentData.enabled, currentData.sensorNumber, currentData.sensitivity, currentData.toSave);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheChassisTemperatureGrid()\nthis.voltageRadGridView.MasterTemplate did not have 1 rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheChassisTemperatureGrid()\nInput List<Main_ConfigComponent_InputChannel>  had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheChassisTemperatureGrid()\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheChassisTemperatureGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        void AddRowToTheChassisTemperatureGrid()
        //        {
        //            try
        //            {
        //                if (this.chassisTemperatureData != null)
        //                {
        //                    this.chassisTemperatureRadGridView.MasterTemplate.Rows.Clear();
        //                    string entryType = "Chassis Temperature";
        //                    this.chassisTemperatureRadGridView.MasterTemplate.Rows.Add(entryType, this.chassisTemperatureData.enabled, this.chassisTemperatureData.threshold, this.chassisTemperatureData.toSave);
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowToTheChassisTemperatureGrid()\nthis.chassisTemperatureData was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowToTheChassisTemperatureGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }


        private void AddDataToThresholdGrid(ref GridViewTemplate thresholdGridViewTemplate, List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList, int startingOffset)
        {
            try
            {
                Main_ConfigComponent_InputChannelThreshold inputChannelThreshold;
                GridViewRowInfo rowInfo;
                int entryCount;
                int i, j;

                string status;
                string mode;
                string type;
                string result;
                string time;
                double level;

                if (thresholdGridViewTemplate != null)
                {
                    entryCount = thresholdGridViewTemplate.RowCount / 6;
                    if (thresholdDataList != null)
                    {
                        if (thresholdDataList.Count > (startingOffset + (6 * entryCount)))
                        {
                            for (i = 0; i < entryCount; i++)
                            {
                                for (j = 0; j < 6; j++)
                                {
                                    inputChannelThreshold = thresholdDataList[startingOffset + (6 * i) + j];
                                    rowInfo = thresholdGridViewTemplate.Rows[(6 * i) + j];

                                    if (inputChannelThreshold.ThresholdEnable == 0)
                                    {
                                        status = offThresholdText;
                                    }
                                    else
                                    {
                                        status = onThresholdText;
                                    }

                                    if (inputChannelThreshold.WorkingBy == 0)
                                    {
                                        mode = exceedingThresholdText;
                                    }
                                    else
                                    {
                                        mode = diminishingThresholdText;
                                    }

                                    if (inputChannelThreshold.ThresholdStatus == 0)
                                    {
                                        type = warningThresholdText;
                                    }
                                    else
                                    {
                                        type = alarmThresholdText;
                                    }

                                    if (inputChannelThreshold.RelayMode == 0)
                                    {
                                        result = ledOnlyThresholdText;
                                    }
                                    else
                                    {
                                        result = ledAndRelayThresholdText;
                                    }
                                    time = GetThresholdTimeAsStringFromInteger(inputChannelThreshold.RelayStatusOnDelay);
                                    level = inputChannelThreshold.ThresholdValue;

                                    rowInfo.Cells[2].Value = status;
                                    rowInfo.Cells[3].Value = mode;
                                    rowInfo.Cells[4].Value = type;
                                    rowInfo.Cells[5].Value = result;
                                    rowInfo.Cells[6].Value = time;
                                    rowInfo.Cells[7].Value = level;
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToThresholdGrid(ref GridViewTemplate, List<Main_ConfigComponent_InputChannelThreshold>, int)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToThresholdGrid(ref GridViewTemplate, List<Main_ConfigComponent_InputChannelThreshold>, int)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToThresholdGrid(ref GridViewTemplate, List<Main_ConfigComponent_InputChannelThreshold>, int)\nInput GridViewTemplate was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToThresholdGrid(ref GridViewTemplate, List<Main_ConfigComponent_InputChannelThreshold>, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToVibrationThresholdGrid()
        {
            try
            {
                int i, j;
                string entryType;
                this.vibrationThresholdGridViewTemplate.Rows.Clear();
                for (i = 0; i < 4; i++)
                {
                    entryType = vibrationGridViewEntryText + " " + (i + 1).ToString();
                    for (j = 0; j < 6; j++)
                    {
                        this.vibrationThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToVibrationThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheVibrationThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > 23)
                    {
                        AddDataToThresholdGrid(ref this.vibrationThresholdGridViewTemplate, thresholdDataList, 0);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVibrationThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVibrationThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheVibrationThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        //        void AddRowsToTheVibrationThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType;
        //                if (this.vibrationThresholdDataList != null)
        //                {
        //                    if (this.vibrationThresholdDataList.Count > 23)
        //                    {
        //                        this.vibrationThresholdGridViewTemplate.Rows.Clear();
        //                        for (int i = 0; i < 4; i++)
        //                        {
        //                            entryType = "Vibration " + (i + 1).ToString();
        //                            for (int j = 0; j < 6; j++)
        //                            {
        //                                thresholdData = this.vibrationThresholdDataList[j + (i * 6)];
        //                                this.vibrationThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheVibrationThresholdGrid()\nthis.vibrationThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheVibrationThresholdGrid()\nthis.vibrationThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheVibrationThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToAnalogInThresholdGrid()
        {
            try
            {
                int i, j;
                string entryType;
                this.analogInThresholdGridViewTemplate.Rows.Clear();
                for (i = 0; i < 6; i++)
                {
                    entryType = analogInGridViewEntryText + " " + (i + 1).ToString();
                    for (j = 0; j < 6; j++)
                    {
                        this.analogInThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToAnalogInThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheAnalogInThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 35))
                    {
                        AddDataToThresholdGrid(ref this.analogInThresholdGridViewTemplate, thresholdDataList, 24);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheAnalogInThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheAnalogInThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheAnalogInThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        void AddRowsToThePressureThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType;
        //                if (this.pressureThresholdDataList != null)
        //                {
        //                    if (this.pressureThresholdDataList.Count > 35)
        //                    {
        //                        this.analogInThresholdGridViewTemplate.Rows.Clear();
        //                        for (int i = 0; i < 6; i++)
        //                        {
        //                            entryType = "Pressure " + (i + 1).ToString();
        //                            for (int j = 0; j < 6; j++)
        //                            {
        //                                thresholdData = this.pressureThresholdDataList[j + (i * 6)];
        //                                this.analogInThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToThePressureThresholdGrid()\nthis.pressureThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToThePressureThresholdGrid()\nthis.pressureThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToThePressureThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToCurrentThresholdGrid()
        {
            try
            {
                int i, j;
                string entryType;
                this.currentThresholdGridViewTemplate.Rows.Clear();
                for (i = 0; i < 3; i++)
                {
                    entryType = currentGridViewEntryText + " " + (i + 1).ToString();
                    for (j = 0; j < 6; j++)
                    {
                        this.currentThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToCurrentThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheCurrentThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 36 + 17))
                    {
                        AddDataToThresholdGrid(ref this.currentThresholdGridViewTemplate, thresholdDataList, 24 + 36);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCurrentThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCurrentThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheCurrentThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private void AddRowsToTheCurrentThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType;
        //                if (this.currentThresholdDataList != null)
        //                {
        //                    if (this.currentThresholdDataList.Count > 17)
        //                    {
        //                        this.currentThresholdGridViewTemplate.Rows.Clear();
        //                        for (int i = 0; i < 3; i++)
        //                        {
        //                            entryType = "Current " + (i + 1).ToString();
        //                            for (int j = 0; j < 6; j++)
        //                            {
        //                                thresholdData = this.currentThresholdDataList[j + (i * 6)];
        //                                this.currentThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheCurrentThresholdGrid()\nthis.currentThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheCurrentThresholdGrid()\nthis.currentThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheCurrentThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToVoltageThresholdGrid()
        {
            try
            {
                int i, j;
                string entryType;
                this.voltageThresholdGridViewTemplate.Rows.Clear();
                for (i = 0; i < 3; i++)
                {
                    entryType = voltageGridViewEntryText + " " + (i + 1).ToString();
                    for (j = 0; j < 6; j++)
                    {
                        this.voltageThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToVoltageThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheVoltageThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 36 + 18 + 17))
                    {
                        AddDataToThresholdGrid(ref this.voltageThresholdGridViewTemplate, thresholdDataList, 24 + 36 + 18);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVoltageThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheVoltageThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheVoltageThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private void AddRowsToTheVoltageThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType;
        //                if (this.voltageThresholdDataList != null)
        //                {
        //                    if (this.voltageThresholdDataList.Count > 17)
        //                    {
        //                        this.voltageThresholdGridViewTemplate.Rows.Clear();
        //                        for (int i = 0; i < 3; i++)
        //                        {
        //                            entryType = "Voltage " + (i + 1).ToString();
        //                            for (int j = 0; j < 6; j++)
        //                            {
        //                                thresholdData = this.voltageThresholdDataList[j + (i * 6)];
        //                                this.voltageThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheVoltageThresholdGrid()\nthis.voltageThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheVoltageThresholdGrid()\nthis.voltageThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheVoltageThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToHumidityThresholdGrid()
        {
            try
            {
                int j;
                string entryType;
                this.humidityThresholdGridViewTemplate.Rows.Clear();
                entryType = humidityGridViewEntryText;
                for (j = 0; j < 6; j++)
                {
                    this.humidityThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToHumidityThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheHumidityThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 36 + 18 + 18))
                    {
                        AddDataToThresholdGrid(ref this.humidityThresholdGridViewTemplate, thresholdDataList, 24 + 36 + 18 + 18);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidityThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheHumidityThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheHumidityThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        void AddRowsToTheHumidityThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType = "Humidity";
        //                if (this.humidityThresholdDataList != null)
        //                {
        //                    if (this.humidityThresholdDataList.Count > 5)
        //                    {
        //                        this.humidityThresholdGridViewTemplate.Rows.Clear();
        //                        for (int j = 0; j < 6; j++)
        //                        {
        //                            thresholdData = this.humidityThresholdDataList[j];
        //                            this.humidityThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheHumidityThresholdGrid()\nthis.humidityThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheHumidityThresholdGrid()\nthis.humidityThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheHumidityThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToTemperatureThresholdGrid()
        {
            try
            {
                int i, j;
                string entryType;
                this.temperatureThresholdGridViewTemplate.Rows.Clear();
                for (i = 0; i < 7; i++)
                {
                    entryType = temperatureGridViewEntryText + " " + (i + 1).ToString();
                    for (j = 0; j < 6; j++)
                    {
                        this.temperatureThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToTemperatureThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 36 + 18 + 18 + 1 + 41))
                    {
                        AddDataToThresholdGrid(ref this.temperatureThresholdGridViewTemplate, thresholdDataList, 24 + 36 + 18 + 18 + 1);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        void AddRowsToTheTemperatureThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType;
        //                if (this.temperatureThresholdDataList != null)
        //                {
        //                    if (this.temperatureThresholdDataList.Count > 41)
        //                    {
        //                        this.temperatureThresholdGridViewTemplate.Rows.Clear();
        //                        for (int i = 0; i < 7; i++)
        //                        {
        //                            entryType = "Temperature " + (i + 1).ToString();
        //                            for (int j = 0; j < 6; j++)
        //                            {
        //                                thresholdData = this.temperatureThresholdDataList[j + (i * 6)];
        //                                this.temperatureThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheTemperatureThresholdGrid()\nthis.temperatureThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheTemperatureThresholdGrid()\nthis.temperatureThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheTemperatureThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddEmptyRowsToChassisTemperatureThresholdGrid()
        {
            try
            {
                int j;
                string entryType;
                this.chassisTemperatureThresholdGridViewTemplate.Rows.Clear();
                entryType = chassisTemperatureGridViewEntryText;
                for (j = 0; j < 6; j++)
                {
                    this.chassisTemperatureThresholdGridViewTemplate.Rows.Add(entryType, (j + 1), offThresholdText, exceedingThresholdText, warningThresholdText, ledOnlyThresholdText, "0 s", 0.0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToChassisTemperatureThresholdGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheChassisTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold> thresholdDataList)
        {
            try
            {
                if (thresholdDataList != null)
                {
                    if (thresholdDataList.Count > (24 + 36 + 18 + 18 + 1 + 42))
                    {
                        AddDataToThresholdGrid(ref this.chassisTemperatureThresholdGridViewTemplate, thresholdDataList, 24 + 36 + 18 + 18 + 1 + 42);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheChassisTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheChassisTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheChassisTemperatureThresholdGrid(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        void AddRowsToTheChassisTemperatureThresholdGrid()
        //        {
        //            try
        //            {
        //                ThresholdData thresholdData;
        //                string entryType = "Chassis Temperature";
        //                if (this.chassisTemperatureThresholdDataList != null)
        //                {
        //                    if (this.chassisTemperatureThresholdDataList.Count > 5)
        //                    {
        //                        this.chassisTemperatureThresholdGridViewTemplate.Rows.Clear();
        //                        for (int j = 0; j < 6; j++)
        //                        {
        //                            thresholdData = this.chassisTemperatureThresholdDataList[j];
        //                            this.chassisTemperatureThresholdGridViewTemplate.Rows.Add(entryType, thresholdData.thresholdNumber, thresholdData.status, thresholdData.mode, thresholdData.type, thresholdData.result, thresholdData.time, thresholdData.level);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheChassisTemperatureThresholdGrid()\nthis.chassisTemperatureThresholdDataList had too few elements.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheChassisTemperatureThresholdGrid()\nthis.chassisTemperatureThresholdDataList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheChassisTemperatureThresholdGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void vibrationRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (vibrationRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Vibration);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.vibrationRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void analogInRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (analogInRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Pressure);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.analogInRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void currentRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (currentRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Current);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void voltageRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (voltageRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Voltage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.voltageRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void humidityRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (humidityRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Humidity);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.humidityRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void temperatureRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (temperatureRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Temperature);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.temperatureRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void chassisTemperatureRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (chassisTemperatureRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.ChassisTemperature);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.chassisTemperatureRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetActiveAnalogInputsRadGridView(ParameterType gridType)
        {
            try
            {
                SetAllAnalogInputsRadGridViewsToInvisible();
                switch (gridType)
                {
                    case ParameterType.Vibration:
                        this.vibrationRadGridView.Visible = true;
                        break;

                    case ParameterType.Pressure:
                        this.analogInRadGridView.Visible = true;
                        break;

                    case ParameterType.Current:
                        this.currentRadGridView.Visible = true;
                        break;

                    case ParameterType.Voltage:
                        this.voltageRadGridView.Visible = true;
                        break;

                    case ParameterType.Humidity:
                        this.humidityRadGridView.Visible = true;
                        break;

                    case ParameterType.Temperature:
                        this.temperatureRadGridView.Visible = true;
                        break;

                    case ParameterType.ChassisTemperature:
                        this.chassisTemperatureRadGridView.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetActiveAnalogInputsRadGridView(ParameterType)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllAnalogInputsRadGridViewsToInvisible()
        {
            try
            {
                this.vibrationRadGridView.Visible = false;
                this.analogInRadGridView.Visible = false;
                this.currentRadGridView.Visible = false;
                this.voltageRadGridView.Visible = false;
                this.humidityRadGridView.Visible = false;
                this.temperatureRadGridView.Visible = false;
                this.chassisTemperatureRadGridView.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAllAnalogInputsRadGridViewsToInvisible()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void vibrationRadGridView_ChildViewExpanded(object sender, ChildViewExpandedEventArgs e)
        {
            //if (e.IsExpanded)
            //{
            //    MessageBox.Show("Expanded");
            //}
            //else
            //{
            //    MessageBox.Show("Contracted");
            //}

        }

        #region Get Data From The Grids

        private void WriteAnalogInputDataToCurrentConfiguration()
        {
            if (this.workingConfiguration != null)
            {
                if (this.workingConfiguration.inputChannelConfigObjects != null)
                {
                    if (this.workingConfiguration.inputChannelThresholdConfigObjects != null)
                    {
                        GetAllInputChannelDataFromGrids(ref this.workingConfiguration.inputChannelConfigObjects);
                        GetAllThresholdDataFromGrids(ref this.workingConfiguration.inputChannelThresholdConfigObjects);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.SetAllAnalogInputsRadGridViewsToInvisible()\nthis.workingConfiguration.inputChannelThresholdConfigObjects was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetAllAnalogInputsRadGridViewsToInvisible()\nthis.workingConfiguration.inputChannelConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            else
            {
                string errorMessage = "Error in Main_MonitorConfiguration.SetAllAnalogInputsRadGridViewsToInvisible()\nthis.workingConfiguration was null.";
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        private void GetAllInputChannelDataFromGrids(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            GetVibrationDataFromGrid(ref inputChannelConfigList);
            GetPressureDataFromGrid(ref inputChannelConfigList);
            GetCurrentDataFromGrid(ref inputChannelConfigList);
            GetVoltageDataFromGrid(ref inputChannelConfigList);
            GetHumidityDataFromGrid(ref inputChannelConfigList);
            GetTemperatureDataFromGrid(ref inputChannelConfigList);
            GetChassisTemperatureDataFromGrid(ref inputChannelConfigList);
        }

        private void GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int readStatus;
                int sensorNumber;
                double sensitivity;
                int boundaryFrequency;
                int numberOfPoints;
                double toSave;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 3)
                    {
                        if (this.vibrationRadGridView.Rows.Count > 3)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                readStatus = 0;
                                sensorNumber = 0;
                                sensitivity = 0.0;
                                boundaryFrequency = 0;
                                numberOfPoints = 0;
                                toSave = 0;

                                rowInfo = this.vibrationRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 6)
                                {
                                    if ((bool)(rowInfo.Cells[1].Value) == true)
                                    {
                                        readStatus = 1;
                                    }

                                    if (rowInfo.Cells[2].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                    }

                                    if (rowInfo.Cells[3].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out sensitivity);
                                    }

                                    if (rowInfo.Cells[4].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[4].Value.ToString(), out boundaryFrequency);
                                    }

                                    if (rowInfo.Cells[5].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[5].Value.ToString(), out numberOfPoints);
                                    }

                                    if (rowInfo.Cells[6].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[6].Value.ToString(), out toSave);
                                    }

                                    inputChannelConfigList[i].ReadStatus = readStatus;
                                    inputChannelConfigList[i].SensorNumber = sensorNumber;
                                    inputChannelConfigList[i].Sensitivity = sensitivity;
                                    inputChannelConfigList[i].ReadFrequency = boundaryFrequency;
                                    inputChannelConfigList[i].ReadPoints = numberOfPoints;
                                    inputChannelConfigList[i].SaveDataIfChangedValue = (int)(Math.Round((toSave * 100.0), 0));
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.vibrationRadGridView.Rows[" + i.ToString() + "] did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.vibrationRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetVibrationDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int readStatus;
                int sensorNumber;
                double sensitivity;
                double toSave;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 9)
                    {
                        if (this.analogInRadGridView.Rows.Count > 5)
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                readStatus = 0;
                                sensorNumber = 0;
                                sensitivity = 0.0;
                                toSave = 0;

                                rowInfo = this.analogInRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 4)
                                {
                                    if ((bool)(rowInfo.Cells[1].Value) == true)
                                    {
                                        readStatus = 1;
                                    }

                                    if (rowInfo.Cells[2].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                    }

                                    if (rowInfo.Cells[3].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out sensitivity);
                                    }

                                    if (rowInfo.Cells[4].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[4].Value.ToString(), out toSave);
                                    }

                                    inputChannelConfigList[i + 4].ReadStatus = readStatus;
                                    inputChannelConfigList[i + 4].SensorNumber = sensorNumber;
                                    inputChannelConfigList[i + 4].Sensitivity = sensitivity;
                                    inputChannelConfigList[i + 4].SaveDataIfChangedValue = (int)(Math.Round((toSave * 100.0), 0));
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.pressureRadGridView.Rows[" + i.ToString() + "] did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.pressureRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetPressureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int readStatus;
                int sensorNumber;
                double sensitivity;
                double toSave;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 12)
                    {
                        if (this.currentRadGridView.Rows.Count > 2)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                readStatus = 0;
                                sensorNumber = 0;
                                sensitivity = 0.0;
                                toSave = 0;

                                rowInfo = this.currentRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 4)
                                {
                                    if ((bool)(rowInfo.Cells[1].Value) == true)
                                    {
                                        readStatus = 1;
                                    }

                                    if (rowInfo.Cells[2].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                    }

                                    if (rowInfo.Cells[3].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out sensitivity);
                                    }

                                    if (rowInfo.Cells[4].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[4].Value.ToString(), out toSave);
                                    }

                                    inputChannelConfigList[i + 10].ReadStatus = readStatus;
                                    inputChannelConfigList[i + 10].SensorNumber = sensorNumber;
                                    inputChannelConfigList[i + 10].Sensitivity = sensitivity;
                                    inputChannelConfigList[i + 10].SaveDataIfChangedValue = (int)(Math.Round((toSave * 10.0), 0));
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.currentRadGridView.Rows[" + i.ToString() + "] did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.currentRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetCurrentDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }



        private void GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int readStatus;
                int sensorNumber;
                double sensitivity;
                double toSave;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 15)
                    {
                        if (this.voltageRadGridView.Rows.Count > 2)
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                readStatus = 0;
                                sensorNumber = 0;
                                sensitivity = 0.0;
                                toSave = 0;

                                rowInfo = this.voltageRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 4)
                                {
                                    if ((bool)(rowInfo.Cells[1].Value) == true)
                                    {
                                        readStatus = 1;
                                    }

                                    if (rowInfo.Cells[2].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                    }

                                    if (rowInfo.Cells[3].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out sensitivity);
                                    }

                                    if (rowInfo.Cells[4].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[4].Value.ToString(), out toSave);
                                    }

                                    inputChannelConfigList[i + 13].ReadStatus = readStatus;
                                    inputChannelConfigList[i + 13].SensorNumber = sensorNumber;
                                    inputChannelConfigList[i + 13].Sensitivity = sensitivity;
                                    inputChannelConfigList[i + 13].SaveDataIfChangedValue = (int)(Math.Round((toSave * 10.0), 0));
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.voltageRadGridView.Rows[" + i.ToString() + "] did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.voltageRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetVoltageDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int readStatus = 0;
                int sensorNumber = 0;
                double toSave = 0;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 16)
                    {
                        if (this.humidityRadGridView.Rows.Count > 0)
                        {
                            rowInfo = this.humidityRadGridView.Rows[0];
                            if (rowInfo.Cells.Count > 3)
                            {
                                if ((bool)(rowInfo.Cells[1].Value) == true)
                                {
                                    readStatus = 1;
                                }

                                if (rowInfo.Cells[2].Value != null)
                                {
                                    Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                }

                                if (rowInfo.Cells[3].Value != null)
                                {
                                    Double.TryParse(rowInfo.Cells[3].Value.ToString(), out toSave);
                                }

                                inputChannelConfigList[16].ReadStatus = readStatus;

                                inputChannelConfigList[16].SensorNumber = sensorNumber;
                                inputChannelConfigList[16].SaveDataIfChangedValue = (int)(Math.Round((toSave * 10.0), 0));
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.humidityRadGridView.Rows[0] did not have enough cells.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.humidityRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetHumidityDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                GridViewRowInfo rowInfo;

                int readStatus;
                int sensorNumber;
                double toSave;
                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 23)
                    {
                        if (this.temperatureRadGridView.Rows.Count > 6)
                        {
                            for (int i = 0; i < 7; i++)
                            {
                                readStatus = 0;
                                sensorNumber = 0;
                                toSave = 0;

                                rowInfo = this.temperatureRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 3)
                                {
                                    if ((bool)(rowInfo.Cells[1].Value) == true)
                                    {
                                        readStatus = 1;
                                    }

                                    if (rowInfo.Cells[2].Value != null)
                                    {
                                        Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out sensorNumber);
                                    }

                                    if (rowInfo.Cells[3].Value != null)
                                    {
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out toSave);
                                    }

                                    inputChannelConfigList[i + 17].ReadStatus = readStatus;


                                    inputChannelConfigList[i + 17].SensorNumber = sensorNumber;
                                    inputChannelConfigList[i + 17].SaveDataIfChangedValue = (int)(Math.Round((toSave * 10.0), 0));
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.temperatureRadGridView.Rows[" + i.ToString() + "] did not have enough cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.temperatureRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel> inputChannelConfigList)
        {
            try
            {
                //GridViewInfo info = new GridViewInfo(this.chassisTemperatureRadGridView.MasterTemplate);
                GridViewRowInfo rowInfo;

                int readStatus = 0;
                int threshold = 0;
                double toSave = 0;

                if (inputChannelConfigList != null)
                {
                    if (inputChannelConfigList.Count > 24)
                    {
                        if (this.chassisTemperatureRadGridView.Rows.Count > 0)
                        {
                            rowInfo = this.chassisTemperatureRadGridView.Rows[0];
                            if (rowInfo.Cells.Count > 3)
                            {
                                if ((bool)(rowInfo.Cells[1].Value) == true)
                                {
                                    readStatus = 1;
                                }

                                if (rowInfo.Cells[2].Value != null)
                                {
                                    Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out threshold);
                                }

                                if (rowInfo.Cells[3].Value != null)
                                {
                                    Double.TryParse(rowInfo.Cells[3].Value.ToString(), out toSave);
                                }

                                inputChannelConfigList[24].ReadStatus = readStatus;
                                inputChannelConfigList[24].SaveDataIfChangedValue = (int)(Math.Round((toSave * 10.0), 0));
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.chassisTemperatureRadGridView.Rows[0] did not have enough cells.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nthis.chassisTemperatureRadGridView did not have enough rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> did not have enough items.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetChassisTemperatureDataFromGrid(ref List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetAllThresholdDataFromGrids(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            GetVibrationThresholdDataFromGrid(ref inputChannelThresholdList);
            GetPressureThresholdDataFromGrid(ref inputChannelThresholdList);
            GetCurrentThresholdDataFromGrid(ref inputChannelThresholdList);
            GetVoltageThresholdDataFromGrid(ref inputChannelThresholdList);
            GetHumidityThresholdDataFromGrid(ref inputChannelThresholdList);
            GetTemperatureThresholdDataFromGrid(ref inputChannelThresholdList);
            GetChassisTemperatureThresholdDataFromGrid(ref inputChannelThresholdList);
        }

        private void GetVibrationThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;
                if (this.vibrationThresholdGridViewTemplate.Rows.Count == 24)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            internalIndex = j + (i * 6);
                            rowInfo = this.vibrationThresholdGridViewTemplate.Rows[internalIndex];
                            AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex, ref inputChannelThresholdList);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetVibrationThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.vibrationThresholdGridViewTemplate did not have 24 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetVibrationThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetPressureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;
                if (this.analogInThresholdGridViewTemplate.Rows.Count == 36)
                {
                    for (int i = 0; i < 6; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            internalIndex = j + (i * 6);
                            rowInfo = this.analogInThresholdGridViewTemplate.Rows[internalIndex];
                            // we need to add 24 to the value to account for the threshold entries for vibration
                            AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24, ref inputChannelThresholdList);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetPressureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.analogInThresholdGridViewTemplate did not have 36 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetPressureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetCurrentThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;
                if (this.currentThresholdGridViewTemplate.Rows.Count == 18)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            internalIndex = j + (i * 6);
                            rowInfo = this.currentThresholdGridViewTemplate.Rows[internalIndex];
                            // we need to add 24 + 36 to the value to account for the threshold entries for vibration and pressure
                            AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24 + 36, ref inputChannelThresholdList);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetCurrentThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.currentThresholdGridViewTemplate did not have 18 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetCurrentThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetVoltageThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;
                if (this.voltageThresholdGridViewTemplate.Rows.Count == 18)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            internalIndex = j + (i * 6);
                            rowInfo = this.voltageThresholdGridViewTemplate.Rows[internalIndex];
                            // we need to add 24 + 36 + 18 to the value to account for the threshold entries for vibration, pressure, and current
                            AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24 + 36 + 18, ref inputChannelThresholdList);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.voltageThresholdGridViewTemplate did not have 18 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetVoltageThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetHumidityThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;
                if (this.humidityThresholdGridViewTemplate.Rows.Count == 6)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        internalIndex = j;
                        rowInfo = this.humidityThresholdGridViewTemplate.Rows[internalIndex];
                        // we need to add 24 + 36 + 18 + 18 to the value to account for the threshold entries for vibration, pressure, current, and voltage
                        AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24 + 36 + 18 + 18, ref inputChannelThresholdList);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetVoltageThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.voltageThresholdGridViewTemplate did not have 6 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetHumidityThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;

                if (this.temperatureThresholdGridViewTemplate.Rows.Count == 42)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        for (int j = 0; j < 6; j++)
                        {
                            internalIndex = j + (i * 6);
                            rowInfo = this.temperatureThresholdGridViewTemplate.Rows[internalIndex];
                            // we need to add 24 + 36 + 18 + 18 + 6 to the value to account for the threshold entries for vibration, pressure, current, voltage, and humidity
                            AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24 + 36 + 18 + 18 + 6, ref inputChannelThresholdList);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.temperatureThresholdGridViewTemplate did not have 42 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetChassisTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                int internalIndex;

                if (this.chassisTemperatureThresholdGridViewTemplate.Rows.Count == 6)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        internalIndex = j;
                        rowInfo = this.chassisTemperatureThresholdGridViewTemplate.Rows[internalIndex];
                        // we need to add 24 + 36 + 18 + 18 to the value to account for the threshold entries for vibration, pressure, current, voltage, humidity, and temperature
                        AssignThresholdValuesToConfigurationObject(rowInfo, internalIndex + 24 + 36 + 18 + 18 + 6 + 42, ref inputChannelThresholdList);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetChassisTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nthis.chassisTemperatureThresholdGridViewTemplate did not have 6 rows.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetChassisTemperatureThresholdDataFromGrid(ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        ///  Placeholder for future error checking.  Currently returns false .
        /// </summary>
        /// <returns></returns>
        private bool ErrorIsPresentInAnAnalogInputsTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.vibrationRadGridView, ref this.vibrationRadRadioButton);
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.analogInRadGridView, ref this.analogInRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.currentRadGridView, ref this.currentRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.voltageRadGridView, ref this.voltageRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.humidityRadGridView, ref this.humidityRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.temperatureRadGridView, ref this.temperatureRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInAnalogInputRadGridView(ref this.chassisTemperatureRadGridView, ref this.chassisTemperatureRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.analogInThresholdGridViewTemplate, ref this.analogInRadGridView, ref this.analogInRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.currentThresholdGridViewTemplate, ref this.currentRadGridView, ref this.currentRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.voltageThresholdGridViewTemplate, ref this.voltageRadGridView, ref this.voltageRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.humidityThresholdGridViewTemplate, ref this.humidityRadGridView, ref this.humidityRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.temperatureThresholdGridViewTemplate, ref this.temperatureRadGridView, ref this.temperatureRadRadioButton);
                }
                if (!errorIsPresent)
                {
                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.chassisTemperatureThresholdGridViewTemplate, ref this.chassisTemperatureRadGridView, ref this.chassisTemperatureRadRadioButton);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ErrorIsPresentInAnAnalogInputsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        //        private bool ErrorIsPresentInAThresholdTabObject()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                errorIsPresent = MissingValueInThresholdRadGridView(ref this.vibrationThresholdGridViewTemplate, ref this.vibrationRadGridView, ref this.vibrationRadRadioButton);
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.analogInThresholdGridViewTemplate, ref this.pressureRadGridView, ref this.pressureRadRadioButton);
        //                }
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.currentThresholdGridViewTemplate, ref this.currentRadGridView, ref this.currentRadRadioButton);
        //                }
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.voltageThresholdGridViewTemplate, ref this.voltageRadGridView, ref this.voltageRadRadioButton);
        //                }
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.humidityThresholdGridViewTemplate, ref this.humidityRadGridView, ref this.humidityRadRadioButton);
        //                }
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.temperatureThresholdGridViewTemplate, ref this.temperatureRadGridView, ref this.temperatureRadRadioButton);
        //                }
        //                if (!errorIsPresent)
        //                {
        //                    errorIsPresent = MissingValueInThresholdRadGridView(ref this.chassisTemperatureThresholdGridViewTemplate, ref this.chassisTemperatureRadGridView, ref this.chassisTemperatureRadRadioButton);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ErrorIsPresentInAnAnalogInputsTabObject()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //this.vibrationRadGridView.Location = startPoint;
        //      this.vibrationRadGridView.Size = new Size(width, 360);

        //      this.pressureRadGridView.Location = startPoint;
        //      this.pressureRadGridView.Size = new Size(width, 395);


        private bool MissingValueInAnalogInputRadGridView(ref RadGridView analogInputRadGridView, ref RadRadioButton analogInputRadRadioButton)
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount;
                int i, j;

                if (analogInputRadGridView != null)
                {
                    if (analogInputRadRadioButton != null)
                    {
                        rowCount = analogInputRadGridView.RowCount;

                        for (i = 0; i < rowCount; i++)
                        {
                            rowInfo = analogInputRadGridView.Rows[i];
                            cellCount = rowInfo.Cells.Count;
                            for (j = 0; j < cellCount; j++)
                            {
                                if (rowInfo.Cells[j].Value == null)
                                {
                                    SelectAnalogInputsTab();
                                    analogInputRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                    rowInfo.Cells[j].IsSelected = true;
                                    RadMessageBox.SetThemeName("office2007BlackTheme");
                                    RadMessageBox.Show(this, emptyCellErrorMessage);
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationRadGridView()\nInput RadRadioButton was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationRadGridView()\nInput RadGridView was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInVibrationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }


        //        private bool MissingValueInVibrationRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.vibrationRadGridView.RowCount;
        //                int i, j;
        //                for (i = 0; i < rowCount; i++)
        //                {
        //                    rowInfo = this.vibrationRadGridView.Rows[i];
        //                    cellCount = rowInfo.Cells.Count;
        //                    for (j = 0; j < cellCount; j++)
        //                    {
        //                        if (rowInfo.Cells[j].Value == null)
        //                        {
        //                            SelectAnalogInputsTab();
        //                            vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                            rowInfo.Cells[j].IsSelected = true;
        //                            RadMessageBox.Show(this, emptyCellErrorMessage);
        //                            return errorIsPresent;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInVibrationRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInPressureRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.pressureRadGridView.RowCount;
        //                int i, j;
        //                for (i = 0; i < rowCount; i++)
        //                {
        //                    rowInfo = this.pressureRadGridView.Rows[i];
        //                    cellCount = rowInfo.Cells.Count;
        //                    for (j = 0; j < cellCount; j++)
        //                    {
        //                        if (rowInfo.Cells[j].Value == null)
        //                        {
        //                            SelectAnalogInputsTab();
        //                            pressureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                            rowInfo.Cells[j].IsSelected = true;
        //                            RadMessageBox.Show(this, emptyCellErrorMessage);
        //                            return errorIsPresent;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInPressureRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInCurrentRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.currentRadGridView.RowCount;
        //                int i, j;
        //                for (i = 0; i < rowCount; i++)
        //                {
        //                    rowInfo = this.currentRadGridView.Rows[i];
        //                    cellCount = rowInfo.Cells.Count;
        //                    for (j = 0; j < cellCount; j++)
        //                    {
        //                        if (rowInfo.Cells[j].Value == null)
        //                        {
        //                            SelectAnalogInputsTab();
        //                            currentRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                            rowInfo.Cells[j].IsSelected = true;
        //                            RadMessageBox.Show(this, emptyCellErrorMessage);
        //                            return errorIsPresent;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInCurrentRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInVoltageRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.voltageRadGridView.RowCount;
        //                int i, j;
        //                for (i = 0; i < rowCount; i++)
        //                {
        //                    rowInfo = this.voltageRadGridView.Rows[i];
        //                    cellCount = rowInfo.Cells.Count;
        //                    for (j = 0; j < cellCount; j++)
        //                    {
        //                        if (rowInfo.Cells[j].Value == null)
        //                        {
        //                            SelectAnalogInputsTab();
        //                            voltageRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                            rowInfo.Cells[j].IsSelected = true;
        //                            RadMessageBox.Show(this, emptyCellErrorMessage);
        //                            return errorIsPresent;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInVoltageRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInHumidityRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.humidityRadGridView.RowCount;
        //                int j;

        //                rowInfo = this.humidityRadGridView.Rows[0];
        //                cellCount = rowInfo.Cells.Count;
        //                for (j = 0; j < cellCount; j++)
        //                {
        //                    if (rowInfo.Cells[j].Value == null)
        //                    {
        //                        SelectAnalogInputsTab();
        //                        humidityRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        rowInfo.Cells[j].IsSelected = true;
        //                        RadMessageBox.Show(this, emptyCellErrorMessage);
        //                        return errorIsPresent;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInChassisTemperatureRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInTemperatureRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.temperatureRadGridView.RowCount;
        //                int i, j;
        //                for (i = 0; i < rowCount; i++)
        //                {
        //                    rowInfo = this.temperatureRadGridView.Rows[i];
        //                    cellCount = rowInfo.Cells.Count;
        //                    for (j = 0; j < cellCount; j++)
        //                    {
        //                        if (rowInfo.Cells[j].Value == null)
        //                        {
        //                            SelectAnalogInputsTab();
        //                            temperatureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                            rowInfo.Cells[j].IsSelected = true;
        //                            RadMessageBox.Show(this, emptyCellErrorMessage);
        //                            return errorIsPresent;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInTemperatureRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

        //        private bool MissingValueInChassisTemperatureRadGridView()
        //        {
        //            bool errorIsPresent = false;
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int cellCount;
        //                int rowCount = this.chassisTemperatureRadGridView.RowCount;
        //                int j;

        //                rowInfo = this.chassisTemperatureRadGridView.Rows[0];
        //                cellCount = rowInfo.Cells.Count;
        //                for (j = 0; j < cellCount; j++)
        //                {
        //                    if (rowInfo.Cells[j].Value == null)
        //                    {
        //                        SelectAnalogInputsTab();
        //                        chassisTemperatureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        rowInfo.Cells[j].IsSelected = true;
        //                        RadMessageBox.Show(this, emptyCellErrorMessage);
        //                        return errorIsPresent;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInChassisTemperatureRadGridView()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorIsPresent;
        //        }

     


        private bool MissingValueInThresholdRadGridView(ref GridViewTemplate thresholdGridViewTemplate, ref RadGridView analogInputRadGridView, ref RadRadioButton analogInputRadRadioButton)
        {
            bool valueIsMissing = false;
            try
            {
                GridViewRowInfo rowInfo;
                int i, j;
                int rowCount;
                int uniqueParentCount;
                if (thresholdGridViewTemplate != null)
                {
                    if (analogInputRadGridView != null)
                    {
                        if (analogInputRadRadioButton != null)
                        {
                            rowCount = thresholdGridViewTemplate.Rows.Count;
                            if ((rowCount % 6) == 0)
                            {
                                uniqueParentCount = rowCount / 6;
                                for (i = 0; i < uniqueParentCount; i++)
                                {
                                    for (j = 0; j < 6; j++)
                                    {
                                        rowInfo = thresholdGridViewTemplate.Rows[(i * 6) + j];
                                        if (rowInfo.Cells.Count > 7)
                                        {
                                            if (rowInfo.Cells[7].Value == null)
                                            {
                                                SelectAnalogInputsTab();
                                                analogInputRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                                analogInputRadGridView.Rows[i].IsExpanded = true;
                                                rowInfo.Cells[7].IsSelected = true;
                                                RadMessageBox.SetThemeName("office2007BlackTheme");
                                                RadMessageBox.Show(this, emptyCellErrorMessage);
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationThresholdRadGridView(ref GridViewTemplate, ref RadGridView, ref RadRadioButton)\nthis.vibrationThresholdGridViewTemplate did not have a multiple of 6 rows, which is incorrect.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationThresholdRadGridView(ref GridViewTemplate, ref RadGridView, ref RadRadioButton)\nInput RadRadioButton was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationThresholdRadGridView(ref GridViewTemplate, ref RadGridView, ref RadRadioButton)\nInput RadRadioButton was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.MissingValueInVibrationThresholdRadGridView(ref GridViewTemplate, ref RadGridView, ref RadRadioButton)\nInput GridViewTemplate was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInVibrationThresholdRadGridView(ref GridViewTemplate, ref RadGridView, ref RadRadioButton)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return valueIsMissing;
        }

        private void AssignThresholdValuesToConfigurationObject(GridViewRowInfo rowInfo, int indexInConfigurationThresholdList, ref List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdList)
        {
            try
            {
                // Note that "Status" was used in the IHM interface, but "Enable" is what's used in the firmware to describe the same thing.  Also, the IHM interface
                // used "Type" while the firmware uses ThresholdStatus.  Frankly, the whole thing is quite confusing.
                string status;
                string mode;
                string type;
                string result;
                string time;
                double level = 0;

                int statusAsInteger = 0;
                int modeAsInteger = 0;
                int typeAsInteger = 0;
                int resultAsInteger = 0;
                int timeAsInteger = 0;

                if (rowInfo.Cells.Count > 7)
                {
                    if (inputChannelThresholdList.Count > indexInConfigurationThresholdList)
                    {
                        status = rowInfo.Cells[2].Value.ToString();
                        mode = rowInfo.Cells[3].Value.ToString();
                        type = rowInfo.Cells[4].Value.ToString();
                        result = rowInfo.Cells[5].Value.ToString();
                        time = rowInfo.Cells[6].Value.ToString();

                        if (rowInfo.Cells[7].Value != null)
                        {
                            Double.TryParse(rowInfo.Cells[7].Value.ToString(), out level);
                        }

                        if (status.CompareTo(onThresholdText) == 0)
                        {
                            statusAsInteger = 1;
                        }
                        if (mode.CompareTo(diminishingThresholdText) == 0)
                        {
                            modeAsInteger = 1;
                        }
                        if (type.CompareTo(alarmThresholdText) == 0)
                        {
                            typeAsInteger = 1;
                        }
                        if (result.CompareTo(ledAndRelayThresholdText) == 0)
                        {
                            resultAsInteger = 1;
                        }
                        timeAsInteger = GetThresholdTimeAsIntegerFromString(time);

                        inputChannelThresholdList[indexInConfigurationThresholdList].ThresholdEnable = statusAsInteger;
                        inputChannelThresholdList[indexInConfigurationThresholdList].WorkingBy = modeAsInteger;
                        inputChannelThresholdList[indexInConfigurationThresholdList].ThresholdStatus = typeAsInteger;
                        inputChannelThresholdList[indexInConfigurationThresholdList].RelayMode = resultAsInteger;
                        inputChannelThresholdList[indexInConfigurationThresholdList].RelayStatusOnDelay = timeAsInteger;
                        inputChannelThresholdList[indexInConfigurationThresholdList].ThresholdValue = level;

                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AssignThresholdValuesToConfigurationObject(GridViewRowInfo, int, ref List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AssignThresholdValuesToConfigurationObject(GridViewRowInfo, int, ref List<Main_ConfigComponent_InputChannelThreshold>)\nInput GridViewRowInfo had too few cells.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AssignThresholdValuesToConfigurationObject(GridViewRowInfo, int, ref List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #endregion // Analog Inputs Tab

    }

    enum ParameterType
    {
        Vibration,
        Pressure,
        Current,
        Voltage,
        Humidity,
        Temperature,
        ChassisTemperature
    }

    public class ThresholdData
    {
        public int thresholdNumber;
        public string status;
        public string mode;
        public string type;
        public string result;
        public string time;
        public double level;
    }

    public class VibrationData
    {
        public bool enabled;
        public int vibrationNumber;
        public int sensorNumber;
        public double sensitivity;
        public int boundaryFrequency;
        public int numberOfPoints;
        public double toSave;
    }

    public class PressureData
    {
        public bool enabled;
        public int pressureNumber;
        public int sensorNumber;
        public double sensitivity;
        public double toSave;
    }

    public class CurrentData
    {
        public bool enabled;
        public int currentNumber;
        public int sensorNumber;
        public double sensitivity;
        public double toSave;
    }

    public class VoltageData
    {
        public bool enabled;
        public int voltageNumber;
        public int sensorNumber;
        public double sensitivity;
        public double toSave;
    }

    public class HumidityData
    {
        public bool enabled;
        public int humidityNumber;
        public int sensorNumber;
        public double toSave;
    }

    public class TemperatureData
    {
        public bool enabled;
        public int temperatureNumber;
        public int sensorNumber;
        public double toSave;
    }

    public class ChassisTemperatureData
    {
        public bool enabled;
        public int chassisTemperatureNumber;
        public int threshold;
        public double toSave;
    }
}
