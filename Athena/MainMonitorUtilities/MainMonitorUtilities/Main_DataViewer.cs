﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;   
using Telerik.WinControls;
using Telerik.WinControls.UI;
using ChartDirector;
using System.Linq;
using System.Data.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using GeneralUtilities;
using ConfigurationObjects;
using DataObjects;
using DatabaseInterface;
using PasswordManagement;
using Dynamics;
using FormatConversion;
using MonitorUtilities;

namespace MainMonitorUtilities
{
    public partial class Main_DataViewer : Telerik.WinControls.UI.RadForm
    {
        private string analog1RadCheckBoxEnabledKeyText = "analog1RadCheckBoxEnabledKey";
        private string analog2RadCheckBoxEnabledKeyText = "analog2RadCheckBoxEnabledKey";
        private string analog3RadCheckBoxEnabledKeyText = "analog3RadCheckBoxEnabledKey";
        private string analog4RadCheckBoxEnabledKeyText = "analog4RadCheckBoxEnabledKey";
        private string analog5RadCheckBoxEnabledKeyText = "analog5RadCheckBoxEnabledKey";
        private string analog6RadCheckBoxEnabledKeyText = "analog6RadCheckBoxEnabledKey";
        private string vibration1RadCheckBoxEnabledKeyText = "vibration1RadCheckBoxEnabledKey";
        private string vibration2RadCheckBoxEnabledKeyText = "vibration2RadCheckBoxEnabledKey";
        private string vibration3RadCheckBoxEnabledKeyText = "vibration3RadCheckBoxEnabledKey";
        private string vibration4RadCheckBoxEnabledKeyText = "vibration4RadCheckBoxEnabledKey";

        private string trueText = "true";
        private string falseText = "false";

        private Dictionary<string, string> miscellaneousProperties;

        private static string htmlPrefix = "<html>";
        private static string htmlPostfix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string noDataForSelectedDataAgeText = "No data available for the selected Data Age";
        // private static string copyGraphText = "Copy Graph";
        private static string addText = "add";
        private static string multiplyText = "multiply";

        //  private static string preferencesMightBeIncompatableText = "The database preferences might be incompatable with this version.  Delete them?";

        private static string failedToReadAnyDataToSaveToCsvFile = "No data was available to save to the csv file";

        private static string idDynamicsGridHeaderText = "ID";
        private static string dataRootIdDynamicsGridHeaderText = "Data Root ID";
        private static string readingDateDynamicsGridHeaderText = "Reading Date Time";
        private static string stateDynamicsGridHeaderText = "State";
        private static string properWorkingOrderDynamicsGridHeaderText = "Proper Working Order";

        private static string humidityRadCheckBoxText = "Humidity (%)";
        private static string moistureContentRadCheckBoxText = "Moisture (kg/M3)";
        private static string temp1RadCheckBoxText = "Temp 1 (єC)";
        private static string temp2RadCheckBoxText = "Temp 2 (єC)";
        private static string temp3RadCheckBoxText = "Temp 3 (єC)";
        private static string temp4RadCheckBoxText = "Temp 4 (єC)";
        private static string loadCurrent1RadCheckBoxText = "Load Current 1 (A)";
        private static string loadCurrent2RadCheckBoxText = "Load Current 2 (A)";
        private static string loadCurrent3RadCheckBoxText = "Load Current 3 (A)";
        private static string voltage1RadCheckBoxText = "Voltage 1 (kV)";
        private static string voltage2RadCheckBoxText = "Voltage 2 (kV)";
        private static string voltage3RadCheckBoxText = "Voltage 3 (kV)";
        private static string analog1RadCheckBoxText = "Analog 1";
        private static string analog2RadCheckBoxText = "Analog 2";
        private static string analog3RadCheckBoxText = "Analog 3";
        private static string analog4RadCheckBoxText = "Analog 4";
        private static string analog5RadCheckBoxText = "Analog 5";
        private static string analog6RadCheckBoxText = "Analog 6";
        private static string vibration1RadCheckBoxText = "Vibration 1";
        private static string vibration2RadCheckBoxText = "Vibration 2";
        private static string vibration3RadCheckBoxText = "Vibration 3";
        private static string vibration4RadCheckBoxText = "Vibration 4";

        private static string mainDataViewerInterfaceTitleText = "Main Monitor Data Viewer";

        // labels that appear in many tabs
        private static string trendRadPageViewPageText = "Trend";

        private static string pointerRadioButtonText = "Pointer";
        private static string zoomInRadioButtonText = "Zoom In";
        private static string zoomOutRadioButtonText = "Zoom Out";
        private static string dataAgeRadGroupBoxText = "Data Age";
        private static string threeMonthsRadRadioButtonText = "3 Months";
        private static string sixMonthsRadRadioButtonText = "6 Months";
        private static string oneYearRadRadioButtonText = "1 Year";
        private static string allDataRadRadioButtonText = "All";
        private static string graphMethodRadGroupBoxText = "Graph Method";
        private static string automaticDrawRadRadioButtonText = "Automatic Draw";
        private static string manualDrawRadRadioButtonText = "Manual Draw";
        private static string drawGraphsRadButtonText = "Draw Graph";

        private static string trendDisplayOptionsRadGroupBoxText = "Display Options";
        private static string showTrendLineRadCheckBoxText = "Show Trend Line";
        private static string linearTrendRadRadioButtonText = "Linear";
        private static string exponentialTrendRadRadioButtonText = "Exponential";
        private static string normalizeDataRadCheckBoxText = "Normalize Data";
        private static string movingAverageRadCheckBoxText = "Show Moving Avg";
        private static string dataItemsRadLabelText = "data items";
        private static string dynamicsRadGroupBoxText = "Dynamics";
        private static string openDynamicsEditorContextMenuText = "Open dynamics editor";
        private static string copyGraphTrendContextMenuText = "Copy Graph";

        private static string dataGridRadPageViewPageText = "Data Grid";
        //private static string expandAllRadButtonText = "Expand All";
        //private static string hideAllRadButtonText = "Hide All";
        private static string gridDataEditRadButtonText = "Edit Data";
        private static string gridDataSaveChangesRadButtonText = "Save Changes";
        private static string gridDataCancelChangesRadButtonText = "Cancel Changes";
        private static string gridDataDeleteSelectedRadButtonText = "Delete Selected Entries";
        private static string gridDataSaveDataToCsvRadButtonText = "Save Data to CSV";

        private static string highVoltageTemperatureRadLabelText = "HV Temp ºC";
        private static string whs1RadCheckBoxLabelText = "WHS 1 ºC";
        private static string whs2RadCheckBoxLabelText = "WHS 2 ºC";
        private static string whs3RadCheckBoxLabelText = "WHS 3 ºC";


        private static string topOilTempRadLabelText = "Top Oil Temp ºC";
        private static string ageingFactor1RadCheckBoxLabelText = "Age Factor 1";
        private static string ageingFactor2RadCheckBoxLabelText = "Age Factor 2";
        private static string ageingFactor3RadCheckBoxLabelText = "Age Factor 3";

        private static string commonVoltageTemperatureRadLabelText = "CV Temp (єC)";
        private static string accumAge1RadLabelText = "Accum Age 1 (Yrs)";
        private static string accumAge2RadLabelText = "Accum Age 2 (Yrs)";
        private static string accumAge3RadLabelText = "Accum Age 3 (Yrs)";

        private static string fan1_StartsRadLabelText = "Group 1 Starts";
        private static string fan2_StartsRadLabelText = "Group 2 Starts";
        private static string fan1_RunTimeRadLabelText = "Group 1 Run  Hrs";
        private static string fan2_RunTimeRadLabelText = "Group 2 Run Hrs";
  
        //private static string agingfactorRadLabelText = "Aging Factor";
        //private static string accumulatedAgingRadLabelText = "Accumulated Aging";

        //private static string currentConfigurationNotSavedInDatabaseText = "No current configuration has been saved in the database.\nSome features will be missing, including display of winding hot spot data.";
        //private static string currentWindingHotSpotConfigurationNotSavedInDatabaseText = "No current winding hot spot configuration has been saved in the database.\nNo winding hot spot data will be displayed.";

        private bool transformerTypeIsStandard;
        private bool transformerTypeIsAuto;
        private bool transformerConfigurationIsThreeSinglePhase;
        private bool transformerConfigurationIsOneThreePhase;

        private bool resettingWindingHotSpotCheckboxToggleState = false;

        private bool windingHotSpotDataIsExpected = false;

        private string dbConnectionString;
        // private string fullPathToXmlLanguageFile;
        private string pathToLanguageFile;

        /// <summary>
        /// The total number of data readings
        /// </summary>
        int totalDynamicsDataReadings;

        int totalWindingHotSpotDataReadings;

        bool hasFinishedInitialization = false;

        MainMonitorData monitorData = new MainMonitorData();
        WindingHotSpotData windingHotSpotData = new WindingHotSpotData();

        ProgramBrand programBrand;
        ProgramType programType;

        Guid monitorID;

        public Main_DataViewer(ProgramBrand inputProgramBrand, ProgramType inputProgramType, Guid inputMonitorID, string inputPathToXmlLanguageFile, string inputDbConnectionString)
        {
            InitializeComponent();

            this.hasFinishedInitialization = false;

            this.programBrand = inputProgramBrand;
            this.programType = inputProgramType;

            monitorID = inputMonitorID;
            dbConnectionString = inputDbConnectionString;
            pathToLanguageFile = inputPathToXmlLanguageFile;

            trendWinChartViewer.ViewPortLeft = 0;
            trendWinChartViewer.ViewPortWidth = 1;
            trendWinChartViewer.ViewPortTop = 0;
            trendWinChartViewer.ViewPortHeight = 1;

            // Horizontal zooming and scrolling only for trend
            trendWinChartViewer.ZoomDirection = WinChartDirection.Horizontal;
            trendWinChartViewer.ScrollDirection = WinChartDirection.Horizontal;

            this.trendPointerRadioButton.Checked = true;

            AssignStringValuesToInterfaceObjects();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void Main_DataViewer_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.UseWaitCursor = true;

                this.trendPointerRadioButton.Checked = false;
                this.trendPointerRadioButton.Checked = true;

                //GetAllGridViewColumnWidths();
                /// You want to load preferences here, before initialization is finished
                PreferencesLoad();

                // setup context menus
                SetupTrendContextMenu();

                this.windingHotSpotRadGroupBox.Visible = false;
                SetWindingHotSpotDataDisplayState();

                // SelectAllRadDropDownListObjects();

                //InitializeAllRadGridViews();
                InitializeDataReadingsGridViewObject();
                InitializeAllGridViewColumnHeaderText();
                InitializeAllGridViewColumnWidths();

                //EstablishAllParentChildGridRelations();

                this.hasFinishedInitialization = true;

                ReloadData();

                using (MonitorInterfaceDB loadDB = new MonitorInterfaceDB(dbConnectionString))
                {
                    SetFormTitleToReflectDataSource(loadDB);
                }

                this.trendChartNeedsUpdating = true;

                DrawAllGraphs();

                // DisableEditingInAllRadDropDownLists();

                // this.trendWinChartViewer.updateViewPort(true, true);
                // set tool tips
                SetToolTips();

                //ChannelSelectRadGroupBoxAssignContextMenuItems();
                //DynamicsRadGroupBoxAssignContextMenuItems();
                GridDataDisableEditing();

                //InitializeChannelSensitivitiesList();

                DynamicsRadGroupBoxAssignContextMenuItems();

                Application.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.Main_DataViewer_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            finally
            {
                Application.UseWaitCursor = false;
            }
        }

        private void Main_DataViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                GetAllGridViewColumnWidths();
                PreferencesSave();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.Main_DataViewer_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetWindingHotSpotDataDisplayState()
        {
            try
            {
                Main_Configuration standardConfiguration = null;
                Main_WHSConfiguration whsConfiguration = null;

                this.windingHotSpotDataIsExpected = false;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    standardConfiguration = Main_MonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(this.monitorID, localDB);
                    whsConfiguration = Main_WHSMonitorConfiguration.LoadCurrentDeviceConfigurationFromDatabase(this.monitorID, localDB);
                }

                if (standardConfiguration != null)
                {
                    /// We can only have a winding hot spot configuration when the firmware is 2.0 or greater
                    if (standardConfiguration.generalConfigObject.FirmwareVersion > 1.99)
                    {
                        this.windingHotSpotDataIsExpected = true;

                        windingHotSpotRadGroupBox.Visible = true;
                        WHS1RadCheckBox.Visible = true;
                        WHS2RadCheckBox.Visible = true;
                        WHS3RadCheckBox.Visible = true;

                        WHS1RadCheckBox.Text = whs1RadCheckBoxLabelText;
                        WHS2RadCheckBox.Text = whs2RadCheckBoxLabelText;
                        WHS3RadCheckBox.Text = whs3RadCheckBoxLabelText;

                        agingFac1RadCheckBox.Visible = true;
                        AgingFac2RadCheckBox.Visible = true;
                        AgingFac3RadCheckBox.Visible = true;

                        agingFac1RadCheckBox.Text = ageingFactor1RadCheckBoxLabelText;
                        AgingFac2RadCheckBox.Text = ageingFactor2RadCheckBoxLabelText;
                        AgingFac3RadCheckBox.Text = ageingFactor3RadCheckBoxLabelText;

                        AccumAge1RadCheckBox.Visible = true;
                        AccumAge2RadCheckBox.Visible = true;
                        AccumAge3RadCheckBox.Visible = true;
                        AccumAge1RadCheckBox.Text = accumAge1RadLabelText;
                        AccumAge2RadCheckBox.Text = accumAge2RadLabelText;
                        AccumAge3RadCheckBox.Text = accumAge3RadLabelText;

                        TopOilTempRadCheckBox.Visible = true;
                        TopOilTempRadCheckBox.Text = topOilTempRadLabelText;
                        Fan1_StartsRadCheckBox.Visible = true;
                        Fan2_StartsRadCheckBox.Visible = true;
                        Fan1_StartsRadCheckBox.Text = fan1_StartsRadLabelText;
                        Fan2_StartsRadCheckBox.Text = fan2_StartsRadLabelText;
                        Fan1_RunTimeRadCheckBox.Visible = true;
                        Fan2_RunTimeRadCheckBox.Visible = true;
                        Fan1_RunTimeRadCheckBox.Text = fan1_RunTimeRadLabelText;
                        Fan2_RunTimeRadCheckBox.Text = fan2_RunTimeRadLabelText;
                    }
                }
                else
                {
                    // RadMessageBox.Show(this, currentWindingHotSpotConfigurationNotSavedInDatabaseText);
                    windingHotSpotRadGroupBox.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetWindingHotSpotDataDisplayState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void ResetWindingHotSpotCheckboxToggleStateToOff()
        {
            try
            {
                resettingWindingHotSpotCheckboxToggleState = true;

                WHS1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                WHS2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                WHS3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                agingFac1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                AgingFac2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                AgingFac3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                AccumAge1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                AccumAge2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                AccumAge3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                TopOilTempRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                Fan1_StartsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                Fan2_StartsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                Fan1_RunTimeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                Fan2_RunTimeRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;

                resettingWindingHotSpotCheckboxToggleState = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.ResetWindingHotSpotCheckboxToggleStateToOff()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        ///  Capture the arrow keys to use for cursor movement in the graphs
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            try
            {
                if ((this.cursorIsEnabled) &&
                    (dataRadPageView.SelectedPage != dataGridRadPageViewPage))
                {
                    const int WM_KEYDOWN = 0x100;
                    const int WM_SYSKEYDOWN = 0x104;

                    if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
                    {
                        //if ((dataDisplayRadPageView.SelectedPage == trendRadPageViewPage) ||
                        //    (dataDisplayRadPageView.SelectedPage == polarRadPageViewPage))
                        //{
                        switch (keyData)
                        {
                            case Keys.Left:
                                MoveXYcursorLeft();
                                return true;

                            case Keys.Right:
                                MoveXYcursorRight();
                                return true;
                        }
                        //}
                        //else
                        //{
                        //    /// not sure anything is necessary here, as I have set up events so that
                        //    /// when the user selects the grid view tab, focus is set to the grid
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DataViewer.ProcessCmdKey(ref Message, Keys)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        //private bool TrendCursorIsInRange(int proposedNewXcoordinateInPixels)
        //{
        //    return !((proposedNewXcoordinateInPixels < this.trendGraphOffsetX) ||
        //        (proposedNewXcoordinateInPixels > (this.trendGraphWidth + this.trendGraphOffsetX)));
        //}

        private void SetToolTips()
        {
            toolTip1.AutoPopDelay = 3000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            toolTip1.SetToolTip(this.dataAgeRadGroupBox, "Load data by its age with respect to the current date");
            toolTip1.SetToolTip(this.dynamicsRadGroupBox, "Use right mouse to open dynamics editor");
        }

        private void dataRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {
        }

        private void trendThreeMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    //SetThreeMonthsRadRadioButtonState();
                    ReloadData();
                    if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawAllGraphs();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendThreeMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendSixMonthsRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    // SetSixMonthsRadRadioButtonState();
                    ReloadData();
                    if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawAllGraphs();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendSixMonthsRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendOneYearRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    // SetOneYearRadRadioButtonState();
                    ReloadData();
                    if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawAllGraphs();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendOneYearRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendAllDataRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
        }

        private void SetThreeMonthsRadRadioButtonState()
        {
            try
            {
                if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    trendThreeMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetThreeMonthsRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Changes the ToggleState of the data age radio buttons in all tabs to have the six months selection
        /// </summary>
        private void SetSixMonthsRadRadioButtonState()
        {
            try
            {
                if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    trendSixMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetSixMonthsRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Changes the ToggleState of the data age radio buttons in all tabs to have the one year selection
        /// </summary>
        private void SetOneYearRadRadioButtonState()
        {
            try
            {
                if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    trendOneYearRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetOneYearRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Changes the ToggleState of the data age radio buttons in all tabs to have the all data selection
        /// </summary>
        private void SetAllDataRadRadioButtonState()
        {
            try
            {
                if (trendAllDataRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
                {
                    trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetAllDataRadRadioButtonState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Makes the call to draw all graphs in the interface.  Only the graph in the selected tab will actually be rendered.
        /// </summary>
        private void DrawAllGraphs()
        {
            try
            {
                if (dataRadPageView.SelectedPage == trendRadPageViewPage)
                {
                    if (this.trendChartNeedsUpdating)
                    {
                        DrawTrendChart(trendWinChartViewer);
                        this.trendChartNeedsUpdating = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.DrawAllGraphs()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Reads the amount of data selected by the user and stores it in memory
        /// </summary>
        private void ReloadData()
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    DateTime dataLimitDateTime = ConversionMethods.MinimumDateTime();
                    if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 0, 3);
                    }
                    else if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 0, 6);
                    }
                    else if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        dataLimitDateTime = ConversionMethods.GetPastDateTime(DateTime.Now, 1, 0);
                    }

                    this.dataCutoffDate = dataLimitDateTime;

                    if (LoadData(dataLimitDateTime))
                    {
                        SetTrendChartDateLimits();

                        SetGridViewBindingSources();
                        SetAllGridViewColumnHeaderText();
                        SetAllGridViewColumnWidths();
                        // reset internal variables to be associated with new data values                       
                    }
                    else
                    {
                        RadMessageBox.Show(this, noDataForSelectedDataAgeText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.ReloadData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Loads the data from the database such that their reading date is greater than the argument date passed in
        /// </summary>
        /// <param name="oldestDataDate"></param>
        public bool LoadData(DateTime oldestDataDate)
        {
           
            bool dataIsPresent = false;
            try
            {
                
                DateTime readingDateTime;

                int counter = 0;
              
                List<Main_SingleDataReading> allDataReadings;
               
                using (MonitorInterfaceDB db = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    /// get all the data at once
                    allDataReadings = DataConversion.Main_GetAllDataForOneMonitor(this.monitorID, oldestDataDate, db);
                    this.totalDynamicsDataReadings = allDataReadings.Count;
                }
               
                this.totalWindingHotSpotDataReadings = 0;
                if (this.windingHotSpotDataIsExpected)
                {
                    for (int i = 0; i < this.totalDynamicsDataReadings; i++)
                    {
                        if (allDataReadings[i].whsData != null)
                        {
                            this.totalWindingHotSpotDataReadings++;
                        }
                    }
                }
                SetNewSQLConnectionAndGetCurrentData();

                if (this.totalDynamicsDataReadings > 0)
                {
                    dataIsPresent = true;

                    /// memory allocation
                    this.monitorData.AllocateAllArrays(this.totalDynamicsDataReadings);

                    if (this.totalWindingHotSpotDataReadings > 0)
                    {
                        this.windingHotSpotData.AllocateAllArrays(this.totalWindingHotSpotDataReadings);
                    }

                    int itemCount = 0;
                    foreach (Main_SingleDataReading singleDataReading in allDataReadings)
                    {
                        itemCount++;
                        readingDateTime = singleDataReading.monitorData.ReadingDateTime;

                        // first, add the common portion of the data to the internal representation
                        this.monitorData.Add(singleDataReading.monitorData);

                        if (this.windingHotSpotDataIsExpected)
                        {
                            if (singleDataReading.whsData != null)
                            {
                                this.windingHotSpotData.Add(singleDataReading.whsData, readingDateTime, this.transformerTypeIsStandard);
                            }
                        }

                        counter++;
                        if (counter > 90)
                        {
                            int glah = counter;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.LoadData(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return dataIsPresent;
        }

        private Main_DataViewerPreferences CloneMain_DataViewerPreferences(Main_DataViewerPreferences otherMonitorPreferences)
        {
            Main_DataViewerPreferences preferences = null;
            try
            {
                preferences = new Main_DataViewerPreferences();
                preferences.ID = Guid.NewGuid();
                preferences.MonitorID = this.monitorID;

                preferences.DataAgeInMonths = otherMonitorPreferences.DataAgeInMonths;
                preferences.AutoDrawIsEnabled = otherMonitorPreferences.AutoDrawIsEnabled;
                preferences.TrendMouseAction = otherMonitorPreferences.TrendMouseAction;

                preferences.TrendShowTrendLineSelected = otherMonitorPreferences.TrendShowTrendLineSelected;
                preferences.TrendChosenTrendLineType = otherMonitorPreferences.TrendChosenTrendLineType;
                preferences.TrendNormalizeDataSelected = otherMonitorPreferences.TrendNormalizeDataSelected;
                preferences.TrendShowMovingAverageSelected = otherMonitorPreferences.TrendShowMovingAverageSelected;
                preferences.TrendMovingAverageItemCount = otherMonitorPreferences.TrendMovingAverageItemCount;
                preferences.TrendHumiditySelected = otherMonitorPreferences.TrendHumiditySelected;
                preferences.TrendLoadCurrent1Selected = otherMonitorPreferences.TrendLoadCurrent1Selected;
                preferences.TrendLoadCurrent2Selected = otherMonitorPreferences.TrendLoadCurrent2Selected;
                preferences.TrendLoadCurrent3Selected = otherMonitorPreferences.TrendLoadCurrent3Selected;
                preferences.TrendTemp1Selected = otherMonitorPreferences.TrendTemp1Selected;
                preferences.TrendTemp2Selected = otherMonitorPreferences.TrendTemp2Selected;
                preferences.TrendTemp3Selected = otherMonitorPreferences.TrendTemp3Selected;
                preferences.TrendTemp4Selected = otherMonitorPreferences.TrendTemp4Selected;
                preferences.TrendVoltage1Selected = otherMonitorPreferences.TrendVoltage1Selected;
                preferences.TrendVoltage2Selected = otherMonitorPreferences.TrendVoltage2Selected;
                preferences.TrendVoltage3Selected = otherMonitorPreferences.TrendVoltage3Selected;
                preferences.TrendCursorIsEnabled = otherMonitorPreferences.TrendCursorIsEnabled;

                preferences.DynamicsGridColumnWidths = otherMonitorPreferences.DynamicsGridColumnWidths;

                preferences.DynamicsVariableNames = otherMonitorPreferences.DynamicsVariableNames;
                preferences.DynamicsScaleFactors = otherMonitorPreferences.DynamicsScaleFactors;
                preferences.DynamicsOperations = otherMonitorPreferences.DynamicsOperations;
                preferences.MiscellaneousProperties = "No values saved at present";
                preferences.DateSaved = otherMonitorPreferences.DateSaved;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.CloneMain_DataViewerPreferences(Main_DataViewerPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return preferences;
        }

        private void PreferencesLoad()
        {
            try
            {
                Main_DataViewerPreferences preferences = null;
                Main_DataViewerPreferences otherMonitorPreferences = null;

                bool preferencesMightBeIncompatable = false;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    preferences = General_DatabaseMethods.Main_DataViewerPreferences_Load(this.monitorID, localDB);
                    if (preferences == null)
                    {
                        otherMonitorPreferences = General_DatabaseMethods.Main_DataViewerPreferences_LoadMostRecentlySavedPreferences(localDB);
                    }
                }
                if (otherMonitorPreferences != null)
                {
                    preferences = CloneMain_DataViewerPreferences(otherMonitorPreferences);
                }
                if (preferences != null)
                {
                    if (preferences.DataAgeInMonths == 3)
                    {
                        trendThreeMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataAgeInMonths == 6)
                    {
                        trendSixMonthsRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.DataAgeInMonths == 12)
                    {
                        trendOneYearRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.AutoDrawIsEnabled)
                    {
                        trendAutomaticDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else
                    {
                        trendManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendMouseAction == 1)
                    {
                        trendPointerRadioButton.Checked = true;
                    }
                    else if (preferences.TrendMouseAction == 2)
                    {
                        trendZoomInRadioButton.Checked = true;
                    }
                    else if (preferences.TrendMouseAction == 3)
                    {
                        trendZoomOutRadioButton.Checked = true;
                    }

                    if (preferences.TrendShowTrendLineSelected)
                    {
                        showTrendLineRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendChosenTrendLineType == 1)
                    {
                        linearTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }
                    else if (preferences.TrendChosenTrendLineType == 2)
                    {
                        exponentialTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendNormalizeDataSelected)
                    {
                        normalizeDataRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendShowMovingAverageSelected)
                    {
                        movingAverageRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    this.movingAverageRadSpinEditor.Value = (decimal)preferences.TrendMovingAverageItemCount;

                    ///Dynamics

                    if (preferences.TrendHumiditySelected)
                    {
                        humidityRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendLoadCurrent1Selected)
                    {
                        loadCurrent1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendLoadCurrent2Selected)
                    {
                        loadCurrent2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendLoadCurrent3Selected)
                    {
                        loadCurrent3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp1Selected)
                    {
                        temp1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp2Selected)
                    {
                        temp2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp3Selected)
                    {
                        temp3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendTemp4Selected)
                    {
                        temp4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendVoltage1Selected)
                    {
                        voltage1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendVoltage2Selected)
                    {
                        voltage2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendVoltage3Selected)
                    {
                        voltage3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    }

                    if (preferences.TrendCursorIsEnabled)
                    {
                        this.cursorIsEnabled = true;
                    }
                    else
                    {
                        this.cursorIsEnabled = false;
                    }

                    //if (preferences.PRPDDMarkerType == 1)
                    //{
                    //    matrixDotMarkerRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    //}
                    //else if (preferences.PRPDDMarkerType == 2)
                    //{
                    //    matrixRectangleMarkerRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    //}

                    this.dynamicsGridColumnWidths = GetCorrectedColumnWidths(preferences.DynamicsGridColumnWidths, this.dynamicsGridColumnWidths);

                    string[] variableNames = preferences.DynamicsVariableNames.Split(',');
                    if (variableNames.Length == 22)
                    {
                        this.humidityRadCheckBox.Text = variableNames[0];
                        this.temp1RadCheckBox.Text = variableNames[1];
                        this.temp2RadCheckBox.Text = variableNames[2];
                        this.temp3RadCheckBox.Text = variableNames[3];
                        this.temp4RadCheckBox.Text = variableNames[4];
                        this.loadCurrent1RadCheckBox.Text = variableNames[5];
                        this.loadCurrent2RadCheckBox.Text = variableNames[6];
                        this.loadCurrent3RadCheckBox.Text = variableNames[7];
                        this.voltage1RadCheckBox.Text = variableNames[8];
                        this.voltage2RadCheckBox.Text = variableNames[9];
                        this.voltage3RadCheckBox.Text = variableNames[10];
                        this.moistureContentRadCheckBox.Text = variableNames[11];
                        this.analog1RadCheckBox.Text = variableNames[12];
                        this.analog2RadCheckBox.Text = variableNames[13];
                        this.analog3RadCheckBox.Text = variableNames[14];
                        this.analog4RadCheckBox.Text = variableNames[15];
                        this.analog5RadCheckBox.Text = variableNames[16];
                        this.analog6RadCheckBox.Text = variableNames[17];
                        this.vibration1RadCheckBox.Text = variableNames[18];
                        this.vibration2RadCheckBox.Text = variableNames[19];
                        this.vibration3RadCheckBox.Text = variableNames[20];
                        this.vibration4RadCheckBox.Text = variableNames[21];
                    }
                    else
                    {
                        preferencesMightBeIncompatable = true;
                    }

                    string[] scaleFactors = preferences.DynamicsScaleFactors.Split(',');
                    if (variableNames.Length == 22)
                    {
                        this.humidityScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[0]);
                        this.temp1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[1]);
                        this.temp2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[2]);
                        this.temp3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[3]);
                        this.temp4ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[4]);
                        this.loadCurrent1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[5]);
                        this.loadCurrent2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[6]);
                        this.loadCurrent3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[7]);
                        this.voltage1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[8]);
                        this.voltage2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[9]);
                        this.voltage3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[10]);
                        this.moistureScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[11]);
                        this.analog1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[12]);
                        this.analog2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[13]);
                        this.analog3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[14]);
                        this.analog4ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[15]);
                        this.analog5ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[16]);
                        this.analog6ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[17]);
                        this.vibration1ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[18]);
                        this.vibration2ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[19]);
                        this.vibration3ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[20]);
                        this.vibration4ScaleFactor = ConversionMethods.ConvertStringToDouble(scaleFactors[21]);
                    }
                    else
                    {
                        preferencesMightBeIncompatable = true;
                    }

                    string[] operations = preferences.DynamicsOperations.Split(',');
                    if (variableNames.Length == 22)
                    {
                        this.humidityOperation = operations[0];
                        this.temp1Operation = operations[1];
                        this.temp2Operation = operations[2];
                        this.temp3Operation = operations[3];
                        this.temp4Operation = operations[4];
                        this.loadCurrent1Operation = operations[5];
                        this.loadCurrent2Operation = operations[6];
                        this.loadCurrent3Operation = operations[7];
                        this.voltage1Operation = operations[8];
                        this.voltage2Operation = operations[9];
                        this.voltage3Operation = operations[10];
                        this.moistureOperation = operations[11];
                        this.analog1Operation = operations[12];
                        this.analog2Operation = operations[13];
                        this.analog3Operation = operations[14];
                        this.analog4Operation = operations[15];
                        this.analog5Operation = operations[16];
                        this.analog6Operation = operations[17];
                        this.vibration1Operation = operations[18];
                        this.vibration2Operation = operations[19];
                        this.vibration3Operation = operations[20];
                        this.vibration4Operation = operations[21];
                    }
                    else
                    {
                        preferencesMightBeIncompatable = true;
                    }

                    if (preferencesMightBeIncompatable)
                    {
                        /// maybe i'll do something here some day. or not.
                    }

                    if (preferences.TabSelected == 0)
                    {
                        this.dataRadPageView.SelectedPage = this.trendRadPageViewPage;
                    }
                    else if (preferences.TabSelected == 1)
                    {
                        this.dataRadPageView.SelectedPage = this.dataGridRadPageViewPage;
                    }

                    string[] miscellaneous = preferences.MiscellaneousProperties.Split(';');
                    string[] pieces;
                    int colorValue;
                    this.miscellaneousProperties = new Dictionary<string, string>();
                    if ((miscellaneous != null) && (miscellaneous.Length > 0))
                    {
                        foreach (string entry in miscellaneous)
                        {
                            pieces = entry.Split(',');
                            if (pieces.Length == 2)
                            {
                                if (!this.miscellaneousProperties.ContainsKey(pieces[0].Trim()))
                                {
                                    this.miscellaneousProperties.Add(pieces[0].Trim(), pieces[1].Trim());
                                }
                            }
                        }
                        // analog 1
                        if (this.miscellaneousProperties.ContainsKey(analog1RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog1RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        //analog 2
                        if (this.miscellaneousProperties.ContainsKey(analog2RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog2RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // analog 3
                        if (this.miscellaneousProperties.ContainsKey(analog3RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog3RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // analog 4
                        if (this.miscellaneousProperties.ContainsKey(analog4RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog4RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // analog 5
                        if (this.miscellaneousProperties.ContainsKey(analog5RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog5RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog5RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog5RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // analog 6
                        if (this.miscellaneousProperties.ContainsKey(analog6RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[analog6RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.analog6RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.analog6RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // vibration 1
                        if (this.miscellaneousProperties.ContainsKey(vibration1RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[vibration1RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.vibration1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.vibration1RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // vibration 2
                        if (this.miscellaneousProperties.ContainsKey(vibration2RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[vibration2RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.vibration2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.vibration2RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // vibration 3
                        if (this.miscellaneousProperties.ContainsKey(vibration3RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[vibration3RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.vibration3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.vibration3RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                        // vibration 4
                        if (this.miscellaneousProperties.ContainsKey(vibration4RadCheckBoxEnabledKeyText))
                        {
                            if (miscellaneousProperties[vibration4RadCheckBoxEnabledKeyText].CompareTo(trueText) == 0)
                            {
                                this.vibration4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                            }
                            else
                            {
                                this.vibration4RadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.PreferencesLoad()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private int[] GetCorrectedColumnWidths(string columnWidthsAsString, int[] currentColumnWidthsAsInts)
        {
            int[] columnWidthsAsInt = null;
            try
            {
                columnWidthsAsInt = ConversionMethods.ConvertCommaSeparatedStringOfIntsToIntegerArray(columnWidthsAsString);
                if (columnWidthsAsInt == null)
                {
                    columnWidthsAsInt = currentColumnWidthsAsInts;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetCorrectedColumnWidths(string, int[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return columnWidthsAsInt;
        }

        private void PreferencesSave()
        {
            try
            {
                Main_DataViewerPreferences preferences = new Main_DataViewerPreferences();
                preferences.ID = Guid.NewGuid();
                preferences.MonitorID = this.monitorID;

                if (trendThreeMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 3;
                }
                else if (trendSixMonthsRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 6;
                }
                else if (trendOneYearRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.DataAgeInMonths = 12;
                }
                else
                {
                    preferences.DataAgeInMonths = 0;
                }

                if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.AutoDrawIsEnabled = true;
                }
                else
                {
                    preferences.AutoDrawIsEnabled = false;
                }

                if (trendPointerRadioButton.Checked)
                {
                    preferences.TrendMouseAction = 1;
                }
                else if (trendZoomInRadioButton.Checked)
                {
                    preferences.TrendMouseAction = 2;
                }
                else if (trendZoomOutRadioButton.Checked)
                {
                    preferences.TrendMouseAction = 3;
                }

                if (showTrendLineRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendShowTrendLineSelected = true;
                }
                else
                {
                    preferences.TrendShowTrendLineSelected = false;
                }

                if (linearTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendChosenTrendLineType = 1;
                }
                else if (exponentialTrendRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendChosenTrendLineType = 2;
                }

                if (normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendNormalizeDataSelected = true;
                }
                else
                {
                    preferences.TrendNormalizeDataSelected = false;
                }

                if (movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendShowMovingAverageSelected = true;
                }
                else
                {
                    preferences.TrendShowMovingAverageSelected = false;
                }

                preferences.TrendMovingAverageItemCount = (int)this.movingAverageRadSpinEditor.Value;

                if (humidityRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendHumiditySelected = true;
                }
                else
                {
                    preferences.TrendHumiditySelected = false;
                }

                if (loadCurrent1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendLoadCurrent1Selected = true;
                }
                else
                {
                    preferences.TrendLoadCurrent1Selected = false;
                }

                if (loadCurrent2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendLoadCurrent2Selected = true;
                }
                else
                {
                    preferences.TrendLoadCurrent2Selected = false;
                }

                if (loadCurrent3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendLoadCurrent3Selected = true;
                }
                else
                {
                    preferences.TrendLoadCurrent3Selected = false;
                }

                if (temp2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendTemp1Selected = true;
                }
                else
                {
                    preferences.TrendTemp1Selected = false;
                }

                if (temp1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendTemp2Selected = true;
                }
                else
                {
                    preferences.TrendTemp2Selected = false;
                }

                if (temp3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendTemp3Selected = true;
                }
                else
                {
                    preferences.TrendTemp3Selected = false;
                }

                if (temp4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendTemp4Selected = true;
                }
                else
                {
                    preferences.TrendTemp4Selected = false;
                }

                if (voltage1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendVoltage1Selected = true;
                }
                else
                {
                    preferences.TrendVoltage1Selected = false;
                }

                if (voltage2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendVoltage2Selected = true;
                }
                else
                {
                    preferences.TrendVoltage2Selected = false;
                }

                if (voltage3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    preferences.TrendVoltage3Selected = true;
                }
                else
                {
                    preferences.TrendVoltage3Selected = false;
                }

                //if (trendEnableCursorRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //{
                preferences.TrendCursorIsEnabled = true;
                //}
                //else if (trendDisableCursorRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //{
                //    preferences.TrendCursorIsEnabled = false;
                //}

                preferences.DynamicsGridColumnWidths = ConversionMethods.ConvertIntegerArrayToStringOfCommaSeparatedInts(this.dynamicsGridColumnWidths);

                StringBuilder dynamicsVariableNames = new StringBuilder();
                dynamicsVariableNames.Append(this.humidityRadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.temp4RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.loadCurrent3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.voltage3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.moistureContentRadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog1RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog2RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog3RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog4RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog5RadCheckBox.Text);
                dynamicsVariableNames.Append(",");
                dynamicsVariableNames.Append(this.analog1RadCheckBox.Text);

                preferences.DynamicsVariableNames = dynamicsVariableNames.ToString();

                StringBuilder dynamicsScaleFactors = new StringBuilder();
                dynamicsScaleFactors.Append(this.humidityScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.temp4ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.loadCurrent3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.voltage3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.moistureScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog1ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog2ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog3ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog4ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog5ScaleFactor.ToString());
                dynamicsScaleFactors.Append(",");
                dynamicsScaleFactors.Append(this.analog6ScaleFactor.ToString());

                preferences.DynamicsScaleFactors = dynamicsScaleFactors.ToString();

                StringBuilder dynamicsOperations = new StringBuilder();
                dynamicsOperations.Append(this.humidityOperation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.temp4Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.loadCurrent3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.voltage3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.moistureOperation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog1Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog2Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog3Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog4Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog5Operation);
                dynamicsOperations.Append(",");
                dynamicsOperations.Append(this.analog6Operation);

                preferences.DynamicsOperations = dynamicsOperations.ToString();

                if (this.dataRadPageView.SelectedPage == this.trendRadPageViewPage)
                {
                    preferences.TabSelected = 0;
                }
                else if (this.dataRadPageView.SelectedPage == this.dataGridRadPageViewPage)
                {
                    preferences.TabSelected = 1;
                }

                StringBuilder miscellaneousPropertiesEntries = new StringBuilder();
                // analog 1
                miscellaneousPropertiesEntries.Append(analog1RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // analog 2
                miscellaneousPropertiesEntries.Append(analog2RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                //analog3
                miscellaneousPropertiesEntries.Append(analog3RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // analog 4
                miscellaneousPropertiesEntries.Append(analog4RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // analog 5
                miscellaneousPropertiesEntries.Append(analog5RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog5RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // analog 6
                miscellaneousPropertiesEntries.Append(analog6RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.analog6RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // vibration 1
                miscellaneousPropertiesEntries.Append(vibration1RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.vibration1RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // vibration 2
                miscellaneousPropertiesEntries.Append(vibration2RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.vibration2RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // vibraiton 3
                miscellaneousPropertiesEntries.Append(vibration3RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.vibration3RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }
                miscellaneousPropertiesEntries.Append(";");
                // vibration 4
                miscellaneousPropertiesEntries.Append(vibration4RadCheckBoxEnabledKeyText);
                miscellaneousPropertiesEntries.Append(",");
                if (this.vibration4RadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    miscellaneousPropertiesEntries.Append(trueText);
                }
                else
                {
                    miscellaneousPropertiesEntries.Append(falseText);
                }

                preferences.MiscellaneousProperties = miscellaneousPropertiesEntries.ToString();

                preferences.DateSaved = DateTime.Now;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    General_DatabaseMethods.Main_DataViewerPreferences_Save(preferences, localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.PreferencesSave()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void dynamicsRadGroupBox_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    dynamicsRadGroupBoxRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.dynamicsRadGroupBox_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendWinChartViewer_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                //trendChartHasMouseFocus = true;
                UpdateTrendImageMap(this.trendWinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendWinChartViewer_MouseEnter(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendWinChartViewer_MouseLeave(object sender, EventArgs e)
        {
            //trendChartHasMouseFocus = false;
        }

        private void trendWinChartViewer_ClickHotSpot(object sender, WinHotSpotEventArgs e)
        {
            try
            {
                if (trendWinChartViewer.MouseUsage == WinChartMouseUsage.ScrollOnDrag)
                {
                    /// If the cursor is active, move it to the new position of the latest click.
                    if (this.cursorIsEnabled)
                    {
                        string attrValueX = e.AttrValues["x"].ToString();
                        this.cursorXval = Double.Parse(attrValueX);
                        this.cursorDateTimeAsDoubleIndex = FindAllDateTimeAsDoubleIndex(this.cursorXval);
                        TrendUpdateCursorPosition();
                    }
                    else
                    {
                        new ParamViewer().Display(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendWinChartViewer_ClickHotSpot(object, WinHotSpotEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendWinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref trendRadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendWinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void movingAverageRadSpinEditor_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.movingAverageRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    this.trendChartNeedsUpdating = true;
                    if (this.trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.movingAverageRadSpinEditor_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #region trend context menu

        private void SetupTrendContextMenu()
        {
            RadMenuItem menuItem;
            // we can eventually switch on the monitorTypeString if we need to customize
            // the context menu for specific monitor types
            trendRadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += trendCopyGraph_Click;
            menuItem.Text = copyGraphTrendContextMenuText;
            trendRadContextMenu.Items.Add(menuItem);
        }

        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    graphRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendCopyGraph_Click(object sender, EventArgs e)
        {
            try
            {
                if (trendWinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(trendWinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendCopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        #endregion

        private void trendPointerRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendPointerRadioButton.Checked)
                {
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ScrollOnDrag;
                    trendPointerRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendPointerRadioButton.BackColor = trendPointerRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.trendPointerRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendZoomInRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendZoomInRadioButton.Checked)
                {
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomIn;
                    trendZoomInRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendZoomInRadioButton.BackColor = trendZoomInRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.zoomInRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendZoomOutRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (trendZoomOutRadioButton.Checked)
                {
                    trendWinChartViewer.MouseUsage = ChartDirector.WinChartMouseUsage.ZoomOut;
                    trendZoomOutRadioButton.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    trendZoomOutRadioButton.BackColor = trendZoomOutRadioButton.Parent.BackColor;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.zoomOutRadioButton_CheckedChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// The ViewPortChanged event handler. This event occurs when the user changes the 
        /// WinChartViewer view port by dragging scrolling, or by zoom in/out, or the 
        /// WinChartViewer.updateViewPort method is being called.
        /// </summary>
        private void trendWinChartViewer_ViewPortChanged(object sender, WinViewPortEventArgs e)
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    // Compute the view port start date and duration
                    DateTime currentStartDate = this.minimumDateOfDataSet.AddSeconds(Math.Round(trendWinChartViewer.ViewPortLeft * dateRange));
                    this.trendGraphCurrentDuration = Math.Round(trendWinChartViewer.ViewPortWidth * dateRange);

                    // Synchronize the horizontal scroll bar with the WinChartViewer
                    trendRadHScrollBar.Enabled = trendWinChartViewer.ViewPortWidth < 1;
                    trendRadHScrollBar.LargeChange = (int)Math.Ceiling(trendWinChartViewer.ViewPortWidth *
                                                                       (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum));
                    trendRadHScrollBar.SmallChange = (int)Math.Ceiling(trendRadHScrollBar.LargeChange * 0.1);
                    trendRadHScrollBar.Value = (int)Math.Round(trendWinChartViewer.ViewPortLeft *
                                                               (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum)) + trendRadHScrollBar.Minimum;

                    // Synchronize the vertical scroll bar with the WinChartViewer
                    //trendRadVScrollBar.Enabled = trendWinChartViewer.ViewPortHeight < 1;
                    //trendRadVScrollBar.LargeChange = (int)Math.Ceiling(trendWinChartViewer.ViewPortHeight *
                    //    (trendRadVScrollBar.Maximum - trendRadVScrollBar.Minimum));
                    //trendRadVScrollBar.SmallChange = (int)Math.Ceiling(trendRadVScrollBar.LargeChange * 0.1);
                    //trendRadVScrollBar.Value = (int)Math.Round(trendWinChartViewer.ViewPortTop *
                    //    (trendRadVScrollBar.Maximum - trendRadVScrollBar.Minimum)) + trendRadVScrollBar.Minimum;

                    // Update chart and image map if necessary
                    if (e.NeedUpdateChart)
                    {
                        DrawTrendChart(trendWinChartViewer);
                    }
                    if (e.NeedUpdateImageMap)
                    {
                        UpdateTrendImageMap(trendWinChartViewer);
                    }
                    if (this.cursorIsEnabled)
                    {
                        TrendUpdateCursorPosition();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendWinChartViewer_ViewPortChanged(object, WinViewPortEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Adjusts the width and scrolling range of the horizontal scrollbar 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trendRadHScrollBar_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (hasFinishedInitialization)
                {
                    // Set the view port based on the scroll bar
                    this.trendWinChartViewer.ViewPortLeft = ((double)(trendRadHScrollBar.Value - trendRadHScrollBar.Minimum)) /
                                                            (trendRadHScrollBar.Maximum - trendRadHScrollBar.Minimum);

                    // Update the chart display without updating the image maps. (We can delay updating
                    // the image map until scrolling is completed and the chart display is stable.)
                    trendWinChartViewer.updateViewPort(true, false);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHMDataViewer.trendRadHScrollBar_ValueChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void expandAllRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                if ((this.dynamicsRadGridView.MasterTemplate.Templates != null) &&
        //                   (this.Main_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0))
        //                {
        //                    foreach (GridViewRowInfo row in this.Main_DataReadingsRadGridView.Rows)
        //                    {
        //                        row.IsExpanded = true;
        //                    }

        //                    if (this.chPDParametersGridViewTemplate != null)
        //                    {
        //                        foreach (GridViewRowInfo row in this.chPDParametersGridViewTemplate.Rows)
        //                        {
        //                            row.IsExpanded = true;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_DataViewer.expandAllRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void hideAllRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                if ((this.Main_DataReadingsRadGridView.MasterTemplate.Templates != null) &&
        //                  (this.Main_DataReadingsRadGridView.MasterTemplate.Templates.Count > 0))
        //                {
        //                    foreach (GridViewRowInfo row in this.Main_DataReadingsRadGridView.Rows)
        //                    {
        //                        row.IsExpanded = false;
        //                    }

        //                    if (this.chPDParametersGridViewTemplate != null)
        //                    {
        //                        foreach (GridViewRowInfo row in this.chPDParametersGridViewTemplate.Rows)
        //                        {
        //                            row.IsExpanded = false;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_DataViewer.hideAllRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void deleteSelectedEntriesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    Main_Data_DataRoot foundDataRootEntry;
                    List<Main_Data_DataRoot> selectedDataRootEntries = new List<Main_Data_DataRoot>();
                    List<DateTime> selectedEntryDates = new List<DateTime>();
                    Dictionary<Guid, int> selectedEntries = GetAllSelectedGridEntries();
                    using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                    {
                        foreach (KeyValuePair<Guid, int> entry in selectedEntries)
                        {
                            foundDataRootEntry = MainMonitor_DatabaseMethods.Main_Data_GetDataRootTableEntry(entry.Key, localDB);
                            if (foundDataRootEntry != null)
                            {
                                selectedDataRootEntries.Add(foundDataRootEntry);
                                selectedEntryDates.Add(foundDataRootEntry.ReadingDateTime);
                            }
                        }
                        selectedEntryDates.Sort();
                        using (DisplayItemsToDelete deleteForm = new DisplayItemsToDelete(selectedEntryDates))
                        {
                            deleteForm.ShowDialog();
                            deleteForm.Hide();
                            if (deleteForm.DeleteSelectedData)
                            {
                                GetAllGridViewColumnWidths();
                                MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntries(selectedDataRootEntries, localDB);
                                ReloadData();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.deleteSelectedEntriesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void gridDataEditRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    GridDataEnableEditing();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDataViewer.gridDataEditRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void gridDataSaveChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.dynamicsBindingSource.EndEdit();

                GetAllGridViewColumnWidths();

                //this.bhm_DataReadingsBindingSource.EndEdit();
                //dbConnection.SubmitChanges();
                // SetBindingSourceAndSetSomeColumnsToReadOnly();
                // See comment for the cancel changes button on the topic of reloading data    
                //SetNewSQLConnectionAndGetCurrentData();
                //SetBindingSourceForGridView();
                ReloadData();
                // Main_DataReadingsRadGridView.Refresh();
                GridDataDisableEditing();
                graphDataNeedsUpdating = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.gridDataSaveChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void gridDataCancelChangesRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.dynamicsBindingSource.CancelEdit();

                GetAllGridViewColumnWidths();

                /// The only way to refresh the data is to reload it along a new connection.
                /// All other methods resulted in changes to the data in the gridview being
                /// "saved" in the connection, even though they were not saved in the database.
                SetNewSQLConnectionAndGetCurrentData();
                // SetBindingSourceForGridView();
                dynamicsRadGridView.Refresh();
                GridDataDisableEditing();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.gridDataCancelChangesRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// Handles all toggle-state changed events for data selection and trend line selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void dataSelection_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            try
            {
                if (!resettingWindingHotSpotCheckboxToggleState)
                {
                    if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.dataSelection_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SaveSelectedDataToCSVFile(DateTime oldestDataDate)
        {
            try
            {
                List<Main_Data_MonitorData> dataList;
                List<string> selectedData;
                StringBuilder outputLine;
                string header;
                string fileNamePrefix = string.Empty;

                ErrorCode errorCode = ErrorCode.None;
                MonitorHierarchy monitorHierarchy;

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    dataList = MainMonitor_DatabaseMethods.Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(this.monitorID, oldestDataDate, localDB);

                    monitorHierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(this.monitorID, localDB);

                    if ((monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                    {
                        fileNamePrefix = monitorHierarchy.company.Name + "-" + monitorHierarchy.plant.Name + "-" + monitorHierarchy.equipment.Name + "-Main-Data-";
                    }
                }
                if ((dataList != null) && (dataList.Count > 0) && (monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                {
                    selectedData = new List<string>();

                    header = "Reading DateTime, State, Proper Working Order, Current One, Current Two, Current Three, Voltage One, Voltage Two, Voltage Three, Humidity, Temperature One, Temperature Two, Temperature Three, Temperature Four";

                    foreach (Main_Data_MonitorData entry in dataList)
                    {
                        outputLine = new StringBuilder();

                        outputLine.Append(entry.ReadingDateTime);
                        outputLine.Append(", ");
                        outputLine.Append(entry.ReadingDateTime.ToString());
                        outputLine.Append(", ");
                        outputLine.Append(entry.State);
                        outputLine.Append(", ");
                        outputLine.Append(entry.ProperWorkingOrder);
                        outputLine.Append(", ");
                        outputLine.Append(entry.CurrentOneValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.CurrentTwoValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.CurrentThreeValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.VoltageOneValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.VoltageTwoValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.VoltageThreeValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.HumidityValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.TemperatureOneValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.TemperatureTwoValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.TemperatureThreeValue);
                        outputLine.Append(", ");
                        outputLine.Append(entry.TemperatureFourValue);
                        outputLine.Append(", ");

                        selectedData.Add(outputLine.ToString());
                    }
                    FileUtilities.SaveCSVFileToDatedString(fileNamePrefix, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), selectedData, header);
                }
                else if ((monitorHierarchy != null) && (monitorHierarchy.errorCode == ErrorCode.None))
                {
                    RadMessageBox.Show(this, failedToReadAnyDataToSaveToCsvFile);
                }
                else
                {
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDataViewer.SaveSelectedDataToCSVFile(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void saveDataToCsvRadButton_Click(object sender, EventArgs e)
        {
            SaveSelectedDataToCSVFile(this.dataCutoffDate);
        }

        private void trendDrawGraphsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DrawTrendChart(trendWinChartViewer);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.trendDrawGraphsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = mainDataViewerInterfaceTitleText;

                trendRadPageViewPage.Text = trendRadPageViewPageText;
                trendPointerRadioButton.Text = pointerRadioButtonText;
                trendZoomInRadioButton.Text = zoomInRadioButtonText;
                trendZoomOutRadioButton.Text = zoomOutRadioButtonText;
                dataAgeRadGroupBox.Text = dataAgeRadGroupBoxText;
                trendThreeMonthsRadRadioButton.Text = threeMonthsRadRadioButtonText;
                trendSixMonthsRadRadioButton.Text = sixMonthsRadRadioButtonText;
                trendOneYearRadRadioButton.Text = oneYearRadRadioButtonText;
                trendAllDataRadRadioButton.Text = allDataRadRadioButtonText;

                trendDisplayOptionsRadGroupBox.Text = trendDisplayOptionsRadGroupBoxText;
                showTrendLineRadCheckBox.Text = showTrendLineRadCheckBoxText;
                showTrendLineRadCheckBox.Text = showTrendLineRadCheckBoxText;
                linearTrendRadRadioButton.Text = linearTrendRadRadioButtonText;
                exponentialTrendRadRadioButton.Text = exponentialTrendRadRadioButtonText;
                normalizeDataRadCheckBox.Text = normalizeDataRadCheckBoxText;
                movingAverageRadCheckBox.Text = movingAverageRadCheckBoxText;
                dataItemsRadLabel.Text = dataItemsRadLabelText;
                trendGraphMethodRadGroupBox.Text = graphMethodRadGroupBoxText;
                trendAutomaticDrawRadRadioButton.Text = automaticDrawRadRadioButtonText;
                trendManualDrawRadRadioButton.Text = manualDrawRadRadioButtonText;
                trendDrawGraphsRadButton.Text = drawGraphsRadButtonText;
                dynamicsRadGroupBox.Text = dynamicsRadGroupBoxText;
                humidityRadCheckBox.Text = humidityRadCheckBoxText;
                moistureContentRadCheckBox.Text = moistureContentRadCheckBoxText;
                temp1RadCheckBox.Text = temp1RadCheckBoxText;
                temp2RadCheckBox.Text = temp2RadCheckBoxText;
                temp3RadCheckBox.Text = temp3RadCheckBoxText;
                temp4RadCheckBox.Text = temp4RadCheckBoxText;
                loadCurrent1RadCheckBox.Text = loadCurrent1RadCheckBoxText;
                loadCurrent2RadCheckBox.Text = loadCurrent2RadCheckBoxText;
                loadCurrent3RadCheckBox.Text = loadCurrent3RadCheckBoxText;
                voltage1RadCheckBox.Text = voltage1RadCheckBoxText;
                voltage2RadCheckBox.Text = voltage2RadCheckBoxText;
                voltage3RadCheckBox.Text = voltage3RadCheckBoxText;
                analog1RadCheckBox.Text = analog1RadCheckBoxText;
                analog2RadCheckBox.Text = analog2RadCheckBoxText;
                analog3RadCheckBox.Text = analog3RadCheckBoxText;
                analog4RadCheckBox.Text = analog4RadCheckBoxText;
                analog5RadCheckBox.Text = analog5RadCheckBoxText;
                analog6RadCheckBox.Text = analog6RadCheckBoxText;
                vibration1RadCheckBox.Text = vibration1RadCheckBoxText;
                vibration2RadCheckBox.Text = vibration2RadCheckBoxText;
                vibration3RadCheckBox.Text = vibration3RadCheckBoxText;
                vibration4RadCheckBox.Text = vibration4RadCheckBoxText;

                dataGridRadPageViewPage.Text = dataGridRadPageViewPageText;
                //expandAllRadButton.Text = expandAllRadButtonText;
                //hideAllRadButton.Text = hideAllRadButtonText;
                gridDataEditRadButton.Text = gridDataEditRadButtonText;
                gridDataSaveChangesRadButton.Text = gridDataSaveChangesRadButtonText;
                gridDataCancelChangesRadButton.Text = gridDataCancelChangesRadButtonText;
                gridDataDeleteSelectedRadButton.Text = gridDataDeleteSelectedRadButtonText;
                gridDataSaveDataToCsvRadButton.Text = gridDataSaveDataToCsvRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                noDataForSelectedDataAgeText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerNoDataForSelectedDataAgeText", noDataForSelectedDataAgeText, htmlFontType, htmlStandardFontSize, "");
                // copyGraphText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerCopyGraphText", copyGraphText, htmlFontType, htmlStandardFontSize, "");
                addText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerAddText", addText, htmlFontType, htmlStandardFontSize, "");
                multiplyText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerMultiplyText", multiplyText, htmlFontType, htmlStandardFontSize, "");
                idDynamicsGridHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerIdDynamicsGridHeaderText", idDynamicsGridHeaderText, htmlFontType, htmlStandardFontSize, "");
                dataRootIdDynamicsGridHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerDataRootIdDynamicsGridHeaderText", dataRootIdDynamicsGridHeaderText, htmlFontType, htmlStandardFontSize, "");
                readingDateDynamicsGridHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerReadingDateDynamicsGridHeaderText", readingDateDynamicsGridHeaderText, htmlFontType, htmlStandardFontSize, "");
                stateDynamicsGridHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerStateDynamicsGridHeaderText", stateDynamicsGridHeaderText, htmlFontType, htmlStandardFontSize, "");
                properWorkingOrderDynamicsGridHeaderText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerProperWorkingOrderDynamicsGridHeaderText", properWorkingOrderDynamicsGridHeaderText, htmlFontType, htmlStandardFontSize, "");

                humidityRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsHumidityRadCheckBoxText", humidityRadCheckBoxText, "", "", "");
                moistureContentRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabMoistureContentRadCheckBoxText", moistureContentRadCheckBoxText, "", "", "");
                temp1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsTemp1RadCheckBoxText", temp1RadCheckBoxText, "", "", "");
                temp2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsTemp2RadCheckBoxText", temp2RadCheckBoxText, "", "", "");
                temp3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsTemp3RadCheckBoxText", temp3RadCheckBoxText, "", "", "");
                temp4RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsTemp4RadCheckBoxText", temp4RadCheckBoxText, "", "", "");
                loadCurrent1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsLoadCurrent1RadCheckBoxText", loadCurrent1RadCheckBoxText, "", "", "");
                loadCurrent2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsLoadCurrent2RadCheckBoxText", loadCurrent2RadCheckBoxText, "", "", "");
                loadCurrent3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsLoadCurrent3RadCheckBoxText", loadCurrent3RadCheckBoxText, "", "", "");
                voltage1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVoltage1RadCheckBoxText", voltage1RadCheckBoxText, "", "", "");
                voltage2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVoltage2RadCheckBoxText", voltage2RadCheckBoxText, "", "", "");
                voltage3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVoltage3RadCheckBoxText", voltage3RadCheckBoxText, "", "", "");
                analog1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog1RadCheckBoxText", analog1RadCheckBoxText, "", "", "");
                analog2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog2RadCheckBoxText", analog2RadCheckBoxText, "", "", "");
                analog3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog3RadCheckBoxText", analog3RadCheckBoxText, "", "", "");
                analog4RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog4RadCheckBoxText", analog4RadCheckBoxText, "", "", "");
                analog5RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog5RadCheckBoxText", analog5RadCheckBoxText, "", "", "");
                analog6RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsAnalog6RadCheckBoxText", analog6RadCheckBoxText, "", "", "");
                vibration1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVibration1RadCheckBoxText", vibration1RadCheckBoxText, "", "", "");
                vibration2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVibration2RadCheckBoxText", vibration2RadCheckBoxText, "", "", "");
                vibration3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVibration3RadCheckBoxText", vibration3RadCheckBoxText, "", "", "");
                vibration4RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabDynamicsVibration4RadCheckBoxText", vibration4RadCheckBoxText, "", "", "");
               
                mainDataViewerInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTitleText", mainDataViewerInterfaceTitleText, htmlFontType, htmlStandardFontSize, "");
                // labels that appear in many tabs
                pointerRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacePointerButtonText", pointerRadioButtonText, "", "", "").Trim();
                zoomInRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceZoomInButtonText", zoomInRadioButtonText, "", "", "").Trim();
                zoomOutRadioButtonText = "     " + LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceZoomOutButtonText", zoomOutRadioButtonText, "", "", "").Trim();
                dataAgeRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataAgeRadGroupBoxText", dataAgeRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                threeMonthsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataAgeThreeMonthsRadRadioButtonText", threeMonthsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                sixMonthsRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataAgeSixMonthsRadRadioButtonText", sixMonthsRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                oneYearRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataAgeOneYearRadRadioButtonText", oneYearRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                allDataRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataAgeAllRadRadioButtonText", allDataRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                graphMethodRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceGraphMethodRadGroupBoxText", graphMethodRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                automaticDrawRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceGraphMethodAutomaticDrawRadRadioButtonText", automaticDrawRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                manualDrawRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceGraphMethodManualDrawText", manualDrawRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                drawGraphsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceGraphMethodDrawGraphsRadButtonText", drawGraphsRadButtonText, htmlFontType, htmlStandardFontSize, "");

                // labels for each separate object

                trendRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTrendTabText", trendRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                trendDisplayOptionsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsRadGroupBoxText", trendDisplayOptionsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                showTrendLineRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsTrendLineRadCheckBoxText", showTrendLineRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                linearTrendRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsLinearTrendRadRadioButtonText", linearTrendRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                exponentialTrendRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsExponentialTrendRadRadioButtonText", exponentialTrendRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                normalizeDataRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsNormalizeDataRadCheckBoxText", normalizeDataRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                movingAverageRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsMovingAverageRadCheckBoxText", movingAverageRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                dataItemsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDisplayOptionsDataItemsRadLabelText", dataItemsRadLabelText, htmlFontType, htmlStandardFontSize, "");
                dynamicsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceTrendTabDynamicsRadGroupBoxText", dynamicsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                dataGridRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceDataGridTabText", dataGridRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
                //expandAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabExpandAllRadButtonText", expandAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //hideAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabHideAllRadButtonText", hideAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataEditRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabEditDataRadButtonText", gridDataEditRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataSaveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabSaveChangesRadButtonText", gridDataSaveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataCancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabCancelChangesText", gridDataCancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataDeleteSelectedRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabDeleteSelectedRadButtonText", gridDataDeleteSelectedRadButtonText, htmlFontType, htmlStandardFontSize, "");
                gridDataSaveDataToCsvRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainDataviewerInterfaceDataGridTabSaveDataToCsvRadButtonText", gridDataSaveDataToCsvRadButtonText, htmlFontType, htmlStandardFontSize, "");

                failedToReadAnyDataToSaveToCsvFile = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerFailedToReadAnyDataToSaveToCsvFileText", failedToReadAnyDataToSaveToCsvFile, htmlFontType, htmlStandardFontSize, "");

                
                whs1RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceWinding1WHSRadLabelText", whs1RadCheckBoxLabelText, "", "", "");
                whs2RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceWinding2WHSRadLabelText", whs2RadCheckBoxLabelText, "", "", "");
                whs3RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceWinding3WHSRadLabelText", whs3RadCheckBoxLabelText, "", "", "");

                topOilTempRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacetopOilTempRadLabelText", topOilTempRadLabelText, "", "", "");
                ageingFactor1RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceAgingFactor1RadLabelText", ageingFactor1RadCheckBoxLabelText, "", "", "");
                ageingFactor2RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceAgingFactor2RadLabelText", ageingFactor2RadCheckBoxLabelText, "", "", "");
                ageingFactor3RadCheckBoxLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceAgingFactor3RadLabelText", ageingFactor3RadCheckBoxLabelText, "", "", "");
                accumAge1RadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceaccumAge1RadLabelText", accumAge1RadLabelText, "", "", "");
                accumAge2RadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceaccumAge2RadLabelText", accumAge2RadLabelText, "", "", "");
                accumAge3RadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceaccumAge3RadLabelText", accumAge3RadLabelText, "", "", "");

                fan1_StartsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacefan1_StartsRadLabelText", fan1_StartsRadLabelText, "", "", "");
                fan2_StartsRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacefan2_StartsRadLabelText", fan2_StartsRadLabelText, "", "", "");
                fan1_RunTimeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacefan1_RunTimeRadLabelText", fan1_RunTimeRadLabelText, "", "", "");
                fan2_RunTimeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfacefan2_RunTimeRadLabelText", fan2_RunTimeRadLabelText, "", "", "");

                //agingfactorRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceAgingfactorRadLabelText", agingfactorRadLabelText, "", "", "");
                //accumulatedAgingRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceAccumulatedAgingRadLabelText", accumulatedAgingRadLabelText, "", "", "");

                //currentConfigurationNotSavedInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerCurrentConfigurationNotSavedInDatabaseText", currentConfigurationNotSavedInDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //currentWindingHotSpotConfigurationNotSavedInDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerCurrentWindingHotSpotConfigurationNotSavedInDatabaseText", currentWindingHotSpotConfigurationNotSavedInDatabaseText, htmlFontType, htmlStandardFontSize, "");

                openDynamicsEditorContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerOpenDynamicsEditorContextMenuText", openDynamicsEditorContextMenuText, htmlFontType, htmlStandardFontSize, "");
                copyGraphTrendContextMenuText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerCopyGraphTrendContextMenuText", copyGraphTrendContextMenuText, htmlFontType, htmlStandardFontSize, "");
                //humidityRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceHumidityRadCheckBoxText", humidityRadCheckBoxText, "", "", "");
                //moistureContentRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceMoistureContentRadCheckBoxText", moistureContentRadCheckBoxText, "", "", "");
                //temp1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTemp1RadCheckBoxText", temp1RadCheckBoxText, "", "", "");
                //temp2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTemp2RadCheckBoxText", temp2RadCheckBoxText, "", "", "");
                //temp3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTemp3RadCheckBoxText", temp3RadCheckBoxText, "", "", "");
                //temp4RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceTemp4RadCheckBoxText", temp4RadCheckBoxText, "", "", "");
                //loadCurrent1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceLoadCurrent1RadCheckBoxText", loadCurrent1RadCheckBoxText, "", "", "");
                //loadCurrent2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceLoadCurrent2RadCheckBoxText", loadCurrent2RadCheckBoxText, "", "", "");
                //loadCurrent3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceLoadCurrent3RadCheckBoxText", loadCurrent3RadCheckBoxText, "", "", "");
                //voltage1RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceVoltage1RadCheckBoxText", voltage1RadCheckBoxText, "", "", "");
                //voltage2RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceVoltage2RadCheckBoxText", voltage2RadCheckBoxText, "", "", "");
                //voltage3RadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainDataViewerInterfaceVoltage3RadCheckBoxText", voltage3RadCheckBoxText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetFormTitleToReflectDataSource(MonitorInterfaceDB localDB)
        {
            try
            {
                string titleString = mainDataViewerInterfaceTitleText;
                MonitorHierarchy hierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(this.monitorID, localDB);
                if (hierarchy.company != null)
                {
                    titleString += " - " + hierarchy.company.Name;
                }
                if (hierarchy.plant != null)
                {
                    titleString += " - " + hierarchy.plant.Name;
                }
                if (hierarchy.equipment != null)
                {
                    titleString += " - " + hierarchy.equipment.Name;
                }
                this.Text = titleString;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDataViewer.SetFormTitleToReflectDataSource(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Main_DataViewer_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.hasFinishedInitialization)
                {
                    this.trendChartNeedsUpdating = true;
                    if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        DrawTrendChart(this.trendWinChartViewer);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDataViewer.Main_DataViewer_SizeChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void trendAutomaticDrawRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (trendAutomaticDrawRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    DrawTrendChart(this.trendWinChartViewer);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDataViewer.autoDrawRadRadioButton_ToggleStateChanged(object, Telerik.WinControls.UI.StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void windingHotSpotRadGroupBox_MouseClick(object sender, MouseEventArgs e)
        {

        }
    }

    /// <summary>
    /// This code was lifted from an online posting.  It puts up a bunch of junk when you click on a data point.
    /// It will probably be useful in the future if the message is customized.  I'm pretty sure it's sample code
    /// from the chartDirector people.
    /// </summary>
    public class ParamViewer : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Button OKPB;
        private System.Windows.Forms.ColumnHeader Key;
        private System.Windows.Forms.ColumnHeader Value;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ListView listView;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public ParamViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView = new System.Windows.Forms.ListView();
            this.OKPB = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.Key = new System.Windows.Forms.ColumnHeader();
            this.Value = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[]
            {
                this.Key,
                this.Value
            });
            this.listView.GridLines = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.Location = new System.Drawing.Point(8, 56);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(304, 168);
            this.listView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView.TabIndex = 0;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // OKPB
            // 
            this.OKPB.Location = new System.Drawing.Point(120, 232);
            this.OKPB.Name = "OKPB";
            this.OKPB.Size = new System.Drawing.Size(72, 24);
            this.OKPB.TabIndex = 1;
            this.OKPB.Text = "OK";
            this.OKPB.Click += new System.EventHandler(this.OKPB_Click);
            // 
            // label
            // 
            this.label.Location = new System.Drawing.Point(8, 8);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(304, 48);
            this.label.TabIndex = 2;
            this.label.Text = "This is to demonstrate that ChartDirector charts are clickable. In this demo prog" +
                              "ram, we just display the information provided to the ClickHotSpot event handler." +
                              " ";
            // 
            // Key
            // 
            this.Key.Text = "Key";
            this.Key.Width = 80;
            // 
            // Value
            // 
            this.Value.Text = "Value";
            this.Value.Width = 220;
            // 
            // ParamViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
            this.ClientSize = new System.Drawing.Size(320, 261);
            this.Controls.AddRange(new System.Windows.Forms.Control[]
            {
                this.label,
                this.OKPB,
                this.listView
            });
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ParamViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hot Spot Parameters";
            this.ResumeLayout(false);
        }
        
        #endregion
        
        /// <summary>
        /// ParamViewer Constructor
        /// </summary>
        public void Display(object sender, ChartDirector.WinHotSpotEventArgs e)
        {
            // Add the name of the ChartViewer control that is being clicked
            listView.Items.Add(new ListViewItem(new string[]
            {
                "source",
                ((ChartDirector.WinChartViewer)sender).Name
            }));
            
            // List out the parameters of the hot spot
            foreach (DictionaryEntry key in e.GetAttrValues())
            {
                listView.Items.Add(new ListViewItem(
                    new string[] { (string)key.Key, (string)key.Value }));
            }
            
            // Display the form
            ShowDialog();
        }
        
        /// <summary>
        /// Handler for the OK button
        /// </summary>
        private void OKPB_Click(object sender, System.EventArgs e)
        {
            // Just close the Form
            Close();
        }
    }
    
    public class MainMonitorData
    {
        public DateTime[] readingDateTime;
        public int[] state;
        public double[] humidity;
        public double[] moisture;
        public double[] loadCurrent1;
        public double[] loadCurrent2;
        public double[] loadCurrent3;
        public double[] temp1;
        public double[] temp2;
        public double[] temp3;
        public double[] temp4;
        public double[] voltage1;
        public double[] voltage2;
        public double[] voltage3;
        public double[] analog1;
        public double[] analog2;
        public double[] analog3;
        public double[] analog4;
        public double[] analog5;
        public double[] analog6;
        public double[] vibration1;
        public double[] vibration2;
        public double[] vibration3;
        public double[] vibration4;
        
        int index = 0;
        
        public void AllocateAllArrays(int numberOfPoints)
        {
            readingDateTime = new DateTime[numberOfPoints];
            humidity = new double[numberOfPoints];
            moisture = new double[numberOfPoints];
            loadCurrent1 = new double[numberOfPoints];
            loadCurrent2 = new double[numberOfPoints];
            loadCurrent3 = new double[numberOfPoints];
            temp1 = new double[numberOfPoints];
            temp2 = new double[numberOfPoints];
            temp3 = new double[numberOfPoints];
            temp4 = new double[numberOfPoints];
            voltage1 = new double[numberOfPoints];
            voltage2 = new double[numberOfPoints];
            voltage3 = new double[numberOfPoints];
            analog1 = new double[numberOfPoints];
            analog2 = new double[numberOfPoints];
            analog3 = new double[numberOfPoints];
            analog4 = new double[numberOfPoints];
            analog5 = new double[numberOfPoints];
            analog6 = new double[numberOfPoints];
            vibration1 = new double[numberOfPoints];
            vibration2 = new double[numberOfPoints];
            vibration3 = new double[numberOfPoints];
            vibration4 = new double[numberOfPoints];
            
            index = 0;
        }
        
        public void Add(Main_DataComponent_MonitorData monitorData)
        {
            try
            {
                if (monitorData != null)
                {
                    readingDateTime[index] = monitorData.ReadingDateTime;
                    
                    humidity[index] = monitorData.HumidityValue;
                    loadCurrent1[index] = monitorData.CurrentOneValue;
                    loadCurrent2[index] = monitorData.CurrentTwoValue;
                    loadCurrent3[index] = monitorData.CurrentThreeValue;
                    
                    temp1[index] = monitorData.TemperatureOneValue;
                    temp2[index] = monitorData.TemperatureTwoValue;
                    temp3[index] = monitorData.TemperatureThreeValue;
                    temp4[index] = monitorData.TemperatureFourValue;
                    
                    voltage1[index] = monitorData.VoltageOneValue;
                    voltage2[index] = monitorData.VoltageTwoValue;
                    voltage3[index] = monitorData.VoltageThreeValue;
                    
                    analog1[index] = monitorData.PressureOneValue;
                    analog2[index] = monitorData.PressureTwoValue;
                    analog3[index] = monitorData.PressureThreeValue;
                    analog4[index] = monitorData.PressureFourValue;
                    analog5[index] = monitorData.PressureFiveValue;
                    analog6[index] = monitorData.PressureSixValue;
                    
                    vibration1[index] = monitorData.VibrationOneValue;
                    vibration2[index] = monitorData.VibrationTwoValue;
                    vibration3[index] = monitorData.VibrationThreeValue;
                    vibration4[index] = monitorData.VibrationFourValue;
                    
                    moisture[index] = DynamicsCalculations.GetMoisture(temp1[index], humidity[index]);
                    
                    index++;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDynamicData.Add(Main_DataComponent_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
    }
    
    public class WindingHotSpotData
    {
        public DateTime[] readingDateTime;
        
        public double[] Whs1TempData;
        public double[] Whs2TempData;
        public double[] Whs3TempData;
        
        public double[] AgingFactor1Data;
        public double[] AgingFactor2Data;
        public double[] AgingFactor3Data;
        
        // these temps can be either low voltage or common voltage, depeding on transformer type
        public double[] AccumAge1Data;
        public double[] AccumAge2Data;
        public double[] AccumAge3Data;
        
        public double[] TopOilTempData;
        public double[] Fan1StartsData;
        public double[] Fan2StartsData;
        
        public double[] Fan1RunTimeData;
        public double[] Fan2RunTimeData;
        
        int index = 0;
        
        public void AllocateAllArrays(int numberOfPoints)
        {
            readingDateTime = new DateTime[numberOfPoints];
            
            Whs1TempData = new double[numberOfPoints];
            Whs2TempData = new double[numberOfPoints];
            Whs3TempData = new double[numberOfPoints];
            
            AgingFactor1Data = new double[numberOfPoints];
            AgingFactor2Data = new double[numberOfPoints];
            AgingFactor3Data = new double[numberOfPoints];
            
            AccumAge1Data = new double[numberOfPoints];
            AccumAge2Data = new double[numberOfPoints];
            AccumAge3Data = new double[numberOfPoints];
            
            TopOilTempData = new double[numberOfPoints];
            Fan1StartsData = new double[numberOfPoints];
            Fan2StartsData = new double[numberOfPoints];
            
            Fan1RunTimeData = new double[numberOfPoints];
            Fan2RunTimeData = new double[numberOfPoints];
            
            index = 0;
        }
        
        public void Add(Main_DataComponent_WindingHotSpotData windingHotSpotData, DateTime inputReadingDateTime, bool transformerTypeIsStandard)
        {
            try
            {
                if (windingHotSpotData != null)
                {
                    this.readingDateTime[index] = inputReadingDateTime;
                    
                    Whs1TempData[index] = windingHotSpotData.MaxWindingTempPhaseA;
                    Whs2TempData[index] = windingHotSpotData.MaxWindingTempPhaseB;
                    Whs3TempData[index] = windingHotSpotData.MaxWindingTempPhaseC;
                    
                    AgingFactor1Data[index] = windingHotSpotData.AgingFactorA;
                    AgingFactor2Data[index] = windingHotSpotData.AgingFactorB;
                    AgingFactor3Data[index] = windingHotSpotData.AgingFactorC;
                    AccumAge1Data[index] = windingHotSpotData.AccumulatedAgingA;
                    AccumAge2Data[index] = windingHotSpotData.AccumulatedAgingB;
                    AccumAge3Data[index] = windingHotSpotData.AccumulatedAgingC;

                                      
                    TopOilTempData[index] = windingHotSpotData.WhsTopOilTemp;
                    Fan1StartsData[index] = windingHotSpotData.WhsFan1Starts;
                    Fan2StartsData[index] = windingHotSpotData.WhsFan2Starts;
                    
                    Fan1RunTimeData[index] = windingHotSpotData.WhsFan1Hours;
                    
                    Fan2RunTimeData[index] = windingHotSpotData.WhsFan2Hours;
                   
                    index++;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDynamicData.Add(Main_DataComponent_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
    }
}