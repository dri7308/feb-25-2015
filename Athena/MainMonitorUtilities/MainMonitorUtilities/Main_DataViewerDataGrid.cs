﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Linq;
using System.Data.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using ChartDirector;
using GeneralUtilities;
using DataObjects;
using PasswordManagement;
using DatabaseInterface;

namespace MainMonitorUtilities
{
    public partial class Main_DataViewer : Telerik.WinControls.UI.RadForm
    {
       
        #region SQL Query Stuff

        private SqlConnection sqlDbConnection;
        private MonitorInterfaceDBDataSet1 monitorInterfaceDBDataSet;

        // Query Strings
        public static string dynamicsQueryString;
      
        // Data Tables
        private DataTable dynamicsDataTable;
       
        // Data Adapters
        private SqlDataAdapter dynamicsDataAdapter;
       
        // Command Builders
        private SqlCommandBuilder dynamicsCommandBuilder;
      
        // Binding sources
        private BindingSource dynamicsBindingSource;
       
        #endregion
 
        int[] dynamicsGridColumnWidths;

        string[] dynamicsGridColumnHeaderText;

        private void FillDynamicsDataTable()
        {
            try
            {
                this.dynamicsDataAdapter.Fill(this.dynamicsDataTable);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.FillDynamicsDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetGridViewBindingSources()
        {
            try
            {
                dynamicsRadGridView.DataSource = null;
                dynamicsRadGridView.Relations.Clear();
                dynamicsRadGridView.MasterTemplate.Templates.Clear();
                dynamicsRadGridView.DataSource = this.dynamicsBindingSource;
                SetAllGridViewColumnHeaderText();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetGridViewBindingSources()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeDataReadingsGridViewObject()
        {
            try
            {
                this.dynamicsRadGridView.AllowColumnReorder = false;
                this.dynamicsRadGridView.AllowColumnChooser = false;
                //  this.Main_DataReadingsRadGridView.ShowGroupPanel = false;
                this.dynamicsRadGridView.EnableGrouping = false;
                this.dynamicsRadGridView.AllowAddNewRow = false;
                //this.dynamicsRadGridView.AllowDeleteRow = false;
                this.dynamicsRadGridView.AutoScroll = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.InitializeParametersGridViewObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAllGridViewColumnHeaderText()
        {
            try
            {
                InitializeDynamicsGridViewColumnHeaderText();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetAllGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeAllGridViewColumnWidths()
        {
            try
            {
                InitializeDynamicsGridViewColumnWidths();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.InitializeAllGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Establishes a new connection to the database and gets all the current data.  This
        /// method will be called every time the database information associated with the 
        /// binding sources has been updated using a different database connection.
        /// </summary>
        private void SetNewSQLConnectionAndGetCurrentData()
        {
            try
            {
                if (this.sqlDbConnection != null)
                {
                    this.sqlDbConnection.Close();
                    this.sqlDbConnection = null;
                }

                this.sqlDbConnection = new SqlConnection(this.dbConnectionString);

                SetDynamicsQueryString();
             
                this.dynamicsDataAdapter = new SqlDataAdapter(dynamicsQueryString, this.sqlDbConnection);
            
                this.dynamicsCommandBuilder = new SqlCommandBuilder(dynamicsDataAdapter);
               
                this.dynamicsDataTable = new DataTable();
                FillDynamicsDataTable();
           
                dynamicsBindingSource = new BindingSource();
                dynamicsBindingSource.DataSource = dynamicsDataTable;             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetNewConnectionAndGetCurrentData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetDynamicsQueryString()
        {
            try
            {
                /// build the query strings, you need to do this here because the limiting times can change
                StringBuilder queryString = new StringBuilder();
                queryString.Append("SELECT ID, DataRootID, ReadingDateTime, State, ProperWorkingOrder, HumidityValue, TemperatureOneValue, TemperatureTwoValue, TemperatureThreeValue, TemperatureFourValue");
                queryString.Append(", CurrentOneValue, CurrentTwoValue, CurrentThreeValue, VoltageOneValue, VoltageTwoValue, VoltageThreeValue");
             
                queryString.Append(" FROM Main_Data_MonitorData WHERE MonitorID = '");
                queryString.Append(this.monitorID.ToString());
                queryString.Append("' AND ReadingDateTime > '");
                queryString.Append(this.dataCutoffDate.ToString());
                queryString.Append("' ORDER BY ReadingDateTime ASC");

                Main_DataViewer.dynamicsQueryString = queryString.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetParametersQueryString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllGridViewColumnHeaderText()
        {
            SetDynamicsGridViewColumnHeaderText();
        }

        private void InitializeDynamicsGridViewColumnHeaderText()
        {
            try
            {
                //pQueryString.Append("SELECT ID, DataRootID, ReadingDateTime, Tempfile, PDExists, AlarmEnable, Initial, Humidity, Temperature, CommonRegisters_2, CommonRegisters_3, CommonRegisters_4");
                //pQueryString.Append(", ExtLoadActive, ExtLoadReactive, CommonRegisters_0, CommonRegisters_1, CommonRegisters_5, CommonRegisters_6, CommonRegisters_7");

                //pQueryString.Append("SELECT ID, DataRootID, ReadingDateTime, Tempfile, PDExists, AlarmEnable, Initial, Humidity, CommonRegisters_2, Temperature, CommonRegisters_3, CommonRegisters_4");
                //pQueryString.Append(", CommonRegisters_0, CommonRegisters_1, CommonRegisters_5, CommonRegisters_6, CommonRegisters_7");

                int index = 0;

                this.dynamicsGridColumnHeaderText = new string[17];

                this.dynamicsGridColumnHeaderText[index++] = idDynamicsGridHeaderText;
                this.dynamicsGridColumnHeaderText[index++] = dataRootIdDynamicsGridHeaderText;
                this.dynamicsGridColumnHeaderText[index++] = readingDateDynamicsGridHeaderText;
                this.dynamicsGridColumnHeaderText[index++] = stateDynamicsGridHeaderText;
                this.dynamicsGridColumnHeaderText[index++] = properWorkingOrderDynamicsGridHeaderText;
                this.dynamicsGridColumnHeaderText[index++] = this.humidityRadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.temp1RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.temp2RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.temp3RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.temp4RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.loadCurrent1RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.loadCurrent2RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.loadCurrent3RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.voltage1RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.voltage2RadCheckBox.Text.Trim();
                this.dynamicsGridColumnHeaderText[index++] = this.voltage3RadCheckBox.Text.Trim();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.InitializeParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //private void SetAllGridViewColumnWidths()
        //{
        //    InitializeDynamicsGridViewColumnWidths();
        //}

        private void InitializeDynamicsGridViewColumnWidths()
        {
            try
            {
                this.dynamicsGridColumnWidths = new int[16];

                this.dynamicsGridColumnWidths[0] = 1;
                this.dynamicsGridColumnWidths[1] = 1;
                this.dynamicsGridColumnWidths[2] = 135;
                this.dynamicsGridColumnWidths[3] = 75;
                this.dynamicsGridColumnWidths[4] = 75;
                // at this point we go to Dynamics names which the user can vary in size
                this.dynamicsGridColumnWidths[5] = 100;
                this.dynamicsGridColumnWidths[6] = 100;
                this.dynamicsGridColumnWidths[7] = 100;
                this.dynamicsGridColumnWidths[8] = 100;
                this.dynamicsGridColumnWidths[9] = 100;
                this.dynamicsGridColumnWidths[10] = 100;
                this.dynamicsGridColumnWidths[11] = 100;
                this.dynamicsGridColumnWidths[12] = 100;
                this.dynamicsGridColumnWidths[13] = 110;
                this.dynamicsGridColumnWidths[14] = 100;
                this.dynamicsGridColumnWidths[15] = 100;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.InitializeDynamicsGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GridDataEnableEditing()
        {
            try
            {
                gridDataSaveChangesRadButton.Enabled = true;
                gridDataCancelChangesRadButton.Enabled = true;
                gridDataEditRadButton.Enabled = false;
                gridDataDeleteSelectedRadButton.Enabled = false;
                gridDataIsBeingEdited = true;
                dynamicsRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GridDataEnableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GridDataDisableEditing()
        {
            try
            {
                gridDataSaveChangesRadButton.Enabled = false;
                gridDataCancelChangesRadButton.Enabled = false;
                gridDataEditRadButton.Enabled = true;
                gridDataDeleteSelectedRadButton.Enabled = true;
                gridDataIsBeingEdited = false;
                dynamicsRadGridView.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDMDataViewer.GridDataDisableEditing()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetAllGridViewColumnWidths()
        {
            try
            {
                GetDynamicsGridViewColumnWidths();
     
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetAllGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetAllGridViewColumnWidths()
        {
            try
            {
                SetDynamicsGridViewColumnWidths();              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetAllGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void GetDynamicsGridViewColumnWidths()
        {
            try
            {
                int count;

                if (dynamicsRadGridView.Columns != null)
                {
                    count = dynamicsRadGridView.Columns.Count;
                    if (count > 0)
                    {
                        if ((this.dynamicsRadGridView == null) ||
                            (this.dynamicsGridColumnWidths.Count() != count))
                        {
                            this.dynamicsGridColumnWidths = new int[count];
                        }
                        if (count <= this.dynamicsRadGridView.Columns.Count())
                        {
                            for (int i = 0; i < count; i++)
                            {
                                this.dynamicsGridColumnWidths[i] =
                                    dynamicsRadGridView.Columns[i].Width;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        private void SetDynamicsGridViewColumnWidths()
        {
            try
            {
                int count;

                if (dynamicsRadGridView.Columns != null)
                {
                    count = dynamicsRadGridView.Columns.Count;
                    if (this.dynamicsGridColumnWidths != null)
                    {
                        if (this.dynamicsGridColumnWidths.Count() < count)
                        {
                            count = this.dynamicsGridColumnWidths.Count();
                        }

                        dynamicsRadGridView.Columns[0].AllowResize = true;
                        dynamicsRadGridView.Columns[1].AllowResize = true;
                        this.dynamicsRadGridView.Columns[0].MinWidth = 1;
                        this.dynamicsRadGridView.Columns[1].MinWidth = 1;
                        this.dynamicsRadGridView.Columns[0].Width = 1;
                        this.dynamicsRadGridView.Columns[1].Width = 1;
                        dynamicsRadGridView.Columns[0].AllowResize = false;
                        dynamicsRadGridView.Columns[1].AllowResize = false;
                        
                        for (int i = 2; i < count; i++)
                        {
                            dynamicsRadGridView.Columns[i].Width = this.dynamicsGridColumnWidths[i];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetParametersGridViewColumnWidths()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetDynamicsGridViewColumnHeaderText()
        {
            try
            {
                int count;
                if (dynamicsRadGridView.Columns != null)
                {
                    count = dynamicsRadGridView.Columns.Count;
                    if (this.dynamicsGridColumnHeaderText != null)
                    {
                        if (this.dynamicsGridColumnHeaderText.Count() < count)
                        {
                            count = this.dynamicsGridColumnHeaderText.Count();
                        }
                        dynamicsRadGridView.Columns[0].IsPinned = true;
                        
                        dynamicsRadGridView.Columns[0].AllowReorder = false;
                        dynamicsRadGridView.Columns[1].IsPinned = true;                       
                        dynamicsRadGridView.Columns[1].AllowReorder = false;
                        dynamicsRadGridView.Columns[2].IsPinned = true;
                        for (int i = 0; i < count; i++)
                        {
                            dynamicsRadGridView.Columns[i].DisableHTMLRendering = false;
                            dynamicsRadGridView.Columns[i].HeaderText = this.dynamicsGridColumnHeaderText[i];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.SetParametersGridViewColumnHeaderText()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Dictionary<Guid, int> GetAllSelectedGridEntries()
        {
            Dictionary<Guid, int> selectedEntries = null;
            try
            {
                selectedEntries = GetSelectedDynamicsEntries();
              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetAllSelectedGridEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }

//        private Dictionary<Guid, int> MergeDictionaries(Dictionary<Guid, int> dictOne, Dictionary<Guid, int> dictTwo)
//        {
//            Dictionary<Guid, int> selectedEntries = dictOne;
//            try
//            {
//                foreach (KeyValuePair<Guid, int> entry in dictTwo)
//                {
//                    if (!selectedEntries.ContainsKey(entry.Key))
//                    {
//                        selectedEntries.Add(entry.Key, entry.Value);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_DataViewer.MergeDictionaries(Dictionary<Guid, int>, Dictionary<Guid, int>)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return selectedEntries;
//        }

        private Dictionary<Guid, int> GetSelectedDynamicsEntries()
        {
            Dictionary<Guid, int> selectedEntries = new Dictionary<Guid, int>();
            try
            {
                Guid dataRootID;
                int rowCount = this.dynamicsRadGridView.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    if (this.dynamicsRadGridView.Rows[i].IsSelected)
                    {
                        dataRootID = Guid.Parse(this.dynamicsRadGridView.Rows[i].Cells[1].Value.ToString());
                        if (!selectedEntries.ContainsKey(dataRootID))
                        {
                            selectedEntries.Add(dataRootID, 1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_DataViewer.GetSelectedParametersEntries()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEntries;
        }


    }
}
