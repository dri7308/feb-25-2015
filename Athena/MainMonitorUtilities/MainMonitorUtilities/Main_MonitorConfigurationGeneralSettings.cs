﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;

namespace MainMonitorUtilities
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string  formatErrorInIPAddressText = "Format error in IP address.  Must be of the form 000.000.000.000.";
        private static string  digitErrorInIPAddressText = "Error in digit selection for IP address, the digits need to be between 0 and 255.";

        private void SelectGeneralSettingsTab()
        {
            configurationRadPageView.SelectedPage = generalSettingsRadPageViewPage;
        }

        private void DisableGeneralSettingsTabConfigurationEditObjects()
        {
            this.enableMonitoringRadRadioButton.Enabled = false;
            this.disableMonitoringRadRadioButton.Enabled = false;
            this.frequencyRadSpinEditor.ReadOnly = true;
            this.frequencyRadSpinEditor.ShowUpDownButtons = false;
            this.firmwareVersionRadTextBox.ReadOnly = true;
            this.objectNameRadTextBox.ReadOnly = true;
            this.modbusAddressRadSpinEditor.ReadOnly = true;
            this.modbusAddressRadSpinEditor.ShowUpDownButtons = false;
            this.serialBaudRateRadDropDownList.Enabled = false;
            this.ethernetBaudRateRadDropDownList.Enabled = false;
            this.ethernetProtocolRadDropDownList.Enabled = false;
            this.ipAddressRadTextBox.ReadOnly = true;
            this.relayAlarmRadDropDownList.Enabled = false;
            this.relayWarningRadDropDownList.Enabled = false;
            this.modeSaveIntervalRadSpinEditor.ReadOnly = true;
            this.modeSaveIntervalRadSpinEditor.ShowUpDownButtons = false;
            this.useDiagnosticResultsRadCheckBox.Enabled = false;
            this.allowDirectAccessRadCheckBox.Enabled = false;
        }

        private void EnableGeneralSettingsTabConfigurationEditObjects()
        {
            this.enableMonitoringRadRadioButton.Enabled = true;
            this.disableMonitoringRadRadioButton.Enabled = true;
            this.frequencyRadSpinEditor.ReadOnly = false;
            this.frequencyRadSpinEditor.ShowUpDownButtons = true;
            this.firmwareVersionRadTextBox.ReadOnly = false;
            this.objectNameRadTextBox.ReadOnly = false;
            this.modbusAddressRadSpinEditor.ReadOnly = false;
            this.modbusAddressRadSpinEditor.ShowUpDownButtons = true;
            this.serialBaudRateRadDropDownList.Enabled = true;
            this.ethernetBaudRateRadDropDownList.Enabled = true;
            this.ethernetProtocolRadDropDownList.Enabled = true;
            this.ipAddressRadTextBox.ReadOnly = false;
            this.relayAlarmRadDropDownList.Enabled = true;
            this.relayWarningRadDropDownList.Enabled = true;
            this.modeSaveIntervalRadSpinEditor.ReadOnly = false;
            this.modeSaveIntervalRadSpinEditor.ShowUpDownButtons = true;
            this.useDiagnosticResultsRadCheckBox.Enabled = true;
            this.allowDirectAccessRadCheckBox.Enabled = true;
        }

        private void SetGeneralSettingsTabValuesFromConfiguration(Main_Configuration inputConfiguration)
        {
            try
            {
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.deviceSetupConfigObject != null)
                    {
                        if (inputConfiguration.generalConfigObject != null)
                        {
                            if (inputConfiguration.communicationSetupConfigObject != null)
                            {
                                int monitoring = inputConfiguration.deviceSetupConfigObject.Monitoring;
                                int frequency = inputConfiguration.deviceSetupConfigObject.Frequency;
                                double boardVersion = inputConfiguration.generalConfigObject.FirmwareVersion;
                                string boardName = Main_Configuration.GetBoardNameFromRegisterValues(inputConfiguration.generalConfigObject);
                                int modBusAddress = inputConfiguration.communicationSetupConfigObject.ModBusAddress;
                                int serialBaudRate = inputConfiguration.communicationSetupConfigObject.BaudRateRS485;
                                int ethernetBaudRate = inputConfiguration.communicationSetupConfigObject.BaudRateXport;
                                int ethernetProtocol = inputConfiguration.communicationSetupConfigObject.ModBusProtocolOverXport;
                                string ipAddress;
                                int ip1 = inputConfiguration.communicationSetupConfigObject.IP1;
                                int ip2 = inputConfiguration.communicationSetupConfigObject.IP2;
                                int ip3 = inputConfiguration.communicationSetupConfigObject.IP3;
                                int ip4 = inputConfiguration.communicationSetupConfigObject.IP4;
                                int relayAlarmSetting = inputConfiguration.deviceSetupConfigObject.AlarmControl;
                                int relayWarningSetting = inputConfiguration.deviceSetupConfigObject.WarningControl;
                                int modeSaveStep = inputConfiguration.communicationSetupConfigObject.SaveDataIntervalInSeconds / 60;
                                int modeSaveDiagnostics = inputConfiguration.communicationSetupConfigObject.SaveDataIfChangedStatus;
                                int enableCurrentSignatureAnalysis;
                                int allowDirectAccess = inputConfiguration.deviceSetupConfigObject.AllowDirectAccess;

                                if (monitoring == 1)
                                {
                                    this.enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }
                                else
                                {
                                    this.disableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }

                                this.frequencyRadSpinEditor.Value = frequency;

                                this.firmwareVersionRadTextBox.Text = Math.Round(boardVersion, 2).ToString();

                                this.objectNameRadTextBox.Text = boardName;

                                this.modbusAddressRadSpinEditor.Value = modBusAddress;

                                this.serialBaudRateRadDropDownList.Text = GetFullBaudRateFromShortenedBaudRate(serialBaudRate).ToString();

                                this.ethernetBaudRateRadDropDownList.Text = GetFullBaudRateFromShortenedBaudRate(ethernetBaudRate).ToString();

                                if (ethernetProtocol == 0)
                                {
                                    this.ethernetProtocolRadDropDownList.Text = "RTU";
                                }
                                else
                                {
                                    this.ethernetProtocolRadDropDownList.Text = "TCP";
                                }
                                this.ethernetProtocolRadDropDownList.SelectedIndex = ethernetProtocol;

                                ipAddress = ip1.ToString() + "." + ip2.ToString() + "." + ip3.ToString() + "." + ip4.ToString();

                                this.ipAddressRadTextBox.Text = ipAddress;

                                this.relayAlarmRadDropDownList.SelectedIndex = relayAlarmSetting;

                                this.relayWarningRadDropDownList.SelectedIndex = relayWarningSetting;

                                this.modeSaveIntervalRadSpinEditor.Value = modeSaveStep;

                                if (modeSaveDiagnostics == 1)
                                {
                                    this.useDiagnosticResultsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }
                                else
                                {
                                    this.useDiagnosticResultsRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                                }

                                if (allowDirectAccess == 1)
                                {
                                    this.allowDirectAccessRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                                }
                                else
                                {
                                    this.allowDirectAccessRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                                }
                            }
                            else
                            {
                                string errorMessage = "Errpr in Main_MonitorConfiguration.SetGeneralSettingsTabValuesFromConfiguration()\nthis.inputConfiguration.communicationSetupConfigObject was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Errpr in Main_MonitorConfiguration.SetGeneralSettingsTabValuesFromConfiguration()\nthis.inputConfiguration.generalConfigObject was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Errpr in Main_MonitorConfiguration.SetGeneralSettingsTabValuesFromConfiguration()\nthis.inputConfiguration.deviceSetupConfigObject was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Errpr in Main_MonitorConfiguration.SetGeneralSettingsTabValuesFromConfiguration()\nthis.inputConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetGeneralSettingsTabValuesFromConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool ErrorIsPresentInAGeneralSettingsTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                string ipAddress;
                string[] ipAddressPieces;
                int ip1 = 0;
                int ip2 = 0;
                int ip3 = 0;
                int ip4 = 0;
                /// I think the only error checking I can do is for the IP address
                ipAddress = this.ipAddressRadTextBox.Text;
                ipAddressPieces = ipAddress.Split('.');
                if (ipAddressPieces.Length == 4)
                {
                    if (!Int32.TryParse(ipAddressPieces[0], out ip1))
                    {
                        errorIsPresent = true;
                    }
                    if (!Int32.TryParse(ipAddressPieces[1], out ip2))
                    {
                        errorIsPresent = true;
                    }
                    if (!Int32.TryParse(ipAddressPieces[2], out ip3))
                    {
                        errorIsPresent = true;
                    }
                    if (!Int32.TryParse(ipAddressPieces[3], out ip4))
                    {
                        errorIsPresent = true;
                    }
                }
                else
                {
                    errorIsPresent = true;
                }
                if (errorIsPresent)
                {
                    SelectGeneralSettingsTab();
                    this.ipAddressRadTextBox.Select();
                    RadMessageBox.SetThemeName("office2007BlackTheme");
                    RadMessageBox.Show(this, formatErrorInIPAddressText);
                }
                else
                {
                    if ((ip1 < 1) || (ip1 > 255))
                    {
                        errorIsPresent = true;
                    }
                    if ((ip2 < 0) || (ip1 > 255))
                    {
                        errorIsPresent = true;
                    }
                    if ((ip3 < 0) || (ip1 > 255))
                    {
                        errorIsPresent = true;
                    }
                    if ((ip4 < 0) || (ip1 > 255))
                    {
                        errorIsPresent = true;
                    }
                    if (errorIsPresent)
                    {
                        RadMessageBox.SetThemeName("office2007BlackTheme");
                        RadMessageBox.Show(this, digitErrorInIPAddressText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ErrorIsPresentInAGeneralSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void WriteGeneralSettingsTabValuesToCurrentConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.deviceSetupConfigObject != null)
                    {
                        if (this.workingConfiguration.communicationSetupConfigObject != null)
                        {
                            int monitoring = 0;
                            int frequency;
                            double firmwareVersion;
                            string boardName;
                            int modBusAddress;
                            int serialBaudRate;
                            int ethernetBaudRate;
                            int ethernetProtocol;
                            string ipAddress;
                            string[] ipAddressPieces;
                            int ip1 = 0;
                            int ip2 = 0;
                            int ip3 = 0;
                            int ip4 = 0;
                            int relayAlarmSetting;
                            int relayWarningSetting;
                            int modeSaveStep;
                            int modeSaveDiagnostics = 0;
                            // int enableCurrentSignatureAnalysis = 0;
                            int allowDirectAccess = 0;

                            if (enableMonitoringRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                monitoring = 1;
                            }
                            else
                            {
                                monitoring = 0;
                            }

                            frequency = (Int32)this.frequencyRadSpinEditor.Value;
                            Double.TryParse(this.firmwareVersionRadTextBox.Text, out firmwareVersion);

                            boardName = this.objectNameRadTextBox.Text;

                            modBusAddress = (Int32)this.modbusAddressRadSpinEditor.Value;

                            serialBaudRate = GetShortendBaudRateFromFullBaudRate(Int32.Parse(this.serialBaudRateRadDropDownList.Text));

                            ethernetBaudRate = GetShortendBaudRateFromFullBaudRate(Int32.Parse(this.ethernetBaudRateRadDropDownList.Text));

                            if (this.ethernetProtocolRadDropDownList.Text.CompareTo("RTU")==0)
                            {
                                ethernetProtocol = 0;
                            }
                            else
                            {
                                ethernetProtocol = 1;
                            }

                            ipAddress = this.ipAddressRadTextBox.Text;
                            ipAddressPieces = ipAddress.Split('.');

                            Int32.TryParse(ipAddressPieces[0], out ip1);
                            Int32.TryParse(ipAddressPieces[1], out ip2);
                            Int32.TryParse(ipAddressPieces[2], out ip3);
                            Int32.TryParse(ipAddressPieces[3], out ip4);

                            relayAlarmSetting = relayAlarmRadDropDownList.SelectedIndex;
                            relayWarningSetting = relayWarningRadDropDownList.SelectedIndex;

                            modeSaveStep = (Int32)modeSaveIntervalRadSpinEditor.Value;

                            if (useDiagnosticResultsRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                modeSaveDiagnostics = 1;
                            }
                            else
                            {
                                modeSaveDiagnostics = 0;
                            }

                            //if (enableCurrentSignatureAnalysisRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            //{
                            //    enableCurrentSignatureAnalysis = 1;
                            //}
                            //else
                            //{
                            //    enableCurrentSignatureAnalysis = 0;
                            //}

                            if (allowDirectAccessRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                            {
                                allowDirectAccess = 1;
                            }
                            else
                            {
                                allowDirectAccess = 0;
                            }

                            ConvertBoardNameToRegisterValues(boardName);

                            this.workingConfiguration.deviceSetupConfigObject.Monitoring = monitoring;
                            this.workingConfiguration.deviceSetupConfigObject.Frequency = frequency;
                            this.workingConfiguration.communicationSetupConfigObject.ModBusAddress = modBusAddress;
                            this.workingConfiguration.communicationSetupConfigObject.BaudRateRS485 = serialBaudRate;
                            this.workingConfiguration.communicationSetupConfigObject.BaudRateXport = ethernetBaudRate;
                            this.workingConfiguration.communicationSetupConfigObject.ModBusProtocolOverXport = ethernetProtocol;

                            this.workingConfiguration.communicationSetupConfigObject.IP1 = ip1;
                            this.workingConfiguration.communicationSetupConfigObject.IP2 = ip2;
                            this.workingConfiguration.communicationSetupConfigObject.IP3 = ip3;
                            this.workingConfiguration.communicationSetupConfigObject.IP4 = ip4;

                            this.workingConfiguration.deviceSetupConfigObject.AlarmControl = relayAlarmSetting;
                            this.workingConfiguration.deviceSetupConfigObject.WarningControl = relayWarningSetting;
                            this.workingConfiguration.communicationSetupConfigObject.SaveDataIntervalInSeconds = modeSaveStep * 60;
                            this.workingConfiguration.communicationSetupConfigObject.SaveDataIfChangedStatus = modeSaveDiagnostics;
                            this.workingConfiguration.deviceSetupConfigObject.AllowDirectAccess = 1;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Monitorthis.workingConfiguration.WriteGeneralSettingsTabValuesToCurrentConfiguration.currentConfigurationValues()\nInput Main_this.workingConfiguration.communicationSetupConfigObject was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Monitorthis.workingConfiguration.WriteGeneralSettingsTabValuesToCurrentConfiguration.currentConfigurationValues()\nInput Main_this.workingConfiguration.deviceSetupConfigObject was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Monitorthis.workingConfiguration.WriteGeneralSettingsTabValuesToCurrentConfiguration.currentConfigurationValues()\nInput Main_this.workingConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Monitorthis.workingConfiguration.WriteGeneralSettingsTabValuesToCurrentConfiguration.currentConfigurationValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ConvertBoardNameToRegisterValues(string boardName)
        {
            try
            {
                byte[] twoBytes = new byte[2];
                StringBuilder boardNameStringBuilder = new StringBuilder(boardName.Trim());
                boardNameStringBuilder.Append("                                   ");

                boardName = boardNameStringBuilder.ToString();

                twoBytes[0] = (byte)((int)boardName[0]);
                twoBytes[1] = (byte)((int)boardName[1]);
                this.workingConfiguration.generalConfigObject.UnitName_0 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[2]);
                twoBytes[1] = (byte)((int)boardName[3]);
                this.workingConfiguration.generalConfigObject.UnitName_1 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[4]);
                twoBytes[1] = (byte)((int)boardName[5]);
                this.workingConfiguration.generalConfigObject.UnitName_2 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[6]);
                twoBytes[1] = (byte)((int)boardName[7]);
                this.workingConfiguration.generalConfigObject.UnitName_3 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[8]);
                twoBytes[1] = (byte)((int)boardName[9]);
                this.workingConfiguration.generalConfigObject.UnitName_4 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[10]);
                twoBytes[1] = (byte)((int)boardName[11]);
                this.workingConfiguration.generalConfigObject.UnitName_5 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[12]);
                twoBytes[1] = (byte)((int)boardName[13]);
                this.workingConfiguration.generalConfigObject.UnitName_6 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[14]);
                twoBytes[1] = (byte)((int)boardName[15]);
                this.workingConfiguration.generalConfigObject.UnitName_7 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[16]);
                twoBytes[1] = (byte)((int)boardName[17]);
                this.workingConfiguration.generalConfigObject.UnitName_8 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[18]);
                twoBytes[1] = (byte)((int)boardName[19]);
                this.workingConfiguration.generalConfigObject.UnitName_9 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[20]);
                twoBytes[1] = (byte)((int)boardName[21]);
                this.workingConfiguration.generalConfigObject.UnitName_10 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[22]);
                twoBytes[1] = (byte)((int)boardName[23]);
                this.workingConfiguration.generalConfigObject.UnitName_11 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[24]);
                twoBytes[1] = (byte)((int)boardName[25]);
                this.workingConfiguration.generalConfigObject.UnitName_12 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[26]);
                twoBytes[1] = (byte)((int)boardName[27]);
                this.workingConfiguration.generalConfigObject.UnitName_13 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[28]);
                twoBytes[1] = (byte)((int)boardName[29]);
                this.workingConfiguration.generalConfigObject.UnitName_14 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[30]);
                twoBytes[1] = (byte)((int)boardName[31]);
                this.workingConfiguration.generalConfigObject.UnitName_15 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[32]);
                twoBytes[1] = (byte)((int)boardName[33]);
                this.workingConfiguration.generalConfigObject.UnitName_16 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[34]);
                twoBytes[1] = (byte)((int)boardName[35]);
                this.workingConfiguration.generalConfigObject.UnitName_17 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[36]);
                twoBytes[1] = (byte)((int)boardName[37]);
                this.workingConfiguration.generalConfigObject.UnitName_18 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[38]);
                twoBytes[1] = (byte)((int)boardName[39]);
                // this.workingConfiguration.generalConfigObject.UnitName_19 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                this.workingConfiguration.generalConfigObject.UnitName_19 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertBoardNameToRegisterValues(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private int GetBaudRateFromSelectedIndex(int selectedIndex)
        {
            int internalBaudRate = 0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        internalBaudRate = 96;
                        break;
                    case 1:
                        internalBaudRate = 384;
                        break;
                    case 2:
                        internalBaudRate = 576;
                        break;
                    case 3:
                        internalBaudRate = 1152;
                        break;
                    case 4:
                        internalBaudRate = 2304;
                        break;
                    case 5:
                        internalBaudRate = 5000;
                        break;
                    case 6:
                        internalBaudRate = 10000;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetBaudRateFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return internalBaudRate;
        }

        private int GetSelectedIndexFromRegisterBaudRate(int registerBaudRate)
        {
            int selectedIndex = 0;
            try
            {
                switch (registerBaudRate)
                {
                    case 96:
                        selectedIndex = 0;
                        break;
                    case 384:
                        selectedIndex = 1;
                        break;
                    case 576:
                        selectedIndex = 2;
                        break;
                    case 1152:
                        selectedIndex = 3;
                        break;
                    case 2304:
                        selectedIndex = 4;
                        break;
                    case 5000:
                        selectedIndex = 5;
                        break;
                    case 10000:
                        selectedIndex = 6;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSelectedIndexFromRegisterBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }
    }
}
