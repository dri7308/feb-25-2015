﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using PasswordManagement;
using DatabaseInterface;
using MonitorCommunication;
using FormatConversion;
using MonitorUtilities;
using System.Threading;

namespace MainMonitorUtilities
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string currentConfigName = "Current Device Configuration";
       

        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }

        private static string htmlPrefix = "<html>";
        private static string htmlSuffix = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        //private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";

        //  private static string noCurrentConfigurationDefinedText = "No current configuration has been saved for this monitor";

        private static string mainMonitorWhsConfigurationTitleText = "Main Monitor WHS Configuration";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText = "Main Monitor WHS Configuration - Displaying Configuration from Database";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText = "Main Monitor WHS Configuration - Displaying Configuration from Device";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText = "Main Monitor WHS Configuration - Displaying Working Copy of Configuration from Device";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText = "Main Monitor WHS Configuration - Displaying Working Copy of Configuration from Database";
        //private static string mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText = "Main Monitor WHS Configuration - Displaying Working Copy of Configuration from Initialization";

        private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Working Copy<br>of Configuration<br>to Database</html>";
        //  private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device<br>with Working Copy<br>of Configuration</html>";
        private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
        //  private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
        // private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        //private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        //private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        //private static string workingConfigurationRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        //private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        //private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";
        //private static string initializeWorkingConfigurationRadButtonText = "Initialize Working Configuration";

        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        private static string failedToUploadConfigurationText = "Failed to upload the configuration for some reason.";
        //private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
        // private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";
        //   private static string currentConfigurationNotSavedWarningText = "You have made changes to the working copy that have not been saved.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";

        //private static string couldNotFindCurrentConfigurationDisplayingMostRecentText = "Could not find the current device configuration, displaying most recent configuration saved.";
        // private static string couldNotFindAnyDeviceConfigurationText = "Could not find any device configurations in the database, displaying intialized working configuration.";
        private static string couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = "Could not find the current device configuration, displaying an empty configuration.";

        //  private static string youWillLoseAllChangesToTheWorkingConfigurationText = "You will lose any changes you made to the working configuration - continue?";

        private static string previousAgingValueChangedSuccessfullyText = "Successfully changed the value for previous aging";
        private static string failedToChangeValueOfPreviousAgingText = "Failed to change the value for previous aging";

        private static string deviceInteractionGroupBoxText = "Device Interaction";
        private static string databaseInteractionGroupBoxText = "Database Interaction";

        private static string noTemplateConfigurationsAvailable = "No template configurations are available";

        private static string deleteSelectedConfigurationRadButtonText = "<html>Delete Selected<br>Configuration from<br>Database</html>";

        private List<Main_Config_WHSConfigurationRoot> availableConfigurations;
        private List<Main_Config_WHSConfigurationRoot> templateConfigurations;

        private Main_WHSConfiguration workingConfiguration;
        private Main_WHSConfiguration uneditedWorkingConfiguration;
        //private Main_WHSConfiguration configurationFromDevice;
        //private Main_WHSConfiguration configurationFromDatabase;

        private int availableConfigurationsSelectedIndex;
        private int templateConfigurationsSelectedIndex;

        // private ConfigurationDisplayed configurationBeingDisplayed = ConfigurationDisplayed.None;

        private Monitor monitor;
        private string serialPort;
        private int baudRate;
        private bool downloadInProgress;
        private string dbConnectionString;
        private string templateDbConnectionString;

        private string nameOfLastConfigurationLoadedFromDatabase = string.Empty;

        private int readDelayInMicroseconds;

        private int[] ratingsBitEncodingAsIntegerArray;

        private bool inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;

        //private bool currentlySettingFromDatabaseRadRadioButtonState=false;
        //private bool currentlySettingFromDeviceRadRadioButtonState=false;
        //private bool currentlySettingWorkingConfigurationRadRadioButtonState = false;
        //private bool currentlyInitializingWorkingConfiguration = false;

        private ProgramBrand programBrand;
        private static ProgramType programType;

        public Main_WHSMonitorConfiguration(ProgramBrand inputProgramBrand, ProgramType inputProgramType, Monitor inputMonitor, string inputSerialPort, int inputBaudRate, bool inputDownloadInProgress, string inputDbConnectionString, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();

                AssignStringValuesToInterfaceObjects();

                ratingsBitEncodingAsIntegerArray = new int[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                this.programBrand = inputProgramBrand;
                Main_WHSMonitorConfiguration.programType = inputProgramType;
                this.monitor = inputMonitor;
                this.serialPort = inputSerialPort;
                this.baudRate = inputBaudRate;
                this.downloadInProgress = inputDownloadInProgress;
                this.dbConnectionString = inputDbConnectionString;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration(Monitor, string, int, bool, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Main_WHSMonitorConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                this.UseWaitCursor = true;
                RadMessageBox.ThemeName = "Office2007Black";
                //disable Program Device button
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = false;
                programDeviceAlarmsAndFansTabRadButton.Enabled = false;
                //DisableWorkingConfigurationRadRadioButtons();
                //DisableFromDatabaseConfigurationRadRadioButtons();
                //DisableFromDeviceConfigurationRadRadioButtons();
                DisableProgressBars();

                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = true;

                // hide calculation tab - not being used
                configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;
                this.calculationSettingsRadPageViewPage.Item.Visibility = ElementVisibility.Collapsed;

                InitializeWhsGeneralSettingsTabObjects();
                //InitializeCalculationSettingsTabObjects();
                InitializeAlarmsAndFansTabObjects();

                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    LoadAvailableConfigurations(localDB);
                    if (this.monitor != null)
                    {
                        this.workingConfiguration = LoadCurrentDeviceConfigurationFromDatabase(this.monitor.ID, localDB);
                        if (this.workingConfiguration != null)
                        {
                            this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                            //EnableCopyDatabaseConfigurationRadButtons();
                            //EnableFromDatabaseRadRadioButtons();
                            //SetFromDatabaseRadRadioButtonState();
                        }
                        else
                        {
                            if (Main_WHSMonitorConfiguration.programType != ProgramType.TemplateEditor)
                            {
                                RadMessageBox.Show(this, couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText);
                            }
                            this.workingConfiguration = new Main_WHSConfiguration();
                            this.workingConfiguration.InitializeConfigurationToDefaults();
                            this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
                        }
                    }
                }

                if (Main_WHSMonitorConfiguration.programType != ProgramType.TemplateEditor)
                {
                    using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                    {
                        LoadTemplateConfigurations(templateDB);
                    }
                }

                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;

                // DisplayMostRecentConfigurationInTheDatabase();

                // this.objectNameRadTextBox.ReadOnly = true; 

                configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;

                SetAllRadDropDownListsToReadOnly();

                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;

                if (Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor)
                {
                    templateConfigurationsGeneralSettingsTabRadGroupBox.Visible = false;
                    templateConfigurationsCalculationSettingsTabRadGroupBox.Visible = false;
                    templateConfigurationsAlarmsAndFansTabRadGroupBox.Visible = false;

                    deviceInteractionWhsGeneralSettingsTabRadGroupBox.Visible = false;
                    deviceInteractionCalculationSettingsTabRadGroupBox.Visible = false;
                    deviceInteractionAlarmsAndFansTabRadGroupBox.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void Main_WHSMonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                WriteAllInterfaceDataToWorkingConfiguration();

                if (this.workingConfiguration != null)
                {
                    if (!this.workingConfiguration.ConfigurationIsTheSame(this.uneditedWorkingConfiguration))
                    {
                        if (RadMessageBox.Show(this, changesToWorkingConfigurationNotSavedExitWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.Main_WHSMonitorConfiguration_FormClosing(object, FormClosingEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetAllRadDropDownListsToReadOnly()
        {
            try
            {
                this.transformerConfigurationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.transformerTypeRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.agingCalculationMethodRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                this.algorithmVariantRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetAllRadDropDownListsToReadOnly()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = mainMonitorWhsConfigurationTitleText;
                AssignStringValuesToWhsGeneralSettingsInterfaceObjects();
               
                AssignStringValuesToAlarmsAndFansInterfaceObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");
                mainMonitorWhsConfigurationTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationMainMonitorWHSConfigurationTitleText", mainMonitorWhsConfigurationTitleText, "", "", "");
                configurationBeingSavedIsFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationConfigurationBeingSavedIsFromDeviceText", configurationBeingSavedIsFromDeviceText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeviceConfigurationSavedToDatabaseText", deviceConfigurationSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeviceConfigurationNotSavedToDatabaseText", deviceConfigurationNotSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
                configurationDeleteFromDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationConfigurationDeleteFromDatabaseWarningText", configurationDeleteFromDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
                cannotDeleteCurrentConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationCannotDeleteCurrentConfigurationWarningText", cannotDeleteCurrentConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");
                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationNoConfigurationSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");
                notConnectedToMainMonitorWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationNotConnectedToMainMonitorWarningText", notConnectedToMainMonitorWarningText, htmlFontType, htmlStandardFontSize, "");
                serialPortNotSetWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationSerialPortNotSetWarningText", serialPortNotSetWarningText, htmlFontType, htmlStandardFontSize, "");
                failedToOpenMonitorConnectionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationFailedToOpenMonitorConnectionText", failedToOpenMonitorConnectionText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotProperlyConfiguredText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeviceCommunicationNotProperlyConfiguredText", deviceCommunicationNotProperlyConfiguredText, htmlFontType, htmlStandardFontSize, "");
                downloadWasInProgressWhenInterfaceWasOpenedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDownloadWasInProgressWhenInterfaceWasOpenedText", downloadWasInProgressWhenInterfaceWasOpenedText, htmlFontType, htmlStandardFontSize, "");
                deviceConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeviceConfigurationDoesNotMatchDatabaseConfigurationText", deviceConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
                deviceCommunicationNotSavedInDatabaseYetText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeviceCommunicationNotSavedInDatabaseYetText", deviceCommunicationNotSavedInDatabaseYetText, htmlFontType, htmlStandardFontSize, "");
                saveDeviceConfigurationQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationSaveDeviceConfigurationQuestionText", saveDeviceConfigurationQuestionText, htmlFontType, htmlStandardFontSize, "");
                workingConfigurationNotDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationWorkingConfigurationNotDefinedText", workingConfigurationNotDefinedText, htmlFontType, htmlStandardFontSize, "");
                availableConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceAvailableConfigurationsRadGroupBoxText", availableConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                saveCurrentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceSaveCurrentConfigurationRadButtonText", saveCurrentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceLoadConfigFromDatabaseRadButtonText", loadConfigurationFromDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                programDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceProgramDeviceRadButtonText", programDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                loadConfigurationFromDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceLoadConfigurationFromDeviceRadButtonText", loadConfigurationFromDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
                uploadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationUploadingConfigurationText", uploadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                downloadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDownloadingConfigurationText", downloadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
                failedToUploadConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationFailedToUploadConfigurationText", failedToUploadConfigurationText, htmlFontType, htmlStandardFontSize, "");
                exitWithoutSavingQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationExitWithoutSavingQuestionText", exitWithoutSavingQuestionText, htmlFontType, htmlStandardFontSize, "");
                enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceGeneralSettingsTabMonitoringEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceGeneralSettingsTabMonitoringDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                configurationWasIncompleteInDatabaseAndHasBeenRemovedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceConfigurationWasIncompleteInDatabaseAndHasBeenRemovedText", configurationWasIncompleteInDatabaseAndHasBeenRemovedText, htmlFontType, htmlStandardFontSize, "");
                previousAgingValueChangedSuccessfullyText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfacePreviousAgingValueChangedSuccessfullyText", previousAgingValueChangedSuccessfullyText, htmlFontType, htmlStandardFontSize, "");
                failedToChangeValueOfPreviousAgingText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceFailedToChangeValueOfPreviousAgingText", failedToChangeValueOfPreviousAgingText, htmlFontType, htmlStandardFontSize, "");
                workingConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationWorkingConfigurationSavedToDatabaseText", workingConfigurationSavedToDatabaseText, "", "", "");
                workingConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationWorkingConfigurationNotSavedToDatabaseText", workingConfigurationNotSavedToDatabaseText, "", "", "");
                configurationWasReadFromTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationConfigurationWasReadFromTheDatabaseText", configurationWasReadFromTheDatabaseText, "", "", "");
                changesToWorkingConfigurationNotSavedOverwriteWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationChangesToWorkingConfigurationNotSavedOverwriteWarningText", changesToWorkingConfigurationNotSavedOverwriteWarningText, "", "", "");
                changesToWorkingConfigurationNotSavedExitWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationChangesToWorkingConfigurationNotSavedExitWarningText", changesToWorkingConfigurationNotSavedExitWarningText, "", "", "");
                discardChangesAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDiscardChangesAsQuestionText", discardChangesAsQuestionText, "", "", "");
                couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationCouldNotFindWorkingConfigurationDisplayingEmptyConfigurationText", couldNotFindWorkingConfigurationDisplayingEmptyConfigurationText, htmlFontType, htmlStandardFontSize, "");
                deviceInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceDeviceInteractionGroupBoxText", deviceInteractionGroupBoxText, "", "", "");
                databaseInteractionGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationInterfaceDatabaseInteractionGroupBoxText", databaseInteractionGroupBoxText, "", "", "");
                configurationLoadCancelledText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationConfigurationLoadCancelledText", configurationLoadCancelledText, htmlFontType, htmlStandardFontSize, "");
                failedToDeleteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationFailedToDeleteCurrentConfigurationText", failedToDeleteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");
                noTemplateConfigurationsAvailable = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationNoTemplateConfigurationsAvailable", noTemplateConfigurationsAvailable, "", "", "");
                failedtoDeleteConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationFailedToDeleteConfigurationText", failedtoDeleteConfigurationText, htmlFontType, htmlStandardFontSize, "");
                replaceExistingConfigurationsWithTheSameNameQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationReplaceExistingConfigurationsWithTheSameNameQuestionText", replaceExistingConfigurationsWithTheSameNameQuestionText, htmlFontType, htmlStandardFontSize, "");
                deleteSelectedConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorWhsConfigurationDeleteSelectedConfigurationRadButtonText", deleteSelectedConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
                AssignValuesToWhsGeneralSettingsInternalStaticStrings();
                
                AssignValuesToAlarmsAndFansInternalStaticStrings();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private bool DisplayMostRecentConfigurationInTheDatabase()
        //        {
        //            bool configFound = false;
        //            try
        //            {
        //                Guid configurationRootID;
        //                if (this.availableConfigurations != null)
        //                {
        //                    if (this.availableConfigurations.Count > 0)
        //                    {
        //                        configurationRootID = this.availableConfigurations[0].ID;
        //                        this.configurationFromDatabase = LoadConfigurationFromDatabase(configurationRootID, this.dbConnectionString, this);
        //                        if ((this.configurationFromDatabase != null) && (this.configurationFromDatabase.AllConfigurationMembersAreNonNull()))
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.configurationFromDatabase);
        //                            EnableFromDatabaseConfigurationRadRadioButtons();
        //                            SetFromDatabaseRadRadioButtonState();
        //                            configFound = true;
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nConfiguration was not complete in the database";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                }
        //                //                else
        //                //                {
        //                //                    string errorMessage = "Error in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nthis.availableConfigurations was null";
        //                //                    LogMessage.LogError(errorMessage);
        //                //#if DEBUG
        //                //                    MessageBox.Show(errorMessage);
        //                //#endif
        //                //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisplayMostRecentConfigurationInTheDatabase()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return configFound;
        //        }

        private void transformerConfigurationRadDropDownList_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    //if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
                    //{
                    WriteAllInterfaceDataToWorkingConfiguration();
                    //                    }
                    //                    else
                    //                    {
                    //                        string errorMessage = "Error in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanging(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nShould not have called this function for this configuration, control should have been disabled.";
                    //                        LogMessage.LogError(errorMessage);
                    //#if DEBUG
                    //                        MessageBox.Show(errorMessage);
                    //#endif
                    //                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanging(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerConfigurationRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    //if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
                    //{
                    AddDataToAllWindingHotSpotRadGridViews(this.workingConfiguration.windingHotSpotSetup);
                    //                    }
                    //                    else
                    //                    {
                    //                        string errorMessage = "Error in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nShould not have called this function for this configuration, control should have been disabled.";
                    //                        LogMessage.LogError(errorMessage);
                    //#if DEBUG
                    //                        MessageBox.Show(errorMessage);
                    //#endif
                    //                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerConfigurationRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerTypeRadDropDownList_SelectedIndexChanging(object sender, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    //if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
                    //{
                    WriteAllInterfaceDataToWorkingConfiguration();
                    //                    }
                    //                    else
                    //                    {
                    //                        string errorMessage = "Error in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanging(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nShould not have called this function for this configuration, control should have been disabled.";
                    //                        LogMessage.LogError(errorMessage);
                    //#if DEBUG
                    //                        MessageBox.Show(errorMessage);
                    //#endif
                    //                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangingCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void transformerTypeRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                if (!inTheProcessOfWritingTheConfigurationToTheInterfaceObjects)
                {
                    //if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
                    //{
                    AddDataToTempRiseAndMaximumCurrentRadGridViews(this.workingConfiguration.windingHotSpotSetup);
                    //                    }
                    //                    else
                    //                    {
                    //                        string errorMessage = "Error in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nShould not have called this function for this configuration, control should have been disabled.";
                    //                        LogMessage.LogError(errorMessage);
                    //#if DEBUG
                    //                        MessageBox.Show(errorMessage);
                    //#endif
                    //                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.transformerTypeRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void saveWorkingConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                // this used to have a "using" statement but that wasn't acceptable in this instance for some reason I don't yet understand.
                using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    SaveWorkingConfigurationToDatabase(localDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.saveCurrentConfigurationCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void saveDeviceConfigurationRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                SaveDeviceConfigurationToDatabase();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.saveDeviceConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void loadConfigurationFromDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadSelectedConfigurationFromDatabase();
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = true;
                programDeviceAlarmsAndFansTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.loadConfigurationFromDatabaseGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void deleteConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteConfigurationFromDatabase();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.deleteConfigurationGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.programDeviceCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToWorkingConfigObject();
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = true;
                programDeviceAlarmsAndFansTabRadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.loadConfigurationFromDeviceGeneralSettingsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void copyDeviceConfigurationRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                CopyConfigurationFromDeviceToWorking();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.copyDeviceConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void copyDatabaseConfigurationRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                CopyConfigurationFromDatabaseToWorking();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.copyDatabaseConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //       private void initializeWorkingConfigurationRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                if (!currentlyInitializingWorkingConfiguration)
        //                {
        //                    InitializeWorkingConfiguration();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.initializeWorkingConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void availableConfigurationsWhsGeneralSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsWhsGeneralSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.availableConfigurationsWhsGeneralSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void availableConfigurationsCalculationSettingsTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsCalculationSettingsTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.availableConfigurationsCalculationSettingsTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void availableConfigurationsAlarmsAndFansTabRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableConfigurationsAlarmsAndFansTabRadListControl.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.availableConfigurationsSelectedIndex)
                    {
                        this.availableConfigurationsSelectedIndex = selectedIndex;
                        SetAvailableConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.availableConfigurationsAlarmsAndFansTabRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetAvailableConfigurationsSelectedIndex()
        {
            try
            {
                availableConfigurationsWhsGeneralSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsCalculationSettingsTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
                availableConfigurationsAlarmsAndFansTabRadListControl.SelectedIndex = this.availableConfigurationsSelectedIndex;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void fromDeviceWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDeviceWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDeviceRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDeviceWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void fromDatabaseWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDatabaseWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDatabaseRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDatabaseWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void workingWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDatabaseWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetWorkingConfigurationRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.workingWhsGeneralSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void fromDeviceCalculationSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDeviceCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDeviceRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDeviceCalculationSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void fromDatabaseCalculationSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDatabaseCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDatabaseRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDatabaseCalculationSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void workingCalculationSettingsTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.workingCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetWorkingConfigurationRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.workingCalculationSettingsTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void fromDeviceAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDeviceAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDeviceRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDeviceAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void fromDatabaseAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.fromDatabaseAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetFromDatabaseRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.fromDatabaseAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void workingAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        //        {
        //            try
        //            {
        //                if (this.workingAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
        //                {
        //                    SetWorkingConfigurationRadRadioButtonState();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.workingAlarmsAndFansTabRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void LoadAvailableConfigurations(MonitorInterfaceDB localDB)
        {
            try
            {
                string messageString;

                if (this.monitor != null)
                {
                    if (Main_WHSMonitorConfiguration.programType == ProgramType.TemplateEditor)
                    {
                        this.availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(localDB);
                    }
                    else
                    {
                        this.availableConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(this.monitor.ID, localDB);
                    }

                    availableConfigurationsWhsGeneralSettingsTabRadListControl.Items.Clear();
                    availableConfigurationsCalculationSettingsTabRadListControl.Items.Clear();
                    availableConfigurationsAlarmsAndFansTabRadListControl.Items.Clear();
                    if ((this.availableConfigurations != null) && (this.availableConfigurations.Count > 0))
                    {
                        messageString = "Date Added                        Description";

                        availableConfigurationsWhsGeneralSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCalculationSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsAlarmsAndFansTabRadListControl.Items.Add(messageString);

                        foreach (Main_Config_WHSConfigurationRoot entry in this.availableConfigurations)
                        {
                            messageString = entry.DateAdded.ToString() + "      " + entry.Description;

                            availableConfigurationsWhsGeneralSettingsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsCalculationSettingsTabRadListControl.Items.Add(messageString);
                            availableConfigurationsAlarmsAndFansTabRadListControl.Items.Add(messageString);
                        }
                    }
                    else
                    {
                        messageString = "None Saved in DB";

                        availableConfigurationsWhsGeneralSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsCalculationSettingsTabRadListControl.Items.Add(messageString);
                        availableConfigurationsAlarmsAndFansTabRadListControl.Items.Add(messageString);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadAvailableConfigurations(MonitorInterfaceDB)\nthis.monitor was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadAvailableConfigurations(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsCalculationSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource;

                string description;

                this.templateConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsCalculationSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsCalculationSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsGeneralSettingsTabRadDropDownList.DataSource = templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                templateConfigurationsCalculationSettingsTabRadDropDownList.DataSource = templateConfigurationsCalculationSettingsTabRadDropDownListDataSource;
                templateConfigurationsAlarmsAndFansTabRadDropDownList.DataSource = templateConfigurationsAlarmsAndFansTabRadDropDownListDataSource;

                templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void CopyConfigurationFromDatabaseToWorking()
        //        {
        //            try
        //            {
        //                bool copyConfiguration = true;
        //                if (this.configurationFromDatabase != null)
        //                {
        //                    if (this.workingConfiguration != null)
        //                    {
        //                        WriteAllInterfaceDataToWorkingConfiguration();
        //                        if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
        //                        {
        //                            if (RadMessageBox.Show(this, youWillLoseAllChangesToTheWorkingConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
        //                            {
        //                                copyConfiguration = false;
        //                            }
        //                        }
        //                    }
        //                    if (copyConfiguration)
        //                    {
        //                        this.workingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.configurationFromDatabase);
        //                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
        //                        EnableWorkingConfigurationRadRadioButtons();
        //                        workingCopyIsFromDatabase = true;
        //                        workingCopyIsFromDevice = false;
        //                        workingCopyIsFromInitialization = false;
        //                        if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
        //                        }
        //                        else
        //                        {
        //                            SetWorkingConfigurationRadRadioButtonState();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    RadMessageBox.Show(this, noDatabaseConfigurationLoadedText);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.CopyConfigurationFromDatabaseToWorking()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void CopyConfigurationFromDeviceToWorking()
        //        {
        //            try
        //            {
        //                bool copyConfiguration = true;
        //                if (this.configurationFromDevice != null)
        //                {
        //                    if (this.workingConfiguration != null)
        //                    {
        //                        WriteAllInterfaceDataToWorkingConfiguration();
        //                        if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
        //                        {
        //                            if (RadMessageBox.Show(this, youWillLoseAllChangesToTheWorkingConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
        //                            {
        //                                copyConfiguration = false;
        //                            }
        //                        }
        //                    }
        //                    if (copyConfiguration)
        //                    {
        //                        this.workingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.configurationFromDevice);
        //                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);
        //                        EnableWorkingConfigurationRadRadioButtons();
        //                        workingCopyIsFromDevice = true;
        //                        workingCopyIsFromDatabase = false;
        //                        workingCopyIsFromInitialization = false;
        //                        if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
        //                        }
        //                        else
        //                        {
        //                            SetWorkingConfigurationRadRadioButtonState();
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    RadMessageBox.Show(this, noDeviceConfigurationLoadedText);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.CopyConfigurationFromDeviceToWorking()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void InitializeWorkingConfiguration()
        //        {
        //            try
        //            {
        //                bool copyConfiguration;

        //                /// We might get caught calling this function multiple times because it can be called each time we change a button state in a given
        //                /// tab.  We will trap that here just in case.
        //                if (!currentlyInitializingWorkingConfiguration)
        //                {
        //                    currentlyInitializingWorkingConfiguration = true;

        //                    copyConfiguration = true;
        //                    if (this.workingConfiguration != null)
        //                    {
        //                        WriteAllInterfaceDataToWorkingConfiguration();
        //                        if ((this.uneditedWorkingConfiguration != null) && (!this.uneditedWorkingConfiguration.ConfigurationIsTheSame(this.workingConfiguration)))
        //                        {
        //                            if (RadMessageBox.Show(this, overwriteCurrentConfigurationText, "", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
        //                            {
        //                                copyConfiguration = false;
        //                            }
        //                        }
        //                    }
        //                    if (copyConfiguration)
        //                    {
        //                        this.workingConfiguration = new Main_WHSConfiguration();
        //                        this.uneditedWorkingConfiguration = Main_WHSConfiguration.CopyConfiguration(this.workingConfiguration);

        //                        EnableWorkingConfigurationRadRadioButtons();
        //                        workingCopyIsFromInitialization = true;
        //                        workingCopyIsFromDevice = false;
        //                        workingCopyIsFromDatabase = false;
        //                        if (configurationBeingDisplayed == ConfigurationDisplayed.Working)
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
        //                        }
        //                        else
        //                        {
        //                            SetWorkingConfigurationRadRadioButtonState();
        //                        }
        //                    }
        //                    currentlyInitializingWorkingConfiguration = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeWorkingConfiguration()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void WriteConfigurationDataToAllInterfaceObjects(Main_WHSConfiguration configuration)
        {
            try
            {
                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = true;
                WriteConfigurationDataToWhsGeneralSettingsInterfaceObjects(configuration.windingHotSpotSetup);
                
                WriteConfigurationDataToAlarmsAndFansInterfaceObjects(configuration.windingHotSpotSetup);
                AddDataToAllWindingHotSpotRadGridViews(configuration.windingHotSpotSetup);
                inTheProcessOfWritingTheConfigurationToTheInterfaceObjects = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteConfigurationDataToAllInterfaceObjects(Main_WHSConfiguration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This is called whenever the transformer configuration is changed
        /// </summary>
        /// <param name="windingHotSpotSetup"></param>
        private void AddDataToAllWindingHotSpotRadGridViews(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                AddDataToMaximumRatedCurrentRadGridView(windingHotSpotSetup);
               
                AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToAllWindingHotSpotRadGridViews(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        /// <summary>
        /// This is called whenever the transformer type is changed
        /// </summary>
        /// <param name="windingHotSpotSetup"></param>
        private void AddDataToTempRiseAndMaximumCurrentRadGridViews(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                AddDataToMaximumRatedCurrentRadGridView(windingHotSpotSetup);
                AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToTempRiseAndMaximumCurrentRadGridViews(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteAllInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (!ErrorIsPresentInSomeTabObject())
                {
                    if (this.workingConfiguration != null)
                    {
                        if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                        {
                            WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration();
                           
                            WriteAlarmsAndFansInterfaceDataToWorkingConfiguration();
                            this.workingConfiguration.windingHotSpotSetup.RatingAsBitString = ConvertRatingsBitEncodingAsIntegerArrayToBitString();
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration has null member objects";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteAllInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void EnableWorkingConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                if (this.workingConfiguration != null)
        //                {
        //                    workingWhsGeneralSettingsTabRadRadioButton.Enabled = true;
        //                    workingCalculationSettingsTabRadRadioButton.Enabled = true;
        //                    workingAlarmsAndFansTabRadRadioButton.Enabled = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableWorkingConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void DisableWorkingConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                workingWhsGeneralSettingsTabRadRadioButton.Enabled = false;
        //                workingCalculationSettingsTabRadRadioButton.Enabled = false;
        //                workingAlarmsAndFansTabRadRadioButton.Enabled = false;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableWorkingConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void EnableFromDeviceConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                if (this.configurationFromDevice != null)
        //                {
        //                    fromDeviceWhsGeneralSettingsTabRadRadioButton.Enabled = true;
        //                    fromDeviceCalculationSettingsTabRadRadioButton.Enabled = true;
        //                    fromDeviceAlarmsAndFansTabRadRadioButton.Enabled = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableFromDeviceConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void DisableFromDeviceConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                fromDeviceWhsGeneralSettingsTabRadRadioButton.Enabled = false;
        //                fromDeviceCalculationSettingsTabRadRadioButton.Enabled = false;
        //                fromDeviceAlarmsAndFansTabRadRadioButton.Enabled = false;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableFromDeviceConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void EnableFromDatabaseConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                if (this.configurationFromDatabase != null)
        //                {
        //                    fromDatabaseWhsGeneralSettingsTabRadRadioButton.Enabled = true;
        //                    fromDatabaseCalculationSettingsTabRadRadioButton.Enabled = true;
        //                    fromDatabaseAlarmsAndFansTabRadRadioButton.Enabled = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableFromDatabaseConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void DisableFromDatabaseConfigurationRadRadioButtons()
        //        {
        //            try
        //            {
        //                fromDatabaseWhsGeneralSettingsTabRadRadioButton.Enabled = false;
        //                fromDatabaseCalculationSettingsTabRadRadioButton.Enabled = false;
        //                fromDatabaseAlarmsAndFansTabRadRadioButton.Enabled = false;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableFromDatabaseConfigurationRadRadioButtons()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void SetFromDeviceRadRadioButtonState()
        //        {
        //            try
        //            {
        //                if (!this.currentlySettingFromDeviceRadRadioButtonState)
        //                {
        //                    this.currentlySettingFromDeviceRadRadioButtonState = true;

        //                    if (this.configurationFromDevice != null)
        //                    {
        //                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsFromDeviceTitleText;

        //                        if (this.fromDeviceWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDeviceWhsGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.fromDeviceCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDeviceCalculationSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.fromDeviceAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDeviceAlarmsAndFansTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDevice)
        //                        {
        //                            if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
        //                            {
        //                                WriteAllInterfaceDataToWorkingConfiguration();
        //                            }
        //                            WriteConfigurationDataToAllInterfaceObjects(this.configurationFromDevice);
        //                            this.configurationBeingDisplayed = ConfigurationDisplayed.FromDevice;
        //                            DisableWhsGeneralSettingsTabConfigurationEditObjects();
        //                            DisableCalculationSettingsTabConfigurationEditObjects();
        //                            DisableAlarmsAndFansTabConfigurationEditObjects();
        //                        }
        //                    }

        //                    this.currentlySettingFromDeviceRadRadioButtonState = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetFromDeviceRadRadioButtonState()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void SetFromDatabaseRadRadioButtonState()
        //        {
        //            try
        //            {
        //                if (!this.currentlySettingFromDatabaseRadRadioButtonState)
        //                {
        //                    this.currentlySettingFromDatabaseRadRadioButtonState = true;

        //                    if (this.configurationFromDatabase != null)
        //                    {
        //                        this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsFromDatabaseTitleText;

        //                        if (this.fromDatabaseWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDatabaseWhsGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.fromDatabaseCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDatabaseCalculationSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.fromDatabaseAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.fromDatabaseAlarmsAndFansTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.configurationBeingDisplayed != ConfigurationDisplayed.FromDatabase)
        //                        {
        //                            if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
        //                            {
        //                                WriteAllInterfaceDataToWorkingConfiguration();
        //                            }
        //                            this.configurationBeingDisplayed = ConfigurationDisplayed.FromDatabase;
        //                            DisableWhsGeneralSettingsTabConfigurationEditObjects();
        //                            DisableCalculationSettingsTabConfigurationEditObjects();
        //                            DisableAlarmsAndFansTabConfigurationEditObjects();
        //                        }
        //                        WriteConfigurationDataToAllInterfaceObjects(this.configurationFromDatabase);
        //                    }

        //                    this.currentlySettingFromDatabaseRadRadioButtonState = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetFromDatabaseRadRadioButtonState()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        //        private void SetWorkingConfigurationRadRadioButtonState()
        //        {
        //            try
        //            {
        //                if (!this.currentlySettingWorkingConfigurationRadRadioButtonState)
        //                {
        //                    this.currentlySettingWorkingConfigurationRadRadioButtonState = true;

        //                    if (this.workingConfiguration != null)
        //                    {
        //                        if (workingCopyIsFromDevice)
        //                        {
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDeviceTitleText;
        //                        }
        //                        else if (workingCopyIsFromDevice)
        //                        {
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromDatabaseTitleText;
        //                        }
        //                        else if (workingCopyIsFromInitialization)
        //                        {
        //                            this.Text = mainMonitorConfigurationInterfaceConfigurationShownIsWorkingCopyFromInitializationTitleText;
        //                        }

        //                        if (this.workingWhsGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.workingWhsGeneralSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.workingCalculationSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.workingCalculationSettingsTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.workingAlarmsAndFansTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off)
        //                        {
        //                            this.workingAlarmsAndFansTabRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        //                        }
        //                        if (this.configurationBeingDisplayed != ConfigurationDisplayed.Working)
        //                        {
        //                            WriteConfigurationDataToAllInterfaceObjects(this.workingConfiguration);
        //                            this.configurationBeingDisplayed = ConfigurationDisplayed.Working;
        //                            EnableWhsGeneralSettingsTabConfigurationEditObjects();
        //                            EnableCalculationSettingsTabConfigurationEditObjects();
        //                            EnableAlarmsAndFansTabConfigurationEditObjects();
        //                        }
        //                    }

        //                    this.currentlySettingWorkingConfigurationRadRadioButtonState = false;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetWorkingRadRadioButtonState()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private bool ErrorIsPresentInSomeTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                errorIsPresent = ErrorIsPresentInAWhsGeneralSettingsTabObject();
                
                if (!errorIsPresent)
                {
                    errorIsPresent = ErrorIsPresentInAnAlarmsAndFansTabObject();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ErrorIsPresentInSomeTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return errorIsPresent;
        }

        private void DisableAllControls()
        {
            try
            {
                saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
                // saveDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Enabled = false;
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Enabled = false;
                //deleteConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;

                saveWorkingConfigurationCalculationSettingsTabRadButton.Enabled = false;
                //saveDeviceConfigurationCalculationSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Enabled = false;
                programDeviceCalculationSettingsTabRadButton.Enabled = false;
                loadConfigurationFromDeviceCalculationSettingsTabRadButton.Enabled = false;
                // deleteConfigurationCalculationSettingsTabRadButton.Enabled = false;

                saveWorkingConfigurationAlarmsAndFansTabRadButton.Enabled = false;
                // saveDeviceConfigurationAlarmsAndFansTabRadButton.Enabled = false;
                loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Enabled = false;
                programDeviceAlarmsAndFansTabRadButton.Enabled = false;
                loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Enabled = false;
                // deleteConfigurationAlarmsAndFansTabRadButton.Enabled = false;

                //DisableFromDatabaseConfigurationRadRadioButtons();
                //DisableFromDeviceConfigurationRadRadioButtons();
                //DisableWorkingConfigurationRadRadioButtons();

                //DisableCopyDatabaseConfigurationRadButtons();
                //DisableCopyDeviceConfigurationRadButtons();
                //DisableInitializeWorkingConfigurationRadButtons();

                DisableWhsGeneralSettingsTabConfigurationEditObjects();
               
                DisableAlarmsAndFansTabConfigurationEditObjects();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //private void DisableCopyDatabaseConfigurationRadButtons()
        //{
        //    copyDatabaseConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationCalculationSettingsTabRadButton.Enabled = false;
        //    copyDatabaseConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //}

        //private void DisableCopyDeviceConfigurationRadButtons()
        //{
        //    copyDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //    copyDeviceConfigurationCalculationSettingsTabRadButton.Enabled = false;
        //    copyDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //}

        //private void DisableInitializeWorkingConfigurationRadButtons()
        //{
        //    initializeWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationCalculationSettingsTabRadButton.Enabled = false;
        //    initializeWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = false;
        //}

        private void EnableAllControls()
        {
            try
            {
                saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
                // saveDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Enabled = true;
                programDeviceWhsGeneralSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Enabled = true;
                // deleteConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;

                saveWorkingConfigurationCalculationSettingsTabRadButton.Enabled = true;
                //saveDeviceConfigurationCalculationSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Enabled = true;
                programDeviceCalculationSettingsTabRadButton.Enabled = true;
                loadConfigurationFromDeviceCalculationSettingsTabRadButton.Enabled = true;
                //deleteConfigurationCalculationSettingsTabRadButton.Enabled = true;

                saveWorkingConfigurationAlarmsAndFansTabRadButton.Enabled = true;
                // saveDeviceConfigurationAlarmsAndFansTabRadButton.Enabled = true;
                loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Enabled = true;
                programDeviceAlarmsAndFansTabRadButton.Enabled = true;
                loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Enabled = true;
                //deleteConfigurationAlarmsAndFansTabRadButton.Enabled = true;

                //EnableFromDatabaseConfigurationRadRadioButtons();
                //EnableFromDeviceConfigurationRadRadioButtons();
                //EnableWorkingConfigurationRadRadioButtons();

                //EnableCopyDatabaseConfigurationRadButtons();
                //EnableCopyDeviceConfigurationRadButtons();
                //EnableInitializeWorkingConfigurationRadButtons();

                //if (this.configurationBeingDisplayed == ConfigurationDisplayed.Working)
                //{
                EnableWhsGeneralSettingsTabConfigurationEditObjects();
                
                EnableAlarmsAndFansTabConfigurationEditObjects();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableAllControls()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //private void EnableCopyDatabaseConfigurationRadButtons()
        //{
        //    if (this.configurationFromDatabase != null)
        //    {
        //        copyDatabaseConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationCalculationSettingsTabRadButton.Enabled = true;
        //        copyDatabaseConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //    }
        //}

        //private void EnableCopyDeviceConfigurationRadButtons()
        //{
        //    if (this.configurationFromDevice != null)
        //    {
        //        copyDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //        copyDeviceConfigurationCalculationSettingsTabRadButton.Enabled = true;
        //        copyDeviceConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //    }
        //}

        //private void EnableInitializeWorkingConfigurationRadButtons()
        //{
        //    initializeWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationCalculationSettingsTabRadButton.Enabled = true;
        //    initializeWorkingConfigurationWhsGeneralSettingsTabRadButton.Enabled = true;
        //}

        private void EnableProgressBars()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Visible = true;
                calculationSettingsRadProgressBar.Visible = true;
                alarmsAndFansRadProgressBar.Visible = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableProgressBars()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Visible = false;
                calculationSettingsRadProgressBar.Visible = false;
                alarmsAndFansRadProgressBar.Visible = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableProgressBars()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarsToUploadState()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Text = uploadingConfigurationText;
                calculationSettingsRadProgressBar.Text = uploadingConfigurationText;
                alarmsAndFansRadProgressBar.Text = uploadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarsToUploadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarsToDownloadState()
        {
            try
            {
                whsGeneralSettingsRadProgressBar.Text = downloadingConfigurationText;
                calculationSettingsRadProgressBar.Text = downloadingConfigurationText;
                alarmsAndFansRadProgressBar.Text = downloadingConfigurationText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarsToDownloadState()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    whsGeneralSettingsRadProgressBar.Value1 = currentProgress;
                    calculationSettingsRadProgressBar.Value1 = currentProgress;
                    alarmsAndFansRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                if (windingHotSpotSetup != null)
                {
                    if (this.ratingsBitEncodingAsIntegerArray != null)
                    {
                        if (this.ratingsBitEncodingAsIntegerArray.Length == 16)
                        {
                            for (int i = 0; i < 16; i++)
                            {
                                if (windingHotSpotSetup.RatingAsBitString[i] == '0')
                                {
                                    this.ratingsBitEncodingAsIntegerArray[i] = 0;
                                }
                                else
                                {
                                    this.ratingsBitEncodingAsIntegerArray[i] = 1;
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nthis.ratingsBitEncodingAsIntegerArray.Length was not 16";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nthis.ratingsBitEncodingAsIntegerArray was null";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nInput Main_ConfigComponent_WindingHotSpotSetup was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.FillRatingsBitEncodingAsIntegerArrayFromConfiguration(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private string ConvertRatingsBitEncodingAsIntegerArrayToBitString()
        {
            string bitEncodedString = string.Empty;
            try
            {
                StringBuilder bitEncodedStringBuilder = new StringBuilder();
                if (this.ratingsBitEncodingAsIntegerArray != null)
                {
                    if (this.ratingsBitEncodingAsIntegerArray.Length == 16)
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            if (this.ratingsBitEncodingAsIntegerArray[i] == 0)
                            {
                                bitEncodedStringBuilder.Append("0");
                            }
                            else
                            {
                                bitEncodedStringBuilder.Append("1");
                            }
                        }
                        bitEncodedString = bitEncodedStringBuilder.ToString();
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nthis.ratingsBitEncodingAsIntegerArray.Length was not 16";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nthis.ratingsBitEncodingAsIntegerArray was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ConvertRatingsBitEncodingAsIntegerArrayToBitString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return bitEncodedString;
        }

        private void setAgingDaysRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                UInt16 previousAgingInDays = (UInt16)previousAgingInDaysRadSpinEditor.Value;
                Int16 previousAgingInDaysAsInt16 = ConversionMethods.UInt16BytesToInt16Value(previousAgingInDays);

                string modBusAddressAsString;
                int modBusAddress;
                int deviceType;
                ErrorCode errorCode;
                bool valueChangedSuccessfully = false;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (!downloadInProgress)
                    {
                        if (this.workingConfiguration != null)
                        {
                            if (this.monitor != null)
                            {
                                modBusAddressAsString = monitor.ModbusAddress.Trim();
                                if (modBusAddressAsString.Length > 0)
                                {
                                    modBusAddress = Int32.Parse(modBusAddressAsString);
                                    if (!ErrorIsPresentInSomeTabObject())
                                    {
                                        WriteAllInterfaceDataToWorkingConfiguration();

                                        using (MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                                        {
                                            errorCode = MonitorConnection.OpenMonitorConnection(this.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                        }
                                        if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                        {
                                            DeviceCommunication.EnableDataDownload();
                                            deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, this.readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);
                                            if (deviceType > 0)
                                            {
                                                if (deviceType == 101)
                                                {
                                                    Application.DoEvents();

                                                    valueChangedSuccessfully = InteractiveDeviceCommunication.WriteSingleRegister(modBusAddress, 5480, previousAgingInDaysAsInt16, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);

                                                    if (valueChangedSuccessfully)
                                                    {
                                                        RadMessageBox.Show(this, previousAgingValueChangedSuccessfullyText);
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, failedToChangeValueOfPreviousAgingText);
                                                    }
                                                }
                                                else
                                                {
                                                    RadMessageBox.Show(this, notConnectedToMainMonitorWarningText);
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DeviceErrorReadFailed));
                                            }
                                        }
                                        else
                                        {
                                            if (errorCode == ErrorCode.SerialPortNotSpecified)
                                            {
                                                RadMessageBox.Show(this, serialPortNotSetWarningText);
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, failedToOpenMonitorConnectionText);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, deviceCommunicationNotProperlyConfiguredText);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.LoadConfigurationFromDevice()\nthis.monitor was null.";
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, workingConfigurationNotDefinedText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, downloadWasInProgressWhenInterfaceWasOpenedText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.setAgingDaysRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsCalculationSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCalculationSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsAlarmsAndFansTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsAlarmsAndFansTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WhsMonitorConfiguration.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsCalculationSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCalculationSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.templateConfigurationsCalculationSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void templateConfigurationsAlarmsAndFansTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsAlarmsAndFansTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.templateConfigurationsAlarmsAndFansTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void copySelectedConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Main_Config_WHSConfigurationRoot templateConfigurationRoot;
                Guid destinationConfigurationRootID;
                if (PasswordUtilities.CheckUserLevelAndCorrectItIfNecessaryAndDesired(this, UserLevel.Manager))
                {
                    if (this.templateConfigurations != null)
                    {
                        if (this.templateConfigurations.Count > 0)
                        {
                            if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                            {
                                templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                                if (templateConfigurationRoot != null)
                                {
                                    destinationConfigurationRootID = Guid.NewGuid();
                                    using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.dbConnectionString))
                                        using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                        {
                                            if (MainMonitor_DatabaseMethods.Main_Config_CopyOneWHSConfigurationToNewDatabase(templateConfigurationRoot.ID, templateDB, destinationConfigurationRootID, this.monitor.ID, destinationDB))
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopySucceeded));
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationCopyFailed));
                                            }
                                            LoadAvailableConfigurations(destinationDB);
                                        }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                MessageBox.Show(errorMessage);
                        #endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.copySelectedConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void whsGeneralSettingsRadPageViewPage_Paint(object sender, PaintEventArgs e)
        {
        }

        private void fanSettingsRadGroupBox_Click(object sender, EventArgs e)
        {
        }

        private bool OpenConnectionInMain()
        {
            string connectionType = string.Empty;
            string ipAddress = string.Empty;
            string modBusAddressAsString = string.Empty;
            int modBusAddress = 0;
            long deviceError = 0;
            int deviceType;
            bool success;

            //bool connectionWasApparentlyOpenedSuccessfully = false;
            ErrorCode errorCode = ErrorCode.MonitorWasNull;
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            Main_Configuration configFromDevice = null;

            try
            {
                if (SafeToLoadConfiguration())
                {
                    if (!downloadInProgress)
                    {
                        if (this.monitor != null)
                        {
                            modBusAddressAsString = monitor.ModbusAddress.Trim();
                            if (modBusAddressAsString.Length > 0)
                            {
                                modBusAddress = Int32.Parse(modBusAddressAsString);

                                MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString);

                                errorCode = MonitorConnection.OpenMonitorConnection(this.monitor, this.serialPort, this.baudRate, ref this.readDelayInMicroseconds, localDB, parentWindowInformation, true);
                                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                                {
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, MonitorConnection.NumberOfNonInteractiveRetriesToAttempt, MonitorConnection.TotalNumberOfRetriesToAttempt, parentWindowInformation);

                                    if (deviceType == 101)
                                    {
                                        
                                        return true;
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Device Type doees not match for Main Monitor");
                                        return false;
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show("Connection Could not be Opened");
                                    return false;
                                }
                            }
                            else
                            {
                                RadMessageBox.Show("ModBus Address is not valid");
                                return false;
                            }
                        }                        
                        else
                        {
                            RadMessageBox.Show("No Monitor is Selected");
                            return false;
                        }
                    }
                    else
                    {
                        RadMessageBox.Show("A Download is in progress. Try again later");
                        return false;
                    }

                    return false;
                }
                else
                {
                    RadMessageBox.Show("System is Busy.  Try again later");
                    return false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ReadWriteModbusRegisterInMain(int RW, int DataArray, int numRegToSart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
                return false;
            }
        }

        private void UpdateAlarmListButton_Click(object sender, EventArgs e)
        {
            AlarmListControl.Items.Clear();
            bool success;
            short[] errorValues = new short[1];
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
           
            try
            {
                success = OpenConnectionInMain();
                if (success == true)
                {
                   errorValues= InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5536, 1, 200, 1, 20, parentWindowInformation);
                }
                else
                {
                    RadMessageBox.Show("Connection could not be opened");
                    
                }
                DeviceCommunication.CloseConnection();
                UInt16 x;
                x = Convert.ToUInt16(errorValues[0]);

                if (errorValues[0] != 0)
                {
                    if ((x & 1) > 0) // bit 1
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit1);
                    }
                    if ((x & 2) > 0) //bit 2
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit2);
                    }
                    if ((x & 4) > 0) // bit 3
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit3);
                    }
                    if ((x & 8) > 0) // bit 4
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit4);
                    }
                    if ((x & 16) > 0) // bit 5
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit5);
                    }
                    if ((x & 32) > 0) // bit 6
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit6);
                    }
                    if ((x & 64) > 0) // bit 7
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit7);
                    }
                    if ((x & 128) > 0) // bit 8
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit8);
                    }
                    if ((x & 256) > 0) // bit 9
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit9);
                    }
                    if ((x & 512) > 0) // bit 10
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit10);
                    }
                    if ((x & 1024) > 0) // bit 11
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit11);
                    }
                    if ((x & 2048) > 0) // bit 12
                    {
                        this.AlarmListControl.Items.Add(AlarmListBit12);
                    }
                }
                else
                {
                    this.AlarmListControl.Items.Add(AlarmListBit0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Alarm List Box \nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void updateErrorButton_Click(object sender, EventArgs e)
        {
            whsErrorListBox.Items.Clear();

            bool success;
            short[] errorValues = new short[1];
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            try
            {
                
                errorValues[0] = 0;
                success = OpenConnectionInMain();
                if (success == true)
                {
                    errorValues = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5537, 1, 200, 1, 20, parentWindowInformation);
                }
                else
                {
                    RadMessageBox.Show("Connection could not be opened");

                }
                
                DeviceCommunication.CloseConnection();

                UInt16 x;
                x = Convert.ToUInt16(errorValues[0]);

                if (errorValues[0] != 0)
                {
                    if ((x & 1) > 0) // bit 1
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit1);
                    }
                    if ((x & 2) > 0) //bit 2
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit2);
                    }
                    if ((x & 4) > 0) // bit 3
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit3);
                    }
                    if ((x & 8) > 0) // bit 4
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit4);
                    }
                    if ((x & 16) > 0) // bit 5
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit5);
                    }
                    if ((x & 32) > 0) // bit 6
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit6);
                    }
                    if ((x & 64) > 0) // bit 7
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit7);
                    }
                    if ((x & 128) > 0) // bit 8
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit8);
                    }
                    if ((x & 256) > 0) // bit 9
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit9);
                    }
                    if ((x & 512) > 0) // bit 10
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit10);
                    }
                    if ((x & 1024) > 0) // bit 11
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit11);
                    }
                    if ((x & 2048) > 0) // bit 12
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit12);
                    }
                    if ((x & 4096) > 0) // bit 13
                    {
                        this.whsErrorListBox.Items.Add(ErrorListBit13);
                    }
                }
                else
                {
                    this.whsErrorListBox.Items.Add(ErrorListBit0);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateErrorListBox \nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void ClearAlarmListButton_Click(object sender, EventArgs e)
        {
            bool success;
            short[] x = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            x[0] = 0;

            success = OpenConnectionInMain();
            if (success == true)
            {
                SendPassword();
                InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5536, x, 200, 1, 20, parentWindowInformation);
            }
            else
            {
                RadMessageBox.Show("Connection could not be opened");

            }
            DeviceCommunication.CloseConnection();
            UpdateAlarmListButton_Click(sender, null);
        }

        private void SendPassword()
        {
            short[] pw = new short[1];  // password
            pw[0] = 5421;
            bool success;
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

            success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 121, pw, 200, 1, 20, parentWindowInformation);
        }

        private void fanTestButton_Click(object sender, EventArgs e)
        {
            // this will start/stop fans for testing purpose.
            bool success;
            short[] x = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
            x[0] = 1;

            try
            {
                success = OpenConnectionInMain();
                if (success == true)
                {

                    if (fanTestButton.ButtonElement.ButtonFillElement.BackColor == Color.Green)
                    {
                        SendPassword();
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5478, x, 200, 1, 20, parentWindowInformation);
                        //MessageBox.Show("1-" + errorCode.ToString());
                    }
                    else
                    {
                        x[0] = 0;
                        SendPassword();
                        success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5478, x, 200, 1, 20, parentWindowInformation);
                        // MessageBox.Show("0-" + success.ToString());
                    }
                    x = InteractiveDeviceCommunication.ReadOneRegister(modBusAddress, 5478, 200, 1, 200, parentWindowInformation);
                    //MessageBox.Show("read-" + x[0].ToString());
                    if (x[0] == 0)
                    {
                        fanTestButton.Text = "Cooling Off";
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                    }
                    else
                    {
                        fanTestButton.Text = "Cooling On";
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                        fanTestButton.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                    }

                }
                DeviceCommunication.CloseConnection();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in fanTestButton_Click(object sender, EventArgs e)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void CalibrateFanButton_Click(object sender, EventArgs e)
        {
            bool success;
            short[] x = new short[1];
            short[] fs = new short[2];

            short[] baseCurrent = new short[2];
            short[] calDone = new short[1];
            ErrorCode errorCode = ErrorCode.None;
            int modBusAddress;
            modBusAddress = Int32.Parse(monitor.ModbusAddress.Trim());
            ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                x[0] = 1;

            try
            {
                success = OpenConnectionInMain();
                if (success == true)
                {
                    SendPassword();
                    success = InteractiveDeviceCommunication.WriteMultipleRegisters(modBusAddress, 5496, x, 200, 1, 20, parentWindowInformation);
                }
               

                //MessageBox.Show("1-" + errorCode.ToString());
                CalibrateFanButton.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                CalibrateFanButton.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                CalibrateFanButton.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                CalibrateFanButton.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                Application.DoEvents();

                for (int zz = 0; zz < 5; zz++)
                {
                    Thread.Sleep(1000);
                    calDone = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5496, 1, 200, 1, 20, parentWindowInformation);
                    if (calDone[0] == 0)
                    {
                        baseCurrent = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5482, 2, 200, 1, 20, parentWindowInformation);
                        fanCurrent1SpinEditor.Value = (baseCurrent[0] / 10M);
                        fanCurrent2SpinEditor.Value = (baseCurrent[1] / 10M);
                        radButton3.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                        radButton3.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                        radButton3.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                        radButton3.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                        break;
                    }
                }

                // read fan status and set colors of buttons
                fs = InteractiveDeviceCommunication.ReadMultipleRegisters(modBusAddress, 5528, 2, 200, 1, 20, parentWindowInformation);

                if (fs[0] == 1)
                {
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                }
                else
                {
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    Fan1Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }
                if (fs[1] == 1)
                {
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Red;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Red;
                }
                else
                {
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor2 = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor3 = Color.Green;
                    Fan2Indicator.ButtonElement.ButtonFillElement.BackColor4 = Color.Green;
                }
                DeviceCommunication.CloseConnection();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CalibrateFanButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
            }
        }

        private void configurationRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {

        }

        private void lowCoolingAlarmSpinEditor_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}