﻿namespace MainMonitorUtilities
{
    partial class Main_WHSMonitorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_WHSMonitorConfiguration));
            this.configurationRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.whsGeneralSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.updateErrorButton = new Telerik.WinControls.UI.RadButton();
            this.ErrorListLabel = new Telerik.WinControls.UI.RadLabel();
            this.whsErrorListBox = new Telerik.WinControls.UI.RadListControl();
            this.ClearAlarmListButton = new Telerik.WinControls.UI.RadButton();
            this.UpdateAlarmListButton = new Telerik.WinControls.UI.RadButton();
            this.AlarmLabel = new Telerik.WinControls.UI.RadLabel();
            this.AlarmListControl = new Telerik.WinControls.UI.RadListControl();
            this.HotSpotFactorGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.hotSpotFactorSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.hotSpotFactorLabelText = new Telerik.WinControls.UI.RadLabel();
            this.TopOilGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.topOilTemperatureComboBox = new Telerik.WinControls.UI.RadDropDownList();
            this.agingCalculationMethodRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.agingCalculationMethodRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsGeneralSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceWhsGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.transformerTypeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.transformerTypeRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.transformerConfigurationRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.transformerConfigurationRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.maximumRatedCurrentRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.maximumRatedCurrentRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.whsGeneralSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.firmwareVersionRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.firmwareVersionValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.tempRiseSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.temperatureRiseOverTopOilRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.whsCalculationEnableDisableRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.disableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.enableMonitoringRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.calculationSettingsRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.PreviousAgingDaysPhaseC = new Telerik.WinControls.UI.RadTextBox();
            this.PreviousAgingDaysPhaseB = new Telerik.WinControls.UI.RadTextBox();
            this.PreviousAgingDaysPhaseA = new Telerik.WinControls.UI.RadTextBox();
            this.PreviousAgingDaysPhaseCLabel = new Telerik.WinControls.UI.RadLabel();
            this.PreviousAgingDaysPhaseBLabel = new Telerik.WinControls.UI.RadLabel();
            this.PreviousAgingDaysPhaseALabel = new Telerik.WinControls.UI.RadLabel();
            this.templateConfigurationsCalculationSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsCalculationSettingsTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionCalculationSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.programDeviceCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionCalculationSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.algorithmVariantRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.algorithmVariantRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.timeConstantRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.timeContantMinutesRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.timeConstantRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.exponentRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.exponentRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.previousAgingRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.valueSetInpedentlyFromOtherSettingsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.agingDaysTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.setAgingDaysRadButton = new Telerik.WinControls.UI.RadButton();
            this.previousAgingInDaysRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.currentInputsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.currentInputsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.temperatureInputsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.temperatureInputsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.calculationSettingsRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsCalculationSettingsTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsCalculationSettingsTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.alarmsAndFansRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.copySelectedConfigurationAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.deviceInteractionAlarmsAndFansTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.programDeviceAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.databaseInteractionAlarmsAndFansTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton = new Telerik.WinControls.UI.RadButton();
            this.deadBandTemperatureRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.deadBandTemperatureRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.deadBandTemperatureTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.alarmSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.alarmDelayRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.alarmDelayTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.topOilTempIsHighRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.topOilTempHighAlarmSetPointRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.topOilTempLowAlarmSetPointRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.topOilTempHighAlarmSetPointTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.topOilTempLowAlarmSetPointTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.whsIsHighRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.whsHighAlarmSetPointRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.whsLowAlarmSetPointRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.whsHighAlarmSetPointTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.whsLowAlarmSetPointTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanSettingsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.fanCurrent2SpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanOperationalInfoGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.fan2NumOfStartsSpinEdito = new Telerik.WinControls.UI.RadSpinEditor();
            this.FanStatusLabel = new Telerik.WinControls.UI.RadLabel();
            this.fan1NumOfStartsSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.Fan1Indicator = new Telerik.WinControls.UI.RadButton();
            this.fan2RuntimeHoursSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.whsFan2StatsLabelText = new Telerik.WinControls.UI.RadLabel();
            this.fan1RuntimeHoursSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.Fan2Indicator = new Telerik.WinControls.UI.RadButton();
            this.whsRunTimeHoursLabelText = new Telerik.WinControls.UI.RadLabel();
            this.whsFan1StatsLabelText = new Telerik.WinControls.UI.RadLabel();
            this.whsNumOfStartsLabelText = new Telerik.WinControls.UI.RadLabel();
            this.fanCurrent1SpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanBankSwapRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.fanBankSwapSetToZeroToDisableTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanBankSwapEveryTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanBankSwapIntervalInHoursRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanBankSwapHoursTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.EnableFanCurrentMonitoring = new Telerik.WinControls.UI.RadCheckBox();
            this.minimumFanRunTimeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.minimumfanRunTimeInMinutesRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.minimumFanRunTimeMinutesTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.Group1CurrentLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanAutoExerciseRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.ManualExcerciseLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanTestButton = new Telerik.WinControls.UI.RadButton();
            this.fanAutoExerciseMinutesTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanAutoExerciseDaysTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanExerciseTimeInMinutesRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanExerciseIntervalInDaysRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanAutoExerciseForTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanAutoExerciseEveryTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.Group2CurrentLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanStartTemperaturesRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.fanBankTwoStartTempTopOilRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanBankTwoStartTempWhsRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanBankOneStartTempWhsRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.whsTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanBankOneStartTempTopOilRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.fanBankTwoTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.fanBankOneTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.topOilTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.CalibrateFanButton = new Telerik.WinControls.UI.RadButton();
            this.NumberofFanGroupsLabel = new Telerik.WinControls.UI.RadLabel();
            this.coolingAlarmGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.highCoolingAlarmLabel = new Telerik.WinControls.UI.RadLabel();
            this.HighCoolingAlarmSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.lowCoolingAlarmlabel = new Telerik.WinControls.UI.RadLabel();
            this.lowCoolingAlarmSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.NumberOfCoolingGroupsComboBox = new Telerik.WinControls.UI.RadDropDownList();
            this.alarmsAndFansRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableConfigurationsAlarmsAndFansTabRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).BeginInit();
            this.configurationRadPageView.SuspendLayout();
            this.whsGeneralSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateErrorButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorListLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsErrorListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearAlarmListButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateAlarmListButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotSpotFactorGroupBox)).BeginInit();
            this.HotSpotFactorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hotSpotFactorSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hotSpotFactorLabelText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopOilGroupBox)).BeginInit();
            this.TopOilGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTemperatureComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingCalculationMethodRadGroupBox)).BeginInit();
            this.agingCalculationMethodRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.agingCalculationMethodRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionWhsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceWhsGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionWhsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformerTypeRadGroupBox)).BeginInit();
            this.transformerTypeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transformerTypeRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformerConfigurationRadGroupBox)).BeginInit();
            this.transformerConfigurationRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transformerConfigurationRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGroupBox)).BeginInit();
            this.maximumRatedCurrentRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsGeneralSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsWhsGeneralSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionRadGroupBox)).BeginInit();
            this.firmwareVersionRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempRiseSettingsRadGroupBox)).BeginInit();
            this.tempRiseSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRiseOverTopOilRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRiseOverTopOilRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsCalculationEnableDisableRadGroupBox)).BeginInit();
            this.whsCalculationEnableDisableRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).BeginInit();
            this.calculationSettingsRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseCLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseBLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseALabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalculationSettingsTabRadGroupBox)).BeginInit();
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalculationSettingsTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCalculationSettingsTabRadGroupBox)).BeginInit();
            this.deviceInteractionCalculationSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCalculationSettingsTabRadGroupBox)).BeginInit();
            this.databaseInteractionCalculationSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmVariantRadGroupBox)).BeginInit();
            this.algorithmVariantRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmVariantRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeConstantRadGroupBox)).BeginInit();
            this.timeConstantRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeContantMinutesRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeConstantRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentRadGroupBox)).BeginInit();
            this.exponentRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exponentRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousAgingRadGroupBox)).BeginInit();
            this.previousAgingRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valueSetInpedentlyFromOtherSettingsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingDaysTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setAgingDaysRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousAgingInDaysRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGroupBox)).BeginInit();
            this.currentInputsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGroupBox)).BeginInit();
            this.temperatureInputsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationSettingsRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalculationSettingsTabRadGroupBox)).BeginInit();
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalculationSettingsTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            this.alarmsAndFansRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmsAndFansTabRadGroupBox)).BeginInit();
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmsAndFansTabRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAlarmsAndFansTabRadGroupBox)).BeginInit();
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAlarmsAndFansTabRadGroupBox)).BeginInit();
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureRadGroupBox)).BeginInit();
            this.deadBandTemperatureRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGroupBox)).BeginInit();
            this.alarmSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDelayRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDelayTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempIsHighRadGroupBox)).BeginInit();
            this.topOilTempIsHighRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempHighAlarmSetPointRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempLowAlarmSetPointRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempHighAlarmSetPointTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempLowAlarmSetPointTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsIsHighRadGroupBox)).BeginInit();
            this.whsIsHighRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.whsHighAlarmSetPointRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsLowAlarmSetPointRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsHighAlarmSetPointTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsLowAlarmSetPointTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanSettingsRadGroupBox)).BeginInit();
            this.fanSettingsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanCurrent2SpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanOperationalInfoGroupBox)).BeginInit();
            this.fanOperationalInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fan2NumOfStartsSpinEdito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FanStatusLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan1NumOfStartsSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1Indicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan2RuntimeHoursSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsFan2StatsLabelText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan1RuntimeHoursSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2Indicator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsRunTimeHoursLabelText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsFan1StatsLabelText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsNumOfStartsLabelText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanCurrent1SpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapRadGroupBox)).BeginInit();
            this.fanBankSwapRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapSetToZeroToDisableTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapEveryTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapIntervalInHoursRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapHoursTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnableFanCurrentMonitoring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumFanRunTimeRadGroupBox)).BeginInit();
            this.minimumFanRunTimeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minimumfanRunTimeInMinutesRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumFanRunTimeMinutesTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1CurrentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseRadGroupBox)).BeginInit();
            this.fanAutoExerciseRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ManualExcerciseLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanTestButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseMinutesTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseDaysTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanExerciseTimeInMinutesRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanExerciseIntervalInDaysRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseForTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseEveryTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group2CurrentLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanStartTemperaturesRadGroupBox)).BeginInit();
            this.fanStartTemperaturesRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoStartTempTopOilRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoStartTempWhsRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneStartTempWhsRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneStartTempTopOilRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalibrateFanButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberofFanGroupsLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolingAlarmGroupBox)).BeginInit();
            this.coolingAlarmGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.highCoolingAlarmLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighCoolingAlarmSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowCoolingAlarmlabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowCoolingAlarmSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfCoolingGroupsComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmsAndFansRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmsAndFansTabRadGroupBox)).BeginInit();
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmsAndFansTabRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationRadPageView
            // 
            this.configurationRadPageView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.configurationRadPageView.Controls.Add(this.whsGeneralSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.calculationSettingsRadPageViewPage);
            this.configurationRadPageView.Controls.Add(this.alarmsAndFansRadPageViewPage);
            this.configurationRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.configurationRadPageView.Location = new System.Drawing.Point(0, 0);
            this.configurationRadPageView.Name = "configurationRadPageView";
            this.configurationRadPageView.SelectedPage = this.alarmsAndFansRadPageViewPage;
            this.configurationRadPageView.Size = new System.Drawing.Size(872, 732);
            this.configurationRadPageView.TabIndex = 1;
            this.configurationRadPageView.Text = "radPageView1";
            this.configurationRadPageView.ThemeName = "Office2007Black";
            this.configurationRadPageView.SelectedPageChanged += new System.EventHandler(this.configurationRadPageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.configurationRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // whsGeneralSettingsRadPageViewPage
            // 
            this.whsGeneralSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.updateErrorButton);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.ErrorListLabel);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.whsErrorListBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.ClearAlarmListButton);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.UpdateAlarmListButton);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.AlarmLabel);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.AlarmListControl);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.HotSpotFactorGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.TopOilGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.agingCalculationMethodRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsGeneralSettingsTabRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.deviceInteractionWhsGeneralSettingsTabRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.databaseInteractionWhsGeneralSettingsTabRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.transformerTypeRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.transformerConfigurationRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.maximumRatedCurrentRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.whsGeneralSettingsRadProgressBar);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.firmwareVersionRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.tempRiseSettingsRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Controls.Add(this.whsCalculationEnableDisableRadGroupBox);
            this.whsGeneralSettingsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whsGeneralSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.whsGeneralSettingsRadPageViewPage.Name = "whsGeneralSettingsRadPageViewPage";
            this.whsGeneralSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 684);
            this.whsGeneralSettingsRadPageViewPage.Text = "General Settings";
            this.whsGeneralSettingsRadPageViewPage.Paint += new System.Windows.Forms.PaintEventHandler(this.whsGeneralSettingsRadPageViewPage_Paint);
            // 
            // updateErrorButton
            // 
            this.updateErrorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.updateErrorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateErrorButton.Location = new System.Drawing.Point(71, 245);
            this.updateErrorButton.Name = "updateErrorButton";
            this.updateErrorButton.Size = new System.Drawing.Size(104, 23);
            this.updateErrorButton.TabIndex = 66;
            this.updateErrorButton.Text = "Update Error List";
            this.updateErrorButton.ThemeName = "Office2007Black";
            this.updateErrorButton.Click += new System.EventHandler(this.updateErrorButton_Click);
            // 
            // ErrorListLabel
            // 
            this.ErrorListLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ErrorListLabel.Location = new System.Drawing.Point(13, 252);
            this.ErrorListLabel.Name = "ErrorListLabel";
            this.ErrorListLabel.Size = new System.Drawing.Size(52, 16);
            this.ErrorListLabel.TabIndex = 67;
            this.ErrorListLabel.Text = "Error List";
            // 
            // whsErrorListBox
            // 
            this.whsErrorListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.whsErrorListBox.AutoScroll = true;
            this.whsErrorListBox.CaseSensitiveSort = true;
            this.whsErrorListBox.ItemHeight = 18;
            this.whsErrorListBox.Location = new System.Drawing.Point(22, 274);
            this.whsErrorListBox.Name = "whsErrorListBox";
            this.whsErrorListBox.Size = new System.Drawing.Size(586, 167);
            this.whsErrorListBox.TabIndex = 68;
            this.whsErrorListBox.Text = "radListControl2";
            this.whsErrorListBox.ThemeName = "ControlDefault";
            // 
            // ClearAlarmListButton
            // 
            this.ClearAlarmListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ClearAlarmListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearAlarmListButton.Location = new System.Drawing.Point(729, 292);
            this.ClearAlarmListButton.Name = "ClearAlarmListButton";
            this.ClearAlarmListButton.Size = new System.Drawing.Size(104, 23);
            this.ClearAlarmListButton.TabIndex = 63;
            this.ClearAlarmListButton.Text = "Clear Alarms";
            this.ClearAlarmListButton.ThemeName = "Office2007Black";
            this.ClearAlarmListButton.Click += new System.EventHandler(this.ClearAlarmListButton_Click);
            // 
            // UpdateAlarmListButton
            // 
            this.UpdateAlarmListButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UpdateAlarmListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateAlarmListButton.Location = new System.Drawing.Point(729, 263);
            this.UpdateAlarmListButton.Name = "UpdateAlarmListButton";
            this.UpdateAlarmListButton.Size = new System.Drawing.Size(104, 23);
            this.UpdateAlarmListButton.TabIndex = 62;
            this.UpdateAlarmListButton.Text = "Update Alarm List";
            this.UpdateAlarmListButton.ThemeName = "Office2007Black";
            this.UpdateAlarmListButton.Click += new System.EventHandler(this.UpdateAlarmListButton_Click);
            // 
            // AlarmLabel
            // 
            this.AlarmLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AlarmLabel.Location = new System.Drawing.Point(637, 64);
            this.AlarmLabel.Name = "AlarmLabel";
            this.AlarmLabel.Size = new System.Drawing.Size(57, 16);
            this.AlarmLabel.TabIndex = 65;
            this.AlarmLabel.Text = "Alarm List";
            // 
            // AlarmListControl
            // 
            this.AlarmListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AlarmListControl.AutoScroll = true;
            this.AlarmListControl.CaseSensitiveSort = true;
            this.AlarmListControl.ItemHeight = 18;
            this.AlarmListControl.Location = new System.Drawing.Point(634, 84);
            this.AlarmListControl.Name = "AlarmListControl";
            this.AlarmListControl.Size = new System.Drawing.Size(199, 167);
            this.AlarmListControl.TabIndex = 64;
            this.AlarmListControl.Text = "radListControl2";
            this.AlarmListControl.ThemeName = "ControlDefault";
            // 
            // HotSpotFactorGroupBox
            // 
            this.HotSpotFactorGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.HotSpotFactorGroupBox.Controls.Add(this.hotSpotFactorSpinEditor);
            this.HotSpotFactorGroupBox.Controls.Add(this.hotSpotFactorLabelText);
            this.HotSpotFactorGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HotSpotFactorGroupBox.FooterImageIndex = -1;
            this.HotSpotFactorGroupBox.FooterImageKey = "";
            this.HotSpotFactorGroupBox.HeaderImageIndex = -1;
            this.HotSpotFactorGroupBox.HeaderImageKey = "";
            this.HotSpotFactorGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.HotSpotFactorGroupBox.HeaderText = "Hot Spot Factor";
            this.HotSpotFactorGroupBox.Location = new System.Drawing.Point(528, 7);
            this.HotSpotFactorGroupBox.Name = "HotSpotFactorGroupBox";
            this.HotSpotFactorGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.HotSpotFactorGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.HotSpotFactorGroupBox.Size = new System.Drawing.Size(166, 51);
            this.HotSpotFactorGroupBox.TabIndex = 58;
            this.HotSpotFactorGroupBox.Text = "Hot Spot Factor";
            this.HotSpotFactorGroupBox.ThemeName = "Office2007Black";
            // 
            // hotSpotFactorSpinEditor
            // 
            this.hotSpotFactorSpinEditor.DecimalPlaces = 2;
            this.hotSpotFactorSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.hotSpotFactorSpinEditor.Location = new System.Drawing.Point(97, 21);
            this.hotSpotFactorSpinEditor.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.hotSpotFactorSpinEditor.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            this.hotSpotFactorSpinEditor.Name = "hotSpotFactorSpinEditor";
            // 
            // 
            // 
            this.hotSpotFactorSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.hotSpotFactorSpinEditor.ShowBorder = true;
            this.hotSpotFactorSpinEditor.Size = new System.Drawing.Size(52, 20);
            this.hotSpotFactorSpinEditor.TabIndex = 0;
            this.hotSpotFactorSpinEditor.TabStop = false;
            this.hotSpotFactorSpinEditor.ThemeName = "Office2007Black";
            this.hotSpotFactorSpinEditor.Value = new decimal(new int[] {
            13,
            0,
            0,
            65536});
            // 
            // hotSpotFactorLabelText
            // 
            this.hotSpotFactorLabelText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.hotSpotFactorLabelText.Location = new System.Drawing.Point(6, 22);
            this.hotSpotFactorLabelText.Name = "hotSpotFactorLabelText";
            this.hotSpotFactorLabelText.Size = new System.Drawing.Size(85, 16);
            this.hotSpotFactorLabelText.TabIndex = 57;
            this.hotSpotFactorLabelText.Text = "Hot Spot Factor";
            // 
            // TopOilGroupBox
            // 
            this.TopOilGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.TopOilGroupBox.Controls.Add(this.topOilTemperatureComboBox);
            this.TopOilGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TopOilGroupBox.FooterImageIndex = -1;
            this.TopOilGroupBox.FooterImageKey = "";
            this.TopOilGroupBox.HeaderImageIndex = -1;
            this.TopOilGroupBox.HeaderImageKey = "";
            this.TopOilGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.TopOilGroupBox.HeaderText = "Top Oil Temperature Input";
            this.TopOilGroupBox.Location = new System.Drawing.Point(348, 7);
            this.TopOilGroupBox.Name = "TopOilGroupBox";
            this.TopOilGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.TopOilGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.TopOilGroupBox.Size = new System.Drawing.Size(157, 51);
            this.TopOilGroupBox.TabIndex = 57;
            this.TopOilGroupBox.Text = "Top Oil Temperature Input";
            this.TopOilGroupBox.ThemeName = "Office2007Black";
            // 
            // topOilTemperatureComboBox
            // 
            this.topOilTemperatureComboBox.DefaultItemsCountInDropDown = 7;
            this.topOilTemperatureComboBox.DropDownAnimationEnabled = true;
            this.topOilTemperatureComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topOilTemperatureComboBox.Location = new System.Drawing.Point(13, 23);
            this.topOilTemperatureComboBox.Name = "topOilTemperatureComboBox";
            this.topOilTemperatureComboBox.ShowImageInEditorArea = true;
            this.topOilTemperatureComboBox.Size = new System.Drawing.Size(122, 20);
            this.topOilTemperatureComboBox.TabIndex = 0;
            this.topOilTemperatureComboBox.ThemeName = "Office2007Black";
            // 
            // agingCalculationMethodRadGroupBox
            // 
            this.agingCalculationMethodRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.agingCalculationMethodRadGroupBox.Controls.Add(this.agingCalculationMethodRadDropDownList);
            this.agingCalculationMethodRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agingCalculationMethodRadGroupBox.FooterImageIndex = -1;
            this.agingCalculationMethodRadGroupBox.FooterImageKey = "";
            this.agingCalculationMethodRadGroupBox.HeaderImageIndex = -1;
            this.agingCalculationMethodRadGroupBox.HeaderImageKey = "";
            this.agingCalculationMethodRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.agingCalculationMethodRadGroupBox.HeaderText = "Aging calculation method";
            this.agingCalculationMethodRadGroupBox.Location = new System.Drawing.Point(169, 7);
            this.agingCalculationMethodRadGroupBox.Name = "agingCalculationMethodRadGroupBox";
            this.agingCalculationMethodRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.agingCalculationMethodRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.agingCalculationMethodRadGroupBox.Size = new System.Drawing.Size(157, 51);
            this.agingCalculationMethodRadGroupBox.TabIndex = 48;
            this.agingCalculationMethodRadGroupBox.Text = "Aging calculation method";
            this.agingCalculationMethodRadGroupBox.ThemeName = "Office2007Black";
            // 
            // agingCalculationMethodRadDropDownList
            // 
            this.agingCalculationMethodRadDropDownList.DropDownAnimationEnabled = true;
            this.agingCalculationMethodRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agingCalculationMethodRadDropDownList.Location = new System.Drawing.Point(13, 23);
            this.agingCalculationMethodRadDropDownList.Name = "agingCalculationMethodRadDropDownList";
            this.agingCalculationMethodRadDropDownList.ShowImageInEditorArea = true;
            this.agingCalculationMethodRadDropDownList.Size = new System.Drawing.Size(122, 20);
            this.agingCalculationMethodRadDropDownList.TabIndex = 29;
            this.agingCalculationMethodRadDropDownList.ThemeName = "Office2007Black";
            // 
            // templateConfigurationsGeneralSettingsTabRadGroupBox
            // 
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationGeneralSettingsTabRadButton);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsGeneralSettingsTabRadDropDownList);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Controls.Add(this.radButton1);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(763, 362);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Name = "templateConfigurationsGeneralSettingsTabRadGroupBox";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(72, 38);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.TabIndex = 47;
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.Visible = false;
            // 
            // copySelectedConfigurationGeneralSettingsTabRadButton
            // 
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Location = new System.Drawing.Point(-56, -16);
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Name = "copySelectedConfigurationGeneralSettingsTabRadButton";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationGeneralSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationGeneralSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsGeneralSettingsTabRadDropDownList
            // 
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 12);
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Name = "templateConfigurationsGeneralSettingsTabRadDropDownList";
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // deviceInteractionWhsGeneralSettingsTabRadGroupBox
            // 
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.programDeviceWhsGeneralSettingsTabRadButton);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(686, 489);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Name = "deviceInteractionWhsGeneralSettingsTabRadGroupBox";
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.TabIndex = 46;
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceWhsGeneralSettingsTabRadButton
            // 
            this.programDeviceWhsGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceWhsGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceWhsGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceWhsGeneralSettingsTabRadButton.Name = "programDeviceWhsGeneralSettingsTabRadButton";
            this.programDeviceWhsGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceWhsGeneralSettingsTabRadButton.TabIndex = 30;
            this.programDeviceWhsGeneralSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceWhsGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceWhsGeneralSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Name = "loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton";
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.TabIndex = 34;
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // databaseInteractionWhsGeneralSettingsTabRadGroupBox
            // 
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationGeneralSettingsTabRadButton);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(13, 489);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Name = "databaseInteractionWhsGeneralSettingsTabRadGroupBox";
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.TabIndex = 45;
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationGeneralSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Name = "deleteSelectedConfigurationGeneralSettingsTabRadButton";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.TabIndex = 48;
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationGeneralSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationWhsGeneralSettingsTabRadButton
            // 
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Name = "saveWorkingConfigurationWhsGeneralSettingsTabRadButton";
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.TabIndex = 31;
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Name = "loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton";
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.TabIndex = 33;
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // transformerTypeRadGroupBox
            // 
            this.transformerTypeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.transformerTypeRadGroupBox.Controls.Add(this.transformerTypeRadDropDownList);
            this.transformerTypeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transformerTypeRadGroupBox.FooterImageIndex = -1;
            this.transformerTypeRadGroupBox.FooterImageKey = "";
            this.transformerTypeRadGroupBox.HeaderImageIndex = -1;
            this.transformerTypeRadGroupBox.HeaderImageKey = "";
            this.transformerTypeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.transformerTypeRadGroupBox.HeaderText = "Transformer type";
            this.transformerTypeRadGroupBox.Location = new System.Drawing.Point(740, 395);
            this.transformerTypeRadGroupBox.Name = "transformerTypeRadGroupBox";
            this.transformerTypeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.transformerTypeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.transformerTypeRadGroupBox.Size = new System.Drawing.Size(44, 41);
            this.transformerTypeRadGroupBox.TabIndex = 44;
            this.transformerTypeRadGroupBox.Text = "Transformer type";
            this.transformerTypeRadGroupBox.ThemeName = "Office2007Black";
            this.transformerTypeRadGroupBox.Visible = false;
            // 
            // transformerTypeRadDropDownList
            // 
            this.transformerTypeRadDropDownList.DropDownAnimationEnabled = true;
            this.transformerTypeRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transformerTypeRadDropDownList.Location = new System.Drawing.Point(13, 23);
            this.transformerTypeRadDropDownList.Name = "transformerTypeRadDropDownList";
            this.transformerTypeRadDropDownList.ShowImageInEditorArea = true;
            this.transformerTypeRadDropDownList.Size = new System.Drawing.Size(122, 20);
            this.transformerTypeRadDropDownList.TabIndex = 29;
            this.transformerTypeRadDropDownList.ThemeName = "Office2007Black";
            this.transformerTypeRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.transformerTypeRadDropDownList_SelectedIndexChanged);
            this.transformerTypeRadDropDownList.SelectedIndexChanging += new Telerik.WinControls.UI.Data.PositionChangingEventHandler(this.transformerTypeRadDropDownList_SelectedIndexChanging);
            // 
            // transformerConfigurationRadGroupBox
            // 
            this.transformerConfigurationRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.transformerConfigurationRadGroupBox.Controls.Add(this.transformerConfigurationRadDropDownList);
            this.transformerConfigurationRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transformerConfigurationRadGroupBox.FooterImageIndex = -1;
            this.transformerConfigurationRadGroupBox.FooterImageKey = "";
            this.transformerConfigurationRadGroupBox.HeaderImageIndex = -1;
            this.transformerConfigurationRadGroupBox.HeaderImageKey = "";
            this.transformerConfigurationRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.transformerConfigurationRadGroupBox.HeaderText = "Transformer configuration";
            this.transformerConfigurationRadGroupBox.Location = new System.Drawing.Point(664, 395);
            this.transformerConfigurationRadGroupBox.Name = "transformerConfigurationRadGroupBox";
            this.transformerConfigurationRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.transformerConfigurationRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.transformerConfigurationRadGroupBox.Size = new System.Drawing.Size(54, 41);
            this.transformerConfigurationRadGroupBox.TabIndex = 43;
            this.transformerConfigurationRadGroupBox.Text = "Transformer configuration";
            this.transformerConfigurationRadGroupBox.ThemeName = "Office2007Black";
            this.transformerConfigurationRadGroupBox.Visible = false;
            // 
            // transformerConfigurationRadDropDownList
            // 
            this.transformerConfigurationRadDropDownList.DropDownAnimationEnabled = true;
            this.transformerConfigurationRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transformerConfigurationRadDropDownList.Location = new System.Drawing.Point(13, 23);
            this.transformerConfigurationRadDropDownList.Name = "transformerConfigurationRadDropDownList";
            this.transformerConfigurationRadDropDownList.ShowImageInEditorArea = true;
            this.transformerConfigurationRadDropDownList.Size = new System.Drawing.Size(137, 20);
            this.transformerConfigurationRadDropDownList.TabIndex = 29;
            this.transformerConfigurationRadDropDownList.ThemeName = "Office2007Black";
            this.transformerConfigurationRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.transformerConfigurationRadDropDownList_SelectedIndexChanged);
            this.transformerConfigurationRadDropDownList.SelectedIndexChanging += new Telerik.WinControls.UI.Data.PositionChangingEventHandler(this.transformerConfigurationRadDropDownList_SelectedIndexChanging);
            // 
            // maximumRatedCurrentRadGroupBox
            // 
            this.maximumRatedCurrentRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.maximumRatedCurrentRadGroupBox.Controls.Add(this.maximumRatedCurrentRadGridView);
            this.maximumRatedCurrentRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maximumRatedCurrentRadGroupBox.FooterImageIndex = -1;
            this.maximumRatedCurrentRadGroupBox.FooterImageKey = "";
            this.maximumRatedCurrentRadGroupBox.HeaderImageIndex = -1;
            this.maximumRatedCurrentRadGroupBox.HeaderImageKey = "";
            this.maximumRatedCurrentRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.maximumRatedCurrentRadGroupBox.HeaderText = "Maximum Rated Current";
            this.maximumRatedCurrentRadGroupBox.Location = new System.Drawing.Point(790, 388);
            this.maximumRatedCurrentRadGroupBox.Name = "maximumRatedCurrentRadGroupBox";
            this.maximumRatedCurrentRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.maximumRatedCurrentRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.maximumRatedCurrentRadGroupBox.Size = new System.Drawing.Size(45, 48);
            this.maximumRatedCurrentRadGroupBox.TabIndex = 28;
            this.maximumRatedCurrentRadGroupBox.Text = "Maximum Rated Current";
            this.maximumRatedCurrentRadGroupBox.ThemeName = "Office2007Black";
            this.maximumRatedCurrentRadGroupBox.Visible = false;
            // 
            // maximumRatedCurrentRadGridView
            // 
            this.maximumRatedCurrentRadGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maximumRatedCurrentRadGridView.Location = new System.Drawing.Point(10, 20);
            // 
            // maximumRatedCurrentRadGridView
            // 
            this.maximumRatedCurrentRadGridView.MasterTemplate.HorizontalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.maximumRatedCurrentRadGridView.MasterTemplate.VerticalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.maximumRatedCurrentRadGridView.Name = "maximumRatedCurrentRadGridView";
            this.maximumRatedCurrentRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.maximumRatedCurrentRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.maximumRatedCurrentRadGridView.Size = new System.Drawing.Size(25, 18);
            this.maximumRatedCurrentRadGridView.TabIndex = 1;
            this.maximumRatedCurrentRadGridView.Text = "radGridView1";
            this.maximumRatedCurrentRadGridView.ThemeName = "Office2007Black";
            // 
            // whsGeneralSettingsRadProgressBar
            // 
            this.whsGeneralSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.whsGeneralSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.whsGeneralSettingsRadProgressBar.ImageIndex = -1;
            this.whsGeneralSettingsRadProgressBar.ImageKey = "";
            this.whsGeneralSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.whsGeneralSettingsRadProgressBar.Location = new System.Drawing.Point(569, 453);
            this.whsGeneralSettingsRadProgressBar.Name = "whsGeneralSettingsRadProgressBar";
            this.whsGeneralSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.whsGeneralSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.whsGeneralSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.whsGeneralSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.whsGeneralSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.whsGeneralSettingsRadProgressBar.TabIndex = 42;
            this.whsGeneralSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsWhsGeneralSettingsTabRadGroupBox
            // 
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsWhsGeneralSettingsTabRadListControl);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Controls.Add(this.radButton5);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Location = new System.Drawing.Point(170, 489);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Name = "availableConfigurationsWhsGeneralSettingsTabRadGroupBox";
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Size = new System.Drawing.Size(503, 192);
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.TabIndex = 32;
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsWhsGeneralSettingsTabRadListControl
            // 
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.Name = "availableConfigurationsWhsGeneralSettingsTabRadListControl";
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.Size = new System.Drawing.Size(483, 162);
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsWhsGeneralSettingsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsWhsGeneralSettingsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton5
            // 
            this.radButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton5.Location = new System.Drawing.Point(0, 232);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(130, 70);
            this.radButton5.TabIndex = 8;
            this.radButton5.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton5.ThemeName = "Office2007Black";
            // 
            // firmwareVersionRadGroupBox
            // 
            this.firmwareVersionRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.firmwareVersionRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.firmwareVersionRadGroupBox.Controls.Add(this.firmwareVersionValueRadLabel);
            this.firmwareVersionRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firmwareVersionRadGroupBox.FooterImageIndex = -1;
            this.firmwareVersionRadGroupBox.FooterImageKey = "";
            this.firmwareVersionRadGroupBox.HeaderImageIndex = -1;
            this.firmwareVersionRadGroupBox.HeaderImageKey = "";
            this.firmwareVersionRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.firmwareVersionRadGroupBox.HeaderText = "Firmware Version";
            this.firmwareVersionRadGroupBox.Location = new System.Drawing.Point(714, 7);
            this.firmwareVersionRadGroupBox.Name = "firmwareVersionRadGroupBox";
            this.firmwareVersionRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.firmwareVersionRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.firmwareVersionRadGroupBox.Size = new System.Drawing.Size(121, 51);
            this.firmwareVersionRadGroupBox.TabIndex = 26;
            this.firmwareVersionRadGroupBox.Text = "Firmware Version";
            this.firmwareVersionRadGroupBox.ThemeName = "Office2007Black";
            // 
            // firmwareVersionValueRadLabel
            // 
            this.firmwareVersionValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.firmwareVersionValueRadLabel.Location = new System.Drawing.Point(33, 25);
            this.firmwareVersionValueRadLabel.Name = "firmwareVersionValueRadLabel";
            this.firmwareVersionValueRadLabel.Size = new System.Drawing.Size(52, 16);
            this.firmwareVersionValueRadLabel.TabIndex = 50;
            this.firmwareVersionValueRadLabel.Text = "unknown";
            // 
            // tempRiseSettingsRadGroupBox
            // 
            this.tempRiseSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.tempRiseSettingsRadGroupBox.Controls.Add(this.temperatureRiseOverTopOilRadGridView);
            this.tempRiseSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempRiseSettingsRadGroupBox.FooterImageIndex = -1;
            this.tempRiseSettingsRadGroupBox.FooterImageKey = "";
            this.tempRiseSettingsRadGroupBox.HeaderImageIndex = -1;
            this.tempRiseSettingsRadGroupBox.HeaderImageKey = "";
            this.tempRiseSettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.tempRiseSettingsRadGroupBox.HeaderText = "Temperature Rise Over Top Oil Settings";
            this.tempRiseSettingsRadGroupBox.Location = new System.Drawing.Point(14, 71);
            this.tempRiseSettingsRadGroupBox.Name = "tempRiseSettingsRadGroupBox";
            this.tempRiseSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.tempRiseSettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.tempRiseSettingsRadGroupBox.Size = new System.Drawing.Size(594, 168);
            this.tempRiseSettingsRadGroupBox.TabIndex = 24;
            this.tempRiseSettingsRadGroupBox.Text = "Temperature Rise Over Top Oil Settings";
            this.tempRiseSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // temperatureRiseOverTopOilRadGridView
            // 
            this.temperatureRiseOverTopOilRadGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.temperatureRiseOverTopOilRadGridView.Location = new System.Drawing.Point(10, 20);
            // 
            // temperatureRiseOverTopOilRadGridView
            // 
            this.temperatureRiseOverTopOilRadGridView.MasterTemplate.HorizontalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.temperatureRiseOverTopOilRadGridView.MasterTemplate.VerticalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.temperatureRiseOverTopOilRadGridView.Name = "temperatureRiseOverTopOilRadGridView";
            this.temperatureRiseOverTopOilRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.temperatureRiseOverTopOilRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.temperatureRiseOverTopOilRadGridView.Size = new System.Drawing.Size(574, 138);
            this.temperatureRiseOverTopOilRadGridView.TabIndex = 0;
            this.temperatureRiseOverTopOilRadGridView.Text = "radGridView1";
            this.temperatureRiseOverTopOilRadGridView.ThemeName = "Office2007Black";
            // 
            // whsCalculationEnableDisableRadGroupBox
            // 
            this.whsCalculationEnableDisableRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.whsCalculationEnableDisableRadGroupBox.Controls.Add(this.disableMonitoringRadRadioButton);
            this.whsCalculationEnableDisableRadGroupBox.Controls.Add(this.enableMonitoringRadRadioButton);
            this.whsCalculationEnableDisableRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whsCalculationEnableDisableRadGroupBox.FooterImageIndex = -1;
            this.whsCalculationEnableDisableRadGroupBox.FooterImageKey = "";
            this.whsCalculationEnableDisableRadGroupBox.HeaderImageIndex = -1;
            this.whsCalculationEnableDisableRadGroupBox.HeaderImageKey = "";
            this.whsCalculationEnableDisableRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.whsCalculationEnableDisableRadGroupBox.HeaderText = "WHS Calculations";
            this.whsCalculationEnableDisableRadGroupBox.Location = new System.Drawing.Point(13, 7);
            this.whsCalculationEnableDisableRadGroupBox.Name = "whsCalculationEnableDisableRadGroupBox";
            this.whsCalculationEnableDisableRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.whsCalculationEnableDisableRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.whsCalculationEnableDisableRadGroupBox.Size = new System.Drawing.Size(138, 51);
            this.whsCalculationEnableDisableRadGroupBox.TabIndex = 22;
            this.whsCalculationEnableDisableRadGroupBox.Text = "WHS Calculations";
            this.whsCalculationEnableDisableRadGroupBox.ThemeName = "Office2007Black";
            // 
            // disableMonitoringRadRadioButton
            // 
            this.disableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.disableMonitoringRadRadioButton.Location = new System.Drawing.Point(75, 23);
            this.disableMonitoringRadRadioButton.Name = "disableMonitoringRadRadioButton";
            this.disableMonitoringRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.disableMonitoringRadRadioButton.TabIndex = 1;
            this.disableMonitoringRadRadioButton.TabStop = true;
            this.disableMonitoringRadRadioButton.Text = "Disable";
            this.disableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // enableMonitoringRadRadioButton
            // 
            this.enableMonitoringRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableMonitoringRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.enableMonitoringRadRadioButton.Location = new System.Drawing.Point(10, 23);
            this.enableMonitoringRadRadioButton.Name = "enableMonitoringRadRadioButton";
            this.enableMonitoringRadRadioButton.Size = new System.Drawing.Size(59, 18);
            this.enableMonitoringRadRadioButton.TabIndex = 0;
            this.enableMonitoringRadRadioButton.Text = "Enable";
            // 
            // calculationSettingsRadPageViewPage
            // 
            this.calculationSettingsRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.calculationSettingsRadPageViewPage.Controls.Add(this.radGroupBox2);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.templateConfigurationsCalculationSettingsTabRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.deviceInteractionCalculationSettingsTabRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.databaseInteractionCalculationSettingsTabRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.algorithmVariantRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.timeConstantRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.exponentRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.previousAgingRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.currentInputsRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.temperatureInputsRadGroupBox);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.calculationSettingsRadProgressBar);
            this.calculationSettingsRadPageViewPage.Controls.Add(this.availableConfigurationsCalculationSettingsTabRadGroupBox);
            this.calculationSettingsRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculationSettingsRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.calculationSettingsRadPageViewPage.Name = "calculationSettingsRadPageViewPage";
            this.calculationSettingsRadPageViewPage.Size = new System.Drawing.Size(851, 684);
            this.calculationSettingsRadPageViewPage.Text = "Calculation Settings";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseC);
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseB);
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseA);
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseCLabel);
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseBLabel);
            this.radGroupBox2.Controls.Add(this.PreviousAgingDaysPhaseALabel);
            this.radGroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGroupBox2.FooterImageIndex = -1;
            this.radGroupBox2.FooterImageKey = "";
            this.radGroupBox2.HeaderImageIndex = -1;
            this.radGroupBox2.HeaderImageKey = "";
            this.radGroupBox2.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.radGroupBox2.HeaderText = "Previous aging - Days";
            this.radGroupBox2.Location = new System.Drawing.Point(13, 265);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(198, 125);
            this.radGroupBox2.TabIndex = 53;
            this.radGroupBox2.Text = "Previous aging - Days";
            this.radGroupBox2.ThemeName = "Office2007Black";
            this.radGroupBox2.Visible = false;
            // 
            // PreviousAgingDaysPhaseC
            // 
            this.PreviousAgingDaysPhaseC.Location = new System.Drawing.Point(100, 90);
            this.PreviousAgingDaysPhaseC.Name = "PreviousAgingDaysPhaseC";
            this.PreviousAgingDaysPhaseC.Size = new System.Drawing.Size(54, 20);
            this.PreviousAgingDaysPhaseC.TabIndex = 56;
            this.PreviousAgingDaysPhaseC.TabStop = false;
            this.PreviousAgingDaysPhaseC.Text = "0";
            this.PreviousAgingDaysPhaseC.ThemeName = "ControlDefault";
            // 
            // PreviousAgingDaysPhaseB
            // 
            this.PreviousAgingDaysPhaseB.Location = new System.Drawing.Point(100, 57);
            this.PreviousAgingDaysPhaseB.Name = "PreviousAgingDaysPhaseB";
            this.PreviousAgingDaysPhaseB.Size = new System.Drawing.Size(54, 20);
            this.PreviousAgingDaysPhaseB.TabIndex = 56;
            this.PreviousAgingDaysPhaseB.TabStop = false;
            this.PreviousAgingDaysPhaseB.Text = "0";
            this.PreviousAgingDaysPhaseB.ThemeName = "ControlDefault";
            // 
            // PreviousAgingDaysPhaseA
            // 
            this.PreviousAgingDaysPhaseA.Location = new System.Drawing.Point(100, 23);
            this.PreviousAgingDaysPhaseA.Name = "PreviousAgingDaysPhaseA";
            this.PreviousAgingDaysPhaseA.Size = new System.Drawing.Size(54, 20);
            this.PreviousAgingDaysPhaseA.TabIndex = 55;
            this.PreviousAgingDaysPhaseA.TabStop = false;
            this.PreviousAgingDaysPhaseA.Text = "0";
            this.PreviousAgingDaysPhaseA.ThemeName = "ControlDefault";
            // 
            // PreviousAgingDaysPhaseCLabel
            // 
            this.PreviousAgingDaysPhaseCLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.PreviousAgingDaysPhaseCLabel.Location = new System.Drawing.Point(13, 94);
            this.PreviousAgingDaysPhaseCLabel.Name = "PreviousAgingDaysPhaseCLabel";
            this.PreviousAgingDaysPhaseCLabel.Size = new System.Drawing.Size(49, 16);
            this.PreviousAgingDaysPhaseCLabel.TabIndex = 52;
            this.PreviousAgingDaysPhaseCLabel.Text = "Phase C";
            // 
            // PreviousAgingDaysPhaseBLabel
            // 
            this.PreviousAgingDaysPhaseBLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.PreviousAgingDaysPhaseBLabel.Location = new System.Drawing.Point(13, 57);
            this.PreviousAgingDaysPhaseBLabel.Name = "PreviousAgingDaysPhaseBLabel";
            this.PreviousAgingDaysPhaseBLabel.Size = new System.Drawing.Size(49, 16);
            this.PreviousAgingDaysPhaseBLabel.TabIndex = 51;
            this.PreviousAgingDaysPhaseBLabel.Text = "Phase B";
            // 
            // PreviousAgingDaysPhaseALabel
            // 
            this.PreviousAgingDaysPhaseALabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.PreviousAgingDaysPhaseALabel.Location = new System.Drawing.Point(13, 25);
            this.PreviousAgingDaysPhaseALabel.Name = "PreviousAgingDaysPhaseALabel";
            this.PreviousAgingDaysPhaseALabel.Size = new System.Drawing.Size(49, 16);
            this.PreviousAgingDaysPhaseALabel.TabIndex = 50;
            this.PreviousAgingDaysPhaseALabel.Text = "Phase A";
            // 
            // templateConfigurationsCalculationSettingsTabRadGroupBox
            // 
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Controls.Add(this.copySelectedConfigurationCalculationSettingsTabRadButton);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Controls.Add(this.templateConfigurationsCalculationSettingsTabRadDropDownList);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Controls.Add(this.radButton4);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 419);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Name = "templateConfigurationsCalculationSettingsTabRadGroupBox";
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Size = new System.Drawing.Size(537, 65);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.TabIndex = 51;
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // copySelectedConfigurationCalculationSettingsTabRadButton
            // 
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Location = new System.Drawing.Point(409, 11);
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Name = "copySelectedConfigurationCalculationSettingsTabRadButton";
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationCalculationSettingsTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationCalculationSettingsTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsCalculationSettingsTabRadDropDownList
            // 
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.Name = "templateConfigurationsCalculationSettingsTabRadDropDownList";
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsCalculationSettingsTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsCalculationSettingsTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton4
            // 
            this.radButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton4.Location = new System.Drawing.Point(0, 232);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(130, 70);
            this.radButton4.TabIndex = 8;
            this.radButton4.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton4.ThemeName = "Office2007Black";
            // 
            // deviceInteractionCalculationSettingsTabRadGroupBox
            // 
            this.deviceInteractionCalculationSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceCalculationSettingsTabRadButton);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Controls.Add(this.programDeviceCalculationSettingsTabRadButton);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionCalculationSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionCalculationSettingsTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionCalculationSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionCalculationSettingsTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionCalculationSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Location = new System.Drawing.Point(697, 489);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Name = "deviceInteractionCalculationSettingsTabRadGroupBox";
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionCalculationSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionCalculationSettingsTabRadGroupBox.TabIndex = 47;
            this.deviceInteractionCalculationSettingsTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionCalculationSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // loadConfigurationFromDeviceCalculationSettingsTabRadButton
            // 
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Name = "loadConfigurationFromDeviceCalculationSettingsTabRadButton";
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.TabIndex = 26;
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceCalculationSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // programDeviceCalculationSettingsTabRadButton
            // 
            this.programDeviceCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceCalculationSettingsTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceCalculationSettingsTabRadButton.Name = "programDeviceCalculationSettingsTabRadButton";
            this.programDeviceCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceCalculationSettingsTabRadButton.TabIndex = 22;
            this.programDeviceCalculationSettingsTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceCalculationSettingsTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // databaseInteractionCalculationSettingsTabRadGroupBox
            // 
            this.databaseInteractionCalculationSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationCalculationSettingsTabRadButton);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationCalculationSettingsTabRadButton);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionCalculationSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionCalculationSettingsTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionCalculationSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionCalculationSettingsTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionCalculationSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Location = new System.Drawing.Point(543, 489);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Name = "databaseInteractionCalculationSettingsTabRadGroupBox";
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionCalculationSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionCalculationSettingsTabRadGroupBox.TabIndex = 50;
            this.databaseInteractionCalculationSettingsTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionCalculationSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationCalculationSettingsTabRadButton
            // 
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Name = "deleteSelectedConfigurationCalculationSettingsTabRadButton";
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.TabIndex = 24;
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationCalculationSettingsTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationCalculationSettingsTabRadButton
            // 
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Name = "saveWorkingConfigurationCalculationSettingsTabRadButton";
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.TabIndex = 23;
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationCalculationSettingsTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseCalculationSettingsTabRadButton
            // 
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Name = "loadConfigurationFromDatabaseCalculationSettingsTabRadButton";
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.TabIndex = 25;
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // algorithmVariantRadGroupBox
            // 
            this.algorithmVariantRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.algorithmVariantRadGroupBox.Controls.Add(this.algorithmVariantRadDropDownList);
            this.algorithmVariantRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.algorithmVariantRadGroupBox.FooterImageIndex = -1;
            this.algorithmVariantRadGroupBox.FooterImageKey = "";
            this.algorithmVariantRadGroupBox.HeaderImageIndex = -1;
            this.algorithmVariantRadGroupBox.HeaderImageKey = "";
            this.algorithmVariantRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.algorithmVariantRadGroupBox.HeaderText = "Algorithm variant";
            this.algorithmVariantRadGroupBox.Location = new System.Drawing.Point(577, 192);
            this.algorithmVariantRadGroupBox.Name = "algorithmVariantRadGroupBox";
            this.algorithmVariantRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.algorithmVariantRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.algorithmVariantRadGroupBox.Size = new System.Drawing.Size(157, 51);
            this.algorithmVariantRadGroupBox.TabIndex = 49;
            this.algorithmVariantRadGroupBox.Text = "Algorithm variant";
            this.algorithmVariantRadGroupBox.ThemeName = "Office2007Black";
            this.algorithmVariantRadGroupBox.Visible = false;
            // 
            // algorithmVariantRadDropDownList
            // 
            this.algorithmVariantRadDropDownList.DropDownAnimationEnabled = true;
            this.algorithmVariantRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.algorithmVariantRadDropDownList.Location = new System.Drawing.Point(13, 23);
            this.algorithmVariantRadDropDownList.Name = "algorithmVariantRadDropDownList";
            this.algorithmVariantRadDropDownList.ShowImageInEditorArea = true;
            this.algorithmVariantRadDropDownList.Size = new System.Drawing.Size(122, 20);
            this.algorithmVariantRadDropDownList.TabIndex = 29;
            this.algorithmVariantRadDropDownList.ThemeName = "Office2007Black";
            this.algorithmVariantRadDropDownList.Visible = false;
            // 
            // timeConstantRadGroupBox
            // 
            this.timeConstantRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.timeConstantRadGroupBox.Controls.Add(this.timeContantMinutesRadLabel);
            this.timeConstantRadGroupBox.Controls.Add(this.timeConstantRadSpinEditor);
            this.timeConstantRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeConstantRadGroupBox.FooterImageIndex = -1;
            this.timeConstantRadGroupBox.FooterImageKey = "";
            this.timeConstantRadGroupBox.HeaderImageIndex = -1;
            this.timeConstantRadGroupBox.HeaderImageKey = "";
            this.timeConstantRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.timeConstantRadGroupBox.HeaderText = "Time constant";
            this.timeConstantRadGroupBox.Location = new System.Drawing.Point(577, 130);
            this.timeConstantRadGroupBox.Name = "timeConstantRadGroupBox";
            this.timeConstantRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.timeConstantRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.timeConstantRadGroupBox.Size = new System.Drawing.Size(157, 53);
            this.timeConstantRadGroupBox.TabIndex = 48;
            this.timeConstantRadGroupBox.Text = "Time constant";
            this.timeConstantRadGroupBox.ThemeName = "Office2007Black";
            // 
            // timeContantMinutesRadLabel
            // 
            this.timeContantMinutesRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeContantMinutesRadLabel.Location = new System.Drawing.Point(103, 23);
            this.timeContantMinutesRadLabel.Name = "timeContantMinutesRadLabel";
            this.timeContantMinutesRadLabel.Size = new System.Drawing.Size(46, 16);
            this.timeContantMinutesRadLabel.TabIndex = 49;
            this.timeContantMinutesRadLabel.Text = "minutes";
            // 
            // timeConstantRadSpinEditor
            // 
            this.timeConstantRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeConstantRadSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.timeConstantRadSpinEditor.Location = new System.Drawing.Point(13, 23);
            this.timeConstantRadSpinEditor.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            65536});
            this.timeConstantRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.timeConstantRadSpinEditor.Name = "timeConstantRadSpinEditor";
            // 
            // 
            // 
            this.timeConstantRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.timeConstantRadSpinEditor.ShowBorder = true;
            this.timeConstantRadSpinEditor.Size = new System.Drawing.Size(76, 19);
            this.timeConstantRadSpinEditor.TabIndex = 0;
            this.timeConstantRadSpinEditor.TabStop = false;
            this.timeConstantRadSpinEditor.ThemeName = "Office2007Black";
            this.timeConstantRadSpinEditor.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // exponentRadGroupBox
            // 
            this.exponentRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.exponentRadGroupBox.Controls.Add(this.exponentRadSpinEditor);
            this.exponentRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exponentRadGroupBox.FooterImageIndex = -1;
            this.exponentRadGroupBox.FooterImageKey = "";
            this.exponentRadGroupBox.HeaderImageIndex = -1;
            this.exponentRadGroupBox.HeaderImageKey = "";
            this.exponentRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.exponentRadGroupBox.HeaderText = "Exponent";
            this.exponentRadGroupBox.Location = new System.Drawing.Point(577, 68);
            this.exponentRadGroupBox.Name = "exponentRadGroupBox";
            this.exponentRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.exponentRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.exponentRadGroupBox.Size = new System.Drawing.Size(157, 53);
            this.exponentRadGroupBox.TabIndex = 47;
            this.exponentRadGroupBox.Text = "Exponent";
            this.exponentRadGroupBox.ThemeName = "Office2007Black";
            // 
            // exponentRadSpinEditor
            // 
            this.exponentRadSpinEditor.DecimalPlaces = 1;
            this.exponentRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exponentRadSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.exponentRadSpinEditor.Location = new System.Drawing.Point(13, 23);
            this.exponentRadSpinEditor.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            this.exponentRadSpinEditor.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.exponentRadSpinEditor.Name = "exponentRadSpinEditor";
            // 
            // 
            // 
            this.exponentRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.exponentRadSpinEditor.ShowBorder = true;
            this.exponentRadSpinEditor.Size = new System.Drawing.Size(76, 19);
            this.exponentRadSpinEditor.TabIndex = 0;
            this.exponentRadSpinEditor.TabStop = false;
            this.exponentRadSpinEditor.ThemeName = "Office2007Black";
            this.exponentRadSpinEditor.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            // 
            // previousAgingRadGroupBox
            // 
            this.previousAgingRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.previousAgingRadGroupBox.Controls.Add(this.valueSetInpedentlyFromOtherSettingsRadLabel);
            this.previousAgingRadGroupBox.Controls.Add(this.agingDaysTextRadLabel);
            this.previousAgingRadGroupBox.Controls.Add(this.setAgingDaysRadButton);
            this.previousAgingRadGroupBox.Controls.Add(this.previousAgingInDaysRadSpinEditor);
            this.previousAgingRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.previousAgingRadGroupBox.FooterImageIndex = -1;
            this.previousAgingRadGroupBox.FooterImageKey = "";
            this.previousAgingRadGroupBox.HeaderImageIndex = -1;
            this.previousAgingRadGroupBox.HeaderImageKey = "";
            this.previousAgingRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.previousAgingRadGroupBox.HeaderText = "Previous aging";
            this.previousAgingRadGroupBox.Location = new System.Drawing.Point(546, 312);
            this.previousAgingRadGroupBox.Name = "previousAgingRadGroupBox";
            this.previousAgingRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.previousAgingRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.previousAgingRadGroupBox.Size = new System.Drawing.Size(198, 99);
            this.previousAgingRadGroupBox.TabIndex = 46;
            this.previousAgingRadGroupBox.Text = "Previous aging";
            this.previousAgingRadGroupBox.ThemeName = "Office2007Black";
            this.previousAgingRadGroupBox.Visible = false;
            // 
            // valueSetInpedentlyFromOtherSettingsRadLabel
            // 
            this.valueSetInpedentlyFromOtherSettingsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.valueSetInpedentlyFromOtherSettingsRadLabel.Location = new System.Drawing.Point(13, 56);
            this.valueSetInpedentlyFromOtherSettingsRadLabel.Name = "valueSetInpedentlyFromOtherSettingsRadLabel";
            this.valueSetInpedentlyFromOtherSettingsRadLabel.Size = new System.Drawing.Size(175, 27);
            this.valueSetInpedentlyFromOtherSettingsRadLabel.TabIndex = 49;
            this.valueSetInpedentlyFromOtherSettingsRadLabel.Text = "<html>Value must be set separately from<br>all other settings using the button</h" +
    "tml>";
            // 
            // agingDaysTextRadLabel
            // 
            this.agingDaysTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.agingDaysTextRadLabel.Location = new System.Drawing.Point(95, 25);
            this.agingDaysTextRadLabel.Name = "agingDaysTextRadLabel";
            this.agingDaysTextRadLabel.Size = new System.Drawing.Size(32, 16);
            this.agingDaysTextRadLabel.TabIndex = 48;
            this.agingDaysTextRadLabel.Text = "Days";
            // 
            // setAgingDaysRadButton
            // 
            this.setAgingDaysRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.setAgingDaysRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setAgingDaysRadButton.Location = new System.Drawing.Point(135, 19);
            this.setAgingDaysRadButton.Name = "setAgingDaysRadButton";
            this.setAgingDaysRadButton.Size = new System.Drawing.Size(49, 31);
            this.setAgingDaysRadButton.TabIndex = 47;
            this.setAgingDaysRadButton.Text = "<html>Set<br>value</html>";
            this.setAgingDaysRadButton.ThemeName = "Office2007Black";
            this.setAgingDaysRadButton.Click += new System.EventHandler(this.setAgingDaysRadButton_Click);
            // 
            // previousAgingInDaysRadSpinEditor
            // 
            this.previousAgingInDaysRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.previousAgingInDaysRadSpinEditor.Location = new System.Drawing.Point(13, 23);
            this.previousAgingInDaysRadSpinEditor.Maximum = new decimal(new int[] {
            40000,
            0,
            0,
            0});
            this.previousAgingInDaysRadSpinEditor.Name = "previousAgingInDaysRadSpinEditor";
            // 
            // 
            // 
            this.previousAgingInDaysRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.previousAgingInDaysRadSpinEditor.ShowBorder = true;
            this.previousAgingInDaysRadSpinEditor.Size = new System.Drawing.Size(76, 19);
            this.previousAgingInDaysRadSpinEditor.TabIndex = 0;
            this.previousAgingInDaysRadSpinEditor.TabStop = false;
            this.previousAgingInDaysRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // currentInputsRadGroupBox
            // 
            this.currentInputsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.currentInputsRadGroupBox.Controls.Add(this.currentInputsRadGridView);
            this.currentInputsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentInputsRadGroupBox.FooterImageIndex = -1;
            this.currentInputsRadGroupBox.FooterImageKey = "";
            this.currentInputsRadGroupBox.HeaderImageIndex = -1;
            this.currentInputsRadGroupBox.HeaderImageKey = "";
            this.currentInputsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.currentInputsRadGroupBox.HeaderText = "Current Inputs";
            this.currentInputsRadGroupBox.Location = new System.Drawing.Point(288, 7);
            this.currentInputsRadGroupBox.Name = "currentInputsRadGroupBox";
            this.currentInputsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.currentInputsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.currentInputsRadGroupBox.Size = new System.Drawing.Size(224, 313);
            this.currentInputsRadGroupBox.TabIndex = 44;
            this.currentInputsRadGroupBox.Text = "Current Inputs";
            this.currentInputsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // currentInputsRadGridView
            // 
            this.currentInputsRadGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentInputsRadGridView.Location = new System.Drawing.Point(10, 20);
            // 
            // 
            // 
            this.currentInputsRadGridView.MasterTemplate.HorizontalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.currentInputsRadGridView.MasterTemplate.VerticalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.currentInputsRadGridView.Name = "currentInputsRadGridView";
            this.currentInputsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.currentInputsRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.currentInputsRadGridView.Size = new System.Drawing.Size(204, 283);
            this.currentInputsRadGridView.TabIndex = 1;
            this.currentInputsRadGridView.Text = "radGridView1";
            this.currentInputsRadGridView.ThemeName = "Office2007Black";
            // 
            // temperatureInputsRadGroupBox
            // 
            this.temperatureInputsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.temperatureInputsRadGroupBox.Controls.Add(this.temperatureInputsRadGridView);
            this.temperatureInputsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureInputsRadGroupBox.FooterImageIndex = -1;
            this.temperatureInputsRadGroupBox.FooterImageKey = "";
            this.temperatureInputsRadGroupBox.HeaderImageIndex = -1;
            this.temperatureInputsRadGroupBox.HeaderImageKey = "";
            this.temperatureInputsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.temperatureInputsRadGroupBox.HeaderText = "Temperature Inputs";
            this.temperatureInputsRadGroupBox.Location = new System.Drawing.Point(26, 7);
            this.temperatureInputsRadGroupBox.Name = "temperatureInputsRadGroupBox";
            this.temperatureInputsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.temperatureInputsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.temperatureInputsRadGroupBox.Size = new System.Drawing.Size(224, 145);
            this.temperatureInputsRadGroupBox.TabIndex = 43;
            this.temperatureInputsRadGroupBox.Text = "Temperature Inputs";
            this.temperatureInputsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // temperatureInputsRadGridView
            // 
            this.temperatureInputsRadGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.temperatureInputsRadGridView.Location = new System.Drawing.Point(10, 20);
            // 
            // 
            // 
            this.temperatureInputsRadGridView.MasterTemplate.HorizontalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.temperatureInputsRadGridView.MasterTemplate.VerticalScrollState = Telerik.WinControls.UI.ScrollState.AlwaysHide;
            this.temperatureInputsRadGridView.Name = "temperatureInputsRadGridView";
            this.temperatureInputsRadGridView.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            // 
            // 
            // 
            this.temperatureInputsRadGridView.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.temperatureInputsRadGridView.Size = new System.Drawing.Size(204, 115);
            this.temperatureInputsRadGridView.TabIndex = 0;
            this.temperatureInputsRadGridView.Text = "radGridView1";
            this.temperatureInputsRadGridView.ThemeName = "Office2007Black";
            // 
            // calculationSettingsRadProgressBar
            // 
            this.calculationSettingsRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.calculationSettingsRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.calculationSettingsRadProgressBar.ImageIndex = -1;
            this.calculationSettingsRadProgressBar.ImageKey = "";
            this.calculationSettingsRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.calculationSettingsRadProgressBar.Location = new System.Drawing.Point(569, 453);
            this.calculationSettingsRadProgressBar.Name = "calculationSettingsRadProgressBar";
            this.calculationSettingsRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.calculationSettingsRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.calculationSettingsRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.calculationSettingsRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.calculationSettingsRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.calculationSettingsRadProgressBar.TabIndex = 42;
            this.calculationSettingsRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsCalculationSettingsTabRadGroupBox
            // 
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Controls.Add(this.availableConfigurationsCalculationSettingsTabRadListControl);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Controls.Add(this.radButton3);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Location = new System.Drawing.Point(3, 490);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Name = "availableConfigurationsCalculationSettingsTabRadGroupBox";
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Size = new System.Drawing.Size(537, 192);
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.TabIndex = 24;
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsCalculationSettingsTabRadListControl
            // 
            this.availableConfigurationsCalculationSettingsTabRadListControl.AutoScroll = true;
            this.availableConfigurationsCalculationSettingsTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsCalculationSettingsTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsCalculationSettingsTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsCalculationSettingsTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsCalculationSettingsTabRadListControl.Name = "availableConfigurationsCalculationSettingsTabRadListControl";
            this.availableConfigurationsCalculationSettingsTabRadListControl.Size = new System.Drawing.Size(517, 162);
            this.availableConfigurationsCalculationSettingsTabRadListControl.TabIndex = 9;
            this.availableConfigurationsCalculationSettingsTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsCalculationSettingsTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsCalculationSettingsTabRadListControl_SelectedIndexChanged);
            // 
            // radButton3
            // 
            this.radButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton3.Location = new System.Drawing.Point(0, 232);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(130, 70);
            this.radButton3.TabIndex = 8;
            this.radButton3.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton3.ThemeName = "Office2007Black";
            // 
            // alarmsAndFansRadPageViewPage
            // 
            this.alarmsAndFansRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.templateConfigurationsAlarmsAndFansTabRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.deviceInteractionAlarmsAndFansTabRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.databaseInteractionAlarmsAndFansTabRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.deadBandTemperatureRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.alarmSettingsRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.fanSettingsRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.alarmsAndFansRadProgressBar);
            this.alarmsAndFansRadPageViewPage.Controls.Add(this.availableConfigurationsAlarmsAndFansTabRadGroupBox);
            this.alarmsAndFansRadPageViewPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmsAndFansRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.alarmsAndFansRadPageViewPage.Name = "alarmsAndFansRadPageViewPage";
            this.alarmsAndFansRadPageViewPage.Size = new System.Drawing.Size(851, 684);
            this.alarmsAndFansRadPageViewPage.Text = "Alarms and Cooling Groups";
            // 
            // templateConfigurationsAlarmsAndFansTabRadGroupBox
            // 
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Controls.Add(this.copySelectedConfigurationAlarmsAndFansTabRadButton);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Controls.Add(this.templateConfigurationsAlarmsAndFansTabRadDropDownList);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Controls.Add(this.radButton7);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.FooterImageIndex = -1;
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.FooterImageKey = "";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.HeaderImageIndex = -1;
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.HeaderImageKey = "";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.HeaderText = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Location = new System.Drawing.Point(13, 409);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Name = "templateConfigurationsAlarmsAndFansTabRadGroupBox";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Size = new System.Drawing.Size(108, 65);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.TabIndex = 52;
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Text = "<html>Template Configurations</html>";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.ThemeName = "Office2007Black";
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.Visible = false;
            // 
            // copySelectedConfigurationAlarmsAndFansTabRadButton
            // 
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(-20, 11);
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Name = "copySelectedConfigurationAlarmsAndFansTabRadButton";
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.TabIndex = 34;
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Text = "<html>Copy Selected<br>Configuration to<br>Database</html>";
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.copySelectedConfigurationAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.copySelectedConfigurationRadButton_Click);
            // 
            // templateConfigurationsAlarmsAndFansTabRadDropDownList
            // 
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.DropDownAnimationEnabled = true;
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.Location = new System.Drawing.Point(10, 25);
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.Name = "templateConfigurationsAlarmsAndFansTabRadDropDownList";
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.ShowImageInEditorArea = true;
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.Size = new System.Drawing.Size(389, 20);
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.TabIndex = 30;
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.ThemeName = "Office2007Black";
            this.templateConfigurationsAlarmsAndFansTabRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.templateConfigurationsAlarmsAndFansTabRadDropDownList_SelectedIndexChanged);
            // 
            // radButton7
            // 
            this.radButton7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton7.Location = new System.Drawing.Point(0, 232);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(130, 70);
            this.radButton7.TabIndex = 8;
            this.radButton7.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton7.ThemeName = "Office2007Black";
            // 
            // deviceInteractionAlarmsAndFansTabRadGroupBox
            // 
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Controls.Add(this.programDeviceAlarmsAndFansTabRadButton);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Controls.Add(this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.FooterImageIndex = -1;
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.FooterImageKey = "";
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.HeaderImageIndex = -1;
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.HeaderImageKey = "";
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.HeaderText = "Device Interaction";
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Location = new System.Drawing.Point(686, 489);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Name = "deviceInteractionAlarmsAndFansTabRadGroupBox";
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.TabIndex = 47;
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.Text = "Device Interaction";
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // programDeviceAlarmsAndFansTabRadButton
            // 
            this.programDeviceAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.programDeviceAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.programDeviceAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(13, 37);
            this.programDeviceAlarmsAndFansTabRadButton.Name = "programDeviceAlarmsAndFansTabRadButton";
            this.programDeviceAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.programDeviceAlarmsAndFansTabRadButton.TabIndex = 28;
            this.programDeviceAlarmsAndFansTabRadButton.Text = "<html>Program Device</html>";
            this.programDeviceAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.programDeviceAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.programDeviceRadButton_Click);
            // 
            // loadConfigurationFromDeviceAlarmsAndFansTabRadButton
            // 
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(13, 119);
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Name = "loadConfigurationFromDeviceAlarmsAndFansTabRadButton";
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 60);
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.TabIndex = 32;
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Text = "<html>Load Configuration<br>from<br>Device</html>";
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDeviceRadButton_Click);
            // 
            // databaseInteractionAlarmsAndFansTabRadGroupBox
            // 
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Controls.Add(this.deleteSelectedConfigurationAlarmsAndFansTabRadButton);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Controls.Add(this.saveWorkingConfigurationAlarmsAndFansTabRadButton);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Controls.Add(this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.FooterImageIndex = -1;
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.FooterImageKey = "";
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.HeaderImageIndex = -1;
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.HeaderImageKey = "";
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.HeaderText = "Database Interaction";
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Location = new System.Drawing.Point(13, 489);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Name = "databaseInteractionAlarmsAndFansTabRadGroupBox";
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Size = new System.Drawing.Size(151, 192);
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.TabIndex = 47;
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.Text = "Database Interaction";
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deleteSelectedConfigurationAlarmsAndFansTabRadButton
            // 
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(13, 135);
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Name = "deleteSelectedConfigurationAlarmsAndFansTabRadButton";
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.TabIndex = 32;
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Text = "<html>Delete Selected<br>Configuration from<br>Database</html>";
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.deleteSelectedConfigurationAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.deleteConfigurationRadButton_Click);
            // 
            // saveWorkingConfigurationAlarmsAndFansTabRadButton
            // 
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(13, 79);
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Name = "saveWorkingConfigurationAlarmsAndFansTabRadButton";
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.TabIndex = 29;
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.saveWorkingConfigurationAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.saveWorkingConfigurationRadButton_Click);
            // 
            // loadConfigurationFromDatabaseAlarmsAndFansTabRadButton
            // 
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Location = new System.Drawing.Point(13, 23);
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Name = "loadConfigurationFromDatabaseAlarmsAndFansTabRadButton";
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Size = new System.Drawing.Size(125, 50);
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.TabIndex = 31;
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Text = "<html>Load Selected<br>Configuration from<br>Database</html>";
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.ThemeName = "Office2007Black";
            this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton.Click += new System.EventHandler(this.loadConfigurationFromDatabaseRadButton_Click);
            // 
            // deadBandTemperatureRadGroupBox
            // 
            this.deadBandTemperatureRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.deadBandTemperatureRadGroupBox.Controls.Add(this.deadBandTemperatureRadSpinEditor);
            this.deadBandTemperatureRadGroupBox.Controls.Add(this.deadBandTemperatureTextRadLabel);
            this.deadBandTemperatureRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deadBandTemperatureRadGroupBox.FooterImageIndex = -1;
            this.deadBandTemperatureRadGroupBox.FooterImageKey = "";
            this.deadBandTemperatureRadGroupBox.HeaderImageIndex = -1;
            this.deadBandTemperatureRadGroupBox.HeaderImageKey = "";
            this.deadBandTemperatureRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.deadBandTemperatureRadGroupBox.HeaderText = "Dead Band Temperature";
            this.deadBandTemperatureRadGroupBox.Location = new System.Drawing.Point(23, 12);
            this.deadBandTemperatureRadGroupBox.Name = "deadBandTemperatureRadGroupBox";
            this.deadBandTemperatureRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.deadBandTemperatureRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.deadBandTemperatureRadGroupBox.Size = new System.Drawing.Size(242, 57);
            this.deadBandTemperatureRadGroupBox.TabIndex = 45;
            this.deadBandTemperatureRadGroupBox.Text = "Dead Band Temperature";
            this.deadBandTemperatureRadGroupBox.ThemeName = "Office2007Black";
            // 
            // deadBandTemperatureRadSpinEditor
            // 
            this.deadBandTemperatureRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deadBandTemperatureRadSpinEditor.Location = new System.Drawing.Point(127, 25);
            this.deadBandTemperatureRadSpinEditor.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.deadBandTemperatureRadSpinEditor.Name = "deadBandTemperatureRadSpinEditor";
            // 
            // 
            // 
            this.deadBandTemperatureRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.deadBandTemperatureRadSpinEditor.ShowBorder = true;
            this.deadBandTemperatureRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.deadBandTemperatureRadSpinEditor.TabIndex = 55;
            this.deadBandTemperatureRadSpinEditor.TabStop = false;
            this.deadBandTemperatureRadSpinEditor.ThemeName = "Office2007Black";
            this.deadBandTemperatureRadSpinEditor.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // deadBandTemperatureTextRadLabel
            // 
            this.deadBandTemperatureTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.deadBandTemperatureTextRadLabel.Location = new System.Drawing.Point(13, 27);
            this.deadBandTemperatureTextRadLabel.Name = "deadBandTemperatureTextRadLabel";
            this.deadBandTemperatureTextRadLabel.Size = new System.Drawing.Size(113, 16);
            this.deadBandTemperatureTextRadLabel.TabIndex = 54;
            this.deadBandTemperatureTextRadLabel.Text = "Dead band temp (ºC)";
            // 
            // alarmSettingsRadGroupBox
            // 
            this.alarmSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.alarmSettingsRadGroupBox.Controls.Add(this.alarmDelayRadSpinEditor);
            this.alarmSettingsRadGroupBox.Controls.Add(this.alarmDelayTextRadLabel);
            this.alarmSettingsRadGroupBox.Controls.Add(this.topOilTempIsHighRadGroupBox);
            this.alarmSettingsRadGroupBox.Controls.Add(this.whsIsHighRadGroupBox);
            this.alarmSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmSettingsRadGroupBox.FooterImageIndex = -1;
            this.alarmSettingsRadGroupBox.FooterImageKey = "";
            this.alarmSettingsRadGroupBox.HeaderImageIndex = -1;
            this.alarmSettingsRadGroupBox.HeaderImageKey = "";
            this.alarmSettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.alarmSettingsRadGroupBox.HeaderText = "Alarm settings";
            this.alarmSettingsRadGroupBox.Location = new System.Drawing.Point(23, 89);
            this.alarmSettingsRadGroupBox.Name = "alarmSettingsRadGroupBox";
            this.alarmSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.alarmSettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.alarmSettingsRadGroupBox.Size = new System.Drawing.Size(242, 243);
            this.alarmSettingsRadGroupBox.TabIndex = 44;
            this.alarmSettingsRadGroupBox.Text = "Alarm settings";
            this.alarmSettingsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // alarmDelayRadSpinEditor
            // 
            this.alarmDelayRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alarmDelayRadSpinEditor.Location = new System.Drawing.Point(165, 28);
            this.alarmDelayRadSpinEditor.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.alarmDelayRadSpinEditor.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.alarmDelayRadSpinEditor.Name = "alarmDelayRadSpinEditor";
            // 
            // 
            // 
            this.alarmDelayRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.alarmDelayRadSpinEditor.ShowBorder = true;
            this.alarmDelayRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.alarmDelayRadSpinEditor.TabIndex = 57;
            this.alarmDelayRadSpinEditor.TabStop = false;
            this.alarmDelayRadSpinEditor.ThemeName = "Office2007Black";
            this.alarmDelayRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // alarmDelayTextRadLabel
            // 
            this.alarmDelayTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.alarmDelayTextRadLabel.Location = new System.Drawing.Point(27, 30);
            this.alarmDelayTextRadLabel.Name = "alarmDelayTextRadLabel";
            this.alarmDelayTextRadLabel.Size = new System.Drawing.Size(119, 16);
            this.alarmDelayTextRadLabel.TabIndex = 56;
            this.alarmDelayTextRadLabel.Text = "Alarm delay (seconds)";
            // 
            // topOilTempIsHighRadGroupBox
            // 
            this.topOilTempIsHighRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.topOilTempIsHighRadGroupBox.Controls.Add(this.topOilTempHighAlarmSetPointRadSpinEditor);
            this.topOilTempIsHighRadGroupBox.Controls.Add(this.topOilTempLowAlarmSetPointRadSpinEditor);
            this.topOilTempIsHighRadGroupBox.Controls.Add(this.topOilTempHighAlarmSetPointTextRadLabel);
            this.topOilTempIsHighRadGroupBox.Controls.Add(this.topOilTempLowAlarmSetPointTextRadLabel);
            this.topOilTempIsHighRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topOilTempIsHighRadGroupBox.FooterImageIndex = -1;
            this.topOilTempIsHighRadGroupBox.FooterImageKey = "";
            this.topOilTempIsHighRadGroupBox.HeaderImageIndex = -1;
            this.topOilTempIsHighRadGroupBox.HeaderImageKey = "";
            this.topOilTempIsHighRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.topOilTempIsHighRadGroupBox.HeaderText = "Top Oil Temp Alarm Set Points";
            this.topOilTempIsHighRadGroupBox.Location = new System.Drawing.Point(12, 52);
            this.topOilTempIsHighRadGroupBox.Name = "topOilTempIsHighRadGroupBox";
            this.topOilTempIsHighRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.topOilTempIsHighRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.topOilTempIsHighRadGroupBox.Size = new System.Drawing.Size(215, 90);
            this.topOilTempIsHighRadGroupBox.TabIndex = 55;
            this.topOilTempIsHighRadGroupBox.Text = "Top Oil Temp Alarm Set Points";
            this.topOilTempIsHighRadGroupBox.ThemeName = "Office2007Black";
            // 
            // topOilTempHighAlarmSetPointRadSpinEditor
            // 
            this.topOilTempHighAlarmSetPointRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topOilTempHighAlarmSetPointRadSpinEditor.Location = new System.Drawing.Point(157, 56);
            this.topOilTempHighAlarmSetPointRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.topOilTempHighAlarmSetPointRadSpinEditor.Name = "topOilTempHighAlarmSetPointRadSpinEditor";
            // 
            // 
            // 
            this.topOilTempHighAlarmSetPointRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.topOilTempHighAlarmSetPointRadSpinEditor.RootElement.StretchVertically = true;
            this.topOilTempHighAlarmSetPointRadSpinEditor.ShowBorder = true;
            this.topOilTempHighAlarmSetPointRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.topOilTempHighAlarmSetPointRadSpinEditor.TabIndex = 54;
            this.topOilTempHighAlarmSetPointRadSpinEditor.TabStop = false;
            this.topOilTempHighAlarmSetPointRadSpinEditor.ThemeName = "Office2007Black";
            this.topOilTempHighAlarmSetPointRadSpinEditor.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // topOilTempLowAlarmSetPointRadSpinEditor
            // 
            this.topOilTempLowAlarmSetPointRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topOilTempLowAlarmSetPointRadSpinEditor.Location = new System.Drawing.Point(156, 30);
            this.topOilTempLowAlarmSetPointRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.topOilTempLowAlarmSetPointRadSpinEditor.Name = "topOilTempLowAlarmSetPointRadSpinEditor";
            // 
            // 
            // 
            this.topOilTempLowAlarmSetPointRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.topOilTempLowAlarmSetPointRadSpinEditor.RootElement.StretchVertically = true;
            this.topOilTempLowAlarmSetPointRadSpinEditor.ShowBorder = true;
            this.topOilTempLowAlarmSetPointRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.topOilTempLowAlarmSetPointRadSpinEditor.TabIndex = 53;
            this.topOilTempLowAlarmSetPointRadSpinEditor.TabStop = false;
            this.topOilTempLowAlarmSetPointRadSpinEditor.ThemeName = "Office2007Black";
            this.topOilTempLowAlarmSetPointRadSpinEditor.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // topOilTempHighAlarmSetPointTextRadLabel
            // 
            this.topOilTempHighAlarmSetPointTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.topOilTempHighAlarmSetPointTextRadLabel.Location = new System.Drawing.Point(36, 60);
            this.topOilTempHighAlarmSetPointTextRadLabel.Name = "topOilTempHighAlarmSetPointTextRadLabel";
            this.topOilTempHighAlarmSetPointTextRadLabel.Size = new System.Drawing.Size(115, 16);
            this.topOilTempHighAlarmSetPointTextRadLabel.TabIndex = 51;
            this.topOilTempHighAlarmSetPointTextRadLabel.Text = "High-High Alarm  (ºC)";
            // 
            // topOilTempLowAlarmSetPointTextRadLabel
            // 
            this.topOilTempLowAlarmSetPointTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.topOilTempLowAlarmSetPointTextRadLabel.Location = new System.Drawing.Point(62, 34);
            this.topOilTempLowAlarmSetPointTextRadLabel.Name = "topOilTempLowAlarmSetPointTextRadLabel";
            this.topOilTempLowAlarmSetPointTextRadLabel.Size = new System.Drawing.Size(88, 16);
            this.topOilTempLowAlarmSetPointTextRadLabel.TabIndex = 50;
            this.topOilTempLowAlarmSetPointTextRadLabel.Text = "High Alarm  (ºC)";
            // 
            // whsIsHighRadGroupBox
            // 
            this.whsIsHighRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.whsIsHighRadGroupBox.Controls.Add(this.whsHighAlarmSetPointRadSpinEditor);
            this.whsIsHighRadGroupBox.Controls.Add(this.whsLowAlarmSetPointRadSpinEditor);
            this.whsIsHighRadGroupBox.Controls.Add(this.whsHighAlarmSetPointTextRadLabel);
            this.whsIsHighRadGroupBox.Controls.Add(this.whsLowAlarmSetPointTextRadLabel);
            this.whsIsHighRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whsIsHighRadGroupBox.FooterImageIndex = -1;
            this.whsIsHighRadGroupBox.FooterImageKey = "";
            this.whsIsHighRadGroupBox.HeaderImageIndex = -1;
            this.whsIsHighRadGroupBox.HeaderImageKey = "";
            this.whsIsHighRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.whsIsHighRadGroupBox.HeaderText = "WHS Alarm Setpoints";
            this.whsIsHighRadGroupBox.Location = new System.Drawing.Point(14, 148);
            this.whsIsHighRadGroupBox.Name = "whsIsHighRadGroupBox";
            this.whsIsHighRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.whsIsHighRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.whsIsHighRadGroupBox.Size = new System.Drawing.Size(215, 86);
            this.whsIsHighRadGroupBox.TabIndex = 47;
            this.whsIsHighRadGroupBox.Text = "WHS Alarm Setpoints";
            this.whsIsHighRadGroupBox.ThemeName = "Office2007Black";
            // 
            // whsHighAlarmSetPointRadSpinEditor
            // 
            this.whsHighAlarmSetPointRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whsHighAlarmSetPointRadSpinEditor.Location = new System.Drawing.Point(158, 51);
            this.whsHighAlarmSetPointRadSpinEditor.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.whsHighAlarmSetPointRadSpinEditor.Name = "whsHighAlarmSetPointRadSpinEditor";
            // 
            // 
            // 
            this.whsHighAlarmSetPointRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.whsHighAlarmSetPointRadSpinEditor.ShowBorder = true;
            this.whsHighAlarmSetPointRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.whsHighAlarmSetPointRadSpinEditor.TabIndex = 54;
            this.whsHighAlarmSetPointRadSpinEditor.TabStop = false;
            this.whsHighAlarmSetPointRadSpinEditor.ThemeName = "Office2007Black";
            this.whsHighAlarmSetPointRadSpinEditor.Value = new decimal(new int[] {
            140,
            0,
            0,
            0});
            // 
            // whsLowAlarmSetPointRadSpinEditor
            // 
            this.whsLowAlarmSetPointRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whsLowAlarmSetPointRadSpinEditor.Location = new System.Drawing.Point(158, 26);
            this.whsLowAlarmSetPointRadSpinEditor.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.whsLowAlarmSetPointRadSpinEditor.Name = "whsLowAlarmSetPointRadSpinEditor";
            // 
            // 
            // 
            this.whsLowAlarmSetPointRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.whsLowAlarmSetPointRadSpinEditor.ShowBorder = true;
            this.whsLowAlarmSetPointRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.whsLowAlarmSetPointRadSpinEditor.TabIndex = 53;
            this.whsLowAlarmSetPointRadSpinEditor.TabStop = false;
            this.whsLowAlarmSetPointRadSpinEditor.ThemeName = "Office2007Black";
            this.whsLowAlarmSetPointRadSpinEditor.Value = new decimal(new int[] {
            110,
            0,
            0,
            0});
            // 
            // whsHighAlarmSetPointTextRadLabel
            // 
            this.whsHighAlarmSetPointTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsHighAlarmSetPointTextRadLabel.Location = new System.Drawing.Point(40, 51);
            this.whsHighAlarmSetPointTextRadLabel.Name = "whsHighAlarmSetPointTextRadLabel";
            this.whsHighAlarmSetPointTextRadLabel.Size = new System.Drawing.Size(112, 16);
            this.whsHighAlarmSetPointTextRadLabel.TabIndex = 51;
            this.whsHighAlarmSetPointTextRadLabel.Text = "High-High Alarm (ºC)";
            // 
            // whsLowAlarmSetPointTextRadLabel
            // 
            this.whsLowAlarmSetPointTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsLowAlarmSetPointTextRadLabel.Location = new System.Drawing.Point(68, 28);
            this.whsLowAlarmSetPointTextRadLabel.Name = "whsLowAlarmSetPointTextRadLabel";
            this.whsLowAlarmSetPointTextRadLabel.Size = new System.Drawing.Size(85, 16);
            this.whsLowAlarmSetPointTextRadLabel.TabIndex = 50;
            this.whsLowAlarmSetPointTextRadLabel.Text = "High Alarm (ºC)";
            // 
            // fanSettingsRadGroupBox
            // 
            this.fanSettingsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fanSettingsRadGroupBox.Controls.Add(this.fanCurrent2SpinEditor);
            this.fanSettingsRadGroupBox.Controls.Add(this.fanOperationalInfoGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.fanCurrent1SpinEditor);
            this.fanSettingsRadGroupBox.Controls.Add(this.fanBankSwapRadGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.EnableFanCurrentMonitoring);
            this.fanSettingsRadGroupBox.Controls.Add(this.minimumFanRunTimeRadGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.Group1CurrentLabel);
            this.fanSettingsRadGroupBox.Controls.Add(this.fanAutoExerciseRadGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.Group2CurrentLabel);
            this.fanSettingsRadGroupBox.Controls.Add(this.fanStartTemperaturesRadGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.CalibrateFanButton);
            this.fanSettingsRadGroupBox.Controls.Add(this.NumberofFanGroupsLabel);
            this.fanSettingsRadGroupBox.Controls.Add(this.coolingAlarmGroupBox);
            this.fanSettingsRadGroupBox.Controls.Add(this.NumberOfCoolingGroupsComboBox);
            this.fanSettingsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanSettingsRadGroupBox.FooterImageIndex = -1;
            this.fanSettingsRadGroupBox.FooterImageKey = "";
            this.fanSettingsRadGroupBox.HeaderImageIndex = -1;
            this.fanSettingsRadGroupBox.HeaderImageKey = "";
            this.fanSettingsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.fanSettingsRadGroupBox.HeaderText = "Cooling settings";
            this.fanSettingsRadGroupBox.Location = new System.Drawing.Point(298, 12);
            this.fanSettingsRadGroupBox.Name = "fanSettingsRadGroupBox";
            this.fanSettingsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.fanSettingsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.fanSettingsRadGroupBox.Size = new System.Drawing.Size(527, 424);
            this.fanSettingsRadGroupBox.TabIndex = 43;
            this.fanSettingsRadGroupBox.Text = "Cooling settings";
            this.fanSettingsRadGroupBox.ThemeName = "Office2007Black";
            this.fanSettingsRadGroupBox.Click += new System.EventHandler(this.fanSettingsRadGroupBox_Click);
            // 
            // fanCurrent2SpinEditor
            // 
            this.fanCurrent2SpinEditor.DecimalPlaces = 2;
            this.fanCurrent2SpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanCurrent2SpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.fanCurrent2SpinEditor.Location = new System.Drawing.Point(183, 121);
            this.fanCurrent2SpinEditor.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.fanCurrent2SpinEditor.Name = "fanCurrent2SpinEditor";
            // 
            // 
            // 
            this.fanCurrent2SpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanCurrent2SpinEditor.ShowBorder = true;
            this.fanCurrent2SpinEditor.Size = new System.Drawing.Size(69, 19);
            this.fanCurrent2SpinEditor.TabIndex = 70;
            this.fanCurrent2SpinEditor.TabStop = false;
            this.fanCurrent2SpinEditor.ThemeName = "Office2007Black";
            // 
            // fanOperationalInfoGroupBox
            // 
            this.fanOperationalInfoGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fanOperationalInfoGroupBox.Controls.Add(this.fan2NumOfStartsSpinEdito);
            this.fanOperationalInfoGroupBox.Controls.Add(this.FanStatusLabel);
            this.fanOperationalInfoGroupBox.Controls.Add(this.fan1NumOfStartsSpinEditor);
            this.fanOperationalInfoGroupBox.Controls.Add(this.Fan1Indicator);
            this.fanOperationalInfoGroupBox.Controls.Add(this.fan2RuntimeHoursSpinEditor);
            this.fanOperationalInfoGroupBox.Controls.Add(this.whsFan2StatsLabelText);
            this.fanOperationalInfoGroupBox.Controls.Add(this.fan1RuntimeHoursSpinEditor);
            this.fanOperationalInfoGroupBox.Controls.Add(this.Fan2Indicator);
            this.fanOperationalInfoGroupBox.Controls.Add(this.whsRunTimeHoursLabelText);
            this.fanOperationalInfoGroupBox.Controls.Add(this.whsFan1StatsLabelText);
            this.fanOperationalInfoGroupBox.Controls.Add(this.whsNumOfStartsLabelText);
            this.fanOperationalInfoGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanOperationalInfoGroupBox.FooterImageIndex = -1;
            this.fanOperationalInfoGroupBox.FooterImageKey = "";
            this.fanOperationalInfoGroupBox.HeaderImageIndex = -1;
            this.fanOperationalInfoGroupBox.HeaderImageKey = "";
            this.fanOperationalInfoGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.fanOperationalInfoGroupBox.HeaderText = "Cooling Statistics";
            this.fanOperationalInfoGroupBox.Location = new System.Drawing.Point(233, 287);
            this.fanOperationalInfoGroupBox.Name = "fanOperationalInfoGroupBox";
            this.fanOperationalInfoGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.fanOperationalInfoGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.fanOperationalInfoGroupBox.Size = new System.Drawing.Size(279, 124);
            this.fanOperationalInfoGroupBox.TabIndex = 58;
            this.fanOperationalInfoGroupBox.Text = "Cooling Statistics";
            this.fanOperationalInfoGroupBox.ThemeName = "Office2007Black";
            // 
            // fan2NumOfStartsSpinEdito
            // 
            this.fan2NumOfStartsSpinEdito.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fan2NumOfStartsSpinEdito.Location = new System.Drawing.Point(155, 83);
            this.fan2NumOfStartsSpinEdito.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.fan2NumOfStartsSpinEdito.Name = "fan2NumOfStartsSpinEdito";
            // 
            // 
            // 
            this.fan2NumOfStartsSpinEdito.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fan2NumOfStartsSpinEdito.ShowBorder = true;
            this.fan2NumOfStartsSpinEdito.Size = new System.Drawing.Size(51, 19);
            this.fan2NumOfStartsSpinEdito.TabIndex = 68;
            this.fan2NumOfStartsSpinEdito.TabStop = false;
            this.fan2NumOfStartsSpinEdito.ThemeName = "Office2007Black";
            // 
            // FanStatusLabel
            // 
            this.FanStatusLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.FanStatusLabel.Location = new System.Drawing.Point(230, 23);
            this.FanStatusLabel.Name = "FanStatusLabel";
            this.FanStatusLabel.Size = new System.Drawing.Size(40, 16);
            this.FanStatusLabel.TabIndex = 61;
            this.FanStatusLabel.Text = "On/Off";
            // 
            // fan1NumOfStartsSpinEditor
            // 
            this.fan1NumOfStartsSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fan1NumOfStartsSpinEditor.Location = new System.Drawing.Point(155, 48);
            this.fan1NumOfStartsSpinEditor.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.fan1NumOfStartsSpinEditor.Name = "fan1NumOfStartsSpinEditor";
            // 
            // 
            // 
            this.fan1NumOfStartsSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fan1NumOfStartsSpinEditor.ShowBorder = true;
            this.fan1NumOfStartsSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.fan1NumOfStartsSpinEditor.TabIndex = 67;
            this.fan1NumOfStartsSpinEditor.TabStop = false;
            this.fan1NumOfStartsSpinEditor.ThemeName = "Office2007Black";
            // 
            // Fan1Indicator
            // 
            this.Fan1Indicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Fan1Indicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan1Indicator.Location = new System.Drawing.Point(237, 45);
            this.Fan1Indicator.Name = "Fan1Indicator";
            this.Fan1Indicator.Size = new System.Drawing.Size(26, 21);
            this.Fan1Indicator.TabIndex = 60;
            this.Fan1Indicator.ThemeName = "Office2007Black";
            // 
            // fan2RuntimeHoursSpinEditor
            // 
            this.fan2RuntimeHoursSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fan2RuntimeHoursSpinEditor.Location = new System.Drawing.Point(65, 83);
            this.fan2RuntimeHoursSpinEditor.Maximum = new decimal(new int[] {
            196600,
            0,
            0,
            0});
            this.fan2RuntimeHoursSpinEditor.Name = "fan2RuntimeHoursSpinEditor";
            // 
            // 
            // 
            this.fan2RuntimeHoursSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fan2RuntimeHoursSpinEditor.ShowBorder = true;
            this.fan2RuntimeHoursSpinEditor.Size = new System.Drawing.Size(58, 19);
            this.fan2RuntimeHoursSpinEditor.TabIndex = 66;
            this.fan2RuntimeHoursSpinEditor.TabStop = false;
            this.fan2RuntimeHoursSpinEditor.ThemeName = "Office2007Black";
            // 
            // whsFan2StatsLabelText
            // 
            this.whsFan2StatsLabelText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsFan2StatsLabelText.Location = new System.Drawing.Point(5, 85);
            this.whsFan2StatsLabelText.Name = "whsFan2StatsLabelText";
            this.whsFan2StatsLabelText.Size = new System.Drawing.Size(47, 16);
            this.whsFan2StatsLabelText.TabIndex = 58;
            this.whsFan2StatsLabelText.Text = "Group 2";
            // 
            // fan1RuntimeHoursSpinEditor
            // 
            this.fan1RuntimeHoursSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fan1RuntimeHoursSpinEditor.Location = new System.Drawing.Point(65, 48);
            this.fan1RuntimeHoursSpinEditor.Maximum = new decimal(new int[] {
            196600,
            0,
            0,
            0});
            this.fan1RuntimeHoursSpinEditor.Name = "fan1RuntimeHoursSpinEditor";
            // 
            // 
            // 
            this.fan1RuntimeHoursSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fan1RuntimeHoursSpinEditor.ShowBorder = true;
            this.fan1RuntimeHoursSpinEditor.Size = new System.Drawing.Size(58, 19);
            this.fan1RuntimeHoursSpinEditor.TabIndex = 65;
            this.fan1RuntimeHoursSpinEditor.TabStop = false;
            this.fan1RuntimeHoursSpinEditor.ThemeName = "Office2007Black";
            // 
            // Fan2Indicator
            // 
            this.Fan2Indicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Fan2Indicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan2Indicator.Location = new System.Drawing.Point(237, 83);
            this.Fan2Indicator.Name = "Fan2Indicator";
            this.Fan2Indicator.Size = new System.Drawing.Size(26, 21);
            this.Fan2Indicator.TabIndex = 59;
            this.Fan2Indicator.ThemeName = "Office2007Black";
            // 
            // whsRunTimeHoursLabelText
            // 
            this.whsRunTimeHoursLabelText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsRunTimeHoursLabelText.Location = new System.Drawing.Point(51, 23);
            this.whsRunTimeHoursLabelText.Name = "whsRunTimeHoursLabelText";
            this.whsRunTimeHoursLabelText.Size = new System.Drawing.Size(82, 16);
            this.whsRunTimeHoursLabelText.TabIndex = 57;
            this.whsRunTimeHoursLabelText.Text = "Runtime Hours";
            // 
            // whsFan1StatsLabelText
            // 
            this.whsFan1StatsLabelText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsFan1StatsLabelText.Location = new System.Drawing.Point(5, 48);
            this.whsFan1StatsLabelText.Name = "whsFan1StatsLabelText";
            this.whsFan1StatsLabelText.Size = new System.Drawing.Size(47, 16);
            this.whsFan1StatsLabelText.TabIndex = 57;
            this.whsFan1StatsLabelText.Text = "Group 1";
            // 
            // whsNumOfStartsLabelText
            // 
            this.whsNumOfStartsLabelText.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsNumOfStartsLabelText.Location = new System.Drawing.Point(133, 23);
            this.whsNumOfStartsLabelText.Name = "whsNumOfStartsLabelText";
            this.whsNumOfStartsLabelText.Size = new System.Drawing.Size(92, 16);
            this.whsNumOfStartsLabelText.TabIndex = 56;
            this.whsNumOfStartsLabelText.Text = "Number of Starts";
            // 
            // fanCurrent1SpinEditor
            // 
            this.fanCurrent1SpinEditor.DecimalPlaces = 2;
            this.fanCurrent1SpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanCurrent1SpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.fanCurrent1SpinEditor.Location = new System.Drawing.Point(183, 86);
            this.fanCurrent1SpinEditor.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.fanCurrent1SpinEditor.Name = "fanCurrent1SpinEditor";
            // 
            // 
            // 
            this.fanCurrent1SpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanCurrent1SpinEditor.ShowBorder = true;
            this.fanCurrent1SpinEditor.Size = new System.Drawing.Size(69, 19);
            this.fanCurrent1SpinEditor.TabIndex = 69;
            this.fanCurrent1SpinEditor.TabStop = false;
            this.fanCurrent1SpinEditor.ThemeName = "Office2007Black";
            // 
            // fanBankSwapRadGroupBox
            // 
            this.fanBankSwapRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fanBankSwapRadGroupBox.Controls.Add(this.fanBankSwapSetToZeroToDisableTextRadLabel);
            this.fanBankSwapRadGroupBox.Controls.Add(this.fanBankSwapEveryTextRadLabel);
            this.fanBankSwapRadGroupBox.Controls.Add(this.fanBankSwapIntervalInHoursRadSpinEditor);
            this.fanBankSwapRadGroupBox.Controls.Add(this.fanBankSwapHoursTextRadLabel);
            this.fanBankSwapRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankSwapRadGroupBox.FooterImageIndex = -1;
            this.fanBankSwapRadGroupBox.FooterImageKey = "";
            this.fanBankSwapRadGroupBox.HeaderImageIndex = -1;
            this.fanBankSwapRadGroupBox.HeaderImageKey = "";
            this.fanBankSwapRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.fanBankSwapRadGroupBox.HeaderText = "Cooling bank swap";
            this.fanBankSwapRadGroupBox.Location = new System.Drawing.Point(342, 198);
            this.fanBankSwapRadGroupBox.Name = "fanBankSwapRadGroupBox";
            this.fanBankSwapRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.fanBankSwapRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.fanBankSwapRadGroupBox.Size = new System.Drawing.Size(167, 81);
            this.fanBankSwapRadGroupBox.TabIndex = 49;
            this.fanBankSwapRadGroupBox.Text = "Cooling bank swap";
            this.fanBankSwapRadGroupBox.ThemeName = "Office2007Black";
            // 
            // fanBankSwapSetToZeroToDisableTextRadLabel
            // 
            this.fanBankSwapSetToZeroToDisableTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanBankSwapSetToZeroToDisableTextRadLabel.Location = new System.Drawing.Point(36, 55);
            this.fanBankSwapSetToZeroToDisableTextRadLabel.Name = "fanBankSwapSetToZeroToDisableTextRadLabel";
            this.fanBankSwapSetToZeroToDisableTextRadLabel.Size = new System.Drawing.Size(96, 16);
            this.fanBankSwapSetToZeroToDisableTextRadLabel.TabIndex = 55;
            this.fanBankSwapSetToZeroToDisableTextRadLabel.Text = "Set to 0 to disable";
            // 
            // fanBankSwapEveryTextRadLabel
            // 
            this.fanBankSwapEveryTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanBankSwapEveryTextRadLabel.Location = new System.Drawing.Point(21, 27);
            this.fanBankSwapEveryTextRadLabel.Name = "fanBankSwapEveryTextRadLabel";
            this.fanBankSwapEveryTextRadLabel.Size = new System.Drawing.Size(35, 16);
            this.fanBankSwapEveryTextRadLabel.TabIndex = 54;
            this.fanBankSwapEveryTextRadLabel.Text = "Every";
            // 
            // fanBankSwapIntervalInHoursRadSpinEditor
            // 
            this.fanBankSwapIntervalInHoursRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankSwapIntervalInHoursRadSpinEditor.Location = new System.Drawing.Point(64, 25);
            this.fanBankSwapIntervalInHoursRadSpinEditor.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.fanBankSwapIntervalInHoursRadSpinEditor.Name = "fanBankSwapIntervalInHoursRadSpinEditor";
            // 
            // 
            // 
            this.fanBankSwapIntervalInHoursRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanBankSwapIntervalInHoursRadSpinEditor.RootElement.StretchVertically = true;
            this.fanBankSwapIntervalInHoursRadSpinEditor.ShowBorder = true;
            this.fanBankSwapIntervalInHoursRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.fanBankSwapIntervalInHoursRadSpinEditor.TabIndex = 53;
            this.fanBankSwapIntervalInHoursRadSpinEditor.TabStop = false;
            this.fanBankSwapIntervalInHoursRadSpinEditor.ThemeName = "Office2007Black";
            this.fanBankSwapIntervalInHoursRadSpinEditor.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // fanBankSwapHoursTextRadLabel
            // 
            this.fanBankSwapHoursTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanBankSwapHoursTextRadLabel.Location = new System.Drawing.Point(121, 27);
            this.fanBankSwapHoursTextRadLabel.Name = "fanBankSwapHoursTextRadLabel";
            this.fanBankSwapHoursTextRadLabel.Size = new System.Drawing.Size(35, 16);
            this.fanBankSwapHoursTextRadLabel.TabIndex = 50;
            this.fanBankSwapHoursTextRadLabel.Text = "hours";
            // 
            // EnableFanCurrentMonitoring
            // 
            this.EnableFanCurrentMonitoring.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnableFanCurrentMonitoring.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.EnableFanCurrentMonitoring.Location = new System.Drawing.Point(13, 49);
            this.EnableFanCurrentMonitoring.Name = "EnableFanCurrentMonitoring";
            this.EnableFanCurrentMonitoring.Size = new System.Drawing.Size(194, 16);
            this.EnableFanCurrentMonitoring.TabIndex = 64;
            this.EnableFanCurrentMonitoring.Text = "Enable Cooling Current Monitoring";
            // 
            // minimumFanRunTimeRadGroupBox
            // 
            this.minimumFanRunTimeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.minimumFanRunTimeRadGroupBox.Controls.Add(this.minimumfanRunTimeInMinutesRadSpinEditor);
            this.minimumFanRunTimeRadGroupBox.Controls.Add(this.minimumFanRunTimeMinutesTextRadLabel);
            this.minimumFanRunTimeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minimumFanRunTimeRadGroupBox.FooterImageIndex = -1;
            this.minimumFanRunTimeRadGroupBox.FooterImageKey = "";
            this.minimumFanRunTimeRadGroupBox.HeaderImageIndex = -1;
            this.minimumFanRunTimeRadGroupBox.HeaderImageKey = "";
            this.minimumFanRunTimeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.minimumFanRunTimeRadGroupBox.HeaderText = "Minimum cooling run time";
            this.minimumFanRunTimeRadGroupBox.Location = new System.Drawing.Point(342, 125);
            this.minimumFanRunTimeRadGroupBox.Name = "minimumFanRunTimeRadGroupBox";
            this.minimumFanRunTimeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.minimumFanRunTimeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.minimumFanRunTimeRadGroupBox.Size = new System.Drawing.Size(167, 67);
            this.minimumFanRunTimeRadGroupBox.TabIndex = 48;
            this.minimumFanRunTimeRadGroupBox.Text = "Minimum cooling run time";
            this.minimumFanRunTimeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // minimumfanRunTimeInMinutesRadSpinEditor
            // 
            this.minimumfanRunTimeInMinutesRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minimumfanRunTimeInMinutesRadSpinEditor.Location = new System.Drawing.Point(22, 31);
            this.minimumfanRunTimeInMinutesRadSpinEditor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.minimumfanRunTimeInMinutesRadSpinEditor.Name = "minimumfanRunTimeInMinutesRadSpinEditor";
            // 
            // 
            // 
            this.minimumfanRunTimeInMinutesRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.minimumfanRunTimeInMinutesRadSpinEditor.RootElement.StretchVertically = true;
            this.minimumfanRunTimeInMinutesRadSpinEditor.ShowBorder = true;
            this.minimumfanRunTimeInMinutesRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.minimumfanRunTimeInMinutesRadSpinEditor.TabIndex = 53;
            this.minimumfanRunTimeInMinutesRadSpinEditor.TabStop = false;
            this.minimumfanRunTimeInMinutesRadSpinEditor.ThemeName = "Office2007Black";
            this.minimumfanRunTimeInMinutesRadSpinEditor.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // minimumFanRunTimeMinutesTextRadLabel
            // 
            this.minimumFanRunTimeMinutesTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.minimumFanRunTimeMinutesTextRadLabel.Location = new System.Drawing.Point(79, 31);
            this.minimumFanRunTimeMinutesTextRadLabel.Name = "minimumFanRunTimeMinutesTextRadLabel";
            this.minimumFanRunTimeMinutesTextRadLabel.Size = new System.Drawing.Size(46, 16);
            this.minimumFanRunTimeMinutesTextRadLabel.TabIndex = 50;
            this.minimumFanRunTimeMinutesTextRadLabel.Text = "minutes";
            // 
            // Group1CurrentLabel
            // 
            this.Group1CurrentLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Group1CurrentLabel.Location = new System.Drawing.Point(130, 89);
            this.Group1CurrentLabel.Name = "Group1CurrentLabel";
            this.Group1CurrentLabel.Size = new System.Drawing.Size(47, 16);
            this.Group1CurrentLabel.TabIndex = 67;
            this.Group1CurrentLabel.Text = "Group 1";
            // 
            // fanAutoExerciseRadGroupBox
            // 
            this.fanAutoExerciseRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.ManualExcerciseLabel);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanTestButton);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanAutoExerciseMinutesTextRadLabel);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanAutoExerciseDaysTextRadLabel);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanExerciseTimeInMinutesRadSpinEditor);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanExerciseIntervalInDaysRadSpinEditor);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanAutoExerciseForTextRadLabel);
            this.fanAutoExerciseRadGroupBox.Controls.Add(this.fanAutoExerciseEveryTextRadLabel);
            this.fanAutoExerciseRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanAutoExerciseRadGroupBox.FooterImageIndex = -1;
            this.fanAutoExerciseRadGroupBox.FooterImageKey = "";
            this.fanAutoExerciseRadGroupBox.HeaderImageIndex = -1;
            this.fanAutoExerciseRadGroupBox.HeaderImageKey = "";
            this.fanAutoExerciseRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.fanAutoExerciseRadGroupBox.HeaderText = "Cooling auto exercise settings";
            this.fanAutoExerciseRadGroupBox.Location = new System.Drawing.Point(20, 262);
            this.fanAutoExerciseRadGroupBox.Name = "fanAutoExerciseRadGroupBox";
            this.fanAutoExerciseRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.fanAutoExerciseRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.fanAutoExerciseRadGroupBox.Size = new System.Drawing.Size(193, 152);
            this.fanAutoExerciseRadGroupBox.TabIndex = 47;
            this.fanAutoExerciseRadGroupBox.Text = "Cooling auto exercise settings";
            this.fanAutoExerciseRadGroupBox.ThemeName = "Office2007Black";
            // 
            // ManualExcerciseLabel
            // 
            this.ManualExcerciseLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ManualExcerciseLabel.Location = new System.Drawing.Point(7, 112);
            this.ManualExcerciseLabel.Name = "ManualExcerciseLabel";
            this.ManualExcerciseLabel.Size = new System.Drawing.Size(96, 16);
            this.ManualExcerciseLabel.TabIndex = 59;
            this.ManualExcerciseLabel.Text = "Manual Excersize";
            // 
            // fanAutoExerciseSetBothToZeroToDisableTextRadLabel
            // 
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.Location = new System.Drawing.Point(28, 82);
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.Name = "fanAutoExerciseSetBothToZeroToDisableTextRadLabel";
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.Size = new System.Drawing.Size(123, 16);
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.TabIndex = 56;
            this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel.Text = "Set days to 0 to disable";
            // 
            // fanTestButton
            // 
            this.fanTestButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.fanTestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanTestButton.Location = new System.Drawing.Point(109, 105);
            this.fanTestButton.Name = "fanTestButton";
            this.fanTestButton.Size = new System.Drawing.Size(59, 34);
            this.fanTestButton.TabIndex = 58;
            this.fanTestButton.Text = "CoolingTest";
            this.fanTestButton.TextWrap = true;
            this.fanTestButton.ThemeName = "Office2007Black";
            this.fanTestButton.Click += new System.EventHandler(this.fanTestButton_Click);
            // 
            // fanAutoExerciseMinutesTextRadLabel
            // 
            this.fanAutoExerciseMinutesTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanAutoExerciseMinutesTextRadLabel.Location = new System.Drawing.Point(128, 59);
            this.fanAutoExerciseMinutesTextRadLabel.Name = "fanAutoExerciseMinutesTextRadLabel";
            this.fanAutoExerciseMinutesTextRadLabel.Size = new System.Drawing.Size(46, 16);
            this.fanAutoExerciseMinutesTextRadLabel.TabIndex = 51;
            this.fanAutoExerciseMinutesTextRadLabel.Text = "minutes";
            // 
            // fanAutoExerciseDaysTextRadLabel
            // 
            this.fanAutoExerciseDaysTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanAutoExerciseDaysTextRadLabel.Location = new System.Drawing.Point(128, 33);
            this.fanAutoExerciseDaysTextRadLabel.Name = "fanAutoExerciseDaysTextRadLabel";
            this.fanAutoExerciseDaysTextRadLabel.Size = new System.Drawing.Size(32, 16);
            this.fanAutoExerciseDaysTextRadLabel.TabIndex = 55;
            this.fanAutoExerciseDaysTextRadLabel.Text = "Days";
            // 
            // fanExerciseTimeInMinutesRadSpinEditor
            // 
            this.fanExerciseTimeInMinutesRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanExerciseTimeInMinutesRadSpinEditor.Location = new System.Drawing.Point(55, 57);
            this.fanExerciseTimeInMinutesRadSpinEditor.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.fanExerciseTimeInMinutesRadSpinEditor.Name = "fanExerciseTimeInMinutesRadSpinEditor";
            // 
            // 
            // 
            this.fanExerciseTimeInMinutesRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanExerciseTimeInMinutesRadSpinEditor.RootElement.StretchVertically = true;
            this.fanExerciseTimeInMinutesRadSpinEditor.ShowBorder = true;
            this.fanExerciseTimeInMinutesRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.fanExerciseTimeInMinutesRadSpinEditor.TabIndex = 54;
            this.fanExerciseTimeInMinutesRadSpinEditor.TabStop = false;
            this.fanExerciseTimeInMinutesRadSpinEditor.ThemeName = "Office2007Black";
            this.fanExerciseTimeInMinutesRadSpinEditor.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // fanExerciseIntervalInDaysRadSpinEditor
            // 
            this.fanExerciseIntervalInDaysRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanExerciseIntervalInDaysRadSpinEditor.Location = new System.Drawing.Point(55, 31);
            this.fanExerciseIntervalInDaysRadSpinEditor.Name = "fanExerciseIntervalInDaysRadSpinEditor";
            // 
            // 
            // 
            this.fanExerciseIntervalInDaysRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanExerciseIntervalInDaysRadSpinEditor.RootElement.StretchVertically = true;
            this.fanExerciseIntervalInDaysRadSpinEditor.ShowBorder = true;
            this.fanExerciseIntervalInDaysRadSpinEditor.Size = new System.Drawing.Size(51, 20);
            this.fanExerciseIntervalInDaysRadSpinEditor.TabIndex = 53;
            this.fanExerciseIntervalInDaysRadSpinEditor.TabStop = false;
            this.fanExerciseIntervalInDaysRadSpinEditor.ThemeName = "Office2007Black";
            this.fanExerciseIntervalInDaysRadSpinEditor.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // fanAutoExerciseForTextRadLabel
            // 
            this.fanAutoExerciseForTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanAutoExerciseForTextRadLabel.Location = new System.Drawing.Point(14, 59);
            this.fanAutoExerciseForTextRadLabel.Name = "fanAutoExerciseForTextRadLabel";
            this.fanAutoExerciseForTextRadLabel.Size = new System.Drawing.Size(23, 16);
            this.fanAutoExerciseForTextRadLabel.TabIndex = 51;
            this.fanAutoExerciseForTextRadLabel.Text = "For";
            // 
            // fanAutoExerciseEveryTextRadLabel
            // 
            this.fanAutoExerciseEveryTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanAutoExerciseEveryTextRadLabel.Location = new System.Drawing.Point(14, 33);
            this.fanAutoExerciseEveryTextRadLabel.Name = "fanAutoExerciseEveryTextRadLabel";
            this.fanAutoExerciseEveryTextRadLabel.Size = new System.Drawing.Size(35, 16);
            this.fanAutoExerciseEveryTextRadLabel.TabIndex = 50;
            this.fanAutoExerciseEveryTextRadLabel.Text = "Every";
            // 
            // Group2CurrentLabel
            // 
            this.Group2CurrentLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Group2CurrentLabel.Location = new System.Drawing.Point(130, 123);
            this.Group2CurrentLabel.Name = "Group2CurrentLabel";
            this.Group2CurrentLabel.Size = new System.Drawing.Size(47, 16);
            this.Group2CurrentLabel.TabIndex = 68;
            this.Group2CurrentLabel.Text = "Group 2";
            // 
            // fanStartTemperaturesRadGroupBox
            // 
            this.fanStartTemperaturesRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankTwoStartTempTopOilRadSpinEditor);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankTwoStartTempWhsRadSpinEditor);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankOneStartTempWhsRadSpinEditor);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.whsTextRadLabel);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankOneStartTempTopOilRadSpinEditor);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankTwoTextRadLabel);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.fanBankOneTextRadLabel);
            this.fanStartTemperaturesRadGroupBox.Controls.Add(this.topOilTextRadLabel);
            this.fanStartTemperaturesRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanStartTemperaturesRadGroupBox.FooterImageIndex = -1;
            this.fanStartTemperaturesRadGroupBox.FooterImageKey = "";
            this.fanStartTemperaturesRadGroupBox.HeaderImageIndex = -1;
            this.fanStartTemperaturesRadGroupBox.HeaderImageKey = "";
            this.fanStartTemperaturesRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.fanStartTemperaturesRadGroupBox.HeaderText = "Cooling start temperatures";
            this.fanStartTemperaturesRadGroupBox.Location = new System.Drawing.Point(20, 155);
            this.fanStartTemperaturesRadGroupBox.Name = "fanStartTemperaturesRadGroupBox";
            this.fanStartTemperaturesRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.fanStartTemperaturesRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.fanStartTemperaturesRadGroupBox.Size = new System.Drawing.Size(267, 101);
            this.fanStartTemperaturesRadGroupBox.TabIndex = 45;
            this.fanStartTemperaturesRadGroupBox.Text = "Cooling start temperatures";
            this.fanStartTemperaturesRadGroupBox.ThemeName = "Office2007Black";
            // 
            // fanBankTwoStartTempTopOilRadSpinEditor
            // 
            this.fanBankTwoStartTempTopOilRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankTwoStartTempTopOilRadSpinEditor.Location = new System.Drawing.Point(117, 69);
            this.fanBankTwoStartTempTopOilRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.fanBankTwoStartTempTopOilRadSpinEditor.Name = "fanBankTwoStartTempTopOilRadSpinEditor";
            // 
            // 
            // 
            this.fanBankTwoStartTempTopOilRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanBankTwoStartTempTopOilRadSpinEditor.ShowBorder = true;
            this.fanBankTwoStartTempTopOilRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.fanBankTwoStartTempTopOilRadSpinEditor.TabIndex = 54;
            this.fanBankTwoStartTempTopOilRadSpinEditor.TabStop = false;
            this.fanBankTwoStartTempTopOilRadSpinEditor.ThemeName = "Office2007Black";
            this.fanBankTwoStartTempTopOilRadSpinEditor.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // fanBankTwoStartTempWhsRadSpinEditor
            // 
            this.fanBankTwoStartTempWhsRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankTwoStartTempWhsRadSpinEditor.Location = new System.Drawing.Point(192, 70);
            this.fanBankTwoStartTempWhsRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.fanBankTwoStartTempWhsRadSpinEditor.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.fanBankTwoStartTempWhsRadSpinEditor.Name = "fanBankTwoStartTempWhsRadSpinEditor";
            // 
            // 
            // 
            this.fanBankTwoStartTempWhsRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanBankTwoStartTempWhsRadSpinEditor.ShowBorder = true;
            this.fanBankTwoStartTempWhsRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.fanBankTwoStartTempWhsRadSpinEditor.TabIndex = 54;
            this.fanBankTwoStartTempWhsRadSpinEditor.TabStop = false;
            this.fanBankTwoStartTempWhsRadSpinEditor.ThemeName = "Office2007Black";
            this.fanBankTwoStartTempWhsRadSpinEditor.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            // 
            // fanBankOneStartTempWhsRadSpinEditor
            // 
            this.fanBankOneStartTempWhsRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankOneStartTempWhsRadSpinEditor.Location = new System.Drawing.Point(192, 44);
            this.fanBankOneStartTempWhsRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.fanBankOneStartTempWhsRadSpinEditor.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.fanBankOneStartTempWhsRadSpinEditor.Name = "fanBankOneStartTempWhsRadSpinEditor";
            // 
            // 
            // 
            this.fanBankOneStartTempWhsRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanBankOneStartTempWhsRadSpinEditor.ShowBorder = true;
            this.fanBankOneStartTempWhsRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.fanBankOneStartTempWhsRadSpinEditor.TabIndex = 53;
            this.fanBankOneStartTempWhsRadSpinEditor.TabStop = false;
            this.fanBankOneStartTempWhsRadSpinEditor.ThemeName = "Office2007Black";
            this.fanBankOneStartTempWhsRadSpinEditor.Value = new decimal(new int[] {
            65,
            0,
            0,
            0});
            // 
            // whsTextRadLabel
            // 
            this.whsTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.whsTextRadLabel.Location = new System.Drawing.Point(201, 22);
            this.whsTextRadLabel.Name = "whsTextRadLabel";
            this.whsTextRadLabel.Size = new System.Drawing.Size(33, 16);
            this.whsTextRadLabel.TabIndex = 52;
            this.whsTextRadLabel.Text = "WHS";
            // 
            // fanBankOneStartTempTopOilRadSpinEditor
            // 
            this.fanBankOneStartTempTopOilRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fanBankOneStartTempTopOilRadSpinEditor.Location = new System.Drawing.Point(117, 43);
            this.fanBankOneStartTempTopOilRadSpinEditor.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.fanBankOneStartTempTopOilRadSpinEditor.Name = "fanBankOneStartTempTopOilRadSpinEditor";
            // 
            // 
            // 
            this.fanBankOneStartTempTopOilRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.fanBankOneStartTempTopOilRadSpinEditor.ShowBorder = true;
            this.fanBankOneStartTempTopOilRadSpinEditor.Size = new System.Drawing.Size(51, 19);
            this.fanBankOneStartTempTopOilRadSpinEditor.TabIndex = 54;
            this.fanBankOneStartTempTopOilRadSpinEditor.TabStop = false;
            this.fanBankOneStartTempTopOilRadSpinEditor.ThemeName = "Office2007Black";
            this.fanBankOneStartTempTopOilRadSpinEditor.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // fanBankTwoTextRadLabel
            // 
            this.fanBankTwoTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanBankTwoTextRadLabel.Location = new System.Drawing.Point(14, 72);
            this.fanBankTwoTextRadLabel.Name = "fanBankTwoTextRadLabel";
            this.fanBankTwoTextRadLabel.Size = new System.Drawing.Size(70, 16);
            this.fanBankTwoTextRadLabel.TabIndex = 51;
            this.fanBankTwoTextRadLabel.Text = "Group 2 (ºC)";
            // 
            // fanBankOneTextRadLabel
            // 
            this.fanBankOneTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.fanBankOneTextRadLabel.Location = new System.Drawing.Point(14, 46);
            this.fanBankOneTextRadLabel.Name = "fanBankOneTextRadLabel";
            this.fanBankOneTextRadLabel.Size = new System.Drawing.Size(70, 16);
            this.fanBankOneTextRadLabel.TabIndex = 50;
            this.fanBankOneTextRadLabel.Text = "Group 1 (ºC)";
            // 
            // topOilTextRadLabel
            // 
            this.topOilTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.topOilTextRadLabel.Location = new System.Drawing.Point(117, 21);
            this.topOilTextRadLabel.Name = "topOilTextRadLabel";
            this.topOilTextRadLabel.Size = new System.Drawing.Size(43, 16);
            this.topOilTextRadLabel.TabIndex = 51;
            this.topOilTextRadLabel.Text = "Top Oil";
            // 
            // CalibrateFanButton
            // 
            this.CalibrateFanButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CalibrateFanButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalibrateFanButton.Location = new System.Drawing.Point(18, 86);
            this.CalibrateFanButton.Name = "CalibrateFanButton";
            this.CalibrateFanButton.Size = new System.Drawing.Size(108, 52);
            this.CalibrateFanButton.TabIndex = 62;
            this.CalibrateFanButton.Text = "Calibrate Motor Currents";
            this.CalibrateFanButton.TextWrap = true;
            this.CalibrateFanButton.ThemeName = "Office2007Black";
            this.CalibrateFanButton.Click += new System.EventHandler(this.CalibrateFanButton_Click);
            // 
            // NumberofFanGroupsLabel
            // 
            this.NumberofFanGroupsLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.NumberofFanGroupsLabel.Location = new System.Drawing.Point(35, 23);
            this.NumberofFanGroupsLabel.Name = "NumberofFanGroupsLabel";
            this.NumberofFanGroupsLabel.Size = new System.Drawing.Size(88, 16);
            this.NumberofFanGroupsLabel.TabIndex = 63;
            this.NumberofFanGroupsLabel.Text = "Groups Enabled";
            // 
            // coolingAlarmGroupBox
            // 
            this.coolingAlarmGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.coolingAlarmGroupBox.Controls.Add(this.highCoolingAlarmLabel);
            this.coolingAlarmGroupBox.Controls.Add(this.HighCoolingAlarmSpinEditor);
            this.coolingAlarmGroupBox.Controls.Add(this.lowCoolingAlarmlabel);
            this.coolingAlarmGroupBox.Controls.Add(this.lowCoolingAlarmSpinEditor);
            this.coolingAlarmGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coolingAlarmGroupBox.FooterImageIndex = -1;
            this.coolingAlarmGroupBox.FooterImageKey = "";
            this.coolingAlarmGroupBox.HeaderImageIndex = -1;
            this.coolingAlarmGroupBox.HeaderImageKey = "";
            this.coolingAlarmGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.coolingAlarmGroupBox.HeaderText = "Per Unit Current Cooling Alarm";
            this.coolingAlarmGroupBox.Location = new System.Drawing.Point(212, 16);
            this.coolingAlarmGroupBox.Name = "coolingAlarmGroupBox";
            this.coolingAlarmGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.coolingAlarmGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.coolingAlarmGroupBox.Size = new System.Drawing.Size(294, 61);
            this.coolingAlarmGroupBox.TabIndex = 65;
            this.coolingAlarmGroupBox.Text = "Per Unit Current Cooling Alarm";
            this.coolingAlarmGroupBox.ThemeName = "Office2007Black";
            // 
            // highCoolingAlarmLabel
            // 
            this.highCoolingAlarmLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.highCoolingAlarmLabel.Location = new System.Drawing.Point(156, 30);
            this.highCoolingAlarmLabel.Name = "highCoolingAlarmLabel";
            this.highCoolingAlarmLabel.Size = new System.Drawing.Size(70, 16);
            this.highCoolingAlarmLabel.TabIndex = 59;
            this.highCoolingAlarmLabel.Text = "High Current";
            // 
            // HighCoolingAlarmSpinEditor
            // 
            this.HighCoolingAlarmSpinEditor.DecimalPlaces = 2;
            this.HighCoolingAlarmSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HighCoolingAlarmSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.HighCoolingAlarmSpinEditor.Location = new System.Drawing.Point(238, 28);
            this.HighCoolingAlarmSpinEditor.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            this.HighCoolingAlarmSpinEditor.Minimum = new decimal(new int[] {
            105,
            0,
            0,
            131072});
            this.HighCoolingAlarmSpinEditor.Name = "HighCoolingAlarmSpinEditor";
            // 
            // 
            // 
            this.HighCoolingAlarmSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.HighCoolingAlarmSpinEditor.RootElement.StretchVertically = true;
            this.HighCoolingAlarmSpinEditor.ShowBorder = true;
            this.HighCoolingAlarmSpinEditor.Size = new System.Drawing.Size(43, 20);
            this.HighCoolingAlarmSpinEditor.TabIndex = 54;
            this.HighCoolingAlarmSpinEditor.TabStop = false;
            this.HighCoolingAlarmSpinEditor.ThemeName = "Office2007Black";
            this.HighCoolingAlarmSpinEditor.Value = new decimal(new int[] {
            115,
            0,
            0,
            131072});
            // 
            // lowCoolingAlarmlabel
            // 
            this.lowCoolingAlarmlabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lowCoolingAlarmlabel.Location = new System.Drawing.Point(7, 30);
            this.lowCoolingAlarmlabel.Name = "lowCoolingAlarmlabel";
            this.lowCoolingAlarmlabel.Size = new System.Drawing.Size(68, 16);
            this.lowCoolingAlarmlabel.TabIndex = 58;
            this.lowCoolingAlarmlabel.Text = "Low Current";
            // 
            // lowCoolingAlarmSpinEditor
            // 
            this.lowCoolingAlarmSpinEditor.DecimalPlaces = 2;
            this.lowCoolingAlarmSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowCoolingAlarmSpinEditor.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.lowCoolingAlarmSpinEditor.Location = new System.Drawing.Point(86, 28);
            this.lowCoolingAlarmSpinEditor.Maximum = new decimal(new int[] {
            95,
            0,
            0,
            131072});
            this.lowCoolingAlarmSpinEditor.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.lowCoolingAlarmSpinEditor.Name = "lowCoolingAlarmSpinEditor";
            // 
            // 
            // 
            this.lowCoolingAlarmSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.lowCoolingAlarmSpinEditor.RootElement.StretchVertically = true;
            this.lowCoolingAlarmSpinEditor.ShowBorder = true;
            this.lowCoolingAlarmSpinEditor.Size = new System.Drawing.Size(43, 20);
            this.lowCoolingAlarmSpinEditor.TabIndex = 53;
            this.lowCoolingAlarmSpinEditor.TabStop = false;
            this.lowCoolingAlarmSpinEditor.ThemeName = "Office2007Black";
            this.lowCoolingAlarmSpinEditor.Value = new decimal(new int[] {
            95,
            0,
            0,
            131072});
            this.lowCoolingAlarmSpinEditor.ValueChanged += new System.EventHandler(this.lowCoolingAlarmSpinEditor_ValueChanged);
            // 
            // NumberOfCoolingGroupsComboBox
            // 
            this.NumberOfCoolingGroupsComboBox.DropDownAnimationEnabled = true;
            this.NumberOfCoolingGroupsComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumberOfCoolingGroupsComboBox.Location = new System.Drawing.Point(128, 23);
            this.NumberOfCoolingGroupsComboBox.Name = "NumberOfCoolingGroupsComboBox";
            this.NumberOfCoolingGroupsComboBox.ShowImageInEditorArea = true;
            this.NumberOfCoolingGroupsComboBox.Size = new System.Drawing.Size(63, 20);
            this.NumberOfCoolingGroupsComboBox.TabIndex = 66;
            this.NumberOfCoolingGroupsComboBox.ThemeName = "Office2007Black";
            // 
            // alarmsAndFansRadProgressBar
            // 
            this.alarmsAndFansRadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.alarmsAndFansRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.alarmsAndFansRadProgressBar.ImageIndex = -1;
            this.alarmsAndFansRadProgressBar.ImageKey = "";
            this.alarmsAndFansRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.alarmsAndFansRadProgressBar.Location = new System.Drawing.Point(569, 453);
            this.alarmsAndFansRadProgressBar.Name = "alarmsAndFansRadProgressBar";
            this.alarmsAndFansRadProgressBar.SeparatorColor1 = System.Drawing.Color.White;
            this.alarmsAndFansRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.alarmsAndFansRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.alarmsAndFansRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.alarmsAndFansRadProgressBar.Size = new System.Drawing.Size(256, 30);
            this.alarmsAndFansRadProgressBar.TabIndex = 42;
            this.alarmsAndFansRadProgressBar.Text = "radProgressBar1";
            // 
            // availableConfigurationsAlarmsAndFansTabRadGroupBox
            // 
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Controls.Add(this.availableConfigurationsAlarmsAndFansTabRadListControl);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Controls.Add(this.radButton11);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.FooterImageIndex = -1;
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.FooterImageKey = "";
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.HeaderImageIndex = -1;
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.HeaderImageKey = "";
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.HeaderText = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Location = new System.Drawing.Point(170, 489);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Name = "availableConfigurationsAlarmsAndFansTabRadGroupBox";
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Size = new System.Drawing.Size(503, 192);
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.TabIndex = 30;
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.Text = "<html>Configurations by Date Saved</html>";
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableConfigurationsAlarmsAndFansTabRadListControl
            // 
            this.availableConfigurationsAlarmsAndFansTabRadListControl.AutoScroll = true;
            this.availableConfigurationsAlarmsAndFansTabRadListControl.CaseSensitiveSort = true;
            this.availableConfigurationsAlarmsAndFansTabRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableConfigurationsAlarmsAndFansTabRadListControl.ItemHeight = 18;
            this.availableConfigurationsAlarmsAndFansTabRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableConfigurationsAlarmsAndFansTabRadListControl.Name = "availableConfigurationsAlarmsAndFansTabRadListControl";
            this.availableConfigurationsAlarmsAndFansTabRadListControl.Size = new System.Drawing.Size(483, 162);
            this.availableConfigurationsAlarmsAndFansTabRadListControl.TabIndex = 9;
            this.availableConfigurationsAlarmsAndFansTabRadListControl.Text = "radListControl2";
            this.availableConfigurationsAlarmsAndFansTabRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableConfigurationsAlarmsAndFansTabRadListControl_SelectedIndexChanged);
            // 
            // radButton11
            // 
            this.radButton11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton11.Location = new System.Drawing.Point(0, 232);
            this.radButton11.Name = "radButton11";
            this.radButton11.Size = new System.Drawing.Size(130, 70);
            this.radButton11.TabIndex = 8;
            this.radButton11.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton11.ThemeName = "Office2007Black";
            // 
            // Main_WHSMonitorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(872, 732);
            this.Controls.Add(this.configurationRadPageView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main_WHSMonitorConfiguration";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Main WHS Configuration";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_WHSMonitorConfiguration_FormClosing);
            this.Load += new System.EventHandler(this.Main_WHSMonitorConfiguration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationRadPageView)).EndInit();
            this.configurationRadPageView.ResumeLayout(false);
            this.whsGeneralSettingsRadPageViewPage.ResumeLayout(false);
            this.whsGeneralSettingsRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateErrorButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorListLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsErrorListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearAlarmListButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateAlarmListButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AlarmListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotSpotFactorGroupBox)).EndInit();
            this.HotSpotFactorGroupBox.ResumeLayout(false);
            this.HotSpotFactorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hotSpotFactorSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hotSpotFactorLabelText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopOilGroupBox)).EndInit();
            this.TopOilGroupBox.ResumeLayout(false);
            this.TopOilGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTemperatureComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingCalculationMethodRadGroupBox)).EndInit();
            this.agingCalculationMethodRadGroupBox.ResumeLayout(false);
            this.agingCalculationMethodRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.agingCalculationMethodRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsGeneralSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsGeneralSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionWhsGeneralSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionWhsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceWhsGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionWhsGeneralSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionWhsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationWhsGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformerTypeRadGroupBox)).EndInit();
            this.transformerTypeRadGroupBox.ResumeLayout(false);
            this.transformerTypeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transformerTypeRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transformerConfigurationRadGroupBox)).EndInit();
            this.transformerConfigurationRadGroupBox.ResumeLayout(false);
            this.transformerConfigurationRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transformerConfigurationRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGroupBox)).EndInit();
            this.maximumRatedCurrentRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maximumRatedCurrentRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsGeneralSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsWhsGeneralSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsWhsGeneralSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionRadGroupBox)).EndInit();
            this.firmwareVersionRadGroupBox.ResumeLayout(false);
            this.firmwareVersionRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firmwareVersionValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempRiseSettingsRadGroupBox)).EndInit();
            this.tempRiseSettingsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRiseOverTopOilRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureRiseOverTopOilRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsCalculationEnableDisableRadGroupBox)).EndInit();
            this.whsCalculationEnableDisableRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.disableMonitoringRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.enableMonitoringRadRadioButton)).EndInit();
            this.calculationSettingsRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseCLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseBLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PreviousAgingDaysPhaseALabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalculationSettingsTabRadGroupBox)).EndInit();
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsCalculationSettingsTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsCalculationSettingsTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionCalculationSettingsTabRadGroupBox)).EndInit();
            this.deviceInteractionCalculationSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionCalculationSettingsTabRadGroupBox)).EndInit();
            this.databaseInteractionCalculationSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseCalculationSettingsTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmVariantRadGroupBox)).EndInit();
            this.algorithmVariantRadGroupBox.ResumeLayout(false);
            this.algorithmVariantRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmVariantRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeConstantRadGroupBox)).EndInit();
            this.timeConstantRadGroupBox.ResumeLayout(false);
            this.timeConstantRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeContantMinutesRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeConstantRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentRadGroupBox)).EndInit();
            this.exponentRadGroupBox.ResumeLayout(false);
            this.exponentRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.exponentRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousAgingRadGroupBox)).EndInit();
            this.previousAgingRadGroupBox.ResumeLayout(false);
            this.previousAgingRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.valueSetInpedentlyFromOtherSettingsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingDaysTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setAgingDaysRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousAgingInDaysRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGroupBox)).EndInit();
            this.currentInputsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentInputsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGroupBox)).EndInit();
            this.temperatureInputsRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureInputsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationSettingsRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalculationSettingsTabRadGroupBox)).EndInit();
            this.availableConfigurationsCalculationSettingsTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsCalculationSettingsTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            this.alarmsAndFansRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmsAndFansTabRadGroupBox)).EndInit();
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.ResumeLayout(false);
            this.templateConfigurationsAlarmsAndFansTabRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copySelectedConfigurationAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.templateConfigurationsAlarmsAndFansTabRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deviceInteractionAlarmsAndFansTabRadGroupBox)).EndInit();
            this.deviceInteractionAlarmsAndFansTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.programDeviceAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDeviceAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseInteractionAlarmsAndFansTabRadGroupBox)).EndInit();
            this.databaseInteractionAlarmsAndFansTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deleteSelectedConfigurationAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveWorkingConfigurationAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadConfigurationFromDatabaseAlarmsAndFansTabRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureRadGroupBox)).EndInit();
            this.deadBandTemperatureRadGroupBox.ResumeLayout(false);
            this.deadBandTemperatureRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deadBandTemperatureTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmSettingsRadGroupBox)).EndInit();
            this.alarmSettingsRadGroupBox.ResumeLayout(false);
            this.alarmSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDelayRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmDelayTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempIsHighRadGroupBox)).EndInit();
            this.topOilTempIsHighRadGroupBox.ResumeLayout(false);
            this.topOilTempIsHighRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempHighAlarmSetPointRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempLowAlarmSetPointRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempHighAlarmSetPointTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTempLowAlarmSetPointTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsIsHighRadGroupBox)).EndInit();
            this.whsIsHighRadGroupBox.ResumeLayout(false);
            this.whsIsHighRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.whsHighAlarmSetPointRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsLowAlarmSetPointRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsHighAlarmSetPointTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsLowAlarmSetPointTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanSettingsRadGroupBox)).EndInit();
            this.fanSettingsRadGroupBox.ResumeLayout(false);
            this.fanSettingsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanCurrent2SpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanOperationalInfoGroupBox)).EndInit();
            this.fanOperationalInfoGroupBox.ResumeLayout(false);
            this.fanOperationalInfoGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fan2NumOfStartsSpinEdito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FanStatusLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan1NumOfStartsSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1Indicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan2RuntimeHoursSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsFan2StatsLabelText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fan1RuntimeHoursSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2Indicator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsRunTimeHoursLabelText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsFan1StatsLabelText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsNumOfStartsLabelText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanCurrent1SpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapRadGroupBox)).EndInit();
            this.fanBankSwapRadGroupBox.ResumeLayout(false);
            this.fanBankSwapRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapSetToZeroToDisableTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapEveryTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapIntervalInHoursRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankSwapHoursTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnableFanCurrentMonitoring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumFanRunTimeRadGroupBox)).EndInit();
            this.minimumFanRunTimeRadGroupBox.ResumeLayout(false);
            this.minimumFanRunTimeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minimumfanRunTimeInMinutesRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimumFanRunTimeMinutesTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group1CurrentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseRadGroupBox)).EndInit();
            this.fanAutoExerciseRadGroupBox.ResumeLayout(false);
            this.fanAutoExerciseRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ManualExcerciseLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseSetBothToZeroToDisableTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanTestButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseMinutesTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseDaysTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanExerciseTimeInMinutesRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanExerciseIntervalInDaysRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseForTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanAutoExerciseEveryTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Group2CurrentLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanStartTemperaturesRadGroupBox)).EndInit();
            this.fanStartTemperaturesRadGroupBox.ResumeLayout(false);
            this.fanStartTemperaturesRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoStartTempTopOilRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoStartTempWhsRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneStartTempWhsRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whsTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneStartTempTopOilRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankTwoTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fanBankOneTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topOilTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalibrateFanButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberofFanGroupsLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolingAlarmGroupBox)).EndInit();
            this.coolingAlarmGroupBox.ResumeLayout(false);
            this.coolingAlarmGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.highCoolingAlarmLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighCoolingAlarmSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowCoolingAlarmlabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowCoolingAlarmSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfCoolingGroupsComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alarmsAndFansRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmsAndFansTabRadGroupBox)).EndInit();
            this.availableConfigurationsAlarmsAndFansTabRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableConfigurationsAlarmsAndFansTabRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView configurationRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage whsGeneralSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadProgressBar whsGeneralSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceWhsGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseWhsGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsWhsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsWhsGeneralSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationWhsGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceWhsGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox firmwareVersionRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox tempRiseSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox maximumRatedCurrentRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox whsCalculationEnableDisableRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton disableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton enableMonitoringRadRadioButton;
        private Telerik.WinControls.UI.RadPageViewPage calculationSettingsRadPageViewPage;
        private Telerik.WinControls.UI.RadProgressBar calculationSettingsRadProgressBar;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsCalculationSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsCalculationSettingsTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton saveWorkingConfigurationCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadPageViewPage alarmsAndFansRadPageViewPage;
        private Telerik.WinControls.UI.RadProgressBar alarmsAndFansRadProgressBar;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDeviceAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadButton loadConfigurationFromDatabaseAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox availableConfigurationsAlarmsAndFansTabRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableConfigurationsAlarmsAndFansTabRadListControl;
        private Telerik.WinControls.UI.RadButton radButton11;
        internal Telerik.WinControls.UI.RadButton saveWorkingConfigurationAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadButton programDeviceAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox transformerTypeRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList transformerTypeRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox transformerConfigurationRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList transformerConfigurationRadDropDownList;
        private Telerik.WinControls.UI.RadGridView maximumRatedCurrentRadGridView;
        private Telerik.WinControls.UI.RadGridView temperatureRiseOverTopOilRadGridView;
        private Telerik.WinControls.UI.RadGroupBox timeConstantRadGroupBox;
        private Telerik.WinControls.UI.RadLabel timeContantMinutesRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor timeConstantRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox exponentRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor exponentRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox previousAgingRadGroupBox;
        private Telerik.WinControls.UI.RadLabel valueSetInpedentlyFromOtherSettingsRadLabel;
        private Telerik.WinControls.UI.RadLabel agingDaysTextRadLabel;
        private Telerik.WinControls.UI.RadButton setAgingDaysRadButton;
        private Telerik.WinControls.UI.RadSpinEditor previousAgingInDaysRadSpinEditor;
        private Telerik.WinControls.UI.RadGroupBox currentInputsRadGroupBox;
        private Telerik.WinControls.UI.RadGridView currentInputsRadGridView;
        private Telerik.WinControls.UI.RadGroupBox temperatureInputsRadGroupBox;
        private Telerik.WinControls.UI.RadGridView temperatureInputsRadGridView;
        private Telerik.WinControls.UI.RadGroupBox alarmSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox fanSettingsRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox fanStartTemperaturesRadGroupBox;
        private Telerik.WinControls.UI.RadLabel topOilTextRadLabel;
        private Telerik.WinControls.UI.RadLabel whsTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanBankTwoTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanBankOneTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox deadBandTemperatureRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor alarmDelayRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fanExerciseTimeInMinutesRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel alarmDelayTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox topOilTempIsHighRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor topOilTempHighAlarmSetPointRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor topOilTempLowAlarmSetPointRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel topOilTempHighAlarmSetPointTextRadLabel;
        private Telerik.WinControls.UI.RadLabel topOilTempLowAlarmSetPointTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox whsIsHighRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor whsHighAlarmSetPointRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor whsLowAlarmSetPointRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fanExerciseIntervalInDaysRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel whsHighAlarmSetPointTextRadLabel;
        private Telerik.WinControls.UI.RadLabel whsLowAlarmSetPointTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox fanBankSwapRadGroupBox;
        private Telerik.WinControls.UI.RadLabel fanBankSwapSetToZeroToDisableTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanBankSwapEveryTextRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor fanBankSwapIntervalInHoursRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel fanBankSwapHoursTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox minimumFanRunTimeRadGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor minimumfanRunTimeInMinutesRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel minimumFanRunTimeMinutesTextRadLabel;
        private Telerik.WinControls.UI.RadGroupBox fanAutoExerciseRadGroupBox;
        private Telerik.WinControls.UI.RadLabel fanAutoExerciseSetBothToZeroToDisableTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanAutoExerciseMinutesTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanAutoExerciseDaysTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanAutoExerciseForTextRadLabel;
        private Telerik.WinControls.UI.RadLabel fanAutoExerciseEveryTextRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor fanBankTwoStartTempTopOilRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fanBankTwoStartTempWhsRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fanBankOneStartTempTopOilRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fanBankOneStartTempWhsRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel firmwareVersionValueRadLabel;
        private Telerik.WinControls.UI.RadGroupBox algorithmVariantRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList algorithmVariantRadDropDownList;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionWhsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionWhsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionCalculationSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionCalculationSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox databaseInteractionAlarmsAndFansTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox deviceInteractionAlarmsAndFansTabRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsGeneralSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsGeneralSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsCalculationSettingsTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsCalculationSettingsTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadGroupBox templateConfigurationsAlarmsAndFansTabRadGroupBox;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadDropDownList templateConfigurationsAlarmsAndFansTabRadDropDownList;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationCalculationSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton deleteSelectedConfigurationAlarmsAndFansTabRadButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadTextBox PreviousAgingDaysPhaseC;
        private Telerik.WinControls.UI.RadTextBox PreviousAgingDaysPhaseB;
        private Telerik.WinControls.UI.RadTextBox PreviousAgingDaysPhaseA;
        private Telerik.WinControls.UI.RadLabel PreviousAgingDaysPhaseCLabel;
        private Telerik.WinControls.UI.RadLabel PreviousAgingDaysPhaseBLabel;
        private Telerik.WinControls.UI.RadLabel PreviousAgingDaysPhaseALabel;
        private Telerik.WinControls.UI.RadGroupBox fanOperationalInfoGroupBox;
        private Telerik.WinControls.UI.RadLabel whsFan2StatsLabelText;
        private Telerik.WinControls.UI.RadLabel whsRunTimeHoursLabelText;
        private Telerik.WinControls.UI.RadLabel whsFan1StatsLabelText;
        private Telerik.WinControls.UI.RadLabel whsNumOfStartsLabelText;
        private Telerik.WinControls.UI.RadButton copySelectedConfigurationGeneralSettingsTabRadButton;
        private Telerik.WinControls.UI.RadButton updateErrorButton;
        private Telerik.WinControls.UI.RadLabel ErrorListLabel;
        private Telerik.WinControls.UI.RadListControl whsErrorListBox;
        private Telerik.WinControls.UI.RadButton ClearAlarmListButton;
        private Telerik.WinControls.UI.RadButton UpdateAlarmListButton;
        private Telerik.WinControls.UI.RadLabel AlarmLabel;
        private Telerik.WinControls.UI.RadListControl AlarmListControl;
        private Telerik.WinControls.UI.RadGroupBox HotSpotFactorGroupBox;
        private Telerik.WinControls.UI.RadSpinEditor hotSpotFactorSpinEditor;
        private Telerik.WinControls.UI.RadLabel hotSpotFactorLabelText;
        private Telerik.WinControls.UI.RadGroupBox TopOilGroupBox;
        private Telerik.WinControls.UI.RadDropDownList topOilTemperatureComboBox;
        private Telerik.WinControls.UI.RadGroupBox agingCalculationMethodRadGroupBox;
        private Telerik.WinControls.UI.RadDropDownList agingCalculationMethodRadDropDownList;
        private Telerik.WinControls.UI.RadButton CalibrateFanButton;
        private Telerik.WinControls.UI.RadSpinEditor deadBandTemperatureRadSpinEditor;
        private Telerik.WinControls.UI.RadLabel deadBandTemperatureTextRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor fanCurrent2SpinEditor;
        private Telerik.WinControls.UI.RadLabel FanStatusLabel;
        private Telerik.WinControls.UI.RadButton Fan1Indicator;
        private Telerik.WinControls.UI.RadButton Fan2Indicator;
        private Telerik.WinControls.UI.RadSpinEditor fanCurrent1SpinEditor;
        private Telerik.WinControls.UI.RadCheckBox EnableFanCurrentMonitoring;
        private Telerik.WinControls.UI.RadLabel Group1CurrentLabel;
        private Telerik.WinControls.UI.RadLabel ManualExcerciseLabel;
        private Telerik.WinControls.UI.RadButton fanTestButton;
        private Telerik.WinControls.UI.RadLabel Group2CurrentLabel;
        private Telerik.WinControls.UI.RadLabel NumberofFanGroupsLabel;
        private Telerik.WinControls.UI.RadGroupBox coolingAlarmGroupBox;
        private Telerik.WinControls.UI.RadLabel highCoolingAlarmLabel;
        private Telerik.WinControls.UI.RadSpinEditor HighCoolingAlarmSpinEditor;
        private Telerik.WinControls.UI.RadLabel lowCoolingAlarmlabel;
        private Telerik.WinControls.UI.RadSpinEditor lowCoolingAlarmSpinEditor;
        private Telerik.WinControls.UI.RadDropDownList NumberOfCoolingGroupsComboBox;
        private Telerik.WinControls.UI.RadSpinEditor fan2NumOfStartsSpinEdito;
        private Telerik.WinControls.UI.RadSpinEditor fan1NumOfStartsSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fan2RuntimeHoursSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor fan1RuntimeHoursSpinEditor;
    }
}