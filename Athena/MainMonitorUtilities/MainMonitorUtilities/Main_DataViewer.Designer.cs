namespace MainMonitorUtilities
{
    partial class Main_DataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_DataViewer));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.dataRadPageView = new Telerik.WinControls.UI.RadPageView();
            this.trendRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.windingHotSpotRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.Fan1_RunTimeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.Fan2_RunTimeRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.WHS2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.AgingFac3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.AgingFac2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.WHS3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.agingFac1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.AccumAge1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.Fan2_StartsRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.Fan1_StartsRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.AccumAge3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.AccumAge2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.TopOilTempRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.WHS1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendCursorLabel = new System.Windows.Forms.Label();
            this.dynamicsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.vibration4RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.vibration3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.vibration2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.vibration1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog6RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog5RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog4RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.analog1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.moistureContentRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp4RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.temp2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.voltage3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.voltage2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent3RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.loadCurrent2RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.voltage1RadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.humidityRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.trendGraphMethodRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendAutomaticDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendManualDrawRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendDrawGraphsRadButton = new Telerik.WinControls.UI.RadButton();
            this.trendDisplayOptionsRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.normalizeDataRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.dataItemsRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.movingAverageRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.movingAverageRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.exponentialTrendRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.linearTrendRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.showTrendLineRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.dataAgeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.trendAllDataRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendOneYearRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendSixMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendThreeMonthsRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.trendZoomButtonsRadPanel = new Telerik.WinControls.UI.RadPanel();
            this.trendPointerRadioButton = new System.Windows.Forms.RadioButton();
            this.trendZoomInRadioButton = new System.Windows.Forms.RadioButton();
            this.trendZoomOutRadioButton = new System.Windows.Forms.RadioButton();
            this.trendWinChartViewer = new ChartDirector.WinChartViewer();
            this.trendRadHScrollBar = new Telerik.WinControls.UI.RadHScrollBar();
            this.dataGridRadPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridDataSaveDataToCsvRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataCancelChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataSaveChangesRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataEditRadButton = new Telerik.WinControls.UI.RadButton();
            this.gridDataDeleteSelectedRadButton = new Telerik.WinControls.UI.RadButton();
            this.dynamicsRadGridView = new Telerik.WinControls.UI.RadGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.trendRadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.dynamicsRadGroupBoxRadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataRadPageView)).BeginInit();
            this.dataRadPageView.SuspendLayout();
            this.trendRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windingHotSpotRadGroupBox)).BeginInit();
            this.windingHotSpotRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1_RunTimeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2_RunTimeRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgingFac3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgingFac2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingFac1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2_StartsRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1_StartsRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopOilTempRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGroupBox)).BeginInit();
            this.dynamicsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vibration4RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog6RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog5RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog4RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moistureContentRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp4RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent3RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent2RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage1RadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGraphMethodRadGroupBox)).BeginInit();
            this.trendGraphMethodRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendAutomaticDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendManualDrawRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawGraphsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDisplayOptionsRadGroupBox)).BeginInit();
            this.trendDisplayOptionsRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemsRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialTrendRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearTrendRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTrendLineRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataAgeRadGroupBox)).BeginInit();
            this.dataAgeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendAllDataRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendOneYearRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendSixMonthsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendThreeMonthsRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).BeginInit();
            this.trendZoomButtonsRadPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trendWinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendRadHScrollBar)).BeginInit();
            this.dataGridRadPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveDataToCsvRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataCancelChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveChangesRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataEditRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDeleteSelectedRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGridView.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataRadPageView
            // 
            this.dataRadPageView.Controls.Add(this.trendRadPageViewPage);
            this.dataRadPageView.Controls.Add(this.dataGridRadPageViewPage);
            this.dataRadPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataRadPageView.Location = new System.Drawing.Point(0, 0);
            this.dataRadPageView.Name = "dataRadPageView";
            this.dataRadPageView.SelectedPage = this.trendRadPageViewPage;
            this.dataRadPageView.Size = new System.Drawing.Size(1016, 736);
            this.dataRadPageView.TabIndex = 0;
            this.dataRadPageView.Text = "radPageView1";
            this.dataRadPageView.ThemeName = "Office2007Black";
            this.dataRadPageView.SelectedPageChanged += new System.EventHandler(this.dataRadPageView_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.dataRadPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // trendRadPageViewPage
            // 
            this.trendRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendRadPageViewPage.Controls.Add(this.windingHotSpotRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendCursorLabel);
            this.trendRadPageViewPage.Controls.Add(this.dynamicsRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendGraphMethodRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendDisplayOptionsRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.dataAgeRadGroupBox);
            this.trendRadPageViewPage.Controls.Add(this.trendZoomButtonsRadPanel);
            this.trendRadPageViewPage.Controls.Add(this.trendWinChartViewer);
            this.trendRadPageViewPage.Controls.Add(this.trendRadHScrollBar);
            this.trendRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.trendRadPageViewPage.Name = "trendRadPageViewPage";
            this.trendRadPageViewPage.Size = new System.Drawing.Size(995, 688);
            this.trendRadPageViewPage.Text = "Trend";
            // 
            // windingHotSpotRadGroupBox
            // 
            this.windingHotSpotRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.windingHotSpotRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.windingHotSpotRadGroupBox.Controls.Add(this.Fan1_RunTimeRadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.Fan2_RunTimeRadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.WHS2RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.AgingFac3RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.AgingFac2RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.WHS3RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.agingFac1RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.AccumAge1RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.Fan2_StartsRadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.Fan1_StartsRadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.AccumAge3RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.AccumAge2RadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.TopOilTempRadCheckBox);
            this.windingHotSpotRadGroupBox.Controls.Add(this.WHS1RadCheckBox);
            this.windingHotSpotRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windingHotSpotRadGroupBox.FooterImageIndex = -1;
            this.windingHotSpotRadGroupBox.FooterImageKey = "";
            this.windingHotSpotRadGroupBox.HeaderImageIndex = -1;
            this.windingHotSpotRadGroupBox.HeaderImageKey = "";
            this.windingHotSpotRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.windingHotSpotRadGroupBox.HeaderText = "Winding Hot Spot";
            this.windingHotSpotRadGroupBox.Location = new System.Drawing.Point(141, 87);
            this.windingHotSpotRadGroupBox.Name = "windingHotSpotRadGroupBox";
            this.windingHotSpotRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.windingHotSpotRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.windingHotSpotRadGroupBox.Size = new System.Drawing.Size(125, 334);
            this.windingHotSpotRadGroupBox.TabIndex = 94;
            this.windingHotSpotRadGroupBox.Text = "Winding Hot Spot";
            this.windingHotSpotRadGroupBox.ThemeName = "Office2007Black";
            this.windingHotSpotRadGroupBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.windingHotSpotRadGroupBox_MouseClick);
            // 
            // Fan1_RunTimeRadCheckBox
            // 
            this.Fan1_RunTimeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan1_RunTimeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Fan1_RunTimeRadCheckBox.Location = new System.Drawing.Point(6, 287);
            this.Fan1_RunTimeRadCheckBox.Name = "Fan1_RunTimeRadCheckBox";
            this.Fan1_RunTimeRadCheckBox.Size = new System.Drawing.Size(94, 16);
            this.Fan1_RunTimeRadCheckBox.TabIndex = 13;
            this.Fan1_RunTimeRadCheckBox.Text = "Runtime Fan 2";
            this.Fan1_RunTimeRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // Fan2_RunTimeRadCheckBox
            // 
            this.Fan2_RunTimeRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan2_RunTimeRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Fan2_RunTimeRadCheckBox.Location = new System.Drawing.Point(6, 309);
            this.Fan2_RunTimeRadCheckBox.Name = "Fan2_RunTimeRadCheckBox";
            this.Fan2_RunTimeRadCheckBox.Size = new System.Drawing.Size(98, 16);
            this.Fan2_RunTimeRadCheckBox.TabIndex = 3;
            this.Fan2_RunTimeRadCheckBox.Text = "RunTime Fan 2";
            this.Fan2_RunTimeRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // WHS2RadCheckBox
            // 
            this.WHS2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WHS2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.WHS2RadCheckBox.Location = new System.Drawing.Point(6, 45);
            this.WHS2RadCheckBox.Name = "WHS2RadCheckBox";
            this.WHS2RadCheckBox.Size = new System.Drawing.Size(80, 16);
            this.WHS2RadCheckBox.TabIndex = 12;
            this.WHS2RadCheckBox.Text = "WHS 2 (�C)";
            this.WHS2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // AgingFac3RadCheckBox
            // 
            this.AgingFac3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AgingFac3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AgingFac3RadCheckBox.Location = new System.Drawing.Point(6, 133);
            this.AgingFac3RadCheckBox.Name = "AgingFac3RadCheckBox";
            this.AgingFac3RadCheckBox.Size = new System.Drawing.Size(94, 16);
            this.AgingFac3RadCheckBox.TabIndex = 7;
            this.AgingFac3RadCheckBox.Text = "Aging Factor 3";
            this.AgingFac3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // AgingFac2RadCheckBox
            // 
            this.AgingFac2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AgingFac2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AgingFac2RadCheckBox.Location = new System.Drawing.Point(6, 111);
            this.AgingFac2RadCheckBox.Name = "AgingFac2RadCheckBox";
            this.AgingFac2RadCheckBox.Size = new System.Drawing.Size(94, 16);
            this.AgingFac2RadCheckBox.TabIndex = 9;
            this.AgingFac2RadCheckBox.Text = "Aging Factor 2";
            this.AgingFac2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // WHS3RadCheckBox
            // 
            this.WHS3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WHS3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.WHS3RadCheckBox.Location = new System.Drawing.Point(6, 67);
            this.WHS3RadCheckBox.Name = "WHS3RadCheckBox";
            this.WHS3RadCheckBox.Size = new System.Drawing.Size(80, 16);
            this.WHS3RadCheckBox.TabIndex = 8;
            this.WHS3RadCheckBox.Text = "WHS 3 (�C)";
            this.WHS3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // agingFac1RadCheckBox
            // 
            this.agingFac1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agingFac1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.agingFac1RadCheckBox.Location = new System.Drawing.Point(6, 89);
            this.agingFac1RadCheckBox.Name = "agingFac1RadCheckBox";
            this.agingFac1RadCheckBox.Size = new System.Drawing.Size(94, 16);
            this.agingFac1RadCheckBox.TabIndex = 6;
            this.agingFac1RadCheckBox.Text = "Aging Factor 1";
            this.agingFac1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // AccumAge1RadCheckBox
            // 
            this.AccumAge1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccumAge1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AccumAge1RadCheckBox.Location = new System.Drawing.Point(6, 155);
            this.AccumAge1RadCheckBox.Name = "AccumAge1RadCheckBox";
            this.AccumAge1RadCheckBox.Size = new System.Drawing.Size(118, 16);
            this.AccumAge1RadCheckBox.TabIndex = 5;
            this.AccumAge1RadCheckBox.Text = "Accumulated Age 1";
            this.AccumAge1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // Fan2_StartsRadCheckBox
            // 
            this.Fan2_StartsRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan2_StartsRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Fan2_StartsRadCheckBox.Location = new System.Drawing.Point(6, 265);
            this.Fan2_StartsRadCheckBox.Name = "Fan2_StartsRadCheckBox";
            this.Fan2_StartsRadCheckBox.Size = new System.Drawing.Size(82, 16);
            this.Fan2_StartsRadCheckBox.TabIndex = 2;
            this.Fan2_StartsRadCheckBox.Text = "Fan 2 Starts";
            this.Fan2_StartsRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // Fan1_StartsRadCheckBox
            // 
            this.Fan1_StartsRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fan1_StartsRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Fan1_StartsRadCheckBox.Location = new System.Drawing.Point(6, 243);
            this.Fan1_StartsRadCheckBox.Name = "Fan1_StartsRadCheckBox";
            this.Fan1_StartsRadCheckBox.Size = new System.Drawing.Size(82, 16);
            this.Fan1_StartsRadCheckBox.TabIndex = 4;
            this.Fan1_StartsRadCheckBox.Text = "Fan 1 Starts";
            this.Fan1_StartsRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // AccumAge3RadCheckBox
            // 
            this.AccumAge3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccumAge3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AccumAge3RadCheckBox.Location = new System.Drawing.Point(6, 199);
            this.AccumAge3RadCheckBox.Name = "AccumAge3RadCheckBox";
            this.AccumAge3RadCheckBox.Size = new System.Drawing.Size(118, 16);
            this.AccumAge3RadCheckBox.TabIndex = 2;
            this.AccumAge3RadCheckBox.Text = "Accumulated Age 3";
            this.AccumAge3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // AccumAge2RadCheckBox
            // 
            this.AccumAge2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccumAge2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.AccumAge2RadCheckBox.Location = new System.Drawing.Point(6, 177);
            this.AccumAge2RadCheckBox.Name = "AccumAge2RadCheckBox";
            this.AccumAge2RadCheckBox.Size = new System.Drawing.Size(118, 16);
            this.AccumAge2RadCheckBox.TabIndex = 3;
            this.AccumAge2RadCheckBox.Text = "Accumulated Age 2";
            this.AccumAge2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // TopOilTempRadCheckBox
            // 
            this.TopOilTempRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TopOilTempRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.TopOilTempRadCheckBox.Location = new System.Drawing.Point(6, 221);
            this.TopOilTempRadCheckBox.Name = "TopOilTempRadCheckBox";
            this.TopOilTempRadCheckBox.Size = new System.Drawing.Size(113, 16);
            this.TopOilTempRadCheckBox.TabIndex = 1;
            this.TopOilTempRadCheckBox.Text = "Top Oil Temp (�C)";
            this.TopOilTempRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // WHS1RadCheckBox
            // 
            this.WHS1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WHS1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.WHS1RadCheckBox.Location = new System.Drawing.Point(6, 23);
            this.WHS1RadCheckBox.Name = "WHS1RadCheckBox";
            this.WHS1RadCheckBox.Size = new System.Drawing.Size(80, 16);
            this.WHS1RadCheckBox.TabIndex = 0;
            this.WHS1RadCheckBox.Text = "WHS 1 (�C)";
            this.WHS1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // trendCursorLabel
            // 
            this.trendCursorLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.trendCursorLabel.Location = new System.Drawing.Point(491, 126);
            this.trendCursorLabel.Name = "trendCursorLabel";
            this.trendCursorLabel.Size = new System.Drawing.Size(12, 436);
            this.trendCursorLabel.TabIndex = 93;
            this.trendCursorLabel.Visible = false;
            // 
            // dynamicsRadGroupBox
            // 
            this.dynamicsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dynamicsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dynamicsRadGroupBox.Controls.Add(this.vibration4RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.vibration3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.vibration2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.vibration1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog6RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog5RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog4RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.analog1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.moistureContentRadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp4RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.temp2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.voltage3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.voltage2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent3RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.loadCurrent2RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.voltage1RadCheckBox);
            this.dynamicsRadGroupBox.Controls.Add(this.humidityRadCheckBox);
            this.dynamicsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dynamicsRadGroupBox.FooterImageIndex = -1;
            this.dynamicsRadGroupBox.FooterImageKey = "";
            this.dynamicsRadGroupBox.HeaderImageIndex = -1;
            this.dynamicsRadGroupBox.HeaderImageKey = "";
            this.dynamicsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dynamicsRadGroupBox.HeaderText = "Dynamics";
            this.dynamicsRadGroupBox.Location = new System.Drawing.Point(6, 87);
            this.dynamicsRadGroupBox.Name = "dynamicsRadGroupBox";
            this.dynamicsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dynamicsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dynamicsRadGroupBox.Size = new System.Drawing.Size(129, 509);
            this.dynamicsRadGroupBox.TabIndex = 92;
            this.dynamicsRadGroupBox.Text = "Dynamics";
            this.dynamicsRadGroupBox.ThemeName = "Office2007Black";
            this.dynamicsRadGroupBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // vibration4RadCheckBox
            // 
            this.vibration4RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vibration4RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.vibration4RadCheckBox.Location = new System.Drawing.Point(6, 485);
            this.vibration4RadCheckBox.Name = "vibration4RadCheckBox";
            this.vibration4RadCheckBox.Size = new System.Drawing.Size(74, 16);
            this.vibration4RadCheckBox.TabIndex = 15;
            this.vibration4RadCheckBox.Text = "Vibration 4";
            this.vibration4RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // vibration3RadCheckBox
            // 
            this.vibration3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vibration3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.vibration3RadCheckBox.Location = new System.Drawing.Point(6, 463);
            this.vibration3RadCheckBox.Name = "vibration3RadCheckBox";
            this.vibration3RadCheckBox.Size = new System.Drawing.Size(74, 16);
            this.vibration3RadCheckBox.TabIndex = 15;
            this.vibration3RadCheckBox.Text = "Vibration 3";
            this.vibration3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // vibration2RadCheckBox
            // 
            this.vibration2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vibration2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.vibration2RadCheckBox.Location = new System.Drawing.Point(6, 441);
            this.vibration2RadCheckBox.Name = "vibration2RadCheckBox";
            this.vibration2RadCheckBox.Size = new System.Drawing.Size(74, 16);
            this.vibration2RadCheckBox.TabIndex = 15;
            this.vibration2RadCheckBox.Text = "Vibration 2";
            this.vibration2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // vibration1RadCheckBox
            // 
            this.vibration1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vibration1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.vibration1RadCheckBox.Location = new System.Drawing.Point(6, 419);
            this.vibration1RadCheckBox.Name = "vibration1RadCheckBox";
            this.vibration1RadCheckBox.Size = new System.Drawing.Size(74, 16);
            this.vibration1RadCheckBox.TabIndex = 16;
            this.vibration1RadCheckBox.Text = "Vibration 1";
            this.vibration1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog6RadCheckBox
            // 
            this.analog6RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog6RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog6RadCheckBox.Location = new System.Drawing.Point(6, 397);
            this.analog6RadCheckBox.Name = "analog6RadCheckBox";
            this.analog6RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog6RadCheckBox.TabIndex = 14;
            this.analog6RadCheckBox.Text = "Analog 6";
            this.analog6RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog5RadCheckBox
            // 
            this.analog5RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog5RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog5RadCheckBox.Location = new System.Drawing.Point(6, 375);
            this.analog5RadCheckBox.Name = "analog5RadCheckBox";
            this.analog5RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog5RadCheckBox.TabIndex = 15;
            this.analog5RadCheckBox.Text = "Analog 5";
            this.analog5RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog4RadCheckBox
            // 
            this.analog4RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog4RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog4RadCheckBox.Location = new System.Drawing.Point(6, 353);
            this.analog4RadCheckBox.Name = "analog4RadCheckBox";
            this.analog4RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog4RadCheckBox.TabIndex = 14;
            this.analog4RadCheckBox.Text = "Analog 4";
            this.analog4RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog3RadCheckBox
            // 
            this.analog3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog3RadCheckBox.Location = new System.Drawing.Point(6, 331);
            this.analog3RadCheckBox.Name = "analog3RadCheckBox";
            this.analog3RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog3RadCheckBox.TabIndex = 14;
            this.analog3RadCheckBox.Text = "Analog 3";
            this.analog3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog2RadCheckBox
            // 
            this.analog2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog2RadCheckBox.Location = new System.Drawing.Point(6, 309);
            this.analog2RadCheckBox.Name = "analog2RadCheckBox";
            this.analog2RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog2RadCheckBox.TabIndex = 14;
            this.analog2RadCheckBox.Text = "Analog 2";
            this.analog2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // analog1RadCheckBox
            // 
            this.analog1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analog1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.analog1RadCheckBox.Location = new System.Drawing.Point(6, 287);
            this.analog1RadCheckBox.Name = "analog1RadCheckBox";
            this.analog1RadCheckBox.Size = new System.Drawing.Size(65, 16);
            this.analog1RadCheckBox.TabIndex = 13;
            this.analog1RadCheckBox.Text = "Analog 1";
            this.analog1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // moistureContentRadCheckBox
            // 
            this.moistureContentRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moistureContentRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.moistureContentRadCheckBox.Location = new System.Drawing.Point(6, 45);
            this.moistureContentRadCheckBox.Name = "moistureContentRadCheckBox";
            this.moistureContentRadCheckBox.Size = new System.Drawing.Size(105, 16);
            this.moistureContentRadCheckBox.TabIndex = 12;
            this.moistureContentRadCheckBox.Text = "Moisture (kg/M3)";
            this.moistureContentRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            // 
            // temp4RadCheckBox
            // 
            this.temp4RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp4RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp4RadCheckBox.Location = new System.Drawing.Point(6, 133);
            this.temp4RadCheckBox.Name = "temp4RadCheckBox";
            this.temp4RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp4RadCheckBox.TabIndex = 7;
            this.temp4RadCheckBox.Text = "Temp 4 (�C)";
            this.temp4RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.temp4RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // temp3RadCheckBox
            // 
            this.temp3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp3RadCheckBox.Location = new System.Drawing.Point(6, 111);
            this.temp3RadCheckBox.Name = "temp3RadCheckBox";
            this.temp3RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp3RadCheckBox.TabIndex = 9;
            this.temp3RadCheckBox.Text = "Temp 3 (�C)";
            this.temp3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.temp3RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // temp1RadCheckBox
            // 
            this.temp1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp1RadCheckBox.Location = new System.Drawing.Point(6, 67);
            this.temp1RadCheckBox.Name = "temp1RadCheckBox";
            this.temp1RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp1RadCheckBox.TabIndex = 8;
            this.temp1RadCheckBox.Text = "Temp 1 (�C)";
            this.temp1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.temp1RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // temp2RadCheckBox
            // 
            this.temp2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.temp2RadCheckBox.Location = new System.Drawing.Point(6, 89);
            this.temp2RadCheckBox.Name = "temp2RadCheckBox";
            this.temp2RadCheckBox.Size = new System.Drawing.Size(83, 16);
            this.temp2RadCheckBox.TabIndex = 6;
            this.temp2RadCheckBox.Text = "Temp 2 (�C)";
            this.temp2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.temp2RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // loadCurrent1RadCheckBox
            // 
            this.loadCurrent1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent1RadCheckBox.Location = new System.Drawing.Point(6, 155);
            this.loadCurrent1RadCheckBox.Name = "loadCurrent1RadCheckBox";
            this.loadCurrent1RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent1RadCheckBox.TabIndex = 5;
            this.loadCurrent1RadCheckBox.Text = "Load Current 1 (A)";
            this.loadCurrent1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.loadCurrent1RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // voltage3RadCheckBox
            // 
            this.voltage3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage3RadCheckBox.Location = new System.Drawing.Point(6, 265);
            this.voltage3RadCheckBox.Name = "voltage3RadCheckBox";
            this.voltage3RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage3RadCheckBox.TabIndex = 2;
            this.voltage3RadCheckBox.Text = "Voltage 3 (kV)";
            this.voltage3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.voltage3RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // voltage2RadCheckBox
            // 
            this.voltage2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage2RadCheckBox.Location = new System.Drawing.Point(6, 243);
            this.voltage2RadCheckBox.Name = "voltage2RadCheckBox";
            this.voltage2RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage2RadCheckBox.TabIndex = 4;
            this.voltage2RadCheckBox.Text = "Voltage 2 (kV)";
            this.voltage2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.voltage2RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // loadCurrent3RadCheckBox
            // 
            this.loadCurrent3RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent3RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent3RadCheckBox.Location = new System.Drawing.Point(6, 199);
            this.loadCurrent3RadCheckBox.Name = "loadCurrent3RadCheckBox";
            this.loadCurrent3RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent3RadCheckBox.TabIndex = 2;
            this.loadCurrent3RadCheckBox.Text = "Load Current 3 (A)";
            this.loadCurrent3RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.loadCurrent3RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // loadCurrent2RadCheckBox
            // 
            this.loadCurrent2RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadCurrent2RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadCurrent2RadCheckBox.Location = new System.Drawing.Point(6, 177);
            this.loadCurrent2RadCheckBox.Name = "loadCurrent2RadCheckBox";
            this.loadCurrent2RadCheckBox.Size = new System.Drawing.Size(114, 16);
            this.loadCurrent2RadCheckBox.TabIndex = 3;
            this.loadCurrent2RadCheckBox.Text = "Load Current 2 (A)";
            this.loadCurrent2RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.loadCurrent2RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // voltage1RadCheckBox
            // 
            this.voltage1RadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltage1RadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.voltage1RadCheckBox.Location = new System.Drawing.Point(6, 221);
            this.voltage1RadCheckBox.Name = "voltage1RadCheckBox";
            this.voltage1RadCheckBox.Size = new System.Drawing.Size(92, 16);
            this.voltage1RadCheckBox.TabIndex = 1;
            this.voltage1RadCheckBox.Text = "Voltage 1 (kV)";
            this.voltage1RadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.voltage1RadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // humidityRadCheckBox
            // 
            this.humidityRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.humidityRadCheckBox.Location = new System.Drawing.Point(6, 23);
            this.humidityRadCheckBox.Name = "humidityRadCheckBox";
            this.humidityRadCheckBox.Size = new System.Drawing.Size(85, 16);
            this.humidityRadCheckBox.TabIndex = 0;
            this.humidityRadCheckBox.Text = "Humidity (%)";
            this.humidityRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.humidityRadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // trendGraphMethodRadGroupBox
            // 
            this.trendGraphMethodRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendGraphMethodRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendGraphMethodRadGroupBox.Controls.Add(this.trendAutomaticDrawRadRadioButton);
            this.trendGraphMethodRadGroupBox.Controls.Add(this.trendManualDrawRadRadioButton);
            this.trendGraphMethodRadGroupBox.Controls.Add(this.trendDrawGraphsRadButton);
            this.trendGraphMethodRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendGraphMethodRadGroupBox.FooterImageIndex = -1;
            this.trendGraphMethodRadGroupBox.FooterImageKey = "";
            this.trendGraphMethodRadGroupBox.HeaderImageIndex = -1;
            this.trendGraphMethodRadGroupBox.HeaderImageKey = "";
            this.trendGraphMethodRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendGraphMethodRadGroupBox.HeaderText = "Graph Method";
            this.trendGraphMethodRadGroupBox.Location = new System.Drawing.Point(141, 584);
            this.trendGraphMethodRadGroupBox.Name = "trendGraphMethodRadGroupBox";
            this.trendGraphMethodRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendGraphMethodRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendGraphMethodRadGroupBox.Size = new System.Drawing.Size(125, 101);
            this.trendGraphMethodRadGroupBox.TabIndex = 91;
            this.trendGraphMethodRadGroupBox.Text = "Graph Method";
            this.trendGraphMethodRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendAutomaticDrawRadRadioButton
            // 
            this.trendAutomaticDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAutomaticDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAutomaticDrawRadRadioButton.Location = new System.Drawing.Point(11, 23);
            this.trendAutomaticDrawRadRadioButton.Name = "trendAutomaticDrawRadRadioButton";
            this.trendAutomaticDrawRadRadioButton.Size = new System.Drawing.Size(101, 18);
            this.trendAutomaticDrawRadRadioButton.TabIndex = 29;
            this.trendAutomaticDrawRadRadioButton.Text = "Automatic Draw";
            this.trendAutomaticDrawRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.trendAutomaticDrawRadRadioButton_ToggleStateChanged);
            // 
            // trendManualDrawRadRadioButton
            // 
            this.trendManualDrawRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendManualDrawRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendManualDrawRadRadioButton.Location = new System.Drawing.Point(11, 40);
            this.trendManualDrawRadRadioButton.Name = "trendManualDrawRadRadioButton";
            this.trendManualDrawRadRadioButton.Size = new System.Drawing.Size(101, 18);
            this.trendManualDrawRadRadioButton.TabIndex = 1;
            this.trendManualDrawRadRadioButton.TabStop = true;
            this.trendManualDrawRadRadioButton.Text = "Manual Draw";
            this.trendManualDrawRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // trendDrawGraphsRadButton
            // 
            this.trendDrawGraphsRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDrawGraphsRadButton.Location = new System.Drawing.Point(10, 64);
            this.trendDrawGraphsRadButton.Name = "trendDrawGraphsRadButton";
            this.trendDrawGraphsRadButton.Size = new System.Drawing.Size(107, 29);
            this.trendDrawGraphsRadButton.TabIndex = 28;
            this.trendDrawGraphsRadButton.Text = "Draw Graph";
            this.trendDrawGraphsRadButton.ThemeName = "Office2007Black";
            this.trendDrawGraphsRadButton.Click += new System.EventHandler(this.trendDrawGraphsRadButton_Click);
            // 
            // trendDisplayOptionsRadGroupBox
            // 
            this.trendDisplayOptionsRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.trendDisplayOptionsRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.normalizeDataRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.dataItemsRadLabel);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.movingAverageRadSpinEditor);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.movingAverageRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.exponentialTrendRadRadioButton);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.linearTrendRadRadioButton);
            this.trendDisplayOptionsRadGroupBox.Controls.Add(this.showTrendLineRadCheckBox);
            this.trendDisplayOptionsRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendDisplayOptionsRadGroupBox.FooterImageIndex = -1;
            this.trendDisplayOptionsRadGroupBox.FooterImageKey = "";
            this.trendDisplayOptionsRadGroupBox.HeaderImageIndex = -1;
            this.trendDisplayOptionsRadGroupBox.HeaderImageKey = "";
            this.trendDisplayOptionsRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.trendDisplayOptionsRadGroupBox.HeaderText = "Display Options";
            this.trendDisplayOptionsRadGroupBox.Location = new System.Drawing.Point(141, 431);
            this.trendDisplayOptionsRadGroupBox.Name = "trendDisplayOptionsRadGroupBox";
            this.trendDisplayOptionsRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.trendDisplayOptionsRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.trendDisplayOptionsRadGroupBox.Size = new System.Drawing.Size(125, 147);
            this.trendDisplayOptionsRadGroupBox.TabIndex = 90;
            this.trendDisplayOptionsRadGroupBox.Text = "Display Options";
            this.trendDisplayOptionsRadGroupBox.ThemeName = "Office2007Black";
            // 
            // normalizeDataRadCheckBox
            // 
            this.normalizeDataRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalizeDataRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalizeDataRadCheckBox.Location = new System.Drawing.Point(11, 79);
            this.normalizeDataRadCheckBox.Name = "normalizeDataRadCheckBox";
            this.normalizeDataRadCheckBox.Size = new System.Drawing.Size(98, 16);
            this.normalizeDataRadCheckBox.TabIndex = 35;
            this.normalizeDataRadCheckBox.Text = "Normalize Data";
            this.normalizeDataRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.normalizeDataRadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // dataItemsRadLabel
            // 
            this.dataItemsRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataItemsRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataItemsRadLabel.Location = new System.Drawing.Point(62, 124);
            this.dataItemsRadLabel.Name = "dataItemsRadLabel";
            this.dataItemsRadLabel.Size = new System.Drawing.Size(58, 16);
            this.dataItemsRadLabel.TabIndex = 34;
            this.dataItemsRadLabel.Text = "data items";
            // 
            // movingAverageRadSpinEditor
            // 
            this.movingAverageRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.movingAverageRadSpinEditor.InterceptArrowKeys = false;
            this.movingAverageRadSpinEditor.Location = new System.Drawing.Point(13, 120);
            this.movingAverageRadSpinEditor.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.movingAverageRadSpinEditor.Name = "movingAverageRadSpinEditor";
            // 
            // 
            // 
            this.movingAverageRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.movingAverageRadSpinEditor.ShowBorder = true;
            this.movingAverageRadSpinEditor.Size = new System.Drawing.Size(39, 18);
            this.movingAverageRadSpinEditor.TabIndex = 33;
            this.movingAverageRadSpinEditor.TabStop = false;
            this.movingAverageRadSpinEditor.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.movingAverageRadSpinEditor.ValueChanged += new System.EventHandler(this.movingAverageRadSpinEditor_ValueChanged);
            // 
            // movingAverageRadCheckBox
            // 
            this.movingAverageRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.movingAverageRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.movingAverageRadCheckBox.Location = new System.Drawing.Point(11, 98);
            this.movingAverageRadCheckBox.Name = "movingAverageRadCheckBox";
            this.movingAverageRadCheckBox.Size = new System.Drawing.Size(111, 16);
            this.movingAverageRadCheckBox.TabIndex = 32;
            this.movingAverageRadCheckBox.Text = "Show Moving Avg";
            this.movingAverageRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.movingAverageRadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // exponentialTrendRadRadioButton
            // 
            this.exponentialTrendRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exponentialTrendRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.exponentialTrendRadRadioButton.Location = new System.Drawing.Point(11, 57);
            this.exponentialTrendRadRadioButton.Name = "exponentialTrendRadRadioButton";
            this.exponentialTrendRadRadioButton.Size = new System.Drawing.Size(90, 18);
            this.exponentialTrendRadRadioButton.TabIndex = 31;
            this.exponentialTrendRadRadioButton.TabStop = true;
            this.exponentialTrendRadRadioButton.Text = "Exponential";
            this.exponentialTrendRadRadioButton.ThemeName = "Office2007Black";
            this.exponentialTrendRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.exponentialTrendRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.exponentialTrendRadRadioButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // linearTrendRadRadioButton
            // 
            this.linearTrendRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linearTrendRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.linearTrendRadRadioButton.Location = new System.Drawing.Point(11, 40);
            this.linearTrendRadRadioButton.Name = "linearTrendRadRadioButton";
            this.linearTrendRadRadioButton.Size = new System.Drawing.Size(53, 18);
            this.linearTrendRadRadioButton.TabIndex = 30;
            this.linearTrendRadRadioButton.Text = "Linear";
            this.linearTrendRadRadioButton.ThemeName = "Office2007Black";
            this.linearTrendRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.linearTrendRadRadioButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // showTrendLineRadCheckBox
            // 
            this.showTrendLineRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showTrendLineRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.showTrendLineRadCheckBox.Location = new System.Drawing.Point(11, 23);
            this.showTrendLineRadCheckBox.Name = "showTrendLineRadCheckBox";
            this.showTrendLineRadCheckBox.Size = new System.Drawing.Size(106, 16);
            this.showTrendLineRadCheckBox.TabIndex = 29;
            this.showTrendLineRadCheckBox.Text = "Show Trend Line";
            this.showTrendLineRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.dataSelection_ToggleStateChanged);
            this.showTrendLineRadCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dynamicsRadGroupBox_MouseClick);
            // 
            // dataAgeRadGroupBox
            // 
            this.dataAgeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.dataAgeRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dataAgeRadGroupBox.Controls.Add(this.trendAllDataRadRadioButton);
            this.dataAgeRadGroupBox.Controls.Add(this.trendOneYearRadRadioButton);
            this.dataAgeRadGroupBox.Controls.Add(this.trendSixMonthsRadRadioButton);
            this.dataAgeRadGroupBox.Controls.Add(this.trendThreeMonthsRadRadioButton);
            this.dataAgeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataAgeRadGroupBox.FooterImageIndex = -1;
            this.dataAgeRadGroupBox.FooterImageKey = "";
            this.dataAgeRadGroupBox.HeaderImageIndex = -1;
            this.dataAgeRadGroupBox.HeaderImageKey = "";
            this.dataAgeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.dataAgeRadGroupBox.HeaderText = "Data Age";
            this.dataAgeRadGroupBox.Location = new System.Drawing.Point(105, 3);
            this.dataAgeRadGroupBox.Name = "dataAgeRadGroupBox";
            this.dataAgeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.dataAgeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.dataAgeRadGroupBox.Size = new System.Drawing.Size(161, 78);
            this.dataAgeRadGroupBox.TabIndex = 89;
            this.dataAgeRadGroupBox.Text = "Data Age";
            this.dataAgeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // trendAllDataRadRadioButton
            // 
            this.trendAllDataRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendAllDataRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendAllDataRadRadioButton.Location = new System.Drawing.Point(84, 47);
            this.trendAllDataRadRadioButton.Name = "trendAllDataRadRadioButton";
            this.trendAllDataRadRadioButton.Size = new System.Drawing.Size(49, 18);
            this.trendAllDataRadRadioButton.TabIndex = 3;
            this.trendAllDataRadRadioButton.TabStop = true;
            this.trendAllDataRadRadioButton.Text = "All";
            this.trendAllDataRadRadioButton.ThemeName = "Office2007Black";
            this.trendAllDataRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // trendOneYearRadRadioButton
            // 
            this.trendOneYearRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendOneYearRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendOneYearRadRadioButton.Location = new System.Drawing.Point(84, 24);
            this.trendOneYearRadRadioButton.Name = "trendOneYearRadRadioButton";
            this.trendOneYearRadRadioButton.Size = new System.Drawing.Size(66, 18);
            this.trendOneYearRadRadioButton.TabIndex = 2;
            this.trendOneYearRadRadioButton.Text = "1 Year";
            this.trendOneYearRadRadioButton.ThemeName = "Office2007Black";
            // 
            // trendSixMonthsRadRadioButton
            // 
            this.trendSixMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendSixMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendSixMonthsRadRadioButton.Location = new System.Drawing.Point(13, 47);
            this.trendSixMonthsRadRadioButton.Name = "trendSixMonthsRadRadioButton";
            this.trendSixMonthsRadRadioButton.Size = new System.Drawing.Size(74, 18);
            this.trendSixMonthsRadRadioButton.TabIndex = 1;
            this.trendSixMonthsRadRadioButton.Text = "6 Months";
            this.trendSixMonthsRadRadioButton.ThemeName = "Office2007Black";
            // 
            // trendThreeMonthsRadRadioButton
            // 
            this.trendThreeMonthsRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendThreeMonthsRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendThreeMonthsRadRadioButton.Location = new System.Drawing.Point(14, 24);
            this.trendThreeMonthsRadRadioButton.Name = "trendThreeMonthsRadRadioButton";
            this.trendThreeMonthsRadRadioButton.Size = new System.Drawing.Size(73, 18);
            this.trendThreeMonthsRadRadioButton.TabIndex = 0;
            this.trendThreeMonthsRadRadioButton.Text = "3 Months";
            this.trendThreeMonthsRadRadioButton.ThemeName = "Office2007Black";
            this.trendThreeMonthsRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.trendThreeMonthsRadRadioButton_ToggleStateChanged);
            // 
            // trendZoomButtonsRadPanel
            // 
            this.trendZoomButtonsRadPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendPointerRadioButton);
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendZoomInRadioButton);
            this.trendZoomButtonsRadPanel.Controls.Add(this.trendZoomOutRadioButton);
            this.trendZoomButtonsRadPanel.Location = new System.Drawing.Point(3, 3);
            this.trendZoomButtonsRadPanel.Name = "trendZoomButtonsRadPanel";
            this.trendZoomButtonsRadPanel.Size = new System.Drawing.Size(96, 78);
            this.trendZoomButtonsRadPanel.TabIndex = 88;
            this.trendZoomButtonsRadPanel.ThemeName = "ControlDefault";
            // 
            // trendPointerRadioButton
            // 
            this.trendPointerRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendPointerRadioButton.Checked = true;
            this.trendPointerRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendPointerRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendPointerRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendPointerRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendPointerRadioButton.Image")));
            this.trendPointerRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendPointerRadioButton.Location = new System.Drawing.Point(3, 3);
            this.trendPointerRadioButton.Name = "trendPointerRadioButton";
            this.trendPointerRadioButton.Size = new System.Drawing.Size(90, 25);
            this.trendPointerRadioButton.TabIndex = 9;
            this.trendPointerRadioButton.TabStop = true;
            this.trendPointerRadioButton.Text = "Pointer";
            this.trendPointerRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.trendPointerRadioButton.CheckedChanged += new System.EventHandler(this.trendPointerRadioButton_CheckedChanged);
            // 
            // trendZoomInRadioButton
            // 
            this.trendZoomInRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendZoomInRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendZoomInRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendZoomInRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendZoomInRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendZoomInRadioButton.Image")));
            this.trendZoomInRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendZoomInRadioButton.Location = new System.Drawing.Point(3, 27);
            this.trendZoomInRadioButton.Name = "trendZoomInRadioButton";
            this.trendZoomInRadioButton.Size = new System.Drawing.Size(90, 25);
            this.trendZoomInRadioButton.TabIndex = 10;
            this.trendZoomInRadioButton.Text = "Zoom In";
            this.trendZoomInRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.trendZoomInRadioButton.CheckedChanged += new System.EventHandler(this.trendZoomInRadioButton_CheckedChanged);
            // 
            // trendZoomOutRadioButton
            // 
            this.trendZoomOutRadioButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.trendZoomOutRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.trendZoomOutRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trendZoomOutRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.trendZoomOutRadioButton.Image = ((System.Drawing.Image)(resources.GetObject("trendZoomOutRadioButton.Image")));
            this.trendZoomOutRadioButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trendZoomOutRadioButton.Location = new System.Drawing.Point(3, 51);
            this.trendZoomOutRadioButton.Name = "trendZoomOutRadioButton";
            this.trendZoomOutRadioButton.Size = new System.Drawing.Size(90, 24);
            this.trendZoomOutRadioButton.TabIndex = 11;
            this.trendZoomOutRadioButton.Text = "Zoom Out";
            this.trendZoomOutRadioButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.trendZoomOutRadioButton.CheckedChanged += new System.EventHandler(this.trendZoomOutRadioButton_CheckedChanged);
            // 
            // trendWinChartViewer
            // 
            this.trendWinChartViewer.Location = new System.Drawing.Point(272, 3);
            this.trendWinChartViewer.Name = "trendWinChartViewer";
            this.trendWinChartViewer.Size = new System.Drawing.Size(720, 660);
            this.trendWinChartViewer.TabIndex = 87;
            this.trendWinChartViewer.TabStop = false;
            this.trendWinChartViewer.ClickHotSpot += new ChartDirector.WinHotSpotEventHandler(this.trendWinChartViewer_ClickHotSpot);
            this.trendWinChartViewer.ViewPortChanged += new ChartDirector.WinViewPortEventHandler(this.trendWinChartViewer_ViewPortChanged);
            this.trendWinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trendWinChartViewer_MouseClick);
            this.trendWinChartViewer.MouseEnter += new System.EventHandler(this.trendWinChartViewer_MouseEnter);
            this.trendWinChartViewer.MouseLeave += new System.EventHandler(this.trendWinChartViewer_MouseLeave);
            // 
            // trendRadHScrollBar
            // 
            this.trendRadHScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trendRadHScrollBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.trendRadHScrollBar.Location = new System.Drawing.Point(272, 669);
            this.trendRadHScrollBar.Name = "trendRadHScrollBar";
            this.trendRadHScrollBar.Size = new System.Drawing.Size(719, 16);
            this.trendRadHScrollBar.TabIndex = 86;
            this.trendRadHScrollBar.Text = "radHScrollBar1";
            this.trendRadHScrollBar.ValueChanged += new System.EventHandler(this.trendRadHScrollBar_ValueChanged);
            // 
            // dataGridRadPageViewPage
            // 
            this.dataGridRadPageViewPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.dataGridRadPageViewPage.Controls.Add(this.gridDataSaveDataToCsvRadButton);
            this.dataGridRadPageViewPage.Controls.Add(this.gridDataCancelChangesRadButton);
            this.dataGridRadPageViewPage.Controls.Add(this.gridDataSaveChangesRadButton);
            this.dataGridRadPageViewPage.Controls.Add(this.gridDataEditRadButton);
            this.dataGridRadPageViewPage.Controls.Add(this.gridDataDeleteSelectedRadButton);
            this.dataGridRadPageViewPage.Controls.Add(this.dynamicsRadGridView);
            this.dataGridRadPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.dataGridRadPageViewPage.Name = "dataGridRadPageViewPage";
            this.dataGridRadPageViewPage.Size = new System.Drawing.Size(995, 688);
            this.dataGridRadPageViewPage.Text = "Data Grid";
            // 
            // gridDataSaveDataToCsvRadButton
            // 
            this.gridDataSaveDataToCsvRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataSaveDataToCsvRadButton.Location = new System.Drawing.Point(274, 661);
            this.gridDataSaveDataToCsvRadButton.Name = "gridDataSaveDataToCsvRadButton";
            this.gridDataSaveDataToCsvRadButton.Size = new System.Drawing.Size(117, 24);
            this.gridDataSaveDataToCsvRadButton.TabIndex = 23;
            this.gridDataSaveDataToCsvRadButton.Text = "Save Data to CSV";
            this.gridDataSaveDataToCsvRadButton.ThemeName = "Office2007Black";
            this.gridDataSaveDataToCsvRadButton.Click += new System.EventHandler(this.saveDataToCsvRadButton_Click);
            // 
            // gridDataCancelChangesRadButton
            // 
            this.gridDataCancelChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataCancelChangesRadButton.Location = new System.Drawing.Point(664, 661);
            this.gridDataCancelChangesRadButton.Name = "gridDataCancelChangesRadButton";
            this.gridDataCancelChangesRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataCancelChangesRadButton.TabIndex = 22;
            this.gridDataCancelChangesRadButton.Text = "Cancel Changes";
            this.gridDataCancelChangesRadButton.ThemeName = "Office2007Black";
            this.gridDataCancelChangesRadButton.Click += new System.EventHandler(this.gridDataCancelChangesRadButton_Click);
            // 
            // gridDataSaveChangesRadButton
            // 
            this.gridDataSaveChangesRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataSaveChangesRadButton.Location = new System.Drawing.Point(560, 661);
            this.gridDataSaveChangesRadButton.Name = "gridDataSaveChangesRadButton";
            this.gridDataSaveChangesRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataSaveChangesRadButton.TabIndex = 21;
            this.gridDataSaveChangesRadButton.Text = "Save Changes";
            this.gridDataSaveChangesRadButton.ThemeName = "Office2007Black";
            this.gridDataSaveChangesRadButton.Click += new System.EventHandler(this.gridDataSaveChangesRadButton_Click);
            // 
            // gridDataEditRadButton
            // 
            this.gridDataEditRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataEditRadButton.Location = new System.Drawing.Point(456, 661);
            this.gridDataEditRadButton.Name = "gridDataEditRadButton";
            this.gridDataEditRadButton.Size = new System.Drawing.Size(98, 24);
            this.gridDataEditRadButton.TabIndex = 20;
            this.gridDataEditRadButton.Text = "Edit Data";
            this.gridDataEditRadButton.ThemeName = "Office2007Black";
            this.gridDataEditRadButton.Click += new System.EventHandler(this.gridDataEditRadButton_Click);
            // 
            // gridDataDeleteSelectedRadButton
            // 
            this.gridDataDeleteSelectedRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.gridDataDeleteSelectedRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridDataDeleteSelectedRadButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridDataDeleteSelectedRadButton.Location = new System.Drawing.Point(814, 661);
            this.gridDataDeleteSelectedRadButton.Name = "gridDataDeleteSelectedRadButton";
            this.gridDataDeleteSelectedRadButton.Size = new System.Drawing.Size(178, 24);
            this.gridDataDeleteSelectedRadButton.TabIndex = 17;
            this.gridDataDeleteSelectedRadButton.Text = "Delete Selected Entries";
            this.gridDataDeleteSelectedRadButton.ThemeName = "Office2007Black";
            this.gridDataDeleteSelectedRadButton.Click += new System.EventHandler(this.deleteSelectedEntriesRadButton_Click);
            // 
            // dynamicsRadGridView
            // 
            this.dynamicsRadGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dynamicsRadGridView.AutoScroll = true;
            this.dynamicsRadGridView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dynamicsRadGridView.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.dynamicsRadGridView.MasterTemplate.MultiSelect = true;
            this.dynamicsRadGridView.Name = "dynamicsRadGridView";
            this.dynamicsRadGridView.Size = new System.Drawing.Size(989, 652);
            this.dynamicsRadGridView.TabIndex = 16;
            this.dynamicsRadGridView.Text = "radGridView1";
            // 
            // Main_DataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 736);
            this.Controls.Add(this.dataRadPageView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1024, 766);
            this.Name = "Main_DataViewer";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Main Monitor Data Viewer";
            this.ThemeName = "Office2007Black";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_DataViewer_FormClosing);
            this.Load += new System.EventHandler(this.Main_DataViewer_Load);
            this.SizeChanged += new System.EventHandler(this.Main_DataViewer_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataRadPageView)).EndInit();
            this.dataRadPageView.ResumeLayout(false);
            this.trendRadPageViewPage.ResumeLayout(false);
            this.trendRadPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windingHotSpotRadGroupBox)).EndInit();
            this.windingHotSpotRadGroupBox.ResumeLayout(false);
            this.windingHotSpotRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1_RunTimeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2_RunTimeRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgingFac3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AgingFac2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.agingFac1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan2_StartsRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Fan1_StartsRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccumAge2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopOilTempRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WHS1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGroupBox)).EndInit();
            this.dynamicsRadGroupBox.ResumeLayout(false);
            this.dynamicsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vibration4RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vibration1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog6RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog5RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog4RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analog1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moistureContentRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp4RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent3RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadCurrent2RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voltage1RadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humidityRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendGraphMethodRadGroupBox)).EndInit();
            this.trendGraphMethodRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendAutomaticDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendManualDrawRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDrawGraphsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendDisplayOptionsRadGroupBox)).EndInit();
            this.trendDisplayOptionsRadGroupBox.ResumeLayout(false);
            this.trendDisplayOptionsRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataItemsRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movingAverageRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialTrendRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearTrendRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showTrendLineRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataAgeRadGroupBox)).EndInit();
            this.dataAgeRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendAllDataRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendOneYearRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendSixMonthsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendThreeMonthsRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendZoomButtonsRadPanel)).EndInit();
            this.trendZoomButtonsRadPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trendWinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trendRadHScrollBar)).EndInit();
            this.dataGridRadPageViewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveDataToCsvRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataCancelChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataSaveChangesRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataEditRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataDeleteSelectedRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGridView.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dynamicsRadGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadPageView dataRadPageView;
        private Telerik.WinControls.UI.RadPageViewPage trendRadPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage dataGridRadPageViewPage;
        private ChartDirector.WinChartViewer trendWinChartViewer;
        private Telerik.WinControls.UI.RadHScrollBar trendRadHScrollBar;
        private Telerik.WinControls.UI.RadButton gridDataSaveDataToCsvRadButton;
        private Telerik.WinControls.UI.RadButton gridDataCancelChangesRadButton;
        private Telerik.WinControls.UI.RadButton gridDataSaveChangesRadButton;
        private Telerik.WinControls.UI.RadButton gridDataEditRadButton;
        private Telerik.WinControls.UI.RadButton gridDataDeleteSelectedRadButton;
        private Telerik.WinControls.UI.RadGridView dynamicsRadGridView;
        private Telerik.WinControls.UI.RadGroupBox trendGraphMethodRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton trendAutomaticDrawRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendManualDrawRadRadioButton;
        private Telerik.WinControls.UI.RadButton trendDrawGraphsRadButton;
        private Telerik.WinControls.UI.RadGroupBox trendDisplayOptionsRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox normalizeDataRadCheckBox;
        private Telerik.WinControls.UI.RadLabel dataItemsRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor movingAverageRadSpinEditor;
        private Telerik.WinControls.UI.RadCheckBox movingAverageRadCheckBox;
        private Telerik.WinControls.UI.RadRadioButton exponentialTrendRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton linearTrendRadRadioButton;
        private Telerik.WinControls.UI.RadCheckBox showTrendLineRadCheckBox;
        private Telerik.WinControls.UI.RadGroupBox dataAgeRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton trendAllDataRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendOneYearRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendSixMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton trendThreeMonthsRadRadioButton;
        private Telerik.WinControls.UI.RadPanel trendZoomButtonsRadPanel;
        private System.Windows.Forms.RadioButton trendPointerRadioButton;
        private System.Windows.Forms.RadioButton trendZoomInRadioButton;
        private System.Windows.Forms.RadioButton trendZoomOutRadioButton;
        private Telerik.WinControls.UI.RadGroupBox dynamicsRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox temp4RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox temp2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox voltage3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox voltage2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox loadCurrent2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox voltage1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox humidityRadCheckBox;
        private System.Windows.Forms.Label trendCursorLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private Telerik.WinControls.UI.RadContextMenu trendRadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu dynamicsRadGroupBoxRadContextMenu;
        private Telerik.WinControls.UI.RadCheckBox moistureContentRadCheckBox;
        private Telerik.WinControls.UI.RadGroupBox windingHotSpotRadGroupBox;
        private Telerik.WinControls.UI.RadCheckBox WHS2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox AgingFac3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox AgingFac2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox WHS3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox agingFac1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox AccumAge1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox Fan2_StartsRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox Fan1_StartsRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox AccumAge3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox AccumAge2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox TopOilTempRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox WHS1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox Fan1_RunTimeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox Fan2_RunTimeRadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog6RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog5RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog4RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox analog1RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox vibration4RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox vibration3RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox vibration2RadCheckBox;
        private Telerik.WinControls.UI.RadCheckBox vibration1RadCheckBox;
    }
}

