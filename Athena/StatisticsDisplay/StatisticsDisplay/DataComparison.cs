﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using ChartDirector;

using GeneralUtilities;

namespace StatisticsDisplay
{
    public partial class DataComparison : Telerik.WinControls.UI.RadForm
    {
        private static string copyGraphText = "Copy Graph";
        private static string inputDataContainedErrorsText = "Input data contained errors - exiting";

        private int chartWidthAdjustment = 80;
        private int chartHeightAdjustment = 80;

        private int chartBottomGap = 30;

        private int chartHeight;
        private int chartWidth;

        private int chartVerticalGap = 6;
        private int chartHorizontalEasement = 8;

        private int topChartStartX = 0;
        private int topChartStartY = 3;

        bool initializationIsComplete = false;

        List<int> colors;

        private DataViewerType dataViewerType;
        List<double[]> dataValues;
        List<double[]> normalizedDataValues;
        List<DataMeasurementUnit> dataUnits;
        List<DataMeasurementUnit> normalizedDataUnits;
        List<string> dataLabels;
        List<string> xAxisLabels;
        string titleString;
        bool isMultiChartGraph;

        public DataComparison(DataViewerType inputDataViewerType, List<double[]> inputDataValues, List<DataMeasurementUnit> inputMeasurementUnits, List<string> inputDataLabels, List<String> inputXAxisLabels, string inputTitleString, bool inputIsMultiChartGraph)
        {
            InitializeComponent();

            initializationIsComplete = false;

            dataViewerType = inputDataViewerType;
            dataValues = inputDataValues;
            dataUnits = inputMeasurementUnits;
            dataLabels = inputDataLabels;
            xAxisLabels = inputXAxisLabels;
            titleString = inputTitleString;
            isMultiChartGraph = inputIsMultiChartGraph;

            InitializeColorList();
            SetupChartContextMenu();
        }

        private void DataComparison_Load(object sender, EventArgs e)
        {
            try
            {
                initializationIsComplete = true;

                if (InputDataIsConsistent())
                {
                    CreateNormalizedData();
                    Size chartSize = ComputeChartSize();
                    this.chartWidth = chartSize.Width;
                    this.chartHeight = chartSize.Height;
                    LayoutCharts(chartSize);
                    DrawCharts();
                }
                else
                {
                    RadMessageBox.Show(this, inputDataContainedErrorsText);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.DataComparison_Load(object EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DataComparison_SizeChanged(object sender, EventArgs e)
        {
            Size chartSize = ComputeChartSize();
            this.chartWidth = chartSize.Width;
            this.chartHeight = chartSize.Height;
            LayoutCharts(chartSize);
            DrawCharts();
        }

        private void normalizeDataRadCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            DrawCharts();
        }

        private void dataComparisonWinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref chart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.dataComparisonWinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    graphRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void copyChart1Graph_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataComparisonChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(dataComparisonChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.trendCopyGraph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool OneSetOfInputDataIsConsistent(List<double[]> correlationValues, List<string> correlationLabels, List<string> xAxisLabels)
        {
            bool dataIsCorrect = false;
            try
            {
                bool isCorrect = true;
                int numberOfCorrelations;
                int numberOfChannelsOrSets;
                int index = 0;

                if (correlationValues.Count > 0)
                {
                    numberOfCorrelations = correlationValues.Count;
                    if (correlationLabels.Count == numberOfCorrelations)
                    {
                        if (correlationValues[0] != null)
                        {
                            numberOfChannelsOrSets = correlationValues[0].Length;

                            /// this only matters when you are graphing multiple bar charts
                            if (this.isMultiChartGraph && (xAxisLabels.Count != numberOfChannelsOrSets))
                            {
                                isCorrect = false;
                            }

                            index = 1;
                            while (isCorrect && (index < numberOfCorrelations))
                            {
                                if (correlationValues[index] != null)
                                {
                                    if (correlationValues[index].Length != numberOfChannelsOrSets)
                                    {
                                        isCorrect = false;
                                    }
                                }
                                else
                                {
                                    isCorrect = false;
                                }
                                index++;
                            }
                        }
                        else
                        {
                            isCorrect = false;
                        }
                    }
                    else
                    {
                        isCorrect = false;
                    }
                }
                else
                {
                    isCorrect = false;
                }
                /// if we wait until here to assign the value, we won't miss evaluating any code due to exceptions
                dataIsCorrect = isCorrect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.OneSetOfInputDataIsConsistent(List<double[]>, List<string>, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataIsCorrect;
        }

        private bool InputDataIsConsistent()
        {
            bool dataIsCorrect = false;
            try
            {
                bool isCorrect = OneSetOfInputDataIsConsistent(dataValues, dataLabels, xAxisLabels);
                
                dataIsCorrect = isCorrect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.InputDataIsConsistent()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataIsCorrect;
        }

        private void CreateNormalizedData()
        {
            double[] dataForOneMeasurementType;
            double maxValue;
            double currentValue;
            int i, j, numberOfMeasurements;

            normalizedDataValues = new List<double[]>();
            normalizedDataUnits = new List<DataMeasurementUnit>();

            for (i = 0; i < this.dataValues.Count; i++)
            {
                numberOfMeasurements = this.dataValues[0].Length;
                if (numberOfMeasurements > 0)
                {
                    dataForOneMeasurementType = new double[numberOfMeasurements];
                    maxValue = Math.Abs(this.dataValues[i][0]);
                    for (j = 1; j < numberOfMeasurements; j++)
                    {
                        currentValue = Math.Abs(this.dataValues[i][j]);
                        if (currentValue > maxValue)
                        {
                            maxValue = currentValue;
                        }
                    }
                    if (maxValue > 0)
                    {
                        for (j = 0; j < numberOfMeasurements; j++)
                        {
                            dataForOneMeasurementType[j] = this.dataValues[i][j] / maxValue;
                        }
                        normalizedDataValues.Add(dataForOneMeasurementType);
                        normalizedDataUnits.Add(DataMeasurementUnit.Unitless);
                    }
                }
            }
        }

        private void LayoutCharts(Size chartSize)
        {
            int startX;
            int startY;

            //if (this.dataViewerType == DataViewerType.BHM)
            //{
            startX = topChartStartX;
            startY = topChartStartY;

            dataComparisonChart1WinChartViewer.Size = chartSize;
            dataComparisonChart1WinChartViewer.Location = new Point(startX, startY);

            //}
            //else if (this.dataViewerType == DataViewerType.PDM)
            //{
            //    startX = topChartStartX;
            //    startY = topChartStartY;

            //    correlationChart1WinChartViewer.Size = chartSize;
            //    correlationChart1WinChartViewer.Location = new Point(startX, startY);

            //    startY = startY + chartVerticalGap + chartSize.Height;

            //    correlationChart2WinChartViewer.Size = chartSize;
            //    correlationChart2WinChartViewer.Location = new Point(startX, startY);

            //    startY = startY + chartVerticalGap + chartSize.Height;

            //    correlationChart3WinChartViewer.Size = chartSize;
            //    correlationChart3WinChartViewer.Location = new Point(startX, startY);
        }


        private void DrawCharts()
             {
                 try
                 {
                     if (initializationIsComplete)
                     {
                         if (normalizeDataRadCheckBox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                         {
                             DrawMultipleBarCharts(dataComparisonChart1WinChartViewer, normalizedDataValues, normalizedDataUnits, dataLabels, xAxisLabels, titleString);
                         }
                         else
                         {
                             DrawMultipleBarCharts(dataComparisonChart1WinChartViewer, dataValues, dataUnits, dataLabels, xAxisLabels, titleString);
                         }

                         //if (this.dataViewerType == DataViewerType.BHM)
                         //{
                         //    DrawMultipleBarCharts(correlationChart1WinChartViewer, graph1CorrelationValues, graph1CorrelationLabels, graph1XAxisLabels, graph1TitleString);
                         //}
                         //else if (this.dataViewerType == DataViewerType.PDM)
                         //{
                         //    DrawMultipleBarCharts(correlationChart1WinChartViewer, graph1CorrelationValues, graph1CorrelationLabels, graph1XAxisLabels, graph1TitleString);
                         //    DrawMultipleBarCharts(correlationChart2WinChartViewer, graph2CorrelationValues, graph2CorrelationLabels, graph2XAxisLabels, graph2TitleString);
                         //    DrawMultipleBarCharts(correlationChart3WinChartViewer, graph3CorrelationValues, graph3CorrelationLabels, graph3XAxisLabels, graph3TitleString);
                         //}

                     }
                 }
                 catch (Exception ex)
                 {
                     string errorMessage = "Exception thrown in DataComparison.DrawCharts()\nMessage: " + ex.Message;
                     LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                 }
             }

        /// <summary>
        /// Adds an additional axis type to the chart.  Each scale type (e.g. percentage, milliwatt, pulse count) requires a different axis.  The input Axis is set to point to the new
        /// axis instance.
        /// </summary>
        /// <param name="axisUseCount"></param>
        /// <param name="axisOffsetValue"></param>
        /// <param name="scaleLabel"></param>
        /// <param name="axisName"></param>
        /// <param name="xyChart"></param>
        private void SetupAdditionalAxis(int axisUseCount, int axisOffsetValue, string scaleLabel, ref Axis axisName, ref XYChart xyChart)
        {
            try
            {
                if (xyChart != null)
                {
                    if (axisUseCount < 1)
                    {
                        xyChart.yAxis().setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                    }
                    else
                    {
                        axisName = xyChart.addAxis(Chart.Left, axisUseCount * axisOffsetValue);
                        axisName.setTitle(scaleLabel).setAlignment(Chart.TopLeft2);
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DataViewer.SetupAdditionalAxis(int, int string, ref Axis, ref XYChart)\nInput ref XYChart was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.SetupAdditionalAxis(int, int string, ref Axis, ref XYChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetNumberOfUniqueYAxisScales(List<DataMeasurementUnit> graphDataUnits)
        {
            int numberOfUniqueScales = 0;
            try
            {
                Dictionary<DataMeasurementUnit, int> uniqueMeasurementUnits = new Dictionary<DataMeasurementUnit, int>();
                for (int i = 0; i < graphDataUnits.Count; i++)
                {
                    if (uniqueMeasurementUnits.ContainsKey(graphDataUnits[i]))
                    {
                        uniqueMeasurementUnits[graphDataUnits[i]]++;
                    }
                    else
                    {
                        uniqueMeasurementUnits.Add(graphDataUnits[i], 1);
                    }
                }
                numberOfUniqueScales = uniqueMeasurementUnits.Count;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DataViewer.GetNumberOfUniqueYAxisScales(int, int string, ref Axis, ref XYChart)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return numberOfUniqueScales;
        }

        private void DrawMultipleBarCharts(WinChartViewer viewer, List<double[]> graphDataValues, List<DataMeasurementUnit> graphDataUnits, List<string> graphDataLabels, List<string> xAxisLabels, string titleString)
        {
            try
            {
                //int chartWidth = viewer.Width;
                //int chartHeight = viewer.Height;
                Axis milliWattsAxis = null;
                Axis timesPerYearAxis = null;
                Axis milliVoltsAxis = null;
                Axis nanoCoulombsAxis = null;
                Axis pulsesAxis = null;
                Axis percentAxis = null;
                Axis degreesCentigradeAxis = null;
                Axis kilogramsPerMeterCubedAxis = null;
                Axis kiloVoltsAxis = null;
                Axis ampsAxis = null;
                Axis unitlessAxis = null;

                int graphWidth = this.chartWidth - this.chartWidthAdjustment;
                int graphHeight = this.chartHeight - this.chartHeightAdjustment;
                int startX = 50;
                int startY = 50;

                int graphXeasementForExtraScales = 0;

                int legendX = 65;
                int legendY = 20;

                int numberOfPoints = graphDataValues.Count;

                int yAxisUseCount = 0;
                int axisOffsetValue = 50;

                int totalYAxisUsed;

                string[] xLabels = new string[xAxisLabels.Count];

                for (int i = 0; i < xAxisLabels.Count; i++)
                {
                    xLabels[i] = xAxisLabels[i];
                }

                totalYAxisUsed = GetNumberOfUniqueYAxisScales(graphDataUnits);

                if (totalYAxisUsed > 1)
                {
                    graphXeasementForExtraScales = (totalYAxisUsed - 1) * axisOffsetValue;
                    graphWidth -= graphXeasementForExtraScales;
                    startX += graphXeasementForExtraScales;
                    legendX += graphXeasementForExtraScales;
                }

                // Create a XYChart object of size 400 x 240 pixels
                XYChart xyChart = new XYChart(this.chartWidth, this.chartHeight);

                // Add a title to the chart using 10 pt Arial font
                xyChart.addTitle(titleString, "", 10);

                // Set the plot area at (50, 25) and of size 320 x 180. Use two
                // alternative background colors (0xffffc0 and 0xffffe0)
                xyChart.setPlotArea(startX, startY, graphWidth, graphHeight, 0xffffc0, 0xffffe0);

                // Add a legend box at (55, 18) using horizontal layout. Use 8 pt Arial
                // font, with transparent background
                xyChart.addLegend(legendX, legendY, false, "", 8).setBackground(Chart.Transparent);

                // Add a title to the y-axis
                xyChart.yAxis().setTitle("graphData");

                // Reserve 20 pixels at the top of the y-axis for the legend box
                xyChart.yAxis().setTopMargin(20);

                // Set the x axis labels
                xyChart.xAxis().setLabels(xLabels);

                if (graphDataUnits.Contains(DataMeasurementUnit.MilliWatts))
                {
                    // this one is different from all the following ones because by default if the
                    // axis is present, it will be the "standard" y axis.
                   //  xyChart.yAxis().setTitle(DataMeasurementLabel.MilliWatts).setAlignment(Chart.TopLeft2);
                    if (milliWattsAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.MilliWatts, ref milliWattsAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.TimesPerYear))
                {
                    if (timesPerYearAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.TimesPerYear, ref timesPerYearAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.MilliVolts))
                {
                    if (milliVoltsAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.MilliVolts, ref milliVoltsAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.NanoCoulombs))
                {
                    if (nanoCoulombsAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.NanoCoulombs, ref nanoCoulombsAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.PulsesPerCycle))
                {
                    if (pulsesAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.PulsesPerCycle, ref pulsesAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                else if (graphDataUnits.Contains(DataMeasurementUnit.PulsesPerSecond))
                {
                    if (pulsesAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.PulsesPerSecond, ref pulsesAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.Percent))
                {
                    if (percentAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.Percent, ref percentAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.DegreesCentigrade))
                {
                    if (degreesCentigradeAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.DegreesCentigrade, ref degreesCentigradeAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.KilogramsPerMeterCubed))
                {
                    if (kilogramsPerMeterCubedAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.KilogramsPerMeterCubed, ref kilogramsPerMeterCubedAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.KiloVolts))
                {
                    if (kiloVoltsAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.KiloVolts, ref kiloVoltsAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.Amps))
                {
                    if (ampsAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.Amps, ref ampsAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }
                if (graphDataUnits.Contains(DataMeasurementUnit.Unitless))
                {
                    if (unitlessAxis == null)
                    {
                        SetupAdditionalAxis(yAxisUseCount, axisOffsetValue, DataMeasurementLabel.Unitless, ref unitlessAxis, ref xyChart);
                        yAxisUseCount++;
                    }
                }

                // Add a multi-bar layer with 3 data sets and 3 pixels 3D depth
                BarLayer layer = xyChart.addBarLayer2(Chart.Side, 3);

                ChartDirector.DataSet dataSet;

                for (int i = 0; i < graphDataValues.Count; i++)
                {
                    dataSet = layer.addDataSet(graphDataValues[i], this.colors[i], graphDataLabels[i]);

                    switch (graphDataUnits[i])
                    {
                        case DataMeasurementUnit.Amps:
                            dataSet.setUseYAxis(ampsAxis);
                            break;
                        case DataMeasurementUnit.DegreesCentigrade:
                            dataSet.setUseYAxis(degreesCentigradeAxis);
                            break;
                        case DataMeasurementUnit.KilogramsPerMeterCubed:
                            dataSet.setUseYAxis(kilogramsPerMeterCubedAxis);
                            break;
                        case DataMeasurementUnit.KiloVolts:
                            dataSet.setUseYAxis(kiloVoltsAxis);
                            break;
                        case DataMeasurementUnit.MilliVolts:
                            dataSet.setUseYAxis(milliVoltsAxis);
                            break;
                        case DataMeasurementUnit.MilliWatts:
                            dataSet.setUseYAxis(milliWattsAxis);
                            break;
                        case DataMeasurementUnit.NanoCoulombs:
                            dataSet.setUseYAxis(nanoCoulombsAxis);
                            break;
                        case DataMeasurementUnit.Percent:
                            dataSet.setUseYAxis(percentAxis);
                            break;
                        case DataMeasurementUnit.PulsesPerCycle:
                        case DataMeasurementUnit.PulsesPerSecond:
                            dataSet.setUseYAxis(pulsesAxis);
                            break;
                        case DataMeasurementUnit.TimesPerYear:
                            dataSet.setUseYAxis(timesPerYearAxis);
                            break;
                        case DataMeasurementUnit.Unitless:
                            dataSet.setUseYAxis(unitlessAxis);
                            break;
                    }
                }

                //dataSet = layer.addDataSet(graphDataValues[0], this.colors[0], graphDataLabels[0]);
                //if (numberOfPoints > 1)
                //{
                //    dataSet = layer.addDataSet(graphDataValues[1], this.colors[1], graphDataLabels[1]);
                //}
                //if (numberOfPoints > 2)
                //{
                //    layer.addDataSet(graphDataValues[2], this.colors[2], graphDataLabels[2]);
                //}
                //if (numberOfPoints > 3)
                //{
                //    layer.addDataSet(graphDataValues[3], this.colors[3], graphDataLabels[3]);
                //}
                //if (numberOfPoints > 4)
                //{
                //    layer.addDataSet(graphDataValues[4], this.colors[4], graphDataLabels[4]);
                //}

                // Output the chart
                viewer.Image = xyChart.makeImage();

                //include tool tip for the chart
                viewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                    "title='{dataSetName} on {xLabel}: {value} MBytes/hour'");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsongraphData.DrawMultipleBarCharts(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetupChartContextMenu()
        {
            RadMenuItem menuItem;
            chart1RadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += copyChart1Graph_Click;
            menuItem.Text = copyGraphText;
            chart1RadContextMenu.Items.Add(menuItem);
        }

        private void InitializeColorList()
        {
            try
            {
                this.colors = new List<int>();

                this.colors.Add(Chart.CColor(Color.Red));
                this.colors.Add(Chart.CColor(Color.Blue));
                this.colors.Add(Chart.CColor(Color.Green));
                this.colors.Add(Chart.CColor(Color.Yellow));
                this.colors.Add(Chart.CColor(Color.DarkGray));
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.InitializeColorList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Size ComputeChartSize()
        {
            Size chartSize = new Size(0, 0);
            try
            {
                int width = 0;
                int height = 0;
                //if (this.dataViewerType == DataViewerType.BHM)
                //{
                width = this.Width - chartHorizontalEasement;
                height = this.Height - 30 - chartVerticalGap - chartBottomGap;
                //}
                //else if (this.dataViewerType == DataViewerType.PDM)
                //{
                //    width = this.Width - chartHorizontalEasement;
                //    height = (this.Height - 40 - chartVerticalGap - chartBottomGap) / 3;
                //}
                chartSize = new Size(width, height);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DataComparison.ComputeChartSize()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chartSize;
        }


    }
}
