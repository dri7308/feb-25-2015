﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using ChartDirector;

using GeneralUtilities;

namespace StatisticsDisplay
{
    public partial class PearsonCorrelation : Telerik.WinControls.UI.RadForm
    {

        private static string copyGraphText = "Copy Graph";
        private static string inputDataContainedErrorsText = "Input data contained errors - exiting";

        private int chartWidthAdjustment = 80;
        private int chartHeightAdjustment = 55;

        private DataViewerType dataViewerType;
        
        private List<double[]> graph1CorrelationValues;
        private List<double[]> graph2CorrelationValues;
        private List<double[]> graph3CorrelationValues;

        private List<string> graph1CorrelationLabels;
        private List<string> graph2CorrelationLabels;
        private List<string> graph3CorrelationLabels;

        private List<string> graph1XAxisLabels;
        private List<string> graph2XAxisLabels;
        private List<string> graph3XAxisLabels;

        private string graph1TitleString;
        private string graph2TitleString;
        private string graph3TitleString;

        bool isMultiChartGraph;

        List<int> colors;

        private int chartHeight;
        private int chartWidth;

        private int chartVerticalGap = 6;
        private int chartHorizontalEasement = 8;

        private int topChartStartX = 0;
        private int topChartStartY = 3;

        private bool initializationIsComplete;

        public PearsonCorrelation(DataViewerType inputDataViewerType, List<double[]> inputCorrelationValues, List<string> inputCorrelationLabels, List<String> inputXAxisLabels, string inputTitleString, bool inputIsMultiChartGraph)
        {
            InitializeComponent();

            initializationIsComplete = false;

            dataViewerType = inputDataViewerType;
            graph1CorrelationValues = inputCorrelationValues;
            graph1CorrelationLabels = inputCorrelationLabels;
            graph1XAxisLabels = inputXAxisLabels;
            graph1TitleString = inputTitleString;
            isMultiChartGraph = inputIsMultiChartGraph;

            correlationChart2WinChartViewer.Visible = false;
            correlationChart3WinChartViewer.Visible = false;

            if (dataViewerType == DataViewerType.BHM)
            {
                this.Size = new Size(650, 270);
            }

            InitializeColorList();
            SetupChartContextMenus();
        }


        public PearsonCorrelation(DataViewerType inputDataViewerType,
                                  List<double[]> inputGraph1CorrelationValues, List<string> inputGraph1CorrelationLabels, List<String> inputGraph1XAxisLabels, string inputGraph1TitleString,
                                  List<double[]> inputGraph2CorrelationValues, List<string> inputGraph2CorrelationLabels, List<String> inputGraph2XAxisLabels, string inputGraph2TitleString,
                                  List<double[]> inputGraph3CorrelationValues, List<string> inputGraph3CorrelationLabels, List<String> inputGraph3XAxisLabels, string inputGraph3TitleString,
                                  bool inputIsMultiChartGraph)
        {
            InitializeComponent();

            initializationIsComplete = false;

            dataViewerType = inputDataViewerType;

            graph1CorrelationValues = inputGraph1CorrelationValues;
            graph1CorrelationLabels = inputGraph1CorrelationLabels;
            graph1XAxisLabels = inputGraph1XAxisLabels;
            graph1TitleString = inputGraph1TitleString;

            graph2CorrelationValues = inputGraph2CorrelationValues;
            graph2CorrelationLabels = inputGraph2CorrelationLabels;
            graph2XAxisLabels = inputGraph2XAxisLabels;
            graph2TitleString = inputGraph2TitleString;

            graph3CorrelationValues = inputGraph3CorrelationValues;
            graph3CorrelationLabels = inputGraph3CorrelationLabels;
            graph3XAxisLabels = inputGraph3XAxisLabels;
            graph3TitleString = inputGraph3TitleString;

            isMultiChartGraph = inputIsMultiChartGraph;

            InitializeColorList();
            SetupChartContextMenus();


        }

        private void PearsonCorrelation_Load(object sender, EventArgs e)
        {
            try
            {
                initializationIsComplete = true;

                if (InputDataIsConsistent())
                {
                    Size chartSize = ComputeChartSize();
                    this.chartWidth = chartSize.Width;
                    this.chartHeight = chartSize.Height;
                    LayoutCharts(chartSize);
                    DrawCharts();
                }
                else
                {
                    RadMessageBox.Show(this, inputDataContainedErrorsText);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.PearsonCorrelation_Load(object EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool OneSetOfInputDataIsConsistent(List<double[]> correlationValues, List<string> correlationLabels, List<string> xAxisLabels)
        {
            bool dataIsCorrect = false;
            try
            {
                bool isCorrect = true;
                int numberOfCorrelations;
                int numberOfChannelsOrSets;
                int index = 0;

                if (correlationValues.Count > 0)
                {
                    numberOfCorrelations = correlationValues.Count;
                    if (correlationLabels.Count == numberOfCorrelations)
                    {
                        if (correlationValues[0] != null)
                        {
                            numberOfChannelsOrSets = correlationValues[0].Length;

                            /// this only matters when you are graphing multiple bar charts
                            if (this.isMultiChartGraph && (xAxisLabels.Count != numberOfChannelsOrSets))
                            {
                                isCorrect = false;
                            }

                            index = 1;
                            while (isCorrect && (index < numberOfCorrelations))
                            {
                                if (correlationValues[index] != null)
                                {
                                    if (correlationValues[index].Length != numberOfChannelsOrSets)
                                    {
                                        isCorrect = false;
                                    }
                                }
                                else
                                {
                                    isCorrect = false;
                                }
                                index++;
                            }
                        }
                        else
                        {
                            isCorrect = false;
                        }
                    }
                    else
                    {
                        isCorrect = false;
                    }
                }
                else
                {
                    isCorrect = false;
                }
                /// if we wait until here to assign the value, we won't miss evaluating any code due to exceptions
                dataIsCorrect = isCorrect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.OneSetOfInputDataIsConsistent(List<double[]>, List<string>, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataIsCorrect;
        }

        private bool InputDataIsConsistent()
        {
            bool dataIsCorrect=false;
            try
            {
                bool isCorrect = OneSetOfInputDataIsConsistent(graph1CorrelationValues, graph1CorrelationLabels, graph1XAxisLabels);
                if (this.dataViewerType == DataViewerType.PDM)
                {
                    if (isCorrect)
                    {
                        isCorrect = OneSetOfInputDataIsConsistent(graph2CorrelationValues, graph2CorrelationLabels, graph2XAxisLabels);
                    }
                    if (isCorrect)
                    {
                        isCorrect = OneSetOfInputDataIsConsistent(graph3CorrelationValues, graph3CorrelationLabels, graph3XAxisLabels);
                    }
                }
                dataIsCorrect = isCorrect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.InputDataIsConsistent()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataIsCorrect;
        }

        private void InitializeColorList()
        {
            try
            {
                this.colors = new List<int>();

                this.colors.Add(Chart.CColor(Color.Red));
                this.colors.Add(Chart.CColor(Color.Blue));
                this.colors.Add(Chart.CColor(Color.Green));
                this.colors.Add(Chart.CColor(Color.Yellow));
                this.colors.Add(Chart.CColor(Color.DarkGray));
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.InitializeColorList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void correlationChart1WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref chart1RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.correlationChart1WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void correlationChart2WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref chart2RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.correlationChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        private void correlationChart3WinChartViewer_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                graphContextMenuEventHandler(sender, e, ref chart3RadContextMenu);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.correlationChart2WinChartViewer_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


//        private void DrawSingleBarChart(WinChartViewer viewer)
//        {
//            try
//            {
//                int chartWidth = viewer.Width;
//                int chartHeight = viewer.Height;
//                int graphWidth = chartWidth - chartWidthAdjustment;
//                int graphHeight = chartHeight - chartHeightAdjustment;
//                int startX = 50;
//                int startY = 25;

//                int legendX = 55;
//                int legendY = 18;

//                int numberOfPoints = this.correlationValues.Count;

//                string[] xLabels = new string[xAxisLabels.Count];
        
//                // Create a XYChart object of size 400 x 240 pixels
//                XYChart xyChart = new XYChart(chartWidth, chartHeight);

//                // Add a title to the chart using 10 pt Arial font
//                xyChart.addTitle(this.titleString, "", 10);

//                // Set the plot area at (50, 25) and of size 320 x 180. Use two
//                // alternative background colors (0xffffc0 and 0xffffe0)
//                xyChart.setPlotArea(startX, startY, graphWidth, graphHeight, 0xffffc0, 0xffffe0);

//                // Add a legend box at (55, 18) using horizontal layout. Use 8 pt Arial
//                // font, with transparent background
//                xyChart.addLegend(legendX, legendY, false, "", 8).setBackground(Chart.Transparent);

//                // Add a title to the y-axis
//                xyChart.yAxis().setTitle("Correlation");

//                // Reserve 20 pixels at the top of the y-axis for the legend box
//                xyChart.yAxis().setTopMargin(20);

//                // Add a multi-bar layer with 3 data sets and 3 pixels 3D depth
//                BarLayer layer = xyChart.addBarLayer2(Chart.Side, 3);

//                layer.addDataSet(this.correlationValues[0], this.colors[0], this.correlationLabels[0]);               

//                // Output the chart
//                viewer.Image = xyChart.makeImage();

//                //include tool tip for the chart
//                viewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
//                    "title='{dataSetName} on {xLabel}: {value} MBytes/hour'");
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PearsonCorrelation.DrawSingleBarChart(WinChartViewer)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void DrawCharts()
        {
            try
            {
                if (initializationIsComplete)
                {
                    if (this.dataViewerType == DataViewerType.BHM)
                    {
                        DrawMultipleBarCharts(correlationChart1WinChartViewer, graph1CorrelationValues, graph1CorrelationLabels, graph1XAxisLabels, graph1TitleString);
                    }
                    else if (this.dataViewerType == DataViewerType.PDM)
                    {
                        DrawMultipleBarCharts(correlationChart1WinChartViewer, graph1CorrelationValues, graph1CorrelationLabels, graph1XAxisLabels, graph1TitleString);
                        DrawMultipleBarCharts(correlationChart2WinChartViewer, graph2CorrelationValues, graph2CorrelationLabels, graph2XAxisLabels, graph2TitleString);
                        DrawMultipleBarCharts(correlationChart3WinChartViewer, graph3CorrelationValues, graph3CorrelationLabels, graph3XAxisLabels, graph3TitleString);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.DrawCharts()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void DrawMultipleBarCharts(WinChartViewer viewer, List<double[]> correlationValues, List<string> correlationLabels, List<string> xAxisLabels, string titleString)
        {
            // The data for the bar chart
            //double[] data0 = { 100, 125, 245, 147, 67 };
            //double[] data1 = { 85, 156, 179, 211, 123 };
            //double[] data2 = { 97, 87, 56, 267, 157 };
            //string[] labels = { "Mon", "Tue", "Wed", "Thu", "Fri" };
            try
            {
                //int chartWidth = viewer.Width;
                //int chartHeight = viewer.Height;
                int graphWidth = this.chartWidth - this.chartWidthAdjustment;
                int graphHeight = this.chartHeight - this.chartHeightAdjustment;
                int startX = 50;
                int startY = 25;

                int legendX = 55;
                int legendY = 18;

                int numberOfPoints = correlationValues.Count;

                string[] xLabels = new string[xAxisLabels.Count];

                for (int i = 0; i < xAxisLabels.Count; i++)
                {
                    xLabels[i] = xAxisLabels[i];
                }

                // Create a XYChart object of size 400 x 240 pixels
                XYChart xyChart = new XYChart(this.chartWidth, this.chartHeight);

                // Add a title to the chart using 10 pt Arial font
                xyChart.addTitle(titleString, "", 10);

                // Set the plot area at (50, 25) and of size 320 x 180. Use two
                // alternative background colors (0xffffc0 and 0xffffe0)
                xyChart.setPlotArea(startX, startY, graphWidth, graphHeight, 0xffffc0, 0xffffe0);

                // Add a legend box at (55, 18) using horizontal layout. Use 8 pt Arial
                // font, with transparent background
                xyChart.addLegend(legendX, legendY, false, "", 8).setBackground(Chart.Transparent);

                // Add a title to the y-axis
                xyChart.yAxis().setTitle("Correlation");

                // Reserve 20 pixels at the top of the y-axis for the legend box
                xyChart.yAxis().setTopMargin(20);

                // Set the x axis labels
                xyChart.xAxis().setLabels(xLabels);

                // Add a multi-bar layer with 3 data sets and 3 pixels 3D depth
                BarLayer layer = xyChart.addBarLayer2(Chart.Side, 3);

                layer.addDataSet(correlationValues[0], this.colors[0], correlationLabels[0]);
                if (numberOfPoints > 1)
                {
                    layer.addDataSet(correlationValues[1], this.colors[1], correlationLabels[1]);
                }
                if (numberOfPoints > 2)
                {
                    layer.addDataSet(correlationValues[2], this.colors[2], correlationLabels[2]);
                }
                if (numberOfPoints > 3)
                {
                    layer.addDataSet(correlationValues[3], this.colors[3], correlationLabels[3]);
                }
                if (numberOfPoints > 4)
                {
                    layer.addDataSet(correlationValues[4], this.colors[4], correlationLabels[4]);
                }

                // Output the chart
                viewer.Image = xyChart.makeImage();

                //include tool tip for the chart
                viewer.ImageMap = xyChart.getHTMLImageMap("clickable", "",
                    "title='{dataSetName} on {xLabel}: {value} MBytes/hour'");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.DrawMultipleBarCharts(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void graphContextMenuEventHandler(object sender, MouseEventArgs e, ref RadContextMenu graphRadContextMenu)
        {
            try
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point menuLocation = (sender as Control).PointToScreen(e.Location);
                    graphRadContextMenu.Show(menuLocation);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.graphContextMenuEventHandler(object, MouseEventArgs, ref RadContextMenu)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetupChartContextMenus()
        {
            RadMenuItem menuItem;

            chart1RadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += copyChart1Graph_Click;
            menuItem.Text = copyGraphText;
            chart1RadContextMenu.Items.Add(menuItem);

            chart2RadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += copyChart2Graph_Click;
            menuItem.Text = copyGraphText;
            chart2RadContextMenu.Items.Add(menuItem);

            chart3RadContextMenu.Items.Clear();

            menuItem = new RadMenuItem();
            menuItem.Click += copyChart3Graph_Click;
            menuItem.Text = copyGraphText;
            chart3RadContextMenu.Items.Add(menuItem);
        }

        private void copyChart1Graph_Click(object sender, EventArgs e)
        {
            try
            {
                if (correlationChart1WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(correlationChart1WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.copyChart1Graph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void copyChart2Graph_Click(object sender, EventArgs e)
        {
            try
            {
                if (correlationChart2WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(correlationChart2WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.copyChart2Graph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void copyChart3Graph_Click(object sender, EventArgs e)
        {
            try
            {
                if (correlationChart3WinChartViewer.Image != null)
                {
                    Clipboard.SetDataObject(correlationChart3WinChartViewer.Image, true);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.copyChart3Graph_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LayoutCharts(Size chartSize)
        {
            int startX;
            int startY;

            if (this.dataViewerType == DataViewerType.BHM)
            {
                startX = topChartStartX;
                startY = topChartStartY;

                correlationChart1WinChartViewer.Size = chartSize;
                correlationChart1WinChartViewer.Location = new Point(startX, startY);

            }
            else if (this.dataViewerType == DataViewerType.PDM)
            {
                startX = topChartStartX;
                startY = topChartStartY;

                correlationChart1WinChartViewer.Size = chartSize;
                correlationChart1WinChartViewer.Location = new Point(startX, startY);

                startY = startY + chartVerticalGap + chartSize.Height;

                correlationChart2WinChartViewer.Size = chartSize;
                correlationChart2WinChartViewer.Location = new Point(startX, startY);

                startY = startY + chartVerticalGap + chartSize.Height;

                correlationChart3WinChartViewer.Size = chartSize;
                correlationChart3WinChartViewer.Location = new Point(startX, startY);
            }
        }

        private Size ComputeChartSize()
        {
            Size chartSize = new Size(0, 0);
            try
            {
                int width = 0;
                int height = 0;
                if (this.dataViewerType == DataViewerType.BHM)
                {
                    width = this.Width - chartHorizontalEasement;
                    height = this.Height - 30 - chartVerticalGap;
                }
                else if (this.dataViewerType == DataViewerType.PDM)
                {
                    width = this.Width - chartHorizontalEasement;
                    height = (this.Height - 40 - chartVerticalGap) / 3;
                }
                chartSize = new Size(width, height);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.ComputeChartSize()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chartSize;
        }

        private void PearsonCorrelation_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                Size chartSize = ComputeChartSize();
                this.chartWidth = chartSize.Width;
                this.chartHeight = chartSize.Height;
                LayoutCharts(chartSize);
                DrawCharts();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PearsonCorrelation.PearsonCorrelation_SizeChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
