﻿namespace StatisticsDisplay
{
    partial class PearsonCorrelation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PearsonCorrelation));
            this.correlationChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.chart1RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.correlationChart2WinChartViewer = new ChartDirector.WinChartViewer();
            this.correlationChart3WinChartViewer = new ChartDirector.WinChartViewer();
            this.chart2RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.chart3RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart1WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart2WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart3WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // correlationChart1WinChartViewer
            // 
            this.correlationChart1WinChartViewer.Location = new System.Drawing.Point(0, 3);
            this.correlationChart1WinChartViewer.Name = "correlationChart1WinChartViewer";
            this.correlationChart1WinChartViewer.Size = new System.Drawing.Size(1016, 240);
            this.correlationChart1WinChartViewer.TabIndex = 81;
            this.correlationChart1WinChartViewer.TabStop = false;
            this.correlationChart1WinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.correlationChart1WinChartViewer_MouseClick);
            // 
            // correlationChart2WinChartViewer
            // 
            this.correlationChart2WinChartViewer.Location = new System.Drawing.Point(0, 249);
            this.correlationChart2WinChartViewer.Name = "correlationChart2WinChartViewer";
            this.correlationChart2WinChartViewer.Size = new System.Drawing.Size(1016, 240);
            this.correlationChart2WinChartViewer.TabIndex = 82;
            this.correlationChart2WinChartViewer.TabStop = false;
            this.correlationChart2WinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.correlationChart2WinChartViewer_MouseClick);
            // 
            // correlationChart3WinChartViewer
            // 
            this.correlationChart3WinChartViewer.Location = new System.Drawing.Point(0, 495);
            this.correlationChart3WinChartViewer.Name = "correlationChart3WinChartViewer";
            this.correlationChart3WinChartViewer.Size = new System.Drawing.Size(1016, 240);
            this.correlationChart3WinChartViewer.TabIndex = 83;
            this.correlationChart3WinChartViewer.TabStop = false;
            this.correlationChart3WinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.correlationChart3WinChartViewer_MouseClick);
            // 
            // PearsonCorrelation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 736);
            this.Controls.Add(this.correlationChart3WinChartViewer);
            this.Controls.Add(this.correlationChart2WinChartViewer);
            this.Controls.Add(this.correlationChart1WinChartViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PearsonCorrelation";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Pearson Correlation";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.PearsonCorrelation_Load);
            this.SizeChanged += new System.EventHandler(this.PearsonCorrelation_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart1WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart2WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.correlationChart3WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ChartDirector.WinChartViewer correlationChart1WinChartViewer;
        private Telerik.WinControls.UI.RadContextMenu chart1RadContextMenu;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private ChartDirector.WinChartViewer correlationChart2WinChartViewer;
        private ChartDirector.WinChartViewer correlationChart3WinChartViewer;
        private Telerik.WinControls.UI.RadContextMenu chart2RadContextMenu;
        private Telerik.WinControls.UI.RadContextMenu chart3RadContextMenu;
    }
}

