﻿namespace StatisticsDisplay
{
    partial class DataComparison
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataComparison));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.dataComparisonChart1WinChartViewer = new ChartDirector.WinChartViewer();
            this.chart1RadContextMenu = new Telerik.WinControls.UI.RadContextMenu(this.components);
            this.normalizeDataRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataComparisonChart1WinChartViewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataComparisonChart1WinChartViewer
            // 
            this.dataComparisonChart1WinChartViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataComparisonChart1WinChartViewer.Location = new System.Drawing.Point(0, 3);
            this.dataComparisonChart1WinChartViewer.Name = "dataComparisonChart1WinChartViewer";
            this.dataComparisonChart1WinChartViewer.Size = new System.Drawing.Size(1016, 240);
            this.dataComparisonChart1WinChartViewer.TabIndex = 82;
            this.dataComparisonChart1WinChartViewer.TabStop = false;
            this.dataComparisonChart1WinChartViewer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataComparisonWinChartViewer_MouseClick);
            // 
            // normalizeDataRadCheckBox
            // 
            this.normalizeDataRadCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.normalizeDataRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.normalizeDataRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.normalizeDataRadCheckBox.Location = new System.Drawing.Point(12, 249);
            this.normalizeDataRadCheckBox.Name = "normalizeDataRadCheckBox";
            this.normalizeDataRadCheckBox.Size = new System.Drawing.Size(98, 16);
            this.normalizeDataRadCheckBox.TabIndex = 83;
            this.normalizeDataRadCheckBox.Text = "Normalize Data";
            this.normalizeDataRadCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.normalizeDataRadCheckBox_ToggleStateChanged);
            // 
            // DataComparison
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(1016, 271);
            this.Controls.Add(this.normalizeDataRadCheckBox);
            this.Controls.Add(this.dataComparisonChart1WinChartViewer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DataComparison";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "DataComparison";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.DataComparison_Load);
            this.SizeChanged += new System.EventHandler(this.DataComparison_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataComparisonChart1WinChartViewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.normalizeDataRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private ChartDirector.WinChartViewer dataComparisonChart1WinChartViewer;
        private Telerik.WinControls.UI.RadContextMenu chart1RadContextMenu;
        private Telerik.WinControls.UI.RadCheckBox normalizeDataRadCheckBox;
    }
}