using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;

namespace MonitorUtilities
{
    public partial class ConfigurationDescription : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string configurationDescriptionTitleText = "Configuration Description";
        private static string configurationDescriptionRadLabelText = "Enter a name for  this configuration (50 character limit)";
        private static string saveRadButtonText = "Save";
        private static string cancelRadButtonText = "Cancel";
        private static string configurationDescriptionReservedForInternalUseWarningText = "The description you entered for the confiugration is the same as one reserved for internal use.";

        private string reservedConfigurationName;
        private string defaultConfigurationName;

        private bool saveConfiguration;
        public bool SaveConfiguration
        {
            get
            {
                return saveConfiguration;
            }
        }

        private string configurationName;
        public string ConfigurationName
        {
            get
            {
                return configurationName;
            }
        }

        public ConfigurationDescription(string inputReservedConfigurationName, string inputDefaultConfigurationName)
        {
            InitializeComponent();
            this.AcceptButton = saveRadButton;
            configurationName = string.Empty;
            saveConfiguration = false;
            this.reservedConfigurationName = inputReservedConfigurationName;
            this.defaultConfigurationName = inputDefaultConfigurationName;

            AssignStringValuesToInterfaceObjects();
            this.StartPosition = FormStartPosition.CenterParent;
        }


        private void ConfigurationDescription_Load(object sender, EventArgs e)
        {
            this.configurationNameRadTextBox.Text = defaultConfigurationName;
        }

        private void saveRadButton_Click(object sender, EventArgs e)
        {
            saveConfiguration = true;
            configurationName = configurationNameRadTextBox.Text;
            if (configurationName.ToLower().CompareTo(reservedConfigurationName.ToLower()) != 0)
            {
                if (configurationName.Length > 50)
                {
                    configurationName = configurationName.Remove(49);
                }
                this.Close();
            }
            else
            {
                RadMessageBox.Show(this, configurationDescriptionReservedForInternalUseWarningText);
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = configurationDescriptionTitleText;

                configurationDescriptionRadLabel.Text = configurationDescriptionRadLabelText;
                saveRadButton.Text = saveRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationDescription.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                configurationDescriptionTitleText = LanguageConversion.GetStringAssociatedWithTag("ConfigurationDescriptionInterfaceText", configurationDescriptionTitleText, htmlFontType, htmlStandardFontSize, "");
                configurationDescriptionRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ConfigurationDescriptionInterfaceConfigurationDescriptionRadLabelText", configurationDescriptionRadLabelText, htmlFontType, htmlStandardFontSize, "");
                saveRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ConfigurationDescriptionInterfaceSaveRadButtonText", saveRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ConfigurationDescriptionInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");
                configurationDescriptionReservedForInternalUseWarningText = LanguageConversion.GetStringAssociatedWithTag("ConfigurationDescriptionConfigurationDescriptionReservedForInternalUseWarningText", configurationDescriptionReservedForInternalUseWarningText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ConfigurationDescription.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
