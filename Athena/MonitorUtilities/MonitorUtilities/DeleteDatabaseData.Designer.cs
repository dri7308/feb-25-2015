﻿namespace MonitorUtilities
{
    partial class DeleteDatabaseData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteDatabaseData));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.deleteInProgressRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.operationTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitorNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.deleteInProgressRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // deleteInProgressRadWaitingBar
            // 
            this.deleteInProgressRadWaitingBar.Location = new System.Drawing.Point(12, 144);
            this.deleteInProgressRadWaitingBar.Name = "deleteInProgressRadWaitingBar";
            this.deleteInProgressRadWaitingBar.Size = new System.Drawing.Size(289, 30);
            this.deleteInProgressRadWaitingBar.TabIndex = 0;
            this.deleteInProgressRadWaitingBar.Text = "radWaitingBar1";
            this.deleteInProgressRadWaitingBar.ThemeName = "Office2007Black";
            this.deleteInProgressRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.deleteInProgressRadWaitingBar.WaitingSpeed = 10;
            this.deleteInProgressRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // operationTypeRadLabel
            // 
            this.operationTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.operationTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.operationTypeRadLabel.Location = new System.Drawing.Point(45, 12);
            this.operationTypeRadLabel.Name = "operationTypeRadLabel";
            this.operationTypeRadLabel.Size = new System.Drawing.Size(206, 18);
            this.operationTypeRadLabel.TabIndex = 1;
            this.operationTypeRadLabel.Text = "Delete operation being performed";
            // 
            // companyNameRadLabel
            // 
            this.companyNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyNameRadLabel.Location = new System.Drawing.Point(12, 46);
            this.companyNameRadLabel.Name = "companyNameRadLabel";
            this.companyNameRadLabel.Size = new System.Drawing.Size(86, 16);
            this.companyNameRadLabel.TabIndex = 2;
            this.companyNameRadLabel.Text = "Company name";
            // 
            // plantNameRadLabel
            // 
            this.plantNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantNameRadLabel.Location = new System.Drawing.Point(12, 69);
            this.plantNameRadLabel.Name = "plantNameRadLabel";
            this.plantNameRadLabel.Size = new System.Drawing.Size(63, 16);
            this.plantNameRadLabel.TabIndex = 3;
            this.plantNameRadLabel.Text = "Plant name";
            // 
            // equipmentNameRadLabel
            // 
            this.equipmentNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentNameRadLabel.Location = new System.Drawing.Point(13, 92);
            this.equipmentNameRadLabel.Name = "equipmentNameRadLabel";
            this.equipmentNameRadLabel.Size = new System.Drawing.Size(92, 16);
            this.equipmentNameRadLabel.TabIndex = 4;
            this.equipmentNameRadLabel.Text = "Equipment name";
            // 
            // monitorNameRadLabel
            // 
            this.monitorNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitorNameRadLabel.Location = new System.Drawing.Point(13, 115);
            this.monitorNameRadLabel.Name = "monitorNameRadLabel";
            this.monitorNameRadLabel.Size = new System.Drawing.Size(77, 16);
            this.monitorNameRadLabel.TabIndex = 5;
            this.monitorNameRadLabel.Text = "Monitor Name";
            // 
            // DeleteDatabaseData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(313, 186);
            this.ControlBox = false;
            this.Controls.Add(this.monitorNameRadLabel);
            this.Controls.Add(this.equipmentNameRadLabel);
            this.Controls.Add(this.plantNameRadLabel);
            this.Controls.Add(this.companyNameRadLabel);
            this.Controls.Add(this.operationTypeRadLabel);
            this.Controls.Add(this.deleteInProgressRadWaitingBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DeleteDatabaseData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Delete Database Data";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.DeleteDatabaseData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.deleteInProgressRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operationTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadWaitingBar deleteInProgressRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel operationTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel companyNameRadLabel;
        private Telerik.WinControls.UI.RadLabel plantNameRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentNameRadLabel;
        private Telerik.WinControls.UI.RadLabel monitorNameRadLabel;
    }
}