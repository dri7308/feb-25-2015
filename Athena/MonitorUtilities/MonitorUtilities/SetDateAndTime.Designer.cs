namespace MonitorUtilities
{
    partial class SetDateAndTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetDateAndTime));
            this.resetDateTimeToPCDatetimeRadButton = new Telerik.WinControls.UI.RadButton();
            this.setDeviceToSelectedDateTimeRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelResetOfDateTimeRadButton = new Telerik.WinControls.UI.RadButton();
            this.refreshDeviceTimeRadButton = new Telerik.WinControls.UI.RadButton();
            this.currentDeviceTimeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.currentDeviceDateAndTimeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.setTimeRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.setDatetimetoDeviceDateTimeRadButton = new Telerik.WinControls.UI.RadButton();
            this.hourRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.minuteRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.secondRadSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.timeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.resetDateTimeToPCDatetimeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceToSelectedDateTimeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelResetOfDateTimeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshDeviceTimeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentDeviceTimeRadGroupBox)).BeginInit();
            this.currentDeviceTimeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentDeviceDateAndTimeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setTimeRadGroupBox)).BeginInit();
            this.setTimeRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setDatetimetoDeviceDateTimeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRadSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // resetDateTimeToPCDatetimeRadButton
            // 
            this.resetDateTimeToPCDatetimeRadButton.Location = new System.Drawing.Point(13, 286);
            this.resetDateTimeToPCDatetimeRadButton.Name = "resetDateTimeToPCDatetimeRadButton";
            this.resetDateTimeToPCDatetimeRadButton.Size = new System.Drawing.Size(130, 34);
            this.resetDateTimeToPCDatetimeRadButton.TabIndex = 9;
            this.resetDateTimeToPCDatetimeRadButton.Text = "<html>Set to PC<br>date and time</html>";
            this.resetDateTimeToPCDatetimeRadButton.ThemeName = "Office2007Black";
            this.resetDateTimeToPCDatetimeRadButton.Click += new System.EventHandler(this.resetDateToCurrentRadButton_Click);
            // 
            // setDeviceToSelectedDateTimeRadButton
            // 
            this.setDeviceToSelectedDateTimeRadButton.Location = new System.Drawing.Point(13, 335);
            this.setDeviceToSelectedDateTimeRadButton.Name = "setDeviceToSelectedDateTimeRadButton";
            this.setDeviceToSelectedDateTimeRadButton.Size = new System.Drawing.Size(270, 37);
            this.setDeviceToSelectedDateTimeRadButton.TabIndex = 28;
            this.setDeviceToSelectedDateTimeRadButton.Text = "Set device time to indicated date and time";
            this.setDeviceToSelectedDateTimeRadButton.ThemeName = "Office2007Black";
            this.setDeviceToSelectedDateTimeRadButton.Click += new System.EventHandler(this.setDeviceToSelectedDateRadButton_Click);
            // 
            // cancelResetOfDateTimeRadButton
            // 
            this.cancelResetOfDateTimeRadButton.Location = new System.Drawing.Point(25, 484);
            this.cancelResetOfDateTimeRadButton.Name = "cancelResetOfDateTimeRadButton";
            this.cancelResetOfDateTimeRadButton.Size = new System.Drawing.Size(270, 37);
            this.cancelResetOfDateTimeRadButton.TabIndex = 29;
            this.cancelResetOfDateTimeRadButton.Text = "Cancel reset of device date and time";
            this.cancelResetOfDateTimeRadButton.ThemeName = "Office2007Black";
            this.cancelResetOfDateTimeRadButton.Click += new System.EventHandler(this.cancelResetOfDateRadButton_Click);
            // 
            // refreshDeviceTimeRadButton
            // 
            this.refreshDeviceTimeRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.refreshDeviceTimeRadButton.Location = new System.Drawing.Point(145, 29);
            this.refreshDeviceTimeRadButton.Name = "refreshDeviceTimeRadButton";
            this.refreshDeviceTimeRadButton.Size = new System.Drawing.Size(138, 28);
            this.refreshDeviceTimeRadButton.TabIndex = 32;
            this.refreshDeviceTimeRadButton.Text = "Refresh";
            this.refreshDeviceTimeRadButton.ThemeName = "Office2007Black";
            this.refreshDeviceTimeRadButton.Click += new System.EventHandler(this.refreshDeviceTimeRadButton_Click);
            // 
            // currentDeviceTimeRadGroupBox
            // 
            this.currentDeviceTimeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.currentDeviceTimeRadGroupBox.Controls.Add(this.currentDeviceDateAndTimeRadLabel);
            this.currentDeviceTimeRadGroupBox.Controls.Add(this.refreshDeviceTimeRadButton);
            this.currentDeviceTimeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentDeviceTimeRadGroupBox.FooterImageIndex = -1;
            this.currentDeviceTimeRadGroupBox.FooterImageKey = "";
            this.currentDeviceTimeRadGroupBox.HeaderImageIndex = -1;
            this.currentDeviceTimeRadGroupBox.HeaderImageKey = "";
            this.currentDeviceTimeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.currentDeviceTimeRadGroupBox.HeaderText = "Current Device Time";
            this.currentDeviceTimeRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.currentDeviceTimeRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.currentDeviceTimeRadGroupBox.Name = "currentDeviceTimeRadGroupBox";
            this.currentDeviceTimeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.currentDeviceTimeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.currentDeviceTimeRadGroupBox.Size = new System.Drawing.Size(296, 64);
            this.currentDeviceTimeRadGroupBox.TabIndex = 33;
            this.currentDeviceTimeRadGroupBox.Text = "Current Device Time";
            this.currentDeviceTimeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // currentDeviceDateAndTimeRadLabel
            // 
            this.currentDeviceDateAndTimeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.currentDeviceDateAndTimeRadLabel.Location = new System.Drawing.Point(10, 35);
            this.currentDeviceDateAndTimeRadLabel.Name = "currentDeviceDateAndTimeRadLabel";
            this.currentDeviceDateAndTimeRadLabel.Size = new System.Drawing.Size(57, 16);
            this.currentDeviceDateAndTimeRadLabel.TabIndex = 33;
            this.currentDeviceDateAndTimeRadLabel.Text = "radLabel1";
            // 
            // setTimeRadGroupBox
            // 
            this.setTimeRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.setTimeRadGroupBox.Controls.Add(this.setDatetimetoDeviceDateTimeRadButton);
            this.setTimeRadGroupBox.Controls.Add(this.hourRadSpinEditor);
            this.setTimeRadGroupBox.Controls.Add(this.minuteRadSpinEditor);
            this.setTimeRadGroupBox.Controls.Add(this.secondRadSpinEditor);
            this.setTimeRadGroupBox.Controls.Add(this.timeRadLabel);
            this.setTimeRadGroupBox.Controls.Add(this.radCalendar1);
            this.setTimeRadGroupBox.Controls.Add(this.resetDateTimeToPCDatetimeRadButton);
            this.setTimeRadGroupBox.Controls.Add(this.setDeviceToSelectedDateTimeRadButton);
            this.setTimeRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setTimeRadGroupBox.FooterImageIndex = -1;
            this.setTimeRadGroupBox.FooterImageKey = "";
            this.setTimeRadGroupBox.HeaderImageIndex = -1;
            this.setTimeRadGroupBox.HeaderImageKey = "";
            this.setTimeRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.setTimeRadGroupBox.HeaderText = "Time to set";
            this.setTimeRadGroupBox.Location = new System.Drawing.Point(12, 82);
            this.setTimeRadGroupBox.Name = "setTimeRadGroupBox";
            this.setTimeRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.setTimeRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.setTimeRadGroupBox.Size = new System.Drawing.Size(296, 385);
            this.setTimeRadGroupBox.TabIndex = 34;
            this.setTimeRadGroupBox.Text = "Time to set";
            this.setTimeRadGroupBox.ThemeName = "Office2007Black";
            // 
            // setDatetimetoDeviceDateTimeRadButton
            // 
            this.setDatetimetoDeviceDateTimeRadButton.Location = new System.Drawing.Point(153, 286);
            this.setDatetimetoDeviceDateTimeRadButton.Name = "setDatetimetoDeviceDateTimeRadButton";
            this.setDatetimetoDeviceDateTimeRadButton.Size = new System.Drawing.Size(130, 34);
            this.setDatetimetoDeviceDateTimeRadButton.TabIndex = 36;
            this.setDatetimetoDeviceDateTimeRadButton.Text = "<html>Set to device<br>date and time</html>";
            this.setDatetimetoDeviceDateTimeRadButton.ThemeName = "Office2007Black";
            this.setDatetimetoDeviceDateTimeRadButton.Click += new System.EventHandler(this.setDatetimetoDeviceDateTimeRadButton_Click);
            // 
            // hourRadSpinEditor
            // 
            this.hourRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourRadSpinEditor.Location = new System.Drawing.Point(105, 250);
            this.hourRadSpinEditor.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.hourRadSpinEditor.Name = "hourRadSpinEditor";
            // 
            // 
            // 
            this.hourRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.hourRadSpinEditor.ShowBorder = true;
            this.hourRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.hourRadSpinEditor.TabIndex = 35;
            this.hourRadSpinEditor.TabStop = false;
            this.hourRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // minuteRadSpinEditor
            // 
            this.minuteRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minuteRadSpinEditor.Location = new System.Drawing.Point(143, 250);
            this.minuteRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.minuteRadSpinEditor.Name = "minuteRadSpinEditor";
            // 
            // 
            // 
            this.minuteRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.minuteRadSpinEditor.ShowBorder = true;
            this.minuteRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.minuteRadSpinEditor.TabIndex = 32;
            this.minuteRadSpinEditor.TabStop = false;
            this.minuteRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // secondRadSpinEditor
            // 
            this.secondRadSpinEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondRadSpinEditor.Location = new System.Drawing.Point(182, 250);
            this.secondRadSpinEditor.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.secondRadSpinEditor.Name = "secondRadSpinEditor";
            // 
            // 
            // 
            this.secondRadSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.secondRadSpinEditor.ShowBorder = true;
            this.secondRadSpinEditor.Size = new System.Drawing.Size(33, 19);
            this.secondRadSpinEditor.TabIndex = 31;
            this.secondRadSpinEditor.TabStop = false;
            this.secondRadSpinEditor.ThemeName = "Office2007Black";
            // 
            // timeRadLabel
            // 
            this.timeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.timeRadLabel.Location = new System.Drawing.Point(68, 251);
            this.timeRadLabel.Name = "timeRadLabel";
            this.timeRadLabel.Size = new System.Drawing.Size(31, 16);
            this.timeRadLabel.TabIndex = 30;
            this.timeRadLabel.Text = "Time";
            // 
            // radCalendar1
            // 
            this.radCalendar1.CellAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radCalendar1.CellMargin = new System.Windows.Forms.Padding(0);
            this.radCalendar1.CellPadding = new System.Windows.Forms.Padding(0);
            this.radCalendar1.FastNavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationNextImage")));
            this.radCalendar1.FastNavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.FastNavigationPrevImage")));
            this.radCalendar1.HeaderHeight = 17;
            this.radCalendar1.HeaderWidth = 17;
            this.radCalendar1.Location = new System.Drawing.Point(13, 33);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.NavigationNextImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationNextImage")));
            this.radCalendar1.NavigationPrevImage = ((System.Drawing.Image)(resources.GetObject("radCalendar1.NavigationPrevImage")));
            this.radCalendar1.RangeMaxDate = new System.DateTime(2099, 12, 30, 0, 0, 0, 0);
            this.radCalendar1.SelectedDates.AddRange(new System.DateTime[] {
            new System.DateTime(1900, 1, 1, 0, 0, 0, 0)});
            this.radCalendar1.Size = new System.Drawing.Size(270, 201);
            this.radCalendar1.TabIndex = 29;
            this.radCalendar1.Text = "radCalendar1";
            this.radCalendar1.ThemeName = "Office2007Black";
            // 
            // SetDateAndTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(325, 539);
            this.Controls.Add(this.setTimeRadGroupBox);
            this.Controls.Add(this.currentDeviceTimeRadGroupBox);
            this.Controls.Add(this.cancelResetOfDateTimeRadButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(333, 302);
            this.Name = "SetDateAndTime";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Set Date and Time";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SetDateAndTime_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resetDateTimeToPCDatetimeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setDeviceToSelectedDateTimeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelResetOfDateTimeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshDeviceTimeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currentDeviceTimeRadGroupBox)).EndInit();
            this.currentDeviceTimeRadGroupBox.ResumeLayout(false);
            this.currentDeviceTimeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentDeviceDateAndTimeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setTimeRadGroupBox)).EndInit();
            this.setTimeRadGroupBox.ResumeLayout(false);
            this.setTimeRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setDatetimetoDeviceDateTimeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hourRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minuteRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondRadSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton resetDateTimeToPCDatetimeRadButton;
        private Telerik.WinControls.UI.RadButton setDeviceToSelectedDateTimeRadButton;
        private Telerik.WinControls.UI.RadButton cancelResetOfDateTimeRadButton;
        private Telerik.WinControls.UI.RadButton refreshDeviceTimeRadButton;
        private Telerik.WinControls.UI.RadGroupBox currentDeviceTimeRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox setTimeRadGroupBox;
        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.WinControls.UI.RadLabel timeRadLabel;
        private Telerik.WinControls.UI.RadSpinEditor hourRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor minuteRadSpinEditor;
        private Telerik.WinControls.UI.RadSpinEditor secondRadSpinEditor;
        private Telerik.WinControls.UI.RadButton setDatetimetoDeviceDateTimeRadButton;
        private Telerik.WinControls.UI.RadLabel currentDeviceDateAndTimeRadLabel;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
    }
}

