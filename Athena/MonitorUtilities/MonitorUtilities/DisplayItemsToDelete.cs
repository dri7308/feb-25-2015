using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;

namespace MonitorUtilities
{
    public partial class DisplayItemsToDelete : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string displayItemsToDeleteTitleText = "Items to Delete";
        private static string deleteMessageRadLabelText = "Delete all data associated<br>with the display reading<br>dates and times?";
        private static string confirmDeleteRadButtonText = "Confirm Delete";
        private static string cancelDeleteRadButtonText = "Cancel Delete";
        private static string dataItemsToDeleteText = "Data items to delete";
    

        private bool deleteSelectedData;
        public bool DeleteSelectedData
        {
            get
            {
                return deleteSelectedData;
            }
        }

        public DisplayItemsToDelete(List<DateTime> readingDates)
        {
            try
            {
                InitializeComponent();
                this.radListControl1.Items.Clear();
                if (readingDates != null)
                {
                    this.radListControl1.Items.Add(dataItemsToDeleteText);
                    this.radListControl1.Items.Add(" ");
                    foreach (DateTime entry in readingDates)
                    {
                        this.radListControl1.Items.Add(entry.ToString());
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.DisplayItemsToDelete(List<DateTime>)\nInput List<DateTime> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                this.CancelButton = cancelDeleteRadButton;

                AssignStringValuesToInterfaceObjects();
                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.DisplayItemsToDelete(List<DateTime>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void confirmDeleteRadButton_Click(object sender, EventArgs e)
        {
            deleteSelectedData = true;
            this.Close();
        }

        private void cancelDeleteRadButton_Click(object sender, EventArgs e)
        {
            deleteSelectedData = false;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {

                this.Text = displayItemsToDeleteTitleText;

                deleteMessageRadLabel.Text = deleteMessageRadLabelText;
                confirmDeleteRadButton.Text = confirmDeleteRadButtonText;
                cancelDeleteRadButton.Text = cancelDeleteRadButtonText;

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DisplayItemsToDelete.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                displayItemsToDeleteTitleText = LanguageConversion.GetStringAssociatedWithTag("DisplayItemsToDeleteInterfaceText", displayItemsToDeleteTitleText, htmlFontType, htmlStandardFontSize, "");
                deleteMessageRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DisplayItemsToDeleteInterfaceDeleteMessageRadLabelText", deleteMessageRadLabelText, htmlFontType, htmlStandardFontSize, "");
                confirmDeleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DisplayItemsToDeleteInterfaceConfirmDeleteRadButtonText", confirmDeleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelDeleteRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DisplayItemsToDeleteInterfaceCancelDeleteRadButtonText", cancelDeleteRadButtonText, htmlFontType, htmlStandardFontSize, "");
                dataItemsToDeleteText = LanguageConversion.GetStringAssociatedWithTag("DisplayItemsToDeleteDataItemsToDeleteText", dataItemsToDeleteText, htmlFontType, htmlStandardFontSize, "");

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DisplayItemsToDelete.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
