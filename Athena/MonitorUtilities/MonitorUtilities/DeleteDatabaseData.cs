﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using DatabaseInterface;
using GeneralUtilities;

namespace MonitorUtilities
{
    public partial class DeleteDatabaseData : Telerik.WinControls.UI.RadForm
    {
        private static string deleteDatabaseDataTitleText = "Delete Database Data";

        private static string deleteAllDatabaseDataText = "Delete all database data";
        private static string deleteDuplicateDatabaseDataText = "Delete duplicate database data";
        private static string companyNameRadLabelText = "Company name";
        private static string plantNameRadLabelText = "Plant name";
        private static string equipmentNameRadLabelText = "Equipment name";
        private static string monitorNameRadLabelText = "Monitor name";

        private static string areYouSureYouWantToDeleteAllDataQuestionText = "Are you sure you want to delete all data for the selected monitor?";
        private static string deleteAllDataQuestionText = "Delete all data?";
        private static string areYouSureYouWantToDeleteDuplicateDataQuestionText = "Are you sure you want to delete the duplicate data for the selected monitor?";
        private static string deleteDuplicateDataQuestionText = "Delete duplicate data?";
        private static string couldNotConnectToDatabaseText = "Could not connect to the database - cancelling";

        private static string someFatalErrorOccurredDuringDeleteText = "Some fatal error occurred during the delete.";

        private static BackgroundWorker dataDeleteBackgroundWorker;

        Guid monitorID;
        bool deleteAllData;
        string dbConnectionString;

        ParentWindowInformation parentWindowInformation;

        public DeleteDatabaseData(Guid inputMonitorID, bool inputDeleteAllData, string inputDbConnnectionString, ParentWindowInformation inputParentWindowInformation)
        {
            InitializeComponent();
            InitializDataDeleteBackgroundWorker();

            monitorID = inputMonitorID;
            deleteAllData = inputDeleteAllData;
            dbConnectionString = inputDbConnnectionString;
            parentWindowInformation = inputParentWindowInformation;

            //this.StartPosition = FormStartPosition.CenterParent;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = this.parentWindowInformation.AssignLocation();
        }

        private void DeleteDatabaseData_Load(object sender, EventArgs e)
        {
            bool proceed = false;

            if (deleteAllData)
            {
                operationTypeRadLabel.Text = deleteAllDatabaseDataText;
            }
            else
            {
                operationTypeRadLabel.Text = deleteDuplicateDatabaseDataText;
            }

            if (deleteAllData)
            {
                if (RadMessageBox.Show(this, areYouSureYouWantToDeleteAllDataQuestionText, deleteAllDataQuestionText, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    proceed = true;
                }
            }
            else
            {
                if (RadMessageBox.Show(this, areYouSureYouWantToDeleteDuplicateDataQuestionText, deleteDuplicateDataQuestionText, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    proceed = true;
                }
            }

            if (proceed)
            {
                using (MonitorInterfaceDB loadDB = new MonitorInterfaceDB(dbConnectionString))
                {
                    MonitorHierarchy hierarchy = General_DatabaseMethods.GetFullMonitorHierarchy(monitorID, loadDB);
                    if (hierarchy.errorCode == ErrorCode.None)
                    {
                        companyNameRadLabel.Text = companyNameRadLabelText + ": " + hierarchy.company.Name.Trim();
                        plantNameRadLabel.Text = plantNameRadLabelText + ": " + hierarchy.plant.Name.Trim();
                        equipmentNameRadLabel.Text = equipmentNameRadLabelText + ": " + hierarchy.equipment.Name.Trim();
                        monitorNameRadLabel.Text = monitorNameRadLabelText + ": " + hierarchy.monitor.MonitorType.Trim();

                        dataDeleteBackgroundWorker.RunWorkerAsync();
                    }
                    else
                    {
                        RadMessageBox.Show(this, couldNotConnectToDatabaseText);
                        this.Close();
                    }
                }
            }
            else
            {
                this.Close();
            }
        }

        private void InitializDataDeleteBackgroundWorker()
        {
            /// Set up the download background worker
            dataDeleteBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            dataDeleteBackgroundWorker.DoWork += dataDeleteBackgroundWorker_DoWork;
            //dataDeleteBackgroundWorker.ProgressChanged += dataDeleteBackgroundWorker_ProgressChanged;
            dataDeleteBackgroundWorker.RunWorkerCompleted += dataDeleteBackgroundWorker_RunWorkerCompleted;
        }

        private void dataDeleteBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Monitor monitor = null;

                DataDeleteReturnObject deleteReturnObject = new DataDeleteReturnObject();

                ErrorCode errorCode = ErrorCode.None;

                this.Invoke(new MethodInvoker(() => deleteInProgressRadWaitingBar.StartWaiting()));
                using (MonitorInterfaceDB deleteDb = new MonitorInterfaceDB(dbConnectionString))
                {
                    monitor = General_DatabaseMethods.GetOneMonitor(monitorID, deleteDb);
                    if (monitor != null)
                    {
                        if (monitor.MonitorType.Trim().CompareTo("Main") == 0)
                        {
                            if (deleteAllData)
                            {
                                errorCode = Main_DeleteAllDatabaseData(monitorID, deleteDb);
                            }
                            else
                            {
                                errorCode = Main_DeleteDuplicateData(monitorID, deleteDb);
                            }
                        }
                        else if (monitor.MonitorType.Trim().CompareTo("BHM") == 0)
                        {
                            if (deleteAllData)
                            {
                                errorCode = BHM_DeleteAllDatabaseData(monitorID, deleteDb);
                            }
                            else
                            {
                                errorCode = BHM_DeleteDuplicateData(monitorID, deleteDb);
                            }
                        }
                        else if (monitor.MonitorType.Trim().CompareTo("PDM") == 0)
                        {
                            if (deleteAllData)
                            {
                                errorCode = PDM_DeleteAllDatabaseData(monitorID, deleteDb);
                            }
                            else
                            {
                                errorCode = PDM_DeleteDuplicateData(monitorID, deleteDb);
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.MonitorTypeIncorrect;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.MonitorNotFound;
                    }
                }
                deleteReturnObject.errorCode = errorCode;
                e.Result = deleteReturnObject;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeleteDatabaseData.dataDeleteBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
              
            }
        }

//        private void dataDeleteBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
//        {
//            try
//            {

//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeleteDatabaseData.dataDeleteBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void dataDeleteBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                DataDeleteReturnObject returnObject;
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in DeleteDatabaseData.dataDeleteBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

                if (e.Result != null)
                {
                    returnObject = e.Result as DataDeleteReturnObject;
                    RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(returnObject.errorCode));
                }
                else
                {
                    RadMessageBox.Show(this, someFatalErrorOccurredDuringDeleteText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeleteDatabaseData.dataDeleteBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.Close();
            }
        }

        private ErrorCode Main_DeleteDuplicateData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.None;
            try
            {
                List<Main_Data_DataRoot> dataRootList = MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    List<Main_Data_DataRoot> duplicateEntries = Main_FindDuplicateDataRootEntries(dataRootList);
                    if ((duplicateEntries != null) && (duplicateEntries.Count > 0))
                    {
                        if (MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntries(duplicateEntries, deleteDb))
                        {
                            error = ErrorCode.DuplicateDataDeleted;
                        }
                        else
                        {
                            error = ErrorCode.DataDeleteFailed;
                        }
                    }
                    else
                    {
                        error = ErrorCode.NoDuplicateDataFound;
                    }
                }
                else
                {
                    error = ErrorCode.NoDataFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.DeleteDuplicateData(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        private ErrorCode BHM_DeleteDuplicateData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.DuplicateDataDeleted;
            try
            {
                List<BHM_Data_DataRoot> dataRootList = BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    List<BHM_Data_DataRoot> duplicateEntries = BHM_FindDuplicateDataRootEntries(dataRootList);
                    if ((duplicateEntries != null) && (duplicateEntries.Count > 0))
                    {
                        if (BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntries(duplicateEntries, deleteDb))
                        {
                            error = ErrorCode.DuplicateDataDeleted;
                        }
                        else
                        {
                            error = ErrorCode.DataDeleteFailed;
                        }
                    }
                    else
                    {
                        error = ErrorCode.NoDuplicateDataFound;
                    }
                }
                else
                {
                    error = ErrorCode.NoDataFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_DeleteDuplicateData(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        private ErrorCode PDM_DeleteDuplicateData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.DuplicateDataDeleted;
            try
            {
                List<PDM_Data_DataRoot> dataRootList = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    List<PDM_Data_DataRoot> duplicateEntries = PDM_FindDuplicateDataRootEntries(dataRootList);
                    if ((duplicateEntries != null) && (duplicateEntries.Count > 0))
                    {
                        if(PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntries(duplicateEntries, deleteDb))
                         {
                            error = ErrorCode.DuplicateDataDeleted;
                        }
                        else
                        {
                            error = ErrorCode.DataDeleteFailed;
                        }
                    }
                    else
                    {
                        error = ErrorCode.NoDuplicateDataFound;
                    }
                }
                else
                {
                    error = ErrorCode.NoDataFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_DeleteDuplicateData(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        public static List<BHM_Data_DataRoot> BHM_FindDuplicateDataRootEntries(List<BHM_Data_DataRoot> dataRootList)
        {
            List<BHM_Data_DataRoot> duplicateList = new List<BHM_Data_DataRoot>();
            try
            {
                Dictionary<DateTime, int> datesFound = new Dictionary<DateTime, int>();
                foreach (BHM_Data_DataRoot entry in dataRootList)
                {
                    if (!datesFound.ContainsKey(entry.ReadingDateTime))
                    {
                        datesFound.Add(entry.ReadingDateTime, 1);
                    }
                    else
                    {
                        duplicateList.Add(entry);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_FindDuplicateDataRootEntries(List<BHM_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return duplicateList;
        }

        public static List<Main_Data_DataRoot> Main_FindDuplicateDataRootEntries(List<Main_Data_DataRoot> dataRootList)
        {
            List<Main_Data_DataRoot> duplicateList = new List<Main_Data_DataRoot>();
            try
            {
                Dictionary<DateTime, int> datesFound = new Dictionary<DateTime, int>();
                foreach (Main_Data_DataRoot entry in dataRootList)
                {
                    if (!datesFound.ContainsKey(entry.ReadingDateTime))
                    {
                        datesFound.Add(entry.ReadingDateTime, 1);
                    }
                    else
                    {
                        duplicateList.Add(entry);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Main_FindDuplicateDataRootEntries(List<Main_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return duplicateList;
        }

        public static List<PDM_Data_DataRoot> PDM_FindDuplicateDataRootEntries(List<PDM_Data_DataRoot> dataRootList)
        {
            List<PDM_Data_DataRoot> duplicateList = new List<PDM_Data_DataRoot>();
            try
            {
                Dictionary<DateTime, int> datesFound = new Dictionary<DateTime, int>();
                foreach (PDM_Data_DataRoot entry in dataRootList)
                {
                    if (!datesFound.ContainsKey(entry.ReadingDateTime))
                    {
                        datesFound.Add(entry.ReadingDateTime, 1);
                    }
                    else
                    {
                        duplicateList.Add(entry);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_FindDuplicateDataRootEntries(List<PDM_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return duplicateList;
        }

        private ErrorCode Main_DeleteAllDatabaseData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.None;
            try
            {
                List<Main_Data_DataRoot> dataRootList = MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    if (MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntries(dataRootList, deleteDb))
                    {
                        error = ErrorCode.AllDataDeleted;
                    }
                    else
                    {
                        error = ErrorCode.DataDeleteFailed;
                    }
                    Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, deleteDb);
                    monitor.DateOfLastDataDownload = ConversionMethods.MinimumDateTime();
                    deleteDb.SubmitChanges();
                }
                else
                {
                    error = ErrorCode.NoDataFound;
                }
                BHM_DatabaseMethods.BHM_AlarmStatus_DeleteAllForOneMonitor(monitorID, deleteDb);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.Main_DeleteAllDatabaseData(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        private ErrorCode BHM_DeleteAllDatabaseData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.None;
            try
            {
                    List<BHM_Data_DataRoot> dataRootList = BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                    if ((dataRootList != null) && (dataRootList.Count > 0))
                    {
                        if (BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntries(dataRootList, deleteDb))
                        {
                            error = ErrorCode.AllDataDeleted;
                        }
                        else
                        {
                            error = ErrorCode.DataDeleteFailed;
                        }
                        Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, deleteDb);
                        monitor.DateOfLastDataDownload = ConversionMethods.MinimumDateTime();
                        deleteDb.SubmitChanges();
                    }
                    else
                    {
                        error = ErrorCode.NoDataFound;
                    }
                    BHM_DatabaseMethods.BHM_AlarmStatus_DeleteAllForOneMonitor(monitorID, deleteDb);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.BHM_DeleteAllDatabaseData(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        private ErrorCode PDM_DeleteAllDatabaseData(Guid monitorID, MonitorInterfaceDB deleteDb)
        {
            ErrorCode error = ErrorCode.None;
            try
            {
                    List<PDM_Data_DataRoot> dataRootList = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, deleteDb);
                    if ((dataRootList != null) && (dataRootList.Count > 0))
                    {
                        if (PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntries(dataRootList, deleteDb))
                        {
                            error = ErrorCode.AllDataDeleted;
                        }
                        else
                        {
                            error = ErrorCode.DataDeleteFailed;
                        }
                        Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, deleteDb);
                        monitor.DateOfLastDataDownload = ConversionMethods.MinimumDateTime();
                        deleteDb.SubmitChanges();
                    }
                    else
                    {
                        error = ErrorCode.NoDataFound;
                    }
                    PDM_DatabaseMethods.PDM_AlarmStatus_DeleteAllEntriesForOneMonitor(monitorID, deleteDb);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.PDM_DeleteAllDatabaseData(Guid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = deleteDatabaseDataTitleText;   
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeleteDatabaseData.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                deleteDatabaseDataTitleText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceDeleteDatabaseDataTitleText", deleteDatabaseDataTitleText, "", "", "");

                deleteAllDatabaseDataText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceDeleteAllDatabaseDataText", deleteAllDatabaseDataText, "", "", "");
                deleteDuplicateDatabaseDataText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceDeleteDuplicateDatabaseDataText", deleteDuplicateDatabaseDataText, "", "", "");
                companyNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceCompanyNameRadLabelText", companyNameRadLabelText, "", "", "");
                plantNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfacePlantNameRadLabelText", plantNameRadLabelText, "", "", "");
                equipmentNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceEquipmentNameRadLabelText", equipmentNameRadLabelText, "", "", "");
                monitorNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataInterfaceMonitorNameRadLabelText", monitorNameRadLabelText, "", "", "");

                areYouSureYouWantToDeleteAllDataQuestionText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataAreYouSureYouWantToDeleteAllDataQuestionText", areYouSureYouWantToDeleteAllDataQuestionText, "", "", "");
                deleteAllDataQuestionText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataDeleteAllDataQuestionText", deleteAllDataQuestionText, "", "", "");
                areYouSureYouWantToDeleteDuplicateDataQuestionText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataAreYouSureYouWantToDeleteDuplicateDataQuestionText", areYouSureYouWantToDeleteDuplicateDataQuestionText, "", "", "");
                deleteDuplicateDataQuestionText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataDeleteDuplicateDataQuestionText", deleteDuplicateDataQuestionText, "", "", "");
                couldNotConnectToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataCouldNotConnectToDatabaseText", couldNotConnectToDatabaseText, "", "", "");
                someFatalErrorOccurredDuringDeleteText = LanguageConversion.GetStringAssociatedWithTag("DeleteDatabaseDataSomeFatalErrorOccurredDuringDeleteText", someFatalErrorOccurredDuringDeleteText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeleteDatabaseData.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }












    }

    public class DataDeleteReturnObject
    {
        public ErrorCode errorCode;
    }
}
