namespace MonitorUtilities
{
    partial class ConfigurationDescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationDescription));
            this.configurationNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveRadButton = new Telerik.WinControls.UI.RadButton();
            this.configurationDescriptionRadLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.configurationNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.configurationDescriptionRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // configurationNameRadTextBox
            // 
            this.configurationNameRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationNameRadTextBox.Location = new System.Drawing.Point(13, 35);
            this.configurationNameRadTextBox.Name = "configurationNameRadTextBox";
            this.configurationNameRadTextBox.Size = new System.Drawing.Size(262, 18);
            this.configurationNameRadTextBox.TabIndex = 23;
            this.configurationNameRadTextBox.TabStop = false;
            this.configurationNameRadTextBox.ThemeName = "Office2007Black";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Location = new System.Drawing.Point(150, 70);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(125, 24);
            this.cancelRadButton.TabIndex = 22;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // saveRadButton
            // 
            this.saveRadButton.Location = new System.Drawing.Point(13, 70);
            this.saveRadButton.Name = "saveRadButton";
            this.saveRadButton.Size = new System.Drawing.Size(125, 24);
            this.saveRadButton.TabIndex = 21;
            this.saveRadButton.Text = "Save";
            this.saveRadButton.ThemeName = "Office2007Black";
            this.saveRadButton.Click += new System.EventHandler(this.saveRadButton_Click);
            // 
            // configurationDescriptionRadLabel
            // 
            this.configurationDescriptionRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configurationDescriptionRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.configurationDescriptionRadLabel.Location = new System.Drawing.Point(12, 12);
            this.configurationDescriptionRadLabel.Name = "configurationDescriptionRadLabel";
            this.configurationDescriptionRadLabel.Size = new System.Drawing.Size(276, 16);
            this.configurationDescriptionRadLabel.TabIndex = 20;
            this.configurationDescriptionRadLabel.Text = "Enter a name for this configuration (50 character limit)";
            this.configurationDescriptionRadLabel.ThemeName = "Office2007Black";
            // 
            // ConfigurationDescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(290, 112);
            this.Controls.Add(this.configurationNameRadTextBox);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.saveRadButton);
            this.Controls.Add(this.configurationDescriptionRadLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigurationDescription";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Configuration Description";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ConfigurationDescription_Load);
            ((System.ComponentModel.ISupportInitialize)(this.configurationNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.configurationDescriptionRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox configurationNameRadTextBox;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton saveRadButton;
        private Telerik.WinControls.UI.RadLabel configurationDescriptionRadLabel;
    }
}

