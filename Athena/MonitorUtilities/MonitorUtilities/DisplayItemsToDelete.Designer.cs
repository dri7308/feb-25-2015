namespace MonitorUtilities
{
    partial class DisplayItemsToDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DisplayItemsToDelete));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.radListControl1 = new Telerik.WinControls.UI.RadListControl();
            this.deleteMessageRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.confirmDeleteRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelDeleteRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radListControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteMessageRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmDeleteRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelDeleteRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radListControl1
            // 
            this.radListControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.radListControl1.CaseSensitiveSort = true;
            this.radListControl1.Location = new System.Drawing.Point(171, 12);
            this.radListControl1.Name = "radListControl1";
            this.radListControl1.Size = new System.Drawing.Size(209, 156);
            this.radListControl1.TabIndex = 0;
            this.radListControl1.Text = "radListControl1";
            // 
            // deleteMessageRadLabel
            // 
            this.deleteMessageRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.deleteMessageRadLabel.Location = new System.Drawing.Point(12, 12);
            this.deleteMessageRadLabel.Name = "deleteMessageRadLabel";
            // 
            // 
            // 
            this.deleteMessageRadLabel.RootElement.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.deleteMessageRadLabel.Size = new System.Drawing.Size(134, 40);
            this.deleteMessageRadLabel.TabIndex = 37;
            this.deleteMessageRadLabel.Text = "<html>Delete all data associated<br>with the displayed reading<br>dates and times" +
                "?</html>";
            // 
            // confirmDeleteRadButton
            // 
            this.confirmDeleteRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.confirmDeleteRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmDeleteRadButton.Location = new System.Drawing.Point(12, 72);
            this.confirmDeleteRadButton.Name = "confirmDeleteRadButton";
            this.confirmDeleteRadButton.Size = new System.Drawing.Size(140, 40);
            this.confirmDeleteRadButton.TabIndex = 38;
            this.confirmDeleteRadButton.Text = "<html>Confirm Delete</html>";
            this.confirmDeleteRadButton.ThemeName = "Office2007Black";
            this.confirmDeleteRadButton.Click += new System.EventHandler(this.confirmDeleteRadButton_Click);
            // 
            // cancelDeleteRadButton
            // 
            this.cancelDeleteRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelDeleteRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelDeleteRadButton.Location = new System.Drawing.Point(12, 128);
            this.cancelDeleteRadButton.Name = "cancelDeleteRadButton";
            this.cancelDeleteRadButton.Size = new System.Drawing.Size(140, 40);
            this.cancelDeleteRadButton.TabIndex = 39;
            this.cancelDeleteRadButton.Text = "<html>Cancel Delete</html>";
            this.cancelDeleteRadButton.ThemeName = "Office2007Black";
            this.cancelDeleteRadButton.Click += new System.EventHandler(this.cancelDeleteRadButton_Click);
            // 
            // DisplayItemsToDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(392, 180);
            this.Controls.Add(this.cancelDeleteRadButton);
            this.Controls.Add(this.confirmDeleteRadButton);
            this.Controls.Add(this.deleteMessageRadLabel);
            this.Controls.Add(this.radListControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(400, 212);
            this.Name = "DisplayItemsToDelete";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.RootElement.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Text = "Items to Delete";
            this.ThemeName = "Office2007Black";
            ((System.ComponentModel.ISupportInitialize)(this.radListControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deleteMessageRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.confirmDeleteRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelDeleteRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadListControl radListControl1;
        private Telerik.WinControls.UI.RadLabel deleteMessageRadLabel;
        private Telerik.WinControls.UI.RadButton confirmDeleteRadButton;
        private Telerik.WinControls.UI.RadButton cancelDeleteRadButton;
    }
}

