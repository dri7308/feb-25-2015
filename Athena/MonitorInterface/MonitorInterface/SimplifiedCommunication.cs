﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace MonitorInterface
{
    public class SimplifiedCommunication
    {
        public static ErrorCode OpenUsbConnectionToAnyMonitor(int modBusAddress, int numberOfUsbConnectionAttempts, int readDelayInMicroseconds, int numberOfNonInteractiveTries, int totalNumberOfTries)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                bool success;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    success = DeviceCommunication.OpenUSBConnection(numberOfUsbConnectionAttempts);

                    if (success)
                    {
                        DeviceCommunication.EnableDataDownload();
                        errorCode = ErrorCode.ConnectionOpenSucceeded;
                    }
                    else
                    {
                        errorCode = ErrorCode.ConnectionOpenFailed;
                    }
                }
                else
                {
                    errorCode = ErrorCode.ModbusAddressOutOfRange;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUsbConnectionToAnyMonitor(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


        public static ErrorCode OpenUsbConnectionToABhm(int modBusAddress, int numberOfUsbConnectionAttempts, int readDelayInMicroseconds, int numberOfNonInteractiveTries, int totalNumberOfTries, ParentWindowInformation parentWindowInformation)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                int deviceType;

                errorCode = OpenUsbConnectionToAnyMonitor(modBusAddress, numberOfUsbConnectionAttempts, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries);

                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries, parentWindowInformation);

                    if (deviceType > 0)
                    {
                        if (deviceType != 15002)
                        {
                            errorCode = ErrorCode.NotConnectedToBHM;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.DeviceTypeReadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUsbConnectionToABhm(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


        public static ErrorCode OpenUsbConnectionToAPdm(int modBusAddress, int numberOfUsbConnectionAttempts, int readDelayInMicroseconds, int numberOfNonInteractiveTries, int totalNumberOfTries, ParentWindowInformation parentWindowInformation)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                int deviceType;

                errorCode = OpenUsbConnectionToAnyMonitor(modBusAddress, numberOfUsbConnectionAttempts, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries);

                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries, parentWindowInformation);

                    if (deviceType > 0)
                    {
                        if (deviceType != 505)
                        {
                            errorCode = ErrorCode.NotConnectedToPDM;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.DeviceTypeReadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUsbConnectionToAPdm(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenUsbConnectionToAMainMonitor(int modBusAddress, int numberOfUsbConnectionAttempts, int readDelayInMicroseconds, int numberOfNonInteractiveTries, int totalNumberOfTries, ParentWindowInformation parentWindowInformation)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                int deviceType;

                errorCode = OpenUsbConnectionToAnyMonitor(modBusAddress, numberOfUsbConnectionAttempts, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries);

                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveTries, totalNumberOfTries, parentWindowInformation);

                    if (deviceType > 0)
                    {
                        if (deviceType != 101)
                        {
                            errorCode = ErrorCode.NotConnectedToMainMonitor;
                        }                       
                    }
                    else
                    {
                        errorCode = ErrorCode.DeviceTypeReadFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUsbConnectionToAMainMonitor(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


    }
}
