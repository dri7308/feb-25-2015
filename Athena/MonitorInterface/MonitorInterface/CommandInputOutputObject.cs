﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonitorInterface
{
    public class CommandInputOutputObject
    {
        public CommandName commandName = CommandName.None;
        public int numberOfRetriesCompleted = 0;
        public int totalNumberOfRetriesToAttempt = 0;

        public string ipAddress = string.Empty;
        public Int16 portNumber = 0;
        public int modBusAddress = 0;
        public int offset = 0;
        public int recordNumber = 0;
        public int groupNumber = 0;
        public int channelNumber = 0;
        public int numberOfItemsToRead = 0;
        public int numberOfItemsToWrite = 0;
        public int readDelayInMicroseconds = 0;
        public int estimatedCommandCompletionTimeInSeconds = 0;
        public Byte[] inputBytes = null;
        public Int16 inputRegister = 0;
        public Int16[] inputRegisters = null;
        public string inputString = string.Empty;
        public DateTime inputDateTime = GeneralUtilities.ConversionMethods.MinimumDateTime();

        public bool returnedBoolean = false;
        public int returnedInteger = 0;
        public Int32[] returnedIntegers = null;
        public long returnedLong = 0;
        public string returnedString = string.Empty;
        public Byte[] returnedBytes = null;
        public Int16[] returnedRegisters = null;
        public DateTime returnedDateTime = GeneralUtilities.ConversionMethods.MinimumDateTime();
    }
}
