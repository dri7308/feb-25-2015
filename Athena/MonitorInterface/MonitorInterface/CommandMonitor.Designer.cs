﻿namespace MonitorInterface
{
    partial class CommandMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommandMonitor));
            this.waitingForReplyRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitoringRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.commandValueTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.stopMonitoringRadButton = new Telerik.WinControls.UI.RadButton();
            this.stopMonitoringWillNotStopCommandTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.commandTextRadLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.waitingForReplyRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandValueTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopMonitoringRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopMonitoringWillNotStopCommandTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandTextRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // waitingForReplyRadLabel
            // 
            this.waitingForReplyRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitingForReplyRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.waitingForReplyRadLabel.Location = new System.Drawing.Point(12, 39);
            this.waitingForReplyRadLabel.Name = "waitingForReplyRadLabel";
            this.waitingForReplyRadLabel.Size = new System.Drawing.Size(157, 16);
            this.waitingForReplyRadLabel.TabIndex = 30;
            this.waitingForReplyRadLabel.Text = "Waiting for the device to finish";
            this.waitingForReplyRadLabel.ThemeName = "Office2007Black";
            // 
            // monitoringRadWaitingBar
            // 
            this.monitoringRadWaitingBar.Location = new System.Drawing.Point(12, 70);
            this.monitoringRadWaitingBar.Name = "monitoringRadWaitingBar";
            this.monitoringRadWaitingBar.Size = new System.Drawing.Size(214, 24);
            this.monitoringRadWaitingBar.TabIndex = 29;
            this.monitoringRadWaitingBar.Text = "radWaitingBar1";
            this.monitoringRadWaitingBar.ThemeName = "Office2007Black";
            this.monitoringRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.monitoringRadWaitingBar.WaitingSpeed = 10;
            // 
            // commandValueTextRadLabel
            // 
            this.commandValueTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandValueTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commandValueTextRadLabel.Location = new System.Drawing.Point(76, 12);
            this.commandValueTextRadLabel.Name = "commandValueTextRadLabel";
            this.commandValueTextRadLabel.Size = new System.Drawing.Size(96, 16);
            this.commandValueTextRadLabel.TabIndex = 27;
            this.commandValueTextRadLabel.Text = "Download Type is";
            this.commandValueTextRadLabel.ThemeName = "Office2007Black";
            // 
            // stopMonitoringRadButton
            // 
            this.stopMonitoringRadButton.Location = new System.Drawing.Point(12, 149);
            this.stopMonitoringRadButton.Name = "stopMonitoringRadButton";
            this.stopMonitoringRadButton.Size = new System.Drawing.Size(214, 24);
            this.stopMonitoringRadButton.TabIndex = 26;
            this.stopMonitoringRadButton.Text = "Stop Monitoring";
            this.stopMonitoringRadButton.ThemeName = "Office2007Black";
            this.stopMonitoringRadButton.Click += new System.EventHandler(this.cancelMonitorRadButton_Click);
            // 
            // stopMonitoringWillNotStopCommandTextRadLabel
            // 
            this.stopMonitoringWillNotStopCommandTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopMonitoringWillNotStopCommandTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.stopMonitoringWillNotStopCommandTextRadLabel.Location = new System.Drawing.Point(12, 116);
            this.stopMonitoringWillNotStopCommandTextRadLabel.Name = "stopMonitoringWillNotStopCommandTextRadLabel";
            this.stopMonitoringWillNotStopCommandTextRadLabel.Size = new System.Drawing.Size(214, 27);
            this.stopMonitoringWillNotStopCommandTextRadLabel.TabIndex = 31;
            this.stopMonitoringWillNotStopCommandTextRadLabel.Text = "<html>Even if you stop monitoring, the command<br>has been sent and won\'t be canc" +
    "elled</html>";
            this.stopMonitoringWillNotStopCommandTextRadLabel.ThemeName = "Office2007Black";
            // 
            // commandTextRadLabel
            // 
            this.commandTextRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandTextRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commandTextRadLabel.Location = new System.Drawing.Point(12, 12);
            this.commandTextRadLabel.Name = "commandTextRadLabel";
            this.commandTextRadLabel.Size = new System.Drawing.Size(58, 16);
            this.commandTextRadLabel.TabIndex = 32;
            this.commandTextRadLabel.Text = "Command";
            this.commandTextRadLabel.ThemeName = "Office2007Black";
            // 
            // CommandMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(242, 186);
            this.Controls.Add(this.commandTextRadLabel);
            this.Controls.Add(this.stopMonitoringWillNotStopCommandTextRadLabel);
            this.Controls.Add(this.waitingForReplyRadLabel);
            this.Controls.Add(this.monitoringRadWaitingBar);
            this.Controls.Add(this.commandValueTextRadLabel);
            this.Controls.Add(this.stopMonitoringRadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CommandMonitor";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Command Monitor";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.CommandMonitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.waitingForReplyRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitoringRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandValueTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopMonitoringRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopMonitoringWillNotStopCommandTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandTextRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel waitingForReplyRadLabel;
        private Telerik.WinControls.UI.RadWaitingBar monitoringRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel commandValueTextRadLabel;
        private Telerik.WinControls.UI.RadButton stopMonitoringRadButton;
        private Telerik.WinControls.UI.RadLabel stopMonitoringWillNotStopCommandTextRadLabel;
        private Telerik.WinControls.UI.RadLabel commandTextRadLabel;
    }
}