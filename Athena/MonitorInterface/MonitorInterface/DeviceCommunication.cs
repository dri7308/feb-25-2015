﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FieldTalk;
using System.Threading;
using GeneralUtilities;

namespace MonitorInterface
{
    public enum ConnectionType
    {
        Serial,
        SOE,
        TCP,
        USB,
        None
    };

    public class DeviceCommunication
    {
        /// This value was pretty much arbitrary, just as long as it's smaller than the limit.  I originally tried to be
        /// tricky and make it a multiple of 64 so the maximumShortsInARead for USB would be "even" but that really doesn't matter
        /// in the long run, since it still gets broken up into pieces on the USB send/receive.  The limit in the ModBus code is 256 
        /// bytes, which includes 6 bytes for the header, 248 bytes for the message, and 2 bytes for crc.  248 bytes = 124 shorts.
        private static int maximumShortsInARead = 124;

        /// <summary>
        /// Maximum retries allowed from this code, does not account for retries in the fieldtalk code, which exist
        /// </summary>
        private static int MAX_RETRY = 20;
        public static int MaxRetries
        {
            get
            {
                return MAX_RETRY;
            }
        }

        //private static int retryCount;
        //public static int RetryCount
        //{
        //    get
        //    {
        //        return retryCount;
        //    }
        //}

        private static ConnectionType currentConnectionType;
        public static ConnectionType CurrentConnectionType
        {
            get
            {
                return currentConnectionType;
            }
        }

        private static int minRetriesBeforeLaunchingCommandRetryInterface = 1;

        public static int DEVICE_AFTER_WRITE_WAIT_INTERVAL = 2;

        public static int DEVICE_AFTER_FAILURE_WAIT_INTERVAL = 2;

        private static bool monitorDataDownloadIsRunning = false;

        /// <summary>
        /// This is pretty much set aside for the routines that open a connection to the monitor
        /// </summary>
        private static bool connectionIsEnabled = false;

        private static Dictionary<int, string> ftalkErrorCodes = null;

        private static string downloadFailedMessage = "DownloadFailed";
        public static string DownloadFailedMessage
        {
            get
            {
                return downloadFailedMessage;
            }
        }

        private static string downloadSucceededMessage = "DownloadSucceeded";
        public static string DownloadSucceededMessage
        {
            get
            {
                return downloadSucceededMessage;
            }
        }

        private static string downloadCancelledMessage = "DownloadCancelled";
        public static string DownloadCancelledMessage
        {
            get
            {
                return downloadCancelledMessage;
            }
        }

        private static string incorrectDeviceTypeMessage = "IncorrectDeviceType";
        public static string IncorrectDeviceTypeMessage
        {
            get
            {
                return incorrectDeviceTypeMessage;
            }
        }

        private static bool commandRetryIsActive;
        public static bool CommandRetryIsActive
        {
            get
            {
                return commandRetryIsActive;
            }
        }

        private static bool commandRetryInvokerIsActive;

        private static bool commandRetryIssuedCancellation;

        private static BackgroundWorker commandRetryInvokerBackgroundWorker;

        //public static void InitializeCommandRetryInvokerBackgroundWorker()
        //{
        //    /// Set up the download background worker
        //    commandRetryInvokerBackgroundWorker = new BackgroundWorker()
        //    {
        //        WorkerSupportsCancellation = true
        //    };

        //    commandRetryInvokerBackgroundWorker.DoWork += commandRetryInvokerBackgroundWorker_DoWork;
        //    commandRetryInvokerBackgroundWorker.RunWorkerCompleted += commandRetryInvokerBackgroundWorker_RunWorkerCompleted;
        //}


//        private static void commandRetryInvokerBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
//        {
//            try
//            {
//                CommandInputOutputObject inputOutputObject = e.Argument as CommandInputOutputObject;
//                commandRetryInvokerIsActive = true;
//                commandRetryIssuedCancellation = false;
//                MessageBox.Show("Boo!");
//                commandRetryInvokerIsActive = false;
//                //using (CommandRetryInterface retry = new CommandRetryInterface(inputOutputObject))
//                //{
//                //    retry.ShowDialog();
//                //    retry.Hide();
//                //    if (retry.CommandWasCancelled)
//                //    {
//                //        inputOutputObject.commandWasCancelled = true;
//                //    }
//                //    else
//                //    {
//                //        inputOutputObject.commandWasCancelled = false;
//                //    }
//                //}
//                e.Result = inputOutputObject;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.progressBarUpdateBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private static void commandRetryInvokerBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
//        {
//            try
//            {
//                CommandInputOutputObject inputOutputObject = e.Result as CommandInputOutputObject;
//                if (e.Error != null)
//                {
//                    string errorMessage = "Exception thrown in DeviceCommunication.progressBarUpdateBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//                if (inputOutputObject.commandWasCancelled)
//                {
//                    commandRetryIssuedCancellation = true;
//                }
//                commandRetryInvokerIsActive = false;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.commandRetryInvokerBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private static void StartCommandRetryInvokerIfConditionsAreMet(string commandName)
//        {
//            try
//            {
//                CommandInputOutputObject inputObject;

//                if ((!commandRetryInvokerIsActive)&&(retryCount == minRetriesBeforeLaunchingCommandRetryInterface) )
//                {
//                    inputObject = new CommandInputOutputObject();

//                    commandRetryInvokerIsActive = true;
//                    commandRetryIsActive = true;
                    
//                    inputObject.failedCommandString = commandName;

//                    commandRetryInvokerBackgroundWorker.RunWorkerAsync(inputObject);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.StartCommandRetryInvokerIfConditionsAreMet(string, string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private static void WaitForCommandRetryInvokerToFinish()
//        {
//            try
//            {
//                commandRetryIsActive = false;
//                while (commandRetryInvokerIsActive)
//                {
//                    Thread.Sleep(5);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.WaitForCommandRetryInvokerToFinish()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        /// <summary>
        /// The protocol used for any communication with the device
        /// </summary>
        private static MbusMasterFunctions selectedCommunicationProtocol;
        public static MbusMasterFunctions SelectedCommunicationProtocol
        {
            get
            {
                return selectedCommunicationProtocol;
            }
        }

        public static void EnableDataDownload()
        {
            DeviceCommunication.monitorDataDownloadIsRunning = true;
        }

        public static void DisableDataDownload()
        {
            DeviceCommunication.monitorDataDownloadIsRunning = false;
        }

        public static bool DownloadIsEnabled()
        {
            return monitorDataDownloadIsRunning;
        }

        /// <summary>
        /// Enably monitor connection
        /// </summary>
        public static void EnableConnection()
        {
            DeviceCommunication.connectionIsEnabled = true;
        }

        /// <summary>
        /// Disable retry of monitor connection routines
        /// </summary>
        public static void DisableConnection()
        {
            DeviceCommunication.connectionIsEnabled = false;
        }

        /// <summary>
        /// Check whether retry of monitor connection routines is set
        /// </summary>
        /// <returns></returns>
        public static bool ConnectionIsEnabled()
        {
            return DeviceCommunication.connectionIsEnabled;
        }

        /// <summary>
        /// Tests whether the communication protocol is null, and then if it is open
        /// </summary>
        /// <returns></returns>
        public static bool SelectedCommunicationProtocolIsOpen()
        {
            return ((selectedCommunicationProtocol != null) && (selectedCommunicationProtocol.isOpen()));
            //return (selectedCommunicationProtocol != null);
        }

        public static bool SetTimeOut(int timeOutInMilliseconds)
        {
            bool success = false;
            try
            {
                if (timeOutInMilliseconds < 1000)
                {
                    timeOutInMilliseconds = 1000;
                }
                if (DeviceCommunication.selectedCommunicationProtocol != null)
                {
                    DeviceCommunication.selectedCommunicationProtocol.setTimeout(timeOutInMilliseconds);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.SetTimeOut(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static int GetTimeOut()
        {
            int timeOut = 0;
            try
            {
                if (DeviceCommunication.selectedCommunicationProtocol != null)
                {
                    timeOut = DeviceCommunication.selectedCommunicationProtocol.getTimeout();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetTimeOut()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return timeOut;
        }

        public static void FillFtalkErrorCodes()
        {
            ftalkErrorCodes = new Dictionary<int, string>();
            ftalkErrorCodes.Add(1, "FTALK_ILLEGAL_ARGUMENT_ERROR");
            ftalkErrorCodes.Add(2, "FTALK_ILLEGAL_STATE_ERROR");
            ftalkErrorCodes.Add(3, "FTALK_EVALUATION_EXPIRED");
            ftalkErrorCodes.Add(4, "FTALK_NO_DATA_TABLE_ERROR");
            ftalkErrorCodes.Add(5, "FTALK_ILLEGAL_SLAVE_ADDRESS");
            ftalkErrorCodes.Add(64, "FTALK_IO_ERROR_CLASS");
            ftalkErrorCodes.Add(65, "FTALK_IO_ERROR");
            ftalkErrorCodes.Add(66, "FTALK_OPEN_ERR");
            ftalkErrorCodes.Add(67, "FTALK_PORT_ALREADY_OPEN");
            ftalkErrorCodes.Add(68, "FTALK_TCPIP_CONNECT_ERR");
            ftalkErrorCodes.Add(69, "FTALK_CONNECTION_WAS_CLOSED");
            ftalkErrorCodes.Add(70, "FTALK_SOCKET_LIB_ERROR");
            ftalkErrorCodes.Add(71, "FTALK_PORT_ALREADY_BOUND");
            ftalkErrorCodes.Add(72, "FTALK_LISTEN_FAILED");
            ftalkErrorCodes.Add(73, "FTALK_FILEDES_EXCEEDED");
            ftalkErrorCodes.Add(74, "FTALK_PORT_NO_ACCESS");
            ftalkErrorCodes.Add(75, "FTALK_PORT_NOT_AVAIL");
            ftalkErrorCodes.Add(128, "FTALK_BUS_PROTOCOL_ERROR_CLASS");
            ftalkErrorCodes.Add(129, "FTALK_CHECKSUM_ERROR");
            ftalkErrorCodes.Add(130, "FTALK_INVALID_FRAME_ERROR");
            ftalkErrorCodes.Add(131, "FTALK_INVALID_REPLY_ERROR");
            ftalkErrorCodes.Add(132, "FTALK_REPLY_TIMEOUT_ERROR");
            ftalkErrorCodes.Add(133, "FTALK_SEND_TIMEOUT_ERROR");
            ftalkErrorCodes.Add(160, "FTALK_MBUS_EXCEPTION_RESPONSE");
            ftalkErrorCodes.Add(161, "FTALK_MBUS_ILLEGAL_FUNCTION_RESPONSE");
            ftalkErrorCodes.Add(162, "FTALK_MBUS_ILLEGAL_ADDRESS_RESPONSE");
            ftalkErrorCodes.Add(163, "FTALK_MBUS_ILLEGAL_VALUE_RESPONSE");
            ftalkErrorCodes.Add(164, "FTALK_MBUS_SLAVE_FAILURE_RESPONSE");
        }

        private static void LogCommunicationErrorMessage(string functionName, int fieldTalkReturnValue)
        {
            try
            {
                if (fieldTalkReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                {
                    string errorMessage = "Error in MonitorInterface." + functionName + ": Error code " + fieldTalkReturnValue.ToString() + "  Error name ";
                    if (ftalkErrorCodes == null)
                    {
                        FillFtalkErrorCodes();
                    }
                    if (ftalkErrorCodes.ContainsKey(fieldTalkReturnValue))
                    {
                        errorMessage += ftalkErrorCodes[fieldTalkReturnValue];
                    }
                    else
                    {
                        errorMessage += "Unknown";
                    }
                    LogMessage.LogError(errorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.LogCommunicationErrorMessage(string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Opens a serial connection to the device with the specified serial port, returns true if the operation
        /// was successful
        /// </summary>
        /// <returns></returns>
        public static bool OpenSerialConnection(string serialPort, int baudRate)
        {
            bool success = false;
            try
            {
                Int32 parity, dataBits, stopBits, protocolInitializationResult;
                int retryCount = 0;
                if (serialPort != null)
                {
                    /// these values were taken from existing code
                    selectedCommunicationProtocol = new MbusRtuMasterProtocol();
                    selectedCommunicationProtocol.timeout = 2000;
                    selectedCommunicationProtocol.retryCnt = 1;
                    selectedCommunicationProtocol.pollDelay = 200;
                    parity = MbusSerialMasterProtocol.SER_PARITY_NONE;
                    dataBits = MbusSerialMasterProtocol.SER_DATABITS_8;
                    stopBits = MbusSerialMasterProtocol.SER_STOPBITS_1;

                    //open port

                    protocolInitializationResult = 100;

                    while ((retryCount < 5) && (protocolInitializationResult != BusProtocolErrors.FTALK_SUCCESS))
                    {
                        protocolInitializationResult = ((MbusRtuMasterProtocol)selectedCommunicationProtocol).openProtocol(serialPort, baudRate, dataBits, stopBits, parity);
                        LogCommunicationErrorMessage("OpenSerialConnection", protocolInitializationResult);
                        retryCount++;
                    }

                    if (protocolInitializationResult == BusProtocolErrors.FTALK_SUCCESS)
                    {
                        // commandsAndResponsesListBox.Items.Add("Opened Connection");
                        currentConnectionType = ConnectionType.Serial;
                        success = true;
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.OpenSerialConnection(string, int)\nCould not open a connection to the device.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.OpenSerialConnection(string, int)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenSerialConnection(string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Opens a serial over ethernet connection to the device with the specified IP address
        /// </summary>
        /// <returns></returns>
        public static bool OpenSerialOverEthernetConnection(string ipAddress, Int16 portNumber)
        {
            bool success = false;
            try
            {
                Int32 protocolInitializationResult;
                int retryCount = 0;
                if (ipAddress != null)
                {
                    /// all the following was copied from other code, under the assumption
                    /// that the parameter values are fairly standard
                    selectedCommunicationProtocol = new MbusRtuOverTcpMasterProtocol();
                    // people seem to like 1000 (1 sec) for this value, but i've changed it 
                    // sometimes as an experiment when trying to communicate with different devices
                    selectedCommunicationProtocol.timeout = 3000;
                    selectedCommunicationProtocol.retryCnt = 0;
                    selectedCommunicationProtocol.pollDelay = 250;

                    protocolInitializationResult = 100;

                    while ((retryCount < 5) && (protocolInitializationResult != BusProtocolErrors.FTALK_SUCCESS))
                    {
                        protocolInitializationResult = ((MbusRtuOverTcpMasterProtocol)selectedCommunicationProtocol).openProtocol(ipAddress, portNumber);
                        LogCommunicationErrorMessage("OpenSerialOverEthernetConnection", protocolInitializationResult);
                        retryCount++;
                    }
                    if (protocolInitializationResult == BusProtocolErrors.FTALK_SUCCESS)
                    {
                        success = true;
                        currentConnectionType = ConnectionType.SOE;
                    }
                    else
                    {
                        LogMessage.LogError("Error in DeviceCommunication.OpenSerialOverEthernetConnection(string)\nFailed to open a SOE connection.");
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.OpenSerialOverEthernetConnection(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenSerialOverEthernetConnection(string, Int16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Opens a serial over ethernet connection to the device with the specified IP address
        /// </summary>
        /// <returns></returns>
        public static bool OpenSerialOverEthernetConnection(string ipAddress, Int16 portNumber, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                Int32 protocolInitializationResult;
                int retryCount = numberOfRetriesAlreadyAttempted;
                List<int> errorCodes;

                if (ipAddress != null)
                {
                    /// all the following was copied from other code, under the assumption
                    /// that the parameter values are fairly standard
                    selectedCommunicationProtocol = new MbusRtuOverTcpMasterProtocol();
                    // people seem to like 1000 (1 sec) for this value, but i've changed it 
                    // sometimes as an experiment when trying to communicate with different devices
                    selectedCommunicationProtocol.timeout = 3000;
                    selectedCommunicationProtocol.retryCnt = 0;
                    selectedCommunicationProtocol.pollDelay = 250;

                    errorCodes = new List<int>();

                    protocolInitializationResult = 100;

                    while ((retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.connectionIsEnabled && (protocolInitializationResult != BusProtocolErrors.FTALK_SUCCESS))
                    {
                        protocolInitializationResult = ((MbusRtuOverTcpMasterProtocol)selectedCommunicationProtocol).openProtocol(ipAddress, portNumber);
                        LogCommunicationErrorMessage("OpenSerialOverEthernetConnection", protocolInitializationResult);
                        retryCount++;
                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                    }
                    if (retryCount == totalNumberOfRetriesToAttempt)
                    {
                        StringBuilder errorMessage = new StringBuilder();
                        errorMessage.Append("Error in DeviceCommunication.OpenSerialOverEthernetConnection(string, Int16, int, int)\nFailed to open a SOE connection after ");
                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                        errorMessage.Append(" attempts.\nError codes seen were: ");
                        foreach (int entry in errorCodes)
                        {
                            errorMessage.Append(entry.ToString());
                            errorMessage.Append("  ");
                        }
                    }
                    if (protocolInitializationResult == BusProtocolErrors.FTALK_SUCCESS)
                    {
                        success = true;
                        currentConnectionType = ConnectionType.SOE;
                    }                   
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.OpenSerialOverEthernetConnection(string, Int16, int, int)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenSerialOverEthernetConnection(string, Int16, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }



        /// <summary>
        /// Opens an ethernet (TCP) connection to the device with the specified IP address
        /// </summary>
        /// <returns></returns>
        public static bool OpenEthernetConnection(string ipAddress, Int16 portNumber)
        {
            bool success = false;
            try
            {
                Int32 protocolInitializationResult;
                int retryCount = 0;
                if (ipAddress != null)
                {
                    /// all the following was copied from other code, under the assumption
                    /// that the parameter values are fairly standard.  I increased the timout though.
                    selectedCommunicationProtocol = new MbusTcpMasterProtocol();
                    selectedCommunicationProtocol.timeout = 3000;
                    selectedCommunicationProtocol.retryCnt = 0;
                    selectedCommunicationProtocol.pollDelay = 250;

                    protocolInitializationResult = 100;

                    while ((retryCount < 5) && (protocolInitializationResult != BusProtocolErrors.FTALK_SUCCESS))
                    {
                        protocolInitializationResult = ((MbusTcpMasterProtocol)selectedCommunicationProtocol).openProtocol(ipAddress, portNumber);
                        LogCommunicationErrorMessage("OpenSerialOverEthernetConnection", protocolInitializationResult);
                        retryCount++;
                    }
                    if (protocolInitializationResult == BusProtocolErrors.FTALK_SUCCESS)
                    {
                        success = true;
                        currentConnectionType = ConnectionType.TCP;
                    }
                    else
                    {
                        LogMessage.LogError("Error in DeviceCommunication.OpenEthernetConnection(string, Int16)\nFailed to open a SOE connection.");
                    }                 
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.OpenEthernetConnection(string, Int16)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenEthernetConnection(string, Int16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool OpenUSBConnection()
        {
            bool success = false;
            try
            {
                success = OpenUSBConnection(1);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUSBConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Opens a USB connection to the connected device
        /// </summary>
        /// <returns></returns>
        public static bool OpenUSBConnection(int numberOfAttempts)
        {
            bool success = false;
            try
            {
                Int32 protocolInitializationResult;
                int attemptCount = 0;

                selectedCommunicationProtocol = new MbusUsbMasterProtocol();
                /// I picked this error just to initialize the return variable, it has no significance
                protocolInitializationResult = BusProtocolErrors.FTALK_IO_ERROR;

                selectedCommunicationProtocol.retryCnt = 1;

                /// Now is the time we loop!
                while ((protocolInitializationResult != BusProtocolErrors.FTALK_SUCCESS) && (attemptCount < numberOfAttempts))
                {
                    protocolInitializationResult = ((MbusUsbMasterProtocol)selectedCommunicationProtocol).openProtocol();
                    LogCommunicationErrorMessage("OpenUSBConnection", protocolInitializationResult);
                    attemptCount++;
                    Thread.Sleep(10);
                }
                if (protocolInitializationResult == BusProtocolErrors.FTALK_SUCCESS)
                {
                    success = true;
                    currentConnectionType = ConnectionType.USB;
                }
                else
                {
                    LogMessage.LogError("Error in DeviceCommunication.OpenUSBConnection(int)\nFailed to open a USB connection.");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.OpenUSBConnection(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

       
        /// <summary>
        /// Closes the current connection if it exists and is open
        /// </summary>
        /// <returns></returns>
        public static bool CloseConnection()
        {
            bool success = false;
            try
            {
                if(selectedCommunicationProtocol!=null)
                {
                    selectedCommunicationProtocol.closeProtocol();
                    selectedCommunicationProtocol = null;
                    success = true;
                    currentConnectionType = ConnectionType.None;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.CloseConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Int16[] ReadOneRegister(int modBusAddress, int register, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerValue = null;
            try
            {
                bool readIsIncorrect = true;
                int retryCount;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (register > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;
                            registerValue = new Int16[1];
                            while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt))
                            {
                                functionCallReturnValue = selectedCommunicationProtocol.readMultipleRegisters(modBusAddress, register, registerValue, 1, readDelayInMicroseconds);
                                if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                {
                                    LogCommunicationErrorMessage("ReadOneRegister", functionCallReturnValue);
                                    errorCodes.Add(functionCallReturnValue);
                                    retryCount++;
                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                }
                                else
                                {
                                    readIsIncorrect = false;
                                }
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.ReadOneRegister(int, int, int, int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                                registerValue = null;
                                LogMessage.LogError(errorMessage.ToString());
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.ReadOneRegister(int, int, int, int, int)\nThird input int needs to be greater than or equal to zero.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ReadOneRegister(int, int, int, int, int)\nSecond input int needs to be greater than 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ReadOneRegister(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ReadOneRegister(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValue;
        }

        /// <summary>
        /// Reads the general information registers (1-20), returns null if the operation failed, otherwise returns the register contents
        /// Expects that communication with the device has already been established.
        /// </summary>
        /// <returns></returns>
        public static Int16[] ReadRegisters1to20(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] HR1HR20 = null;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            HR1HR20 = ReadMultipleRegisters(modBusAddress, 1, 20, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (HR1HR20 == null)
                            {
                                string errorMessage = "Error in DeviceCommunication.ReadRegisters1to20(int, int, int, int)\nFailed to read the data";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.ReadRegisters1to20(int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ReadRegisters1to20(int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ReadRegisters1to20(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ReadRegisters1to20(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return HR1HR20;
        }

        /// <summary>
        /// Reads the registers specified by the starting register (registerOffset) and the numberOfRecordsToRead, inclusive.
        /// Returns null if the read failed.
        /// Expects that communication with the device has already been established.
        /// </summary>
        /// <returns>Null on read failure, Int16 array on read success</returns>
        public static Int16[] ReadMultipleRegisters(int modBusAddress, int registerOffset, int numberOfRecordsToRead, int readDelayInMicroseconds, 
                                                    int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerValues = null;
            try
            {
                bool readIsIncorrect = true;
                int retryCount;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (registerOffset > 0)
                    {
                        if (numberOfRecordsToRead > 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (numberOfRecordsToRead > DeviceCommunication.maximumShortsInARead)
                                {
                                    registerValues = ReadOverTheLimitMultipleRegisters(modBusAddress, registerOffset, numberOfRecordsToRead, readDelayInMicroseconds, 0, totalNumberOfRetriesToAttempt);
                                }
                                else
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    if (SelectedCommunicationProtocolIsOpen())
                                    {
                                        registerValues = new Int16[numberOfRecordsToRead];
                                        while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                        {
                                            functionCallReturnValue = selectedCommunicationProtocol.readMultipleRegisters(modBusAddress, registerOffset, registerValues, numberOfRecordsToRead, readDelayInMicroseconds);
                                            if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                            {
                                                //string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nFailed to read the data from the device.\nError code is " + functionCallReturnValue.ToString();
                                                //Logging.LogError(errorMessage);
                                                // registerValues = null;
                                                LogCommunicationErrorMessage("ReadMutipleRegisters", functionCallReturnValue);
                                                errorCodes.Add(functionCallReturnValue);
                                                retryCount++;
                                                RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                            }
                                            else
                                            {
                                                readIsIncorrect = false;
                                            }
                                        }
                                        if (retryCount == totalNumberOfRetriesToAttempt)
                                        {
                                            StringBuilder errorMessage = new StringBuilder();
                                            errorMessage.Append("Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nFailed to read the data from the device after ");
                                            errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                            errorMessage.Append(" attempts.\nError codes seen were: ");
                                            foreach (int entry in errorCodes)
                                            {
                                                errorMessage.Append(entry.ToString());
                                                errorMessage.Append("  ");
                                            }
                                            registerValues = null;
                                        }
                                        if (!DeviceCommunication.monitorDataDownloadIsRunning)
                                        {                                           
                                            registerValues = null;
                                        }
#if DEBUG
                                        if (retryCount > 0)
                                        {
                                            string retryMessage = "In DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                            LogMessage.LogError(retryMessage);
                                        }
#endif
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nYou need to open a connection to the device first.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nFourth input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nThird input int needs to be > 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ReadMultipleRegisters(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        /// <summary>
        /// Reads more than the hard limit of 260 registers by reading them in smaller pieces.  It returns a concatenated array of the results
        /// as would be expected if one could read all pieces at once.  
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="registerOffset"></param>
        /// <param name="numberOfRecordsToRead"></param>
        /// <returns></returns>
        private static Int16[] ReadOverTheLimitMultipleRegisters(int modBusAddress, int registerOffset, int numberOfRecordsToRead, int readDelayInMicroseconds, 
                                                                 int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            /// This does not need error checks on the input values because it can only be called internally.  The function that calls it already has
            /// checks on the input values.
            Int16[] registerValues = null;
            try
            {
                bool readSuccess = true;
                registerValues = new Int16[numberOfRecordsToRead];
                Int16[] registerValuesForOneRead;

                int chunkCount = numberOfRecordsToRead / DeviceCommunication.maximumShortsInARead;
                int remainder = numberOfRecordsToRead % DeviceCommunication.maximumShortsInARead;
                int runningRegisterOffset = registerOffset;
                int chunkIndex = 0;
                //int copyLoopIndex;
                //int registerValuesIndex = 0;

                while (readSuccess && (chunkIndex < chunkCount) && DeviceCommunication.monitorDataDownloadIsRunning)
                {
                    registerValuesForOneRead = ReadMultipleRegisters(modBusAddress, runningRegisterOffset, DeviceCommunication.maximumShortsInARead, 
                                                                     readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                    if (registerValuesForOneRead == null)
                    {
                        readSuccess = false;
                    }
                    else
                    {
                        Array.Copy(registerValuesForOneRead, 0, registerValues, runningRegisterOffset, DeviceCommunication.maximumShortsInARead);

                        //for (copyLoopIndex = 0; copyLoopIndex < DeviceCommunication.maximumShortsInARead; copyLoopIndex++)
                        //{
                        //    registerValues[registerValuesIndex] = registerValuesForOneRead[copyLoopIndex];
                        //    registerValuesIndex++;
                        //    Application.DoEvents();
                        //}
                    }
                    chunkIndex++;
                    runningRegisterOffset += DeviceCommunication.maximumShortsInARead;
                }

                if (remainder > 0)
                {
                    registerValuesForOneRead = ReadMultipleRegisters(modBusAddress, runningRegisterOffset, remainder, readDelayInMicroseconds, 0, totalNumberOfRetriesToAttempt);
                    if (registerValuesForOneRead == null)
                    {
                        readSuccess = false;
                    }
                    else
                    {
                        Array.Copy(registerValuesForOneRead, 0, registerValues, runningRegisterOffset, remainder);
                        //for (copyLoopIndex = 0; copyLoopIndex < remainder; copyLoopIndex++)
                        //{
                        //    registerValues[registerValuesIndex] = registerValuesForOneRead[copyLoopIndex];
                        //    registerValuesIndex++;
                        //}
                    }
                }

                if ((!DeviceCommunication.monitorDataDownloadIsRunning) || (!readSuccess))
                {
                    registerValues = null;
                }               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ReadOverTheLimitMultipleRegisters(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static bool WriteSingleRegister(int modBusAddress, int registerOffset, Int16 valueToWrite, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                int functionCallReturnValue;
                int retryCount;
                bool writeIsIncorrect;
                List<int> errorCodes;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (registerOffset > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                retryCount = numberOfRetriesAlreadyAttempted;
                                writeIsIncorrect = true;
                                errorCodes = new List<int>();
                                while (writeIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt))
                                {
                                    functionCallReturnValue = selectedCommunicationProtocol.writeSingleRegister(modBusAddress, registerOffset, valueToWrite, readDelayInMicroseconds);
                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                    {
                                        LogCommunicationErrorMessage("WriteSingleRegister", functionCallReturnValue);
                                        errorCodes.Add(functionCallReturnValue);
                                        retryCount++;
                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                    }
                                    else
                                    {
                                        success = true;
                                        writeIsIncorrect = false;
                                    }
                                }
                                if (retryCount == totalNumberOfRetriesToAttempt)
                                {
                                    StringBuilder errorMessage = new StringBuilder();
                                    errorMessage.Append("Error in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nFailed to write to the device after ");
                                    errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                    errorMessage.Append(" attempts.\nError codes seen were: ");
                                    foreach (int entry in errorCodes)
                                    {
                                        errorMessage.Append(entry.ToString());
                                        errorMessage.Append("  ");
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nYou need to open a connection to the device before trying to read registers";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nFourth input in needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nSecond input in needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nFirst input in needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.WriteSingleRegister(int, int, Int16, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool WriteMultipleRegisters(int modBusAddress, int registerOffset, Int16[] values, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                int functionCallReturnValue;
                int retryCount;
                bool writeIsIncorrect;
                List<int> errorCodes;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (registerOffset > 0)
                    {
                        if (values != null)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;
                                    writeIsIncorrect = true;
                                    errorCodes = new List<int>();
                                    while (writeIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.writeMultipleRegisters(modBusAddress, registerOffset, values, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("WriteMultipleRegisters", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            success = true;
                                            writeIsIncorrect = false;
                                        }
                                    }
                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nFailed to write to the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
                                    Thread.Sleep(DEVICE_AFTER_WRITE_WAIT_INTERVAL);
                                }
                                else
                                {
                                    string errorMessage = "In DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nYou need to open a connection to the device before trying to read registers";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nInput Int16[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Sends a string command to see if a device is paused.  Expects that communication with the device has already been established.  Returns 1 if paused, 0 if not paused, -1 for error, 2 for busy.
        /// </summary>
        /// <returns></returns>
        public static int DeviceIsPaused(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int paused = -1;
            try
            {
                string completeResponseMessage;
                string deviceResponce;
                string[] pieces;
                int pauseCount;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                                completeResponseMessage = SendStringCommand(modBusAddress, "PAUSE ?;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (StringCommandWasSuccessful(completeResponseMessage))
                                {
                                    deviceResponce = GetSendStringCommandReturnMessage(completeResponseMessage);
                                    pieces = deviceResponce.Split(' ');
                                    if (pieces.Length > 1)
                                    {
                                        pieces = pieces[1].Split(';');
                                        if (Int32.TryParse(pieces[0], out pauseCount))
                                        {
                                            if (pauseCount > 0)
                                            {
                                                paused = 1;
                                            }
                                            else
                                            {
                                                paused = 0;
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int, int, int)\nFailed to parse the pause time from the string command response message.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int, int, int)\nPause command failed.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }                                                       
                        }
                        else
                        {
#if DEBUG
                            MessageBox.Show("In DeviceCommunication.DeviceIsPaused: You need to open a connection to the device before trying to read registers");
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.DeviceIsPaused(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paused;
        }

//        /// <summary>
//        /// Checks the value in register 602 to see if the device is paused.  Expects that communication with the device has already been established.
//        /// </summary>
//        /// <returns></returns>
//        public static int DeviceIsPaused(int modBusAddress, int readDelayInMicroseconds)
//        {
//            int paused = -1;
//            try
//            {
//                Int16[] pauseBit;
//                if ((modBusAddress > 0) && (modBusAddress < 256))
//                {
//                    if (readDelayInMicroseconds >= 0)
//                    {
//                        if (SelectedCommunicationProtocolIsOpen())
//                        {
//                            pauseBit = ReadOneRegister(modBusAddress, 602, readDelayInMicroseconds);
//                            if (pauseBit == null)
//                            {
//                                string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int)\nFailed to determine if the device is paused.";
//                                LogMessage.LogError(errorMessage);
//                            }
//                            else
//                            {
//                                paused = pauseBit[0];
//                            }
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int)\nFailed to open a connection first";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int)\nSecond input int needs to be >= 0.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in DeviceCommunication.DeviceIsPaused(int, int)\nFirst input int needs to be between 1 and 255.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.DeviceIsPaused(int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return paused;
//        }

        /// <summary>
        /// Paused the device by issuing a string command to Pause to the device.  Expects that communication with the device has already been established.
        /// </summary>
        /// <returns></returns>
        public static bool PauseDevice(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                string completeResponseMessage;
                //string deviceResponce;
                //string[] pieces;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            completeResponseMessage = SendStringCommand(modBusAddress, "PAUSE;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (StringCommandWasSuccessful(completeResponseMessage))
                            {
                                success = true;
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int, int, int)\nPause command failed.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
#if DEBUG
                            MessageBox.Show("In DeviceCommunication.PauseDevice: You need to open a connection to the device before trying to read registers");
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PauseDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Resumes the device by sending a function 71 string command
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="readDelayInMicroseconds"></param>
        /// <returns></returns>
        public static bool ResumeDevice(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                string response = string.Empty;
                string returnMessage = string.Empty;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            returnMessage = SendStringCommand(modBusAddress, "RESUME;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (StringCommandWasSuccessful(returnMessage))
                            {
                                response = GetSendStringCommandReturnMessage(returnMessage);
                                if (response.Trim().CompareTo("RESUME") == 0)
                                {
                                    success = true;
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "In DeviceCommunication.ResumeDevice(int, int, int, int)\nYou need to open a connection to the device before trying to read registers";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ResumeDevice(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ResumeDevice(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ResumeDevice(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Sends the command to take a single measurement once.  The user must verify whether it succeeded in some other manner.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="readDelayInMicroseconds"></param>
        /// <returns></returns>
        public static bool StartMeasurement(int modBusAddress, int readDelayInMicroseconds)
        {
            bool success = false;
            try
            {
                string returnMessage = string.Empty;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            returnMessage = SendStringCommand(modBusAddress, "SINGLE;", readDelayInMicroseconds, 0, 1);     
                     
                        }
                        else
                        {
                            string errorMessage = "In DeviceCommunication.StartMeasurement(int, int)\nYou need to open a connection to the device before trying to start a measurement";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.StartMeasurement(int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.StartMeasurement(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.StartMeasurement(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        /// <summary>
        /// Paused the device by issuing a string command to Pause to the device.  Expects that communication with the device has already been established.
        /// </summary>
        /// <returns></returns>
        public static bool ClearDeviceData(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                string completeResponseMessage;
                //string deviceResponce;
                //string[] pieces;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            completeResponseMessage = SendStringCommand(modBusAddress, "CLEAR;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (StringCommandWasSuccessful(completeResponseMessage))
                            {
                                success = true;
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.ClearDeviceData(int, int, int, int)\nPause command failed.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
#if DEBUG
                            MessageBox.Show("In DeviceCommunication.ClearDeviceData: You need to open a connection to the device before trying to read registers");
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ClearDeviceData(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ClearDeviceData(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ClearDeviceData(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static DateTime GetDeviceTime(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            DateTime deviceTime = ConversionMethods.MinimumDateTime();
            try
            {
                string completeResponseMessage;
                string deviceResponce;
                string[] pieces;               
                bool failedToParseDate = false;
                int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            completeResponseMessage = SendStringCommand(modBusAddress, "TIME ?;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (StringCommandWasSuccessful(completeResponseMessage))
                            {
                                deviceResponce = GetSendStringCommandReturnMessage(completeResponseMessage);
                                pieces = deviceResponce.Split(' ');
                                /// Response should look like: 'TIME yyyymmdd hhmmss;'
                                if (pieces.Length == 3)
                                {
                                    if (pieces[1].Length == 8)
                                    {
                                        if (!Int32.TryParse(pieces[1].Substring(0, 4), out year))
                                        {
                                            failedToParseDate = true;
                                        }
                                        if (!failedToParseDate)
                                        {
                                            if (!Int32.TryParse(pieces[1].Substring(4, 2), out month))
                                            {
                                                failedToParseDate = true;
                                            }
                                        }
                                        if (!failedToParseDate)
                                        {
                                            if (!Int32.TryParse(pieces[1].Substring(6, 2), out day))
                                            {
                                                failedToParseDate = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nResponse message from device did not have the proper date format.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                        failedToParseDate = true;
                                    }
                                    // recall that the last character is a ';'
                                    if (pieces[2].Length == 6)
                                    {
                                        if (!failedToParseDate)
                                        {
                                            if (!Int32.TryParse(pieces[2].Substring(0, 2), out hour))
                                            {
                                                failedToParseDate = true;
                                            }
                                        }
                                        if (!failedToParseDate)
                                        {
                                            if (!Int32.TryParse(pieces[2].Substring(2, 2), out minute))
                                            {
                                                failedToParseDate = true;
                                            }
                                        }
                                        if (!failedToParseDate)
                                        {
                                            if (!Int32.TryParse(pieces[2].Substring(4, 2), out second))
                                            {
                                                failedToParseDate = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nResponse message from device did not have the proper time format.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                        failedToParseDate = true;
                                    }
                                    if (!failedToParseDate)
                                    {
                                        deviceTime = new DateTime(year, month, day, hour, minute, second);
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nFailed to parse the pause time from the string command response message.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nThe command to get the device time failed.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
#if DEBUG
                            MessageBox.Show("In DeviceCommunication.GetDeviceTime: You need to open a connection to the device before trying to read registers");
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetDeviceTime(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetDeviceTime(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceTime;
        }

//        /// <summary>
//        /// Paused the device by writing "1" to register 602.  Expects that communication with the device has already been established.
//        /// </summary>
//        /// <returns></returns>
//        public static bool PauseDevice(int modBusAddress, int readDelayInMicroseconds)
//        {
//            bool success = false;
//            try
//            {
//                if ((modBusAddress > 0) && (modBusAddress < 256))
//                {
//                    if (readDelayInMicroseconds >= 0)
//                    {
//                        if (SelectedCommunicationProtocolIsOpen())
//                        {
//                            success = WriteSingleRegister(modBusAddress, 602, (Int16)1, readDelayInMicroseconds);
//                            if (!success)
//                            {
//                                string errorMessage = "Error in DeviceCommunication.PauseDevice(int)\nFailed to pause the device.";
//                                LogMessage.LogError(errorMessage);
//                            }
//                        }
//                        else
//                        {
//#if DEBUG
//                            MessageBox.Show("In DeviceCommunication.PauseDevice: You need to open a connection to the device before trying to read registers");
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int)\nSecond input int needs to be >= 0.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in DeviceCommunication.PauseDevice(int, int)\nFirst input int needs to be between 1 and 255.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.PauseDevice(int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return success;
//        }

//        /// <summary>
//        /// Resumes (unpauses) the device by writing a "0" to register 602.  Expects that communication with the device has already been established.
//        /// </summary>
//        /// <returns></returns>
//        public static bool ResumeDevice(int modBusAddress, int readDelayInMicroseconds)
//        {
//            bool success = false;
//            try
//            {
//                if ((modBusAddress > 0) && (modBusAddress < 256))
//                {
//                    if (readDelayInMicroseconds >= 0)
//                    {
//                        if (SelectedCommunicationProtocolIsOpen())
//                        {
//                            success = WriteSingleRegister(modBusAddress, 602, (Int16)0, readDelayInMicroseconds);
//                            if (!success)
//                            {
//                                string errorMessage = "Error in DeviceCommunication.ResumeDevice(int)\nFailed to resume the device.";
//                                LogMessage.LogError(errorMessage);
//                            }
//                        }
//                        else
//                        {
//                            string errorMessage = "In DeviceCommunication.ResumeDevice(int, int)\nYou need to open a connection to the device before trying to read registers";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in DeviceCommunication.ResumeDevice(int, int)\nSecond input int needs to be >= 0.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in DeviceCommunication.ResumeDevice(int, int)\nFirst input int needs to be between 1 and 255.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DeviceCommunication.ResumeDevice(int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return success;
//        }

        /// <summary>
        /// Gets the type of device present at the input modbus address by reading register 1.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <returns></returns>
        public static int GetDeviceTypeRegisterVersion(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int deviceType = 0;
            Int16[] values;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        values = ReadOneRegister(modBusAddress, 1, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                        if (values != null)
                        {
                            deviceType = values[0];
                            if (!((deviceType == 505) || (deviceType == 15002) || (deviceType == 6001) || (deviceType == 101)))
                            {
                                string errorMessage = "Error in DeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int, int)\nDevice type not recognized.  Value is " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int, int)\nFailed to read the register.";
                            LogMessage.LogError(errorMessage);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        #region General Methods

        /// <summary>
        /// Gets the number of archived records in a BHM or PDM from register 618.  Expects that communication with the device has already been established.
        /// </summary>
        /// <returns></returns>
        public static Int16 GetNumberOfArchiveRecordsSinceLastRecordRequest(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16 numberOfArchiveRecords = -1;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            int deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if ((deviceType == 15002) || (deviceType == 505))
                            {
                                Int16[] HR618 = ReadOneRegister(modBusAddress, 618, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (HR618 == null)
                                {
                                    string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nFailed to read the register.";
                                    LogMessage.LogError(errorMessage);
                                }
                                else
                                {
                                    numberOfArchiveRecords = HR618[0];
                                    if (numberOfArchiveRecords == -1)
                                    {
                                        string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nFailed to convert the number of archived records to a string.\nValue was " + HR618[0].ToString();
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                //  MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nFailed to open a connection to the device first.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return numberOfArchiveRecords;
        }

        /// <summary>
        /// Gets the total number of stored records in a BHM or PDM.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <returns></returns>
        public static Int16 GetNumberOfRecordsInDeviceMemory(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16 numberOfRecords = -1;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            int deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if ((deviceType == 15002) || (deviceType == 505) || (deviceType == 6001))
                            {
                                Int16[] HR15 = ReadOneRegister(modBusAddress, 15, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (HR15 == null)
                                {
                                    string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nFailed to read the register.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    // MessageBox.Show(errorMessage);
#endif
                                }
                                else
                                {
                                    numberOfRecords = HR15[0];
                                    if (numberOfRecords < 0)
                                    {
                                        string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nFailed to convert the number of records in memory to a string.\nValue was " + HR15[0].ToString();
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nFailed to open a connection to the device first.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return numberOfRecords;
        }

        /// <summary>
        /// Used to set the date of the last record downloaded for a BHM or PDM, so the device can get a count of all records not
        /// downloaded yet.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="lastRecordDownloadedDate"></param>
        /// <returns></returns>
        public static bool SetLastRecordDownloadedDate(int modBusAddress, DateTime lastRecordDownloadedDate, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            int deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if ((deviceType == 15002) || (deviceType == 505))
                            {
                                Int16[] integerDateTime = ConversionMethods.ConvertDateToIntegerArray(lastRecordDownloadedDate);
                                success = WriteMultipleRegisters(modBusAddress, 613, integerDateTime, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (!success)
                                {
                                    string errorMessage = "Error in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int).\nFailed to write the record download date to the device.";
                                    LogMessage.LogError(errorMessage);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int)\nFailed to open a connection to the device first.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the date set for the last record download for a BHM or PDM.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <returns></returns>
        public static DateTime GetLastRecordDownloadedDate(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            DateTime dateTimeRead = ConversionMethods.MinimumDateTime();
            try
            {
                Int16[] valuesRead;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            int deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if ((deviceType == 15002) || (deviceType == 505))
                            {
                                valuesRead = ReadMultipleRegisters(modBusAddress, 613, 5, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (valuesRead == null)
                                {
                                    if (valuesRead.Length == 5)
                                    {
                                        dateTimeRead = ConversionMethods.ConvertIntegerValuesToDateTime(valuesRead[0], valuesRead[1], valuesRead[2], valuesRead[3], valuesRead[4]);                                        
                                    }                                   
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.GetLastRecordDownloadedDate(int, int, int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "In DeviceCommunication.GetLastRecordDownloadedDate(int, int, int, int)\nYou need to open a connection to the device before trying to read registers";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.GetLastRecordDownloadedDate(int, int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetLastRecordDownloadedDate(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetLastRecordDownloadedDate(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dateTimeRead;
        }

        /// <summary>
        /// This writes the index of the BHM or PDM data sample we wish to read to register 619, which moves the associated
        /// data to the archive registers so we can read it.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="dataSampleIndex"></param>
        /// <returns></returns>
        public static bool MoveDataSampleIntoTheArchiveRegisters(int modBusAddress, Int16 dataSampleIndex, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = true;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (dataSampleIndex > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                int deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if ((deviceType == 15002) || (deviceType == 505))
                                {
                                    success = WriteSingleRegister(modBusAddress, 619, dataSampleIndex, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                    if (!success)
                                    {
                                        string errorMessage = "Error in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nFailed to write to the register and move the data into the archives.";
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    // MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "In DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nYou need to open a connection to the device before trying to read registers";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nSecond input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nInput Int16 needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #region String Command (function 71)

        /// <summary>
        /// Sends a string command (function 71) to a device.  Returns the error code as well as the return message, if applicable.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string SendStringCommand(int modBusAddress, string message, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            string commandResponse = string.Empty;
            try
            {
                Char[] commandCharacters;
                int numberOfResponseCharactersExpected = 240;
                Byte[] responseCharacters = new Byte[numberOfResponseCharactersExpected];
                Int32[] numberOfCharactersInReturnMessage = new Int32[1];
                Int32 functionCallReturnValue = 0;
                StringBuilder responseToCommand = new StringBuilder();
                int numberOfCharactersInCommandMessage;
                string command = string.Empty;
                string response = string.Empty;
                string fullResponse = string.Empty;
                string[] pieces = null;
                bool readIsCorrect = false;
                int retryCount;
               
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (message != null)
                    {
                        pieces = message.Split(';');
                        if (pieces.Length > 0)
                        {
                            pieces = pieces[0].Split(' ');
                            command = pieces[0];
                            if (command.Length > 0)
                            {
                                if (readDelayInMicroseconds >= 0)
                                {
                                    numberOfCharactersInReturnMessage[0] = numberOfResponseCharactersExpected;

                                    if (SelectedCommunicationProtocolIsOpen())
                                    {
                                        retryCount = numberOfRetriesAlreadyAttempted;
                                        commandCharacters = PrepareCommandMessage(message);
                                        numberOfCharactersInCommandMessage = CountCharactersInCommandMessage(commandCharacters);
                                        while (!readIsCorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                        {
                                            functionCallReturnValue = selectedCommunicationProtocol.sendFunction71Command(modBusAddress, commandCharacters, numberOfCharactersInCommandMessage, responseCharacters, numberOfCharactersInReturnMessage, readDelayInMicroseconds);
                                            if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                            {
                                                //errorCodes.Add(functionCallReturnValue);
                                                LogCommunicationErrorMessage("SendStringCommand", functionCallReturnValue);
                                                retryCount++;
                                                RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                                //if (!commandRetryInvokerIsActive)
                                                //{
                                                //    StartCommandRetryInvokerIfConditionsAreMet(command);
                                                //}
                                            }
                                            else
                                            {
                                                commandRetryIsActive = false;

                                                fullResponse = ConvertByteArrayToString(responseCharacters, numberOfCharactersInReturnMessage[0]);
                                                pieces = fullResponse.Split(';');
                                                pieces = pieces[0].Split(' ');
                                                response = pieces[0];
                                                if (command.CompareTo(response) != 0)
                                                {
                                                    retryCount++;
                                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                                }
                                                else
                                                {
                                                    readIsCorrect = true;
                                                }
                                            }
                                        }
                                        if (readIsCorrect)
                                        {
                                            // limit = numberOfCharactersInReturnMessage[0];
                                            if (numberOfCharactersInReturnMessage[0] > numberOfResponseCharactersExpected)
                                            {
                                                responseToCommand.Append("200:Insufficient number of chars in ouput array");
                                                string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nReturn message exceeded the limit of the return array.";
                                                LogMessage.LogError(errorMessage);
#if DEBUG
                                                MessageBox.Show(errorMessage);
#endif
                                            }
                                            else
                                            {
                                                // I don't think you need to do this, as the value should already have been extracted inside the command loop
                                                fullResponse = ConvertByteArrayToString(responseCharacters, numberOfCharactersInReturnMessage[0]);

                                                responseToCommand.Append("0:");
                                                responseToCommand.Append(fullResponse);
                                            }
                                        }
                                        else if (functionCallReturnValue == BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            responseToCommand.Append("200:");
                                            responseToCommand.Append("String Command/Response mismatch");
                                        }
                                        else
                                        {
                                            responseToCommand.Append(functionCallReturnValue.ToString() + ":Command Failed");
                                        }
                                        commandResponse = responseToCommand.ToString();
                                    }
                                    else
                                    {
                                        string errorMessage = "In DeviceCommunication.SendStringCommand(int, string, int, int, int)\nYou need to open a connection to the device before trying to send a command";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nSecond input int needs to be >= 0.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nInput string command was malformed.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nInput string command was malformed.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nInput string was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.SendStringCommand(int, string, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                // WaitForCommandRetryInvokerToFinish();
            }
            return commandResponse;
        }

        /// <summary>
        /// Converts a string command to an array of chars
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static char[] PrepareCommandMessage(string message)
        {
            char[] returnMessage = null;
            try
            {
                // int index;
                int messageLength;
                if (message != null)
                {
                    messageLength = message.Length;
                    // the size isn't critical, it just has to be large enough to contain the command message
                    returnMessage = new char[messageLength + 2];

                    message.CopyTo(0, returnMessage, 0, messageLength);

                    //// we want to add characters to the returnMessage array starting after any characters
                    //// which were added as padding
                    //for (index = 0; index < messageLength; index++)
                    //{
                    //    returnMessage[index] = message[index];
                    //}

                    /// the number of characters in the message get counted elsewhere, and use the null
                    /// to check for the end of the message
                    returnMessage[messageLength] = '\0';
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PrepareCommandMessage(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PrepareCommandMessage(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnMessage;
        }

        /// <summary>
        /// Counts the number of characters in the char[] commandMessage
        /// </summary>
        /// <param name="commandMessage"></param>
        /// <returns></returns>
        public static int CountCharactersInCommandMessage(char[] commandMessage)
        {
            int count = 0;
            try
            {
                while (commandMessage[count] != '\0')
                {
                    count++;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.CountCharactersInCommandMessage(char[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return count;
        }

        public static string ConvertByteArrayToString(byte[] characterArray, int numberOfActiveCharacters)
        {
            string arrayAsString = string.Empty;
            try
            {
                int index;
                StringBuilder arrayStringBuilder = new StringBuilder();
                int limit = characterArray.Length;
                if (numberOfActiveCharacters < limit)
                {
                    limit = numberOfActiveCharacters;
                }
                for (index = 0; index < limit; index++)
                {
                    arrayStringBuilder.Append((char)characterArray[index]);
                }
                arrayAsString = arrayStringBuilder.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ConvertCharArrayToString(byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return arrayAsString;
        }


        /// <summary>
        /// Determines whether a SendStringCommand was successful by examining the returned message
        /// </summary>
        /// <param name="returnedMessage"></param>
        /// <returns></returns>
        public static bool StringCommandWasSuccessful(string returnedMessage)
        {
            bool success = false;
            try
            {
                int errorCode;
                if (returnedMessage != null)
                {
                    errorCode = GetSendStringCommandErrorCode(returnedMessage);
                    if (errorCode == 0)
                    {
                        success = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.StringCommandWasSuccessful(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.StringCommandWasSuccessful(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Extracts the message returnd from a successful SendStringCommand
        /// </summary>
        /// <param name="returnedMessage"></param>
        /// <returns></returns>
        public static string GetSendStringCommandReturnMessage(string returnedMessage)
        {
            string parsedMessage = string.Empty;
            try
            {
                if (returnedMessage != null)
                {
                    if (returnedMessage.Contains(":"))
                    {
                        string[] values = returnedMessage.Split(':');
                        parsedMessage = values[1];
                        values = parsedMessage.Split(';');
                        parsedMessage = values[0];
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetSendStringCommandReturnMessage(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetSendStringCommandReturnMessage(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parsedMessage;
        }

        public static int GetSendStringCommandErrorCode(string returnedMessage)
        {
            int errorCode = -1;
            try
            {
                if (returnedMessage != null)
                {
                    if (returnedMessage.Contains(":"))
                    {
                        string[] values = returnedMessage.Split(':');
                        errorCode = ConversionMethods.ConvertStringToInteger(values[0]);
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.GetSendStringCommandErrorCode(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetSendStringCommandErrorCode(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static long GetDeviceError(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            long error = -1;
            try
            {
                string response = string.Empty;
                string returnMessage = string.Empty;
                string[] pieces = null;

                returnMessage = SendStringCommand(modBusAddress, "ERROR ?;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                if (StringCommandWasSuccessful(returnMessage))
                {
                    response = GetSendStringCommandReturnMessage(returnMessage);
                    pieces = response.Split(' ');
                    if (pieces.Length == 2)
                    {
                        if (pieces[0].CompareTo("ERROR") == 0)
                        {
                            if (!long.TryParse(pieces[1], out error))
                            {
                                error = -3;
                            }
                        }
                        else
                        {
                            error = -4;
                        }
                    }
                    else
                    {
                        error = -5;
                    }
                }
                else
                {
                    error = -2;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetDeviceError(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        public static int GetDeviceType(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int deviceType = -1;
            try
            {
                string response = string.Empty;
                string returnMessage = string.Empty;
                string[] pieces = null;
                string deviceTypeAsString = string.Empty;
                returnMessage = SendStringCommand(modBusAddress, "DEVICETYPE ?;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                if (StringCommandWasSuccessful(returnMessage))
                {
                    response = GetSendStringCommandReturnMessage(returnMessage);
                    pieces = response.Split(' ');
                    if (pieces.Length == 2)
                    {
                        if (pieces[0].CompareTo("DEVICETYPE") == 0)
                        {
                            deviceTypeAsString = pieces[1].Trim();
                            if (deviceTypeAsString.CompareTo("TDM") == 0)
                            {
                                deviceType = 101;
                            }
                            else if (deviceTypeAsString.CompareTo("R1500/6") == 0)
                            {
                                deviceType = 15002;
                            }
                            else if (deviceTypeAsString.CompareTo("R505") == 0)
                            {
                                deviceType = 505;
                            }
                        }
                        else
                        {
                            deviceType = -4;
                        }
                    }
                    else
                    {
                        deviceType = -5;
                    }
                }
                else
                {
                    deviceType = -2;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetDeviceTypeStringCommandVersion(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        public static int DeviceIsBusy(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int busy = -1;
            try
            {
                string response = string.Empty;
                //int deviceType = GetDeviceTypeStringCommandVersion(modBusAddress, readDelayInMicroseconds);
                //if (deviceType != 101)
                //{
                string returnMessage = SendStringCommand(modBusAddress, "BUSY ?;", readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                if (StringCommandWasSuccessful(returnMessage))
                {
                    response = GetSendStringCommandReturnMessage(returnMessage);
                    if (response.ToUpper().CompareTo("BUSY 0") == 0)
                    {
                        busy = 0;
                    }
                    else
                    {
                        busy = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.DeviceIsBusy(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return busy;
        }

        public static Int32[] GetArchivedDataNotDownloadedLimits(int modBusAddress, DateTime lastDataDownloadedDate, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int32[] dataLimits = null;
            try
            {
                string[] pieces;
                string response = string.Empty;
                string commandString;
                string returnMessage;
                int dataLimitsIndex = 0;
                int piecesIndex = 1;
                string entry;

                string convertedDateTime = ConvertDateTimeToMondateFormat(lastDataDownloadedDate);
                if (convertedDateTime.Length == 15)
                {
                    commandString = "MONDATE " + convertedDateTime + ";";
                    returnMessage = SendStringCommand(modBusAddress, commandString, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                    if (StringCommandWasSuccessful(returnMessage))
                    {
                        response = GetSendStringCommandReturnMessage(returnMessage);
                        pieces = response.Split(';');
                        pieces = pieces[0].Split(' ');
                        if (pieces[0].CompareTo("MONDATE") == 0)
                        {
                            dataLimits = new Int32[2];
                            /// This messy code is here because I don't think the string returned by MONDATE is consistent.  This code will handle a split
                            /// string with empty values in it.
                            while ((piecesIndex < pieces.Length) && (dataLimitsIndex < 2))
                            {
                                entry = pieces[piecesIndex];
                                if (entry.Length > 0)
                                {
                                    if (Int32.TryParse(entry, out dataLimits[dataLimitsIndex]))
                                    {
                                        dataLimitsIndex++;
                                    }
                                }
                                piecesIndex++;
                            }

                            if (dataLimitsIndex != 2)
                            {
                                dataLimits = null;
                            }

                            //if (!(Int32.TryParse(pieces[1], out dataLimits[0]) && Int32.TryParse(pieces[2], out dataLimits[1])))
                            //{
                            //    dataLimits = null;
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.GetArchivedDataNotDownloadedLimits(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataLimits;
        }

        public static string ConvertDateTimeToMondateFormat(DateTime inputDateTime)
        {
            string stringifiedDateTime = string.Empty;
            try
            {
                StringBuilder dateTimeStringBuilder = new StringBuilder();
                dateTimeStringBuilder.Append(inputDateTime.Year.ToString());

                if (inputDateTime.Month < 10)
                {
                    dateTimeStringBuilder.Append("0");
                }
                dateTimeStringBuilder.Append(inputDateTime.Month.ToString());

                if (inputDateTime.Day < 10)
                {
                    dateTimeStringBuilder.Append("0");
                }
                dateTimeStringBuilder.Append(inputDateTime.Day.ToString());

                dateTimeStringBuilder.Append(" ");

                if (inputDateTime.Hour < 10)
                {
                    dateTimeStringBuilder.Append("0");
                }
                dateTimeStringBuilder.Append(inputDateTime.Hour.ToString());

                if (inputDateTime.Minute < 10)
                {
                    dateTimeStringBuilder.Append("0");
                }
                dateTimeStringBuilder.Append(inputDateTime.Minute.ToString());

                if (inputDateTime.Second < 10)
                {
                    dateTimeStringBuilder.Append("0");
                }
                dateTimeStringBuilder.Append(inputDateTime.Second.ToString());

                stringifiedDateTime = dateTimeStringBuilder.ToString();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ConvertDateTimeToMondateFormat(DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringifiedDateTime;
        }

        #endregion

        #endregion

        #region Main specific methods

        public static Int16[] Main_GetDeviceSetup(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int offset = 0;
                int registersSaved = 0;
                int registersReturned = 0;
                int registersPerDataRequest = 100;
                int numberOfInitializationRegisters = 200;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;
                            allRegisterValues = new Int16[6100];
                            currentRegisterValues = new Int16[numberOfInitializationRegisters];
                            numberOfEntries = new Int32[1];
                            numberOfEntries[0] = numberOfInitializationRegisters;
                            while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                readIsIncorrect = true;
                                while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupMain(modBusAddress, offset, registersPerDataRequest, currentRegisterValues, numberOfEntries, readDelayInMicroseconds);
                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                    {
                                        LogCommunicationErrorMessage("Main_GetDeviceSetup", functionCallReturnValue);
                                        errorCodes.Add(functionCallReturnValue);
                                        retryCount++;
                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                    }
                                    else
                                    {
                                        readIsIncorrect = false;
                                        errorCodes.Clear();
                                        if (retryCount > 0)
                                        {
                                            string retryMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                            LogMessage.LogError(retryMessage);
                                        }
                                        retryCount = 0;
                                    }
                                    Application.DoEvents();
                                }
                                if (retryCount < totalNumberOfRetriesToAttempt)
                                {
                                    registersReturned = numberOfEntries[0];
                                    Array.Copy(currentRegisterValues, 0, allRegisterValues, registersSaved, registersReturned);
                                    registersSaved += registersReturned;
                                    offset += registersReturned;

                                    if ((registersReturned < registersPerDataRequest) || (registersSaved >= 6000))
                                    {
                                        receivedAllData = true;
                                    }
                                }
                                else
                                {
                                    receivedAllData = true;
                                }

                                Application.DoEvents();
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.Main_GetDeviceSetup(int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                            }
                            else if ((allRegisterValues != null) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                returnedRegisterValues = new Int16[registersSaved];
                                Array.Copy(allRegisterValues, returnedRegisterValues, registersSaved);
                            }
#if DEBUG
                            if (retryCount > 0)
                            {
                                string retryMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                LogMessage.LogError(retryMessage);
                            }
#endif
                        }
                        else
                        {
                            string errorMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int)\nYou need to open a connection to the device before trying to read registers";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.Main_GetDeviceSetup(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedRegisterValues;
        }

        /// <summary>
        /// Gets one chunk of Main configuration data
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="startingOffset"></param>
        /// <param name="registersPerDataRequest"></param>
        /// <returns></returns>
        public static Int16[] Main_GetDeviceSetup(int modBusAddress, int startingOffset, int registersPerDataRequest, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int registersReturned = 0;

                int maximumReturnedRegisters;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (startingOffset >= 0)
                    {
                        if ((registersPerDataRequest > 0) && (registersPerDataRequest <= maximumShortsInARead))
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                maximumReturnedRegisters = registersPerDataRequest + 20;
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;
                                    currentRegisterValues = new Int16[maximumReturnedRegisters];
                                    numberOfEntries = new Int32[1];

                                    readIsIncorrect = true;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        numberOfEntries[0] = maximumReturnedRegisters;
                                        functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupMain(modBusAddress, startingOffset, registersPerDataRequest, currentRegisterValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("Main_GetDeviceSetup", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            readIsIncorrect = false;
                                            registersReturned = numberOfEntries[0];
                                            returnedRegisterValues = new Int16[registersReturned];
                                            Array.Copy(currentRegisterValues, 0, returnedRegisterValues, 0, registersReturned);
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to successfully read one chunk of registers.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                        }
                                        Application.DoEvents();
                                    }

                                    if (retryCount >= totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.Main_GetDeviceSetup(int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nYou need to open a connection to the device before trying to read registers";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nFourth input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nThird input int needs to be between 1 and " + maximumShortsInARead.ToString();
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nSecont input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedRegisterValues;
        }

        public static bool Main_SetDeviceSetup(int modBusAddress, int startingOffset, Int16[] registersToWrite, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = true;
            try
            {
                bool sendHasFailed;
                int retryCount;
                int functionCallReturnValue;
                List<int> errorCodes = new List<int>();
                int numberOfRegistersSent = registersToWrite.Length;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (startingOffset >= 0)
                    {
                        if (registersToWrite != null)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    sendHasFailed = true;
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    while (sendHasFailed && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.setDeviceSetupMain(modBusAddress, startingOffset, registersToWrite, numberOfRegistersSent, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("Main_SetDeviceSetup", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            if (retryCount == totalNumberOfRetriesToAttempt)
                                            {
                                                sendHasFailed = false;
                                                success = false;
                                            }
                                            else
                                            {
                                                Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                            }
                                        }
                                        else
                                        {
                                            sendHasFailed = false;
                                            errorCodes.Clear();
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int) it took " + (retryCount + 1).ToString() + " attempts to write one piece of configuration data.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                            retryCount = 0;
                                        }
                                        Application.DoEvents();
                                    }
                                }
                                else
                                {
                                    string errorMessage = "In DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nYou need to open a connection to the device before trying to write the registers";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nInput Int16[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.Main_SetDeviceSetup(int, int, Int16[], int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Int16[] Main_GetPartOfMeasurement(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int offset = 0;
                int registersSaved = 0;
                int registersReturned = 0;
                int successCount = 0;
                int registersPerDataRequest = 100;
                int numberOfInitializationRegisters = 1050;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                retryCount = numberOfRetriesAlreadyAttempted;
                                allRegisterValues = new Int16[20000];
                                currentRegisterValues = new Int16[numberOfInitializationRegisters];
                                numberOfEntries = new Int32[1];
                                numberOfEntries[0] = numberOfInitializationRegisters;
                                while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    readIsIncorrect = true;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementMain(modBusAddress, offset, recordNumber, currentRegisterValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("Main_GetPartOfMeasurement", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            readIsIncorrect = false;
                                            errorCodes.Clear();
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                            retryCount = 0;
                                        }
                                        Application.DoEvents();
                                    }
                                    if (retryCount < totalNumberOfRetriesToAttempt)
                                    {
                                        registersReturned = numberOfEntries[0];
                                        Array.Copy(currentRegisterValues, 0, allRegisterValues, registersSaved, registersReturned);
                                        registersSaved += registersReturned;

                                        /// TEMPORARY CHANGE, MAY NOT WORK FOR ALL MONITORS
                                        //offset += registersReturned;
                                        offset += 260;
                                        successCount++;
                                        //// TEMPORARY FOR TESTING, THIS WILL NOT WORK IN GENERAL
                                        if ((registersReturned < registersPerDataRequest) || (registersSaved >= 2000))
                                        {
                                            receivedAllData = true;
                                        }
                                    }
                                    else
                                    {
                                        receivedAllData = true;
                                    }

                                    /// TEMPORARY FOR TESTING OF DATA FORMAT
                                    if (successCount > 1)
                                    {
                                        receivedAllData = true;
                                    }

                                    Application.DoEvents();
                                }
                                if (retryCount == totalNumberOfRetriesToAttempt)
                                {
                                    StringBuilder errorMessage = new StringBuilder();
                                    errorMessage.Append("Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nFailed to read the data from the device after ");
                                    errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                    errorMessage.Append(" attempts.\nError codes seen were: ");
                                    foreach (int entry in errorCodes)
                                    {
                                        errorMessage.Append(entry.ToString());
                                        errorMessage.Append("  ");
                                    }
                                }
                                else if ((allRegisterValues != null) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    returnedRegisterValues = new Int16[registersSaved];
                                    Array.Copy(allRegisterValues, returnedRegisterValues, registersSaved);
                                }
#if DEBUG
                                if (retryCount > 0)
                                {
                                    string retryMessage = "In DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                    LogMessage.LogError(retryMessage);
                                }
#endif
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nThird input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedRegisterValues;
        }


        /// <summary>
        /// Get one chunk of measurment data for a main monitor
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <param name="startingOffset"></param>
        /// <returns></returns>
        public static Int16[] Main_GetPartOfMeasurement(int modBusAddress, int recordNumber, int startingOffset, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int numberOfRegistersReturned = 0;
                int currentRegisterValuesCount = 1050;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (startingOffset >= 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    currentRegisterValues = new Int16[currentRegisterValuesCount];
                                    numberOfEntries = new Int32[1];

                                    readIsIncorrect = true;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        numberOfEntries[0] = currentRegisterValuesCount;
                                        functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementMain(modBusAddress, startingOffset, recordNumber, currentRegisterValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("Main_GetPartOfMeasurement", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            numberOfRegistersReturned = numberOfEntries[0];

                                            // If one is just reading the first chunk of data anyway, and not worrying about the data from the devices, one need not copy
                                            // all the other data.  On the other hand, that isn't general, and is not expected behavior from someone not familiar with
                                            // the code innards.  So, I changed it back to being general.  Shipping a little extra data around probabaly won't kill us.

                                            returnedRegisterValues = new Int16[numberOfRegistersReturned];
                                            Array.Copy(currentRegisterValues, 0, returnedRegisterValues, 0, numberOfRegistersReturned);

                                            //returnedRegisterValues = new Int16[260];
                                            //Array.Copy(currentRegisterValues, 0, returnedRegisterValues, 0, 260);

                                            readIsIncorrect = false;
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to successfully read one chunk of bytes.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                        }
                                        Application.DoEvents();
                                    }

                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nFourth input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nThird input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedRegisterValues;
        }

        #endregion

        #region ADM specific methods

        public static Byte[] ADM_GetPartOfMeasurement(int modBusAddress, int recordNumber, int offset, int minimumChunkSize, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] currentByteValues = null;
                bool attemptRead = true;
                int retryCount;
                int bytesReturned = 0;
                int adjustedminimumChunkSize = minimumChunkSize + 50;
                Int32[] numberOfEntries;
                List<int> errorCodes = null;
                int functionCallReturnValue;
                StringBuilder errorMessageStringBuilder;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (offset >= 0)
                        {
                            if (minimumChunkSize > 0)
                            {
                                if (readDelayInMicroseconds >= 0)
                                {
                                    if (SelectedCommunicationProtocolIsOpen())
                                    {
                                        retryCount = numberOfRetriesAlreadyAttempted;

                                        currentByteValues = new Byte[adjustedminimumChunkSize];
                                        numberOfEntries = new Int32[1];
                                        errorCodes = new List<int>();
                                        attemptRead = true;
                                        while (attemptRead && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                        {
                                            /// we have to be careful here because we use this value not only to send the size of the data array but to receive the 
                                            /// amount of data returned... therefore we must reset this value after every read attempt
                                            numberOfEntries[0] = adjustedminimumChunkSize;
                                            functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementADM(modBusAddress, recordNumber, offset, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                            if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                            {
                                                LogCommunicationErrorMessage("ADM_GetPartOfMeasurement", functionCallReturnValue);
                                                errorCodes.Add(functionCallReturnValue);
                                                retryCount++;
                                                RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);

                                                errorMessageStringBuilder = new StringBuilder();
                                                errorMessageStringBuilder.Append("Error returned by function call: error code = " + functionCallReturnValue.ToString());
                                                errorMessageStringBuilder.Append("\n");
                                                if (ftalkErrorCodes.ContainsKey(functionCallReturnValue))
                                                {
                                                    errorMessageStringBuilder.Append("That error translates to " + ftalkErrorCodes[functionCallReturnValue]);
                                                    errorMessageStringBuilder.Append("\n");
                                                }
                                                errorMessageStringBuilder.Append("\n");
                                                errorMessageStringBuilder.Append("Selected Values for variables at the time of the error:\n");
                                                errorMessageStringBuilder.Append("\n");
                                                //errorMessageStringBuilder.Append("Number of chunks already download = " + numberOfRecordsDownloaded.ToString());
                                                //errorMessageStringBuilder.Append("\n");
                                                errorMessageStringBuilder.Append("Value for the offset = " + offset.ToString());
                                                errorMessageStringBuilder.Append("\n");
                                                errorMessageStringBuilder.Append("Select OK to continue attempting download or Cancel to quit");

                                                if (MessageBox.Show(errorMessageStringBuilder.ToString(), "Download error", MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                                                {
                                                    attemptRead = false;
                                                }
                                            }
                                            else
                                            {
                                                attemptRead = false;
                                                errorCodes.Clear();
#if DEBUG
                                                if (retryCount > 0)
                                                {
                                                    string retryMessage = "In DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the byte array";
                                                    LogMessage.LogError(retryMessage);
                                                }
#endif
                                                bytesReturned = numberOfEntries[0];
                                                returnedByteValues = new Byte[bytesReturned];
                                                Array.Copy(currentByteValues, 0, returnedByteValues, 0, bytesReturned);
                                            }
                                            Application.DoEvents();
                                        }

                                        if (retryCount == totalNumberOfRetriesToAttempt)
                                        {
                                            StringBuilder errorMessage = new StringBuilder();
                                            errorMessage.Append("Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nFailed to read the data from the device after ");
                                            errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                            errorMessage.Append(" attempts.\nError codes seen were: ");
                                            foreach (int entry in errorCodes)
                                            {
                                                errorMessage.Append(entry.ToString());
                                                errorMessage.Append("  ");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nYou need to open a connection to the device.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nFifth input in has to be >= 0.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nFourth input in has to be > 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nThird input in has to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nSecond input in has to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ADM_GetPartOfMeasurement(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        public static Byte[] ADM_GetLongValueMeasurments(int modBusAddress, int recordNumber, int startingOffset, int offsetIncrement, int numberOfRecordsToDownload, 
                                                         int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int offset = startingOffset;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int numberOfReturnBytes = 16500;
                int numberOfRecordsDownloaded = 0;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;
                StringBuilder errorMessageStringBuilder;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (startingOffset >= 0)
                        {
                            if (offsetIncrement > 0)
                            {
                                if (numberOfRecordsToDownload > 0)
                                {
                                    if (readDelayInMicroseconds >= 0)
                                    {
                                        if (SelectedCommunicationProtocolIsOpen())
                                        {
                                            retryCount = numberOfRetriesAlreadyAttempted;

                                            allByteValues = new Byte[numberOfReturnBytes * numberOfRecordsToDownload];
                                            currentByteValues = new Byte[numberOfReturnBytes];
                                            numberOfEntries = new Int32[1];
                                            numberOfEntries[0] = numberOfReturnBytes;

                                            if (DeviceCommunication.ftalkErrorCodes == null)
                                            {
                                                FillFtalkErrorCodes();
                                            }

                                            while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                            {
                                                readIsIncorrect = true;
                                                while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                                {
                                                    functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementADM(modBusAddress, recordNumber, offset, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                                    {
                                                        LogCommunicationErrorMessage("ADM_GetLongValueMeasurements", functionCallReturnValue);
                                                        errorCodes.Add(functionCallReturnValue);
                                                        retryCount++;
                                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                        errorMessageStringBuilder = new StringBuilder();
                                                        errorMessageStringBuilder.Append("Error returned by function call: error code = " + functionCallReturnValue.ToString());
                                                        errorMessageStringBuilder.Append("\n");
                                                        if (ftalkErrorCodes.ContainsKey(functionCallReturnValue))
                                                        {
                                                            errorMessageStringBuilder.Append("That error translates to " + ftalkErrorCodes[functionCallReturnValue]);
                                                            errorMessageStringBuilder.Append("\n");
                                                        }
                                                        errorMessageStringBuilder.Append("\n");
                                                        errorMessageStringBuilder.Append("Selected Values for variables at the time of the error:\n");
                                                        errorMessageStringBuilder.Append("\n");
                                                        errorMessageStringBuilder.Append("Number of chunks already download = " + numberOfRecordsDownloaded.ToString());
                                                        errorMessageStringBuilder.Append("\n");
                                                        errorMessageStringBuilder.Append("Value for the offset = " + offset.ToString());

                                                        MessageBox.Show(errorMessageStringBuilder.ToString());
                                                    }
                                                    else
                                                    {
                                                        bytesReturned = numberOfEntries[0];
                                                        readIsIncorrect = false;
                                                        errorCodes.Clear();
                                                        if (retryCount > 0)
                                                        {
                                                            string retryMessage = "In DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                                            LogMessage.LogError(retryMessage);
                                                        }
                                                        retryCount = 0;
                                                    }
                                                    Application.DoEvents();
                                                }
                                                if (retryCount < totalNumberOfRetriesToAttempt)
                                                {
                                                    bytesReturned = numberOfEntries[0];
                                                    Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                                                    bytesSaved += bytesReturned;
                                                    numberOfRecordsDownloaded++;
                                                    offset += bytesReturned;

                                                    //if (numberOfEntries[0] < 1000)
                                                    //{
                                                    //    receivedAllData = true;
                                                    //}
                                                }
                                                else
                                                {
                                                    receivedAllData = true;
                                                }

                                                /// You cannot get more than this many long records
                                                if ((numberOfRecordsDownloaded == numberOfRecordsToDownload) || (numberOfRecordsDownloaded == 128))
                                                {
                                                    receivedAllData = true;
                                                }

                                                Application.DoEvents();
                                            }
                                            if (retryCount == totalNumberOfRetriesToAttempt)
                                            {
                                                StringBuilder errorMessage = new StringBuilder();
                                                errorMessage.Append("Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nFailed to read the data from the device after ");
                                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                                foreach (int entry in errorCodes)
                                                {
                                                    errorMessage.Append(entry.ToString());
                                                    errorMessage.Append("  ");
                                                }
                                            }
                                            else if (DeviceCommunication.monitorDataDownloadIsRunning)
                                            {
                                                returnedByteValues = new Byte[bytesSaved];
                                                Array.Copy(allByteValues, returnedByteValues, bytesSaved);
                                            }
#if DEBUG
                                            //if (retryCount > 0)
                                            //{
                                            //    string retryMessage = "In DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                            //    LogMessage.LogError(retryMessage);
                                            //}
#endif
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nYou need to open a connection to the device.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nSixth input int needs to be >= 0.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nFifth input int needs to be > 0.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nFourth input int needs to be > 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nThird input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                // public static Byte[] ADM_GetLongValueMeasurments(int modBusAddress, int recordNumber, int startingOffset, int offsetIncrement, int numberOfRecordsToDownload, int readDelayInMicroseconds)
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.ADM_GetLongValueMeasurments(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        #endregion

        #region BHM specific methods

        /// <summary>
        /// Balance a BHM using the "BALANCE" string command
        /// </summary>
        /// <returns></returns>
        public static bool BHM_BalanceDevice(int modBusAddress, int readDelayInMicroseconds)
        {
            bool success = false;
            try
            {
                string returnMessage = string.Empty;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            //System.Diagnostics.Debugger.Break();
                            returnMessage = SendStringCommand(modBusAddress, "BALANCE;", readDelayInMicroseconds, 0, 1);
                        }                            
                        else
                        {
                            string errorMessage = "In DeviceCommunication.BHM_BalanceDevice(int, int)\nYou need to open a connection to the device before trying to balance the device";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_BalanceDevice(int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_BalanceDevice(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_BalanceDevice(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Reads the data archive for a BHM module and saves the data to a BHM_DataReading object.  Reads data for both groups 1 and 2, 
        /// depending on if they are active.  Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="monitorID"></param>
        /// <param name="group1IsActive"></param>
        /// <param name="group2IsActive"></param>
        /// <returns></returns>
        public static Int16[] BHM_ReadSingleArchiveRecord(int modBusAddress, short recordNumber, int groupNumber, int readDelayInMicroseconds, 
                                                          int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerData = null;
            try
            {
                int offset = 0;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (groupNumber == 1)
                            {
                                offset = 1051;
                            }
                            else if (groupNumber == 2)
                            {
                                offset = 1082;
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int, short, int, int, int, int)\nIncorrect group number (third int arguement), input value was " + groupNumber.ToString() + ".";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                if (MoveDataSampleIntoTheArchiveRegisters(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt))
                                {
                                    if (offset > 0)
                                    {
                                        registerData = ReadMultipleRegisters(modBusAddress, offset, 31, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                        if (registerData == null)
                                        {
                                            if (DownloadIsEnabled())
                                            {
                                                string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int,  short, int, int, int, int)\nFailed read the register values.";
                                                LogMessage.LogError(errorMessage);
#if DEBUG
                                                MessageBox.Show(errorMessage);
#endif
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int, short, int, int, int, int)\nFailed to move the record into the archives.";
                                    LogMessage.LogError(errorMessage);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int,  short, int, int, int, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int,  short, int, int, int, int)\nFourth input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int,  short, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_ReadSingleArchiveRecord(int, short, int,  int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_ReadSingleArchiveRecord(int, short, int,  int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerData;
        }

        /// <summary>
        /// Determines whether group 1 for a BHM is active by reading the value in register 151.  
        /// Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <returns></returns>
        public static int BHM_Group1IsActive(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int active = -1;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            Int16[] R151 = ReadOneRegister(modBusAddress, 151, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (R151 != null)
                            {
                                if (R151[0] == 1)
                                {
                                    active = 1;
                                }
                                else
                                {
                                    active = 0;
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_Group1IsActive(int, int, int, int)\nFailed to read register 151.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                // MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_Group1IsActive(int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_Group1IsActive(int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_Group1IsActive(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_Group1IsActive(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return active;
        }

        /// <summary>
        /// Determines whether group 2 for a BHM is active by reading the value in register 181.  
        /// Expects that communication with the device has already been established.
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <returns></returns>
        public static int BHM_Group2IsActive(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            int active = -1;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            Int16[] R181 = ReadOneRegister(modBusAddress, 181, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (R181 != null)
                            {
                                if (R181[0] == 1)
                                {
                                    active = 1;
                                }
                                else
                                {
                                    active = 0;
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_Group2IsActive(int, int, int, int)\nFailed to read register 181.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                // MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_Group2IsActive(int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_Group2IsActive(int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_Group2IsActive(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_Group2IsActive(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return active;
        }

        /// <summary>
        /// Function 72 command that gets the data for one measurement
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] BHM_GetPartOfMeasurement(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] byteValuesReturned = null;
            try
            {
                bool readIsIncorrect = true;
                int retryCount;
                int byteArrayInitialSize = 1000;
                int numberOfBytesReturned;
                int[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                retryCount = numberOfRetriesAlreadyAttempted;

                                Byte[] byteValues = new Byte[byteArrayInitialSize];
                                numberOfEntries = new int[1];
                                numberOfEntries[0] = byteArrayInitialSize;
                                while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementBHM(modBusAddress, recordNumber, byteValues, numberOfEntries, readDelayInMicroseconds);
                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                    {
                                        LogCommunicationErrorMessage("BHM_GetPartOfMeasurement", functionCallReturnValue);
                                        errorCodes.Add(functionCallReturnValue);
                                        retryCount++;
                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                    }
                                    else
                                    {
                                        readIsIncorrect = false;
                                        numberOfBytesReturned = numberOfEntries[0];
                                        byteValuesReturned = new Byte[numberOfBytesReturned];
                                        Array.Copy(byteValues, byteValuesReturned, numberOfBytesReturned);
                                    }
                                }
                                if ((retryCount == totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    StringBuilder errorMessage = new StringBuilder();
                                    errorMessage.Append("Error in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nFailed to read the data from the device after ");
                                    errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                    errorMessage.Append(" attempts.\nError codes seen were: ");
                                    foreach (int entry in errorCodes)
                                    {
                                        errorMessage.Append(entry.ToString());
                                        errorMessage.Append("  ");
                                    }
                                }
                               
                                if (retryCount > 0)
                                {
                                    string retryMessage = "In DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                    LogMessage.LogError(retryMessage);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nThird input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValuesReturned;
        }

        /// <summary>
        /// Function 72 command that gets the setup for one BHM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] BHM_GetDeviceSetup(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int offset = 1;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int successCount = 0;
                int bytesPerDataRequest = 200;
                int numberOfInitializationBytes = 300;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;

                            allByteValues = new Byte[2000];
                            currentByteValues = new Byte[numberOfInitializationBytes];
                            numberOfEntries = new Int32[1];
                            // numberOfEntries[0] = numberOfInitializationBytes;
                            numberOfEntries[0] = bytesPerDataRequest;
                            while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                readIsIncorrect = true;
                                while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupBHM(modBusAddress, offset, bytesPerDataRequest, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                    {
                                        LogCommunicationErrorMessage("BHM_GetDeviceSetup", functionCallReturnValue);
                                        errorCodes.Add(functionCallReturnValue);
                                        retryCount++;
                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                    }
                                    else
                                    {
                                        readIsIncorrect = false;
                                        errorCodes.Clear();
                                        if (retryCount > 0)
                                        {
                                            string retryMessage = "In DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                            LogMessage.LogError(retryMessage);
                                        }
                                        retryCount = 0;
                                    }
                                    Application.DoEvents();
                                }
                                if (retryCount < totalNumberOfRetriesToAttempt)
                                {
                                    bytesReturned = numberOfEntries[0];
                                    Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                                    bytesSaved += bytesReturned;
                                    offset += bytesReturned;

                                    if (numberOfEntries[0] < bytesPerDataRequest)
                                    {
                                        receivedAllData = true;
                                    }
                                    successCount++;
                                    // if this happens, there has been some kind of error
                                    if (successCount > 5)
                                    {
                                        receivedAllData = true;
                                        allByteValues = null;
                                    }
                                }
                                else
                                {
                                    receivedAllData = true;
                                }

                                Application.DoEvents();
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                            }
                            else if ((allByteValues != null) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                returnedByteValues = new Byte[bytesSaved];
                                Array.Copy(allByteValues, returnedByteValues, bytesSaved);
                            }
#if DEBUG
                            if (retryCount > 0)
                            {
                                string retryMessage = "In DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                LogMessage.LogError(retryMessage);
                            }
#endif
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Function 72 command that gets the setup for one BHM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] BHM_GetDeviceSetup(int modBusAddress, int offset, int numberOfBytesToRead, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int bytesReturned = 0;
                int numberOfInitializationBytes = 300;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;
                           
                            currentByteValues = new Byte[numberOfInitializationBytes];
                            numberOfEntries = new Int32[1];
                            // numberOfEntries[0] = numberOfInitializationBytes;
                            numberOfEntries[0] = numberOfBytesToRead;
                            while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupBHM(modBusAddress, offset, numberOfBytesToRead, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                {
                                    LogCommunicationErrorMessage("BHM_GetDeviceSetup", functionCallReturnValue);
                                    errorCodes.Add(functionCallReturnValue);
                                    retryCount++;
                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                }
                                else
                                {
                                    readIsIncorrect = false;
                                    bytesReturned = numberOfEntries[0];
                                    returnedByteValues = new byte[bytesReturned];
                                    Array.Copy(currentByteValues, 0, returnedByteValues, 0, bytesReturned);

                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                        LogMessage.LogError(retryMessage);
                                    }
                                }
                                Application.DoEvents();
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                            }
#if DEBUG
                            if (retryCount > 0)
                            {
                                string retryMessage = "In DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                LogMessage.LogError(retryMessage);
                            }
#endif
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_GetDeviceSetup(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Function 72 command that writes the setup data for one BHM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static bool BHM_SetDeviceSetup(int modBusAddress, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds, 
                                              int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool fullSendWorked = false;
            try
            {
                Byte[] currentByteValues;
                bool sendHasFailed = true;
                int retryCount;
                int offset = 1;
                int numberOfBytesSent = 0;
                int numberOfBytesPerFullSend = 200;
                int numberOfBytesToSend;
                int totalBytesAvailable;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool sentAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (bytesToWrite != null)
                    {
                        if (numberOfBytesToWrite > 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                totalBytesAvailable = bytesToWrite.Length;

                                if (numberOfBytesToWrite > totalBytesAvailable)
                                {
                                    string errorMessage = "In DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int),\nThe number of bytes to write is more than can be held in the input array";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    while ((!sentAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        if ((numberOfBytesToWrite - numberOfBytesSent) > numberOfBytesPerFullSend)
                                        {
                                            numberOfBytesToSend = numberOfBytesPerFullSend;
                                        }
                                        else
                                        {
                                            numberOfBytesToSend = numberOfBytesToWrite - numberOfBytesSent;
                                        }
                                        if (numberOfBytesToSend > 0)
                                        {
                                            currentByteValues = new Byte[numberOfBytesToSend];

                                            Array.Copy(bytesToWrite, numberOfBytesSent, currentByteValues, 0, numberOfBytesToSend);

                                            sendHasFailed = true;

                                            while (sendHasFailed && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                            {
                                                functionCallReturnValue = selectedCommunicationProtocol.setDeviceSetupBHM(modBusAddress, offset, currentByteValues, numberOfBytesToSend, readDelayInMicroseconds);
                                                if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                                {
                                                    LogCommunicationErrorMessage("BHM_SetDeviceSetup", functionCallReturnValue);
                                                    errorCodes.Add(functionCallReturnValue);
                                                    retryCount++;
                                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                                }
                                                else
                                                {
                                                    sendHasFailed = false;
                                                    errorCodes.Clear();
                                                    if (retryCount > 0)
                                                    {
                                                        string retryMessage = "In DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int) it took " + (retryCount + 1).ToString() + " attempts to write one piece of configuration data.";
                                                        LogMessage.LogError(retryMessage);
                                                    }
                                                    retryCount = 0;
                                                }
                                                Application.DoEvents();
                                            }
                                            if (retryCount == totalNumberOfRetriesToAttempt)
                                            {
                                                sentAllData = true;
                                            }

                                            // setup for the next send
                                            numberOfBytesSent += numberOfBytesToSend;
                                            offset += numberOfBytesToSend;
                                        }
                                        else
                                        {
                                            sentAllData = true;
                                        }

                                        Application.DoEvents();
                                    }
                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }                                        
                                    }
#if DEBUG
                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                        LogMessage.LogError(retryMessage);
                                    }
#endif
                                    if (numberOfBytesSent >= 998)
                                    {
                                        fullSendWorked = true;
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nSecond input int needs to be > 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nInput Byte[]was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }

     //     public static bool BHM_SetDeviceSetup(int modBusAddress, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds)
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return fullSendWorked;
        }


        /// <summary>
        /// Function 72 command that writes the setup data for one BHM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static bool BHM_SetDeviceSetup(int modBusAddress, int offset, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds,
                                              int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool sendWorked = false;
            try
            {
                bool retryingSend = true;
                int retryCount;
                int numberOfBytesSent = 0;
                int totalBytesAvailable;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (bytesToWrite != null)
                    {
                        if (numberOfBytesToWrite > 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                totalBytesAvailable = bytesToWrite.Length;

                                if (numberOfBytesToWrite > totalBytesAvailable)
                                {
                                    string errorMessage = "In DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int),\nThe number of bytes to write is more than can be held in the input array";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    while (retryingSend && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.setDeviceSetupBHM(modBusAddress, offset, bytesToWrite, numberOfBytesToWrite, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("BHM_SetDeviceSetup", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);

                                        }
                                        else
                                        {
                                            retryingSend = false;
                                            sendWorked = true;
                                        }
                                        Application.DoEvents();
                                    }

                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
#if DEBUG
                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                        LogMessage.LogError(retryMessage);
                                    }
#endif
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nSecond input int needs to be > 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nInput Byte[]was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }

     //     public static bool BHM_SetDeviceSetup(int modBusAddress, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds)
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.BHM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sendWorked;
        }

        #endregion

        #region PDM specific methods

        public static bool PDM_ComputeNumberOfRecordsNotDownloadedYet(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                int deviceType;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            deviceType = GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                            if (deviceType == 505)
                            {
                                int functionCallReturnValue = selectedCommunicationProtocol.writeSingleRegister(modBusAddress, 612, (short)1, readDelayInMicroseconds);
                                if (functionCallReturnValue == BusProtocolErrors.FTALK_SUCCESS)
                                {
                                    success = true;
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nFailed to set register 612 to a value of 1.\nError code was " + functionCallReturnValue.ToString();
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    // MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nIncorrect device type.  Read device type as " + deviceType.ToString();
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static string ConvertBitEncodingToStringRepresentationOfBits(Int16 registerValue)
        {
            StringBuilder bitsAsAString = new StringBuilder();
            int[] powersOfTwo = new int[16] { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
            for (int i = 0; i < 16; i++)
            {
                if ((registerValue & powersOfTwo[i]) != 0)
                {
                    bitsAsAString.Append("1");
                }
                else
                {
                    bitsAsAString.Append("0");
                }
            }
            return bitsAsAString.ToString();
        }

        /// <summary>
        /// Gets a piece of the measurement.  Returns null if it fails for some reason, with the reason passed in the downloadErrorMessage string
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <param name="offset"></param>
        /// <param name="readDelayInMicroseconds"></param>
        /// <param name="downloadErrorMessage"></param>
        /// <returns></returns>
        public static Byte[] PDM_GetPartOfMeasurement(int modBusAddress, int recordNumber, int offset, int readDelayInMicroseconds,
                                                      int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;

                int bytesReturned = 0;
                int numberOfReturnBytes = 1024;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (offset >= 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;
                                    currentByteValues = new Byte[numberOfReturnBytes];
                                    numberOfEntries = new Int32[1];
                                    numberOfEntries[0] = numberOfReturnBytes;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        numberOfEntries[0] = numberOfReturnBytes;
                                        functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementPDM(modBusAddress, recordNumber, offset, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("PDM_GetPartOfMeasurement", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            bytesReturned = numberOfEntries[0];
                                            readIsIncorrect = false;
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.  The read finally succeeded.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                            returnedByteValues = new Byte[bytesReturned];
                                            Array.Copy(currentByteValues, 0, returnedByteValues, 0, bytesReturned);
                                        }
                                        Application.DoEvents();
                                    }
                                    if ((retryCount == totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nFourth input int has to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nThird input int has to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nSecond input int has to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }

                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Gets the common data and all channel data for one measurement, function 72 command
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] PDM_GetCommonAndChannelData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                // int offset = 0;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int numberOfReturnBytes = 1024;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;
                int loopCount = 0;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                retryCount = numberOfRetriesAlreadyAttempted;
                                allByteValues = new Byte[3000];
                                currentByteValues = new Byte[numberOfReturnBytes];
                                numberOfEntries = new Int32[1];
                                numberOfEntries[0] = numberOfReturnBytes;
                                while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    readIsIncorrect = true;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        numberOfEntries[0] = numberOfReturnBytes;
                                        functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementPDM(modBusAddress, recordNumber, bytesSaved, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("PDM_GetCommonAndChannelData", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            bytesReturned = numberOfEntries[0];
                                            readIsIncorrect = false;
                                            errorCodes.Clear();
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                            retryCount = 0;
                                        }
                                        Application.DoEvents();
                                    }
                                    if (retryCount < totalNumberOfRetriesToAttempt)
                                    {
                                        if (loopCount < 2)
                                        {
                                            bytesReturned = numberOfEntries[0];
                                            Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                                            bytesSaved += bytesReturned;
                                        }
                                        else
                                        {
                                            Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, 48);
                                            bytesSaved += 48;
                                        }
                                        if (numberOfEntries[0] < 1000)
                                        {
                                            receivedAllData = true;
                                        }
                                    }
                                    else
                                    {
                                        receivedAllData = true;
                                    }

                                    loopCount++;
                                    if (loopCount == 3)
                                    {
                                        receivedAllData = true;
                                    }
                                    Application.DoEvents();
                                }
                                if ((retryCount == totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    StringBuilder errorMessage = new StringBuilder();
                                    errorMessage.Append("Error in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nFailed to read the data from the device after ");
                                    errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                    errorMessage.Append(" attempts.\nError codes seen were: ");
                                    foreach (int entry in errorCodes)
                                    {
                                        errorMessage.Append(entry.ToString());
                                        errorMessage.Append("  ");
                                    }
                                }
                                else if (DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    if (bytesSaved > 2048)
                                    {
                                        bytesSaved = 2048;
                                    }
                                    returnedByteValues = new Byte[bytesSaved];
                                    Array.Copy(allByteValues, returnedByteValues, bytesSaved);
                                }
#if DEBUG
                                if (retryCount > 0)
                                {
                                    string retryMessage = "In DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int,int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                    LogMessage.LogError(retryMessage);
                                }
#endif
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nThird input int has to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nSecond input int has to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Gets all the phase resolved data for one measurement, function 72 command
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] PDM_GetPhaseResolvedData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] allPhaseResolvedData = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                // int offset = 0;
                int bytesSaved = 0;
                int numberOfReturnBytes = 1024;
                int[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;
                int loopCount = 0;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                retryCount = numberOfRetriesAlreadyAttempted;
                                /// the most we can ever have is 15 matrices, which is 46080 ints
                                allByteValues = new Byte[100000];
                                currentByteValues = new Byte[numberOfReturnBytes];
                                numberOfEntries = new int[1];
                                numberOfEntries[0] = numberOfReturnBytes;
                                while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    // offset = loopCount * 1000 + startingOffset;
                                    readIsIncorrect = true;
                                    while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        numberOfEntries[0] = numberOfReturnBytes;
                                        functionCallReturnValue = selectedCommunicationProtocol.getPartOfMeasurementPDM(modBusAddress, recordNumber, bytesSaved + 2048, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("PDM_GetPhaseResolvedData", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            readIsIncorrect = false;
                                            errorCodes.Clear();
                                            if (retryCount > 0)
                                            {
                                                string retryMessage = "In DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers before succeeding.";
                                                LogMessage.LogError(retryMessage);
                                            }
                                            retryCount = 0;
                                        }
                                        Application.DoEvents();
                                    }
                                    if (retryCount < totalNumberOfRetriesToAttempt)
                                    {
                                        Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, numberOfEntries[0]);
                                        bytesSaved += numberOfEntries[0];
                                        if (numberOfEntries[0] < 1000)
                                        {
                                            receivedAllData = true;
                                        }
                                    }
                                    else
                                    {
                                        receivedAllData = true;
                                    }

                                    loopCount++;
                                    if (loopCount == 93)
                                    {
                                        receivedAllData = true;
                                    }
                                    Application.DoEvents();
                                }
                                if ((retryCount == totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    StringBuilder errorMessage = new StringBuilder();
                                    errorMessage.Append("Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nFailed to read the data from the device after ");
                                    errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                    errorMessage.Append(" attempts.\nError codes seen were: ");
                                    foreach (int entry in errorCodes)
                                    {
                                        errorMessage.Append(entry.ToString());
                                        errorMessage.Append("  ");
                                    }
                                }
                                else
                                {
                                    if ((bytesSaved % 6144) > 0)
                                    {
                                        string errorMessage = "Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nNumber of bytes read was incorrect, this implies a read failure of some kind";
                                        LogMessage.LogError(errorMessage);
                                    }
                                    else if (DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        allPhaseResolvedData = new Byte[bytesSaved];
                                        Array.Copy(allByteValues, allPhaseResolvedData, bytesSaved);
                                    }
                                }
#if DEBUG
                                if (retryCount > 0)
                                {
                                    string retryMessage = "In DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                    LogMessage.LogError(retryMessage);
                                }
#endif
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nThird input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nSecond input int needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allPhaseResolvedData;
        }


        /// <summary>
        /// Function 72 command that gets the setup for one PDM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] PDM_GetDeviceSetup(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] allByteValues = null;
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int offset = 0;
                int bytesSaved = 0;
                int bytesReturned = 0;
                int totalAttempts = 0;
                int numberOfInitializationBytes = 1100;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool receivedAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;
                            allByteValues = new Byte[3300];
                            currentByteValues = new Byte[numberOfInitializationBytes];
                            numberOfEntries = new Int32[1];
                            numberOfEntries[0] = numberOfInitializationBytes;
                            while ((!receivedAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                readIsIncorrect = true;
                                while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                {
                                    functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupPDM(modBusAddress, offset, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                    if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                    {
                                        LogCommunicationErrorMessage("PDM_GetDeviceSetup", functionCallReturnValue);
                                        retryCount++;
                                        RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                        Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                    }
                                    else
                                    {
                                        readIsIncorrect = false;
                                        errorCodes.Clear();
                                        if (retryCount > 0)
                                        {
                                            string retryMessage = "In DeviceCommunication.PDM_GetDeviceSetup(int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                            LogMessage.LogError(retryMessage);
                                        }
                                        retryCount = 0;
                                    }
                                    Application.DoEvents();
                                }
                                if (retryCount < totalNumberOfRetriesToAttempt)
                                {
                                    bytesReturned = numberOfEntries[0];
                                    Array.Copy(currentByteValues, 0, allByteValues, bytesSaved, bytesReturned);
                                    bytesSaved += bytesReturned;
                                    offset += bytesReturned;

                                    if (bytesReturned < 1000)
                                    {
                                        receivedAllData = true;
                                    }

                                    totalAttempts++;
                                    if (totalAttempts > 3)
                                    {
                                        receivedAllData = true;
                                        allByteValues = null;
                                    }
                                }
                                else
                                {
                                    receivedAllData = true;
                                }

                                Application.DoEvents();
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.PDM_GetDeviceSetup(int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                            }
                            else if ((allByteValues != null) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                returnedByteValues = new Byte[bytesSaved];
                                Array.Copy(allByteValues, returnedByteValues, bytesSaved);
                            }
#if DEBUG
                            if (retryCount > 0)
                            {
                                string retryMessage = "In DeviceCommunication.PDM_GetDeviceSetup(int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                LogMessage.LogError(retryMessage);
                            }
#endif
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_GetDeviceSetup(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Function 72 command that gets the setup for one BHM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static Byte[] PDM_GetDeviceSetup(int modBusAddress, int offset, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Byte[] returnedByteValues = null;
            try
            {
                Byte[] currentByteValues = null;
                bool readIsIncorrect = true;
                int retryCount;
                int bytesReturned = 0;
                int numberOfInitializationBytes = 1100;
                Int32[] numberOfEntries;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            retryCount = numberOfRetriesAlreadyAttempted;

                            currentByteValues = new Byte[numberOfInitializationBytes];
                            numberOfEntries = new Int32[1];
                            numberOfEntries[0] = numberOfInitializationBytes;
                            while (readIsIncorrect && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                            {
                                functionCallReturnValue = selectedCommunicationProtocol.getDeviceSetupPDM(modBusAddress, offset, currentByteValues, numberOfEntries, readDelayInMicroseconds);
                                if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                {
                                    LogCommunicationErrorMessage("PDM_GetDeviceSetup", functionCallReturnValue);
                                    errorCodes.Add(functionCallReturnValue);
                                    retryCount++;
                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                }
                                else
                                {
                                    readIsIncorrect = false;
                                    bytesReturned = numberOfEntries[0];
                                    returnedByteValues = new byte[bytesReturned];
                                    Array.Copy(currentByteValues, 0, returnedByteValues, 0, bytesReturned);

                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                        LogMessage.LogError(retryMessage);
                                    }
                                }
                                Application.DoEvents();
                            }
                            if (retryCount == totalNumberOfRetriesToAttempt)
                            {
                                StringBuilder errorMessage = new StringBuilder();
                                errorMessage.Append("Error in DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int)\nFailed to read the data from the device after ");
                                errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                errorMessage.Append(" attempts.\nError codes seen were: ");
                                foreach (int entry in errorCodes)
                                {
                                    errorMessage.Append(entry.ToString());
                                    errorMessage.Append("  ");
                                }
                            }
#if DEBUG
                            if (retryCount > 0)
                            {
                                string retryMessage = "In DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read one chunk of bytes.";
                                LogMessage.LogError(retryMessage);
                            }
#endif
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int)\nYou need to open a connection to the device.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int)\nSecond input int has to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_GetDeviceSetup(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return returnedByteValues;
        }

        /// <summary>
        /// Function 72 command that writes the setup data for one BHM, 0x7010
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static bool PDM_SetDeviceSetup(int modBusAddress, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds, 
                                              int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = true;
            try
            {
                Byte[] currentByteValues;
                bool retrySend = true;
                int retryCount;
                int offset = 0;
                int numberOfBytesSent = 0;
                int numberOfBytesPerFullSend = 200;
                int numberOfBytesToSend;
                int totalBytesAvailable = bytesToWrite.Length;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;
                bool sentAllData = false;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (bytesToWrite != null)
                    {
                        if (numberOfBytesToWrite > 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {

                                if (numberOfBytesToWrite > totalBytesAvailable)
                                {
                                    string errorMessage = "In DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int),\nThe number of bytes to write is more than can be held in the input array";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    while ((!sentAllData) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        if ((numberOfBytesToWrite - numberOfBytesSent) > numberOfBytesPerFullSend)
                                        {
                                            numberOfBytesToSend = numberOfBytesPerFullSend;
                                        }
                                        else
                                        {
                                            numberOfBytesToSend = numberOfBytesToWrite - numberOfBytesSent;
                                        }
                                        if (numberOfBytesToSend > 0)
                                        {
                                            currentByteValues = new Byte[numberOfBytesToSend];

                                            Array.Copy(bytesToWrite, numberOfBytesSent, currentByteValues, 0, numberOfBytesToSend);

                                            retrySend = true;

                                            while (retrySend && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                            {
                                                functionCallReturnValue = selectedCommunicationProtocol.setDeviceSetupPDM(modBusAddress, offset, currentByteValues, numberOfBytesToSend, readDelayInMicroseconds);
                                                if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                                {
                                                    LogCommunicationErrorMessage("PDM_SetDeviceSetup", functionCallReturnValue);
                                                    errorCodes.Add(functionCallReturnValue);
                                                    retryCount++;
                                                    RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                                    Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                                }
                                                else
                                                {
                                                    retrySend = false;
                                                    errorCodes.Clear();
                                                    if (retryCount > 0)
                                                    {
                                                        string retryMessage = "In DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int) it took " + (retryCount + 1).ToString() + " attempts to write one piece of configuration data.";
                                                        LogMessage.LogError(retryMessage);
                                                    }
                                                    retryCount = 0;
                                                }
                                                Application.DoEvents();
                                            }
                                            if (retryCount == totalNumberOfRetriesToAttempt)
                                            {
                                                sentAllData = true;
                                            }

                                            // setup for the next send
                                            numberOfBytesSent += numberOfBytesToSend;
                                            offset += numberOfBytesToSend;
                                        }
                                        else
                                        {
                                            sentAllData = true;
                                        }

                                        Application.DoEvents();
                                    }

                                    if (retryCount < totalNumberOfRetriesToAttempt)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.resetSetupParamPDM(modBusAddress, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            string retryMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int):\n call to resetSetupParamPDM(int) failed, device configuration probably not updated.";
                                            LogMessage.LogError(retryMessage);
                                            success = false;
                                        }
                                    }

                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nFailed to write the data to the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                        success = false;
                                    }
#if DEBUG
                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                        LogMessage.LogError(retryMessage);
                                    }
#endif
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nSecond input int needs to be > 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Function 72 command that writes one block of setup data for a PDM
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="recordNumber"></param>
        /// <returns></returns>
        public static bool PDM_SetDeviceSetup(int modBusAddress, int offset, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds,
                                              int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                bool retrySend = true;
                int retryCount;
                int totalBytesAvailable;
                List<int> errorCodes = new List<int>();
                int functionCallReturnValue;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (bytesToWrite != null)
                    {
                        if (numberOfBytesToWrite > 0)
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                totalBytesAvailable = bytesToWrite.Length;

                                if (numberOfBytesToWrite > totalBytesAvailable)
                                {
                                    string errorMessage = "In DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int),\nThe number of bytes to write is more than can be held in the input array";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    retryCount = numberOfRetriesAlreadyAttempted;

                                    while (retrySend && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.monitorDataDownloadIsRunning)
                                    {
                                        functionCallReturnValue = selectedCommunicationProtocol.setDeviceSetupPDM(modBusAddress, offset, bytesToWrite, numberOfBytesToWrite, readDelayInMicroseconds);
                                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                                        {
                                            LogCommunicationErrorMessage("PDM_SetDeviceSetup", functionCallReturnValue);
                                            errorCodes.Add(functionCallReturnValue);
                                            retryCount++;
                                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                                            Thread.Sleep(DEVICE_AFTER_FAILURE_WAIT_INTERVAL);
                                        }
                                        else
                                        {
                                            retrySend = false;
                                            success = true;
                                        }
                                        Application.DoEvents();
                                    }

                                    if (retryCount == totalNumberOfRetriesToAttempt)
                                    {
                                        StringBuilder errorMessage = new StringBuilder();
                                        errorMessage.Append("Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nFailed to read the data from the device after ");
                                        errorMessage.Append((totalNumberOfRetriesToAttempt - numberOfRetriesAlreadyAttempted).ToString());
                                        errorMessage.Append(" attempts.\nError codes seen were: ");
                                        foreach (int entry in errorCodes)
                                        {
                                            errorMessage.Append(entry.ToString());
                                            errorMessage.Append("  ");
                                        }
                                    }
#if DEBUG
                                    if (retryCount > 0)
                                    {
                                        string retryMessage = "In DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int) it took " + (retryCount + 1).ToString() + " attempts to read the registers.";
                                        LogMessage.LogError(retryMessage);
                                    }
#endif
                                   
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nThird input int needs to be >= 0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nSecond input int needs to be > 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nInput Byte[]was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }

     //     public static bool PDM_SetDeviceSetup(int modBusAddress, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds)
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_SetDeviceSetup(int, int, Byte[], int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool PDM_ResetSetupParameters(int modBusAddress, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool resetSuccessful = false;
            try
            {
                int functionCallReturnValue = 0;
                int retryCount = 0;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    while (!resetSuccessful && (retryCount < totalNumberOfRetriesToAttempt) && DeviceCommunication.DownloadIsEnabled())
                    {
                        functionCallReturnValue = selectedCommunicationProtocol.resetSetupParamPDM(modBusAddress, readDelayInMicroseconds);
                        if (functionCallReturnValue != BusProtocolErrors.FTALK_SUCCESS)
                        {
                            LogCommunicationErrorMessage("PDM_ResetSetupParameters", functionCallReturnValue);
                            retryCount++;
                            RetryWindow.UpdateProgressBar(retryCount, totalNumberOfRetriesToAttempt);
                        }
                        else
                        {
                            resetSuccessful = true;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_ResetSetupParamters(int, int, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_ResetSetupParamters(int, int,int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return resetSuccessful;
        }


        public static Int16[] PDM_ReadArchivedCommonData(int modBusAddress, short recordNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerData = null;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if (readDelayInMicroseconds >= 0)
                        {
                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                if (MoveDataSampleIntoTheArchiveRegisters(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt))
                                {
                                    registerData = ReadMultipleRegisters(modBusAddress, 4001, 31, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                    if (registerData == null)
                                    {
                                        if (DownloadIsEnabled())
                                        {
                                            string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nFailed to read the archived record.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nFailed to move the data into the archive.";
                                    LogMessage.LogError(errorMessage);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nSecond input int needs to be >= 0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nInput short needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_ReadArchivedCommonData(int, short, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerData;
        }

        /// <summary>
        /// Reads all channel data associated with a single archived PDM_CommonData reading
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="commonDataID"></param>
        /// <returns></returns>
        public static Int16[] PDM_ReadArchivedChannelDataForOneChannel(int modBusAddress, short recordNumber, int channelNumber, int readDelayInMicroseconds, 
                                                                       int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerData = null;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if ((channelNumber > 0) && (channelNumber < 16))
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    if (MoveDataSampleIntoTheArchiveRegisters(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt))
                                    {
                                        int offset = 4041 + ((channelNumber - 1) * 30);
                                        registerData = ReadMultipleRegisters(modBusAddress, offset, 30, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                        if (registerData == null)
                                        {
                                            if (DownloadIsEnabled())
                                            {
                                                string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nFailed to read the archived record for channel " + channelNumber.ToString();
                                                LogMessage.LogError(errorMessage);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nFailed to move the data into the archive.";
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nThird input int needs to be >=0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nIncorrect channel number was input as " + channelNumber.ToString();
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nInput short needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_ReadArchivedChannelDataForOneArchivedCommonDataReading(int, short, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerData;
        }

        public static Int16[] PDM_ReadArchivedMatrixDataForOneChannel(int modBusAddress, short recordNumber, int channelNumber, int readDelayInMicroseconds, 
                                                                      int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            Int16[] registerData = null;
            try
            {
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (recordNumber > 0)
                    {
                        if ((channelNumber > 0) && (channelNumber < 16))
                        {
                            if (readDelayInMicroseconds >= 0)
                            {
                                if (SelectedCommunicationProtocolIsOpen())
                                {
                                    if (MoveDataSampleIntoTheArchiveRegisters(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt))
                                    {
                                        if (PDM_MoveMatrixDataIntoArchivesForOneChannel(modBusAddress, (Int16)channelNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt))
                                        {
                                            registerData = ReadMultipleRegisters(modBusAddress, 5001, 3072, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);

                                            if (registerData == null)
                                            {
                                                if (DownloadIsEnabled())
                                                {
                                                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nFailed to read the archived record.";
                                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                                // MessageBox.Show(errorMessage);
#endif
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nFailed to move the matrix data to the archives.";
                                            LogMessage.LogError(errorMessage);
                                        }
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nYou need to open a connection to the device.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nThird input int needs to be >=0.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nIncorrect channel number was input as " + channelNumber.ToString();
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nInput short needs to be > 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, short, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerData;
        }

        /// <summary>
        /// Writes the PDM channel number to register 621, which moves the phase resolved data for that channel to the archives.  The archive record number must
        /// be set by MoveDataSampleIntoTheArchiveRegisters(int, int).
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="channelNumber"></param>
        /// <returns></returns>
        public static bool PDM_MoveMatrixDataIntoArchivesForOneChannel(int modBusAddress, Int16 channelNumber, int readDelayInMicroseconds, int numberOfRetriesAlreadyAttempted, int totalNumberOfRetriesToAttempt)
        {
            bool success = false;
            try
            {
                int deviceType;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if ((channelNumber > 0) && (channelNumber < 16))
                    {
                        if (readDelayInMicroseconds >= 0)
                        {

                            if (SelectedCommunicationProtocolIsOpen())
                            {
                                deviceType = GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                if (deviceType == 505)
                                {
                                    success = WriteSingleRegister(modBusAddress, 621, channelNumber, readDelayInMicroseconds, numberOfRetriesAlreadyAttempted, totalNumberOfRetriesToAttempt);
                                    if (!success)
                                    {
                                        string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nFailed to move the matricies into the archives.";
                                        LogMessage.LogError(errorMessage);
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nDevice type is incorrect.  Expected device 505, but got device " + deviceType.ToString();
                                    LogMessage.LogError(errorMessage);
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nYou need to open a connection to the device.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nSecond input int needs to be >=0.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nIncorrect channel number was input as " + channelNumber.ToString();
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, Int16, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Sends the "RUNINITIAL" command to a PD monitor
        /// </summary>
        /// <param name="modBusAddress"></param>
        /// <param name="readDelayInMicroseconds"></param>
        /// <returns></returns>
        public static bool PDM_RunInitial(int modBusAddress, int channelNumber, int readDelayInMicroseconds)
        {
            bool success = false;
            try
            {
                string returnMessage = string.Empty;
                string command = "RUNINITIAL " + channelNumber.ToString() + ";";
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (readDelayInMicroseconds >= 0)
                    {
                        if (SelectedCommunicationProtocolIsOpen())
                        {
                            returnMessage = SendStringCommand(modBusAddress, command, readDelayInMicroseconds, 0, 1);
                        }
                        else
                        {
                            string errorMessage = "In DeviceCommunication.PDM_RunInitial(int, int)\nYou need to open a connection to the device before trying to balance the device";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DeviceCommunication.PDM_RunInitial(int, int)\nSecond input int needs to be >= 0.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DeviceCommunication.PDM_RunInitial(int, int)\nFirst input int needs to be between 1 and 255.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceCommunication.PDM_RunInitial(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion
    }

   

}
