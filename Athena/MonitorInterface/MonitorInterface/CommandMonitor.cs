﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

using Telerik.WinControls;

using GeneralUtilities;

namespace MonitorInterface
{
    public partial class CommandMonitor : Telerik.WinControls.UI.RadForm
    {
        int threadSleepIntervalInMicroseconds = 200;
        int devicePollingIntervalInSeconds = 10;

        bool continueMonitoringCommand;

        private static BackgroundWorker commandMonitorBackgroundWorker;

        private CommandInputOutputObject inputCommandInputOutputObject;

        private CommandInputOutputObject outputCommandInputOutputObject;
        public CommandInputOutputObject OutputCommandInputOutputObject
        {
            get
            {
                return outputCommandInputOutputObject;
            }
        }

        public static bool isInteractive = false;

        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string startMeasurmentText = "Start measurement";
        private static string bhmBalanceDeviceText = "Balance bushing monitor";
        private static string deleteDeviceDataText = "Delete device data";
        private static string runInitialText = "Calibrate selected PDM channel";

        private static string commandNotSpecifiedText = "Command was not specified";

        private static string commandMonitorText = "Command Monitor";
        private static string stopMonitoringRadButtonText = "Stop monitoring";
        private static string commandTextRadLabelText = "Command";
        private static string waitingForReplyRadLabelText = "Waiting for the device to finish";
        private static string stopMonitoringWillNotStopCommandTextRadLabelText = "<html>Even if you stop monitoring, the command<br>has been sent and won't be cancelled</html>";

        //private static string commandFailedText = "Command failed";
        //private static string retryingCommandPrefixText = "Retrying command: ";
        //private static string cancellingCommandPrefixText = "Cancelling command: ";
        private static string monitoringRadWaitingBarText = "Monitoring device ...";

        private static string monitoringStoppedText = "<html>Montoring stopped.  It may take<br>a few seconds for the window to close.</html>";
        private static string commandAssumedToBeFinishedText = "Device is not busy so the command is assumed to be finished";

        private ParentWindowInformation parentWindowInformation;

        public CommandMonitor(CommandInputOutputObject argInputCommandInputOutputObject, ParentWindowInformation argParentWindowInformation)
        {
            try
            {
                InitializeComponent();
                InitializeCommandMonitorBackgroundWorker();

                this.inputCommandInputOutputObject = argInputCommandInputOutputObject;
                this.parentWindowInformation = argParentWindowInformation;

                commandValueTextRadLabel.Text = GetStringVersionOfCommand(inputCommandInputOutputObject.commandName);
                //commandTextRadLabel.Text = retryingCommandPrefixText + GetStringVersionOfCommand(inputCommandInputOutputObject.commandName);
                monitoringRadWaitingBar.Text = monitoringRadWaitingBarText;
                monitoringRadWaitingBar.Visible = true;
                monitoringRadWaitingBar.StartWaiting();

                //this.StartPosition = FormStartPosition.CenterParent;
                this.StartPosition = FormStartPosition.Manual;
                this.Location = this.parentWindowInformation.AssignLocation();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.CommandMonitor(CommandInputOutputObject, ParentWindowInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CommandMonitor_Load(object sender, EventArgs e)
        {
            try
            {
                AssignStringValuesToInterfaceObjects();
                commandMonitorBackgroundWorker.RunWorkerAsync(inputCommandInputOutputObject);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.CommandMonitor_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void cancelMonitorRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                continueMonitoringCommand = false;
                monitoringRadWaitingBar.Visible = false;
                waitingForReplyRadLabel.Text = monitoringStoppedText;
                commandMonitorBackgroundWorker.CancelAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.cancelMonitorRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void UpdateProgressBar(int retryCount, int totalTries)
        {
            try
            {
                if (isInteractive)
                {
                    if ((retryCount >= 0) && (retryCount <= totalTries))
                    {
                        commandMonitorBackgroundWorker.ReportProgress((retryCount * 100) / totalTries);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCommandMonitorBackgroundWorker()
        {
            try
            {
                /// Set up the download background worker
                commandMonitorBackgroundWorker = new BackgroundWorker()
                {
                    WorkerSupportsCancellation = true,
                    WorkerReportsProgress = true
                };

                commandMonitorBackgroundWorker.DoWork += commandMonitorBackgroundWorker_DoWork;
                commandMonitorBackgroundWorker.ProgressChanged += commandMonitorBackgroundWorker_ProgressChanged;
                commandMonitorBackgroundWorker.RunWorkerCompleted += commandMonitorBackgroundWorker_RunWorkerCompleted;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandMonitor.InitializeCommandMonitorBackgroundWorker()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void commandMonitorBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bool commandHasFinised = false;
                bool minimumTimeForCommandHasElapsed = false;
                int deviceIsBusy = 0;

                DateTime lastPollDateTime = DateTime.Now;

                CommandInputOutputObject inputArgument = e.Argument as CommandInputOutputObject;
                CommandInputOutputObject outputArgument = new CommandInputOutputObject();

                CommandName commandName = inputArgument.commandName;

                bool commandExisted = true;

                int modBusAddress = inputArgument.modBusAddress;
                //int offset = inputArgument.offset;
                //int recordNumber = inputArgument.recordNumber;
                //int groupNumber = inputArgument.groupNumber;
                int channelNumber = inputArgument.channelNumber;
                //int numberOfItemsToRead = inputArgument.numberOfItemsToRead;
                //int numberOfItemsToWrite = inputArgument.numberOfItemsToWrite;
                int readDelayInMicroseconds = inputArgument.readDelayInMicroseconds;
                int estimatedCommandCompletionTimeInSeconds = inputArgument.estimatedCommandCompletionTimeInSeconds;
                //int numberOfRetryAttemptsCompleted = inputArgument.numberOfRetriesCompleted;
                //int totalNumberOfRetriesToAttempt = inputArgument.totalNumberOfRetriesToAttempt;
                //string inputString = inputArgument.inputString;
                //DateTime inputDateTime = inputArgument.inputDateTime;

                //Byte[] inputBytes = inputArgument.inputBytes;
                //Int16 inputRegister = inputArgument.inputRegister;
                //Int16[] inputRegisters = inputArgument.inputRegisters;

                continueMonitoringCommand = true;

                outputCommandInputOutputObject = null;

                CommandMonitor.isInteractive = true;

                // UpdateProgressBar(numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);

                if (commandName == CommandName.StartMeasurement)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.StartMeasurement(modBusAddress, readDelayInMicroseconds);
                }
                else if (commandName == CommandName.ClearDeviceData)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.ClearDeviceData(modBusAddress, readDelayInMicroseconds, 0, 1);
                }
                else if (commandName == CommandName.BHMBalanceDevice)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.BHM_BalanceDevice(modBusAddress, readDelayInMicroseconds);
                }
                else if (commandName == CommandName.PDMRunInitial)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PDM_RunInitial(modBusAddress, channelNumber, readDelayInMicroseconds);
                }
                else
                {
                    commandExisted = false;

                    string errorMessage = "Error in CommandRetry.commandMonitorBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nCommand name was not specified, no command executed.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                if (commandExisted)
                {
                    //  now we do a busy loop of fun!!
                    while (continueMonitoringCommand && (!commandHasFinised) && (!commandMonitorBackgroundWorker.CancellationPending))
                    {
                        Thread.Sleep(threadSleepIntervalInMicroseconds);

                        if (!minimumTimeForCommandHasElapsed && (lastPollDateTime.CompareTo(DateTime.Now.AddSeconds(-estimatedCommandCompletionTimeInSeconds)) < 0))
                        {
                            minimumTimeForCommandHasElapsed = true;
                        }

                        if (minimumTimeForCommandHasElapsed)
                        {
                            if (lastPollDateTime.CompareTo(DateTime.Now.AddSeconds(-devicePollingIntervalInSeconds)) < 0)
                            {
                                lastPollDateTime = DateTime.Now;
                                deviceIsBusy = DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds, 0, 1);
                                if (deviceIsBusy == -1)
                                {
                                    //MessageBox.Show("Device didn't respond to the DeviceIsBusy command");
                                }
                                else if (deviceIsBusy == 0)
                                {
                                    commandHasFinised = true;
                                    commandMonitorBackgroundWorker.ReportProgress(1);
                                    /// I used to display a RadMessageBox here, but moved that to the RunWorkerCompleted code.
                                    outputArgument.returnedString = commandAssumedToBeFinishedText;
                                }
                                else if (deviceIsBusy == 1)
                                {
                                    //MessageBox.Show("device said it's busy");
                                }
                            }
                        }
                    }
                    e.Result = outputArgument;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                isInteractive = false;
            }
        }

        private void commandMonitorBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                int progress = e.ProgressPercentage;

                if (progress == 1)
                {
                    monitoringRadWaitingBar.Visible = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void commandMonitorBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                //else if (e.Cancelled)
                //{

                //}
                //else
                //{

                //}

                if (e.Result != null)
                {
                    outputCommandInputOutputObject = e.Result as CommandInputOutputObject;
                    if (outputCommandInputOutputObject != null)
                    {
                        if ((outputCommandInputOutputObject.returnedString != null)&&(outputCommandInputOutputObject.returnedString.Length > 0))
                        {
                            RadMessageBox.Show(this, outputCommandInputOutputObject.returnedString);
                        }
                    }
                }
                else
                {
                    outputCommandInputOutputObject = null;
                    string errorMessage = "Error in CommandRetry.commandMonitorBackgroundWorker_DoWork(object, DoWorkEventArgs)\nResult from DoWork was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandMonitorBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.Close();
            }
        }

        private string GetStringVersionOfCommand(CommandName commandName)
        {
            string stringVersionOfCommand = string.Empty;
            try
            {
                if (commandName == CommandName.StartMeasurement)
                {
                    stringVersionOfCommand = startMeasurmentText;
                }
                else if (commandName == CommandName.BHMBalanceDevice)
                {
                    stringVersionOfCommand = bhmBalanceDeviceText;
                }
                else if (commandName == CommandName.ClearDeviceData)
                {
                    stringVersionOfCommand = deleteDeviceDataText;
                }
                else if (commandName == CommandName.PDMRunInitial)
                {
                    stringVersionOfCommand = runInitialText;
                }
                else
                {
                    stringVersionOfCommand = commandNotSpecifiedText;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.GetStringVersionOfCommand(CommandName)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return stringVersionOfCommand;
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = commandMonitorText;
                stopMonitoringRadButton.Text = stopMonitoringRadButtonText;

                commandTextRadLabel.Text = commandTextRadLabelText;
                waitingForReplyRadLabel.Text = waitingForReplyRadLabelText;
                stopMonitoringWillNotStopCommandTextRadLabel.Text = stopMonitoringWillNotStopCommandTextRadLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                startMeasurmentText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceStartMeasurmentText", startMeasurmentText, "", "", "");
                bhmBalanceDeviceText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceBhmBalanceDeviceText", bhmBalanceDeviceText, "", "", "");
                deleteDeviceDataText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceDeleteDeviceDataText", deleteDeviceDataText, "", "", "");
                runInitialText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceRunInitialText", runInitialText, "", "", "");
                commandNotSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceCommandNotSpecifiedText", commandNotSpecifiedText, "", "", "");
                monitoringRadWaitingBarText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceMonitoringRadWaitingBarText", monitoringRadWaitingBarText, "", "", "");
                commandMonitorText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceCommandMonitorText", commandMonitorText, "", "", "");
                stopMonitoringRadButtonText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceStopMonitoringRadButtonText", stopMonitoringRadButtonText, "", "", "");
                commandTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceCommandTextRadLabelText", commandTextRadLabelText, "", "", "");
                waitingForReplyRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceWaitingForReplyRadLabelText", waitingForReplyRadLabelText, "", "", "");
                stopMonitoringWillNotStopCommandTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceStopMonitoringWillNotStopCommandTextRadLabelText", stopMonitoringWillNotStopCommandTextRadLabelText, htmlFontType, htmlStandardFontSize, "");

                monitoringStoppedText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceMonitoringStoppedText", monitoringStoppedText, htmlFontType, htmlStandardFontSize, "");
                commandAssumedToBeFinishedText = LanguageConversion.GetStringAssociatedWithTag("CommandMonitorInterfaceCommandAssumedToBeFinishedText", commandAssumedToBeFinishedText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
}
