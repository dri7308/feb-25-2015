﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using GeneralUtilities;

namespace MonitorInterface
{
    public class InteractiveDeviceCommunication
    {
        //        private static int GetnumberOfNonInteractiveRetriesToAttempt(int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        //        {
        //            int numberOfNonInteractiveRetries = totalNumberOfRetriesToAttempt;
        //            try
        //            {
        //                if (DeviceCommunication.CurrentConnectionType == ConnectionType.Serial)
        //                {
        //                    numberOfNonInteractiveRetries = 1;
        //                }
        //                else
        //                {
        //                    if (totalNumberOfRetriesToAttempt > 5)
        //                    {
        //                        numberOfNonInteractiveRetries = 5;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_GetDeviceSetup(int, int, int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return numberOfNonInteractiveRetries;
        //        }

        //public int functionNumber;
        //public int numberOfRetriesCompleted;
        //public int totalNumberOfRetriesToAttempt;

        //public string commandName;
        //public int modBusAddress;
        //public int readDelayInMicroseconds;

        //public string inputString;

        //public int returnedInteger = 0;
        //public string returnedString = string.Empty;
        //public Byte[] returnedBytes = null;
        //public Int16[] returnedRegisters = null;       

        public static Boolean OpenSerialOverEthernetConnection(string ipAddress, Int16 portNumber, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.OpenSerialOverEthernetConnection(ipAddress, portNumber, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.ipAddress = ipAddress;
                        inputObject.portNumber = portNumber;

                        inputObject.commandName = CommandName.OpenSerialOverEthernetConnection;
                     
                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }

                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.OpenSerialOverEthernetConnection(string, Int16, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Int16[] ReadOneRegister(int modBusAddress, int register, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValue = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValue = DeviceCommunication.ReadOneRegister(modBusAddress, register, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValue == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.ReadOneRegister;
                        inputObject.offset = register;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValue = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.ReadOneRegister(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValue;
        }

        public static Int16[] ReadRegisters1to20(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.ReadRegisters1to20;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.ReadRegisters1to20(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static Int16[] ReadMultipleRegisters(int modBusAddress, int offset, int numberOfRegistersToRead, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.ReadMultipleRegisters(modBusAddress, offset, numberOfRegistersToRead, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.ReadMultipleRegisters;
                        inputObject.offset = offset;
                        inputObject.numberOfItemsToRead = numberOfRegistersToRead;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.ReadMultipleRegisters(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static Boolean WriteSingleRegister(int modBusAddress, int registerBeingWritten, Int16 registerValue, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.WriteSingleRegister(modBusAddress, registerBeingWritten, registerValue, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.WriteSingleRegister;
                        inputObject.offset = registerBeingWritten;
                        inputObject.inputRegister = registerValue;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.WriteSingleRegister(int, int, Int16, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }



        public static bool WriteMultipleRegisters(int modBusAddress, int offset, Int16[] inputRegisters, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.WriteMultipleRegisters(modBusAddress, offset, inputRegisters, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.WriteMultipleRegisters;
                        inputObject.offset = offset;
                        inputObject.inputRegisters = inputRegisters;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.WriteMultipleRegisters(int, int, Int16[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static int DeviceIsPaused(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int paused = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    paused = DeviceCommunication.DeviceIsPaused(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((paused < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.DeviceIsPaused;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                paused = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.DeviceIsPaused(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paused;
        }

        public static bool PauseDevice(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool paused = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    paused = DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!paused) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PauseDevice;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                paused = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PauseDevice(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return paused;
        }

        public static bool ResumeDevice(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.ResumeDevice;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.ResumeDevice(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_BalanceDevice(int modBusAddress, int estimatedCommandCompletionTimeInSeconds, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                CommandInputOutputObject inputObject = new CommandInputOutputObject();
                inputObject.modBusAddress = modBusAddress;
                inputObject.readDelayInMicroseconds = readDelayInMicroseconds;
                inputObject.estimatedCommandCompletionTimeInSeconds = estimatedCommandCompletionTimeInSeconds;

                inputObject.commandName = CommandName.BHMBalanceDevice;

                using (CommandMonitor monitor = new CommandMonitor(inputObject, parentWindowInformation))
                {
                    monitor.ShowDialog();
                    monitor.Hide();
                    if (monitor.OutputCommandInputOutputObject != null)
                    {
                        success = monitor.OutputCommandInputOutputObject.returnedBoolean;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_BalanceDevice(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool StartMeasurement(int modBusAddress, int estimatedCommandCompletionTimeInSeconds, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                CommandInputOutputObject inputObject = new CommandInputOutputObject();                
                inputObject.modBusAddress = modBusAddress;
                inputObject.readDelayInMicroseconds = readDelayInMicroseconds;
                inputObject.estimatedCommandCompletionTimeInSeconds = estimatedCommandCompletionTimeInSeconds;

                inputObject.commandName = CommandName.StartMeasurement;

                using (CommandMonitor monitor = new CommandMonitor(inputObject, parentWindowInformation))
                {
                    monitor.ShowDialog();
                    monitor.Hide();
                    if (monitor.OutputCommandInputOutputObject != null)
                    {
                        success = monitor.OutputCommandInputOutputObject.returnedBoolean;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.StartMeasurement(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool ClearDeviceData(int modBusAddress, int estimatedCommandCompletionTimeInSeconds, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation)
        {
            bool cleared = false;
            try
            {
                CommandInputOutputObject inputObject = new CommandInputOutputObject();
                inputObject.modBusAddress = modBusAddress;
                inputObject.readDelayInMicroseconds = readDelayInMicroseconds;
                inputObject.estimatedCommandCompletionTimeInSeconds = estimatedCommandCompletionTimeInSeconds;

                inputObject.commandName = CommandName.ClearDeviceData;

                using (CommandMonitor monitor = new CommandMonitor(inputObject, parentWindowInformation))
                {
                    monitor.ShowDialog();
                    monitor.Hide();
                    if (monitor.OutputCommandInputOutputObject != null)
                    {
                        cleared = monitor.OutputCommandInputOutputObject.returnedBoolean;
                    }
                }              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.ClearDeviceData(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return cleared;
        }

        public static DateTime GetDeviceTime(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            DateTime deviceTime = ConversionMethods.MinimumDateTime();
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    deviceTime = DeviceCommunication.GetDeviceTime(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((deviceTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetDeviceTime;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                deviceTime = retry.OutputCommandInputOutputObject.returnedDateTime;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetDeviceTime(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceTime;
        }

        public static int GetDeviceTypeRegisterVersion(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int deviceType = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    deviceType = DeviceCommunication.GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((deviceType < 1) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetDeviceTypeRegisterVersion;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                deviceType = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetDeviceTypeRegisterVersion(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        public static int GetNumberOfArchiveRecordsSinceLastRecordRequest(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int numberOfArchivedRecords = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    numberOfArchivedRecords = DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((numberOfArchivedRecords < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetNumberOfArchiveRecordsSinceLastRecordRequest;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                numberOfArchivedRecords = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return numberOfArchivedRecords;
        }

        public static int GetNumberOfRecordsInDeviceMemory(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int numberOfRecordsInDeviceMemory = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    numberOfRecordsInDeviceMemory = DeviceCommunication.GetNumberOfRecordsInDeviceMemory(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((numberOfRecordsInDeviceMemory < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetNumberOfRecordsInDeviceMemory;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                numberOfRecordsInDeviceMemory = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetNumberOfRecordsInDeviceMemory(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return numberOfRecordsInDeviceMemory;
        }

        public static bool SetLastRecordDownloadedDate(int modBusAddress, DateTime dateTimeToSet, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.SetLastRecordDownloadedDate(modBusAddress, dateTimeToSet, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.SetLastRecordDownloadedDate;
                        inputObject.inputDateTime = dateTimeToSet;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.SetLastRecordDownloadedDate(int, DateTime, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static DateTime GetLastRecordDownloadedDate(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            DateTime lastRecordDownloadedDate = ConversionMethods.MinimumDateTime();
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    lastRecordDownloadedDate = DeviceCommunication.GetLastRecordDownloadedDate(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((lastRecordDownloadedDate.CompareTo(ConversionMethods.MinimumDateTime()) == 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetLastRecordDownloadedDate;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                lastRecordDownloadedDate = retry.OutputCommandInputOutputObject.returnedDateTime;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetLastRecordDownloadedDate(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lastRecordDownloadedDate;
        }

        public static bool MoveDataSampleIntoTheArchiveRegisters(int modBusAddress, Int16 dataSampleNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(modBusAddress, dataSampleNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.MoveDataSampleIntoTheArchiveRegisters;
                        inputObject.recordNumber = dataSampleNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(int, Int16, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static string SendStringCommand(int modBusAddress, string command, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            string commandResponse = string.Empty;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    commandResponse = DeviceCommunication.SendStringCommand(modBusAddress, command, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!DeviceCommunication.StringCommandWasSuccessful(commandResponse)) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.SendStringCommand;
                        inputObject.inputString = command;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                commandResponse = retry.OutputCommandInputOutputObject.returnedString;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.SendStringCommand(int, string, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return commandResponse;
        }

        public static long GetDeviceError(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            long error = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    error = DeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((error < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetDeviceError;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                error = retry.OutputCommandInputOutputObject.returnedLong;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetDeviceError(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        public static int GetDeviceType(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int deviceType = 0;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    deviceType = DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((deviceType < 1) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetDeviceType;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                deviceType = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetDeviceType(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceType;
        }

        public static int DeviceIsBusy(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int busy = -1;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    busy = DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((busy < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.DeviceIsBusy;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                busy = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.DeviceIsBusy(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return busy;
        }

        public static Int32[] GetArchivedDataNotDownloadedLimits(int modBusAddress, DateTime dateOfLastArchiveRecordDownloaded, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int32[] dataLimits = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    dataLimits = DeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, dateOfLastArchiveRecordDownloaded, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((dataLimits == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.GetArchivedDataNotDownloadedLimits;
                        inputObject.inputDateTime = dateOfLastArchiveRecordDownloaded;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                dataLimits = retry.OutputCommandInputOutputObject.returnedIntegers;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.GetArchivedDataNotDownloadedLimits(int, DateTime, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataLimits;
        }

        public static Int16[] Main_GetDeviceSetup(int modBusAddress, int offset, int numberOfRegistersToRead, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, numberOfRegistersToRead, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.MainGetDeviceSetup;
                        inputObject.offset = offset;
                        inputObject.numberOfItemsToRead = numberOfRegistersToRead;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_GetDeviceSetup(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static bool Main_SetDeviceSetup(int modBusAddress, int offset, Int16[] registersToWrite, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.MainSetDeviceSetup;
                        inputObject.offset = offset;
                        inputObject.inputRegisters = registersToWrite;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_SetDeviceSetup(int, int, Ibt16[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Int16[] Main_GetPartOfMeasurement(int modBusAddress, int recordNumber, int offset, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.MainGetPartOfMeasurement;
                        inputObject.recordNumber = recordNumber;
                        inputObject.offset = offset;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_GetPartOfMeasurement(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        //        public static Int16[] ADMGetPartOfMeasurement(int modBusAddress, int register, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        //        {
        //            Int16[] registerValue = null;
        //            try
        //            {
        //                int totalRetriesForNonInteractiveCall = numberOfNonInteractiveRetriesToAttempt;
        //                int totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - totalRetriesForNonInteractiveCall;

        //                registerValue = DeviceCommunication.ReadOneRegister(modBusAddress, register, readDelayInMicroseconds, 0, totalRetriesForNonInteractiveCall);
        //                if ((registerValue == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
        //                {
        //                    CommandInputOutputObject inputObject = new CommandInputOutputObject();
        //                    inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
        //                    inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
        //                    inputObject.modBusAddress = modBusAddress;
        //                    inputObject.readDelayInMicroseconds = readDelayInMicroseconds;


        //                    using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
        //                    {
        //                        retry.ShowDialog();
        //                        retry.Hide();

        //                        registerValue = retry.OutputCommandInputOutputObject.returnedRegisters;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_GetDeviceSetup(int, int, int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return registerValue;
        //        }

        //        public static Int16[] ADMGetLongValueMeasurements(int modBusAddress, int register, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        //        {
        //            Int16[] registerValue = null;
        //            try
        //            {
        //                int totalRetriesForNonInteractiveCall = numberOfNonInteractiveRetriesToAttempt;
        //                int totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - totalRetriesForNonInteractiveCall;

        //                registerValue = DeviceCommunication.ReadOneRegister(modBusAddress, register, readDelayInMicroseconds, 0, totalRetriesForNonInteractiveCall);
        //                if ((registerValue == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
        //                {
        //                    CommandInputOutputObject inputObject = new CommandInputOutputObject();
        //                    inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
        //                    inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
        //                    inputObject.modBusAddress = modBusAddress;
        //                    inputObject.readDelayInMicroseconds = readDelayInMicroseconds;


        //                    using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
        //                    {
        //                        retry.ShowDialog();
        //                        retry.Hide();

        //                        registerValue = retry.OutputCommandInputOutputObject.returnedRegisters;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.Main_GetDeviceSetup(int, int, int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return registerValue;
        //        }

//        public static bool BHM_BalanceDevice(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
//        {
//            bool balanced = false;
//            try
//            {
//                int totalRetriesForInteractiveCall;
//                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
//                {
//                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

//                    balanced = DeviceCommunication.BHM_BalanceDevice(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
//                    if ((!balanced) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
//                    {
//                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
//                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
//                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
//                        inputObject.modBusAddress = modBusAddress;
//                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

//                        inputObject.commandName = CommandName.BHMBalanceDevice;

//                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
//                        {
//                            retry.ShowDialog();
//                            retry.Hide();
//                            if (retry.OutputCommandInputOutputObject != null)
//                            {
//                                balanced = retry.OutputCommandInputOutputObject.returnedBoolean;
//                            }
//                        }
//                    }
//                }
//                else
//                {

//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_BalanceDevice(int, int, int, int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return balanced;
//        }


        public static Int16[] BHM_ReadSingleArchiveRecord(int modBusAddress, int recordNumber, int groupNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValue = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValue = DeviceCommunication.BHM_ReadSingleArchiveRecord(modBusAddress, (Int16)recordNumber, groupNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValue == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMReadSingleArchiveRecord;
                        inputObject.recordNumber = recordNumber;
                        inputObject.groupNumber = groupNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValue = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_ReadSingleArchiveRecord(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValue;
        }

        public static int BHM_Group1IsActive(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int active = -1;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    active = DeviceCommunication.BHM_Group1IsActive(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((active < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMGroup1IsActive;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                active = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_Group1IsActive(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return active;
        }

        public static int BHM_Group2IsActive(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            int active = -1;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    active = DeviceCommunication.BHM_Group2IsActive(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((active < 0) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMGroup2IsActive;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                active = retry.OutputCommandInputOutputObject.returnedInteger;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_Group2IsActive(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return active;
        }

        public static Byte[] BHM_GetPartOfMeasurement(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMGetPartOfMeasurement;
                        inputObject.recordNumber = recordNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_GetPartOfMeasurement(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static Byte[] BHM_GetDeviceSetup(int modBusAddress, int offset, int numberOfBytesToRead, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, numberOfBytesToRead, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMGetDeviceSetup;
                        inputObject.offset = offset;
                        inputObject.numberOfItemsToRead = numberOfBytesToRead;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_GetDeviceSetup(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static bool BHM_SetDeviceSetup(int modBusAddress, int offset, Byte[] valuesToWrite, int numberOfItemsToWrite, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.BHM_SetDeviceSetup(modBusAddress, offset, valuesToWrite, numberOfItemsToWrite, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.BHMSetDeviceSetup;
                        inputObject.inputBytes = valuesToWrite;
                        inputObject.offset = offset;
                        inputObject.numberOfItemsToWrite = numberOfItemsToWrite;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.BHM_SetDeviceSetup(int, Byte[], int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool PDM_ComputeNumberOfRecordsNotDownloadedYet(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMComputeNumberOfRecordsNotDownloadedYet;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Byte[] PDM_GetPartOfMeasurement(int modBusAddress, int recordNumber, int offset, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMGetPartOfMeasurement;
                        inputObject.recordNumber = recordNumber;
                        inputObject.offset = offset;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_GetPartOfMeasurement(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static Byte[] PDM_GetCommonAndChannelData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.PDM_GetCommonAndChannelData(modBusAddress, recordNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMGetCommonAndChannelData;
                        inputObject.recordNumber = recordNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_GetCommonAndChannelData(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static Byte[] PDM_GetPhaseResolvedData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.PDM_GetPhaseResolvedData(modBusAddress, recordNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMGetPhaseResolvedData;
                        inputObject.recordNumber = recordNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_GetPhaseResolvedData(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static Byte[] PDM_GetDeviceSetup(int modBusAddress, int offset, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Byte[] byteValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    byteValues = DeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((byteValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMGetDeviceSetup;
                        inputObject.offset = offset;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                byteValues = retry.OutputCommandInputOutputObject.returnedBytes;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_GetDeviceSetup(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteValues;
        }

        public static bool PDM_SetDeviceSetup(int modBusAddress, int offset, Byte[] bytesToWrite, int numberOfBytesToWrite, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.PDM_SetDeviceSetup(modBusAddress, offset, bytesToWrite, numberOfBytesToWrite, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMSetDeviceSetup;
                        inputObject.offset = offset;
                        inputObject.inputBytes = bytesToWrite;
                        inputObject.numberOfItemsToWrite = numberOfBytesToWrite;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_SetDeviceSetup(int, Byte[], int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_ResetSetupParameters(int modBusAddress, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.PDM_ResetSetupParameters(modBusAddress, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMResetSetupParameters;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_ResetSetupParameters(int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Int16[] PDM_ReadArchivedCommonData(int modBusAddress, int recordNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.PDM_ReadArchivedCommonData(modBusAddress, (Int16)recordNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMReadArchivedCommonData;
                        inputObject.recordNumber = recordNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_ReadArchivedCommonData(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static Int16[] PDM_ReadArchivedChannelDataForOneChannel(int modBusAddress, int recordNumber, int channelNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.PDM_ReadArchivedChannelDataForOneChannel(modBusAddress, (Int16)recordNumber, channelNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMReadArchivedChannelDataForOneChannel;
                        inputObject.recordNumber = recordNumber;
                        inputObject.channelNumber = channelNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_ReadArchivedChannelDataForOneChannel(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static Int16[] PDM_ReadArchivedMatrixDataForOneChannel(int modBusAddress, int recordNumber, int channelNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            Int16[] registerValues = null;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    registerValues = DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(modBusAddress, (Int16)recordNumber, channelNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((registerValues == null) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMReadArchivedMatrixDataForOneChannel;
                        inputObject.recordNumber = recordNumber;
                        inputObject.channelNumber = channelNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                registerValues = retry.OutputCommandInputOutputObject.returnedRegisters;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(int, int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return registerValues;
        }

        public static bool PDM_RunInitial(int modBusAddress, int channelNumber, int estimatedCommandCompletionTimeInSeconds, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation)
        {
            bool cleared = false;
            try
            {
                CommandInputOutputObject inputObject = new CommandInputOutputObject();
                inputObject.modBusAddress = modBusAddress;
                inputObject.channelNumber = channelNumber;
                inputObject.readDelayInMicroseconds = readDelayInMicroseconds;
                inputObject.estimatedCommandCompletionTimeInSeconds = estimatedCommandCompletionTimeInSeconds;

                inputObject.commandName = CommandName.PDMRunInitial;

                using (CommandMonitor monitor = new CommandMonitor(inputObject, parentWindowInformation))
                {
                    monitor.ShowDialog();
                    monitor.Hide();
                    if (monitor.OutputCommandInputOutputObject != null)
                    {
                        cleared = monitor.OutputCommandInputOutputObject.returnedBoolean;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_RunInitial(int, int, int, ParentWindowInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return cleared;
        }

        public static bool PDM_MoveMatrixDataIntoArchivesForOneChannel(int modBusAddress, int channelNumber, int readDelayInMicroseconds, int numberOfNonInteractiveRetriesToAttempt, int totalNumberOfRetriesToAttempt, ParentWindowInformation parentWindowInformation)
        {
            bool success = false;
            try
            {
                int totalRetriesForInteractiveCall;
                if ((numberOfNonInteractiveRetriesToAttempt > -1) && (totalNumberOfRetriesToAttempt > 0))
                {
                    totalRetriesForInteractiveCall = totalNumberOfRetriesToAttempt - numberOfNonInteractiveRetriesToAttempt;

                    success = DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(modBusAddress, (Int16)channelNumber, readDelayInMicroseconds, 0, numberOfNonInteractiveRetriesToAttempt);
                    if ((!success) && DeviceCommunication.DownloadIsEnabled() && (totalRetriesForInteractiveCall > 0))
                    {
                        CommandInputOutputObject inputObject = new CommandInputOutputObject();
                        inputObject.numberOfRetriesCompleted = numberOfNonInteractiveRetriesToAttempt;
                        inputObject.totalNumberOfRetriesToAttempt = totalNumberOfRetriesToAttempt;
                        inputObject.modBusAddress = modBusAddress;
                        inputObject.readDelayInMicroseconds = readDelayInMicroseconds;

                        inputObject.commandName = CommandName.PDMMoveMatrixDataIntoArchivesForOneChannel;
                        inputObject.channelNumber = channelNumber;

                        using (RetryWindow retry = new RetryWindow(inputObject, parentWindowInformation))
                        {
                            retry.ShowDialog();
                            retry.Hide();
                            if (retry.OutputCommandInputOutputObject != null)
                            {
                                success = retry.OutputCommandInputOutputObject.returnedBoolean;
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in InteractiveDeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(int, int, int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }
    }
}
