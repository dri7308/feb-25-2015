using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using GeneralUtilities;

namespace MonitorInterface
{
    public partial class RetryWindow : Telerik.WinControls.UI.RadForm
    {
        private static BackgroundWorker commandRetryBackgroundWorker;

        private CommandInputOutputObject inputCommandInputOutputObject;

        private CommandInputOutputObject outputCommandInputOutputObject;
        public CommandInputOutputObject OutputCommandInputOutputObject
        {
            get
            {
                return outputCommandInputOutputObject;
            }
        }

        public static bool isInteractive = false;

        private static string openSerialOverEthernetConnectionText = "Open serial over ethernet connection";
        private static string readOneRegisterText = "Read one device register";
        private static string readRegisters1to20Text = "Read device alarm status";
        private static string readMultipleRegistersText = "Read multiple device registers";
        private static string writeSingleRegisterText = "Write value to a single register";
        private static string writeMultipleRegistersText = "Write values to multiple registers";
        private static string deviceIsPausedText = "Determine if the device is paused";
        private static string pauseDeviceText = "Pause device";
        private static string resumeDeviceText = "Resume device";
        private static string clearDeviceDataText = "Clear device data";
        private static string getDeviceTimeText = "Get the device date and time";
        private static string getDeviceTypeRegisterVersionText = "Get device type from the registers";
        private static string getNumberOfArchivedRecordsSinceLastRecordRequestText = "Get the number of archived records to be downloaded";
        private static string getNumberOfRecordsInDeviceMemoryText = "Get the total number of archived records";
        private static string setLastRecordDownloadedDateText = "Set the date of the last record downloaded";
        private static string getLastRecordDownloadedDateText = "Get the date of the last record downloaded";
        private static string moveDataSampleIntoTheArchiveRegistersText = "Move the desired data sample into the archive registers";
        private static string sendStringCommandText = "Send string command to the device";
        private static string getDeviceErrorText = "Get the error state of the device";
        private static string getDeviceTypeText = "Get the device type";
        private static string deviceIsBusyText = "Determine if the device is busy";
        private static string getArchivedDataNotDownloadedLimitsText = "Get the first and last archived data to download";
        private static string mainGetDeviceSetupText = "Get portion of Main monitor configuration";
        private static string mainSetDeviceSetupText = "Set protion of Main monitor configuration";
        private static string mainGetPartOfMeasurementText = "Get portion of Main monitor data";
        private static string admGetPartOfMeasurementText = "Get portion of ADM data";
        private static string admGetLongValueMeasurementsText = "Get portion of ADM long data";
        private static string bhmBalanceDeviceText = "Balance device";
        private static string bhmReadSingleArchiveRecordText = "Get single BHM archived record";
        private static string bhmGroup1IsActiveText = "Determine if BHM set 1 is active";
        private static string bhmGroup2IsActiveText = "Determine if BHM set 2 is active";
        private static string bhmGetPartOfMeasurementText = "Get portion of BHM data";
        private static string bhmGetDeviceSetupText = "Get portion of BHM configuration";
        private static string bhmSetDeviceSetupText = "Set portion of BHM configuration";
        private static string pdmComputeNumberOfRecordsNotDownloadedYetText = "Compute number of PDM archived records not downloaded";
        private static string pdmGetPartOfMeasurementText = "Get portion of PDM data";
        private static string pdmGetCommonAndChannelDataText = "Get PDM common and channel data";
        private static string pdmGetPhaseResolvedDataText = "Get PDM phase resolved data";
        private static string pdmGetDeviceSetupText = "Get portion of PDM configuration";
        private static string pdmSetDeviceSetupText = "Set portion of PDM configuration";
        private static string pdmResetSetupParametersText = "Set PDM configuration upload as finished";
        private static string pdmReadArchivedCommonDataText = "Get archived PDM common data";
        private static string pdmReadArchivedChannelDataForOneChannelText = "Get archived PDM channel data for one channel";
        private static string pdmReadArchivedMatrixDataForOneChannelText = "Get archived PDM phase resolved data for one channel";
        private static string pdmMoveMatrixDataIntoArchivesForOneChannelText = "Move PDM phase resolved data for one channel into archive registers";
        private static string commandNotSpecifiedText = "Command was not specified";
       

        private static string commandFailedText = "Command failed";
        private static string retryingCommandPrefixText = "Retrying command:";
        private static string cancellingCommandPrefixText = "Cancelling command:";
        private static string cancelRadWaitingBarText = "Cancelling ...";

        private static string commandRetryTitleText = "Command Retry";
        private static string commandRetriesRadProgressBarText = "Command Retries";
        private static string cancelRetryRadButtonText = "Cancel Operation";
        private static string downloadTypeRadLabelText = "Download type is";

        ParentWindowInformation parentWindowInformation;

        public RetryWindow(CommandInputOutputObject argInputCommandInputOutputObject, ParentWindowInformation argParentWindowInformation)
        {
            InitializeComponent();
            InitializeCommandRetryBackgroundWorker();
            
            this.inputCommandInputOutputObject = argInputCommandInputOutputObject;
            this.parentWindowInformation = argParentWindowInformation;

            commandFailedRadLabel.Text = commandFailedText;
            downloadTypeRadLabel.Text = retryingCommandPrefixText + " " + GetStringVersionOfCommand(inputCommandInputOutputObject.commandName);
            cancelRadWaitingBar.Visible = false;

           // this.StartPosition = FormStartPosition.CenterParent;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = this.parentWindowInformation.AssignLocation();
        }

        private void RetryWindow_Load(object sender, EventArgs e)
        {
            commandRetryBackgroundWorker.RunWorkerAsync(inputCommandInputOutputObject);
        }

        private void cancelRetryRadButton_Click(object sender, EventArgs e)
        {
            downloadTypeRadLabel.Text = cancellingCommandPrefixText + " " + GetStringVersionOfCommand(inputCommandInputOutputObject.commandName);
            cancelRadWaitingBar.Text = cancelRadWaitingBarText;
            //commandRetriesRadProgressBar.Visible = false;
            //cancelRadWaitingBar.Location = new Point(commandRetriesRadProgressBar.Location.X, commandRetriesRadProgressBar.Location.Y);
            //cancelRadWaitingBar.Size = new Size(commandRetriesRadProgressBar.Size.Width, commandRetriesRadProgressBar.Size.Height);
            cancelRadWaitingBar.Visible = true;
            cancelRadWaitingBar.StartWaiting();
            DeviceCommunication.DisableDataDownload();
            DeviceCommunication.DisableConnection();
        }

        public static void UpdateProgressBar(int retryCount, int totalTries)
        {
            try
            {
                if (isInteractive)
                {
                    if ((retryCount >= 0) && (retryCount <= totalTries))
                    {
                        commandRetryBackgroundWorker.ReportProgress((retryCount * 100) / totalTries);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in RetryWindow.UpdateProgressBar(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void InitializeCommandRetryBackgroundWorker()
        {
            /// Set up the download background worker
            commandRetryBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            commandRetryBackgroundWorker.DoWork += commandRetryBackgroundWorker_DoWork;
            commandRetryBackgroundWorker.ProgressChanged += commandRetryBackgroundWorker_ProgressChanged;
            commandRetryBackgroundWorker.RunWorkerCompleted += commandRetryBackgroundWorker_RunWorkerCompleted;
        }

        private void commandRetryBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                CommandInputOutputObject inputArgument = e.Argument as CommandInputOutputObject;
                CommandInputOutputObject outputArgument = new CommandInputOutputObject();

                CommandName commandName = inputArgument.commandName;

                string ipAddress = inputArgument.ipAddress;
                Int16 portNumber = inputArgument.portNumber;
                int modBusAddress = inputArgument.modBusAddress;
                int offset = inputArgument.offset;
                int recordNumber = inputArgument.recordNumber;
                int groupNumber = inputArgument.groupNumber;
                int channelNumber = inputArgument.channelNumber;
                int numberOfItemsToRead = inputArgument.numberOfItemsToRead;
                int numberOfItemsToWrite = inputArgument.numberOfItemsToWrite;
                int readDelayInMicroseconds = inputArgument.readDelayInMicroseconds;
                int numberOfRetryAttemptsCompleted = inputArgument.numberOfRetriesCompleted;
                int totalNumberOfRetriesToAttempt = inputArgument.totalNumberOfRetriesToAttempt;
                string inputString = inputArgument.inputString;
                DateTime inputDateTime = inputArgument.inputDateTime;

                Byte[] inputBytes = inputArgument.inputBytes;
                Int16 inputRegister = inputArgument.inputRegister;
                Int16[] inputRegisters = inputArgument.inputRegisters;

                outputCommandInputOutputObject = null;

                isInteractive = true;

                UpdateProgressBar(numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);

                if (commandName == CommandName.OpenSerialOverEthernetConnection)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.OpenSerialOverEthernetConnection(ipAddress, portNumber, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ReadOneRegister)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.ReadOneRegister(modBusAddress, offset, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ReadRegisters1to20)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.ReadRegisters1to20(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ReadMultipleRegisters)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.ReadMultipleRegisters(modBusAddress, offset, numberOfItemsToRead, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.WriteSingleRegister)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.WriteSingleRegister(modBusAddress, offset, inputRegister, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.WriteMultipleRegisters)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.WriteMultipleRegisters(modBusAddress, offset, inputRegisters, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.DeviceIsPaused)
                {
                    outputArgument.returnedInteger = DeviceCommunication.DeviceIsPaused(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PauseDevice)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PauseDevice(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ResumeDevice)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.ResumeDevice(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ClearDeviceData)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.ClearDeviceData(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetDeviceTime)
                {
                    outputArgument.returnedDateTime = DeviceCommunication.GetDeviceTime(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetDeviceTypeRegisterVersion)
                {
                    outputArgument.returnedInteger = DeviceCommunication.GetDeviceTypeRegisterVersion(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetNumberOfArchiveRecordsSinceLastRecordRequest)
                {
                    outputArgument.returnedInteger = DeviceCommunication.GetNumberOfArchiveRecordsSinceLastRecordRequest(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetNumberOfRecordsInDeviceMemory)
                {
                    outputArgument.returnedInteger = DeviceCommunication.GetNumberOfRecordsInDeviceMemory(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.SetLastRecordDownloadedDate)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.SetLastRecordDownloadedDate(modBusAddress, inputDateTime, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetLastRecordDownloadedDate)
                {
                    outputArgument.returnedDateTime = DeviceCommunication.GetLastRecordDownloadedDate(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.MoveDataSampleIntoTheArchiveRegisters)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.MoveDataSampleIntoTheArchiveRegisters(modBusAddress, (Int16)recordNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.SendStringCommand)
                {
                    outputArgument.returnedString = DeviceCommunication.SendStringCommand(modBusAddress, inputString, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetDeviceError)
                {
                    outputArgument.returnedLong = DeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetDeviceType)
                {
                    outputArgument.returnedInteger = DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.DeviceIsBusy)
                {
                    outputArgument.returnedInteger = DeviceCommunication.DeviceIsBusy(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.GetArchivedDataNotDownloadedLimits)
                {
                    outputArgument.returnedIntegers = DeviceCommunication.GetArchivedDataNotDownloadedLimits(modBusAddress, inputDateTime, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.MainGetDeviceSetup)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, numberOfItemsToRead, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.MainSetDeviceSetup)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, inputRegisters, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.MainGetPartOfMeasurement)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.Main_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.ADMGetPartOfMeasurement)
                {
                    // outputArgument.returnedBytes = DeviceCommunication.ADM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, 
                }
                else if (commandName == CommandName.ADMGetLongValueMeasurements)
                {
                    //
                }
                else if (commandName == CommandName.BHMReadSingleArchiveRecord)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.BHM_ReadSingleArchiveRecord(modBusAddress, (Int16)recordNumber, groupNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.BHMGroup1IsActive)
                {
                    outputArgument.returnedInteger = DeviceCommunication.BHM_Group1IsActive(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.BHMGroup2IsActive)
                {
                    outputArgument.returnedInteger = DeviceCommunication.BHM_Group2IsActive(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.BHMGetPartOfMeasurement)
                {
                    outputArgument.returnedBytes = DeviceCommunication.BHM_GetPartOfMeasurement(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.BHMGetDeviceSetup)
                {
                    outputArgument.returnedBytes = DeviceCommunication.BHM_GetDeviceSetup(modBusAddress, offset, numberOfItemsToRead, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.BHMSetDeviceSetup)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.BHM_SetDeviceSetup(modBusAddress, offset, inputBytes, numberOfItemsToWrite, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMComputeNumberOfRecordsNotDownloadedYet)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PDM_ComputeNumberOfRecordsNotDownloadedYet(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMGetPartOfMeasurement)
                {
                    outputArgument.returnedBytes = DeviceCommunication.PDM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMGetCommonAndChannelData)
                {
                    outputArgument.returnedBytes = DeviceCommunication.PDM_GetCommonAndChannelData(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMGetPhaseResolvedData)
                {
                    outputArgument.returnedBytes = DeviceCommunication.PDM_GetPhaseResolvedData(modBusAddress, recordNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMGetDeviceSetup)
                {
                    outputArgument.returnedBytes = DeviceCommunication.PDM_GetDeviceSetup(modBusAddress, offset, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMSetDeviceSetup)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PDM_SetDeviceSetup(modBusAddress, offset, inputBytes, numberOfItemsToWrite, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMResetSetupParameters)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PDM_ResetSetupParameters(modBusAddress, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMReadArchivedCommonData)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.PDM_ReadArchivedCommonData(modBusAddress, (Int16)recordNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMReadArchivedChannelDataForOneChannel)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.PDM_ReadArchivedChannelDataForOneChannel(modBusAddress, (Int16)recordNumber, channelNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMReadArchivedMatrixDataForOneChannel)
                {
                    outputArgument.returnedRegisters = DeviceCommunication.PDM_ReadArchivedMatrixDataForOneChannel(modBusAddress, (Int16)recordNumber, channelNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else if (commandName == CommandName.PDMMoveMatrixDataIntoArchivesForOneChannel)
                {
                    outputArgument.returnedBoolean = DeviceCommunication.PDM_MoveMatrixDataIntoArchivesForOneChannel(modBusAddress, (Int16)channelNumber, readDelayInMicroseconds, numberOfRetryAttemptsCompleted, totalNumberOfRetriesToAttempt);
                }
                else
                {
                    string errorMessage = "Error thrown in CommandRetry.commandRetryBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nCommand name was not specified, no command executed.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                e.Result = outputArgument;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandRetryBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
          
        }

        private void commandRetryBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                int progress = e.ProgressPercentage;

                if (progress < 0)
                {
                    progress = 0;
                }
                if (progress > 100)
                {
                    progress = 100;
                }

                this.commandRetriesRadProgressBar.Value1 = progress;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandRetryBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void commandRetryBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in CommandRetry.commandRetryBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif                    
                }
                //else if (e.Cancelled)
                //{
                                     
                //}
                //else
                //{

                //}

                if (e.Result != null)
                {
                    outputCommandInputOutputObject = e.Result as CommandInputOutputObject;
                }
                else
                {
                    outputCommandInputOutputObject = null;
                    string errorMessage = "Error in CommandRetry.commandRetryBackgroundWorker_DoWork(object, DoWorkEventArgs)\nResult from DoWork was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif                  
                }

                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CommandRetry.commandRetryBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                isInteractive = false;
            }
        }

        private string GetStringVersionOfCommand(CommandName commandName)
        {
            string stringVersionOfCommand = string.Empty;

            if (commandName == CommandName.OpenSerialOverEthernetConnection)
            {
                stringVersionOfCommand = openSerialOverEthernetConnectionText;
            }
            else if (commandName == CommandName.ReadOneRegister)
            {
                stringVersionOfCommand = readOneRegisterText;
            }
            else if (commandName == CommandName.ReadRegisters1to20)
            {
                stringVersionOfCommand = readRegisters1to20Text;
            }
            else if (commandName == CommandName.ReadMultipleRegisters)
            {
                stringVersionOfCommand = readMultipleRegistersText;
            }
            else if (commandName == CommandName.WriteSingleRegister)
            {
                stringVersionOfCommand = writeSingleRegisterText;
            }
            else if (commandName == CommandName.WriteMultipleRegisters)
            {
                stringVersionOfCommand = writeMultipleRegistersText;
            }
            else if (commandName == CommandName.DeviceIsPaused)
            {
                stringVersionOfCommand = deviceIsPausedText;
            }
            else if (commandName == CommandName.PauseDevice)
            {
               stringVersionOfCommand = pauseDeviceText;
            }
            else if (commandName == CommandName.ResumeDevice)
            {
                stringVersionOfCommand = resumeDeviceText;
            }
            else if (commandName == CommandName.ClearDeviceData)
            {
                stringVersionOfCommand = clearDeviceDataText;
            }
            else if (commandName == CommandName.GetDeviceTime)
            {
                stringVersionOfCommand = getDeviceTimeText;
            }
            else if (commandName == CommandName.GetDeviceTypeRegisterVersion)
            {
                stringVersionOfCommand = getDeviceTypeRegisterVersionText;
            }
            else if (commandName == CommandName.GetNumberOfArchiveRecordsSinceLastRecordRequest)
            {
                stringVersionOfCommand = getNumberOfArchivedRecordsSinceLastRecordRequestText;
            }
            else if (commandName == CommandName.GetNumberOfRecordsInDeviceMemory)
            {
               stringVersionOfCommand = getNumberOfRecordsInDeviceMemoryText;
            }
            else if (commandName == CommandName.SetLastRecordDownloadedDate)
            {
                stringVersionOfCommand = setLastRecordDownloadedDateText;
            }
            else if (commandName == CommandName.GetLastRecordDownloadedDate)
            {
               stringVersionOfCommand = getLastRecordDownloadedDateText;
            }
            else if (commandName == CommandName.MoveDataSampleIntoTheArchiveRegisters)
            {
               stringVersionOfCommand = moveDataSampleIntoTheArchiveRegistersText;
            }
            else if (commandName == CommandName.SendStringCommand)
            {
               stringVersionOfCommand = sendStringCommandText;
            }
            else if (commandName == CommandName.GetDeviceError)
            {
                stringVersionOfCommand = getDeviceErrorText;
            }
            else if (commandName == CommandName.GetDeviceType)
            {
                stringVersionOfCommand = getDeviceTypeText;
            }
            else if (commandName == CommandName.DeviceIsBusy)
            {
               stringVersionOfCommand = deviceIsBusyText;
            }
            else if (commandName == CommandName.GetArchivedDataNotDownloadedLimits)
            {
               stringVersionOfCommand = getArchivedDataNotDownloadedLimitsText;
            }
            else if (commandName == CommandName.MainGetDeviceSetup)
            {
               stringVersionOfCommand = mainGetDeviceSetupText;
            }
            else if (commandName == CommandName.MainSetDeviceSetup)
            {
               stringVersionOfCommand = mainSetDeviceSetupText;
            }
            else if (commandName == CommandName.MainGetPartOfMeasurement)
            {
                stringVersionOfCommand = mainGetPartOfMeasurementText;
            }
            else if (commandName == CommandName.ADMGetPartOfMeasurement)
            {
                // outputArgument.returnedBytes = DeviceCommunication.ADM_GetPartOfMeasurement(modBusAddress, recordNumber, offset, 
            }
            else if (commandName == CommandName.ADMGetLongValueMeasurements)
            {
                //
            }
            else if (commandName == CommandName.BHMBalanceDevice)
            {
                stringVersionOfCommand = bhmBalanceDeviceText;
            }
            else if (commandName == CommandName.BHMReadSingleArchiveRecord)
            {
               stringVersionOfCommand = bhmReadSingleArchiveRecordText;
            }
            else if (commandName == CommandName.BHMGroup1IsActive)
            {
               stringVersionOfCommand = bhmGroup1IsActiveText;
            }
            else if (commandName == CommandName.BHMGroup2IsActive)
            {
                stringVersionOfCommand = bhmGroup2IsActiveText;
            }
            else if (commandName == CommandName.BHMGetPartOfMeasurement)
            {
                stringVersionOfCommand = bhmGetPartOfMeasurementText;
            }
            else if (commandName == CommandName.BHMGetDeviceSetup)
            {
                stringVersionOfCommand = bhmGetDeviceSetupText;
            }
            else if (commandName == CommandName.BHMSetDeviceSetup)
            {
                stringVersionOfCommand = bhmSetDeviceSetupText;
            }
            else if (commandName == CommandName.PDMComputeNumberOfRecordsNotDownloadedYet)
            {
               stringVersionOfCommand = pdmComputeNumberOfRecordsNotDownloadedYetText;
            }
            else if (commandName == CommandName.PDMGetPartOfMeasurement)
            {
                stringVersionOfCommand = pdmGetPartOfMeasurementText;
            }
            else if (commandName == CommandName.PDMGetCommonAndChannelData)
            {
                stringVersionOfCommand = pdmGetCommonAndChannelDataText;
            }
            else if (commandName == CommandName.PDMGetPhaseResolvedData)
            {
               stringVersionOfCommand = pdmGetPhaseResolvedDataText;
            }
            else if (commandName == CommandName.PDMGetDeviceSetup)
            {
               stringVersionOfCommand = pdmGetDeviceSetupText;
            }
            else if (commandName == CommandName.PDMSetDeviceSetup)
            {
                stringVersionOfCommand = pdmSetDeviceSetupText;
            }
            else if (commandName == CommandName.PDMResetSetupParameters)
            {
                stringVersionOfCommand = pdmResetSetupParametersText;
            }
            else if (commandName == CommandName.PDMReadArchivedCommonData)
            {
               stringVersionOfCommand = pdmReadArchivedCommonDataText;
            }
            else if (commandName == CommandName.PDMReadArchivedChannelDataForOneChannel)
            {
                stringVersionOfCommand = pdmReadArchivedChannelDataForOneChannelText;
            }
            else if (commandName == CommandName.PDMReadArchivedMatrixDataForOneChannel)
            {
                stringVersionOfCommand = pdmReadArchivedMatrixDataForOneChannelText;
            }
            else if (commandName == CommandName.PDMMoveMatrixDataIntoArchivesForOneChannel)
            {
                stringVersionOfCommand = pdmMoveMatrixDataIntoArchivesForOneChannelText;
            }
            else
            {
                stringVersionOfCommand = commandNotSpecifiedText;
            }
            return stringVersionOfCommand;
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = commandRetryTitleText;
                commandRetriesRadProgressBar.Text = commandRetriesRadProgressBarText;
                commandFailedRadLabel.Text = commandFailedText;
                downloadTypeRadLabel.Text = downloadTypeRadLabelText;
                cancelRetryRadButton.Text = cancelRetryRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                commandRetryTitleText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCommandRetryTitleText", commandRetryTitleText, "", "", "");
                commandRetriesRadProgressBarText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCommandRetriesRadProgressBarText", commandRetriesRadProgressBarText, "", "", "");
                cancelRetryRadButtonText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCancelRetryRadButtonText", cancelRetryRadButtonText, "", "", "");
                downloadTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceDownloadTypeRadLabelText", downloadTypeRadLabelText, "", "", "");
                commandFailedText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCommandFailedText", commandFailedText, "", "", "");
                retryingCommandPrefixText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceRetryingCommandPrefixText", retryingCommandPrefixText, "", "", "");
                cancellingCommandPrefixText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCancellingCommandPrefixText", cancellingCommandPrefixText, "", "", "");
                cancelRadWaitingBarText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowInterfaceCancelRadWaitingBarText", cancelRadWaitingBarText, "", "", "");

                openSerialOverEthernetConnectionText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowOpenSerialOverEthernetConnectionText", openSerialOverEthernetConnectionText, "", "", "");
                readOneRegisterText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowReadOneRegisterText", readOneRegisterText, "", "", "");
                readRegisters1to20Text = LanguageConversion.GetStringAssociatedWithTag("RetryWindowReadRegisters1to20Text", readRegisters1to20Text, "", "", "");
                readMultipleRegistersText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowReadMultipleRegistersText", readMultipleRegistersText, "", "", "");
                writeSingleRegisterText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowWriteSingleRegisterText", writeSingleRegisterText, "", "", "");
                writeMultipleRegistersText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowWriteMultipleRegistersText", writeMultipleRegistersText, "", "", "");
                deviceIsPausedText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowDeviceIsPausedText", deviceIsPausedText, "", "", "");
                pauseDeviceText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPauseDeviceText", pauseDeviceText, "", "", "");
                resumeDeviceText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowResumeDeviceText", resumeDeviceText, "", "", "");
                clearDeviceDataText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowClearDeviceDataText", clearDeviceDataText, "", "", "");
                getDeviceTimeText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetDeviceTimeText", getDeviceTimeText, "", "", "");
                getDeviceTypeRegisterVersionText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetDeviceTypeRegisterVersionText", getDeviceTypeRegisterVersionText, "", "", "");
                getNumberOfArchivedRecordsSinceLastRecordRequestText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetNumberOfArchivedRecordsSinceLastRecordRequestText", getNumberOfArchivedRecordsSinceLastRecordRequestText, "", "", "");
                getNumberOfRecordsInDeviceMemoryText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetNumberOfRecordsInDeviceMemoryText", getNumberOfRecordsInDeviceMemoryText, "", "", "");
                setLastRecordDownloadedDateText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowSetLastRecordDownloadedDateText", setLastRecordDownloadedDateText, "", "", "");
                getLastRecordDownloadedDateText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetLastRecordDownloadedDateText", getLastRecordDownloadedDateText, "", "", "");
                moveDataSampleIntoTheArchiveRegistersText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowMoveDataSampleIntoTheArchiveRegistersText", moveDataSampleIntoTheArchiveRegistersText, "", "", "");
                sendStringCommandText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowSendStringCommandText", sendStringCommandText, "", "", "");
                getDeviceErrorText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetDeviceErrorText", getDeviceErrorText, "", "", "");
                getDeviceTypeText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetDeviceTypeText", getDeviceTypeText, "", "", "");
                deviceIsBusyText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowDeviceIsBusyText", deviceIsBusyText, "", "", "");
                getArchivedDataNotDownloadedLimitsText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowGetArchivedDataNotDownloadedLimitsText", getArchivedDataNotDownloadedLimitsText, "", "", "");
                mainGetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowMainGetDeviceSetupText", mainGetDeviceSetupText, "", "", "");
                mainSetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowMainSetDeviceSetupText", mainSetDeviceSetupText, "", "", "");
                mainGetPartOfMeasurementText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowMainGetPartOfMeasurementText", mainGetPartOfMeasurementText, "", "", "");
                admGetPartOfMeasurementText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowAdmGetPartOfMeasurementText", admGetPartOfMeasurementText, "", "", "");
                admGetLongValueMeasurementsText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowAdmGetLongValueMeasurementsText", admGetLongValueMeasurementsText, "", "", "");
                bhmBalanceDeviceText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmBalanceDeviceText", bhmBalanceDeviceText, "", "", "");
                bhmReadSingleArchiveRecordText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmReadSingleArchiveRecordText", bhmReadSingleArchiveRecordText, "", "", "");
                bhmGroup1IsActiveText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmGroup1IsActiveText", bhmGroup1IsActiveText, "", "", "");
                bhmGroup2IsActiveText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowbhmGroup2IsActiveText", bhmGroup2IsActiveText, "", "", "");
                bhmGetPartOfMeasurementText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmGetPartOfMeasurementText", bhmGetPartOfMeasurementText, "", "", "");
                bhmGetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmGetDeviceSetupText", bhmGetDeviceSetupText, "", "", "");
                bhmSetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowBhmSetDeviceSetupText", bhmSetDeviceSetupText, "", "", "");
                pdmComputeNumberOfRecordsNotDownloadedYetText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmComputeNumberOfRecordsNotDownloadedYetText", pdmComputeNumberOfRecordsNotDownloadedYetText, "", "", "");
                pdmGetPartOfMeasurementText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmGetPartOfMeasurementText", pdmGetPartOfMeasurementText, "", "", "");
                pdmGetCommonAndChannelDataText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmGetCommonAndChannelDataText", pdmGetCommonAndChannelDataText, "", "", "");
                pdmGetPhaseResolvedDataText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmGetPhaseResolvedDataText", pdmGetPhaseResolvedDataText, "", "", "");
                pdmGetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmGetDeviceSetupText", pdmGetDeviceSetupText, "", "", "");
                pdmSetDeviceSetupText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmSetDeviceSetupText", pdmSetDeviceSetupText, "", "", "");
                pdmResetSetupParametersText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmResetSetupParametersText", pdmResetSetupParametersText, "", "", "");
                pdmReadArchivedCommonDataText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmReadArchivedCommonDataText", pdmReadArchivedCommonDataText, "", "", "");
                pdmReadArchivedChannelDataForOneChannelText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmReadArchivedChannelDataForOneChannelText", pdmReadArchivedChannelDataForOneChannelText, "", "", "");
                pdmReadArchivedMatrixDataForOneChannelText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmReadArchivedMatrixDataForOneChannelText", pdmReadArchivedMatrixDataForOneChannelText, "", "", "");
                pdmMoveMatrixDataIntoArchivesForOneChannelText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowPdmMoveMatrixDataIntoArchivesForOneChannelText", pdmMoveMatrixDataIntoArchivesForOneChannelText, "", "", "");
                commandNotSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("RetryWindowCommandNotSpecifiedText", commandNotSpecifiedText, "", "", "");               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
     

    }

    public enum CommandName
    {
        OpenSerialOverEthernetConnection,
        ReadOneRegister,
        ReadRegisters1to20,
        ReadMultipleRegisters,
        WriteSingleRegister,
        WriteMultipleRegisters,
        DeviceIsPaused,
        PauseDevice,
        ResumeDevice,
        ClearDeviceData,
        GetDeviceTime,
        GetDeviceTypeRegisterVersion,
        GetNumberOfArchiveRecordsSinceLastRecordRequest,
        GetNumberOfRecordsInDeviceMemory,
        SetLastRecordDownloadedDate,
        GetLastRecordDownloadedDate,
        MoveDataSampleIntoTheArchiveRegisters,
        SendStringCommand,
        StartMeasurement,
        GetDeviceError,
        GetDeviceType,
        DeviceIsBusy,
        GetArchivedDataNotDownloadedLimits,
        MainGetDeviceSetup,
        MainSetDeviceSetup,
        MainGetPartOfMeasurement,
        ADMGetPartOfMeasurement,
        ADMGetLongValueMeasurements,
        BHMBalanceDevice,
        BHMReadSingleArchiveRecord,
        BHMGroup1IsActive,
        BHMGroup2IsActive,
        BHMGetPartOfMeasurement,
        BHMGetDeviceSetup,
        BHMSetDeviceSetup,
        PDMComputeNumberOfRecordsNotDownloadedYet,
        PDMGetPartOfMeasurement,
        PDMGetCommonAndChannelData,
        PDMGetPhaseResolvedData,
        PDMGetDeviceSetup,
        PDMSetDeviceSetup,
        PDMResetSetupParameters,
        PDMReadArchivedCommonData,
        PDMReadArchivedChannelDataForOneChannel,
        PDMReadArchivedMatrixDataForOneChannel,
        PDMMoveMatrixDataIntoArchivesForOneChannel,
        PDMRunInitial,
        None        
    };

}
