namespace MonitorInterface
{
    partial class RetryWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RetryWindow));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.commandRetriesRadProgressBar = new Telerik.WinControls.UI.RadProgressBar();
            this.downloadTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cancelRetryRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.commandFailedRadLabel = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.commandRetriesRadProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRetryRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandFailedRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // commandRetriesRadProgressBar
            // 
            this.commandRetriesRadProgressBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commandRetriesRadProgressBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandRetriesRadProgressBar.ImageIndex = -1;
            this.commandRetriesRadProgressBar.ImageKey = "";
            this.commandRetriesRadProgressBar.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.commandRetriesRadProgressBar.Location = new System.Drawing.Point(12, 65);
            this.commandRetriesRadProgressBar.Name = "commandRetriesRadProgressBar";
            this.commandRetriesRadProgressBar.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.commandRetriesRadProgressBar.SeparatorColor2 = System.Drawing.Color.White;
            this.commandRetriesRadProgressBar.SeparatorColor3 = System.Drawing.Color.White;
            this.commandRetriesRadProgressBar.SeparatorColor4 = System.Drawing.Color.White;
            this.commandRetriesRadProgressBar.SeparatorWidth = 4;
            this.commandRetriesRadProgressBar.Size = new System.Drawing.Size(281, 23);
            this.commandRetriesRadProgressBar.StepWidth = 13;
            this.commandRetriesRadProgressBar.TabIndex = 23;
            this.commandRetriesRadProgressBar.Text = "Command Retries";
            this.commandRetriesRadProgressBar.ThemeName = "Office2007Black";
            // 
            // downloadTypeRadLabel
            // 
            this.downloadTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.downloadTypeRadLabel.Location = new System.Drawing.Point(12, 34);
            this.downloadTypeRadLabel.Name = "downloadTypeRadLabel";
            this.downloadTypeRadLabel.Size = new System.Drawing.Size(96, 16);
            this.downloadTypeRadLabel.TabIndex = 22;
            this.downloadTypeRadLabel.Text = "Download Type is";
            this.downloadTypeRadLabel.ThemeName = "Office2007Black";
            // 
            // cancelRetryRadButton
            // 
            this.cancelRetryRadButton.Location = new System.Drawing.Point(12, 111);
            this.cancelRetryRadButton.Name = "cancelRetryRadButton";
            this.cancelRetryRadButton.Size = new System.Drawing.Size(155, 24);
            this.cancelRetryRadButton.TabIndex = 21;
            this.cancelRetryRadButton.Text = "Cancel Operation";
            this.cancelRetryRadButton.ThemeName = "Office2007Black";
            this.cancelRetryRadButton.Click += new System.EventHandler(this.cancelRetryRadButton_Click);
            // 
            // cancelRadWaitingBar
            // 
            this.cancelRadWaitingBar.Location = new System.Drawing.Point(173, 111);
            this.cancelRadWaitingBar.Name = "cancelRadWaitingBar";
            this.cancelRadWaitingBar.Size = new System.Drawing.Size(120, 24);
            this.cancelRadWaitingBar.TabIndex = 24;
            this.cancelRadWaitingBar.Text = "radWaitingBar1";
            this.cancelRadWaitingBar.ThemeName = "Office2007Black";
            this.cancelRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.cancelRadWaitingBar.WaitingSpeed = 10;
            // 
            // commandFailedRadLabel
            // 
            this.commandFailedRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandFailedRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.commandFailedRadLabel.Location = new System.Drawing.Point(12, 12);
            this.commandFailedRadLabel.Name = "commandFailedRadLabel";
            this.commandFailedRadLabel.Size = new System.Drawing.Size(92, 16);
            this.commandFailedRadLabel.TabIndex = 25;
            this.commandFailedRadLabel.Text = "Command Failed";
            this.commandFailedRadLabel.ThemeName = "Office2007Black";
            // 
            // RetryWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(418, 156);
            this.Controls.Add(this.commandFailedRadLabel);
            this.Controls.Add(this.cancelRadWaitingBar);
            this.Controls.Add(this.commandRetriesRadProgressBar);
            this.Controls.Add(this.downloadTypeRadLabel);
            this.Controls.Add(this.cancelRetryRadButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RetryWindow";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Command Retry";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.RetryWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.commandRetriesRadProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRetryRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.commandFailedRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadProgressBar commandRetriesRadProgressBar;
        private Telerik.WinControls.UI.RadLabel downloadTypeRadLabel;
        private Telerik.WinControls.UI.RadButton cancelRetryRadButton;
        private Telerik.WinControls.UI.RadWaitingBar cancelRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel commandFailedRadLabel;
    }
}

