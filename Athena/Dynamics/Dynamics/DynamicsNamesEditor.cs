﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;

namespace Dynamics
{
    public partial class DynamicsNamesEditor : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string humidityText = "Humidity (%)";
        private static string moistureText = "Moisture (kg/M3)";
        private static string temp1Text = "Correlating Temp (ºC)";
        private static string temp2Text = "Temp 2 (ºC)";
        private static string temp3Text = "Temp 3 (ºC)";
        private static string temp4Text = "Temp 4 (ºC)";

        private static string loadCurrent1Text = "Load Current 1 (A)";
        private static string loadCurrent2Text = "Load Current 2 (A)";
        private static string loadCurrent3Text = "Load Current 3 (A)";
        private static string voltage1Text = "Voltage 1 (kV)";
        private static string voltage2Text = "Voltage 2 (kV)";
        private static string voltage3Text = "Voltage 3 (kV)";

        private static string analog1Text = "Analog 1";
        private static string analog2Text = "Analog 2";
        private static string analog3Text = "Analog 3";
        private static string analog4Text = "Analog 4";
        private static string analog5Text = "Analog 5";
        private static string analog6Text = "Analog 6";

        private static string vibration1Text = "Vibration 1";
        private static string vibration2Text = "Vibration 2";
        private static string vibration3Text = "Vibration 3";
        private static string vibration4Text = "Vibration 4";

        private static string addText = "add";
        private static string multiplyText = "multiply";

        private static string dynamicsNamesEditorTitleText = "<html><font=Microsoft Sans Serif><size=8.25>Edit Dynamics Names</html>";
        private static string originalInterfaceTextRadLabelText = "<html><font=Microsoft Sans Serif><size=8.25>Original Interface Text<br>(reformatted for clarity)</html>";
        private static string newTextToDisplayRadLabelText = "<html><font=Microsoft Sans Serif><size=8.25>New Text to Display</html>";
        private static string scaleFactorRadLabelText = "<html><font=Microsoft Sans Serif><size=8.25>Scale Factor</html>";
        private static string applicationOfScaleFactorRadLabelText = "<html><font=Microsoft Sans Serif><size=8.25>Application of<br>Scale Factor</html>";
        private static string dynamicsRadGroupBoxText = "<html><font=Microsoft Sans Serif><size=8.25>Dynamics</html>";
        private static string humidityRadCheckBoxText = "Humidity (%)";
        private static string moistureRadCheckBoxText = "Moisture (kg/M3)";
        private static string temp1RadCheckBoxText = "Correlating Temp (ºC)";
        private static string temp2RadCheckBoxText = "Temp 2 (ºC)";
        private static string temp3RadCheckBoxText = "Temp 3 (ºC)";
        private static string temp4RadCheckBoxText = "Temp 4 (ºC)";
        private static string loadCurrent1RadCheckBoxText = "Load Current 1 (A)";
        private static string loadCurrent2RadCheckBoxText = "Load Current 2 (A)";
        private static string loadCurrent3RadCheckBoxText = "Load Current 3 (A)";
        private static string voltage1RadCheckBoxText = "Voltage 1 (kV)";
        private static string voltage2RadCheckBoxText = "Voltage 2 (kV)";
        private static string voltage3RadCheckBoxText = "Voltage 3 (kV)";
        private static string analog1RadCheckBoxText = "Analog 1";
        private static string analog2RadCheckBoxText = "Analog 2";
        private static string analog3RadCheckBoxText = "Analog 3";
        private static string analog4RadCheckBoxText = "Analog 4";
        private static string analog5RadCheckBoxText = "Analog 5";
        private static string analog6RadCheckBoxText = "Analog 6";
        private static string vibration1RadCheckBoxText = "Vibration 1";
        private static string vibration2RadCheckBoxText = "Vibration 2";
        private static string vibration3RadCheckBoxText = "Vibration 3";
        private static string vibration4RadCheckBoxText = "Vibration 4";
        private static string resetDefaultValuesRadButtonText = "<html><font=Microsoft Sans Serif><size=8.25>Reset Values<br>to Defaults</html>";
        private static string resetLastSavedValuesRadButtonText = "<html><font=Microsoft Sans Serif><size=8.25>ResetValues<br>to Last Saved</html>";
        private static string saveChangesRadButtonText = "<html><font=Microsoft Sans Serif><size=8.25>Save Changes<br>and Exit</html>";
        private static string cancelChangesRadButtonText = "<html><font=Microsoft Sans Serif><size=8.25>Cancel Changes<br>and Exit</html>";

        private static string incorrectEntryInScaleFactorTextBoxText = "<html><font=Microsoft Sans Serif><size=8.25>Illegal character in Scale Factor text box or a value is missing.</html>";

        private string humidityDefaultText = humidityText;
        private string humidityInputText;
        private string humidityNewText;
        public string HumidityNewText
        {
            get
            {
                return humidityNewText;
            }
        }

        private string moistureDefaultText = moistureText;
        private string moistureInputText;
        private string moistureNewText;
        public string MoistureNewText
        {
            get
            {
                return moistureNewText;
            }
        }

        private string temp1DefaultText = temp1Text;
        private string temp1InputText;
        private string temp1NewText;
        public string Temp1NewText
        {
            get
            {
                return temp1NewText;
            }
        }

        private string temp2DefaultText = temp2Text;
        private string temp2InputText;
        private string temp2NewText;
        public string Temp2NewText
        {
            get
            {
                return temp2NewText;
            }
        }

        private string temp3DefaultText = temp3Text;
        private string temp3InputText;
        private string temp3NewText;
        public string Temp3NewText
        {
            get
            {
                return temp3NewText;
            }
        }

        private string temp4DefaultText = temp4Text;
        private string temp4InputText;
        private string temp4NewText;
        public string Temp4NewText
        {
            get
            {
                return temp4NewText;
            }
        }

        private string loadCurrent1DefaultText = loadCurrent1Text;
        private string loadCurrent1InputText;
        private string loadCurrent1NewText;
        public string LoadCurrent1NewText
        {
            get
            {
                return loadCurrent1NewText;
            }
        }

        private string loadCurrent2DefaultText = loadCurrent2Text;
        private string loadCurrent2InputText;
        private string loadCurrent2NewText;
        public string LoadCurrent2NewText
        {
            get
            {
                return loadCurrent2NewText;
            }
        }

        private string loadCurrent3DefaultText = loadCurrent3Text;
        private string loadCurrent3InputText;
        private string loadCurrent3NewText;
        public string LoadCurrent3NewText
        {
            get
            {
                return loadCurrent3NewText;
            }
        }

        private string voltage1DefaultText = voltage1Text;
        private string voltage1InputText;
        private string voltage1NewText;
        public string Voltage1NewText
        {
            get
            {
                return voltage1NewText;
            }
        }

        private string voltage2DefaultText = voltage2Text;
        private string voltage2InputText;
        private string voltage2NewText;
        public string Voltage2NewText
        {
            get
            {
                return voltage2NewText;
            }
        }

        private string voltage3DefaultText = voltage3Text;
        private string voltage3InputText;
        private string voltage3NewText;
        public string Voltage3NewText
        {
            get
            {
                return voltage3NewText;
            }
        }

        private string analog1DefaultText = analog1Text;
        private string analog1InputText;
        private string analog1NewText;
        public string Analog1NewText
        {
            get
            {
                return analog1NewText;
            }
        }

        private string analog2DefaultText = analog2Text;
        private string analog2InputText;
        private string analog2NewText;
        public string Analog2NewText
        {
            get
            {
                return analog2NewText;
            }
        }

        private string analog3DefaultText = analog3Text;
        private string analog3InputText;
        private string analog3NewText;
        public string Analog3NewText
        {
            get
            {
                return analog3NewText;
            }
        }

        private string analog4DefaultText = analog4Text;
        private string analog4InputText;
        private string analog4NewText;
        public string Analog4NewText
        {
            get
            {
                return analog4NewText;
            }
        }

        private string analog5DefaultText = analog5Text;
        private string analog5InputText;
        private string analog5NewText;
        public string Analog5NewText
        {
            get
            {
                return analog5NewText;
            }
        }

        private string analog6DefaultText = analog6Text;
        private string analog6InputText;
        private string analog6NewText;
        public string Analog6NewText
        {
            get
            {
                return analog6NewText;
            }
        }

        private string vibration1DefaultText = vibration1Text;
        private string vibration1InputText;
        private string vibration1NewText;
        public string Vibration1NewText
        {
            get
            {
                return vibration1NewText;
            }
        }

        private string vibration2DefaultText = vibration2Text;
        private string vibration2InputText;
        private string vibration2NewText;
        public string Vibration2NewText
        {
            get
            {
                return vibration2NewText;
            }
        }

        private string vibration3DefaultText = vibration3Text;
        private string vibration3InputText;
        private string vibration3NewText;
        public string Vibration3NewText
        {
            get
            {
                return vibration3NewText;
            }
        }

        private string vibration4DefaultText = vibration4Text;
        private string vibration4InputText;
        private string vibration4NewText;
        public string Vibration4NewText
        {
            get
            {
                return vibration4NewText;
            }
        }

        private double humidityDefaultScaleFactor = 0;
        private double humidityInputScaleFactor;
        private double humidityNewScaleFactor;
        public double HumidityNewScaleFactor
        {
            get
            {
                return humidityNewScaleFactor;
            }
        }

        private double moistureDefaultScaleFactor = 0;
        private double moistureInputScaleFactor;
        private double moistureNewScaleFactor;
        public double MoistureNewScaleFactor
        {
            get
            {
                return moistureNewScaleFactor;
            }
        }

        private double temp1DefaultScaleFactor = 0;
        private double temp1InputScaleFactor;
        private double temp1NewScaleFactor;
        public double Temp1NewScaleFactor
        {
            get
            {
                return temp1NewScaleFactor;
            }
        }

        private double temp2DefaultScaleFactor = 0;
        private double temp2InputScaleFactor;
        private double temp2NewScaleFactor;
        public double Temp2NewScaleFactor
        {
            get
            {
                return temp2NewScaleFactor;
            }
        }

        private double temp3DefaultScaleFactor = 0;
        private double temp3InputScaleFactor;
        private double temp3NewScaleFactor;
        public double Temp3NewScaleFactor
        {
            get
            {
                return temp3NewScaleFactor;
            }
        }

        private double temp4DefaultScaleFactor = 0;
        private double temp4InputScaleFactor;
        private double temp4NewScaleFactor;
        public double Temp4NewScaleFactor
        {
            get
            {
                return temp4NewScaleFactor;
            }
        }

        private double loadCurrent1DefaultScaleFactor = 1.0;
        private double loadCurrent1InputScaleFactor;
        private double loadCurrent1NewScaleFactor;
        public double LoadCurrent1NewScaleFactor
        {
            get
            {
                return loadCurrent1NewScaleFactor;
            }
        }

        private double loadCurrent2DefaultScaleFactor = 1.0;
        private double loadCurrent2InputScaleFactor;
        private double loadCurrent2NewScaleFactor;
        public double LoadCurrent2NewScaleFactor
        {
            get
            {
                return loadCurrent2NewScaleFactor;
            }
        }

        private double loadCurrent3DefaultScaleFactor = 1.0;
        private double loadCurrent3InputScaleFactor;
        private double loadCurrent3NewScaleFactor;
        public double LoadCurrent3NewScaleFactor
        {
            get
            {
                return loadCurrent3NewScaleFactor;
            }
        }

        private double voltage1DefaultScaleFactor = 1.0;
        private double voltage1InputScaleFactor;
        private double voltage1NewScaleFactor;
        public double Voltage1NewScaleFactor
        {
            get
            {
                return voltage1NewScaleFactor;
            }
        }

        private double voltage2DefaultScaleFactor = 1.0;
        private double voltage2InputScaleFactor;
        private double voltage2NewScaleFactor;
        public double Voltage2NewScaleFactor
        {
            get
            {
                return voltage2NewScaleFactor;
            }
        }

        private double voltage3DefaultScaleFactor = 1.0;
        private double voltage3InputScaleFactor;
        private double voltage3NewScaleFactor;
        public double Voltage3NewScaleFactor
        {
            get
            {
                return voltage3NewScaleFactor;
            }
        }

        private double analog1DefaultScaleFactor = 1.0;
        private double analog1InputScaleFactor;
        private double analog1NewScaleFactor;
        public double Analog1NewScaleFactor
        {
            get
            {
                return analog1NewScaleFactor;
            }
        }

        private double analog2DefaultScaleFactor = 1.0;
        private double analog2InputScaleFactor;
        private double analog2NewScaleFactor;
        public double Analog2NewScaleFactor
        {
            get
            {
                return analog2NewScaleFactor;
            }
        }

        private double analog3DefaultScaleFactor = 1.0;
        private double analog3InputScaleFactor;
        private double analog3NewScaleFactor;
        public double Analog3NewScaleFactor
        {
            get
            {
                return analog3NewScaleFactor;
            }
        }

        private double analog4DefaultScaleFactor = 1.0;
        private double analog4InputScaleFactor;
        private double analog4NewScaleFactor;
        public double Analog4NewScaleFactor
        {
            get
            {
                return analog4NewScaleFactor;
            }
        }

        private double analog5DefaultScaleFactor = 1.0;
        private double analog5InputScaleFactor;
        private double analog5NewScaleFactor;
        public double Analog5NewScaleFactor
        {
            get
            {
                return analog5NewScaleFactor;
            }
        }

        private double analog6DefaultScaleFactor = 1.0;
        private double analog6InputScaleFactor;
        private double analog6NewScaleFactor;
        public double Analog6NewScaleFactor
        {
            get
            {
                return analog6NewScaleFactor;
            }
        }

        private double vibration1DefaultScaleFactor = 1.0;
        private double vibration1InputScaleFactor;
        private double vibration1NewScaleFactor;
        public double Vibration1NewScaleFactor
        {
            get
            {
                return vibration1NewScaleFactor;
            }
        }

        private double vibration2DefaultScaleFactor = 1.0;
        private double vibration2InputScaleFactor;
        private double vibration2NewScaleFactor;
        public double Vibration2NewScaleFactor
        {
            get
            {
                return vibration2NewScaleFactor;
            }
        }

        private double vibration3DefaultScaleFactor = 1.0;
        private double vibration3InputScaleFactor;
        private double vibration3NewScaleFactor;
        public double Vibration3NewScaleFactor
        {
            get
            {
                return vibration3NewScaleFactor;
            }
        }

        private double vibration4DefaultScaleFactor = 1.0;
        private double vibration4InputScaleFactor;
        private double vibration4NewScaleFactor;
        public double Vibration4NewScaleFactor
        {
            get
            {
                return vibration4NewScaleFactor;
            }
        }

        private string humidityDefaultOperation = addText;
        private string humidityInputOperation;
        private string humidityNewOperation;
        public string HumidityNewOperation
        {
            get
            {
                return humidityNewOperation;
            }
        }

        private string moistureDefaultOperation = addText;
        private string moistureInputOperation;
        private string moistureNewOperation;
        public string MoistureNewOperation
        {
            get
            {
                return moistureNewOperation;
            }
        }

        private string temp1DefaultOperation = addText;
        private string temp1InputOperation;
        private string temp1NewOperation;
        public string Temp1NewOperation
        {
            get
            {
                return temp1NewOperation;
            }
        }

        private string temp2DefaultOperation = addText;
        private string temp2InputOperation;
        private string temp2NewOperation;
        public string Temp2NewOperation
        {
            get
            {
                return temp2NewOperation;
            }
        }

        private string temp3DefaultOperation = addText;
        private string temp3InputOperation;
        private string temp3NewOperation;
        public string Temp3NewOperation
        {
            get
            {
                return temp3NewOperation;
            }
        }

        private string temp4DefaultOperation = addText;
        private string temp4InputOperation;
        private string temp4NewOperation;
        public string Temp4NewOperation
        {
            get
            {
                return temp4NewOperation;
            }
        }

        private string loadCurrent1DefaultOperation = multiplyText;
        private string loadCurrent1InputOperation;
        private string loadCurrent1NewOperation;
        public string LoadCurrent1NewOperation
        {
            get
            {
                return loadCurrent1NewOperation;
            }
        }

        private string loadCurrent2DefaultOperation = multiplyText;
        private string loadCurrent2InputOperation;
        private string loadCurrent2NewOperation;
        public string LoadCurrent2NewOperation
        {
            get
            {
                return loadCurrent2NewOperation;
            }
        }

        private string loadCurrent3DefaultOperation = multiplyText;
        private string loadCurrent3InputOperation;
        private string loadCurrent3NewOperation;
        public string LoadCurrent3NewOperation
        {
            get
            {
                return loadCurrent3NewOperation;
            }
        }

        private string voltage1DefaultOperation = multiplyText;
        private string voltage1InputOperation;
        private string voltage1NewOperation;
        public string Voltage1NewOperation
        {
            get
            {
                return voltage1NewOperation;
            }
        }

        private string voltage2DefaultOperation = multiplyText;
        private string voltage2InputOperation;
        private string voltage2NewOperation;
        public string Voltage2NewOperation
        {
            get
            {
                return voltage2NewOperation;
            }
        }

        private string voltage3DefaultOperation = multiplyText;
        private string voltage3InputOperation;
        private string voltage3NewOperation;
        public string Voltage3NewOperation
        {
            get
            {
                return voltage3NewOperation;
            }
        }

        private string analog1DefaultOperation = multiplyText;
        private string analog1InputOperation;
        private string analog1NewOperation;
        public string Analog1NewOperation
        {
            get
            {
                return analog1NewOperation;
            }
        }

        private string analog2DefaultOperation = multiplyText;
        private string analog2InputOperation;
        private string analog2NewOperation;
        public string Analog2NewOperation
        {
            get
            {
                return analog2NewOperation;
            }
        }

        private string analog3DefaultOperation = multiplyText;
        private string analog3InputOperation;
        private string analog3NewOperation;
        public string Analog3NewOperation
        {
            get
            {
                return analog3NewOperation;
            }
        }

        private string analog4DefaultOperation = multiplyText;
        private string analog4InputOperation;
        private string analog4NewOperation;
        public string Analog4NewOperation
        {
            get
            {
                return analog4NewOperation;
            }
        }

        private string analog5DefaultOperation = multiplyText;
        private string analog5InputOperation;
        private string analog5NewOperation;
        public string Analog5NewOperation
        {
            get
            {
                return analog5NewOperation;
            }
        }

        private string analog6DefaultOperation = multiplyText;
        private string analog6InputOperation;
        private string analog6NewOperation;
        public string Analog6NewOperation
        {
            get
            {
                return analog6NewOperation;
            }
        }

        private string vibration1DefaultOperation = multiplyText;
        private string vibration1InputOperation;
        private string vibration1NewOperation;
        public string Vibration1NewOperation
        {
            get
            {
                return vibration1NewOperation;
            }
        }

        private string vibration2DefaultOperation = multiplyText;
        private string vibration2InputOperation;
        private string vibration2NewOperation;
        public string Vibration2NewOperation
        {
            get
            {
                return vibration2NewOperation;
            }
        }

        private string vibration3DefaultOperation = multiplyText;
        private string vibration3InputOperation;
        private string vibration3NewOperation;
        public string Vibration3NewOperation
        {
            get
            {
                return vibration3NewOperation;
            }
        }

        private string vibration4DefaultOperation = multiplyText;
        private string vibration4InputOperation;
        private string vibration4NewOperation;
        public string Vibration4NewOperation
        {
            get
            {
                return vibration4NewOperation;
            }
        }

        private bool resetValues = false;
        public bool ResetValues
        {
            get
            {
                return resetValues;
            }
        }

        bool isBHMApplication = false;
        bool isPDMApplication = false;

        ProgramBrand programBrand;
        ProgramType programType;
        DataViewerType dataViewerType;

        public DynamicsNamesEditor(ProgramBrand inputProgramBrand, ProgramType inputProgramType, DataViewerType inputDataViewerType, 
                                   string inputHumidityText, string inputMoistureText, string inputTemp1Text, string inputTemp2Text, string inputTemp3Text, string inputTemp4Text,
                                   string inputLoadCurrent1Text, string inputLoadCurrent2Text, string inputLoadCurrent3Text,
                                   string inputVoltage1Text, string inputVoltage2Text, string inputVoltage3Text,
                                   string inputAnalog1Text, string inputAnalog2Text, string inputAnalog3Text, string inputAnalog4Text, string inputAnalog5Text, string inputAnalog6Text,
                                   string inputVibration1Text, string inputVibration2Text, string inputVibration3Text, string inputVibration4Text, 
                                   double inputHumidityScaleFactor, double inputMoistureScaleFactor, double inputTemp1ScaleFactor, double inputTemp2ScaleFactor, double inputTemp3ScaleFactor, double inputTemp4ScaleFactor,
                                   double inputLoadCurrent1ScaleFactor, double inputLoadCurrent2ScaleFactor, double inputLoadCurrent3ScaleFactor,
                                   double inputVoltage1ScaleFactor, double inputVoltage2ScaleFactor, double inputVoltage3ScaleFactor,
                                   double inputAnalog1ScaleFactor, double inputAnalog2ScaleFactor, double inputAnalog3ScaleFactor, double inputAnalog4ScaleFactor, double inputAnalog5ScaleFactor, double inputAnalog6ScaleFactor,
                                   double inputVibration1ScaleFactor, double inputVibration2ScaleFactor, double inputVibration3ScaleFactor, double inputVibration4ScaleFactor, 
                                   string inputHumidityOperation, string inputMoistureOperation, string inputTemp1Operation, string inputTemp2Operation, string inputTemp3Operation, string inputTemp4Operation,
                                   string inputLoadCurrent1Operation, string inputLoadCurrent2Operation, string inputLoadCurrent3Operation,
                                   string inputVoltage1Operation, string inputVoltage2Operation, string inputVoltage3Operation,
                                   string inputAnalog1Operation, string inputAnalog2Operation, string inputAnalog3Operation, string inputAnalog4Operation, string inputAnalog5Operation, string inputAnalog6Operation,
                                   string inputVibration1Operation,  string inputVibration2Operation, string inputVibration3Operation, string inputVibration4Operation)
        {
            InitializeComponent();

            this.programBrand = inputProgramBrand;
            this.programType = inputProgramType;
            this.dataViewerType = inputDataViewerType;

            humidityInputText = inputHumidityText;
            moistureInputText = inputMoistureText;
            temp1InputText = inputTemp1Text;
            temp2InputText = inputTemp2Text;
            temp3InputText = inputTemp3Text;
            temp4InputText = inputTemp4Text;
            loadCurrent1InputText = inputLoadCurrent1Text;
            loadCurrent2InputText = inputLoadCurrent2Text;
            loadCurrent3InputText = inputLoadCurrent3Text;
            voltage1InputText = inputVoltage1Text;
            voltage2InputText = inputVoltage2Text;
            voltage3InputText = inputVoltage3Text;
            analog1InputText = inputAnalog1Text;
            analog2InputText = inputAnalog2Text;
            analog3InputText = inputAnalog3Text;
            analog4InputText = inputAnalog4Text;
            analog5InputText = inputAnalog5Text;
            analog6InputText = inputAnalog6Text;
            vibration1InputText = inputVibration1Text;
            vibration2InputText = inputVibration2Text;
            vibration3InputText = inputVibration3Text;
            vibration4InputText = inputVibration4Text;

            humidityInputScaleFactor = inputHumidityScaleFactor;
            moistureInputScaleFactor = inputMoistureScaleFactor;
            temp1InputScaleFactor = inputTemp1ScaleFactor;
            temp2InputScaleFactor = inputTemp2ScaleFactor;
            temp3InputScaleFactor = inputTemp3ScaleFactor;
            temp4InputScaleFactor = inputTemp4ScaleFactor;
            loadCurrent1InputScaleFactor = inputLoadCurrent1ScaleFactor;
            loadCurrent2InputScaleFactor = inputLoadCurrent2ScaleFactor;
            loadCurrent3InputScaleFactor = inputLoadCurrent3ScaleFactor;
            voltage1InputScaleFactor = inputVoltage1ScaleFactor;
            voltage2InputScaleFactor = inputVoltage2ScaleFactor;
            voltage3InputScaleFactor = inputVoltage3ScaleFactor;
            analog1InputScaleFactor = inputAnalog1ScaleFactor;
            analog2InputScaleFactor = inputAnalog2ScaleFactor;
            analog3InputScaleFactor = inputAnalog3ScaleFactor;
            analog4InputScaleFactor = inputAnalog4ScaleFactor;
            analog5InputScaleFactor = inputAnalog5ScaleFactor;
            analog6InputScaleFactor = inputAnalog6ScaleFactor;
            vibration1InputScaleFactor = inputVibration1ScaleFactor;
            vibration2InputScaleFactor = inputVibration2ScaleFactor;
            vibration3InputScaleFactor = inputVibration3ScaleFactor;
            vibration4InputScaleFactor = inputVibration4ScaleFactor;

            humidityInputOperation = inputHumidityOperation;
            moistureInputOperation = inputMoistureOperation;
            temp1InputOperation = inputTemp1Operation;
            temp2InputOperation = inputTemp2Operation;
            temp3InputOperation = inputTemp3Operation;
            temp4InputOperation = inputTemp4Operation;
            loadCurrent1InputOperation = inputLoadCurrent1Operation;
            loadCurrent2InputOperation = inputLoadCurrent2Operation;
            loadCurrent3InputOperation = inputLoadCurrent3Operation;
            voltage1InputOperation = inputVoltage1Operation;
            voltage2InputOperation = inputVoltage2Operation;
            voltage3InputOperation = inputVoltage3Operation;
            analog1InputOperation = inputAnalog1Operation;
            analog2InputOperation = inputAnalog2Operation;
            analog3InputOperation = inputAnalog3Operation;
            analog4InputOperation = inputAnalog4Operation;
            analog5InputOperation = inputAnalog5Operation;
            analog6InputOperation = inputAnalog6Operation;
            vibration1InputOperation = inputVibration1Operation;
            vibration2InputOperation = inputVibration2Operation;
            vibration3InputOperation = inputVibration3Operation;
            vibration4InputOperation = inputVibration4Operation;

            AssignStringValuesToInterfaceObjects();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        public DynamicsNamesEditor(ProgramBrand inputProgramBrand, ProgramType inputProgramType, DataViewerType inputDataViewerType, 
                                   string inputHumidityText, string inputMoistureText, string inputTemp1Text, string inputTemp2Text, string inputTemp3Text, string inputTemp4Text,
                                   string inputLoadCurrent1Text, string inputLoadCurrent2Text, string inputLoadCurrent3Text,
                                   string inputVoltage1Text, string inputVoltage2Text, string inputVoltage3Text,
                                   double inputHumidityScaleFactor, double inputMoistureScaleFactor, double inputTemp1ScaleFactor, double inputTemp2ScaleFactor, double inputTemp3ScaleFactor, double inputTemp4ScaleFactor,
                                   double inputLoadCurrent1ScaleFactor, double inputLoadCurrent2ScaleFactor, double inputLoadCurrent3ScaleFactor,
                                   double inputVoltage1ScaleFactor, double inputVoltage2ScaleFactor, double inputVoltage3ScaleFactor,
                                   string inputHumidityOperation, string inputMoistureOperation, string inputTemp1Operation, string inputTemp2Operation, string inputTemp3Operation, string inputTemp4Operation,
                                   string inputLoadCurrent1Operation, string inputLoadCurrent2Operation, string inputLoadCurrent3Operation,
                                   string inputVoltage1Operation, string inputVoltage2Operation, string inputVoltage3Operation)
        {
            InitializeComponent();

            ShrinkInterfaceToPDMApplicationSize();

            this.programBrand = inputProgramBrand;
            this.programType = inputProgramType;
            this.dataViewerType = inputDataViewerType;

            humidityInputText = inputHumidityText;
            moistureInputText = inputMoistureText;
            temp1InputText = inputTemp1Text;
            temp2InputText = inputTemp2Text;
            temp3InputText = inputTemp3Text;
            temp4InputText = inputTemp4Text;
            loadCurrent1InputText = inputLoadCurrent1Text;
            loadCurrent2InputText = inputLoadCurrent2Text;
            loadCurrent3InputText = inputLoadCurrent3Text;
            voltage1InputText = inputVoltage1Text;
            voltage2InputText = inputVoltage2Text;
            voltage3InputText = inputVoltage3Text;

            humidityInputScaleFactor = inputHumidityScaleFactor;
            moistureInputScaleFactor = inputMoistureScaleFactor;
            temp1InputScaleFactor = inputTemp1ScaleFactor;
            temp2InputScaleFactor = inputTemp2ScaleFactor;
            temp3InputScaleFactor = inputTemp3ScaleFactor;
            temp4InputScaleFactor = inputTemp4ScaleFactor;
            loadCurrent1InputScaleFactor = inputLoadCurrent1ScaleFactor;
            loadCurrent2InputScaleFactor = inputLoadCurrent2ScaleFactor;
            loadCurrent3InputScaleFactor = inputLoadCurrent3ScaleFactor;
            voltage1InputScaleFactor = inputVoltage1ScaleFactor;
            voltage2InputScaleFactor = inputVoltage2ScaleFactor;
            voltage3InputScaleFactor = inputVoltage3ScaleFactor;

            humidityInputOperation = inputHumidityOperation;
            moistureInputOperation = inputMoistureOperation;
            temp1InputOperation = inputTemp1Operation;
            temp2InputOperation = inputTemp2Operation;
            temp3InputOperation = inputTemp3Operation;
            temp4InputOperation = inputTemp4Operation;
            loadCurrent1InputOperation = inputLoadCurrent1Operation;
            loadCurrent2InputOperation = inputLoadCurrent2Operation;
            loadCurrent3InputOperation = inputLoadCurrent3Operation;
            voltage1InputOperation = inputVoltage1Operation;
            voltage2InputOperation = inputVoltage2Operation;
            voltage3InputOperation = inputVoltage3Operation;

            AssignStringValuesToInterfaceObjects();
            this.StartPosition = FormStartPosition.CenterParent;

            if (inputProgramType == ProgramType.PD15)
            {
                ShrinkInterfaceToPD15Size();
            }
        }

        public DynamicsNamesEditor(ProgramBrand inputProgramBrand, ProgramType inputProgramType, DataViewerType inputDataViewerType, 
                                   string inputHumidityText, string inputMoistureText, string inputTemp1Text, string inputTemp2Text, string inputTemp3Text, string inputTemp4Text,
                                   string inputExtLoadActiveText,
                                   double inputHumidityScaleFactor, double inputMoistureScaleFactor, double inputTemp1ScaleFactor, double inputTemp2ScaleFactor, double inputTemp3ScaleFactor, double inputTemp4ScaleFactor,
                                   double inputExtLoadActiveScaleFactor,
                                   string inputHumidityOperation, string inputMoistureOperation, string inputTemp1Operation, string inputTemp2Operation, string inputTemp3Operation, string inputTemp4Operation,
                                   string inputExtLoadActiveOperation)
        {
            InitializeComponent();

            ShrinkInterfaceToBHMApplicationSize();

            this.programBrand = inputProgramBrand;
            this.programType = inputProgramType;
            this.dataViewerType = inputDataViewerType;

            humidityInputText = inputHumidityText;
            moistureInputText = inputMoistureText;
            temp1InputText = inputTemp1Text;
            temp2InputText = inputTemp2Text;
            temp3InputText = inputTemp3Text;
            temp4InputText = inputTemp4Text;
            loadCurrent1InputText = inputExtLoadActiveText;

            humidityInputScaleFactor = inputHumidityScaleFactor;
            moistureInputScaleFactor = inputMoistureScaleFactor;
            temp1InputScaleFactor = inputTemp1ScaleFactor;
            temp2InputScaleFactor = inputTemp2ScaleFactor;
            temp3InputScaleFactor = inputTemp3ScaleFactor;
            temp4InputScaleFactor = inputTemp4ScaleFactor;
            loadCurrent1InputScaleFactor = inputExtLoadActiveScaleFactor;

            humidityInputOperation = inputHumidityOperation;
            moistureInputOperation = inputMoistureOperation;
            temp1InputOperation = inputTemp1Operation;
            temp2InputOperation = inputTemp2Operation;
            temp3InputOperation = inputTemp3Operation;
            temp4InputOperation = inputTemp4Operation;
            loadCurrent1InputOperation = inputExtLoadActiveOperation;
          
            AssignStringValuesToInterfaceObjects();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void ShrinkInterfaceToPD15Size()
        {
            try
            {
                int newYLocation = 178;

                temp2RadCheckBox.Visible = false;
                temp3RadCheckBox.Visible = false;
                temp4RadCheckBox.Visible = false;
                loadCurrent2RadCheckBox.Visible = false;
                loadCurrent3RadCheckBox.Visible = false;
                voltage1RadCheckBox.Visible = false;
                voltage2RadCheckBox.Visible = false;
                voltage3RadCheckBox.Visible = false;

                analog1RadCheckBox.Visible = false;
                analog2RadCheckBox.Visible = false;
                analog3RadCheckBox.Visible = false;
                analog4RadCheckBox.Visible = false;
                analog5RadCheckBox.Visible = false;
                analog6RadCheckBox.Visible = false;

                vibration1RadCheckBox.Visible = false;
                vibration2RadCheckBox.Visible = false;
                vibration3RadCheckBox.Visible = false;
                vibration4RadCheckBox.Visible = false;

                temp2TextRadDropDownList.Visible = false;
                temp3TextRadDropDownList.Visible = false;
                temp4TextRadDropDownList.Visible = false;
                loadCurrent2TextRadDropDownList.Visible = false;
                loadCurrent3TextRadDropDownList.Visible = false;
                voltage1TextRadDropDownList.Visible = false;
                voltage2TextRadDropDownList.Visible = false;
                voltage3TextRadDropDownList.Visible = false;

                analog1TextRadDropDownList.Visible = false;
                analog2TextRadDropDownList.Visible = false;
                analog3TextRadDropDownList.Visible = false;
                analog4TextRadDropDownList.Visible = false;
                analog5TextRadDropDownList.Visible = false;
                analog6TextRadDropDownList.Visible = false;

                vibration1TextRadDropDownList.Visible = false;
                vibration2TextRadDropDownList.Visible = false;
                vibration3TextRadDropDownList.Visible = false;
                vibration4TextRadDropDownList.Visible = false;

                temp2ScaleFactorRadTextBox.Visible = false;
                temp3ScaleFactorRadTextBox.Visible = false;
                temp4ScaleFactorRadTextBox.Visible = false;
                loadCurrent2ScaleFactorRadTextBox.Visible = false;
                loadCurrent3ScaleFactorRadTextBox.Visible = false;
                voltage1ScaleFactorRadTextBox.Visible = false;
                voltage2ScaleFactorRadTextBox.Visible = false;
                voltage3ScaleFactorRadTextBox.Visible = false;

                analog1ScaleFactorRadTextBox.Visible = false;
                analog2ScaleFactorRadTextBox.Visible = false;
                analog3ScaleFactorRadTextBox.Visible = false;
                analog4ScaleFactorRadTextBox.Visible = false;
                analog5ScaleFactorRadTextBox.Visible = false;
                analog6ScaleFactorRadTextBox.Visible = false;

                vibration1ScaleFactorRadTextBox.Visible = false;
                vibration2ScaleFactorRadTextBox.Visible = false;
                vibration3ScaleFactorRadTextBox.Visible = false;
                vibration4ScaleFactorRadTextBox.Visible = false;

                temp2OperationRadDropDownList.Visible = false;
                temp3OperationRadDropDownList.Visible = false;
                temp4OperationRadDropDownList.Visible = false;
                loadCurrent2OperationRadDropDownList.Visible = false;
                loadCurrent3OperationRadDropDownList.Visible = false;
                voltage1OperationRadDropDownList.Visible = false;
                voltage2OperationRadDropDownList.Visible = false;
                voltage3OperationRadDropDownList.Visible = false;

                analog1OperationRadDropDownList.Visible = false;
                analog2OperationRadDropDownList.Visible = false;
                analog3OperationRadDropDownList.Visible = false;
                analog4OperationRadDropDownList.Visible = false;
                analog5OperationRadDropDownList.Visible = false;
                analog6OperationRadDropDownList.Visible = false;

                vibration1OperationRadDropDownList.Visible = false;
                vibration2OperationRadDropDownList.Visible = false;
                vibration3OperationRadDropDownList.Visible = false;
                vibration4OperationRadDropDownList.Visible = false;

                loadCurrent1RadCheckBox.Location = new Point(13, 132);
                loadCurrent1TextRadDropDownList.Location = new Point(204, newYLocation);
                loadCurrent1ScaleFactorRadTextBox.Location = new Point(414, newYLocation);
                loadCurrent1OperationRadDropDownList.Location = new Point(510, newYLocation);

                this.MinimumSize = new Size(608, 330);
                this.MaximumSize = new Size(608, 330);
                this.Size = new Size(608, 330);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DynamicsNamesEditor.ShrinkInterfaceToPD15Size()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ShrinkInterfaceToBHMApplicationSize()
        {
            isBHMApplication = true;
            //this.temp1RadCheckBox.Text = "Temp 1 (ºC)";
            //this.temp1DefaultText = "Temp 1 (ºC)";

            loadCurrent2RadCheckBox.Visible = false;
            loadCurrent3RadCheckBox.Visible = false;
            voltage1RadCheckBox.Visible = false;
            voltage2RadCheckBox.Visible = false;
            voltage3RadCheckBox.Visible = false;

            analog1RadCheckBox.Visible = false;
            analog2RadCheckBox.Visible = false;
            analog3RadCheckBox.Visible = false;
            analog4RadCheckBox.Visible = false;
            analog5RadCheckBox.Visible = false;
            analog6RadCheckBox.Visible = false;

            vibration1RadCheckBox.Visible = false;
            vibration2RadCheckBox.Visible = false;
            vibration3RadCheckBox.Visible = false;
            vibration4RadCheckBox.Visible = false;

            loadCurrent2TextRadDropDownList.Visible = false;
            loadCurrent3TextRadDropDownList.Visible = false;
            voltage1TextRadDropDownList.Visible = false;
            voltage2TextRadDropDownList.Visible = false;
            voltage3TextRadDropDownList.Visible = false;

            analog1TextRadDropDownList.Visible = false;
            analog2TextRadDropDownList.Visible = false;
            analog3TextRadDropDownList.Visible = false;
            analog4TextRadDropDownList.Visible = false;
            analog5TextRadDropDownList.Visible = false;
            analog6TextRadDropDownList.Visible = false;

            vibration1TextRadDropDownList.Visible = false;
            vibration2TextRadDropDownList.Visible = false;
            vibration3TextRadDropDownList.Visible = false;
            vibration4TextRadDropDownList.Visible = false;

            loadCurrent2ScaleFactorRadTextBox.Visible = false;
            loadCurrent3ScaleFactorRadTextBox.Visible = false;
            voltage1ScaleFactorRadTextBox.Visible = false;
            voltage2ScaleFactorRadTextBox.Visible = false;
            voltage3ScaleFactorRadTextBox.Visible = false;

            analog1ScaleFactorRadTextBox.Visible = false;
            analog2ScaleFactorRadTextBox.Visible = false;
            analog3ScaleFactorRadTextBox.Visible = false;
            analog4ScaleFactorRadTextBox.Visible = false;
            analog5ScaleFactorRadTextBox.Visible = false;
            analog6ScaleFactorRadTextBox.Visible = false;

            vibration1ScaleFactorRadTextBox.Visible = false;
            vibration2ScaleFactorRadTextBox.Visible = false;
            vibration3ScaleFactorRadTextBox.Visible = false;
            vibration4ScaleFactorRadTextBox.Visible = false;

            loadCurrent2OperationRadDropDownList.Visible = false;
            loadCurrent3OperationRadDropDownList.Visible = false;
            voltage1OperationRadDropDownList.Visible = false;
            voltage2OperationRadDropDownList.Visible = false;
            voltage3OperationRadDropDownList.Visible = false;

            analog1OperationRadDropDownList.Visible = false;
            analog2OperationRadDropDownList.Visible = false;
            analog3OperationRadDropDownList.Visible = false;
            analog4OperationRadDropDownList.Visible = false;
            analog5OperationRadDropDownList.Visible = false;
            analog6OperationRadDropDownList.Visible = false;

            vibration1OperationRadDropDownList.Visible = false;
            vibration2OperationRadDropDownList.Visible = false;
            vibration3OperationRadDropDownList.Visible = false;
            vibration4OperationRadDropDownList.Visible = false;

            //this.dynamicsRadGroupBox.Size = new Size(164, 240);
            //this.resetDefaultValuesRadButton.Location = new Point(29, 268);
            //this.resetLastSavedValuesRadButton.Location = new Point(154, 268);
            //this.saveChangesRadButton.Location = new Point(280, 268);
            //this.cancelChangesRadButton.Location = new Point(406, 268);

            this.MinimumSize = new Size(608, 420);
            this.MaximumSize = new Size(608, 420);
            this.Size = new Size(608, 420);
        }

        private void ShrinkInterfaceToPDMApplicationSize()
        {
            isPDMApplication = true;
            //this.temp1RadCheckBox.Text = "Temp 1 (ºC)";
            //this.temp1DefaultText = "Temp 1 (ºC)";

            analog1RadCheckBox.Visible = false;
            analog2RadCheckBox.Visible = false;
            analog3RadCheckBox.Visible = false;
            analog4RadCheckBox.Visible = false;
            analog5RadCheckBox.Visible = false;
            analog6RadCheckBox.Visible = false;

            vibration1RadCheckBox.Visible = false;
            vibration2RadCheckBox.Visible = false;
            vibration3RadCheckBox.Visible = false;
            vibration4RadCheckBox.Visible = false;

            analog1TextRadDropDownList.Visible = false;
            analog2TextRadDropDownList.Visible = false;
            analog3TextRadDropDownList.Visible = false;
            analog4TextRadDropDownList.Visible = false;
            analog5TextRadDropDownList.Visible = false;
            analog6TextRadDropDownList.Visible = false;

            vibration1TextRadDropDownList.Visible = false;
            vibration2TextRadDropDownList.Visible = false;
            vibration3TextRadDropDownList.Visible = false;
            vibration4TextRadDropDownList.Visible = false;

            analog1ScaleFactorRadTextBox.Visible = false;
            analog2ScaleFactorRadTextBox.Visible = false;
            analog3ScaleFactorRadTextBox.Visible = false;
            analog4ScaleFactorRadTextBox.Visible = false;
            analog5ScaleFactorRadTextBox.Visible = false;
            analog6ScaleFactorRadTextBox.Visible = false;
            
            vibration1ScaleFactorRadTextBox.Visible = false;
            vibration2ScaleFactorRadTextBox.Visible = false;
            vibration3ScaleFactorRadTextBox.Visible = false;
            vibration4ScaleFactorRadTextBox.Visible = false;

            analog1OperationRadDropDownList.Visible = false;
            analog2OperationRadDropDownList.Visible = false;
            analog3OperationRadDropDownList.Visible = false;
            analog4OperationRadDropDownList.Visible = false;
            analog5OperationRadDropDownList.Visible = false;
            analog6OperationRadDropDownList.Visible = false;

            vibration1OperationRadDropDownList.Visible = false;
            vibration2OperationRadDropDownList.Visible = false;
            vibration3OperationRadDropDownList.Visible = false;
            vibration4OperationRadDropDownList.Visible = false;

            //this.dynamicsRadGroupBox.Size = new Size(164, 240);
            //this.resetDefaultValuesRadButton.Location = new Point(29, 268);
            //this.resetLastSavedValuesRadButton.Location = new Point(154, 268);
            //this.saveChangesRadButton.Location = new Point(280, 268);
            //this.cancelChangesRadButton.Location = new Point(406, 268);

            this.MinimumSize = new Size(608, 485);
            this.MaximumSize = new Size(608, 485);
            this.Size = new Size(608, 485);
        }



        private void DynamicsNamesEditor_Load(object sender, EventArgs e)
        {
            humidityTextRadDropDownList.DataSource = new string[] { humidityText };
            moistureTextRadDropDownList.DataSource = new string[] { moistureText };
            temp1TextRadDropDownList.DataSource = new string[] { temp1Text };
            temp2TextRadDropDownList.DataSource = new string[] { temp2Text };
            temp3TextRadDropDownList.DataSource = new string[] { temp3Text };
            temp4TextRadDropDownList.DataSource = new string[] { temp4Text };
            loadCurrent1TextRadDropDownList.DataSource = new string[] { loadCurrent1Text };
            loadCurrent2TextRadDropDownList.DataSource = new string[] { loadCurrent2Text };
            loadCurrent3TextRadDropDownList.DataSource = new string[] { loadCurrent3Text };
            voltage1TextRadDropDownList.DataSource = new string[] { voltage1Text };
            voltage2TextRadDropDownList.DataSource = new string[] { voltage2Text };
            voltage3TextRadDropDownList.DataSource = new string[] { voltage3Text };
            analog1TextRadDropDownList.DataSource = new string[] { analog1Text };
            analog2TextRadDropDownList.DataSource = new string[] { analog2Text };
            analog3TextRadDropDownList.DataSource = new string[] { analog3Text };
            analog4TextRadDropDownList.DataSource = new string[] { analog4Text };
            analog5TextRadDropDownList.DataSource = new string[] { analog5Text };
            analog6TextRadDropDownList.DataSource = new string[] { analog6Text };
            vibration1TextRadDropDownList.DataSource = new string[] { vibration1Text };
            vibration2TextRadDropDownList.DataSource = new string[] { vibration2Text };
            vibration3TextRadDropDownList.DataSource = new string[] { vibration3Text };
            vibration4TextRadDropDownList.DataSource = new string[] { vibration4Text };

            humidityTextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            moistureTextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            temp1TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            temp2TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            temp3TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            temp4TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            loadCurrent1TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            loadCurrent2TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            loadCurrent3TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            voltage1TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            voltage2TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            voltage3TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog1TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog2TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog3TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog4TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog5TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            analog6TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            vibration1TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            vibration2TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            vibration3TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;
            vibration4TextRadDropDownList.DropDownStyle = RadDropDownStyle.DropDown;

            humidityOperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            moistureOperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            temp1OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            temp2OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            temp3OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            temp4OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            loadCurrent1OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            loadCurrent2OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            loadCurrent3OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            voltage1OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            voltage2OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            voltage3OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog1OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog2OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog3OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog4OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog5OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            analog6OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            vibration1OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            vibration2OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            vibration3OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };
            vibration4OperationRadDropDownList.DataSource = new string[] { addText, multiplyText };

            humidityOperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            moistureOperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            temp1OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            temp2OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            temp3OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            temp4OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            loadCurrent1OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            loadCurrent2OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            loadCurrent3OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            voltage1OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            voltage2OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            voltage3OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog1OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog2OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog3OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog4OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog5OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            analog6OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            vibration1OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            vibration2OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            vibration3OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
            vibration4OperationRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

            SetNewVariablesToInputObjectValues();
            WriteNewValuesToDisplayObjects();
        }

        private void resetDefaultValuesRadButton_Click(object sender, EventArgs e)
        {
            SetNewValuesToDefaultValues();
            WriteNewValuesToDisplayObjects();
        }

        private void resetLastSavedValuesRadButton_Click(object sender, EventArgs e)
        {
            SetNewVariablesToInputObjectValues();
            WriteNewValuesToDisplayObjects();
        }

        private void SetNewVariablesToInputObjectValues()
        {
            humidityNewText = humidityInputText;
            moistureNewText = moistureInputText;
            temp1NewText = temp1InputText;
            temp2NewText = temp2InputText;
            temp3NewText = temp3InputText;
            temp4NewText = temp4InputText;
            loadCurrent1NewText = loadCurrent1InputText;
            loadCurrent2NewText = loadCurrent2InputText;
            loadCurrent3NewText = loadCurrent3InputText;
            voltage1NewText = voltage1InputText;
            voltage2NewText = voltage2InputText;
            voltage3NewText = voltage3InputText;
            analog1NewText = analog1InputText;
            analog2NewText = analog2InputText;
            analog3NewText = analog3InputText;
            analog4NewText = analog4InputText;
            analog5NewText = analog5InputText;
            analog6NewText = analog6InputText;
            vibration1NewText = vibration1InputText;
            vibration2NewText = vibration2InputText;
            vibration3NewText = vibration3InputText;
            vibration4NewText = vibration4InputText;

            humidityNewScaleFactor = humidityInputScaleFactor;
            moistureNewScaleFactor = moistureInputScaleFactor;
            temp1NewScaleFactor = temp1InputScaleFactor;
            temp2NewScaleFactor = temp2InputScaleFactor;
            temp3NewScaleFactor = temp3InputScaleFactor;
            temp4NewScaleFactor = temp4InputScaleFactor;
            loadCurrent1NewScaleFactor = loadCurrent1InputScaleFactor;
            loadCurrent2NewScaleFactor = loadCurrent2InputScaleFactor;
            loadCurrent3NewScaleFactor = loadCurrent3InputScaleFactor;
            voltage1NewScaleFactor = voltage1InputScaleFactor;
            voltage2NewScaleFactor = voltage2InputScaleFactor;
            voltage3NewScaleFactor = voltage3InputScaleFactor;
            analog1NewScaleFactor = analog1InputScaleFactor;
            analog2NewScaleFactor = analog2InputScaleFactor;
            analog3NewScaleFactor = analog3InputScaleFactor;
            analog4NewScaleFactor = analog4InputScaleFactor;
            analog5NewScaleFactor = analog5InputScaleFactor;
            analog6NewScaleFactor = analog6InputScaleFactor;
            vibration1NewScaleFactor = vibration1InputScaleFactor;
            vibration2NewScaleFactor = vibration2InputScaleFactor;
            vibration3NewScaleFactor = vibration3InputScaleFactor;
            vibration4NewScaleFactor = vibration4InputScaleFactor;

            humidityNewOperation = humidityInputOperation;
            moistureNewOperation = moistureInputOperation;
            temp1NewOperation = temp1InputOperation;
            temp2NewOperation = temp2InputOperation;
            temp3NewOperation = temp3InputOperation;
            temp4NewOperation = temp4InputOperation;
            loadCurrent1NewOperation = loadCurrent1InputOperation;
            loadCurrent2NewOperation = loadCurrent2InputOperation;
            loadCurrent3NewOperation = loadCurrent3InputOperation;
            voltage1NewOperation = voltage1InputOperation;
            voltage2NewOperation = voltage2InputOperation;
            voltage3NewOperation = voltage3InputOperation;
            analog1NewOperation = analog1InputOperation;
            analog2NewOperation = analog2InputOperation;
            analog3NewOperation = analog3InputOperation;
            analog4NewOperation = analog4InputOperation;
            analog5NewOperation = analog5InputOperation;
            analog6NewOperation = analog6InputOperation;
            vibration1NewOperation = vibration1InputOperation;
            vibration2NewOperation = vibration2InputOperation;
            vibration3NewOperation = vibration3InputOperation;
            vibration4NewOperation = vibration4InputOperation;
        }

        private void SetNewValuesToDefaultValues()
        {
            humidityNewText = humidityDefaultText;
            moistureNewText = moistureDefaultText;
            temp1NewText = temp1DefaultText;
            temp2NewText = temp2DefaultText;
            temp3NewText = temp3DefaultText;
            temp4NewText = temp4DefaultText;
            loadCurrent1NewText = loadCurrent1DefaultText;
            loadCurrent2NewText = loadCurrent2DefaultText;
            loadCurrent3NewText = loadCurrent3DefaultText;
            voltage1NewText = voltage1DefaultText;
            voltage2NewText = voltage2DefaultText;
            voltage3NewText = voltage3DefaultText;
            analog1NewText = analog1DefaultText;
            analog2NewText = analog2DefaultText;
            analog3NewText = analog3DefaultText;
            analog4NewText = analog4DefaultText;
            analog5NewText = analog5DefaultText;
            analog6NewText = analog6DefaultText;
            vibration1NewText = vibration1DefaultText;
            vibration2NewText = vibration2DefaultText;
            vibration3NewText = vibration3DefaultText;
            vibration4NewText = vibration4DefaultText;

            humidityNewScaleFactor = humidityDefaultScaleFactor;
            moistureNewScaleFactor = moistureDefaultScaleFactor;
            temp1NewScaleFactor = temp1DefaultScaleFactor;
            temp2NewScaleFactor = temp2DefaultScaleFactor;
            temp3NewScaleFactor = temp3DefaultScaleFactor;
            temp4NewScaleFactor = temp4DefaultScaleFactor;
            loadCurrent1NewScaleFactor = loadCurrent1DefaultScaleFactor;
            loadCurrent2NewScaleFactor = loadCurrent2DefaultScaleFactor;
            loadCurrent3NewScaleFactor = loadCurrent3DefaultScaleFactor;
            voltage1NewScaleFactor = voltage1DefaultScaleFactor;
            voltage2NewScaleFactor = voltage2DefaultScaleFactor;
            voltage3NewScaleFactor = voltage3DefaultScaleFactor;
            analog1NewScaleFactor = analog1DefaultScaleFactor;
            analog2NewScaleFactor = analog2DefaultScaleFactor;
            analog3NewScaleFactor = analog3DefaultScaleFactor;
            analog4NewScaleFactor = analog4DefaultScaleFactor;
            analog5NewScaleFactor = analog5DefaultScaleFactor;
            analog6NewScaleFactor = analog6DefaultScaleFactor;
            vibration1NewScaleFactor = vibration1DefaultScaleFactor;
            vibration2NewScaleFactor = vibration2DefaultScaleFactor;
            vibration3NewScaleFactor = vibration3DefaultScaleFactor;
            vibration4NewScaleFactor = vibration4DefaultScaleFactor;

            humidityNewOperation = humidityDefaultOperation;
            moistureNewOperation = moistureDefaultOperation;
            temp1NewOperation = temp1DefaultOperation;
            temp2NewOperation = temp2DefaultOperation;
            temp3NewOperation = temp3DefaultOperation;
            temp4NewOperation = temp4DefaultOperation;
            loadCurrent1NewOperation = loadCurrent1DefaultOperation;
            loadCurrent2NewOperation = loadCurrent2DefaultOperation;
            loadCurrent3NewOperation = loadCurrent3DefaultOperation;
            voltage1NewOperation = voltage1DefaultOperation;
            voltage2NewOperation = voltage2DefaultOperation;
            voltage3NewOperation = voltage3DefaultOperation;
            analog1NewOperation = analog1DefaultOperation;
            analog2NewOperation = analog2DefaultOperation;
            analog3NewOperation = analog3DefaultOperation;
            analog4NewOperation = analog4DefaultOperation;
            analog5NewOperation = analog5DefaultOperation;
            analog6NewOperation = analog6DefaultOperation;
            vibration1NewOperation = vibration1DefaultOperation;
            vibration2NewOperation = vibration2DefaultOperation;
            vibration3NewOperation = vibration3DefaultOperation;
            vibration4NewOperation = vibration4DefaultOperation;
        }

        private void WriteNewValuesToDisplayObjects()
        {
            humidityTextRadDropDownList.Text = humidityNewText;
            moistureTextRadDropDownList.Text = moistureNewText;
            temp1TextRadDropDownList.Text = temp1NewText;
            temp2TextRadDropDownList.Text = temp2NewText;
            temp3TextRadDropDownList.Text = temp3NewText;
            temp4TextRadDropDownList.Text = temp4NewText;
            loadCurrent1TextRadDropDownList.Text = loadCurrent1NewText;
            loadCurrent2TextRadDropDownList.Text = loadCurrent2NewText;
            loadCurrent3TextRadDropDownList.Text = loadCurrent3NewText;
            voltage1TextRadDropDownList.Text = voltage1NewText;
            voltage2TextRadDropDownList.Text = voltage2NewText;
            voltage3TextRadDropDownList.Text = voltage3NewText;
            analog1TextRadDropDownList.Text = analog1NewText;
            analog2TextRadDropDownList.Text = analog2NewText;
            analog3TextRadDropDownList.Text = analog3NewText;
            analog4TextRadDropDownList.Text = analog4NewText;
            analog5TextRadDropDownList.Text = analog5NewText;
            analog6TextRadDropDownList.Text = analog6NewText;
            vibration1TextRadDropDownList.Text = vibration1NewText;
            vibration2TextRadDropDownList.Text = vibration2NewText;
            vibration3TextRadDropDownList.Text = vibration3NewText;
            vibration4TextRadDropDownList.Text = vibration4NewText;

            humidityScaleFactorRadTextBox.Text = humidityNewScaleFactor.ToString();
            moistureScaleFactorRadTextBox.Text = moistureNewScaleFactor.ToString();
            temp1ScaleFactorRadTextBox.Text = temp1NewScaleFactor.ToString();
            temp2ScaleFactorRadTextBox.Text = temp2NewScaleFactor.ToString();
            temp3ScaleFactorRadTextBox.Text = temp3NewScaleFactor.ToString();
            temp4ScaleFactorRadTextBox.Text = temp4NewScaleFactor.ToString();
            loadCurrent1ScaleFactorRadTextBox.Text = loadCurrent1NewScaleFactor.ToString();
            loadCurrent2ScaleFactorRadTextBox.Text = loadCurrent2NewScaleFactor.ToString();
            loadCurrent3ScaleFactorRadTextBox.Text = loadCurrent3NewScaleFactor.ToString();
            voltage1ScaleFactorRadTextBox.Text = voltage1NewScaleFactor.ToString();
            voltage2ScaleFactorRadTextBox.Text = voltage2NewScaleFactor.ToString();
            voltage3ScaleFactorRadTextBox.Text = voltage3NewScaleFactor.ToString();
            analog1ScaleFactorRadTextBox.Text = analog1NewScaleFactor.ToString();
            analog2ScaleFactorRadTextBox.Text = analog2NewScaleFactor.ToString();
            analog3ScaleFactorRadTextBox.Text = analog3NewScaleFactor.ToString();
            analog4ScaleFactorRadTextBox.Text = analog4NewScaleFactor.ToString();
            analog5ScaleFactorRadTextBox.Text = analog5NewScaleFactor.ToString();
            analog6ScaleFactorRadTextBox.Text = analog6NewScaleFactor.ToString();
            vibration1ScaleFactorRadTextBox.Text = vibration1NewScaleFactor.ToString();
            vibration2ScaleFactorRadTextBox.Text = vibration2NewScaleFactor.ToString();
            vibration3ScaleFactorRadTextBox.Text = vibration3NewScaleFactor.ToString();
            vibration4ScaleFactorRadTextBox.Text = vibration4NewScaleFactor.ToString();

            humidityOperationRadDropDownList.Text = humidityNewOperation;
            moistureOperationRadDropDownList.Text = moistureNewOperation;
            temp1OperationRadDropDownList.Text = temp1NewOperation;
            temp2OperationRadDropDownList.Text = temp2NewOperation;
            temp3OperationRadDropDownList.Text = temp3NewOperation;
            temp4OperationRadDropDownList.Text = temp4NewOperation;
            loadCurrent1OperationRadDropDownList.Text = loadCurrent1NewOperation;
            loadCurrent2OperationRadDropDownList.Text = loadCurrent2NewOperation;
            loadCurrent3OperationRadDropDownList.Text = loadCurrent3NewOperation;
            voltage1OperationRadDropDownList.Text = voltage1NewOperation;
            voltage2OperationRadDropDownList.Text = voltage2NewOperation;
            voltage3OperationRadDropDownList.Text = voltage3NewOperation;
            analog1OperationRadDropDownList.Text = analog1NewOperation;
            analog2OperationRadDropDownList.Text = analog2NewOperation;
            analog3OperationRadDropDownList.Text = analog3NewOperation;
            analog4OperationRadDropDownList.Text = analog4NewOperation;
            analog5OperationRadDropDownList.Text = analog5NewOperation;
            analog6OperationRadDropDownList.Text = analog6NewOperation;
            vibration1OperationRadDropDownList.Text = vibration1NewOperation;
            vibration2OperationRadDropDownList.Text = vibration2NewOperation;
            vibration3OperationRadDropDownList.Text = vibration3NewOperation;
            vibration4OperationRadDropDownList.Text = vibration4NewOperation;
        }

        private bool ReadNewValuesFromDisplayObjects()
        {
            bool allValuesWereOkay = true;
            double value;

            humidityNewText = humidityTextRadDropDownList.Text;
            moistureNewText = moistureTextRadDropDownList.Text;
            temp1NewText = temp1TextRadDropDownList.Text;
            temp2NewText = temp2TextRadDropDownList.Text;
            temp3NewText = temp3TextRadDropDownList.Text;
            temp4NewText = temp4TextRadDropDownList.Text;
            loadCurrent1NewText = loadCurrent1TextRadDropDownList.Text;
            loadCurrent2NewText = loadCurrent2TextRadDropDownList.Text;
            loadCurrent3NewText = loadCurrent3TextRadDropDownList.Text;
            voltage1NewText = voltage1TextRadDropDownList.Text;
            voltage2NewText = voltage2TextRadDropDownList.Text;
            voltage3NewText = voltage3TextRadDropDownList.Text;
            analog1NewText = analog1TextRadDropDownList.Text;
            analog2NewText = analog2TextRadDropDownList.Text;
            analog3NewText = analog3TextRadDropDownList.Text;
            analog4NewText = analog4TextRadDropDownList.Text;
            analog5NewText = analog5TextRadDropDownList.Text;
            analog6NewText = analog6TextRadDropDownList.Text;
            vibration1NewText = vibration1TextRadDropDownList.Text;
            vibration2NewText = vibration2TextRadDropDownList.Text;
            vibration3NewText = vibration3TextRadDropDownList.Text;
            vibration4NewText = vibration4TextRadDropDownList.Text;

            if (!Double.TryParse(humidityScaleFactorRadTextBox.Text, out value))
            {
                allValuesWereOkay = false;
                RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                humidityScaleFactorRadTextBox.Select();
            }
            else
            {
                humidityNewScaleFactor = value;
            }
            if (allValuesWereOkay)
            {
                if (!Double.TryParse(moistureScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    moistureScaleFactorRadTextBox.Select();
                }
                else
                {
                    moistureNewScaleFactor = value;
                }
            }
            if (allValuesWereOkay)
            {
                if (!Double.TryParse(temp2ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    temp2ScaleFactorRadTextBox.Select();
                }
                else
                {
                    temp1NewScaleFactor = value;
                }
            }
            if (allValuesWereOkay)
            {
                if (!Double.TryParse(temp1ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    temp1ScaleFactorRadTextBox.Select();
                }
                else
                {
                    temp2NewScaleFactor = value;
                }
            }
            if (allValuesWereOkay)
            {
                if (!Double.TryParse(temp3ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    temp3ScaleFactorRadTextBox.Select();
                }
                else
                {
                    temp3NewScaleFactor = value;
                }
            }
            if (allValuesWereOkay)
            {
                if (!Double.TryParse(temp4ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    temp4ScaleFactorRadTextBox.Select();
                }
                else
                {
                    temp4NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(loadCurrent1ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    loadCurrent1ScaleFactorRadTextBox.Select();
                }
                else
                {
                    loadCurrent1NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(loadCurrent2ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    loadCurrent2ScaleFactorRadTextBox.Select();
                }
                else
                {
                    loadCurrent2NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(loadCurrent3ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    loadCurrent3ScaleFactorRadTextBox.Select();
                }
                else
                {
                    loadCurrent3NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(voltage1ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    voltage1ScaleFactorRadTextBox.Select();
                }
                else
                {
                    voltage1NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(voltage2ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    voltage2ScaleFactorRadTextBox.Select();
                }
                else
                {
                    voltage2NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(voltage3ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    voltage3ScaleFactorRadTextBox.Select();
                }
                else
                {
                    voltage3NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog1ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog1ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog1NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog2ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog2ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog2NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog3ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog3ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog3NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog4ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog4ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog4NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog5ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog5ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog5NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(analog6ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    analog6ScaleFactorRadTextBox.Select();
                }
                else
                {
                    analog6NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(vibration1ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    vibration1ScaleFactorRadTextBox.Select();
                }
                else
                {
                    vibration1NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(vibration2ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    vibration2ScaleFactorRadTextBox.Select();
                }
                else
                {
                    vibration2NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(vibration3ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    vibration3ScaleFactorRadTextBox.Select();
                }
                else
                {
                    vibration3NewScaleFactor = value;
                }
            }

            if (allValuesWereOkay)
            {
                if (!Double.TryParse(vibration4ScaleFactorRadTextBox.Text, out value))
                {
                    allValuesWereOkay = false;
                    RadMessageBox.Show(this, incorrectEntryInScaleFactorTextBoxText);
                    vibration4ScaleFactorRadTextBox.Select();
                }
                else
                {
                    vibration4NewScaleFactor = value;
                }
            }

            humidityNewOperation = humidityOperationRadDropDownList.Text;
            moistureNewOperation = moistureOperationRadDropDownList.Text;
            temp1NewOperation = temp1OperationRadDropDownList.Text;
            temp2NewOperation = temp2OperationRadDropDownList.Text;
            temp3NewOperation = temp3OperationRadDropDownList.Text;
            temp4NewOperation = temp4OperationRadDropDownList.Text;
            loadCurrent1NewOperation = loadCurrent1OperationRadDropDownList.Text;
            loadCurrent2NewOperation = loadCurrent2OperationRadDropDownList.Text;
            loadCurrent3NewOperation = loadCurrent3OperationRadDropDownList.Text;
            voltage1NewOperation = voltage1OperationRadDropDownList.Text;
            voltage2NewOperation = voltage2OperationRadDropDownList.Text;
            voltage3NewOperation = voltage3OperationRadDropDownList.Text;
            analog1NewOperation = analog1OperationRadDropDownList.Text;
            analog2NewOperation = analog2OperationRadDropDownList.Text;
            analog3NewOperation = analog3OperationRadDropDownList.Text;
            analog4NewOperation = analog4OperationRadDropDownList.Text;
            analog5NewOperation = analog5OperationRadDropDownList.Text;
            analog6NewOperation = analog6OperationRadDropDownList.Text;
            vibration1NewOperation = vibration1OperationRadDropDownList.Text;
            vibration2NewOperation = vibration2OperationRadDropDownList.Text;
            vibration3NewOperation = vibration3OperationRadDropDownList.Text;
            vibration4NewOperation = vibration4OperationRadDropDownList.Text;

            return allValuesWereOkay;
        }

        private void saveChangesRadButton_Click(object sender, EventArgs e)
        {
            if (ReadNewValuesFromDisplayObjects())
            {
                resetValues = true;
                this.Close();
            }
        }

        private void cancelChangesRadButton_Click(object sender, EventArgs e)
        {
            resetValues = false;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = dynamicsNamesEditorTitleText;

                humidityDefaultText = humidityText;
                moistureDefaultText = moistureText;
                temp1DefaultText = temp1Text;
                temp2DefaultText = temp2Text;
                temp3DefaultText = temp3Text;
                temp4DefaultText = temp4Text;
                loadCurrent1DefaultText = loadCurrent1Text;
                loadCurrent2DefaultText = loadCurrent2Text;
                loadCurrent3DefaultText = loadCurrent3Text;
                voltage1DefaultText = voltage1Text;
                voltage2DefaultText = voltage2Text;
                voltage3DefaultText = voltage3Text;
                analog1DefaultText = analog1Text;
                analog2DefaultText = analog2Text;
                analog3DefaultText = analog3Text;
                analog4DefaultText = analog4Text;
                analog5DefaultText = analog5Text;
                analog6DefaultText = analog6Text;
                vibration1DefaultText = vibration1Text;
                vibration2DefaultText = vibration2Text;
                vibration3DefaultText = vibration3Text;
                vibration4DefaultText = vibration4Text;

                humidityDefaultOperation = addText;
                moistureDefaultOperation = addText;
                temp1DefaultOperation = addText;
                temp2DefaultOperation = addText;
                temp3DefaultOperation = addText;
                temp4DefaultOperation = addText;
                loadCurrent1DefaultOperation = multiplyText;
                loadCurrent2DefaultOperation = multiplyText;
                loadCurrent3DefaultOperation = multiplyText;
                voltage1DefaultOperation = multiplyText;
                voltage2DefaultOperation = multiplyText;
                voltage3DefaultOperation = multiplyText;
                analog1DefaultOperation = multiplyText;
                analog2DefaultOperation = multiplyText;
                analog3DefaultOperation = multiplyText;
                analog4DefaultOperation = multiplyText;
                analog5DefaultOperation = multiplyText;
                analog6DefaultOperation = multiplyText;
                vibration1DefaultOperation = multiplyText;
                vibration2DefaultOperation = multiplyText;
                vibration3DefaultOperation = multiplyText;
                vibration4DefaultOperation = multiplyText;

                humidityRadCheckBoxText = humidityText;
                moistureRadCheckBoxText = moistureText;
                temp1RadCheckBoxText = temp1Text;
                temp2RadCheckBoxText = temp2Text;
                temp3RadCheckBoxText = temp3Text;
                temp4RadCheckBoxText = temp4Text;
                loadCurrent1RadCheckBoxText = loadCurrent1Text;
                loadCurrent2RadCheckBoxText = loadCurrent2Text;
                loadCurrent3RadCheckBoxText = loadCurrent3Text;
                voltage1RadCheckBoxText = voltage1Text;
                voltage2RadCheckBoxText = voltage2Text;
                voltage3RadCheckBoxText = voltage3Text;
                analog1RadCheckBoxText = analog1Text;
                analog2RadCheckBoxText = analog2Text;
                analog3RadCheckBoxText = analog3Text;
                analog4RadCheckBoxText = analog4Text;
                analog5RadCheckBoxText = analog5Text;
                analog6RadCheckBoxText = analog6Text;
                vibration1RadCheckBoxText = vibration1Text;
                vibration2RadCheckBoxText = vibration2Text;
                vibration3RadCheckBoxText = vibration3Text;
                vibration4RadCheckBoxText = vibration4Text; 

                originalInterfaceTextRadLabel.Text = originalInterfaceTextRadLabelText;
                newTextToDisplayRadLabel.Text = newTextToDisplayRadLabelText;
                scaleFactorRadLabel.Text = scaleFactorRadLabelText;
                applicationOfScaleFactorRadLabel.Text = applicationOfScaleFactorRadLabelText;
                dynamicsRadGroupBox.Text = dynamicsRadGroupBoxText;
                humidityRadCheckBox.Text = humidityRadCheckBoxText;
                moistureRadCheckBox.Text = moistureRadCheckBoxText;
                temp1RadCheckBox.Text = temp1RadCheckBoxText;
                temp2RadCheckBox.Text = temp2RadCheckBoxText;
                temp3RadCheckBox.Text = temp3RadCheckBoxText;
                temp4RadCheckBox.Text = temp4RadCheckBoxText;
                loadCurrent1RadCheckBox.Text = loadCurrent1RadCheckBoxText;
                loadCurrent2RadCheckBox.Text = loadCurrent2RadCheckBoxText;
                loadCurrent3RadCheckBox.Text = loadCurrent3RadCheckBoxText;
                voltage1RadCheckBox.Text = voltage1RadCheckBoxText;
                voltage2RadCheckBox.Text = voltage2RadCheckBoxText;
                voltage3RadCheckBox.Text = voltage3RadCheckBoxText;
                analog1RadCheckBox.Text = analog1RadCheckBoxText;
                analog2RadCheckBox.Text = analog2RadCheckBoxText;
                analog3RadCheckBox.Text = analog3RadCheckBoxText;
                analog4RadCheckBox.Text = analog4RadCheckBoxText;
                analog5RadCheckBox.Text = analog5RadCheckBoxText;
                analog6RadCheckBox.Text = analog6RadCheckBoxText;
                vibration1RadCheckBox.Text = vibration1RadCheckBoxText;
                vibration2RadCheckBox.Text = vibration2RadCheckBoxText;
                vibration3RadCheckBox.Text = vibration3RadCheckBoxText;
                vibration4RadCheckBox.Text = vibration4RadCheckBoxText;
                resetDefaultValuesRadButton.Text = resetDefaultValuesRadButtonText;
                resetLastSavedValuesRadButton.Text = resetLastSavedValuesRadButtonText;
                saveChangesRadButton.Text = saveChangesRadButtonText;
                cancelChangesRadButton.Text = cancelChangesRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DynamicsNamesEditor.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                humidityText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorHumidityText", humidityText, "", "", "");
                moistureText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorMoistureText", moistureText, "", "", "");
                temp1Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorCorrelatingTempText", temp1Text, "", "", "");
                temp2Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorTemp2Text", temp2Text, "", "", "");
                temp3Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorTemp3Text", temp3Text, "", "", "");
                temp4Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorTemp4Text", temp4Text, "", "", "");
                loadCurrent1Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorLoadCurrent1Text", loadCurrent1Text, "", "", "");
                loadCurrent2Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorLoadCurrent2Text", loadCurrent2Text, "", "", "");
                loadCurrent3Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorLoadCurrent3Text", loadCurrent3Text, "", "", "");
                voltage1Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVoltage1Text", voltage1Text, "", "", "");
                voltage2Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVoltage2Text", voltage2Text, "", "", "");
                voltage3Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVoltage3Text", voltage3Text, "", "", "");
                analog1Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog1Text", analog1Text, "", "", "");
                analog2Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog2Text", analog2Text, "", "", "");
                analog3Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog3Text", analog3Text, "", "", "");
                analog4Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog4Text", analog4Text, "", "", "");
                analog5Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog5Text", analog5Text, "", "", "");
                analog6Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAnalog6Text", analog6Text, "", "", "");
                vibration1Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVibration1Text", vibration1Text, "", "", "");
                vibration2Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVibration2Text", vibration2Text, "", "", "");
                vibration3Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVibration3Text", vibration3Text, "", "", "");
                vibration4Text = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorVibration4Text", vibration4Text, "", "", "");

                addText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorAddText", addText, "", "", "");
                multiplyText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorMultiplyText", multiplyText, "", "", "");

                dynamicsNamesEditorTitleText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceText", dynamicsNamesEditorTitleText, "", "", "");
                originalInterfaceTextRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceOriginalInterfaceTextRadLabelText", originalInterfaceTextRadLabelText, htmlFontType, htmlStandardFontSize, "");
                newTextToDisplayRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceNextTextToDisplayRadLabelText", newTextToDisplayRadLabelText, htmlFontType, htmlStandardFontSize, "");
                scaleFactorRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceScaleFactorRadLabelText", scaleFactorRadLabelText, htmlFontType, htmlStandardFontSize, "");
                applicationOfScaleFactorRadLabelText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceApplicationOfScaleFactorRadLabelText", applicationOfScaleFactorRadLabelText, htmlFontType, htmlStandardFontSize, "");
                dynamicsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceDynamicsRadGroupBoxText", dynamicsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                resetDefaultValuesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceResetDefaultValuesRadButtonText", resetDefaultValuesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                resetLastSavedValuesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceResetLastSavedValuesRadButtonText", resetLastSavedValuesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                saveChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceSaveChangesRadButtonText", saveChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelChangesRadButtonText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorInterfaceCancelChangesRadButtonText", cancelChangesRadButtonText, htmlFontType, htmlStandardFontSize, "");
                incorrectEntryInScaleFactorTextBoxText = LanguageConversion.GetStringAssociatedWithTag("DynamicsNamesEditorIncorrectEntryInScaleFactorTextBoxText", incorrectEntryInScaleFactorTextBoxText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DynamicsNamesEditor.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
