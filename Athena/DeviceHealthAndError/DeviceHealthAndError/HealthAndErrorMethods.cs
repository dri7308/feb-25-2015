﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DeviceHealthAndError
{
    public class HealthAndErrorMethods
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string errorInSetOneTestDynamicRatingsText = "Error In Set 1 Test - Call Dynamic Ratings";
        private static string errorInSetTwoTestDynamicRatingsText = "Error In Set 2 Test - Call Dynamic Ratings";
        private static string flashWriteErrorDynamicRatingsText = "Flash write error - Call Dynamic Ratings";
        private static string flashReadErrorDynamicRatingsText = "Flash read error - Call Dynamic Ratings";
        private static string noSignalsSetOneText = "No signals for Set 1";
        private static string noSignalsSetTwoText = "No signals for Set 2";
        private static string wrongPhaseRotationSetOneText = "Set 1 - Wrong Phase Rotation - Flip two connections";
        private static string wrongPhaseRotationSetTwoText = "Set 2 - Wrong Phase Rotation - Flip two connections";
        private static string calibrationErrorDynamicRatingsText = "Calibration Error - Call Dynamic Ratings";
        private static string phaseShiftErrorAAText = "Phase shift error A-a";
        private static string phaseShiftErrorBBText = "Phase shift error B-b";
        private static string phaseShiftErrorCCText = "Phase shift error C-c";
        private static string transformerOffText = "Transformer Off";
        private static string frequencyMismatchText = "Frequency in Configuration does not match measured frequency";
        private static string inputMagnitudeIsTooLowText = "Input Magnitude is too low - check resistors";
        private static string inputMagnitudeIsTooHighText = "Input Magnitude is too high - Check resistors";

        private static string errorInSetOneTestMeggitText = "Error In Set 1 Test - Call Meggit";
        private static string errorInSetTwoTestMeggitText = "Error In Set 2 Test - Call Meggit";
        private static string flashWriteErrorMeggitText = "Flash write error - Call Meggit";
        private static string flashReadErrorMeggitText = "Flash read error - Call Meggit";
        private static string calibrationErrorMeggitText = "Calibration Error - Call Meggit";

        private static string systemInStopModeText = "System in Stop Mode - To Reset Select Resume";
        private static string systemInPauseModeText = "System in Pause Mode  - To Reset Select Resume";
        private static string errorRealTimeClockText = "Error real time clock";

        private static string monitoringStoppedText = "Monitoring stopped, MUST be resumed by user";
        private static string monitoringPausedText = "Monitoring paused, will resume automatically";
        private static string synchronizationMalfunctionOrSignalLossText = "Synchronization malfunction or signal loss";

        private static string measuringChannelDidNotPassCalibrationAndTestText = "Measuring channel did not pass calibration and test";
        private static string amplitudeFilterChannelDidNotPassCalibrationAndTestText = "Amplitude filter channel did not pass calibration and test";
        private static string timeAndPolarityChannelDidNotPassCalibrationAndTestText = "Time/Polarity filter channel did not pass calibration and test";
        private static string clockTimeIsEarlierThanTheTimeOfTheLastRecordInMemoryText = "Clock error - clock time is earlier than the time of the last record in memory";
        private static string errorInInitialReadingText = "Error in initial reading";

        private static string maxPulseAmplitudeText = "Q02 - Max pulse amplitude";
        private static string partialDischargeIntensityText = "PDI - Partial discharge intensity";
        private static string maxPulseAmplitudeTrendText = "Q02_t - Max pulse amplitude trend";
        private static string partialDischargeIntensityTrendText = "PDI_t - Partial discharge intensity trend";
        private static string maxPulseAmplitudeChangeText = "Q02_j - Max pulse amplitude change";
        private static string partialDischargeIntensityChangeText = "PDI_j - Partial discharge intensity change";

        private static string dataMemoryClearedText = "Data memory cleared";
        private static string dataAndBalancingInfoClearedText = "Data and balancing info cleared";
        private static string gammaRedAlarmText = "Gamma Red Alarm";
        private static string gammaTrendAlarmText = "Gamma Trend Alarm";
        private static string powerTurnedOnText = "Power turned on";
        private static string flashWriteFailureText = "Flash Write Failure";
        private static string flashReadFailureText = "Flash Read Failure";
        private static string phaseShiftSetOneText = "Phase Shift Set 1";
        private static string phaseShiftSetTwoText = "Phase Shift Set 2";
        private static string channelsQuestionText = "Channels?";
        private static string wrongFrequencyText = "Wrong Frequency";
        private static string lowSignalText = "Low Signal";
        private static string highSignalText = "High Signal";
        private static string unitOffText = "Unit Off";
        private static string trendAlarmText = "Trend Alarm";
        private static string tkAlarmText = "TK Alarm";
        private static string setOneOffText = "Set 1 Off";
        private static string setTwoOffText = "Set 2 Off";
        private static string systemBalancedText = "System Balanced";
        private static string gammaYellowAlarmText = "Gamma Yellow Alarm";
        private static string logWasClearedText = "Log was Cleared";

        private static string mainRegistersShowedNoWarningOrAlarmFromAnyDeviceText = "Main registers showed no warnings or alarms from any connected device";
        private static string mainRegistersShowedAWarningFromAtLeastOneDeviceText = "Main registers showed a warning from at least one connected device";
        private static string mainRegistersShowedAnAlarmFromAtLeastOneDeviceText = "Main registers showed an alarm from at least one connected device";

        private static string bhmRegistersShowedNoWarningOrAlarmText = "BHM registers showed no warnings or alarms";
        private static string bhmRegistersShowedAWarningText = "BHM registers showed a warning";
        private static string bhmRegistersShowedAnAlarmText = "BHM registers showed an alarm";

        private static string pdmRegistersShowedNoWarningOrAlarmText = "PDM registers showed no warnings or alarms";
        private static string pdmRegistersShowedAWarningText = "PDM registers showed a warning";
        private static string pdmRegistersShowedAnAlarmText = "PDM registers showed an alarm";

        private static string noAlarmsSetOneText = "Set 1 - no alarms";
        private static string alarmOnGammaMagnitudeSetOneText = "Set 1 - alarm on gamma magnitude";
        private static string alarmOnGammaTrendSetOneText = "Set 1 - alarm on gamma trend";
        private static string alarmOnTemperatureVariationSetOneText = "Set 1 - alarm on temperature variations";
        private static string warningOnGammaSetOneText = "Set 1 - warning on gamma";

        private static string noAlarmsSetTwoText = "Set 2 - no alarms";
        private static string alarmOnGammaMagnitudeSetTwoText = "Set 2 - alarm on gamma magnitude";
        private static string alarmOnGammaTrendSetTwoText = "Set 2 - alarm on gamma trend";
        private static string alarmOnTemperatureVariationSetTwoText = "Set 2 - alarm on temperature variations";
        private static string warningOnGammaSetTwoText = "Set 2 - warning on gamma";

        private static string bcPhaseSwapSetOneText = "Set 1 - BC Phase Swap has occurred";
        private static string highInitialBalanceSetOneText = "Set 1 has high initial balance > 0.5%";

        private static string bcPhaseSwapSetTwoText = "Set 2 - BC Phase Swap has occurred";
        private static string highInitialBalanceSetTwoText = "Set 2 has high initial balance > 0.5%";

        private static string magnitudeMaxAmplitudeWarningText = "Warning on magnitude maximum amplitude";
        private static string magnitudeMaxAmplitudeAlarmText = "Alarm on magnitude maximum amplitude";
        private static string magnitudeMaxTrendWarningText = "Warning on magnitude maximum trend";
        private static string magnitudeMaxTrendAlarmText = "Alarm on magnitude maximum trend";
        private static string magnitudeMaxChangeWarningText = "Warning on magnitude maximum change";
        private static string magnitudeMaxChangeAlarmText = "Alarm on magnitude maximum change";
        private static string pdiChangeWarningText = "Warning on PDI change";
        private static string pdiChangeAlarmText = "Alarm on PDI change";
        private static string spuriousText = "Should never be seen";

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                errorInSetOneTestDynamicRatingsText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorInSetOneTestDynamicRatingsText", errorInSetOneTestDynamicRatingsText, "", "", "");
                errorInSetTwoTestDynamicRatingsText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorInSetTwoTestDynamicRatingsText", errorInSetTwoTestDynamicRatingsText, "", "", "");
                flashWriteErrorDynamicRatingsText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFlashWriteErrorDynamicRatingsText", flashWriteErrorDynamicRatingsText, "", "", "");
                flashReadErrorDynamicRatingsText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodssFlashReadErrorDynamicRatingsText", flashReadErrorDynamicRatingsText, "", "", "");
                noSignalsSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsNoSignalsSetOneText", noSignalsSetOneText, "", "", "");
                noSignalsSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsNoSignalsSetTwoText", noSignalsSetTwoText, "", "", "");
                wrongPhaseRotationSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsWrongPhaseRotationSetOneText", wrongPhaseRotationSetOneText, "", "", "");
                wrongPhaseRotationSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsWrongPhaseRotationSetTwoText", wrongPhaseRotationSetTwoText, "", "", "");
                calibrationErrorDynamicRatingsText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsCalibrationErrorDynamicRatingsText", calibrationErrorDynamicRatingsText, "", "", "");
                phaseShiftErrorAAText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsphaseShiftErrorAAText", phaseShiftErrorAAText, "", "", "");
                phaseShiftErrorBBText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPhaseShiftErrorBBText", phaseShiftErrorBBText, "", "", "");
                phaseShiftErrorCCText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPhaseShiftErrorCCText", phaseShiftErrorCCText, "", "", "");
                transformerOffText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsTransformerOffText", transformerOffText, "", "", "");
                frequencyMismatchText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFrequencyMismatchText", frequencyMismatchText, "", "", "");
                inputMagnitudeIsTooLowText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsInputMagnitudeIsTooLowText", inputMagnitudeIsTooLowText, "", "", "");
                inputMagnitudeIsTooHighText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsInputMagnitudeIsTooHighText", inputMagnitudeIsTooHighText, "", "", "");
                errorInSetOneTestMeggitText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorInSetOneTestMeggitText", errorInSetOneTestMeggitText, "", "", "");
                errorInSetTwoTestMeggitText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorInSetTwoTestMeggitText", errorInSetTwoTestMeggitText, "", "", "");
                flashWriteErrorMeggitText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFlashWriteErrorMeggitText", flashWriteErrorMeggitText, "", "", "");
                flashReadErrorMeggitText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFlashReadErrorMeggitText", flashReadErrorMeggitText, "", "", "");
                calibrationErrorMeggitText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsCalibrationErrorMeggitText", calibrationErrorMeggitText, "", "", "");
                systemInStopModeText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSystemInStopModeText", systemInStopModeText, "", "", "");
                systemInPauseModeText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSystemInPauseModeText", systemInPauseModeText, "", "", "");
                errorRealTimeClockText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorRealTimeClockText", errorRealTimeClockText, "", "", "");
                monitoringStoppedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMonitoringStoppedText", monitoringStoppedText, "", "", "");
                monitoringPausedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMonitoringPausedText", monitoringPausedText, "", "", "");
                synchronizationMalfunctionOrSignalLossText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSynchronizationMalfunctionOrSignalLossText", synchronizationMalfunctionOrSignalLossText, "", "", "");
                measuringChannelDidNotPassCalibrationAndTestText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMeasuringChannelDidNotPassCalibrationAndTestText", measuringChannelDidNotPassCalibrationAndTestText, "", "", "");
                amplitudeFilterChannelDidNotPassCalibrationAndTestText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAmplitudeFilterChannelDidNotPassCalibrationAndTestText", amplitudeFilterChannelDidNotPassCalibrationAndTestText, "", "", "");
                timeAndPolarityChannelDidNotPassCalibrationAndTestText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsTimeAndPolarityChannelDidNotPassCalibrationAndTestText", timeAndPolarityChannelDidNotPassCalibrationAndTestText, "", "", "");
                clockTimeIsEarlierThanTheTimeOfTheLastRecordInMemoryText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsClockTimeIsEarlierThanTheTimeOfTheLastRecordInMemoryText", clockTimeIsEarlierThanTheTimeOfTheLastRecordInMemoryText, "", "", "");
                errorInInitialReadingText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsErrorInInitialReadingText", errorInInitialReadingText, "", "", "");
                maxPulseAmplitudeText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMaxPulseAmplitudeText", maxPulseAmplitudeText, "", "", "");
                partialDischargeIntensityText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPartialDischargeIntensityText", partialDischargeIntensityText, "", "", "");
                maxPulseAmplitudeTrendText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMaxPulseAmplitudeTrendText", maxPulseAmplitudeTrendText, "", "", "");
                partialDischargeIntensityTrendText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPartialDischargeIntensityTrendText", partialDischargeIntensityTrendText, "", "", "");
                maxPulseAmplitudeChangeText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMaxPulseAmplitudeChangeText", maxPulseAmplitudeChangeText, "", "", "");
                partialDischargeIntensityChangeText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPartialDischargeIntensityChangeText", partialDischargeIntensityChangeText, "", "", "");
                dataMemoryClearedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsDataMemoryClearedText", dataMemoryClearedText, "", "", "");
                dataAndBalancingInfoClearedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsDataAndBalancingInfoClearedText", dataAndBalancingInfoClearedText, "", "", "");
                gammaRedAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsGammaRedAlarmText", gammaRedAlarmText, "", "", "");
                gammaTrendAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsGammaTrendAlarmText", gammaTrendAlarmText, "", "", "");
                powerTurnedOnText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPowerTurnedOnText", powerTurnedOnText, "", "", "");
                flashWriteFailureText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFlashWriteFailureText", flashWriteFailureText, "", "", "");
                flashReadFailureText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsFlashReadFailureText", flashReadFailureText, "", "", "");
                phaseShiftSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPhaseShiftSetOneText", phaseShiftSetOneText, "", "", "");
                phaseShiftSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPhaseShiftSetTwoText", phaseShiftSetTwoText, "", "", "");
                channelsQuestionText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsChannelsQuestionText", channelsQuestionText, "", "", "");
                wrongFrequencyText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsWrongFrequencyText", wrongFrequencyText, "", "", "");
                lowSignalText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsLowSignalText", lowSignalText, "", "", "");
                highSignalText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsHighSignalText", highSignalText, "", "", "");
                unitOffText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsUnitOffText", unitOffText, "", "", "");
                trendAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsTrendAlarmText", trendAlarmText, "", "", "");
                tkAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsTKAlarmText", tkAlarmText, "", "", "");
                setOneOffText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSetOneOffText", setOneOffText, "", "", "");
                setTwoOffText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSetTwoOffText", setTwoOffText, "", "", "");
                systemBalancedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsSystemBalancedText", systemBalancedText, "", "", "");
                gammaYellowAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsGammaYellowAlarmText", gammaYellowAlarmText, "", "", "");
                logWasClearedText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsLogWasClearedText", logWasClearedText, "", "", "");
                mainRegistersShowedNoWarningOrAlarmFromAnyDeviceText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMainRegistersShowedNoWarningOrAlarmFromAnyDeviceText", mainRegistersShowedNoWarningOrAlarmFromAnyDeviceText, "", "", "");
                mainRegistersShowedAWarningFromAtLeastOneDeviceText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMainRegistersShowedAWarningFromAtLeastOneDeviceText", mainRegistersShowedAWarningFromAtLeastOneDeviceText, "", "", "");
                mainRegistersShowedAnAlarmFromAtLeastOneDeviceText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMainRegistersShowedAnAlarmFromAtLeastOneDeviceText", mainRegistersShowedAnAlarmFromAtLeastOneDeviceText, "", "", "");
                bhmRegistersShowedNoWarningOrAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsBHMRegistersShowedNoWarningOrAlarmText", bhmRegistersShowedNoWarningOrAlarmText, "", "", "");
                bhmRegistersShowedAWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsBHMRegistersShowedAWarningText", bhmRegistersShowedAWarningText, "", "", "");
                bhmRegistersShowedAnAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsBHMRegistersShowedAnAlarmText", bhmRegistersShowedAnAlarmText, "", "", "");
                pdmRegistersShowedNoWarningOrAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPDMRegistersShowedNoWarningOrAlarmText", pdmRegistersShowedNoWarningOrAlarmText, "", "", "");
                pdmRegistersShowedAWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPDMRegistersShowedAWarningText", pdmRegistersShowedAWarningText, "", "", "");
                pdmRegistersShowedAnAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPDMRegistersShowedAnAlarmText", pdmRegistersShowedAnAlarmText, "", "", "");
                noAlarmsSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsNoAlarmsSetOneText", noAlarmsSetOneText, "", "", "");
                alarmOnGammaMagnitudeSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnGammaMagnitudeSetOneText", alarmOnGammaMagnitudeSetOneText, "", "", "");
                alarmOnGammaTrendSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnGammaTrendSetOneText", alarmOnGammaTrendSetOneText, "", "", "");
                alarmOnTemperatureVariationSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnTemperatureVariationSetOneText", alarmOnTemperatureVariationSetOneText, "", "", "");
                warningOnGammaSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsWarningOnGammaSetOneText", warningOnGammaSetOneText, "", "", "");
                noAlarmsSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsNoAlarmsSetTwoText", noAlarmsSetTwoText, "", "", "");
                alarmOnGammaMagnitudeSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnGammaMagnitudeSetTwoText", alarmOnGammaMagnitudeSetTwoText, "", "", "");
                alarmOnGammaTrendSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnGammaTrendSetTwoText", alarmOnGammaTrendSetTwoText, "", "", "");
                alarmOnTemperatureVariationSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsAlarmOnTemperatureVariationSetTwoText", alarmOnTemperatureVariationSetTwoText, "", "", "");
                warningOnGammaSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsWarningOnGammaSetTwoText", warningOnGammaSetTwoText, "", "", "");
                bcPhaseSwapSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsBCPhaseSwapSetOneText", bcPhaseSwapSetOneText, "", "", "");
                highInitialBalanceSetOneText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsHighInitialBalanceSetOneText", highInitialBalanceSetOneText, "", "", "");
                bcPhaseSwapSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsBCPhaseSwapSetTwoText", bcPhaseSwapSetTwoText, "", "", "");
                highInitialBalanceSetTwoText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsHighInitialBalanceSetTwoText", highInitialBalanceSetTwoText, "", "", "");

                magnitudeMaxAmplitudeWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxAmplitudeWarningText", magnitudeMaxAmplitudeWarningText, "", "", "");
                magnitudeMaxAmplitudeAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxAmplitudeAlarmText", magnitudeMaxAmplitudeAlarmText, "", "", "");
                magnitudeMaxTrendWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxTrendWarningText", magnitudeMaxTrendWarningText, "", "", "");
                magnitudeMaxTrendAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxTrendAlarmText", magnitudeMaxTrendAlarmText, "", "", "");
                magnitudeMaxChangeWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxChangeWarningText", magnitudeMaxChangeWarningText, "", "", "");
                magnitudeMaxChangeAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsMagnitudeMaxChangeAlarmText", magnitudeMaxChangeAlarmText, "", "", "");
                pdiChangeWarningText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPdiChangeWarningText", pdiChangeWarningText, "", "", "");
                pdiChangeAlarmText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsPdiChangeAlarmText", pdiChangeAlarmText, "", "", "");
                spuriousText = LanguageConversion.GetStringAssociatedWithTag("HealthAndErrorMethodsspuriousText", spuriousText, "", "", "");
 
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in HealthAndErrorMethods.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        #region Alarm Status

        public static string GetMainGlobalAlarmStatus(int alarmStatus)
        {
            string alarmStatusAsString = string.Empty;
            try
            {
                if (alarmStatus == 0)
                {
                    alarmStatusAsString = mainRegistersShowedNoWarningOrAlarmFromAnyDeviceText;
                }
                else if (alarmStatus == 1)
                {
                    alarmStatusAsString = mainRegistersShowedAWarningFromAtLeastOneDeviceText;
                }
                else if (alarmStatus == 2)
                {
                    alarmStatusAsString = mainRegistersShowedAnAlarmFromAtLeastOneDeviceText;
                }
                else
                {
                    string errorMessage = "Error in DeviceHealthAndError.GetMainAlarmStatus(int)\nInput alarmStatus was " + alarmStatus.ToString() + " which is not a correct value";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in HealthAndErrorMethods.GetMainGlobalAlarmStatus(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmStatusAsString;
        }

        public static List<string> GetBHMAlarmStatus(int alarmStatus)
        {
            List<string> errorStrings = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive;

                if (alarmStatus == 0)
                {
                    errorStrings.Add(bhmRegistersShowedNoWarningOrAlarmText);
                }
                else
                {
                    bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary((UInt16)alarmStatus);

                    if (bitIsActive[0])
                    {
                        errorStrings.Add(bhmRegistersShowedAWarningText);
                    }
                    if (bitIsActive[1])
                    {
                        errorStrings.Add(bhmRegistersShowedAnAlarmText);
                    }

                    if (bitIsActive[8])
                    {
                        errorStrings.Add(bcPhaseSwapSetOneText);
                    }

                    if (bitIsActive[9])
                    {
                        errorStrings.Add(highInitialBalanceSetOneText);
                    }

                    if (bitIsActive[10])
                    {
                        errorStrings.Add(bcPhaseSwapSetTwoText);
                    }

                    if (bitIsActive[11])
                    {
                        errorStrings.Add(highInitialBalanceSetTwoText);
                    }

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in HealthAndErrorMethods.GetBHMAlarmStatus(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorStrings;
        }

        public static List<string> GetPDMAlarmStatus(int alarmStatus)
        {
            List<string> errorStrings = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive;

                if (alarmStatus == 0)
                {
                    errorStrings.Add(pdmRegistersShowedNoWarningOrAlarmText);
                }
                else
                {
                    bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary((UInt16)alarmStatus);

                    if (bitIsActive[0])
                    {
                        errorStrings.Add(pdmRegistersShowedAWarningText);
                    }
                    if (bitIsActive[1])
                    {
                        errorStrings.Add(pdmRegistersShowedAnAlarmText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in HealthAndErrorMethods.GetPDMAlarmStatus(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorStrings;
        }

        public static List<string> GetBHMRegisterAlarmSources(int alarmSource, int setNumber)
        {
            List<string> alarmSources = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive;

                if (alarmSource == 0)
                {
                    if (setNumber == 1)
                    {
                        alarmSources.Add(noAlarmsSetOneText);
                    }
                    else
                    {
                        alarmSources.Add(noAlarmsSetTwoText);
                    }
                }
                else
                {
                    bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary((UInt16)alarmSource);

                    if (bitIsActive[0])
                    {
                        if (setNumber == 1)
                        {
                            alarmSources.Add(alarmOnGammaMagnitudeSetOneText);
                        }
                        else
                        {
                            alarmSources.Add(alarmOnGammaMagnitudeSetTwoText);
                        }
                    }

                    if (bitIsActive[1])
                    {
                        if (setNumber == 1)
                        {
                            alarmSources.Add(alarmOnGammaTrendSetOneText);
                        }
                        else
                        {
                            alarmSources.Add(alarmOnGammaTrendSetTwoText);
                        }
                    }

                    if (bitIsActive[2])
                    {
                        if (setNumber == 1)
                        {
                            alarmSources.Add(alarmOnTemperatureVariationSetOneText);
                        }
                        else
                        {
                            alarmSources.Add(alarmOnTemperatureVariationSetTwoText);
                        }
                    }

                    /// bit 3 (0-indexed) does not have an error code assigned to it

                    if (bitIsActive[4])
                    {
                        if (setNumber == 1)
                        {
                            alarmSources.Add(warningOnGammaSetOneText);
                        }
                        else
                        {
                            alarmSources.Add(warningOnGammaSetTwoText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetBHMRegisterAlarmSource(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmSources;
        }

        public static List<string> GetPDMRegisterAlarmSources(Dictionary<int, bool> bitIsActive)
        {
            List<string> errorStrings = new List<string>();
            try
            {
                if (bitIsActive != null)
                {
                    if (bitIsActive[8])
                    {
                        errorStrings.Add(maxPulseAmplitudeText);
                    }

                    if (bitIsActive[9])
                    {
                        errorStrings.Add(partialDischargeIntensityText);
                    }

                    if (bitIsActive[10])
                    {
                        errorStrings.Add(maxPulseAmplitudeTrendText);
                    }

                    if (bitIsActive[11])
                    {
                        errorStrings.Add(partialDischargeIntensityTrendText);
                    }

                    if (bitIsActive[14])
                    {
                        errorStrings.Add(maxPulseAmplitudeChangeText);
                    }

                    if (bitIsActive[15])
                    {
                        errorStrings.Add(partialDischargeIntensityChangeText);
                    }
                }
                else
                {
                    string errorMessage = "Error in MainDisplayPdmTab.GetPDMRegisterAlarmSources(Dictionary<int, bool>)\nInput bitIsActive was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetPDMRegisterAlarmSources(Dictionary<int, bool>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorStrings;
        }

        public static List<string> GetPDMRegisterAlarmSources(UInt16 equipmentErrorBitEncoded)
        {
            List<string> errorStrings = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(equipmentErrorBitEncoded);
                errorStrings = GetPDMRegisterAlarmSources(bitIsActive);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetPDMRegisterAlarmSources(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorStrings;
        }

        #endregion

        #region Device Health

        public static List<string> GetMainDeviceHealthStatus(UInt16 deviceHealthBitEncoded, ProgramBrand argProgramBrand)
        {
            //#define erStop              0
            //#define erPause             1
            //#define erFlashWriteFail    2
            //#define erFlashReadFail     3
            //#define erTime              4

            List<string> deviceHealthStatus = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(deviceHealthBitEncoded);

                if (bitIsActive[0])
                {
                    deviceHealthStatus.Add(monitoringStoppedText);
                }
                if (bitIsActive[1])
                {
                    /// do nothing, this is the indicator that the device is paused, which it always will be when getting the health status
                }
                if (bitIsActive[2])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        deviceHealthStatus.Add(flashWriteErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        deviceHealthStatus.Add(flashWriteErrorMeggitText);
                    }
                }
                if (bitIsActive[3])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        deviceHealthStatus.Add(flashReadErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        deviceHealthStatus.Add(flashReadErrorMeggitText);
                    }
                }
                if (bitIsActive[4])
                {
                    deviceHealthStatus.Add(errorRealTimeClockText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceHealthAndError.GetMainDeviceHealthStatus(UInt16, ProgramBrand)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceHealthStatus;
        }

        public static List<string> GetBhmDeviceHealthStatus(UInt32 deviceHealthBitEncoded, ProgramBrand argProgramBrand)
        {
            List<string> statusStrings = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(deviceHealthBitEncoded);
                /// we are using zero-indexing
                if (bitIsActive[0])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(errorInSetOneTestDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(errorInSetOneTestMeggitText);
                    }
                }
                if (bitIsActive[1])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(errorInSetTwoTestDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(errorInSetTwoTestMeggitText);
                    }
                }
                if (bitIsActive[2])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(flashWriteErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(flashWriteErrorMeggitText);
                    }
                }
                if (bitIsActive[3])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(flashReadErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(flashReadErrorMeggitText);
                    }
                }
                if (bitIsActive[4])
                {
                    statusStrings.Add(noSignalsSetOneText);
                }
                if (bitIsActive[5])
                {
                    statusStrings.Add(noSignalsSetTwoText);
                }
                if (bitIsActive[6])
                {
                    statusStrings.Add(wrongPhaseRotationSetOneText);
                }
                if (bitIsActive[7])
                {
                    statusStrings.Add(wrongPhaseRotationSetTwoText);
                }
                if (bitIsActive[8])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(calibrationErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(calibrationErrorMeggitText);
                    }
                }
                if (bitIsActive[9])
                {
                    statusStrings.Add(phaseShiftErrorAAText);
                }
                if (bitIsActive[10])
                {
                    statusStrings.Add(phaseShiftErrorBBText);
                }
                if (bitIsActive[11])
                {
                    statusStrings.Add(phaseShiftErrorCCText);
                }
                if (bitIsActive[12])
                {
                    statusStrings.Add(transformerOffText);
                }
                if (bitIsActive[13])
                {
                    statusStrings.Add(frequencyMismatchText);
                }
                if (bitIsActive[14])
                {
                    statusStrings.Add(inputMagnitudeIsTooLowText);
                }
                if (bitIsActive[15])
                {
                    statusStrings.Add(inputMagnitudeIsTooHighText);
                }
                if (bitIsActive[16])
                {
                    statusStrings.Add(systemInStopModeText);
                }
                if (bitIsActive[17])
                {
                    statusStrings.Add(systemInPauseModeText);
                }
                if (bitIsActive[18])
                {
                    /// Not used at this time
                }
                if (bitIsActive[19])
                {
                    /// Not used at this time
                }
                if (bitIsActive[20])
                {
                    statusStrings.Add(errorRealTimeClockText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DeviceHealthAndError.GetBhmDeviceHealthStatus(UInt32, ProgramBrand)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return statusStrings;
        }

        //        public static List<string> GetBhmExtendedDeviceHealthStatus(UInt16 extendedDeviceHealthBitEncoded)
        //        {
        //            List<string> statusStrings = new List<string>();
        //            try
        //            {
        //                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(extendedDeviceHealthBitEncoded);
        //                /// we are using zero-indexing
        //                if (bitIsActive[0])
        //                {
        //                    statusStrings.Add(systemInStopModeText);
        //                }
        //                if (bitIsActive[1])
        //                {
        //                    statusStrings.Add(systemInPauseModeText);
        //                }
        //                if (bitIsActive[2])
        //                {
        //                    /// Not used at this time
        //                }
        //                if (bitIsActive[3])
        //                {
        //                    /// Not used at this time
        //                }
        //                if (bitIsActive[4])
        //                {
        //                    statusStrings.Add(errorRealTimeClockText);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetDeviceHealthStatus(UInt16)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return statusStrings;
        //        }




        //        public static List<string> GetBhmEquipmentErrors(UInt16 equipmentErrorBitEncoded)
        //        {
        //            List<string> errorStrings = new List<string>();
        //            try
        //            {
        //                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(equipmentErrorBitEncoded);

        //                if (bitIsActive[0])
        //                {
        //                    errorStrings.Add("Yellow Alarm");
        //                }
        //                else if (bitIsActive[1])
        //                {
        //                    errorStrings.Add("Red Alarm");
        //                }
        //                else
        //                {
        //                    errorStrings.Add("No alarms, everything is normal");
        //                }

        //                if (bitIsActive[8])
        //                {
        //                    errorStrings.Add("Set 1 BC Phase Swap has occurred");
        //                }

        //                if (bitIsActive[9])
        //                {
        //                    errorStrings.Add("Set 1 has high initial balance > 0.5%");
        //                }

        //                if (bitIsActive[10])
        //                {
        //                    errorStrings.Add("Set 2 BC Phase Swap has occurred");
        //                }

        //                if (bitIsActive[11])
        //                {
        //                    errorStrings.Add("Set 2 has high initial balance > 0.5%");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetEquipmentErrors(UInt16)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorStrings;
        //        }


        public static List<string> GetPdmDeviceHealthStatus(UInt16 deviceHealthBitEncoded, bool ignoreDisabledSetting, ProgramBrand argProgramBrand)
        {
            List<string> statusStrings = new List<string>();
            try
            {
                Dictionary<int, bool> bitIsActive = ConversionMethods.GetZeroIndexedBitEncodingAsDictionary(deviceHealthBitEncoded);
                /// we are using zero-indexing
                if (bitIsActive[0])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(flashWriteErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(flashWriteErrorMeggitText);
                    }
                }
                if (bitIsActive[1])
                {
                    if ((argProgramBrand == ProgramBrand.DynamicRatings) || (argProgramBrand == ProgramBrand.DevelopmentVersion))
                    {
                        statusStrings.Add(flashReadErrorDynamicRatingsText);
                    }
                    else if (argProgramBrand == ProgramBrand.Meggitt)
                    {
                        statusStrings.Add(flashReadErrorMeggitText);
                    }
                }
                if (bitIsActive[2])
                {
                    if (!ignoreDisabledSetting)
                    {
                        statusStrings.Add(monitoringStoppedText);
                    }
                }
                if (bitIsActive[3])
                {
                    statusStrings.Add(monitoringPausedText);
                }
                if (bitIsActive[4])
                {
                    statusStrings.Add(synchronizationMalfunctionOrSignalLossText);
                }
                if (bitIsActive[5])
                {
                    statusStrings.Add(measuringChannelDidNotPassCalibrationAndTestText);
                }
                if (bitIsActive[6])
                {
                    statusStrings.Add(amplitudeFilterChannelDidNotPassCalibrationAndTestText);
                }
                if (bitIsActive[7])
                {
                    statusStrings.Add(timeAndPolarityChannelDidNotPassCalibrationAndTestText);
                }
                if (bitIsActive[8])
                {
                    statusStrings.Add(clockTimeIsEarlierThanTheTimeOfTheLastRecordInMemoryText);
                }
                if (bitIsActive[9])
                {
                    statusStrings.Add(errorInInitialReadingText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetDeviceHealthStatus(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return statusStrings;
        }

        #endregion

        #region Event Codes

        public static string GetBhmEventCodeAsString(int eventCode)
        {
            string eventAsString = string.Empty;
            try
            {
                switch (eventCode)
                {
                    case 1:
                        eventAsString = dataMemoryClearedText;
                        break;
                    case 2:
                        eventAsString = dataAndBalancingInfoClearedText;
                        break;
                    case 3:
                        eventAsString = gammaRedAlarmText;
                        break;
                    case 4:
                        eventAsString = gammaTrendAlarmText;
                        break;
                    case 5:
                        eventAsString = powerTurnedOnText;
                        break;
                    case 6:
                        eventAsString = flashWriteFailureText;
                        break;
                    case 7:
                        eventAsString = flashReadFailureText;
                        break;
                    case 8:
                        eventAsString = phaseShiftSetOneText;
                        break;
                    case 9:
                        eventAsString = phaseShiftSetTwoText;
                        break;
                    case 10:
                        eventAsString = channelsQuestionText;
                        break;
                    case 11:
                        eventAsString = wrongFrequencyText;
                        break;
                    case 12:
                        eventAsString = lowSignalText;
                        break;
                    case 13:
                        eventAsString = highSignalText;
                        break;
                    case 14:
                        eventAsString = unitOffText;
                        break;
                    case 15:
                        eventAsString = trendAlarmText;
                        break;
                    case 16:
                        eventAsString = tkAlarmText;
                        break;
                    case 17:
                        eventAsString = setOneOffText;
                        break;
                    case 18:
                        eventAsString = setTwoOffText;
                        break;
                    case 19:
                        eventAsString = systemBalancedText;
                        break;
                    case 20:
                        eventAsString = gammaYellowAlarmText;
                        break;
                    case 21:
                        eventAsString = logWasClearedText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayBhmTab.GetEventCodeAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return eventAsString;
        }

        public static string GetPdmEventCodeAsString(int eventCode)
        {
            string eventAsString = string.Empty;
            try
            {
                switch (eventCode)
                {
                    case 1:
                        eventAsString = dataMemoryClearedText;
                        break;
                    case 2:
                        eventAsString = dataAndBalancingInfoClearedText;
                        break;
                    case 3:
                        eventAsString = gammaRedAlarmText;
                        break;
                    case 4:
                        eventAsString = gammaTrendAlarmText;
                        break;
                    case 5:
                        eventAsString = powerTurnedOnText;
                        break;
                    case 6:
                        eventAsString = flashWriteFailureText;
                        break;
                    case 7:
                        eventAsString = flashReadFailureText;
                        break;
                    case 8:
                        eventAsString = phaseShiftSetOneText;
                        break;
                    case 9:
                        eventAsString = phaseShiftSetTwoText;
                        break;
                    case 10:
                        eventAsString = channelsQuestionText;
                        break;
                    case 11:
                        eventAsString = wrongFrequencyText;
                        break;
                    case 12:
                        eventAsString = lowSignalText;
                        break;
                    case 13:
                        eventAsString = highSignalText;
                        break;
                    case 14:
                        eventAsString = unitOffText;
                        break;
                    case 15:
                        eventAsString = trendAlarmText;
                        break;
                    case 16:
                        eventAsString = tkAlarmText;
                        break;
                    case 17:
                        eventAsString = setOneOffText;
                        break;
                    case 18:
                        eventAsString = setTwoOffText;
                        break;
                    case 19:
                        eventAsString = systemBalancedText;
                        break;
                    case 20:
                        eventAsString = gammaYellowAlarmText;
                        break;
                    case 21:
                        eventAsString = magnitudeMaxAmplitudeWarningText;
                        break;
                    case 22:
                        eventAsString = magnitudeMaxAmplitudeAlarmText;
                        break;
                    case 23:
                        eventAsString = magnitudeMaxTrendWarningText;
                        break;
                    case 24:
                        eventAsString = magnitudeMaxTrendAlarmText;
                        break;
                    case 25:
                        eventAsString = magnitudeMaxChangeWarningText;
                        break;
                    case 26:
                        eventAsString = magnitudeMaxChangeAlarmText;
                        break;
                    case 27:
                        eventAsString = pdiChangeWarningText;
                        break;
                    case 28:
                        eventAsString = pdiChangeAlarmText;
                        break;
                    case 29:
                        eventAsString = spuriousText;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplayPdmTab.GetEventCodeAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return eventAsString;
        }
        #endregion
    }
}
