using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Commands;
using Telerik.WinControls.Enumerations;
using GeneralUtilities;
using DatabaseInterface;

namespace DatabaseFileInteraction
{
    public partial class ExportData : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// A DataSet representation of the database, used for SQL-type interaction, rather than Linq to SQL interaction
        /// </summary>
        MonitorInterfaceDBDataSet1 monitorInterfaceDBDataSet;
        /// <summary>
        /// A linq connection to the database
        /// </summary>
        // MonitorInterfaceDB db;
        /// <summary>
        /// An sql connection to the database
        /// </summary>
        SqlConnection dbConnection;

        // Data Adapters
        SqlDataAdapter companyDataAdapter;
        SqlDataAdapter plantDataAdapter;
        SqlDataAdapter equipmentDataAdapter;
        SqlDataAdapter monitorDataAdapter;

        // Command Builders
        SqlCommandBuilder companyCommandBuilder;
        SqlCommandBuilder plantCommandBuilder;
        SqlCommandBuilder equipmentCommandBuilder;
        SqlCommandBuilder monitorCommandBuilder;

        // Binding sources
        BindingSource companyBindingSource;
        BindingSource plantCompanyBindingSource;
        BindingSource equipmentPlantBindingSource;
        BindingSource monitorEquipmentBindingSource;

      // List<string> exportErrorList;
        bool errorsOccurredDuringLastExport = false;
       
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

      //  private static string generalErrorsOccurredDuringExportText = "There were errors during the data export operation.\nClick on the 'View Export Errors' button to see them.";
      //  private static string requiredStructureWasNotCreatedInNewDatabaseText = "Failed to find the required Companies, Plants, Equipment, and Monitors in the new database.";
        private static string failedToCopyEmptyDBToNewLocationText = "Failed to copy the default empty database to the new location.  Export of data failed.";
        private static string cannotFindDefaultEmptyDatabaseText = "Cannot find default empty datbase to copy.\nIt has been moved or deleted.\nYou may have to re-install Athena.";
        private static string noMonitorsSelectedText = "No monitors were selected so no data can be exported.";
        private static string cannotConnectToCurrentDatabaseText = "Could not connect to the current database.\nYou will need to open a connection in the main interface first.";
        private static string noErrorsDuringLastExportText = "No errors occurred during the last data export.";

       // private static string exportDataThreadProbablyTerminatedInErrorStateText = "Data export probably terminated in an error state.";

       // private static string failedToCopyTheDataToNewDatabaseText = "Failed to copy the data to the new database.  Data dates follow.";
      //  private static string failedToCopyTheMonitorConfigurationsText = "Failed to copy the monitor configurations to the new database.";
       // private static string failedToCopyTheDataViewerPreferencesText = "Failed to copy the data viewer preferences to the new database.";

       // private static string configurationsText = "Configurations";
        private static string notepadFileName = "notepad.exe";
       // private static string ofText = "of";
       // private static string noneText = "None";
       // private static string dataText = "Data";

        private static string exportDataTitleText = "Export Data";
        private static string expandTreeRadButtonText = "Expand Tree";
        private static string collapseTreeRadButtonText = "Collapse Tree";
        private static string selectAllRadButtonText = "Select All";
        private static string unselectAllRadButtonText = "Unselect All";
        private static string exportDataRadButtonText = "Export Data\nfor\nSelected Monitors As";
        private static string viewExportErrorsRadButtonText = "View Export Errors";
        //private static string copyingInformationFromRadLabelText = "Copying information from";
        //private static string monitorNumberRadLabelText = "Monitor No.:";
       // private static string monitorNumberValueRadLabelText = "";
       // private static string companyRadLabelText = "Company:";
     //   private static string companyNameValueRadLabelText = "";
       // private static string plantRadLabelText = "Plant:";
      //  private static string plantNameValueRadLabelText = "";
       // private static string equipmentRadLabelText = "Equipment:";
       // private static string equipmentNameValueRadLabelText = "";
       // private static string monitorRadLabelText = "Monitor:";
       // private static string monitorNameValueRadLabelText = "";
      //  private static string typeRadLabelText = "Type:";
      //  private static string dataTypeValueRadLabelText = "";
      //  private static string itemNumberRadLabelText = "Item No.:";
      //  private static string itemNumberValueRadLabelText = "";

        private static string dbConnectionString;
        private static string applicationDataPath;
        //private static string executablePath;
        //private static string emptyDatabaseFolderName;
        //private static string defaultDatabaseName;
        //private static string defaultDatabaseLogfileName;
        private static string emptyProgramDatabaseNameWithFullPath;
        private static string databaseExportErrorFileName;

      //  private static string copyingDataText = "Copying data";
      //  private static string copyingPreferencesText = "Copying preferences";
     //   private static string copyingConfigurationsText = "Copying configurations";

        private static string exportCancelledMessageText = "Export cancelled - you may want to delete the incomplete export database";
    //    private static string exportNotInprogressText = "No data export is running";
        private static string errorDuringExportWarningText = "Some potential errors occurred during data export - view the export error log for details";

        private static string companyQueryString = "SELECT ID, Name, Address, Comments FROM Company ORDER BY Name";
        private static string plantQueryString = "SELECT ID, CompanyID, Name, Address, Longitude, Latitude, Comments FROM Plant ORDER BY Name";
        private static string equipmentQueryString = "SELECT ID, PlantID, Name, Tag, SerialNumber, Voltage, Type, Comments, Enabled FROM Equipment ORDER BY Name";
        private static string monitorQueryString = "SELECT ID, EquipmentID, MonitorType, ConnectionType, BaudRate, ModbusAddress, Enabled, IPaddress, MonitorNumber, DateOfLastDataDownload FROM Monitor ORDER BY MonitorNumber";


        public ExportData(string argDbConnectionString, string argApplicationDataPath, string argEmptyProgramDatabaseNameWithFullPath, string argDatabaseExportErrorFileName)
        {
            InitializeComponent();

            dbConnectionString = argDbConnectionString;
            applicationDataPath = argApplicationDataPath;
            emptyProgramDatabaseNameWithFullPath = argEmptyProgramDatabaseNameWithFullPath;
            databaseExportErrorFileName = argDatabaseExportErrorFileName;

            AssignStringValuesToInterfaceObjects();

            this.StartPosition = FormStartPosition.CenterParent;
        }


        //public ExportData(string inputDbConnectionString, string inputApplicationDataPath, string inputExecutablePath, string inputEmptyDatabaseFolderName, string inputDefaultDatabaseName, string inputDefaultDatabaseLogfileName, string inputDatabaseExportErrorFileName)
        //{
        //    InitializeComponent();

        //    dbConnectionString = inputDbConnectionString;
        //    applicationDataPath = inputApplicationDataPath;
        //    executablePath = inputExecutablePath;
        //    emptyDatabaseFolderName = inputEmptyDatabaseFolderName;
        //    defaultDatabaseName = inputDefaultDatabaseName;
        //    defaultDatabaseLogfileName = inputDefaultDatabaseLogfileName;
        //    databaseExportErrorFileName = inputDatabaseExportErrorFileName;

        //    AssignStringValuesToInterfaceObjects();
        //    this.StartPosition = FormStartPosition.CenterParent;
        //}



        private void ExportData_Load(object sender, EventArgs e)
        {
            InitializeDatabaseInteractionVariables();
            SetNewConnectionAndGetCurrentData();

            this.monitorRadTreeView.CheckBoxes = true;


            BuildTreeFromBindingSourceData();
            // InitializeExportDataBackgroungWorker();
        }

        private void BuildTreeFromBindingSourceData()
        {
            /// This was done, instead of just attaching the binding source to the tree and letting it do all the work, so that I could assign the database ID value
            /// of the object associated with each node to the node's Tag object as the tree was built.  Having the Tag be the ID helped when finding nodes to change
            /// their color and checkstate.  This assigment wasn't possible using the standard RadTree commands, though it sort of looks like you can do that. With
            /// standard commands, you can only assign IDs to Tags at the topmost level of the tree.
            try
            {
                int companyBindingSourcePosition;
                int plantCompanyBindingSourcePosition;
                int equipmentPlantBindingSourcePosition;
                int monitorEquipmentBindingSourcePosition;

                RadTreeNode companyNode;
                RadTreeNode plantNode;
                RadTreeNode equipmentNode;
                RadTreeNode monitorNode;

                // newer version of telerik was buggy at this point and would not clear the nodes without barfing, 
                // even this mess of fixes didn't help (RemoveAt() failed as well on the 0th node), so I rolled back to the previous version.

                //if (this.monitorRadTreeView.Nodes != null)
                //{
                //    try
                //    {
                //        for (int i = monitorRadTreeView.Nodes.Count - 1; i > -1; i--)
                //        {
                //            monitorRadTreeView.Nodes.RemoveAt(i);
                //        }

                //        // this.monitorRadTreeView.Nodes.Clear();
                //    }
                //    catch (Exception)
                //    {

                //    }
                //}

                this.monitorRadTreeView.Nodes.Clear();

                if (companyBindingSource.Count > 0)
                {
                    companyBindingSourcePosition = 0;
                    while (companyBindingSourcePosition < companyBindingSource.Count)
                    {
                        companyBindingSource.Position = companyBindingSourcePosition;
                        companyNode = new RadTreeNode();
                        companyNode.Text = ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).Name;
                        companyNode.Tag = ((MonitorInterfaceDBDataSet1.CompanyRow)((DataRowView)companyBindingSource.Current).Row).ID;
                       // companyNode.ShowCheckBox = false;
                        companyNode.CheckType = CheckType.CheckBox;

                        if (plantCompanyBindingSource.Count > 0)
                        {
                            plantCompanyBindingSourcePosition = 0;
                            while (plantCompanyBindingSourcePosition < plantCompanyBindingSource.Count)
                            {
                                plantCompanyBindingSource.Position = plantCompanyBindingSourcePosition;
                                plantNode = new RadTreeNode();
                                plantNode.Text = ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).Name;
                                plantNode.Tag = ((MonitorInterfaceDBDataSet1.PlantRow)((DataRowView)plantCompanyBindingSource.Current).Row).ID;
                                // plantNode.ShowCheckBox = false;
                                plantNode.CheckType = CheckType.None;

                                if (equipmentPlantBindingSource.Count > 0)
                                {
                                    equipmentPlantBindingSourcePosition = 0;
                                    while (equipmentPlantBindingSourcePosition < equipmentPlantBindingSource.Count)
                                    {
                                        equipmentPlantBindingSource.Position = equipmentPlantBindingSourcePosition;
                                        equipmentNode = new RadTreeNode();
                                        equipmentNode.Text = ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).Name;
                                        equipmentNode.Tag = ((MonitorInterfaceDBDataSet1.EquipmentRow)((DataRowView)equipmentPlantBindingSource.Current).Row).ID;
                                        // equipmentNode.ShowCheckBox = false;
                                        equipmentNode.CheckType = CheckType.None;

                                        if (monitorEquipmentBindingSource.Count > 0)
                                        {
                                            monitorEquipmentBindingSourcePosition = 0;
                                            while (monitorEquipmentBindingSourcePosition < monitorEquipmentBindingSource.Count)
                                            {
                                                monitorEquipmentBindingSource.Position = monitorEquipmentBindingSourcePosition;
                                                monitorNode = new RadTreeNode();
                                                monitorNode.Text = ((MonitorInterfaceDBDataSet1.MonitorRow)((DataRowView)monitorEquipmentBindingSource.Current).Row).MonitorType;
                                                monitorNode.Tag = ((MonitorInterfaceDBDataSet1.MonitorRow)((DataRowView)monitorEquipmentBindingSource.Current).Row).ID;
                                                // monitorNode.ShowCheckBox = true;

                                                equipmentNode.Nodes.Add(monitorNode);
                                                monitorEquipmentBindingSourcePosition++;
                                            }
                                        }
                                        plantNode.Nodes.Add(equipmentNode);
                                        equipmentPlantBindingSourcePosition++;
                                    }
                                }
                                companyNode.Nodes.Add(plantNode);
                                plantCompanyBindingSourcePosition++;
                            }
                        }
                        this.monitorRadTreeView.Nodes.Add(companyNode);
                        companyBindingSourcePosition++;
                    }

                    /// Hack to keep the checkboxes off of the company level nodes.  The Telerik people could not help
                    /// me with finding a more elegant solution.
                    //if (this.monitorRadTreeView.Nodes != null)
                    //{
                    //    for (int i = 0; i < this.monitorRadTreeView.Nodes.Count; i++)
                    //    {
                    //        // this.monitorRadTreeView.Nodes[i].CheckType = CheckType.None;
                    //        this.monitorRadTreeView.Nodes[i].ShowCheckBox = false;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.BuildTreeFromBindingSourceData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Dictionary<Guid, int> GetSelectedMonitors()
        {
            Dictionary<Guid, int> selectedMonitors = new Dictionary<Guid, int>();
            try
            {
                Guid monitorID;
                if (this.monitorRadTreeView.Nodes != null)
                {
                    for (int i = 0; i < monitorRadTreeView.Nodes.Count; i++)
                    {
                        if (monitorRadTreeView.Nodes[i].Nodes != null)
                        {
                            for (int j = 0; j < monitorRadTreeView.Nodes[i].Nodes.Count; j++)
                            {
                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes != null)
                                {
                                    for (int k = 0; k < monitorRadTreeView.Nodes[i].Nodes[j].Nodes.Count; k++)
                                    {
                                        if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Nodes != null)
                                        {
                                            for (int l = 0; l < monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Nodes.Count; l++)
                                            {
                                                if (monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Nodes[l].CheckState == ToggleState.On)
                                                {
                                                    monitorID = (Guid)monitorRadTreeView.Nodes[i].Nodes[j].Nodes[k].Nodes[l].Tag;
                                                    if (!selectedMonitors.ContainsKey(monitorID))
                                                    {
                                                        selectedMonitors.Add(monitorID, 1);
                                                    }
                                                    else
                                                    {
                                                        string errorMessage = "Error in MainDisplay.GetSelectedMonitors()\nDuplicate monitor IDs found.";
                                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                                        MessageBox.Show(errorMessage);
#endif
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.GetSelectedMonitors()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedMonitors;
        }



        /// <summary>
        /// Initializes the Linq and DataSet Class DB interaction variables and the 
        /// binding source variables.
        /// </summary>
        private void InitializeDatabaseInteractionVariables()
        {
            try
            {
                // db = new MonitorInterfaceDB(dbConnectionString);
                monitorInterfaceDBDataSet = new MonitorInterfaceDBDataSet1();

                companyBindingSource = new BindingSource();
                companyBindingSource.DataSource = monitorInterfaceDBDataSet;
                companyBindingSource.DataMember = "Company";

                plantCompanyBindingSource = new BindingSource();
                plantCompanyBindingSource.DataSource = companyBindingSource;
                plantCompanyBindingSource.DataMember = "FK_Plant_Company";

                equipmentPlantBindingSource = new BindingSource();
                equipmentPlantBindingSource.DataSource = plantCompanyBindingSource;
                equipmentPlantBindingSource.DataMember = "FK_Equipment_Plant";

                monitorEquipmentBindingSource = new BindingSource();
                monitorEquipmentBindingSource.DataSource = equipmentPlantBindingSource;
                monitorEquipmentBindingSource.DataMember = "FK_Monitor_Equipment";
                monitorEquipmentBindingSource.Sort = "MonitorNumber ASC";
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.InitializeDatabaseInteractionVariables()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Establishes a new connection to the database and gets all the current data.  This
        /// method will be called every time the database information associated with the 
        /// binding sources has been updated using a different database connection.
        /// </summary>
        private void SetNewConnectionAndGetCurrentData()
        {
            try
            {
                //if (this.databaseConnectionStringIsCorrect)
                //{
                this.dbConnection = new SqlConnection(ExportData.dbConnectionString);

                companyDataAdapter = new SqlDataAdapter(ExportData.companyQueryString, this.dbConnection);
                plantDataAdapter = new SqlDataAdapter(ExportData.plantQueryString, this.dbConnection);
                equipmentDataAdapter = new SqlDataAdapter(ExportData.equipmentQueryString, this.dbConnection);
                monitorDataAdapter = new SqlDataAdapter(ExportData.monitorQueryString, this.dbConnection);

                companyCommandBuilder = new SqlCommandBuilder(companyDataAdapter);
                plantCommandBuilder = new SqlCommandBuilder(plantDataAdapter);
                equipmentCommandBuilder = new SqlCommandBuilder(equipmentDataAdapter);
                monitorCommandBuilder = new SqlCommandBuilder(monitorDataAdapter);

                FillCompanyDataTable();
                FillPlantDataTable();
                FillEquipmentDataTable();
                FillMonitorDataTable();
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.SetNewConnectionAndGetCurrentData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        #region Table Fill methods

        /// <summary>
        /// Fills the company data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillCompanyDataTable()
        {
            try
            {
                companyDataAdapter.Fill(monitorInterfaceDBDataSet.Company);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.FillCompanyDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the plant data table assoicated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillPlantDataTable()
        {
            try
            {
                plantDataAdapter.Fill(this.monitorInterfaceDBDataSet.Plant);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.FillPlantDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the equipment data table associated with th MonitorInterfaceDBDataSet
        /// </summary>
        private void FillEquipmentDataTable()
        {
            try
            {
                equipmentDataAdapter.Fill(this.monitorInterfaceDBDataSet.Equipment);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.FillEquipmentDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Fills the monitor data table associated with the MonitorInterfaceDBDataSet
        /// </summary>
        private void FillMonitorDataTable()
        {
            try
            {
                monitorDataAdapter.Fill(this.monitorInterfaceDBDataSet.Monitor);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.FillMonitorDataTable()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        #endregion

        private void expandTreeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.monitorRadTreeView.ExpandAll();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.expandTreeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void collapseTreeRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.monitorRadTreeView.CollapseAll();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.collapseTreeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void exportDataRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                CopyHierarchyAndDataToExportDatabase();
                SqlConnection.ClearAllPools();
                // here we can do other things like zip up the copied database and logfile
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.collapseTreeRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CopyHierarchyAndDataToExportDatabase()
        {
            try
            {
                if (TestDatabaseConnectionString(ExportData.dbConnectionString))
                {
                    Dictionary<Guid, int> selectedMonitors = GetSelectedMonitors();
                    if (selectedMonitors.Count > 0)
                    {
                        SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(ExportData.dbConnectionString);
                        string dbNameWithFullPath = connectionStringBuilder.AttachDBFilename;
                        //string dbNameWithExtension = Path.GetFileName(dbNameWithFullPath);
                        string dbNameWithoutExtension = Path.GetFileNameWithoutExtension(dbNameWithFullPath);
                        string dateTimeString = FileUtilities.GetCurrentDateTimeAsPartialFileNameString();

                        //string temp = DateTime.Now.ToString();
                        //temp = temp.Replace('/', '-');
                        //temp = temp.Replace(':', '-');
                        //string suffix = "_Export_" + temp;
                        //string initialSaveFileName = dbNameWithoutExtension + suffix + ".mdf";

                        string initialSaveFileName = dbNameWithoutExtension + "_Export_" + dateTimeString + ".mdf";

                        string initialSaveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                        string saveDbNameWithPath = FileUtilities.GetNewFileNameToCopy(initialSaveFileName, initialSaveDirectory);

                        string saveDbPath = Path.GetDirectoryName(saveDbNameWithPath);
                        string saveDbName = Path.GetFileName(saveDbNameWithPath);
                        string saveDbNameWithoutExtension = Path.GetFileNameWithoutExtension(saveDbNameWithPath);
                        string saveDbLogFileName = saveDbNameWithoutExtension + "_log.ldf";

                        connectionStringBuilder.AttachDBFilename = saveDbNameWithPath;

                        string destinationDbConnectionString = connectionStringBuilder.ConnectionString;

                        /// create a copy of the empty db in the new location
                        // check to see if the source db exists
                        // if so, copy it
                        if(DatabaseFileMethods.DatabaseExists(emptyProgramDatabaseNameWithFullPath)== ErrorCode.DatabaseFound)
                       // if (DatabaseFileMethods.DefaultEmptyDatabaseExists(executablePath, emptyDatabaseFolderName, defaultDatabaseName, defaultDatabaseLogfileName))
                        {
                            if(DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(this, emptyProgramDatabaseNameWithFullPath, saveDbNameWithPath))
                            //if (DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(this, saveDbName, saveDbPath, executablePath, emptyDatabaseFolderName, defaultDatabaseName, defaultDatabaseLogfileName))
                            {
                                using (CopyDatabaseData copyData = new CopyDatabaseData(selectedMonitors, ExportData.dbConnectionString, destinationDbConnectionString, CopyOperationType.Export))
                                {
                                    copyData.ShowDialog();
                                    copyData.Hide();
                                    if (copyData.CopyWasCancelled)
                                    {
                                        RadMessageBox.Show(this, exportCancelledMessageText);
                                    }
                                    else if ((copyData.CopyErrorsList != null) && (copyData.CopyErrorsList.Count > 0))
                                    {
                                        FileUtilities.SaveListOfStringAsFile(ExportData.applicationDataPath, ExportData.databaseExportErrorFileName, copyData.CopyErrorsList, false);
                                        this.errorsOccurredDuringLastExport = true;
                                        RadMessageBox.Show(this, errorDuringExportWarningText);
                                    }
                                }   
                            }
                            else
                            {
                                RadMessageBox.Show(this, failedToCopyEmptyDBToNewLocationText);
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, cannotFindDefaultEmptyDatabaseText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noMonitorsSelectedText);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, cannotConnectToCurrentDatabaseText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.CopyHierarchyAndDataToExportDatabase()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public bool TestDatabaseConnectionString(string connectionString)
        {
            bool databaseConnectionStringIsCorrect = false;
            try
            {
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(connectionString))
                {
                    databaseConnectionStringIsCorrect = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.TestDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return databaseConnectionStringIsCorrect;
        }

     
        private void viewExportErrorsRadButton_Click(object sender, EventArgs e)
        {
            if (errorsOccurredDuringLastExport)
            {
                Process p = new Process();
                p.StartInfo.FileName = notepadFileName;
                p.StartInfo.Arguments = Path.Combine(ExportData.applicationDataPath, ExportData.databaseExportErrorFileName);
                p.Start();
            }
            else
            {
                RadMessageBox.Show(this, noErrorsDuringLastExportText);
            }
        }

        private void selectAllRadButton_Click(object sender, EventArgs e)
        {
            SetCheckboxToggleState(true);
        }

        private void unselectAllRadButton_Click(object sender, EventArgs e)
        {
            SetCheckboxToggleState(false);
        }

        /// <summary>
        /// Checks or unchecks the checkboxes for all equipment in the tree
        /// </summary>
        /// <param name="toggleStateOn"></param>
        private void SetCheckboxToggleState(bool toggleStateOn)
        {
            try
            {
                if (this.monitorRadTreeView.Nodes != null)
                {
                    foreach (RadTreeNode company in this.monitorRadTreeView.Nodes)
                    {
                        if (company.Nodes != null)
                        {
                            foreach (RadTreeNode plant in company.Nodes)
                            {
                                if (plant.Nodes != null)
                                {
                                    foreach (RadTreeNode equipment in plant.Nodes)
                                    {
                                        if (equipment.Nodes != null)
                                        {
                                            foreach (RadTreeNode monitor in equipment.Nodes)
                                            {
                                                if (toggleStateOn)
                                                {
                                                    monitor.CheckState = ToggleState.On;
                                                }
                                                else
                                                {
                                                    monitor.CheckState = ToggleState.Off;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ExportData.SetCheckboxToggleState(bool)\nTree view had no nodes.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.SetCheckboxToggleState(bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = exportDataTitleText;

                expandTreeRadButton.Text = expandTreeRadButtonText;
                collapseTreeRadButton.Text = collapseTreeRadButtonText;
                selectAllRadButton.Text = selectAllRadButtonText;
                unselectAllRadButton.Text = unselectAllRadButtonText;
                exportDataRadButton.Text = exportDataRadButtonText;
                viewExportErrorsRadButton.Text = viewExportErrorsRadButtonText;
                //copyingInformationFromRadLabel.Text = copyingInformationFromRadLabelText;
                //monitorNumberRadLabel.Text = monitorNumberRadLabelText;
                //monitorNumberValueRadLabel.Text = monitorNumberValueRadLabelText;
                //companyRadLabel.Text = companyRadLabelText;
                //companyNameValueRadLabel.Text = companyNameValueRadLabelText;
                //plantRadLabel.Text = plantRadLabelText;
                //plantNameValueRadLabel.Text = plantNameValueRadLabelText;
                //equipmentRadLabel.Text = equipmentRadLabelText;
                //equipmentNameValueRadLabel.Text = equipmentNameValueRadLabelText;
                //monitorRadLabel.Text = monitorRadLabelText;
                //monitorNameValueRadLabel.Text = monitorNameValueRadLabelText;
                //typeRadLabel.Text = typeRadLabelText;
                //dataTypeValueRadLabel.Text = dataTypeValueRadLabelText;
                //itemNumberRadLabel.Text = itemNumberRadLabelText;
                //itemNumberValueRadLabel.Text = itemNumberValueRadLabelText;

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, htmlFontType, htmlStandardFontSize, "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, htmlFontType, htmlStandardFontSize, "");

                //generalErrorsOccurredDuringExportText = LanguageConversion.GetStringAssociatedWithTag("ExportDataGeneralErrorsOccurredDuringExportText", generalErrorsOccurredDuringExportText, htmlFontType, htmlStandardFontSize, "");
                //requiredStructureWasNotCreatedInNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ExportDataRequiredStructureWasNotCreatedInNewDatabaseText", requiredStructureWasNotCreatedInNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                failedToCopyEmptyDBToNewLocationText = LanguageConversion.GetStringAssociatedWithTag("ExportDataFailedToCopyEmbtyDbToNewLocationText", failedToCopyEmptyDBToNewLocationText, htmlFontType, htmlStandardFontSize, "");
                cannotFindDefaultEmptyDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ExportDataCannotFindDefaultEmptyDatabaseText", cannotFindDefaultEmptyDatabaseText, htmlFontType, htmlStandardFontSize, "");
                noMonitorsSelectedText = LanguageConversion.GetStringAssociatedWithTag("ExportDataNoMonitorsSelectedText", noMonitorsSelectedText, htmlFontType, htmlStandardFontSize, "");
                cannotConnectToCurrentDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ExportDataCannotConnectToCurrentDatabaseText", cannotConnectToCurrentDatabaseText, htmlFontType, htmlStandardFontSize, "");
                noErrorsDuringLastExportText = LanguageConversion.GetStringAssociatedWithTag("ExportDataNoErrorsDuringLastExportText", noErrorsDuringLastExportText, htmlFontType, htmlStandardFontSize, "");
                //failedToCopyTheDataToNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("ExportDataFailedToCopyTheDataToNewDatabaseText", failedToCopyTheDataToNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //failedToCopyTheMonitorConfigurationsText = LanguageConversion.GetStringAssociatedWithTag("ExportDataFailedToCopyTheMonitorConfigurationsText", failedToCopyTheMonitorConfigurationsText, htmlFontType, htmlStandardFontSize, "");
                //failedToCopyTheDataViewerPreferencesText = LanguageConversion.GetStringAssociatedWithTag("ExportDataFailedToCopyTheDataViewerPreferencesText", failedToCopyTheDataViewerPreferencesText, htmlFontType, htmlStandardFontSize, "");
                //configurationsText = LanguageConversion.GetStringAssociatedWithTag("ExportDataConfigurationsText", configurationsText, htmlFontType, htmlStandardFontSize, "");
                notepadFileName = LanguageConversion.GetStringAssociatedWithTag("ExportDataNotepadExeText", notepadFileName, "", "", "");
                //ofText = LanguageConversion.GetStringAssociatedWithTag("ExportDataOfText", ofText, htmlFontType, htmlStandardFontSize, "");
                //noneText = LanguageConversion.GetStringAssociatedWithTag("ExportDataNoneText", noneText, htmlFontType, htmlStandardFontSize, "");
                //dataText = LanguageConversion.GetStringAssociatedWithTag("ExportDataDataText", dataText, htmlFontType, htmlStandardFontSize, "");

                exportDataTitleText = LanguageConversion.GetStringAssociatedWithTag("ExportDataTitleText", exportDataTitleText, htmlFontType, htmlStandardFontSize, "");
                expandTreeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceExpandTreeRadButtonTree", expandTreeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                collapseTreeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceCollapseTreeRadButtonTree", collapseTreeRadButtonText, htmlFontType, htmlStandardFontSize, "");
                selectAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceSelectAllRadButtonText", selectAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                unselectAllRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceUnselectAllRadButtonText", unselectAllRadButtonText, htmlFontType, htmlStandardFontSize, "");
                exportDataRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceExportDataRadButtonText", exportDataRadButtonText, htmlFontType, htmlStandardFontSize, "");
                viewExportErrorsRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceViewExportErrorsRadButtonText", viewExportErrorsRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyingInformationFromRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceCopyingInformationFromRadLabelText", copyingInformationFromRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //monitorNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceMonitorNumberRadLabelText", monitorNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");
               // monitorNumberValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for monitorNumberValueRadLabelText", monitorNumberValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
               // companyRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceCompanyRadLabelText", companyRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //companyNameValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for companyNameValueRadLabelText", companyNameValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
               // plantRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfacePlantRadLabelText", plantRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //plantNameValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for plantNameValueRadLabelText", plantNameValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
               // equipmentRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceEquipmentRadLabelText", equipmentRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //equipmentNameValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for equipmentNameValueRadLabelText", equipmentNameValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
              //  monitorRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceMonitorRadLabelText", monitorRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //monitorNameValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for monitorNameValueRadLabelText", monitorNameValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
              //  typeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceTypeRadLabelText", typeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //dataTypeValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for dataTypeValueRadLabelText", dataTypeValueRadLabelText, htmlFontType, htmlStandardFontSize, "");
             //   itemNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("ExportDataInterfaceItemNumberRadLabelText", itemNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //itemNumberValueRadLabelText = LanguageConversion.GetStringAssociatedWithTag("tag for itemNumberValueRadLabelText", itemNumberValueRadLabelText, htmlFontType, htmlStandardFontSize, "");

                //copyingDataText = LanguageConversion.GetStringAssociatedWithTag("ExportDataCopyingDataText", copyingDataText, htmlFontType, htmlStandardFontSize, "");
                //copyingPreferencesText = LanguageConversion.GetStringAssociatedWithTag("ExportDataCopyingPreferencesText", copyingPreferencesText, htmlFontType, htmlStandardFontSize, "");
                //copyingConfigurationsText = LanguageConversion.GetStringAssociatedWithTag("ExportDataCopyingConfigurationsText", copyingConfigurationsText, htmlFontType, htmlStandardFontSize, "");

                //exportDataThreadProbablyTerminatedInErrorStateText = LanguageConversion.GetStringAssociatedWithTag("ExportDataExportDataThreadProbablyTerminatedInErrorStateText", exportDataThreadProbablyTerminatedInErrorStateText, htmlFontType, htmlStandardFontSize, "");

                exportCancelledMessageText = LanguageConversion.GetStringAssociatedWithTag("ExportDataExportCancelledMessageText", exportCancelledMessageText, htmlFontType, htmlStandardFontSize, "");
              //  exportNotInprogressText = LanguageConversion.GetStringAssociatedWithTag("ExportDataExportNotInProgressText", exportNotInprogressText, htmlFontType, htmlStandardFontSize, "");
                errorDuringExportWarningText = LanguageConversion.GetStringAssociatedWithTag("ExportDataErrorDuringExportWarningText", errorDuringExportWarningText, htmlFontType, htmlStandardFontSize, "");

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ExportData.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }       

    }

    public class ExportDataThreadInputObject
    {
        public string sourceDatabaseConnectionString;
        public string destinationDatabaseConnectionString;
        public Dictionary<Guid, int> monitorsBeingExported;

    }


    public class ExportDataThreadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public List<String> errorsList;
    }


}
