﻿namespace DatabaseFileInteraction
{
    partial class CreateNewDatabase : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.destinationDirectoryRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.newDatabaseNameRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.selectDirectoryRadButton = new Telerik.WinControls.UI.RadButton();
            this.createDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.destinationDirectoryRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.destinationDatabaseFileNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.showAllDbFilesInDirectoryRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDirectoryRadGroupBox)).BeginInit();
            this.destinationDirectoryRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newDatabaseNameRadGroupBox)).BeginInit();
            this.newDatabaseNameRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectDirectoryRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.createDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDirectoryRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseFileNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showAllDbFilesInDirectoryRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // destinationDirectoryRadGroupBox
            // 
            this.destinationDirectoryRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.destinationDirectoryRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationDirectoryRadGroupBox.Controls.Add(this.destinationDirectoryRadTextBox);
            this.destinationDirectoryRadGroupBox.Controls.Add(this.selectDirectoryRadButton);
            this.destinationDirectoryRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.destinationDirectoryRadGroupBox.FooterImageIndex = -1;
            this.destinationDirectoryRadGroupBox.FooterImageKey = "";
            this.destinationDirectoryRadGroupBox.HeaderImageIndex = -1;
            this.destinationDirectoryRadGroupBox.HeaderImageKey = "";
            this.destinationDirectoryRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.destinationDirectoryRadGroupBox.HeaderText = "Create new database in directory";
            this.destinationDirectoryRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.destinationDirectoryRadGroupBox.Name = "destinationDirectoryRadGroupBox";
            this.destinationDirectoryRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.destinationDirectoryRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.destinationDirectoryRadGroupBox.Size = new System.Drawing.Size(534, 68);
            this.destinationDirectoryRadGroupBox.TabIndex = 0;
            this.destinationDirectoryRadGroupBox.Text = "Create new database in directory";
            this.destinationDirectoryRadGroupBox.ThemeName = "Office2007Black";
            // 
            // newDatabaseNameRadGroupBox
            // 
            this.newDatabaseNameRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.newDatabaseNameRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newDatabaseNameRadGroupBox.Controls.Add(this.showAllDbFilesInDirectoryRadButton);
            this.newDatabaseNameRadGroupBox.Controls.Add(this.destinationDatabaseFileNameRadTextBox);
            this.newDatabaseNameRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newDatabaseNameRadGroupBox.FooterImageIndex = -1;
            this.newDatabaseNameRadGroupBox.FooterImageKey = "";
            this.newDatabaseNameRadGroupBox.HeaderImageIndex = -1;
            this.newDatabaseNameRadGroupBox.HeaderImageKey = "";
            this.newDatabaseNameRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.newDatabaseNameRadGroupBox.HeaderText = "New database name";
            this.newDatabaseNameRadGroupBox.Location = new System.Drawing.Point(12, 86);
            this.newDatabaseNameRadGroupBox.Name = "newDatabaseNameRadGroupBox";
            this.newDatabaseNameRadGroupBox.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            // 
            // 
            // 
            this.newDatabaseNameRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.newDatabaseNameRadGroupBox.Size = new System.Drawing.Size(534, 68);
            this.newDatabaseNameRadGroupBox.TabIndex = 1;
            this.newDatabaseNameRadGroupBox.Text = "New database name";
            this.newDatabaseNameRadGroupBox.ThemeName = "Office2007Black";
            // 
            // selectDirectoryRadButton
            // 
            this.selectDirectoryRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectDirectoryRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectDirectoryRadButton.Location = new System.Drawing.Point(412, 24);
            this.selectDirectoryRadButton.Name = "selectDirectoryRadButton";
            this.selectDirectoryRadButton.Size = new System.Drawing.Size(117, 29);
            this.selectDirectoryRadButton.TabIndex = 54;
            this.selectDirectoryRadButton.Text = "<html>Select Directory</html>";
            this.selectDirectoryRadButton.ThemeName = "Office2007Black";
            this.selectDirectoryRadButton.Click += new System.EventHandler(this.selectDirectoryRadButton_Click);
            // 
            // createDatabaseRadButton
            // 
            this.createDatabaseRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.createDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createDatabaseRadButton.Location = new System.Drawing.Point(301, 170);
            this.createDatabaseRadButton.Name = "createDatabaseRadButton";
            this.createDatabaseRadButton.Size = new System.Drawing.Size(117, 38);
            this.createDatabaseRadButton.TabIndex = 56;
            this.createDatabaseRadButton.Text = "<html>Create Database</html>";
            this.createDatabaseRadButton.ThemeName = "Office2007Black";
            this.createDatabaseRadButton.Click += new System.EventHandler(this.createDatabaseRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(429, 170);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(117, 38);
            this.cancelRadButton.TabIndex = 55;
            this.cancelRadButton.Text = "<html>Cancel</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // destinationDirectoryRadTextBox
            // 
            this.destinationDirectoryRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationDirectoryRadTextBox.Location = new System.Drawing.Point(5, 29);
            this.destinationDirectoryRadTextBox.Name = "destinationDirectoryRadTextBox";
            this.destinationDirectoryRadTextBox.Size = new System.Drawing.Size(401, 20);
            this.destinationDirectoryRadTextBox.TabIndex = 55;
            this.destinationDirectoryRadTextBox.TabStop = false;
            this.destinationDirectoryRadTextBox.Text = "radTextBox1";
            // 
            // destinationDatabaseFileNameRadTextBox
            // 
            this.destinationDatabaseFileNameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationDatabaseFileNameRadTextBox.Location = new System.Drawing.Point(5, 29);
            this.destinationDatabaseFileNameRadTextBox.Name = "destinationDatabaseFileNameRadTextBox";
            this.destinationDatabaseFileNameRadTextBox.Size = new System.Drawing.Size(401, 20);
            this.destinationDatabaseFileNameRadTextBox.TabIndex = 0;
            this.destinationDatabaseFileNameRadTextBox.TabStop = false;
            this.destinationDatabaseFileNameRadTextBox.Text = "radTextBox1";
            // 
            // showAllDbFilesInDirectoryRadButton
            // 
            this.showAllDbFilesInDirectoryRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showAllDbFilesInDirectoryRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showAllDbFilesInDirectoryRadButton.Location = new System.Drawing.Point(412, 18);
            this.showAllDbFilesInDirectoryRadButton.Name = "showAllDbFilesInDirectoryRadButton";
            this.showAllDbFilesInDirectoryRadButton.Size = new System.Drawing.Size(117, 40);
            this.showAllDbFilesInDirectoryRadButton.TabIndex = 55;
            this.showAllDbFilesInDirectoryRadButton.Text = "<html>Show All Databases<br>in Selected Directory</html>";
            this.showAllDbFilesInDirectoryRadButton.ThemeName = "Office2007Black";
            this.showAllDbFilesInDirectoryRadButton.Click += new System.EventHandler(this.showAllDbFilesInDirectoryRadButton_Click);
            // 
            // CreateNewDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(558, 218);
            this.ControlBox = false;
            this.Controls.Add(this.createDatabaseRadButton);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.newDatabaseNameRadGroupBox);
            this.Controls.Add(this.destinationDirectoryRadGroupBox);
            this.Name = "CreateNewDatabase";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Create New Database";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.CreateNewDatabase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.destinationDirectoryRadGroupBox)).EndInit();
            this.destinationDirectoryRadGroupBox.ResumeLayout(false);
            this.destinationDirectoryRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.newDatabaseNameRadGroupBox)).EndInit();
            this.newDatabaseNameRadGroupBox.ResumeLayout(false);
            this.newDatabaseNameRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectDirectoryRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.createDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDirectoryRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.destinationDatabaseFileNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showAllDbFilesInDirectoryRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox destinationDirectoryRadGroupBox;
        private Telerik.WinControls.UI.RadGroupBox newDatabaseNameRadGroupBox;
        private Telerik.WinControls.UI.RadButton selectDirectoryRadButton;
        private Telerik.WinControls.UI.RadTextBox destinationDirectoryRadTextBox;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton createDatabaseRadButton;
        private Telerik.WinControls.UI.RadTextBox destinationDatabaseFileNameRadTextBox;
        private Telerik.WinControls.UI.RadButton showAllDbFilesInDirectoryRadButton;
    }
}