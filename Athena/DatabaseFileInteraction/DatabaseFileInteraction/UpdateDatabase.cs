using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

using Telerik.WinControls;

using GeneralUtilities;
using DatabaseInterface;

namespace DatabaseFileInteraction
{
    public partial class UpdateDatabase : Telerik.WinControls.UI.RadForm
    {
        private string initialDatabasePath;
        private string initialDatabaseName;
        private string initialLogFileName;
        private string applicationDirectory;

        private string backupDatabasePath;
        private string backupDatabaseName;
        private string backupLogFileName;

        private bool cancelUpdate = false;

        private bool databaseWasChanged;
        public bool DatabaseWasChanged
        {
            get
            {
                return databaseWasChanged;
            }
        }

        private bool finishedDatabaseUpdateProcedure;
        private bool errorOccurredDuringLastUpdate;

        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";
        private static string htmlLargerFontSize = "<size=12>";

        private static string fileDoesNotExistText = "File does not exist";
        private static string failedToConnectToDestinationDatabaseText = "Failed to connect to the destination database, operation termination.";
        private static string failedToConnectToSourceDatabaseText = "Failed to connect to the source database, operation termination.";
        private static string couldNotCopyDefaultEmptyDatabaseText = "Could not create a copy of the default empty database, operation terminated.";
        private static string couldNotCopyExistingDatabaseAsBackupText = "Could not create a copy of the existing database, operation terminated.";
        private static string cannotConnectToSourceDatabaseText = "Cannot connect to the source database.";
        private static string sourceDatabaseNotProperlyDefinedText = "Source database was not properly defined.";
        private static string cancelUpdateWarningText = "Currently the automated recovery system isn't working.\nUse the 'Recover Old Database' button to restore the database you just cancelled the update on.\nIf it fails, wait a few seconds and try again.";
      //  private static string cancelUpdateWarningVersion2Text = "Are you sure you want to cancel the update?\nThe new database will be deleted, and the old database will be returned";
      //  private static string cancelUpdateQuestionText = "Cancel Update?";
        private static string failedToRecoverDatabaseToInitialLocationText = "Failed to recover the database back to its initial location.";
        private static string originalDatabaseRecoveredText = "Original database recovered.";
        private static string errorDuringUpdateWarningText = "There were errors during the database update operation.\nClick on the 'View Update Error Log' button to see them.";
       // private static string failedToFindCoreStructureInNewDatabaseText = "Failed to find the required Companies, Plants, Equipment, and Monitors in the new database.  Export of data failed.";
       // private static string failedToFindCoreStructureInSourceDatabaseText = "Failed to find the required Companies, Plants, Equipment, and Monitors in the source database.  Details follow:";
        private static string noErrorDuringLastUpdate = "No errors occurred during the last database upgrade.";
        private static string fileNameToRecoverIsWrongType = "You must choose a file name with a .mdf extension.  Recover cancelled.";
        private static string failedToRestoreTheDatabaseText = "Failed to restore the database";
        private static string databaseRestoredText = "Database restored";
        private static string youNeedToSelectAnMdfFileText = "You need to select an mdf file to backup";
        private static string tryRecoverOperationAgainWarningText = "Operation failed.  Wait a few seconds and try again.\nSometimes the program is slow to release db objects so they can be moved or deleted.";
        private static string emptyInitialDatabaseIsMissingText = "Empty initial database has been moved or deleted.  Cannot proceed.";
       // private static string failedToCopyTheDataToTheNewDatabaseText = "Failed to copy the data to the new database.  Data dates follow.";
       // private static string failedToCopyTheMonitorConfigurationsToTheNewDatabaseText = "Failed to copy the monitor configurations to the new database.";
       // private static string failedToCopyTheDataViewerPreferencesToTheNewDatabaseText = "Failed to copy the data viewer preferences to the new database.";
        private static string noFileFoundText = "No file found";
       // private static string ofText = "of";
     //   private static string noneText = "None";
       // private static string configurationsText = "Configurations";
     //   private static string dataText = "Data";

        private static string notepadFileName = "notepad.exe";

        private static string updateSucceededText = "Update succeeded";
        private static string updateCancelledAttemptingToRestoreOldDatabaseText = "Update cancelled - attempting to restore the old database";
      
        // interface strings 
        private static string updateDatabaseInterfaceTitleText = "Upgrade Database";

        private static string upgradeDatabaseTitleRadLabelText = "Upgrade your database to one that is compatable with the latest version of the program.";
        private static string someInformationMayBeLostRadLabelText = "Note that only the core setup and data can be transferred reliably, some or all preferences data may be lost";
        private static string sourceDatabaseWillBeReplacedRadLabelText = "The source database will be replaced by a new database with the same name";
        private static string sourceDatabaseRadGroupBoxText = "Source Database";
        private static string selectSourceDatabaseRadButtonText = "Select Database";
        private static string saveCurrentDatabaseRadCheckBoxText = "Save a backup copy of the source database";
        private static string updateDatabaseRadButtonText = "Update Database";
        private static string viewUpdateErrorLogRadButtonText = "View Update Error Log";
       // private static string cancelRadButtonText = "Cancel Update";
        private static string recoverDatabaseRadButtonText = "Recover Old Database";
        //private static string copyingInformationFromRadLabelText = "Copying information from";
        //private static string monitorNumberRadLabelText = "Monitor No.:";
        //private static string monitorNumberValueRadLabelText = "0 of 0";
        //private static string companyNameRadLabelText = "Company:";
        //private static string companyNameValueRadLabelText = noneText;
        //private static string plantNameRadLabelText = "Plant:";
        //private static string plantNameValueRadLabelText = noneText;
        //private static string equipmentNameRadLabelText = "Equipment:";
        //private static string equipmentNameValueRadLabelText = noneText;
        //private static string monitorNameRadLabelText = "Monitor:";
        //private static string monitorNameValueRadLabelText = noneText;
        //private static string typeRadLabelText = "Type:";
        //private static string dataTypeValueRadLabelText = noneText;
        //private static string itemNumberRadLabelText = "Item No.:";
        //private static string itemNumberValueRadLabelText = "0 of 0";

        private string dbConnectionString;
        private string executablePath;
        private string applicationDataPath;
        private string backupDatabaseFolderName;
        private string emptyDatabaseFolderName;
        private string defaultDatabaseName;
        private string defaultDatabaseLogfileName;
        private string databaseUpdateErrorFileName;

        //private static string generalErrorsOccurredDuringExportText = "There were errors during the data export operation.\nClick on the 'Show Errors' button to see them.";
        //private static string requiredStructureWasNotCreatedInNewDatabaseText = "Failed to find the required Companies, Plants, Equipment, and Monitors in the new database.";
        //private static string failedToCopyEmptyDBToNewLocationText = "Failed to copy the default empty database to the new location.  Export of data failed.";
        //private static string cannotFindDefaultEmptyDatabaseText = "Cannot find default empty datbase to copy.\nIt has been moved or deleted.\nYou may have to re-install Athena.";
        //private static string noMonitorsSelectedText = "No monitors were selected so no data can be exported.";
        //private static string cannotConnectToCurrentDatabaseText = "Could not connect to the current database.\nYou will need to open a connection in the main interface first.";
        //private static string noErrorsDuringLastExportText = "No errors occurred during the last data export.";


        public UpdateDatabase(string inputDbConnectionString, string inputExecutablePath, string inputApplicationDataPath, string inputBackupDatabaseFolderName, string inputEmptyDatabaseFolderName, string inputDefaultDatabaseName, string inputDefaultDatabaseLogfileName, string inputdatabaseUpdateErrorFileName)
        {
            InitializeComponent();

            dbConnectionString = inputDbConnectionString;
            executablePath = inputExecutablePath;
            applicationDataPath = inputApplicationDataPath;
            backupDatabaseFolderName = inputBackupDatabaseFolderName;
            emptyDatabaseFolderName = inputEmptyDatabaseFolderName;
            defaultDatabaseName = inputDefaultDatabaseName;
            defaultDatabaseLogfileName = inputDefaultDatabaseLogfileName;
            databaseUpdateErrorFileName = inputdatabaseUpdateErrorFileName;

            AssignStringValuesToInterfaceObjects();

            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void UpdateDatabase_Load(object sender, EventArgs e)
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(this.dbConnectionString);
            string initialFileNameWithPath = connectionStringBuilder.AttachDBFilename;
            this.initialDatabaseName = Path.GetFileName(initialFileNameWithPath);
            this.initialLogFileName = Path.GetFileNameWithoutExtension(this.initialDatabaseName) + "_log.ldf";
            this.initialDatabasePath = Path.GetDirectoryName(initialFileNameWithPath);

            if ((this.initialDatabaseName != null) && (this.initialDatabaseName.Length > 0))
            {
                this.databaseBeingUpdatedRadTextBox.Text = this.initialDatabaseName;
            }
            else
            {
                this.databaseBeingUpdatedRadTextBox.Text = noFileFoundText;
            }
#if DEBUG
            this.applicationDirectory = this.executablePath;
#else
            this.applicationDirectory = this.applicationDataPath;
#endif
            this.finishedDatabaseUpdateProcedure = false;
        }

        private void selectNewDatabaseRadButton_Click(object sender, EventArgs e)
        {
            string pathBeingUsed;
            string fileNameWithFullPath;
            if ((this.initialDatabasePath != null) && Directory.Exists(this.initialDatabasePath))
            {
                pathBeingUsed = this.initialDatabasePath;
            }
            else
            {
                pathBeingUsed = this.applicationDirectory;
            }

            fileNameWithFullPath = FileUtilities.GetFileNameWithFullPath(pathBeingUsed, "mdf");

            if (File.Exists(fileNameWithFullPath))
            {
                this.initialDatabasePath = Path.GetDirectoryName(fileNameWithFullPath);
                this.initialDatabaseName = Path.GetFileName(fileNameWithFullPath);
                this.initialLogFileName = Path.GetFileNameWithoutExtension(this.initialDatabaseName) + "_log.ldf";

                this.databaseBeingUpdatedRadTextBox.Text = this.initialDatabaseName;
            }
            else
            {
                RadMessageBox.Show(this, fileDoesNotExistText);
            }
        }

        private void updateDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(this.dbConnectionString);
                Dictionary<Guid, int> allMonitors=null;
                string sourceDirectoryPath;
                //string sourceDBName;
                //string sourceLogFileName;
                string sourceDbConnectionString;
                string destinationDbConnectionString;
                // bool moveWasSuccessful = true;
                this.cancelUpdate = false;
                this.finishedDatabaseUpdateProcedure = false;
                this.errorOccurredDuringLastUpdate = false;

                allMonitors = GetAllMonitors();
                if (allMonitors.Count > 0)
                {
                    if ((this.initialDatabasePath != null) && Directory.Exists(this.initialDatabasePath) && (this.initialDatabaseName != null) && File.Exists(Path.Combine(this.initialDatabasePath, this.initialDatabaseName)))
                    {
                        if (EmptySourceDatabaseIsAvailable())
                        {
                            /// determine if it's possible to connect to the source database
                            /// move that database to its new location, tack on the date to let everyone know when it was backed up
                            /// create a new instance of the latest datbase, give it the same name as the old one
                            /// copy the data from the old db to the new one
                            /// perhaps delete the old db (though probably not yet, until testing is further along)
                            /// ? is there a way to determine the db version externally?
                            sourceDirectoryPath = Path.Combine(this.applicationDirectory, this.backupDatabaseFolderName);
                            FileUtilities.MakeFullPathToDirectory(sourceDirectoryPath);
                            connectionStringBuilder.AttachDBFilename = Path.Combine(this.initialDatabasePath, this.initialDatabaseName);
                            if (TestDatabaseConnectionString(connectionStringBuilder.ConnectionString))
                            {
                                // unhook the current database so we can move it to another directory and rename it
                                SqlConnection.ClearAllPools();
                                Thread.Sleep(500);
                                string temp = DateTime.Now.ToString();
                                temp = temp.Replace('/', '-');
                                temp = temp.Replace(':', '-');
                                this.backupDatabaseName = Path.GetFileNameWithoutExtension(this.initialDatabaseName) + "_Backup_" + temp + ".mdf";
                                this.backupLogFileName = Path.GetFileNameWithoutExtension(this.initialDatabaseName) + "_Backup_" + temp + "_log.ldf";
                                this.backupDatabasePath = Path.Combine(this.applicationDirectory, this.backupDatabaseFolderName);

                                if (MoveDatabaseToAnotherDirectory(this.initialDatabaseName, this.initialLogFileName, this.initialDatabasePath,
                                                                   this.backupDatabaseName, this.backupLogFileName, this.backupDatabasePath))
                                {
                                    // create a new instance of the latest database, give it he same name as the old one.
                                    if (CreateNewDatabaseWithInitialDatabaseName())
                                    {
                                        databaseWasChanged = true;
                                        // now we have to create the connection strings to the databases and then copy the data
                                        // this is where the database containing data is
                                        connectionStringBuilder.AttachDBFilename = Path.Combine(this.backupDatabasePath, this.backupDatabaseName);
                                        sourceDbConnectionString = connectionStringBuilder.ConnectionString;
                                        // this is the new database, but it has the same name as the old database
                                        connectionStringBuilder.AttachDBFilename = Path.Combine(this.initialDatabasePath, this.initialDatabaseName);
                                        destinationDbConnectionString = connectionStringBuilder.ConnectionString;

                                        if (TestDatabaseConnectionString(sourceDbConnectionString))
                                        {
                                            if (TestDatabaseConnectionString(destinationDbConnectionString))
                                            {
                                                // allMonitors = GetAllMonitors();
                                                using (CopyDatabaseData copyData = new CopyDatabaseData(allMonitors, sourceDbConnectionString, destinationDbConnectionString, CopyOperationType.Upgrade))
                                                {
                                                    copyData.ShowDialog();
                                                    copyData.Hide();
                                                    if (copyData.CopyWasCancelled)
                                                    {
                                                        RadMessageBox.Show(this, updateCancelledAttemptingToRestoreOldDatabaseText);
                                                        RecoverDatabase(this.initialDatabaseName, this.initialLogFileName, this.initialDatabasePath,
                                                                        this.backupDatabaseName, this.backupLogFileName, this.backupDatabasePath);
                                                    }
                                                    else if ((copyData.CopyErrorsList != null) && (copyData.CopyErrorsList.Count > 0))
                                                    {
                                                        FileUtilities.SaveListOfStringAsFile(this.applicationDataPath, this.databaseUpdateErrorFileName, copyData.CopyErrorsList, false);
                                                        this.errorOccurredDuringLastUpdate = true;
                                                        RadMessageBox.Show(this, errorDuringUpdateWarningText);
                                                    }
                                                    else
                                                    {
                                                        RadMessageBox.Show(this, updateSucceededText);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                RadMessageBox.Show(this, failedToConnectToDestinationDatabaseText);
                                            }
                                        }
                                        else
                                        {
                                            RadMessageBox.Show(this, failedToConnectToSourceDatabaseText);
                                        }
                                    }
                                    else
                                    {
                                        RadMessageBox.Show(this, couldNotCopyDefaultEmptyDatabaseText);
                                    }
                                }
                                else
                                {
                                    RadMessageBox.Show(this, couldNotCopyExistingDatabaseAsBackupText);
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, cannotConnectToSourceDatabaseText);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in UpdateDatabase.updateDatabaseRadButton_Click(object, EventArgs)\nThe latest database was not found in its expected location.\nIt may have been moved or deleted.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, sourceDatabaseNotProperlyDefinedText);
                    }
                    if (this.cancelUpdate)
                    {
                        RadMessageBox.Show(this, cancelUpdateWarningText);
                        // RecoverDatabaseFromCancelledUpdate();
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.updateDatabaseRadButton_Click(object, EventArgs)\nMessage:" + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                this.finishedDatabaseUpdateProcedure = true;
            }
        }

        private bool EmptySourceDatabaseIsAvailable()
        {
            bool available = true;
            try
            {
                available = File.Exists(Path.Combine(this.executablePath, this.emptyDatabaseFolderName, this.defaultDatabaseName));
                if (available)
                {
                    available = File.Exists(Path.Combine(this.executablePath, this.emptyDatabaseFolderName, this.defaultDatabaseLogfileName));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.EmptySourceDatabaseIsAvailable()\nMessage:" + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return available;
        }

        private void RecoverDatabase(string originalDatabaseName, string originalLogFileName, string originalPath,
                                     string backupDatabaseName, string backupLogFileName, string backupPath)
        {
            try
            {
                SqlConnection.ClearAllPools();
                Thread.Sleep(500);
                RemovePartialDatabaseCopy(originalDatabaseName, originalLogFileName, originalPath);
                if (!MoveDatabaseToAnotherDirectory(backupDatabaseName, backupLogFileName, backupDatabasePath,
                                                    originalDatabaseName, originalLogFileName, originalPath))
                {
                    RadMessageBox.Show(this, failedToRecoverDatabaseToInitialLocationText);
                }
                else
                {
                    RadMessageBox.Show(this, originalDatabaseRecoveredText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.EmptySourceDatabaseIsAvailable()\nMessage:" + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool RemovePartialDatabaseCopy(string databaseName, string databaseLogFileName, string databasePath)
        {
            bool success = true;

            try
            {
                File.Delete(Path.Combine(databasePath, databaseName));
                File.Delete(Path.Combine(databasePath, databaseLogFileName));
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.RemovePartialDatabaseCopy()\nMessage:" + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        private bool MoveDatabaseToAnotherDirectory(string startingDatabaseName, string startingLogFileName, string startingPath,
                                                    string destinationDatabaseName, string destinationLogFileName, string destinationPath)
        {
            bool success = true;
            try
            {
                string backupDirectoryPath = Path.Combine(this.applicationDirectory, this.backupDatabaseFolderName);

                string startingDatabaseNameWithPath = Path.Combine(startingPath, startingDatabaseName);
                string startingLogFileNameWithPath = Path.Combine(startingPath, startingLogFileName);

                string destinationDatabaseNameWithPath = Path.Combine(destinationPath, destinationDatabaseName);
                string destinationLogFileNameWithPath = Path.Combine(destinationPath, destinationLogFileName);

                //string sourceDbWithFullPath = Path.Combine(this.initialDatabasePath, this.initialDatabaseName);
                //string sourceLogFileWithFullPath = Path.Combine(this.initialDatabasePath, this.initialLogFileName);
                //string backupDbWithFullPath = Path.Combine(backupDirectoryPath, backupDbName);
                //string backupLogFileWithFullPath = Path.Combine(backupDirectoryPath, backupLogFileName);

                FileUtilities.MakeFullPathToDirectory(destinationPath);
              
                if ((!File.Exists(destinationDatabaseNameWithPath)) && (!File.Exists(destinationLogFileNameWithPath)))
                {
                    try
                    {
                        File.Move(startingDatabaseNameWithPath, destinationDatabaseNameWithPath);
                        // File.Copy(startingDatabaseNameWithPath, destinationDatabaseNameWithPath);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = "Exception thrown in UpdateDatabase.MoveDatabaseToAnotherDirectory(string, string, string, string, string, string)\nFailed to copy the directory to the backup directory.\nMessage:" + ex.Message;
                        LogMessage.LogError(errorMessage);
                        success = false;
                        // File.Delete(destinationDatabaseNameWithPath);
                    }

                    if (success)
                    {
                        try
                        {
                            // File.Copy(startingLogFileNameWithPath, destinationLogFileNameWithPath);
                            File.Move(startingLogFileNameWithPath, destinationLogFileNameWithPath);
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = "Exception thrown in UpdateDatabase.EmptySourceDatabaseIsAvailable()\nFailed to copy the db log file to the backup directory.\nMessage:" + ex.Message;
                            LogMessage.LogError(errorMessage);
                            success = false;
                            //File.Delete(destinationDatabaseNameWithPath);
                            //File.Delete(destinationLogFileNameWithPath);
                        }
                        if (success)
                        {
                            //if (FilesAreSimilarEnough(startingDatabaseNameWithPath, destinationDatabaseNameWithPath) &&
                            //    FilesAreSimilarEnough(startingLogFileNameWithPath, destinationLogFileNameWithPath))
                            //{
                            //    File.Delete(startingDatabaseNameWithPath);
                            //    File.Delete(startingLogFileNameWithPath);
                            //}
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in UpdateDatabase.MoveDatabaseToAnotherDirectory(string, string, string, string, string, string)\nCannot move the database because at least one file with the same name already exists";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.MoveDatabaseToAnotherDirectory(string, string, string, string, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool CreateNewDatabaseWithInitialDatabaseName()
        {
            bool success = false;
            try
            {
                bool copiedDatabase = false;
                string sourceDatabaseName;
                string sourceLogfileName;
                string destinationDatabaseName;
                string destinationLogFileName;

                sourceDatabaseName = Path.Combine(this.executablePath, this.emptyDatabaseFolderName, this.defaultDatabaseName);
                sourceLogfileName = Path.Combine(this.executablePath, this.emptyDatabaseFolderName, this.defaultDatabaseLogfileName);

                destinationDatabaseName = Path.Combine(this.initialDatabasePath, this.initialDatabaseName);
                destinationLogFileName = Path.Combine(this.initialDatabasePath, Path.GetFileNameWithoutExtension(this.initialDatabaseName) + "_log.ldf");

                if ((File.Exists(sourceDatabaseName)) && (File.Exists(sourceLogfileName)))
                {
                    if ((!File.Exists(destinationDatabaseName)) && (!File.Exists(destinationLogFileName)))
                    {
                        try
                        {
                            File.Copy(sourceDatabaseName, destinationDatabaseName);
                            copiedDatabase = true;
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = "Exception thrown in UpdateDatabase.CreateNewDatabaseWithInitialDatabaseName()\nFailed to create a copy of the database\nMessage: " + ex.Message;
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        if (copiedDatabase)
                        {
                            try
                            {
                                File.Copy(sourceLogfileName, destinationLogFileName);
                                success = true;
                            }
                            catch (Exception ex)
                            {
                                string errorMessage = "Exception thrown in UpdateDatabase.CreateNewDatabaseWithInitialDatabaseName()\nFailed to create a copy of the log file\nMessage: " + ex.Message;
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in UpdateDatabase.CreateNewDatabaseWithInitialDatabaseName()\nFailed to move the old database before trying to copy the new one.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in UpdateDatabase.CreateNewDatabaseWithInitialDatabaseName()\nEmpty initial database has been moved or deleted.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#else
                    RadMessageBox.Show(this, emptyInitialDatabaseIsMissingText);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.newDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        private bool FilesAreSimilarEnough(string fileOne, string fileTwo)
        {
            bool similarEnough = true;
            try
            {
                FileInfo fileOneInfo = new FileInfo(fileOne);
                FileInfo fileTwoInfo = new FileInfo(fileTwo);

                if (!(fileOneInfo.Length == fileTwoInfo.Length))
                {
                    similarEnough = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.FilesAreSimilarEnough()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return similarEnough;
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.cancelRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private Dictionary<Guid, int> GetAllMonitors()
        {
            Dictionary<Guid, int> allMonitors = new Dictionary<Guid, int>();
            try
            {
                List<Monitor> monitorsList = null;
                using(MonitorInterfaceDB localDB = new MonitorInterfaceDB(this.dbConnectionString))
                {
                    monitorsList = General_DatabaseMethods.GetAllMonitors(localDB);
                }
                if ((monitorsList != null) && (monitorsList.Count > 0))
                {
                    foreach (Monitor monitor in monitorsList)
                    {
                        if (!allMonitors.ContainsKey(monitor.ID))
                        {
                            allMonitors.Add(monitor.ID, 1);
                        }
                        else
                        {
                            /// This should, of course, never happen.  Ever.  Like, it's impossible, right?
                            allMonitors[monitor.ID] += 1;
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.GetAllMonitors()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allMonitors;
        }
    
        public bool TestDatabaseConnectionString(string connectionString)
        {
            bool databaseConnectionStringIsCorrect = false;
            try
            {
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(connectionString))
                {
                    databaseConnectionStringIsCorrect = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.TestDatabaseConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return databaseConnectionStringIsCorrect;
        }

        private void viewExportErrorsRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.errorOccurredDuringLastUpdate)
                {
                    Process p = new Process();
                    p.StartInfo.FileName = notepadFileName;
                    p.StartInfo.Arguments = Path.Combine(this.applicationDataPath, this.databaseUpdateErrorFileName);
                    p.Start();
                }
                else
                {
                    RadMessageBox.Show(this, noErrorDuringLastUpdate);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.viewExportErrorsRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void recoverDatabaseRadButton_Click(object sender, EventArgs e)
        /// HERE WE WILL OPEN A LIST OF ALL DATABASES IN THE BACKUP DIRECTORY, USING THE STANDARD
        /// WINDOZE INTERFACE.  WE SHOULD COMPARE THE SIZE OF THE BACKUP AGAINST THE ONE IN THE 
        /// APPLICATION DIRECTORY AND WARN THE USER IF THE APPLICATION ONE IS LARGER THAN THE BACKUP, WHICH
        /// WOULD INDICATE THAT WE MIGHT NOT WANT TO RECOVER IT.
        /// COULD ALSO HAVE AN OPTION TO JUST MOVE THE BACKUP DB TO THE APPLICATION DIRECTORY.
        /// 
        /// COULD ALSO HAVE SOME KIND OF COMPARITOR METHOD TO INSURE THAT WHEN A DB IS UPDATED THAT IT CONTAINS ALL
        /// THE DATA THE OLD DB HAD.  THIS MIGHT BE A WASTE OF TIME, ASK CLAUDE.
        /// 
        /// I RECALL THAT CLAUDE SAID NOT TO BOTHER VERIFYING ALL DATA
        /// 
        {
            try
            {
                string backupDirectory;
                string backupFileNameWithPath;
                string backupFileName;
                string backupLogFileName;
                string backupFileNameWithoutExtension;
                string originalFileName;
                string originalLogFileName;
                string originalFileNameWithoutExtension;
                string originalFileNameWithPath;
                string originalDirectory;
                //string chosenFileName;
                //string chosenPath;
                string extension;
                int indexValue;
                bool moveFile = false;

                if ((this.backupDatabasePath == null) || (this.backupDatabasePath == string.Empty))
                {
                    this.backupDatabasePath = Path.Combine(this.applicationDirectory, this.backupDatabaseFolderName);
                }
                
                SqlConnection.ClearAllPools();
                Thread.Sleep(500);

                // have the user select which database to recover
                backupFileNameWithPath = FileUtilities.GetFileNameWithFullPath(this.backupDatabasePath, ".mdf");
                if ((backupFileNameWithPath != null) && (backupFileNameWithPath.Length > 0))
                {
                    /// extract all the information about the database being recovered, as selected by the user
                    backupDirectory = Path.GetDirectoryName(backupFileNameWithPath);
                    backupFileName = Path.GetFileName(backupFileNameWithPath);
                    backupFileNameWithoutExtension = Path.GetFileNameWithoutExtension(backupFileNameWithPath);
                    backupLogFileName = backupFileNameWithoutExtension + "_log.ldf";

                    extension = FileUtilities.GetFileExtension(backupFileNameWithPath);
                    if (extension.ToLower().CompareTo("mdf") == 0)
                    {
                        /// get the original name of the database and log file
                        indexValue = backupFileName.IndexOf("_Backup");
                        originalFileNameWithoutExtension = backupFileName.Substring(0, indexValue);
                        originalFileName = originalFileNameWithoutExtension + ".mdf";
                        originalLogFileName = originalFileNameWithoutExtension + "_log.ldf";
                        originalDirectory = this.applicationDirectory;

                        /// see if the original file name is already being used
                        if ((File.Exists(Path.Combine(this.applicationDirectory, originalFileName))) ||
                            (File.Exists(Path.Combine(this.applicationDirectory, originalLogFileName))))
                        {
                            // if it is being used, allow the user to pick a new name and/or location for the recovered database
                            originalFileNameWithPath = FileUtilities.GetNewFileNameToCopy(originalFileName, this.applicationDirectory);
                            if (originalFileNameWithPath.CompareTo("") != 0)
                            {
                                if (FileUtilities.GetFileExtension(originalFileNameWithPath).ToLower().CompareTo("mdf") == 0)
                                {
                                    moveFile = true;
                                    // all the overwrite warning takes place in the previous function call, so go with it.
                                    originalDirectory = Path.GetDirectoryName(originalFileNameWithPath);
                                    originalFileNameWithoutExtension = Path.GetFileNameWithoutExtension(originalFileNameWithPath);
                                    originalFileName = originalFileNameWithoutExtension + ".mdf";
                                    originalLogFileName = originalFileNameWithoutExtension + "_log.ldf";
                                    File.Delete(Path.Combine(originalDirectory, originalFileName));
                                    File.Delete(Path.Combine(originalDirectory, originalLogFileName));
                                }
                                else
                                {
                                    RadMessageBox.Show(this, fileNameToRecoverIsWrongType);
                                }
                            }
                        }
                        else
                        {
                            moveFile = true;
                        }

                        if (moveFile)
                        {
                            if (!MoveDatabaseToAnotherDirectory(backupFileName, backupLogFileName, backupDirectory, originalFileName, originalLogFileName, originalDirectory))
                            {
                                RadMessageBox.Show(this, failedToRestoreTheDatabaseText);
                            }
                            else
                            {
                                RadMessageBox.Show(this, databaseRestoredText);
                            }
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, youNeedToSelectAnMdfFileText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.openDatabaseRadMenuItem_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#else
                RadMessageBox.Show(this, tryRecoverOperationAgainWarningText);
#endif
            }
        }
     
        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = updateDatabaseInterfaceTitleText;

                upgradeDatabaseTitleRadLabel.Text = upgradeDatabaseTitleRadLabelText;
                someInformationMayBeLostRadLabel.Text = someInformationMayBeLostRadLabelText;
                sourceDatabaseWillBeReplacedRadLabel.Text = sourceDatabaseWillBeReplacedRadLabelText;
                sourceDatabaseRadGroupBox.Text = sourceDatabaseRadGroupBoxText;
                selectSourceDatabaseRadButton.Text = selectSourceDatabaseRadButtonText;
                saveCurrentDatabaseRadCheckBox.Text = saveCurrentDatabaseRadCheckBoxText;
                updateDatabaseRadButton.Text = updateDatabaseRadButtonText;
                viewUpdateErrorLogRadButton.Text = viewUpdateErrorLogRadButtonText;
                // cancelRadButton.Text = cancelRadButtonText;
                recoverDatabaseRadButton.Text = recoverDatabaseRadButtonText;
                //copyingInformationFromRadLabel.Text = copyingInformationFromRadLabelText;
                //monitorNumberRadLabel.Text = monitorNumberRadLabelText;
                //monitorNumberValueRadLabel.Text = monitorNumberValueRadLabelText;
                //companyNameRadLabel.Text = companyNameRadLabelText;
                //companyNameValueRadLabel.Text = companyNameValueRadLabelText;
                //plantNameRadLabel.Text = plantNameRadLabelText;
                //plantNameValueRadLabel.Text = plantNameValueRadLabelText;
                //equipmentNameRadLabel.Text = equipmentNameRadLabelText;
                //equipmentNameValueRadLabel.Text = equipmentNameValueRadLabelText;
                //monitorNameRadLabel.Text = monitorNameRadLabelText;
                //monitorNameValueRadLabel.Text = monitorNameValueRadLabelText;
                //typeRadLabel.Text = typeRadLabelText;
                //dataTypeValueRadLabel.Text = dataTypeValueRadLabelText;
                //itemNumberRadLabel.Text = itemNumberRadLabelText;
                //itemNumberValueRadLabel.Text = itemNumberValueRadLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");
                htmlLargerFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLLargerFontSize", htmlLargerFontSize, "", "", "");

                fileDoesNotExistText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFileDoesNotExistText", fileDoesNotExistText, htmlFontType, htmlStandardFontSize, "");
                failedToConnectToDestinationDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToConnectToDestinationDatabaseText", failedToConnectToDestinationDatabaseText, htmlFontType, htmlStandardFontSize, "");
                failedToConnectToSourceDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToConnectToSourceDatabaseText", failedToConnectToSourceDatabaseText, htmlFontType, htmlStandardFontSize, "");
                couldNotCopyDefaultEmptyDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCouldNotCopyDefaultEmptyDatabaseText", couldNotCopyDefaultEmptyDatabaseText, htmlFontType, htmlStandardFontSize, "");
                couldNotCopyExistingDatabaseAsBackupText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCouldNotCopyExistingDatabaseAsBackupText", couldNotCopyExistingDatabaseAsBackupText, htmlFontType, htmlStandardFontSize, "");
                cannotConnectToSourceDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCannotConnectToSourceDatabaseText", cannotConnectToSourceDatabaseText, htmlFontType, htmlStandardFontSize, "");
                sourceDatabaseNotProperlyDefinedText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseSourceDatabaseNotProperlyDefinedText", sourceDatabaseNotProperlyDefinedText, htmlFontType, htmlStandardFontSize, "");
                cancelUpdateWarningText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCancelUpdateWarningText", cancelUpdateWarningText, htmlFontType, htmlStandardFontSize, "");
                //cancelUpdateWarningVersion2Text = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCancelUpdateWarningVersion2Text", cancelUpdateWarningVersion2Text, htmlFontType, htmlStandardFontSize, "");
                //cancelUpdateQuestionText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseCancelUpdateQuestionText", cancelUpdateQuestionText, htmlFontType, htmlStandardFontSize, "");
                failedToRecoverDatabaseToInitialLocationText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToRecoverDatabaseToInitialLocationText", failedToRecoverDatabaseToInitialLocationText, htmlFontType, htmlStandardFontSize, "");
                originalDatabaseRecoveredText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseOriginalDatabaseRecoveredText", originalDatabaseRecoveredText, htmlFontType, htmlStandardFontSize, "");
                errorDuringUpdateWarningText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseErrorDuringUpdateWarningText", errorDuringUpdateWarningText, htmlFontType, htmlStandardFontSize, "");
                //failedToFindCoreStructureInNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToFindCoreStructureInNewDatabaseText", failedToFindCoreStructureInNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //failedToFindCoreStructureInSourceDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToFindCoreStructureInSourceDatabaseText", failedToFindCoreStructureInSourceDatabaseText, htmlFontType, htmlStandardFontSize, "");
                noErrorDuringLastUpdate = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseNoErrorDuringLastUpdateText", noErrorDuringLastUpdate, htmlFontType, htmlStandardFontSize, "");
                fileNameToRecoverIsWrongType = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFileNameToRecoverIsWrongTypeText", fileNameToRecoverIsWrongType, htmlFontType, htmlStandardFontSize, "");
                failedToRestoreTheDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToRestoreTheDatabaseText", failedToRestoreTheDatabaseText, htmlFontType, htmlStandardFontSize, "");
                databaseRestoredText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseDatabaseRestoredText", databaseRestoredText, htmlFontType, htmlStandardFontSize, "");
                youNeedToSelectAnMdfFileText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseYouNeedToSelectAnMdfFileText", youNeedToSelectAnMdfFileText, htmlFontType, htmlStandardFontSize, "");
                tryRecoverOperationAgainWarningText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseTryRecoverOperationAgainWarningText", tryRecoverOperationAgainWarningText, htmlFontType, htmlStandardFontSize, "");
                emptyInitialDatabaseIsMissingText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseEmptyInitialDatabaseIsMissingText", emptyInitialDatabaseIsMissingText, htmlFontType, htmlStandardFontSize, "");

                //failedToCopyTheDataToTheNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToCopyTheDataToTheNewDatabase", failedToCopyTheDataToTheNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //failedToCopyTheMonitorConfigurationsToTheNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToCopyTheMonitorConfigurationsToTheNewDatabaseText", failedToCopyTheMonitorConfigurationsToTheNewDatabaseText, htmlFontType, htmlStandardFontSize, "");
                //failedToCopyTheDataViewerPreferencesToTheNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseFailedToCopyTheDataViewerPreferencesToTheNewDatabaseText", failedToCopyTheDataViewerPreferencesToTheNewDatabaseText, htmlFontType, htmlStandardFontSize, "");

                updateSucceededText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseUpdateSucceededText", updateSucceededText, htmlFontType, htmlStandardFontSize, "");
                updateCancelledAttemptingToRestoreOldDatabaseText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseUpdateCancelledAttemptingToRestoreOldDatabaseText", updateCancelledAttemptingToRestoreOldDatabaseText, htmlFontType, htmlStandardFontSize, "");

                noFileFoundText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseNoFileFoundText", noFileFoundText, htmlFontType, htmlStandardFontSize, "");
                //ofText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseOfText", ofText, htmlFontType, htmlStandardFontSize, "");
                //noneText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseNoneText", noneText, htmlFontType, htmlStandardFontSize, "");
                //configurationsText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseConfigurationsText", configurationsText, htmlFontType, htmlStandardFontSize, "");
                //dataText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseDataText", dataText, htmlFontType, htmlStandardFontSize, "");
                // interface strings 
                updateDatabaseInterfaceTitleText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceTitleText", updateDatabaseInterfaceTitleText, "", "", "");
                upgradeDatabaseTitleRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceUpdateDatabaseTitleRadLabelText", upgradeDatabaseTitleRadLabelText, htmlFontType, htmlLargerFontSize, "");
                someInformationMayBeLostRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceSomeInformationMayBeLostRadLabel", someInformationMayBeLostRadLabelText, htmlFontType, htmlStandardFontSize, "");
                sourceDatabaseWillBeReplacedRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceSourceDatabaseWillBeReplacedRadLabel", sourceDatabaseWillBeReplacedRadLabelText, htmlFontType, htmlStandardFontSize, "");
                sourceDatabaseRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceSourceDatabaseRadGroupBoxText", sourceDatabaseRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                selectSourceDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceSelectSourceDatabaseRadButton", selectSourceDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                saveCurrentDatabaseRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceSaveCurrentDatabaseRadCheckBoxText", saveCurrentDatabaseRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
                updateDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceUpdateDatabaseRadButtonText", updateDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                viewUpdateErrorLogRadButtonText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceViewUpdateErrorLogRadButtonText", viewUpdateErrorLogRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceCancelRadButton", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");
                recoverDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceRecoverDatabaseRadButton", recoverDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                //copyingInformationFromRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceCopyingInformationFromRadLabelText", copyingInformationFromRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //monitorNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceMonitorNumberRadLabelText", monitorNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //companyNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceCompanyNameRadLabelText", companyNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //plantNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfacePlantNameRadLabelText", plantNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //equipmentNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceEquipmentNameRadLabelText", equipmentNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //monitorNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceMonitorNameRadLabelText", monitorNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //typeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceTypeRadLabelText", typeRadLabelText, htmlFontType, htmlStandardFontSize, "");
                //itemNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseInterfaceItemNumberRadLabelText", itemNumberRadLabelText, htmlFontType, htmlStandardFontSize, "");

                notepadFileName = LanguageConversion.GetStringAssociatedWithTag("UpdateDatabaseNotepadExeText", notepadFileName, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in UpdateDatabase.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

    }
    public class UpdateDatabaseThreadInputObject
    {
        public string sourceDatabaseConnectionString;
        public string destinationDatabaseConnectionString;
        public Dictionary<Guid, int> monitorsBeingExported;

    }


    public class UpdateDatabaseThreadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public List<String> errorsList;
    }
}