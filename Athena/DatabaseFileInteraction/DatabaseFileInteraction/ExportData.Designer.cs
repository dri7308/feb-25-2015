namespace DatabaseFileInteraction
{
    partial class ExportData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportData));
            this.monitorRadTreeView = new Telerik.WinControls.UI.RadTreeView();
            this.radBreadCrumb1 = new Telerik.WinControls.UI.RadBreadCrumb();
            this.collapseTreeRadButton = new Telerik.WinControls.UI.RadButton();
            this.expandTreeRadButton = new Telerik.WinControls.UI.RadButton();
            this.exportDataRadButton = new Telerik.WinControls.UI.RadButton();
            this.viewExportErrorsRadButton = new Telerik.WinControls.UI.RadButton();
            this.selectAllRadButton = new Telerik.WinControls.UI.RadButton();
            this.unselectAllRadButton = new Telerik.WinControls.UI.RadButton();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            ((System.ComponentModel.ISupportInitialize)(this.monitorRadTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.collapseTreeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandTreeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportDataRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewExportErrorsRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectAllRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unselectAllRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // monitorRadTreeView
            // 
            this.monitorRadTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.monitorRadTreeView.Location = new System.Drawing.Point(3, 65);
            this.monitorRadTreeView.Name = "monitorRadTreeView";
            this.monitorRadTreeView.Size = new System.Drawing.Size(208, 444);
            this.monitorRadTreeView.SpacingBetweenNodes = -1;
            this.monitorRadTreeView.TabIndex = 0;
            this.monitorRadTreeView.Text = "radTreeView1";
            this.monitorRadTreeView.ThemeName = "Office2007Black";
            // 
            // radBreadCrumb1
            // 
            this.radBreadCrumb1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radBreadCrumb1.DefaultTreeView = this.monitorRadTreeView;
            this.radBreadCrumb1.Location = new System.Drawing.Point(3, 515);
            this.radBreadCrumb1.Name = "radBreadCrumb1";
            this.radBreadCrumb1.Size = new System.Drawing.Size(401, 23);
            this.radBreadCrumb1.TabIndex = 1;
            this.radBreadCrumb1.Text = "radBreadCrumb1";
            this.radBreadCrumb1.ThemeName = "Office2007Black";
            // 
            // collapseTreeRadButton
            // 
            this.collapseTreeRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.collapseTreeRadButton.Location = new System.Drawing.Point(111, 9);
            this.collapseTreeRadButton.Name = "collapseTreeRadButton";
            this.collapseTreeRadButton.Size = new System.Drawing.Size(100, 24);
            this.collapseTreeRadButton.TabIndex = 3;
            this.collapseTreeRadButton.Text = "Collapse Tree";
            this.collapseTreeRadButton.ThemeName = "Office2007Black";
            this.collapseTreeRadButton.Click += new System.EventHandler(this.collapseTreeRadButton_Click);
            // 
            // expandTreeRadButton
            // 
            this.expandTreeRadButton.Location = new System.Drawing.Point(3, 9);
            this.expandTreeRadButton.Name = "expandTreeRadButton";
            this.expandTreeRadButton.Size = new System.Drawing.Size(100, 24);
            this.expandTreeRadButton.TabIndex = 2;
            this.expandTreeRadButton.Text = "Expand Tree";
            this.expandTreeRadButton.ThemeName = "Office2007Black";
            this.expandTreeRadButton.Click += new System.EventHandler(this.expandTreeRadButton_Click);
            // 
            // exportDataRadButton
            // 
            this.exportDataRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.exportDataRadButton.Location = new System.Drawing.Point(217, 65);
            this.exportDataRadButton.Name = "exportDataRadButton";
            this.exportDataRadButton.Size = new System.Drawing.Size(187, 56);
            this.exportDataRadButton.TabIndex = 4;
            this.exportDataRadButton.Text = "<html>Export Data<br>for<br>Selected Monitors As</html>";
            this.exportDataRadButton.ThemeName = "Office2007Black";
            this.exportDataRadButton.Click += new System.EventHandler(this.exportDataRadButton_Click);
            // 
            // viewExportErrorsRadButton
            // 
            this.viewExportErrorsRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.viewExportErrorsRadButton.Location = new System.Drawing.Point(218, 127);
            this.viewExportErrorsRadButton.Name = "viewExportErrorsRadButton";
            this.viewExportErrorsRadButton.Size = new System.Drawing.Size(187, 56);
            this.viewExportErrorsRadButton.TabIndex = 88;
            this.viewExportErrorsRadButton.Text = "<html>View Export Errors</html>";
            this.viewExportErrorsRadButton.ThemeName = "Office2007Black";
            this.viewExportErrorsRadButton.Click += new System.EventHandler(this.viewExportErrorsRadButton_Click);
            // 
            // selectAllRadButton
            // 
            this.selectAllRadButton.Location = new System.Drawing.Point(3, 35);
            this.selectAllRadButton.Name = "selectAllRadButton";
            this.selectAllRadButton.Size = new System.Drawing.Size(100, 24);
            this.selectAllRadButton.TabIndex = 89;
            this.selectAllRadButton.Text = "Select All";
            this.selectAllRadButton.ThemeName = "Office2007Black";
            this.selectAllRadButton.Click += new System.EventHandler(this.selectAllRadButton_Click);
            // 
            // unselectAllRadButton
            // 
            this.unselectAllRadButton.Location = new System.Drawing.Point(111, 35);
            this.unselectAllRadButton.Name = "unselectAllRadButton";
            this.unselectAllRadButton.Size = new System.Drawing.Size(100, 24);
            this.unselectAllRadButton.TabIndex = 3;
            this.unselectAllRadButton.Text = "Unselect All";
            this.unselectAllRadButton.ThemeName = "Office2007Black";
            this.unselectAllRadButton.Click += new System.EventHandler(this.unselectAllRadButton_Click);
            // 
            // ExportData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(407, 549);
            this.Controls.Add(this.unselectAllRadButton);
            this.Controls.Add(this.selectAllRadButton);
            this.Controls.Add(this.viewExportErrorsRadButton);
            this.Controls.Add(this.exportDataRadButton);
            this.Controls.Add(this.collapseTreeRadButton);
            this.Controls.Add(this.expandTreeRadButton);
            this.Controls.Add(this.radBreadCrumb1);
            this.Controls.Add(this.monitorRadTreeView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(415, 550);
            this.Name = "ExportData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Export Data";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ExportData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.monitorRadTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.collapseTreeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expandTreeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportDataRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewExportErrorsRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectAllRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unselectAllRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTreeView monitorRadTreeView;
        private Telerik.WinControls.UI.RadBreadCrumb radBreadCrumb1;
        private Telerik.WinControls.UI.RadButton collapseTreeRadButton;
        private Telerik.WinControls.UI.RadButton expandTreeRadButton;
        private Telerik.WinControls.UI.RadButton exportDataRadButton;
        private Telerik.WinControls.UI.RadButton viewExportErrorsRadButton;
        private Telerik.WinControls.UI.RadButton selectAllRadButton;
        private Telerik.WinControls.UI.RadButton unselectAllRadButton;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
    }
}

