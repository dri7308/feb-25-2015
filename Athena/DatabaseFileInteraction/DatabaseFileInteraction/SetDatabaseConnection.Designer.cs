namespace DatabaseFileInteraction
{
    partial class SetDatabaseConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetDatabaseConnection));
            this.databaseNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.databaseLocationRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.databaseLocationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.databaseNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serverInstanceRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.serverInstanceRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serverNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serverNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.availableServersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableServersRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.getServersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.allServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.networkServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.localServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.getServersRadButton = new Telerik.WinControls.UI.RadButton();
            this.selectDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveAsDefaultConnectionRadButton = new Telerik.WinControls.UI.RadButton();
            this.authenticationRadDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.authenticationRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.usernameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.passwordRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.userNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.passwordRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.testConnectionRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.getDbNames = new Telerik.WinControls.UI.RadDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseLocationRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseLocationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadGroupBox)).BeginInit();
            this.availableServersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadGroupBox)).BeginInit();
            this.getServersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsDefaultConnectionRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authenticationRadDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.authenticationRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usernameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testConnectionRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDbNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // databaseNameRadTextBox
            // 
            this.databaseNameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseNameRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseNameRadTextBox.Location = new System.Drawing.Point(134, 318);
            this.databaseNameRadTextBox.Name = "databaseNameRadTextBox";
            this.databaseNameRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.databaseNameRadTextBox.TabIndex = 6;
            this.databaseNameRadTextBox.TabStop = false;
            this.databaseNameRadTextBox.Text = "Not Specified";
            // 
            // databaseLocationRadTextBox
            // 
            this.databaseLocationRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.databaseLocationRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseLocationRadTextBox.Location = new System.Drawing.Point(134, 342);
            this.databaseLocationRadTextBox.Name = "databaseLocationRadTextBox";
            this.databaseLocationRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.databaseLocationRadTextBox.TabIndex = 7;
            this.databaseLocationRadTextBox.TabStop = false;
            this.databaseLocationRadTextBox.Text = "Not Specified";
            // 
            // databaseLocationRadLabel
            // 
            this.databaseLocationRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseLocationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.databaseLocationRadLabel.Location = new System.Drawing.Point(25, 344);
            this.databaseLocationRadLabel.Name = "databaseLocationRadLabel";
            this.databaseLocationRadLabel.Size = new System.Drawing.Size(101, 16);
            this.databaseLocationRadLabel.TabIndex = 42;
            this.databaseLocationRadLabel.Text = "Database Location";
            this.databaseLocationRadLabel.ThemeName = "Office2007Black";
            // 
            // databaseNameRadLabel
            // 
            this.databaseNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.databaseNameRadLabel.Location = new System.Drawing.Point(25, 320);
            this.databaseNameRadLabel.Name = "databaseNameRadLabel";
            this.databaseNameRadLabel.Size = new System.Drawing.Size(88, 16);
            this.databaseNameRadLabel.TabIndex = 41;
            this.databaseNameRadLabel.Text = "Database Name";
            this.databaseNameRadLabel.ThemeName = "Office2007Black";
            // 
            // serverInstanceRadTextBox
            // 
            this.serverInstanceRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverInstanceRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverInstanceRadTextBox.Location = new System.Drawing.Point(134, 217);
            this.serverInstanceRadTextBox.Name = "serverInstanceRadTextBox";
            this.serverInstanceRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.serverInstanceRadTextBox.TabIndex = 2;
            this.serverInstanceRadTextBox.TabStop = false;
            this.serverInstanceRadTextBox.Text = "Not Specified";
            // 
            // serverInstanceRadLabel
            // 
            this.serverInstanceRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverInstanceRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serverInstanceRadLabel.Location = new System.Drawing.Point(25, 219);
            this.serverInstanceRadLabel.Name = "serverInstanceRadLabel";
            this.serverInstanceRadLabel.Size = new System.Drawing.Size(85, 16);
            this.serverInstanceRadLabel.TabIndex = 39;
            this.serverInstanceRadLabel.Text = "Server Instance";
            this.serverInstanceRadLabel.ThemeName = "Office2007Black";
            // 
            // serverNameRadLabel
            // 
            this.serverNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serverNameRadLabel.Location = new System.Drawing.Point(25, 195);
            this.serverNameRadLabel.Name = "serverNameRadLabel";
            this.serverNameRadLabel.Size = new System.Drawing.Size(73, 16);
            this.serverNameRadLabel.TabIndex = 38;
            this.serverNameRadLabel.Text = "Server Name";
            this.serverNameRadLabel.ThemeName = "Office2007Black";
            // 
            // serverNameRadTextBox
            // 
            this.serverNameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverNameRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverNameRadTextBox.Location = new System.Drawing.Point(134, 193);
            this.serverNameRadTextBox.Name = "serverNameRadTextBox";
            this.serverNameRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.serverNameRadTextBox.TabIndex = 1;
            this.serverNameRadTextBox.TabStop = false;
            this.serverNameRadTextBox.Text = "Not Specified";
            // 
            // availableServersRadGroupBox
            // 
            this.availableServersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableServersRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableServersRadGroupBox.Controls.Add(this.availableServersRadListControl);
            this.availableServersRadGroupBox.Controls.Add(this.radButton2);
            this.availableServersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableServersRadGroupBox.FooterImageIndex = -1;
            this.availableServersRadGroupBox.FooterImageKey = "";
            this.availableServersRadGroupBox.HeaderImageIndex = -1;
            this.availableServersRadGroupBox.HeaderImageKey = "";
            this.availableServersRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.availableServersRadGroupBox.HeaderText = "<html>Available SQL Servers</html>";
            this.availableServersRadGroupBox.Location = new System.Drawing.Point(186, 12);
            this.availableServersRadGroupBox.Name = "availableServersRadGroupBox";
            this.availableServersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.availableServersRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableServersRadGroupBox.Size = new System.Drawing.Size(399, 165);
            this.availableServersRadGroupBox.TabIndex = 45;
            this.availableServersRadGroupBox.Text = "<html>Available SQL Servers</html>";
            this.availableServersRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableServersRadListControl
            // 
            this.availableServersRadListControl.AutoScroll = true;
            this.availableServersRadListControl.CaseSensitiveSort = true;
            this.availableServersRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableServersRadListControl.ItemHeight = 18;
            this.availableServersRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableServersRadListControl.Name = "availableServersRadListControl";
            this.availableServersRadListControl.Size = new System.Drawing.Size(379, 135);
            this.availableServersRadListControl.TabIndex = 9;
            this.availableServersRadListControl.Text = "radListControl2";
            this.availableServersRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableServersRadListControl_SelectedIndexChanged);
            this.availableServersRadListControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.availableServersRadListControl_MouseClick);
            // 
            // radButton2
            // 
            this.radButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton2.Location = new System.Drawing.Point(0, 232);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(130, 70);
            this.radButton2.TabIndex = 8;
            this.radButton2.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton2.ThemeName = "Office2007Black";
            // 
            // getServersRadGroupBox
            // 
            this.getServersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.getServersRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.getServersRadGroupBox.Controls.Add(this.allServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.networkServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.localServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.radButton1);
            this.getServersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getServersRadGroupBox.FooterImageIndex = -1;
            this.getServersRadGroupBox.FooterImageKey = "";
            this.getServersRadGroupBox.HeaderImageIndex = -1;
            this.getServersRadGroupBox.HeaderImageKey = "";
            this.getServersRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.getServersRadGroupBox.HeaderText = "<html>File Type</html>";
            this.getServersRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.getServersRadGroupBox.Name = "getServersRadGroupBox";
            this.getServersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.getServersRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.getServersRadGroupBox.Size = new System.Drawing.Size(168, 93);
            this.getServersRadGroupBox.TabIndex = 46;
            this.getServersRadGroupBox.Text = "<html>File Type</html>";
            this.getServersRadGroupBox.ThemeName = "Office2007Black";
            this.getServersRadGroupBox.Click += new System.EventHandler(this.getServersRadGroupBox_Click);
            // 
            // allServersRadRadioButton
            // 
            this.allServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allServersRadRadioButton.Location = new System.Drawing.Point(13, 68);
            this.allServersRadRadioButton.Name = "allServersRadRadioButton";
            this.allServersRadRadioButton.Size = new System.Drawing.Size(75, 16);
            this.allServersRadRadioButton.TabIndex = 11;
            this.allServersRadRadioButton.Text = "All Servers";
            this.allServersRadRadioButton.Visible = false;
            // 
            // networkServersRadRadioButton
            // 
            this.networkServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.networkServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.networkServersRadRadioButton.Location = new System.Drawing.Point(13, 44);
            this.networkServersRadRadioButton.Name = "networkServersRadRadioButton";
            this.networkServersRadRadioButton.Size = new System.Drawing.Size(89, 16);
            this.networkServersRadRadioButton.TabIndex = 10;
            this.networkServersRadRadioButton.Text = "Server Based";
            this.networkServersRadRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.networkServersRadRadioButton_ToggleStateChanged);
            // 
            // localServersRadRadioButton
            // 
            this.localServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.localServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.localServersRadRadioButton.Location = new System.Drawing.Point(13, 20);
            this.localServersRadRadioButton.Name = "localServersRadRadioButton";
            this.localServersRadRadioButton.Size = new System.Drawing.Size(74, 16);
            this.localServersRadRadioButton.TabIndex = 9;
            this.localServersRadRadioButton.TabStop = true;
            this.localServersRadRadioButton.Text = "File Based";
            this.localServersRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // getServersRadButton
            // 
            this.getServersRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getServersRadButton.Location = new System.Drawing.Point(12, 111);
            this.getServersRadButton.Name = "getServersRadButton";
            this.getServersRadButton.Size = new System.Drawing.Size(142, 38);
            this.getServersRadButton.TabIndex = 32;
            this.getServersRadButton.Text = "<html>Get Servers</html>";
            this.getServersRadButton.ThemeName = "Office2007Black";
            this.getServersRadButton.Click += new System.EventHandler(this.getServersRadButton_Click);
            // 
            // selectDatabaseRadButton
            // 
            this.selectDatabaseRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectDatabaseRadButton.Location = new System.Drawing.Point(25, 404);
            this.selectDatabaseRadButton.Name = "selectDatabaseRadButton";
            this.selectDatabaseRadButton.Size = new System.Drawing.Size(245, 24);
            this.selectDatabaseRadButton.TabIndex = 8;
            this.selectDatabaseRadButton.Text = "Select Database";
            this.selectDatabaseRadButton.ThemeName = "Office2007Black";
            this.selectDatabaseRadButton.Click += new System.EventHandler(this.selectDatabaseRadButton_Click);
            // 
            // saveAsDefaultConnectionRadButton
            // 
            this.saveAsDefaultConnectionRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveAsDefaultConnectionRadButton.Location = new System.Drawing.Point(341, 434);
            this.saveAsDefaultConnectionRadButton.Name = "saveAsDefaultConnectionRadButton";
            this.saveAsDefaultConnectionRadButton.Size = new System.Drawing.Size(245, 24);
            this.saveAsDefaultConnectionRadButton.TabIndex = 47;
            this.saveAsDefaultConnectionRadButton.Text = "Save as Default Connection";
            this.saveAsDefaultConnectionRadButton.ThemeName = "Office2007Black";
            this.saveAsDefaultConnectionRadButton.Click += new System.EventHandler(this.saveAsDefaultConnectionRadButton_Click);
            // 
            // authenticationRadDropDownList
            // 
            this.authenticationRadDropDownList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.authenticationRadDropDownList.DropDownAnimationEnabled = true;
            this.authenticationRadDropDownList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            radListDataItem1.Text = "9600";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "38400";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "57600";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "115200";
            radListDataItem4.TextWrap = true;
            this.authenticationRadDropDownList.Items.Add(radListDataItem1);
            this.authenticationRadDropDownList.Items.Add(radListDataItem2);
            this.authenticationRadDropDownList.Items.Add(radListDataItem3);
            this.authenticationRadDropDownList.Items.Add(radListDataItem4);
            this.authenticationRadDropDownList.Location = new System.Drawing.Point(134, 241);
            this.authenticationRadDropDownList.Name = "authenticationRadDropDownList";
            this.authenticationRadDropDownList.ShowImageInEditorArea = true;
            this.authenticationRadDropDownList.Size = new System.Drawing.Size(441, 18);
            this.authenticationRadDropDownList.TabIndex = 3;
            this.authenticationRadDropDownList.ThemeName = "Office2007Black";
            this.authenticationRadDropDownList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.authenticationRadDropDownList_SelectedIndexChanged);
            // 
            // authenticationRadLabel
            // 
            this.authenticationRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authenticationRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.authenticationRadLabel.Location = new System.Drawing.Point(25, 247);
            this.authenticationRadLabel.Name = "authenticationRadLabel";
            this.authenticationRadLabel.Size = new System.Drawing.Size(78, 16);
            this.authenticationRadLabel.TabIndex = 51;
            this.authenticationRadLabel.Text = "Authentication";
            this.authenticationRadLabel.ThemeName = "Office2007Black";
            // 
            // usernameRadTextBox
            // 
            this.usernameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameRadTextBox.Location = new System.Drawing.Point(134, 269);
            this.usernameRadTextBox.Name = "usernameRadTextBox";
            this.usernameRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.usernameRadTextBox.TabIndex = 4;
            this.usernameRadTextBox.TabStop = false;
            // 
            // passwordRadTextBox
            // 
            this.passwordRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordRadTextBox.Location = new System.Drawing.Point(134, 293);
            this.passwordRadTextBox.Name = "passwordRadTextBox";
            this.passwordRadTextBox.PasswordChar = '*';
            this.passwordRadTextBox.Size = new System.Drawing.Size(441, 18);
            this.passwordRadTextBox.TabIndex = 5;
            this.passwordRadTextBox.TabStop = false;
            // 
            // userNameRadLabel
            // 
            this.userNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.userNameRadLabel.Location = new System.Drawing.Point(25, 271);
            this.userNameRadLabel.Name = "userNameRadLabel";
            this.userNameRadLabel.Size = new System.Drawing.Size(63, 16);
            this.userNameRadLabel.TabIndex = 54;
            this.userNameRadLabel.Text = "User Name";
            this.userNameRadLabel.ThemeName = "Office2007Black";
            // 
            // passwordRadLabel
            // 
            this.passwordRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.passwordRadLabel.Location = new System.Drawing.Point(25, 295);
            this.passwordRadLabel.Name = "passwordRadLabel";
            this.passwordRadLabel.Size = new System.Drawing.Size(56, 16);
            this.passwordRadLabel.TabIndex = 55;
            this.passwordRadLabel.Text = "Password";
            this.passwordRadLabel.ThemeName = "Office2007Black";
            // 
            // testConnectionRadButton
            // 
            this.testConnectionRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testConnectionRadButton.Location = new System.Drawing.Point(341, 404);
            this.testConnectionRadButton.Name = "testConnectionRadButton";
            this.testConnectionRadButton.Size = new System.Drawing.Size(245, 24);
            this.testConnectionRadButton.TabIndex = 56;
            this.testConnectionRadButton.Text = "Test Connection";
            this.testConnectionRadButton.ThemeName = "Office2007Black";
            this.testConnectionRadButton.Click += new System.EventHandler(this.testConnectionRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(342, 464);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(245, 24);
            this.cancelRadButton.TabIndex = 57;
            this.cancelRadButton.Text = "Cancel Changes";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // getDbNames
            // 
            this.getDbNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.getDbNames.DropDownAnimationEnabled = true;
            this.getDbNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getDbNames.Location = new System.Drawing.Point(25, 440);
            this.getDbNames.Name = "getDbNames";
            this.getDbNames.ShowImageInEditorArea = true;
            this.getDbNames.Size = new System.Drawing.Size(245, 18);
            this.getDbNames.TabIndex = 58;
            this.getDbNames.ThemeName = "Office2007Black";
            this.getDbNames.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.getDbNames_SelectedIndexChanged);
            // 
            // SetDatabaseConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(598, 666);
            this.Controls.Add(this.getServersRadButton);
            this.Controls.Add(this.getDbNames);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.testConnectionRadButton);
            this.Controls.Add(this.passwordRadLabel);
            this.Controls.Add(this.userNameRadLabel);
            this.Controls.Add(this.usernameRadTextBox);
            this.Controls.Add(this.passwordRadTextBox);
            this.Controls.Add(this.authenticationRadLabel);
            this.Controls.Add(this.authenticationRadDropDownList);
            this.Controls.Add(this.selectDatabaseRadButton);
            this.Controls.Add(this.saveAsDefaultConnectionRadButton);
            this.Controls.Add(this.getServersRadGroupBox);
            this.Controls.Add(this.availableServersRadGroupBox);
            this.Controls.Add(this.databaseNameRadTextBox);
            this.Controls.Add(this.databaseLocationRadTextBox);
            this.Controls.Add(this.databaseLocationRadLabel);
            this.Controls.Add(this.databaseNameRadLabel);
            this.Controls.Add(this.serverInstanceRadTextBox);
            this.Controls.Add(this.serverInstanceRadLabel);
            this.Controls.Add(this.serverNameRadLabel);
            this.Controls.Add(this.serverNameRadTextBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(606, 555);
            this.Name = "SetDatabaseConnection";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Set Database Connection";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SetDatabaseConnection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseLocationRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseLocationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadGroupBox)).EndInit();
            this.availableServersRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadGroupBox)).EndInit();
            this.getServersRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveAsDefaultConnectionRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authenticationRadDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.authenticationRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usernameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.passwordRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testConnectionRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getDbNames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox databaseNameRadTextBox;
        private Telerik.WinControls.UI.RadTextBox databaseLocationRadTextBox;
        private Telerik.WinControls.UI.RadLabel databaseLocationRadLabel;
        private Telerik.WinControls.UI.RadLabel databaseNameRadLabel;
        private Telerik.WinControls.UI.RadTextBox serverInstanceRadTextBox;
        private Telerik.WinControls.UI.RadLabel serverInstanceRadLabel;
        private Telerik.WinControls.UI.RadLabel serverNameRadLabel;
        private Telerik.WinControls.UI.RadTextBox serverNameRadTextBox;
        private Telerik.WinControls.UI.RadGroupBox availableServersRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableServersRadListControl;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadGroupBox getServersRadGroupBox;
        private Telerik.WinControls.UI.RadRadioButton allServersRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton networkServersRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton localServersRadRadioButton;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadButton getServersRadButton;
        private Telerik.WinControls.UI.RadButton selectDatabaseRadButton;
        private Telerik.WinControls.UI.RadButton saveAsDefaultConnectionRadButton;
        private Telerik.WinControls.UI.RadLabel authenticationRadLabel;
        private Telerik.WinControls.UI.RadDropDownList authenticationRadDropDownList;
        private Telerik.WinControls.UI.RadTextBox usernameRadTextBox;
        private Telerik.WinControls.UI.RadTextBox passwordRadTextBox;
        private Telerik.WinControls.UI.RadLabel userNameRadLabel;
        private Telerik.WinControls.UI.RadLabel passwordRadLabel;
        private Telerik.WinControls.UI.RadButton testConnectionRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadDropDownList getDbNames;
    }
}

