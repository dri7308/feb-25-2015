﻿namespace DatabaseFileInteraction
{
    partial class SelectSaveDatabaseName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectSaveDatabaseName));
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.existingDatabasesRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.existingDatabasesRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.useSelectedNameRadButton = new Telerik.WinControls.UI.RadButton();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.selectedDatabaseNameRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.selectedDatabaseNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.existingDatabasesRadGroupBox)).BeginInit();
            this.existingDatabasesRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.existingDatabasesRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedNameRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatabaseNameRadGroupBox)).BeginInit();
            this.selectedDatabaseNameRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatabaseNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // existingDatabasesRadGroupBox
            // 
            this.existingDatabasesRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.existingDatabasesRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.existingDatabasesRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.existingDatabasesRadGroupBox.Controls.Add(this.existingDatabasesRadListControl);
            this.existingDatabasesRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.existingDatabasesRadGroupBox.FooterImageIndex = -1;
            this.existingDatabasesRadGroupBox.FooterImageKey = "";
            this.existingDatabasesRadGroupBox.HeaderImageIndex = -1;
            this.existingDatabasesRadGroupBox.HeaderImageKey = "";
            this.existingDatabasesRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.existingDatabasesRadGroupBox.HeaderText = "<html>Existing Databases</html>";
            this.existingDatabasesRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.existingDatabasesRadGroupBox.Name = "existingDatabasesRadGroupBox";
            this.existingDatabasesRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.existingDatabasesRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.existingDatabasesRadGroupBox.Size = new System.Drawing.Size(399, 145);
            this.existingDatabasesRadGroupBox.TabIndex = 52;
            this.existingDatabasesRadGroupBox.Text = "<html>Existing Databases</html>";
            this.existingDatabasesRadGroupBox.ThemeName = "Office2007Black";
            // 
            // existingDatabasesRadListControl
            // 
            this.existingDatabasesRadListControl.AutoScroll = true;
            this.existingDatabasesRadListControl.CaseSensitiveSort = true;
            this.existingDatabasesRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.existingDatabasesRadListControl.ItemHeight = 18;
            this.existingDatabasesRadListControl.Location = new System.Drawing.Point(10, 20);
            this.existingDatabasesRadListControl.Name = "existingDatabasesRadListControl";
            this.existingDatabasesRadListControl.Size = new System.Drawing.Size(379, 115);
            this.existingDatabasesRadListControl.TabIndex = 9;
            this.existingDatabasesRadListControl.Text = "radListControl2";
            this.existingDatabasesRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.existingDatabasesRadListControl_SelectedIndexChanged);
            // 
            // useSelectedNameRadButton
            // 
            this.useSelectedNameRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.useSelectedNameRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useSelectedNameRadButton.Location = new System.Drawing.Point(171, 224);
            this.useSelectedNameRadButton.Name = "useSelectedNameRadButton";
            this.useSelectedNameRadButton.Size = new System.Drawing.Size(117, 38);
            this.useSelectedNameRadButton.TabIndex = 56;
            this.useSelectedNameRadButton.Text = "<html>Use Selected Name</html>";
            this.useSelectedNameRadButton.ThemeName = "Office2007Black";
            this.useSelectedNameRadButton.Click += new System.EventHandler(this.useSelectedNameRadButton_Click);
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(294, 224);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(117, 38);
            this.cancelRadButton.TabIndex = 55;
            this.cancelRadButton.Text = "<html>Cancel</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // selectedDatabaseNameRadGroupBox
            // 
            this.selectedDatabaseNameRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.selectedDatabaseNameRadGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.selectedDatabaseNameRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.selectedDatabaseNameRadGroupBox.Controls.Add(this.selectedDatabaseNameRadTextBox);
            this.selectedDatabaseNameRadGroupBox.Controls.Add(this.radButton1);
            this.selectedDatabaseNameRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectedDatabaseNameRadGroupBox.FooterImageIndex = -1;
            this.selectedDatabaseNameRadGroupBox.FooterImageKey = "";
            this.selectedDatabaseNameRadGroupBox.HeaderImageIndex = -1;
            this.selectedDatabaseNameRadGroupBox.HeaderImageKey = "";
            this.selectedDatabaseNameRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.selectedDatabaseNameRadGroupBox.HeaderText = "<html>Selected Database Name</html>";
            this.selectedDatabaseNameRadGroupBox.Location = new System.Drawing.Point(12, 163);
            this.selectedDatabaseNameRadGroupBox.Name = "selectedDatabaseNameRadGroupBox";
            this.selectedDatabaseNameRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.selectedDatabaseNameRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.selectedDatabaseNameRadGroupBox.Size = new System.Drawing.Size(399, 55);
            this.selectedDatabaseNameRadGroupBox.TabIndex = 53;
            this.selectedDatabaseNameRadGroupBox.Text = "<html>Selected Database Name</html>";
            this.selectedDatabaseNameRadGroupBox.ThemeName = "Office2007Black";
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // selectedDatabaseNameRadTextBox
            // 
            this.selectedDatabaseNameRadTextBox.Location = new System.Drawing.Point(10, 23);
            this.selectedDatabaseNameRadTextBox.Name = "selectedDatabaseNameRadTextBox";
            this.selectedDatabaseNameRadTextBox.Size = new System.Drawing.Size(379, 20);
            this.selectedDatabaseNameRadTextBox.TabIndex = 9;
            this.selectedDatabaseNameRadTextBox.TabStop = false;
            this.selectedDatabaseNameRadTextBox.ThemeName = "Office2007Black";
            // 
            // SelectSaveDatabaseName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(423, 274);
            this.Controls.Add(this.selectedDatabaseNameRadGroupBox);
            this.Controls.Add(this.useSelectedNameRadButton);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.existingDatabasesRadGroupBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectSaveDatabaseName";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Select Save Database Name";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SelectSaveDatabaseName_Load);
            ((System.ComponentModel.ISupportInitialize)(this.existingDatabasesRadGroupBox)).EndInit();
            this.existingDatabasesRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.existingDatabasesRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.useSelectedNameRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatabaseNameRadGroupBox)).EndInit();
            this.selectedDatabaseNameRadGroupBox.ResumeLayout(false);
            this.selectedDatabaseNameRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectedDatabaseNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.UI.RadGroupBox existingDatabasesRadGroupBox;
        private Telerik.WinControls.UI.RadListControl existingDatabasesRadListControl;
        private Telerik.WinControls.UI.RadButton useSelectedNameRadButton;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadGroupBox selectedDatabaseNameRadGroupBox;
        private Telerik.WinControls.UI.RadTextBox selectedDatabaseNameRadTextBox;
        private Telerik.WinControls.UI.RadButton radButton1;
    }
}
