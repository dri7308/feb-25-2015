﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using DatabaseInterface;
using GeneralUtilities;

namespace DatabaseFileInteraction
{
    public partial class CopyDatabaseData : Telerik.WinControls.UI.RadForm
    {
        //private static string generalErrorsOccurredDuringExportText = "There were errors during the data export operation.\nClick on the 'Show Errors' button to see them.";
        //private static string generalErrorsOccurredDuringUpgradeText = "There were errors during the database update operation.\nClick on the 'Show Errors' button to see them.";
       
        //private static string failedToCopyTheDataToNewDatabaseText = "Failed to copy the data to the new database.  Data dates follow.";
        private static string failedToCopyTheMonitorConfigurationsText = "Failed to copy the monitor configurations to the new database.";
        private static string failedToCopyTheDataViewerPreferencesText = "Failed to copy the data viewer preferences to the new database.";

        private static string cancelUpdateText = "Cancel Update";
        private static string cancelExportText = "Cancel Export";

        private static string ofText = "of";
        private static string noneText = "None";
        private static string dataText = "Data";
        private static string configurationsText = "Configurations";
       // private static string templateConfigurationsText = "Template Configurations";
        private static string preferencesText = "Preferences";

        //private static string noMainConfigurationTemplatesToCopyText = "No Main Monitor configuration templates to copy";
        //private static string noBhmConfigurationTemplatesToCopyText = "No BHM configuration templates to copy";
        //private static string noPdmConfigurationTemplatesToCopyText = "No PDM configuration templates to copy";

        private static string copyingInformationFromRadLabelText = "Copying information from";
        private static string monitorNumberRadLabelText = "Monitor No.:";
        private static string companyNameRadLabelText = "Company:";
        private static string plantNameRadLabelText = "Plant:";
        private static string equipmentNameRadLabelText = "Equipment:";
        private static string monitorNameRadLabelText = "Monitor:";
        private static string dataTransferTypeRadLabelText = "Type:";

        private static string copyDatabaseDataTitleText = "Copy Database Data";

        private static string requiredStructureWasNotCreatedInNewDatabaseText = "Failed to find the required Companies, Plants, Equipment, and Monitors in the new database.";

        //private static string exportDataThreadProbablyTerminatedInErrorStateText = "Data export probably terminated in an error state.";
        //private static string updateDatabaseThreadProbablyTerminatedInErrorStateText = "Update database probably terminated in an error state.";

        private static string copyCancelledMessageText = "Copying will be cancelled after the data for the current monitor has been transferred";

        BackgroundWorker copyDataBackgroundWorker;

        bool errorsOccurredDuringCopy = false;

        Dictionary<Guid, int> monitorsToCopy;
        string sourceDatabaseConnectionString;
        string destinationDatabaseConnectionString;
        CopyOperationType copyOperationType;

        private List<String> copyErrorsList;
        public List<string> CopyErrorsList
        {
            get
            {
                return copyErrorsList;
            }
        }

        private bool copyWasCancelled;
        public bool CopyWasCancelled
        {
            get
            {
                return copyWasCancelled;
            }
        }

        public CopyDatabaseData(Dictionary<Guid, int> inputMonitorsToCopy, string inputSourceDatabaseConnectionString, string inputDestinationDatabaseConnectionString, CopyOperationType inputCopyOperationType)
        {
            InitializeComponent();

            this.monitorsToCopy = inputMonitorsToCopy;
            this.sourceDatabaseConnectionString = inputSourceDatabaseConnectionString;
            this.destinationDatabaseConnectionString = inputDestinationDatabaseConnectionString;
            this.copyOperationType = inputCopyOperationType;

            copyErrorsList = new List<string>();

            if (this.copyOperationType == CopyOperationType.Export)
            {
                cancelRadButton.Text = cancelExportText;
            }
            else if (this.copyOperationType == CopyOperationType.Upgrade)
            {
                cancelRadButton.Text = cancelUpdateText;
            }

            InitializeCopyDataBackgroundWorker();

            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void CopyDatabaseData_Load(object sender, EventArgs e)
        {
            AssignStringValuesToInterfaceObjects();
            this.copyDataRunningRadWaitingBar.StartWaiting();
            copyDataBackgroundWorker.RunWorkerAsync();
        }

        #region Thread Methods

        private void InitializeCopyDataBackgroundWorker()
        {
            copyDataBackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };

            copyDataBackgroundWorker.DoWork += copyDataBackgroundWorker_DoWork;
            // copyDataBackgroundWorker.ProgressChanged += copyDataBackgroundWorker_ProgressChanged;
            copyDataBackgroundWorker.RunWorkerCompleted += copyDataBackgroundWorker_RunWorkerCompleted;
        }

//        private void ExportDataThreadInvoker(Dictionary<Guid, int> monitorsToCopyByGuid, string sourceDatabaseConnectionString, string destinationDatabaseConnectionString)
//        {
//            try
//            {
//                //CopyDataThreadInputObject inputObject = new CopyDataThreadInputObject();
//                //inputObject.monitorsBeingExported = monitorsToCopyByGuid;
//                //inputObject.sourceDatabaseConnectionString = sourceDatabaseConnectionString;
//                //inputObject.destinationDatabaseConnectionString = destinationDatabaseConnectionString;

//                copyDataBackgroundWorker.RunWorkerAsync(inputObject);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in CopyDatabaseData.ExportDataThreadInvoker(Dictionary<Guid, int>, string, string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void copyDataBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                CopyDataThreadReturnObject returnObject = new CopyDataThreadReturnObject();

                List<string> generalTransferErrorsList = null;
                List<string> dataTransferErrorsList = new List<string>();
                bool foundErrorForCurrentMonitor = false;
                int numberOfMonitorsBeingCopied = 0;
                int counter = 1;

                List<Company> companyList = null;
                List<Plant> plantList = null;
                List<Equipment> equipmentList = null;
                List<Monitor> monitorList = null;

                ErrorCode errorCode = ErrorCode.None;
                string hierarchyString;
             
                List<string> emptyList = new List<string>();

                returnObject.errorCode = ErrorCode.None;

                this.copyErrorsList.Clear();

                using (MonitorInterfaceDB destinationDB = new MonitorInterfaceDB(this.destinationDatabaseConnectionString))
                using (MonitorInterfaceDB sourceDB = new MonitorInterfaceDB(this.sourceDatabaseConnectionString))
                {
                    if (this.copyOperationType == CopyOperationType.Export)
                    {
                        generalTransferErrorsList = General_DatabaseMethods.PartialDatabaseStructureClone(this.monitorsToCopy, sourceDB, destinationDB);
                    }
                    else if (this.copyOperationType == CopyOperationType.Upgrade)
                    {
                        generalTransferErrorsList = General_DatabaseMethods.FullDatabaseStructureCloneThatAllowsForIncompleteEntries(sourceDB, destinationDB);
                    }
                    if (generalTransferErrorsList != null)
                    {
                        if (generalTransferErrorsList.Count == 0)
                        {
                            /// Get the skeleton of the database representation from the destination database.  This works as a nice check to be sure that we actually
                            /// created it, as opposed to pulling the data from the source database.
                            if (General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref companyList, ref plantList, ref equipmentList, ref monitorList, destinationDB))
                            {
                                /// Now we can actually start copying the data, and it's about time.
                                numberOfMonitorsBeingCopied = monitorList.Count;
                                foreach (Monitor monitor in monitorList)
                                {
                                    this.Invoke(new MethodInvoker(() => this.monitorNumberValueRadLabel.Text = counter.ToString() + " " + ofText + " " + numberOfMonitorsBeingCopied.ToString()));
                                    this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = monitor.MonitorType.Trim()));

                                    hierarchyString = General_DatabaseMethods.GetHierarchyForOneMonitor(monitor, companyList, plantList, equipmentList);
                                    SetHierarchyDisplayNames(hierarchyString);
                                    foundErrorForCurrentMonitor = false;
                                    this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = dataText));
                                    errorCode = TransferDataForOneMonitor(monitor.ID, sourceDB, destinationDB);
                                    if (errorCode != ErrorCode.DatabaseWriteSucceeded)
                                    {
                                        foundErrorForCurrentMonitor = true;
                                        this.copyErrorsList.AddRange(CreateErrorHeader(monitor, companyList, plantList, equipmentList));
                                        dataTransferErrorsList.Clear();
                                        dataTransferErrorsList.Add(ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                                        this.copyErrorsList.AddRange(CreateErrorsList(0, dataTransferErrorsList));
                                    }
                                    this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = configurationsText));
                                    generalTransferErrorsList = TransferConfigurationsForOneMonitor(monitor.ID, sourceDB, destinationDB);
                                    if (generalTransferErrorsList.Count > 0)
                                    {
                                        if (!foundErrorForCurrentMonitor)
                                        {
                                            foundErrorForCurrentMonitor = true;
                                            this.copyErrorsList.AddRange(CreateErrorHeader(monitor, companyList, plantList, equipmentList));
                                        }
                                        this.copyErrorsList.AddRange(CreateErrorsList(1, generalTransferErrorsList));
                                    }
                                    this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = preferencesText));
                                    if (!General_DatabaseMethods.CopyPreferencesForOneMonitorToTheNewDatabase(monitor, sourceDB, destinationDB))
                                    {
                                        if (!foundErrorForCurrentMonitor)
                                        {
                                            foundErrorForCurrentMonitor = true;
                                            this.copyErrorsList.AddRange(CreateErrorHeader(monitor, companyList, plantList, equipmentList));
                                        }
                                        this.copyErrorsList.AddRange(CreateErrorsList(2, emptyList));
                                    }
                                    if (foundErrorForCurrentMonitor)
                                    {
                                        this.copyErrorsList.Add(string.Empty);
                                        this.copyErrorsList.Add(string.Empty);
                                    }
                                    counter++;
                                    this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = noneText));
                                    this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = noneText));

                                    if (copyDataBackgroundWorker.CancellationPending)
                                    {
                                        if (this.copyOperationType == CopyOperationType.Export)
                                        {
                                            returnObject.errorCode = ErrorCode.DataExportCancelled;
                                        }
                                        else if(this.copyOperationType == CopyOperationType.Upgrade)
                                        {
                                            returnObject.errorCode = ErrorCode.DatabaseUpgradeCancelled;
                                        }
                                        copyWasCancelled = true;
                                        break;
                                    }
                                }

                                this.Invoke(new MethodInvoker(() => this.monitorNumberValueRadLabel.Text = "0 " + ofText + " 0"));
                                /// We don't want to copy templates.  They are part of the "empty" source database, so they are already
                                /// present.  So, this was a waste of time.
                                //this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = templateConfigurationsText));
                                //generalTransferErrorsList = CopyTemplateConfigurationsForAllMonitorTypes(sourceDB, destinationDB);
                                //if (generalTransferErrorsList.Count > 0)
                                //{
                                //    this.copyErrorsList.Add("");
                                //    this.copyErrorsList.Add(templateConfigurationsText);
                                //    this.copyErrorsList.Add("");
                                //    this.copyErrorsList.AddRange(generalTransferErrorsList);
                                //}
                                this.Invoke(new MethodInvoker(() => this.dataTransferTypeValueRadLabel.Text = noneText));
                                SetHierarchyDisplayNames(string.Empty);
                            }
                            else
                            {
                                returnObject.errorCode = ErrorCode.GeneralDatabaseError;
                                this.copyErrorsList.Add(requiredStructureWasNotCreatedInNewDatabaseText);
                            }
                        }
                        else
                        {
                            this.copyErrorsList.Add(requiredStructureWasNotCreatedInNewDatabaseText);
                            this.copyErrorsList.Add(string.Empty);
                            this.copyErrorsList.AddRange(generalTransferErrorsList);
                            returnObject.errorCode = ErrorCode.GeneralDatabaseError;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in CopyDatabaseData.copyDataBackgroundWorker_DoWork(object, DoWorkEventArgs)\ngeneralTransferErrorsList was null";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                //returnObject.errorsList = copyErrorsList;
                e.Result = returnObject;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyDatabaseData.copyDataBackgroundWorker_DoWork(object, DoWorkEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {

            }
        }

        //        private void copyDataBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //        {
        //            try
        //            {
        //                UpdateDownloadProgressInterfaceObjects(e.ProgressPercentage);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in CopyDatabaseData.copyDataBackgroundWorker_ProgressChanged(object, ProgressChangedEventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void copyDataBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    string errorMessage = "Exception thrown in CopyDatabaseData.copyDataBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + e.Error.Message;
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                else if (e.Cancelled)
                {
                    if (this.copyOperationType == CopyOperationType.Export)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DataExportCancelled));
                    }
                    else if (this.copyOperationType == CopyOperationType.Upgrade)
                    {
                        RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DatabaseUpgradeCancelled));
                    }
                }
                else
                {
                    CopyDataThreadReturnObject exportDataThreadReturnObj = e.Result as CopyDataThreadReturnObject;
                    if (exportDataThreadReturnObj != null)
                    {
                        if (exportDataThreadReturnObj.errorCode != ErrorCode.None)
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(exportDataThreadReturnObj.errorCode));
                        }
                        //if ((exportDataThreadReturnObj.errorsList != null) && (exportDataThreadReturnObj.errorsList.Count > 0))
                        if(this.copyErrorsList.Count>0)
                        {
                            if (this.copyOperationType == CopyOperationType.Export)
                            {
                                //RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(generalErrorsOccurredDuringExportText);
                            }
                            else if(this.copyOperationType == CopyOperationType.Upgrade)
                            {
                                //RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(generalErrorsOccurredDuringUpgradeText);
                            }
                            errorsOccurredDuringCopy = true;                           
                        }
                        else
                        {
                            errorsOccurredDuringCopy = false;
                        }
                    }
                    else
                    {
                        if (this.copyOperationType == CopyOperationType.Export)
                        {
                            //RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(exportDataThreadProbablyTerminatedInErrorStateText);
                        }
                        else
                        {
                           // RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(updateDatabaseThreadProbablyTerminatedInErrorStateText);
                        }
                        string errorMessage = "Error in CopyDatabaseData.copyDataBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nBad message passed back from the DoWork method.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyDatabaseData.manualDownloadBackgroundWorker_RunWorkerCompleted(object, RunWorkerCompletedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                copyDataRunningRadWaitingBar.StopWaiting();
                this.Close();
            }
        }

        #endregion

        private void SetHierarchyDisplayNames(string hierarchy)
        {
            try
            {
                string[] pieces = hierarchy.Split(':');
                if (pieces.Length == 4)
                {
                    this.Invoke(new MethodInvoker(() => this.companyNameValueRadLabel.Text = pieces[0].Trim()));
                    this.Invoke(new MethodInvoker(() => this.plantNameValueRadLabel.Text = pieces[1].Trim()));
                    this.Invoke(new MethodInvoker(() => this.equipmentNameValueRadLabel.Text = pieces[2].Trim()));
                    this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = pieces[3].Trim()));
                }
                else
                {
                    this.Invoke(new MethodInvoker(() => this.companyNameValueRadLabel.Text = noneText));
                    this.Invoke(new MethodInvoker(() => this.plantNameValueRadLabel.Text = noneText));
                    this.Invoke(new MethodInvoker(() => this.equipmentNameValueRadLabel.Text = noneText));
                    this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = noneText));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.SetHierarchyDisplayNames(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private List<String> CreateErrorHeader(Monitor monitor, List<Company> companyList, List<Plant> plantList, List<Equipment> equipmentList)
        {
            List<string> errorHeader = new List<string>();
            try
            {
                errorHeader.Add("Error for " + General_DatabaseMethods.GetHierarchyForOneMonitor(monitor, companyList, plantList, equipmentList));
                errorHeader.Add(" ");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.WriteErrorHeader(Monitor, List<Company>, List<Plant>, List<Equipment>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorHeader;
        }

        private List<String> CreateErrorsList(int errorType, List<string> errorList)
        {
            List<string> outputErrorList = new List<string>();
            try
            {
                //outputErrorList.Add(" ");
                if (errorType == 0)
                {
                   // outputErrorList.Add(failedToCopyTheDataToNewDatabaseText);
                }
                else if (errorType == 1)
                {
                    outputErrorList.Add(failedToCopyTheMonitorConfigurationsText);
                }
                else if (errorType == 2)
                {
                    outputErrorList.Add(failedToCopyTheDataViewerPreferencesText);
                }
               // outputErrorList.Add(" ");

                foreach (string entry in errorList)
                {
                    outputErrorList.Add(entry);
                }

               // outputErrorList.Add(" ");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.AddErrorsToList(int, List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputErrorList;
        }

        public ErrorCode TransferDataForOneMonitor(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB exportDB)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor;
                string monitorType;

                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, sourceDB);
                if (monitor != null)
                {
                    monitorType = monitor.MonitorType.Trim();

                    if (monitorType.CompareTo("Main") == 0)
                    {
                        errorCode = MainMonitor_DatabaseMethods.Main_Data_CopyAllDataForOneMonitorToNewDatabase(monitorID, sourceDB, exportDB);
                    }
                    else if (monitorType.CompareTo("ADM") == 0)
                    {

                    }
                    else if (monitorType.CompareTo("BHM") == 0)
                    {
                        errorCode = BHM_DatabaseMethods.BHM_Data_CopyAllDataForOneMonitorToNewDatabase(monitorID, sourceDB, exportDB);
                    }
                    else if (monitorType.CompareTo("PDM") == 0)
                    {
                        errorCode = PDM_DatabaseMethods.PDM_Data_CopyAllDataForOneMonitorToNewDatabase(monitorID, sourceDB, exportDB);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.TransferDataForOneMonitor(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

//        private List<string> CopyTemplateConfigurationsForAllMonitorTypes(MonitorInterfaceDB sourceDB, MonitorInterfaceDB exportDB)
//        {
//            List<string> failedConfigurationTransfers = new List<string>();
//            try
//            {
//                List<Main_Config_ConfigurationRoot> mainTemplateConfigurationRoots =
//                    MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), sourceDB);
//                List<BHM_Config_ConfigurationRoot> bhmTemplateConfigurationRoots =
//                    BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), sourceDB);
//                List<PDM_Config_ConfigurationRoot> pdmTemplateConfigurationRoots =
//                    PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID(), sourceDB);

//                if ((mainTemplateConfigurationRoots != null) && (mainTemplateConfigurationRoots.Count > 0))
//                {
//                    failedConfigurationTransfers.AddRange(Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(mainTemplateConfigurationRoots, sourceDB, exportDB));
//                }
//                else
//                {
//                    failedConfigurationTransfers.Add(noMainConfigurationTemplatesToCopyText);
//                }
//                if ((bhmTemplateConfigurationRoots != null) && (bhmTemplateConfigurationRoots.Count > 0))
//                {
//                    failedConfigurationTransfers.AddRange(BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(bhmTemplateConfigurationRoots, sourceDB, exportDB));
//                }
//                else
//                {
//                    failedConfigurationTransfers.Add(noBhmConfigurationTemplatesToCopyText);
//                }
//                if ((pdmTemplateConfigurationRoots != null) && (pdmTemplateConfigurationRoots.Count > 0))
//                {
//                    failedConfigurationTransfers.AddRange(PDM_Config_CopyAllConfigurationsForOneMonitorToTheNewDatabase(pdmTemplateConfigurationRoots, sourceDB, exportDB));
//                }
//                else
//                {
//                    failedConfigurationTransfers.Add(noPdmConfigurationTemplatesToCopyText);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in CopyData.TransferDataForOneMonitor(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return failedConfigurationTransfers;
//        }

        public List<string> TransferConfigurationsForOneMonitor(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB exportDB)
        {
            List<string> failedConfigurationTransfers = null;
            try
            {
                Monitor monitor;
                string monitorType;

                monitor = General_DatabaseMethods.GetOneMonitor(monitorID, sourceDB);
                if (monitor != null)
                {
                    monitorType = monitor.MonitorType.Trim();

                    if (monitorType.CompareTo("Main") == 0)
                    {
                        failedConfigurationTransfers = Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(monitor, sourceDB, exportDB);
                    }
                    else if (monitorType.CompareTo("ADM") == 0)
                    {
                        failedConfigurationTransfers = new List<string>();
                    }
                    else if (monitorType.CompareTo("BHM") == 0)
                    {
                        failedConfigurationTransfers = BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(monitor, sourceDB, exportDB);
                    }
                    else if (monitorType.CompareTo("PDM") == 0)
                    {
                        failedConfigurationTransfers = PDM_Config_CopyAllConfigurationsForOneMonitorToTheNewDatabase(monitor, sourceDB, exportDB);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.TransferDataForOneMonitor(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedConfigurationTransfers;
        }

        public List<string> BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
                List<BHM_Config_ConfigurationRoot> configurationRootList = BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if ((configurationRootList != null) && (configurationRootList.Count > 0))
                    {
                        failedWrites = BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(configurationRootList, sourceDB, destinationDB);
                    }
                    else
                    {
                        string errorMessage = "Potential error in CopyData.BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nFound no configurations for the input monitor.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        //MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in CopyData.BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nTarget monitor was not present in the destination database.  Could not write the configuration.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        private List<string> BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(List<BHM_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                string errorString;
                int counter = 1;
                int numberOfDataItems;

                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    numberOfDataItems = configurationRootList.Count;
                    //this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = "BHM"));
                    //this.dataTypeValueRadLabel.Text = configurationsText;
                    foreach (BHM_Config_ConfigurationRoot configurationRoot in configurationRootList)
                    {
                        //this.itemNumberValueRadLabel.Text = counter.ToString() + " " + ofText + " " + numberOfDataItems.ToString();
                        if (!BHM_DatabaseMethods.BHM_Config_CopyOneConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationDB))
                        {
                            errorString = configurationRoot.DateAdded.ToString() + "    " + configurationRoot.Description;
                            failedWrites.Add(errorString);
                            BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                        }
                        counter++;
                    }
                    //this.dataTypeValueRadLabel.Text = noneText;
                    //this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = noneText));
                    //this.itemNumberValueRadLabel.Text = "0 " + ofText + " 0";
                }               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.BHM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(List<BHM_Config_ConfigurationRoot>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        public List<string> Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
                List<Main_Config_ConfigurationRoot> configurationRootList = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if ((configurationRootList != null) && (configurationRootList.Count > 0))
                    {
                        failedWrites = Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(configurationRootList, sourceDB, destinationDB);
                    }
                    else
                    {
                        string errorMessage = "Potential error in CopyData.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nFound no configurations for the input monitor.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        //MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in CopyData.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nTarget monitor was not present in the destination database.  Could not write the configuration.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        private List<string> Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(List<Main_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                string errorString;
                int counter = 1;
                int numberOfDataItems;

                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    numberOfDataItems = configurationRootList.Count;
                    //this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = "Main"));
                    //this.dataTypeValueRadLabel.Text = configurationsText;
                    foreach (Main_Config_ConfigurationRoot configurationRoot in configurationRootList)
                    {
                        // this.itemNumberValueRadLabel.Text = counter.ToString() + " " + ofText + " " + numberOfDataItems.ToString();
                        if (!MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationDB))
                        {
                            errorString = configurationRoot.DateAdded.ToString() + "    " + configurationRoot.Description;
                            failedWrites.Add(errorString);
                            MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                        }
                        counter++;
                    }
                    //this.dataTypeValueRadLabel.Text = noneText;
                    // this.Invoke(new MethodInvoker(() =>  this.monitorNameValueRadLabel.Text = noneText));
                    //this.itemNumberValueRadLabel.Text = "0 " + ofText + " 0";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(List<Main_Config_ConfigurationRoot>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        public List<string> PDM_Config_CopyAllConfigurationsForOneMonitorToTheNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> errorList = new List<string>();
            try
            {
                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
                List<PDM_Config_ConfigurationRoot> configurationRootList = PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if ((configurationRootList != null) && (configurationRootList.Count > 0))
                    {
                        errorList = PDM_Config_CopyAllConfigurationsForOneMonitorToTheNewDatabase(configurationRootList, sourceDB, destinationDB);
                    }
                    else
                    {
                        string errorMessage = "Potential error in CopyData.PDM_Config_CopyAllConfigurationsForOneMonitor(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find any configurations to copy.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                       // MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in CopyData.PDM_Config_CopyAllConfigurationsForOneMonitor(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the monitor in the destination database.  Could not copy the configurations.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.PDM_Config_CopyAllConfigurationsForOneMonitor(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorList;
        }

        private List<string> PDM_Config_CopyAllConfigurationsForOneMonitorToTheNewDatabase(List<PDM_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> errorList = new List<string>();
            try
            {
                string errorString;
                int counter = 1;
                int numberOfDataItems;

                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    //this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = "PDM"));
                    //this.dataTypeValueRadLabel.Text = dataText;
                    numberOfDataItems = configurationRootList.Count;
                    foreach (PDM_Config_ConfigurationRoot configurationRoot in configurationRootList)
                    {
                        //this.itemNumberValueRadLabel.Text = counter.ToString() + " " + ofText + " " + numberOfDataItems.ToString();
                        if (!PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationDB))
                        {
                            errorString = configurationRoot.DateAdded.ToString() + "    " + configurationRoot.Description;
                            errorList.Add(errorString);
                            PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                        }
                        counter++;
                    }
                    // this.dataTypeValueRadLabel.Text = noneText;
                    //this.Invoke(new MethodInvoker(() => this.monitorNameValueRadLabel.Text = noneText));
                    //this.itemNumberValueRadLabel.Text = "0 " + ofText + " 0";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.PDM_Config_CopyAllConfigurationsForOneMonitor(List<PDM_Config_ConfigurationRoot>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorList;
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            copyDataBackgroundWorker.CancelAsync();
            copyWasCancelled = true;
            RadMessageBox.Show(this, copyCancelledMessageText);
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = copyDatabaseDataTitleText;

                copyingInformationFromRadLabel.Text = copyingInformationFromRadLabelText;
                monitorNumberRadLabel.Text = monitorNumberRadLabelText;
                companyNameRadLabel.Text = companyNameRadLabelText;
                plantNameRadLabel.Text = plantNameRadLabelText;
                equipmentNameRadLabel.Text = equipmentNameRadLabelText;
                monitorNameRadLabel.Text = monitorNameRadLabelText;
                dataTransferTypeRadLabel.Text = dataTransferTypeRadLabelText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                failedToCopyTheMonitorConfigurationsText = LanguageConversion.GetStringAssociatedWithTag("CopyDataFailedToCopyTheMonitorConfigurationsText", failedToCopyTheMonitorConfigurationsText, "", "", "");
                failedToCopyTheDataViewerPreferencesText = LanguageConversion.GetStringAssociatedWithTag("CopyDataFailedToCopyTheDataViewerPreferencesText", failedToCopyTheDataViewerPreferencesText, "", "", "");

                cancelUpdateText = LanguageConversion.GetStringAssociatedWithTag("CopyDataCancelUpdateText", cancelUpdateText, "", "", "");
                cancelExportText = LanguageConversion.GetStringAssociatedWithTag("CopyDataCancelExportText", cancelExportText, "", "", "");

                ofText = LanguageConversion.GetStringAssociatedWithTag("CopyDataOfText", ofText, "", "", "");
                noneText = LanguageConversion.GetStringAssociatedWithTag("CopyDataNoneText", noneText, "", "", "");
                dataText = LanguageConversion.GetStringAssociatedWithTag("CopyDataDataText", dataText, "", "", "");
                configurationsText = LanguageConversion.GetStringAssociatedWithTag("CopyDataConfigurationsText", configurationsText, "", "", "");
                //templateConfigurationsText = LanguageConversion.GetStringAssociatedWithTag("CopyDataTemplateConfigurationsText", templateConfigurationsText, "", "", "");
                
                preferencesText = LanguageConversion.GetStringAssociatedWithTag("CopyDataPreferencesText", preferencesText, "", "", "");

                //noMainConfigurationTemplatesToCopyText = LanguageConversion.GetStringAssociatedWithTag("CopyDataNoMainConfigurationTemplatesToCopyText", noMainConfigurationTemplatesToCopyText, "", "", "");
                //noBhmConfigurationTemplatesToCopyText = LanguageConversion.GetStringAssociatedWithTag("CopyDataNoBhmConfigurationTemplatesToCopyText", noBhmConfigurationTemplatesToCopyText, "", "", "");
                //noPdmConfigurationTemplatesToCopyText = LanguageConversion.GetStringAssociatedWithTag("CopyDataNoPdmConfigurationTemplatesToCopyText", noPdmConfigurationTemplatesToCopyText, "", "", "");


                copyingInformationFromRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceCopyingInformationFromRadLabelText", copyingInformationFromRadLabelText, "", "", "");

                monitorNumberRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceMonitorNumberRadLabelText", monitorNumberRadLabelText, "", "", "");
                companyNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceCompanyNameRadLabelText", companyNameRadLabelText, "", "", "");
                plantNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfacePlantNameRadLabelText", plantNameRadLabelText, "", "", "");
                equipmentNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceEquipmentNameRadLabelText", equipmentNameRadLabelText, "", "", "");
                monitorNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceMonitorNameRadLabelText", monitorNameRadLabelText, "", "", "");
                dataTransferTypeRadLabelText = LanguageConversion.GetStringAssociatedWithTag("CopyDataInterfaceDataTransferTypeRadLabelText", dataTransferTypeRadLabelText, "", "", "");

                copyDatabaseDataTitleText = LanguageConversion.GetStringAssociatedWithTag("CopyDataCopyDatabaseDataTitleText", copyDatabaseDataTitleText, "", "", "");

                requiredStructureWasNotCreatedInNewDatabaseText = LanguageConversion.GetStringAssociatedWithTag("CopyDataRequiredStructureWasNotCreatedInNewDatabaseText", requiredStructureWasNotCreatedInNewDatabaseText, "", "", "");

                copyCancelledMessageText = LanguageConversion.GetStringAssociatedWithTag("CopyDatacopyCancelledMessageText", copyCancelledMessageText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in CopyData.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }       

    }

    public class CopyDataThreadInputObject
    {
       
    }

    public class CopyDataThreadReturnObject
    {
        public ErrorCode errorCode = ErrorCode.None;
        public List<String> errorsList;
    }

    public enum CopyOperationType
    {
        Export,
        Upgrade
    }
}
