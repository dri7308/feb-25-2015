﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer.Management.Smo;

using GeneralUtilities;
using DatabaseInterface;

namespace DatabaseFileInteraction
{
    public partial class SetServerName : Telerik.WinControls.UI.RadForm
    {
        DataTable availableServers;
        Int32[] availableServersUsedIndex;

        private static string setServerNameText = "Set Server Name";

        private static string noServerInstanceDefinedText = "No server instance defined, will set it to SQLExpress";
        private static string sqlExpressServerNameText = "SQLExpress";

        private static string notSpecifiedText = "Not specified";

        private static string getServersRadGroupBoxText = "File Type";
        private static string localServersRadRadioButtonText = "Local Only";
        private static string networkServersRadRadioButtonText = "Network Only";
        private static string allServersRadRadioButtonText = "All Servers";
        private static string getServersRadButtonText = "Get Servers";
        private static string availableServersRadGroupBoxText = "Available SQL Servers";
        private static string serverNameRadLabelText = "Server Name";
        private static string serverInstanceRadLabelText = "Server Instance";

        private static string haveNotSpecifiedAServerToSaveText = "You have not yet specified a server to save";

        private string serverName = string.Empty;
        public string ServerName
        {
            get
            {
                return serverName;
            }
        }

        private string serverInstance = string.Empty;
        public string ServerInstance
        {
            get
            {
                return serverInstance;
            }
        }

        private bool saveServerChoice = false;
        public bool SaveServerChoice
        {
            get
            {
                return saveServerChoice;
            }
        }

        public SetServerName()
        {
            InitializeComponent();

            this.serverName = notSpecifiedText;
            this.serverInstance = notSpecifiedText;
        }

        public SetServerName(string argServerInstance, string argServerName)
        {
            InitializeComponent();

            this.serverName = argServerName;
            this.serverInstance = argServerInstance;
        }

        private void SetServerName_Load(object sender, EventArgs e)
        {
           // localServersRadRadioButton.Enabled = false;
           // networkServersRadRadioButton.Enabled = false;

            this.serverNameRadTextBox.Text = this.serverName;
            this.serverInstanceRadTextBox.Text = this.serverInstance;
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                setServerNameText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameSetServerNameText", setServerNameText, "", "", "");
                noServerInstanceDefinedText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameNoServerInstanceDefinedText", noServerInstanceDefinedText, "", "", "");

                sqlExpressServerNameText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameSqlExpressServerNameText", sqlExpressServerNameText, "", "", "");
                notSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameNotSpecifiedText", notSpecifiedText, "", "", "");
                getServersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameGetServersRadGroupBoxText", getServersRadGroupBoxText, "", "", "");
                localServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameLocalServersRadRadioButtonText", localServersRadRadioButtonText, "", "", "");
                networkServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameNetworkServersRadRadioButtonText", noServerInstanceDefinedText, "", "", "");
                allServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameAllServersRadRadioButtonText", allServersRadRadioButtonText, "", "", "");
                getServersRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameGetServersRadButtonText", getServersRadButtonText, "", "", "");
                availableServersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameAvailableServersRadGroupBoxText", availableServersRadGroupBoxText, "", "", "");
                serverNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameServerNameRadLabelText", serverNameRadLabelText, "", "", "");
                serverInstanceRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameServerInstanceRadLabelText", serverInstanceRadLabelText, "", "", "");
                haveNotSpecifiedAServerToSaveText = LanguageConversion.GetStringAssociatedWithTag("SetServerNameHaveNotSpecifiedAServerToSaveText", haveNotSpecifiedAServerToSaveText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetServerName.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }       

        private void getServersRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.availableServers = SmoApplication.EnumAvailableSqlServers(true);
                this.availableServersRadListControl.Items.Clear();
                this.availableServersUsedIndex = new int[this.availableServers.Rows.Count];

                string displayString;
                int serverUsedIndex = 0;
                int serverAvailableIndex = 0;
                foreach (DataRow dr in this.availableServers.Rows)
                {
                    displayString = dr["Name"].ToString();

                    /// will change this later when filtering servers, for now we display all
                    /// available servers
                    availableServersUsedIndex[serverUsedIndex] = serverAvailableIndex;
                    serverUsedIndex++;
                    serverAvailableIndex++;

                    displayString = displayString + "    Is Local Server = " + dr["IsLocal"];

                    //if(dr["IsLocal"] == "True")
                    //{
                    //    displayString = displayString + "    Local Server";
                    //}
                    //else
                    //{
                    //    displayString = displayString + "    Remote Server";
                    //}

                    this.availableServersRadListControl.Items.Add(displayString);

                    //listBox1.Items.Add("server = " + dr["Server"]);
                    //listBox1.Items.Add("Instance = " + dr["Instance"]);
                    //listBox1.Items.Add("Version = " + dr["Version"]);
                    //listBox1.Items.Add("IsLocal = " + dr["IsLocal"]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetServerName.getServersRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableServersRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableServersRadListControl.SelectedIndex;

                if (selectedIndex > -1)
                {
                    int availableServerRowNumber = this.availableServersUsedIndex[selectedIndex];
                    this.serverNameRadTextBox.Text = this.availableServers.Rows[availableServerRowNumber]["Server"].ToString();

                    string serverInstance = this.availableServers.Rows[availableServerRowNumber]["Instance"].ToString();
                    if (serverInstance == string.Empty)
                    {
                        RadMessageBox.Show(this, noServerInstanceDefinedText);
                        serverInstance = sqlExpressServerNameText;
                    }
                    this.serverInstanceRadTextBox.Text = serverInstance;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetServerName.availableServersRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveServerChoiceRadButton_Click(object sender, EventArgs e)
        {
            serverName = this.serverNameRadTextBox.Text.Trim();
            serverInstance = this.serverInstanceRadTextBox.Text.Trim();
            if ((serverName.CompareTo(notSpecifiedText) != 0) && (serverInstance.CompareTo(notSpecifiedText) != 0))
            {
                saveServerChoice = true;
                this.Close();
            }
            else
            {
                RadMessageBox.Show(this, haveNotSpecifiedAServerToSaveText);
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            serverName = string.Empty;
            serverInstance = string.Empty;
            saveServerChoice = false;
            this.Close();
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = setServerNameText;

                getServersRadGroupBox.Text = getServersRadGroupBoxText;
                localServersRadRadioButton.Text = localServersRadRadioButtonText;
                networkServersRadRadioButton.Text = networkServersRadRadioButtonText;
                allServersRadRadioButton.Text = allServersRadRadioButtonText;
                getServersRadButton.Text = getServersRadButtonText;
                availableServersRadGroupBox.Text = availableServersRadGroupBoxText;
                serverNameRadLabel.Text = serverNameRadLabelText;
                serverInstanceRadLabel.Text = serverInstanceRadLabelText;
                //databaseNameRadLabel.Text = databaseNameRadLabelText;
                //databaseLocationRadLabel.Text = databaseLocationRadLabelText;
                //authenticationRadLabel.Text = authenticationRadLabelText;
                //userNameRadLabel.Text = userNameRadLabelText;
                //passwordRadLabel.Text = passwordRadLabelText;
                //selectDatabaseRadButton.Text = selectDatabaseRadButtonText;
                //testConnectionRadButton.Text = testConnectionRadButtonText;
                //saveAsDefaultConnectionRadButton.Text = saveAsDefaultConnectionRadButtonText;
                //cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetServerName.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void availableServersRadListControl_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (this.availableServers != null)
                {
                    if (this.availableServers.Rows.Count > 0)
                    {
                        int selectedIndex = availableServersRadListControl.SelectedIndex;
                        if (selectedIndex > -1)
                        {
                            int availableServerRowNumber = this.availableServersUsedIndex[selectedIndex];
                            this.serverNameRadTextBox.Text = this.availableServers.Rows[availableServerRowNumber]["Server"].ToString();

                            string serverInstance = this.availableServers.Rows[availableServerRowNumber]["Instance"].ToString();
                            if (serverInstance == string.Empty)
                            {
                                RadMessageBox.Show(this, noServerInstanceDefinedText);
                                serverInstance = sqlExpressServerNameText;
                            }
                            this.serverInstanceRadTextBox.Text = serverInstance;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetServerName.availableServersRadListControl_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        } 
    }
}
