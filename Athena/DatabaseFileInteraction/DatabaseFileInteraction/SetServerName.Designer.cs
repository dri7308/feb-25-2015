﻿namespace DatabaseFileInteraction
{
    public partial class SetServerName : Telerik.WinControls.UI.RadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetServerName));
            this.getServersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.getServersRadButton = new Telerik.WinControls.UI.RadButton();
            this.allServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.networkServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.localServersRadRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.availableServersRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.availableServersRadListControl = new Telerik.WinControls.UI.RadListControl();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.serverInstanceRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.serverInstanceRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serverNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.serverNameRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            this.saveServerChoiceRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadGroupBox)).BeginInit();
            this.getServersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localServersRadRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadGroupBox)).BeginInit();
            this.availableServersRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveServerChoiceRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // getServersRadGroupBox
            // 
            this.getServersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.getServersRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.getServersRadGroupBox.Controls.Add(this.getServersRadButton);
            this.getServersRadGroupBox.Controls.Add(this.allServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.networkServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.localServersRadRadioButton);
            this.getServersRadGroupBox.Controls.Add(this.radButton1);
            this.getServersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getServersRadGroupBox.HeaderText = "<html>File Type</html>";
            this.getServersRadGroupBox.Location = new System.Drawing.Point(12, 12);
            this.getServersRadGroupBox.Name = "getServersRadGroupBox";
            this.getServersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.getServersRadGroupBox.Size = new System.Drawing.Size(168, 165);
            this.getServersRadGroupBox.TabIndex = 52;
            this.getServersRadGroupBox.Text = "<html>File Type</html>";
            this.getServersRadGroupBox.ThemeName = "Office2007Black";
            // 
            // getServersRadButton
            // 
            this.getServersRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getServersRadButton.Location = new System.Drawing.Point(13, 114);
            this.getServersRadButton.Name = "getServersRadButton";
            this.getServersRadButton.Size = new System.Drawing.Size(142, 38);
            this.getServersRadButton.TabIndex = 32;
            this.getServersRadButton.Text = "<html>Get Servers</html>";
            this.getServersRadButton.ThemeName = "Office2007Black";
            this.getServersRadButton.Click += new System.EventHandler(this.getServersRadButton_Click);
            // 
            // allServersRadRadioButton
            // 
          //  this.allServersRadRadioButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.allServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.allServersRadRadioButton.Location = new System.Drawing.Point(13, 80);
            this.allServersRadRadioButton.Name = "allServersRadRadioButton";
            this.allServersRadRadioButton.Size = new System.Drawing.Size(75, 16);
            this.allServersRadRadioButton.TabIndex = 11;
            this.allServersRadRadioButton.Text = "All Servers";
            this.allServersRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // networkServersRadRadioButton
            // 
            this.networkServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.networkServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.networkServersRadRadioButton.Location = new System.Drawing.Point(13, 56);
            this.networkServersRadRadioButton.Name = "networkServersRadRadioButton";
            this.networkServersRadRadioButton.Size = new System.Drawing.Size(88, 16);
            this.networkServersRadRadioButton.TabIndex = 10;
            this.networkServersRadRadioButton.Text = "Network Only";
            // 
            // localServersRadRadioButton
            // 
            this.localServersRadRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.localServersRadRadioButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.localServersRadRadioButton.Location = new System.Drawing.Point(13, 32);
            this.localServersRadRadioButton.Name = "localServersRadRadioButton";
            this.localServersRadRadioButton.Size = new System.Drawing.Size(74, 16);
            this.localServersRadRadioButton.TabIndex = 9;
            this.localServersRadRadioButton.Text = "Local Only";
            // 
            // radButton1
            // 
            this.radButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton1.Location = new System.Drawing.Point(0, 232);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(130, 70);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton1.ThemeName = "Office2007Black";
            // 
            // availableServersRadGroupBox
            // 
            this.availableServersRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.availableServersRadGroupBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.availableServersRadGroupBox.Controls.Add(this.availableServersRadListControl);
            this.availableServersRadGroupBox.Controls.Add(this.radButton2);
            this.availableServersRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableServersRadGroupBox.HeaderText = "<html>Available SQL Servers</html>";
            this.availableServersRadGroupBox.Location = new System.Drawing.Point(186, 12);
            this.availableServersRadGroupBox.Name = "availableServersRadGroupBox";
            this.availableServersRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.availableServersRadGroupBox.Size = new System.Drawing.Size(399, 165);
            this.availableServersRadGroupBox.TabIndex = 51;
            this.availableServersRadGroupBox.Text = "<html>Available SQL Servers</html>";
            this.availableServersRadGroupBox.ThemeName = "Office2007Black";
            // 
            // availableServersRadListControl
            // 
            this.availableServersRadListControl.AutoScroll = true;
            this.availableServersRadListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableServersRadListControl.Location = new System.Drawing.Point(10, 20);
            this.availableServersRadListControl.Name = "availableServersRadListControl";
            this.availableServersRadListControl.Size = new System.Drawing.Size(379, 135);
            this.availableServersRadListControl.TabIndex = 9;
            this.availableServersRadListControl.Text = "radListControl2";
            this.availableServersRadListControl.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.availableServersRadListControl_SelectedIndexChanged);
            this.availableServersRadListControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.availableServersRadListControl_MouseClick);
            // 
            // radButton2
            // 
            this.radButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButton2.Location = new System.Drawing.Point(0, 232);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(130, 70);
            this.radButton2.TabIndex = 8;
            this.radButton2.Text = "<html>Save Configuration<br>to<br>Database</html>";
            this.radButton2.ThemeName = "Office2007Black";
            // 
            // serverInstanceRadTextBox
            // 
            this.serverInstanceRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverInstanceRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverInstanceRadTextBox.Location = new System.Drawing.Point(134, 217);
            this.serverInstanceRadTextBox.Name = "serverInstanceRadTextBox";
            this.serverInstanceRadTextBox.Size = new System.Drawing.Size(451, 18);
            this.serverInstanceRadTextBox.TabIndex = 50;
            this.serverInstanceRadTextBox.TabStop = false;
            this.serverInstanceRadTextBox.Text = "Not Specified";
            // 
            // serverInstanceRadLabel
            // 
            this.serverInstanceRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverInstanceRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serverInstanceRadLabel.Location = new System.Drawing.Point(25, 219);
            this.serverInstanceRadLabel.Name = "serverInstanceRadLabel";
            this.serverInstanceRadLabel.Size = new System.Drawing.Size(85, 16);
            this.serverInstanceRadLabel.TabIndex = 49;
            this.serverInstanceRadLabel.Text = "Server Instance";
            this.serverInstanceRadLabel.ThemeName = "Office2007Black";
            // 
            // serverNameRadLabel
            // 
            this.serverNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.serverNameRadLabel.Location = new System.Drawing.Point(25, 195);
            this.serverNameRadLabel.Name = "serverNameRadLabel";
            this.serverNameRadLabel.Size = new System.Drawing.Size(73, 16);
            this.serverNameRadLabel.TabIndex = 48;
            this.serverNameRadLabel.Text = "Server Name";
            this.serverNameRadLabel.ThemeName = "Office2007Black";
            // 
            // serverNameRadTextBox
            // 
            this.serverNameRadTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverNameRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverNameRadTextBox.Location = new System.Drawing.Point(134, 193);
            this.serverNameRadTextBox.Name = "serverNameRadTextBox";
            this.serverNameRadTextBox.Size = new System.Drawing.Size(451, 18);
            this.serverNameRadTextBox.TabIndex = 47;
            this.serverNameRadTextBox.TabStop = false;
            this.serverNameRadTextBox.Text = "Not Specified";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelRadButton.Location = new System.Drawing.Point(468, 244);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(117, 38);
            this.cancelRadButton.TabIndex = 53;
            this.cancelRadButton.Text = "<html>Cancel</html>";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // saveServerChoiceRadButton
            // 
            this.saveServerChoiceRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveServerChoiceRadButton.Location = new System.Drawing.Point(345, 244);
            this.saveServerChoiceRadButton.Name = "saveServerChoiceRadButton";
            this.saveServerChoiceRadButton.Size = new System.Drawing.Size(117, 38);
            this.saveServerChoiceRadButton.TabIndex = 54;
            this.saveServerChoiceRadButton.Text = "<html>Save Server</html>";
            this.saveServerChoiceRadButton.ThemeName = "Office2007Black";
            this.saveServerChoiceRadButton.Click += new System.EventHandler(this.saveServerChoiceRadButton_Click);
            // 
            // SetServerName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(601, 289);
            this.ControlBox = false;
            this.Controls.Add(this.saveServerChoiceRadButton);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.getServersRadGroupBox);
            this.Controls.Add(this.availableServersRadGroupBox);
            this.Controls.Add(this.serverInstanceRadTextBox);
            this.Controls.Add(this.serverInstanceRadLabel);
            this.Controls.Add(this.serverNameRadLabel);
            this.Controls.Add(this.serverNameRadTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetServerName";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Set Server Name";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.SetServerName_Load);
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadGroupBox)).EndInit();
            this.getServersRadGroupBox.ResumeLayout(false);
            this.getServersRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.getServersRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.networkServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localServersRadRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadGroupBox)).EndInit();
            this.availableServersRadGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.availableServersRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverInstanceRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveServerChoiceRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox getServersRadGroupBox;
        private Telerik.WinControls.UI.RadButton getServersRadButton;
        private Telerik.WinControls.UI.RadRadioButton allServersRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton networkServersRadRadioButton;
        private Telerik.WinControls.UI.RadRadioButton localServersRadRadioButton;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox availableServersRadGroupBox;
        private Telerik.WinControls.UI.RadListControl availableServersRadListControl;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadTextBox serverInstanceRadTextBox;
        private Telerik.WinControls.UI.RadLabel serverInstanceRadLabel;
        private Telerik.WinControls.UI.RadLabel serverNameRadLabel;
        private Telerik.WinControls.UI.RadTextBox serverNameRadTextBox;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
        private Telerik.WinControls.UI.RadButton saveServerChoiceRadButton;
    }
}