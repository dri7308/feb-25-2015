﻿namespace DatabaseFileInteraction
{
    partial class ShowFileListing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowFileListing));
            this.closeRadButton = new Telerik.WinControls.UI.RadButton();
            this.fileNamesRadListControl = new Telerik.WinControls.UI.RadListControl();
            ((System.ComponentModel.ISupportInitialize)(this.closeRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileNamesRadListControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // closeRadButton
            // 
            this.closeRadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeRadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeRadButton.Location = new System.Drawing.Point(272, 122);
            this.closeRadButton.Name = "closeRadButton";
            this.closeRadButton.Size = new System.Drawing.Size(88, 38);
            this.closeRadButton.TabIndex = 56;
            this.closeRadButton.Text = "<html>Close</html>";
            this.closeRadButton.ThemeName = "Office2007Black";
            this.closeRadButton.Click += new System.EventHandler(this.closeRadButton_Click);
            // 
            // fileNamesRadListControl
            // 
            this.fileNamesRadListControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNamesRadListControl.AutoScroll = true;
            this.fileNamesRadListControl.CaseSensitiveSort = true;
            this.fileNamesRadListControl.ItemHeight = 18;
            this.fileNamesRadListControl.Location = new System.Drawing.Point(8, 8);
            this.fileNamesRadListControl.Name = "fileNamesRadListControl";
            this.fileNamesRadListControl.Size = new System.Drawing.Size(352, 108);
            this.fileNamesRadListControl.TabIndex = 57;
            this.fileNamesRadListControl.Text = "radListControl2";
            // 
            // ShowFileListing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(372, 172);
            this.Controls.Add(this.fileNamesRadListControl);
            this.Controls.Add(this.closeRadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShowFileListing";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Show File Listing";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.ShowFileListing_Load);
            ((System.ComponentModel.ISupportInitialize)(this.closeRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileNamesRadListControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton closeRadButton;
        private Telerik.WinControls.UI.RadListControl fileNamesRadListControl;
    }
}