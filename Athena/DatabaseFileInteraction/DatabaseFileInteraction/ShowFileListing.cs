﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;

namespace DatabaseFileInteraction
{
    public partial class ShowFileListing : Telerik.WinControls.UI.RadForm
    {
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string showFileListingTitleText = "Show File Listing";
        private static string closeRadButtonText = "Close";

        List<string> fileNames = null;

        public ShowFileListing(List<string> argFileNames)
        {
            try
            {
                InitializeComponent();
                fileNames = argFileNames;
                AssignStringValuesToInterfaceObjects();
                this.StartPosition = FormStartPosition.CenterParent;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ShowFileListing.ShowFileListing(List<string>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void ShowFileListing_Load(object sender, EventArgs e)
        {
            try
            {
                if ((fileNames != null) && (fileNames.Count > 0))
                {
                    foreach (string filename in this.fileNames)
                    {
                        fileNamesRadListControl.Items.Add(filename);
                    }
                }
                else
                {
                    string errorMessage = "Error in ShowFileListing.ShowFileListing_Load(object, EventArgs)\nNo files sent as inputs";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ShowFileListing.ShowFileListing_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void closeRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, htmlFontType, htmlStandardFontSize, "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, htmlFontType, htmlStandardFontSize, "");

                showFileListingTitleText = LanguageConversion.GetStringAssociatedWithTag("ShowFileListingShowFileListingTitleText", showFileListingTitleText, "", "", "");
                closeRadButtonText = LanguageConversion.GetStringAssociatedWithTag("ShowFileListingCloseRadButtonText", closeRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ShowFileListing.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = showFileListingTitleText;
                this.closeRadButton.Text = closeRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ShowFileListing.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
