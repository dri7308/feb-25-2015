﻿namespace DatabaseFileInteraction
{
    partial class CopyDatabaseData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyDatabaseData));
            this.copyDataRunningRadWaitingBar = new Telerik.WinControls.UI.RadWaitingBar();
            this.monitorNumberValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitorNumberRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataTransferTypeValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitorNameValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentNameValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantNameValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyNameValueRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.dataTransferTypeRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.monitorNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.equipmentNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.plantNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.companyNameRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.copyingInformationFromRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.cancelRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.copyDataRunningRadWaitingBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNumberValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNumberRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferTypeValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameValueRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferTypeRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyingInformationFromRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // copyDataRunningRadWaitingBar
            // 
            this.copyDataRunningRadWaitingBar.Location = new System.Drawing.Point(49, 195);
            this.copyDataRunningRadWaitingBar.Name = "copyDataRunningRadWaitingBar";
            this.copyDataRunningRadWaitingBar.Size = new System.Drawing.Size(221, 23);
            this.copyDataRunningRadWaitingBar.TabIndex = 119;
            this.copyDataRunningRadWaitingBar.ThemeName = "Office2007Black";
            this.copyDataRunningRadWaitingBar.WaitingIndicatorSize = new System.Drawing.Size(50, 30);
            this.copyDataRunningRadWaitingBar.WaitingSpeed = 45;
            this.copyDataRunningRadWaitingBar.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.Dash;
            // 
            // monitorNumberValueRadLabel
            // 
            this.monitorNumberValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorNumberValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitorNumberValueRadLabel.Location = new System.Drawing.Point(123, 48);
            this.monitorNumberValueRadLabel.Name = "monitorNumberValueRadLabel";
            this.monitorNumberValueRadLabel.Size = new System.Drawing.Size(34, 16);
            this.monitorNumberValueRadLabel.TabIndex = 118;
            this.monitorNumberValueRadLabel.Text = "0 of 0";
            // 
            // monitorNumberRadLabel
            // 
            this.monitorNumberRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorNumberRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitorNumberRadLabel.Location = new System.Drawing.Point(49, 48);
            this.monitorNumberRadLabel.Name = "monitorNumberRadLabel";
            this.monitorNumberRadLabel.Size = new System.Drawing.Size(68, 16);
            this.monitorNumberRadLabel.TabIndex = 117;
            this.monitorNumberRadLabel.Text = "Monitor No.:";
            // 
            // dataTransferTypeValueRadLabel
            // 
            this.dataTransferTypeValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTransferTypeValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataTransferTypeValueRadLabel.Location = new System.Drawing.Point(123, 156);
            this.dataTransferTypeValueRadLabel.Name = "dataTransferTypeValueRadLabel";
            this.dataTransferTypeValueRadLabel.Size = new System.Drawing.Size(33, 16);
            this.dataTransferTypeValueRadLabel.TabIndex = 113;
            this.dataTransferTypeValueRadLabel.Text = "None";
            // 
            // monitorNameValueRadLabel
            // 
            this.monitorNameValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorNameValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitorNameValueRadLabel.Location = new System.Drawing.Point(123, 134);
            this.monitorNameValueRadLabel.Name = "monitorNameValueRadLabel";
            this.monitorNameValueRadLabel.Size = new System.Drawing.Size(33, 16);
            this.monitorNameValueRadLabel.TabIndex = 114;
            this.monitorNameValueRadLabel.Text = "None";
            // 
            // equipmentNameValueRadLabel
            // 
            this.equipmentNameValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentNameValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentNameValueRadLabel.Location = new System.Drawing.Point(123, 114);
            this.equipmentNameValueRadLabel.Name = "equipmentNameValueRadLabel";
            this.equipmentNameValueRadLabel.Size = new System.Drawing.Size(33, 16);
            this.equipmentNameValueRadLabel.TabIndex = 115;
            this.equipmentNameValueRadLabel.Text = "None";
            // 
            // plantNameValueRadLabel
            // 
            this.plantNameValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantNameValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantNameValueRadLabel.Location = new System.Drawing.Point(123, 92);
            this.plantNameValueRadLabel.Name = "plantNameValueRadLabel";
            this.plantNameValueRadLabel.Size = new System.Drawing.Size(33, 16);
            this.plantNameValueRadLabel.TabIndex = 112;
            this.plantNameValueRadLabel.Text = "None";
            // 
            // companyNameValueRadLabel
            // 
            this.companyNameValueRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameValueRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyNameValueRadLabel.Location = new System.Drawing.Point(123, 70);
            this.companyNameValueRadLabel.Name = "companyNameValueRadLabel";
            this.companyNameValueRadLabel.Size = new System.Drawing.Size(33, 16);
            this.companyNameValueRadLabel.TabIndex = 116;
            this.companyNameValueRadLabel.Text = "None";
            // 
            // dataTransferTypeRadLabel
            // 
            this.dataTransferTypeRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataTransferTypeRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataTransferTypeRadLabel.Location = new System.Drawing.Point(49, 156);
            this.dataTransferTypeRadLabel.Name = "dataTransferTypeRadLabel";
            this.dataTransferTypeRadLabel.Size = new System.Drawing.Size(35, 16);
            this.dataTransferTypeRadLabel.TabIndex = 108;
            this.dataTransferTypeRadLabel.Text = "Type:";
            // 
            // monitorNameRadLabel
            // 
            this.monitorNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitorNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitorNameRadLabel.Location = new System.Drawing.Point(49, 134);
            this.monitorNameRadLabel.Name = "monitorNameRadLabel";
            this.monitorNameRadLabel.Size = new System.Drawing.Size(47, 16);
            this.monitorNameRadLabel.TabIndex = 111;
            this.monitorNameRadLabel.Text = "Monitor:";
            // 
            // equipmentNameRadLabel
            // 
            this.equipmentNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equipmentNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.equipmentNameRadLabel.Location = new System.Drawing.Point(49, 114);
            this.equipmentNameRadLabel.Name = "equipmentNameRadLabel";
            this.equipmentNameRadLabel.Size = new System.Drawing.Size(64, 16);
            this.equipmentNameRadLabel.TabIndex = 110;
            this.equipmentNameRadLabel.Text = "Equipment:";
            // 
            // plantNameRadLabel
            // 
            this.plantNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plantNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.plantNameRadLabel.Location = new System.Drawing.Point(49, 92);
            this.plantNameRadLabel.Name = "plantNameRadLabel";
            this.plantNameRadLabel.Size = new System.Drawing.Size(35, 16);
            this.plantNameRadLabel.TabIndex = 109;
            this.plantNameRadLabel.Text = "Plant:";
            // 
            // companyNameRadLabel
            // 
            this.companyNameRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.companyNameRadLabel.Location = new System.Drawing.Point(49, 70);
            this.companyNameRadLabel.Name = "companyNameRadLabel";
            this.companyNameRadLabel.Size = new System.Drawing.Size(58, 16);
            this.companyNameRadLabel.TabIndex = 107;
            this.companyNameRadLabel.Text = "Company:";
            // 
            // copyingInformationFromRadLabel
            // 
            this.copyingInformationFromRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyingInformationFromRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.copyingInformationFromRadLabel.Location = new System.Drawing.Point(69, 26);
            this.copyingInformationFromRadLabel.Name = "copyingInformationFromRadLabel";
            this.copyingInformationFromRadLabel.Size = new System.Drawing.Size(133, 16);
            this.copyingInformationFromRadLabel.TabIndex = 106;
            this.copyingInformationFromRadLabel.Text = "Copying information from";
            // 
            // cancelRadButton
            // 
            this.cancelRadButton.Location = new System.Drawing.Point(49, 241);
            this.cancelRadButton.Name = "cancelRadButton";
            this.cancelRadButton.Size = new System.Drawing.Size(221, 24);
            this.cancelRadButton.TabIndex = 120;
            this.cancelRadButton.Text = "Cancel";
            this.cancelRadButton.ThemeName = "Office2007Black";
            this.cancelRadButton.Click += new System.EventHandler(this.cancelRadButton_Click);
            // 
            // CopyDatabaseData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(308, 296);
            this.Controls.Add(this.cancelRadButton);
            this.Controls.Add(this.copyDataRunningRadWaitingBar);
            this.Controls.Add(this.monitorNumberValueRadLabel);
            this.Controls.Add(this.monitorNumberRadLabel);
            this.Controls.Add(this.dataTransferTypeValueRadLabel);
            this.Controls.Add(this.monitorNameValueRadLabel);
            this.Controls.Add(this.equipmentNameValueRadLabel);
            this.Controls.Add(this.plantNameValueRadLabel);
            this.Controls.Add(this.companyNameValueRadLabel);
            this.Controls.Add(this.dataTransferTypeRadLabel);
            this.Controls.Add(this.monitorNameRadLabel);
            this.Controls.Add(this.equipmentNameRadLabel);
            this.Controls.Add(this.plantNameRadLabel);
            this.Controls.Add(this.companyNameRadLabel);
            this.Controls.Add(this.copyingInformationFromRadLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CopyDatabaseData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Copy Database Data";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.CopyDatabaseData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.copyDataRunningRadWaitingBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNumberValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNumberRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferTypeValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameValueRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTransferTypeRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monitorNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyNameRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copyingInformationFromRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadWaitingBar copyDataRunningRadWaitingBar;
        private Telerik.WinControls.UI.RadLabel monitorNumberValueRadLabel;
        private Telerik.WinControls.UI.RadLabel monitorNumberRadLabel;
        private Telerik.WinControls.UI.RadLabel dataTransferTypeValueRadLabel;
        private Telerik.WinControls.UI.RadLabel monitorNameValueRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentNameValueRadLabel;
        private Telerik.WinControls.UI.RadLabel plantNameValueRadLabel;
        private Telerik.WinControls.UI.RadLabel companyNameValueRadLabel;
        private Telerik.WinControls.UI.RadLabel dataTransferTypeRadLabel;
        private Telerik.WinControls.UI.RadLabel monitorNameRadLabel;
        private Telerik.WinControls.UI.RadLabel equipmentNameRadLabel;
        private Telerik.WinControls.UI.RadLabel plantNameRadLabel;
        private Telerik.WinControls.UI.RadLabel companyNameRadLabel;
        private Telerik.WinControls.UI.RadLabel copyingInformationFromRadLabel;
        private Telerik.WinControls.UI.RadButton cancelRadButton;
    }
}
