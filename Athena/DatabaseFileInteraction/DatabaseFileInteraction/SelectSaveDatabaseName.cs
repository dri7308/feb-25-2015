﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

using GeneralUtilities;

namespace DatabaseFileInteraction
{
    public partial class SelectSaveDatabaseName : Telerik.WinControls.UI.RadForm
    {
        private string databaseName;
        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
        }

        private bool useSelectedName = false;
        public bool UseSelectedName
        {
            get
            {
                return useSelectedName;
            }
        }

        List<string> existingDatabasesList;

        string initialDirectory;
        string initialDatabaseName;
        bool promptInCaseOfOverwrite;

        private static string noDatabaseFilesPresentText = "No database files present";
        private static string overwriteExistingDatabaseQuestionText = "Overwrite existing database with that name?";


        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                noDatabaseFilesPresentText = LanguageConversion.GetStringAssociatedWithTag("SelectSaveDatabaseNameNoDatabaseFilesPresentText", noDatabaseFilesPresentText, "", "", "");
                overwriteExistingDatabaseQuestionText = LanguageConversion.GetStringAssociatedWithTag("SelectSaveDatabaseNameOverwriteExistingDatabaseQuestionText", overwriteExistingDatabaseQuestionText, "", "", "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SelectSaveDatabaseName.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }       

        public SelectSaveDatabaseName(string argInitialDirectory, string argInitialDatabaseName, bool argPromptInCaseOfOverwrite)
        {
            InitializeComponent();

            this.initialDirectory = argInitialDirectory;
            this.initialDatabaseName = argInitialDatabaseName;
            this.promptInCaseOfOverwrite = argPromptInCaseOfOverwrite;

            this.existingDatabasesList = new List<string>();
        }

        private void SelectSaveDatabaseName_Load(object sender, EventArgs e)
        {
            string[] directoryListing = Directory.GetFiles(this.initialDirectory);

            if ((directoryListing != null) && (directoryListing.Length > 0))
            {
                foreach (string fileName in directoryListing)
                {
                    if (FileUtilities.GetFileExtension(fileName).Trim().ToLower().CompareTo("mdf") == 0)
                    {
                        existingDatabasesList.Add(Path.GetFileNameWithoutExtension(fileName));
                    }
                }
                foreach (string fileName in this.existingDatabasesList)
                {
                    this.existingDatabasesRadListControl.Items.Add(fileName);
                }
            }
            else
            {
                existingDatabasesRadListControl.Items.Add(noDatabaseFilesPresentText);
            }

            this.selectedDatabaseNameRadTextBox.Text = this.initialDatabaseName;
        }

        private void existingDatabasesRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            int selectedIndex = existingDatabasesRadListControl.SelectedIndex;
            if ((selectedIndex > -1) && (this.existingDatabasesList.Count > selectedIndex))
            {
                this.selectedDatabaseNameRadTextBox.Text = this.existingDatabasesList[selectedIndex];
            }
        }

        private void useSelectedNameRadButton_Click(object sender, EventArgs e)
        {
            this.useSelectedName = true;
            this.databaseName = Path.GetFileNameWithoutExtension(this.selectedDatabaseNameRadTextBox.Text.Trim());
            if (this.existingDatabasesList.Contains(this.databaseName))
            {
                if (RadMessageBox.Show(this, overwriteExistingDatabaseQuestionText, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.databaseName += ".mdf";
                    this.Close();
                }
            }
            else
            {
                this.databaseName += ".mdf";
                this.Close();
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            this.useSelectedName = false;
            this.databaseName = string.Empty;
            this.Close();
        }   
    }
}
