namespace DatabaseFileInteraction
{
    partial class UpdateDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateDatabase));
            this.upgradeDatabaseTitleRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.someInformationMayBeLostRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.saveCurrentDatabaseRadCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.updateDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.databaseBeingUpdatedRadTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.selectSourceDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            this.sourceDatabaseRadGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.sourceDatabaseWillBeReplacedRadLabel = new Telerik.WinControls.UI.RadLabel();
            this.viewUpdateErrorLogRadButton = new Telerik.WinControls.UI.RadButton();
            this.recoverDatabaseRadButton = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.upgradeDatabaseTitleRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.someInformationMayBeLostRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentDatabaseRadCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBeingUpdatedRadTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectSourceDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseRadGroupBox)).BeginInit();
            this.sourceDatabaseRadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseWillBeReplacedRadLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewUpdateErrorLogRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recoverDatabaseRadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // upgradeDatabaseTitleRadLabel
            // 
            this.upgradeDatabaseTitleRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upgradeDatabaseTitleRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.upgradeDatabaseTitleRadLabel.Location = new System.Drawing.Point(32, 12);
            this.upgradeDatabaseTitleRadLabel.Name = "upgradeDatabaseTitleRadLabel";
            this.upgradeDatabaseTitleRadLabel.Size = new System.Drawing.Size(637, 21);
            this.upgradeDatabaseTitleRadLabel.TabIndex = 34;
            this.upgradeDatabaseTitleRadLabel.Text = "<html>Upgrade your database to one that is compatable with the latest version of " +
    "the program</html>";
            this.upgradeDatabaseTitleRadLabel.ThemeName = "Office2007Black";
            // 
            // someInformationMayBeLostRadLabel
            // 
            this.someInformationMayBeLostRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.someInformationMayBeLostRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.someInformationMayBeLostRadLabel.Location = new System.Drawing.Point(63, 48);
            this.someInformationMayBeLostRadLabel.Name = "someInformationMayBeLostRadLabel";
            this.someInformationMayBeLostRadLabel.Size = new System.Drawing.Size(545, 15);
            this.someInformationMayBeLostRadLabel.TabIndex = 35;
            this.someInformationMayBeLostRadLabel.Text = "<html>Note that only the core setup and data can be transferred reliably, some or" +
    " all preferences data may be lost</html>";
            this.someInformationMayBeLostRadLabel.ThemeName = "Office2007Black";
            // 
            // saveCurrentDatabaseRadCheckBox
            // 
            this.saveCurrentDatabaseRadCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.saveCurrentDatabaseRadCheckBox.Enabled = false;
            this.saveCurrentDatabaseRadCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveCurrentDatabaseRadCheckBox.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveCurrentDatabaseRadCheckBox.Location = new System.Drawing.Point(222, 180);
            this.saveCurrentDatabaseRadCheckBox.Name = "saveCurrentDatabaseRadCheckBox";
            this.saveCurrentDatabaseRadCheckBox.Size = new System.Drawing.Size(240, 16);
            this.saveCurrentDatabaseRadCheckBox.TabIndex = 60;
            this.saveCurrentDatabaseRadCheckBox.Text = "Save a backup copy of the source database";
            this.saveCurrentDatabaseRadCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // updateDatabaseRadButton
            // 
            this.updateDatabaseRadButton.Location = new System.Drawing.Point(158, 245);
            this.updateDatabaseRadButton.Name = "updateDatabaseRadButton";
            this.updateDatabaseRadButton.Size = new System.Drawing.Size(180, 24);
            this.updateDatabaseRadButton.TabIndex = 61;
            this.updateDatabaseRadButton.Text = "Update Database";
            this.updateDatabaseRadButton.ThemeName = "Office2007Black";
            this.updateDatabaseRadButton.Click += new System.EventHandler(this.updateDatabaseRadButton_Click);
            // 
            // databaseBeingUpdatedRadTextBox
            // 
            this.databaseBeingUpdatedRadTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.databaseBeingUpdatedRadTextBox.Location = new System.Drawing.Point(13, 23);
            this.databaseBeingUpdatedRadTextBox.Name = "databaseBeingUpdatedRadTextBox";
            this.databaseBeingUpdatedRadTextBox.ReadOnly = true;
            this.databaseBeingUpdatedRadTextBox.Size = new System.Drawing.Size(227, 18);
            this.databaseBeingUpdatedRadTextBox.TabIndex = 63;
            this.databaseBeingUpdatedRadTextBox.TabStop = false;
            // 
            // selectSourceDatabaseRadButton
            // 
            this.selectSourceDatabaseRadButton.Location = new System.Drawing.Point(13, 47);
            this.selectSourceDatabaseRadButton.Name = "selectSourceDatabaseRadButton";
            this.selectSourceDatabaseRadButton.Size = new System.Drawing.Size(225, 24);
            this.selectSourceDatabaseRadButton.TabIndex = 62;
            this.selectSourceDatabaseRadButton.Text = "Select Database";
            this.selectSourceDatabaseRadButton.ThemeName = "Office2007Black";
            this.selectSourceDatabaseRadButton.Click += new System.EventHandler(this.selectNewDatabaseRadButton_Click);
            // 
            // sourceDatabaseRadGroupBox
            // 
            this.sourceDatabaseRadGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.sourceDatabaseRadGroupBox.Controls.Add(this.databaseBeingUpdatedRadTextBox);
            this.sourceDatabaseRadGroupBox.Controls.Add(this.selectSourceDatabaseRadButton);
            this.sourceDatabaseRadGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceDatabaseRadGroupBox.FooterImageIndex = -1;
            this.sourceDatabaseRadGroupBox.FooterImageKey = "";
            this.sourceDatabaseRadGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sourceDatabaseRadGroupBox.HeaderAlignment = Telerik.WinControls.UI.HeaderAlignment.Center;
            this.sourceDatabaseRadGroupBox.HeaderImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sourceDatabaseRadGroupBox.HeaderImageIndex = -1;
            this.sourceDatabaseRadGroupBox.HeaderImageKey = "";
            this.sourceDatabaseRadGroupBox.HeaderMargin = new System.Windows.Forms.Padding(0);
            this.sourceDatabaseRadGroupBox.HeaderText = "Source Database";
            this.sourceDatabaseRadGroupBox.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sourceDatabaseRadGroupBox.Location = new System.Drawing.Point(222, 90);
            this.sourceDatabaseRadGroupBox.Name = "sourceDatabaseRadGroupBox";
            this.sourceDatabaseRadGroupBox.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.sourceDatabaseRadGroupBox.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.sourceDatabaseRadGroupBox.Size = new System.Drawing.Size(251, 84);
            this.sourceDatabaseRadGroupBox.TabIndex = 64;
            this.sourceDatabaseRadGroupBox.Text = "Source Database";
            this.sourceDatabaseRadGroupBox.ThemeName = "Office2007Black";
            // 
            // sourceDatabaseWillBeReplacedRadLabel
            // 
            this.sourceDatabaseWillBeReplacedRadLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sourceDatabaseWillBeReplacedRadLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.sourceDatabaseWillBeReplacedRadLabel.Location = new System.Drawing.Point(63, 69);
            this.sourceDatabaseWillBeReplacedRadLabel.Name = "sourceDatabaseWillBeReplacedRadLabel";
            this.sourceDatabaseWillBeReplacedRadLabel.Size = new System.Drawing.Size(397, 15);
            this.sourceDatabaseWillBeReplacedRadLabel.TabIndex = 36;
            this.sourceDatabaseWillBeReplacedRadLabel.Text = "<html>The source database will be replaced by a new database with the same name</" +
    "html>";
            this.sourceDatabaseWillBeReplacedRadLabel.ThemeName = "Office2007Black";
            // 
            // viewUpdateErrorLogRadButton
            // 
            this.viewUpdateErrorLogRadButton.Location = new System.Drawing.Point(344, 245);
            this.viewUpdateErrorLogRadButton.Name = "viewUpdateErrorLogRadButton";
            this.viewUpdateErrorLogRadButton.Size = new System.Drawing.Size(180, 24);
            this.viewUpdateErrorLogRadButton.TabIndex = 103;
            this.viewUpdateErrorLogRadButton.Text = "View Update Error Log";
            this.viewUpdateErrorLogRadButton.ThemeName = "Office2007Black";
            this.viewUpdateErrorLogRadButton.Click += new System.EventHandler(this.viewExportErrorsRadButton_Click);
            // 
            // recoverDatabaseRadButton
            // 
            this.recoverDatabaseRadButton.Location = new System.Drawing.Point(344, 275);
            this.recoverDatabaseRadButton.Name = "recoverDatabaseRadButton";
            this.recoverDatabaseRadButton.Size = new System.Drawing.Size(180, 24);
            this.recoverDatabaseRadButton.TabIndex = 104;
            this.recoverDatabaseRadButton.Text = "Recover Old Database";
            this.recoverDatabaseRadButton.ThemeName = "Office2007Black";
            this.recoverDatabaseRadButton.Click += new System.EventHandler(this.recoverDatabaseRadButton_Click);
            // 
            // UpdateDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(83)))), ((int)(((byte)(83)))));
            this.ClientSize = new System.Drawing.Size(694, 313);
            this.Controls.Add(this.recoverDatabaseRadButton);
            this.Controls.Add(this.viewUpdateErrorLogRadButton);
            this.Controls.Add(this.sourceDatabaseWillBeReplacedRadLabel);
            this.Controls.Add(this.sourceDatabaseRadGroupBox);
            this.Controls.Add(this.updateDatabaseRadButton);
            this.Controls.Add(this.saveCurrentDatabaseRadCheckBox);
            this.Controls.Add(this.someInformationMayBeLostRadLabel);
            this.Controls.Add(this.upgradeDatabaseTitleRadLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UpdateDatabase";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Upgrade Database";
            this.ThemeName = "Office2007Black";
            this.Load += new System.EventHandler(this.UpdateDatabase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.upgradeDatabaseTitleRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.someInformationMayBeLostRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveCurrentDatabaseRadCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBeingUpdatedRadTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectSourceDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseRadGroupBox)).EndInit();
            this.sourceDatabaseRadGroupBox.ResumeLayout(false);
            this.sourceDatabaseRadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sourceDatabaseWillBeReplacedRadLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.viewUpdateErrorLogRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recoverDatabaseRadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel upgradeDatabaseTitleRadLabel;
        private Telerik.WinControls.UI.RadLabel someInformationMayBeLostRadLabel;
        private Telerik.WinControls.UI.RadCheckBox saveCurrentDatabaseRadCheckBox;
        private Telerik.WinControls.UI.RadButton updateDatabaseRadButton;
        private Telerik.WinControls.UI.RadTextBox databaseBeingUpdatedRadTextBox;
        private Telerik.WinControls.UI.RadButton selectSourceDatabaseRadButton;
        private Telerik.WinControls.UI.RadGroupBox sourceDatabaseRadGroupBox;
        private Telerik.WinControls.UI.RadLabel sourceDatabaseWillBeReplacedRadLabel;
        private Telerik.WinControls.UI.RadButton viewUpdateErrorLogRadButton;
        private Telerik.WinControls.UI.RadButton recoverDatabaseRadButton;
    }
}

