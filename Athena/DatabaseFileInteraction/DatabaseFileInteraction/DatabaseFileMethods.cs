﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Threading;
using Telerik.WinControls;
using Telerik.WinControls.UI;

using DatabaseInterface;
using GeneralUtilities;

namespace DatabaseFileInteraction
{    
    public class DatabaseFileMethods
    {
        //private static string initialDatabaseMovedText = "The source database has been moved or deleted from the program directory.";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string newDatabaseAlreadyExistsText = "New database already exists.";
        private static string couldNotCreateNewDatabaseInUsersAppDirectoryText = "Could not create a new initial database in the user's application directory.";
        private static string databaseAlreadyExistsText = "A database with that name already exists.";
        private static string couldNotFindDefaultEmptyDatabaseText = "<html><font=Microsoft Sans Serif><size=8.25>Cannot find default empty datbase to copy.<br>It has been moved or deleted.<br>You may have to re-install Athena.</font></html>";

        private static string templateDatabaseIsMissingItsLogFileText = "The template database is missing its log file (DbName_log.ldf)";
        private static string templateDatabaseCouldNotBeFoundText = "The template database could not be found";
        private static string templateDatabaseNotDeletedText = "The existing template database needs to be deleted before importing a new one";

        //private static string nonStandardPathChosenForNewDatabaseWarningText = "The path chosen for the new database is non-standard. This may cause problems.  Continue?";        

//        public static bool DefaultEmptyDatabaseExists(string executablePath, string emptyDatabaseFolderName, string defaultDatabaseName, string defaultDatabaseLogfileName)
//        {
//            bool exists = false;
//            try
//            {
//                string databasePath = Path.Combine(executablePath, emptyDatabaseFolderName);
//                string fullDatabasePath = Path.Combine(databasePath, defaultDatabaseName);
//                string fullDatabaseLogfilePath = Path.Combine(databasePath, defaultDatabaseLogfileName);

//                exists = File.Exists(fullDatabasePath);
//                if (exists)
//                {
//                    exists = File.Exists(fullDatabaseLogfilePath);
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DatabaseFileMethods.DefaultEmptyDatabaseExists()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return exists;
//        }

        public static ErrorCode DatabaseExists(string argDatabaseNameWithFullPath)
        {
            ErrorCode exists = ErrorCode.None;
            try
            {
                string logFileName;
                string logFileNameWithFullPath;
                bool mainFileExists;
                bool logFileExists;

                logFileName = GetDatabaseLogfileNameFromDatabaseName(argDatabaseNameWithFullPath);
                if ((logFileName != null) && (logFileName.Length > 0))
                {
                    logFileNameWithFullPath = Path.Combine(Path.GetDirectoryName(argDatabaseNameWithFullPath), logFileName);
                    if ((logFileNameWithFullPath != null) && (logFileNameWithFullPath.Length > 0))
                    {
                        mainFileExists = File.Exists(argDatabaseNameWithFullPath);
                        logFileExists = File.Exists(logFileNameWithFullPath);

                        if (mainFileExists && logFileExists)
                        {
                            exists = ErrorCode.DatabaseFound;
                        }
                        else if (mainFileExists && (!logFileExists))
                        {
                            exists = ErrorCode.DatabaseLogfileMissing;
                        }
                        else if ((!mainFileExists) && (logFileExists))
                        {
                            exists = ErrorCode.DatabaseMainfileMissing;
                        }
                        else
                        {
                            exists = ErrorCode.DatabaseNotFound;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DatabaseFileMethods.DatabaseExists(string)\nFailed to create the logFileNameWithFullPath";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DatabaseFileMethods.SourceDatabaseExists(string)\nFailed to get the logFileName from the function GetDatabaseLogfileNameFromDatabaseName()";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.SourceDatabaseExists(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return exists;
        }

        public static string GetDatabaseLogfileNameFromDatabaseName(string argDatabaseName)
        {
            string logFileName = string.Empty;
            try
            {
                string databaseNameWithoutExtension = Path.GetFileNameWithoutExtension(argDatabaseName);
                if ((databaseNameWithoutExtension != null) && (databaseNameWithoutExtension.Length > 0))
                {
                    logFileName = databaseNameWithoutExtension + "_log.ldf";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.GetDatabaseLogfileNameFromDatabaseName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return logFileName;
        }

        public static ErrorCode CopyDatabase(Telerik.WinControls.UI.RadForm parentWindow, string sourceDbNameWithFullPath, string destinationDbNameWithFullPath)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                string sourceDatabasePath = Path.GetDirectoryName(sourceDbNameWithFullPath);
                string sourceDbLogfileNameWithFullPath = Path.Combine(sourceDatabasePath, GetDatabaseLogfileNameFromDatabaseName(sourceDbNameWithFullPath));

                string destinationLogFileName = GetDatabaseLogfileNameFromDatabaseName(destinationDbNameWithFullPath);
                string destinationDbLocation = Path.GetDirectoryName(destinationDbNameWithFullPath);
                string destinationLogFileNameWithFullPath = Path.Combine(destinationDbLocation, destinationLogFileName);

                if (DatabaseExists(sourceDbNameWithFullPath) == ErrorCode.DatabaseFound)
                {
                     FileUtilities.MakeFullPathToDirectory(destinationDbLocation);
                    if (DatabaseExists(destinationLogFileNameWithFullPath) == ErrorCode.DatabaseNotFound)
                    {
                        try
                        {
                            File.Copy(sourceDbNameWithFullPath, destinationDbNameWithFullPath);
                            File.Copy(sourceDbLogfileNameWithFullPath, destinationLogFileNameWithFullPath);
                        }
                        catch (Exception ex)
                        {
                            string errorMessage = "Error in DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(string, string)\nFailed to copy the database files to the new location\nMessage: " + ex.Message;
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                        if (DatabaseExists(destinationDbNameWithFullPath) == ErrorCode.DatabaseFound)
                        {
                            errorCode = ErrorCode.DatabaseCopySucceeded;
                        }
                        else
                        {
                            errorCode = ErrorCode.DatabaseCopyFailed;
                            string errorMessage = "Error in DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(string, string)\nFailed to copy the database files to the new location";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.DatabaseFound;
                    }
                }
                else
                {
                    errorCode = ErrorCode.DatabaseNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.CopyDatabase(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static bool CopyDefaultEmptyDbToNewLocation(Telerik.WinControls.UI.RadForm parentWindow, string sourceDbNameWithFullPath, string newDbNameWithFullPath)
        {
            bool success = false;
            try
            {
                ErrorCode errorCode;

                errorCode = CopyDatabase(parentWindow, sourceDbNameWithFullPath, newDbNameWithFullPath);
                if (errorCode == ErrorCode.DatabaseCopySucceeded)
                {
                    success = true;
                }
                else
                {
                    if (errorCode == ErrorCode.DatabaseNotFound)
                    {
                        RadMessageBox.Show(parentWindow, couldNotFindDefaultEmptyDatabaseText);
                    }
                    else if (errorCode == ErrorCode.DatabaseFound)
                    {
                        RadMessageBox.Show(parentWindow, newDatabaseAlreadyExistsText);
                    }
                    else if (errorCode == ErrorCode.DatabaseCopyFailed)
                    {
                        RadMessageBox.Show(parentWindow, couldNotCreateNewDatabaseInUsersAppDirectoryText);
                    }
                    else
                    {
                        RadMessageBox.Show(parentWindow, ErrorCodeDisplay.GetErrorCodeErrorMessage(errorCode));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.CopyDefaultEmptyDbToNewLocationTelerik.WinControls.UI.RadForm, (string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool ImportTemplateDatabase(Telerik.WinControls.UI.RadForm parentWindow, string sourceDbNameWithFullPath, string destinationPath, bool overwriteExistingTemplateDb)
        {
            bool importSucceeded = false;
            try
            {
                ErrorCode errorCode = ErrorCode.None;
                string templateDbNameWithFullPath = Path.Combine(destinationPath, ProgramStrings.DefaultTemplateDatabaseName);

                errorCode = DatabaseExists(sourceDbNameWithFullPath);
                if (errorCode == ErrorCode.DatabaseFound)
                {
                    if ((DatabaseExists(templateDbNameWithFullPath) == ErrorCode.DatabaseFound) && (overwriteExistingTemplateDb))
                    {
                        errorCode = DeleteDatabase(templateDbNameWithFullPath);
                    }
                    if ((errorCode == ErrorCode.DatabaseDeleteSucceeded) || (errorCode == ErrorCode.None))
                    {
                        errorCode = CopyDatabase(parentWindow, sourceDbNameWithFullPath, templateDbNameWithFullPath);
                        if (errorCode == ErrorCode.DatabaseCopySucceeded)
                        {
                            RadMessageBox.Show(parentWindow, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DatabaseImportSucceeded));
                        }
                        else if (errorCode == ErrorCode.DatabaseNotFound)
                        {
                            RadMessageBox.Show(parentWindow, templateDatabaseCouldNotBeFoundText);
                        }
                        else if (errorCode == ErrorCode.DatabaseFound)
                        {
                            RadMessageBox.Show(parentWindow, templateDatabaseNotDeletedText);
                        }
                        else if (errorCode == ErrorCode.DatabaseCopyFailed)
                        {
                            RadMessageBox.Show(parentWindow, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.DatabaseImportFailed));
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DatabaseFileMethods.ImportTemplateDatabase(Telerik.WinControls.UI.RadForm parentWindow, string, string)\nFailed to delete existing template database";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    if (errorCode == ErrorCode.DatabaseNotFound)
                    {
                        RadMessageBox.Show(parentWindow, templateDatabaseCouldNotBeFoundText);
                    }
                    else if (errorCode == ErrorCode.DatabaseLogfileMissing)
                    {
                        RadMessageBox.Show(parentWindow, templateDatabaseIsMissingItsLogFileText);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.ImportTemplateDatabase(Telerik.WinControls.UI.RadForm parentWindow, string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return importSucceeded;
        }


//        public static bool CopyDefaultEmptyDbToNewLocation(Telerik.WinControls.UI.RadForm parentWindow, string newDbName, string newDbLocation, string executablePath, string emptyDatabaseFolderName, string defaultDatabaseName, string defaultDatabaseLogfileName)
//        {
//            bool success = false;
//            try
//            {
//                string sourceDbNameWithFullPath = Path.Combine(executablePath, emptyDatabaseFolderName, defaultDatabaseName);
//                string newDbNameWithFullPath = Path.Combine(newDbLocation, newDbName);

//                success = CopyDefaultEmptyDbToNewLocation(parentWindow, sourceDbNameWithFullPath, newDbNameWithFullPath);
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in DatabaseFileMethods.CopyDefaultEmptyDbToNewLocation(string, string)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return success;
//        }

        public static string CreateInitialDatabaseAndConnection(Telerik.WinControls.UI.RadForm parentWindow, string argEmptyProgramDatabaseNameWithFullPath, string argProgramApplicationDataPath)
        {
            string newDatabaseConnectionString = string.Empty;
            try
            {
                // copy the database to the user's app directory
                string emptyProgramDatabaseName = Path.GetFileName(argEmptyProgramDatabaseNameWithFullPath);
                string userDatabaseFileNameWithFullPath = Path.Combine(argProgramApplicationDataPath, emptyProgramDatabaseName);
         
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();

                if(DatabaseExists(argEmptyProgramDatabaseNameWithFullPath) == ErrorCode.DatabaseFound)
                {
                    if(DatabaseExists(userDatabaseFileNameWithFullPath)== ErrorCode.DatabaseNotFound)
                    {
                        if(CopyDefaultEmptyDbToNewLocation(parentWindow, argEmptyProgramDatabaseNameWithFullPath, userDatabaseFileNameWithFullPath))
                        {
                            newDatabaseConnectionString = CreateStandardConnectionStringToLocalDatabaseInstance(userDatabaseFileNameWithFullPath);
                        }
                        else
                        {
                            RadMessageBox.Show(parentWindow, couldNotCreateNewDatabaseInUsersAppDirectoryText);
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(parentWindow, databaseAlreadyExistsText);
                    }
                }
                else
                {
                    RadMessageBox.Show(parentWindow, couldNotFindDefaultEmptyDatabaseText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainDisplay.CreateInitialDatabaseAndConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return newDatabaseConnectionString;
        }

        public static string CreateStandardConnectionStringToLocalDatabaseInstance(string argDatabaseNameWithFullPath, string serverName, string serverInstance)
        {
            string connectionString = string.Empty;
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();

                connectionStringBuilder.AttachDBFilename = argDatabaseNameWithFullPath;
                connectionStringBuilder.DataSource = Path.Combine(serverName, serverInstance);
                connectionStringBuilder.UserInstance = true;
                connectionStringBuilder.IntegratedSecurity = true;

                connectionString = connectionStringBuilder.ConnectionString;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.CreateStandardConnectionStringToLocalDatabaseInstance(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return connectionString;
        }

        public static string CreateStandardConnectionStringToLocalDatabaseInstance(string argDatabaseNameWithFullPath)
        {
            string connectionString = string.Empty;
            try
            {
                string serverName = System.Environment.MachineName;
                string serverInstance = "SQLEXPRESS";

                connectionString = CreateStandardConnectionStringToLocalDatabaseInstance(argDatabaseNameWithFullPath, serverName, serverInstance);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.CreateStandardConnectionStringToLocalDatabaseInstance(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return connectionString;
        }

        public static ErrorCode StealthDeleteDatabase(string argDatabaseNameWithFullPath)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                string logFileNameWithFullPath;
                string deletedFileNameWithFullPath;
                string deletedLogfileNameWithFullPath;
                ErrorCode returnErrorCode;
                string deletedFileName = Guid.NewGuid().ToString();
                string deletedLogfileName = deletedFileName + "_log.ldf";
                deletedFileName = deletedFileName + ".mdf";

                deletedFileNameWithFullPath = Path.Combine(System.IO.Path.GetTempPath(), deletedFileName);
                deletedLogfileNameWithFullPath = Path.Combine(System.IO.Path.GetTempPath(), deletedLogfileName);

                returnErrorCode = DatabaseExists(argDatabaseNameWithFullPath);
                if (returnErrorCode != ErrorCode.DatabaseNotFound)
                {
                    if ((returnErrorCode == ErrorCode.DatabaseFound) || (returnErrorCode == ErrorCode.DatabaseLogfileMissing))
                    {
                        File.Move(argDatabaseNameWithFullPath, deletedFileNameWithFullPath);
                    }
                    if ((returnErrorCode == ErrorCode.DatabaseFound) || (returnErrorCode == ErrorCode.DatabaseMainfileMissing))
                    {
                        logFileNameWithFullPath = Path.Combine(Path.GetDirectoryName(argDatabaseNameWithFullPath), GetDatabaseLogfileNameFromDatabaseName(argDatabaseNameWithFullPath));
                        File.Move(logFileNameWithFullPath, deletedLogfileNameWithFullPath);
                    }
                    if (DatabaseExists(argDatabaseNameWithFullPath) == ErrorCode.DatabaseNotFound)
                    {
                        errorCode = ErrorCode.DatabaseDeleteSucceeded;
                    }
                    else
                    {
                        errorCode = ErrorCode.DatabaseDeleteFailed;
                    }
                }
                else
                {
                    errorCode = ErrorCode.DatabaseNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.DeleteDatabase(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode DeleteDatabase(string argDatabaseNameWithFullPath)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                string logFileNameWithFullPath;
                ErrorCode returnErrorCode;

                returnErrorCode = DatabaseExists(argDatabaseNameWithFullPath);
                if (returnErrorCode != ErrorCode.DatabaseNotFound)
                {
                    if ((returnErrorCode == ErrorCode.DatabaseFound) || (returnErrorCode == ErrorCode.DatabaseLogfileMissing))
                    {
                        File.Delete(argDatabaseNameWithFullPath);
                    }
                    if ((returnErrorCode == ErrorCode.DatabaseFound) || (returnErrorCode == ErrorCode.DatabaseMainfileMissing))
                    {
                        logFileNameWithFullPath = Path.Combine(Path.GetDirectoryName(argDatabaseNameWithFullPath), GetDatabaseLogfileNameFromDatabaseName(argDatabaseNameWithFullPath));
                        File.Delete(logFileNameWithFullPath);
                    }
                    if (DatabaseExists(argDatabaseNameWithFullPath) == ErrorCode.DatabaseNotFound)
                    {
                        errorCode = ErrorCode.DatabaseDeleteSucceeded;
                    }
                    else
                    {
                        errorCode = ErrorCode.DatabaseDeleteFailed;
                    }
                }
                else
                {
                    errorCode = ErrorCode.DatabaseNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.DeleteDatabase(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static void ClearLinqDatabaseConnectionsForOneDatabase(string dbConnectionString)
        {
            using (SqlConnection dbConnection = new SqlConnection(dbConnectionString))
            {
                if (dbConnection != null)
                {
                    SqlConnection.ClearPool(dbConnection);
                    Thread.Sleep(400);
                }
                if (dbConnection != null)
                {
                    dbConnection.Close();
                }
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
               

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, htmlFontType, htmlStandardFontSize, "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, htmlFontType, htmlStandardFontSize, "");

                newDatabaseAlreadyExistsText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsNewDatabaseAlreadyExistsText", newDatabaseAlreadyExistsText, htmlFontType, htmlStandardFontSize, "");
                couldNotCreateNewDatabaseInUsersAppDirectoryText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsCouldNotCreateNewDatabaseInUsersAppDirectoryText", couldNotCreateNewDatabaseInUsersAppDirectoryText, htmlFontType, htmlStandardFontSize, "");
                databaseAlreadyExistsText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsDatabaseAlreadyExistsText", databaseAlreadyExistsText, htmlFontType, htmlStandardFontSize, "");
                couldNotFindDefaultEmptyDatabaseText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsCouldNotFindDefaultEmptyDatabaseText", couldNotFindDefaultEmptyDatabaseText, htmlFontType, htmlStandardFontSize, "");
                templateDatabaseIsMissingItsLogFileText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsTemplateDatabaseIsMissingItsLogFileText", templateDatabaseIsMissingItsLogFileText, htmlFontType, htmlStandardFontSize, "");
                templateDatabaseCouldNotBeFoundText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsTemplateDatabaseCouldNotBeFoundText", templateDatabaseCouldNotBeFoundText, htmlFontType, htmlStandardFontSize, "");
                templateDatabaseNotDeletedText = LanguageConversion.GetStringAssociatedWithTag("DatabaseFileMethodsTemplateDatabaseNotDeletedText", templateDatabaseNotDeletedText, htmlFontType, htmlStandardFontSize, "");

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static bool DatabaseConnectionStringIsCorrect(string connectionString)
        {
            bool success = false;
            try
            {
                using (MonitorInterfaceDB testDb = new MonitorInterfaceDB(connectionString))
                {
                    success = General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDb);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.TestDatabaseConnectionString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static string GetDataSourceFromDatabaseConnectionString(string dbConnectionString)
        {
            string dataSource = string.Empty;
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(dbConnectionString);
                if (builder != null)
                {
                    dataSource = builder.DataSource;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.GetDataSourceFromDatabaseConnectionString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataSource;
        }

        public static string GetServerNameFromDataSourceString(string dataSource)
        {
            string serverName = string.Empty;
            try
            {
                string[] pieces = dataSource.Split('\\');
                if (pieces.Length == 2)
                {
                    serverName = pieces[0];
                }
                else
                {
                    string errorMessage = "Error in DatabaseFileMethods.GetServerNameFromDataSourceString(string)\nInput dataSource had the wrong format";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.GetServerNameFromDataSourceString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return serverName;
        }

        public static string GetServerInstanceFromDataSourceString(string dataSource)
        {
            string serverInstance = string.Empty;
            try
            {
                string[] pieces = dataSource.Split('\\');
                if (pieces.Length == 2)
                {
                    serverInstance = pieces[1];
                }
                else
                {
                    string errorMessage = "Error in DatabaseFileMethods.GetServerInstanceFromDataSourceString(string)\nInput dataSource had the wrong format";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseFileMethods.GetServerInstanceFromDataSourceString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return serverInstance;
        }

    }
}
