using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer.Management.Smo;
using GeneralUtilities;
using DatabaseInterface;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.ServiceProcess;


namespace DatabaseFileInteraction
{
    public partial class SetDatabaseConnection : Telerik.WinControls.UI.RadForm
    {
        private static string htmlPrefix = "<html>";
        private static string htmlSuffis = "</html>";
        private static string htmlFontType = "<font=Microsoft Sans Serif>";
        private static string htmlStandardFontSize = "<size=8.25>";

        private static string databaseConnectionAppearsToFunctionProperlyText = "Database connection appears to function properly";
        private static string databaseConnectionFailedText = "Database connection failed.";
        private static string noServerInstanceDefinedText = "No server instance defined, will set it to SQLExpress";
        private static string sqlExpressServerNameText = "SQLExpress";

        private static string notSpecifiedText = "Not specified";

        private static string setDatabaseConnectionTitleText = "Set Database Connection";

        private static string getServersRadGroupBoxText = "File Type";
        private static string localServersRadRadioButtonText = "File Based";
        private static string networkServersRadRadioButtonText = "Server Based";
        private static string allServersRadRadioButtonText = "All Servers";
        private static string getServersRadButtonText = "Get Servers";
        private static string availableServersRadGroupBoxText = "Available SQL Servers";
        private static string serverNameRadLabelText = "Server Name";
        private static string serverInstanceRadLabelText = "Server Instance";
        private static string databaseNameRadLabelText = "Database Name";
        private static string databaseLocationRadLabelText = "Database Location";
        private static string authenticationRadLabelText = "Authentication";
        private static string userNameRadLabelText = "User Name";
        private static string passwordRadLabelText = "Password";
        private static string selectDatabaseRadButtonText = "Select Database";
        private static string testConnectionRadButtonText = "Test Connection";
        private static string saveAsDefaultConnectionRadButtonText = "Save as Default Connection";
        private static string cancelRadButtonText = "Cancel Changes";

        private string applicationDataPath;

        private bool saveConnectionStringAsDefault;

        public bool SaveConnectionStringAsDefault
        {
            get
            {
                return saveConnectionStringAsDefault;
            }
        }

        private string connectionString;

        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
        }

        private string databaseName;

        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
        }

        DataTable availableServers;
        Int32[] availableServersUsedIndex;

        public SetDatabaseConnection()
        {
            InitializeComponent();
        }

        public SetDatabaseConnection(string currentConnectionString, string inputApplicationDataPath, bool placeHolder)
        {
            try
            {
                InitializeComponent();
                AssignStringValuesToInterfaceObjects();
                this.StartPosition = FormStartPosition.CenterParent;
                if (currentConnectionString != null)
                {
                    SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(currentConnectionString);

                    SetServerNameAndInstance(connectionStringBuilder);

                    SetDatabaseNameAndLocation(connectionStringBuilder.AttachDBFilename);

                    SetNameAndPassword(connectionStringBuilder);

                    this.connectionString = currentConnectionString;
                    this.applicationDataPath = inputApplicationDataPath;
                    /// placeHolder is only there to keep the signature separate from the other constructor
                }
                else
                {
                    string errorMessage = "Error in SetDatabaseConnection.SetDatabaseConnection(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetDatabaseConnection(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public SetDatabaseConnection(string currentConnectionString, string newDatabase)
        {
            try
            {
                InitializeComponent();
                AssignStringValuesToInterfaceObjects();
                if (currentConnectionString != null)
                {
                    if (newDatabase != null)
                    {
                        SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(currentConnectionString);

                        SetServerNameAndInstance(connectionStringBuilder);

                        if ((newDatabase != null) && (newDatabase != string.Empty))
                        {
                            SetDatabaseNameAndLocation(newDatabase);
                        }
                        else
                        {
                            SetDatabaseNameAndLocation(connectionStringBuilder.AttachDBFilename);
                        }

                        SetNameAndPassword(connectionStringBuilder);

                        this.connectionString = currentConnectionString;
                    }
                    else
                    {
                        string errorMessage = "Error in SetDatabaseConnection.SetDatabaseConnection(string, string)\nSecond string input was null.";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in SetDatabaseConnection.SetDatabaseConnection(string, string)\nFirst string input was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetDatabaseConnection(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetServerNameAndInstance(SqlConnectionStringBuilder connectionStringBuilder)
        {
            try
            {
                if (connectionStringBuilder != null)
                {
                    string fullDataSource = connectionStringBuilder.DataSource;
                    string[] pieces = fullDataSource.Split('\\');
                    string serverName = string.Empty;
                    string serverInstance = string.Empty;

                    if (pieces.Length == 2)
                    {
                        serverName = pieces[0];
                        if (serverName.CompareTo(".") == 0)
                        {
                            serverName = System.Environment.MachineName;
                        }
                        serverInstance = pieces[1];
                    }
                    else
                    {
                        serverName = notSpecifiedText;
                        serverInstance = notSpecifiedText;
                    }

                    this.serverNameRadTextBox.Text = serverName;
                    this.serverInstanceRadTextBox.Text = serverInstance;
                }
                else
                {
                    string errorMessage = "Error in SetDatabaseConnection.SetDatabaseConnection(SqlConnectionStringBuilder)\nInput SqlConnectionStringBuilder was null.";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetDatabaseConnection(string, string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetDatabaseNameAndLocation(string fullPath)
        {
            try
            {
                string fileName = notSpecifiedText;
                string filePath = notSpecifiedText;
                if ((fullPath != null) && (fullPath != string.Empty))
                {
                    try
                    {
                        fileName = Path.GetFileName(fullPath);
                        filePath = Path.GetDirectoryName(fullPath);
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    #if DEBUG
                    fileName = "MonitorInterfaceDB.mdf";
                    filePath = Path.GetDirectoryName(Application.ExecutablePath);
                    #else
                    string[] filesInDirectory = Directory.GetFiles(this.applicationDataPath, "*.mdf");
                    if ((filesInDirectory != null) && (filesInDirectory.Length > 0))
                    {
                        fileName = Path.GetFileName(filesInDirectory[0]);
                    }
                    filePath = this.applicationDataPath;
                    #endif
                }
                this.databaseNameRadTextBox.Text = fileName;
                this.databaseLocationRadTextBox.Text = filePath;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetDatabaseNameAndLocation(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetNameAndPassword(SqlConnectionStringBuilder connectionStringBuilder)
        {
            try
            {
                this.usernameRadTextBox.Text = connectionStringBuilder.UserID;
                this.passwordRadTextBox.Text = connectionStringBuilder.Password;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetNameAndPassword(SqlConnectionStringBuilder)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void SetDatabaseConnection_Load(object sender, EventArgs e)
        {
            try
            { // decrypt file

               // System.Diagnostics.Debugger.Break();
                this.authenticationRadDropDownList.DataSource = new string[] { "Windows Authentication", "SQL Server Authentication" };
                this.authenticationRadDropDownList.SelectedIndex = 0;
                // read stored connections string
                string file = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup3.dll";

                StreamReader reader = new StreamReader(file);
                
                string strToDecrypt = string.Empty;
                strToDecrypt = reader.ReadLine();
                reader.Close();
                string decrypt = FileUtilities.DecryptStringFromBytes_Aes(strToDecrypt);

                string[] line = decrypt.Split(',');
                localServersRadRadioButton.IsChecked = Convert.ToBoolean(line[0]);
                networkServersRadRadioButton.IsChecked = Convert.ToBoolean(line[1]);

                serverNameRadTextBox.Text = line[2];
                serverInstanceRadTextBox.Text = line[3];
                if (line[4] == "0")
                {
                    authenticationRadDropDownList.SelectedIndex = 0;
                }
                else
                {
                    authenticationRadDropDownList.SelectedIndex = 1;
                }

                usernameRadTextBox.Text = line[5];
                passwordRadTextBox.Text = line[6];
                databaseNameRadTextBox.Text = line[7];
                databaseLocationRadTextBox.Text = line[8];
                // localServersRadRadioButton.Enabled = false;
                // networkServersRadRadioButton.Enabled = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.SetDatabaseConnection_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void getServersRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                
                Cursor.Current = Cursors.WaitCursor;

                // see if sql browser is in run state.  If not start it
                ServiceController oController = new ServiceController("SQL Server Browser");
                if (oController.Status.Equals(ServiceControllerStatus.Stopped) || oController.Status.Equals(ServiceControllerStatus.Paused))
                    oController.Start();

               
                bool localOrNot = false;
                if (localServersRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    localOrNot = true;
                }
               this.availableServers = SmoApplication.EnumAvailableSqlServers(false);
                

                

                this.availableServersRadListControl.Items.Clear();
                this.availableServersUsedIndex = new int[this.availableServers.Rows.Count];

                string displayString;
                int serverUsedIndex = 0;
                int serverAvailableIndex = 0;
                foreach (DataRow dr in this.availableServers.Rows)
                {
                    displayString = dr["Name"].ToString();

                    /// will change this later when filtering servers, for now we display all
                    /// available servers
                    availableServersUsedIndex[serverUsedIndex] = serverAvailableIndex;
                    serverUsedIndex++;
                    serverAvailableIndex++;

                    displayString = displayString + "    Is Local Server = " + dr["IsLocal"];

                    //if(dr["IsLocal"] == "True")
                    //{
                    //    displayString = displayString + "    Local Server";
                    //}
                    //else
                    //{
                    //    displayString = displayString + "    Remote Server";
                    //}

                    this.availableServersRadListControl.Items.Add(displayString);
                    Cursor.Current = Cursors.Default;
                    //listBox1.Items.Add("server = " + dr["Server"]);
                    //listBox1.Items.Add("Instance = " + dr["Instance"]);
                    //listBox1.Items.Add("Version = " + dr["Version"]);
                    //listBox1.Items.Add("IsLocal = " + dr["IsLocal"]);
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                string errorMessage = "Exception thrown in SetDatabaseConnection.getServersRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private string CreateConnectionString()
        {
            string newConnectionString = string.Empty;
            try
            {
                if (this.localServersRadRadioButton.IsChecked)
                {
                    SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();

                    connectionStringBuilder.DataSource = Path.Combine(this.serverNameRadTextBox.Text, this.serverInstanceRadTextBox.Text);
                    connectionStringBuilder.AttachDBFilename = Path.Combine(this.databaseLocationRadTextBox.Text, this.databaseNameRadTextBox.Text);

                    connectionStringBuilder.IntegratedSecurity = true;
                    connectionStringBuilder.UserInstance = true;
                    newConnectionString = connectionStringBuilder.ConnectionString;
                }
                else
                {
                    SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
                    connectionStringBuilder.DataSource = serverNameRadTextBox.Text;
                    connectionStringBuilder.InitialCatalog = databaseNameRadTextBox.Text;
                    connectionStringBuilder.UserID = usernameRadTextBox.Text;
                    connectionStringBuilder.Password = passwordRadTextBox.Text;
                    newConnectionString = connectionStringBuilder.ConnectionString;
                    databaseLocationRadTextBox.Text = "";
                    //newConnectionString = "Data Source=" + serverNameRadTextBox.Text + "; Initial Catalog=" + databaseNameRadTextBox.Text + ";User ID = " + usernameRadTextBox.Text + ";Password=" + passwordRadTextBox.Text;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.CreateConnectionString()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return newConnectionString;
        }

        private void selectDatabaseRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                //check to see if username and password are required
                if (this.authenticationRadDropDownList.SelectedIndex == 1)
                {
                    if (usernameRadTextBox.Text == "" || passwordRadTextBox.Text == "")
                    {
                        MessageBox.Show("there must be a valid User Name and Password");
                        return;
                    }
                }
                if (this.localServersRadRadioButton.IsChecked)
                {
                    string filePathWithFileName;
                    
                    filePathWithFileName = FileUtilities.GetFileNameWithFullPath(this.databaseLocationRadTextBox.Text, "mdf");

                    if ((filePathWithFileName != null) && (filePathWithFileName.Length > 0))
                    {
                        this.databaseLocationRadTextBox.Text = Path.GetDirectoryName(filePathWithFileName);
                        this.databaseNameRadTextBox.Text = Path.GetFileName(filePathWithFileName);
                    }
                }
                else
                {
                    using (var con = new SqlConnection("Data Source=" + serverNameRadTextBox.Text + ";User ID = " + usernameRadTextBox.Text + ";Password=" + passwordRadTextBox.Text))
                    {
                        con.Open();
                        DataTable databases = con.GetSchema("Databases");
                        foreach (DataRow database in databases.Rows)
                        {
                            String databaseName = database.Field<String>("database_name");
                            getDbNames.Items.Add(databaseName);
                        }
                        this.getDbNames.SelectedIndex = 0;
                        databaseLocationRadTextBox.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.selectDatabaseRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void testConnectionRadButton_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (this.authenticationRadDropDownList.SelectedIndex == 1)
                {
                    if (usernameRadTextBox.Text == "" || passwordRadTextBox.Text == "")
                    {
                        MessageBox.Show("there must be a valid User Name and Password");
                        return;
                    }
                }
                Cursor.Current = Cursors.WaitCursor;
                
                string currentConnectionString = CreateConnectionString();
                using (MonitorInterfaceDB testDbConnection = new MonitorInterfaceDB(currentConnectionString))
                {
                    if (General_DatabaseMethods.DatabaseConnectionStringIsCorrect(testDbConnection))
                    {
                        RadMessageBox.Show(this, databaseConnectionAppearsToFunctionProperlyText);
                    }
                    else
                    {
                        RadMessageBox.Show(this, databaseConnectionFailedText);
                    }
                }

                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.testConnectionRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                Cursor.Current = Cursors.Default;
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        //        private void useConnectionForThisSessionRadButton_Click(object sender, EventArgs e)
        //        {
        //            try
        //            {
        //                this.connectionString = CreateConnectionString();
        //                this.saveConnectionStringAsDefault = false;
        //                this.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in SetDatabaseConnection.useConnectionForThisSessionRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void saveAsDefaultConnectionRadButton_Click(object sender, EventArgs e)
        {
            try
            {
               // string file = applicationDataPath + "\\connection.txt";
                string sFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Dynamic Ratings\\Athena\\setup3.dll";
              
                string[] line = new string[9];
                string strToEncrypt;
               
                line[0] = this.localServersRadRadioButton.IsChecked.ToString();
                line[1] = this.networkServersRadRadioButton.IsChecked.ToString();
                line[2] = serverNameRadTextBox.Text;
                line[3] = serverInstanceRadTextBox.Text;
                line[4] = authenticationRadDropDownList.SelectedIndex.ToString();
                line[5] = usernameRadTextBox.Text;
                line[6] = passwordRadTextBox.Text;
                line[7] = databaseNameRadTextBox.Text;
                line[8] = databaseLocationRadTextBox.Text;
               
                // now join them
                strToEncrypt = string.Join(",", line);
               // file = applicationDataPath + "\\connection.txt";
                
                string encrypted = FileUtilities.EncryptStringToBytes_Aes(strToEncrypt);
                using (StreamWriter writer = new StreamWriter(sFolder))
                {
                    writer.WriteLine(encrypted);
                    writer.Close();
                }
                
                //  using (StreamWriter writer = new StreamWriter(file))
                // {
                //     writer.WriteLine(
                //    writer.WriteLine();
                //    writer.WriteLine(this.);
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.WriteLine();
                //    writer.Close();
                // }
                this.connectionString = CreateConnectionString();
                this.databaseName = databaseNameRadTextBox.Text.Trim();
                this.saveConnectionStringAsDefault = true;
                this.Close();
            }
            catch (Exception
            ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.saveAsDefaultConnectionRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.connectionString = string.Empty;
                this.databaseName = string.Empty;
                this.saveConnectionStringAsDefault = false;
                this.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.cancelRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void availableServersRadListControl_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (this.availableServers != null)
                {
                    if (this.availableServers.Rows.Count > 0)
                    {
                        int selectedIndex = availableServersRadListControl.SelectedIndex;
                        if (selectedIndex > -1)
                        {
                            int availableServerRowNumber = this.availableServersUsedIndex[selectedIndex];
                            this.serverNameRadTextBox.Text = this.availableServers.Rows[availableServerRowNumber]["Server"].ToString();

                            string serverInstance = this.availableServers.Rows[availableServerRowNumber]["Instance"].ToString();
                            if (serverInstance == string.Empty)
                            {
                                //RadMessageBox.Show(this, noServerInstanceDefinedText);
                                serverInstance = "MSSQLSERVER";
                            }
                            this.serverInstanceRadTextBox.Text = serverInstance;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.availableServersRadListControl_MouseClick(object, MouseEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void availableServersRadListControl_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = availableServersRadListControl.SelectedIndex;

                if (selectedIndex > -1)
                {
                    int availableServerRowNumber = this.availableServersUsedIndex[selectedIndex];
                    this.serverNameRadTextBox.Text = this.availableServers.Rows[availableServerRowNumber]["Server"].ToString();

                    string serverInstance = this.availableServers.Rows[availableServerRowNumber]["Instance"].ToString();
                    if (serverInstance == string.Empty)
                    {
                        // RadMessageBox.Show(this, noServerInstanceDefinedText);
                        serverInstance = "MSSQLSERVER";
                    }
                    this.serverInstanceRadTextBox.Text = serverInstance;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.availableServersRadListControl_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public void AssignStringValuesToInterfaceObjects()
        {
            try
            {
                this.Text = setDatabaseConnectionTitleText;

                getServersRadGroupBox.Text = getServersRadGroupBoxText;
                localServersRadRadioButton.Text = localServersRadRadioButtonText;
                networkServersRadRadioButton.Text = networkServersRadRadioButtonText;
                allServersRadRadioButton.Text = allServersRadRadioButtonText;
                getServersRadButton.Text = getServersRadButtonText;
                availableServersRadGroupBox.Text = availableServersRadGroupBoxText;
                serverNameRadLabel.Text = serverNameRadLabelText;
                serverInstanceRadLabel.Text = serverInstanceRadLabelText;
                databaseNameRadLabel.Text = databaseNameRadLabelText;
                databaseLocationRadLabel.Text = databaseLocationRadLabelText;
                authenticationRadLabel.Text = authenticationRadLabelText;
                userNameRadLabel.Text = userNameRadLabelText;
                passwordRadLabel.Text = passwordRadLabelText;
                selectDatabaseRadButton.Text = selectDatabaseRadButtonText;
                testConnectionRadButton.Text = testConnectionRadButtonText;
                saveAsDefaultConnectionRadButton.Text = saveAsDefaultConnectionRadButtonText;
                cancelRadButton.Text = cancelRadButtonText;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        public static void AssignValuesToInternalStaticStrings()
        {
            try
            {
                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

                databaseConnectionAppearsToFunctionProperlyText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionDatabaseConnectionAppearsToFunctionProperlyText", databaseConnectionAppearsToFunctionProperlyText, htmlFontType, htmlStandardFontSize, "");
                databaseConnectionFailedText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionDatabaseConnectionFailedText", databaseConnectionFailedText, htmlFontType, htmlStandardFontSize, "");
                noServerInstanceDefinedText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionNoServerInstanceDefinedText", noServerInstanceDefinedText, htmlFontType, htmlStandardFontSize, "");
                sqlExpressServerNameText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionSqlExpressServerNameText", sqlExpressServerNameText, htmlFontType, htmlStandardFontSize, "");
                notSpecifiedText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionNotSpecifiedText", notSpecifiedText, htmlFontType, htmlStandardFontSize, "");

                setDatabaseConnectionTitleText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceTitleText", setDatabaseConnectionTitleText, "", "", "");
                getServersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceGetServersRadGroupBoxText", getServersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                localServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceGetServersLocalServersRadRadioButtonText", localServersRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                networkServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceGetServersNetworkServersRadRadioButtonText", networkServersRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                allServersRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceGetServersAllServersRadRadioButtonText", allServersRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
                getServersRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceGetServersRadButtonText", getServersRadButtonText, htmlFontType, htmlStandardFontSize, "");
                availableServersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceAvailableServersRadGroupBoxText", availableServersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
                serverNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceServerNameRadLabelText", serverNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                serverInstanceRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceServerInstanceRadLabelText", serverInstanceRadLabelText, htmlFontType, htmlStandardFontSize, "");
                databaseNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceDatabaseNameRadLabelText", databaseNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                databaseLocationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceDatabaseLocationRadLabelText", databaseLocationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                authenticationRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceAuthenticationRadLabelText", authenticationRadLabelText, htmlFontType, htmlStandardFontSize, "");
                userNameRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceUserNameRadLabelText", userNameRadLabelText, htmlFontType, htmlStandardFontSize, "");
                passwordRadLabelText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfacePasswordRadLabelText", passwordRadLabelText, htmlFontType, htmlStandardFontSize, "");
                selectDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceSelectDatabaseRadButtonText", selectDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
                testConnectionRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceTestConnectionRadButtonText", testConnectionRadButtonText, htmlFontType, htmlStandardFontSize, "");
                saveAsDefaultConnectionRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceSaveAsDefaultConnectionRadButtonText", saveAsDefaultConnectionRadButtonText, htmlFontType, htmlStandardFontSize, "");
                cancelRadButtonText = LanguageConversion.GetStringAssociatedWithTag("SetDatabaseConnectionInterfaceCancelRadButtonText", cancelRadButtonText, htmlFontType, htmlStandardFontSize, "");
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in SetDatabaseConnection.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void authenticationRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (this.authenticationRadDropDownList.SelectedIndex == 0)
            {
                usernameRadTextBox.Text = "";
                passwordRadTextBox.Text = "";
            }
        }

        private void getDbNames_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            databaseNameRadTextBox.Text = getDbNames.SelectedText;
        }

        private void networkServersRadRadioButton_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
        }

        private void getServersRadGroupBox_Click(object sender, EventArgs e)
        {
        }
    }
}