﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

using Telerik.WinControls;
using Telerik.WinControls.UI;

using GeneralUtilities;

namespace DatabaseFileInteraction
{
    public partial class CreateNewDatabase : Telerik.WinControls.UI.RadForm
    {
        private static string noDatabaseFilesFoundText = "No database files found in selected directory";
        private static string directoryDoesNotExistText = "Selected directory does not exist";
        private static string notSpecifiedText = "Not specified";
        private static string aDatabaseWithThatNameAlreadyExistsText = "A database with that name already exists";

        private string sourceDirectoryNameWithFullPath;
        private string sourceDirectoryName;

        public CreateNewDatabase(string argSourceDirectoryNameWithFullPath)
        {
            InitializeComponent();
            this.sourceDirectoryNameWithFullPath = argSourceDirectoryNameWithFullPath;
            if (File.Exists(this.sourceDirectoryNameWithFullPath))
            {
                this.sourceDirectoryName = Path.GetFileName(this.sourceDirectoryNameWithFullPath);
            }
            else
            {
                string errorMessage = "Error in CreateNewDatabase.CreateNewDatabase()\nBad argument passed in for source database; file does not exist";
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void CreateNewDatabase_Load(object sender, EventArgs e)
        {
            this.destinationDatabaseFileNameRadTextBox.Text = this.sourceDirectoryName;
        }

        private void selectDirectoryRadButton_Click(object sender, EventArgs e)
        {
            string directoryName = string.Empty;
            directoryName = FileUtilities.GetDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            if ((directoryName != null) && (directoryName.Length > 0))
            {
                this.destinationDirectoryRadTextBox.Text = directoryName;
            }
        }

        private void createDatabaseRadButton_Click(object sender, EventArgs e)
        {
            string destinationDatabaseFileName;

            destinationDatabaseFileName = Path.Combine(this.destinationDirectoryRadTextBox.Text.Trim(), this.destinationDatabaseFileNameRadTextBox.Text.Trim());

            if (!File.Exists(destinationDatabaseFileName))
            {










            }
            else
            {
                RadMessageBox.Show(this, CreateNewDatabase.aDatabaseWithThatNameAlreadyExistsText);
            }
        }

        private void cancelRadButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void showAllDbFilesInDirectoryRadButton_Click(object sender, EventArgs e)
        {
            List<string> fileNames;

            string sourceDirectory = Path.GetDirectoryName(this.destinationDirectoryRadTextBox.Text.Trim());

            if (Directory.Exists(sourceDirectory))
            {
                fileNames = FileUtilities.GetAllFilesWithTheSpecifiedExtension(sourceDirectory, "mdf");
                if (fileNames.Count > 0)
                {
                    ShowFileListing showFileListing = new ShowFileListing(fileNames);
                    showFileListing.Show();
                }
                else
                {
                    RadMessageBox.Show(this, noDatabaseFilesFoundText);
                }
            }
            else
            {
                RadMessageBox.Show(this, directoryDoesNotExistText);
            }
        }

    }
}
