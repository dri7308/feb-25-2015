﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DatabaseInterface;
using GeneralUtilities;
using MonitorInterface;

namespace MonitorCommunication
{
    public class MonitorConnection
    {
        private static int TOTAL_RETRIES = 20;
        private static int DEFAULT_NON_SERIAL_NON_INTERACTIVE_RETRIES = 2;
        private static int DEFAULT_SERIAL_NON_INTERACTIVE_RETRIES = 1;

        private static int directConnectionUSBReadDelay = 2;
        public static int DirectConnectionUSBReadDelay
        {
            get
            {
                return directConnectionUSBReadDelay;
            }
        }

        private static int directConnectionEthernetReadDelay = 2;
        public static int DirectConnectionEthernetReadDelay
        {
            get
            {
                return directConnectionEthernetReadDelay;
            }
        }

        private static int directConnectionSerialReadDelay = 2;
        public static int DirectConnectionSerialReadDelay
        {
            get
            {
                return directConnectionSerialReadDelay;
            }
        }

        private static int viaMainConnectionUSBReadDelay = 200;
        public static int ViaMainConnectionUSBReadDelay
        {
            get
            {
                return viaMainConnectionUSBReadDelay;
            }
        }

        private static int viaMainConnectionEthernetReadDelay = 200;
        public static int ViaMainConnectionEthernetReadDelay
        {
            get
            {
                return viaMainConnectionEthernetReadDelay;
            }
        }

        private static int viaMainConnectionSerialReadDelay = 200;
        public static int ViaMainConnectionSerialReadDelay
        {
            get
            {
                return viaMainConnectionSerialReadDelay;
            }
        }

        private static int numberOfNonInteractiveRetriesToAttempt;
        public static int NumberOfNonInteractiveRetriesToAttempt
        {
            get
            {
                return numberOfNonInteractiveRetriesToAttempt;
            }
            set
            {
                if (value < 1) value = 1;
                totalNumberOfRetriesToAttempt = value;
            }
        }

        private static int totalNumberOfRetriesToAttempt = TOTAL_RETRIES;
        public static int TotalNumberOfRetriesToAttempt
        {
            get
            {
                return totalNumberOfRetriesToAttempt;
            }
            set
            {
                if (value < numberOfNonInteractiveRetriesToAttempt) value = numberOfNonInteractiveRetriesToAttempt;
                totalNumberOfRetriesToAttempt = value;
            }
        }

        private static ConnectionType currentConnectionType;
        public static ConnectionType CurrentConnectionType
        {
            get
            {
                return currentConnectionType;
            }
        }

        public static ErrorCode OpenMonitorConnection(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.MonitorNotFound;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnection(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnection(Guid, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnection(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.MonitorWasNull;
            try
            {
                string monitorType = string.Empty;
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                Int16 portNumber = 0;
                bool noErrorsSoFar = true;
                Monitor mainMonitor = null;
                bool connectionIsViaMain = false;

                currentConnectionType = ConnectionType.None;
                numberOfNonInteractiveRetriesToAttempt = DEFAULT_NON_SERIAL_NON_INTERACTIVE_RETRIES;

                /// Why not try to close a connection if it exists?  I doubt it can hurt, since I test the connection object for null before trying to close it, and set it to null
                /// after I close it
                MonitorConnection.CloseConnection();

                if (monitor != null)
                {
                    errorCode = ErrorCode.None;
                    monitorType = monitor.MonitorType.Trim();
                    connectionType = monitor.ConnectionType.Trim();
                    if (connectionType.Length > 2)
                    {
                        /// modified the code a bit because we now carry the IP and port number on all monitors 
                        /// Can probably further modify the code after some testing
                        ipAddress = monitor.IPaddress.Trim();
                        portNumber = (Int16)monitor.PortNumber;

                        if (connectionType.CompareTo("Via Main") == 0)
                        {
                            connectionIsViaMain = true;
                            mainMonitor = General_DatabaseMethods.GetAssociatedMainMonitor(monitor.ID, localDB);
                            if (mainMonitor != null)
                            {
                                connectionType = mainMonitor.ConnectionType.Trim();
                            }
                            else
                            {
                                errorCode = ErrorCode.MainMonitorNotFound;
                                noErrorsSoFar = false;
                                string errorMessage = "Error in MonitorConnection.OpenMonitorConnection(Monitor, MonitorInterfaceDB)\nConnection was Via Main, but no Main monitor could be found in the database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        if (noErrorsSoFar)
                        {
                            /// this is here to allow for cancelling connection retries intelligently
                            DeviceCommunication.EnableConnection();

                            if (connectionType.CompareTo("USB") == 0)
                            {
                                if (connectionIsViaMain)
                                {
                                    readDelayInMicroseconds = viaMainConnectionUSBReadDelay;
                                }
                                else
                                {
                                    readDelayInMicroseconds = directConnectionUSBReadDelay;
                                }
                                if (DeviceCommunication.OpenUSBConnection(5))
                                {
                                    errorCode = ErrorCode.ConnectionOpenSucceeded;
                                    currentConnectionType = ConnectionType.USB;
                                }
                                else
                                {
                                    errorCode = ErrorCode.ConnectionOpenFailed;
                                }
                            }
                            else if (connectionType.CompareTo("SOE") == 0)
                            {
                                if (connectionIsViaMain)
                                {
                                    readDelayInMicroseconds = viaMainConnectionEthernetReadDelay;
                                }
                                else
                                {
                                    readDelayInMicroseconds = directConnectionEthernetReadDelay;
                                }
                                if (DeviceCommunication.OpenSerialOverEthernetConnection(ipAddress, portNumber, 1, 20))
                                {
                                    errorCode = ErrorCode.ConnectionOpenSucceeded;
                                    currentConnectionType = ConnectionType.SOE;
                                }
                                else
                                {
                                    errorCode = ErrorCode.ConnectionOpenFailed;
                                }
                            }
                            else if ((connectionType.CompareTo("TCP") == 0)||(connectionType.CompareTo("PCS")==0))
                            {
                                if (connectionIsViaMain)
                                {
                                    readDelayInMicroseconds = viaMainConnectionEthernetReadDelay;
                                }
                                else
                                {
                                    readDelayInMicroseconds = directConnectionEthernetReadDelay;
                                }
                                if (DeviceCommunication.OpenEthernetConnection(ipAddress, portNumber))
                                {
                                    errorCode = ErrorCode.ConnectionOpenSucceeded;
                                    currentConnectionType = ConnectionType.TCP;
                                }
                                else
                                {
                                    errorCode = ErrorCode.ConnectionOpenFailed;
                                }
                            }
                            else if (connectionType.CompareTo("Serial") == 0)
                            {
                                if ((serialPort != null) && (serialPort.Length > 0) && (baudRate > 0))
                                {
                                    if (connectionIsViaMain)
                                    {
                                        readDelayInMicroseconds = viaMainConnectionSerialReadDelay;
                                    }
                                    else
                                    {
                                        readDelayInMicroseconds = directConnectionSerialReadDelay;
                                    }
                                    if (DeviceCommunication.OpenSerialConnection(serialPort, baudRate))
                                    {
                                        errorCode = ErrorCode.ConnectionOpenSucceeded;
                                        currentConnectionType = ConnectionType.Serial;
                                        numberOfNonInteractiveRetriesToAttempt = DEFAULT_SERIAL_NON_INTERACTIVE_RETRIES;
                                    }
                                    else
                                    {
                                        errorCode = ErrorCode.ConnectionOpenFailed;
                                    }
                                }
                                else
                                {
                                    errorCode = ErrorCode.SerialPortNotSpecified;
                                    string errorMessage = "Error in MonitorConnection.OpenMonitorConnection(Monitor, int, string, ref int, MonitorInterfaceDB)\nInput serialPort was necessary, but not specified.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                DeviceCommunication.EnableDataDownload();
                            }
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.GeneralDatabaseError;
                        string errorMessage = "Error in MonitorConnection.OpenMonitorConnection(Monitor, int, string, ref int, MonitorInterfaceDB)\nError.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MonitorConnection.OpenMonitorConnection(Monitor, int, string, ref int, MonitorInterfaceDB)\nInput Monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnection(Monitor, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode ClearUsbBuffer()
        {
            ErrorCode error = ErrorCode.None;
            try
            {
                if (currentConnectionType != ConnectionType.USB)
                {
                    error = ErrorCode.NotConnectedWithUSB;
                }
                else
                {
                    if (!DeviceCommunication.OpenUSBConnection(20))
                    {
                        error = ErrorCode.ConnectionFailed;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.ClearUsbBuffer(Monitor, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return error;
        }

        private static ErrorCode GetAndResolveDeviceErrorCode(Monitor monitor, int readDelayInMicroseconds, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                long deviceError = -1;
                int modBusAddress = 0;
                string modBusAddressAsString;

                if (monitor != null)
                {
                    modBusAddressAsString = monitor.ModbusAddress.Trim();
                    if (Int32.TryParse(modBusAddressAsString, out modBusAddress))
                    {
                        if ((modBusAddress > 0) && (modBusAddress < 256))
                        {
                            if (DeviceCommunication.SelectedCommunicationProtocolIsOpen())
                            {
                                if (showRetryWindow)
                                {
                                    deviceError = InteractiveDeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveRetriesToAttempt, totalNumberOfRetriesToAttempt, parentWindowInformation);
                                }
                                else
                                {
                                    deviceError = DeviceCommunication.GetDeviceError(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveRetriesToAttempt, totalNumberOfRetriesToAttempt);
                                }
                                if (deviceError > -1)
                                {
                                    errorCode = ErrorCode.ConnectionOpenSucceeded;
                                }
                                else
                                {
                                    errorCode = ErrorCode.DeviceErrorReadFailed;
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.CommunicationClosed;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.ModbusAddressOutOfRange;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.GeneralDatabaseError;
                    }
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.GetAndResolveDeviceErrorCode(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        private static ErrorCode OpenMonitorConnectionToSpecifiedMonitorType(Monitor monitor, int expectedDeviceType, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                int deviceType=-1;
                int modBusAddress = 0;
                string modBusAddressAsString;

                if (monitor != null)
                {
                    modBusAddressAsString = monitor.ModbusAddress.Trim();
                    if (Int32.TryParse(modBusAddressAsString, out modBusAddress))
                    {
                        if ((modBusAddress > 0) && (modBusAddress < 256))
                        {
                            errorCode = OpenMonitorConnection(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                            if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                            {
                                if (showRetryWindow)
                                {
                                    deviceType = InteractiveDeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveRetriesToAttempt, totalNumberOfRetriesToAttempt, parentWindowInformation);
                                }
                                else
                                {
                                    deviceType = DeviceCommunication.GetDeviceType(modBusAddress, readDelayInMicroseconds, numberOfNonInteractiveRetriesToAttempt, totalNumberOfRetriesToAttempt);
                                }
                                if (deviceType > 0)
                                {
                                    if (deviceType == expectedDeviceType)
                                    {
                                        errorCode = ErrorCode.ConnectionOpenSucceeded;
                                    }
                                    else
                                    {
                                        if (expectedDeviceType == 101) errorCode = ErrorCode.NotConnectedToMainMonitor;
                                        else if (expectedDeviceType == 505) errorCode = ErrorCode.NotConnectedToPDM;
                                        else if (expectedDeviceType == 15002) errorCode = ErrorCode.NotConnectedToBHM;
                                        else errorCode = ErrorCode.UnknownDeviceType;
                                    }
                                }
                                else
                                {
                                    errorCode = ErrorCode.DeviceTypeReadFailed;
                                }
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.ModbusAddressOutOfRange;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.GeneralDatabaseError;
                    }

                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToSpecifiedMonitorType(Guid, int, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


        public static ErrorCode OpenMonitorConnectionToBHM(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToBHM(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToBHM(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToBHM(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToSpecifiedMonitorType(monitor, 15002, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToBHM(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToBHMWithDeviceErrorCheck(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToBHMWithDeviceErrorCheck(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToBHMWithDeviceErrorCheck(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToBHMWithDeviceErrorCheck(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToBHM(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    errorCode = GetAndResolveDeviceErrorCode(monitor, readDelayInMicroseconds, parentWindowInformation,showRetryWindow);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToBHMWithDeviceErrorCheck(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }


        public static ErrorCode OpenMonitorConnectionToMainMonitor(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToMainMonitor(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToMainMonitor(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToMainMonitor(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToSpecifiedMonitorType(monitor, 101, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToMainMonitor(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToMainMonitor(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB,parentWindowInformation, showRetryWindow);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    errorCode = GetAndResolveDeviceErrorCode(monitor, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToMainMonitorWithDeviceErrorCheck(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToPDM(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToPDM(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToPDM(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToPDM(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToSpecifiedMonitorType(monitor, 505, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToPDM(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToPDMWithDeviceErrorCheck(Guid monitorID, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                Monitor monitor = General_DatabaseMethods.GetOneMonitor(monitorID, localDB);
                if (monitor != null)
                {
                    errorCode = OpenMonitorConnectionToPDMWithDeviceErrorCheck(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                }
                else
                {
                    errorCode = ErrorCode.MonitorWasNull;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToPDMWithDeviceErrorCheck(Guid, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static ErrorCode OpenMonitorConnectionToPDMWithDeviceErrorCheck(Monitor monitor, string serialPort, int baudRate, ref int readDelayInMicroseconds, MonitorInterfaceDB localDB, ParentWindowInformation parentWindowInformation, bool showRetryWindow)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                errorCode = OpenMonitorConnectionToPDM(monitor, serialPort, baudRate, ref readDelayInMicroseconds, localDB, parentWindowInformation, showRetryWindow);
                if (errorCode == ErrorCode.ConnectionOpenSucceeded)
                {
                    errorCode = GetAndResolveDeviceErrorCode(monitor, readDelayInMicroseconds, parentWindowInformation, showRetryWindow);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnectionToPDMWithDeviceErrorCheck(Monitor, string, int, string, ref int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static void CloseConnection()
        {
            try
            {
                DeviceCommunication.CloseConnection();
                currentConnectionType = ConnectionType.None;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MonitorConnection.CloseConnection()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        public static ErrorCode OpenMonitorConnection(Monitor monitor)
        //        {
        //            ErrorCode errorCode = ErrorCode.MonitorWasNull;
        //            try
        //            {




        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in MonitorConnection.OpenMonitorConnection(Monitor)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return errorCode;
        //        }


    }

    public enum ConnectionType
    {
        Serial,
        SOE,
        TCP,
        USB,
        None
    };
}
