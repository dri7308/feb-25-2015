﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using GeneralUtilities;

namespace DatabaseInterface
{
    public class ADM_DatabaseMethods
    {      
        /// <summary>
        /// Adds one ADM_AlarmStatus entry to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void ADM_WriteToAlarmStatus(ADM_AlarmStatus AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                db.ADM_AlarmStatus.InsertOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_WriteToAlarmStatus(ADM_AlarmStatus, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Adds a list of ADM_AlarmStatus entries to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void ADM_WriteToAlarmStatus(List<ADM_AlarmStatus> AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                db.ADM_AlarmStatus.InsertAllOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_WriteToAlarmStatus(List<ADM_AlarmStatus>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Reads the ADM_AlarmStatus for a given monitor with the most recent DateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static ADM_AlarmStatus ADM_ReadLatestEntryFromAlarmStatus(Guid monitorID, MonitorInterfaceDB db)
        {
            ADM_AlarmStatus AlarmStatus = null;
            try
            {
                var admAlarmStatusQuery =
                   from item in db.ADM_AlarmStatus
                   where item.MonitorID == monitorID
                   orderby item.DeviceTime descending
                   select item;

                if (admAlarmStatusQuery.Count() > 0)
                {
                    AlarmStatus = admAlarmStatusQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_ReadLatestEntryFromAlarmStatus(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Reads all ADM_AlarmStatus entries for a given monitor and returns them as a list
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<ADM_AlarmStatus> ADM_ReadAllEntriesFromAlarmStatus(Guid monitorID, MonitorInterfaceDB db)
        {
            List<ADM_AlarmStatus> AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                        from entry in db.ADM_AlarmStatus
                        where entry.MonitorID == monitorID
                        orderby entry.DeviceTime ascending
                        select entry;

                AlarmStatus = alarmStatusQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_ReadAllEntriesFromAlarmStatus(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Deletes a single ADM_AlarmStatus entry
        /// </summary>
        /// <param name="entryID"></param>
        /// <param name="db"></param>
        public static void ADM_DeleteOneEntryFromAlarmStatus(Guid entryID, MonitorInterfaceDB db)
        {
            try
            {
                var alarmStatusQuery =
                       from entry in db.ADM_AlarmStatus
                       where entry.ID == entryID
                       select entry;

                if (alarmStatusQuery.Count() > 0)
                {
                    db.ADM_AlarmStatus.DeleteOnSubmit(alarmStatusQuery.First());
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_DeleteOneEntryFromAlarmStatus(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Deletes all ADM_AlarmStatus entries for a given monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        public static void ADM_DeleteAllFromAlarmStatus(Guid monitorID, MonitorInterfaceDB db)
        {
            try
            {
                List<ADM_AlarmStatus> AlarmStatus = ADM_ReadAllEntriesFromAlarmStatus(monitorID, db);

                db.ADM_AlarmStatus.DeleteAllOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in DatabaseInterface.ADM_DeleteAllFromAlarmStatus(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
