﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DatabaseInterface
{
    public class PDM_DatabaseMethods
    {
        #region PDM specific methods

        #region Alarm Status

        /// <summary>
        /// Writes one PDM_AlarmStatus entry to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void PDM_AlarmStatus_Write(PDM_AlarmStatus AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                if (AlarmStatus != null)
                {
                    db.PDM_AlarmStatus.InsertOnSubmit(AlarmStatus);
                    db.SubmitChanges();
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_AlarmStatus_Write(PDM_AlarmStatus, MonitorInterfaceDB)\nNull PDM_AlarmStatus passed as an argument";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_AlarmStatus_Write(PDM_AlarmStatus, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Writes a list of PDM_AlarmStatus entries to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void PDM_AlarmStatus_Write(List<PDM_AlarmStatus> AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                if ((AlarmStatus != null) && (AlarmStatus.Count > 0))
                {
                    db.PDM_AlarmStatus.InsertAllOnSubmit(AlarmStatus);
                    db.SubmitChanges();
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_AlarmStatus_Write(List<PDM_AlarmStatus>, MonitorInterfaceDB)\nNull or empty list passed as an argument";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_AlarmStatus_Write(List<PDM_AlarmStatus>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Returns all PDM_AlarmStatus entries for a given monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_AlarmStatus> PDM_AlarmStatus_GetAllEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_AlarmStatus> AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                    from entry in db.PDM_AlarmStatus
                    where entry.MonitorID == monitorID
                    orderby entry.DeviceTime descending
                    select entry;

                AlarmStatus = alarmStatusQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_AlarmStatus_GetAllEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Returns the PDM_AlarmStatus for a given monitor with the most recent DateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static PDM_AlarmStatus PDM_AlarmStatus_GetLatestEntryForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            PDM_AlarmStatus AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                    from entry in db.PDM_AlarmStatus
                    where entry.MonitorID == monitorID
                    orderby entry.DeviceTime descending
                    select entry;

                if (alarmStatusQuery.Count() > 0)
                {
                    AlarmStatus = alarmStatusQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_AlarmStatus_GetLatestEntryForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Deletes all PDM_AlarmStatus entries for a given monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        public static void PDM_AlarmStatus_DeleteAllEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            try
            {
                List<PDM_AlarmStatus> AlarmStatus = PDM_AlarmStatus_GetAllEntriesForOneMonitor(monitorID, db);

                db.PDM_AlarmStatus.DeleteAllOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_AlarmStatus_DeleteAllEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion // alarm status

        #region PDM Configuration

        #region ConfigurationRoot

        public static bool PDM_Config_WriteConfigurationRootTableEntry(PDM_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.PDM_Config_ConfigurationRoot.InsertOnSubmit(configurationRoot);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteConfigurationRootTableEntry(PDM_Config_ConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_ConfigurationRoot PDM_Config_GetConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            PDM_Config_ConfigurationRoot configurationRoot = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_ConfigurationRoot
                    where item.ID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    configurationRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetConfigurationRootTableEntry(PDM_Config_ConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRoot;
        }

        public static PDM_Config_ConfigurationRoot PDM_Config_CloneOneConfigurationRootDbObject(PDM_Config_ConfigurationRoot configurationRoot)
        {
            PDM_Config_ConfigurationRoot clonedConfigurationRoot = null;
            try
            {
                if (configurationRoot != null)
                {
                    clonedConfigurationRoot = new PDM_Config_ConfigurationRoot();

                    clonedConfigurationRoot.ID = configurationRoot.ID;
                    clonedConfigurationRoot.MonitorID = configurationRoot.MonitorID;
                    clonedConfigurationRoot.DateAdded = configurationRoot.DateAdded;
                    clonedConfigurationRoot.Description = configurationRoot.Description;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneConfigurationRootDbObject(PDM_Config_ConfigurationRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedConfigurationRoot;
        }

        public static List<PDM_Config_ConfigurationRoot> PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB db)
        {
            List<PDM_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                var queryList =
                   from item in db.PDM_Config_ConfigurationRoot
                   orderby item.DateAdded descending
                   select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static PDM_Config_ConfigurationRoot PDM_Config_GetNewestConfigurationRootTabelEntryForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            PDM_Config_ConfigurationRoot configurationRoot = null;
            try
            {
                List<PDM_Config_ConfigurationRoot> configurationRootList = PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitorID, db);
                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    configurationRoot = configurationRootList[0];
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetNewestConfigurationRootTabelEntryForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRoot;
        }

        public static List<PDM_Config_ConfigurationRoot> PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                var queryList =
                   from item in db.PDM_Config_ConfigurationRoot
                   where item.MonitorID == monitorID
                   orderby item.DateAdded descending
                   select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static List<PDM_Config_ConfigurationRoot> PDM_Config_CloneConfigurationRootDbObjectList(List<PDM_Config_ConfigurationRoot> configurationRootList)
        {
            List<PDM_Config_ConfigurationRoot> clonedConfigurationRootList = null;
            try
            {
                PDM_Config_ConfigurationRoot configurationRoot;
                if (configurationRootList != null)
                {
                    clonedConfigurationRootList = new List<PDM_Config_ConfigurationRoot>();
                    foreach (PDM_Config_ConfigurationRoot entry in configurationRootList)
                    {
                        configurationRoot = PDM_Config_CloneOneConfigurationRootDbObject(entry);
                        if (configurationRoot != null)
                        {
                            clonedConfigurationRootList.Add(configurationRoot);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneConfigurationRootDbObjectList(List<PDM_Config_ConfigurationRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedConfigurationRootList;
        }

        public static bool PDM_Config_DeleteConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_ConfigurationRoot configurationRoot;

                var queryList =
                    from item in db.PDM_Config_ConfigurationRoot
                    where item.ID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    configurationRoot = queryList.First();
                    success = PDM_Config_DeleteConfigurationRootTableEntry(configurationRoot, db);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteConfigurationRootTableEntry(PDM_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.PDM_Config_ConfigurationRoot.DeleteOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntry(PDM_Config_ConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteConfigurationRootTableEntries(List<PDM_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRootList != null)
                {
                    db.PDM_Config_ConfigurationRoot.DeleteAllOnSubmit(configurationRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteConfigurationRootTableEntries(List<PDM_Config_ConfigurationRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<PDM_Config_ConfigurationRoot> configurationRootList = PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitorID, db);
                success = PDM_Config_DeleteConfigurationRootTableEntries(configurationRootList, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Config_ConfigurationRoot destinationConfigurationRoot;
                PDM_Config_ConfigurationRoot sourceConfigurationRoot = PDM_Config_GetConfigurationRootTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceConfigurationRoot != null)
                {
                    destinationConfigurationRoot = PDM_Config_CloneOneConfigurationRootDbObject(sourceConfigurationRoot);
                    destinationConfigurationRoot.ID = destinationConfigurationRootID;
                    destinationConfigurationRoot.MonitorID = destinationMonitorID;
                    success = PDM_Config_WriteConfigurationRootTableEntry(destinationConfigurationRoot, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the PDM_Config_ConfigurationRoot to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the PDM_Config_ConfigurationRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Config_ConfigurationRoot configurationRoot = PDM_Config_GetConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the PDM_Config_ConfigurationRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        //        public static bool PDM_Config_CreateOneConfiguration(Monitor monitor, DateTime dateAdded, List<PDM_Config_ChannelInfo> channelInfoList, 
        //                                                             List<PDM_Config_MeasurementsInfo> measurementsInfoList, PDM_Config_PDSetupInfo pdSetupInfo,
        //                                                             PDM_Config_SetupInfo setupInfo, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                PDM_Config_AllIDsForOneConfiguration allIDsForOneConfiguration = new PDM_Config_AllIDsForOneConfiguration();
        //                StringBuilder channelInfoIdsString = new StringBuilder();
        //                StringBuilder measurementsInfoIDsString = new StringBuilder();

        //                allIDsForOneConfiguration.MonitorID = monitor.ID;
        //                allIDsForOneConfiguration.DateAdded = dateAdded;

        //                foreach (PDM_Config_ChannelInfo channelInfo in channelInfoList)
        //                {
        //                    channelInfoIdsString.Append(channelInfo.ID.ToString());

        //                }
        //                allIDsForOneConfiguration.ChannelInfoIDs = channelInfoIdsString.ToString();

        //                foreach (PDM_Config_MeasurementsInfo measurementsInfo in measurementsInfoList)
        //                {
        //                    measurementsInfoIDsString.Append(measurementsInfo.ToString());
        //                    measurementsInfoIDsString.Append(";");
        //                }
        //                allIDsForOneConfiguration.MeasurementsInfoIDs = measurementsInfoIDsString.ToString();

        //                allIDsForOneConfiguration.PDSetupInfoID = pdSetupInfo.ID;
        //                allIDsForOneConfiguration.SetupInfoID = setupInfo.ID;

        //                success = PDM_DatabaseMethods.PDM_Config_WriteAllIDsForOneConfigurationTableEntry(allIDsForOneConfiguration, db);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CreateOneConfiguration(Monitor, DateTime, List<PDM_Config_ChannelInfo>, List<PDM_Config_MeasurementsInfo>, PDM_Config_PDSetupInfo, PDM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        //        public static bool PDM_Config_DeleteOneConfiguration(PDM_Config_AllIDsForOneConfiguration allIDsForOneConfiguration, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                String[] channelInfoIDsAsStrings = allIDsForOneConfiguration.ChannelInfoIDs.Split(';');
        //                String[] measurementsInfoIDsAsStrings = allIDsForOneConfiguration.MeasurementsInfoIDs.Split(';');

        //                // there is a trailing ';' that must be accounted for
        //                int channelInfoIDsCount = channelInfoIDsAsStrings.Length - 1;
        //                int measurementsInfoIDsCount = measurementsInfoIDsAsStrings.Length - 1;

        //                Guid channelInfoID;
        //                for (int i = 0; i < channelInfoIDsCount; i++)
        //                {
        //                    channelInfoID = new Guid(channelInfoIDsAsStrings[i]);
        //                    PDM_DatabaseMethods.PDM_Config_DeleteChannelInfoTableEntry(channelInfoID, db);
        //                }

        //                Guid measurementsInfoID;
        //                for (int i = 0; i < measurementsInfoIDsCount; i++)
        //                {
        //                    measurementsInfoID = new Guid(measurementsInfoIDsAsStrings[i]);
        //                    PDM_DatabaseMethods.PDM_Config_DeleteMeasurementsInfoTableEntry(measurementsInfoID, db);
        //                }

        //                PDM_DatabaseMethods.PDM_Config_DeletePDSetupInfoTableEntry(allIDsForOneConfiguration.PDSetupInfoID, db);
        //                PDM_DatabaseMethods.PDM_Config_DeleteSetupInfoTableEntry(allIDsForOneConfiguration.SetupInfoID, db);

        //                PDM_DatabaseMethods.PDM_Config_DeleteAllIDsForOneConfigurationTableEntry(allIDsForOneConfiguration, db);
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteOneConfiguration(PDM_Config_AllIDsForOneConfiguration, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        #endregion

        #region ChannelInfo

        public static bool PDM_Config_WriteChannelInfoTableEntry(PDM_Config_ChannelInfo channelInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (channelInfo != null)
                {
                    db.PDM_Config_ChannelInfo.InsertOnSubmit(channelInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteChannelInfoTableEntry(PDM_Config_ChannelInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_WriteChannelInfoListTableEntry(List<PDM_Config_ChannelInfo> channelInfoList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((channelInfoList != null) && (channelInfoList.Count > 0))
                {
                    db.PDM_Config_ChannelInfo.InsertAllOnSubmit(channelInfoList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteChannelInfoListTableEntry(List<PDM_Config_ChannelInfo>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_ChannelInfo PDM_Config_GetChannelInfoTableEntry(Guid channelInfoID, MonitorInterfaceDB db)
        {
            PDM_Config_ChannelInfo channelInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_ChannelInfo
                    where item.ID == channelInfoID
                    select item;

                if (queryList.Count() > 0)
                {
                    channelInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetChannelInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfo;
        }

        public static PDM_Config_ChannelInfo PDM_Config_CloneOneChannelInfoDbObject(PDM_Config_ChannelInfo channelInfo)
        {
            PDM_Config_ChannelInfo clonedChannelInfo = null;
            try
            {
                if (channelInfo != null)
                {
                    clonedChannelInfo = new PDM_Config_ChannelInfo();

                    clonedChannelInfo.ID = channelInfo.ID;
                    clonedChannelInfo.ConfigurationRootID = channelInfo.ConfigurationRootID;
                    clonedChannelInfo.DateAdded = channelInfo.DateAdded;

                    clonedChannelInfo.ChannelNumber = channelInfo.ChannelNumber;
                    clonedChannelInfo.ChannelIsOn = channelInfo.ChannelIsOn;
                    clonedChannelInfo.ChannelSensitivity = channelInfo.ChannelSensitivity;
                    clonedChannelInfo.PDMaxWidth = channelInfo.PDMaxWidth;
                    clonedChannelInfo.PDIntervalTime = channelInfo.PDIntervalTime;
                    clonedChannelInfo.P_0 = channelInfo.P_0;
                    clonedChannelInfo.P_1 = channelInfo.P_1;
                    clonedChannelInfo.P_2 = channelInfo.P_2;
                    clonedChannelInfo.P_3 = channelInfo.P_3;
                    clonedChannelInfo.P_4 = channelInfo.P_4;
                    clonedChannelInfo.P_5 = channelInfo.P_5;
                    clonedChannelInfo.CHPhase = channelInfo.CHPhase;
                    clonedChannelInfo.CalcPDILimit = channelInfo.CalcPDILimit;
                    clonedChannelInfo.Filter = channelInfo.Filter;
                    clonedChannelInfo.NoiseOn_0 = channelInfo.NoiseOn_0;
                    clonedChannelInfo.NoiseOn_1 = channelInfo.NoiseOn_1;
                    clonedChannelInfo.NoiseOn_2 = channelInfo.NoiseOn_2;
                    clonedChannelInfo.NoiseType_0 = channelInfo.NoiseType_0;
                    clonedChannelInfo.NoiseType_1 = channelInfo.NoiseType_1;
                    clonedChannelInfo.NoiseType_2 = channelInfo.NoiseType_2;
                    clonedChannelInfo.NoiseShift_0 = channelInfo.NoiseShift_0;
                    clonedChannelInfo.NoiseShift_1 = channelInfo.NoiseShift_1;
                    clonedChannelInfo.NoiseShift_2 = channelInfo.NoiseShift_2;
                    clonedChannelInfo.MinNoiseLevel_0 = channelInfo.MinNoiseLevel_0;
                    clonedChannelInfo.MinNoiseLevel_1 = channelInfo.MinNoiseLevel_1;
                    clonedChannelInfo.MinNoiseLevel_2 = channelInfo.MinNoiseLevel_2;
                    clonedChannelInfo.TimeOfArrival_0 = channelInfo.TimeOfArrival_0;
                    clonedChannelInfo.Polarity_0 = channelInfo.Polarity_0;
                    clonedChannelInfo.Ref_0 = channelInfo.Ref_0;
                    clonedChannelInfo.RefShift_0 = channelInfo.RefShift_0;

                    clonedChannelInfo.Reserved_0 = channelInfo.Reserved_0;
                    clonedChannelInfo.Reserved_1 = channelInfo.Reserved_1;
                    clonedChannelInfo.Reserved_2 = channelInfo.Reserved_2;
                    clonedChannelInfo.Reserved_3 = channelInfo.Reserved_3;
                    clonedChannelInfo.Reserved_4 = channelInfo.Reserved_4;
                    clonedChannelInfo.Reserved_5 = channelInfo.Reserved_5;
                    clonedChannelInfo.Reserved_6 = channelInfo.Reserved_6;
                    clonedChannelInfo.Reserved_7 = channelInfo.Reserved_7;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneOneChannelInfoDbObject(PDM_Config_ChannelInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedChannelInfo;
        }

        public static List<PDM_Config_ChannelInfo> PDM_Config_CloneChannelInfoDbObjectList(List<PDM_Config_ChannelInfo> channelInfoList)
        {
            List<PDM_Config_ChannelInfo> clonedChannelInfoList = null;
            try
            {
                PDM_Config_ChannelInfo channelInfo;
                if (channelInfoList != null)
                {
                    clonedChannelInfoList = new List<PDM_Config_ChannelInfo>();
                    foreach (PDM_Config_ChannelInfo entry in channelInfoList)
                    {
                        channelInfo = PDM_Config_CloneOneChannelInfoDbObject(entry);
                        if (channelInfo != null)
                        {
                            clonedChannelInfoList.Add(channelInfo);
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CloneChannelInfoDbObjectList(List<PDM_Config_ChannelInfo>)\nFailed to clone a PDM_Config_ChannelInfo object.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CloneChannelInfoDbObjectList(List<PDM_Config_ChannelInfo>)\nInput List<PDM_Config_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneChannelInfoDbObjectList(List<PDM_Config_ChannelInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedChannelInfoList;
        }

        public static List<PDM_Config_ChannelInfo> PDM_Config_GetAllChannelInfoTableEntriesForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<PDM_Config_ChannelInfo> channelInfoList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_ChannelInfo
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.ChannelNumber ascending
                    select item;

                channelInfoList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllChannelInfoTableEntriesForOneConfigurationRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfoList;
        }

        public static List<PDM_Config_ChannelInfo> PDM_Config_GetAllChannelInfoTableEntriesForMostRecentMonitorConfiguration(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Config_ChannelInfo> channelInfoList = null;
            try
            {
                PDM_Config_ConfigurationRoot configurationRoot = PDM_Config_GetNewestConfigurationRootTabelEntryForOneMonitor(monitorID, db);
                if (configurationRoot != null)
                {
                    channelInfoList = PDM_Config_GetAllChannelInfoTableEntriesForOneConfiguration(configurationRoot.ID, db);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllChannelInfoTableEntriesForMostRecentMonitorConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfoList;
        }

//        public static List<PDM_Config_ChannelInfo> PDM_Config_GetAllChannelInfoTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
//        {
//            List<PDM_Config_ChannelInfo> channelInfoList = null;
//            try
//            {
//                //var queryList =
//                //    from item in db.PDM_Config_ChannelInfo
//                //    where item. == configurationRootID
//                //    orderby item.ChannelNumber ascending
//                //    select item;

//                //channelInfoList = queryList.ToList();
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllChannelInfoTableEntriesForOneConfigurationRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return channelInfoList;
//        }

        public static bool PDM_Config_DeleteChannelInfoTableEntry(PDM_Config_ChannelInfo channelInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (channelInfo != null)
                {
                    db.PDM_Config_ChannelInfo.DeleteOnSubmit(channelInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteChannelInfoTableEntry(PDM_Config_ChannelInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteChannelInfoTableEntry(Guid channelInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_ChannelInfo channelInfo = PDM_Config_GetChannelInfoTableEntry(channelInfoID, db);
                success = PDM_Config_DeleteChannelInfoTableEntry(channelInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteChannelInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteChannelInfoListTableEntry(List<PDM_Config_ChannelInfo> channelInfoList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((channelInfoList != null) && (channelInfoList.Count > 0))
                {
                    db.PDM_Config_ChannelInfo.DeleteAllOnSubmit(channelInfoList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteChannelInfoListTableEntry(List<PDM_Config_ChannelInfo>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<PDM_Config_ChannelInfo> destinationChannelInfoList;
                List<PDM_Config_ChannelInfo> sourceChannelInfoList = PDM_Config_GetAllChannelInfoTableEntriesForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if ((sourceChannelInfoList != null)&&(sourceChannelInfoList.Count>0))
                {
                    destinationChannelInfoList=PDM_Config_CloneChannelInfoDbObjectList(sourceChannelInfoList);
                    for (int i = 0; i < destinationChannelInfoList.Count; i++)
                    {
                        destinationChannelInfoList[i].ID = Guid.NewGuid();
                        destinationChannelInfoList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = PDM_Config_WriteChannelInfoListTableEntry(destinationChannelInfoList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<PDM_Config_ChannelInfo> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<PDM_Config_ChannelInfo> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region MeasurementsInfo

        public static bool PDM_Config_WriteMeasurementsInfoTableEntry(PDM_Config_MeasurementsInfo measurementsInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (measurementsInfo != null)
                {
                    db.PDM_Config_MeasurementsInfo.InsertOnSubmit(measurementsInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteMeasurementsInfoTableEntry(PDM_Config_MeasurementsInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_WriteMeasurementsInfoListTableEntry(List<PDM_Config_MeasurementsInfo> measurementsInfoList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((measurementsInfoList != null) && (measurementsInfoList.Count > 0))
                {
                    db.PDM_Config_MeasurementsInfo.InsertAllOnSubmit(measurementsInfoList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteMeasurementsInfoListTableEntry(List<PDM_Config_MeasurementsInfo>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_MeasurementsInfo PDM_Config_GetMeasurementsInfoTableEntry(Guid measurementsInfoID, MonitorInterfaceDB db)
        {
            PDM_Config_MeasurementsInfo measurementsInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_MeasurementsInfo
                    where item.ID == measurementsInfoID
                    select item;

                if (queryList.Count() > 0)
                {
                    measurementsInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetMeasurementsInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfo;
        }

        public static PDM_Config_MeasurementsInfo PDM_Config_CloneOneMeasurementsInfoDbObject(PDM_Config_MeasurementsInfo measurementsInfo)
        {
            PDM_Config_MeasurementsInfo clonedMeasurementsInfo = null;
            try
            {
                if (measurementsInfo != null)
                {
                    clonedMeasurementsInfo = new PDM_Config_MeasurementsInfo();

                    clonedMeasurementsInfo.ID = measurementsInfo.ID;
                    clonedMeasurementsInfo.ConfigurationRootID = measurementsInfo.ConfigurationRootID;
                    clonedMeasurementsInfo.DateAdded = measurementsInfo.DateAdded;

                    clonedMeasurementsInfo.ItemNumber = measurementsInfo.ItemNumber;
                    clonedMeasurementsInfo.Hour = measurementsInfo.Hour;
                    clonedMeasurementsInfo.Minute = measurementsInfo.Minute;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneOneMeasurementsInfoDbObject(PDM_Config_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMeasurementsInfo;
        }

        public static List<PDM_Config_MeasurementsInfo> PDM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<PDM_Config_MeasurementsInfo> measurementsInfoList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_MeasurementsInfo
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.ItemNumber ascending
                    select item;

                measurementsInfoList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetAllMeasurementsInfoTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        public static List<PDM_Config_MeasurementsInfo> PDM_Config_CloneMeasurementsInfoDbObjectList(List<PDM_Config_MeasurementsInfo> measurementsInfoList)
        {
            List<PDM_Config_MeasurementsInfo> clonedMeasurementsInfoList = null;
            try
            {
                PDM_Config_MeasurementsInfo measurementsInfo;
                if (measurementsInfoList != null)
                {
                    clonedMeasurementsInfoList = new List<PDM_Config_MeasurementsInfo>();
                    foreach (PDM_Config_MeasurementsInfo entry in measurementsInfoList)
                    {
                        measurementsInfo = PDM_Config_CloneOneMeasurementsInfoDbObject(entry);
                        if (measurementsInfo != null)
                        {
                            clonedMeasurementsInfoList.Add(measurementsInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneMeasurementsInfoDbObjectList(List<PDM_Config_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMeasurementsInfoList;
        }

        public static bool PDM_Config_DeleteMeasurementsInfoTableEntry(PDM_Config_MeasurementsInfo measurementsInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (measurementsInfo != null)
                {
                    db.PDM_Config_MeasurementsInfo.DeleteOnSubmit(measurementsInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteMeasurementsInfoTableEntry(PDM_Config_MeasurementsInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteMeasurementsInfoTableEntry(Guid measurementsInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_MeasurementsInfo measurementsInfo = PDM_DatabaseMethods.PDM_Config_GetMeasurementsInfoTableEntry(measurementsInfoID, db);
                success = PDM_DatabaseMethods.PDM_Config_DeleteMeasurementsInfoTableEntry(measurementsInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteMeasurementsInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteMeasurementsInfoListTableEntry(List<PDM_Config_MeasurementsInfo> measurementsInfoList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((measurementsInfoList != null) && (measurementsInfoList.Count > 0))
                {
                    db.PDM_Config_MeasurementsInfo.DeleteAllOnSubmit(measurementsInfoList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteMeasurementsInfoListTableEntry(List<PDM_Config_MeasurementsInfo>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<PDM_Config_MeasurementsInfo> destinationMeasurementsInfoList;
                List<PDM_Config_MeasurementsInfo> sourceMeasurementsInfoList = PDM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if ((sourceMeasurementsInfoList != null) && (sourceMeasurementsInfoList.Count > 0))
                {
                    destinationMeasurementsInfoList = PDM_Config_CloneMeasurementsInfoDbObjectList(sourceMeasurementsInfoList);
                    for (int i = 0; i < destinationMeasurementsInfoList.Count; i++)
                    {
                        destinationMeasurementsInfoList[i].ID = Guid.NewGuid();
                        destinationMeasurementsInfoList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = PDM_Config_WriteMeasurementsInfoListTableEntry(destinationMeasurementsInfoList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<PDM_Config_MeasurementsInfo> measurementsInfoList to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<PDM_Config_MeasurementsInfo> measurementsInfoList from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region PDSetupInfo

        public static bool PDM_Config_WritePDSetupInfoTableEntry(PDM_Config_PDSetupInfo pdSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (pdSetupInfo != null)
                {
                    db.PDM_Config_PDSetupInfo.InsertOnSubmit(pdSetupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WritePDSetupInfoTableEntry(PDM_Config_PDSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_PDSetupInfo PDM_Config_GetPDSetupInfoTableEntry(Guid pdSetupInfoID, MonitorInterfaceDB db)
        {
            PDM_Config_PDSetupInfo pdSetupInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_PDSetupInfo
                    where item.ID == pdSetupInfoID
                    select item;

                if (queryList.Count() > 0)
                {
                    pdSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetPDSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdSetupInfo;
        }

        public static PDM_Config_PDSetupInfo PDM_Config_GetPDSetupInfoTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            PDM_Config_PDSetupInfo pdSetupInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_PDSetupInfo
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    pdSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetPDSetupInfoTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdSetupInfo;
        }

        public static PDM_Config_PDSetupInfo PDM_Config_CloneOnePDSetupInfoDbObject(PDM_Config_PDSetupInfo pdSetupInfo)
        {
            PDM_Config_PDSetupInfo clonedPDSetupInfo = null;
            try
            {
                if (pdSetupInfo != null)
                {
                    clonedPDSetupInfo = new PDM_Config_PDSetupInfo();

                    clonedPDSetupInfo.ID = pdSetupInfo.ID;
                    clonedPDSetupInfo.ConfigurationRootID = pdSetupInfo.ConfigurationRootID;
                    clonedPDSetupInfo.DateAdded = pdSetupInfo.DateAdded;

                    clonedPDSetupInfo.SaveMode = pdSetupInfo.SaveMode;
                    clonedPDSetupInfo.ReadingSinPeriods = pdSetupInfo.ReadingSinPeriods;
                    clonedPDSetupInfo.SaveDays = pdSetupInfo.SaveDays;
                    clonedPDSetupInfo.SaveNum = pdSetupInfo.SaveNum;
                    clonedPDSetupInfo.PhaseShift = pdSetupInfo.PhaseShift;
                    clonedPDSetupInfo.PhShift = pdSetupInfo.PhShift;
                    clonedPDSetupInfo.CalcSpeedStackSize = pdSetupInfo.CalcSpeedStackSize;
                    clonedPDSetupInfo.PSpeed_0 = pdSetupInfo.PSpeed_0;
                    clonedPDSetupInfo.PSpeed_1 = pdSetupInfo.PSpeed_1;
                    clonedPDSetupInfo.PJump_0 = pdSetupInfo.PJump_0;
                    clonedPDSetupInfo.PJump_1 = pdSetupInfo.PJump_1;
                    clonedPDSetupInfo.SyncType = pdSetupInfo.SyncType;
                    clonedPDSetupInfo.InternalSyncFrequency = pdSetupInfo.InternalSyncFrequency;
                    clonedPDSetupInfo.PulsesAmpl = pdSetupInfo.PulsesAmpl;

                    clonedPDSetupInfo.Reserved_0 = pdSetupInfo.Reserved_0;
                    clonedPDSetupInfo.Reserved_1 = pdSetupInfo.Reserved_1;
                    clonedPDSetupInfo.Reserved_2 = pdSetupInfo.Reserved_2;
                    clonedPDSetupInfo.Reserved_3 = pdSetupInfo.Reserved_3;
                    clonedPDSetupInfo.Reserved_4 = pdSetupInfo.Reserved_4;
                    clonedPDSetupInfo.Reserved_5 = pdSetupInfo.Reserved_5;
                    clonedPDSetupInfo.Reserved_6 = pdSetupInfo.Reserved_6;
                    clonedPDSetupInfo.Reserved_7 = pdSetupInfo.Reserved_7;
                    clonedPDSetupInfo.Reserved_8 = pdSetupInfo.Reserved_8;
                    clonedPDSetupInfo.Reserved_9 = pdSetupInfo.Reserved_9;
                    clonedPDSetupInfo.Reserved_10 = pdSetupInfo.Reserved_10;
                    clonedPDSetupInfo.Reserved_11 = pdSetupInfo.Reserved_11;
                    clonedPDSetupInfo.Reserved_12 = pdSetupInfo.Reserved_12;
                    clonedPDSetupInfo.Reserved_13 = pdSetupInfo.Reserved_13;
                    clonedPDSetupInfo.Reserved_14 = pdSetupInfo.Reserved_14;
                    clonedPDSetupInfo.Reserved_15 = pdSetupInfo.Reserved_15;
                    clonedPDSetupInfo.Reserved_16 = pdSetupInfo.Reserved_16;
                    clonedPDSetupInfo.Reserved_17 = pdSetupInfo.Reserved_17;
                    clonedPDSetupInfo.Reserved_18 = pdSetupInfo.Reserved_18;
                    clonedPDSetupInfo.Reserved_19 = pdSetupInfo.Reserved_19;
                    clonedPDSetupInfo.Reserved_20 = pdSetupInfo.Reserved_20;
                    clonedPDSetupInfo.Reserved_21 = pdSetupInfo.Reserved_21;
                    clonedPDSetupInfo.Reserved_22 = pdSetupInfo.Reserved_22;
                    clonedPDSetupInfo.Reserved_23 = pdSetupInfo.Reserved_23;
                    clonedPDSetupInfo.Reserved_24 = pdSetupInfo.Reserved_24;
                    clonedPDSetupInfo.Reserved_25 = pdSetupInfo.Reserved_25;
                    clonedPDSetupInfo.Reserved_26 = pdSetupInfo.Reserved_26;
                    clonedPDSetupInfo.Reserved_27 = pdSetupInfo.Reserved_27;
                    clonedPDSetupInfo.Reserved_28 = pdSetupInfo.Reserved_28;
                    clonedPDSetupInfo.Reserved_29 = pdSetupInfo.Reserved_29;
                    clonedPDSetupInfo.Reserved_30 = pdSetupInfo.Reserved_30;
                    clonedPDSetupInfo.Reserved_31 = pdSetupInfo.Reserved_31;
                    clonedPDSetupInfo.Reserved_32 = pdSetupInfo.Reserved_32;
                    clonedPDSetupInfo.Reserved_33 = pdSetupInfo.Reserved_33;
                    clonedPDSetupInfo.Reserved_34 = pdSetupInfo.Reserved_34;
                    clonedPDSetupInfo.Reserved_35 = pdSetupInfo.Reserved_35;
                    clonedPDSetupInfo.Reserved_36 = pdSetupInfo.Reserved_36;
                    clonedPDSetupInfo.Reserved_37 = pdSetupInfo.Reserved_37;
                    clonedPDSetupInfo.Reserved_38 = pdSetupInfo.Reserved_38;
                    clonedPDSetupInfo.Reserved_39 = pdSetupInfo.Reserved_39;
                    clonedPDSetupInfo.Reserved_40 = pdSetupInfo.Reserved_40;
                    clonedPDSetupInfo.Reserved_41 = pdSetupInfo.Reserved_41;
                    clonedPDSetupInfo.Reserved_42 = pdSetupInfo.Reserved_42;
                    clonedPDSetupInfo.Reserved_43 = pdSetupInfo.Reserved_43;
                    clonedPDSetupInfo.Reserved_44 = pdSetupInfo.Reserved_44;
                    clonedPDSetupInfo.Reserved_45 = pdSetupInfo.Reserved_45;
                    clonedPDSetupInfo.Reserved_46 = pdSetupInfo.Reserved_46;
                    clonedPDSetupInfo.Reserved_47 = pdSetupInfo.Reserved_47;
                    clonedPDSetupInfo.Reserved_48 = pdSetupInfo.Reserved_48;
                    clonedPDSetupInfo.Reserved_49 = pdSetupInfo.Reserved_49;
                    clonedPDSetupInfo.Reserved_50 = pdSetupInfo.Reserved_50;
                    clonedPDSetupInfo.Reserved_51 = pdSetupInfo.Reserved_51;
                    clonedPDSetupInfo.Reserved_52 = pdSetupInfo.Reserved_52;
                    clonedPDSetupInfo.Reserved_53 = pdSetupInfo.Reserved_53;
                    clonedPDSetupInfo.Reserved_54 = pdSetupInfo.Reserved_54;
                    clonedPDSetupInfo.Reserved_55 = pdSetupInfo.Reserved_55;
                    clonedPDSetupInfo.Reserved_56 = pdSetupInfo.Reserved_56;
                    clonedPDSetupInfo.Reserved_57 = pdSetupInfo.Reserved_57;
                    clonedPDSetupInfo.Reserved_58 = pdSetupInfo.Reserved_58;
                    clonedPDSetupInfo.Reserved_59 = pdSetupInfo.Reserved_59;
                    clonedPDSetupInfo.Reserved_60 = pdSetupInfo.Reserved_60;
                    clonedPDSetupInfo.Reserved_61 = pdSetupInfo.Reserved_61;
                    clonedPDSetupInfo.Reserved_62 = pdSetupInfo.Reserved_62;
                    clonedPDSetupInfo.Reserved_63 = pdSetupInfo.Reserved_63;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneOnePDSetupInfoDbObject(PDM_Config_PDSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPDSetupInfo;
        }

        public static bool PDM_Config_DeletePDSetupInfoTableEntry(PDM_Config_PDSetupInfo pdSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (pdSetupInfo != null)
                {
                    db.PDM_Config_PDSetupInfo.DeleteOnSubmit(pdSetupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeletePDSetupInfoTableEntry(PDM_Config_PDSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeletePDSetupInfoTableEntry(Guid pdSetupInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_PDSetupInfo PDSetupInfo = PDM_DatabaseMethods.PDM_Config_GetPDSetupInfoTableEntry(pdSetupInfoID, db);
                success = PDM_DatabaseMethods.PDM_Config_DeletePDSetupInfoTableEntry(PDSetupInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeletePDSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Config_PDSetupInfo destinationPdSetupInfo;
                PDM_Config_PDSetupInfo sourcePdSetupInfo = PDM_Config_GetPDSetupInfoTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourcePdSetupInfo != null)
                {
                    destinationPdSetupInfo=PDM_Config_CloneOnePDSetupInfoDbObject(sourcePdSetupInfo);
                    destinationPdSetupInfo.ID = Guid.NewGuid();
                    destinationPdSetupInfo.ConfigurationRootID=destinationConfigurationRootID;
                    success = PDM_Config_WritePDSetupInfoTableEntry(destinationPdSetupInfo, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the PDM_Config_PDSetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the PDM_Config_PDSetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region SetupInfo

        public static bool PDM_Config_WriteSetupInfoTableEntry(PDM_Config_SetupInfo setupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (setupInfo != null)
                {
                    db.PDM_Config_SetupInfo.InsertOnSubmit(setupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_WriteSetupInfoTableEntry(PDM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Config_SetupInfo PDM_Config_GetSetupInfoTableEntry(Guid setupInfoID, MonitorInterfaceDB db)
        {
            PDM_Config_SetupInfo setupInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_SetupInfo
                    where item.ID == setupInfoID
                    select item;

                if (queryList.Count() > 0)
                {
                    setupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        public static PDM_Config_SetupInfo PDM_Config_GetSetupInfoTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            PDM_Config_SetupInfo setupInfo = null;
            try
            {
                var queryList =
                    from item in db.PDM_Config_SetupInfo
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    setupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_GetSetupInfoTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        public static PDM_Config_SetupInfo PDM_Config_CloneOneSetupInfoDbObject(PDM_Config_SetupInfo setupInfo)
        {
            PDM_Config_SetupInfo clonedSetupInfo = null;
            try
            {
                if (setupInfo != null)
                {
                    clonedSetupInfo = new PDM_Config_SetupInfo();

                    clonedSetupInfo.ID = setupInfo.ID;
                    clonedSetupInfo.ConfigurationRootID = setupInfo.ConfigurationRootID;
                    clonedSetupInfo.DateAdded = setupInfo.DateAdded;
                    clonedSetupInfo.FirmwareVersion = setupInfo.FirmwareVersion;

                    clonedSetupInfo.ScheduleType = setupInfo.ScheduleType;
                    clonedSetupInfo.DTime_Hour = setupInfo.DTime_Hour;
                    clonedSetupInfo.DTime_Minute = setupInfo.DTime_Minute;
                    clonedSetupInfo.DisplayFlag = setupInfo.DisplayFlag;
                    clonedSetupInfo.RelayMode = setupInfo.RelayMode;
                    clonedSetupInfo.TimeOfRelayAlarm = setupInfo.TimeOfRelayAlarm;
                    clonedSetupInfo.OutTime = setupInfo.OutTime;
                    clonedSetupInfo.DeviceNumber = setupInfo.DeviceNumber;
                    clonedSetupInfo.BaudRate = setupInfo.BaudRate;
                    clonedSetupInfo.ModBusProtocol = setupInfo.ModBusProtocol;
                    clonedSetupInfo.Stopped = setupInfo.Stopped;
                    clonedSetupInfo.AlarmEnable = setupInfo.AlarmEnable;
                    clonedSetupInfo.Termostat = setupInfo.Termostat;
                    clonedSetupInfo.ReReadOnAlarm = setupInfo.ReReadOnAlarm;

                    clonedSetupInfo.RatedCurrent_0 = setupInfo.RatedCurrent_0;
                    clonedSetupInfo.RatedCurrent_1 = setupInfo.RatedCurrent_1;
                    clonedSetupInfo.RatedCurrent_2 = setupInfo.RatedCurrent_2;
                    clonedSetupInfo.RatedCurrent_3 = setupInfo.RatedCurrent_3;
                    clonedSetupInfo.RatedCurrent_4 = setupInfo.RatedCurrent_4;
                    clonedSetupInfo.RatedCurrent_5 = setupInfo.RatedCurrent_5;
                    clonedSetupInfo.RatedCurrent_6 = setupInfo.RatedCurrent_6;
                    clonedSetupInfo.RatedCurrent_7 = setupInfo.RatedCurrent_7;
                    clonedSetupInfo.RatedCurrent_8 = setupInfo.RatedCurrent_8;
                    clonedSetupInfo.RatedCurrent_9 = setupInfo.RatedCurrent_9;
                    clonedSetupInfo.RatedCurrent_10 = setupInfo.RatedCurrent_10;
                    clonedSetupInfo.RatedCurrent_11 = setupInfo.RatedCurrent_11;
                    clonedSetupInfo.RatedCurrent_12 = setupInfo.RatedCurrent_12;
                    clonedSetupInfo.RatedCurrent_13 = setupInfo.RatedCurrent_13;
                    clonedSetupInfo.RatedCurrent_14 = setupInfo.RatedCurrent_14;

                    clonedSetupInfo.RatedVoltage_0 = setupInfo.RatedVoltage_0;
                    clonedSetupInfo.RatedVoltage_1 = setupInfo.RatedVoltage_1;
                    clonedSetupInfo.RatedVoltage_2 = setupInfo.RatedVoltage_2;
                    clonedSetupInfo.RatedVoltage_3 = setupInfo.RatedVoltage_3;
                    clonedSetupInfo.RatedVoltage_4 = setupInfo.RatedVoltage_4;
                    clonedSetupInfo.RatedVoltage_5 = setupInfo.RatedVoltage_5;
                    clonedSetupInfo.RatedVoltage_6 = setupInfo.RatedVoltage_6;
                    clonedSetupInfo.RatedVoltage_7 = setupInfo.RatedVoltage_7;
                    clonedSetupInfo.RatedVoltage_8 = setupInfo.RatedVoltage_8;
                    clonedSetupInfo.RatedVoltage_9 = setupInfo.RatedVoltage_9;
                    clonedSetupInfo.RatedVoltage_10 = setupInfo.RatedVoltage_10;
                    clonedSetupInfo.RatedVoltage_11 = setupInfo.RatedVoltage_11;
                    clonedSetupInfo.RatedVoltage_12 = setupInfo.RatedVoltage_12;
                    clonedSetupInfo.RatedVoltage_13 = setupInfo.RatedVoltage_13;
                    clonedSetupInfo.RatedVoltage_14 = setupInfo.RatedVoltage_14;

                    clonedSetupInfo.ReadAnalogFromRegisters = setupInfo.ReadAnalogFromRegisters;
                    clonedSetupInfo.ObjectType = setupInfo.ObjectType;
                    clonedSetupInfo.Power = setupInfo.Power;
                    clonedSetupInfo.ExtTemperature = setupInfo.ExtTemperature;
                    clonedSetupInfo.ExtHumidity = setupInfo.ExtHumidity;
                    clonedSetupInfo.ExtLoadActive = setupInfo.ExtLoadActive;
                    clonedSetupInfo.ExtLoadReactive = setupInfo.ExtLoadReactive;

                    clonedSetupInfo.Reserved_0 = setupInfo.Reserved_0;
                    clonedSetupInfo.Reserved_1 = setupInfo.Reserved_1;
                    clonedSetupInfo.Reserved_2 = setupInfo.Reserved_2;
                    clonedSetupInfo.Reserved_3 = setupInfo.Reserved_3;
                    clonedSetupInfo.Reserved_4 = setupInfo.Reserved_4;
                    clonedSetupInfo.Reserved_5 = setupInfo.Reserved_5;
                    clonedSetupInfo.Reserved_6 = setupInfo.Reserved_6;
                    clonedSetupInfo.Reserved_7 = setupInfo.Reserved_7;
                    clonedSetupInfo.Reserved_8 = setupInfo.Reserved_8;
                    clonedSetupInfo.Reserved_9 = setupInfo.Reserved_9;
                    clonedSetupInfo.Reserved_10 = setupInfo.Reserved_10;
                    clonedSetupInfo.Reserved_11 = setupInfo.Reserved_11;
                    clonedSetupInfo.Reserved_12 = setupInfo.Reserved_12;
                    clonedSetupInfo.Reserved_13 = setupInfo.Reserved_13;
                    clonedSetupInfo.Reserved_14 = setupInfo.Reserved_14;
                    clonedSetupInfo.Reserved_15 = setupInfo.Reserved_15;
                    clonedSetupInfo.Reserved_16 = setupInfo.Reserved_16;
                    clonedSetupInfo.Reserved_17 = setupInfo.Reserved_17;
                    clonedSetupInfo.Reserved_18 = setupInfo.Reserved_18;
                    clonedSetupInfo.Reserved_19 = setupInfo.Reserved_19;
                    clonedSetupInfo.Reserved_20 = setupInfo.Reserved_20;
                    clonedSetupInfo.Reserved_21 = setupInfo.Reserved_21;
                    clonedSetupInfo.Reserved_22 = setupInfo.Reserved_22;
                    clonedSetupInfo.Reserved_23 = setupInfo.Reserved_23;
                    clonedSetupInfo.Reserved_24 = setupInfo.Reserved_24;
                    clonedSetupInfo.Reserved_25 = setupInfo.Reserved_25;
                    clonedSetupInfo.Reserved_26 = setupInfo.Reserved_26;
                    clonedSetupInfo.Reserved_27 = setupInfo.Reserved_27;
                    clonedSetupInfo.Reserved_28 = setupInfo.Reserved_28;
                    clonedSetupInfo.Reserved_29 = setupInfo.Reserved_29;
                    clonedSetupInfo.Reserved_30 = setupInfo.Reserved_30;
                    clonedSetupInfo.Reserved_31 = setupInfo.Reserved_31;
                    clonedSetupInfo.Reserved_32 = setupInfo.Reserved_32;
                    clonedSetupInfo.Reserved_33 = setupInfo.Reserved_33;
                    clonedSetupInfo.Reserved_34 = setupInfo.Reserved_34;
                    clonedSetupInfo.Reserved_35 = setupInfo.Reserved_35;
                    clonedSetupInfo.Reserved_36 = setupInfo.Reserved_36;
                    clonedSetupInfo.Reserved_37 = setupInfo.Reserved_37;
                    clonedSetupInfo.Reserved_38 = setupInfo.Reserved_38;
                    clonedSetupInfo.Reserved_39 = setupInfo.Reserved_39;
                    clonedSetupInfo.Reserved_40 = setupInfo.Reserved_40;
                    clonedSetupInfo.Reserved_41 = setupInfo.Reserved_41;
                    clonedSetupInfo.Reserved_42 = setupInfo.Reserved_42;
                    clonedSetupInfo.Reserved_43 = setupInfo.Reserved_43;
                    clonedSetupInfo.Reserved_44 = setupInfo.Reserved_44;
                    clonedSetupInfo.Reserved_45 = setupInfo.Reserved_45;
                    clonedSetupInfo.Reserved_46 = setupInfo.Reserved_46;
                    clonedSetupInfo.Reserved_47 = setupInfo.Reserved_47;
                    clonedSetupInfo.Reserved_48 = setupInfo.Reserved_48;
                    clonedSetupInfo.Reserved_49 = setupInfo.Reserved_49;
                    clonedSetupInfo.Reserved_50 = setupInfo.Reserved_50;
                    clonedSetupInfo.Reserved_51 = setupInfo.Reserved_51;
                    clonedSetupInfo.Reserved_52 = setupInfo.Reserved_52;
                    clonedSetupInfo.Reserved_53 = setupInfo.Reserved_53;
                    clonedSetupInfo.Reserved_54 = setupInfo.Reserved_54;
                    clonedSetupInfo.Reserved_55 = setupInfo.Reserved_55;
                    clonedSetupInfo.Reserved_56 = setupInfo.Reserved_56;
                    clonedSetupInfo.Reserved_57 = setupInfo.Reserved_57;
                    clonedSetupInfo.Reserved_58 = setupInfo.Reserved_58;
                    clonedSetupInfo.Reserved_59 = setupInfo.Reserved_59;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CloneOneSetupInfoDbObject(PDM_Config_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedSetupInfo;
        }

        public static bool PDM_Config_DeleteSetupInfoTableEntry(PDM_Config_SetupInfo setupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (setupInfo != null)
                {
                    db.PDM_Config_SetupInfo.DeleteOnSubmit(setupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteSetupInfoTableEntry(PDM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_DeleteSetupInfoTableEntry(Guid setupInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Config_SetupInfo setupInfo = PDM_DatabaseMethods.PDM_Config_GetSetupInfoTableEntry(setupInfoID, db);
                success = PDM_DatabaseMethods.PDM_Config_DeleteSetupInfoTableEntry(setupInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_DeleteSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Config_SetupInfo destinationSetupInfo;
                PDM_Config_SetupInfo sourceSetupInfo = PDM_Config_GetSetupInfoTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceSetupInfo != null)
                {
                    destinationSetupInfo=PDM_Config_CloneOneSetupInfoDbObject(sourceSetupInfo);
                    destinationSetupInfo.ID = Guid.NewGuid();
                    destinationSetupInfo.ConfigurationRootID = destinationConfigurationRootID;
                    success = PDM_Config_WriteSetupInfoTableEntry(destinationSetupInfo, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the PDM_Config_SetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the PDM_Config_SetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        public static bool PDM_Config_CopyOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationMonitorID, destinationDB);
                if (success)
                {
                    success = PDM_Config_CopyChannelInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = PDM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = PDM_Config_CopyPDSetupInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = PDM_Config_CopySetupInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Config_CopyOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Config_CopyOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Config_ConfigurationRoot configurationRoot = PDM_Config_GetConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = PDM_Config_CopyOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find configuration in sourceDB.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        #endregion // PDM Configuration Methods

        #region Data Methods

        #region DataRoot

        public static bool PDM_Data_WriteDataRootTableEntry(PDM_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.PDM_Data_DataRoot.InsertOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteDataRootTableEntry(PDM_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_WriteDataRootTableEntries(List<PDM_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.PDM_Data_DataRoot.InsertAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteDataRootTableEntries(List<PDM_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Data_DataRoot PDM_Data_GetDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            PDM_Data_DataRoot dataRoot = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_DataRoot
                    where item.ID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        public static PDM_Data_DataRoot PDM_Data_CloneOneDataRootDbObject(PDM_Data_DataRoot dataRoot)
        {
            PDM_Data_DataRoot clonedDataRoot = null;
            try
            {
                if (dataRoot != null)
                {
                    clonedDataRoot = new PDM_Data_DataRoot();

                    clonedDataRoot.ID = dataRoot.ID;
                    clonedDataRoot.MonitorID = dataRoot.MonitorID;
                    clonedDataRoot.ReadingDateTime = dataRoot.ReadingDateTime;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneOneDataRootDbObject(PDM_Data_DataRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRoot;
        }

        public static PDM_Data_DataRoot PDM_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            PDM_Data_DataRoot dataRoot = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime descending
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        /// <summary>
        /// Gets every BHM_Data_SideToSideData table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_DataRoot> PDM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Data_DataRoot> dataRootList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }

        /// <summary>
        /// Gets every BHM_Data_SideToSideData table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_DataRoot> PDM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_Data_DataRoot> dataRootList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_DataRoot
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }


        public static List<PDM_Data_DataRoot> PDM_Data_CloneDataRootDbObjectList(List<PDM_Data_DataRoot> dataRootList)
        {
            List<PDM_Data_DataRoot> clonedDataRootList = null;
            try
            {
                PDM_Data_DataRoot dataRootEntry;
                if (dataRootList != null)
                {
                    clonedDataRootList = new List<PDM_Data_DataRoot>();
                    foreach (PDM_Data_DataRoot entry in dataRootList)
                    {
                        dataRootEntry = PDM_Data_CloneOneDataRootDbObject(entry);
                        if (dataRootEntry != null)
                        {
                            clonedDataRootList.Add(dataRootEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneDataRootDbObjectList(List<PDM_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRootList;
        }

        public static bool PDM_Data_DeleteDataRootTableEntry(PDM_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.PDM_Data_DataRoot.DeleteOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntry(PDM_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_DeleteDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                PDM_Data_DataRoot dataRoot = PDM_Data_GetDataRootTableEntry(dataRootID, db);
                success = PDM_Data_DeleteDataRootTableEntry(dataRoot, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_DeleteDataRootTableEntries(List<PDM_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.PDM_Data_DataRoot.DeleteAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_DeleteDataRootTableEntries(List<PDM_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<PDM_Data_DataRoot> dataRootList = PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, db);
                success = PDM_Data_DeleteDataRootTableEntries(dataRootList, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_CopyDataRootForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Data_DataRoot dataRoot = PDM_Data_GetDataRootTableEntry(dataRootID, sourceDB);
                if (dataRoot != null)
                {
                    success = PDM_Data_WriteDataRootTableEntry(PDM_Data_CloneOneDataRootDbObject(dataRoot), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyDataRootForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the PDM_Data_DataRoot to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyDataRootForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the PDM_Data_DataRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CopyDataRootForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region ChPDParameters

        public static bool PDM_Data_WriteChPDParametersTableEntry(PDM_Data_ChPDParameters chPDParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (chPDParameters != null)
                {
                    db.PDM_Data_ChPDParameters.InsertOnSubmit(chPDParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteChPDParametersTableEntry(PDM_Data_ChPDParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_WriteChPDParametersTableEntries(List<PDM_Data_ChPDParameters> chPDParametersList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((chPDParametersList != null) && (chPDParametersList.Count > 0))
                {
                    db.PDM_Data_ChPDParameters.InsertAllOnSubmit(chPDParametersList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteChPDParametersTableEntries(List<PDM_Data_ChPDParameters>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Data_ChPDParameters PDM_Data_GetChPDParametersTableEntry(Guid chPDParametersID, MonitorInterfaceDB db)
        {
            PDM_Data_ChPDParameters chPDParameters = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_ChPDParameters
                    where item.ID == chPDParametersID
                    select item;

                if (queryList.Count() > 0)
                {
                    chPDParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetChPDParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParameters;
        }

        public static PDM_Data_ChPDParameters PDM_Data_CloneOneChPDParametersDbObject(PDM_Data_ChPDParameters chPDParameters)
        {
            PDM_Data_ChPDParameters clonedChPDParameters = null;
            try
            {
                if (chPDParameters != null)
                {
                    clonedChPDParameters = new PDM_Data_ChPDParameters();

                    clonedChPDParameters.ID = chPDParameters.ID;
                    clonedChPDParameters.DataRootID = chPDParameters.DataRootID;
                    clonedChPDParameters.MonitorID = chPDParameters.MonitorID;
                    clonedChPDParameters.ReadingDateTime = chPDParameters.ReadingDateTime;

                    clonedChPDParameters.FromChannel = chPDParameters.FromChannel;
                    clonedChPDParameters.WithChannels_0 = chPDParameters.WithChannels_0;
                    clonedChPDParameters.ChannelSensitivity = chPDParameters.ChannelSensitivity;
                    clonedChPDParameters.Q02 = chPDParameters.Q02;
                    clonedChPDParameters.Q02Plus = chPDParameters.Q02Plus;
                    clonedChPDParameters.Q02Minus = chPDParameters.Q02Minus;
                    clonedChPDParameters.Q02mV = chPDParameters.Q02mV;
                    clonedChPDParameters.PDI = chPDParameters.PDI;
                    clonedChPDParameters.PDIPlus = chPDParameters.PDIPlus;
                    clonedChPDParameters.PDIMinus = chPDParameters.PDIMinus;
                    clonedChPDParameters.SumPDAmpl = chPDParameters.SumPDAmpl;
                    clonedChPDParameters.SumPDPlus = chPDParameters.SumPDPlus;
                    clonedChPDParameters.SumPDMinus = chPDParameters.SumPDMinus;
                    clonedChPDParameters.PDI_t = chPDParameters.PDI_t;
                    clonedChPDParameters.PDI_j = chPDParameters.PDI_j;
                    clonedChPDParameters.Q02_t = chPDParameters.Q02_t;
                    clonedChPDParameters.Q02_j = chPDParameters.Q02_j;
                    clonedChPDParameters.Separations_0 = chPDParameters.Separations_0;
                    clonedChPDParameters.RefShift_0 = chPDParameters.RefShift_0;
                    clonedChPDParameters.WasBlockedByT = chPDParameters.WasBlockedByT;
                    clonedChPDParameters.WasBlockedByP = chPDParameters.WasBlockedByP;
                    clonedChPDParameters.RatedVoltage = chPDParameters.RatedVoltage;
                    clonedChPDParameters.RatedCurrent = chPDParameters.RatedCurrent;
                    clonedChPDParameters.State = chPDParameters.State;
                    clonedChPDParameters.AlarmStatus = chPDParameters.AlarmStatus;
                    clonedChPDParameters.P_0 = chPDParameters.P_0;
                    clonedChPDParameters.P_1 = chPDParameters.P_1;
                    clonedChPDParameters.P_2 = chPDParameters.P_2;
                    clonedChPDParameters.P_3 = chPDParameters.P_3;
                    clonedChPDParameters.P_4 = chPDParameters.P_4;
                    clonedChPDParameters.P_5 = chPDParameters.P_5;
                    clonedChPDParameters.Reserved_0 = chPDParameters.Reserved_0;
                    clonedChPDParameters.Reserved_1 = chPDParameters.Reserved_1;
                    clonedChPDParameters.Reserved_2 = chPDParameters.Reserved_2;
                    clonedChPDParameters.Reserved_3 = chPDParameters.Reserved_3;
                    clonedChPDParameters.Reserved_4 = chPDParameters.Reserved_4;
                    clonedChPDParameters.Reserved_5 = chPDParameters.Reserved_5;
                    clonedChPDParameters.Reserved_6 = chPDParameters.Reserved_6;
                    clonedChPDParameters.Reserved_7 = chPDParameters.Reserved_7;
                    clonedChPDParameters.Reserved_8 = chPDParameters.Reserved_8;
                    clonedChPDParameters.Reserved_9 = chPDParameters.Reserved_9;
                    clonedChPDParameters.Reserved_10 = chPDParameters.Reserved_10;
                    clonedChPDParameters.Reserved_11 = chPDParameters.Reserved_11;
                    clonedChPDParameters.Reserved_12 = chPDParameters.Reserved_12;
                    clonedChPDParameters.Reserved_13 = chPDParameters.Reserved_13;
                    clonedChPDParameters.Reserved_14 = chPDParameters.Reserved_14;
                    clonedChPDParameters.Reserved_15 = chPDParameters.Reserved_15;
                    clonedChPDParameters.Reserved_16 = chPDParameters.Reserved_16;
                    clonedChPDParameters.Reserved_17 = chPDParameters.Reserved_17;
                    clonedChPDParameters.Reserved_18 = chPDParameters.Reserved_18;
                    clonedChPDParameters.Reserved_19 = chPDParameters.Reserved_19;
                    clonedChPDParameters.Reserved_20 = chPDParameters.Reserved_20;
                    clonedChPDParameters.Reserved_21 = chPDParameters.Reserved_21;
                    clonedChPDParameters.Reserved_22 = chPDParameters.Reserved_22;
                    clonedChPDParameters.Reserved_23 = chPDParameters.Reserved_23;
                    clonedChPDParameters.Reserved_24 = chPDParameters.Reserved_24;
                    clonedChPDParameters.Reserved_25 = chPDParameters.Reserved_25;
                    clonedChPDParameters.Reserved_26 = chPDParameters.Reserved_26;
                    clonedChPDParameters.Reserved_27 = chPDParameters.Reserved_27;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneOneChPDParametersDbObject(PDM_Data_ChPDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedChPDParameters;
        }

        public static List<PDM_Data_ChPDParameters> PDM_Data_GetAllChPDParametersTableEntriesForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            List<PDM_Data_ChPDParameters> chPDParametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_ChPDParameters
                    where item.DataRootID == dataRootID
                    orderby item.FromChannel ascending
                    select item;

                chPDParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParametersList;
        }

        /// <summary>
        /// Gets every PDM_Data_ChPDParameters table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_ChPDParameters> PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Data_ChPDParameters> chPDParametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_ChPDParameters
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending, item.FromChannel ascending
                    select item;

                chPDParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParametersList;
        }

        /// <summary>
        /// Gets every PDM_Data_ChPDParameters table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_ChPDParameters> PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_Data_ChPDParameters> chPDParametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_ChPDParameters
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending, item.FromChannel ascending
                    select item;

                chPDParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParametersList;
        }

        public static List<PDM_Data_ChPDParameters> PDM_Data_CloneChPDParametersDbObjectList(List<PDM_Data_ChPDParameters> chPDParametersList)
        {
            List<PDM_Data_ChPDParameters> clonedChPDParametersList = null;
            try
            {
                PDM_Data_ChPDParameters chPDParametersEntry;
                if (chPDParametersList != null)
                {
                    clonedChPDParametersList = new List<PDM_Data_ChPDParameters>();
                    foreach (PDM_Data_ChPDParameters entry in chPDParametersList)
                    {
                        chPDParametersEntry = PDM_Data_CloneOneChPDParametersDbObject(entry);
                        if (chPDParametersEntry != null)
                        {
                            clonedChPDParametersList.Add(chPDParametersEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneChPDParametersDbObjectList(List<PDM_Data_ChPDParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedChPDParametersList;
        }

        public static bool PDM_Data_CopyChPDParametersForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<PDM_Data_ChPDParameters> chPDParametersList = PDM_Data_GetAllChPDParametersTableEntriesForOneDataRoot(dataRootID, sourceDB);
                if (chPDParametersList != null)
                {
                    success = PDM_Data_WriteChPDParametersTableEntries(PDM_Data_CloneChPDParametersDbObjectList(chPDParametersList), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyChPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the List<PDM_Data_ChPDParameters> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyChPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the List<PDM_Data_ChPDParameters> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CopyChPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region Parameters

        public static bool PDM_Data_WriteParametersTableEntry(PDM_Data_Parameters parameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (parameters != null)
                {
                    db.PDM_Data_Parameters.InsertOnSubmit(parameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteParametersTableEntry(PDM_Data_Parameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_WriteParametersTableEntries(List<PDM_Data_Parameters> parametersList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((parametersList != null) && (parametersList.Count > 0))
                {
                    db.PDM_Data_Parameters.InsertAllOnSubmit(parametersList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WriteParametersTableEntries(List<PDM_Data_Parameters>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Data_Parameters PDM_Data_GetParametersTableEntry(Guid parametersID, MonitorInterfaceDB db)
        {
            PDM_Data_Parameters parameters = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_Parameters
                    where item.ID == parametersID
                    select item;

                if (queryList.Count() > 0)
                {
                    parameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parameters;
        }

        public static PDM_Data_Parameters PDM_Data_GetParametersTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            PDM_Data_Parameters parameters = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_Parameters
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    parameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetParametersTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parameters;
        }

        public static PDM_Data_Parameters PDM_Data_CloneOneParametersDbObject(PDM_Data_Parameters parameters)
        {
            PDM_Data_Parameters clonedParameters = null;
            try
            {
                if (parameters != null)
                {
                    clonedParameters = new PDM_Data_Parameters();

                    clonedParameters.ID = parameters.ID;
                    clonedParameters.DataRootID = parameters.DataRootID;
                    clonedParameters.MonitorID = parameters.MonitorID;
                    clonedParameters.ReadingDateTime = parameters.ReadingDateTime;

                    clonedParameters.Tempfile = parameters.Tempfile;
                    clonedParameters.PDExists = parameters.PDExists;
                    clonedParameters.AlarmEnable = parameters.AlarmEnable;
                    clonedParameters.Initial = parameters.Initial;
                    clonedParameters.Humidity = parameters.Humidity;
                    clonedParameters.Temperature = parameters.Temperature;
                    clonedParameters.ExtLoadActive = parameters.ExtLoadActive;
                    clonedParameters.ExtLoadReactive = parameters.ExtLoadReactive;
                    clonedParameters.CommonRegisters_0 = parameters.CommonRegisters_0;
                    clonedParameters.CommonRegisters_1 = parameters.CommonRegisters_1;
                    clonedParameters.CommonRegisters_2 = parameters.CommonRegisters_2;
                    clonedParameters.CommonRegisters_3 = parameters.CommonRegisters_3;
                    clonedParameters.CommonRegisters_4 = parameters.CommonRegisters_4;
                    clonedParameters.CommonRegisters_5 = parameters.CommonRegisters_5;
                    clonedParameters.CommonRegisters_6 = parameters.CommonRegisters_6;
                    clonedParameters.CommonRegisters_7 = parameters.CommonRegisters_7;
                    clonedParameters.CommonRegisters_8 = parameters.CommonRegisters_8;
                    clonedParameters.CommonRegisters_9 = parameters.CommonRegisters_9;
                    clonedParameters.CommonRegisters_10 = parameters.CommonRegisters_10;
                    clonedParameters.CommonRegisters_11 = parameters.CommonRegisters_11;
                    clonedParameters.CommonRegisters_12 = parameters.CommonRegisters_12;
                    clonedParameters.CommonRegisters_13 = parameters.CommonRegisters_13;
                    clonedParameters.CommonRegisters_14 = parameters.CommonRegisters_14;
                    clonedParameters.CommonRegisters_15 = parameters.CommonRegisters_15;
                    clonedParameters.CommonRegisters_16 = parameters.CommonRegisters_16;
                    clonedParameters.CommonRegisters_17 = parameters.CommonRegisters_17;
                    clonedParameters.CommonRegisters_18 = parameters.CommonRegisters_18;
                    clonedParameters.CommonRegisters_19 = parameters.CommonRegisters_19;
                    clonedParameters.Reserved_0 = parameters.Reserved_0;
                    clonedParameters.Reserved_1 = parameters.Reserved_1;
                    clonedParameters.Reserved_2 = parameters.Reserved_2;
                    clonedParameters.Reserved_3 = parameters.Reserved_3;
                    clonedParameters.Reserved_4 = parameters.Reserved_4;
                    clonedParameters.Reserved_5 = parameters.Reserved_5;
                    clonedParameters.Reserved_6 = parameters.Reserved_6;
                    clonedParameters.Reserved_7 = parameters.Reserved_7;
                    clonedParameters.Reserved_8 = parameters.Reserved_8;
                    clonedParameters.Reserved_9 = parameters.Reserved_9;
                    clonedParameters.Reserved_10 = parameters.Reserved_10;
                    clonedParameters.Reserved_11 = parameters.Reserved_11;
                    clonedParameters.Reserved_12 = parameters.Reserved_12;
                    clonedParameters.Reserved_13 = parameters.Reserved_13;
                    clonedParameters.Reserved_14 = parameters.Reserved_14;
                    clonedParameters.Reserved_15 = parameters.Reserved_15;
                    clonedParameters.Reserved_16 = parameters.Reserved_16;
                    clonedParameters.Reserved_17 = parameters.Reserved_17;
                    clonedParameters.Reserved_18 = parameters.Reserved_18;
                    clonedParameters.Reserved_19 = parameters.Reserved_19;
                    clonedParameters.Reserved_20 = parameters.Reserved_20;
                    clonedParameters.Reserved_21 = parameters.Reserved_21;
                    clonedParameters.Reserved_22 = parameters.Reserved_22;
                    clonedParameters.Reserved_23 = parameters.Reserved_23;
                    clonedParameters.Reserved_24 = parameters.Reserved_24;
                    clonedParameters.Reserved_25 = parameters.Reserved_25;
                    clonedParameters.Reserved_26 = parameters.Reserved_26;
                    clonedParameters.Reserved_27 = parameters.Reserved_27;
                    clonedParameters.Reserved_28 = parameters.Reserved_28;
                    clonedParameters.Reserved_29 = parameters.Reserved_29;
                    clonedParameters.Reserved_30 = parameters.Reserved_30;
                    clonedParameters.Reserved_31 = parameters.Reserved_31;
                    clonedParameters.Reserved_32 = parameters.Reserved_32;
                    clonedParameters.Reserved_33 = parameters.Reserved_33;
                    clonedParameters.Reserved_34 = parameters.Reserved_34;
                    clonedParameters.Reserved_35 = parameters.Reserved_35;
                    clonedParameters.Reserved_36 = parameters.Reserved_36;
                    clonedParameters.Reserved_37 = parameters.Reserved_37;
                    clonedParameters.Reserved_38 = parameters.Reserved_38;
                    clonedParameters.Reserved_39 = parameters.Reserved_39;
                    clonedParameters.Reserved_40 = parameters.Reserved_40;
                    clonedParameters.Reserved_41 = parameters.Reserved_41;
                    clonedParameters.Reserved_42 = parameters.Reserved_42;
                    clonedParameters.Reserved_43 = parameters.Reserved_43;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneOneParametersDbObject(PDM_Data_Parameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedParameters;
        }

        /// <summary>
        /// Gets every PDM_Data_Parameters table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_Parameters> PDM_Data_GetAllParametersTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Data_Parameters> parametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_Parameters
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                parametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parametersList;
        }

        /// <summary>
        /// Gets every PDM_Data_Parameters table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_Parameters> PDM_Data_GetAllParametersTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_Data_Parameters> parametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_Parameters
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                parametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parametersList;
        }

        public static List<PDM_Data_Parameters> PDM_Data_CloneParametersDbObjectList(List<PDM_Data_Parameters> parametersList)
        {
            List<PDM_Data_Parameters> clonedParametersList = null;
            try
            {
                PDM_Data_Parameters parametersEntry;
                if (parametersList != null)
                {
                    clonedParametersList = new List<PDM_Data_Parameters>();
                    foreach (PDM_Data_Parameters entry in parametersList)
                    {
                        parametersEntry = PDM_Data_CloneOneParametersDbObject(entry);
                        if (parametersEntry != null)
                        {
                            clonedParametersList.Add(parametersEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneParametersDbObjectList(List<PDM_Data_Parameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedParametersList;
        }

        public static bool PDM_Data_CopyParametersForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Data_Parameters parameters = PDM_Data_GetParametersTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (parameters != null)
                {
                    success = PDM_Data_WriteParametersTableEntry(PDM_Data_CloneOneParametersDbObject(parameters), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the PDM_Data_Parameters to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the PDM_Data_Parameters from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CopyParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region PDParmeters

        public static bool PDM_Data_WritePDParametersTableEntry(PDM_Data_PDParameters pdParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (pdParameters != null)
                {
                    db.PDM_Data_PDParameters.InsertOnSubmit(pdParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WritePDParametersTableEntry(PDM_Data_PDParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_WritePDParametersTableEntries(List<PDM_Data_PDParameters> pdParametersList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((pdParametersList != null) && (pdParametersList.Count > 0))
                {
                    db.PDM_Data_PDParameters.InsertAllOnSubmit(pdParametersList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WritePDParametersTableEntries(List<PDM_Data_PDParameters>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Data_PDParameters PDM_Data_GetPDParametersTableEntry(Guid pdParametersID, MonitorInterfaceDB db)
        {
            PDM_Data_PDParameters pdParameters = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PDParameters
                    where item.ID == pdParametersID
                    select item;

                if (queryList.Count() > 0)
                {
                    pdParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetPDParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParameters;
        }

        public static PDM_Data_PDParameters PDM_Data_GetPDParametersTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            PDM_Data_PDParameters pdParameters = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PDParameters
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    pdParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetPDParametersTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParameters;
        }

        public static PDM_Data_PDParameters PDM_Data_CloneOnePDParametersDbObject(PDM_Data_PDParameters pdParameters)
        {
            PDM_Data_PDParameters clonedPDParameters = null;
            try
            {
                if (pdParameters != null)
                {
                    clonedPDParameters = new PDM_Data_PDParameters();

                    clonedPDParameters.ID = pdParameters.ID;
                    clonedPDParameters.DataRootID = pdParameters.DataRootID;
                    clonedPDParameters.MonitorID = pdParameters.MonitorID;
                    clonedPDParameters.ReadingDateTime = pdParameters.ReadingDateTime;

                    clonedPDParameters.Channels = pdParameters.Channels;
                    clonedPDParameters.Matrix = pdParameters.Matrix;
                    clonedPDParameters.MaxPDIChannel = pdParameters.MaxPDIChannel;
                    clonedPDParameters.MaxQ02Channel = pdParameters.MaxQ02Channel;
                    clonedPDParameters.NumberOfPeriods = pdParameters.NumberOfPeriods;
                    clonedPDParameters.Frequency = pdParameters.Frequency;
                    clonedPDParameters.PSpeed_0 = pdParameters.PSpeed_0;
                    clonedPDParameters.PSpeed_1 = pdParameters.PSpeed_1;
                    clonedPDParameters.PJump_0 = pdParameters.PJump_0;
                    clonedPDParameters.PJump_1 = pdParameters.PJump_1;
                    clonedPDParameters.State = pdParameters.State;
                    clonedPDParameters.ZoneWidth = pdParameters.ZoneWidth;
                    clonedPDParameters.ZonesCount = pdParameters.ZonesCount;
                    clonedPDParameters.AngleStep = pdParameters.AngleStep;
                    clonedPDParameters.AnglesCount = pdParameters.AnglesCount;
                    clonedPDParameters.TablesOnChannel = pdParameters.TablesOnChannel;
                    clonedPDParameters.Reserved_0 = pdParameters.Reserved_0;
                    clonedPDParameters.Reserved_1 = pdParameters.Reserved_1;
                    clonedPDParameters.Reserved_2 = pdParameters.Reserved_2;
                    clonedPDParameters.Reserved_3 = pdParameters.Reserved_3;
                    clonedPDParameters.Reserved_4 = pdParameters.Reserved_4;
                    clonedPDParameters.Reserved_5 = pdParameters.Reserved_5;
                    clonedPDParameters.Reserved_6 = pdParameters.Reserved_6;
                    clonedPDParameters.Reserved_7 = pdParameters.Reserved_7;
                    clonedPDParameters.Reserved_8 = pdParameters.Reserved_8;
                    clonedPDParameters.Reserved_9 = pdParameters.Reserved_9;
                    clonedPDParameters.Reserved_10 = pdParameters.Reserved_10;
                    clonedPDParameters.Reserved_11 = pdParameters.Reserved_11;
                    clonedPDParameters.Reserved_12 = pdParameters.Reserved_12;
                    clonedPDParameters.Reserved_13 = pdParameters.Reserved_13;
                    clonedPDParameters.Reserved_14 = pdParameters.Reserved_14;
                    clonedPDParameters.Reserved_15 = pdParameters.Reserved_15;
                    clonedPDParameters.Reserved_16 = pdParameters.Reserved_16;
                    clonedPDParameters.Reserved_17 = pdParameters.Reserved_17;
                    clonedPDParameters.Reserved_18 = pdParameters.Reserved_18;
                    clonedPDParameters.Reserved_19 = pdParameters.Reserved_19;
                    clonedPDParameters.Reserved_20 = pdParameters.Reserved_20;
                    clonedPDParameters.Reserved_21 = pdParameters.Reserved_21;
                    clonedPDParameters.Reserved_22 = pdParameters.Reserved_22;
                    clonedPDParameters.Reserved_23 = pdParameters.Reserved_23;
                    clonedPDParameters.Reserved_24 = pdParameters.Reserved_24;
                    clonedPDParameters.Reserved_25 = pdParameters.Reserved_25;
                    clonedPDParameters.Reserved_26 = pdParameters.Reserved_26;
                    clonedPDParameters.Reserved_27 = pdParameters.Reserved_27;
                    clonedPDParameters.Reserved_28 = pdParameters.Reserved_28;
                    clonedPDParameters.Reserved_29 = pdParameters.Reserved_29;
                    clonedPDParameters.Reserved_30 = pdParameters.Reserved_30;
                    clonedPDParameters.Reserved_31 = pdParameters.Reserved_31;
                    clonedPDParameters.Reserved_32 = pdParameters.Reserved_32;
                    clonedPDParameters.Reserved_33 = pdParameters.Reserved_33;
                    clonedPDParameters.Reserved_34 = pdParameters.Reserved_34;
                    clonedPDParameters.Reserved_35 = pdParameters.Reserved_35;
                    clonedPDParameters.Reserved_36 = pdParameters.Reserved_36;
                    clonedPDParameters.Reserved_37 = pdParameters.Reserved_37;
                    clonedPDParameters.Reserved_38 = pdParameters.Reserved_38;
                    clonedPDParameters.Reserved_39 = pdParameters.Reserved_39;
                    clonedPDParameters.Reserved_40 = pdParameters.Reserved_40;
                    clonedPDParameters.Reserved_41 = pdParameters.Reserved_41;
                    clonedPDParameters.Reserved_42 = pdParameters.Reserved_42;
                    clonedPDParameters.Reserved_43 = pdParameters.Reserved_43;
                    clonedPDParameters.Reserved_44 = pdParameters.Reserved_44;
                    clonedPDParameters.Reserved_45 = pdParameters.Reserved_45;
                    clonedPDParameters.Reserved_46 = pdParameters.Reserved_46;
                    clonedPDParameters.Reserved_47 = pdParameters.Reserved_47;
                    clonedPDParameters.Reserved_48 = pdParameters.Reserved_48;
                    clonedPDParameters.Reserved_49 = pdParameters.Reserved_49;
                    clonedPDParameters.Reserved_50 = pdParameters.Reserved_50;
                    clonedPDParameters.Reserved_51 = pdParameters.Reserved_51;
                    clonedPDParameters.Reserved_52 = pdParameters.Reserved_52;
                    clonedPDParameters.Reserved_53 = pdParameters.Reserved_53;
                    clonedPDParameters.Reserved_54 = pdParameters.Reserved_54;
                    clonedPDParameters.Reserved_55 = pdParameters.Reserved_55;
                    clonedPDParameters.Reserved_56 = pdParameters.Reserved_56;
                    clonedPDParameters.Reserved_57 = pdParameters.Reserved_57;
                    clonedPDParameters.Reserved_58 = pdParameters.Reserved_58;
                    clonedPDParameters.Reserved_59 = pdParameters.Reserved_59;
                    clonedPDParameters.Reserved_60 = pdParameters.Reserved_60;
                    clonedPDParameters.Reserved_61 = pdParameters.Reserved_61;
                    clonedPDParameters.Reserved_62 = pdParameters.Reserved_62;
                    clonedPDParameters.Reserved_63 = pdParameters.Reserved_63;
                    clonedPDParameters.Reserved_64 = pdParameters.Reserved_64;
                    clonedPDParameters.Reserved_65 = pdParameters.Reserved_65;
                    clonedPDParameters.Reserved_66 = pdParameters.Reserved_66;
                    clonedPDParameters.Reserved_67 = pdParameters.Reserved_67;
                    clonedPDParameters.Reserved_68 = pdParameters.Reserved_68;
                    clonedPDParameters.Reserved_69 = pdParameters.Reserved_69;
                    clonedPDParameters.Reserved_70 = pdParameters.Reserved_70;
                    clonedPDParameters.Reserved_71 = pdParameters.Reserved_71;
                    clonedPDParameters.Reserved_72 = pdParameters.Reserved_72;
                    clonedPDParameters.Reserved_73 = pdParameters.Reserved_73;
                    clonedPDParameters.Reserved_74 = pdParameters.Reserved_74;
                    clonedPDParameters.Reserved_75 = pdParameters.Reserved_75;
                    clonedPDParameters.Reserved_76 = pdParameters.Reserved_76;
                    clonedPDParameters.Reserved_77 = pdParameters.Reserved_77;
                    clonedPDParameters.Reserved_78 = pdParameters.Reserved_78;
                    clonedPDParameters.Reserved_79 = pdParameters.Reserved_79;
                    clonedPDParameters.Reserved_80 = pdParameters.Reserved_80;
                    clonedPDParameters.Reserved_81 = pdParameters.Reserved_81;
                    clonedPDParameters.Reserved_82 = pdParameters.Reserved_82;
                    clonedPDParameters.Reserved_83 = pdParameters.Reserved_83;
                    clonedPDParameters.Reserved_84 = pdParameters.Reserved_84;
                    clonedPDParameters.Reserved_85 = pdParameters.Reserved_85;
                    clonedPDParameters.Reserved_86 = pdParameters.Reserved_86;
                    clonedPDParameters.Reserved_87 = pdParameters.Reserved_87;
                    clonedPDParameters.Reserved_88 = pdParameters.Reserved_88;
                    clonedPDParameters.Reserved_89 = pdParameters.Reserved_89;
                    clonedPDParameters.Reserved_90 = pdParameters.Reserved_90;
                    clonedPDParameters.Reserved_91 = pdParameters.Reserved_91;
                    clonedPDParameters.Reserved_92 = pdParameters.Reserved_92;
                    clonedPDParameters.Reserved_93 = pdParameters.Reserved_93;
                    clonedPDParameters.Reserved_94 = pdParameters.Reserved_94;
                    clonedPDParameters.Reserved_95 = pdParameters.Reserved_95;
                    clonedPDParameters.Reserved_96 = pdParameters.Reserved_96;
                    clonedPDParameters.Reserved_97 = pdParameters.Reserved_97;
                    clonedPDParameters.Reserved_98 = pdParameters.Reserved_98;
                    clonedPDParameters.Reserved_99 = pdParameters.Reserved_99;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneOnePDParametersDbObject(PDM_Data_PDParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPDParameters;
        }

        /// <summary>
        /// Gets every PDM_Data_PDParameters table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_PDParameters> PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Data_PDParameters> pdParametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PDParameters
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                pdParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParametersList;
        }

        /// <summary>
        /// Gets every PDM_Data_PDParameters table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_PDParameters> PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_Data_PDParameters> pdParametersList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PDParameters
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                pdParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParametersList;
        }

        public static List<PDM_Data_PDParameters> PDM_Data_ClonePDParametersDbObjectList(List<PDM_Data_PDParameters> pdParametersList)
        {
            List<PDM_Data_PDParameters> clonedPDParametersList = null;
            try
            {
                PDM_Data_PDParameters pdParametersEntry;
                if (pdParametersList != null)
                {
                    clonedPDParametersList = new List<PDM_Data_PDParameters>();
                    foreach (PDM_Data_PDParameters entry in pdParametersList)
                    {
                        pdParametersEntry = PDM_Data_CloneOnePDParametersDbObject(entry);
                        if (pdParametersEntry != null)
                        {
                            clonedPDParametersList.Add(pdParametersEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_ClonePDParametersDbObjectList(List<PDM_Data_PDParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPDParametersList;
        }

        public static bool PDM_Data_CopyPDParametersForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                PDM_Data_PDParameters pdParameters = PDM_Data_GetPDParametersTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (pdParameters != null)
                {
                    success = PDM_Data_WritePDParametersTableEntry(PDM_Data_CloneOnePDParametersDbObject(pdParameters), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the PDM_Data_PDParameters to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the PDM_Data_PDParameters from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CopyPDParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region PhaseResolvedData

        public static bool PDM_Data_WritePhaseResolvedDataTableEntry(PDM_Data_PhaseResolvedData phaseResolvedData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (phaseResolvedData != null)
                {
                    db.PDM_Data_PhaseResolvedData.InsertOnSubmit(phaseResolvedData);
                    db.SubmitChanges();
                    success = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WritePhaseResolvedDataTableEntry(PDM_Data_PhaseResolvedData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_Data_WritePhaseResolvedDataTableEntries(List<PDM_Data_PhaseResolvedData> phaseResolvedDataList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((phaseResolvedDataList != null) && (phaseResolvedDataList.Count > 0))
                {
                    db.PDM_Data_PhaseResolvedData.InsertAllOnSubmit(phaseResolvedDataList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_WritePhaseResolvedDataTableEntries(List<PDM_Data_PhaseResolvedData>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static PDM_Data_PhaseResolvedData PDM_Data_GetPhaseResolvedDataTableEntry(Guid phaseResolvedDataID, MonitorInterfaceDB db)
        {
            PDM_Data_PhaseResolvedData phaseResolvedData = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PhaseResolvedData
                    where item.ID == phaseResolvedDataID
                    select item;

                if (queryList.Count() > 0)
                {
                    phaseResolvedData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetPhaseResolvedDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedData;
        }

        public static PDM_Data_PhaseResolvedData PDM_Data_CloneOnePhaseResolvedDataDbObject(PDM_Data_PhaseResolvedData phaseResolvedData)
        {
            PDM_Data_PhaseResolvedData clonedPhaseResolvedData = null;
            try
            {
                if (phaseResolvedData != null)
                {
                    clonedPhaseResolvedData = new PDM_Data_PhaseResolvedData();

                    clonedPhaseResolvedData.ID = phaseResolvedData.ID;
                    clonedPhaseResolvedData.DataRootID = phaseResolvedData.DataRootID;
                    clonedPhaseResolvedData.MonitorID = phaseResolvedData.MonitorID;
                    clonedPhaseResolvedData.ReadingDateTime = phaseResolvedData.ReadingDateTime;

                    clonedPhaseResolvedData.ChannelNumber = phaseResolvedData.ChannelNumber;
                    clonedPhaseResolvedData.PhasePolarity = phaseResolvedData.PhasePolarity;
                    clonedPhaseResolvedData.NonZeroEntries = phaseResolvedData.NonZeroEntries;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CloneOnePhaseResolvedDataDbObject(PDM_Data_PhaseResolvedData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPhaseResolvedData;
        }

        public static List<PDM_Data_PhaseResolvedData> PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PhaseResolvedData
                    where item.DataRootID == dataRootID
                    orderby item.ChannelNumber ascending, item.PhasePolarity descending
                    select item;

                phaseResolvedDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedDataList;
        }

        /// <summary>
        /// Gets every PDM_Data_PhaseResolvedData table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_PhaseResolvedData> PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PhaseResolvedData
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending, item.ChannelNumber ascending, item.PhasePolarity descending
                    select item;

                phaseResolvedDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedDataList;
        }

        /// <summary>
        /// Gets every PDM_Data_PhaseResolvedData table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<PDM_Data_PhaseResolvedData> PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = null;
            try
            {
                var queryList =
                    from item in db.PDM_Data_PhaseResolvedData
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending, item.ChannelNumber ascending, item.PhasePolarity descending
                    select item;

                phaseResolvedDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedDataList;
        }

        public static List<PDM_Data_PhaseResolvedData> PDM_Data_ClonePhaseResolvedDataDbObjectList(List<PDM_Data_PhaseResolvedData> phaseResolvedDataList)
        {
            List<PDM_Data_PhaseResolvedData> clonedPhaseResolvedDataList = null;
            try
            {
                PDM_Data_PhaseResolvedData phaseResolvedDataEntry;
                if (phaseResolvedDataList != null)
                {
                    clonedPhaseResolvedDataList = new List<PDM_Data_PhaseResolvedData>();
                    foreach (PDM_Data_PhaseResolvedData entry in phaseResolvedDataList)
                    {
                        phaseResolvedDataEntry = PDM_Data_CloneOnePhaseResolvedDataDbObject(entry);
                        if (phaseResolvedDataEntry != null)
                        {
                            clonedPhaseResolvedDataList.Add(phaseResolvedDataEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_ClonePhaseResolvedDataDbObjectList(List<PDM_Data_PhaseResolvedData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPhaseResolvedDataList;
        }

        public static bool PDM_Data_CopyPhaseResolvedDataForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneDataRoot(dataRootID, sourceDB);
                if (phaseResolvedDataList != null)
                {
                    if (phaseResolvedDataList.Count > 0)
                    {
                        success = PDM_Data_WritePhaseResolvedDataTableEntries(PDM_Data_ClonePhaseResolvedDataDbObjectList(phaseResolvedDataList), destinationDB);
                        if (!success)
                        {
                            string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyPhaseResolvedDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the List<PDM_Data_PhaseResolvedData> to the destination database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        // empty list will be returned if the call was successful but no data were found.
                        success = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_DatabaseMethods.PDM_Data_CopyPhaseResolvedDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the List<PDM_Data_PhaseResolvedData> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_Data_CopyPhaseResolvedDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        public static bool PDM_Data_CopyOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = PDM_Data_CopyDataRootForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                if (success)
                {
                    success = PDM_Data_CopyChPDParametersForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
                if (success)
                {
                    success = PDM_Data_CopyParametersForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
                if (success)
                {
                    success = PDM_Data_CopyPDParametersForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
                if (success)
                {
                    success = PDM_Data_CopyPhaseResolvedDataForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static ErrorCode PDM_Data_CopyAllDataForOneMonitorToNewDatabase(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                bool failedToClonePhaseResolvedData = false;
                bool writePhaseResolvedData = false;

                List<PDM_Data_DataRoot> clonedDataRootList = null;
                List<PDM_Data_Parameters> clonedParametersList = null;
                List<PDM_Data_PDParameters> clonedPDParametersList = null;
                List<PDM_Data_ChPDParameters> clonedChPDParametersList = null;
                List<PDM_Data_PhaseResolvedData> clonedPhaseResolvedDataList = null;

                List<PDM_Data_Parameters> parametersList = null;
                List<PDM_Data_PDParameters> pdParametersList = null;
                List<PDM_Data_ChPDParameters> chPDParametersList = null;
                List<PDM_Data_PhaseResolvedData> phaseResolvedDataList = null;

                List<PDM_Data_DataRoot> dataRootList = PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, sourceDB);
                if (dataRootList != null)
                {
                    clonedDataRootList = PDM_Data_CloneDataRootDbObjectList(dataRootList);
                    if ((clonedDataRootList != null) && (clonedDataRootList.Count > 0))
                    {
                        parametersList = PDM_Data_GetAllParametersTableEntriesForOneMonitor(monitorID, sourceDB);
                        pdParametersList = PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(monitorID, sourceDB);
                        chPDParametersList = PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(monitorID, sourceDB);
                        phaseResolvedDataList = PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(monitorID, sourceDB);
                        if ((parametersList != null) && (parametersList.Count > 0))
                        {
                            if ((pdParametersList != null) && (pdParametersList.Count > 0))
                            {
                                if ((chPDParametersList != null) && (chPDParametersList.Count > 0))
                                {
                                    /// Now that we are sure we have the necessary (we don't know if it's sufficient, as in complete data for every reading, we just assume it is) amount of data to make up
                                    /// proper data entries for a PDM, we can clone the data and add it to the new db.  We have to clone because VS will object if you
                                    /// try to insert an object taken from one database into another database directly
                                    clonedParametersList = PDM_Data_CloneParametersDbObjectList(parametersList);
                                    if ((clonedParametersList != null) && (clonedParametersList.Count > 0))
                                    {
                                        clonedPDParametersList = PDM_Data_ClonePDParametersDbObjectList(pdParametersList);
                                        if ((clonedPDParametersList != null) && (clonedPDParametersList.Count > 0))
                                        {
                                            clonedChPDParametersList = PDM_Data_CloneChPDParametersDbObjectList(chPDParametersList);
                                            if ((clonedChPDParametersList != null) && (clonedChPDParametersList.Count > 0))
                                            {
                                                /// One never knows if phase resolved data is present.  It probably is, but it isn't necessarily present.  What we'll do is handle it if it's present,
                                                /// and if there is data present that we fail to clone, we will call it an error and not copy any data to the new db.
                                                if ((phaseResolvedDataList != null) && (phaseResolvedDataList.Count > 0))
                                                {
                                                    clonedPhaseResolvedDataList = PDM_Data_ClonePhaseResolvedDataDbObjectList(phaseResolvedDataList);
                                                    if ((clonedPhaseResolvedDataList != null) && (clonedPhaseResolvedDataList.Count > 0))
                                                    {
                                                        writePhaseResolvedData = true;
                                                    }
                                                    else
                                                    {
                                                        failedToClonePhaseResolvedData = true;
                                                        errorCode = ErrorCode.NoPhaseResolvedDataEntriesFound;
                                                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone phase resolved data";
                                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                                        MessageBox.Show(errorMessage);
#endif
                                                    }
                                                }
                                                else
                                                {
                                                    // let's not call this an error, because it may not be one
                                                    // errorCode = ErrorCode.NoPhaseResolvedDataEntriesFound;
                                                }

                                                /// here is where we add the data to the export db
                                                if (!failedToClonePhaseResolvedData)
                                                {
                                                    errorCode = ErrorCode.DatabaseWriteFailed;
                                                    /// The way I set up the db, I need to have data root entries for each data item present before putting in the other data.  I don't know
                                                    /// if it's necessary to call SubmitChanges() twice or not, but I know it's correct.
                                                    destinationDB.PDM_Data_DataRoot.InsertAllOnSubmit(clonedDataRootList);
                                                    destinationDB.SubmitChanges();

                                                    destinationDB.PDM_Data_Parameters.InsertAllOnSubmit(clonedParametersList);
                                                    destinationDB.PDM_Data_PDParameters.InsertAllOnSubmit(clonedPDParametersList);
                                                    destinationDB.PDM_Data_ChPDParameters.InsertAllOnSubmit(clonedChPDParametersList);

                                                    if (writePhaseResolvedData)
                                                    {
                                                        destinationDB.PDM_Data_PhaseResolvedData.InsertAllOnSubmit(clonedPhaseResolvedDataList);
                                                    }

                                                    destinationDB.SubmitChanges();

                                                    errorCode = ErrorCode.DatabaseWriteSucceeded;
                                                }
                                            }
                                            else
                                            {
                                                errorCode = ErrorCode.NoChPDParametersEntriesFound;
                                                string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone ChPDParameters data";
                                                LogMessage.LogError(errorMessage);
#if DEBUG
                                                MessageBox.Show(errorMessage);
#endif
                                            }
                                        }
                                        else
                                        {
                                            errorCode = ErrorCode.NoPDParametersEntriesFound;
                                            string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone pdParameters data";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    else
                                    {
                                        errorCode = ErrorCode.NoParametersEntriesFound;
                                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone parameters data";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    errorCode = ErrorCode.NoChPDParametersEntriesFound;
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.NoPDParametersEntriesFound;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.NoParametersEntriesFound;
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.NoDataRootEntriesFound;
                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone data root list";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    errorCode = ErrorCode.NoDataRootEntriesFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

//        public static ErrorCode PDM_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
//        {
//            ErrorCode errorCode = ErrorCode.None;
//            try
//            {
//                bool failedToClonePhaseResolvedData = false;
//                bool writePhaseResolvedData = false;

//                List<PDM_Config_ConfigurationRoot> clonedConfigurationRootList = null;
//                List<PDM_Config_Parameters> clonedParametersList = null;
//                List<PDM_Config_PDParameters> clonedPDParametersList = null;
//                List<PDM_Config_ChPDParameters> clonedChPDParametersList = null;
//                List<PDM_Config_PhaseResolvedData> clonedPhaseResolvedDataList = null;

//                List<PDM_Config_Parameters> parametersList = null;
//                List<PDM_Config_PDParameters> pdParametersList = null;
//                List<PDM_Config_ChPDParameters> chPDParametersList = null;
//                List<PDM_Config_PhaseResolvedData> phaseResolvedDataList = null;

//                List<PDM_Config_ConfigurationRoot> ConfigurationRootList = PDM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitorID, sourceDB);
//                if (ConfigurationRootList != null)
//                {
//                    clonedConfigurationRootList = PDM_Config_CloneConfigurationRootDbObjectList(ConfigurationRootList);
//                    if ((clonedConfigurationRootList != null) && (clonedConfigurationRootList.Count > 0))
//                    {
//                        parametersList = PDM_Config_GetAllParametersTableEntriesForOneMonitor(monitorID, sourceDB);
//                        pdParametersList = PDM_Config_GetAllPDParametersTableEntriesForOneMonitor(monitorID, sourceDB);
//                        chPDParametersList = PDM_Config_GetAllChPDParametersTableEntriesForOneMonitor(monitorID, sourceDB);
//                        phaseResolvedDataList = PDM_Config_GetAllPhaseResolvedDataTableEntriesForOneMonitor(monitorID, sourceDB);
//                        if ((parametersList != null) && (parametersList.Count > 0))
//                        {
//                            if ((pdParametersList != null) && (pdParametersList.Count > 0))
//                            {
//                                if ((chPDParametersList != null) && (chPDParametersList.Count > 0))
//                                {
//                                    /// Now that we are sure we have the necessary (we don't know if it's sufficient, as in complete data for every reading, we just assume it is) amount of data to make up
//                                    /// proper data entries for a PDM, we can clone the data and add it to the new db.  We have to clone because VS will object if you
//                                    /// try to insert an object taken from one database into another database directly
//                                    clonedParametersList = PDM_Config_CloneParametersDbObjectList(parametersList);
//                                    if ((clonedParametersList != null) && (clonedParametersList.Count > 0))
//                                    {
//                                        clonedPDParametersList = PDM_Config_ClonePDParametersDbObjectList(pdParametersList);
//                                        if ((clonedPDParametersList != null) && (clonedPDParametersList.Count > 0))
//                                        {
//                                            clonedChPDParametersList = PDM_Config_CloneChPDParametersDbObjectList(chPDParametersList);
//                                            if ((clonedChPDParametersList != null) && (clonedChPDParametersList.Count > 0))
//                                            {
//                                                /// One never knows if phase resolved data is present.  It probably is, but it isn't necessarily present.  What we'll do is handle it if it's present,
//                                                /// and if there is data present that we fail to clone, we will call it an error and not copy any data to the new db.
//                                                if ((phaseResolvedDataList != null) && (phaseResolvedDataList.Count > 0))
//                                                {
//                                                    clonedPhaseResolvedDataList = PDM_Config_ClonePhaseResolvedDataDbObjectList(phaseResolvedDataList);
//                                                    if ((clonedPhaseResolvedDataList != null) && (clonedPhaseResolvedDataList.Count > 0))
//                                                    {
//                                                        writePhaseResolvedData = true;
//                                                    }
//                                                    else
//                                                    {
//                                                        failedToClonePhaseResolvedData = true;
//                                                        errorCode = ErrorCode.NoPhaseResolvedDataEntriesFound;
//                                                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone phase resolved data";
//                                                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                                                        MessageBox.Show(errorMessage);
//#endif
//                                                    }
//                                                }
//                                                else
//                                                {
//                                                    // let's not call this an error, because it may not be one
//                                                    // errorCode = ErrorCode.NoPhaseResolvedDataEntriesFound;
//                                                }

//                                                /// here is where we add the data to the export db
//                                                if (!failedToClonePhaseResolvedData)
//                                                {
//                                                    /// The way I set up the db, I need to have data root entries for each data item present before putting in the other data.  I don't know
//                                                    /// if it's necessary to call SubmitChanges() twice or not, but I know it's correct.
//                                                    destinationDB.PDM_Config_ConfigurationRoot.InsertAllOnSubmit(ConfigurationRootList);
//                                                    destinationDB.SubmitChanges();

//                                                    destinationDB.PDM_Config_Parameters.InsertAllOnSubmit(clonedParametersList);
//                                                    destinationDB.PDM_Config_PDParameters.InsertAllOnSubmit(clonedPDParametersList);
//                                                    destinationDB.PDM_Config_ChPDParameters.InsertAllOnSubmit(clonedChPDParametersList);

//                                                    if (writePhaseResolvedData)
//                                                    {
//                                                        destinationDB.PDM_Config_PhaseResolvedData.InsertAllOnSubmit(clonedPhaseResolvedDataList);
//                                                    }

//                                                    destinationDB.SubmitChanges();
//                                                }
//                                            }
//                                            else
//                                            {
//                                                errorCode = ErrorCode.NoChPDParametersEntriesFound;
//                                                string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone ChPDParameters data";
//                                                LogMessage.LogError(errorMessage);
//#if DEBUG
//                                                MessageBox.Show(errorMessage);
//#endif
//                                            }
//                                        }
//                                        else
//                                        {
//                                            errorCode = ErrorCode.NoPDParametersEntriesFound;
//                                            string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone pdParameters data";
//                                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                                            MessageBox.Show(errorMessage);
//#endif
//                                        }
//                                    }
//                                    else
//                                    {
//                                        errorCode = ErrorCode.NoParametersEntriesFound;
//                                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone parameters data";
//                                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                                        MessageBox.Show(errorMessage);
//#endif
//                                    }
//                                }
//                                else
//                                {
//                                    errorCode = ErrorCode.NoChPDParametersEntriesFound;
//                                }
//                            }
//                            else
//                            {
//                                errorCode = ErrorCode.NoPDParametersEntriesFound;
//                            }
//                        }
//                        else
//                        {
//                            errorCode = ErrorCode.NoParametersEntriesFound;
//                        }
//                    }
//                    else
//                    {
//                        errorCode = ErrorCode.NoConfigurationRootEntriesFound;
//                        string errorMessage = "Error in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone data root list";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    errorCode = ErrorCode.NoConfigurationRootEntriesFound;
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in PDM_DatabaseMethods.PDM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return errorCode;
//        }






        #endregion // PDM Data Methods

        #endregion // PDM Specific Methods

        public static Guid GetGuidForTemplateConfigurationMonitorID()
        {
            Guid fakeMonitorID = new Guid("50500000000000000000000000000000");
            return fakeMonitorID;
        }

        public static Monitor CreateNewTemplateMonitor()
        {
            Monitor partialDischargeMonitor = null;
            try
            {
                partialDischargeMonitor = new Monitor();
                partialDischargeMonitor.ID = PDM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                partialDischargeMonitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                partialDischargeMonitor.MonitorType = "PDM";
                partialDischargeMonitor.ConnectionType = String.Empty;
                partialDischargeMonitor.BaudRate = string.Empty;
                partialDischargeMonitor.ModbusAddress = "20";
                partialDischargeMonitor.Enabled = false;
                partialDischargeMonitor.IPaddress = "000.000.000.000";
                partialDischargeMonitor.PortNumber = 502;
                partialDischargeMonitor.MonitorNumber = 2;
                partialDischargeMonitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_DatabaseMethods.CreateNewTemplateMonitor()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return partialDischargeMonitor;
        }
    }
}
