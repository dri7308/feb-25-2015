﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DatabaseInterface
{
    public class MonitorHierarchy
    {
        public ErrorCode errorCode = ErrorCode.None;
        public Monitor monitor = null;
        public Equipment equipment = null;
        public Plant plant = null;
        public Company company = null;
    }

    public class General_DatabaseMethods
    {
        private static string companyNameForTemplates = "companyNameForTemplates";
        public static string CompanyNameForTemplates
        {
            get
            {
                return companyNameForTemplates;
            }
        }

        private static string plantNameForTemplates = "plantNameForTemplates";
        public static string PlantNameForTemplates
        {
            get
            {
                return plantNameForTemplates;
            }
        }

        private static string equipmentNameForTemplates = "equipmentNameForTemplates";
        public static string EquipmentNameForTemplates
        {
            get
            {
                return equipmentNameForTemplates;
            }
        }

        public static DateTime MinimumDateTime()
        {
            return new DateTime(2000, 1, 1);
        }

        public static DateTime MaximumDateTime()
        {
            return new DateTime(3000, 12, 31);
        }

        #region Company

        /// <summary>
        /// Returns the Company object that has the input companyID as its ID
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Company GetCompany(Guid companyID, MonitorInterfaceDB db)
        {
            Company company = null;
            try
            {
                var companyQuery =
                    from item in db.Company
                    where item.ID == companyID
                    select item;

                if (companyQuery.Count() > 0)
                {
                    company = companyQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetCompany(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return company;
        }

        public static List<Company> GetCompanies(List<Guid> companyIDs, MonitorInterfaceDB db)
        {
            List<Company> companyList = null;
            try
            {
                Company currentCompany;
                if ((companyIDs != null) && (companyIDs.Count > 0))
                {
                    companyList = new List<Company>();
                    foreach (Guid companyID in companyIDs)
                    {
                        currentCompany = GetCompany(companyID, db);
                        if (currentCompany != null)
                        {
                            companyList.Add(currentCompany);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetCompany(List<Guid>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return companyList;
        }

        public static List<Company> GetCompaniesAssociatedWithInputPlants(List<Plant> plantList, MonitorInterfaceDB db)
        {
            List<Company> companyList = null;
            try
            {
                Company company;
                if (plantList != null)
                {
                    companyList = new List<Company>();
                    foreach (Plant plant in plantList)
                    {
                        company = GetCompany(plant.CompanyID, db);
                        if ((company != null) && (!companyList.Contains(company)))
                        {
                            companyList.Add(company);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetCompaniesAssociatedWithInputPlants(List<Plant>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return companyList;
        }

        public static List<Company> GetAllCompanies(MonitorInterfaceDB db)
        {
            List<Company> companyList = null;
            try
            {
                var companyQuery =
                   from item in db.Company
                   select item;

                if (companyQuery.Count() > 0)
                {
                    companyList = companyQuery.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllCompanies(MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return companyList;
        }

        public static Company CloneOneCompanyDbObject(Company company)
        {
            Company outputCompany = null;
            try
            {
                if (company != null)
                {
                    outputCompany = new Company();

                    outputCompany.ID = company.ID;
                    outputCompany.Name = company.Name;
                    outputCompany.Address = company.Address;
                    outputCompany.Comments = company.Comments;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneOneCompanyDbObject(Company)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return outputCompany;
        }

        public static List<Company> CloneCompanyDbObjectList(List<Company> companyList)
        {
            List<Company> cloneCompanyList = null;
            try
            {
                Company cloneCompany;
                if (companyList != null)
                {
                    cloneCompanyList = new List<Company>();
                    foreach (Company company in companyList)
                    {
                        cloneCompany = CloneOneCompanyDbObject(company);
                        if (cloneCompany != null)
                        {
                            cloneCompanyList.Add(cloneCompany);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneCompanyDbObjectList(List<Company>)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return cloneCompanyList;
        }


        /// <summary>
        /// Writes on Company object to the database
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool CompanyWrite(Company company, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Company.InsertOnSubmit(company);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CompanyWrite(Company, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Company object from the database, which will delete all associated Plants, Equipment, Monitors, and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool CompanyDelete(Company company, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Company.DeleteOnSubmit(company);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CompanyDelete(Company, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Company object from the database, which will delete all associated Plants, Equipment, Monitors, and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool CompanyDelete(Guid companyID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Company company = GetCompany(companyID, db);
                if (company != null)
                {
                    db.Company.DeleteOnSubmit(company);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CompanyDelete(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        public static Dictionary<Guid, int> GetSelectedCompanies(Dictionary<Guid, int> selectedPlants, MonitorInterfaceDB db)
        {
            Dictionary<Guid, int> selectedCompanies = new Dictionary<Guid, int>();
            try
            {
                Guid companyID;
                Plant plant;
                foreach (KeyValuePair<Guid, int> kvPair in selectedPlants)
                {
                    plant = GetPlant(kvPair.Key, db);
                    if (plant != null)
                    {
                        companyID = plant.CompanyID;
                        if (!selectedCompanies.ContainsKey(companyID))
                        {
                            selectedCompanies.Add(companyID, 1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetSelectedCompanies(Dictionary<Guid, int>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedCompanies;
        }

        public static Guid GetGuidForTemplateConfigurationCompanyID()
        {
            Guid fakeCompanyID = new Guid("11111111111111111111111111111111");
            return fakeCompanyID;
        }

        public static Company CreateNewTemplateCompany()
        {
            Company company = null;
            try
            {
                company = new Company();
                company.ID = GetGuidForTemplateConfigurationCompanyID();
                company.Name = "Template";
                company.Address = string.Empty;
                company.Comments = string.Empty;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CreateNewTemplateCompany()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return company;
        }


        #endregion

        #region Plant

        /// <summary>
        /// Returns the Plant object that has the input plantID as its ID
        /// </summary>
        /// <param name="plantID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Plant GetPlant(Guid plantID, MonitorInterfaceDB db)
        {
            Plant plant = null;

            try
            {
                var plantQuery =
                    from item in db.Plant
                    where item.ID == plantID
                    select item;

                if (plantQuery.Count() > 0)
                {
                    plant = plantQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetPlant(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return plant;
        }

        public static List<Plant> GetPlants(List<Guid> plantIDs, MonitorInterfaceDB db)
        {
            List<Plant> plantsList = null;
            try
            {
                Plant currentPlant;
                if ((plantIDs != null) && (plantIDs.Count > 0))
                {
                    foreach (Guid plantID in plantIDs)
                    {
                        currentPlant = GetPlant(plantID, db);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetPlants(List<Guid>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return plantsList;
        }

        public static List<Plant> GetAllPlants(MonitorInterfaceDB db)
        {
            List<Plant> plantsList = null;
            try
            {
                var plantQuery =
                    from item in db.Plant
                    select item;

                if (plantQuery.Count() > 0)
                {
                    plantsList = plantQuery.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllPlants(MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return plantsList;
        }

        public static List<Plant> GetPlantsAssociatedWithInputEquipment(List<Equipment> equipmentList, MonitorInterfaceDB db)
        {
            List<Plant> plantsList = null;
            try
            {
                Plant plant;
                if (equipmentList != null)
                {
                    plantsList = new List<Plant>();
                    foreach (Equipment equipment in equipmentList)
                    {
                        plant = GetPlant(equipment.PlantID, db);
                        if ((plant != null) && (!plantsList.Contains(plant)))
                        {
                            plantsList.Add(plant);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetPlantsAssociatedWithInputEquipment(List<Equipment>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return plantsList;
        }

        public static Plant CloneOnePlantDbObject(Plant plant)
        {
            Plant outputPlant = null;
            try
            {
                if (plant != null)
                {
                    outputPlant = new Plant();

                    outputPlant.ID = plant.ID;
                    outputPlant.CompanyID = plant.CompanyID;
                    outputPlant.Name = plant.Name;
                    outputPlant.Address = plant.Address;
                    outputPlant.Comments = plant.Comments;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneOnePlantDbObject(Plant)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return outputPlant;
        }

        public static List<Plant> ClonePlantDbObjectList(List<Plant> plantList)
        {
            List<Plant> clonedPlantList = null;
            try
            {
                Plant clonedPlant;
                if (plantList != null)
                {
                    clonedPlantList = new List<Plant>();
                    foreach (Plant plant in plantList)
                    {
                        clonedPlant = CloneOnePlantDbObject(plant);
                        if (clonedPlant != null)
                        {
                            clonedPlantList.Add(clonedPlant);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.ClonePlantDbObjectList(List<Plant>)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return clonedPlantList;
        }

        /// <summary>
        /// Writes one Plant object to the database
        /// </summary>
        /// <param name="plant"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool PlantWrite(Plant plant, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Plant.InsertOnSubmit(plant);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PlantWrite(Plant, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Plant object from the database, which will delete all associated Equipment, Monitors, and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool PlantDelete(Plant plant, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Plant.DeleteOnSubmit(plant);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PlantDelete(Plant, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Plant object from the database, which will delete all associated Equipment, Monitors, and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool PlantDelete(Guid plantID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Plant plant = General_DatabaseMethods.GetPlant(plantID, db);
                if (plant != null)
                {
                    db.Plant.DeleteOnSubmit(plant);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PlantDelete(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        public static Dictionary<Guid, int> GetSelectedPlants(Dictionary<Guid, int> selectedEquipment, MonitorInterfaceDB db)
        {
            Dictionary<Guid, int> selectedPlants = new Dictionary<Guid, int>();
            try
            {
                Guid plantID;
                Equipment equipment;
                foreach (KeyValuePair<Guid, int> kvPair in selectedEquipment)
                {
                    equipment = GetOneEquipment(kvPair.Key, db);
                    if (equipment != null)
                    {
                        plantID = equipment.PlantID;
                        if (!selectedPlants.ContainsKey(plantID))
                        {
                            selectedPlants.Add(plantID, 1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetSelectedPlants(Dictionary<Guid, int>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedPlants;
        }

        public static Guid GetGuidForTemplateConfigurationPlantID()
        {
            Guid fakePlantID = new Guid("22222222222222222222222222222222");
            return fakePlantID;
        }

        public static Plant CreateNewTemplatePlant()
        {
            Plant plant = null;
            try
            {
                plant = new Plant();
                plant.ID = GetGuidForTemplateConfigurationPlantID();
                plant.CompanyID = GetGuidForTemplateConfigurationCompanyID();
                plant.Name = "Template";
                plant.Address = string.Empty;
                plant.Longitude = string.Empty;
                plant.Latitude = string.Empty;
                plant.Comments = string.Empty;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CreateNewTemplatePlant()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return plant;
        }

        #endregion

        #region Equipment

        /// <summary>
        /// Returns the Equipment object that has the input equipmentID as its ID
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Equipment GetOneEquipment(Guid equipmentID, MonitorInterfaceDB db)
        {
            Equipment equipment = null;

            try
            {
                var equipmentQuery =
                    from item in db.Equipment
                    where item.ID == equipmentID
                    select item;

                if (equipmentQuery.Count() > 0)
                {
                    equipment = equipmentQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return equipment;
        }

        public static List<Equipment> GetEquipment(List<Guid> equipmentIDs, MonitorInterfaceDB db)
        {
            List<Equipment> equipmentList = null;
            try
            {
                Equipment currentEquipment;
                if ((equipmentIDs != null) && (equipmentIDs.Count > 0))
                {
                    equipmentList = new List<Equipment>();
                    foreach (Guid equipmentID in equipmentIDs)
                    {
                        currentEquipment = GetOneEquipment(equipmentID, db);
                        if (currentEquipment != null)
                        {
                            equipmentList.Add(currentEquipment);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetEquipment(List<Guid>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return equipmentList;
        }

        public static List<Equipment> GetEquipmentAssociatedWithInputMonitors(List<Monitor> monitorList, MonitorInterfaceDB db)
        {
            List<Equipment> equipmentList = null;
            try
            {
                Equipment equipment;
                if (monitorList != null)
                {
                    equipmentList = new List<Equipment>();
                    foreach (Monitor monitor in monitorList)
                    {
                        equipment = GetOneEquipment(monitor.EquipmentID, db);
                        if ((equipment != null) && (!equipmentList.Contains(equipment)))
                        {
                            equipmentList.Add(equipment);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetEquipmentAssociatedWithInputMonitors(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return equipmentList;
        }

        //public static List<Equipment> GetAllEquipment(MonitorInterfaceDB db)
        //{
        //    List<Equipment> equipmentList = null;
        //    try
        //    {
        //        var equipmentQuery =
        //            from item in db.Equipment
        //            select item;

        //        if (equipmentQuery.Count() > 0)
        //        {
        //            equipmentList = equipmentQuery.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllEquipment(MonitorInterfaceDB)\nMessage: " + ex;

        //        LogMessage.LogError(errorMessage);
        //    }
        //    return equipmentList;
        //}



        public static Equipment CloneOneEquipmentDbObject(Equipment equipment)
        {
            Equipment outputEquipment = null;
            try
            {
                if (equipment != null)
                {
                    outputEquipment = new Equipment();

                    outputEquipment.ID = equipment.ID;
                    outputEquipment.PlantID = equipment.PlantID;
                    outputEquipment.Name = equipment.Name;
                    outputEquipment.Tag = equipment.Tag;
                    outputEquipment.SerialNumber = equipment.SerialNumber;
                    outputEquipment.Voltage = equipment.Voltage;
                    outputEquipment.Type = equipment.Type;
                    outputEquipment.Comments = equipment.Comments;
                    outputEquipment.Enabled = equipment.Enabled;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneOneEquipmentDbObject(Equipment)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return outputEquipment;
        }

        public static List<Equipment> CloneEquipmentDbObjectList(List<Equipment> equipmentList)
        {
            List<Equipment> cloneEquipmentList = null;
            try
            {
                Equipment cloneEquipment;
                if (equipmentList != null)
                {
                    cloneEquipmentList = new List<Equipment>();
                    foreach (Equipment equipment in equipmentList)
                    {
                        cloneEquipment = CloneOneEquipmentDbObject(equipment);
                        if (cloneEquipment != null)
                        {
                            cloneEquipmentList.Add(cloneEquipment);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneEquipmentDbObjectList(List<Equipment>)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return cloneEquipmentList;
        }

        /// <summary>
        /// Writes one Equipment object to the database
        /// </summary>
        /// <param name="equipment"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool EquipmentWrite(Equipment equipment, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Equipment.InsertOnSubmit(equipment);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.EquipmentWrite(Equipment, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Equipment object from the database, which will delete all associated Monitors and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool EquipmentDelete(Equipment equipment, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Equipment.DeleteOnSubmit(equipment);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.EquipmentDelete(Equipment, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Deletes one Equipment object from the database, which will delete all associated Monitors and data
        /// as well.
        /// </summary>
        /// <param name="company"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool EquipmentDelete(Guid equipmentID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Equipment equipment = GetOneEquipment(equipmentID, db);
                if (equipment != null)
                {
                    db.Equipment.DeleteOnSubmit(equipment);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.EquipmentDelete(Guid, MonitorInterfaceDB)\nMessage: " + ex;

                LogMessage.LogError(errorMessage);
            }
            return success;
        }

        /// <summary>
        /// Returns the List of Equipment for all active (enabled) equipment in the database
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Equipment> GetAllActiveEquipment(MonitorInterfaceDB db)
        {
            List<Equipment> activeEquipment = null;
            try
            {
                List<Equipment> equipmentList =
                    (from equipment in db.Equipment
                     where equipment.Enabled == true
                     select equipment).ToList();

                activeEquipment = equipmentList;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods:GetAllActiveEquipment(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeEquipment;
        }

        /// <summary>
        /// Returns the List of all Equipment in the database
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Equipment> GetAllEquipment(MonitorInterfaceDB db)
        {
            List<Equipment> allEquipment = null;
            try
            {
                List<Equipment> equipmentList =
                    (from equipment in db.Equipment
                     select equipment).ToList();

                allEquipment = equipmentList;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods:GetAllActiveEquipment(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allEquipment;
        }

        /// <summary>
        /// Changes the enabled state one equipment to reflect the input boolean
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="enabled"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool ChangeEquipmentEnabledState(Guid equipmentID, bool enabled, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Equipment equipment;
                var equipmentQuery =
                    from item in db.Equipment
                    where item.ID == equipmentID
                    select item;

                if (equipmentQuery.Count() > 0)
                {
                    equipment = equipmentQuery.First();
                    equipment.Enabled = enabled;
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.ChangeEquipmentEnabledState(Guid, bool, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Changes the enabled state of all equipment whose ID is in the input dictionary to the associated boolean in the dictionary
        /// </summary>
        /// <param name="equipmentCheckstate"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool ChangeEquipmentEnabledState(Dictionary<Guid, bool> equipmentCheckstate, MonitorInterfaceDB db)
        {
            bool success = false;

            try
            {
                Guid equipmentID;

                List<Equipment> equipmentList =
                    (from item in db.Equipment
                     select item).ToList();

                for (int i = 0; i < equipmentList.Count; i++)
                {
                    equipmentID = equipmentList[i].ID;
                    if (equipmentCheckstate.ContainsKey(equipmentID))
                    {
                        equipmentList[i].Enabled = equipmentCheckstate[equipmentID];
                    }
                }
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.ChangeEquipmentEnabledState(Dictionary<Guid, bool>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Dictionary<Guid, int> GetSelectedEquipment(Dictionary<Guid, int> selectedMonitors, MonitorInterfaceDB db)
        {
            Dictionary<Guid, int> selectedEquipment = new Dictionary<Guid, int>();
            try
            {
                Guid equipmentID;
                Monitor monitor;
                foreach (KeyValuePair<Guid, int> kvPair in selectedMonitors)
                {
                    monitor = GetOneMonitor(kvPair.Key, db);
                    if (monitor != null)
                    {
                        equipmentID = monitor.EquipmentID;
                        if (!selectedEquipment.ContainsKey(equipmentID))
                        {
                            selectedEquipment.Add(equipmentID, 1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetSelectedEquipment(Dictionary<Guid, int>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedEquipment;
        }

        public static Guid GetGuidForTemplateConfigurationEquipmentID()
        {
            Guid fakeCompanyID = new Guid("33333333333333333333333333333333");
            return fakeCompanyID;
        }

        public static Equipment CreateNewTemplateEquipment()
        {
            Equipment equipment = null;
            try
            {
                equipment = new Equipment();
                equipment.ID = GetGuidForTemplateConfigurationEquipmentID();
                equipment.PlantID = GetGuidForTemplateConfigurationPlantID();
                equipment.Name = "Template";
                equipment.Tag = string.Empty;
                equipment.SerialNumber = string.Empty;
                equipment.Voltage = 0.0;
                equipment.Type = string.Empty;
                equipment.Comments = string.Empty;
                equipment.Enabled = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CreateNewTemplateEquipment()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return equipment;
        }

        #endregion

        #region Monitor

        /// <summary>
        /// Writes one Monitor object to the database
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool MonitorWrite(Monitor monitor, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Monitor.InsertOnSubmit(monitor);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorWrite(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one Monitor object from the database, along with all data associated with the Monitor object
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool MonitorDelete(Monitor monitor, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.Monitor.DeleteOnSubmit(monitor);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorDelete(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one Monitor object from the database, along with all data associated with the Monitor object
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool MonitorDelete(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Monitor monitor = GetOneMonitor(monitorID, db);
                if (monitor != null)
                {
                    db.Monitor.DeleteOnSubmit(monitor);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorDelete(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool MonitorDelete(List<Monitor> monitorList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {

                if ((monitorList != null) && (monitorList.Count > 0))
                {
                    db.Monitor.DeleteAllOnSubmit(monitorList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorDelete(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the Monitor object whose ID is the same as the input monitorID
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Monitor GetOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            Monitor monitor = null;
            try
            {
                var monitorQuery =
                    from item in db.Monitor
                    where item.ID == monitorID
                    select item;

                if (monitorQuery.Count() > 0)
                {
                    monitor = monitorQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitor;
        }

        public static List<Monitor> GetMonitors(List<Guid> monitorIDs, MonitorInterfaceDB db)
        {
            List<Monitor> monitorList = null;
            try
            {
                Monitor currentMonitor;
                if ((monitorIDs != null) && (monitorIDs.Count > 0))
                {
                    monitorList = new List<Monitor>();
                    foreach (Guid monitorID in monitorIDs)
                    {
                        currentMonitor = GetOneMonitor(monitorID, db);
                        if (currentMonitor != null)
                        {
                            monitorList.Add(currentMonitor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetMonitors(List<Guid>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorList;
        }

        public static List<Monitor> GetAllMonitors(MonitorInterfaceDB db)
        {
            List<Monitor> monitorList = null;
            try
            {
                var monitorQuery =
                    from item in db.Monitor
                    select item;

                if (monitorQuery.Count() > 0)
                {
                    monitorList = monitorQuery.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorList;
        }

        public static Monitor CloneOneMonitorDbObject(Monitor monitor)
        {
            Monitor outputMonitor = null;
            try
            {
                if (monitor != null)
                {
                    outputMonitor = new Monitor();

                    outputMonitor.ID = monitor.ID;
                    outputMonitor.EquipmentID = monitor.EquipmentID;
                    outputMonitor.MonitorType = monitor.MonitorType;
                    outputMonitor.ConnectionType = monitor.ConnectionType;
                    outputMonitor.BaudRate = monitor.BaudRate;
                    outputMonitor.ModbusAddress = monitor.ModbusAddress;
                    outputMonitor.Enabled = monitor.Enabled;
                    outputMonitor.IPaddress = monitor.IPaddress;
                    outputMonitor.PortNumber = monitor.PortNumber;
                    outputMonitor.MonitorNumber = monitor.MonitorNumber;
                    outputMonitor.DateOfLastDataDownload = monitor.DateOfLastDataDownload;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneOneMonitorDbObject(Monitor)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputMonitor;
        }

        public static List<Monitor> CloneMonitorDbObjectList(List<Monitor> monitorList)
        {
            List<Monitor> cloneMonitorList = null;
            try
            {
                Monitor cloneMonitor;
                if (monitorList != null)
                {
                    cloneMonitorList = new List<Monitor>();
                    foreach (Monitor monitor in monitorList)
                    {
                        cloneMonitor = CloneOneMonitorDbObject(monitor);
                        if (cloneMonitor != null)
                        {
                            cloneMonitorList.Add(cloneMonitor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CloneMonitorDbObjectList(List<Monitor>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return cloneMonitorList;
        }

        public static Monitor GetAssociatedMainMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            Monitor monitor = null;
            try
            {
                monitor = GetAssociatedMainMonitor(GetOneMonitor(monitorID, db), db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAssociatedMainMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitor;
        }

        public static Monitor GetAssociatedMainMonitor(Monitor inputMonitor, MonitorInterfaceDB db)
        {
            Monitor monitor = null;
            try
            {
                Guid equipmentID = inputMonitor.EquipmentID;
                List<Monitor> monitorList = GetAllMonitorsForOneEquipment(equipmentID, db);
                foreach (Monitor entry in monitorList)
                {
                    if (entry.MonitorType.Trim().CompareTo("Main") == 0)
                    {
                        monitor = entry;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAssociatedMainMonitor(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitor;
        }

        /// <summary>
        /// Returns a List of Monitor for all active (enabled) monitors in the database
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Monitor> GetAllActiveMonitors(MonitorInterfaceDB db)
        {
            List<Monitor> allActiveMonitors = null;
            try
            {
                List<Monitor> foundActiveMonitors =
                    (from monitor in db.Monitor
                     where monitor.Enabled == true
                     && (from equipment in db.Equipment
                         where equipment.ID == monitor.EquipmentID
                         && equipment.Enabled == true
                         select equipment).Count() > 0
                     select monitor).ToList();

                allActiveMonitors = foundActiveMonitors;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllActiveMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allActiveMonitors;
        }

        /// <summary>
        /// Returns all monitors associated witeh one piece of eqiupment
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Monitor> GetAllMonitorsForOneEquipment(Guid equipmentID, MonitorInterfaceDB db)
        {
            List<Monitor> allMonitors = null;
            try
            {
                List<Monitor> foundActiveMembers =
                    (from monitor in db.Monitor
                     where monitor.EquipmentID == equipmentID
                     orderby monitor.MonitorNumber ascending
                     select monitor).ToList();

                allMonitors = foundActiveMembers;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllMonitorsForOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allMonitors;
        }


        /// <summary>
        /// Returns a List of Monitor objects for one piece of equipment, such that all monitors returned are active (enabled)
        /// </summary>
        /// <param name="equipmentID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Monitor> GetAllActiveMonitorsForOneEquipment(Guid equipmentID, MonitorInterfaceDB db)
        {
            List<Monitor> allActiveMonitors = null;
            try
            {
                List<Monitor> foundActiveMembers =
                    (from monitor in db.Monitor
                     where monitor.EquipmentID == equipmentID
                     && monitor.Enabled == true
                     orderby monitor.MonitorNumber ascending
                     select monitor).ToList();

                allActiveMonitors = foundActiveMembers;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetAllActiveMonitorsForOneEquipment(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allActiveMonitors;
        }

        /// <summary>
        /// Sets the value for the DataOfLastDataDownload for one monitor to the value of the input DateTime.
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="dateToSet"></param>
        /// <param name="db"></param>
        public static void MonitorSetDownloadDate(Guid monitorID, DateTime dateToSet, MonitorInterfaceDB db)
        {
            try
            {
                Monitor foundMonitor;
                var monitorQuery =
                    from monitor in db.Monitor
                    where monitor.ID == monitorID
                    select monitor;

                if (monitorQuery.Count() > 0)
                {
                    foundMonitor = monitorQuery.First();
                    foundMonitor.DateOfLastDataDownload = dateToSet;
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorSetDownloadDate(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Preferences

        /// <summary>
        /// Returns the most recent MainDisplayPreferences from the database
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static MainDisplayPreferences MainDisplayPreferences_Load(MonitorInterfaceDB db)
        {
            MainDisplayPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.MainDisplayPreferences
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MainDisplayPreferences_Load(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static MainDisplayPreferences MainDisplayPreferences_CloneDbObject(MainDisplayPreferences preferences)
        {
            MainDisplayPreferences clonedPreferences = null;
            try
            {
                if (preferences != null)
                {
                    clonedPreferences = new MainDisplayPreferences();

                    clonedPreferences.ID = preferences.ID;
                    clonedPreferences.StartAutomatedDownloadOnStartup = preferences.StartAutomatedDownloadOnStartup;
                    clonedPreferences.SingleEquipmentAutomatedDownloadIsActive = preferences.SingleEquipmentAutomatedDownloadIsActive;
                    clonedPreferences.ManualDownloadIsActive = preferences.ManualDownloadIsActive;
                    clonedPreferences.AllEquipmentAutomatedDownloadIsActive = preferences.AllEquipmentAutomatedDownloadIsActive;
                    clonedPreferences.EquipmentSelectedForAutomatedDownload = preferences.EquipmentSelectedForAutomatedDownload;
                    clonedPreferences.CurrentSelectedEquipment = preferences.CurrentSelectedEquipment;
                    clonedPreferences.GeneralInfoDownloadHourInterval = preferences.GeneralInfoDownloadHourInterval;
                    clonedPreferences.GeneralInforDownloadMinuteInterval = preferences.GeneralInforDownloadMinuteInterval;
                    clonedPreferences.EquipmentDataDownloadHourInterval = preferences.EquipmentDataDownloadHourInterval;
                    clonedPreferences.EquipmentDataDownloadMinuteInterval = preferences.EquipmentDataDownloadMinuteInterval;
                    clonedPreferences.FileLoadInitialDirectory = preferences.FileLoadInitialDirectory;
                    clonedPreferences.DateSaved = preferences.DateSaved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MainDisplayPreferences_CloneDbObject(MainDisplayPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPreferences;
        }

        /// <summary>
        /// Saves an instance of MainDisplayPreferences to the database after deleting any previous entries
        /// </summary>
        /// <param name="preferences"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool MainDisplayPreferences_Save(MainDisplayPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var preferencesQuery =
                    from item in db.MainDisplayPreferences
                    select item;

                db.MainDisplayPreferences.DeleteAllOnSubmit(preferencesQuery);
                db.MainDisplayPreferences.InsertOnSubmit(preferences);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MainDisplayPreferences_Save(MainDisplayPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool MainDisplayPreferences_CopyToNewDatabase(MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                MainDisplayPreferences preferences = MainDisplayPreferences_Load(sourceDB);
                if (preferences != null)
                {
                    success = MainDisplayPreferences_Save(preferences, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in General_DatabaseMethods.MainDisplayPreferences_CopyToNewDatabase(MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the MainDisplayPreferences to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.MainDisplayPreferences_CopyToNewDatabase(MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the MainDisplayPreferences from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MainDisplayPreferences_CopyToNewDatabase(MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_DataViewerPreferences BHM_DataViewerPreferences_Load(Guid monitorID, MonitorInterfaceDB db)
        {
            BHM_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.BHM_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_Load(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static List<BHM_DataViewerPreferences> BHM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_DataViewerPreferences> preferencesList = null;
            try
            {
                var preferencesQuery =
                    from item in db.BHM_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                preferencesList = preferencesQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferencesList;
        }

        public static BHM_DataViewerPreferences BHM_DataViewerPreferences_CloneDbObject(BHM_DataViewerPreferences preferences)
        {
            BHM_DataViewerPreferences clonedPreferences = null;
            try
            {
                if (preferences != null)
                {
                    clonedPreferences = new BHM_DataViewerPreferences();

                    clonedPreferences.Key = preferences.Key;
                    clonedPreferences.MonitorID = preferences.MonitorID;
                    clonedPreferences.PointerSelect = preferences.PointerSelect;
                    clonedPreferences.DataAgeInMonths = preferences.DataAgeInMonths;
                    clonedPreferences.DataGroup = preferences.DataGroup;
                    clonedPreferences.CursorIsEnabled = preferences.CursorIsEnabled;
                    clonedPreferences.AutoDrawIsEnabled = preferences.AutoDrawIsEnabled;
                    clonedPreferences.ShowTrendLine = preferences.ShowTrendLine;
                    clonedPreferences.ShowMovingAverage = preferences.ShowMovingAverage;
                    clonedPreferences.ShowExponentialTrend = preferences.ShowExponentialTrend;
                    clonedPreferences.MovingAverageItemCount = preferences.MovingAverageItemCount;
                    clonedPreferences.TrendGammaSelected = preferences.TrendGammaSelected;
                    clonedPreferences.TrendGammaPhaseSelected = preferences.TrendGammaPhaseSelected;
                    clonedPreferences.TrendGammaTrendSelected = preferences.TrendGammaTrendSelected;
                    clonedPreferences.TrendFrequencySelected = preferences.TrendFrequencySelected;
                    clonedPreferences.TrendTemperatureSelected = preferences.TrendTemperatureSelected;
                    clonedPreferences.TrendBushingCurrentASelected = preferences.TrendBushingCurrentASelected;
                    clonedPreferences.TrendBushingCurrentBSelected = preferences.TrendBushingCurrentBSelected;
                    clonedPreferences.TrendBushingCurrentCSelected = preferences.TrendBushingCurrentCSelected;
                    clonedPreferences.TrendBushingCurrentPhaseAASelected = preferences.TrendBushingCurrentPhaseAASelected;
                    clonedPreferences.TrendBushingCurrentPhaseABSelected = preferences.TrendBushingCurrentPhaseABSelected;
                    clonedPreferences.TrendBushingCurrentPhaseACSelected = preferences.TrendBushingCurrentPhaseACSelected;
                    clonedPreferences.TrendCapacitanceASelected = preferences.TrendCapacitanceASelected;
                    clonedPreferences.TrendCapacitanceBSelected = preferences.TrendCapacitanceBSelected;
                    clonedPreferences.TrendCapacitanceCSelected = preferences.TrendCapacitanceCSelected;
                    clonedPreferences.TrendTangentASelected = preferences.TrendTangentASelected;
                    clonedPreferences.TrendTangentBSelected = preferences.TrendTangentBSelected;
                    clonedPreferences.TrendTangentCSelected = preferences.TrendTangentCSelected;
                    clonedPreferences.TrendTemperatureCoefficientSelected = preferences.TrendTemperatureCoefficientSelected;
                    clonedPreferences.TrendPhaseTemperatureCoefficientSelected = preferences.TrendPhaseTemperatureCoefficientSelected;
                    clonedPreferences.TrendLTCPositionNumberSelected = preferences.TrendLTCPositionNumberSelected;
                    clonedPreferences.TrendAlarmStatusSelected = preferences.TrendAlarmStatusSelected;
                    clonedPreferences.TrendHumiditySelected = preferences.TrendHumiditySelected;
                    clonedPreferences.TrendTemp1Selected = preferences.TrendTemp1Selected;
                    clonedPreferences.TrendTemp2Selected = preferences.TrendTemp2Selected;
                    clonedPreferences.TrendTemp3Selected = preferences.TrendTemp3Selected;
                    clonedPreferences.TrendTemp4Selected = preferences.TrendTemp4Selected;
                    clonedPreferences.TrendLoadCurrent1Selected = preferences.TrendLoadCurrent1Selected;
                    clonedPreferences.TrendLoadCurrent2Selected = preferences.TrendLoadCurrent2Selected;
                    clonedPreferences.TrendLoadCurrent3Selected = preferences.TrendLoadCurrent3Selected;
                    clonedPreferences.TrendLoadVoltage1Selected = preferences.TrendLoadVoltage1Selected;
                    clonedPreferences.TrendLoadVoltage2Selected = preferences.TrendLoadVoltage2Selected;
                    clonedPreferences.TrendLoadVoltage3Selected = preferences.TrendLoadVoltage3Selected;
                    clonedPreferences.TrendExtLoadActiveSelected = preferences.TrendExtLoadActiveSelected;
                    clonedPreferences.TrendExtLoadReactiveSelected = preferences.TrendExtLoadReactiveSelected;
                    clonedPreferences.PolarGammaSelected = preferences.PolarGammaSelected;
                    clonedPreferences.PolarTemperatureCoefficientSelected = preferences.PolarTemperatureCoefficientSelected;
                    clonedPreferences.PolarCapacitanceASelected = preferences.PolarCapacitanceASelected;
                    clonedPreferences.PolarCapacitanceBSelected = preferences.PolarCapacitanceBSelected;
                    clonedPreferences.PolarCapacitanceCSelected = preferences.PolarCapacitanceCSelected;
                    clonedPreferences.PolarTangentASelected = preferences.PolarTangentASelected;
                    clonedPreferences.PolarTangentBSelected = preferences.PolarTangentBSelected;
                    clonedPreferences.PolarTangentCSelected = preferences.PolarTangentCSelected;
                    clonedPreferences.PolarShowRadialLabelsSelected = preferences.PolarShowRadialLabelsSelected;
                    clonedPreferences.TabSelected = preferences.TabSelected;
                    clonedPreferences.InsulationGridViewColumnWidths = preferences.InsulationGridViewColumnWidths;
                    clonedPreferences.TransformerGridViewColumnWidths = preferences.TransformerGridViewColumnWidths;
                    clonedPreferences.DynamicsVariableNames = preferences.DynamicsVariableNames;
                    clonedPreferences.DynamicsScaleFactors = preferences.DynamicsScaleFactors;
                    clonedPreferences.DynamicsOperations = preferences.DynamicsOperations;
                    clonedPreferences.MiscellaneousProperties = preferences.MiscellaneousProperties;
                    clonedPreferences.DateSaved = preferences.DateSaved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_CloneDbObject(BHM_DataViewerPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPreferences;
        }

        public static BHM_DataViewerPreferences BHM_DataViewerPreferences_LoadMostRecentlySavedPreferences(MonitorInterfaceDB db)
        {
            BHM_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.BHM_DataViewerPreferences
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_LoadMostRecentlySavedPreferences(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;

        }

        public static bool BHM_DataViewerPreferences_Save(BHM_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferences != null)
                {
                    BHM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(preferences.MonitorID, db);
                    db.BHM_DataViewerPreferences.InsertOnSubmit(preferences);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_Save(MainDisplayPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_DataViewerPreferences_Delete(BHM_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferences != null)
                {
                    db.BHM_DataViewerPreferences.DeleteOnSubmit(preferences);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_Delete(BHM_DataViewerPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_DataViewerPreferences_Delete(List<BHM_DataViewerPreferences> preferencesList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferencesList != null)
                {
                    db.BHM_DataViewerPreferences.DeleteAllOnSubmit(preferencesList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_Delete(List<BHM_DataViewerPreferences>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<BHM_DataViewerPreferences> preferencesList = BHM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(monitorID, db);
                success = BHM_DataViewerPreferences_Delete(preferencesList, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_DataViewerPreferences_CopyToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Monitor destinationMonitor = GetOneMonitor(monitor.ID, destinationDB);
                BHM_DataViewerPreferences preferences = BHM_DataViewerPreferences_Load(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if (preferences != null)
                    {
                        success = BHM_DataViewerPreferences_Save(BHM_DataViewerPreferences_CloneDbObject(preferences), destinationDB);
                        if (!success)
                        {
                            string errorMessage = "Error in General_DatabaseMethods.BHM_DataViewerPreferences_CopyToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the BHM_DataViewerPreferences to the destination database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        /// not finding any preferences is not an error.  perhaps the code needs to be rewritten to distinguish between a failed query and one that returns nothing
                        success = true;
//                        string errorMessage = "Error in General_DatabaseMethods.BHM_DataViewerPreferences_CopyToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the BHM_DataViewerPreferences from the source database.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.BHM_DataViewerPreferences_CopyToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the monitor in the destination database.  Preferences not copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.BHM_DataViewerPreferences_CopyToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #region Main Preferences

        public static Main_DataViewerPreferences Main_DataViewerPreferences_Load(Guid monitorID, MonitorInterfaceDB db)
        {
            Main_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.Main_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_Load(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static Main_DataViewerPreferences Main_DataViewerPreferences_CloneDbObject(Main_DataViewerPreferences preferences)
        {
            Main_DataViewerPreferences clonedPreferences = null;
            try
            {
                if (preferences != null)
                {
                    clonedPreferences = new Main_DataViewerPreferences();

                    clonedPreferences.ID = preferences.ID;
                    clonedPreferences.MonitorID = preferences.MonitorID;
                    clonedPreferences.DataAgeInMonths = preferences.DataAgeInMonths;
                    clonedPreferences.AutoDrawIsEnabled = preferences.AutoDrawIsEnabled;
                    clonedPreferences.TrendMouseAction = preferences.TrendMouseAction;
                    clonedPreferences.TrendShowTrendLineSelected = preferences.TrendShowTrendLineSelected;
                    clonedPreferences.TrendChosenTrendLineType = preferences.TrendChosenTrendLineType;
                    clonedPreferences.TrendNormalizeDataSelected = preferences.TrendNormalizeDataSelected;
                    clonedPreferences.TrendShowMovingAverageSelected = preferences.TrendShowMovingAverageSelected;
                    clonedPreferences.TrendMovingAverageItemCount = preferences.TrendMovingAverageItemCount;
                    clonedPreferences.TrendHumiditySelected = preferences.TrendHumiditySelected;
                    clonedPreferences.TrendLoadCurrent1Selected = preferences.TrendLoadCurrent1Selected;
                    clonedPreferences.TrendLoadCurrent2Selected = preferences.TrendLoadCurrent2Selected;
                    clonedPreferences.TrendLoadCurrent3Selected = preferences.TrendLoadCurrent3Selected;
                    clonedPreferences.TrendTemp1Selected = preferences.TrendTemp1Selected;
                    clonedPreferences.TrendTemp2Selected = preferences.TrendTemp2Selected;
                    clonedPreferences.TrendTemp3Selected = preferences.TrendTemp3Selected;
                    clonedPreferences.TrendTemp4Selected = preferences.TrendTemp4Selected;
                    clonedPreferences.TrendVoltage1Selected = preferences.TrendVoltage1Selected;
                    clonedPreferences.TrendVoltage2Selected = preferences.TrendVoltage2Selected;
                    clonedPreferences.TrendVoltage3Selected = preferences.TrendVoltage3Selected;
                    clonedPreferences.TrendCursorIsEnabled = preferences.TrendCursorIsEnabled;
                    clonedPreferences.DynamicsGridColumnWidths = preferences.DynamicsGridColumnWidths;
                    clonedPreferences.DynamicsVariableNames = preferences.DynamicsVariableNames;
                    clonedPreferences.DynamicsScaleFactors = preferences.DynamicsScaleFactors;
                    clonedPreferences.DynamicsOperations = preferences.DynamicsOperations;
                    clonedPreferences.TabSelected = preferences.TabSelected;
                    clonedPreferences.MiscellaneousProperties = preferences.MiscellaneousProperties;
                    clonedPreferences.DateSaved = preferences.DateSaved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_CloneDbObject(Main_DataViewerPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPreferences;
        }

        public static Main_DataViewerPreferences Main_DataViewerPreferences_LoadMostRecentlySavedPreferences(MonitorInterfaceDB db)
        {
            Main_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.Main_DataViewerPreferences
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_LoadMostRecentlySavedPreferences(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static List<Main_DataViewerPreferences> Main_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_DataViewerPreferences> preferencesList = null;
            try
            {
                var preferencesQuery =
                    from item in db.Main_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                preferencesList = preferencesQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferencesList;
        }

        public static bool Main_DataViewerPreferences_Save(Main_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var preferencesQuery =
                    from item in db.Main_DataViewerPreferences
                    where item.MonitorID == preferences.MonitorID
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    db.Main_DataViewerPreferences.DeleteAllOnSubmit(preferencesQuery);
                }
                db.Main_DataViewerPreferences.InsertOnSubmit(preferences);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_Save(MainDisplayPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_DataViewerPreferences_Delete(Main_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferences != null)
                {
                    db.Main_DataViewerPreferences.DeleteOnSubmit(preferences);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_Delete(Main_DataViewerPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_DataViewerPreferences_Delete(List<Main_DataViewerPreferences> preferencesList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferencesList != null)
                {
                    db.Main_DataViewerPreferences.DeleteAllOnSubmit(preferencesList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_Delete(List<Main_DataViewerPreferences>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<Main_DataViewerPreferences> preferencesList = Main_DataViewerPreferences_LoadAllPreferencesForOneMonitor(monitorID, db);
                success = Main_DataViewerPreferences_Delete(preferencesList, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Monitor destinationMonitor = GetOneMonitor(monitor.ID, destinationDB);
                Main_DataViewerPreferences preferences = Main_DataViewerPreferences_Load(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if (preferences != null)
                    {
                        success = Main_DataViewerPreferences_Save(Main_DataViewerPreferences_CloneDbObject(preferences), destinationDB);
                        if (!success)
                        {
                            string errorMessage = "Error in General_DatabaseMethods.Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the preferences to the destination database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        /// not finding any preferences is not an error.  perhaps the code needs to be rewritten to distinguish between a failed query and one that returns nothing
                        success = true;
//                        string errorMessage = "Error in General_DatabaseMethods.Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the preferences in the source database.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the monitor in the destination database.  Preferences not copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        #endregion

        #region PDM Preferences

        public static PDM_DataViewerPreferences PDM_DataViewerPreferences_Load(Guid monitorID, MonitorInterfaceDB db)
        {
            PDM_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.PDM_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_Load(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static PDM_DataViewerPreferences PDM_DataViewerPreferences_CloneDbObject(PDM_DataViewerPreferences preferences)
        {
            PDM_DataViewerPreferences clonedPreferences = null;
            try
            {
                if (preferences != null)
                {
                    clonedPreferences = new PDM_DataViewerPreferences();

                    clonedPreferences.ID = preferences.ID;
                    clonedPreferences.MonitorID = preferences.MonitorID;
                    clonedPreferences.DataAgeInMonths = preferences.DataAgeInMonths;
                    clonedPreferences.AutoDrawIsEnabled = preferences.AutoDrawIsEnabled;
                    clonedPreferences.AmplitudeDisplayUnits = preferences.AmplitudeDisplayUnits;
                    clonedPreferences.InitialPhaseShift = preferences.InitialPhaseShift;
                    clonedPreferences.DisplayScaleSelectedIndex = preferences.DisplayScaleSelectedIndex;
                    clonedPreferences.TrendMouseAction = preferences.TrendMouseAction;
                    clonedPreferences.TrendChannelsToDisplay = preferences.TrendChannelsToDisplay;
                    clonedPreferences.TrendQmaxSelected = preferences.TrendQmaxSelected;
                    clonedPreferences.TrendQmaxUnits = preferences.TrendQmaxUnits;
                    clonedPreferences.TrendQmaxTrendSelected = preferences.TrendQmaxTrendSelected;
                    clonedPreferences.TrendQmaxPlusSelected = preferences.TrendQmaxPlusSelected;
                    clonedPreferences.TrendQmaxMinusSelected = preferences.TrendQmaxMinusSelected;
                    clonedPreferences.TrendPDISelected = preferences.TrendPDISelected;
                    clonedPreferences.TrendPDITrendSelected = preferences.TrendPDITrendSelected;
                    clonedPreferences.TrendPDIPlusSelected = preferences.TrendPDIPlusSelected;
                    clonedPreferences.TrendPDIMinusSelected = preferences.TrendPDIMinusSelected;
                    clonedPreferences.TrendPulseCountSelected = preferences.TrendPulseCountSelected;
                    clonedPreferences.TrendPulseCountPlusSelected = preferences.TrendPulseCountPlusSelected;
                    clonedPreferences.TrendPulseCountMinusSelected = preferences.TrendPulseCountMinusSelected;
                    clonedPreferences.TrendPulseCountDisplayType = preferences.TrendPulseCountDisplayType;
                    clonedPreferences.TrendShowTrendLineSelected = preferences.TrendShowTrendLineSelected;
                    clonedPreferences.TrendChosenTrendLineType = preferences.TrendChosenTrendLineType;
                    clonedPreferences.TrendNormalizeDataSelected = preferences.TrendNormalizeDataSelected;
                    clonedPreferences.TrendShowMovingAverageSelected = preferences.TrendShowMovingAverageSelected;
                    clonedPreferences.TrendMovingAverageItemCount = preferences.TrendMovingAverageItemCount;
                    clonedPreferences.TrendHumiditySelected = preferences.TrendHumiditySelected;
                    clonedPreferences.TrendExtLoadActiveSelected = preferences.TrendExtLoadActiveSelected;
                    clonedPreferences.TrendExtLoadReactiveSelected = preferences.TrendExtLoadReactiveSelected;
                    clonedPreferences.TrendLoadCurrent1Selected = preferences.TrendLoadCurrent1Selected;
                    clonedPreferences.TrendLoadCurrent2Selected = preferences.TrendLoadCurrent2Selected;
                    clonedPreferences.TrendLoadCurrent3Selected = preferences.TrendLoadCurrent3Selected;
                    clonedPreferences.TrendTemperatureSelected = preferences.TrendTemperatureSelected;
                    clonedPreferences.TrendTemp1Selected = preferences.TrendTemp1Selected;
                    clonedPreferences.TrendTemp2Selected = preferences.TrendTemp2Selected;
                    clonedPreferences.TrendTemp3Selected = preferences.TrendTemp3Selected;
                    clonedPreferences.TrendTemp4Selected = preferences.TrendTemp4Selected;
                    clonedPreferences.TrendVoltage1Selected = preferences.TrendVoltage1Selected;
                    clonedPreferences.TrendVoltage2Selected = preferences.TrendVoltage2Selected;
                    clonedPreferences.TrendVoltage3Selected = preferences.TrendVoltage3Selected;
                    clonedPreferences.TrendCursorIsEnabled = preferences.TrendCursorIsEnabled;
                    clonedPreferences.PRPDDShowZeroDegreeSineWave = preferences.PRPDDShowZeroDegreeSineWave;
                    clonedPreferences.PRPDDShowOneTwentyDegreeSineWave = preferences.PRPDDShowOneTwentyDegreeSineWave;
                    clonedPreferences.PRPDDShowTwoFortyDegreeSineWave = preferences.PRPDDShowTwoFortyDegreeSineWave;
                    clonedPreferences.PRPDDMouseAction = preferences.PRPDDMouseAction;
                    clonedPreferences.PRPDDMarkerType = preferences.PRPDDMarkerType;
                    clonedPreferences.PRPDDGraphLayout = preferences.PRPDDGraphLayout;
                    clonedPreferences.PRPDDChartPhaseShiftSelectedIndex = preferences.PRPDDChartPhaseShiftSelectedIndex;
                    clonedPreferences.PRPDDMouseAction = preferences.PRPDDMouseAction;
                    clonedPreferences.PRPDDPolarShowRadialAxis = preferences.PRPDDPolarShowRadialAxis;
                    clonedPreferences.PHDMouseAction = preferences.PHDMouseAction;
                    clonedPreferences.ParamtersGridColumnWidths = preferences.ParamtersGridColumnWidths;
                    clonedPreferences.PDParametersGridColumnWidths = preferences.PDParametersGridColumnWidths;
                    clonedPreferences.CHPDParametersGridColumnWidths = preferences.CHPDParametersGridColumnWidths;
                    clonedPreferences.PhaseResolvedDataGridColumnWidths = preferences.PhaseResolvedDataGridColumnWidths;
                    clonedPreferences.DynamicsVariableNames = preferences.DynamicsVariableNames;
                    clonedPreferences.DynamicsScaleFactors = preferences.DynamicsScaleFactors;
                    clonedPreferences.DynamicsOperations = preferences.DynamicsOperations;
                    clonedPreferences.TabSelected = preferences.TabSelected;
                    clonedPreferences.MiscellaneousProperties = preferences.MiscellaneousProperties;
                    clonedPreferences.DateSaved = preferences.DateSaved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_CloneDbObject(PDM_DataViewerPreferences)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedPreferences;
        }

        public static PDM_DataViewerPreferences PDM_DataViewerPreferences_LoadMostRecentlySavedPreferences(MonitorInterfaceDB db)
        {
            PDM_DataViewerPreferences preferences = null;
            try
            {
                var preferencesQuery =
                    from item in db.PDM_DataViewerPreferences
                    orderby item.DateSaved descending
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    preferences = preferencesQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_LoadMostRecentlySavedPreferences(DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferences;
        }

        public static List<PDM_DataViewerPreferences> PDM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<PDM_DataViewerPreferences> preferencesList = null;
            try
            {
                var preferencesQuery =
                    from item in db.PDM_DataViewerPreferences
                    where item.MonitorID == monitorID
                    orderby item.DateSaved descending
                    select item;

                preferencesList = preferencesQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(Guid, DataContext)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return preferencesList;
        }

        public static bool PDM_DataViewerPreferences_Save(PDM_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var preferencesQuery =
                    from item in db.PDM_DataViewerPreferences
                    where item.MonitorID == preferences.MonitorID
                    select item;

                if (preferencesQuery.Count() > 0)
                {
                    db.PDM_DataViewerPreferences.DeleteAllOnSubmit(preferencesQuery);
                }
                db.PDM_DataViewerPreferences.InsertOnSubmit(preferences);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_Save(MainDisplayPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_DataViewerPreferences_Delete(PDM_DataViewerPreferences preferences, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferences != null)
                {
                    db.PDM_DataViewerPreferences.DeleteOnSubmit(preferences);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_Delete(PDM_DataViewerPreferences, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_DataViewerPreferences_Delete(List<PDM_DataViewerPreferences> preferencesList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (preferencesList != null)
                {
                    db.PDM_DataViewerPreferences.DeleteAllOnSubmit(preferencesList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_Delete(List<PDM_DataViewerPreferences>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<PDM_DataViewerPreferences> preferencesList = PDM_DataViewerPreferences_LoadAllPreferencesForOneMonitor(monitorID, db);
                success = PDM_DataViewerPreferences_Delete(preferencesList, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Monitor destinationMonitor = GetOneMonitor(monitor.ID, destinationDB);
                PDM_DataViewerPreferences preferences = PDM_DataViewerPreferences_Load(monitor.ID, sourceDB);
                if (destinationMonitor != null)
                {
                    if (preferences != null)
                    {
                        success = PDM_DataViewerPreferences_Save(PDM_DataViewerPreferences_CloneDbObject(preferences), destinationDB);
                        if (!success)
                        {
                            string errorMessage = "Error in General_DatabaseMethods.PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the preferences to the destination database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        /// not finding any preferences is not an error.  perhaps the code needs to be rewritten to distinguish between a failed query and one that returns nothing
                        success = true;
                        //                        string errorMessage = "Error in General_DatabaseMethods.PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the preferences in the source database.";
                        //                        LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                        MessageBox.Show(errorMessage);
                        //#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the monitor in the destination database.  Preferences not copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #endregion

        public static bool DatabaseConnectionStringIsCorrect(MonitorInterfaceDB db)
        {
            bool success = false;
            
            try
            {
                int query =
                (from item in db.Company
                 select item).Count();
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                string errorMessage = "Exception thrown in General_DatabaseMethods.DatabaseConnectionStringIsCorrect(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                // MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Creates a databasen skeleton that will support the list of input monitors.  The data, configurations, and preferences must be copied separately.
        /// </summary>
        /// <param name="inputMonitors"></param>
        /// <param name="sourceDB"></param>
        /// <param name="destinationDB"></param>
        /// <returns></returns>
        public static List<string> PartialDatabaseStructureClone(Dictionary<Guid, int> inputMonitors, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> errorList = new List<string>();
            try
            {
                List<Monitor> selectedMonitors;
                List<Equipment> selectedEquipment;
                List<Plant> selectedPlants;
                List<Company> selectedCompanies;
                List<Guid> inputMonitorsAsList = new List<Guid>();

                if (inputMonitors != null)
                {
                    foreach (KeyValuePair<Guid, int> kvPair in inputMonitors)
                    {
                        inputMonitorsAsList.Add(kvPair.Key);
                    }

                    selectedMonitors = GetMonitors(inputMonitorsAsList, sourceDB);
                    selectedEquipment = GetEquipmentAssociatedWithInputMonitors(selectedMonitors, sourceDB);
                    if ((selectedEquipment != null) && (selectedEquipment.Count > 0))
                    {
                        selectedPlants = GetPlantsAssociatedWithInputEquipment(selectedEquipment, sourceDB);
                        if ((selectedPlants != null) && (selectedPlants.Count > 0))
                        {
                            selectedCompanies = GetCompaniesAssociatedWithInputPlants(selectedPlants, sourceDB);
                            if ((selectedCompanies != null) && (selectedCompanies.Count > 0))
                            {
                                errorList.AddRange(WriteCompanyPlantEquipmentMonitorHierarchyToNewDatabase(selectedCompanies, selectedPlants, selectedEquipment, selectedMonitors, destinationDB));
                            }
                            else
                            {
                                string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any companies associated with the found plants.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any plants associated with the found equipment.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any equipment associated with the input monitors.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nInput Dictionary<Guid, int> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorList;
        }

        /// <summary>
        /// This cloning operation will clone incomplete entries (those without monitors defined) as well as complete ones
        /// </summary>
        /// <param name="sourceDB"></param>
        /// <param name="destinationDB"></param>
        /// <returns></returns>
        public static List<string> FullDatabaseStructureCloneThatAllowsForIncompleteEntries(MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> errorList = new List<string>();
            try
            {
                List<Monitor> monitorsList = GetAllMonitors(sourceDB);
                List<Equipment> equipmentList = GetAllEquipment(sourceDB);
                List<Plant> plantList = GetAllPlants(sourceDB);
                List<Company> companyList = GetAllCompanies(sourceDB);

                errorList.AddRange(WriteCompanyPlantEquipmentMonitorHierarchyToNewDatabase(companyList, plantList, equipmentList, monitorsList, destinationDB));
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.FullDatabaseStructureCloneThatAllowsForIncompleteEntries(MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorList;
        }

        /// <summary>
        /// Creates a databasen skeleton that will support all monitors currently in the database.  The data, configurations, and preferences must be copied separately.
        /// </summary>
        /// <param name="inputMonitors"></param>
        /// <param name="sourceDB"></param>
        /// <param name="destinationDB"></param>
        /// <returns></returns>
        public static List<string> FullDatabaseStructureClone(MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> errorList = new List<string>();
            try
            {
                List<Monitor> monitorsList = GetAllMonitors(sourceDB);
                List<Equipment> equipmentList = GetAllEquipment(sourceDB);
                List<Plant> plantList = GetAllPlants(sourceDB);
                List<Company> companyList = GetAllCompanies(sourceDB);

                if ((companyList != null) && (companyList.Count > 0))
                {
                    if ((plantList != null) && (plantList.Count > 0))
                    {
                        if ((equipmentList != null) && (equipmentList.Count > 0))
                        {
                            if ((monitorsList != null) && (monitorsList.Count > 0))
                            {
                                errorList.AddRange(WriteCompanyPlantEquipmentMonitorHierarchyToNewDatabase(companyList, plantList, equipmentList, monitorsList, destinationDB));
                            }
                            else
                            {
                                string errorMessage = "Error in General_DatabaseMethods.FullDatabaseStructureClone(MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any monitors in the source database.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in General_DatabaseMethods.FullDatabaseStructureClone(MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any equipment in the source database.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.FullDatabaseStructureClone(MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any plants in the source database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.FullDatabaseStructureClone(MonitorInterfaceDB, MonitorInterfaceDB)\nDid not find any companies in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorList;
        }

        public static List<String> WriteCompanyPlantEquipmentMonitorHierarchyToNewDatabase(List<Company> companyList, List<Plant> plantList, List<Equipment> equipmentList, List<Monitor> monitorList, MonitorInterfaceDB destinationDB)
        {
            List<String> failedWriteMessagesList = new List<string>();
            try
            {
                if ((companyList != null) && (companyList.Count > 0))
                {
                    if (WriteCompanyObjectsToNewDatabase(companyList, destinationDB))
                    {
                        if ((plantList != null) && (plantList.Count > 0))
                        {
                            if (WritePlantObjectsToNewDatabase(plantList, destinationDB))
                            {
                                if ((equipmentList != null) && (equipmentList.Count > 0))
                                {
                                    if (WriteEquipmentObjectsToNewDatabase(equipmentList, destinationDB))
                                    {
                                        if ((monitorList != null) && (monitorList.Count > 0))
                                        {
                                            if (!WriteMonitorObjectsToNewDatabase(monitorList, destinationDB))
                                            {
                                                failedWriteMessagesList.Add("Failed to write the monitors to the new database.");
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nInput List<Monitor> was either null or empty.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    else
                                    {
                                        failedWriteMessagesList.Add("failed to write the equipment to the new database.");
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nInput List<Equipment> was either null or empty.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                failedWriteMessagesList.Add("failed to write the plants to the new database.");
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nInput List<Plant> was either null or empty.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        failedWriteMessagesList.Add("failed to write the companies to the new database.");
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nInput List<Company> was either null or empty.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.PartialDatabaseClone(Dictionary<Guid, int>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWriteMessagesList;
        }

        public static bool WriteCompanyObjectsToNewDatabase(List<Company> companiesList, MonitorInterfaceDB destinationDB)
        {
            bool success = true;
            try
            {
                if (companiesList != null)
                {
                    if (companiesList.Count > 0)
                    {
                        foreach (Company company in companiesList)
                        {
                            if (!General_DatabaseMethods.CompanyWrite(General_DatabaseMethods.CloneOneCompanyDbObject(company), destinationDB))
                            {
                                string errorMessage = "Error in General_DatabaseMethods.WriteCompanyObjectsToNewDatabase(List<Company>, MonitorInterfaceDB)\nFailed to write a company to the new database.  Quitting.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                success = false;
                                break;
                            }
                            Application.DoEvents();
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.WriteCompanyObjectsToNewDatabase(List<Company>, MonitorInterfaceDB)\nInput List<Company> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.WriteCompanyObjectsToNewDatabase(List<Company>, MonitorInterfaceDB)\nInput List<Company> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.WriteCompanyObjectsToNewDatabase(List<Company>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool WritePlantObjectsToNewDatabase(List<Plant> plantsList, MonitorInterfaceDB destinationDB)
        {
            bool success = true;
            try
            {
                if (plantsList != null)
                {
                    if (plantsList.Count > 0)
                    {
                        foreach (Plant plant in plantsList)
                        {
                            if (!General_DatabaseMethods.PlantWrite(General_DatabaseMethods.CloneOnePlantDbObject(plant), destinationDB))
                            {
                                string errorMessage = "Error in General_DatabaseMethods.WritePlantObjectsToNewDatabase(List<Plant>, MonitorInterfaceDB)\nFailed to write a plant to the new database.  Quitting.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                success = false;
                                break;
                            }
                            Application.DoEvents();
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.WritePlantObjectsToNewDatabase(List<Plant>, MonitorInterfaceDB)\nInput List<Plant> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.WritePlantObjectsToNewDatabase(List<Plant>, MonitorInterfaceDB)\nInput List<Plant> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.WritePlantObjectsToNewDatabase(List<Plant>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool WriteEquipmentObjectsToNewDatabase(List<Equipment> equipmentList, MonitorInterfaceDB destinationDB)
        {
            bool success = true;
            try
            {
                if (equipmentList != null)
                {
                    if (equipmentList.Count > 0)
                    {
                        foreach (Equipment equipment in equipmentList)
                        {
                            if (!EquipmentWrite(CloneOneEquipmentDbObject(equipment), destinationDB))
                            {
                                string errorMessage = "Error in General_DatabaseMethods.WriteEquipmentObjectsToNewDatabase(List<Equipment>, MonitorInterfaceDB)\nFailed to write one equipment to the new database.  Quitting.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                success = false;
                                break;
                            }
                            Application.DoEvents();
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.WriteEquipmentObjectsToNewDatabase(List<Equipment>, MonitorInterfaceDB)\nInput List<Equipment> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.WriteEquipmentObjectsToNewDatabase(List<Equipment>, MonitorInterfaceDB)\nInput List<Equipment> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.WriteEquipmentObjectsToNewDatabase(List<Equipment>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool WriteMonitorObjectsToNewDatabase(List<Monitor> monitorList, MonitorInterfaceDB destinationDB)
        {
            bool success = true;
            try
            {
                if (monitorList != null)
                {
                    if (monitorList.Count > 0)
                    {
                        foreach (Monitor monitor in monitorList)
                        {
                            if (!MonitorWrite(CloneOneMonitorDbObject(monitor), destinationDB))
                            {
                                string errorMessage = "Error in General_DatabaseMethods.WriteMonitorObjectsToNewDatabase(List<Monitor>, MonitorInterfaceDB)\nFailed to write one monitor to the new database.  Quitting.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                success = false;
                                break;
                            }
                            Application.DoEvents();
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.WriteMonitorObjectsToNewDatabase(List<Monitor>, MonitorInterfaceDB)\nInput List<Monitor> was empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.WriteMonitorObjectsToNewDatabase(List<Monitor>, MonitorInterfaceDB)\nInput List<Monitor> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.WriteMonitorObjectsToNewDatabase(List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the base members from the supplied database and copyies them to the reference inputs.  Returns false if at least one list is null or empty, true otherwise.
        /// </summary>
        /// <param name="companyList"></param>
        /// <param name="plantList"></param>
        /// <param name="equipmentList"></param>
        /// <param name="monitorList"></param>
        /// <param name="sourceDB"></param>
        /// <returns></returns>
        public static bool GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company> companyList, ref List<Plant> plantList, ref List<Equipment> equipmentList, ref List<Monitor> monitorList, MonitorInterfaceDB sourceDB)
        {
            bool success = true;
            try
            {
                companyList = GetAllCompanies(sourceDB);
                if ((companyList != null) && (companyList.Count > 0))
                {
                    plantList = GetAllPlants(sourceDB);
                    if ((plantList != null) && (plantList.Count > 0))
                    {
                        equipmentList = GetAllEquipment(sourceDB);
                        if ((equipmentList != null) && (equipmentList.Count > 0))
                        {
                            monitorList = GetAllMonitors(sourceDB);
                            if ((monitorList == null) || (monitorList.Count == 0))
                            {
                                string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company>, ref List<Plant>, ref List<Equipment>, ref List<Monitor>, MonitorInterfaceDB)\nNo monitors found.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                                success = false;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company>, ref List<Plant>, ref List<Equipment>, ref List<Monitor>, MonitorInterfaceDB)\nNo equipment found.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                            success = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company>, ref List<Plant>, ref List<Equipment>, ref List<Monitor>, MonitorInterfaceDB)\nNo plants found.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        success = false;
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company>, ref List<Plant>, ref List<Equipment>, ref List<Monitor>, MonitorInterfaceDB)\nNo companies found.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    success = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetCompanyPlantEquipmentMonitorsFromDatabase(ref List<Company>, ref List<Plant>, ref List<Equipment>, ref List<Monitor>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        //        public static List<string> CopyMonitorDataToTheNewDatabase(MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        //        {
        //             List<string> transferErrorList = new List<string>();
        //            try
        //            {
        //                List<string> oneTransferErrorList;

        //                List<Monitor> monitorList = GetAllMonitors(destinationDB);
        //                List<Equipment> equipmentList = GetAllEquipment(destinationDB);
        //                List<Plant> plantList = GetAllPlants(destinationDB);
        //                List<Company> companyList = GetAllCompanies(destinationDB);

        //                if (monitorList != null)
        //                {
        //                    foreach (Monitor monitor in monitorList)
        //                    {
        //                        oneTransferErrorList = TransferDataForOneMonitor(monitor.ID, sourceDB, destinationDB);
        //                        if (oneTransferErrorList.Count > 0)
        //                        {
        //                            transferErrorList.AddRange(oneTransferErrorList);

        //                            string errorMessage = "Error in General_DatabaseMethods.CopyMonitorDataToTheNewDatabase(List<Monitor>, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to transfer the data for one monitor.  Quitting.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif 
        //                        }
        //                        Application.DoEvents();
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Exception thrown in General_DatabaseMethods.CopyMonitorDataToTheNewDatabase(List<Monitor>, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find any monitors in the destination database, cannot copy any data.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                    transferErrorList.Add("No monitors in destination database, so no data can be transferred.");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in General_DatabaseMethods.CopyMonitorDataToTheNewDatabase(List<Monitor>, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return transferErrorList;
        //        }

        public static string GetHierarchyForOneMonitor(Monitor monitor, List<Company> companyList, List<Plant> plantList, List<Equipment> equipmentList)
        {
            string hierarchyString = string.Empty;
            try
            {
                Company company;
                Plant plant;
                Equipment equipment;

                if (monitor != null)
                {
                    if ((equipmentList != null) && (equipmentList.Count > 0))
                    {
                        equipment = FindEquipmentAssociatedWithInputMonitor(monitor, equipmentList);
                        if (equipment != null)
                        {
                            if ((plantList != null) && (plantList.Count > 0))
                            {
                                plant = FindPlantAssociatedWithInputEquipment(equipment, plantList);
                                if (plant != null)
                                {
                                    if ((companyList != null) && (companyList.Count > 0))
                                    {
                                        company = FindCompanyAssociatedWithInputPlant(plant, companyList);
                                        if (company != null)
                                        {
                                            hierarchyString = company.Name + ": " + plant.Name + ": " + equipment.Name + ": " + monitor.MonitorType.Trim();
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nFailed to find the company corresponding to the input monitor.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nInput List<Company> was either null or empty.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nFailed to find the plant corresponding to the input monitor.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nInput List<Plant> was either null or empty.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nFailed to find the equipment corresponding to the input monitor.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nInput List<Equipment> was either null or empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nInput Monitor was either null or empty.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetHierarchyForOneMonitor(Monitor, List<Company>, List<Plant>, List<Equipment>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchyString;
        }

        public static Company FindCompanyAssociatedWithInputPlant(Plant plant, List<Company> companyList)
        {
            Company company = null;
            try
            {
                foreach (Company entry in companyList)
                {
                    if (entry.ID.CompareTo(plant.CompanyID) == 0)
                    {
                        company = entry;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.FindCompanyAssociatedWithInputPlant(Plant, List<Company>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return company;
        }

        public static Plant FindPlantAssociatedWithInputEquipment(Equipment equipment, List<Plant> plantList)
        {
            Plant plant = null;
            try
            {
                foreach (Plant entry in plantList)
                {
                    if (entry.ID.CompareTo(equipment.PlantID) == 0)
                    {
                        plant = entry;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.FindPlantAssociatedWithInputEquipment(Equipment, List<Plant>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return plant;
        }

        public static Equipment FindEquipmentAssociatedWithInputMonitor(Monitor monitor, List<Equipment> equipmentList)
        {
            Equipment equipment = null;
            try
            {
                foreach (Equipment entry in equipmentList)
                {
                    if (entry.ID.CompareTo(monitor.EquipmentID) == 0)
                    {
                        equipment = entry;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.FindEquipmentAssociatedWithInputMonitor(Monitor, List<Equipment>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return equipment;
        }

        public static bool CopyPreferencesForOneMonitorToTheNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Monitor destinationMonitor = GetOneMonitor(monitor.ID, destinationDB);
                string monitorType = monitor.MonitorType.Trim();
                if (destinationMonitor != null)
                {
                    if (monitorType.CompareTo("Main") == 0)
                    {
                        success = Main_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(monitor, sourceDB, destinationDB);
                    }
                    else if (monitorType.CompareTo("ADM") == 0)
                    {
                        success = true;
                    }
                    else if (monitorType.CompareTo("BHM") == 0)
                    {
                        success = BHM_DataViewerPreferences_CopyToNewDatabase(monitor, sourceDB, destinationDB);
                    }
                    else if (monitorType.CompareTo("PDM") == 0)
                    {
                        success = PDM_DataViewerPreferences_CopyPreferencesForOneMonitorToNewDatabase(monitor, sourceDB, destinationDB);
                    }
                    else
                    {
                        string errorMessage = "Error in General_DatabaseMethods.CopyPreferencesForOneMonitorToTheNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nUnknown monitor type.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.CopyPreferencesForOneMonitorToTheNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find monitor in destination database.  Preferences not copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CopyPreferencesForOneMonitorToTheNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool DeleteAllDependentDataForOneMonitor(Monitor monitor, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                string monitorType = monitor.MonitorType.Trim();

                if (monitorType.CompareTo("Main") == 0)
                {
                    MainMonitor_DatabaseMethods.Main_AlarmStatus_DeleteAllEntriesForOneMonitor(monitor.ID, db);
                    success = MainMonitor_DatabaseMethods.Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if (success)
                    {
                        success = MainMonitor_DatabaseMethods.Main_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, db);
                    }
                    if (success)
                    {
                        success = Main_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(monitor.ID, db);
                    }
                }
                else if (monitorType.CompareTo("ADM") == 0)
                {
                    /// nothing here yet so no worries
                    success = true;
                }
                else if (monitorType.CompareTo("BHM") == 0)
                {
                    BHM_DatabaseMethods.BHM_AlarmStatus_DeleteAllForOneMonitor(monitor.ID, db);
                    success = BHM_DatabaseMethods.BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if (success)
                    {
                        success = BHM_DatabaseMethods.BHM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(monitor, db);
                    }
                    if (success)
                    {
                        success = BHM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(monitor.ID, db);
                    }
                }
                else if (monitorType.CompareTo("PDM") == 0)
                {
                    PDM_DatabaseMethods.PDM_AlarmStatus_DeleteAllEntriesForOneMonitor(monitor.ID, db);
                    success = PDM_DatabaseMethods.PDM_Data_DeleteAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if (success)
                    {
                        success = PDM_DatabaseMethods.PDM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, db);
                    }
                    if (success)
                    {
                        success = PDM_DataViewerPreferences_DeleteAllPreferencesForOneMonitor(monitor.ID, db);
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.MonitorHasDataAssociatedWithIt(Monitor)\nMonitor type as read from the database was of incorrect form.  Type extracted from DB was " + monitorType + ".  Fix it.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.CopyPreferencesForOneMonitorToTheNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Indicates whether a given monitor has data saved in the database
        /// </summary>
        /// <param name="monitor"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool MonitorHasDataAssociatedWithIt(Monitor monitor, MonitorInterfaceDB db)
        {
            bool hasData = false;
            try
            {
                string monitorType = monitor.MonitorType.Trim();

                if (monitorType.CompareTo("Main") == 0)
                {
                    List<Main_Data_DataRoot> dataRootList = MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if ((dataRootList != null) && (dataRootList.Count > 0))
                    {
                        hasData = true;
                    }
                }
                else if (monitorType.CompareTo("ADM") == 0)
                {
                    /// nothing here yet so no worries
                }
                else if (monitorType.CompareTo("BHM") == 0)
                {
                    List<BHM_Data_DataRoot> dataRootList = BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if ((dataRootList != null) && (dataRootList.Count > 0))
                    {
                        hasData = true;
                    }
                }
                else if (monitorType.CompareTo("PDM") == 0)
                {
                    List<PDM_Data_DataRoot> dataRootList = PDM_DatabaseMethods.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitor.ID, db);
                    if ((dataRootList != null) && (dataRootList.Count > 0))
                    {
                        hasData = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.MonitorHasDataAssociatedWithIt(Monitor)\nMonitor type as read from the database was of incorrect form.  Type extracted from DB was " + monitorType + ".  Fix it.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.MonitorHasDataAssociatedWithIt(Monitor)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hasData;

        }

        public static MonitorHierarchy GetFullMonitorHierarchy(Guid monitorID, MonitorInterfaceDB db)
        {
            MonitorHierarchy hierarchy = null;
            try
            {
                Monitor monitor = GetOneMonitor(monitorID, db);
                if (monitor != null)
                {
                    hierarchy = GetFullMonitorHierarchy(monitor, db);
                }
                else
                {
                    hierarchy.errorCode = ErrorCode.MonitorNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetFullMonitorHierarchy(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchy;
        }

        public static MonitorHierarchy GetFullMonitorHierarchy(Monitor monitor, MonitorInterfaceDB db)
        {
            MonitorHierarchy hierarchy = new MonitorHierarchy();
            try
            {
                hierarchy.errorCode = ErrorCode.ObjectNotProperlyAssignedTo;
                if (monitor != null)
                {
                    hierarchy.monitor = monitor;
                    hierarchy.equipment = GetOneEquipment(monitor.EquipmentID, db);
                    if (hierarchy.equipment != null)
                    {
                        hierarchy.plant = GetPlant(hierarchy.equipment.PlantID, db);
                        if (hierarchy.plant != null)
                        {
                            hierarchy.company = GetCompany(hierarchy.plant.CompanyID, db);
                            if (hierarchy.company == null)
                            {
                                hierarchy.errorCode = ErrorCode.CompanyNotFound;
                            }
                            else
                            {
                                hierarchy.errorCode = ErrorCode.None;
                            }
                        }
                        else
                        {
                            hierarchy.errorCode = ErrorCode.PlantNotFound;
                        }
                    }
                    else
                    {
                        hierarchy.errorCode = ErrorCode.EquipmentNotFound;
                    }
                }
                else
                {
                    hierarchy.errorCode = ErrorCode.MonitorNotFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetFullMonitorHierarchy(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchy;
        }

        public static MonitorHierarchy GetFullEquipmentHierarchy(Equipment equipment, MonitorInterfaceDB db)
        {
            MonitorHierarchy hierarchy = new MonitorHierarchy();
            try
            {
                hierarchy.errorCode = ErrorCode.ObjectNotProperlyAssignedTo;
                if (equipment != null)
                {
                    hierarchy.equipment = equipment;

                    hierarchy.plant = GetPlant(hierarchy.equipment.PlantID, db);
                    if (hierarchy.plant != null)
                    {
                        hierarchy.company = GetCompany(hierarchy.plant.CompanyID, db);
                        if (hierarchy.company == null)
                        {
                            hierarchy.errorCode = ErrorCode.CompanyNotFound;
                        }
                        else
                        {
                            hierarchy.errorCode = ErrorCode.None;
                        }
                    }
                    else
                    {
                        hierarchy.errorCode = ErrorCode.PlantNotFound;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetFullMonitorHierarchy(Monitor, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchy;
        }

        public static List<string> GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy(MonitorHierarchy monitorHierarchy)
        {
            List<string> hierarchyNames = new List<string>();
            try
            {
                bool hierarchyWasCorrect = true;
                if (monitorHierarchy != null)
                {
                    if (monitorHierarchy.company != null)
                    {
                        hierarchyNames.Add(monitorHierarchy.company.Name);
                    }
                    else
                    {
                        hierarchyWasCorrect = false;
                    }
                    if (hierarchyWasCorrect && (monitorHierarchy.plant != null))
                    {
                        hierarchyNames.Add(monitorHierarchy.plant.Name);
                    }
                    else
                    {
                        hierarchyWasCorrect = false;
                    }
                    if (hierarchyWasCorrect && (monitorHierarchy.equipment != null))
                    {
                        hierarchyNames.Add(monitorHierarchy.equipment.Name);
                    }
                    else
                    {
                        hierarchyWasCorrect = false;
                    }
                    if (!hierarchyWasCorrect)
                    {
                        hierarchyNames.Clear();
                        string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy()\nMessage: Monitor hierarchy was not well-defined, which should not happen, ever.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy()\nMessage: Monitor hierarchy was not well-defined, which should not happen, ever.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in General_DatabaseMethods.GetCompanyPlantAndEquipmentNamesFromMonitorHierarchy(WinChartViewer)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return hierarchyNames;
        }
    }
}
 