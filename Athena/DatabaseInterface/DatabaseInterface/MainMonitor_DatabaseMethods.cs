﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DatabaseInterface
{
    public class MainMonitor_DatabaseMethods
    {
        #region Main Alarm Status

        /// <summary>
        /// Writes one Main_AlarmStatus object to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void Main_AlarmStatus_Write(Main_AlarmStatus AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                db.Main_AlarmStatus.InsertOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_AlarmStatus_Write(Main_AlarmStatus, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Returns the Main_AlarmStatus object with the latest DateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Main_AlarmStatus Main_AlarmStatus_ReadLatestEntry(Guid monitorID, MonitorInterfaceDB db)
        {
            Main_AlarmStatus AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                    from entry in db.Main_AlarmStatus
                    where entry.MonitorID == monitorID
                    orderby entry.DeviceTime descending
                    select entry;

                if (alarmStatusQuery.Count() > 0)
                {
                    AlarmStatus = alarmStatusQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_AlarmStatus_ReadLatestEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Returns a list of all Main_AlarmStatus entries for one monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_AlarmStatus> Main_AlarmStatus_ReadAllEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_AlarmStatus> AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                    from entry in db.Main_AlarmStatus
                    where entry.MonitorID == monitorID
                    orderby entry.DeviceTime descending
                    select entry;

                AlarmStatus = alarmStatusQuery.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_AlarmStatus_ReadAllEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Removes all Main_AlarmStatus entries for one monitor.
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        public static void Main_AlarmStatus_DeleteAllEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            try
            {
                List<Main_AlarmStatus> AlarmStatus = Main_AlarmStatus_ReadAllEntriesForOneMonitor(monitorID, db);

                db.Main_AlarmStatus.DeleteAllOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_AlarmStatus_DeleteAllEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Deletes a single entry from Main_AlarmStatus
        /// </summary>
        /// <param name="entryID"></param>
        /// <param name="db"></param>
        public static void Main_AlarmStatus_Delete(Guid entryID, MonitorInterfaceDB db)
        {
            try
            {
                var alarmStatusQuery =
                          from entry in db.Main_AlarmStatus
                          where entry.ID == entryID
                          select entry;

                if (alarmStatusQuery.Count() > 0)
                {
                    db.Main_AlarmStatus.DeleteOnSubmit(alarmStatusQuery.First());
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_AlarmStatus_Delete(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Main Configuration

        #region Main Configuration  Configuration Root

        public static Main_Config_ConfigurationRoot Main_Config_ReadConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_ConfigurationRoot configurationRoot = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Config_ConfigurationRoot
                    where item.ID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    configurationRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRoot;
        }

        public static Main_Config_ConfigurationRoot Main_Config_CloneOneConfigurationRootDbObject(Main_Config_ConfigurationRoot configurationRoot)
        {
            Main_Config_ConfigurationRoot clonedConfigurationRoot = null;
            try
            {
                if (configurationRoot != null)
                {
                    clonedConfigurationRoot = new Main_Config_ConfigurationRoot();

                    clonedConfigurationRoot.ID = configurationRoot.ID;
                    clonedConfigurationRoot.MonitorID = configurationRoot.MonitorID;
                    clonedConfigurationRoot.DateAdded = configurationRoot.DateAdded;
                    clonedConfigurationRoot.Description = configurationRoot.Description;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneConfigurationRootDbObject(Main_Config_ConfigurationRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedConfigurationRoot;
        }

        public static List<Main_Config_ConfigurationRoot> Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB db)
        {
            List<Main_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Config_ConfigurationRoot
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static List<Main_Config_ConfigurationRoot> Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Config_ConfigurationRoot
                    where item.MonitorID == monitorID
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static bool Main_Config_WriteConfigurationRootTableEntry(Main_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.Main_Config_ConfigurationRoot.InsertOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteConfigurationRootTableEntry(Main_Config_ConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Main_Config_ConfigurationRoot configurationRoot = Main_Config_ReadConfigurationRootTableEntry(configurationRootID, db);
                success = Main_Config_DeleteConfigurationRootTableEntry(configurationRoot, db);
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return success;
        }

        public static bool Main_Config_DeleteConfigurationRootTableEntry(Main_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.Main_Config_ConfigurationRoot.DeleteOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteConfigurationRootTableEntries(List<Main_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    db.Main_Config_ConfigurationRoot.DeleteAllOnSubmit(configurationRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntries(List<Main_Config_ConfigurationRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<Main_Config_ConfigurationRoot> configurations = Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitorID, db);
                success = Main_Config_DeleteConfigurationRootTableEntries(configurations, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_ConfigurationRoot destinationConfigurationRoot;
                Main_Config_ConfigurationRoot sourceConfigurationRoot = Main_Config_ReadConfigurationRootTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceConfigurationRoot != null)
                {
                    destinationConfigurationRoot = Main_Config_CloneOneConfigurationRootDbObject(sourceConfigurationRoot);
                    destinationConfigurationRoot.ID = destinationConfigurationRootID;
                    destinationConfigurationRoot.MonitorID = destinationMonitorID;
                    success = Main_Config_WriteConfigurationRootTableEntry(destinationConfigurationRoot, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the configurationRoot object to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to find the configurationRoot object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_ConfigurationRoot configurationRoot = Main_Config_ReadConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to find the configurationRoot object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region Main Configuration  Calibration Data

        public static List<Main_Config_CalibrationData> Main_Config_ReadCalibrationDataTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_CalibrationData> calibrationData = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Config_CalibrationData
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.ChannelNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    calibrationData = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadCalibrationDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationData;
        }

        public static bool Main_Config_WriteCalibrationDataTableEntries(List<Main_Config_CalibrationData> calibrationData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((calibrationData != null) && (calibrationData.Count > 0))
                {
                    db.Main_Config_CalibrationData.InsertAllOnSubmit(calibrationData);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteCalibrationDataTableEntry(List<Main_Config_CalibrationData>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_CalibrationData Main_Config_CloneOneCalibrationDataDbObject(Main_Config_CalibrationData calibrationData)
        {
            Main_Config_CalibrationData clonedCalibrationData = null;
            try
            {
                if (calibrationData != null)
                {
                    clonedCalibrationData = new Main_Config_CalibrationData();

                    clonedCalibrationData.ID = calibrationData.ID;
                    clonedCalibrationData.ConfigurationRootID = calibrationData.ConfigurationRootID;
                    clonedCalibrationData.DateAdded = calibrationData.DateAdded;
                    clonedCalibrationData.ChannelNumber = calibrationData.ChannelNumber;
                    clonedCalibrationData.ChannelNoise = calibrationData.ChannelNoise;
                    clonedCalibrationData.ZeroLine = calibrationData.ZeroLine;
                    clonedCalibrationData.Multiplier = calibrationData.Multiplier;
                    clonedCalibrationData.Offset = calibrationData.Offset;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneCalibrationDataDbObject(Main_Config_CalibrationData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedCalibrationData;
        }

        public static List<Main_Config_CalibrationData> Main_Config_CloneCalibrationDataDbObjectList(List<Main_Config_CalibrationData> calibrationDataList)
        {
            List<Main_Config_CalibrationData> clonedCalibrationDataList = null;
            try
            {
                Main_Config_CalibrationData currentCalibrationData;
                if (calibrationDataList != null)
                {
                    clonedCalibrationDataList = new List<Main_Config_CalibrationData>();
                    foreach (Main_Config_CalibrationData entry in calibrationDataList)
                    {
                        currentCalibrationData = Main_Config_CloneOneCalibrationDataDbObject(entry);
                        if (currentCalibrationData != null)
                        {
                            clonedCalibrationDataList.Add(currentCalibrationData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneCalibrationDataDbObjectList(List<Main_Config_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedCalibrationDataList;
        }

        public static bool Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_CalibrationData> destinationCalibrationDataList;
                List<Main_Config_CalibrationData> sourceCalibrationDataList = Main_Config_ReadCalibrationDataTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceCalibrationDataList != null) && (sourceCalibrationDataList.Count > 0))
                {
                    destinationCalibrationDataList = Main_Config_CloneCalibrationDataDbObjectList(sourceCalibrationDataList);
                    for (int i = 0; i < destinationCalibrationDataList.Count; i++)
                    {
                        destinationCalibrationDataList[i].ID = Guid.NewGuid();
                        destinationCalibrationDataList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteCalibrationDataTableEntries(destinationCalibrationDataList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the data to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to get the data from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region Main Configuration  Communication Setup

        public static Main_Config_CommunicationSetup Main_Config_ReadCommunicationSetupTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_CommunicationSetup communicationSetup = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Config_CommunicationSetup
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    communicationSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadCommunicationSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return communicationSetup;
        }

        public static bool Main_Config_WriteCommunicationSetupTableEntry(Main_Config_CommunicationSetup communicationSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (communicationSetup != null)
                {
                    db.Main_Config_CommunicationSetup.InsertOnSubmit(communicationSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteCommunicationSetupTableEntry(Main_Config_CommunicationSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_CommunicationSetup Main_Config_CloneOneCommunicationSetupDbObject(Main_Config_CommunicationSetup communicationSetup)
        {
            Main_Config_CommunicationSetup clonedCommunicationSetup = null;
            try
            {
                if (communicationSetup != null)
                {
                    clonedCommunicationSetup = new Main_Config_CommunicationSetup();

                    clonedCommunicationSetup.ID = communicationSetup.ID;
                    clonedCommunicationSetup.ConfigurationRootID = communicationSetup.ConfigurationRootID;
                    clonedCommunicationSetup.DateAdded = communicationSetup.DateAdded;

                    clonedCommunicationSetup.ModBusAddress = communicationSetup.ModBusAddress;
                    clonedCommunicationSetup.BaudRateRS485 = communicationSetup.BaudRateRS485;
                    clonedCommunicationSetup.ModBusProtocol = communicationSetup.ModBusProtocol;
                    clonedCommunicationSetup.BaudRateXport = communicationSetup.BaudRateXport;
                    clonedCommunicationSetup.ModBusProtocolOverXport = communicationSetup.ModBusProtocolOverXport;
                    clonedCommunicationSetup.IP1 = communicationSetup.IP1;
                    clonedCommunicationSetup.IP2 = communicationSetup.IP2;
                    clonedCommunicationSetup.IP3 = communicationSetup.IP3;
                    clonedCommunicationSetup.IP4 = communicationSetup.IP4;
                    clonedCommunicationSetup.SaveDataIntervalInSeconds = communicationSetup.SaveDataIntervalInSeconds;
                    clonedCommunicationSetup.SaveDataIfChangedByPercent = communicationSetup.SaveDataIfChangedByPercent;
                    clonedCommunicationSetup.SaveDataIfChangedStatus = communicationSetup.SaveDataIfChangedStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneCommunicationSetupDbObject(Main_Config_CommunicationSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedCommunicationSetup;
        }

        public static bool Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_CommunicationSetup destinationCommunicationSetup;
                Main_Config_CommunicationSetup sourceCommunicationSetup = Main_Config_ReadCommunicationSetupTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceCommunicationSetup != null)
                {
                    destinationCommunicationSetup = Main_Config_CloneOneCommunicationSetupDbObject(sourceCommunicationSetup);
                    destinationCommunicationSetup.ID = Guid.NewGuid();
                    destinationCommunicationSetup.ConfigurationRootID = destinationConfigurationRootID;
                    success = Main_Config_WriteCommunicationSetupTableEntry(destinationCommunicationSetup, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the data to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to find the Main_Config_CommunicationSetup object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Device Setup

        public static Main_Config_DeviceSetup Main_Config_ReadDeviceSetupTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_DeviceSetup deviceSetup = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_DeviceSetup
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    deviceSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadDeviceSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceSetup;
        }

        public static bool Main_Config_WriteDeviceSetupTableEntry(Main_Config_DeviceSetup deviceSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (deviceSetup != null)
                {
                    db.Main_Config_DeviceSetup.InsertOnSubmit(deviceSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadDeviceSetupTableEntry(Main_Config_DeviceSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_DeviceSetup Main_Config_CloneOneDeviceSetupDbObject(Main_Config_DeviceSetup deviceSetup)
        {
            Main_Config_DeviceSetup clonedDeviceSetup = null;
            try
            {
                if (deviceSetup != null)
                {
                    clonedDeviceSetup = new Main_Config_DeviceSetup();

                    clonedDeviceSetup.ID = deviceSetup.ID;
                    clonedDeviceSetup.ConfigurationRootID = deviceSetup.ConfigurationRootID;
                    clonedDeviceSetup.DateAdded = deviceSetup.DateAdded;
                    clonedDeviceSetup.SetupStatus = deviceSetup.SetupStatus;
                    clonedDeviceSetup.Monitoring = deviceSetup.Monitoring;
                    clonedDeviceSetup.Frequency = deviceSetup.Frequency;
                    clonedDeviceSetup.AlarmControl = deviceSetup.AlarmControl;
                    clonedDeviceSetup.WarningControl = deviceSetup.WarningControl;
                    clonedDeviceSetup.AllowDirectAccess = deviceSetup.AllowDirectAccess;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneDeviceSetupDbObject(Main_Config_DeviceSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDeviceSetup;
        }

        public static bool Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_DeviceSetup destinationDeviceSetup;
                Main_Config_DeviceSetup sourceDeviceSetup = Main_Config_ReadDeviceSetupTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceDeviceSetup != null)
                {
                    destinationDeviceSetup = Main_Config_CloneOneDeviceSetupDbObject(sourceDeviceSetup);
                    destinationDeviceSetup.ID = Guid.NewGuid();
                    destinationDeviceSetup.ConfigurationRootID = destinationConfigurationRootID;
                    success = Main_Config_WriteDeviceSetupTableEntry(destinationDeviceSetup, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the Main_Config_DeviceSetup object to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to find the Main_Config_DeviceSetup object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  FullStatusInformation

        public static List<Main_Config_FullStatusInformation> Main_Config_ReadFullStatusInformationTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_FullStatusInformation> fullStatusInformation = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_FullStatusInformation
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.ModuleNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    fullStatusInformation = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadFullStatusInformationTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return fullStatusInformation;
        }

        public static bool Main_Config_WriteFullStatusInformationTableEntries(List<Main_Config_FullStatusInformation> fullStatusInformation, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((fullStatusInformation != null) && (fullStatusInformation.Count > 0))
                {
                    db.Main_Config_FullStatusInformation.InsertAllOnSubmit(fullStatusInformation);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteFullStatusInformationTableEntries(List<Main_Config_FullStatusInformation>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_FullStatusInformation Main_Config_CloneOneFullStatusInformationDbObject(Main_Config_FullStatusInformation fullStatusInformation)
        {
            Main_Config_FullStatusInformation clonedFullStatusInformation = null;
            try
            {
                if (fullStatusInformation != null)
                {
                    clonedFullStatusInformation = new Main_Config_FullStatusInformation();

                    clonedFullStatusInformation.ID = fullStatusInformation.ID;
                    clonedFullStatusInformation.ConfigurationRootID = fullStatusInformation.ConfigurationRootID;
                    clonedFullStatusInformation.DateAdded = fullStatusInformation.DateAdded;

                    clonedFullStatusInformation.ModuleNumber = fullStatusInformation.ModuleNumber;
                    clonedFullStatusInformation.ModuleStatus = fullStatusInformation.ModuleStatus;
                    clonedFullStatusInformation.ModuleType = fullStatusInformation.ModuleType;
                    clonedFullStatusInformation.ModBusAddress = fullStatusInformation.ModBusAddress;
                    clonedFullStatusInformation.BaudRate = fullStatusInformation.BaudRate;
                    clonedFullStatusInformation.ReadDatePeriodInSeconds = fullStatusInformation.ReadDatePeriodInSeconds;
                    clonedFullStatusInformation.ConnectionType = fullStatusInformation.ConnectionType;
                    clonedFullStatusInformation.Reserved_0 = fullStatusInformation.Reserved_0;
                    clonedFullStatusInformation.Reserved_1 = fullStatusInformation.Reserved_1;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneFullStatusInformationDbObject(Main_Config_FullStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedFullStatusInformation;
        }

        public static List<Main_Config_FullStatusInformation> Main_Config_CloneFullStatusInformationDbObjectList(List<Main_Config_FullStatusInformation> fullStatusInformationList)
        {
            List<Main_Config_FullStatusInformation> clonedFullStatusInformationList = null;
            try
            {
                Main_Config_FullStatusInformation fullStatusInformation;
                if (fullStatusInformationList != null)
                {
                    clonedFullStatusInformationList = new List<Main_Config_FullStatusInformation>();
                    foreach (Main_Config_FullStatusInformation entry in fullStatusInformationList)
                    {
                        fullStatusInformation = Main_Config_CloneOneFullStatusInformationDbObject(entry);
                        if (fullStatusInformation != null)
                        {
                            clonedFullStatusInformationList.Add(fullStatusInformation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteFullStatusInformationTableEntries(List<Main_Config_FullStatusInformation>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedFullStatusInformationList;
        }

        public static bool Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_FullStatusInformation> destinationullStatusInformationList;
                List<Main_Config_FullStatusInformation> sourceFullStatusInformationList = Main_Config_ReadFullStatusInformationTableEntries(sourceConfigurationRootID, sourceDB);
                if (sourceFullStatusInformationList != null)
                {
                    destinationullStatusInformationList=Main_Config_CloneFullStatusInformationDbObjectList(sourceFullStatusInformationList);
                    for (int i = 0; i < destinationullStatusInformationList.Count; i++)
                    {
                        destinationullStatusInformationList[i].ID = Guid.NewGuid();
                        destinationullStatusInformationList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteFullStatusInformationTableEntries(destinationullStatusInformationList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_FullStatusInformation> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_FullStatusInformation> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  General

        public static Main_Config_General Main_Config_ReadGeneralTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_General general = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_General
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    general = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadGeneralTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return general;
        }

        public static bool Main_Config_WriteGeneralTableEntry(Main_Config_General general, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (general != null)
                {
                    db.Main_Config_General.InsertOnSubmit(general);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteGeneralTableEntry(Main_Config_General, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_General Main_Config_CloneOneGeneralDbObject(Main_Config_General general)
        {
            Main_Config_General clonedGeneral = null;
            try
            {
                if (general != null)
                {
                    clonedGeneral = new Main_Config_General();

                    clonedGeneral.ID = general.ID;
                    clonedGeneral.ConfigurationRootID = general.ConfigurationRootID;
                    clonedGeneral.DateAdded = general.DateAdded;

                    clonedGeneral.DeviceType = general.DeviceType;
                    clonedGeneral.FirmwareVersion = general.FirmwareVersion;
                    clonedGeneral.GlobalStatus = general.GlobalStatus;
                    clonedGeneral.DevicesError = general.DevicesError;
                    clonedGeneral.DeviceDateTime = general.DeviceDateTime;
                    clonedGeneral.UnitName_0 = general.UnitName_0;
                    clonedGeneral.UnitName_1 = general.UnitName_1;
                    clonedGeneral.UnitName_2 = general.UnitName_2;
                    clonedGeneral.UnitName_3 = general.UnitName_3;
                    clonedGeneral.UnitName_4 = general.UnitName_4;
                    clonedGeneral.UnitName_5 = general.UnitName_5;
                    clonedGeneral.UnitName_6 = general.UnitName_6;
                    clonedGeneral.UnitName_7 = general.UnitName_7;
                    clonedGeneral.UnitName_8 = general.UnitName_8;
                    clonedGeneral.UnitName_9 = general.UnitName_9;
                    clonedGeneral.UnitName_10 = general.UnitName_10;
                    clonedGeneral.UnitName_11 = general.UnitName_11;
                    clonedGeneral.UnitName_12 = general.UnitName_12;
                    clonedGeneral.UnitName_13 = general.UnitName_13;
                    clonedGeneral.UnitName_14 = general.UnitName_14;
                    clonedGeneral.UnitName_15 = general.UnitName_15;
                    clonedGeneral.UnitName_16 = general.UnitName_16;
                    clonedGeneral.UnitName_17 = general.UnitName_17;
                    clonedGeneral.UnitName_18 = general.UnitName_18;
                    clonedGeneral.UnitName_19 = general.UnitName_19;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneGeneralDbObject(Main_Config_General)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedGeneral;
        }     

        public static bool Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyGeneralForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_General destinationGeneral;
                Main_Config_General sourceGeneral = Main_Config_ReadGeneralTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceGeneral != null)
                {
                    destinationGeneral = Main_Config_CloneOneGeneralDbObject(sourceGeneral);
                    destinationGeneral.ID = Guid.NewGuid();
                    destinationGeneral.ConfigurationRootID = destinationConfigurationRootID;
                    success = Main_Config_WriteGeneralTableEntry(destinationGeneral, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the Main_Config_General object to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the Main_Config_General object from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyGeneralForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Input Channel

        public static List<Main_Config_InputChannel> Main_Config_ReadInputChannelTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_InputChannel> inputChannel = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_InputChannel
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.InputChannelNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    inputChannel = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannel;
        }

        public static bool Main_Config_WriteInputChannelTableEntries(List<Main_Config_InputChannel> inputChannel, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((inputChannel != null) && (inputChannel.Count > 0))
                {
                    db.Main_Config_InputChannel.InsertAllOnSubmit(inputChannel);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelTableEntries(List<Main_Config_InputChannel>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_InputChannel Main_Config_CloneOneInputChannelDbObject(Main_Config_InputChannel inputChannel)
        {
            Main_Config_InputChannel clonedInputChannel = null;
            try
            {
                if (inputChannel != null)
                {
                    clonedInputChannel = new Main_Config_InputChannel();

                    clonedInputChannel.ID = inputChannel.ID;
                    clonedInputChannel.ConfigurationRootID = inputChannel.ConfigurationRootID;
                    clonedInputChannel.DateAdded = inputChannel.DateAdded;

                    clonedInputChannel.InputChannelNumber = inputChannel.InputChannelNumber;
                    clonedInputChannel.ReadStatus = inputChannel.ReadStatus;
                    clonedInputChannel.SensorType = inputChannel.SensorType;
                    clonedInputChannel.Unit = inputChannel.Unit;
                    clonedInputChannel.SensorID = inputChannel.SensorID;
                    clonedInputChannel.SensorNumber = inputChannel.SensorNumber;
                    clonedInputChannel.Sensitivity = inputChannel.Sensitivity;
                    clonedInputChannel.HighPassFilter = inputChannel.HighPassFilter;
                    clonedInputChannel.LowPassFilter = inputChannel.LowPassFilter;
                    clonedInputChannel.ReadFrequency = inputChannel.ReadFrequency;
                    clonedInputChannel.ReadPoints = inputChannel.ReadPoints;
                    clonedInputChannel.Averages = inputChannel.Averages;
                    clonedInputChannel.SaveDataIfChangedPercent = inputChannel.SaveDataIfChangedPercent;
                    clonedInputChannel.SaveDataIfChangedValue = inputChannel.SaveDataIfChangedValue;
                    clonedInputChannel.JunkValue_1 = inputChannel.JunkValue_1;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneInputChannel(Main_Config_InputChannel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannel;
        }

        public static List<Main_Config_InputChannel> Main_Config_CloneInputChannelDbObjectList(List<Main_Config_InputChannel> inputChannelList)
        {
            List<Main_Config_InputChannel> clonedInputChannelList = null;
            try
            {
                Main_Config_InputChannel inputChannel;
                if (inputChannelList != null)
                {
                    clonedInputChannelList = new List<Main_Config_InputChannel>();
                    foreach (Main_Config_InputChannel entry in inputChannelList)
                    {
                        inputChannel = Main_Config_CloneOneInputChannelDbObject(entry);
                        if (inputChannel != null)
                        {
                            clonedInputChannelList.Add(inputChannel);
                        }
                        else
                        {
                            string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CloneInputChannelDbObjectList(List<Main_Config_InputChannel>)\nFailed to clone an internal Main_Config_InputChannel object.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CloneInputChannelDbObjectList(List<Main_Config_InputChannel>)\nInput List<Main_Config_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneInputChannelDbObjectList(List<Main_Config_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannelList;
        }

        public static bool Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_InputChannel> destinationInputChannelList;
                List<Main_Config_InputChannel> sourceInputChannelList = Main_Config_ReadInputChannelTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceInputChannelList != null)&&(sourceInputChannelList.Count > 0))
                {
                    destinationInputChannelList = Main_Config_CloneInputChannelDbObjectList(sourceInputChannelList);
                    for (int i = 0; i < destinationInputChannelList.Count; i++)
                    {
                        destinationInputChannelList[i].ID = Guid.NewGuid();
                        destinationInputChannelList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteInputChannelTableEntries(destinationInputChannelList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_InputChannel> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_InputChannel> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Input Channel Effluvia

        public static List<Main_Config_InputChannelEffluvia> Main_Config_ReadInputChannelEffluviaTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_InputChannelEffluvia> inputChannel = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_InputChannelEffluvia
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.InputChannelNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    inputChannel = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelEffluviaTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannel;
        }

        public static bool Main_Config_WriteInputChannelEffluviaTableEntries(List<Main_Config_InputChannelEffluvia> inputChannel, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((inputChannel != null) && (inputChannel.Count > 0))
                {
                    db.Main_Config_InputChannelEffluvia.InsertAllOnSubmit(inputChannel);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelEffluviaTableEntries(List<Main_Config_InputChannelEffluvia>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<Main_Config_InputChannelEffluvia> Main_Config_CloneInputChannelEffluviaDbObjectList(List<Main_Config_InputChannelEffluvia> inputChannelEffluviaList)
        {
            List<Main_Config_InputChannelEffluvia> clonedInputChannelEffluviaList = null;
            try
            {
                Main_Config_InputChannelEffluvia inputChannelEffluvia;
                if (inputChannelEffluviaList != null)
                {
                    clonedInputChannelEffluviaList = new List<Main_Config_InputChannelEffluvia>();
                    foreach (Main_Config_InputChannelEffluvia entry in inputChannelEffluviaList)
                    {
                        inputChannelEffluvia = Main_Config_CloneOneInputChannelEffluviaDbObject(entry);
                        if (inputChannelEffluvia != null)
                        {
                            clonedInputChannelEffluviaList.Add(inputChannelEffluvia);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneInputChannelEffluviaDbObjectList(List<Main_Config_InputChannelEffluvia>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannelEffluviaList;
        }

        public static Main_Config_InputChannelEffluvia Main_Config_CloneOneInputChannelEffluviaDbObject(Main_Config_InputChannelEffluvia inputChannel)
        {
            Main_Config_InputChannelEffluvia clonedInputChannel = null;
            try
            {
                if (inputChannel != null)
                {
                    clonedInputChannel = new Main_Config_InputChannelEffluvia();

                    clonedInputChannel.ID = inputChannel.ID;
                    clonedInputChannel.ConfigurationRootID = inputChannel.ConfigurationRootID;
                    clonedInputChannel.DateAdded = inputChannel.DateAdded;
                    clonedInputChannel.InputChannelNumber = inputChannel.InputChannelNumber;

                    clonedInputChannel.Value_1 = inputChannel.Value_1;
                    clonedInputChannel.Value_2 = inputChannel.Value_2;
                    clonedInputChannel.Value_3 = inputChannel.Value_3;
                    clonedInputChannel.Value_4 = inputChannel.Value_4;
                    clonedInputChannel.Value_5 = inputChannel.Value_5;
                    clonedInputChannel.Value_6 = inputChannel.Value_6;
                    clonedInputChannel.Value_7 = inputChannel.Value_7;
                    clonedInputChannel.Value_8 = inputChannel.Value_8;
                    clonedInputChannel.Value_9 = inputChannel.Value_9;
                    clonedInputChannel.Value_10 = inputChannel.Value_10;
                    clonedInputChannel.Value_11 = inputChannel.Value_11;
                    clonedInputChannel.Value_12 = inputChannel.Value_12;
                    clonedInputChannel.Value_13 = inputChannel.Value_13;
                    clonedInputChannel.Value_14 = inputChannel.Value_14;
                    clonedInputChannel.Value_15 = inputChannel.Value_15;
                    clonedInputChannel.Value_16 = inputChannel.Value_16;
                    clonedInputChannel.Value_17 = inputChannel.Value_17;
                    clonedInputChannel.Value_18 = inputChannel.Value_18;
                    clonedInputChannel.Value_19 = inputChannel.Value_19;
                    clonedInputChannel.Value_20 = inputChannel.Value_20;
                    clonedInputChannel.Value_21 = inputChannel.Value_21;
                    clonedInputChannel.Value_22 = inputChannel.Value_22;
                    clonedInputChannel.Value_23 = inputChannel.Value_23;
                    clonedInputChannel.Value_24 = inputChannel.Value_24;
                    clonedInputChannel.Value_25 = inputChannel.Value_25;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneInputChannelEffluviaDbObject(Main_Config_InputChannelEffluvia)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannel;
        }
 
        public static bool Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_InputChannelEffluvia> destinationInputChannelEffluviaList;
                List<Main_Config_InputChannelEffluvia> sourceInputChannelEffluviaList = Main_Config_ReadInputChannelEffluviaTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceInputChannelEffluviaList != null)&&(sourceInputChannelEffluviaList.Count > 0))
                {
                    destinationInputChannelEffluviaList = Main_Config_CloneInputChannelEffluviaDbObjectList(sourceInputChannelEffluviaList);
                    for (int i = 0; i < destinationInputChannelEffluviaList.Count; i++)
                    {
                        destinationInputChannelEffluviaList[i].ID = Guid.NewGuid();
                        destinationInputChannelEffluviaList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteInputChannelEffluviaTableEntries(destinationInputChannelEffluviaList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_InputChannelEffluvia> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_InputChannelEffluvia> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Input Channel Threshold

        public static List<Main_Config_InputChannelThreshold> Main_Config_ReadInputChannelThresholdTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_InputChannelThreshold> inputChannelThreshold = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_InputChannelThreshold
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.InputChannelNumber ascending, item.ThresholdNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    inputChannelThreshold = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadInputChannelThresholdTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannelThreshold;
        }

        public static bool Main_Config_WriteInputChannelThresholdTableEntries(List<Main_Config_InputChannelThreshold> inputChannelThreshold, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((inputChannelThreshold != null) && (inputChannelThreshold.Count > 0))
                {
                    db.Main_Config_InputChannelThreshold.InsertAllOnSubmit(inputChannelThreshold);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteInputChannelThresholdTableEntries(List<Main_Config_InputChannel>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<Main_Config_InputChannelThreshold> Main_Config_CloneInputChannelThresholdDbObjectList(List<Main_Config_InputChannelThreshold> inputChannelThresholdList)
        {
            List<Main_Config_InputChannelThreshold> clonedInputChannelThresholdList = null;
            try
            {
                Main_Config_InputChannelThreshold inputChannelThreshold;
                if (inputChannelThresholdList != null)
                {
                    clonedInputChannelThresholdList = new List<Main_Config_InputChannelThreshold>();
                    foreach (Main_Config_InputChannelThreshold entry in inputChannelThresholdList)
                    {
                        inputChannelThreshold = Main_Config_CloneOneInputChannelThresholdDbObject(entry);
                        if (inputChannelThreshold != null)
                        {
                            clonedInputChannelThresholdList.Add(inputChannelThreshold);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneInputChannelThresholdDbObjectList(List<Main_Config_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannelThresholdList;
        }

        public static Main_Config_InputChannelThreshold Main_Config_CloneOneInputChannelThresholdDbObject(Main_Config_InputChannelThreshold inputChannelThreshold)
        {
            Main_Config_InputChannelThreshold clonedInputChannelThreshold = null;
            try
            {
                if (inputChannelThreshold != null)
                {
                    clonedInputChannelThreshold = new Main_Config_InputChannelThreshold();

                    clonedInputChannelThreshold.ID = inputChannelThreshold.ID;
                    clonedInputChannelThreshold.ConfigurationRootID = inputChannelThreshold.ConfigurationRootID;
                    clonedInputChannelThreshold.DateAdded = inputChannelThreshold.DateAdded;

                    clonedInputChannelThreshold.InputChannelNumber = inputChannelThreshold.InputChannelNumber;
                    clonedInputChannelThreshold.ThresholdNumber = inputChannelThreshold.ThresholdNumber;
                    clonedInputChannelThreshold.ThresholdEnable = inputChannelThreshold.ThresholdEnable;
                    clonedInputChannelThreshold.ThresholdStatus = inputChannelThreshold.ThresholdStatus;
                    clonedInputChannelThreshold.RelayMode = inputChannelThreshold.RelayMode;
                    clonedInputChannelThreshold.Calculation = inputChannelThreshold.Calculation;
                    clonedInputChannelThreshold.WorkingBy = inputChannelThreshold.WorkingBy;
                    clonedInputChannelThreshold.LowFrequencyOfSignal = inputChannelThreshold.LowFrequencyOfSignal;
                    clonedInputChannelThreshold.HighFrequencyOfSignal = inputChannelThreshold.HighFrequencyOfSignal;
                    clonedInputChannelThreshold.RelayStatusOnDelay = inputChannelThreshold.RelayStatusOnDelay;
                    clonedInputChannelThreshold.RelayStatusOffDelay = inputChannelThreshold.RelayStatusOffDelay;
                    clonedInputChannelThreshold.ThresholdValue = inputChannelThreshold.ThresholdValue;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneInputChannelThresholdDbObject(Main_Config_InputChannelThreshold)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInputChannelThreshold;
        }

        public static bool Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_InputChannelThreshold> destinationInputChannelThresholdList;
                List<Main_Config_InputChannelThreshold> sourceInputChannelThresholdList = Main_Config_ReadInputChannelThresholdTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceInputChannelThresholdList != null)&&(sourceInputChannelThresholdList.Count > 0))
                {
                    destinationInputChannelThresholdList=Main_Config_CloneInputChannelThresholdDbObjectList(sourceInputChannelThresholdList);
                    for (int i = 0; i < destinationInputChannelThresholdList.Count; i++)
                    {
                        destinationInputChannelThresholdList[i].ID = Guid.NewGuid();
                        destinationInputChannelThresholdList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteInputChannelThresholdTableEntries(destinationInputChannelThresholdList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_InputChannelThreshold> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_InputChannelThreshold> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown MainMonitor_DatabaseMethods.Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Short Status Information Setup

        public static List<Main_Config_ShortStatusInformation> Main_Config_ReadShortStatusInformationTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_ShortStatusInformation> shortStatusInformation = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_ShortStatusInformation
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.ModuleNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    shortStatusInformation = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadShortStatusInformationTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shortStatusInformation;
        }

        public static bool Main_Config_WriteShortStatusInformationTableEntries(List<Main_Config_ShortStatusInformation> shortStatusInformation, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((shortStatusInformation != null) && (shortStatusInformation.Count > 0))
                {
                    db.Main_Config_ShortStatusInformation.InsertAllOnSubmit(shortStatusInformation);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteShortStatusInformationTableEntries(List<Main_Config_ShortStatusInformation>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<Main_Config_ShortStatusInformation> Main_Config_CloneShortStatusInformationDbObjectList(List<Main_Config_ShortStatusInformation> shortStatusInformationList)
        {
            List<Main_Config_ShortStatusInformation> clonedShortStatusInformationList = null;
            try
            {
                Main_Config_ShortStatusInformation shortStatusInformation;
                if (shortStatusInformationList != null)
                {
                    clonedShortStatusInformationList = new List<Main_Config_ShortStatusInformation>();
                    foreach (Main_Config_ShortStatusInformation entry in shortStatusInformationList)
                    {
                        shortStatusInformation = Main_Config_CloneOneShortStatusInformationDbObject(entry);
                        if (shortStatusInformation != null)
                        {
                            clonedShortStatusInformationList.Add(shortStatusInformation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneShortStatusInformationDbObjectList(List<Main_Config_ShortStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedShortStatusInformationList;
        }

        public static Main_Config_ShortStatusInformation Main_Config_CloneOneShortStatusInformationDbObject(Main_Config_ShortStatusInformation shortStatusInformation)
        {
            Main_Config_ShortStatusInformation clonedShortStatusInformation = null;
            try
            {
                if (shortStatusInformation != null)
                {
                    clonedShortStatusInformation = new Main_Config_ShortStatusInformation();

                    clonedShortStatusInformation.ID = shortStatusInformation.ID;
                    clonedShortStatusInformation.ConfigurationRootID = shortStatusInformation.ConfigurationRootID;
                    clonedShortStatusInformation.DateAdded = shortStatusInformation.DateAdded;

                    clonedShortStatusInformation.ModuleNumber = shortStatusInformation.ModuleNumber;
                    clonedShortStatusInformation.ModuleStatus = shortStatusInformation.ModuleStatus;
                    clonedShortStatusInformation.ModuleType = shortStatusInformation.ModuleType;
                    clonedShortStatusInformation.Reserved = shortStatusInformation.Reserved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneShortStatusInformationDbObject(Main_Config_ShortStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedShortStatusInformation;
        }

        public static bool Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_ShortStatusInformation> destinationShortStatusInformationList;
                List<Main_Config_ShortStatusInformation> sourceShortStatusInformationList = Main_Config_ReadShortStatusInformationTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceShortStatusInformationList != null) && (sourceShortStatusInformationList.Count > 0))
                {
                    destinationShortStatusInformationList = Main_Config_CloneShortStatusInformationDbObjectList(sourceShortStatusInformationList);
                    for (int i = 0; i < destinationShortStatusInformationList.Count; i++)
                    {
                        destinationShortStatusInformationList[i].ID = Guid.NewGuid();
                        destinationShortStatusInformationList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteShortStatusInformationTableEntries(destinationShortStatusInformationList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_ShortStatusInformation> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_ShortStatusInformation> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region  Main Configuration  Transfer Setup

        public static List<Main_Config_TransferSetup> Main_Config_ReadTransferSetupTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<Main_Config_TransferSetup> transferSetup = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_TransferSetup
                    where item.ConfigurationRootID == configurationRootID
                    orderby item.TransferNumber ascending
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    transferSetup = queryList.ToList();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadTransferSetupTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transferSetup;
        }

        public static bool Main_Config_WriteTransferSetupTableEntries(List<Main_Config_TransferSetup> transferSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((transferSetup != null) && (transferSetup.Count > 0))
                {
                    db.Main_Config_TransferSetup.InsertAllOnSubmit(transferSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteTransferSetupTableEntries(List<Main_Config_TransferSetup>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<Main_Config_TransferSetup> Main_Config_CloneTransferSetupDbObjectList(List<Main_Config_TransferSetup> transferSetupList)
        {
            List<Main_Config_TransferSetup> clonedTransferSetupList = null;
            try
            {
                Main_Config_TransferSetup transferSetup;
                if (transferSetupList != null)
                {
                    clonedTransferSetupList = new List<Main_Config_TransferSetup>();
                    foreach (Main_Config_TransferSetup entry in transferSetupList)
                    {
                        transferSetup = Main_Config_CloneOneTransferSetupDbObject(entry);
                        if (transferSetup != null)
                        {
                            clonedTransferSetupList.Add(transferSetup);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneTransferSetupDbObjectList(List<Main_Config_TransferSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTransferSetupList;
        }

        public static Main_Config_TransferSetup Main_Config_CloneOneTransferSetupDbObject(Main_Config_TransferSetup transferSetup)
        {
            Main_Config_TransferSetup clonedTransferSetup = null;
            try
            {
                if (transferSetup != null)
                {
                    clonedTransferSetup = new Main_Config_TransferSetup();

                    clonedTransferSetup.ID = transferSetup.ID;
                    clonedTransferSetup.ConfigurationRootID = transferSetup.ConfigurationRootID;
                    clonedTransferSetup.DateAdded = transferSetup.DateAdded;

                    clonedTransferSetup.TransferSetupNumber = transferSetup.TransferSetupNumber;
                    clonedTransferSetup.TransferNumber = transferSetup.TransferNumber;
                    clonedTransferSetup.Working = transferSetup.Working;
                    clonedTransferSetup.SourceModuleNumber = transferSetup.SourceModuleNumber;
                    clonedTransferSetup.SourceRegisterNumber = transferSetup.SourceRegisterNumber;
                    clonedTransferSetup.DestinationModuleNumber = transferSetup.DestinationModuleNumber;
                    clonedTransferSetup.DestinationRegisterNumber = transferSetup.DestinationRegisterNumber;
                    clonedTransferSetup.Multiplier = transferSetup.Multiplier;
                    clonedTransferSetup.Reserved = transferSetup.Reserved;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneTransferSetupDbObject(Main_Config_TransferSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTransferSetup;
        }

        public static bool Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<Main_Config_TransferSetup> destinationTransferSetupList;
                List<Main_Config_TransferSetup> sourceTransferSetupList = Main_Config_ReadTransferSetupTableEntries(sourceConfigurationRootID, sourceDB);
                if ((sourceTransferSetupList != null) && (sourceTransferSetupList.Count > 0))
                {
                    destinationTransferSetupList = Main_Config_CloneTransferSetupDbObjectList(sourceTransferSetupList);
                    for (int i = 0; i < destinationTransferSetupList.Count; i++)
                    {
                        destinationTransferSetupList[i].ID = Guid.NewGuid();
                        destinationTransferSetupList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = Main_Config_WriteTransferSetupTableEntries(destinationTransferSetupList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<Main_Config_TransferSetup> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<Main_Config_TransferSetup> frome the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        public static bool Main_Config_CopyOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationMonitorID, destinationDB);
                if (success)
                {
                    success = Main_Config_CopyCalibrationDataForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyCommunicationSetupForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyDeviceSetupForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyFullStatusInformationForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyGeneralForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyInputChannelDataForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyInputChannelEffluviaForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyInputChannelThresholdDataForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyShortStatusInformationForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = Main_Config_CopyTransferSetupForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool Main_Config_CopyOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_ConfigurationRoot configurationRoot = Main_Config_ReadConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = Main_Config_CopyOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }           
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<string> Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                string errorString;
                List<Main_Config_ConfigurationRoot> configurationRootList = null;
                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(destinationMonitorID, destinationDB);
                if (destinationMonitor != null)
                {
                    if (monitor != null)
                    {
                        configurationRootList = Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);

                        if ((configurationRootList != null) && (configurationRootList.Count > 0))
                        {
                            foreach (Main_Config_ConfigurationRoot configurationRoot in configurationRootList)
                            {
                                if (!Main_Config_CopyOneConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationMonitorID, destinationDB))
                                {
                                    errorString = configurationRoot.DateAdded.ToString() + "     " + configurationRoot.Description;
                                    failedWrites.Add(errorString);
                                    Main_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nNo configurations found for the input monitor.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nInput monitor was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to find the monitor in the destination database.  The configurations could not be copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        public static List<string> Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                if (monitor != null)
                {
                    failedWrites = Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(monitor, sourceDB, monitor.ID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Monitor, MonitorInterfaceDB, MonitorInterfaceDB)\nInput monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
//                string errorString;
//                List<Main_Config_ConfigurationRoot> configurationRootList = null;
//                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
//                if (destinationMonitor != null)
//                {
//                    configurationRootList = Main_Config_ReadAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);

//                    if ((configurationRootList != null) && (configurationRootList.Count > 0))
//                    {
//                        foreach (Main_Config_ConfigurationRoot configurationRoot in configurationRootList)
//                        {
//                            if (!Main_Config_CopyOneConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationDB))
//                            {
//                                errorString = configurationRoot.DateAdded.ToString() + "     " + configurationRoot.Description;
//                                failedWrites.Add(errorString);
//                                Main_Config_DeleteConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
//                            }
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nNo configurations found for the input monitor.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }                    
//                }
//                else
//                {
//                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to find the monitor in the destination database.  The configurations could not be copied.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyAllConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        #endregion

        #region WHS Configuration

        #region Main Configuration  WHS Configuration Root

        public static Main_Config_WHSConfigurationRoot Main_Config_ReadWHSConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_WHSConfigurationRoot configurationRoot = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_WHSConfigurationRoot
                    where item.ID == configurationRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    configurationRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadWHSConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRoot;
        }

        public static Main_Config_WHSConfigurationRoot Main_Config_CloneOneWHSConfigurationRootDbObject(Main_Config_WHSConfigurationRoot configurationRoot)
        {
            Main_Config_WHSConfigurationRoot clonedConfigurationRoot = null;
            try
            {
                if (configurationRoot != null)
                {
                    clonedConfigurationRoot = new Main_Config_WHSConfigurationRoot();

                    clonedConfigurationRoot.ID = configurationRoot.ID;
                    clonedConfigurationRoot.MonitorID = configurationRoot.MonitorID;
                    clonedConfigurationRoot.DateAdded = configurationRoot.DateAdded;
                    clonedConfigurationRoot.Description = configurationRoot.Description;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneWHSConfigurationRootDbObject(Main_Config_WHSConfigurationRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedConfigurationRoot;
        }

        public static List<Main_Config_WHSConfigurationRoot> Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB db)
        {
            List<Main_Config_WHSConfigurationRoot> configurationRootList = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_WHSConfigurationRoot
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static List<Main_Config_WHSConfigurationRoot> Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_Config_WHSConfigurationRoot> configurationRootList = null;
            
            try
            {
                var queryList =
                    from item in db.Main_Config_WHSConfigurationRoot
                    where item.MonitorID == monitorID
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static bool Main_Config_WriteWHSConfigurationRootTableEntry(Main_Config_WHSConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.Main_Config_WHSConfigurationRoot.InsertOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteWHSConfigurationRootTableEntry(Main_Config_WHSConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteWHSConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Main_Config_WHSConfigurationRoot configurationRoot = Main_Config_ReadWHSConfigurationRootTableEntry(configurationRootID, db);
                success = Main_Config_DeleteWHSConfigurationRootTableEntry(configurationRoot, db);
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return success;
        }

        public static bool Main_Config_DeleteWHSConfigurationRootTableEntry(Main_Config_WHSConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.Main_Config_WHSConfigurationRoot.DeleteOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntry(Main_Config_WHSConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteWHSConfigurationRootTableEntries(List<Main_Config_WHSConfigurationRoot> configurationRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    db.Main_Config_WHSConfigurationRoot.DeleteAllOnSubmit(configurationRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteWHSConfigurationRootTableEntries(List<Main_Config_WHSConfigurationRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_DeleteAllWHSConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            
            try
            {
                List<Main_Config_WHSConfigurationRoot> configurations = Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitorID, db);
                success = Main_Config_DeleteWHSConfigurationRootTableEntries(configurations, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_WHSConfigurationRoot destinationConfigurationRoot;
                Main_Config_WHSConfigurationRoot sourceConfigurationRoot = Main_Config_ReadWHSConfigurationRootTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceConfigurationRoot != null)
                {
                    destinationConfigurationRoot = Main_Config_CloneOneWHSConfigurationRootDbObject(sourceConfigurationRoot);
                    destinationConfigurationRoot.ID = destinationConfigurationRootID;
                    destinationConfigurationRoot.MonitorID = destinationMonitorID;
                    success = Main_Config_WriteWHSConfigurationRootTableEntry(destinationConfigurationRoot, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nFailed to write the configurationRoot object to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB. Guid, Guid, MonitorInterfaceDB)\nFailed to find the configurationRoot object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_WHSConfigurationRoot configurationRoot = Main_Config_ReadWHSConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to find the configurationRoot object in the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion


        #region  Main Configuration  Winding Hot Spot Setup

        public static Main_Config_WindingHotSpotSetup Main_Config_ReadWindingHotSpotTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            Main_Config_WindingHotSpotSetup windingHotSpotSetup = null;
            try
            {
                var queryList =
                    from item in db.Main_Config_WindingHotSpotSetup
                    where item.ConfigurationRootID == configurationRootID
                    select item;

                if ((queryList != null) && (queryList.Count() > 0))
                {
                    windingHotSpotSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_ReadWindingHotSpotSetupTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return windingHotSpotSetup;
        }

        public static bool Main_Config_WriteWindingHotSpotSetupTableEntry(Main_Config_WindingHotSpotSetup windingHotSpotSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (windingHotSpotSetup != null)
                {
                    db.Main_Config_WindingHotSpotSetup.InsertOnSubmit(windingHotSpotSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_WriteWindingHotSpotSetupTableEntries(List<Main_Config_WindingHotSpotSetup>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Config_WindingHotSpotSetup Main_Config_CloneOneWindingHotSpotSetupDbObject(Main_Config_WindingHotSpotSetup windingHotSpotSetup)
        {
            Main_Config_WindingHotSpotSetup clonedWindingHotSpotSetup = null;
            try
            {
                if (windingHotSpotSetup != null)
                {
                    clonedWindingHotSpotSetup = new Main_Config_WindingHotSpotSetup();

                    clonedWindingHotSpotSetup.ID = windingHotSpotSetup.ID;
                    clonedWindingHotSpotSetup.ConfigurationRootID = windingHotSpotSetup.ConfigurationRootID;
                    clonedWindingHotSpotSetup.DateAdded = windingHotSpotSetup.DateAdded;
                    clonedWindingHotSpotSetup.FirmwareVersion = windingHotSpotSetup.FirmwareVersion;

                    clonedWindingHotSpotSetup.RatingAsBitString = windingHotSpotSetup.RatingAsBitString;

                    clonedWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = windingHotSpotSetup.H_RatedHotSpotRisePhaseA;
                    clonedWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = windingHotSpotSetup.H_RatedHotSpotRisePhaseB;
                    clonedWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = windingHotSpotSetup.H_RatedHotSpotRisePhaseC;
                    clonedWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = windingHotSpotSetup.H_MaximumRatedCurrentPhaseA;
                    clonedWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = windingHotSpotSetup.H_MaximumRatedCurrentPhaseB;
                    clonedWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = windingHotSpotSetup.H_MaximumRatedCurrentPhaseC;

                    
                    clonedWindingHotSpotSetup.FanSetpoint_H = windingHotSpotSetup.FanSetpoint_H;
                    clonedWindingHotSpotSetup.FanSetpoint_HH = windingHotSpotSetup.FanSetpoint_HH;

                    clonedWindingHotSpotSetup.Exponent = windingHotSpotSetup.Exponent;
                    clonedWindingHotSpotSetup.WindingTimeConstant = windingHotSpotSetup.WindingTimeConstant;
                   
                    clonedWindingHotSpotSetup.AlarmDeadBandTemperature = windingHotSpotSetup.AlarmDeadBandTemperature;
                    clonedWindingHotSpotSetup.AlarmDelayInSeconds = windingHotSpotSetup.AlarmDelayInSeconds;

                    clonedWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = windingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                    clonedWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = windingHotSpotSetup.AlarmMaxSetPointTemperature_HH;

                    clonedWindingHotSpotSetup.AlarmTopOilTemperature_H = windingHotSpotSetup.AlarmTopOilTemperature_H;
                    clonedWindingHotSpotSetup.AlarmTopOilTemperature_HH = windingHotSpotSetup.AlarmTopOilTemperature_HH;

                    clonedWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber;
                    

                    clonedWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber;
                    clonedWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber;
                    clonedWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber;

                   

                    clonedWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = windingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                    clonedWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;

                    clonedWindingHotSpotSetup.MinimumFanRunTimeInMinutes = windingHotSpotSetup.MinimumFanRunTimeInMinutes;
                    clonedWindingHotSpotSetup.TimeBetweenFanExerciseInDays = windingHotSpotSetup.TimeBetweenFanExerciseInDays;
                    clonedWindingHotSpotSetup.TimeToExerciseFanInMinutes = windingHotSpotSetup.TimeToExerciseFanInMinutes;
                   // clonedWindingHotSpotSetup.DateOfLastFanExercise = windingHotSpotSetup.DateOfLastFanExercise;
                    clonedWindingHotSpotSetup.FanOneRuntimeInHours = windingHotSpotSetup.FanOneRuntimeInHours;
                    clonedWindingHotSpotSetup.FanTwoRuntimeInHours = windingHotSpotSetup.FanTwoRuntimeInHours;

                    clonedWindingHotSpotSetup.FanBankSwitchingTimeInHours = windingHotSpotSetup.FanBankSwitchingTimeInHours;

                    clonedWindingHotSpotSetup.HotSpotFactor = windingHotSpotSetup.HotSpotFactor;
                    clonedWindingHotSpotSetup.WHS_Exponent_B = windingHotSpotSetup.WHS_Exponent_B;
                    clonedWindingHotSpotSetup.WHS_Exponent_C = windingHotSpotSetup.WHS_Exponent_C; 
                    clonedWindingHotSpotSetup.WHS_Winding_Time_Const_B = windingHotSpotSetup.WHS_Winding_Time_Const_B;
                    clonedWindingHotSpotSetup.WHS_Winding_Time_Const_C = windingHotSpotSetup.WHS_Winding_Time_Const_C;
                    clonedWindingHotSpotSetup.Fan1_BaseCurrent = windingHotSpotSetup.Fan1_BaseCurrent;
                    clonedWindingHotSpotSetup.Fan2_BaseCurrent = windingHotSpotSetup.Fan2_BaseCurrent;
                    clonedWindingHotSpotSetup.Fan_pu_low_alarm_factor = windingHotSpotSetup.Fan_pu_low_alarm_factor;
                    clonedWindingHotSpotSetup.Fan_pu_high_alarm_factor = windingHotSpotSetup.Fan_pu_high_alarm_factor;
                    clonedWindingHotSpotSetup.Num_of_cooling_groups = windingHotSpotSetup.Num_of_cooling_groups;
                    clonedWindingHotSpotSetup.Whs_previous_ageingA = windingHotSpotSetup.Whs_previous_ageingA;
                    clonedWindingHotSpotSetup.Whs_previous_ageingB = windingHotSpotSetup.Whs_previous_ageingB;
                    clonedWindingHotSpotSetup.Whs_previous_ageingC = windingHotSpotSetup.Whs_previous_ageingC;


                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CloneOneWindingHotSpotSetupDbObject(Main_Config_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedWindingHotSpotSetup;
        }

        public static bool Main_Config_CopyWindingHotSpotSetupForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyWindingHotSpotSetupForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneWindingHotSpotSetupTableEntryToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyWindingHotSpotSetupForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_WindingHotSpotSetup destinationWindingHotSpotSetup;
                Main_Config_WindingHotSpotSetup sourceWindingHotSpotSetup = Main_Config_ReadWindingHotSpotTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceWindingHotSpotSetup != null)
                {
                    destinationWindingHotSpotSetup=Main_Config_CloneOneWindingHotSpotSetupDbObject(sourceWindingHotSpotSetup);
                    destinationWindingHotSpotSetup.ID = Guid.NewGuid();
                    destinationWindingHotSpotSetup.ConfigurationRootID = destinationConfigurationRootID;
                    success = Main_Config_WriteWindingHotSpotSetupTableEntry(destinationWindingHotSpotSetup, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyWindingHotSpotSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the Main_Config_General object to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneWindingHotSpotSetupTableEntryToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        #endregion

        public static bool Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyWHSConfigruationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationMonitorID, destinationDB);
                if (success)
                {
                    success = Main_Config_CopyWindingHotSpotSetupForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = Main_Config_CopyOneWHSConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Config_WHSConfigurationRoot configurationRoot = Main_Config_ReadWHSConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = Main_Config_CopyOneWHSConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }      
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyOneWHSConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<string> Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                string errorString;
                List<Main_Config_WHSConfigurationRoot> configurationRootList = null;
                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
                if (destinationMonitor != null)
                {
                    if (monitor != null)
                    {
                        configurationRootList = Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);

                        if ((configurationRootList != null) && (configurationRootList.Count > 0))
                        {
                            foreach (Main_Config_WHSConfigurationRoot configurationRoot in configurationRootList)
                            {
                                if (!Main_Config_CopyOneWHSConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationMonitorID, destinationDB))
                                {
                                    errorString = configurationRoot.DateAdded.ToString() + "     " + configurationRoot.Description;
                                    failedWrites.Add(errorString);
                                    Main_Config_DeleteWHSConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nNo configurations found for the input monitor.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nInput monitor was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to find the monitor in the destination database.  The configurations could not be copied.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        public static List<string> Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Monitor monitor, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            List<string> failedWrites = new List<string>();
            try
            {
                if (monitor != null)
                {
                    failedWrites = Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(monitor, sourceDB, monitor.ID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nInput monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                //                string errorString;
                //                List<Main_Config_WHSConfigurationRoot> configurationRootList = null;
                //                Monitor destinationMonitor = General_DatabaseMethods.GetOneMonitor(monitor.ID, destinationDB);
                //                if (destinationMonitor != null)
                //                {
                //                    configurationRootList = Main_Config_ReadAllWHSConfigurationRootTableEntriesForOneMonitor(monitor.ID, sourceDB);

                //                    if ((configurationRootList != null) && (configurationRootList.Count > 0))
                //                    {
                //                        foreach (Main_Config_WHSConfigurationRoot configurationRoot in configurationRootList)
                //                        {
                //                            if (!Main_Config_CopyOneWHSConfigurationToNewDatabase(configurationRoot.ID, sourceDB, destinationDB))
                //                            {
                //                                errorString = configurationRoot.DateAdded.ToString() + "     " + configurationRoot.Description;
                //                                failedWrites.Add(errorString);
                //                                Main_Config_DeleteWHSConfigurationRootTableEntry(configurationRoot.ID, destinationDB);
                //                            }
                //                        }
                //                    }
                //                    else
                //                    {
                //                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nNo configurations found for the input monitor.";
                //                        LogMessage.LogError(errorMessage);
                //#if DEBUG
                //                        MessageBox.Show(errorMessage);
                //#endif
                //                    }
                //                }
                //                else
                //                {
                //                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to find the monitor in the destination database.  The configurations could not be copied.";
                //                    LogMessage.LogError(errorMessage);
                //#if DEBUG
                //                    MessageBox.Show(errorMessage);
                //#endif
                //                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Config_CopyAllWHSConfigurationsForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return failedWrites;
        }

        #endregion

        #region Main Data DataRoot

        public static bool Main_Data_WriteDataRootTableEntry(Main_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.Main_Data_DataRoot.InsertOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteDataRootTableEntry(Main_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_WriteDataRootTableEntries(List<Main_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.Main_Data_DataRoot.InsertAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteDataRootTableEntries(List<Main_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Data_DataRoot Main_Data_GetDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            Main_Data_DataRoot dataRoot = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_DataRoot
                    where item.ID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        public static Main_Data_DataRoot Main_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            Main_Data_DataRoot dataRoot = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime descending
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        /// <summary>
        /// Gets every Main_Data_DataRoot table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_DataRoot> Main_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_Data_DataRoot> dataRootList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }



        public static List<Main_Data_DataRoot> Main_Data_CloneDataRootDbObjectList(List<Main_Data_DataRoot> dataRootList)
        {
            List<Main_Data_DataRoot> clonedDataRootList = null;
            try
            {
                Main_Data_DataRoot dataRootEntry;
                if (dataRootList != null)
                {
                    clonedDataRootList = new List<Main_Data_DataRoot>();
                    foreach (Main_Data_DataRoot entry in dataRootList)
                    {
                        dataRootEntry = Main_Data_CloneOneDataRootDbObject(entry);
                        if (dataRootEntry != null)
                        {
                            clonedDataRootList.Add(dataRootEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneDataRootDbObjectList(List<Main_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRootList;
        }

        public static Main_Data_DataRoot Main_Data_CloneOneDataRootDbObject(Main_Data_DataRoot dataRoot)
        {
            Main_Data_DataRoot clonedDataRoot = null;
            try
            {
                if (dataRoot != null)
                {
                    clonedDataRoot = new Main_Data_DataRoot();
                    clonedDataRoot.ID = dataRoot.ID;
                    clonedDataRoot.MonitorID = dataRoot.MonitorID;
                    clonedDataRoot.ReadingDateTime = dataRoot.ReadingDateTime;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneOneDataRootDbObject(Main_Data_DataRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRoot;
        }

        /// <summary>
        /// Gets every Main_Data_DataRoot table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_DataRoot> Main_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<Main_Data_DataRoot> dataRootList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_DataRoot
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }

        public static bool Main_Data_DeleteDataRootTableEntry(Main_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.Main_Data_DataRoot.DeleteOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntry(Main_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_DeleteDataRootTableEntries(List<Main_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.Main_Data_DataRoot.DeleteAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntries(List<Main_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_DeleteDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                Main_Data_DataRoot dataRoot = MainMonitor_DatabaseMethods.Main_Data_GetDataRootTableEntry(dataRootID, db);
                success = MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntry(dataRoot, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_DeleteDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<Main_Data_DataRoot> dataRootList = Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, db);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.Main_Data_DataRoot.DeleteAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_CopyDataRootForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Data_DataRoot dataRoot = Main_Data_GetDataRootTableEntry(dataRootID, sourceDB);
                if (dataRoot != null)
                {
                    success = Main_Data_WriteDataRootTableEntry(Main_Data_CloneOneDataRootDbObject(dataRoot), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nFailed to write the Main_Data_DataRoot to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nFailed to read the Main_Data_DataRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion // data root

        #region Main Data MonitorData

        public static bool Main_Data_WriteMonitorDataTableEntry(Main_Data_MonitorData monitorData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (monitorData != null)
                {
                    db.Main_Data_MonitorData.InsertOnSubmit(monitorData);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteMonitorDataTableEntry(Main_Data_MonitorData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_WriteMonitorDataTableEntries(List<Main_Data_MonitorData> monitorDataList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((monitorDataList != null) && (monitorDataList.Count > 0))
                {
                    db.Main_Data_MonitorData.InsertAllOnSubmit(monitorDataList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteMonitorDataTableEntries(List<Main_Data_MonitorData>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Data_MonitorData Main_Data_GetMonitorDataTableEntry(Guid monitorDataID, MonitorInterfaceDB db)
        {
            Main_Data_MonitorData monitorData = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_MonitorData
                    where item.ID == monitorDataID
                    select item;

                if (queryList.Count() > 0)
                {
                    monitorData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetMonitorDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorData;
        }

        public static Main_Data_MonitorData Main_Data_GetMonitorDataTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            Main_Data_MonitorData monitorData = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_MonitorData
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    monitorData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetMonitorDataTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorData;
        }

        public static Main_Data_MonitorData Main_Data_CloneOneMonitorDataDbObject(Main_Data_MonitorData monitorData)
        {
            Main_Data_MonitorData clonedMonitorData = null;
            try
            {
                if (monitorData != null)
                {
                    clonedMonitorData = new Main_Data_MonitorData();

                    clonedMonitorData.ID = monitorData.ID;
                    clonedMonitorData.DataRootID = monitorData.DataRootID;
                    clonedMonitorData.MonitorID = monitorData.MonitorID;

                    clonedMonitorData.ReadingDateTime = monitorData.ReadingDateTime;
                    clonedMonitorData.State = monitorData.State;
                    clonedMonitorData.ProperWorkingOrder = monitorData.ProperWorkingOrder;
                    clonedMonitorData.Reserved_0 = monitorData.Reserved_0;
                    clonedMonitorData.Reserved_1 = monitorData.Reserved_1;
                    clonedMonitorData.VibrationOneValue = monitorData.VibrationOneValue;
                    clonedMonitorData.VibrationOneStatus = monitorData.VibrationOneStatus;
                    clonedMonitorData.VibrationTwoValue = monitorData.VibrationTwoValue;
                    clonedMonitorData.VibrationTwoStatus = monitorData.VibrationTwoStatus;
                    clonedMonitorData.VibrationThreeValue = monitorData.VibrationThreeValue;
                    clonedMonitorData.VibrationThreeStatus = monitorData.VibrationThreeStatus;
                    clonedMonitorData.VibrationFourValue = monitorData.VibrationFourValue;
                    clonedMonitorData.VibrationFourStatus = monitorData.VibrationFourStatus;
                    clonedMonitorData.PressureOneValue = monitorData.PressureOneValue;
                    clonedMonitorData.PressureOneStatus = monitorData.PressureOneStatus;
                    clonedMonitorData.PressureTwoValue = monitorData.PressureTwoValue;
                    clonedMonitorData.PressureTwoStatus = monitorData.PressureTwoStatus;
                    clonedMonitorData.PressureThreeValue = monitorData.PressureThreeValue;
                    clonedMonitorData.PressureThreeStatus = monitorData.PressureThreeStatus;
                    clonedMonitorData.PressureFourValue = monitorData.PressureFourValue;
                    clonedMonitorData.PressureFourStatus = monitorData.PressureFourStatus;
                    clonedMonitorData.PressureFiveValue = monitorData.PressureFiveValue;
                    clonedMonitorData.PressureFiveStatus = monitorData.PressureFiveStatus;
                    clonedMonitorData.PressureSixValue = monitorData.PressureSixValue;
                    clonedMonitorData.PressureSixStatus = monitorData.PressureSixStatus;
                    clonedMonitorData.CurrentOneValue = monitorData.CurrentOneValue;
                    clonedMonitorData.CurrentOneStatus = monitorData.CurrentOneStatus;
                    clonedMonitorData.CurrentTwoValue = monitorData.CurrentTwoValue;
                    clonedMonitorData.CurrentTwoStatus = monitorData.CurrentTwoStatus;
                    clonedMonitorData.CurrentThreeValue = monitorData.CurrentThreeValue;
                    clonedMonitorData.CurrentThreeStatus = monitorData.CurrentThreeStatus;
                    clonedMonitorData.VoltageOneValue = monitorData.VoltageOneValue;
                    clonedMonitorData.VoltageOneStatus = monitorData.VoltageOneStatus;
                    clonedMonitorData.VoltageTwoValue = monitorData.VoltageTwoValue;
                    clonedMonitorData.VoltageTwoStatus = monitorData.VoltageTwoStatus;
                    clonedMonitorData.VoltageThreeValue = monitorData.VoltageThreeValue;
                    clonedMonitorData.VoltageThreeStatus = monitorData.VoltageThreeStatus;
                    clonedMonitorData.HumidityValue = monitorData.HumidityValue;
                    clonedMonitorData.HumidityStatus = monitorData.HumidityStatus;
                    clonedMonitorData.TemperatureOneValue = monitorData.TemperatureOneValue;
                    clonedMonitorData.TemperatureOneStatus = monitorData.TemperatureOneStatus;
                    clonedMonitorData.TemperatureTwoValue = monitorData.TemperatureTwoValue;
                    clonedMonitorData.TemperatureTwoStatus = monitorData.TemperatureTwoStatus;
                    clonedMonitorData.TemperatureThreeValue = monitorData.TemperatureThreeValue;
                    clonedMonitorData.TemperatureThreeStatus = monitorData.TemperatureThreeStatus;
                    clonedMonitorData.TemperatureFourValue = monitorData.TemperatureFourValue;
                    clonedMonitorData.TemperatureFourStatus = monitorData.TemperatureFourStatus;
                    clonedMonitorData.TemperatureFiveValue = monitorData.TemperatureFiveValue;
                    clonedMonitorData.TemperatureFiveStatus = monitorData.TemperatureFiveStatus;
                    clonedMonitorData.TemperatureSixValue = monitorData.TemperatureSixValue;
                    clonedMonitorData.TemperatureSixStatus = monitorData.TemperatureSixStatus;
                    clonedMonitorData.TemperatureSevenValue = monitorData.TemperatureSevenValue;
                    clonedMonitorData.TemperatureSevenStatus = monitorData.TemperatureSevenStatus;
                    clonedMonitorData.ChassisTemperatureValue = monitorData.ChassisTemperatureValue;
                    clonedMonitorData.ChassisTemperatureStatus = monitorData.ChassisTemperatureStatus;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneOneMonitorDataDbObject(Main_Data_MonitorData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMonitorData;
        }

        /// <summary>
        /// Gets every Main_Data_MonitorData table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_MonitorData> Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_Data_MonitorData> monitorDataList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_MonitorData
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                monitorDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorDataList;
        }

        /// <summary>
        /// Gets every Main_Data_MonitorData table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_MonitorData> Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<Main_Data_MonitorData> monitorDataList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_MonitorData
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                monitorDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorDataList;
        }

        public static List<Main_Data_MonitorData> Main_Data_CloneMonitorDataDbObjectList(List<Main_Data_MonitorData> monitorDataList)
        {
            List<Main_Data_MonitorData> clonedMonitorDataList = null;
            try
            {
                Main_Data_MonitorData monitorDataEntry;
                if (monitorDataList != null)
                {
                    clonedMonitorDataList = new List<Main_Data_MonitorData>();
                    foreach (Main_Data_MonitorData entry in monitorDataList)
                    {
                        monitorDataEntry = Main_Data_CloneOneMonitorDataDbObject(entry);
                        if (monitorDataEntry != null)
                        {
                            clonedMonitorDataList.Add(monitorDataEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneMonitorDataDbObjectList(List<Main_Data_MonitorData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMonitorDataList;
        }

        public static bool Main_Data_CopyMonitorDataForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Data_MonitorData monitorData = Main_Data_GetMonitorDataTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (monitorData != null)
                {
                    success = Main_Data_WriteMonitorDataTableEntry(Main_Data_CloneOneMonitorDataDbObject(monitorData), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyMonitorDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the Main_Data_MonitorData to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyMonitorDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the Main_Data_MonitorData from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CopyMonitorDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

   
        #endregion

        #region Main Data windingHotSpotData

        public static bool Main_Data_WriteWindingHotSpotDataTableEntry(Main_Data_WindingHotSpotData windingHotSpotData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (windingHotSpotData != null)
                {
                    db.Main_Data_WindingHotSpotData.InsertOnSubmit(windingHotSpotData);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteWindingHotSpotDataTableEntry(Main_Data_WindingHotSpotData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool Main_Data_WriteWindingHotSpotDataTableEntries(List<Main_Data_WindingHotSpotData> windingHotSpotDataList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                
                if ((windingHotSpotDataList != null) && (windingHotSpotDataList.Count > 0))
                {
                    db.Main_Data_WindingHotSpotData.InsertAllOnSubmit(windingHotSpotDataList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_WriteWindingHotSpotDataTableEntries(List<Main_Data_WindingHotSpotData>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static Main_Data_WindingHotSpotData Main_Data_GetWindingHotSpotDataTableEntry(Guid windingHotSpotDataID, MonitorInterfaceDB db)
        {
            Main_Data_WindingHotSpotData windingHotSpotData = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_WindingHotSpotData
                    where item.ID == windingHotSpotDataID
                    select item;

                if (queryList.Count() > 0)
                {
                    windingHotSpotData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetWindingHotSpotDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return windingHotSpotData;
        }

        public static Main_Data_WindingHotSpotData Main_Data_GetWindingHotSpotDataTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            Main_Data_WindingHotSpotData windingHotSpotData = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_WindingHotSpotData
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    windingHotSpotData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetWindingHotSpotDataTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return windingHotSpotData;
        }

        public static Main_Data_WindingHotSpotData Main_Data_CloneOneWindingHotSpotDataDbObject(Main_Data_WindingHotSpotData windingHotSpotData)
        {
            Main_Data_WindingHotSpotData clonedWindingHotSpotData = null;
            try
            {
                if (windingHotSpotData != null)
                {
                    clonedWindingHotSpotData = new Main_Data_WindingHotSpotData();
                   
                    clonedWindingHotSpotData.ID = windingHotSpotData.ID;
                    clonedWindingHotSpotData.DataRootID = windingHotSpotData.DataRootID;
                    clonedWindingHotSpotData.MonitorID = windingHotSpotData.MonitorID;
                    clonedWindingHotSpotData.ReadingDateTime = windingHotSpotData.ReadingDateTime;                   
                    clonedWindingHotSpotData.MaxWindingTempPhaseA = windingHotSpotData.MaxWindingTempPhaseA;
                    clonedWindingHotSpotData.MaxWindingTempPhaseB = windingHotSpotData.MaxWindingTempPhaseB;
                    clonedWindingHotSpotData.MaxWindingTempPhaseC = windingHotSpotData.MaxWindingTempPhaseC;
                    clonedWindingHotSpotData.AgingFactorA = windingHotSpotData.AgingFactorA;
                    clonedWindingHotSpotData.AgingFactorB = windingHotSpotData.AgingFactorB;
                    clonedWindingHotSpotData.AgingFactorC = windingHotSpotData.AgingFactorC;
                    clonedWindingHotSpotData.AccumulatedAgingA = windingHotSpotData.AccumulatedAgingA;
                    clonedWindingHotSpotData.AccumulatedAgingB = windingHotSpotData.AccumulatedAgingB;
                    clonedWindingHotSpotData.AccumulatedAgingC = windingHotSpotData.AccumulatedAgingC;
                    clonedWindingHotSpotData.AlarmSourceAsBitString = windingHotSpotData.AlarmSourceAsBitString;
                    clonedWindingHotSpotData.WHS_Max = windingHotSpotData.WHS_Max;
                    clonedWindingHotSpotData.Top_oilTemp = windingHotSpotData.Top_oilTemp;
                    clonedWindingHotSpotData.Fan1_num_Starts = windingHotSpotData.Fan1_num_Starts;
                    clonedWindingHotSpotData.Fan2_num_Starts = windingHotSpotData.Fan2_num_Starts;
                    clonedWindingHotSpotData.Fan1_runTime = windingHotSpotData.Fan1_runTime;
                    clonedWindingHotSpotData.Fan2_runTime = windingHotSpotData.Fan2_runTime;
                    clonedWindingHotSpotData.Errorsource_AsBitString = windingHotSpotData.Errorsource_AsBitString;
                    


                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneOneWindingHotSpotDataDbObject(Main_Data_WindingHotSpotData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedWindingHotSpotData;
        }

        /// <summary>
        /// Gets every Main_Data_WindingHotSpotData table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_WindingHotSpotData> Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<Main_Data_WindingHotSpotData> windingHotSpotDataList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_WindingHotSpotData
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                windingHotSpotDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return windingHotSpotDataList;
        }

        /// <summary>
        /// Gets every Main_Data_WindingHotSpotData table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<Main_Data_WindingHotSpotData> Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<Main_Data_WindingHotSpotData> windingHotSpotDataList = null;
            try
            {
                
                var queryList =
                    from item in db.Main_Data_WindingHotSpotData
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                windingHotSpotDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return windingHotSpotDataList;
        }

        public static List<Main_Data_WindingHotSpotData> Main_Data_CloneWindingHotSpotDataDbObjectList(List<Main_Data_WindingHotSpotData> windingHotSpotDataList)
        {
            List<Main_Data_WindingHotSpotData> clonedWindingHotSpotDataList = null;
            try
            {
                Main_Data_WindingHotSpotData windingHotSpotDataEntry;
                if (windingHotSpotDataList != null)
                {
                    clonedWindingHotSpotDataList = new List<Main_Data_WindingHotSpotData>();
                    foreach (Main_Data_WindingHotSpotData entry in windingHotSpotDataList)
                    {
                        windingHotSpotDataEntry = Main_Data_CloneOneWindingHotSpotDataDbObject(entry);
                        if (windingHotSpotDataEntry != null)
                        {
                            clonedWindingHotSpotDataList.Add(windingHotSpotDataEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CloneWindingHotSpotDataDbObjectList(List<Main_Data_WindingHotSpotData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedWindingHotSpotDataList;
        }

        public static bool Main_Data_CopyWindingHotSpotDataForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                Main_Data_WindingHotSpotData windingHotSpotData = Main_Data_GetWindingHotSpotDataTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (windingHotSpotData != null)
                {
                    success = Main_Data_WriteWindingHotSpotDataTableEntry(Main_Data_CloneOneWindingHotSpotDataDbObject(windingHotSpotData), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyWindingHotSpotDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the Main_Data_WindingHotSpotData to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyWindingHotSpotDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the Main_Data_WindingHotSpotData from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CopyWindingHotSpotDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        public static ErrorCode Main_Data_CopyAllDataForOneMonitorToNewDatabase(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                List<Main_Data_DataRoot> dataRootList;
                List<Main_Data_MonitorData> monitorDataList;
                List<Main_Data_WindingHotSpotData> windingHotSpotDataList;

                List<Main_Data_DataRoot> clonedDataRootList = null;
                List<Main_Data_MonitorData> clonedMonitorDataList = null;
                List<Main_Data_WindingHotSpotData> clonedWindingHotSpotDataList = null;

                dataRootList = Main_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, sourceDB);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    clonedDataRootList = Main_Data_CloneDataRootDbObjectList(dataRootList);
                    if ((clonedDataRootList != null) && (clonedDataRootList.Count > 0))
                    {
                        monitorDataList = Main_Data_GetAllMonitorDataTableEntriesForOneMonitor(monitorID, sourceDB);
                        if ((monitorDataList != null) && (monitorDataList.Count > 0))
                        {
                            clonedMonitorDataList = Main_Data_CloneMonitorDataDbObjectList(monitorDataList);
                            if ((clonedMonitorDataList != null) && (clonedMonitorDataList.Count > 0))
                            {
                                // At this point, we have all the required data elements to make up a data item.  We can now deal
                                // with the optional winding hot spot data.
                                windingHotSpotDataList = Main_Data_GetAllWindingHotSpotDataTableEntriesForOneMonitor(monitorID, sourceDB);
                                if ((windingHotSpotDataList != null) && (windingHotSpotDataList.Count > 0))
                                {
                                    clonedWindingHotSpotDataList = Main_Data_CloneWindingHotSpotDataDbObjectList(windingHotSpotDataList);
                                }

                                errorCode = ErrorCode.DatabaseWriteFailed;
                                destinationDB.Main_Data_DataRoot.InsertAllOnSubmit(clonedDataRootList);
                                destinationDB.SubmitChanges();

                                destinationDB.Main_Data_MonitorData.InsertAllOnSubmit(clonedMonitorDataList);

                                // The winding hot spot data may not be present, and if it's not present that is not an error.
                                if ((clonedWindingHotSpotDataList != null) && (clonedWindingHotSpotDataList.Count > 0))
                                {
                                    destinationDB.Main_Data_WindingHotSpotData.InsertAllOnSubmit(clonedWindingHotSpotDataList);
                                }

                                destinationDB.SubmitChanges();
                                errorCode = ErrorCode.DatabaseWriteSucceeded;
                            }
                            else
                            {
                                errorCode = ErrorCode.NoMonitorDataEntriesFound;
                                string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyAllDataForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone MonitorData table entries";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.NoMonitorDataEntriesFound;
                            string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyAllDataForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nNo MonitorData table enties found";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        errorCode = ErrorCode.NoDataRootEntriesFound;
                        string errorMessage = "Error in MainMonitor_DatabaseMethods.Main_Data_CopyAllDataForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone data root entries";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    errorCode = ErrorCode.NoDataRootEntriesFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.Main_Data_CopyAllDataForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static Guid GetGuidForTemplateConfigurationMonitorID()
        {
            Guid fakeMonitorID = new Guid("10100000000000000000000000000000");
            return fakeMonitorID;
        }

        public static Monitor CreateNewTemplateMonitor()
        {
            Monitor mainMonitor = null;
            try
            {
                mainMonitor = new Monitor();
                mainMonitor.ID = MainMonitor_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                mainMonitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID(); 
                mainMonitor.MonitorType = "Main";
                mainMonitor.ConnectionType = String.Empty;
                mainMonitor.BaudRate = string.Empty;
                mainMonitor.ModbusAddress = "1";
                mainMonitor.Enabled = false;
                mainMonitor.IPaddress = "000.000.000.000";
                mainMonitor.PortNumber = 502;
                mainMonitor.MonitorNumber = 0;
                mainMonitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in MainMonitor_DatabaseMethods.CreateNewTemplateMonitor()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return mainMonitor;
        }
    }
}
