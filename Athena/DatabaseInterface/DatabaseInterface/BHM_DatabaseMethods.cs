﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DatabaseInterface
{
    public class BHM_DatabaseMethods
    {
        #region Alarm Status

        /// <summary>
        /// Write one BHM_AlarmStatus entry to the database
        /// </summary>
        /// <param name="AlarmStatus"></param>
        /// <param name="db"></param>
        public static void BHM_AlarmStatus_Write(BHM_AlarmStatus AlarmStatus, MonitorInterfaceDB db)
        {
            try
            {
                if (AlarmStatus != null)
                {
                    db.BHM_AlarmStatus.InsertOnSubmit(AlarmStatus);
                    db.SubmitChanges();
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_AlarmStatus_Write(BHM_AlarmStatus, MonitorInterfaceDB)\nNull BHM_AlarmStatus passed as an argument";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_AlarmStatus_Write(BHM_AlarmStatus, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Writes a list of BHM_AlarmStatus entries to the database
        /// </summary>
        /// <param name="latestData"></param>
        /// <param name="db"></param>
        public static void BHM_AlarmStatus_Write(List<BHM_AlarmStatus> latestData, MonitorInterfaceDB db)
        {
            try
            {
                if ((latestData != null) && (latestData.Count > 0))
                {
                    db.BHM_AlarmStatus.InsertAllOnSubmit(latestData);
                    db.SubmitChanges();
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_AlarmStatus_Write(List<BHM_AlarmStatus>, MonitorInterfaceDB)\nNull or empty list passed as an arguement";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_AlarmStatus_Write(List<BHM_AlarmStatus>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        /// <summary>
        /// Reads all BHM_AlarmStatus entries for a given monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_AlarmStatus> BHM_AlarmStatus_GetAllEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_AlarmStatus> AlarmStatus = null;

            try
            {
                var alarmStatusQuery =
                    from entry in db.BHM_AlarmStatus
                    where entry.MonitorID == monitorID
                    orderby entry.DeviceTime ascending
                    select entry;

                AlarmStatus = alarmStatusQuery.ToList();

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_AlarmStatus_GetAllEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return AlarmStatus;
        }

        /// <summary>
        /// Returns the BHM_AlarmStatus for a given monitor with the most recent DateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_AlarmStatus BHM_AlarmStatus_GetLatestEntry(Guid monitorID, MonitorInterfaceDB db)
        {
            BHM_AlarmStatus AlarmStatus = null;
            try
            {
                var alarmStatusQuery =
                   from entry in db.BHM_AlarmStatus
                   where entry.MonitorID == monitorID
                   orderby entry.DeviceTime descending
                   select entry;

                if (alarmStatusQuery.Count() > 0)
                {
                    AlarmStatus = alarmStatusQuery.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_AlarmStatus_GetLatestEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return AlarmStatus;
        }

        /// <summary>
        /// Deletes all BHM_AlarmStatus entries for a given monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        public static void BHM_AlarmStatus_DeleteAllForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            try
            {
                List<BHM_AlarmStatus> AlarmStatus = BHM_AlarmStatus_GetAllEntriesForOneMonitor(monitorID, db);

                db.BHM_AlarmStatus.DeleteAllOnSubmit(AlarmStatus);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_AlarmStatus_DeleteAllForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion  // alarm status

        #region BHM Configuration

        #region ConfigurationRoot

        public static bool BHM_Config_WriteConfigurationRootTableEntry(BHM_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_ConfigurationRoot.InsertOnSubmit(configurationRoot);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteAllIDsForOneConfiguration(BHM_Config_AllIDsForOneConfiguration, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CreateOneConfiguration(Monitor monitor, DateTime dateAdded, BHM_Config_CalibrationCoeff calibrationCoeff, BHM_Config_GammaSetupInfo gammaSetupInfo,
                                                             BHM_Config_GammaSideSetup gammaSideSetupSideOne, BHM_Config_GammaSideSetup gammaSideSetupSideTwo,
                                                             BHM_Config_InitialParameters initialParameters, BHM_Config_InitialZkParameters initialZkParameters,
                                                             List<BHM_Config_MeasurementsInfo> measurementsInfoList, BHM_Config_SetupInfo setupInfo,
                                                             BHM_Config_SideToSideData sideToSideData, BHM_Config_TrSideParam trSideParamSideOne,
                                                             BHM_Config_TrSideParam trSideParamSideTwo, BHM_Config_ZkSetupInfo zkSetupInfo,
                                                             BHM_Config_ZkSideToSideSetup zkSideToSideSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                //// BHM_Config_AllIDsForOneConfiguration allIDsForOneConfiguration = new BHM_Config_AllIDsForOneConfiguration();
                // StringBuilder measurementsInfoIDsString = new StringBuilder();

                // allIDsForOneConfiguration.MonitorID = monitor.ID;
                // allIDsForOneConfiguration.DateAdded = dateAdded;
                // allIDsForOneConfiguration.CalibrationCoeffID = calibrationCoeff.ID;
                // allIDsForOneConfiguration.GammaSetupInfoID = gammaSetupInfo.ID;
                // allIDsForOneConfiguration.GammaSideSetupSideOneID = gammaSideSetupSideOne.ID;
                // allIDsForOneConfiguration.GammaSideSetupSideTwoID = gammaSideSetupSideTwo.ID;
                // allIDsForOneConfiguration.InitialParametersID = initialParameters.ID;
                // allIDsForOneConfiguration.InitialZkParametersID = initialZkParameters.ID;

                // foreach (BHM_Config_MeasurementsInfo entry in measurementsInfoList)
                // {
                //     measurementsInfoIDsString.Append(entry.ToString());
                //     measurementsInfoIDsString.Append(";");
                // }
                // allIDsForOneConfiguration.MeasurementsInfoIDs = measurementsInfoIDsString.ToString();

                // allIDsForOneConfiguration.SetupInfoID = setupInfo.ID;
                // allIDsForOneConfiguration.SideToSideDataID = sideToSideData.ID;
                // allIDsForOneConfiguration.TrSideParamSideOneID = trSideParamSideOne.ID;
                // allIDsForOneConfiguration.TrSideParamSideTwoID = trSideParamSideTwo.ID;
                // allIDsForOneConfiguration.ZkSetupInfoID = zkSetupInfo.ID;
                // allIDsForOneConfiguration.ZkSideToSideSetupID = zkSideToSideSetup.ID;

                // success = BHM_DatabaseMethods.BHM_Config_WriteAllIDsForOneConfiguration(allIDsForOneConfiguration, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CreateOneConfiguration(BHM_Config_AllIDsForOneConfiguration, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static List<BHM_Config_ConfigurationRoot> BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB db)
        {
            List<BHM_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Config_ConfigurationRoot
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesInTheDatabase(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static List<BHM_Config_ConfigurationRoot> BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_Config_ConfigurationRoot> configurationRootList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Config_ConfigurationRoot
                    where item.MonitorID == monitorID
                    orderby item.DateAdded descending
                    select item;

                configurationRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRootList;
        }

        public static BHM_Config_ConfigurationRoot BHM_Config_GetConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_ConfigurationRoot configurationRoot = null;
            try
            {
                var queryList =
                    from item in db.BHM_Config_ConfigurationRoot
                    where item.ID == configurationRootID
                    orderby item.DateAdded descending
                    select item;

                if (queryList.Count() > 0)
                {
                    configurationRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationRoot;
        }

        public static BHM_Config_ConfigurationRoot BHM_Config_CloneOneConfigurationRootDbObject(BHM_Config_ConfigurationRoot configurationRoot)
        {
            BHM_Config_ConfigurationRoot clonedConfigurationRoot = null;
            try
            {
                if (configurationRoot != null)
                {
                    clonedConfigurationRoot = new BHM_Config_ConfigurationRoot();

                    clonedConfigurationRoot.ID = configurationRoot.ID;
                    clonedConfigurationRoot.MonitorID = configurationRoot.MonitorID;
                    clonedConfigurationRoot.DateAdded = configurationRoot.DateAdded;
                    clonedConfigurationRoot.Description = configurationRoot.Description;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneConfigurationRootDbObject(BHM_Config_ConfigurationRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedConfigurationRoot;
        }


        public static bool BHM_Config_DeleteConfigurationRootTableEntry(Guid configurationRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_ConfigurationRoot configurationRoot = BHM_Config_GetConfigurationRootTableEntry(configurationRootID, db);
                success = BHM_Config_DeleteConfigurationRootTableEntry(configurationRoot, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_DeleteConfigurationRootTableEntry(BHM_Config_ConfigurationRoot configurationRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (configurationRoot != null)
                {
                    db.BHM_Config_ConfigurationRoot.DeleteOnSubmit(configurationRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntry(BHM_Config_ConfigurationRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_DeleteConfigurationRootTableEntries(List<BHM_Config_ConfigurationRoot> configurationRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((configurationRootList != null) && (configurationRootList.Count > 0))
                {
                    db.BHM_Config_ConfigurationRoot.DeleteAllOnSubmit(configurationRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteConfigurationRootTableEntries(List<BHM_Config_ConfigurationRoot> , MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Monitor monitor, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<BHM_Config_ConfigurationRoot> configurationsList;
                if (monitor != null)
                {
                    if (monitor.MonitorType.Trim().CompareTo("BHM") == 0)
                    {
                        configurationsList = BHM_Config_GetAllConfigurationRootTableEntriesForOneMonitor(monitor.ID, db);
                        success = BHM_Config_DeleteConfigurationRootTableEntries(configurationsList, db);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Monitor , MonitorInterfaceDB)\nInput Monitor was not a BHM.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Monitor , MonitorInterfaceDB)\nInput Monitor was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllConfigurationRootTableEntriesForOneMonitor(Monitor , MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_ConfigurationRoot destinationConfigurationRoot;
                BHM_Config_ConfigurationRoot sourceConfigurationRoot = BHM_Config_GetConfigurationRootTableEntry(sourceConfigurationRootID, sourceDB);
                if (sourceConfigurationRoot != null)
                {
                    destinationConfigurationRoot = BHM_Config_CloneOneConfigurationRootDbObject(sourceConfigurationRoot);
                    destinationConfigurationRoot.ID = destinationConfigurationRootID;
                    destinationConfigurationRoot.MonitorID = destinationMonitorID;
                    success = BHM_Config_WriteConfigurationRootTableEntry(destinationConfigurationRoot, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_ConfigurationRoot to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_ConfigurationRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_ConfigurationRoot configurationRoot = BHM_Config_GetConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the BHM_Config_ConfigurationRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region BaseLineZk

        /// <summary>
        /// Writes a BHM_Config_BaseLineZk object to the database
        /// </summary>
        /// <param name="baseLineZk"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteBaseLineZkTableEntry(BHM_Config_BaseLineZk baseLineZk, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_BaseLineZk.InsertOnSubmit(baseLineZk);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteBaseLineZkTableEntry(BHM_Config_BaseLineZk, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_BaseLineZk object in the database based on the date it was added
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_BaseLineZk BHM_Config_GetBaseLineZkTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_BaseLineZk baseLineZk = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_BaseLineZk
                    where entry.ConfigurationRootID == configurationRootID
                    select entry;

                if (queryList.Count() > 0)
                {
                    baseLineZk = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestGaseLineZkTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return baseLineZk;
        }

        /// as of 7-1-11, not doing anything with baselineZk, apparently.  the table is in the db, but that's it.

        //        public static BHM_Config_BaseLineZk BHM_Config_CloneBaseLineZkTableEntryForOneConfiguration(BHM_Config_BaseLineZk baseLineZk)
        //        {
        //            BHM_Config_BaseLineZk clonedBaseLineZk = null;
        //            try
        //            {
        //                if (baseLineZk != null)
        //                {
        //                    clonedBaseLineZk = new BHM_Config_BaseLineZk();

        //                    clonedBaseLineZk.ID = baseLineZk.ID;
        //                    clonedBaseLineZk.ConfigurationRootID = baseLineZk.ConfigurationRootID;
        //                    clonedBaseLineZk.DateAdded = baseLineZk.DateAdded;


        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestGaseLineZkTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return clonedBaseLineZk;
        //        }


        /// <summary>
        /// Deletes all entries in the BHM_Config_BaseLineZk Table, probably only useful for testing
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllBaseLineZkTableEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_BaseLineZk
                    select entry;

                db.BHM_Config_BaseLineZk.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllBaseLineZkTableEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region CalibrationCoeff

        /// <summary>
        /// Writes one BHM_Config_CalibrationCoeff object to the database
        /// </summary>
        /// <param name="calibrationCoeff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteCalibrationCoeffTableEntry(BHM_Config_CalibrationCoeff calibrationCoeff, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_CalibrationCoeff.InsertOnSubmit(calibrationCoeff);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteCalibrationCoeffTableEntry(BHM_Config_CalibrationCoeff, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_CalibrationCoeff BHM_Config_GetCalibrationCoeffTableEntry(Guid calibrationCoeffID, MonitorInterfaceDB db)
        {
            BHM_Config_CalibrationCoeff calibrationCoeff = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_CalibrationCoeff
                    where entry.ID == calibrationCoeffID
                    select entry;

                if (queryList.Count() > 0)
                {
                    calibrationCoeff = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetCalibrationCoeffEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationCoeff;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_CalibrationCoeff object in the database, based on the date it was added
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_CalibrationCoeff BHM_Config_GetCalibrationCoeffTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_CalibrationCoeff calibrationCoeff = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_CalibrationCoeff
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    calibrationCoeff = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestCalibrationCoeffTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationCoeff;
        }

        public static BHM_Config_CalibrationCoeff BHM_Config_CloneOneCalibrationCoeffDbObject(BHM_Config_CalibrationCoeff calibrationCoeff)
        {
            BHM_Config_CalibrationCoeff clonedCalibrationCoeff = null;
            try
            {
                if (calibrationCoeff != null)
                {
                    clonedCalibrationCoeff = new BHM_Config_CalibrationCoeff();

                    clonedCalibrationCoeff.ID = calibrationCoeff.ID;
                    clonedCalibrationCoeff.ConfigurationRootID = calibrationCoeff.ConfigurationRootID;
                    clonedCalibrationCoeff.DateAdded = calibrationCoeff.DateAdded;

                    clonedCalibrationCoeff.TemperatureK_0 = calibrationCoeff.TemperatureK_0;
                    clonedCalibrationCoeff.TemperatureK_1 = calibrationCoeff.TemperatureK_1;
                    clonedCalibrationCoeff.TemperatureK_2 = calibrationCoeff.TemperatureK_2;
                    clonedCalibrationCoeff.TemperatureK_3 = calibrationCoeff.TemperatureK_3;
                    clonedCalibrationCoeff.TemperatureB_0 = calibrationCoeff.TemperatureB_0;
                    clonedCalibrationCoeff.TemperatureB_1 = calibrationCoeff.TemperatureB_1;
                    clonedCalibrationCoeff.TemperatureB_2 = calibrationCoeff.TemperatureB_2;
                    clonedCalibrationCoeff.TemperatureB_3 = calibrationCoeff.TemperatureB_3;
                    clonedCalibrationCoeff.CurrentK_0 = calibrationCoeff.CurrentK_0;
                    clonedCalibrationCoeff.CurrentK_1 = calibrationCoeff.CurrentK_1;
                    clonedCalibrationCoeff.CurrentK_2 = calibrationCoeff.CurrentK_2;
                    clonedCalibrationCoeff.CurrentK_3 = calibrationCoeff.CurrentK_3;
                    clonedCalibrationCoeff.HumidityOffset_0 = calibrationCoeff.HumidityOffset_0;
                    clonedCalibrationCoeff.HumidityOffset_1 = calibrationCoeff.HumidityOffset_1;
                    clonedCalibrationCoeff.HumidityOffset_2 = calibrationCoeff.HumidityOffset_2;
                    clonedCalibrationCoeff.HumidityOffset_3 = calibrationCoeff.HumidityOffset_3;
                    clonedCalibrationCoeff.HumiditySlope_0 = calibrationCoeff.HumiditySlope_0;
                    clonedCalibrationCoeff.HumiditySlope_1 = calibrationCoeff.HumiditySlope_1;
                    clonedCalibrationCoeff.HumiditySlope_2 = calibrationCoeff.HumiditySlope_2;
                    clonedCalibrationCoeff.HumiditySlope_3 = calibrationCoeff.HumiditySlope_3;
                    clonedCalibrationCoeff.VoltageK_0_0 = calibrationCoeff.VoltageK_0_0;
                    clonedCalibrationCoeff.VoltageK_0_1 = calibrationCoeff.VoltageK_0_1;
                    clonedCalibrationCoeff.VoltageK_0_2 = calibrationCoeff.VoltageK_0_2;
                    clonedCalibrationCoeff.VoltageK_1_0 = calibrationCoeff.VoltageK_1_0;
                    clonedCalibrationCoeff.VoltageK_1_1 = calibrationCoeff.VoltageK_1_1;
                    clonedCalibrationCoeff.VoltageK_1_2 = calibrationCoeff.VoltageK_1_2;
                    clonedCalibrationCoeff.CurrentB_0 = calibrationCoeff.CurrentB_0;
                    clonedCalibrationCoeff.CurrentB_1 = calibrationCoeff.CurrentB_1;
                    clonedCalibrationCoeff.CurrentB_2 = calibrationCoeff.CurrentB_2;
                    clonedCalibrationCoeff.CurrentB_3 = calibrationCoeff.CurrentB_3;
                    clonedCalibrationCoeff.I4_20B = calibrationCoeff.I4_20B;
                    clonedCalibrationCoeff.I4_20K = calibrationCoeff.I4_20K;
                    clonedCalibrationCoeff.CurrentChannelOnPhase_0 = calibrationCoeff.CurrentChannelOnPhase_0;
                    clonedCalibrationCoeff.CurrentChannelOnPhase_1 = calibrationCoeff.CurrentChannelOnPhase_1;
                    clonedCalibrationCoeff.CurrentChannelOnPhase_2 = calibrationCoeff.CurrentChannelOnPhase_2;
                    clonedCalibrationCoeff.CurrentChannelOnPhase_3 = calibrationCoeff.CurrentChannelOnPhase_3;

                    clonedCalibrationCoeff.Reserved_0 = calibrationCoeff.Reserved_0;
                    clonedCalibrationCoeff.Reserved_1 = calibrationCoeff.Reserved_1;
                    clonedCalibrationCoeff.Reserved_2 = calibrationCoeff.Reserved_2;
                    clonedCalibrationCoeff.Reserved_3 = calibrationCoeff.Reserved_3;
                    clonedCalibrationCoeff.Reserved_4 = calibrationCoeff.Reserved_4;
                    clonedCalibrationCoeff.Reserved_5 = calibrationCoeff.Reserved_5;
                    clonedCalibrationCoeff.Reserved_6 = calibrationCoeff.Reserved_6;
                    clonedCalibrationCoeff.Reserved_7 = calibrationCoeff.Reserved_7;
                    clonedCalibrationCoeff.Reserved_8 = calibrationCoeff.Reserved_8;
                    clonedCalibrationCoeff.Reserved_9 = calibrationCoeff.Reserved_9;
                    clonedCalibrationCoeff.Reserved_10 = calibrationCoeff.Reserved_10;
                    clonedCalibrationCoeff.Reserved_11 = calibrationCoeff.Reserved_11;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneCalibrationCoeffDbObject(BHM_Config_CalibrationCoeff)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedCalibrationCoeff;
        }


        //        /// <summary>
        //        /// Gets a list of all BHM_Config_CalibrationCoeff objects associated with one Monitor object
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_CalibrationCoeff> BHM_Config_GetAllCalibrationCoeffEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_CalibrationCoeff> calibrationCoeffList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_CalibrationCoeff
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                calibrationCoeffList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestCalibrationCoeffTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return calibrationCoeffList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_CalibrationCoeff objects associated with one Monitor object
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllCalibrationCoeffEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false; 
        //            try
        //            {
        //               List<BHM_Config_CalibrationCoeff> calibrationCoeffList = BHM_Config_GetAllCalibrationCoeffEntries(monitorID, db);
        //                if(calibrationCoeffList.Count > 0)
        //                {
        //                    db.BHM_Config_CalibrationCoeff.DeleteAllOnSubmit(calibrationCoeffList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteCalibrationCoeffTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_CalibrationCoeff object from the database
        /// </summary>
        /// <param name="calibrationCoeff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteCalibrationCoeffTableEntry(BHM_Config_CalibrationCoeff calibrationCoeff, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (calibrationCoeff != null)
                {
                    db.BHM_Config_CalibrationCoeff.DeleteOnSubmit(calibrationCoeff);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteCalibrationCoeffTableEntry(BHM_Config_CalibrationCoeff, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_CalibrationCoeff object from the database
        /// </summary>
        /// <param name="calibrationCoeff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteCalibrationCoeffTableEntry(Guid calibrationCoeffID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_CalibrationCoeff calibrationCoeff = BHM_DatabaseMethods.BHM_Config_GetCalibrationCoeffTableEntry(calibrationCoeffID, db);
                success = BHM_Config_DeleteCalibrationCoeffTableEntry(calibrationCoeff, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteCalibrationCoeffTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all entries in the BHM_Config_CalibrationCoeff Table, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllCalibrationCoeffEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_CalibrationCoeff
                    select entry;

                db.BHM_Config_CalibrationCoeff.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllCalibrationCoeffEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_CalibrationCoeff destinationCalibrationCoeff;
                BHM_Config_CalibrationCoeff sourceCalibrationCoeff = BHM_Config_GetCalibrationCoeffTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceCalibrationCoeff != null)
                {
                    destinationCalibrationCoeff = BHM_Config_CloneOneCalibrationCoeffDbObject(sourceCalibrationCoeff);
                    destinationCalibrationCoeff.ID = Guid.NewGuid();
                    destinationCalibrationCoeff.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteCalibrationCoeffTableEntry(destinationCalibrationCoeff, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_CalibrationCoeff to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_CalibrationCoeff from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region GammaSetupInfo

        /// <summary>
        /// Writes one BHM_Config_GammaSetupInfo object to the database
        /// </summary>
        /// <param name="gammaSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteGammaSetupInfoTableEntry(BHM_Config_GammaSetupInfo gammaSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_GammaSetupInfo.InsertOnSubmit(gammaSetupInfo);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteGammaSetupInfoTableEntry(BHM_Config_GammaSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_GammaSetupInfo object in the database, based on the date it was added
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_GammaSetupInfo BHM_Config_GetGammaSetupInfoTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_GammaSetupInfo gammaSetupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSetupInfo
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    gammaSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetGammaSetupInfoTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSetupInfo;
        }

        /// <summary>
        /// Gets a specific GammaSideSetup entry
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_GammaSetupInfo BHM_Config_GetGammaSetupInfoTableEntry(Guid gammaSetupInfoID, MonitorInterfaceDB db)
        {
            BHM_Config_GammaSetupInfo gammaSetupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSetupInfo
                    where entry.ID == gammaSetupInfoID
                    select entry;

                if (queryList.Count() > 0)
                {
                    gammaSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllGammaSetupInfoEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSetupInfo;
        }

        public static BHM_Config_GammaSetupInfo BHM_Config_CloneOneGammaSetupInfoDbObject(BHM_Config_GammaSetupInfo gammaSetupInfo)
        {
            BHM_Config_GammaSetupInfo clonedGammaSetupInfo = null;
            try
            {
                if (gammaSetupInfo != null)
                {
                    clonedGammaSetupInfo = new BHM_Config_GammaSetupInfo();

                    clonedGammaSetupInfo.ID = gammaSetupInfo.ID;
                    clonedGammaSetupInfo.ConfigurationRootID = gammaSetupInfo.ConfigurationRootID;
                    clonedGammaSetupInfo.DateAdded = gammaSetupInfo.DateAdded;

                    clonedGammaSetupInfo.ReadOnSide = gammaSetupInfo.ReadOnSide;
                    clonedGammaSetupInfo.MaxParameterChange = gammaSetupInfo.MaxParameterChange;
                    clonedGammaSetupInfo.AlarmHysteresis = gammaSetupInfo.AlarmHysteresis;
                    clonedGammaSetupInfo.AllowedPhaseDispersion = gammaSetupInfo.AllowedPhaseDispersion;
                    clonedGammaSetupInfo.DaysToCalculateTrend = gammaSetupInfo.DaysToCalculateTrend;
                    clonedGammaSetupInfo.DaysToCalculateTCoefficient = gammaSetupInfo.DaysToCalculateTCoefficient;
                    clonedGammaSetupInfo.AveragingForGamma = gammaSetupInfo.AveragingForGamma;
                    clonedGammaSetupInfo.ReReadOnAlarm = gammaSetupInfo.ReReadOnAlarm;
                    clonedGammaSetupInfo.MinDiagGamma = gammaSetupInfo.MinDiagGamma;
                    clonedGammaSetupInfo.NEGtg = gammaSetupInfo.NEGtg;
                    clonedGammaSetupInfo.DaysToCalculateBASELINE = gammaSetupInfo.DaysToCalculateBASELINE;
                    clonedGammaSetupInfo.LoadChannel = gammaSetupInfo.LoadChannel;
                    clonedGammaSetupInfo.ReduceCoeff = gammaSetupInfo.ReduceCoeff;

                    clonedGammaSetupInfo.Reserved_0 = gammaSetupInfo.Reserved_0;
                    clonedGammaSetupInfo.Reserved_1 = gammaSetupInfo.Reserved_1;
                    clonedGammaSetupInfo.Reserved_2 = gammaSetupInfo.Reserved_2;
                    clonedGammaSetupInfo.Reserved_3 = gammaSetupInfo.Reserved_3;
                    clonedGammaSetupInfo.Reserved_4 = gammaSetupInfo.Reserved_4;
                    clonedGammaSetupInfo.Reserved_5 = gammaSetupInfo.Reserved_5;
                    clonedGammaSetupInfo.Reserved_6 = gammaSetupInfo.Reserved_6;
                    clonedGammaSetupInfo.Reserved_7 = gammaSetupInfo.Reserved_7;
                    clonedGammaSetupInfo.Reserved_8 = gammaSetupInfo.Reserved_8;
                    clonedGammaSetupInfo.Reserved_9 = gammaSetupInfo.Reserved_9;
                    clonedGammaSetupInfo.Reserved_10 = gammaSetupInfo.Reserved_10;
                    clonedGammaSetupInfo.Reserved_11 = gammaSetupInfo.Reserved_11;
                    clonedGammaSetupInfo.Reserved_12 = gammaSetupInfo.Reserved_12;
                    clonedGammaSetupInfo.Reserved_13 = gammaSetupInfo.Reserved_13;
                    clonedGammaSetupInfo.Reserved_14 = gammaSetupInfo.Reserved_14;
                    clonedGammaSetupInfo.Reserved_15 = gammaSetupInfo.Reserved_15;
                    clonedGammaSetupInfo.Reserved_16 = gammaSetupInfo.Reserved_16;
                    clonedGammaSetupInfo.Reserved_17 = gammaSetupInfo.Reserved_17;
                    clonedGammaSetupInfo.Reserved_18 = gammaSetupInfo.Reserved_18;
                    clonedGammaSetupInfo.Reserved_19 = gammaSetupInfo.Reserved_19;
                    clonedGammaSetupInfo.Reserved_20 = gammaSetupInfo.Reserved_20;
                    clonedGammaSetupInfo.Reserved_21 = gammaSetupInfo.Reserved_21;
                    clonedGammaSetupInfo.Reserved_22 = gammaSetupInfo.Reserved_22;
                    clonedGammaSetupInfo.Reserved_23 = gammaSetupInfo.Reserved_23;
                    clonedGammaSetupInfo.Reserved_24 = gammaSetupInfo.Reserved_24;
                    clonedGammaSetupInfo.Reserved_25 = gammaSetupInfo.Reserved_25;
                    clonedGammaSetupInfo.Reserved_26 = gammaSetupInfo.Reserved_26;
                    clonedGammaSetupInfo.Reserved_27 = gammaSetupInfo.Reserved_27;
                    clonedGammaSetupInfo.Reserved_28 = gammaSetupInfo.Reserved_28;
                    clonedGammaSetupInfo.Reserved_29 = gammaSetupInfo.Reserved_29;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneGammaSetupInfoDbObject(BHM_Config_GammaSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedGammaSetupInfo;
        }


        //        /// <summary>
        //        /// Gets a list of all BHM_Config_GammaSetupInfo objects associated with one Monitor object
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_GammaSetupInfo> BHM_Config_GetAllGammaSetupInfoEntries(Guid configurationRootID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_GammaSetupInfo> gammaSetupInfoList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_GammaSetupInfo
        //                    where entry.MonitorID == configurationRootID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                gammaSetupInfoList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllGammaSetupInfoEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return gammaSetupInfoList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_GammaSetupInfo objects associated with on Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllGamaSetupInfoEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_GammaSetupInfo> gammaSetupInfoList = BHM_Config_GetAllGammaSetupInfoEntries(monitorID, db);
        //                if (gammaSetupInfoList.Count > 0)
        //                {
        //                    db.BHM_Config_GammaSetupInfo.DeleteAllOnSubmit(gammaSetupInfoList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllGamaSetupInfoEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_GammaSetupInfo object from the database
        /// </summary>
        /// <param name="gammaSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteGammaSetupInfoTableEntry(BHM_Config_GammaSetupInfo gammaSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (gammaSetupInfo != null)
                {
                    db.BHM_Config_GammaSetupInfo.DeleteOnSubmit(gammaSetupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteGammaSetupInfoTableEntry(BHM_Config_GammaSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_GammaSetupInfo object from the database
        /// </summary>
        /// <param name="gammaSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteGammaSetupInfoTableEntry(Guid gammaSetupInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_GammaSetupInfo gammaSetupInfo = BHM_DatabaseMethods.BHM_Config_GetGammaSetupInfoTableEntry(gammaSetupInfoID, db);
                success = BHM_Config_DeleteGammaSetupInfoTableEntry(gammaSetupInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteGammaSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        /// <summary>
        /// Deletes all entries in the BHM_Config_GammaSetupInfo Table, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllGammaSetupInfoEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSetupInfo
                    select entry;

                db.BHM_Config_GammaSetupInfo.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllGammaSetupEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_GammaSetupInfo destinationGammaSetupInfo;
                BHM_Config_GammaSetupInfo sourceGammaSetupInfo = BHM_Config_GetGammaSetupInfoTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceGammaSetupInfo != null)
                {
                    destinationGammaSetupInfo = BHM_Config_CloneOneGammaSetupInfoDbObject(sourceGammaSetupInfo);
                    destinationGammaSetupInfo.ID = Guid.NewGuid();
                    destinationGammaSetupInfo.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteGammaSetupInfoTableEntry(destinationGammaSetupInfo, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_GammaSetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_GammaSetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region GammaSideSetup

        /// <summary>
        /// Writes a list of BHM_Config_GammaSideSetup objects to the database
        /// </summary>
        /// <param name="gammaSideSetupList"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteGammaSideSetupTableEntry(List<BHM_Config_GammaSideSetup> gammaSideSetupList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_GammaSideSetup.InsertAllOnSubmit(gammaSideSetupList);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteGammaSideSetupTableEntry(List<BHM_Config_GammaSideSetup>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Writes one BHM_Config_GammaSideSetup object to the database
        /// </summary>
        /// <param name="gammaSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteGammaSideSetupTableEntry(BHM_Config_GammaSideSetup gammaSideSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_GammaSideSetup.InsertOnSubmit(gammaSideSetup);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteGammaSideSetupTableEntry(BHM_Config_GammaSideSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }




        /// <summary>
        /// Gets the most recent BHM_Config_GammaSideSetup object for a given Monitor object for one side, based on the date the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="sideNumber"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_GammaSideSetup BHM_Config_GetGammaSideSetupTableEntry(Guid gammaSideSetupID, MonitorInterfaceDB db)
        {
            BHM_Config_GammaSideSetup gammaSideSetup = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSideSetup
                    where entry.ID == gammaSideSetupID
                    select entry;

                if (queryList.Count() > 0)
                {
                    gammaSideSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupTableEntry(Guid, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSideSetup;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_GammaSideSetup object for a given Monitor object for one side, based on the date the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="sideNumber"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_GammaSideSetup BHM_Config_GetGammaSideSetupTableEntryForOneSideForOneConfiguration(Guid configurationRootID, int sideNumber, MonitorInterfaceDB db)
        {
            BHM_Config_GammaSideSetup gammaSideSetup = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSideSetup
                    where entry.ConfigurationRootID == configurationRootID
                    && entry.SideNumber == sideNumber
                    select entry;

                if (queryList.Count() > 0)
                {
                    gammaSideSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupTableEntryForOneSideForOneConfiguration(Guid, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSideSetup;
        }

        public static BHM_Config_GammaSideSetup BHM_Config_CloneOneGammaSideSetupDbObject(BHM_Config_GammaSideSetup gammaSideSetup)
        {
            BHM_Config_GammaSideSetup clonedGammaSideSetup = null;
            try
            {
                if (gammaSideSetup != null)
                {
                    clonedGammaSideSetup = new BHM_Config_GammaSideSetup();

                    clonedGammaSideSetup.ID = gammaSideSetup.ID;
                    clonedGammaSideSetup.ConfigurationRootID = gammaSideSetup.ConfigurationRootID;
                    clonedGammaSideSetup.DateAdded = gammaSideSetup.DateAdded;
                    clonedGammaSideSetup.SideNumber = gammaSideSetup.SideNumber;

                    clonedGammaSideSetup.AlarmEnable = gammaSideSetup.AlarmEnable;
                    clonedGammaSideSetup.GammaYellowThreshold = gammaSideSetup.GammaYellowThreshold;
                    clonedGammaSideSetup.GammaRedThreshold = gammaSideSetup.GammaRedThreshold;
                    clonedGammaSideSetup.TempCoefficient = gammaSideSetup.TempCoefficient;
                    clonedGammaSideSetup.TrendAlarm = gammaSideSetup.TrendAlarm;
                    clonedGammaSideSetup.Temperature0 = gammaSideSetup.Temperature0;
                    clonedGammaSideSetup.InputC_0 = gammaSideSetup.InputC_0;
                    clonedGammaSideSetup.InputC_1 = gammaSideSetup.InputC_1;
                    clonedGammaSideSetup.InputC_2 = gammaSideSetup.InputC_2;
                    clonedGammaSideSetup.InputCoeff_0 = gammaSideSetup.InputCoeff_0;
                    clonedGammaSideSetup.InputCoeff_1 = gammaSideSetup.InputCoeff_1;
                    clonedGammaSideSetup.InputCoeff_2 = gammaSideSetup.InputCoeff_2;
                    clonedGammaSideSetup.Tg0_0 = gammaSideSetup.Tg0_0;
                    clonedGammaSideSetup.Tg0_1 = gammaSideSetup.Tg0_1;
                    clonedGammaSideSetup.Tg0_2 = gammaSideSetup.Tg0_2;
                    clonedGammaSideSetup.C0_0 = gammaSideSetup.C0_0;
                    clonedGammaSideSetup.C0_1 = gammaSideSetup.C0_1;
                    clonedGammaSideSetup.C0_2 = gammaSideSetup.C0_2;
                    clonedGammaSideSetup.InputImpedance_0 = gammaSideSetup.InputImpedance_0;
                    clonedGammaSideSetup.InputImpedance_1 = gammaSideSetup.InputImpedance_1;
                    clonedGammaSideSetup.InputImpedance_2 = gammaSideSetup.InputImpedance_2;
                    clonedGammaSideSetup.MinTemperature1 = gammaSideSetup.MinTemperature1;
                    clonedGammaSideSetup.AvgTemperature1 = gammaSideSetup.AvgTemperature1;
                    clonedGammaSideSetup.MaxTemperature1 = gammaSideSetup.MaxTemperature1;
                    clonedGammaSideSetup.B_0 = gammaSideSetup.B_0;
                    clonedGammaSideSetup.B_1 = gammaSideSetup.B_1;
                    clonedGammaSideSetup.B_2 = gammaSideSetup.B_2;
                    clonedGammaSideSetup.K_0 = gammaSideSetup.K_0;
                    clonedGammaSideSetup.K_1 = gammaSideSetup.K_1;
                    clonedGammaSideSetup.K_2 = gammaSideSetup.K_2;
                    clonedGammaSideSetup.InputVoltage_0 = gammaSideSetup.InputVoltage_0;
                    clonedGammaSideSetup.InputVoltage_1 = gammaSideSetup.InputVoltage_1;
                    clonedGammaSideSetup.InputVoltage_2 = gammaSideSetup.InputVoltage_2;
                    clonedGammaSideSetup.STABLEDeltaTg_0 = gammaSideSetup.STABLEDeltaTg_0;
                    clonedGammaSideSetup.STABLEDeltaTg_1 = gammaSideSetup.STABLEDeltaTg_1;
                    clonedGammaSideSetup.STABLEDeltaTg_2 = gammaSideSetup.STABLEDeltaTg_2;
                    clonedGammaSideSetup.STABLEDeltaC_0 = gammaSideSetup.STABLEDeltaC_0;
                    clonedGammaSideSetup.STABLEDeltaC_1 = gammaSideSetup.STABLEDeltaC_1;
                    clonedGammaSideSetup.STABLEDeltaC_2 = gammaSideSetup.STABLEDeltaC_2;
                    clonedGammaSideSetup.STABLEDate = gammaSideSetup.STABLEDate;
                    clonedGammaSideSetup.HeatDate = gammaSideSetup.HeatDate;
                    clonedGammaSideSetup.STABLETemperature = gammaSideSetup.STABLETemperature;
                    clonedGammaSideSetup.STABLETg_0 = gammaSideSetup.STABLETg_0;
                    clonedGammaSideSetup.STABLETg_1 = gammaSideSetup.STABLETg_1;
                    clonedGammaSideSetup.STABLETg_2 = gammaSideSetup.STABLETg_2;
                    clonedGammaSideSetup.STABLEC_0 = gammaSideSetup.STABLEC_0;
                    clonedGammaSideSetup.STABLEC_1 = gammaSideSetup.STABLEC_1;
                    clonedGammaSideSetup.STABLEC_2 = gammaSideSetup.STABLEC_2;
                    clonedGammaSideSetup.STABLESaved = gammaSideSetup.STABLESaved;
                    clonedGammaSideSetup.RatedVoltage = gammaSideSetup.RatedVoltage;
                    clonedGammaSideSetup.RatedCurrent = gammaSideSetup.RatedCurrent;
                    clonedGammaSideSetup.StablePhaseAmplitude_0 = gammaSideSetup.StablePhaseAmplitude_0;
                    clonedGammaSideSetup.StablePhaseAmplitude_1 = gammaSideSetup.StablePhaseAmplitude_1;
                    clonedGammaSideSetup.StablePhaseAmplitude_2 = gammaSideSetup.StablePhaseAmplitude_2;
                    clonedGammaSideSetup.StableSourceAmplitude_0 = gammaSideSetup.StableSourceAmplitude_0;
                    clonedGammaSideSetup.StableSourceAmplitude_1 = gammaSideSetup.StableSourceAmplitude_1;
                    clonedGammaSideSetup.StableSourceAmplitude_2 = gammaSideSetup.StableSourceAmplitude_2;
                    clonedGammaSideSetup.StableSignalPhase_0 = gammaSideSetup.StableSignalPhase_0;
                    clonedGammaSideSetup.StableSignalPhase_1 = gammaSideSetup.StableSignalPhase_1;
                    clonedGammaSideSetup.StableSignalPhase_2 = gammaSideSetup.StableSignalPhase_2;
                    clonedGammaSideSetup.StableSourcePhase_0 = gammaSideSetup.StableSourcePhase_0;
                    clonedGammaSideSetup.StableSourcePhase_1 = gammaSideSetup.StableSourcePhase_1;
                    clonedGammaSideSetup.StableSourcePhase_2 = gammaSideSetup.StableSourcePhase_2;
                    clonedGammaSideSetup.STABLEAvgCurrent = gammaSideSetup.STABLEAvgCurrent;
                    clonedGammaSideSetup.TgYellowThreshold = gammaSideSetup.TgYellowThreshold;
                    clonedGammaSideSetup.TgRedThreshold = gammaSideSetup.TgRedThreshold;
                    clonedGammaSideSetup.TgVariationThreshold = gammaSideSetup.TgVariationThreshold;
                    clonedGammaSideSetup.ImpedanceValue_0 = gammaSideSetup.ImpedanceValue_0;
                    clonedGammaSideSetup.ImpedanceValue_1 = gammaSideSetup.ImpedanceValue_1;
                    clonedGammaSideSetup.ImpedanceValue_2 = gammaSideSetup.ImpedanceValue_2;
                    clonedGammaSideSetup.TemperatureConfig = gammaSideSetup.TemperatureConfig;
                    clonedGammaSideSetup.ExtImpedanceValue_0 = gammaSideSetup.ExtImpedanceValue_0;
                    clonedGammaSideSetup.ExtImpedanceValue_1 = gammaSideSetup.ExtImpedanceValue_1;
                    clonedGammaSideSetup.ExtImpedanceValue_2 = gammaSideSetup.ExtImpedanceValue_2;
                    clonedGammaSideSetup.ExternalSync = gammaSideSetup.ExternalSync;

                    clonedGammaSideSetup.Reserved_0 = gammaSideSetup.Reserved_0;
                    clonedGammaSideSetup.Reserved_1 = gammaSideSetup.Reserved_1;
                    clonedGammaSideSetup.Reserved_2 = gammaSideSetup.Reserved_2;
                    clonedGammaSideSetup.Reserved_3 = gammaSideSetup.Reserved_3;
                    clonedGammaSideSetup.Reserved_4 = gammaSideSetup.Reserved_4;
                    clonedGammaSideSetup.Reserved_5 = gammaSideSetup.Reserved_5;
                    clonedGammaSideSetup.Reserved_6 = gammaSideSetup.Reserved_6;
                    clonedGammaSideSetup.Reserved_7 = gammaSideSetup.Reserved_7;
                    clonedGammaSideSetup.Reserved_8 = gammaSideSetup.Reserved_8;
                    clonedGammaSideSetup.Reserved_9 = gammaSideSetup.Reserved_9;
                    clonedGammaSideSetup.Reserved_10 = gammaSideSetup.Reserved_10;
                    clonedGammaSideSetup.Reserved_11 = gammaSideSetup.Reserved_11;
                    clonedGammaSideSetup.Reserved_12 = gammaSideSetup.Reserved_12;
                    clonedGammaSideSetup.Reserved_13 = gammaSideSetup.Reserved_13;
                    clonedGammaSideSetup.Reserved_14 = gammaSideSetup.Reserved_14;
                    clonedGammaSideSetup.Reserved_15 = gammaSideSetup.Reserved_15;
                    clonedGammaSideSetup.Reserved_16 = gammaSideSetup.Reserved_16;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneGammaSideSetupDbObject(BHM_Config_GammaSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedGammaSideSetup;
        }

        public static List<BHM_Config_GammaSideSetup> BHM_Config_CloneGammaSideSetupDbObjectsList(List<BHM_Config_GammaSideSetup> gammaSideSetupList)
        {
            List<BHM_Config_GammaSideSetup> clonedGammaSideSetupList = null;
            try
            {
                BHM_Config_GammaSideSetup gammaSideSetup;
                if (gammaSideSetupList != null)
                {
                    clonedGammaSideSetupList = new List<BHM_Config_GammaSideSetup>();
                    foreach (BHM_Config_GammaSideSetup entry in gammaSideSetupList)
                    {
                        gammaSideSetup = BHM_Config_CloneOneGammaSideSetupDbObject(entry);
                        if (gammaSideSetup != null)
                        {
                            clonedGammaSideSetupList.Add(gammaSideSetup);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CloneGammaSideSetupDbObjectsList(List<BHM_Config_GammaSideSetup>)\nFailed to clone one input object.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CloneGammaSideSetupDbObjectsList(List<BHM_Config_GammaSideSetup>)\nInput List<BHM_Config_GammaSideSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneGammaSideSetupDbObjectsList(List<BHM_Config_GammaSideSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedGammaSideSetupList;
        }

        /// <summary>
        /// Gets the most recent pair of BHM_Config_GammaSideSetup objects for a given Monitor, one for each side
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Config_GammaSideSetup> BHM_Config_GetGammaSideSetupEntriesForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<BHM_Config_GammaSideSetup> gammaSideSetupList = new List<BHM_Config_GammaSideSetup>();
            try
            {
                BHM_Config_GammaSideSetup entry = BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupTableEntryForOneSideForOneConfiguration(configurationRootID, 0, db);
                if (entry != null)
                {
                    gammaSideSetupList.Add(entry);
                }
                entry = BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupTableEntryForOneSideForOneConfiguration(configurationRootID, 1, db);
                if (entry != null)
                {
                    gammaSideSetupList.Add(entry);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupEntriesForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSideSetupList;
        }

        //        /// <summary>
        //        /// Gets a list of all BHM_Config_GammaSideSetup objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_GammaSideSetup> BHM_Config_GetAllGammaSideSetupTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_GammaSideSetup> gammaSideSetupList = null;
        //            try
        //            {
        //                var queryList =
        //                   from entry in db.BHM_Config_GammaSideSetup
        //                   where entry.MonitorID == configurationRootID
        //                   orderby entry.DateAdded descending
        //                   select entry;

        //                gammaSideSetupList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllGammaSideSetupTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return gammaSideSetupList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_GammaSideSetup objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllGammaSideSetupTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_GammaSideSetup> gammaSideSetupList = BHM_Config_GetAllGammaSideSetupTableEntries(monitorID, db);
        //                db.BHM_Config_GammaSideSetup.DeleteAllOnSubmit(gammaSideSetupList);
        //                db.SubmitChanges();
        //                success = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllGammaSideSetupTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_GammaSideSetup object from the database
        /// </summary>
        /// <param name="gammaSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteGammaSideSetupTableEntry(BHM_Config_GammaSideSetup gammaSideSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (gammaSideSetup != null)
                {
                    db.BHM_Config_GammaSideSetup.DeleteOnSubmit(gammaSideSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteGammaSideSetupTableEntry(BHM_Config_GammaSideSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_GammaSideSetup object from the database
        /// </summary>
        /// <param name="gammaSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteGammaSideSetupTableEntry(Guid gammaSideSetupID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_GammaSideSetup gammaSideSetup = BHM_DatabaseMethods.BHM_Config_GetGammaSideSetupTableEntry(gammaSideSetupID, db);
                success = BHM_Config_DeleteGammaSideSetupTableEntry(gammaSideSetup, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteGammaSideSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all entries in the BHM_Config_GammaSideSetup database table, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllGammaSideSetupEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_GammaSideSetup
                    select entry;

                db.BHM_Config_GammaSideSetup.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllGammaSideSetupEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<BHM_Config_GammaSideSetup> destinationGammaSideSetupList;
                List<BHM_Config_GammaSideSetup> sourceGammaSideSetupList = BHM_Config_GetGammaSideSetupEntriesForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceGammaSideSetupList != null)
                {
                    destinationGammaSideSetupList = BHM_Config_CloneGammaSideSetupDbObjectsList(sourceGammaSideSetupList);
                    for (int i = 0; i < destinationGammaSideSetupList.Count; i++)
                    {
                        destinationGammaSideSetupList[i].ID = Guid.NewGuid();
                        destinationGammaSideSetupList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = BHM_Config_WriteGammaSideSetupTableEntry(destinationGammaSideSetupList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<BHM_Config_GammaSideSetup> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<BHM_Config_GammaSideSetup> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region InitialParameters

        /// <summary>
        /// Writes one BHM_Config_InitialParameters object to the database
        /// </summary>
        /// <param name="initialParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteInitialParametersTableEntry(BHM_Config_InitialParameters initialParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_InitialParameters.InsertOnSubmit(initialParameters);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteInitialParametersTableEntry(BHM_Config_InitialParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_InitialParameters object in the database for a given Monitor, based on the date it was added
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_InitialParameters BHM_Config_GetInitialParametersTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_InitialParameters initialParameters = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialParameters
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    initialParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetInitialParametersTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialParameters;
        }


        /// <summary>
        /// Gets the BHM_Config_InitialParameters object associated with the initialParametersID
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_InitialParameters BHM_Config_GetInitialParametersTableEntry(Guid initialParametersID, MonitorInterfaceDB db)
        {
            BHM_Config_InitialParameters initialParameters = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialParameters
                    where entry.ID == initialParametersID
                    select entry;

                if (queryList.Count() > 0)
                {
                    initialParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetInitialParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialParameters;
        }

        public static BHM_Config_InitialParameters BHM_Config_CloneOneInitialParametersDbObject(BHM_Config_InitialParameters initialParameters)
        {
            BHM_Config_InitialParameters clonedInitialParameters = null;
            try
            {
                if (initialParameters != null)
                {
                    clonedInitialParameters = new BHM_Config_InitialParameters();

                    clonedInitialParameters.ID = initialParameters.ID;
                    clonedInitialParameters.ConfigurationRootID = initialParameters.ConfigurationRootID;
                    clonedInitialParameters.DateAdded = initialParameters.DateAdded;

                    clonedInitialParameters.Day = initialParameters.Day;
                    clonedInitialParameters.Month = initialParameters.Month;
                    clonedInitialParameters.Year = initialParameters.Year;
                    clonedInitialParameters.Hour = initialParameters.Hour;
                    clonedInitialParameters.Min = initialParameters.Min;

                    clonedInitialParameters.Reserved_0 = initialParameters.Reserved_0;
                    clonedInitialParameters.Reserved_1 = initialParameters.Reserved_1;
                    clonedInitialParameters.Reserved_2 = initialParameters.Reserved_2;
                    clonedInitialParameters.Reserved_3 = initialParameters.Reserved_3;
                    clonedInitialParameters.Reserved_4 = initialParameters.Reserved_4;
                    clonedInitialParameters.Reserved_5 = initialParameters.Reserved_5;
                    clonedInitialParameters.Reserved_6 = initialParameters.Reserved_6;
                    clonedInitialParameters.Reserved_7 = initialParameters.Reserved_7;
                    clonedInitialParameters.Reserved_8 = initialParameters.Reserved_8;
                    clonedInitialParameters.Reserved_9 = initialParameters.Reserved_9;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneInitialParametersDbObject(BHM_Config_InitialParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInitialParameters;
        }

        //        /// <summary>
        //        /// Gets a list of all BHM_Config_InitialParameters objects associated with one Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_InitialParameters> BHM_Config_GetAllInitialParametersEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_InitialParameters> initialParametersList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_InitialParameters
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                initialParametersList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllInitialParametersEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return initialParametersList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_InitialParameters entries for one Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllInitialParametersEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_InitialParameters> initialParametersList = BHM_Config_GetAllInitialParametersEntries(monitorID, db);
        //                if (initialParametersList.Count > 0)
        //                {
        //                    db.BHM_Config_InitialParameters.DeleteAllOnSubmit(initialParametersList);
        //                    db.SubmitChanges();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllInitialParametersEntries(BHM_Config_InitialZkParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_InitialParameters object from the database
        /// </summary>
        /// <param name="initialParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteInitialParametersTableEntry(BHM_Config_InitialParameters initialParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (initialParameters != null)
                {
                    db.BHM_Config_InitialParameters.DeleteOnSubmit(initialParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteInitialParametersTableEntry(BHM_Config_InitialParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_InitialParameters object from the database
        /// </summary>
        /// <param name="initialParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteInitialParametersTableEntry(Guid initialParametersID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_InitialParameters initialParameters = BHM_DatabaseMethods.BHM_Config_GetInitialParametersTableEntry(initialParametersID, db);
                success = BHM_Config_DeleteInitialParametersTableEntry(initialParameters, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteInitialZkParameters(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_InitialParameters Table entries in the database, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllInitialParametersTableEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialParameters
                    select entry;

                if (queryList.Count() > 0)
                {
                    db.BHM_Config_InitialParameters.DeleteAllOnSubmit(queryList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllInitialParametersTableEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_InitialParameters destinationInitialParameters;
                BHM_Config_InitialParameters sourceInitialParameters = BHM_Config_GetInitialParametersTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceInitialParameters != null)
                {
                    destinationInitialParameters = BHM_Config_CloneOneInitialParametersDbObject(sourceInitialParameters);
                    destinationInitialParameters.ID = Guid.NewGuid();
                    destinationInitialParameters.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteInitialParametersTableEntry(destinationInitialParameters, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_InitialParameters to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_InitialParameters from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region InitialZkParameters

        /// <summary>
        /// Write one BHM_Config_InitialZkParameters object to the database
        /// </summary>
        /// <param name="initialZkParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteInitialZkParametersTableEntry(BHM_Config_InitialZkParameters initialZkParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_InitialZkParameters.InsertOnSubmit(initialZkParameters);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteInitialZkParameters(BHM_Config_InitialZkParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_InitialZkParameters BHM_Config_GetInitialZkParametersTableEntry(Guid initialZkParametersID, MonitorInterfaceDB db)
        {
            BHM_Config_InitialZkParameters initialZkParameters = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialZkParameters
                    where entry.ID == initialZkParametersID
                    select entry;

                if (queryList.Count() > 0)
                {
                    initialZkParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetInitialZkParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialZkParameters;
        }

        public static BHM_Config_InitialZkParameters BHM_Config_GetInitialZkParametersTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_InitialZkParameters initialZkParameters = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialZkParameters
                    where entry.ConfigurationRootID == configurationRootID
                    select entry;

                if (queryList.Count() > 0)
                {
                    initialZkParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetInitialZkParametersTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialZkParameters;
        }


        /// <summary>
        /// Gets the most recent BHM_Config_InitialZkParameters object associated with a given Monitor, selected with respect to the date it was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_InitialZkParameters BHM_Config_CloneOneInitialZkParametersDbObject(BHM_Config_InitialZkParameters initialZkParameters)
        {
            BHM_Config_InitialZkParameters clonedInitialZkParameters = null;
            try
            {
                if (initialZkParameters != null)
                {
                    clonedInitialZkParameters = new BHM_Config_InitialZkParameters();

                    clonedInitialZkParameters.ID = initialZkParameters.ID;
                    clonedInitialZkParameters.ConfigurationRootID = initialZkParameters.ConfigurationRootID;
                    clonedInitialZkParameters.DateAdded = initialZkParameters.DateAdded;

                    clonedInitialZkParameters.Day = initialZkParameters.Day;
                    clonedInitialZkParameters.Month = initialZkParameters.Month;
                    clonedInitialZkParameters.Year = initialZkParameters.Year;
                    clonedInitialZkParameters.Hour = initialZkParameters.Hour;
                    clonedInitialZkParameters.Min = initialZkParameters.Min;

                    clonedInitialZkParameters.Reserved_0 = initialZkParameters.Reserved_0;
                    clonedInitialZkParameters.Reserved_1 = initialZkParameters.Reserved_1;
                    clonedInitialZkParameters.Reserved_2 = initialZkParameters.Reserved_2;
                    clonedInitialZkParameters.Reserved_3 = initialZkParameters.Reserved_3;
                    clonedInitialZkParameters.Reserved_4 = initialZkParameters.Reserved_4;
                    clonedInitialZkParameters.Reserved_5 = initialZkParameters.Reserved_5;
                    clonedInitialZkParameters.Reserved_6 = initialZkParameters.Reserved_6;
                    clonedInitialZkParameters.Reserved_7 = initialZkParameters.Reserved_7;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneInitialZkParametersDbObject(BHM_Config_InitialZkParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInitialZkParameters;
        }

        //        /// <summary>
        //        /// Gets a list of all BHM_Config_InitialZkParameters objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_InitialZkParameters> BHM_Config_GetAllInitialZkParametersEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_InitialZkParameters> initialZkParametersList = null;
        //            try
        //            {
        //                var queryList =
        //                        from entry in db.BHM_Config_InitialZkParameters
        //                        where entry.MonitorID == monitorID
        //                        orderby entry.DateAdded descending
        //                        select entry;

        //                initialZkParametersList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestInitialZkParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return initialZkParametersList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_InitialZkParameters database objects associated with one Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllInitialZkParametersEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_InitialZkParameters> initialZkParametersList = BHM_Config_GetAllInitialZkParametersEntries(monitorID, db);
        //                if (initialZkParametersList.Count > 0)
        //                {
        //                    db.BHM_Config_InitialZkParameters.DeleteAllOnSubmit(initialZkParametersList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllInitialZkParametersEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            } 
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_InitialZkParameters entry from the database
        /// </summary>
        /// <param name="initialZkParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteInitialZkParametersTableEntry(BHM_Config_InitialZkParameters initialZkParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (initialZkParameters != null)
                {
                    db.BHM_Config_InitialZkParameters.DeleteOnSubmit(initialZkParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteInitialZkParametersTableEntry(BHM_Config_InitialZkParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_InitialZkParameters entry from the database
        /// </summary>
        /// <param name="initialZkParameters"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteInitialZkParametersTableEntry(Guid initialZkParametersID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_InitialZkParameters initialZkParameters = BHM_DatabaseMethods.BHM_Config_GetInitialZkParametersTableEntry(initialZkParametersID, db);
                success = BHM_Config_DeleteInitialZkParametersTableEntry(initialZkParameters, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteInitialZkParametersTableEntry(BHM_Config_InitialZkParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_InitialZkParameters Table entries from the database, probably only useful for testing
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllInitialZkParametersTableEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_InitialZkParameters
                    select entry;

                db.BHM_Config_InitialZkParameters.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllInitialZkParametersTableEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_InitialZkParameters destinationInitialZkParameters;
                BHM_Config_InitialZkParameters sourceInitialZkParameters = BHM_Config_GetInitialZkParametersTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceInitialZkParameters != null)
                {
                    destinationInitialZkParameters = BHM_Config_CloneOneInitialZkParametersDbObject(sourceInitialZkParameters);
                    destinationInitialZkParameters.ID = Guid.NewGuid();
                    destinationInitialZkParameters.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteInitialZkParametersTableEntry(destinationInitialZkParameters, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_InitialZkParameters to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_InitialZkParameters from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region MeasurementsInfo

        /// <summary>
        /// Writes a list of BHM_Config_MeasurementsInfo objects to the database
        /// </summary>
        /// <param name="measurementsInfoList"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteMeasurementsInfoTableEntry(List<BHM_Config_MeasurementsInfo> measurementsInfoList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_MeasurementsInfo.InsertAllOnSubmit(measurementsInfoList);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteMeasurementsInfoTableEntry(List<BHM_Config_MeasurementsInfo>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Writes one BHM_Config_MeasurementsInfo object to the database
        /// </summary>
        /// <param name="measurementsInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteMeasurementsInfoTableEntry(BHM_Config_MeasurementsInfo measurementsInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_MeasurementsInfo.InsertOnSubmit(measurementsInfo);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteMeasurementsInfoTableEntry(BHM_Config_MeasurementsInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_MeasurementsInfo BHM_Config_GetMeasurementsInfoTableEntry(Guid measurementsInfoID, MonitorInterfaceDB db)
        {
            BHM_Config_MeasurementsInfo measurementsInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_MeasurementsInfo
                    where entry.ID == measurementsInfoID
                    select entry;

                if (queryList.Count() > 0)
                {
                    measurementsInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetMeasurementsInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfo;
        }


        public static BHM_Config_MeasurementsInfo BHM_Config_GetMeasurementsInfoTableEntryForOneItemForOneConfiguration(Guid configurationRootID, int itemNumber, MonitorInterfaceDB db)
        {
            BHM_Config_MeasurementsInfo measurementsInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_MeasurementsInfo
                    where entry.ConfigurationRootID == configurationRootID
                    && entry.ItemNumber == itemNumber
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    measurementsInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetMeasurementsInfoTableEntryForOneItemForOneConfiguration(Guid, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfo;
        }

        public static BHM_Config_MeasurementsInfo BHM_Config_CloneOneMeasurementsInfoDbObject(BHM_Config_MeasurementsInfo measurementsInfo)
        {
            BHM_Config_MeasurementsInfo clonedMeasurementsInfo = null;
            try
            {
                if (measurementsInfo != null)
                {
                    clonedMeasurementsInfo = new BHM_Config_MeasurementsInfo();

                    clonedMeasurementsInfo.ID = measurementsInfo.ID;
                    clonedMeasurementsInfo.ConfigurationRootID = measurementsInfo.ConfigurationRootID;
                    clonedMeasurementsInfo.DateAdded = measurementsInfo.DateAdded;
                    clonedMeasurementsInfo.ItemNumber = measurementsInfo.ItemNumber;

                    clonedMeasurementsInfo.Hour = measurementsInfo.Hour;
                    clonedMeasurementsInfo.Minute = measurementsInfo.Minute;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneMeasurementsInfoDbObject(BHM_Config_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMeasurementsInfo;
        }

        public static List<BHM_Config_MeasurementsInfo> BHM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<BHM_Config_MeasurementsInfo> measurementsInfoList = null;
            try
            {
                var queryList =
                   from entry in db.BHM_Config_MeasurementsInfo
                   where entry.ConfigurationRootID == configurationRootID
                   orderby entry.ItemNumber ascending
                   select entry;

                measurementsInfoList = queryList.ToList();

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        public static List<BHM_Config_MeasurementsInfo> BHM_Config_CloneMeasurementsInfoDbObjectList(List<BHM_Config_MeasurementsInfo> measurementsInfoList)
        {
            List<BHM_Config_MeasurementsInfo> clonedMeasurementsInfoList = null;
            try
            {
                BHM_Config_MeasurementsInfo measurementsInfo;
                if (measurementsInfoList != null)
                {
                    clonedMeasurementsInfoList = new List<BHM_Config_MeasurementsInfo>();
                    foreach (BHM_Config_MeasurementsInfo entry in measurementsInfoList)
                    {
                        measurementsInfo = BHM_Config_CloneOneMeasurementsInfoDbObject(entry);
                        if (measurementsInfo != null)
                        {
                            clonedMeasurementsInfoList.Add(measurementsInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneMeasurementsInfoDbObjectList(List<BHM_Config_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedMeasurementsInfoList;
        }


        //        /// <summary>
        //        /// Gets a list of all BHM_Config_MeasurementsInfo database entries associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_MeasurementsInfo> BHM_Config_GetAllMeasurementsInfoTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_MeasurementsInfo> measurementsInfoList = new List<BHM_Config_MeasurementsInfo>();
        //            try
        //            {
        //                var queryList =
        //                   from entry in db.BHM_Config_MeasurementsInfo
        //                   where entry.MonitorID == monitorID
        //                   orderby entry.DateAdded descending
        //                   select entry;

        //                measurementsInfoList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllMeasurementsInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return measurementsInfoList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_MeasurementsInfo database entries associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllMeasurementsInfoTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_MeasurementsInfo> measurementsInfoList = BHM_Config_GetAllMeasurementsInfoTableEntries(monitorID, db);
        //                if (measurementsInfoList.Count > 0)
        //                {
        //                    db.BHM_Config_MeasurementsInfo.DeleteAllOnSubmit(measurementsInfoList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllMeasurementsInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes a single BHM_Config_MeasurementsInfo object from the database
        /// </summary>
        /// <param name="measurementsInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteMeasurementsInfoTableEntry(BHM_Config_MeasurementsInfo measurementsInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (measurementsInfo != null)
                {
                    db.BHM_Config_MeasurementsInfo.DeleteOnSubmit(measurementsInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteMeasurementsInfoTableEntry(BHM_Config_MeasurementsInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_DeleteMeasurementsInfoTableEntry(Guid measurementsInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_MeasurementsInfo measurementsInfo = BHM_DatabaseMethods.BHM_Config_GetMeasurementsInfoTableEntry(measurementsInfoID, db);
                success = BHM_Config_DeleteMeasurementsInfoTableEntry(measurementsInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteMeasurementsInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        /// <summary>
        /// Deletes all BHM_Config_MeasurementsInfo Table entries, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllMeasurementsInfoEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_MeasurementsInfo
                    select entry;

                db.BHM_Config_MeasurementsInfo.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllMeasurementsInfoEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<BHM_Config_MeasurementsInfo> destinationMeasurementsInfoList;
                List<BHM_Config_MeasurementsInfo> sourceMeasurementsInfoList = BHM_Config_GetAllMeasurementsInfoTableEntriesForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceMeasurementsInfoList != null)
                {
                    destinationMeasurementsInfoList = BHM_Config_CloneMeasurementsInfoDbObjectList(sourceMeasurementsInfoList);
                    for (int i = 0; i < destinationMeasurementsInfoList.Count; i++)
                    {
                        destinationMeasurementsInfoList[i].ID = Guid.NewGuid();
                        destinationMeasurementsInfoList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = BHM_Config_WriteMeasurementsInfoTableEntry(destinationMeasurementsInfoList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<BHM_Config_MeasurementsInfo> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<BHM_Config_MeasurementsInfo> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region SetupInfo

        /// <summary>
        /// Writes one BHM_Config_SetupInfo entry to the database
        /// </summary>
        /// <param name="setupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteSetupInfoTableEntry(BHM_Config_SetupInfo setupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_SetupInfo.InsertOnSubmit(setupInfo);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteSetupInfoTableEntry(BHM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Gets a specific entry from the BHM_Config_SetupInfo table
        /// </summary>
        /// <param name="setupInfoID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_SetupInfo BHM_Config_GetSetupInfoTableEntry(Guid setupInfoID, MonitorInterfaceDB db)
        {
            BHM_Config_SetupInfo setupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SetupInfo
                    where entry.ID == setupInfoID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    setupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        public static BHM_Config_SetupInfo BHM_Config_GetSetupInfoTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_SetupInfo setupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SetupInfo
                    where entry.ConfigurationRootID == configurationRootID
                    select entry;

                if (queryList.Count() > 0)
                {
                    setupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetSetupInfoTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        public static BHM_Config_SetupInfo BHM_Config_CloneOneSetupInfoDbObject(BHM_Config_SetupInfo setupInfo)
        {
            BHM_Config_SetupInfo clonedSetupInfo = null;
            try
            {
                if (setupInfo != null)
                {
                    clonedSetupInfo = new BHM_Config_SetupInfo();

                    clonedSetupInfo.ID = setupInfo.ID;
                    clonedSetupInfo.ConfigurationRootID = setupInfo.ConfigurationRootID;
                    clonedSetupInfo.DateAdded = setupInfo.DateAdded;
                    clonedSetupInfo.FirmwareVersion = setupInfo.FirmwareVersion;

                    clonedSetupInfo.DeviceNumber = setupInfo.DeviceNumber;
                    clonedSetupInfo.BaudRate = setupInfo.BaudRate;
                    clonedSetupInfo.OutTime = setupInfo.OutTime;
                    clonedSetupInfo.DisplayFlag = setupInfo.DisplayFlag;
                    clonedSetupInfo.SaveDays = setupInfo.SaveDays;
                    clonedSetupInfo.Stopped = setupInfo.Stopped;
                    clonedSetupInfo.Relay = setupInfo.Relay;
                    clonedSetupInfo.TimeOfRelayAlarm = setupInfo.TimeOfRelayAlarm;
                    clonedSetupInfo.ScheduleType = setupInfo.ScheduleType;
                    clonedSetupInfo.DTime_Hour = setupInfo.DTime_Hour;
                    clonedSetupInfo.DTime_Min = setupInfo.DTime_Min;
                    clonedSetupInfo.SyncType = setupInfo.SyncType;
                    clonedSetupInfo.InternalSyncFrequency = setupInfo.InternalSyncFrequency;
                    clonedSetupInfo.ModBusProtocol = setupInfo.ModBusProtocol;
                    clonedSetupInfo.ReadAnalogFromRegister = setupInfo.ReadAnalogFromRegister;
                    clonedSetupInfo.HeaterOnTemperature = setupInfo.HeaterOnTemperature;
                    clonedSetupInfo.AutoBalans_Month = setupInfo.AutoBalans_Month;
                    clonedSetupInfo.AutoBalans_Year = setupInfo.AutoBalans_Year;
                    clonedSetupInfo.AutoBalans_Day = setupInfo.AutoBalans_Day;
                    clonedSetupInfo.AutoBalans_Hour = setupInfo.AutoBalans_Hour;
                    clonedSetupInfo.AutoBalansActive = setupInfo.AutoBalansActive;
                    clonedSetupInfo.NoLoadTestActive = setupInfo.NoLoadTestActive;
                    clonedSetupInfo.WorkingDays = setupInfo.WorkingDays;

                    clonedSetupInfo.Reserved_0 = setupInfo.Reserved_0;
                    clonedSetupInfo.Reserved_1 = setupInfo.Reserved_1;
                    clonedSetupInfo.Reserved_2 = setupInfo.Reserved_2;
                    clonedSetupInfo.Reserved_3 = setupInfo.Reserved_3;
                    clonedSetupInfo.Reserved_4 = setupInfo.Reserved_4;
                    clonedSetupInfo.Reserved_5 = setupInfo.Reserved_5;
                    clonedSetupInfo.Reserved_6 = setupInfo.Reserved_6;
                    clonedSetupInfo.Reserved_7 = setupInfo.Reserved_7;
                    clonedSetupInfo.Reserved_8 = setupInfo.Reserved_8;
                    clonedSetupInfo.Reserved_9 = setupInfo.Reserved_9;
                    clonedSetupInfo.Reserved_10 = setupInfo.Reserved_10;
                    clonedSetupInfo.Reserved_11 = setupInfo.Reserved_11;
                    clonedSetupInfo.Reserved_12 = setupInfo.Reserved_12;
                    clonedSetupInfo.Reserved_13 = setupInfo.Reserved_13;
                    clonedSetupInfo.Reserved_14 = setupInfo.Reserved_14;
                    clonedSetupInfo.Reserved_15 = setupInfo.Reserved_15;
                    clonedSetupInfo.Reserved_16 = setupInfo.Reserved_16;
                    clonedSetupInfo.Reserved_17 = setupInfo.Reserved_17;
                    clonedSetupInfo.Reserved_18 = setupInfo.Reserved_18;
                    clonedSetupInfo.Reserved_19 = setupInfo.Reserved_19;
                    clonedSetupInfo.Reserved_20 = setupInfo.Reserved_20;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneSetupInfoDbObject(BHM_Config_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedSetupInfo;
        }



        //        /// <summary>
        //        /// Gets all BHM_Config_SetupInfo objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_SetupInfo> BHM_Config_GetAllSetupInfoTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_SetupInfo> setupInfoList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_SetupInfo
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                setupInfoList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllSetupInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return setupInfoList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_SetupInfo objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllSetupInfoTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_SetupInfo> setupInfoList = BHM_Config_GetAllSetupInfoTableEntries(monitorID, db);
        //                if (setupInfoList.Count > 0)
        //                {
        //                    db.BHM_Config_SetupInfo.DeleteAllOnSubmit(setupInfoList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllSetupInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_SetupInfo object from the database
        /// </summary>
        /// <param name="setupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteSetupInfoTableEntry(BHM_Config_SetupInfo setupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (setupInfo != null)
                {
                    db.BHM_Config_SetupInfo.DeleteOnSubmit(setupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteSetupInfoTableEntry(BHM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_SetupInfo object from the database
        /// </summary>
        /// <param name="setupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteSetupInfoTableEntry(Guid setupInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_SetupInfo setupInfo = BHM_DatabaseMethods.BHM_Config_GetSetupInfoTableEntry(setupInfoID, db);
                success = BHM_Config_DeleteSetupInfoTableEntry(setupInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteSetupInfoTableEntry(BHM_Config_SetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_SetupInfo Table entries from the database, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllSetupInfoEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SetupInfo
                    select entry;

                db.BHM_Config_SetupInfo.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllSetupInfoEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_SetupInfo setupInfo = BHM_Config_GetSetupInfoTableEntryForOneConfiguration(configurationRootID, sourceDB);
                if (setupInfo != null)
                {
                    success = BHM_Config_WriteSetupInfoTableEntry(BHM_Config_CloneOneSetupInfoDbObject(setupInfo), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the BHM_Config_SetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the BHM_Config_SetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_SetupInfo destinationSetupInfo;
                BHM_Config_SetupInfo sourceSetupInfo = BHM_Config_GetSetupInfoTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceSetupInfo != null)
                {
                    destinationSetupInfo = BHM_Config_CloneOneSetupInfoDbObject(sourceSetupInfo);
                    destinationSetupInfo.ID = Guid.NewGuid();
                    destinationSetupInfo.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteSetupInfoTableEntry(destinationSetupInfo, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_SetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_SetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region SideToSideData

        /// <summary>
        /// Writes one BHM_Config_SideToSideData object to the database
        /// </summary>
        /// <param name="sideToSideData"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteSideToSideDataTableEntry(BHM_Config_SideToSideData sideToSideData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_SideToSideData.InsertOnSubmit(sideToSideData);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteSideToSideDataTableEntry(BHM_Config_SideToSideData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_SideToSideData BHM_Config_GetSideToSideDataTableEntry(Guid sideToSideDataID, MonitorInterfaceDB db)
        {
            BHM_Config_SideToSideData sideToSideData = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SideToSideData
                    where entry.ID == sideToSideDataID
                    select entry;

                if (queryList.Count() > 0)
                {
                    sideToSideData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetSideToSideDataTableTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_SideToSideData object associated with a given Monitor, selected with respect to the date the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_SideToSideData BHM_Config_GetSideToSideDataTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_SideToSideData sideToSideData = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SideToSideData
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    sideToSideData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetSideToSideDataTableEntryForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }

        public static BHM_Config_SideToSideData BHM_Config_CloneOneSideToSideDataDbObject(BHM_Config_SideToSideData sideToSideData)
        {
            BHM_Config_SideToSideData clonedSideToSideData = null;
            try
            {
                if (sideToSideData != null)
                {
                    clonedSideToSideData = new BHM_Config_SideToSideData();

                    clonedSideToSideData.ID = sideToSideData.ID;
                    clonedSideToSideData.ConfigurationRootID = sideToSideData.ConfigurationRootID;
                    clonedSideToSideData.DateAdded = sideToSideData.DateAdded;
                    clonedSideToSideData.SideNumber = sideToSideData.SideNumber;

                    clonedSideToSideData.Amplitude1_0 = sideToSideData.Amplitude1_0;
                    clonedSideToSideData.Amplitude1_1 = sideToSideData.Amplitude1_1;
                    clonedSideToSideData.Amplitude1_2 = sideToSideData.Amplitude1_2;
                    clonedSideToSideData.Amplitude2_0 = sideToSideData.Amplitude2_0;
                    clonedSideToSideData.Amplitude2_1 = sideToSideData.Amplitude2_1;
                    clonedSideToSideData.Amplitude2_2 = sideToSideData.Amplitude2_2;
                    clonedSideToSideData.PhaseShift_0 = sideToSideData.PhaseShift_0;
                    clonedSideToSideData.PhaseShift_1 = sideToSideData.PhaseShift_1;
                    clonedSideToSideData.PhaseShift_2 = sideToSideData.PhaseShift_2;
                    clonedSideToSideData.CurrentAmpl_0 = sideToSideData.CurrentAmpl_0;
                    clonedSideToSideData.CurrentAmpl_1 = sideToSideData.CurrentAmpl_1;
                    clonedSideToSideData.CurrentAmpl_2 = sideToSideData.CurrentAmpl_2;
                    clonedSideToSideData.CurrentPhase_0 = sideToSideData.CurrentPhase_0;
                    clonedSideToSideData.CurrentPhase_1 = sideToSideData.CurrentPhase_1;
                    clonedSideToSideData.CurrentPhase_2 = sideToSideData.CurrentPhase_2;
                    clonedSideToSideData.ZkAmpl_0 = sideToSideData.ZkAmpl_0;
                    clonedSideToSideData.ZkAmpl_1 = sideToSideData.ZkAmpl_1;
                    clonedSideToSideData.ZkAmpl_2 = sideToSideData.ZkAmpl_2;
                    clonedSideToSideData.ZkPhase_0 = sideToSideData.ZkPhase_0;
                    clonedSideToSideData.ZkPhase_1 = sideToSideData.ZkPhase_1;
                    clonedSideToSideData.ZkPhase_2 = sideToSideData.ZkPhase_2;
                    clonedSideToSideData.CurrentChannelShift = sideToSideData.CurrentChannelShift;
                    clonedSideToSideData.ZkPercent_0 = sideToSideData.ZkPercent_0;
                    clonedSideToSideData.ZkPercent_1 = sideToSideData.ZkPercent_1;
                    clonedSideToSideData.ZkPercent_2 = sideToSideData.ZkPercent_2;

                    clonedSideToSideData.Reserved_0 = sideToSideData.Reserved_0;
                    clonedSideToSideData.Reserved_1 = sideToSideData.Reserved_1;
                    clonedSideToSideData.Reserved_2 = sideToSideData.Reserved_2;
                    clonedSideToSideData.Reserved_3 = sideToSideData.Reserved_3;
                    clonedSideToSideData.Reserved_4 = sideToSideData.Reserved_4;
                    clonedSideToSideData.Reserved_5 = sideToSideData.Reserved_5;
                    clonedSideToSideData.Reserved_6 = sideToSideData.Reserved_6;
                    clonedSideToSideData.Reserved_7 = sideToSideData.Reserved_7;
                    clonedSideToSideData.Reserved_8 = sideToSideData.Reserved_8;
                    clonedSideToSideData.Reserved_9 = sideToSideData.Reserved_9;
                    clonedSideToSideData.Reserved_10 = sideToSideData.Reserved_10;
                    clonedSideToSideData.Reserved_11 = sideToSideData.Reserved_11;
                    clonedSideToSideData.Reserved_12 = sideToSideData.Reserved_12;
                    clonedSideToSideData.Reserved_13 = sideToSideData.Reserved_13;
                    clonedSideToSideData.Reserved_14 = sideToSideData.Reserved_14;
                    clonedSideToSideData.Reserved_15 = sideToSideData.Reserved_15;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneSideToSideDataDbObject(BHM_Config_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedSideToSideData;
        }

        //        /// <summary>
        //        /// Gets a list of all BHM_Config_SideToSideData objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_SideToSideData> BHM_Config_GetAllSideToSideDataTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_SideToSideData> sideToSideDataList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_SideToSideData
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                    sideToSideDataList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllSideToSideDataEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return sideToSideDataList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_SideToSideData objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllSideToSideDataTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_SideToSideData> sideToSideDataList = BHM_Config_GetAllSideToSideDataTableEntries(monitorID, db);
        //                if (sideToSideDataList.Count > 0)
        //                {
        //                    db.BHM_Config_SideToSideData.DeleteAllOnSubmit(sideToSideDataList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllSideToSideDataTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_SideToSideData entry from the database
        /// </summary>
        /// <param name="sideToSideData"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteSideToSideDataTableEntry(BHM_Config_SideToSideData sideToSideData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_SideToSideData.DeleteOnSubmit(sideToSideData);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteSideToSideDataTableEntry(BHM_Config_SideToSideData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_SideToSideData entry from the database
        /// </summary>
        /// <param name="sideToSideData"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteSideToSideDataTableEntry(Guid sideToSideDataID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_SideToSideData sideToSideData = BHM_Config_GetSideToSideDataTableEntry(sideToSideDataID, db);
                success = BHM_Config_DeleteSideToSideDataTableEntry(sideToSideData, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteSideToSideDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_SideToSideData Table entries, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllSideToSideDataEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_SideToSideData
                    select entry;

                db.BHM_Config_SideToSideData.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllSideToSideDataEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_SideToSideData destinationSideToSideData;
                BHM_Config_SideToSideData sourceSideToSideData = BHM_Config_GetSideToSideDataTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceSideToSideData != null)
                {
                    destinationSideToSideData = BHM_Config_CloneOneSideToSideDataDbObject(sourceSideToSideData);
                    destinationSideToSideData.ID = Guid.NewGuid();
                    destinationSideToSideData.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteSideToSideDataTableEntry(destinationSideToSideData, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_SideToSideData to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_SideToSideData from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region TrSideParam

        /// <summary>
        /// Writes a list of BHM_Config_TrSideParam objects to the database
        /// </summary>
        /// <param name="trSideParamList"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteTrSideParamTableEntries(List<BHM_Config_TrSideParam> trSideParamList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_TrSideParam.InsertAllOnSubmit(trSideParamList);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteTrSideParamTableEntry(List<BHM_Config_TrSideParam>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Writes one BHM_Config_TrSideParam object to the database
        /// </summary>
        /// <param name="trSideParam"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteTrSideParamTableEntry(BHM_Config_TrSideParam trSideParam, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_TrSideParam.InsertOnSubmit(trSideParam);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteTrSideParamTableEntry(BHM_Config_TrSideParam, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_TrSideParam BHM_Config_GetTrSideParamTableEntry(Guid trSideParamID, MonitorInterfaceDB db)
        {
            BHM_Config_TrSideParam trSideParam = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_TrSideParam
                    where entry.ID == trSideParamID
                    select entry;

                if (queryList.Count() > 0)
                {
                    trSideParam = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntry(Guid, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return trSideParam;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_TrSideParam object for a given side for a given Monitor, selected with respect to the data the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="sideNumber"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_TrSideParam BHM_Config_GetTrSideParamTableEntryForOneSideForOneConfiguration(Guid configurationRootID, int sideNumber, MonitorInterfaceDB db)
        {
            BHM_Config_TrSideParam trSideParam = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_TrSideParam
                    where entry.ConfigurationRootID == configurationRootID
                    && entry.SideNumber == sideNumber
                    select entry;

                if (queryList.Count() > 0)
                {
                    trSideParam = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntryForOneSideForOneConfiguration(Guid, int, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return trSideParam;
        }

        public static BHM_Config_TrSideParam BHM_Config_CloneOneTrSideParamDbObject(BHM_Config_TrSideParam trSideParam)
        {
            BHM_Config_TrSideParam clonedTrSideParam = null;
            try
            {
                if (trSideParam != null)
                {
                    clonedTrSideParam = new BHM_Config_TrSideParam();

                    clonedTrSideParam.ID = trSideParam.ID;
                    clonedTrSideParam.ConfigurationRootID = trSideParam.ConfigurationRootID;
                    clonedTrSideParam.DateAdded = trSideParam.DateAdded;
                    clonedTrSideParam.SideNumber = trSideParam.SideNumber;

                    clonedTrSideParam.ChPhaseShift = trSideParam.ChPhaseShift;
                    clonedTrSideParam.PhaseAmplitude_0 = trSideParam.PhaseAmplitude_0;
                    clonedTrSideParam.PhaseAmplitude_1 = trSideParam.PhaseAmplitude_1;
                    clonedTrSideParam.PhaseAmplitude_2 = trSideParam.PhaseAmplitude_2;
                    clonedTrSideParam.SourceAmplitude_0 = trSideParam.SourceAmplitude_0;
                    clonedTrSideParam.SourceAmplitude_1 = trSideParam.SourceAmplitude_1;
                    clonedTrSideParam.SourceAmplitude_2 = trSideParam.SourceAmplitude_2;
                    clonedTrSideParam.Temperature = trSideParam.Temperature;
                    clonedTrSideParam.GammaAmplitude = trSideParam.GammaAmplitude;
                    clonedTrSideParam.GammaPhase = trSideParam.GammaPhase;
                    clonedTrSideParam.KT = trSideParam.KT;
                    clonedTrSideParam.KTPhase = trSideParam.KTPhase;
                    clonedTrSideParam.DefectCode = trSideParam.DefectCode;
                    clonedTrSideParam.KTSaved = trSideParam.KTSaved;
                    clonedTrSideParam.SignalPhase_0 = trSideParam.SignalPhase_0;
                    clonedTrSideParam.SignalPhase_1 = trSideParam.SignalPhase_1;
                    clonedTrSideParam.SignalPhase_2 = trSideParam.SignalPhase_2;
                    clonedTrSideParam.SourcePhase_0 = trSideParam.SourcePhase_0;
                    clonedTrSideParam.SourcePhase_1 = trSideParam.SourcePhase_1;
                    clonedTrSideParam.SourcePhase_2 = trSideParam.SourcePhase_2;
                    clonedTrSideParam.RPN = trSideParam.RPN;
                    clonedTrSideParam.TrSideParamCurrent = trSideParam.TrSideParamCurrent;

                    clonedTrSideParam.Reserved_0 = trSideParam.Reserved_0;
                    clonedTrSideParam.Reserved_1 = trSideParam.Reserved_1;
                    clonedTrSideParam.Reserved_2 = trSideParam.Reserved_2;
                    clonedTrSideParam.Reserved_3 = trSideParam.Reserved_3;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneTrSideParamDbObject(BHM_Config_TrSideParam)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTrSideParam;
        }


        /// <summary>
        /// Gets the latest pair of BHM_Config_TrSideParam objects associated with a given Monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Config_TrSideParam> BHM_Config_GetTrSideParamTableEntriesForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            List<BHM_Config_TrSideParam> trSideParamList = new List<BHM_Config_TrSideParam>();
            try
            {
                BHM_Config_TrSideParam entry = BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntryForOneSideForOneConfiguration(configurationRootID, 0, db);
                if (entry != null)
                {
                    trSideParamList.Add(entry);
                }
                entry = BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntryForOneSideForOneConfiguration(configurationRootID, 1, db);
                if (entry != null)
                {
                    trSideParamList.Add(entry);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetTrSideParamTableEntriesForOneConfiguration(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return trSideParamList;
        }

        public static List<BHM_Config_TrSideParam> BHM_Config_CloneTrSideParamDbObjectList(List<BHM_Config_TrSideParam> trSideParamList)
        {
            List<BHM_Config_TrSideParam> clonedTrSideParamList = null;
            try
            {
                BHM_Config_TrSideParam trSideParam;
                if (trSideParamList != null)
                {
                    clonedTrSideParamList = new List<BHM_Config_TrSideParam>();
                    foreach (BHM_Config_TrSideParam entry in trSideParamList)
                    {
                        trSideParam = BHM_Config_CloneOneTrSideParamDbObject(entry);
                        if (trSideParam != null)
                        {
                            clonedTrSideParamList.Add(trSideParam);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneTrSideParamDbObjectList(List<BHM_Config_TrSideParam>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTrSideParamList;
        }


        //        /// <summary>
        //        /// Gets a list of all BHM_Config_TrSideParam entries for one Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_TrSideParam> BHM_Config_GetAllTrSideParamTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_TrSideParam> trSideParamList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_TrSideParam
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                trSideParamList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllTrSideParamTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return trSideParamList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_TrSideParam entries for one Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllTrSideParamEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_TrSideParam> trSideParamList = BHM_Config_GetAllTrSideParamTableEntries(monitorID, db);
        //                if (trSideParamList.Count > 0)
        //                {
        //                    db.BHM_Config_TrSideParam.DeleteAllOnSubmit(trSideParamList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllTrSideParamEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_TrSideParam object from the database
        /// </summary>
        /// <param name="trSideParam"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteTrSideParamTableEntry(BHM_Config_TrSideParam trSideParam, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (trSideParam != null)
                {
                    db.BHM_Config_TrSideParam.DeleteOnSubmit(trSideParam);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteTrSideParamTableEntry(BHM_Config_TrSideParam, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_TrSideParam object from the database
        /// </summary>
        /// <param name="trSideParam"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteTrSideParamTableEntry(Guid trSideParamID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_TrSideParam trSideParam = BHM_Config_GetTrSideParamTableEntry(trSideParamID, db);
                success = BHM_Config_DeleteTrSideParamTableEntry(trSideParam, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteTrSideParamTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_TrSideParam Table entries from the database, probably only useful for testing purposes
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllTrSideParamEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_TrSideParam
                    select entry;

                db.BHM_Config_TrSideParam.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllTrSideParamEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<BHM_Config_TrSideParam> destinationTrSideParamList;
                List<BHM_Config_TrSideParam> sourceTrSideParamList = BHM_Config_GetTrSideParamTableEntriesForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceTrSideParamList != null)
                {
                    destinationTrSideParamList = BHM_Config_CloneTrSideParamDbObjectList(sourceTrSideParamList);
                    for(int i=0; i< destinationTrSideParamList.Count;i++)
                    {
                        destinationTrSideParamList[i].ID = Guid.NewGuid();
                        destinationTrSideParamList[i].ConfigurationRootID = destinationConfigurationRootID;
                    }
                    success = BHM_Config_WriteTrSideParamTableEntries(destinationTrSideParamList, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the List<BHM_Config_TrSideParam> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the List<BHM_Config_TrSideParam> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region ZkSetupInfo

        /// <summary>
        /// Writes one BHM_Config_ZkSetupInfo object do the database
        /// </summary>
        /// <param name="zkSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteZkSetupInfoTableEntry(BHM_Config_ZkSetupInfo zkSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_ZkSetupInfo.InsertOnSubmit(zkSetupInfo);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteZkSetupInfoTableEntry(BHM_Config_ZkSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_ZkSetupInfo BHM_Config_GetZkSetupInfoTableEntry(Guid zkSetupInfoID, MonitorInterfaceDB db)
        {
            BHM_Config_ZkSetupInfo zkSetupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSetupInfo
                    where entry.ID == zkSetupInfoID
                    select entry;

                if (queryList.Count() > 0)
                {
                    zkSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetZkSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSetupInfo;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_ZkSetupInfo object associated with a given Monitor, selected with respect to the date the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_ZkSetupInfo BHM_Config_GetZkSetupInfoTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_ZkSetupInfo zkSetupInfo = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSetupInfo
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    zkSetupInfo = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestZkSetupInfoTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSetupInfo;
        }

        public static BHM_Config_ZkSetupInfo BHM_Config_CloneOneZkSetupInfoDbObject(BHM_Config_ZkSetupInfo zkSetupInfo)
        {
            BHM_Config_ZkSetupInfo clonedZkSetupInfo = null;
            try
            {
                if (zkSetupInfo != null)
                {
                    clonedZkSetupInfo = new BHM_Config_ZkSetupInfo();

                    clonedZkSetupInfo.ID = zkSetupInfo.ID;
                    clonedZkSetupInfo.ConfigurationRootID = zkSetupInfo.ConfigurationRootID;
                    clonedZkSetupInfo.DateAdded = zkSetupInfo.DateAdded;

                    clonedZkSetupInfo.CalcZkOnSide = zkSetupInfo.CalcZkOnSide;
                    clonedZkSetupInfo.MaxParameterChange = zkSetupInfo.MaxParameterChange;
                    clonedZkSetupInfo.AlarmHysteresis = zkSetupInfo.AlarmHysteresis;
                    clonedZkSetupInfo.DaysToCalculateTrend = zkSetupInfo.DaysToCalculateTrend;
                    clonedZkSetupInfo.AveragingForZk = zkSetupInfo.AveragingForZk;
                    clonedZkSetupInfo.ReReadOnAlarm = zkSetupInfo.ReReadOnAlarm;
                    clonedZkSetupInfo.ZkDays = zkSetupInfo.ZkDays;
                    clonedZkSetupInfo.Empty = zkSetupInfo.Empty;
                    clonedZkSetupInfo.FaultDate = zkSetupInfo.FaultDate;

                    clonedZkSetupInfo.Reserved_0 = zkSetupInfo.Reserved_0;
                    clonedZkSetupInfo.Reserved_1 = zkSetupInfo.Reserved_1;
                    clonedZkSetupInfo.Reserved_2 = zkSetupInfo.Reserved_2;
                    clonedZkSetupInfo.Reserved_3 = zkSetupInfo.Reserved_3;
                    clonedZkSetupInfo.Reserved_4 = zkSetupInfo.Reserved_4;
                    clonedZkSetupInfo.Reserved_5 = zkSetupInfo.Reserved_5;
                    clonedZkSetupInfo.Reserved_6 = zkSetupInfo.Reserved_6;
                    clonedZkSetupInfo.Reserved_7 = zkSetupInfo.Reserved_7;
                    clonedZkSetupInfo.Reserved_8 = zkSetupInfo.Reserved_8;
                    clonedZkSetupInfo.Reserved_9 = zkSetupInfo.Reserved_9;
                    clonedZkSetupInfo.Reserved_10 = zkSetupInfo.Reserved_10;
                    clonedZkSetupInfo.Reserved_11 = zkSetupInfo.Reserved_11;
                    clonedZkSetupInfo.Reserved_12 = zkSetupInfo.Reserved_12;
                    clonedZkSetupInfo.Reserved_13 = zkSetupInfo.Reserved_13;
                    clonedZkSetupInfo.Reserved_14 = zkSetupInfo.Reserved_14;
                    clonedZkSetupInfo.Reserved_15 = zkSetupInfo.Reserved_15;
                    clonedZkSetupInfo.Reserved_16 = zkSetupInfo.Reserved_16;
                    clonedZkSetupInfo.Reserved_17 = zkSetupInfo.Reserved_17;
                    clonedZkSetupInfo.Reserved_18 = zkSetupInfo.Reserved_18;
                    clonedZkSetupInfo.Reserved_19 = zkSetupInfo.Reserved_19;
                    clonedZkSetupInfo.Reserved_20 = zkSetupInfo.Reserved_20;
                    clonedZkSetupInfo.Reserved_21 = zkSetupInfo.Reserved_21;
                    clonedZkSetupInfo.Reserved_22 = zkSetupInfo.Reserved_22;
                    clonedZkSetupInfo.Reserved_23 = zkSetupInfo.Reserved_23;
                    clonedZkSetupInfo.Reserved_24 = zkSetupInfo.Reserved_24;
                    clonedZkSetupInfo.Reserved_25 = zkSetupInfo.Reserved_25;
                    clonedZkSetupInfo.Reserved_26 = zkSetupInfo.Reserved_26;
                    clonedZkSetupInfo.Reserved_27 = zkSetupInfo.Reserved_27;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneZkSetupInfoDbObject(BHM_Config_ZkSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedZkSetupInfo;
        }



        //        /// <summary>
        //        /// Gets a list of all BHM_Config_ZkSetupInfo objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_ZkSetupInfo> BHM_Config_GetAllZkSetupInfoTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_ZkSetupInfo> zkSetupInfoList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_ZkSetupInfo
        //                    where entry.ConfigurationRootID == configurationRootID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                zkSetupInfoList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllZkSetupInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return zkSetupInfoList;
        //        }

        //        /// <summary>
        //        ///  Deletes all BHM_Config_ZkSetupInfo objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllZkSetupInfoTableEntries(Guid configurationRootID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_ZkSetupInfo> zkSetupInfoList = BHM_Config_GetAllZkSetupInfoTableEntries(monitorID, db);
        //                if (zkSetupInfoList.Count > 0)
        //                {
        //                    db.BHM_Config_ZkSetupInfo.DeleteAllOnSubmit(zkSetupInfoList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllZkSetupInfoTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Deletes one BHM_Config_ZkSetupInfo object from the database
        /// </summary>
        /// <param name="zkSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteZkSetupInfoTableEntry(BHM_Config_ZkSetupInfo zkSetupInfo, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (zkSetupInfo != null)
                {
                    db.BHM_Config_ZkSetupInfo.DeleteOnSubmit(zkSetupInfo);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteZkSetupInfoTableEntry(BHM_Config_ZkSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes one BHM_Config_ZkSetupInfo object from the database
        /// </summary>
        /// <param name="zkSetupInfo"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteZkSetupInfoTableEntry(Guid zkSetupInfoID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_ZkSetupInfo zkSetupInfo = BHM_Config_GetZkSetupInfoTableEntry(zkSetupInfoID, db);
                BHM_Config_DeleteZkSetupInfoTableEntry(zkSetupInfo, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteZkSetupInfoTableEntry(BHM_Config_ZkSetupInfo, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_ZkSetupInfo table entries, probably only useful for testing
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllZkSetupInfoEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSetupInfo
                    select entry;

                db.BHM_Config_ZkSetupInfo.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllZkSetupInfoEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_ZkSetupInfo destinationZkSetupInfo;
                BHM_Config_ZkSetupInfo sourceZkSetupInfo = BHM_Config_GetZkSetupInfoTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceZkSetupInfo != null)
                {
                    destinationZkSetupInfo = BHM_Config_CloneOneZkSetupInfoDbObject(sourceZkSetupInfo);
                    destinationZkSetupInfo.ID = Guid.NewGuid();
                    destinationZkSetupInfo.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteZkSetupInfoTableEntry(destinationZkSetupInfo, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_ZkSetupInfo to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_ZkSetupInfo from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region ZkSideToSideSetup

        /// <summary>
        /// Writes one BHM_Config_ZkSideToSideSetup object to the database
        /// </summary>
        /// <param name="zkSideToSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_WriteZkSideToSideSetupTableEntry(BHM_Config_ZkSideToSideSetup zkSideToSideSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                db.BHM_Config_ZkSideToSideSetup.InsertOnSubmit(zkSideToSideSetup);
                db.SubmitChanges();
                success = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_WriteZkSideToSideSetupTableEntry(BHM_Config_ZkSideToSideSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Config_ZkSideToSideSetup BHM_Config_GetZkSideToSideSetupTableEntry(Guid zkSideToSideSetupID, MonitorInterfaceDB db)
        {
            BHM_Config_ZkSideToSideSetup zkSideToSideSetup = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSideToSideSetup
                    where entry.ID == zkSideToSideSetupID
                    select entry;

                if (queryList.Count() > 0)
                {
                    zkSideToSideSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetZkSideToSideSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSideToSideSetup;
        }

        /// <summary>
        /// Gets the most recent BHM_Config_ZkSideToSideSetup object associated with a given monitor, selected with respect to the date the object was added to the database
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static BHM_Config_ZkSideToSideSetup BHM_Config_GetZkSideToSideSetupTableEntryForOneConfiguration(Guid configurationRootID, MonitorInterfaceDB db)
        {
            BHM_Config_ZkSideToSideSetup zkSideToSideSetup = null;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSideToSideSetup
                    where entry.ConfigurationRootID == configurationRootID
                    orderby entry.DateAdded descending
                    select entry;

                if (queryList.Count() > 0)
                {
                    zkSideToSideSetup = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetLatestZkSideToSideSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSideToSideSetup;
        }

        public static BHM_Config_ZkSideToSideSetup BHM_Config_CloneOneZkSideToSideSetupDbObject(BHM_Config_ZkSideToSideSetup zkSideToSideSetup)
        {
            BHM_Config_ZkSideToSideSetup clonedZkSideToSideSetup = null;
            try
            {
                if (zkSideToSideSetup != null)
                {
                    clonedZkSideToSideSetup = new BHM_Config_ZkSideToSideSetup();

                    clonedZkSideToSideSetup.ID = zkSideToSideSetup.ID;
                    clonedZkSideToSideSetup.ConfigurationRootID = zkSideToSideSetup.ConfigurationRootID;
                    clonedZkSideToSideSetup.DateAdded = zkSideToSideSetup.DateAdded;
                    clonedZkSideToSideSetup.SideNumber = zkSideToSideSetup.SideNumber;

                    clonedZkSideToSideSetup.ZkYellowThreshold = zkSideToSideSetup.ZkYellowThreshold;
                    clonedZkSideToSideSetup.ZkRedThreshold = zkSideToSideSetup.ZkRedThreshold;
                    clonedZkSideToSideSetup.ZkTrendAlarm = zkSideToSideSetup.ZkTrendAlarm;

                    clonedZkSideToSideSetup.Reserved_0 = zkSideToSideSetup.Reserved_0;
                    clonedZkSideToSideSetup.Reserved_1 = zkSideToSideSetup.Reserved_1;
                    clonedZkSideToSideSetup.Reserved_2 = zkSideToSideSetup.Reserved_2;
                    clonedZkSideToSideSetup.Reserved_3 = zkSideToSideSetup.Reserved_3;
                    clonedZkSideToSideSetup.Reserved_4 = zkSideToSideSetup.Reserved_4;
                    clonedZkSideToSideSetup.Reserved_5 = zkSideToSideSetup.Reserved_5;
                    clonedZkSideToSideSetup.Reserved_6 = zkSideToSideSetup.Reserved_6;
                    clonedZkSideToSideSetup.Reserved_7 = zkSideToSideSetup.Reserved_7;
                    clonedZkSideToSideSetup.Reserved_8 = zkSideToSideSetup.Reserved_8;
                    clonedZkSideToSideSetup.Reserved_9 = zkSideToSideSetup.Reserved_9;
                    clonedZkSideToSideSetup.Reserved_10 = zkSideToSideSetup.Reserved_10;
                    clonedZkSideToSideSetup.Reserved_11 = zkSideToSideSetup.Reserved_11;
                    clonedZkSideToSideSetup.Reserved_12 = zkSideToSideSetup.Reserved_12;
                    clonedZkSideToSideSetup.Reserved_13 = zkSideToSideSetup.Reserved_13;
                    clonedZkSideToSideSetup.Reserved_14 = zkSideToSideSetup.Reserved_14;
                    clonedZkSideToSideSetup.Reserved_15 = zkSideToSideSetup.Reserved_15;
                    clonedZkSideToSideSetup.Reserved_16 = zkSideToSideSetup.Reserved_16;
                    clonedZkSideToSideSetup.Reserved_17 = zkSideToSideSetup.Reserved_17;
                    clonedZkSideToSideSetup.Reserved_18 = zkSideToSideSetup.Reserved_18;
                    clonedZkSideToSideSetup.Reserved_19 = zkSideToSideSetup.Reserved_19;
                    clonedZkSideToSideSetup.Reserved_20 = zkSideToSideSetup.Reserved_20;
                    clonedZkSideToSideSetup.Reserved_21 = zkSideToSideSetup.Reserved_21;
                    clonedZkSideToSideSetup.Reserved_22 = zkSideToSideSetup.Reserved_22;
                    clonedZkSideToSideSetup.Reserved_23 = zkSideToSideSetup.Reserved_23;
                    clonedZkSideToSideSetup.Reserved_24 = zkSideToSideSetup.Reserved_24;
                    clonedZkSideToSideSetup.Reserved_25 = zkSideToSideSetup.Reserved_25;
                    clonedZkSideToSideSetup.Reserved_26 = zkSideToSideSetup.Reserved_26;
                    clonedZkSideToSideSetup.Reserved_27 = zkSideToSideSetup.Reserved_27;
                    clonedZkSideToSideSetup.Reserved_28 = zkSideToSideSetup.Reserved_28;
                    clonedZkSideToSideSetup.Reserved_29 = zkSideToSideSetup.Reserved_29;
                    clonedZkSideToSideSetup.Reserved_30 = zkSideToSideSetup.Reserved_30;
                    clonedZkSideToSideSetup.Reserved_31 = zkSideToSideSetup.Reserved_31;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CloneOneZkSideToSideSetupDbObject(BHM_Config_ZkSideToSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedZkSideToSideSetup;
        }

        //        /// <summary>
        //        /// Gets a list of all BHM_Config_ZkSideToSideSetup objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static List<BHM_Config_ZkSideToSideSetup> BHM_Config_GetAllZkSideToSideSetupTableEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            List<BHM_Config_ZkSideToSideSetup> zkSideToSideSetupList = null;
        //            try
        //            {
        //                var queryList =
        //                    from entry in db.BHM_Config_ZkSideToSideSetup
        //                    where entry.MonitorID == monitorID
        //                    orderby entry.DateAdded descending
        //                    select entry;

        //                zkSideToSideSetupList = queryList.ToList();
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_GetAllZkSideToSideSetupTableEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return zkSideToSideSetupList;
        //        }

        //        /// <summary>
        //        /// Deletes all BHM_Config_ZkSideToSideSetup objects associated with a given Monitor
        //        /// </summary>
        //        /// <param name="monitorID"></param>
        //        /// <param name="db"></param>
        //        /// <returns></returns>
        //        public static bool BHM_Config_DeleteAllZkSideToSideSetupEntries(Guid monitorID, MonitorInterfaceDB db)
        //        {
        //            bool success = false;
        //            try
        //            {
        //                List<BHM_Config_ZkSideToSideSetup> zkSideToSideSetupList = BHM_Config_GetAllZkSideToSideSetupTableEntries(monitorID, db);
        //                if (zkSideToSideSetupList.Count > 0)
        //                {
        //                    db.BHM_Config_ZkSideToSideSetup.DeleteAllOnSubmit(zkSideToSideSetupList);
        //                    db.SubmitChanges();
        //                    success = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllZkSideToSideSetupEntries(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return success;
        //        }

        /// <summary>
        /// Delete one BHM_Config_ZkSideToSideSetup object from the database
        /// </summary>
        /// <param name="zkSideToSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteZkSideToSideSetupTableEntry(BHM_Config_ZkSideToSideSetup zkSideToSideSetup, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (zkSideToSideSetup != null)
                {
                    db.BHM_Config_ZkSideToSideSetup.DeleteOnSubmit(zkSideToSideSetup);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteZkSideToSideSetupTableEntry(BHM_Config_ZkSideToSideSetup, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Delete one BHM_Config_ZkSideToSideSetup object from the database
        /// </summary>
        /// <param name="zkSideToSideSetup"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteZkSideToSideSetupTableEntry(Guid zkSideToSideSetupID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Config_ZkSideToSideSetup zkSideToSideSetup = BHM_Config_GetZkSideToSideSetupTableEntry(zkSideToSideSetupID, db);
                BHM_Config_DeleteZkSideToSideSetupTableEntry(zkSideToSideSetup, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteZkSideToSideSetupTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        /// <summary>
        /// Deletes all BHM_Config_ZkSideToSideSetup table entries, probably only useful for testing, and maybe not even then
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool BHM_Config_DeleteAllZkSideToSideSetupEntriesForAllMonitors(MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                var queryList =
                    from entry in db.BHM_Config_ZkSideToSideSetup
                    select entry;

                db.BHM_Config_ZkSideToSideSetup.DeleteAllOnSubmit(queryList);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_DeleteAllZkSideToSideSetupEntriesForAllMonitors(MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(configurationRootID, sourceDB, configurationRootID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_ZkSideToSideSetup destinationZkSideToSideSetup;
                BHM_Config_ZkSideToSideSetup sourceZkSideToSideSetup = BHM_Config_GetZkSideToSideSetupTableEntryForOneConfiguration(sourceConfigurationRootID, sourceDB);
                if (sourceZkSideToSideSetup != null)
                {
                    destinationZkSideToSideSetup = BHM_Config_CloneOneZkSideToSideSetupDbObject(sourceZkSideToSideSetup);
                    destinationZkSideToSideSetup.ID = Guid.NewGuid();
                    destinationZkSideToSideSetup.ConfigurationRootID = destinationConfigurationRootID;
                    success = BHM_Config_WriteZkSideToSideSetupTableEntry(destinationZkSideToSideSetup, destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to write the BHM_Config_ZkSideToSideSetup to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nFailed to read the BHM_Config_ZkSideToSideSetup from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(Guid, MonitorInterfaceDB, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        public static bool BHM_Config_CopyOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationConfigurationRootID, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyConfigurationRootForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationMonitorID, destinationDB);
                if (success)
                {
                    success = BHM_Config_CopyCalibrationCoeffForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyGammaSetupInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyGammaSideSetupForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyInitialParametersForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyInitialZkParametersForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyMeasurementsInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopySetupInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopySideToSideDataForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyTrSideParamForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyZkSetupInfoForOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
                if (success)
                {
                    success = BHM_Config_CopyZkSideToSideSetupForOneConfiguratioToNewDatabase(sourceConfigurationRootID, sourceDB, destinationConfigurationRootID, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, Guid, Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool BHM_Config_CopyOneConfigurationToNewDatabase(Guid sourceConfigurationRootID, MonitorInterfaceDB sourceDB, Guid destinationMonitorID, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Config_CopyOneConfigurationToNewDatabase(sourceConfigurationRootID, sourceDB, sourceConfigurationRootID, destinationMonitorID, destinationDB);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Config_CopyOneConfigurationToNewDatabase(Guid configurationRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Config_ConfigurationRoot configurationRoot = BHM_Config_GetConfigurationRootTableEntry(configurationRootID, sourceDB);
                if (configurationRoot != null)
                {
                    success = BHM_Config_CopyOneConfigurationToNewDatabase(configurationRootID, sourceDB, configurationRootID, configurationRoot.MonitorID, destinationDB);
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nCould not find the configuration in the sourceDB";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Config_CopyOneConfigurationToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region DataRoot

        public static bool BHM_Data_WriteDataRootTableEntry(BHM_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.BHM_Data_DataRoot.InsertOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteDataRootTableEntry(BHM_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_WriteDataRootTableEntries(List<BHM_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.BHM_Data_DataRoot.InsertAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteDataRootTableEntries(List<BHM_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Data_DataRoot BHM_Data_GetDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            BHM_Data_DataRoot dataRoot = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_DataRoot
                    where item.ID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        public static BHM_Data_DataRoot BHM_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            BHM_Data_DataRoot dataRoot = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime descending
                    select item;

                if (queryList.Count() > 0)
                {
                    dataRoot = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetMostRecentDataRootTableEntryForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRoot;
        }

        /// <summary>
        /// Gets every BHM_Data_DataRoot table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_DataRoot> BHM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_Data_DataRoot> dataRootList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_DataRoot
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }



        public static List<BHM_Data_DataRoot> BHM_Data_CloneDataRootDbObjectList(List<BHM_Data_DataRoot> dataRootList)
        {
            List<BHM_Data_DataRoot> clonedDataRootList = null;
            try
            {
                BHM_Data_DataRoot dataRootEntry;
                if (dataRootList != null)
                {
                    clonedDataRootList = new List<BHM_Data_DataRoot>();
                    foreach (BHM_Data_DataRoot entry in dataRootList)
                    {
                        dataRootEntry = BHM_Data_CloneOneDataRootDbObject(entry);
                        if (dataRootEntry != null)
                        {
                            clonedDataRootList.Add(dataRootEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneDataRootDbObjectList(List<BHM_Data_DataRoot>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRootList;
        }

        public static BHM_Data_DataRoot BHM_Data_CloneOneDataRootDbObject(BHM_Data_DataRoot dataRoot)
        {
            BHM_Data_DataRoot clonedDataRoot = null;
            try
            {
                if (dataRoot != null)
                {
                    clonedDataRoot = new BHM_Data_DataRoot();
                    clonedDataRoot.ID = dataRoot.ID;
                    clonedDataRoot.MonitorID = dataRoot.MonitorID;
                    clonedDataRoot.ReadingDateTime = dataRoot.ReadingDateTime;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneOneDataRootDbObject(BHM_Data_DataRoot)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedDataRoot;
        }

        /// <summary>
        /// Gets every BHM_Data_DataRoot table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_DataRoot> BHM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<BHM_Data_DataRoot> dataRootList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_DataRoot
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                dataRootList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return dataRootList;
        }

        public static bool BHM_Data_DeleteDataRootTableEntry(BHM_Data_DataRoot dataRoot, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (dataRoot != null)
                {
                    db.BHM_Data_DataRoot.DeleteOnSubmit(dataRoot);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntry(BHM_Data_DataRoot, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_DeleteDataRootTableEntries(List<BHM_Data_DataRoot> dataRootList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.BHM_Data_DataRoot.DeleteAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntries(List<BHM_Data_DataRoot>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_DeleteDataRootTableEntry(Guid dataRootID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                BHM_Data_DataRoot dataRoot = BHM_DatabaseMethods.BHM_Data_GetDataRootTableEntry(dataRootID, db);
                success = BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntry(dataRoot, db);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_DeleteDataRootTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        public static bool BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                List<BHM_Data_DataRoot> dataRootList = BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, db);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    db.BHM_Data_DataRoot.DeleteAllOnSubmit(dataRootList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_CopyDataRootForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Data_DataRoot dataRoot = BHM_Data_GetDataRootTableEntry(dataRootID, sourceDB);
                if (dataRoot != null)
                {
                    success = BHM_Data_WriteDataRootTableEntry(BHM_Data_CloneOneDataRootDbObject(dataRoot), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nFailed to write the BHM_Data_DataRoot to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nFailed to read the BHM_Data_DataRoot from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_DeleteAllDataRootTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion // data root

        #region InsulationParameters

        public static bool BHM_Data_WriteInsulationParametersTableEntry(BHM_Data_InsulationParameters insulationParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (insulationParameters != null)
                {
                    db.BHM_Data_InsulationParameters.InsertOnSubmit(insulationParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteInsulationParametersTableEntry(BHM_Data_InsulationParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_WriteInsulationParametersTableEntries(List<BHM_Data_InsulationParameters> insulationParametersList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((insulationParametersList != null) && (insulationParametersList.Count > 0))
                {
                    db.BHM_Data_InsulationParameters.InsertAllOnSubmit(insulationParametersList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteInsulationParametersTableEntries(List<BHM_Data_InsulationParameters>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Data_InsulationParameters BHM_Data_GetInsulationParametersTableEntry(Guid insulationParametersID, MonitorInterfaceDB db)
        {
            BHM_Data_InsulationParameters insulationParameters = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_InsulationParameters
                    where item.ID == insulationParametersID
                    select item;

                if (queryList.Count() > 0)
                {
                    insulationParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetInsulationParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return insulationParameters;
        }

        public static BHM_Data_InsulationParameters BHM_Data_GetInsulationParametersTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            BHM_Data_InsulationParameters insulationParameters = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_InsulationParameters
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    insulationParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetInsulationParametersTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return insulationParameters;
        }

        public static BHM_Data_InsulationParameters BHM_Data_CloneOneInsulationParametersDbObject(BHM_Data_InsulationParameters insulationParameters)
        {
            BHM_Data_InsulationParameters clonedInsulationParameters = null;
            try
            {
                if (insulationParameters != null)
                {
                    clonedInsulationParameters = new BHM_Data_InsulationParameters();

                    clonedInsulationParameters.ID = insulationParameters.ID;
                    clonedInsulationParameters.DataRootID = insulationParameters.DataRootID;
                    clonedInsulationParameters.MonitorID = insulationParameters.MonitorID;
                    clonedInsulationParameters.ReadingDateTime = insulationParameters.ReadingDateTime;

                    clonedInsulationParameters.TrSideCount = insulationParameters.TrSideCount;
                    clonedInsulationParameters.RPN_0 = insulationParameters.RPN_0;
                    clonedInsulationParameters.RPN_1 = insulationParameters.RPN_1;
                    clonedInsulationParameters.Current_0 = insulationParameters.Current_0;
                    clonedInsulationParameters.Current_1 = insulationParameters.Current_1;
                    clonedInsulationParameters.Temperature_0 = insulationParameters.Temperature_0;
                    clonedInsulationParameters.Temperature_1 = insulationParameters.Temperature_1;
                    clonedInsulationParameters.Temperature_2 = insulationParameters.Temperature_2;
                    clonedInsulationParameters.Temperature_3 = insulationParameters.Temperature_3;
                    clonedInsulationParameters.CurrentR_0 = insulationParameters.CurrentR_0;
                    clonedInsulationParameters.CurrentR_1 = insulationParameters.CurrentR_1;
                    clonedInsulationParameters.Humidity = insulationParameters.Humidity;
                    clonedInsulationParameters.CalcZk = insulationParameters.CalcZk;
                    clonedInsulationParameters.Reserved_0 = insulationParameters.Reserved_0;
                    clonedInsulationParameters.Reserved_1 = insulationParameters.Reserved_1;
                    clonedInsulationParameters.Reserved_2 = insulationParameters.Reserved_2;
                    clonedInsulationParameters.Reserved_3 = insulationParameters.Reserved_3;
                    clonedInsulationParameters.Reserved_4 = insulationParameters.Reserved_4;
                    clonedInsulationParameters.Reserved_5 = insulationParameters.Reserved_5;
                    clonedInsulationParameters.Reserved_6 = insulationParameters.Reserved_6;
                    clonedInsulationParameters.Reserved_7 = insulationParameters.Reserved_7;
                    clonedInsulationParameters.Reserved_8 = insulationParameters.Reserved_8;
                    clonedInsulationParameters.Reserved_9 = insulationParameters.Reserved_9;
                    clonedInsulationParameters.Reserved_10 = insulationParameters.Reserved_10;
                    clonedInsulationParameters.Reserved_11 = insulationParameters.Reserved_11;
                    clonedInsulationParameters.Reserved_12 = insulationParameters.Reserved_12;
                    clonedInsulationParameters.Reserved_13 = insulationParameters.Reserved_13;
                    clonedInsulationParameters.Reserved_14 = insulationParameters.Reserved_14;
                    clonedInsulationParameters.Reserved_15 = insulationParameters.Reserved_15;
                    clonedInsulationParameters.Reserved_16 = insulationParameters.Reserved_16;
                    clonedInsulationParameters.Reserved_17 = insulationParameters.Reserved_17;
                    clonedInsulationParameters.Reserved_18 = insulationParameters.Reserved_18;
                    clonedInsulationParameters.Reserved_19 = insulationParameters.Reserved_19;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneOneInsulationParametersDbObject(BHM_Data_InsulationParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInsulationParameters;
        }

        /// <summary>
        /// Gets every BHM_Data_InsulationParameters table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_InsulationParameters> BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_Data_InsulationParameters> insulationParametersList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_InsulationParameters
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending
                    select item;

                insulationParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return insulationParametersList;
        }

        /// <summary>
        /// Gets every BHM_Data_InsulationParameters table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_InsulationParameters> BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<BHM_Data_InsulationParameters> insulationParametersList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_InsulationParameters
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending
                    select item;

                insulationParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return insulationParametersList;
        }

        public static List<BHM_Data_InsulationParameters> BHM_Data_CloneInsulationParametersDbObjectList(List<BHM_Data_InsulationParameters> insulationParametersList)
        {
            List<BHM_Data_InsulationParameters> clonedInsulationParametersList = null;
            try
            {
                BHM_Data_InsulationParameters insulationParametersEntry;
                if (insulationParametersList != null)
                {
                    clonedInsulationParametersList = new List<BHM_Data_InsulationParameters>();
                    foreach (BHM_Data_InsulationParameters entry in insulationParametersList)
                    {
                        insulationParametersEntry = BHM_Data_CloneOneInsulationParametersDbObject(entry);
                        if (insulationParametersEntry != null)
                        {
                            clonedInsulationParametersList.Add(insulationParametersEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneInsulationParametersDbObjectList(List<BHM_Data_InsulationParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedInsulationParametersList;
        }

        public static bool BHM_Data_CopyInsulationParametersForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Data_InsulationParameters insulationParameters = BHM_Data_GetInsulationParametersTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (insulationParameters != null)
                {
                    success = BHM_Data_WriteInsulationParametersTableEntry(BHM_Data_CloneOneInsulationParametersDbObject(insulationParameters), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopyInsulationParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the BHM_Data_InsulationParameters to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopyInsulationParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the BHM_Data_InsulationParameters from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CopyInsulationParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion

        #region SideToSideData

        public static bool BHM_Data_WriteSideToSideDataTableEntry(BHM_Data_SideToSideData sideToSideData, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (sideToSideData != null)
                {
                    db.BHM_Data_SideToSideData.InsertOnSubmit(sideToSideData);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteSideToSideDataTableEntry(BHM_Data_SideToSideData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_WriteSideToSideDataTableEntries(List<BHM_Data_SideToSideData> sideToSideDataList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((sideToSideDataList != null) && (sideToSideDataList.Count > 0))
                {
                    db.BHM_Data_SideToSideData.InsertAllOnSubmit(sideToSideDataList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteSideToSideDataTableEntry(BHM_Data_SideToSideData, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Data_SideToSideData BHM_Data_GetSideToSideDataTableEntry(Guid sideToSideDataID, MonitorInterfaceDB db)
        {
            BHM_Data_SideToSideData sideToSideData = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_SideToSideData
                    where item.ID == sideToSideDataID
                    select item;

                if (queryList.Count() > 0)
                {
                    sideToSideData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetSideToSideDataTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }


        public static BHM_Data_SideToSideData BHM_Data_GetSideToSideDataTableEntryForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            BHM_Data_SideToSideData sideToSideData = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_SideToSideData
                    where item.DataRootID == dataRootID
                    select item;

                if (queryList.Count() > 0)
                {
                    sideToSideData = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetSideToSideDataTableEntryForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }

        public static BHM_Data_SideToSideData BHM_Data_CloneOneSideToSideDataDbObject(BHM_Data_SideToSideData sideToSideData)
        {
            BHM_Data_SideToSideData clonedSideToSideData = null;
            try
            {
                if (sideToSideData != null)
                {
                    clonedSideToSideData = new BHM_Data_SideToSideData();

                    clonedSideToSideData.ID = sideToSideData.ID;
                    clonedSideToSideData.DataRootID = sideToSideData.DataRootID;
                    clonedSideToSideData.MonitorID = sideToSideData.MonitorID;
                    clonedSideToSideData.ReadingDateTime = sideToSideData.ReadingDateTime;

                    clonedSideToSideData.SideNumber = sideToSideData.SideNumber;
                    clonedSideToSideData.Amplitude1_0 = sideToSideData.Amplitude1_0;
                    clonedSideToSideData.Amplitude1_1 = sideToSideData.Amplitude1_1;
                    clonedSideToSideData.Amplitude1_2 = sideToSideData.Amplitude1_2;
                    clonedSideToSideData.Amplitude2_0 = sideToSideData.Amplitude2_0;
                    clonedSideToSideData.Amplitude2_1 = sideToSideData.Amplitude2_1;
                    clonedSideToSideData.Amplitude2_2 = sideToSideData.Amplitude2_2;
                    clonedSideToSideData.PhaseShift_0 = sideToSideData.PhaseShift_0;
                    clonedSideToSideData.PhaseShift_1 = sideToSideData.PhaseShift_1;
                    clonedSideToSideData.PhaseShift_2 = sideToSideData.PhaseShift_2;
                    clonedSideToSideData.CurrentAmpl_0 = sideToSideData.CurrentAmpl_0;
                    clonedSideToSideData.CurrentAmpl_1 = sideToSideData.CurrentAmpl_1;
                    clonedSideToSideData.CurrentAmpl_2 = sideToSideData.CurrentAmpl_2;
                    clonedSideToSideData.CurrentPhase_0 = sideToSideData.CurrentPhase_0;
                    clonedSideToSideData.CurrentPhase_1 = sideToSideData.CurrentPhase_1;
                    clonedSideToSideData.CurrentPhase_2 = sideToSideData.CurrentPhase_2;
                    clonedSideToSideData.ZkAmpl_0 = sideToSideData.ZkAmpl_0;
                    clonedSideToSideData.ZkAmpl_1 = sideToSideData.ZkAmpl_1;
                    clonedSideToSideData.ZkAmpl_2 = sideToSideData.ZkAmpl_1;
                    clonedSideToSideData.ZkPhase_0 = sideToSideData.ZkPhase_0;
                    clonedSideToSideData.ZkPhase_1 = sideToSideData.ZkPhase_1;
                    clonedSideToSideData.ZkPhase_2 = sideToSideData.ZkPhase_2;
                    clonedSideToSideData.CurrentChannelShift = sideToSideData.CurrentChannelShift;
                    clonedSideToSideData.ZkPercent_0 = sideToSideData.ZkPercent_0;
                    clonedSideToSideData.ZkPercent_1 = sideToSideData.ZkPercent_1;
                    clonedSideToSideData.ZkPercent_2 = sideToSideData.ZkPercent_2;
                    clonedSideToSideData.Reserved_0 = sideToSideData.Reserved_0;
                    clonedSideToSideData.Reserved_1 = sideToSideData.Reserved_1;
                    clonedSideToSideData.Reserved_2 = sideToSideData.Reserved_2;
                    clonedSideToSideData.Reserved_3 = sideToSideData.Reserved_3;
                    clonedSideToSideData.Reserved_4 = sideToSideData.Reserved_4;
                    clonedSideToSideData.Reserved_5 = sideToSideData.Reserved_5;
                    clonedSideToSideData.Reserved_6 = sideToSideData.Reserved_6;
                    clonedSideToSideData.Reserved_7 = sideToSideData.Reserved_7;
                    clonedSideToSideData.Reserved_8 = sideToSideData.Reserved_8;
                    clonedSideToSideData.Reserved_9 = sideToSideData.Reserved_9;
                    clonedSideToSideData.Reserved_10 = sideToSideData.Reserved_10;
                    clonedSideToSideData.Reserved_11 = sideToSideData.Reserved_11;
                    clonedSideToSideData.Reserved_12 = sideToSideData.Reserved_12;
                    clonedSideToSideData.Reserved_13 = sideToSideData.Reserved_13;
                    clonedSideToSideData.Reserved_14 = sideToSideData.Reserved_14;
                    clonedSideToSideData.Reserved_15 = sideToSideData.Reserved_15;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneOneSideToSideDataDbObject(BHM_Data_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedSideToSideData;
        }

        /// <summary>
        /// Gets every BHM_Data_SideToSideData table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_SideToSideData> BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_Data_SideToSideData> sideToSideDataList = null;
            try
            {
                var queryList =
                   from item in db.BHM_Data_SideToSideData
                   where item.MonitorID == monitorID
                   orderby item.ReadingDateTime ascending
                   select item;

                sideToSideDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideDataList;
        }

        /// <summary>
        /// Gets every BHM_Data_SideToSideData table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_SideToSideData> BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<BHM_Data_SideToSideData> sideToSideDataList = null;
            try
            {
                var queryList =
                   from item in db.BHM_Data_SideToSideData
                   where item.MonitorID == monitorID
                   & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                   orderby item.ReadingDateTime ascending
                   select item;

                sideToSideDataList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideDataList;
        }

        public static List<BHM_Data_SideToSideData> BHM_Data_CloneSideToSideDataList(List<BHM_Data_SideToSideData> sideToSideDataList)
        {
            List<BHM_Data_SideToSideData> clonedSideToSideDataList = null;
            try
            {
                BHM_Data_SideToSideData sideToSideDataEntry;
                if (sideToSideDataList != null)
                {
                    clonedSideToSideDataList = new List<BHM_Data_SideToSideData>();
                    foreach (BHM_Data_SideToSideData entry in sideToSideDataList)
                    {
                        sideToSideDataEntry = BHM_Data_CloneOneSideToSideDataDbObject(entry);
                        if (entry != null)
                        {
                            clonedSideToSideDataList.Add(sideToSideDataEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneSideToSideDataList(List<BHM_Data_SideToSideData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedSideToSideDataList;
        }

        public static bool BHM_Data_CopySideToSideDataForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                BHM_Data_SideToSideData sideToSideData = BHM_Data_GetSideToSideDataTableEntryForOneDataRoot(dataRootID, sourceDB);
                if (sideToSideData != null)
                {
                    success = BHM_Data_WriteSideToSideDataTableEntry(BHM_Data_CloneOneSideToSideDataDbObject(sideToSideData), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopySideToSideDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the BHM_Data_SideToSideData to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    //                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopySideToSideDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the BHM_Data_SideToSideData from the source database.";
                    //                    LogMessage.LogError(errorMessage); 
                    //#if DEBUG
                    //                    MessageBox.Show(errorMessage);
                    //#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CopySideToSideDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion // Side to Side data

        #region TransformerSideParameters

        public static bool BHM_Data_WriteTransformerSideParametersTableEntry(BHM_Data_TransformerSideParameters transformerSideParameters, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if (transformerSideParameters != null)
                {
                    db.BHM_Data_TransformerSideParameters.InsertOnSubmit(transformerSideParameters);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteTransformerSideParametersTableEntry(BHM_Data_TransformerSideParameters, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static bool BHM_Data_WriteTransformerSideParametersTableEntries(List<BHM_Data_TransformerSideParameters> transformerSideParametersList, MonitorInterfaceDB db)
        {
            bool success = false;
            try
            {
                if ((transformerSideParametersList != null) && (transformerSideParametersList.Count > 0))
                {
                    db.BHM_Data_TransformerSideParameters.InsertAllOnSubmit(transformerSideParametersList);
                    db.SubmitChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_WriteTransformerSideParametersTableEntries(List<BHM_Data_TransformerSideParameters>, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static BHM_Data_TransformerSideParameters BHM_Data_GetTransformerSideParametersTableEntry(Guid transformerSideParametersID, MonitorInterfaceDB db)
        {
            BHM_Data_TransformerSideParameters transformerSideParameters = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_TransformerSideParameters
                    where item.ID == transformerSideParametersID
                    select item;

                if (queryList.Count() > 0)
                {
                    transformerSideParameters = queryList.First();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetTransformerSideParametersTableEntry(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transformerSideParameters;
        }

        public static BHM_Data_TransformerSideParameters BHM_Data_CloneOneTransformerSideParametersDbObject(BHM_Data_TransformerSideParameters transformerSideParameters)
        {
            BHM_Data_TransformerSideParameters clonedTransformerSideParameters = null;
            try
            {
                if (transformerSideParameters != null)
                {
                    clonedTransformerSideParameters = new BHM_Data_TransformerSideParameters();

                    clonedTransformerSideParameters.ID = transformerSideParameters.ID;
                    clonedTransformerSideParameters.DataRootID = transformerSideParameters.DataRootID;
                    clonedTransformerSideParameters.MonitorID = transformerSideParameters.MonitorID;
                    clonedTransformerSideParameters.ReadingDateTime = transformerSideParameters.ReadingDateTime;

                    clonedTransformerSideParameters.SideNumber = transformerSideParameters.SideNumber;
                    clonedTransformerSideParameters.SourceAmplitude_0 = transformerSideParameters.SourceAmplitude_0;
                    clonedTransformerSideParameters.SourceAmplitude_1 = transformerSideParameters.SourceAmplitude_1;
                    clonedTransformerSideParameters.SourceAmplitude_2 = transformerSideParameters.SourceAmplitude_2;
                    clonedTransformerSideParameters.PhaseAmplitude_0 = transformerSideParameters.PhaseAmplitude_0;
                    clonedTransformerSideParameters.PhaseAmplitude_1 = transformerSideParameters.PhaseAmplitude_1;
                    clonedTransformerSideParameters.PhaseAmplitude_2 = transformerSideParameters.PhaseAmplitude_2;
                    clonedTransformerSideParameters.Gamma = transformerSideParameters.Gamma;
                    clonedTransformerSideParameters.GammaPhase = transformerSideParameters.GammaPhase;
                    clonedTransformerSideParameters.KT = transformerSideParameters.KT;
                    clonedTransformerSideParameters.AlarmStatus = transformerSideParameters.AlarmStatus;
                    clonedTransformerSideParameters.RemainingLife_0 = transformerSideParameters.RemainingLife_0;
                    clonedTransformerSideParameters.RemainingLife_1 = transformerSideParameters.RemainingLife_1;
                    clonedTransformerSideParameters.RemainingLife_2 = transformerSideParameters.RemainingLife_2;
                    clonedTransformerSideParameters.DefectCode = transformerSideParameters.DefectCode;
                    clonedTransformerSideParameters.ChPhaseShift = transformerSideParameters.ChPhaseShift;
                    clonedTransformerSideParameters.Trend = transformerSideParameters.Trend;
                    clonedTransformerSideParameters.KTPhase = transformerSideParameters.KTPhase;
                    clonedTransformerSideParameters.SignalPhase_0 = transformerSideParameters.SignalPhase_0;
                    clonedTransformerSideParameters.SignalPhase_1 = transformerSideParameters.SignalPhase_1;
                    clonedTransformerSideParameters.SignalPhase_2 = transformerSideParameters.SignalPhase_2;
                    clonedTransformerSideParameters.SourcePhase_0 = transformerSideParameters.SourcePhase_0;
                    clonedTransformerSideParameters.SourcePhase_1 = transformerSideParameters.SourcePhase_1;
                    clonedTransformerSideParameters.SourcePhase_2 = transformerSideParameters.SourcePhase_2;
                    clonedTransformerSideParameters.Frequency = transformerSideParameters.Frequency;
                    clonedTransformerSideParameters.Temperature = transformerSideParameters.Temperature;
                    clonedTransformerSideParameters.C_0 = transformerSideParameters.C_0;
                    clonedTransformerSideParameters.C_1 = transformerSideParameters.C_1;
                    clonedTransformerSideParameters.C_2 = transformerSideParameters.C_2;
                    clonedTransformerSideParameters.Tg_0 = transformerSideParameters.Tg_0;
                    clonedTransformerSideParameters.Tg_1 = transformerSideParameters.Tg_1;
                    clonedTransformerSideParameters.Tg_2 = transformerSideParameters.Tg_2;
                    clonedTransformerSideParameters.ExternalSyncShift = transformerSideParameters.ExternalSyncShift;
                    clonedTransformerSideParameters.Reserved_0 = transformerSideParameters.Reserved_0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneOneTransformerSideParametersDbObject(BHM_Data_TransformerSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTransformerSideParameters;
        }

        public static List<BHM_Data_TransformerSideParameters> BHM_Data_GetAllTransformerSideParametersTableEntriesForOneDataRoot(Guid dataRootID, MonitorInterfaceDB db)
        {
            List<BHM_Data_TransformerSideParameters> transformerSideParametersList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_TransformerSideParameters
                    where item.DataRootID == dataRootID
                    orderby item.SideNumber ascending
                    select item;

                transformerSideParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneDataRoot(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transformerSideParametersList;
        }

        /// <summary>
        /// Gets every BHM_Data_TransformerSideParameters table entry for the specified monitor
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_TransformerSideParameters> BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        {
            List<BHM_Data_TransformerSideParameters> transformerSideParametersList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_TransformerSideParameters
                    where item.MonitorID == monitorID
                    orderby item.ReadingDateTime ascending, item.SideNumber ascending
                    select item;

                transformerSideParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(Guid, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transformerSideParametersList;
        }

        /// <summary>
        /// Gets every BHM_Data_TransformerSideParameters table entry for the specified monitor that has a ReadingDateTime after the specified minimumDateTime
        /// </summary>
        /// <param name="monitorID"></param>
        /// <param name="minimumDateTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<BHM_Data_TransformerSideParameters> BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        {
            List<BHM_Data_TransformerSideParameters> transformerSideParametersList = null;
            try
            {
                var queryList =
                    from item in db.BHM_Data_TransformerSideParameters
                    where item.MonitorID == monitorID
                    & item.ReadingDateTime.CompareTo(minimumDateTime) >= 0
                    orderby item.ReadingDateTime ascending, item.SideNumber ascending
                    select item;

                transformerSideParametersList = queryList.ToList();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(Guid, DateTime, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transformerSideParametersList;
        }

        public static List<BHM_Data_TransformerSideParameters> BHM_Data_CloneTransformerSideParametersDbObjectList(List<BHM_Data_TransformerSideParameters> transformerSideParametersList)
        {
            List<BHM_Data_TransformerSideParameters> clonedTransformerSideParametersList = null;
            try
            {
                BHM_Data_TransformerSideParameters transformerSideParametersEntry;
                if (transformerSideParametersList != null)
                {
                    clonedTransformerSideParametersList = new List<BHM_Data_TransformerSideParameters>();
                    foreach (BHM_Data_TransformerSideParameters entry in transformerSideParametersList)
                    {
                        transformerSideParametersEntry = BHM_Data_CloneOneTransformerSideParametersDbObject(entry);
                        if (transformerSideParametersEntry != null)
                        {
                            clonedTransformerSideParametersList.Add(transformerSideParametersEntry);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CloneTransformerSideParametersDbObjectList(List<BHM_Data_TransformerSideParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return clonedTransformerSideParametersList;
        }

        public static bool BHM_Data_CopyTransformerSideParametersForOneDataItemToNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                List<BHM_Data_TransformerSideParameters> transformerSideParametersList = BHM_Data_GetAllTransformerSideParametersTableEntriesForOneDataRoot(dataRootID, sourceDB);
                if ((transformerSideParametersList != null) && (transformerSideParametersList.Count == 2))
                {
                    success = BHM_Data_WriteTransformerSideParametersTableEntries(BHM_Data_CloneTransformerSideParametersDbObjectList(transformerSideParametersList), destinationDB);
                    if (!success)
                    {
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopyTransformerSideParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to write the List<BHM_Data_TransformerSideParameters> to the destination database.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_Data_CopyTransformerSideParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to read the List<BHM_Data_TransformerSideParameters> from the source database.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CopyTransformerSideParametersForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        #endregion  // transformer side parameters

        public static bool BHM_Data_CopyOneDataReadingToTheNewDatabase(Guid dataRootID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            bool success = false;
            try
            {
                success = BHM_Data_CopyDataRootForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                if (success)
                {
                    success = BHM_Data_CopyInsulationParametersForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
                //if (success)
                //{
                //    success = BHM_Data_CopySideToSideDataForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                //}
                if (success)
                {
                    success = BHM_Data_CopyTransformerSideParametersForOneDataItemToNewDatabase(dataRootID, sourceDB, destinationDB);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CopyOneDataReadingToTheNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public static ErrorCode BHM_Data_CopyAllDataForOneMonitorToNewDatabase(Guid monitorID, MonitorInterfaceDB sourceDB, MonitorInterfaceDB destinationDB)
        {
            ErrorCode errorCode = ErrorCode.None;
            try
            {
                List<BHM_Data_DataRoot> dataRootList;
                List<BHM_Data_InsulationParameters> insulationParametersList;
                List<BHM_Data_TransformerSideParameters> transformerSideParametersList;

                List<BHM_Data_DataRoot> clonedDataRootList;
                List<BHM_Data_InsulationParameters> clonedInsulationParametersList = null;
                List<BHM_Data_TransformerSideParameters> clonedTransformerSideParametersList = null;

                dataRootList = BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, sourceDB);
                if ((dataRootList != null) && (dataRootList.Count > 0))
                {
                    clonedDataRootList = BHM_Data_CloneDataRootDbObjectList(dataRootList);
                    if ((clonedDataRootList != null) && (clonedDataRootList.Count > 0))
                    {
                        insulationParametersList = BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(monitorID, sourceDB);
                        transformerSideParametersList = BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(monitorID, sourceDB);
                        if ((insulationParametersList != null) && (insulationParametersList.Count > 0))
                        {
                            if ((transformerSideParametersList != null) && (insulationParametersList.Count > 0))
                            {
                                clonedInsulationParametersList = BHM_Data_CloneInsulationParametersDbObjectList(insulationParametersList);
                                if ((clonedInsulationParametersList != null) && (clonedInsulationParametersList.Count > 0))
                                {
                                    clonedTransformerSideParametersList = BHM_Data_CloneTransformerSideParametersDbObjectList(transformerSideParametersList);
                                    if ((clonedTransformerSideParametersList != null) && (clonedTransformerSideParametersList.Count > 0))
                                    {
                                        errorCode = ErrorCode.DatabaseWriteFailed;
                                        destinationDB.BHM_Data_DataRoot.InsertAllOnSubmit(clonedDataRootList);
                                        destinationDB.SubmitChanges();

                                        destinationDB.BHM_Data_InsulationParameters.InsertAllOnSubmit(clonedInsulationParametersList);
                                        destinationDB.BHM_Data_TransformerSideParameters.InsertAllOnSubmit(clonedTransformerSideParametersList);
                                        destinationDB.SubmitChanges();
                                        errorCode = ErrorCode.DatabaseWriteSucceeded;
                                    }
                                    else
                                    {
                                        errorCode = ErrorCode.NoTransformerSideParametersEntriesFound;
                                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone transformer side parameters";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                                else
                                {
                                    errorCode = ErrorCode.NoInsulationParametersEntriesFound;
                                    string errorMessage = "Error in BHM_DatabaseMethods.BHM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone insulatation parameters";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                errorCode = ErrorCode.NoTransformerSideParametersEntriesFound;
                            }
                        }
                        else
                        {
                            errorCode = ErrorCode.NoInsulationParametersEntriesFound;
                        }

                    }
                    else
                    {
                        errorCode = ErrorCode.NoDataRootEntriesFound;
                        string errorMessage = "Error in BHM_DatabaseMethods.BHM_CopyDataForOneDataItemToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nFailed to clone data root list";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    errorCode = ErrorCode.NoDataRootEntriesFound;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.BHM_Data_CopyAllDataForOneMonitorToNewDatabase(Guid, MonitorInterfaceDB, MonitorInterfaceDB)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorCode;
        }

        public static Guid GetGuidForTemplateConfigurationMonitorID()
        {
            Guid fakeMonitorID = new Guid("15002000000000000000000000000000");
            return fakeMonitorID;
        }

        public static Monitor CreateNewTemplateMonitor()
        {
            Monitor bushingMonitor = null;
            try
            {
                bushingMonitor = new Monitor();
                bushingMonitor.ID = BHM_DatabaseMethods.GetGuidForTemplateConfigurationMonitorID();
                bushingMonitor.EquipmentID = General_DatabaseMethods.GetGuidForTemplateConfigurationEquipmentID();
                bushingMonitor.MonitorType = "BHM";
                bushingMonitor.ConnectionType = String.Empty;
                bushingMonitor.BaudRate = string.Empty;
                bushingMonitor.ModbusAddress = "10";
                bushingMonitor.Enabled = false;
                bushingMonitor.IPaddress = "000.000.000.000";
                bushingMonitor.PortNumber = 502;
                bushingMonitor.MonitorNumber = 1;
                bushingMonitor.DateOfLastDataDownload = General_DatabaseMethods.MinimumDateTime();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_DatabaseMethods.CreateNewTemplateMonitor()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return bushingMonitor;
        }
    }
}
