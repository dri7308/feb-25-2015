﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneralUtilities;
using System.Windows.Forms;

namespace ConfigurationObjects
{
    public class ADM_Configuration
    {
        public List<ADM_ConfigComponent_ChannelInfo> channelInfoList = null;
        public List<ADM_ConfigComponent_MeasurementsInfo> measurementsInfoList = null;
        public ADM_ConfigComponent_PDSetupInfo pdSetupInfo = null;
        public ADM_ConfigComponent_SetupInfo setupInfo = null;



        private static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 560;
        private Byte[] rawByteData;

        public ADM_Configuration(List<ADM_ConfigComponent_ChannelInfo> inputChannelInfoList, List<ADM_ConfigComponent_MeasurementsInfo> inputMeasurementsInfoList,
                                 ADM_ConfigComponent_PDSetupInfo inputPDSetupInfo, ADM_ConfigComponent_SetupInfo inputSetupInfo)
        {
            try
            {
                if (inputChannelInfoList.Count == 4)
                {
                    channelInfoList = inputChannelInfoList;
                    measurementsInfoList = inputMeasurementsInfoList;
                    pdSetupInfo = inputPDSetupInfo;
                    setupInfo = inputSetupInfo;
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ADM_Configuration()\nThe channel info list has the wrong number of items in it.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                if (!AllMembersAreNonNull())
                {
                    SetAllMembersToNull();

                    string errorMessage = "Error in ADM_Configuration.ADM_Configuration()\nFailed to read some entries from the database";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ADM_Configuration(List<ADM_ConfigComponent_ChannelInfo>, List<ADM_ConfigComponent_MeasurementsInfo>, ADM_ConfigComponent_PDSetupInfo, ADM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public ADM_Configuration(Byte[] byteData)
        {
            try
            {
                if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                {
                    rawByteData = byteData;

                    channelInfoList = ExtractChannelInfoDataFromByteData(byteData);
                    measurementsInfoList = ExtractMeasurementsInfoDataFromByteData(byteData);
                    pdSetupInfo = ExtractPDSetupInfoDataFromByteData(byteData);
                    setupInfo = ExtractSetupInfoDataFromByteData(byteData);

                    if (!AllMembersAreNonNull())
                    {
                        SetAllMembersToNull();
                        string errorMessage = "Error in ADM_Configuration.ADM_Configuration(Byte[], Guid)\nFailed to convert the byte data to internal data";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    SetAllMembersToNull();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ADM_Configuration(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Byte[] ConvertConfigurationToByteArray()
        {
            Byte[] byteData = null;
            try
            {
                if (AllMembersAreNonNull())
                {
                    byteData = new Byte[EXPECTED_LENGTH_OF_BYTE_ARRAY];
                    ConvertChannelInfoToBytes(channelInfoList, ref byteData);
                    ConvertMeasurementsInfoToBytes(measurementsInfoList, ref byteData);
                    ConvertPDSetupInfoToBytes(pdSetupInfo, ref byteData);
                    ConvertSetupInfoToBytes(setupInfo, ref byteData);
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertConfigurationToByteArray()\nSome members were null, so you cannot create the byte array";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertConfigurationToByteArray()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteData;
        }

        public bool AllMembersAreNonNull()
        {
            bool allNonNull = true;
            try
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("The following ADM_Configuration members are null: ");

                if (channelInfoList == null)
                {
                    allNonNull = false;
                    errorMessage.Append("channelInfoList ");
                }
                if (measurementsInfoList == null)
                {
                    allNonNull = false;
                    errorMessage.Append("measurementsInfoList ");
                }
                if (pdSetupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("pdSetupInfo ");
                }
                if (setupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("setupInfo ");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.AllMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allNonNull;
        }

        private void SetAllMembersToNull()
        {
            channelInfoList = null;
            measurementsInfoList = null;
            pdSetupInfo = null;
            setupInfo = null;
        }

        public void DeleteAllInternalData()
        {
            SetAllMembersToNull();
        }

        #region Configuration

        #region ChannelInfo

        private List<ADM_ConfigComponent_ChannelInfo> ExtractChannelInfoDataFromByteData(Byte[] byteData)
        {
            List<ADM_ConfigComponent_ChannelInfo> channelInfoList = null;
            try
            {
                int offset = 300;
                ADM_ConfigComponent_ChannelInfo channelInfo;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        channelInfoList = new List<ADM_ConfigComponent_ChannelInfo>();

                        for (int i = 0; i < 4; i++)
                        {
                            channelInfo = new ADM_ConfigComponent_ChannelInfo();

                            channelInfo.ChannelNumber = i;
                            channelInfo.ChannelIsOn = ConversionMethods.ByteToInt32(byteData[offset]);
                            // skipped 3 bytes, probably due to alignment, not used at all by the device
                            channelInfo.ChannelSensitivity = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                            channelInfo.PDMaxWidth = ConversionMethods.ByteToInt32(byteData[offset + 8]);
                            channelInfo.PDIntervalTime = ConversionMethods.ByteToInt32(byteData[offset + 9]);
                            // skipped 2 bytes, probably due to alignment, not used at all by the device
                            channelInfo.P_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            channelInfo.P_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            channelInfo.P_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                            channelInfo.P_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                            channelInfo.P_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                            channelInfo.P_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);

                            channelInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 36]);
                            channelInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 37]);
                            channelInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 38]);
                            channelInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 39]);
                            channelInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 40]);
                            channelInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 41]);
                            channelInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 42]);
                            channelInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 43]);

                            channelInfoList.Add(channelInfo);

                            offset += 44;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ExtractChannelInfoDataFromByteData(Byte[], Guid, DateTime)\nbyteData array does not have the expected minimum number of entries.";
                        LogMessage.LogError(errorMessage);
                        channelInfoList = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ExtractChannelInfoDataFromByteData(Byte[], Guid, DateTime)\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ExtractChannelInfoDataFromByteData(Byte[], Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfoList;
        }

        private void ConvertChannelInfoToBytes(List<ADM_ConfigComponent_ChannelInfo> channelInfoList, ref Byte[] byteData)
        {
            try
            {
                int offset;
                if (channelInfoList != null)
                {
                   
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            foreach (ADM_ConfigComponent_ChannelInfo channelInfo in channelInfoList)
                            {
                                offset = 300 + channelInfo.ChannelNumber * 44;

                                byteData[offset] = ConversionMethods.Int32ToByte(channelInfo.ChannelIsOn);
                                // skip 3 bytes, probably for alignment, not used at all by device
                                Array.Copy(ConversionMethods.SingleToBytes((Single)channelInfo.ChannelSensitivity), 0, byteData, offset + 4, 4);
                                byteData[offset + 8] = ConversionMethods.Int32ToByte(channelInfo.PDMaxWidth);
                                byteData[offset + 9] = ConversionMethods.Int32ToByte(channelInfo.PDIntervalTime);
                                // skip 2 bytes, probably for alignment, not used at all by device
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_0), 0, byteData, offset + 12, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_1), 0, byteData, offset + 16, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_2), 0, byteData, offset + 20, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_3), 0, byteData, offset + 24, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_4), 0, byteData, offset + 28, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_5), 0, byteData, offset + 32, 4);
                                //
                                byteData[offset + 36] = ConversionMethods.Int32ToByte(channelInfo.Reserved_0);
                                byteData[offset + 37] = ConversionMethods.Int32ToByte(channelInfo.Reserved_1);
                                byteData[offset + 38] = ConversionMethods.Int32ToByte(channelInfo.Reserved_2);
                                byteData[offset + 39] = ConversionMethods.Int32ToByte(channelInfo.Reserved_3);
                                byteData[offset + 40] = ConversionMethods.Int32ToByte(channelInfo.Reserved_4);
                                byteData[offset + 41] = ConversionMethods.Int32ToByte(channelInfo.Reserved_5);
                                byteData[offset + 42] = ConversionMethods.Int32ToByte(channelInfo.Reserved_6);
                                byteData[offset + 43] = ConversionMethods.Int32ToByte(channelInfo.Reserved_7);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(List<ADM_Config_ChannelInfo>, ref Byte[])\nbyteData array does not have the expected minimum number of entries.";
                            LogMessage.LogError(errorMessage);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(List<ADM_Config_ChannelInfo>, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(List<ADM_Config_ChannelInfo>, ref Byte[])\nInput List<ADM_ConfigComponent_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertSetupInfoToBytes(List<ADM_Config_ChannelInfo>, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region MeasurementsInfo

        private List<ADM_ConfigComponent_MeasurementsInfo> ExtractMeasurementsInfoDataFromByteData(Byte[] byteData)
        {
            List<ADM_ConfigComponent_MeasurementsInfo> measurementsInfoList = null;
            try
            {
                ADM_ConfigComponent_MeasurementsInfo measurementsInfo;
                int offset = 15;
                int hour;
                int minute;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        measurementsInfoList = new List<ADM_ConfigComponent_MeasurementsInfo>();
                        
                        for (int i = 0; i < 50; i++)
                        {
                            hour = ConversionMethods.ByteToInt32(byteData[offset]);
                            offset++;
                            minute = ConversionMethods.ByteToInt32(byteData[offset]);
                            offset++;
                            if ((hour != 0) || (minute != 0))
                            {
                                measurementsInfo = new ADM_ConfigComponent_MeasurementsInfo();
                                measurementsInfo.ItemNumber = i;
                                measurementsInfo.Hour = hour;
                                measurementsInfo.Minute = minute;
                                measurementsInfoList.Add(measurementsInfo);
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nInput Byte[] does not have the expected minimum number of entries.";
                        LogMessage.LogError(errorMessage);
                        measurementsInfoList = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[], Guid, DateTime)\nInput Byte[] was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[], Guid, DateTime)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        private void ConvertMeasurementsInfoToBytes(List<ADM_ConfigComponent_MeasurementsInfo> measurementsInfoList, ref Byte[] byteData)
        {
            try
            {
                int offset;
                if (measurementsInfoList != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            foreach (ADM_ConfigComponent_MeasurementsInfo measurementsInfo in measurementsInfoList)
                            {
                                offset = 15 + measurementsInfo.ItemNumber * 2;
                                byteData[offset] = ConversionMethods.Int32ToByte(measurementsInfo.Hour);
                                byteData[offset + 1] = ConversionMethods.Int32ToByte(measurementsInfo.Minute);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in ADM_Configuration.ConvertMeasurementsInfoToBytes(List<ADM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nbyteData array does not have the expected minimum number of entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ConvertMeasurementsInfoToBytes(List<ADM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertMeasurementsInfoToBytes(List<ADM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput List<ADM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertMeasurementsInfoToBytes(List<ADM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        private ADM_ConfigComponent_PDSetupInfo ExtractPDSetupInfoDataFromByteData(Byte[] byteData)
        {
            ADM_ConfigComponent_PDSetupInfo pdSetupInfo = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        pdSetupInfo = new ADM_ConfigComponent_PDSetupInfo();

                        pdSetupInfo.SaveMode = ConversionMethods.ByteToInt32(byteData[288]);
                        // skip a byte, probaby for alignment, not used at all by device
                        pdSetupInfo.ReadingSinPeriods = ConversionMethods.UnsignedShortBytesToUInt16(byteData[290], byteData[291]);
                        pdSetupInfo.SaveDays = ConversionMethods.ByteToInt32(byteData[292]);
                        pdSetupInfo.SaveNum = ConversionMethods.ByteToInt32(byteData[293]);
                        pdSetupInfo.PhaseShift = ConversionMethods.ByteToInt32(byteData[294]);
                        // skip a byte, probaby for alignment, not used at all by device
                        pdSetupInfo.PhShift = ConversionMethods.UnsignedShortBytesToUInt16(byteData[296], byteData[297]);
                        pdSetupInfo.CalcSpeedStackSize = ConversionMethods.ByteToInt32(byteData[298]);
                        // skip a lot of bytes where the ChannelInfo data is held, that data goes to a different table
                        pdSetupInfo.PSpeed_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[476], byteData[477]);
                        pdSetupInfo.PSpeed_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[478], byteData[479]);
                        pdSetupInfo.PJump_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[480], byteData[481]);
                        pdSetupInfo.PJump_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[482], byteData[483]);
                        pdSetupInfo.SyncType = ConversionMethods.ByteToInt32(byteData[484]);
                        // skip 3 bytes, probably for alignment, not used at all by device
                        pdSetupInfo.InternalSyncFrequency = ConversionMethods.BytesToSingle(byteData[488], byteData[489], byteData[490], byteData[491]);
                        pdSetupInfo.PulsesAmpl = ConversionMethods.BytesToSingle(byteData[492], byteData[493], byteData[494], byteData[495]);
                        // 
                        pdSetupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[496]);
                        pdSetupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[497]);
                        pdSetupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[498]);
                        pdSetupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[499]);
                        pdSetupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[500]);
                        pdSetupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[501]);
                        pdSetupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[502]);
                        pdSetupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[503]);
                        pdSetupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[504]);
                        pdSetupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[505]);
                        pdSetupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[506]);
                        pdSetupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[507]);
                        pdSetupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[508]);
                        pdSetupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[509]);
                        pdSetupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[510]);
                        pdSetupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[511]);
                        pdSetupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[512]);
                        pdSetupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[513]);
                        pdSetupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[514]);
                        pdSetupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[515]);
                        pdSetupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[516]);
                        pdSetupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[517]);
                        pdSetupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[518]);
                        pdSetupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[519]);
                        pdSetupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[520]);
                        pdSetupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[521]);
                        pdSetupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[522]);
                        pdSetupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[523]);
                        pdSetupInfo.Reserved_28 = ConversionMethods.ByteToInt32(byteData[524]);
                        pdSetupInfo.Reserved_29 = ConversionMethods.ByteToInt32(byteData[525]);
                        pdSetupInfo.Reserved_30 = ConversionMethods.ByteToInt32(byteData[526]);
                        pdSetupInfo.Reserved_31 = ConversionMethods.ByteToInt32(byteData[527]);
                        pdSetupInfo.Reserved_32 = ConversionMethods.ByteToInt32(byteData[528]);
                        pdSetupInfo.Reserved_33 = ConversionMethods.ByteToInt32(byteData[529]);
                        pdSetupInfo.Reserved_34 = ConversionMethods.ByteToInt32(byteData[530]);
                        pdSetupInfo.Reserved_35 = ConversionMethods.ByteToInt32(byteData[531]);
                        pdSetupInfo.Reserved_36 = ConversionMethods.ByteToInt32(byteData[532]);
                        pdSetupInfo.Reserved_37 = ConversionMethods.ByteToInt32(byteData[533]);
                        pdSetupInfo.Reserved_38 = ConversionMethods.ByteToInt32(byteData[534]);
                        pdSetupInfo.Reserved_39 = ConversionMethods.ByteToInt32(byteData[535]);
                        pdSetupInfo.Reserved_40 = ConversionMethods.ByteToInt32(byteData[536]);
                        pdSetupInfo.Reserved_41 = ConversionMethods.ByteToInt32(byteData[537]);
                        pdSetupInfo.Reserved_42 = ConversionMethods.ByteToInt32(byteData[538]);
                        pdSetupInfo.Reserved_43 = ConversionMethods.ByteToInt32(byteData[539]);
                        pdSetupInfo.Reserved_44 = ConversionMethods.ByteToInt32(byteData[540]);
                        pdSetupInfo.Reserved_45 = ConversionMethods.ByteToInt32(byteData[541]);
                        pdSetupInfo.Reserved_46 = ConversionMethods.ByteToInt32(byteData[542]);
                        pdSetupInfo.Reserved_47 = ConversionMethods.ByteToInt32(byteData[543]);
                        pdSetupInfo.Reserved_48 = ConversionMethods.ByteToInt32(byteData[544]);
                        pdSetupInfo.Reserved_49 = ConversionMethods.ByteToInt32(byteData[545]);
                        pdSetupInfo.Reserved_50 = ConversionMethods.ByteToInt32(byteData[546]);
                        pdSetupInfo.Reserved_51 = ConversionMethods.ByteToInt32(byteData[547]);
                        pdSetupInfo.Reserved_52 = ConversionMethods.ByteToInt32(byteData[548]);
                        pdSetupInfo.Reserved_53 = ConversionMethods.ByteToInt32(byteData[549]);
                        pdSetupInfo.Reserved_54 = ConversionMethods.ByteToInt32(byteData[550]);
                        pdSetupInfo.Reserved_55 = ConversionMethods.ByteToInt32(byteData[551]);
                        pdSetupInfo.Reserved_56 = ConversionMethods.ByteToInt32(byteData[552]);
                        pdSetupInfo.Reserved_57 = ConversionMethods.ByteToInt32(byteData[553]);
                        pdSetupInfo.Reserved_58 = ConversionMethods.ByteToInt32(byteData[554]);
                        pdSetupInfo.Reserved_59 = ConversionMethods.ByteToInt32(byteData[555]);
                        pdSetupInfo.Reserved_60 = ConversionMethods.ByteToInt32(byteData[556]);
                        pdSetupInfo.Reserved_61 = ConversionMethods.ByteToInt32(byteData[557]);
                        pdSetupInfo.Reserved_62 = ConversionMethods.ByteToInt32(byteData[558]);
                        pdSetupInfo.Reserved_63 = ConversionMethods.ByteToInt32(byteData[559]);
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nbyteData array does not have the expected minimum number of entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        pdSetupInfo = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdSetupInfo;
        }

        private void ConvertPDSetupInfoToBytes(ADM_ConfigComponent_PDSetupInfo pdSetupInfo, ref Byte[] byteData)
        {
            try
            {
                if (pdSetupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[288] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveMode);
                            // skip a byte, probaby for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.ReadingSinPeriods), 0, byteData, 290, 2);
                            byteData[292] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveDays);
                            byteData[293] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveNum);
                            byteData[294] = ConversionMethods.Int32ToByte(pdSetupInfo.PhaseShift);
                            // skip a byte, probaby for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PhShift), 0, byteData, 296, 2);
                            byteData[298] = ConversionMethods.Int32ToByte(pdSetupInfo.CalcSpeedStackSize);
                            // skip a lot of bytes where the ChannelInfo data is held, that data goes to a different table
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PSpeed_0), 0, byteData, 476, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PSpeed_1), 0, byteData, 478, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PJump_0), 0, byteData, 480, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PJump_1), 0, byteData, 482, 2);
                            byteData[484] = ConversionMethods.Int32ToByte(pdSetupInfo.SyncType);
                            // skip 3 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)pdSetupInfo.InternalSyncFrequency), 0, byteData, 488, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)pdSetupInfo.PulsesAmpl), 0, byteData, 492, 4);
                            //
                            byteData[496] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_0);
                            byteData[497] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_1);
                            byteData[498] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_2);
                            byteData[499] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_3);
                            byteData[500] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_4);
                            byteData[501] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_5);
                            byteData[502] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_6);
                            byteData[503] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_7);
                            byteData[504] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_8);
                            byteData[505] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_9);
                            byteData[506] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_10);
                            byteData[507] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_11);
                            byteData[508] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_12);
                            byteData[509] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_13);
                            byteData[510] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_14);
                            byteData[511] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_15);
                            byteData[512] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_16);
                            byteData[513] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_17);
                            byteData[514] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_18);
                            byteData[515] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_19);
                            byteData[516] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_20);
                            byteData[517] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_21);
                            byteData[518] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_22);
                            byteData[519] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_23);
                            byteData[520] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_24);
                            byteData[521] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_25);
                            byteData[522] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_26);
                            byteData[523] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_27);
                            byteData[524] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_28);
                            byteData[525] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_29);
                            byteData[526] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_30);
                            byteData[527] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_31);
                            byteData[528] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_32);
                            byteData[529] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_33);
                            byteData[530] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_34);
                            byteData[531] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_35);
                            byteData[532] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_36);
                            byteData[533] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_37);
                            byteData[534] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_38);
                            byteData[535] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_39);
                            byteData[536] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_40);
                            byteData[537] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_41);
                            byteData[538] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_42);
                            byteData[539] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_43);
                            byteData[540] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_44);
                            byteData[541] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_45);
                            byteData[542] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_46);
                            byteData[543] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_47);
                            byteData[544] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_48);
                            byteData[545] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_49);
                            byteData[546] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_50);
                            byteData[547] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_51);
                            byteData[548] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_52);
                            byteData[549] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_53);
                            byteData[550] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_54);
                            byteData[551] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_55);
                            byteData[552] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_56);
                            byteData[553] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_57);
                            byteData[554] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_58);
                            byteData[555] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_59);
                            byteData[556] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_60);
                            byteData[557] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_61);
                            byteData[558] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_62);
                            byteData[559] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_63);
                        }
                        else
                        {
                            string errorMessage = "Error in ADM_Configuration.ConvertPDSetupInfoToBytes(ADM_ConfigComponent_PDSetupInfo, ref Byte[])\nbyteData array does not have the expected minimum number of entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ConvertPDSetupInfoToBytes(ADM_ConfigComponent_PDSetupInfo, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertPDSetupInfoToBytes(ADM_ConfigComponent_PDSetupInfo, ref Byte[])\nInput ADM_ConfigComponent_PDSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertPDSetupInfoToBytes(ADM_ConfigComponent_PDSetupInfo, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private ADM_ConfigComponent_SetupInfo ExtractSetupInfoDataFromByteData(Byte[] byteData)
        {
            ADM_ConfigComponent_SetupInfo setupInfo = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        setupInfo = new ADM_ConfigComponent_SetupInfo();

                        setupInfo.ID_0 = ConversionMethods.ByteToInt32(byteData[0]);
                        setupInfo.ID_1 = ConversionMethods.ByteToInt32(byteData[1]);
                        setupInfo.ID_2 = ConversionMethods.ByteToInt32(byteData[2]);
                        setupInfo.ID_3 = ConversionMethods.ByteToInt32(byteData[3]);
                        setupInfo.ID_4 = ConversionMethods.ByteToInt32(byteData[4]);
                        setupInfo.ID_5 = ConversionMethods.ByteToInt32(byteData[5]);
                        setupInfo.ID_6 = ConversionMethods.ByteToInt32(byteData[6]);
                        // skip a byte
                        setupInfo.FW_Ver = ConversionMethods.UnsignedBytesToUInt32(byteData[8], byteData[9], byteData[10], byteData[11]);

                        setupInfo.ScheduleType = ConversionMethods.ByteToInt32(byteData[12]);
                        setupInfo.DTime_Hour = ConversionMethods.ByteToInt32(byteData[13]);
                        setupInfo.DTime_Minute = ConversionMethods.ByteToInt32(byteData[14]);
                        // many bytes skipped that hold MeasurementsInfo
                        setupInfo.DisplayFlag = ConversionMethods.UnsignedShortBytesToUInt16(byteData[116], byteData[117]);
                        setupInfo.RelayMode = ConversionMethods.ByteToInt32(byteData[118]);
                        // skipped a byte, probably for alignment, not used at all by device
                        setupInfo.TimeOfRelayAlarm = ConversionMethods.UnsignedShortBytesToUInt16(byteData[120], byteData[121]);
                        setupInfo.OutTime = ConversionMethods.ByteToInt32(byteData[122]);
                        setupInfo.DeviceNumber = ConversionMethods.ByteToInt32(byteData[123]);
                        setupInfo.BaudRate = ConversionMethods.ByteToInt32(byteData[124]);
                        setupInfo.ModBusProtocol = ConversionMethods.ByteToInt32(byteData[125]);
                        setupInfo.Stopped = ConversionMethods.ByteToInt32(byteData[126]);
                        setupInfo.ReadAnalogFromRegisters = ConversionMethods.ByteToInt32(byteData[127]);

                        setupInfo.Coordinate_0_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[128], byteData[129]);
                        setupInfo.Coordinate_0_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[130], byteData[131]);
                        setupInfo.Coordinate_0_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[132], byteData[133]);
                        setupInfo.Coordinate_1_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[134], byteData[135]);
                        setupInfo.Coordinate_1_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[136], byteData[137]);
                        setupInfo.Coordinate_1_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[138], byteData[139]);
                        setupInfo.Coordinate_2_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[140], byteData[141]);
                        setupInfo.Coordinate_2_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[142], byteData[143]);
                        setupInfo.Coordinate_2_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[144], byteData[145]);
                        setupInfo.Coordinate_3_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[146], byteData[147]);
                        setupInfo.Coordinate_3_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[148], byteData[149]);
                        setupInfo.Coordinate_3_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[150], byteData[151]);

                        setupInfo.SizeX = ConversionMethods.UnsignedShortBytesToUInt16(byteData[152], byteData[153]);
                        setupInfo.SizeY = ConversionMethods.UnsignedShortBytesToUInt16(byteData[154], byteData[155]);
                        setupInfo.SizeZ = ConversionMethods.UnsignedShortBytesToUInt16(byteData[156], byteData[157]);
                        setupInfo.Scorost = ConversionMethods.ByteToInt32(byteData[158]);
                        // skipped a byte
                        setupInfo.Porog = ConversionMethods.UnsignedShortBytesToUInt16(byteData[160], byteData[161]);
                        setupInfo.Dopusk = ConversionMethods.UnsignedShortBytesToUInt16(byteData[162], byteData[163]);
                        setupInfo.Channels = ConversionMethods.ConvertEightBitEncodingToStringRepresenatationOfBits(byteData[164]);
                        // skipped a byte
                        setupInfo.Freq = ConversionMethods.UnsignedShortBytesToUInt16(byteData[166], byteData[167]);
                        setupInfo.CalcPer = ConversionMethods.UnsignedShortBytesToUInt16(byteData[168], byteData[169]);
                        setupInfo.Exist3D = ConversionMethods.ByteToInt32(byteData[170]);
                        setupInfo.ExistMeas = ConversionMethods.ByteToInt32(byteData[171]);

                        setupInfo.AlarmEnable = ConversionMethods.UnsignedShortBytesToUInt16(byteData[172], byteData[173]);
                        setupInfo.Termostat = ConversionMethods.ShortBytesToInt16(byteData[174], byteData[175]);
                        setupInfo.ReReadOnAlarm = ConversionMethods.ByteToInt32(byteData[176]);
                        // skipped 3 bytes, probably for alignment, not used at all by device
                        setupInfo.RatedCurrent_0 = ConversionMethods.BytesToSingle(byteData[180], byteData[181], byteData[182], byteData[183]);
                        setupInfo.RatedCurrent_1 = ConversionMethods.BytesToSingle(byteData[184], byteData[185], byteData[186], byteData[187]);
                        setupInfo.RatedCurrent_2 = ConversionMethods.BytesToSingle(byteData[188], byteData[189], byteData[190], byteData[191]);
                        setupInfo.RatedCurrent_3 = ConversionMethods.BytesToSingle(byteData[192], byteData[193], byteData[194], byteData[195]);
                        //
                        setupInfo.RatedVoltage_0 = ConversionMethods.BytesToSingle(byteData[196], byteData[197], byteData[198], byteData[199]);
                        setupInfo.RatedVoltage_1 = ConversionMethods.BytesToSingle(byteData[200], byteData[201], byteData[202], byteData[203]);
                        setupInfo.RatedVoltage_2 = ConversionMethods.BytesToSingle(byteData[204], byteData[205], byteData[206], byteData[207]);
                        setupInfo.RatedVoltage_3 = ConversionMethods.BytesToSingle(byteData[208], byteData[209], byteData[210], byteData[211]);
                        //
                        setupInfo.ObjectType = ConversionMethods.ByteToInt32(byteData[212]);
                        // skipped 3 bytes, probably for alignment, not used at all by device
                        setupInfo.Power = ConversionMethods.BytesToSingle(byteData[216], byteData[217], byteData[218], byteData[219]);
                        setupInfo.ExtTemperature = ConversionMethods.ByteToInt32(byteData[220]);
                        setupInfo.ExtHumidity = ConversionMethods.ByteToInt32(byteData[221]);
                        setupInfo.ExtLoadActive = ConversionMethods.ByteToInt32(byteData[222]);
                        setupInfo.ExtLoadReactive = ConversionMethods.ByteToInt32(byteData[223]);
                        //
                        setupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[224]);
                        setupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[225]);
                        setupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[226]);
                        setupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[227]);
                        setupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[228]);
                        setupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[229]);
                        setupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[230]);
                        setupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[231]);
                        setupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[232]);
                        setupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[233]);
                        setupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[234]);
                        setupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[235]);
                        setupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[236]);
                        setupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[237]);
                        setupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[238]);
                        setupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[239]);
                        setupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[240]);
                        setupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[241]);
                        setupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[242]);
                        setupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[243]);
                        setupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[244]);
                        setupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[245]);
                        setupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[246]);
                        setupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[247]);
                        setupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[248]);
                        setupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[249]);
                        setupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[250]);
                        setupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[251]);
                        setupInfo.Reserved_28 = ConversionMethods.ByteToInt32(byteData[252]);
                        setupInfo.Reserved_29 = ConversionMethods.ByteToInt32(byteData[253]);
                        setupInfo.Reserved_30 = ConversionMethods.ByteToInt32(byteData[254]);
                        setupInfo.Reserved_31 = ConversionMethods.ByteToInt32(byteData[255]);
                        setupInfo.Reserved_32 = ConversionMethods.ByteToInt32(byteData[256]);
                        setupInfo.Reserved_33 = ConversionMethods.ByteToInt32(byteData[257]);
                        setupInfo.Reserved_34 = ConversionMethods.ByteToInt32(byteData[258]);
                        setupInfo.Reserved_35 = ConversionMethods.ByteToInt32(byteData[259]);
                        setupInfo.Reserved_36 = ConversionMethods.ByteToInt32(byteData[260]);
                        setupInfo.Reserved_37 = ConversionMethods.ByteToInt32(byteData[261]);
                        setupInfo.Reserved_38 = ConversionMethods.ByteToInt32(byteData[262]);
                        setupInfo.Reserved_39 = ConversionMethods.ByteToInt32(byteData[263]);
                        setupInfo.Reserved_40 = ConversionMethods.ByteToInt32(byteData[264]);
                        setupInfo.Reserved_41 = ConversionMethods.ByteToInt32(byteData[265]);
                        setupInfo.Reserved_42 = ConversionMethods.ByteToInt32(byteData[266]);
                        setupInfo.Reserved_43 = ConversionMethods.ByteToInt32(byteData[267]);
                        setupInfo.Reserved_44 = ConversionMethods.ByteToInt32(byteData[268]);
                        setupInfo.Reserved_45 = ConversionMethods.ByteToInt32(byteData[269]);
                        setupInfo.Reserved_46 = ConversionMethods.ByteToInt32(byteData[270]);
                        setupInfo.Reserved_47 = ConversionMethods.ByteToInt32(byteData[271]);
                        setupInfo.Reserved_48 = ConversionMethods.ByteToInt32(byteData[272]);
                        setupInfo.Reserved_49 = ConversionMethods.ByteToInt32(byteData[273]);
                        setupInfo.Reserved_50 = ConversionMethods.ByteToInt32(byteData[274]);
                        setupInfo.Reserved_51 = ConversionMethods.ByteToInt32(byteData[275]);
                        setupInfo.Reserved_52 = ConversionMethods.ByteToInt32(byteData[276]);
                        setupInfo.Reserved_53 = ConversionMethods.ByteToInt32(byteData[277]);
                        setupInfo.Reserved_54 = ConversionMethods.ByteToInt32(byteData[278]);
                        setupInfo.Reserved_55 = ConversionMethods.ByteToInt32(byteData[279]);
                        setupInfo.Reserved_56 = ConversionMethods.ByteToInt32(byteData[280]);
                        setupInfo.Reserved_57 = ConversionMethods.ByteToInt32(byteData[281]);
                        setupInfo.Reserved_58 = ConversionMethods.ByteToInt32(byteData[282]);
                        setupInfo.Reserved_59 = ConversionMethods.ByteToInt32(byteData[283]);
                        setupInfo.Reserved_60 = ConversionMethods.ByteToInt32(byteData[284]);
                        setupInfo.Reserved_61 = ConversionMethods.ByteToInt32(byteData[285]);

                        setupInfo.CRC = ConversionMethods.UnsignedShortBytesToUInt16(byteData[286], byteData[287]);
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nbyteData array does not have the expected minimum number of entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        setupInfo = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        private void ConvertSetupInfoToBytes(ADM_ConfigComponent_SetupInfo setupInfo, ref Byte[] byteData)
        {
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        if (setupInfo != null)
                        {
                            byteData[0] = ConversionMethods.Int32ToByte(setupInfo.ID_0);
                            byteData[1] = ConversionMethods.Int32ToByte(setupInfo.ID_1);
                            byteData[2] = ConversionMethods.Int32ToByte(setupInfo.ID_2);
                            byteData[3] = ConversionMethods.Int32ToByte(setupInfo.ID_3);
                            byteData[4] = ConversionMethods.Int32ToByte(setupInfo.ID_4);
                            byteData[5] = ConversionMethods.Int32ToByte(setupInfo.ID_5);
                            byteData[6] = ConversionMethods.Int32ToByte(setupInfo.ID_6);
                            // skip a byte
                            Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)setupInfo.FW_Ver), 0, byteData, 8, 4);
                            byteData[12] = ConversionMethods.Int32ToByte(setupInfo.ScheduleType);
                            byteData[13] = ConversionMethods.Int32ToByte(setupInfo.DTime_Hour);
                            byteData[14] = ConversionMethods.Int32ToByte(setupInfo.DTime_Minute);
                            // break for MeasurementInfo stuff, saved in a list of other objects
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.DisplayFlag), 0, byteData, 116, 2);
                            byteData[118] = ConversionMethods.Int32ToByte(setupInfo.RelayMode);
                            // skip a byte, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.TimeOfRelayAlarm), 0, byteData, 120, 2);
                            byteData[122] = ConversionMethods.Int32ToByte(setupInfo.OutTime);
                            byteData[123] = ConversionMethods.Int32ToByte(setupInfo.DeviceNumber);
                            byteData[124] = ConversionMethods.Int32ToByte(setupInfo.BaudRate);
                            byteData[125] = ConversionMethods.Int32ToByte(setupInfo.ModBusProtocol);
                            byteData[126] = ConversionMethods.Int32ToByte(setupInfo.Stopped);
                            byteData[127] = ConversionMethods.Int32ToByte(setupInfo.ReadAnalogFromRegisters);
                            // array data
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_0_0), 0, byteData, 128, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_0_1), 0, byteData, 130, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_0_2), 0, byteData, 132, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_1_0), 0, byteData, 134, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_1_1), 0, byteData, 136, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_1_2), 0, byteData, 138, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_2_0), 0, byteData, 140, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_2_1), 0, byteData, 142, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_2_2), 0, byteData, 144, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_3_0), 0, byteData, 146, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_3_1), 0, byteData, 148, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Coordinate_3_2), 0, byteData, 150, 2);
                            //
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.SizeX), 0, byteData, 152, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.SizeY), 0, byteData, 154, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.SizeZ), 0, byteData, 156, 2);
                            byteData[158] = ConversionMethods.Int32ToByte(setupInfo.Scorost);
                            // skip a byte
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Porog), 0, byteData, 160, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Dopusk), 0, byteData, 162, 2);
                            byteData[164] = ConversionMethods.ConvertEightBitEncodedStringToByte(setupInfo.Channels);
                            // skip a byte
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.Freq), 0, byteData, 166, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.CalcPer), 0, byteData, 168, 2);
                            byteData[170] = ConversionMethods.Int32ToByte(setupInfo.Exist3D);
                            byteData[171] = ConversionMethods.Int32ToByte(setupInfo.ExistMeas);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.AlarmEnable), 0, byteData, 172, 2);
                            Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)setupInfo.Termostat), 0, byteData, 174, 2);
                            byteData[176] = ConversionMethods.Int32ToByte(setupInfo.ReReadOnAlarm);
                            // skip 3 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_0), 0, byteData, 180, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_1), 0, byteData, 184, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_2), 0, byteData, 188, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_3), 0, byteData, 192, 4);
                            //
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_0), 0, byteData, 196, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_1), 0, byteData, 200, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_2), 0, byteData, 204, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_3), 0, byteData, 208, 4);
                            //
                            byteData[212] = ConversionMethods.Int32ToByte(setupInfo.ObjectType);
                            // skip 3 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.Power), 0, byteData, 216, 4);
                            byteData[220] = ConversionMethods.Int32ToByte(setupInfo.ExtTemperature);
                            byteData[221] = ConversionMethods.Int32ToByte(setupInfo.ExtHumidity);
                            byteData[222] = ConversionMethods.Int32ToByte(setupInfo.ExtLoadActive);
                            byteData[223] = ConversionMethods.Int32ToByte(setupInfo.ExtLoadReactive);
                            //
                            byteData[224] = ConversionMethods.Int32ToByte(setupInfo.Reserved_0);
                            byteData[225] = ConversionMethods.Int32ToByte(setupInfo.Reserved_1);
                            byteData[226] = ConversionMethods.Int32ToByte(setupInfo.Reserved_2);
                            byteData[227] = ConversionMethods.Int32ToByte(setupInfo.Reserved_3);
                            byteData[228] = ConversionMethods.Int32ToByte(setupInfo.Reserved_4);
                            byteData[229] = ConversionMethods.Int32ToByte(setupInfo.Reserved_5);
                            byteData[230] = ConversionMethods.Int32ToByte(setupInfo.Reserved_6);
                            byteData[231] = ConversionMethods.Int32ToByte(setupInfo.Reserved_7);
                            byteData[232] = ConversionMethods.Int32ToByte(setupInfo.Reserved_8);
                            byteData[233] = ConversionMethods.Int32ToByte(setupInfo.Reserved_9);
                            byteData[234] = ConversionMethods.Int32ToByte(setupInfo.Reserved_10);
                            byteData[235] = ConversionMethods.Int32ToByte(setupInfo.Reserved_11);
                            byteData[236] = ConversionMethods.Int32ToByte(setupInfo.Reserved_12);
                            byteData[237] = ConversionMethods.Int32ToByte(setupInfo.Reserved_13);
                            byteData[238] = ConversionMethods.Int32ToByte(setupInfo.Reserved_14);
                            byteData[239] = ConversionMethods.Int32ToByte(setupInfo.Reserved_15);
                            byteData[240] = ConversionMethods.Int32ToByte(setupInfo.Reserved_16);
                            byteData[241] = ConversionMethods.Int32ToByte(setupInfo.Reserved_17);
                            byteData[242] = ConversionMethods.Int32ToByte(setupInfo.Reserved_18);
                            byteData[243] = ConversionMethods.Int32ToByte(setupInfo.Reserved_19);
                            byteData[244] = ConversionMethods.Int32ToByte(setupInfo.Reserved_20);
                            byteData[245] = ConversionMethods.Int32ToByte(setupInfo.Reserved_21);
                            byteData[246] = ConversionMethods.Int32ToByte(setupInfo.Reserved_22);
                            byteData[247] = ConversionMethods.Int32ToByte(setupInfo.Reserved_23);
                            byteData[248] = ConversionMethods.Int32ToByte(setupInfo.Reserved_24);
                            byteData[249] = ConversionMethods.Int32ToByte(setupInfo.Reserved_25);
                            byteData[250] = ConversionMethods.Int32ToByte(setupInfo.Reserved_26);
                            byteData[251] = ConversionMethods.Int32ToByte(setupInfo.Reserved_27);
                            byteData[252] = ConversionMethods.Int32ToByte(setupInfo.Reserved_28);
                            byteData[253] = ConversionMethods.Int32ToByte(setupInfo.Reserved_29);
                            byteData[254] = ConversionMethods.Int32ToByte(setupInfo.Reserved_30);
                            byteData[255] = ConversionMethods.Int32ToByte(setupInfo.Reserved_31);
                            byteData[256] = ConversionMethods.Int32ToByte(setupInfo.Reserved_32);
                            byteData[257] = ConversionMethods.Int32ToByte(setupInfo.Reserved_33);
                            byteData[258] = ConversionMethods.Int32ToByte(setupInfo.Reserved_34);
                            byteData[259] = ConversionMethods.Int32ToByte(setupInfo.Reserved_35);
                            byteData[260] = ConversionMethods.Int32ToByte(setupInfo.Reserved_36);
                            byteData[261] = ConversionMethods.Int32ToByte(setupInfo.Reserved_37);
                            byteData[262] = ConversionMethods.Int32ToByte(setupInfo.Reserved_38);
                            byteData[263] = ConversionMethods.Int32ToByte(setupInfo.Reserved_39);
                            byteData[264] = ConversionMethods.Int32ToByte(setupInfo.Reserved_40);
                            byteData[265] = ConversionMethods.Int32ToByte(setupInfo.Reserved_41);
                            byteData[266] = ConversionMethods.Int32ToByte(setupInfo.Reserved_42);
                            byteData[267] = ConversionMethods.Int32ToByte(setupInfo.Reserved_43);
                            byteData[268] = ConversionMethods.Int32ToByte(setupInfo.Reserved_44);
                            byteData[269] = ConversionMethods.Int32ToByte(setupInfo.Reserved_45);
                            byteData[270] = ConversionMethods.Int32ToByte(setupInfo.Reserved_46);
                            byteData[271] = ConversionMethods.Int32ToByte(setupInfo.Reserved_47);
                            byteData[272] = ConversionMethods.Int32ToByte(setupInfo.Reserved_48);
                            byteData[273] = ConversionMethods.Int32ToByte(setupInfo.Reserved_49);
                            byteData[274] = ConversionMethods.Int32ToByte(setupInfo.Reserved_50);
                            byteData[275] = ConversionMethods.Int32ToByte(setupInfo.Reserved_51);
                            byteData[276] = ConversionMethods.Int32ToByte(setupInfo.Reserved_52);
                            byteData[277] = ConversionMethods.Int32ToByte(setupInfo.Reserved_53);
                            byteData[278] = ConversionMethods.Int32ToByte(setupInfo.Reserved_54);
                            byteData[279] = ConversionMethods.Int32ToByte(setupInfo.Reserved_55);
                            byteData[280] = ConversionMethods.Int32ToByte(setupInfo.Reserved_56);
                            byteData[281] = ConversionMethods.Int32ToByte(setupInfo.Reserved_57);
                            byteData[282] = ConversionMethods.Int32ToByte(setupInfo.Reserved_58);
                            byteData[283] = ConversionMethods.Int32ToByte(setupInfo.Reserved_59);
                            byteData[284] = ConversionMethods.Int32ToByte(setupInfo.Reserved_60);
                            byteData[285] = ConversionMethods.Int32ToByte(setupInfo.Reserved_61);
                            //
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.CRC), 0, byteData, 286, 2);
                        }
                        else
                        {
                            string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(ADM_Config_SetupInfo, ref Byte[])\nInput ADM_ConfigComponent_SetupInfo was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(ADM_Config_SetupInfo, ref Byte[])\nbyteData array does not have the expected minimum number of entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToBytes(ADM_Config_SetupInfo, ref Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertSetupInfoToBytes(ADM_Config_SetupInfo, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        #endregion

        #region Show Raw Data

        public List<String> ConvertSetupInfoToStringValues()
        {
            List<String> variablesAsStrings = new List<string>();
            try
            {
                if (setupInfo != null)
                {                   
                    variablesAsStrings.Add("SetupInfo Entries");
                    variablesAsStrings.Add("");
                    variablesAsStrings.Add("ID_0: " + setupInfo.ID_0.ToString());
                    variablesAsStrings.Add("ID_1: " + setupInfo.ID_1.ToString());
                    variablesAsStrings.Add("ID_2: " + setupInfo.ID_2.ToString());
                    variablesAsStrings.Add("ID_3: " + setupInfo.ID_3.ToString());
                    variablesAsStrings.Add("ID_4: " + setupInfo.ID_4.ToString());
                    variablesAsStrings.Add("ID_5: " + setupInfo.ID_5.ToString());
                    variablesAsStrings.Add("ID_6: " + setupInfo.ID_6.ToString());

                    variablesAsStrings.Add("FW_Ver: " + setupInfo.FW_Ver.ToString());
                    variablesAsStrings.Add("ScheduleType: " + setupInfo.ScheduleType.ToString());
                    variablesAsStrings.Add("DTime_Hour: " + setupInfo.DTime_Hour.ToString());
                    variablesAsStrings.Add("DTime_Minute: " + setupInfo.DTime_Minute.ToString());
                    variablesAsStrings.Add("DisplayFlag: " + setupInfo.DisplayFlag.ToString());
                    variablesAsStrings.Add("RelayMode: " + setupInfo.RelayMode.ToString());
                    variablesAsStrings.Add("TimeOfRelayALarm: " + setupInfo.TimeOfRelayAlarm.ToString());
                    variablesAsStrings.Add("OutTime: " + setupInfo.OutTime.ToString());
                    variablesAsStrings.Add("DeviceNumber: " + setupInfo.DeviceNumber.ToString());
                    variablesAsStrings.Add("BaudRate: " + setupInfo.BaudRate.ToString());
                    variablesAsStrings.Add("ModBusProtocol: " + setupInfo.ModBusProtocol.ToString());
                    variablesAsStrings.Add("Stopped: " + setupInfo.Stopped.ToString());
                    variablesAsStrings.Add("ReadAnalogFromRegisters: " + setupInfo.ReadAnalogFromRegisters.ToString());
                    variablesAsStrings.Add("Coordinate_0_0: " + setupInfo.Coordinate_0_0.ToString());
                    variablesAsStrings.Add("Coordinate_0_1: " + setupInfo.Coordinate_0_1.ToString());
                    variablesAsStrings.Add("Coordinate_0_2: " + setupInfo.Coordinate_0_2.ToString());
                    variablesAsStrings.Add("Coordinate_1_0: " + setupInfo.Coordinate_1_0.ToString());
                    variablesAsStrings.Add("Coordinate_1_1: " + setupInfo.Coordinate_1_1.ToString());
                    variablesAsStrings.Add("Coordinate_1_2: " + setupInfo.Coordinate_1_2.ToString());
                    variablesAsStrings.Add("Coordinate_2_0: " + setupInfo.Coordinate_2_0.ToString());
                    variablesAsStrings.Add("Coordinate_2_1: " + setupInfo.Coordinate_2_1.ToString());
                    variablesAsStrings.Add("Coordinate_2_2: " + setupInfo.Coordinate_2_2.ToString());
                    variablesAsStrings.Add("Coordinate_3_0: " + setupInfo.Coordinate_3_0.ToString());
                    variablesAsStrings.Add("Coordinate_3_1: " + setupInfo.Coordinate_3_1.ToString());
                    variablesAsStrings.Add("Coordinate_3_2: " + setupInfo.Coordinate_3_2.ToString());

                    variablesAsStrings.Add("SizeX: " + setupInfo.SizeX.ToString());
                    variablesAsStrings.Add("SizeY: " + setupInfo.SizeY.ToString());
                    variablesAsStrings.Add("SizeZ: " + setupInfo.SizeZ.ToString());
                    variablesAsStrings.Add("Scorost: " + setupInfo.Scorost.ToString());
                    variablesAsStrings.Add("Porog: " + setupInfo.Porog.ToString());
                    variablesAsStrings.Add("Dopusk: " + setupInfo.Dopusk.ToString());
                    variablesAsStrings.Add("Channels: " + setupInfo.Channels);
                    variablesAsStrings.Add("Freq: " + setupInfo.Freq.ToString());
                    variablesAsStrings.Add("CalcPer: " + setupInfo.CalcPer.ToString());
                    variablesAsStrings.Add("Exist3D: " + setupInfo.Exist3D.ToString());
                    variablesAsStrings.Add("ExistMeas: " + setupInfo.ExistMeas.ToString());
                    variablesAsStrings.Add("AlarmEnable: " + setupInfo.AlarmEnable.ToString());
                    variablesAsStrings.Add("Termostat: " + setupInfo.Termostat.ToString());
                    variablesAsStrings.Add("ReReadOnAlarm: " + setupInfo.ReReadOnAlarm.ToString());
                    variablesAsStrings.Add("RatedCurrent_0: " + setupInfo.RatedCurrent_0.ToString());
                    variablesAsStrings.Add("RatedCurrent_1: " + setupInfo.RatedCurrent_1.ToString());
                    variablesAsStrings.Add("RatedCurrent_2: " + setupInfo.RatedCurrent_2.ToString());
                    variablesAsStrings.Add("RatedCurrent_3: " + setupInfo.RatedCurrent_3.ToString());
                    variablesAsStrings.Add("RatedVoltage_0: " + setupInfo.RatedVoltage_0.ToString());
                    variablesAsStrings.Add("RatedVoltage_1: " + setupInfo.RatedVoltage_1.ToString());
                    variablesAsStrings.Add("RatedVoltage_2: " + setupInfo.RatedVoltage_2.ToString());
                    variablesAsStrings.Add("RatedVoltage_3: " + setupInfo.RatedVoltage_3.ToString());

                    variablesAsStrings.Add("ObjectType: " + setupInfo.ObjectType.ToString());
                    variablesAsStrings.Add("Power: " + setupInfo.Power.ToString());
                    variablesAsStrings.Add("ExtTemperature: " + setupInfo.ExtTemperature.ToString());
                    variablesAsStrings.Add("ExtHumidity: " + setupInfo.ExtHumidity.ToString());
                    variablesAsStrings.Add("ExtLoadActive: " + setupInfo.ExtLoadActive.ToString());
                    variablesAsStrings.Add("ExtLoadReactive: " + setupInfo.ExtLoadReactive.ToString());
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertSetupInfoToStringValues()\nthis.setupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertSetupInfoToStringValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return variablesAsStrings;
        }

        public List<String> ConvertPDSetupInfoToStringValues()
        {
            List<String> variablesAsStrings = new List<string>();
            try
            {
                if (pdSetupInfo != null)
                {
                    variablesAsStrings.Add("PDSetupInfo values");
                    variablesAsStrings.Add("");

                    variablesAsStrings.Add("SaveMode: " + pdSetupInfo.SaveMode.ToString());
                    variablesAsStrings.Add("ReadingSinPeriods: " + pdSetupInfo.ReadingSinPeriods.ToString());
                    variablesAsStrings.Add("SaveDays: " + pdSetupInfo.SaveDays.ToString());
                    variablesAsStrings.Add("SaveNum: " + pdSetupInfo.SaveNum.ToString());
                    variablesAsStrings.Add("PhaseShift: " + pdSetupInfo.PhaseShift.ToString());
                    variablesAsStrings.Add("PhShift: " + pdSetupInfo.PhShift.ToString());
                    variablesAsStrings.Add("CalcSpeedStackSize: " + pdSetupInfo.CalcSpeedStackSize.ToString());
                    variablesAsStrings.Add("PSpeed_0: " + pdSetupInfo.PSpeed_0.ToString());
                    variablesAsStrings.Add("PSpeed_1: " + pdSetupInfo.PSpeed_1.ToString());
                    variablesAsStrings.Add("PJump_0: " + pdSetupInfo.PJump_0.ToString());
                    variablesAsStrings.Add("PJump_1: " + pdSetupInfo.PJump_1.ToString());
                    variablesAsStrings.Add("SyncType: " + pdSetupInfo.SyncType.ToString());
                    variablesAsStrings.Add("InternalSyncFrequency: " + pdSetupInfo.InternalSyncFrequency.ToString());
                    variablesAsStrings.Add("PulsesAmpl: " + pdSetupInfo.PulsesAmpl.ToString());
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertPDSetupInfoToStringValues()\nthis.pdSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertPDSetupInfoToStringValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return variablesAsStrings;
        }

        public List<String> ConvertChPDPararmetersToStringValues()
        {
            List<String> variablesAsStrings = new List<string>();
            try
            {
                if (this.channelInfoList != null)
                {
                    variablesAsStrings.Add("ChPDParameters data");
                    variablesAsStrings.Add("");
                    foreach (ADM_ConfigComponent_ChannelInfo channelInfo in this.channelInfoList)
                    {
                        variablesAsStrings.Add("ChannelNumber: " + channelInfo.ChannelNumber.ToString());
                        variablesAsStrings.Add("ChannelIsOn: " + channelInfo.ChannelIsOn.ToString());
                        variablesAsStrings.Add("ChannelSensitivity: " + channelInfo.ChannelSensitivity.ToString());
                        variablesAsStrings.Add("PDMaxWidth: " + channelInfo.PDMaxWidth.ToString());
                        variablesAsStrings.Add("PDIntervalTime: " + channelInfo.PDIntervalTime.ToString());
                        variablesAsStrings.Add("P_0: " + channelInfo.P_0.ToString());
                        variablesAsStrings.Add("P_1: " + channelInfo.P_1.ToString());
                        variablesAsStrings.Add("P_2: " + channelInfo.P_2.ToString());
                        variablesAsStrings.Add("P_3: " + channelInfo.P_3.ToString());
                        variablesAsStrings.Add("P_4: " + channelInfo.P_4.ToString());
                        variablesAsStrings.Add("P_5: " + channelInfo.P_5.ToString());
                        variablesAsStrings.Add("");
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_Configuration.ConvertChPDPararmetersToStringValues()\nthis.channelInfoList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertChPDPararmetersToStringValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return variablesAsStrings;
        }

        public List<String> ConvertAllDataToStringValues()
        {
            List<String> classesAsStrings = new List<string>();
            try
            {
                classesAsStrings.AddRange(ConvertSetupInfoToStringValues());
                classesAsStrings.Add("");
                classesAsStrings.AddRange(ConvertPDSetupInfoToStringValues());
                classesAsStrings.Add("");
                classesAsStrings.AddRange(ConvertChPDPararmetersToStringValues());
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_Configuration.ConvertAllDataToStringValues()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return classesAsStrings;
        }

        #endregion

    }
}

