﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_TrSideParameters
    {
        public int ChPhaseShift;
        public int PhaseAmplitude_0;
        public int PhaseAmplitude_1;
        public int PhaseAmplitude_2;
        public int SourceAmplitude_0;
        public int SourceAmplitude_1;
        public int SourceAmplitude_2;
        public int Temperature;
        public int GammaAmplitude;
        public int GammaPhase;
        public int KT;
        public int KTPhase;
        public int DefectCode;
        public int KTSaved;
        public int SignalPhase_0;
        public int SignalPhase_1;
        public int SignalPhase_2;
        public int SourcePhase_0;
        public int SourcePhase_1;
        public int SourcePhase_2;
        public int RPN;
        public int TrSideParamCurrent;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
    }
}
