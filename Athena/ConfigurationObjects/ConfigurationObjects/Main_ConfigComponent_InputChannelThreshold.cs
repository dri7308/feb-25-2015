﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_InputChannelThreshold
    {
        public int InputChannelNumber;
        public int ThresholdNumber;
        [XmlElement(ElementName = "Status")]
        public int ThresholdEnable;
        [XmlElement(ElementName = "StatusInternalValue")]
        public int ThresholdStatus;
        public int ThresholdType;
        [XmlElement(ElementName = "Result")]
        public int RelayMode;
        public int Calculation;
        [XmlElement(ElementName = "Mode")]
        public int WorkingBy;
        public int LowFrequencyOfSignal;
        public int HighFrequencyOfSignal;
        [XmlElement(ElementName = "TimeInSeconds")]
        public int RelayStatusOnDelay;
        public int RelayStatusOffDelay;
        [XmlElement(ElementName = "Level")]
        public double ThresholdValue;
    }
}
