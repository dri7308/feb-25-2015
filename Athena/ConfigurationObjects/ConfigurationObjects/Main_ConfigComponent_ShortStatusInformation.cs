﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_ShortStatusInformation
    {
        public int ModuleNumber;
        public int ModuleStatus;
        public int ModuleType;
        public int Reserved;
    }
}
