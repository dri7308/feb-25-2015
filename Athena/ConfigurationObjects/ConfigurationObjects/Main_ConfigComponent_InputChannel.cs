﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace ConfigurationObjects
{
    public class Main_ConfigComponent_InputChannel
    {
        public int InputChannelNumber;
        [XmlElement(ElementName = "Enable")]
        public int ReadStatus;
        public int SensorType;
        public int Unit;
        public int SensorID;
        public int SensorNumber;
        [XmlElement(ElementName = "Sensitivity")]
        public double Sensitivity;
        public int HighPassFilter;
        public int LowPassFilter;
        [XmlElement(ElementName = "BoundaryFrequencyInASpectrum")]
        public int ReadFrequency;
        [XmlElement(ElementName = "NumberOfPoints")]
        public int ReadPoints;
        public int Averages;
        public int SaveDataIfChangedPercent;
        [XmlElement(ElementName = "ToSaveAtChangeOn")]
        public int SaveDataIfChangedValue;
        public int JunkValue_1;
    }
}
