﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_CommunicationSetup
    {
        public int ModBusAddress;
        [XmlElement(ElementName = "BaudRateOverInternalRS485")]
        public int BaudRateRS485;
        public int ModBusProtocol;
        [XmlElement(ElementName = "EthernetBaudRate")]
        public int BaudRateXport;
        [XmlElement(ElementName = "EthernetProtocol")]
        public int ModBusProtocolOverXport;
        [XmlElement(ElementName = "EthernetAddressPartOne")]
        public int IP1;
        [XmlElement(ElementName = "EthernetAddressPartTwo")]
        public int IP2;
        [XmlElement(ElementName = "EthernetAddressPartThree")]
        public int IP3;
        [XmlElement(ElementName = "EthernetAddressPartFour")]
        public int IP4;
        [XmlElement(ElementName = "ModeSaveStepInSeconds")]
        public int SaveDataIntervalInSeconds;
        public int SaveDataIfChangedByPercent;
        [XmlElement(ElementName = "TakeIntoAccountResultsOfDiagnostic")]
        public int SaveDataIfChangedStatus;
    }
}
