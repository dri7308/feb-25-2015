﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_FullStatusInformation
    {
        [XmlElement(ElementName = "MonitorNumber")]
        public int ModuleNumber;
        public int ModuleStatus;
        [XmlElement(ElementName = "MonitorType")]
        public int ModuleType;
        public int ModBusAddress;
        public int BaudRate;
        [XmlElement(ElementName = "DataTransferIntervalInMilliseconds")]
        public int ReadDatePeriodInSeconds;
        public int ConnectionType;
        public int Reserved_0;
        public int Reserved_1;
    }
}
