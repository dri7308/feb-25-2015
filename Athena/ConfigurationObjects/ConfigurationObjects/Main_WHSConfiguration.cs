﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using GeneralUtilities;

namespace ConfigurationObjects
{
    public class Main_WHSConfiguration
    {
        [XmlIgnore]
        public Int16[] rawValues;

        public Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup;

        public Main_WHSConfiguration()
        {
            this.windingHotSpotSetup = new Main_ConfigComponent_WindingHotSpotSetup();
        }

        public void InitializeConfigurationToDefaults()
        {
            this.windingHotSpotSetup = InitializeWindingHotSpotSetup();
        }

        public Main_WHSConfiguration(Int16[] shortValues, int firmwareVersion)
        {
            rawValues = shortValues;

            InitializeInternalData(shortValues, firmwareVersion);
        }

        public Main_WHSConfiguration(Main_ConfigComponent_WindingHotSpotSetup inputWindingHotSpotSetup)
        {
            this.windingHotSpotSetup = inputWindingHotSpotSetup;
        }

        private void InitializeInternalData(Int16[] shortValues, int firmwareVersion)
        {
            this.windingHotSpotSetup = ExtractWindingHotSpotSetupFromInputData(shortValues, firmwareVersion);
        }

        public Int16[] GetShortValuesFromAllContributors()
        {
            Int16[] outputValues = null;
            try
            {
                if (this.rawValues != null)
                {
                    int length = this.rawValues.Length;
                    outputValues = new Int16[length];
                    Array.Copy(this.rawValues, outputValues, length);
                }
                else
                {
                    outputValues = new Int16[60];  
                }
                ConvertWindingHotSpotSetupToShorts(windingHotSpotSetup, ref outputValues);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSConfiguration.InitializeInternalData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputValues;
        }

        public bool AllConfigurationMembersAreNonNull()
        {
            bool allNonNull = true;
            if (this.windingHotSpotSetup == null)
            {
                allNonNull = false;
            }
            return allNonNull;
        }

        private Main_ConfigComponent_WindingHotSpotSetup ExtractWindingHotSpotSetupFromInputData(Int16[] shortValues, int firmwareVersion)
        {
            Main_ConfigComponent_WindingHotSpotSetup localWindingHotSpotSetup = null;
            try
            {
                int offset = 0;
               
                UInt16[] unsignedShortValues;
                DateTime dateOfLastFanExercise = ConversionMethods.MinimumDateTime();
                if (shortValues.Length >= 60)
                {
                    unsignedShortValues = ConversionMethods.Int16ArrayToUInt16Array(shortValues);

                    localWindingHotSpotSetup = new Main_ConfigComponent_WindingHotSpotSetup();

                    localWindingHotSpotSetup.FirmwareVersion = firmwareVersion;

                    localWindingHotSpotSetup.RatingAsBitString = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits(unsignedShortValues[offset + 0]);

                    localWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = unsignedShortValues[offset + 1];
                    localWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = unsignedShortValues[offset + 2];
                    localWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = unsignedShortValues[offset + 3];
                    localWindingHotSpotSetup.HotSpotFactor = unsignedShortValues[offset + 4];
                    localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = unsignedShortValues[offset + 5];
                    localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = unsignedShortValues[offset + 6];
                    localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = unsignedShortValues[offset + 7];
                    localWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = unsignedShortValues[offset + 8];
                    localWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = unsignedShortValues[offset + 9];
                    localWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = unsignedShortValues[offset + 10];
                    localWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = unsignedShortValues[offset + 11];
                    localWindingHotSpotSetup.Exponent = unsignedShortValues[offset + 12];
                    localWindingHotSpotSetup.Exponent_B = unsignedShortValues[offset + 13];
                    localWindingHotSpotSetup.Exponent_C = unsignedShortValues[offset + 14];
                    localWindingHotSpotSetup.WindingTimeConstant = unsignedShortValues[offset + 15];
                    localWindingHotSpotSetup.WindingTimeConstant_B = unsignedShortValues[offset + 16];
                    localWindingHotSpotSetup.WindingTimeConstant_C = unsignedShortValues[offset + 17];
                    localWindingHotSpotSetup.FanSetpoint_H = shortValues[offset + 18];
                    localWindingHotSpotSetup.FanSetpoint_HH = shortValues[offset + 19];
                    localWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = shortValues[offset + 20];
                    localWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = shortValues[offset + 21];
                    localWindingHotSpotSetup.MinimumFanRunTimeInMinutes = unsignedShortValues[offset + 22];
                    localWindingHotSpotSetup.TimeBetweenFanExerciseInDays = unsignedShortValues[offset + 23];
                    localWindingHotSpotSetup.TimeToExerciseFanInMinutes = unsignedShortValues[offset + 24];
                    localWindingHotSpotSetup.whsFan1NumberOfStarts = (int)unsignedShortValues[offset + 25];
                    localWindingHotSpotSetup.whsFan2NumberOfStarts = (int)unsignedShortValues[offset + 26];
                    localWindingHotSpotSetup.fanTest = (int)unsignedShortValues[offset + 27];
                    localWindingHotSpotSetup.FanOneRuntimeInHours = unsignedShortValues[offset + 28];
                    localWindingHotSpotSetup.FanTwoRuntimeInHours = unsignedShortValues[offset + 29];
                    localWindingHotSpotSetup.FanBankSwitchingTimeInHours = unsignedShortValues[offset + 30];
                    localWindingHotSpotSetup.fan_1BaseCurrent = unsignedShortValues[offset + 31];
                    localWindingHotSpotSetup.fan_2BaseCurrent = unsignedShortValues[offset + 32];                   
                    localWindingHotSpotSetup.fan_lowLimitAlarm = unsignedShortValues[offset + 33];
                    localWindingHotSpotSetup.fan_highLimitAlarm = unsignedShortValues[offset + 34];
                    localWindingHotSpotSetup.NumberOfCoolingGroups = unsignedShortValues[offset + 35];
                    localWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = shortValues[offset + 36];
                    localWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = shortValues[offset + 37];
                    localWindingHotSpotSetup.AlarmTopOilTemperature_H = shortValues[offset + 38];
                    localWindingHotSpotSetup.AlarmTopOilTemperature_HH = shortValues[offset + 39];
                    localWindingHotSpotSetup.AlarmDeadBandTemperature = unsignedShortValues[offset + 40];
                    localWindingHotSpotSetup.AlarmDelayInSeconds = unsignedShortValues[offset + 41];
                    localWindingHotSpotSetup.whsPreviousAgingPhaseA = (double)unsignedShortValues[offset + 42];
                    localWindingHotSpotSetup.whsPreviousAgingPhaseB = (double)unsignedShortValues[offset + 43];
                    localWindingHotSpotSetup.whsPreviousAgingPhaseC = (double)unsignedShortValues[offset + 44];

                    

/*
                    localWindingHotSpotSetup.X_RatedHotSpotRisePhaseA = unsignedShortValues[offset + 4];
                    localWindingHotSpotSetup.X_RatedHotSpotRisePhaseB = unsignedShortValues[offset + 5];
                    localWindingHotSpotSetup.X_RatedHotSpotRisePhaseC = unsignedShortValues[offset + 6];

                    localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseA = unsignedShortValues[offset + 7];
                    localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseB = unsignedShortValues[offset + 8];
                    localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseC = unsignedShortValues[offset + 9];

                    localWindingHotSpotSetup.C_RatedHotSpotRisePhaseA = unsignedShortValues[offset + 10];
                    localWindingHotSpotSetup.C_RatedHotSpotRisePhaseB = unsignedShortValues[offset + 11];
                    localWindingHotSpotSetup.C_RatedHotSpotRisePhaseC = unsignedShortValues[offset + 12];

                   
                    localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseA = unsignedShortValues[offset + 16];
                    localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseB = unsignedShortValues[offset + 17];
                    localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseC = unsignedShortValues[offset + 18];

                    localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseA = unsignedShortValues[offset + 19];
                    localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseB = unsignedShortValues[offset + 20];
                    localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseC = unsignedShortValues[offset + 21];

                    localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseA = unsignedShortValues[offset + 22];
                    localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseB = unsignedShortValues[offset + 23];
                    localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseC = unsignedShortValues[offset + 24];
*/
                    // these are defined as Int16s in the spreadsheet
                   

                    //localWindingHotSpotSetup.Exponent = unsignedShortValues[offset + 27];
                    //localWindingHotSpotSetup.WindingTimeConstant = unsignedShortValues[offset + 28];
                  //  localWindingHotSpotSetup.PreviousAging = unsignedShortValues[offset + 29];

                   

                    // these are defined as Int16s in the spreadsheet
                   

                    // these are defined as Int16s in the spreadsheet
                   
                    
                    // back to unsigned ints
                   // localWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = unsignedShortValues[offset + 36];
                   // localWindingHotSpotSetup.TopOilTempPhaseBSourceRegisterNumber = unsignedShortValues[offset + 37];
                   // localWindingHotSpotSetup.TopOilTempPhaseCSourceRegisterNumber = unsignedShortValues[offset + 38];

                   // localWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = unsignedShortValues[offset + 39];
                   // localWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = unsignedShortValues[offset + 40];
                   // localWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = unsignedShortValues[offset + 41];

                   // localWindingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber = unsignedShortValues[offset + 42];
                  //  localWindingHotSpotSetup.X_LoadCurrentPhaseBSourceRegisterNumber = unsignedShortValues[offset + 43];
                  //  localWindingHotSpotSetup.X_LoadCurrentPhaseCSourceRegisterNumber = unsignedShortValues[offset + 44];

                   // localWindingHotSpotSetup.Y_LoadCurrentPhaseASourceRegisterNumber = unsignedShortValues[offset + 45];
                  //  localWindingHotSpotSetup.Y_LoadCurrentPhaseBSourceRegisterNumber = unsignedShortValues[offset + 46];
                  //  localWindingHotSpotSetup.Y_LoadCurrentPhaseCSourceRegisterNumber = unsignedShortValues[offset + 47];

                    // these are defined as Int16s in the spreadsheet
                   

                    

                    // code changes by cfk 12/17/13
                    //-------------------------------------
                    /* month = (int)unsignedShortValues[offset + 53];
                    day = (int)unsignedShortValues[offset + 54];
                    year = (int)unsignedShortValues[offset + 55];

                    /// We have already initialized dataOfLastFanExercise to MinimumDateTime above, so if this
                    /// test fails we need not assign the value again.
                    if ((year > 1999) && (year < 2100) && (month > 0) && (month < 13) && (day > 0) && (day < 32))
                    {
                        DateTime dateOfLastFanExerciseFromRegisterValues = new DateTime(year, month, day);
                        if (dateOfLastFanExerciseFromRegisterValues.CompareTo(dateOfLastFanExercise) > 0)
                        {
                            dateOfLastFanExercise = dateOfLastFanExerciseFromRegisterValues;
                        }
                    }
                   */
                    //-------------------------------------------------------

                    
                  //  localWindingHotSpotSetup.DateOfLastFanExercise = dateOfLastFanExercise;
                    
                    
                    
                    
                   

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSConfiguration.ExtractWhsSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return localWindingHotSpotSetup;
        }

        public static void ConvertWindingHotSpotSetupToShorts(Main_ConfigComponent_WindingHotSpotSetup inputWindingHotSpotSetup, ref Int16[] shortValues)
        {
            try
            {
                int offset;
                //UInt16[] unsignedShortValues;
                //Int16 convertedUnsignedShortValues;
                if (inputWindingHotSpotSetup != null)
                {
                    if (shortValues != null)
                    {
                        
                        /// this value is correct as long as this conversion remains independent from the Main_Configuration.  In that case the offset would be
                        /// 5450
                        offset = 0;
                       
                        if (shortValues.Length >= (offset + 60))
                        {
                           // unsignedShortValues = new UInt16[

                            /// there are a bunch of values that are treated as UInt16 in the registers, but we can only write Int16 values to the device
                            shortValues[offset + 0] = ConversionMethods.UInt16BytesToInt16Value(ConversionMethods.ConvertSixteenBitEncodedStringToUInt16(inputWindingHotSpotSetup.RatingAsBitString));

                            shortValues[offset + 1] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseA);
                            shortValues[offset + 2] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseB);
                            shortValues[offset + 3] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseC);
                            shortValues[offset + 4] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.HotSpotFactor);
                            shortValues[offset + 5] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA);
                            shortValues[offset + 6] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB);
                            shortValues[offset + 7] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC);                           
                            shortValues[offset + 8] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber);
                            shortValues[offset + 9] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber);
                            shortValues[offset + 10] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber);
                            shortValues[offset + 11] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber);
                            shortValues[offset + 12] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.Exponent);
                            shortValues[offset + 13] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.Exponent_B);
                            shortValues[offset + 14] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.Exponent_C);
                            shortValues[offset + 15] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.WindingTimeConstant);
                            shortValues[offset + 16] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.WindingTimeConstant_B);
                            shortValues[offset + 17] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.WindingTimeConstant_C);
                            shortValues[offset + 18] = (Int16)inputWindingHotSpotSetup.FanSetpoint_H;
                            shortValues[offset + 19] = (Int16)inputWindingHotSpotSetup.FanSetpoint_HH;
                            shortValues[offset + 20] = (Int16)inputWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                            shortValues[offset + 21] = (Int16)inputWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;
                            shortValues[offset + 22] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.MinimumFanRunTimeInMinutes);
                            shortValues[offset + 23] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.TimeBetweenFanExerciseInDays);
                            shortValues[offset + 24] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.TimeToExerciseFanInMinutes);
                            shortValues[offset + 25] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.whsFan1NumberOfStarts);
                            shortValues[offset + 26] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.whsFan2NumberOfStarts);
                            shortValues[offset + 27] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.fanTest);                           
                            shortValues[offset + 28] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.FanOneRuntimeInHours);
                            shortValues[offset + 29] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.FanTwoRuntimeInHours);
                            shortValues[offset + 30] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.FanBankSwitchingTimeInHours);
                            shortValues[offset + 31] = (Int16)inputWindingHotSpotSetup.fan_1BaseCurrent;
                            shortValues[offset + 32] = (Int16)inputWindingHotSpotSetup.fan_2BaseCurrent;
                            shortValues[offset + 33] = (Int16)inputWindingHotSpotSetup.fan_lowLimitAlarm;
                            shortValues[offset + 34] = (Int16)inputWindingHotSpotSetup.fan_highLimitAlarm;
                            shortValues[offset + 35] = (Int16)inputWindingHotSpotSetup.NumberOfCoolingGroups;                            
                            shortValues[offset + 36] = (Int16)inputWindingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                            shortValues[offset + 37] = (Int16)inputWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                            shortValues[offset + 38] = (Int16)inputWindingHotSpotSetup.AlarmTopOilTemperature_H;
                            shortValues[offset + 39] = (Int16)inputWindingHotSpotSetup.AlarmTopOilTemperature_HH;
                            shortValues[offset + 40] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.AlarmDeadBandTemperature);
                            shortValues[offset + 41] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.AlarmDelayInSeconds);
                            shortValues[offset + 42] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.whsPreviousAgingPhaseA);
                            shortValues[offset + 43] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.whsPreviousAgingPhaseB);
                            shortValues[offset + 44] = ConversionMethods.UInt16BytesToInt16Value((UInt16)inputWindingHotSpotSetup.whsPreviousAgingPhaseC);
                            
                           
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Main_ConfigComponent_InputChannelEffluvia was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #region Copy Configuration

        public static Main_WHSConfiguration CopyConfiguration(Main_WHSConfiguration inputConfiguration)
        {
            Main_WHSConfiguration copyOfConfiguration = null;
            try
            {
                Int16[] registerData = null;
                if (inputConfiguration != null)
                {
                    registerData = inputConfiguration.GetShortValuesFromAllContributors();
                    if (registerData != null)
                    {
                        copyOfConfiguration = new Main_WHSConfiguration(registerData, inputConfiguration.windingHotSpotSetup.FirmwareVersion);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyCalibrationDataList(List<Main_ConfigComponent_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfConfiguration;
        }

        private Main_ConfigComponent_WindingHotSpotSetup CopyWindingHotSpotSetup(Main_ConfigComponent_WindingHotSpotSetup inputWindingHotSpotSetup)
        {
            Main_ConfigComponent_WindingHotSpotSetup copyOfWindingHotSpotSetup = null;

            try
            {
                if (inputWindingHotSpotSetup != null)
                {
                    copyOfWindingHotSpotSetup = new Main_ConfigComponent_WindingHotSpotSetup();

                    copyOfWindingHotSpotSetup.FirmwareVersion = inputWindingHotSpotSetup.FirmwareVersion;
                    copyOfWindingHotSpotSetup.RatingAsBitString = inputWindingHotSpotSetup.RatingAsBitString;
                    copyOfWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseA;
                    copyOfWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseB;
                    copyOfWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = inputWindingHotSpotSetup.H_RatedHotSpotRisePhaseC;
                    copyOfWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA;
                    copyOfWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB;
                    copyOfWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = inputWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC;
                    copyOfWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = inputWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber;
                    copyOfWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = inputWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber;
                    copyOfWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = inputWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber;
                    copyOfWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = inputWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber;
                    copyOfWindingHotSpotSetup.Exponent = inputWindingHotSpotSetup.Exponent;
                    copyOfWindingHotSpotSetup.Exponent_B = inputWindingHotSpotSetup.Exponent_B;
                    copyOfWindingHotSpotSetup.Exponent_C = inputWindingHotSpotSetup.Exponent_C;
                    copyOfWindingHotSpotSetup.WindingTimeConstant = inputWindingHotSpotSetup.WindingTimeConstant;
                    copyOfWindingHotSpotSetup.WindingTimeConstant_B = inputWindingHotSpotSetup.WindingTimeConstant_B;
                    copyOfWindingHotSpotSetup.WindingTimeConstant_C = inputWindingHotSpotSetup.WindingTimeConstant_C;                    
                    copyOfWindingHotSpotSetup.FanSetpoint_H = inputWindingHotSpotSetup.FanSetpoint_H;
                    copyOfWindingHotSpotSetup.FanSetpoint_HH = inputWindingHotSpotSetup.FanSetpoint_HH;
                    copyOfWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = inputWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                    copyOfWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = inputWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;
                    copyOfWindingHotSpotSetup.MinimumFanRunTimeInMinutes = inputWindingHotSpotSetup.MinimumFanRunTimeInMinutes;
                    copyOfWindingHotSpotSetup.TimeBetweenFanExerciseInDays = inputWindingHotSpotSetup.TimeBetweenFanExerciseInDays;
                    copyOfWindingHotSpotSetup.TimeToExerciseFanInMinutes = inputWindingHotSpotSetup.TimeToExerciseFanInMinutes;
                    copyOfWindingHotSpotSetup.whsFan1NumberOfStarts = inputWindingHotSpotSetup.whsFan1NumberOfStarts;
                    copyOfWindingHotSpotSetup.whsFan2NumberOfStarts = inputWindingHotSpotSetup.whsFan2NumberOfStarts;
                    copyOfWindingHotSpotSetup.fanTest = inputWindingHotSpotSetup.fanTest;
                    copyOfWindingHotSpotSetup.FanOneRuntimeInHours = inputWindingHotSpotSetup.FanOneRuntimeInHours;
                    copyOfWindingHotSpotSetup.FanTwoRuntimeInHours = inputWindingHotSpotSetup.FanTwoRuntimeInHours;
                    copyOfWindingHotSpotSetup.FanBankSwitchingTimeInHours = inputWindingHotSpotSetup.FanBankSwitchingTimeInHours;
                    copyOfWindingHotSpotSetup.fan_1BaseCurrent = inputWindingHotSpotSetup.fan_1BaseCurrent;
                    copyOfWindingHotSpotSetup.fan_2BaseCurrent = inputWindingHotSpotSetup.fan_2BaseCurrent;                 
                    copyOfWindingHotSpotSetup.fan_lowLimitAlarm = inputWindingHotSpotSetup.fan_lowLimitAlarm;
                    copyOfWindingHotSpotSetup.fan_highLimitAlarm = inputWindingHotSpotSetup.fan_highLimitAlarm;
                    copyOfWindingHotSpotSetup.NumberOfCoolingGroups = inputWindingHotSpotSetup.NumberOfCoolingGroups;
                    copyOfWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = inputWindingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                    copyOfWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = inputWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                    copyOfWindingHotSpotSetup.AlarmTopOilTemperature_H = inputWindingHotSpotSetup.AlarmTopOilTemperature_H;
                    copyOfWindingHotSpotSetup.AlarmTopOilTemperature_HH = inputWindingHotSpotSetup.AlarmTopOilTemperature_HH;
                    copyOfWindingHotSpotSetup.AlarmDeadBandTemperature = inputWindingHotSpotSetup.AlarmDeadBandTemperature;
                    copyOfWindingHotSpotSetup.AlarmDelayInSeconds = inputWindingHotSpotSetup.AlarmDelayInSeconds;
                    copyOfWindingHotSpotSetup.whsPreviousAgingPhaseA = inputWindingHotSpotSetup.whsPreviousAgingPhaseA;
                    copyOfWindingHotSpotSetup.whsPreviousAgingPhaseB = inputWindingHotSpotSetup.whsPreviousAgingPhaseB;
                    copyOfWindingHotSpotSetup.whsPreviousAgingPhaseC = inputWindingHotSpotSetup.whsPreviousAgingPhaseC;
                    
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSConfiguration.CopyWindingHotSpotSetup(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfWindingHotSpotSetup;
        }

        #endregion

        public bool ConfigurationIsTheSame(Main_WHSConfiguration externalConfiguration)
        {
            bool configurationIsTheSame = false;
            try
            {
                configurationIsTheSame = WindingHotSpotSetupIsTheSame(externalConfiguration.windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSConfiguration.ConfigurationIsTheSame(Main_WHSConfiguration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationIsTheSame;
        }

        private bool WindingHotSpotSetupIsTheSame(Main_ConfigComponent_WindingHotSpotSetup externalWindingHotSpotSetup)
        {
           
            bool isTheSame = true;

            if (windingHotSpotSetup.RatingAsBitString.CompareTo(externalWindingHotSpotSetup.RatingAsBitString) != 0)
            {
                isTheSame = false;
            }

            if (isTheSame && (windingHotSpotSetup.H_RatedHotSpotRisePhaseA != externalWindingHotSpotSetup.H_RatedHotSpotRisePhaseA))
            {
                isTheSame = false;
            }

            if (isTheSame && (windingHotSpotSetup.H_RatedHotSpotRisePhaseB != externalWindingHotSpotSetup.H_RatedHotSpotRisePhaseB))
            {
                isTheSame = false;
            }

            if (isTheSame && (windingHotSpotSetup.H_RatedHotSpotRisePhaseC != externalWindingHotSpotSetup.H_RatedHotSpotRisePhaseC))
            {
                isTheSame = false;
            }

            if (isTheSame && (windingHotSpotSetup.H_MaximumRatedCurrentPhaseA != externalWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.H_MaximumRatedCurrentPhaseB != externalWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.H_MaximumRatedCurrentPhaseC != externalWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber != externalWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber != externalWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber != externalWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber != externalWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber))
            {
                isTheSame = false;
            }

            if (isTheSame && (windingHotSpotSetup.Exponent != externalWindingHotSpotSetup.Exponent))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.Exponent_B != externalWindingHotSpotSetup.Exponent_B))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.Exponent_C != externalWindingHotSpotSetup.Exponent_C))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.WindingTimeConstant != externalWindingHotSpotSetup.WindingTimeConstant))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.WindingTimeConstant_B != externalWindingHotSpotSetup.WindingTimeConstant_B))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.WindingTimeConstant_C != externalWindingHotSpotSetup.WindingTimeConstant_C))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.FanSetpoint_H != externalWindingHotSpotSetup.FanSetpoint_H))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.FanSetpoint_HH != externalWindingHotSpotSetup.FanSetpoint_HH))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.TopOilTemperatureFanSetpoint_H != externalWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH != externalWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.MinimumFanRunTimeInMinutes != externalWindingHotSpotSetup.MinimumFanRunTimeInMinutes))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.TimeBetweenFanExerciseInDays != externalWindingHotSpotSetup.TimeBetweenFanExerciseInDays))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.TimeToExerciseFanInMinutes != externalWindingHotSpotSetup.TimeToExerciseFanInMinutes))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.FanBankSwitchingTimeInHours != externalWindingHotSpotSetup.FanBankSwitchingTimeInHours))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.fan_lowLimitAlarm != externalWindingHotSpotSetup.fan_lowLimitAlarm))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.fan_highLimitAlarm != externalWindingHotSpotSetup.fan_highLimitAlarm))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.NumberOfCoolingGroups != externalWindingHotSpotSetup.NumberOfCoolingGroups))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmMaxSetPointTemperature_H != externalWindingHotSpotSetup.AlarmMaxSetPointTemperature_H))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmMaxSetPointTemperature_HH != externalWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmTopOilTemperature_H != externalWindingHotSpotSetup.AlarmTopOilTemperature_H))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmTopOilTemperature_HH != externalWindingHotSpotSetup.AlarmTopOilTemperature_HH))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmDeadBandTemperature != externalWindingHotSpotSetup.AlarmDeadBandTemperature))
            {
                isTheSame = false;
            }
            if (isTheSame && (windingHotSpotSetup.AlarmDelayInSeconds != externalWindingHotSpotSetup.AlarmDelayInSeconds))
            {
                isTheSame = false;
            }
            
            if (isTheSame && (windingHotSpotSetup.FanBankSwitchingTimeInHours != externalWindingHotSpotSetup.FanBankSwitchingTimeInHours))
            {
                isTheSame = false;
            }

            return isTheSame;
        }


        private Main_ConfigComponent_WindingHotSpotSetup InitializeWindingHotSpotSetup()
        {
            Main_ConfigComponent_WindingHotSpotSetup localWindingHotSpotSetup = null;
            try
            {
                localWindingHotSpotSetup = new Main_ConfigComponent_WindingHotSpotSetup();

                localWindingHotSpotSetup.FirmwareVersion = 0;

                localWindingHotSpotSetup.RatingAsBitString = "0000000000000000";

                localWindingHotSpotSetup.H_RatedHotSpotRisePhaseA = 300;
                localWindingHotSpotSetup.H_RatedHotSpotRisePhaseB = 300;
                localWindingHotSpotSetup.H_RatedHotSpotRisePhaseC = 300;
/*
                localWindingHotSpotSetup.X_RatedHotSpotRisePhaseA = 300;
                localWindingHotSpotSetup.X_RatedHotSpotRisePhaseB = 300;
                localWindingHotSpotSetup.X_RatedHotSpotRisePhaseC = 300;

                localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseA = 300;
                localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseB = 300;
                localWindingHotSpotSetup.Y_RatedHotSpotRisePhaseC = 300;

                localWindingHotSpotSetup.C_RatedHotSpotRisePhaseA = 300;
                localWindingHotSpotSetup.C_RatedHotSpotRisePhaseB = 300;
                localWindingHotSpotSetup.C_RatedHotSpotRisePhaseC = 300;
*/
                localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseA = 250;
                localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseB = 250;
                localWindingHotSpotSetup.H_MaximumRatedCurrentPhaseC = 250;
/*
                localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseA = 250;
                localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseB = 250;
                localWindingHotSpotSetup.X_MaximumRatedCurrentPhaseC = 250;

                localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseA = 250;
                localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseB = 250;
                localWindingHotSpotSetup.Y_MaximumRatedCurrentPhaseC = 250;

                localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseA = 250;
                localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseB = 250;
                localWindingHotSpotSetup.C_MaximumRatedCurrentPhaseC = 250;
*/
                // these are defined as Int16s in the spreadsheet
                localWindingHotSpotSetup.FanSetpoint_H = 65;
                localWindingHotSpotSetup.FanSetpoint_HH = 75;

                localWindingHotSpotSetup.Exponent = 8;
                localWindingHotSpotSetup.WindingTimeConstant = 60;
//                localWindingHotSpotSetup.PreviousAging = 0;

                localWindingHotSpotSetup.AlarmDeadBandTemperature = 5;
                localWindingHotSpotSetup.AlarmDelayInSeconds = 60;

                // these are defined as Int16s in the spreadsheet
                localWindingHotSpotSetup.AlarmMaxSetPointTemperature_H = 100;
                localWindingHotSpotSetup.AlarmMaxSetPointTemperature_HH = 115;

                // these are defined as Int16s in the spreadsheet
                localWindingHotSpotSetup.AlarmTopOilTemperature_H = 80;
                localWindingHotSpotSetup.AlarmTopOilTemperature_HH = 90;

                // On the spreadsheet the following values are stated as being multiplied by 10
                // on the machine.  I was informed later that was incorrect.
                localWindingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = 17;
//                localWindingHotSpotSetup.TopOilTempPhaseBSourceRegisterNumber = 0;
//                localWindingHotSpotSetup.TopOilTempPhaseCSourceRegisterNumber = 0;

                localWindingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = 10;
                localWindingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = 11;
                localWindingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = 12;
/*
                localWindingHotSpotSetup.X_LoadCurrentPhaseASourceRegisterNumber = 6035;
                localWindingHotSpotSetup.X_LoadCurrentPhaseBSourceRegisterNumber = 0;
                localWindingHotSpotSetup.X_LoadCurrentPhaseCSourceRegisterNumber = 0;

                localWindingHotSpotSetup.Y_LoadCurrentPhaseASourceRegisterNumber = 6041;
                localWindingHotSpotSetup.Y_LoadCurrentPhaseBSourceRegisterNumber = 0;
                localWindingHotSpotSetup.Y_LoadCurrentPhaseCSourceRegisterNumber = 0;
*/
                // these are defined as Int16s in the spreadsheet
                localWindingHotSpotSetup.TopOilTemperatureFanSetpoint_H = 60;
                localWindingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = 70;

                localWindingHotSpotSetup.MinimumFanRunTimeInMinutes = 5;
                localWindingHotSpotSetup.TimeBetweenFanExerciseInDays = 0;
                localWindingHotSpotSetup.TimeToExerciseFanInMinutes = 0;

               // localWindingHotSpotSetup.DateOfLastFanExercise = ConversionMethods.MinimumDateTime();

                localWindingHotSpotSetup.FanOneRuntimeInHours = 0;
                localWindingHotSpotSetup.FanTwoRuntimeInHours = 0;

              //  localWindingHotSpotSetup.FanBankSwitchingTimeInHours = 0;
                

                //code changes, cfk 12/17/13
                //-------------------------
                localWindingHotSpotSetup.whsPreviousAgingPhaseA = 0;
                localWindingHotSpotSetup.whsPreviousAgingPhaseB = 0;
                localWindingHotSpotSetup.whsPreviousAgingPhaseC = 0;
                localWindingHotSpotSetup.whsFan1NumberOfStarts = 0;
                localWindingHotSpotSetup.whsFan2NumberOfStarts = 0;
                localWindingHotSpotSetup.fanTest = 0;
                localWindingHotSpotSetup.HotSpotFactor = 13;
                localWindingHotSpotSetup.Exponent_B = 8;
                localWindingHotSpotSetup.Exponent_C = 8;
                localWindingHotSpotSetup.WindingTimeConstant_B = 60;
                localWindingHotSpotSetup.WindingTimeConstant_C = 60;
                localWindingHotSpotSetup.fan_lowLimitAlarm = 85;
                localWindingHotSpotSetup.fan_highLimitAlarm = 115;
                localWindingHotSpotSetup.NumberOfCoolingGroups = 2;
                localWindingHotSpotSetup.fan_1BaseCurrent = 0;
                localWindingHotSpotSetup.fan_2BaseCurrent = 0;





                //-----------------------------------------------


               // localWindingHotSpotSetup.Reserved = 0;
               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSConfiguration.ExtractWhsSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return localWindingHotSpotSetup;
        }
    }
}
