﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using GeneralUtilities;

namespace ConfigurationObjects
{
    public class BHM_Configuration
    {
        private static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 998;
        public static int expectedLengthOfBHMConfigurationArray
        {
            get
            {
                return EXPECTED_LENGTH_OF_BYTE_ARRAY;
            }
        }

        // public BHM_Config_BaseLineZk baseLineZk;
        public BHM_ConfigComponent_CalibrationCoeff calibrationCoeff;
        public BHM_ConfigComponent_GammaSetupInfo gammaSetupInfo;
        public BHM_ConfigComponent_GammaSideSetup gammaSideSetupSideOne;
        public BHM_ConfigComponent_GammaSideSetup gammaSideSetupSideTwo;
        public BHM_ConfigComponent_InitialParameters initialParameters;
        public BHM_ConfigComponent_InitialZkParameters initialZkParameters;
        public List<BHM_ConfigComponent_MeasurementsInfo> measurementsInfoList;
        public BHM_ConfigComponent_SetupInfo setupInfo;
        public BHM_ConfigComponent_SideToSideData sideToSideData;
        public BHM_ConfigComponent_TrSideParameters trSideParamSideOne;
        public BHM_ConfigComponent_TrSideParameters trSideParamSideTwo;
        public BHM_ConfigComponent_ZkSetupInfo zkSetupInfo;
        public BHM_ConfigComponent_ZkSideToSideSetup zkSideToSideSetup;

        double epsilon = 1.0e-5;

        public BHM_Configuration(BHM_ConfigComponent_CalibrationCoeff inputCalibrationCoeff, BHM_ConfigComponent_GammaSetupInfo inputGammaSetupInfo,
                                 BHM_ConfigComponent_GammaSideSetup inputGammaSideSetupSideOne, BHM_ConfigComponent_GammaSideSetup inputGammaSideSetupSideTwo,
                                 BHM_ConfigComponent_InitialParameters inputInitialParameters, BHM_ConfigComponent_InitialZkParameters inputInitialZkParameters,
                                 List<BHM_ConfigComponent_MeasurementsInfo> inputMeasurementsInfoList, BHM_ConfigComponent_SetupInfo inputSetupInfo,
                                 BHM_ConfigComponent_SideToSideData inputSideToSideData, BHM_ConfigComponent_TrSideParameters inputTrSideParamsSideOne,
                                 BHM_ConfigComponent_TrSideParameters inputTrSideParamsSideTwo, BHM_ConfigComponent_ZkSetupInfo inputZkSetupInfo,
                                 BHM_ConfigComponent_ZkSideToSideSetup inputZkSideToSideSetup)
        {
            //if ((inputMeasurementsInfoList != null) && (inputMeasurementsInfoList.Count == 50))
            //{
            try
            {
                calibrationCoeff = inputCalibrationCoeff;
                gammaSetupInfo = inputGammaSetupInfo;
                gammaSideSetupSideOne = inputGammaSideSetupSideOne;
                gammaSideSetupSideTwo = inputGammaSideSetupSideTwo;
                initialParameters = inputInitialParameters;
                initialZkParameters = inputInitialZkParameters;
                measurementsInfoList = inputMeasurementsInfoList;
                setupInfo = inputSetupInfo;
                sideToSideData = inputSideToSideData;
                trSideParamSideOne = inputTrSideParamsSideOne;
                trSideParamSideTwo = inputTrSideParamsSideTwo;
                zkSetupInfo = inputZkSetupInfo;
                zkSideToSideSetup = inputZkSideToSideSetup;
                if (!AllConfigurationMembersAreNonNull())
                {
                    string errorMessage = "Error in BHM_Configuration.BHM_Configuration(BHM_ConfigComponent_CalibrationCoeff, BHM_ConfigComponent_GammaSetupInfo, BHM_ConfigComponent_GammaSideSetup, BHM_ConfigComponent_GammaSideSetup, BHM_ConfigComponent_InitialParameters, BHM_ConfigComponent_InitialZkParameters, List<BHM_ConfigComponent_MeasurementsInfo>, BHM_ConfigComponent_SetupInfo, BHM_ConfigComponent_SideToSideData, BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_ZkSetupInfo, BHM_ConfigComponent_ZkSideToSideSetup)\nAt least one input object was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    SetAllMembersToNull();
                }
                //}
                //            else
                //            {
                //                string errorMessage = "Error in BHM_Configuration.BHM_Configuration(Buid, MonitorInterfaceDB)\nThe input list of BHM_ConfigComponent_MeasurementsInfo does not have exactly 50 entries";
                //                LogMessage.LogError(errorMessage);
                //#if DEBUG
                //                MessageBox.Show(errorMessage);
                //#endif
                //                SetAllMembersToNull();
                //            }              
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.BHM_Configuration(BHM_ConfigComponent_CalibrationCoeff, BHM_ConfigComponent_GammaSetupInfo, BHM_ConfigComponent_GammaSideSetup, BHM_ConfigComponent_GammaSideSetup, BHM_ConfigComponent_InitialParameters, BHM_ConfigComponent_InitialZkParameters, List<BHM_ConfigComponent_MeasurementsInfo>, BHM_ConfigComponent_SetupInfo, BHM_ConfigComponent_SideToSideData, BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_ZkSetupInfo, BHM_ConfigComponent_ZkSideToSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public BHM_Configuration(Byte[] byteData, double firmwareVersion)
        {
            try
            {
                SetConfiguration(byteData, firmwareVersion);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.BHM_Configuration(Byte[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public BHM_Configuration(Byte[] byteData)
        {
            try
            {
                SetConfiguration(byteData, 0.00);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.BHM_Configuration(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public BHM_Configuration()
        {
            try
            {
                calibrationCoeff = new BHM_ConfigComponent_CalibrationCoeff();
                gammaSetupInfo = new BHM_ConfigComponent_GammaSetupInfo();
                gammaSideSetupSideOne = new BHM_ConfigComponent_GammaSideSetup();
                gammaSideSetupSideTwo = new BHM_ConfigComponent_GammaSideSetup();
                initialParameters = new BHM_ConfigComponent_InitialParameters();
                initialZkParameters = new BHM_ConfigComponent_InitialZkParameters();
                measurementsInfoList = new List<BHM_ConfigComponent_MeasurementsInfo>();
                setupInfo = new BHM_ConfigComponent_SetupInfo();
                sideToSideData = new BHM_ConfigComponent_SideToSideData();
                trSideParamSideOne = new BHM_ConfigComponent_TrSideParameters();
                trSideParamSideTwo = new BHM_ConfigComponent_TrSideParameters();
                zkSetupInfo = new BHM_ConfigComponent_ZkSetupInfo();
                zkSideToSideSetup = new BHM_ConfigComponent_ZkSideToSideSetup();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.BHM_Configuration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void InitializeConfigurationToZeroes()
        {
            try
            {
                calibrationCoeff = InitializeEmptyCalibrationCoeffData();
                gammaSetupInfo = InitializeEmptyGammaSetupInfo();
                gammaSideSetupSideOne = InitializeEmptyGammaSideSetup();
                gammaSideSetupSideTwo = InitializeEmptyGammaSideSetup();
                initialParameters = InitializeEmptyInitialParameters();
                initialZkParameters = InitializeEmptyInitialZkParameters();
                measurementsInfoList = InitializeEmptyMeasurementsInfoList();
                setupInfo = InitializeEmptySetupInfo();
                sideToSideData = InitializeEmptySideToSideData();
                trSideParamSideOne = InitializeEmptyTrSideParam();
                trSideParamSideTwo = InitializeEmptyTrSideParam();
                zkSetupInfo = InitializeEmptyZkSetupInfo();
                zkSideToSideSetup = InitializeEmptyZkSideToSideSetup();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.BHM_Configuration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetConfiguration(Byte[] byteData, double firmwareVersion)
        {
            try
            {
                if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                {
                    calibrationCoeff = ExtractCalibrationCoeffDataFromByteData(byteData);
                    gammaSetupInfo = ExtractGammaSetupInfoDataFromByteData(byteData);
                    gammaSideSetupSideOne = ExtractGammaSideSetupDataFromByteData(byteData, 0);
                    gammaSideSetupSideTwo = ExtractGammaSideSetupDataFromByteData(byteData, 1);
                    initialParameters = ExtractInitialParametersDataFromByteData(byteData);
                    initialZkParameters = ExtractInitialZkParametersDataFromByteData(byteData);
                    measurementsInfoList = ExtractMeasurementsInfoDataFromByteData(byteData);
                    setupInfo = ExtractSetupInfoDataFromByteData(byteData);
                    sideToSideData = ExtractSideToSideDataDataFromByteData(byteData, 0);
                    trSideParamSideOne = ExtractTrSideParamDataFromByteData(byteData, 0);
                    trSideParamSideTwo = ExtractTrSideParamDataFromByteData(byteData, 1);
                    zkSetupInfo = ExtractZkSetupInfoDataFromByteData(byteData);
                    zkSideToSideSetup = ExtractZkSideToSideSetupDataFromByteData(byteData, 0);

                    setupInfo.FirmwareVersion = firmwareVersion;

                    if (!AllConfigurationMembersAreNonNull())
                    {
                        SetAllMembersToNull();
                    }
                }
                else
                {
                    SetAllMembersToNull();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.SetConfiguration(Byte[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Byte[] ConvertConfigurationToByteArray()
        {
            Byte[] byteData = null;
            try
            {
                if (AllConfigurationMembersAreNonNull())
                {
                    byteData = new Byte[EXPECTED_LENGTH_OF_BYTE_ARRAY];

                    ConvertCalibrationCoeffToBytes(calibrationCoeff, ref byteData);
                    ConvertGammaSetupInfoToBytes(gammaSetupInfo, ref byteData);
                    ConvertGammaSideSetupToBytes(gammaSideSetupSideOne, 0, ref byteData);
                    ConvertGammaSideSetupToBytes(gammaSideSetupSideTwo, 1, ref byteData);
                    ConvertInitialParametersToBytes(initialParameters, ref byteData);
                    ConvertInitialZkParametersToBytes(initialZkParameters, ref byteData);
                    ConvertMeasurementsInfoToBytes(measurementsInfoList, ref byteData);
                    ConvertSetupInfoToBytes(setupInfo, ref byteData);
                    ConvertSideToSideDataToBytes(sideToSideData, 0, ref byteData);
                    ConvertTrSideParamToBytes(trSideParamSideOne, 0, ref byteData);
                    ConvertTrSideParamToBytes(trSideParamSideTwo, 1, ref byteData);
                    ConvertZkSetupInfoToBytes(zkSetupInfo, ref byteData);
                    ConvertZkSideToSideSetupToBytes(zkSideToSideSetup, 0, ref byteData);
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertConfigurationToByteArray()\nAt least one piece of internal data was null, could not convert to bytes.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertConfigurationToByteArray()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteData;
        }

        private void SetAllMembersToNull()
        {
            try
            {
                calibrationCoeff = null;
                gammaSetupInfo = null;
                gammaSideSetupSideOne = null;
                gammaSideSetupSideTwo = null;
                initialParameters = null;
                initialZkParameters = null;
                measurementsInfoList = null;
                setupInfo = null;
                sideToSideData = null;
                trSideParamSideOne = null;
                trSideParamSideTwo = null;
                zkSetupInfo = null;
                zkSideToSideSetup = null;

                string errorMessage = "In BHM_Configuration.SetAllMembersToNull()\nAll components of the configuration have been set to null to make sure it will not be used.";
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.SetAllMembersToNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public bool AllConfigurationMembersAreNonNull()
        {
            bool allNonNull = true;
            try
            {
                StringBuilder errorMessage = new StringBuilder();

                errorMessage.Append("The following BHM_Configuration members are null: ");

                if (calibrationCoeff == null)
                {
                    allNonNull = false;
                    errorMessage.Append("calibrationCoeff ");
                }

                if (gammaSetupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("gammaSetupInfo ");
                }
                if (gammaSideSetupSideOne == null)
                {
                    allNonNull = false;
                    errorMessage.Append("gammaSideSetupSideOne ");
                }
                if (gammaSideSetupSideTwo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("gammaSideSetupSideTwo ");
                }
                if (initialParameters == null)
                {
                    allNonNull = false;
                    errorMessage.Append("initialParameters ");
                }
                if (initialZkParameters == null)
                {
                    allNonNull = false;
                    errorMessage.Append("initialZkParameters ");
                }
                if (measurementsInfoList == null)
                {
                    allNonNull = false;
                    errorMessage.Append("measurementsInfoList ");
                }
                if (setupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("setupInfo ");
                }
                if (sideToSideData == null)
                {
                    allNonNull = false;
                    errorMessage.Append("sideToSideData ");
                }
                if (trSideParamSideOne == null)
                {
                    allNonNull = false;
                    errorMessage.Append("trSideParamSideOne ");
                }
                if (trSideParamSideTwo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("trSideParamSideTwo ");
                }
                if (zkSetupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("zkSetupInfo ");
                }
                if (zkSideToSideSetup == null)
                {
                    allNonNull = false;
                    errorMessage.Append("zkSideToSideSetup ");
                }

                if (!allNonNull)
                {
                    LogMessage.LogError(errorMessage.ToString());
#if DEBUG
                    MessageBox.Show(errorMessage.ToString());
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.AllConfigurationMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allNonNull;
        }

        #region BHM Configuration Methods

        private BHM_ConfigComponent_CalibrationCoeff InitializeEmptyCalibrationCoeffData()
        {
            BHM_ConfigComponent_CalibrationCoeff calibrationCoeff = new BHM_ConfigComponent_CalibrationCoeff();
            try
            {
                calibrationCoeff.TemperatureK_0 = 1.039;
                calibrationCoeff.TemperatureK_1 = 1.039;
                calibrationCoeff.TemperatureK_2 = 1.039;
                calibrationCoeff.TemperatureK_3 = 1.039;
                calibrationCoeff.TemperatureB_0 = -.253;
                calibrationCoeff.TemperatureB_1 = -.253;
                calibrationCoeff.TemperatureB_2 = -.253;
                calibrationCoeff.TemperatureB_3 = -.253;
                calibrationCoeff.CurrentK_0 = 1.0;
                calibrationCoeff.CurrentK_1 = 1.0;
                calibrationCoeff.CurrentK_2 = 1.0;
                calibrationCoeff.CurrentK_3 = 1.0;
                calibrationCoeff.HumidityOffset_0 = 838.0;
                calibrationCoeff.HumidityOffset_1 = 838.0;
                calibrationCoeff.HumidityOffset_2 = 838.0;
                calibrationCoeff.HumidityOffset_3 = 838.0;
                calibrationCoeff.HumiditySlope_0 = 31.575;
                calibrationCoeff.HumiditySlope_1 = 31.575;
                calibrationCoeff.HumiditySlope_2 = 31.575;
                calibrationCoeff.HumiditySlope_3 = 31.575;
                calibrationCoeff.VoltageK_0_0 = 1.0;
                calibrationCoeff.VoltageK_0_1 = 1.0;
                calibrationCoeff.VoltageK_0_2 = 1.0;
                calibrationCoeff.VoltageK_1_0 = 1.0;
                calibrationCoeff.VoltageK_1_1 = 1.0;
                calibrationCoeff.VoltageK_1_2 = 1.0;
                calibrationCoeff.CurrentB_0 = 0;
                calibrationCoeff.CurrentB_1 = 0;
                calibrationCoeff.CurrentB_2 = 0;
                calibrationCoeff.CurrentB_3 = 0;
                calibrationCoeff.I4_20K = 0;
                calibrationCoeff.I4_20B = 0;
                calibrationCoeff.CurrentChannelOnPhase_0 = 0;
                calibrationCoeff.CurrentChannelOnPhase_1 = 0;
                calibrationCoeff.CurrentChannelOnPhase_2 = 0;
                calibrationCoeff.CurrentChannelOnPhase_3 = 0;
                calibrationCoeff.Reserved_0 = 187;
                calibrationCoeff.Reserved_1 = 223;
                calibrationCoeff.Reserved_2 = 35;
                calibrationCoeff.Reserved_3 = 215;
                calibrationCoeff.Reserved_4 = 129;
                calibrationCoeff.Reserved_5 = 69;
                calibrationCoeff.Reserved_6 = 174;
                calibrationCoeff.Reserved_7 = 139;
                calibrationCoeff.Reserved_8 = 167;
                calibrationCoeff.Reserved_9 = 174;
                calibrationCoeff.Reserved_10 = 77;
                calibrationCoeff.Reserved_11 = 95;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitializeEmptyCalibrationCoeffData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationCoeff;
        }

        /// <summary>
        /// Converts the appropriate entries in the byteData array to the variables in the BHM_Config_CalibrationCoeff database object.  byteData is an array of calibration parameters for a BHM.
        /// </summary>
        /// <param name="byteData"></param>
        /// <param name="monitorID"></param>
        /// <param name="extractDateTime"></param>
        /// <returns></returns>
        private BHM_ConfigComponent_CalibrationCoeff ExtractCalibrationCoeffDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_CalibrationCoeff calibrationCoeff = new BHM_ConfigComponent_CalibrationCoeff();
            try
            {
                int offset = 650;
                if (byteData != null)
                {
                    if (byteData.Length >= (offset + 144))
                    {
                        calibrationCoeff.TemperatureK_0 = ConversionMethods.BytesToSingle(byteData[offset], byteData[offset + 1], byteData[offset + 2], byteData[offset + 3]);
                        calibrationCoeff.TemperatureK_1 = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                        calibrationCoeff.TemperatureK_2 = ConversionMethods.BytesToSingle(byteData[offset + 8], byteData[offset + 9], byteData[offset + 10], byteData[offset + 11]);
                        calibrationCoeff.TemperatureK_3 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                        calibrationCoeff.TemperatureB_0 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                        calibrationCoeff.TemperatureB_1 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                        calibrationCoeff.TemperatureB_2 = ConversionMethods.BytesToSingle(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                        calibrationCoeff.TemperatureB_3 = ConversionMethods.BytesToSingle(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                        calibrationCoeff.CurrentK_0 = ConversionMethods.BytesToSingle(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);
                        calibrationCoeff.CurrentK_1 = ConversionMethods.BytesToSingle(byteData[offset + 36], byteData[offset + 37], byteData[offset + 38], byteData[offset + 39]);
                        calibrationCoeff.CurrentK_2 = ConversionMethods.BytesToSingle(byteData[offset + 40], byteData[offset + 41], byteData[offset + 42], byteData[offset + 43]);
                        calibrationCoeff.CurrentK_3 = ConversionMethods.BytesToSingle(byteData[offset + 44], byteData[offset + 45], byteData[offset + 46], byteData[offset + 47]);
                        calibrationCoeff.HumidityOffset_0 = ConversionMethods.BytesToSingle(byteData[offset + 48], byteData[offset + 49], byteData[offset + 50], byteData[offset + 51]);
                        calibrationCoeff.HumidityOffset_1 = ConversionMethods.BytesToSingle(byteData[offset + 52], byteData[offset + 53], byteData[offset + 54], byteData[offset + 55]);
                        calibrationCoeff.HumidityOffset_2 = ConversionMethods.BytesToSingle(byteData[offset + 56], byteData[offset + 57], byteData[offset + 58], byteData[offset + 59]);
                        calibrationCoeff.HumidityOffset_3 = ConversionMethods.BytesToSingle(byteData[offset + 60], byteData[offset + 61], byteData[offset + 62], byteData[offset + 63]);
                        calibrationCoeff.HumiditySlope_0 = ConversionMethods.BytesToSingle(byteData[offset + 64], byteData[offset + 65], byteData[offset + 66], byteData[offset + 67]);
                        calibrationCoeff.HumiditySlope_1 = ConversionMethods.BytesToSingle(byteData[offset + 68], byteData[offset + 69], byteData[offset + 70], byteData[offset + 71]);
                        calibrationCoeff.HumiditySlope_2 = ConversionMethods.BytesToSingle(byteData[offset + 72], byteData[offset + 73], byteData[offset + 74], byteData[offset + 75]);
                        calibrationCoeff.HumiditySlope_3 = ConversionMethods.BytesToSingle(byteData[offset + 76], byteData[offset + 77], byteData[offset + 78], byteData[offset + 79]);
                        calibrationCoeff.VoltageK_0_0 = ConversionMethods.BytesToSingle(byteData[offset + 80], byteData[offset + 81], byteData[offset + 82], byteData[offset + 83]);
                        calibrationCoeff.VoltageK_0_1 = ConversionMethods.BytesToSingle(byteData[offset + 84], byteData[offset + 85], byteData[offset + 86], byteData[offset + 87]);
                        calibrationCoeff.VoltageK_0_2 = ConversionMethods.BytesToSingle(byteData[offset + 88], byteData[offset + 89], byteData[offset + 90], byteData[offset + 91]);
                        calibrationCoeff.VoltageK_1_0 = ConversionMethods.BytesToSingle(byteData[offset + 92], byteData[offset + 93], byteData[offset + 94], byteData[offset + 95]);
                        calibrationCoeff.VoltageK_1_1 = ConversionMethods.BytesToSingle(byteData[offset + 96], byteData[offset + 97], byteData[offset + 98], byteData[offset + 99]);
                        calibrationCoeff.VoltageK_1_2 = ConversionMethods.BytesToSingle(byteData[offset + 100], byteData[offset + 101], byteData[offset + 102], byteData[offset + 103]);
                        calibrationCoeff.CurrentB_0 = ConversionMethods.BytesToSingle(byteData[offset + 104], byteData[offset + 105], byteData[offset + 106], byteData[offset + 107]);
                        calibrationCoeff.CurrentB_1 = ConversionMethods.BytesToSingle(byteData[offset + 108], byteData[offset + 109], byteData[offset + 110], byteData[offset + 111]);
                        calibrationCoeff.CurrentB_2 = ConversionMethods.BytesToSingle(byteData[offset + 112], byteData[offset + 113], byteData[offset + 114], byteData[offset + 115]);
                        calibrationCoeff.CurrentB_3 = ConversionMethods.BytesToSingle(byteData[offset + 116], byteData[offset + 117], byteData[offset + 118], byteData[offset + 119]);

                        /// BEGIN QUESTIONABLE CODE 

                        /// I was having trouble with SDG&E.  When they downloaded a configuration from a particular device, the database write failed because of a bad float value.
                        /// These two variables don't appear to serve any purpose currently.  For two different devices, I saw a value of -5.5E+32 and .9E-32 for I4_20K, and 90 and 0 
                        /// for I4_20B.  I have the feeling they were created for some new feature but are now not used, and are not even being set to an initial value of 0.  If the bits
                        /// making up the values are indeed random, it's not hard to believe that one could create a value that is incorrect and that would result in a NaN or the like.  
                        /// So, after asking a few people and not getting a response, I decided to zero them out for now.  DL 2012-10-12

                        //calibrationCoeff.I4_20K = ConversionMethods.BytesToSingle(byteData[offset + 120], byteData[offset + 121], byteData[offset + 122], byteData[offset + 123]);
                        //calibrationCoeff.I4_20B = ConversionMethods.BytesToSingle(byteData[offset + 124], byteData[offset + 125], byteData[offset + 126], byteData[offset + 127]);

                        calibrationCoeff.I4_20K = 0;
                        calibrationCoeff.I4_20B = 0;

                        /// END QUESTIONABLE CODE

                        calibrationCoeff.CurrentChannelOnPhase_0 = ConversionMethods.ByteToInt32(byteData[offset + 128]);
                        calibrationCoeff.CurrentChannelOnPhase_1 = ConversionMethods.ByteToInt32(byteData[offset + 129]);
                        calibrationCoeff.CurrentChannelOnPhase_2 = ConversionMethods.ByteToInt32(byteData[offset + 130]);
                        calibrationCoeff.CurrentChannelOnPhase_3 = ConversionMethods.ByteToInt32(byteData[offset + 131]);
                        calibrationCoeff.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 132]);
                        calibrationCoeff.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 133]);
                        calibrationCoeff.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 134]);
                        calibrationCoeff.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 135]);
                        calibrationCoeff.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 136]);
                        calibrationCoeff.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 137]);
                        calibrationCoeff.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 138]);
                        calibrationCoeff.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 139]);
                        calibrationCoeff.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 140]);
                        calibrationCoeff.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 141]);
                        calibrationCoeff.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 142]);
                        calibrationCoeff.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 143]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractCalibrationCoeffDataFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractCalibrationCoeffDataFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractCalibrationCoeffDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationCoeff;
        }

        /// <summary>
        /// Converts the values in a BHM_Config_CalibrationCoeff object to bytes and writes them to the appropriate place in byteData.  byteData is an array of calibration parameters for a BHM
        /// </summary>
        /// <param name="calibrationCoeff"></param>
        /// <param name="byteData"></param>
        private void ConvertCalibrationCoeffToBytes(BHM_ConfigComponent_CalibrationCoeff calibrationCoeff, ref Byte[] byteData)
        {
            try
            {
                int offset = 650;
                if (calibrationCoeff != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureK_0), 0, byteData, offset, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureK_1), 0, byteData, offset + 4, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureK_2), 0, byteData, offset + 8, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureK_3), 0, byteData, offset + 12, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureB_0), 0, byteData, offset + 16, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureB_1), 0, byteData, offset + 20, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureB_2), 0, byteData, offset + 24, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.TemperatureB_3), 0, byteData, offset + 28, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentK_0), 0, byteData, offset + 32, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentK_1), 0, byteData, offset + 36, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentK_2), 0, byteData, offset + 40, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentK_3), 0, byteData, offset + 44, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumidityOffset_0), 0, byteData, offset + 48, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumidityOffset_1), 0, byteData, offset + 52, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumidityOffset_2), 0, byteData, offset + 56, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumidityOffset_3), 0, byteData, offset + 60, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumiditySlope_0), 0, byteData, offset + 64, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumiditySlope_1), 0, byteData, offset + 68, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumiditySlope_2), 0, byteData, offset + 72, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.HumiditySlope_3), 0, byteData, offset + 76, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_0_0), 0, byteData, offset + 80, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_0_1), 0, byteData, offset + 84, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_0_2), 0, byteData, offset + 88, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_1_0), 0, byteData, offset + 92, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_1_1), 0, byteData, offset + 96, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.VoltageK_1_2), 0, byteData, offset + 100, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentB_0), 0, byteData, offset + 104, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentB_1), 0, byteData, offset + 108, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentB_2), 0, byteData, offset + 112, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.CurrentB_3), 0, byteData, offset + 116, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.I4_20K), 0, byteData, offset + 120, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)calibrationCoeff.I4_20B), 0, byteData, offset + 124, 4);
                            byteData[offset + 128] = ConversionMethods.Int32ToByte(calibrationCoeff.CurrentChannelOnPhase_0);
                            byteData[offset + 129] = ConversionMethods.Int32ToByte(calibrationCoeff.CurrentChannelOnPhase_1);
                            byteData[offset + 130] = ConversionMethods.Int32ToByte(calibrationCoeff.CurrentChannelOnPhase_2);
                            byteData[offset + 131] = ConversionMethods.Int32ToByte(calibrationCoeff.CurrentChannelOnPhase_3);
                            byteData[offset + 132] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_0);
                            byteData[offset + 133] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_1);
                            byteData[offset + 134] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_2);
                            byteData[offset + 135] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_3);
                            byteData[offset + 136] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_4);
                            byteData[offset + 137] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_5);
                            byteData[offset + 138] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_6);
                            byteData[offset + 139] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_7);
                            byteData[offset + 140] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_8);
                            byteData[offset + 141] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_9);
                            byteData[offset + 142] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_10);
                            byteData[offset + 143] = ConversionMethods.Int32ToByte(calibrationCoeff.Reserved_11);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_Config_TrSideParam, ref Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_Config_TrSideParam, ref Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_Config_TrSideParam, ref Byte[]):\nInput BHM_Config_TrSideParam was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertTrSideParamToBytes(BHM_Config_TrSideParam, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_GammaSideSetup InitializeEmptyGammaSideSetup()
        {
            BHM_ConfigComponent_GammaSideSetup gammaSideSetup = new BHM_ConfigComponent_GammaSideSetup();
            try
            {
                gammaSideSetup.AlarmEnable = 1;
                gammaSideSetup.GammaYellowThreshold = 0;
                gammaSideSetup.GammaRedThreshold = 0;
                gammaSideSetup.TempCoefficient = 0;
                gammaSideSetup.TrendAlarm = 0;
                gammaSideSetup.Temperature0 = 0;
                gammaSideSetup.InputC_0 = 108;
                gammaSideSetup.InputC_1 = 108;
                gammaSideSetup.InputC_2 = 108;
                gammaSideSetup.InputCoeff_0 = 3.5;
                gammaSideSetup.InputCoeff_1 = 3.5;
                gammaSideSetup.InputCoeff_2 = 3.5;
                gammaSideSetup.Tg0_0 = 0;
                gammaSideSetup.Tg0_1 = 0;
                gammaSideSetup.Tg0_2 = 0;
                gammaSideSetup.C0_0 = 0;
                gammaSideSetup.C0_1 = 0;
                gammaSideSetup.C0_2 = 0;
                gammaSideSetup.InputImpedance_0 = 0;
                gammaSideSetup.InputImpedance_1 = 0;
                gammaSideSetup.InputImpedance_2 = 0;
                gammaSideSetup.MinTemperature1 = 0;
                gammaSideSetup.AvgTemperature1 = 0;
                gammaSideSetup.MaxTemperature1 = 0;
                gammaSideSetup.B_0 = 0;
                gammaSideSetup.B_1 = 0;
                gammaSideSetup.B_2 = 0;
                gammaSideSetup.K_0 = 0;
                gammaSideSetup.K_1 = 0;
                gammaSideSetup.K_2 = 0;
                gammaSideSetup.InputVoltage_0 = 1.0;
                gammaSideSetup.InputVoltage_1 = 1.0;
                gammaSideSetup.InputVoltage_2 = 1.0;
                gammaSideSetup.STABLEDeltaTg_0 = 0;
                gammaSideSetup.STABLEDeltaTg_1 = 0;
                gammaSideSetup.STABLEDeltaTg_2 = 0;
                gammaSideSetup.STABLEDeltaC_0 = 0;
                gammaSideSetup.STABLEDeltaC_1 = 0;
                gammaSideSetup.STABLEDeltaC_2 = 0;
                gammaSideSetup.STABLEDate = 382233240;
                gammaSideSetup.HeatDate = 0;
                gammaSideSetup.STABLETemperature = 64;
                gammaSideSetup.STABLETg_0 = 35;
                gammaSideSetup.STABLETg_1 = 35;
                gammaSideSetup.STABLETg_2 = 35;
                gammaSideSetup.STABLEC_0 = 5000;
                gammaSideSetup.STABLEC_1 = 5000;
                gammaSideSetup.STABLEC_2 = 5000;
                gammaSideSetup.STABLESaved = 1;
                gammaSideSetup.RatedVoltage = 230.0;
                gammaSideSetup.RatedCurrent = 5.0;
                gammaSideSetup.StablePhaseAmplitude_0 = 2483;
                gammaSideSetup.StablePhaseAmplitude_1 = 2603;
                gammaSideSetup.StablePhaseAmplitude_2 = 2596;
                gammaSideSetup.StableSourceAmplitude_0 = 20943;
                gammaSideSetup.StableSourceAmplitude_1 = 8525;
                gammaSideSetup.StableSourceAmplitude_2 = 30800;
                gammaSideSetup.StableSignalPhase_0 = 0;
                gammaSideSetup.StableSignalPhase_1 = 12025;
                gammaSideSetup.StableSignalPhase_2 = 24027;
                gammaSideSetup.StableSourcePhase_0 = 29797;
                gammaSideSetup.StableSourcePhase_1 = 34804;
                gammaSideSetup.StableSourcePhase_2 = 26050;
                gammaSideSetup.STABLEAvgCurrent = 34;
                gammaSideSetup.TgYellowThreshold = 0;
                gammaSideSetup.TgRedThreshold = 0;
                gammaSideSetup.TgVariationThreshold = 0;
                gammaSideSetup.ImpedanceValue_0 = 93;
                gammaSideSetup.ImpedanceValue_1 = 138;
                gammaSideSetup.ImpedanceValue_2 = 151;
                gammaSideSetup.TemperatureConfig = 0;
                gammaSideSetup.ExtImpedanceValue_0 = 32;
                gammaSideSetup.ExtImpedanceValue_1 = 26;
                gammaSideSetup.ExtImpedanceValue_2 = 50;
                gammaSideSetup.ExternalSync = 0;

                gammaSideSetup.Reserved_0 = 234;
                gammaSideSetup.Reserved_1 = 164;
                gammaSideSetup.Reserved_2 = 19;
                gammaSideSetup.Reserved_3 = 208;
                gammaSideSetup.Reserved_4 = 62;
                gammaSideSetup.Reserved_5 = 130;
                gammaSideSetup.Reserved_6 = 157;
                gammaSideSetup.Reserved_7 = 214;
                gammaSideSetup.Reserved_8 = 252;
                gammaSideSetup.Reserved_9 = 99;
                gammaSideSetup.Reserved_10 = 121;
                gammaSideSetup.Reserved_11 = 149;
                gammaSideSetup.Reserved_12 = 133;
                gammaSideSetup.Reserved_13 = 152;
                gammaSideSetup.Reserved_14 = 192;
                gammaSideSetup.Reserved_15 = 196;
                gammaSideSetup.Reserved_16 = 28;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitializeEmptyGammaSideSetup()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSideSetup;
        }

        /// <summary>
        /// Converts the appropriate parts of the byteData to values in a BHM_Config_GammaSideSetup object for one side number
        /// </summary>
        /// <param name="byteData"></param>
        /// <param name="monitorID"></param>
        /// <param name="sideNumber"></param>
        /// <param name="extractDateTime"></param>
        /// <returns></returns>
        private BHM_ConfigComponent_GammaSideSetup ExtractGammaSideSetupDataFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_ConfigComponent_GammaSideSetup gammaSideSetup = null;
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 158;
                }
                else if (sideNumber == 1)
                {
                    offset = 337;
                }
                if (offset != -1)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= (offset + 179))
                        {
                            gammaSideSetup = new BHM_ConfigComponent_GammaSideSetup();

                            gammaSideSetup.AlarmEnable = ConversionMethods.ByteToInt32(byteData[offset]);
                            gammaSideSetup.GammaYellowThreshold = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                            gammaSideSetup.GammaRedThreshold = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                            gammaSideSetup.TempCoefficient = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                            gammaSideSetup.TrendAlarm = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                            gammaSideSetup.Temperature0 = ConversionMethods.ByteToInt32(byteData[offset + 5]);
                            gammaSideSetup.InputC_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 6], byteData[offset + 7]);
                            gammaSideSetup.InputC_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 8], byteData[offset + 9]);
                            gammaSideSetup.InputC_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 10], byteData[offset + 11]);
                            gammaSideSetup.InputCoeff_0 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            gammaSideSetup.InputCoeff_1 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            gammaSideSetup.InputCoeff_2 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                            gammaSideSetup.Tg0_0 = ConversionMethods.ShortBytesToInt16(byteData[offset + 24], byteData[offset + 25]);
                            gammaSideSetup.Tg0_1 = ConversionMethods.ShortBytesToInt16(byteData[offset + 26], byteData[offset + 27]);
                            gammaSideSetup.Tg0_2 = ConversionMethods.ShortBytesToInt16(byteData[offset + 28], byteData[offset + 29]);
                            gammaSideSetup.C0_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 30], byteData[offset + 31]);
                            gammaSideSetup.C0_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 32], byteData[offset + 33]);
                            gammaSideSetup.C0_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 34], byteData[offset + 35]);
                            gammaSideSetup.InputImpedance_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 36], byteData[offset + 37]);
                            gammaSideSetup.InputImpedance_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 38], byteData[offset + 39]);
                            gammaSideSetup.InputImpedance_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 40], byteData[offset + 41]);
                            gammaSideSetup.MinTemperature1 = ConversionMethods.ByteToInt32(byteData[offset + 42]);
                            gammaSideSetup.AvgTemperature1 = ConversionMethods.ByteToInt32(byteData[offset + 43]);
                            gammaSideSetup.MaxTemperature1 = ConversionMethods.ByteToInt32(byteData[offset + 44]);
                            gammaSideSetup.B_0 = ConversionMethods.BytesToSingle(byteData[offset + 45], byteData[offset + 46], byteData[offset + 47], byteData[offset + 48]);
                            gammaSideSetup.B_1 = ConversionMethods.BytesToSingle(byteData[offset + 49], byteData[offset + 50], byteData[offset + 51], byteData[offset + 52]);
                            gammaSideSetup.B_2 = ConversionMethods.BytesToSingle(byteData[offset + 53], byteData[offset + 54], byteData[offset + 55], byteData[offset + 56]);
                            gammaSideSetup.K_0 = ConversionMethods.BytesToSingle(byteData[offset + 57], byteData[offset + 58], byteData[offset + 59], byteData[offset + 60]);
                            gammaSideSetup.K_1 = ConversionMethods.BytesToSingle(byteData[offset + 61], byteData[offset + 62], byteData[offset + 63], byteData[offset + 64]);
                            gammaSideSetup.K_2 = ConversionMethods.BytesToSingle(byteData[offset + 65], byteData[offset + 66], byteData[offset + 67], byteData[offset + 68]);
                            gammaSideSetup.InputVoltage_0 = ConversionMethods.BytesToSingle(byteData[offset + 69], byteData[offset + 70], byteData[offset + 71], byteData[offset + 72]);
                            gammaSideSetup.InputVoltage_1 = ConversionMethods.BytesToSingle(byteData[offset + 73], byteData[offset + 74], byteData[offset + 75], byteData[offset + 76]);
                            gammaSideSetup.InputVoltage_2 = ConversionMethods.BytesToSingle(byteData[offset + 77], byteData[offset + 78], byteData[offset + 79], byteData[offset + 80]);
                            gammaSideSetup.STABLEDeltaTg_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 81], byteData[offset + 82]);
                            gammaSideSetup.STABLEDeltaTg_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 83], byteData[offset + 84]);
                            gammaSideSetup.STABLEDeltaTg_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 85], byteData[offset + 86]);
                            gammaSideSetup.STABLEDeltaC_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 87], byteData[offset + 88]);
                            gammaSideSetup.STABLEDeltaC_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 89], byteData[offset + 90]);
                            gammaSideSetup.STABLEDeltaC_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 91], byteData[offset + 92]);
                            gammaSideSetup.STABLEDate = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 93], byteData[offset + 94], byteData[offset + 95], byteData[offset + 96]);
                            gammaSideSetup.HeatDate = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 97], byteData[offset + 98], byteData[offset + 99], byteData[offset + 100]);
                            gammaSideSetup.STABLETemperature = ConversionMethods.ByteToInt32(byteData[offset + 101]);
                            gammaSideSetup.STABLETg_0 = ConversionMethods.ShortBytesToInt16(byteData[offset + 102], byteData[offset + 103]);
                            gammaSideSetup.STABLETg_1 = ConversionMethods.ShortBytesToInt16(byteData[offset + 104], byteData[offset + 105]);
                            gammaSideSetup.STABLETg_2 = ConversionMethods.ShortBytesToInt16(byteData[offset + 106], byteData[offset + 107]);
                            gammaSideSetup.STABLEC_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 108], byteData[offset + 109]);
                            gammaSideSetup.STABLEC_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 110], byteData[offset + 111]);
                            gammaSideSetup.STABLEC_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 112], byteData[offset + 113]);
                            gammaSideSetup.STABLESaved = ConversionMethods.ByteToInt32(byteData[offset + 114]);
                            gammaSideSetup.RatedVoltage = ConversionMethods.BytesToSingle(byteData[offset + 115], byteData[offset + 116], byteData[offset + 117], byteData[offset + 118]);
                            gammaSideSetup.RatedCurrent = ConversionMethods.BytesToSingle(byteData[offset + 119], byteData[offset + 120], byteData[offset + 121], byteData[offset + 122]);
                            gammaSideSetup.StablePhaseAmplitude_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 123], byteData[offset + 124]);
                            gammaSideSetup.StablePhaseAmplitude_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 125], byteData[offset + 126]);
                            gammaSideSetup.StablePhaseAmplitude_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 127], byteData[offset + 128]);
                            gammaSideSetup.StableSourceAmplitude_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 129], byteData[offset + 130]);
                            gammaSideSetup.StableSourceAmplitude_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 131], byteData[offset + 132]);
                            gammaSideSetup.StableSourceAmplitude_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 133], byteData[offset + 134]);
                            gammaSideSetup.StableSignalPhase_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 135], byteData[offset + 136]);
                            gammaSideSetup.StableSignalPhase_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 137], byteData[offset + 138]);
                            gammaSideSetup.StableSignalPhase_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 139], byteData[offset + 140]);
                            gammaSideSetup.StableSourcePhase_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 141], byteData[offset + 142]);
                            gammaSideSetup.StableSourcePhase_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 143], byteData[offset + 144]);
                            gammaSideSetup.StableSourcePhase_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 145], byteData[offset + 146]);
                            gammaSideSetup.STABLEAvgCurrent = ConversionMethods.ByteToInt32(byteData[offset + 147]);
                            gammaSideSetup.TgYellowThreshold = ConversionMethods.ShortBytesToInt16(byteData[offset + 148], byteData[offset + 149]);
                            gammaSideSetup.TgRedThreshold = ConversionMethods.ShortBytesToInt16(byteData[offset + 150], byteData[offset + 151]);
                            gammaSideSetup.TgVariationThreshold = ConversionMethods.ShortBytesToInt16(byteData[offset + 152], byteData[offset + 153]);
                            gammaSideSetup.ImpedanceValue_0 = ConversionMethods.ByteToInt32(byteData[offset + 154]);
                            gammaSideSetup.ImpedanceValue_1 = ConversionMethods.ByteToInt32(byteData[offset + 155]);
                            gammaSideSetup.ImpedanceValue_2 = ConversionMethods.ByteToInt32(byteData[offset + 156]);
                            gammaSideSetup.TemperatureConfig = ConversionMethods.ByteToInt32(byteData[offset + 157]);
                            gammaSideSetup.ExtImpedanceValue_0 = ConversionMethods.ByteToInt32(byteData[offset + 158]);
                            gammaSideSetup.ExtImpedanceValue_1 = ConversionMethods.ByteToInt32(byteData[offset + 159]);
                            gammaSideSetup.ExtImpedanceValue_2 = ConversionMethods.ByteToInt32(byteData[offset + 160]);
                            gammaSideSetup.ExternalSync = ConversionMethods.ByteToInt32(byteData[offset + 161]);
                            gammaSideSetup.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 162]);
                            gammaSideSetup.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 163]);
                            gammaSideSetup.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 164]);
                            gammaSideSetup.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 165]);
                            gammaSideSetup.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 166]);
                            gammaSideSetup.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 167]);
                            gammaSideSetup.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 168]);
                            gammaSideSetup.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 169]);
                            gammaSideSetup.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 170]);
                            gammaSideSetup.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 171]);
                            gammaSideSetup.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 172]);
                            gammaSideSetup.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 173]);
                            gammaSideSetup.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 174]);
                            gammaSideSetup.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 175]);
                            gammaSideSetup.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 176]);
                            gammaSideSetup.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 177]);
                            gammaSideSetup.Reserved_16 = ConversionMethods.ByteToInt32(byteData[offset + 178]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nBad input value for sideNumber, expect 0 or 1, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSideSetup;
        }

        private void ConvertGammaSideSetupToBytes(BHM_ConfigComponent_GammaSideSetup gammaSideSetup, int sideNumber, ref Byte[] byteData)
        {
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 158;
                }
                else if (sideNumber == 1)
                {
                    offset = 337;
                }

                if (offset != -1)
                {
                    if (gammaSideSetup != null)
                    {
                        if (byteData != null)
                        {
                            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                            {
                                byteData[offset] = ConversionMethods.Int32ToByte(gammaSideSetup.AlarmEnable);
                                byteData[offset + 1] = ConversionMethods.Int32ToByte(gammaSideSetup.GammaYellowThreshold);
                                byteData[offset + 2] = ConversionMethods.Int32ToByte(gammaSideSetup.GammaRedThreshold);
                                byteData[offset + 3] = ConversionMethods.Int32ToByte(gammaSideSetup.TempCoefficient);
                                byteData[offset + 4] = ConversionMethods.Int32ToByte(gammaSideSetup.TrendAlarm);
                                byteData[offset + 5] = ConversionMethods.Int32ToByte(gammaSideSetup.Temperature0);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputC_0), 0, byteData, offset + 6, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputC_1), 0, byteData, offset + 8, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputC_2), 0, byteData, offset + 10, 2);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputCoeff_0), 0, byteData, offset + 12, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputCoeff_1), 0, byteData, offset + 16, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputCoeff_2), 0, byteData, offset + 20, 4);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.Tg0_0), 0, byteData, offset + 24, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.Tg0_1), 0, byteData, offset + 26, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.Tg0_2), 0, byteData, offset + 28, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.C0_0), 0, byteData, offset + 30, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.C0_1), 0, byteData, offset + 32, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.C0_2), 0, byteData, offset + 34, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputImpedance_0), 0, byteData, offset + 36, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputImpedance_1), 0, byteData, offset + 38, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.InputImpedance_2), 0, byteData, offset + 40, 2);
                                byteData[offset + 42] = ConversionMethods.Int32ToByte(gammaSideSetup.MinTemperature1);
                                byteData[offset + 43] = ConversionMethods.Int32ToByte(gammaSideSetup.AvgTemperature1);
                                byteData[offset + 44] = ConversionMethods.Int32ToByte(gammaSideSetup.MaxTemperature1);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.B_0), 0, byteData, offset + 45, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.B_1), 0, byteData, offset + 49, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.B_2), 0, byteData, offset + 53, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.K_0), 0, byteData, offset + 57, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.K_1), 0, byteData, offset + 61, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.K_2), 0, byteData, offset + 65, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputVoltage_0), 0, byteData, offset + 69, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputVoltage_1), 0, byteData, offset + 73, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.InputVoltage_2), 0, byteData, offset + 77, 4);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaTg_0), 0, byteData, offset + 81, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaTg_1), 0, byteData, offset + 83, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaTg_2), 0, byteData, offset + 85, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaC_0), 0, byteData, offset + 87, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaC_1), 0, byteData, offset + 89, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEDeltaC_2), 0, byteData, offset + 91, 2);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)gammaSideSetup.STABLEDate), 0, byteData, offset + 93, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)gammaSideSetup.HeatDate), 0, byteData, offset + 97, 4);
                                byteData[offset + 101] = ConversionMethods.Int32ToByte(gammaSideSetup.STABLETemperature);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.STABLETg_0), 0, byteData, offset + 102, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.STABLETg_1), 0, byteData, offset + 104, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.STABLETg_2), 0, byteData, offset + 106, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEC_0), 0, byteData, offset + 108, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEC_1), 0, byteData, offset + 110, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.STABLEC_2), 0, byteData, offset + 112, 2);
                                byteData[offset + 114] = ConversionMethods.Int32ToByte(gammaSideSetup.STABLESaved);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.RatedVoltage), 0, byteData, offset + 115, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)gammaSideSetup.RatedCurrent), 0, byteData, offset + 119, 4);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StablePhaseAmplitude_0), 0, byteData, offset + 123, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StablePhaseAmplitude_1), 0, byteData, offset + 125, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StablePhaseAmplitude_2), 0, byteData, offset + 127, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourceAmplitude_0), 0, byteData, offset + 129, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourceAmplitude_1), 0, byteData, offset + 131, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourceAmplitude_2), 0, byteData, offset + 133, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSignalPhase_0), 0, byteData, offset + 135, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSignalPhase_1), 0, byteData, offset + 137, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSignalPhase_2), 0, byteData, offset + 139, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourcePhase_0), 0, byteData, offset + 141, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourcePhase_1), 0, byteData, offset + 143, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)gammaSideSetup.StableSourcePhase_2), 0, byteData, offset + 145, 2);
                                byteData[offset + 147] = ConversionMethods.Int32ToByte(gammaSideSetup.STABLEAvgCurrent);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.TgYellowThreshold), 0, byteData, offset + 148, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.TgRedThreshold), 0, byteData, offset + 150, 2);
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)gammaSideSetup.TgVariationThreshold), 0, byteData, offset + 152, 2);
                                byteData[offset + 154] = ConversionMethods.Int32ToByte(gammaSideSetup.ImpedanceValue_0);
                                byteData[offset + 155] = ConversionMethods.Int32ToByte(gammaSideSetup.ImpedanceValue_1);
                                byteData[offset + 156] = ConversionMethods.Int32ToByte(gammaSideSetup.ImpedanceValue_2);
                                byteData[offset + 157] = ConversionMethods.Int32ToByte(gammaSideSetup.TemperatureConfig);
                                byteData[offset + 158] = ConversionMethods.Int32ToByte(gammaSideSetup.ExtImpedanceValue_0);
                                byteData[offset + 159] = ConversionMethods.Int32ToByte(gammaSideSetup.ExtImpedanceValue_1);
                                byteData[offset + 160] = ConversionMethods.Int32ToByte(gammaSideSetup.ExtImpedanceValue_2);
                                byteData[offset + 161] = ConversionMethods.Int32ToByte(gammaSideSetup.ExternalSync);
                                byteData[offset + 162] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_0);
                                byteData[offset + 163] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_1);
                                byteData[offset + 164] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_2);
                                byteData[offset + 165] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_3);
                                byteData[offset + 166] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_4);
                                byteData[offset + 167] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_5);
                                byteData[offset + 168] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_6);
                                byteData[offset + 169] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_7);
                                byteData[offset + 170] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_8);
                                byteData[offset + 171] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_9);
                                byteData[offset + 172] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_10);
                                byteData[offset + 173] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_11);
                                byteData[offset + 174] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_12);
                                byteData[offset + 175] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_13);
                                byteData[offset + 176] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_14);
                                byteData[offset + 177] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_15);
                                byteData[offset + 178] = ConversionMethods.Int32ToByte(gammaSideSetup.Reserved_16);
                            }
                            else
                            {
                                string errorMessage = "Error in BHM_Configuration.ConvertGammaSideSetupToBytes(BHM_Config_GammaSideSetup, int, ref Byte[]):\nInput Byte[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertGammaSideSetupToBytes(BHM_Config_GammaSideSetup, int, ref Byte[]):\nInput Byte[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertGammaSideSetupToBytes(BHM_Config_GammaSideSetup, int, ref Byte[]):\nInput BHM_Config_GammaSideSetup was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertGammaSideSetupToBytes(BHM_Config_GammaSideSetup, int, ref Byte[]):\nBad value for sideNumber, expect 0 or 1, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertGammaSideSetupToBytes(BHM_Config_GammaSideSetup, int, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_TrSideParameters InitializeEmptyTrSideParam()
        {
            BHM_ConfigComponent_TrSideParameters trSideParam = new BHM_ConfigComponent_TrSideParameters();
            try
            {
                trSideParam.ChPhaseShift = 0;
                trSideParam.PhaseAmplitude_0 = 0;
                trSideParam.PhaseAmplitude_1 = 0;
                trSideParam.PhaseAmplitude_2 = 0;
                trSideParam.SourceAmplitude_0 = 0;
                trSideParam.SourceAmplitude_1 = 0;
                trSideParam.SourceAmplitude_2 = 0;
                trSideParam.Temperature = 0;
                trSideParam.GammaAmplitude = 0;
                trSideParam.GammaPhase = 0;
                trSideParam.KT = 0;
                trSideParam.KTPhase = 0;
                trSideParam.DefectCode = 0;
                trSideParam.KTSaved = 0;
                trSideParam.SignalPhase_0 = 0;
                trSideParam.SignalPhase_1 = 0;
                trSideParam.SignalPhase_2 = 0;
                trSideParam.SourcePhase_0 = 0;
                trSideParam.SourcePhase_1 = 0;
                trSideParam.SourcePhase_2 = 0;
                trSideParam.RPN = 0;
                trSideParam.TrSideParamCurrent = 0;
                trSideParam.Reserved_0 = 0;
                trSideParam.Reserved_1 = 0;
                trSideParam.Reserved_2 = 0;
                trSideParam.Reserved_3 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitializeEmptyTrSideParam()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return trSideParam;
        }


        private BHM_ConfigComponent_TrSideParameters ExtractTrSideParamDataFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_ConfigComponent_TrSideParameters trSideParam = null;
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 554;
                }
                else if (sideNumber == 1)
                {
                    offset = 597;
                }

                if (offset != -1)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= (offset + 43))
                        {
                            trSideParam = new BHM_ConfigComponent_TrSideParameters();

                            trSideParam.ChPhaseShift = ConversionMethods.ShortBytesToInt16(byteData[offset], byteData[offset + 1]);
                            trSideParam.PhaseAmplitude_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 2], byteData[offset + 3]);
                            trSideParam.PhaseAmplitude_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 4], byteData[offset + 5]);
                            trSideParam.PhaseAmplitude_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 6], byteData[offset + 7]);
                            trSideParam.SourceAmplitude_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 8], byteData[offset + 9]);
                            trSideParam.SourceAmplitude_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 10], byteData[offset + 11]);
                            trSideParam.SourceAmplitude_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 12], byteData[offset + 13]);
                            trSideParam.Temperature = ConversionMethods.ByteToInt32(byteData[offset + 14]);
                            trSideParam.GammaAmplitude = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 15], byteData[offset + 16]);
                            trSideParam.GammaPhase = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 17], byteData[offset + 18]);
                            trSideParam.KT = ConversionMethods.ByteToInt32(byteData[offset + 19]);
                            trSideParam.KTPhase = ConversionMethods.ByteToInt32(byteData[offset + 20]);
                            trSideParam.DefectCode = ConversionMethods.ByteToInt32(byteData[offset + 21]);
                            trSideParam.KTSaved = ConversionMethods.ByteToInt32(byteData[offset + 22]);
                            trSideParam.SignalPhase_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 23], byteData[offset + 24]);
                            trSideParam.SignalPhase_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 25], byteData[offset + 26]);
                            trSideParam.SignalPhase_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 27], byteData[offset + 28]);
                            trSideParam.SourcePhase_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 29], byteData[offset + 30]);
                            trSideParam.SourcePhase_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 31], byteData[offset + 32]);
                            trSideParam.SourcePhase_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 33], byteData[offset + 34]);
                            trSideParam.RPN = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 35], byteData[offset + 36]);
                            trSideParam.TrSideParamCurrent = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 37], byteData[offset + 38]);
                            trSideParam.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 39]);
                            trSideParam.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 40]);
                            trSideParam.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 41]);
                            trSideParam.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 42]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int):\nBad input value for sideNumber, expect 0 or 1, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractGammaSideSetupDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return trSideParam;
        }

        private void ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters trSideParam, int sideNumber, ref Byte[] byteData)
        {
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 554;
                }
                else if (sideNumber == 1)
                {
                    offset = 597;
                }

                if (offset != -1)
                {
                    if (trSideParam != null)
                    {
                        if (byteData != null)
                        {
                            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                            {
                                Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)trSideParam.ChPhaseShift), 0, byteData, offset, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.PhaseAmplitude_0), 0, byteData, offset + 2, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.PhaseAmplitude_1), 0, byteData, offset + 4, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.PhaseAmplitude_2), 0, byteData, offset + 6, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourceAmplitude_0), 0, byteData, offset + 8, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourceAmplitude_1), 0, byteData, offset + 10, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourceAmplitude_2), 0, byteData, offset + 12, 2);
                                byteData[offset + 14] = ConversionMethods.Int32ToByte(trSideParam.Temperature);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.GammaAmplitude), 0, byteData, offset + 15, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.GammaPhase), 0, byteData, offset + 17, 2);
                                byteData[offset + 19] = ConversionMethods.Int32ToByte(trSideParam.KT);
                                byteData[offset + 20] = ConversionMethods.Int32ToByte(trSideParam.KTPhase);
                                byteData[offset + 21] = ConversionMethods.Int32ToByte(trSideParam.DefectCode);
                                byteData[offset + 22] = ConversionMethods.Int32ToByte(trSideParam.KTSaved);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SignalPhase_0), 0, byteData, offset + 23, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SignalPhase_1), 0, byteData, offset + 25, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SignalPhase_2), 0, byteData, offset + 27, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourcePhase_0), 0, byteData, offset + 29, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourcePhase_1), 0, byteData, offset + 31, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.SourcePhase_2), 0, byteData, offset + 33, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.RPN), 0, byteData, offset + 35, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)trSideParam.TrSideParamCurrent), 0, byteData, offset + 37, 2);
                                byteData[offset + 39] = ConversionMethods.Int32ToByte(trSideParam.Reserved_0);
                                byteData[offset + 40] = ConversionMethods.Int32ToByte(trSideParam.Reserved_1);
                                byteData[offset + 41] = ConversionMethods.Int32ToByte(trSideParam.Reserved_2);
                                byteData[offset + 42] = ConversionMethods.Int32ToByte(trSideParam.Reserved_3);
                            }
                            else
                            {
                                string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters, int, ref Byte[]):\nInput Byte[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters, int, ref Byte[]):\nInput Byte[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters, int, ref Byte[]):\nInput BHM_ConfigComponent_TrSideParameters was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters, int, ref Byte[]):\nBad value for sideNumber, expect 0 or 1, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertTrSideParamToBytes(BHM_ConfigComponent_TrSideParameters, int, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_ZkSideToSideSetup InitializeEmptyZkSideToSideSetup()
        {
            BHM_ConfigComponent_ZkSideToSideSetup zkSideToSideSetup = new BHM_ConfigComponent_ZkSideToSideSetup();
            try
            {
                zkSideToSideSetup.ZkYellowThreshold = 209;
                zkSideToSideSetup.ZkRedThreshold = 220;
                zkSideToSideSetup.ZkTrendAlarm = 185;

                zkSideToSideSetup.Reserved_0 = 74;
                zkSideToSideSetup.Reserved_1 = 222;
                zkSideToSideSetup.Reserved_2 = 154;
                zkSideToSideSetup.Reserved_3 = 83;
                zkSideToSideSetup.Reserved_4 = 229;
                zkSideToSideSetup.Reserved_5 = 61;
                zkSideToSideSetup.Reserved_6 = 70;
                zkSideToSideSetup.Reserved_7 = 141;
                zkSideToSideSetup.Reserved_8 = 139;
                zkSideToSideSetup.Reserved_9 = 164;
                zkSideToSideSetup.Reserved_10 = 234;
                zkSideToSideSetup.Reserved_11 = 238;
                zkSideToSideSetup.Reserved_12 = 240;
                zkSideToSideSetup.Reserved_13 = 244;
                zkSideToSideSetup.Reserved_14 = 110;
                zkSideToSideSetup.Reserved_15 = 2;
                zkSideToSideSetup.Reserved_16 = 58;
                zkSideToSideSetup.Reserved_17 = 229;
                zkSideToSideSetup.Reserved_18 = 200;
                zkSideToSideSetup.Reserved_19 = 147;
                zkSideToSideSetup.Reserved_20 = 48;
                zkSideToSideSetup.Reserved_21 = 213;
                zkSideToSideSetup.Reserved_22 = 226;
                zkSideToSideSetup.Reserved_23 = 250;
                zkSideToSideSetup.Reserved_24 = 6;
                zkSideToSideSetup.Reserved_25 = 169;
                zkSideToSideSetup.Reserved_26 = 160;
                zkSideToSideSetup.Reserved_27 = 83;
                zkSideToSideSetup.Reserved_28 = 159;
                zkSideToSideSetup.Reserved_29 = 207;
                zkSideToSideSetup.Reserved_30 = 212;
                zkSideToSideSetup.Reserved_31 = 251;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractZkSideToSideSetupDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSideToSideSetup;
        }




        private BHM_ConfigComponent_ZkSideToSideSetup ExtractZkSideToSideSetupDataFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_ConfigComponent_ZkSideToSideSetup zkSideToSideSetup = null;
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 802;
                }

                if (offset != -1)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= (offset + 35))
                        {
                            zkSideToSideSetup = new BHM_ConfigComponent_ZkSideToSideSetup();

                            zkSideToSideSetup.ZkYellowThreshold = ConversionMethods.ByteToInt32(byteData[offset]);
                            zkSideToSideSetup.ZkRedThreshold = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                            zkSideToSideSetup.ZkTrendAlarm = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                            zkSideToSideSetup.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                            zkSideToSideSetup.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                            zkSideToSideSetup.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 5]);
                            zkSideToSideSetup.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 6]);
                            zkSideToSideSetup.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 7]);
                            zkSideToSideSetup.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 8]);
                            zkSideToSideSetup.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 9]);
                            zkSideToSideSetup.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 10]);
                            zkSideToSideSetup.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 11]);
                            zkSideToSideSetup.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 12]);
                            zkSideToSideSetup.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 13]);
                            zkSideToSideSetup.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 14]);
                            zkSideToSideSetup.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 15]);
                            zkSideToSideSetup.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 16]);
                            zkSideToSideSetup.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 17]);
                            zkSideToSideSetup.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 18]);
                            zkSideToSideSetup.Reserved_16 = ConversionMethods.ByteToInt32(byteData[offset + 19]);
                            zkSideToSideSetup.Reserved_17 = ConversionMethods.ByteToInt32(byteData[offset + 20]);
                            zkSideToSideSetup.Reserved_18 = ConversionMethods.ByteToInt32(byteData[offset + 21]);
                            zkSideToSideSetup.Reserved_19 = ConversionMethods.ByteToInt32(byteData[offset + 22]);
                            zkSideToSideSetup.Reserved_20 = ConversionMethods.ByteToInt32(byteData[offset + 23]);
                            zkSideToSideSetup.Reserved_21 = ConversionMethods.ByteToInt32(byteData[offset + 24]);
                            zkSideToSideSetup.Reserved_22 = ConversionMethods.ByteToInt32(byteData[offset + 25]);
                            zkSideToSideSetup.Reserved_23 = ConversionMethods.ByteToInt32(byteData[offset + 26]);
                            zkSideToSideSetup.Reserved_24 = ConversionMethods.ByteToInt32(byteData[offset + 27]);
                            zkSideToSideSetup.Reserved_25 = ConversionMethods.ByteToInt32(byteData[offset + 28]);
                            zkSideToSideSetup.Reserved_26 = ConversionMethods.ByteToInt32(byteData[offset + 29]);
                            zkSideToSideSetup.Reserved_27 = ConversionMethods.ByteToInt32(byteData[offset + 30]);
                            zkSideToSideSetup.Reserved_28 = ConversionMethods.ByteToInt32(byteData[offset + 31]);
                            zkSideToSideSetup.Reserved_29 = ConversionMethods.ByteToInt32(byteData[offset + 32]);
                            zkSideToSideSetup.Reserved_30 = ConversionMethods.ByteToInt32(byteData[offset + 33]);
                            zkSideToSideSetup.Reserved_31 = ConversionMethods.ByteToInt32(byteData[offset + 34]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ExtractZkSideToSideSetupDataFromByteData(Byte[], int):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractZkSideToSideSetupDataFromByteData(Byte[], int):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractZkSideToSideSetupDataFromByteData(Byte[], int):\nBad value for sideNumber, expected 0, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractZkSideToSideSetupDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSideToSideSetup;
        }

        private void ConvertZkSideToSideSetupToBytes(BHM_ConfigComponent_ZkSideToSideSetup zkSideToSideSetup, int sideNumber, ref Byte[] byteData)
        {
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 802;
                }
                if (offset != -1)
                {
                    if (zkSideToSideSetup != null)
                    {
                        if (byteData != null)
                        {
                            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                            {
                                byteData[offset] = ConversionMethods.Int32ToByte(zkSideToSideSetup.ZkYellowThreshold);
                                byteData[offset + 1] = ConversionMethods.Int32ToByte(zkSideToSideSetup.ZkRedThreshold);
                                byteData[offset + 2] = ConversionMethods.Int32ToByte(zkSideToSideSetup.ZkTrendAlarm);
                                byteData[offset + 3] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_0);
                                byteData[offset + 4] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_1);
                                byteData[offset + 5] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_2);
                                byteData[offset + 6] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_3);
                                byteData[offset + 7] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_4);
                                byteData[offset + 8] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_5);
                                byteData[offset + 9] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_6);
                                byteData[offset + 10] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_7);
                                byteData[offset + 11] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_8);
                                byteData[offset + 12] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_9);
                                byteData[offset + 13] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_10);
                                byteData[offset + 14] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_11);
                                byteData[offset + 15] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_12);
                                byteData[offset + 16] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_13);
                                byteData[offset + 17] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_14);
                                byteData[offset + 18] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_15);
                                byteData[offset + 19] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_16);
                                byteData[offset + 20] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_17);
                                byteData[offset + 21] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_18);
                                byteData[offset + 22] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_19);
                                byteData[offset + 23] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_20);
                                byteData[offset + 24] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_21);
                                byteData[offset + 25] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_22);
                                byteData[offset + 26] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_23);
                                byteData[offset + 27] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_24);
                                byteData[offset + 28] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_25);
                                byteData[offset + 29] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_26);
                                byteData[offset + 30] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_27);
                                byteData[offset + 31] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_28);
                                byteData[offset + 32] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_29);
                                byteData[offset + 33] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_30);
                                byteData[offset + 34] = ConversionMethods.Int32ToByte(zkSideToSideSetup.Reserved_31);
                            }
                            else
                            {
                                string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, int, ref Byte[]):\nInput Byte[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, int, ref Byte[]):\nInput Byte[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, int, ref Byte[]):\nInput BHM_Config_ZkSideToSideSetup was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, int, ref Byte[]):\nBad value for sideNumber, expected 0, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, int, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_SetupInfo InitializeEmptySetupInfo()
        {
            BHM_ConfigComponent_SetupInfo setupInfo = new BHM_ConfigComponent_SetupInfo();
            try
            {
                setupInfo.DeviceNumber = 10;
                setupInfo.BaudRate = 3;
                setupInfo.OutTime = 3;
                setupInfo.DisplayFlag = 65535;
                setupInfo.SaveDays = 1;
                setupInfo.Stopped = 1;
                setupInfo.Relay = 1;
                setupInfo.TimeOfRelayAlarm = 10;
                setupInfo.ScheduleType = 0;
                setupInfo.DTime_Hour = 0;
                setupInfo.DTime_Min = 0;

                setupInfo.SyncType = 152;
                setupInfo.InternalSyncFrequency = 45813;
                setupInfo.ModBusProtocol = 0;
                setupInfo.ReadAnalogFromRegister = 1;
                setupInfo.HeaterOnTemperature = 0;
                setupInfo.AutoBalans_Month = 0;
                setupInfo.AutoBalans_Year = 0;
                setupInfo.AutoBalans_Day = 0;
                setupInfo.AutoBalans_Hour = 0;
                setupInfo.AutoBalansActive = 0;
                setupInfo.NoLoadTestActive = 0;
                setupInfo.WorkingDays = 0;

                setupInfo.Reserved_0 = 190;
                setupInfo.Reserved_1 = 23;
                setupInfo.Reserved_2 = 114;
                setupInfo.Reserved_3 = 42;
                setupInfo.Reserved_4 = 164;
                setupInfo.Reserved_5 = 69;
                setupInfo.Reserved_6 = 46;
                setupInfo.Reserved_7 = 181;
                setupInfo.Reserved_8 = 160;
                setupInfo.Reserved_9 = 33;
                setupInfo.Reserved_10 = 17;
                setupInfo.Reserved_11 = 237;
                setupInfo.Reserved_12 = 176;
                setupInfo.Reserved_13 = 219;
                setupInfo.Reserved_14 = 75;
                setupInfo.Reserved_15 = 166;
                setupInfo.Reserved_16 = 225;
                setupInfo.Reserved_17 = 137;
                setupInfo.Reserved_18 = 231;
                setupInfo.Reserved_19 = 252;
                setupInfo.Reserved_20 = 117;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitializeEmptySetupInfo()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        private BHM_ConfigComponent_SetupInfo ExtractSetupInfoDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_SetupInfo setupInfo = null;
            try
            {
                int offset = 0;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        setupInfo = new BHM_ConfigComponent_SetupInfo();

                        setupInfo.DeviceNumber = ConversionMethods.ByteToInt32(byteData[offset]);
                        setupInfo.BaudRate = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                        setupInfo.OutTime = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                        setupInfo.DisplayFlag = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 3], byteData[offset + 4]);
                        setupInfo.SaveDays = ConversionMethods.ByteToInt32(byteData[offset + 5]);
                        setupInfo.Stopped = ConversionMethods.ByteToInt32(byteData[offset + 6]);
                        setupInfo.Relay = ConversionMethods.ByteToInt32(byteData[offset + 7]);
                        setupInfo.TimeOfRelayAlarm = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 8], byteData[offset + 9]);
                        setupInfo.ScheduleType = ConversionMethods.ByteToInt32(byteData[offset + 10]);
                        setupInfo.DTime_Hour = ConversionMethods.ByteToInt32(byteData[offset + 11]);
                        setupInfo.DTime_Min = ConversionMethods.ByteToInt32(byteData[offset + 12]);
                        // this jump in offset is correct
                        setupInfo.SyncType = ConversionMethods.ByteToInt32(byteData[offset + 113]);
                        setupInfo.InternalSyncFrequency = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 114], byteData[offset + 115]);
                        setupInfo.ModBusProtocol = ConversionMethods.ByteToInt32(byteData[offset + 116]);
                        setupInfo.ReadAnalogFromRegister = ConversionMethods.ByteToInt32(byteData[offset + 117]);
                        setupInfo.HeaterOnTemperature = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 118], byteData[offset + 119]);
                        setupInfo.AutoBalans_Month = ConversionMethods.ByteToInt32(byteData[offset + 120]);
                        setupInfo.AutoBalans_Year = ConversionMethods.ByteToInt32(byteData[offset + 121]);
                        setupInfo.AutoBalans_Day = ConversionMethods.ByteToInt32(byteData[offset + 122]);
                        setupInfo.AutoBalans_Hour = ConversionMethods.ByteToInt32(byteData[offset + 123]);
                        setupInfo.AutoBalansActive = ConversionMethods.ByteToInt32(byteData[offset + 124]);
                        setupInfo.NoLoadTestActive = ConversionMethods.ByteToInt32(byteData[offset + 125]);
                        setupInfo.WorkingDays = ConversionMethods.ByteToInt32(byteData[offset + 126]);
                        setupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 127]);
                        setupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 128]);
                        setupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 129]);
                        setupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 130]);
                        setupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 131]);
                        setupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 132]);
                        setupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 133]);
                        setupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 134]);
                        setupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 135]);
                        setupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 136]);
                        setupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 137]);
                        setupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 138]);
                        setupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 139]);
                        setupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 140]);
                        setupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 141]);
                        setupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 142]);
                        setupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[offset + 143]);
                        setupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[offset + 144]);
                        setupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[offset + 145]);
                        setupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[offset + 146]);
                        setupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[offset + 147]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractSetupInfoDataFromByteData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractSetupInfoDataFromByteData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        private void ConvertSetupInfoToBytes(BHM_ConfigComponent_SetupInfo setupInfo, ref Byte[] byteData)
        {
            try
            {
                int offset = 0;
                if (setupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[offset] = ConversionMethods.Int32ToByte(setupInfo.DeviceNumber);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(setupInfo.BaudRate);
                            byteData[offset + 2] = ConversionMethods.Int32ToByte(setupInfo.OutTime);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.DisplayFlag), 0, byteData, offset + 3, 2);
                            byteData[offset + 5] = ConversionMethods.Int32ToByte(setupInfo.SaveDays);
                            byteData[offset + 6] = ConversionMethods.Int32ToByte(setupInfo.Stopped);
                            byteData[offset + 7] = ConversionMethods.Int32ToByte(setupInfo.Relay);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.TimeOfRelayAlarm), 0, byteData, offset + 8, 2);
                            byteData[offset + 10] = ConversionMethods.Int32ToByte(setupInfo.ScheduleType);
                            byteData[offset + 11] = ConversionMethods.Int32ToByte(setupInfo.DTime_Hour);
                            byteData[offset + 12] = ConversionMethods.Int32ToByte(setupInfo.DTime_Min);
                            // this jump in offset is correct
                            byteData[offset + 113] = ConversionMethods.Int32ToByte(setupInfo.SyncType);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.InternalSyncFrequency), 0, byteData, offset + 114, 2);
                            byteData[offset + 116] = ConversionMethods.Int32ToByte(setupInfo.ModBusProtocol);
                            byteData[offset + 117] = ConversionMethods.Int32ToByte(setupInfo.ReadAnalogFromRegister);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.HeaterOnTemperature), 0, byteData, offset + 118, 2);
                            byteData[offset + 120] = ConversionMethods.Int32ToByte(setupInfo.AutoBalans_Month);
                            byteData[offset + 121] = ConversionMethods.Int32ToByte(setupInfo.AutoBalans_Year);
                            byteData[offset + 122] = ConversionMethods.Int32ToByte(setupInfo.AutoBalans_Day);
                            byteData[offset + 123] = ConversionMethods.Int32ToByte(setupInfo.AutoBalans_Hour);
                            byteData[offset + 124] = ConversionMethods.Int32ToByte(setupInfo.AutoBalansActive);
                            byteData[offset + 125] = ConversionMethods.Int32ToByte(setupInfo.NoLoadTestActive);
                            byteData[offset + 126] = ConversionMethods.Int32ToByte(setupInfo.WorkingDays);
                            byteData[offset + 127] = ConversionMethods.Int32ToByte(setupInfo.Reserved_0);
                            byteData[offset + 128] = ConversionMethods.Int32ToByte(setupInfo.Reserved_1);
                            byteData[offset + 129] = ConversionMethods.Int32ToByte(setupInfo.Reserved_2);
                            byteData[offset + 130] = ConversionMethods.Int32ToByte(setupInfo.Reserved_3);
                            byteData[offset + 131] = ConversionMethods.Int32ToByte(setupInfo.Reserved_4);
                            byteData[offset + 132] = ConversionMethods.Int32ToByte(setupInfo.Reserved_5);
                            byteData[offset + 133] = ConversionMethods.Int32ToByte(setupInfo.Reserved_6);
                            byteData[offset + 134] = ConversionMethods.Int32ToByte(setupInfo.Reserved_7);
                            byteData[offset + 135] = ConversionMethods.Int32ToByte(setupInfo.Reserved_8);
                            byteData[offset + 136] = ConversionMethods.Int32ToByte(setupInfo.Reserved_9);
                            byteData[offset + 137] = ConversionMethods.Int32ToByte(setupInfo.Reserved_10);
                            byteData[offset + 138] = ConversionMethods.Int32ToByte(setupInfo.Reserved_11);
                            byteData[offset + 139] = ConversionMethods.Int32ToByte(setupInfo.Reserved_12);
                            byteData[offset + 140] = ConversionMethods.Int32ToByte(setupInfo.Reserved_13);
                            byteData[offset + 141] = ConversionMethods.Int32ToByte(setupInfo.Reserved_14);
                            byteData[offset + 142] = ConversionMethods.Int32ToByte(setupInfo.Reserved_15);
                            byteData[offset + 143] = ConversionMethods.Int32ToByte(setupInfo.Reserved_16);
                            byteData[offset + 144] = ConversionMethods.Int32ToByte(setupInfo.Reserved_17);
                            byteData[offset + 145] = ConversionMethods.Int32ToByte(setupInfo.Reserved_18);
                            byteData[offset + 146] = ConversionMethods.Int32ToByte(setupInfo.Reserved_19);
                            byteData[offset + 147] = ConversionMethods.Int32ToByte(setupInfo.Reserved_20);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, ref Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, ref Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, ref Byte[]):\nInput BHM_Config_ZkSideToSideSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertZkSideToSideSetupToBytes(BHM_Config_ZkSideToSideSetup, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_GammaSetupInfo InitializeEmptyGammaSetupInfo()
        {
            BHM_ConfigComponent_GammaSetupInfo gammaSetupInfo = new BHM_ConfigComponent_GammaSetupInfo();
            try
            {
                gammaSetupInfo.ReadOnSide = 0;
                gammaSetupInfo.MaxParameterChange = 10;
                gammaSetupInfo.AlarmHysteresis = 0;
                gammaSetupInfo.AllowedPhaseDispersion = 0;
                gammaSetupInfo.DaysToCalculateTrend = 0;
                gammaSetupInfo.DaysToCalculateTCoefficient = 0;
                gammaSetupInfo.AveragingForGamma = 0;
                gammaSetupInfo.ReReadOnAlarm = 0;
                gammaSetupInfo.MinDiagGamma = 0;
                gammaSetupInfo.NEGtg = 1;
                gammaSetupInfo.DaysToCalculateBASELINE = 30;
                gammaSetupInfo.LoadChannel = 177;
                gammaSetupInfo.ReduceCoeff = 162;

                gammaSetupInfo.Reserved_0 = 64;
                gammaSetupInfo.Reserved_1 = 240;
                gammaSetupInfo.Reserved_2 = 52;
                gammaSetupInfo.Reserved_3 = 62;
                gammaSetupInfo.Reserved_4 = 53;
                gammaSetupInfo.Reserved_5 = 171;
                gammaSetupInfo.Reserved_6 = 251;
                gammaSetupInfo.Reserved_7 = 183;
                gammaSetupInfo.Reserved_8 = 186;
                gammaSetupInfo.Reserved_9 = 252;
                gammaSetupInfo.Reserved_10 = 253;
                gammaSetupInfo.Reserved_11 = 118;
                gammaSetupInfo.Reserved_12 = 214;
                gammaSetupInfo.Reserved_13 = 169;
                gammaSetupInfo.Reserved_14 = 169;
                gammaSetupInfo.Reserved_15 = 108;
                gammaSetupInfo.Reserved_16 = 76;
                gammaSetupInfo.Reserved_17 = 220;
                gammaSetupInfo.Reserved_18 = 87;
                gammaSetupInfo.Reserved_19 = 245;
                gammaSetupInfo.Reserved_20 = 122;
                gammaSetupInfo.Reserved_21 = 5;
                gammaSetupInfo.Reserved_22 = 243;
                gammaSetupInfo.Reserved_23 = 59;
                gammaSetupInfo.Reserved_24 = 75;
                gammaSetupInfo.Reserved_25 = 69;
                gammaSetupInfo.Reserved_26 = 251;
                gammaSetupInfo.Reserved_27 = 59;
                gammaSetupInfo.Reserved_28 = 27;
                gammaSetupInfo.Reserved_29 = 23;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractGammaSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSetupInfo;
        }

        private BHM_ConfigComponent_GammaSetupInfo ExtractGammaSetupInfoDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_GammaSetupInfo gammaSetupInfo = null;
            try
            {
                int offset = 148;

                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        gammaSetupInfo = new BHM_ConfigComponent_GammaSetupInfo();

                        gammaSetupInfo.ReadOnSide = ConversionMethods.ByteToInt32(byteData[offset]);
                        gammaSetupInfo.MaxParameterChange = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                        gammaSetupInfo.AlarmHysteresis = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                        gammaSetupInfo.AllowedPhaseDispersion = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                        gammaSetupInfo.DaysToCalculateTrend = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                        gammaSetupInfo.DaysToCalculateTCoefficient = ConversionMethods.ByteToInt32(byteData[offset + 5]);
                        gammaSetupInfo.AveragingForGamma = ConversionMethods.ByteToInt32(byteData[offset + 6]);
                        gammaSetupInfo.ReReadOnAlarm = ConversionMethods.ByteToInt32(byteData[offset + 7]);
                        gammaSetupInfo.MinDiagGamma = ConversionMethods.ByteToInt32(byteData[offset + 8]);
                        gammaSetupInfo.NEGtg = ConversionMethods.ByteToInt32(byteData[offset + 9]);
                        // this jump in offset is correct
                        gammaSetupInfo.DaysToCalculateBASELINE = ConversionMethods.ByteToInt32(byteData[offset + 368]);
                        gammaSetupInfo.LoadChannel = ConversionMethods.ByteToInt32(byteData[offset + 369]);
                        gammaSetupInfo.ReduceCoeff = ConversionMethods.ByteToInt32(byteData[offset + 370]);
                        gammaSetupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 371]);
                        gammaSetupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 372]);
                        gammaSetupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 373]);
                        gammaSetupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 374]);
                        gammaSetupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 375]);
                        gammaSetupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 376]);
                        gammaSetupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 377]);
                        gammaSetupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 378]);
                        gammaSetupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 379]);
                        gammaSetupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 380]);
                        gammaSetupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 381]);
                        gammaSetupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 382]);
                        gammaSetupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 383]);
                        gammaSetupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 384]);
                        gammaSetupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 385]);
                        gammaSetupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 386]);
                        gammaSetupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[offset + 387]);
                        gammaSetupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[offset + 388]);
                        gammaSetupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[offset + 389]);
                        gammaSetupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[offset + 390]);
                        gammaSetupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[offset + 391]);
                        gammaSetupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[offset + 392]);
                        gammaSetupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[offset + 393]);
                        gammaSetupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[offset + 394]);
                        gammaSetupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[offset + 395]);
                        gammaSetupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[offset + 396]);
                        gammaSetupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[offset + 397]);
                        gammaSetupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[offset + 398]);
                        gammaSetupInfo.Reserved_28 = ConversionMethods.ByteToInt32(byteData[offset + 399]);
                        gammaSetupInfo.Reserved_29 = ConversionMethods.ByteToInt32(byteData[offset + 400]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractGammaSetupInfoDataFromByteData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractGammaSetupInfoDataFromByteData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractGammaSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return gammaSetupInfo;
        }

        private void ConvertGammaSetupInfoToBytes(BHM_ConfigComponent_GammaSetupInfo gammaSetupInfo, ref Byte[] byteData)
        {
            try
            {
                int offset = 148;

                if (gammaSetupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[offset] = ConversionMethods.Int32ToByte(gammaSetupInfo.ReadOnSide);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(gammaSetupInfo.MaxParameterChange);
                            byteData[offset + 2] = ConversionMethods.Int32ToByte(gammaSetupInfo.AlarmHysteresis);
                            byteData[offset + 3] = ConversionMethods.Int32ToByte(gammaSetupInfo.AllowedPhaseDispersion);
                            byteData[offset + 4] = ConversionMethods.Int32ToByte(gammaSetupInfo.DaysToCalculateTrend);
                            byteData[offset + 5] = ConversionMethods.Int32ToByte(gammaSetupInfo.DaysToCalculateTCoefficient);
                            byteData[offset + 6] = ConversionMethods.Int32ToByte(gammaSetupInfo.AveragingForGamma);
                            byteData[offset + 7] = ConversionMethods.Int32ToByte(gammaSetupInfo.ReReadOnAlarm);
                            byteData[offset + 8] = ConversionMethods.Int32ToByte(gammaSetupInfo.MinDiagGamma);
                            byteData[offset + 9] = ConversionMethods.Int32ToByte(gammaSetupInfo.NEGtg);
                            // this jump in offset is correct
                            byteData[offset + 368] = ConversionMethods.Int32ToByte(gammaSetupInfo.DaysToCalculateBASELINE);
                            byteData[offset + 369] = ConversionMethods.Int32ToByte(gammaSetupInfo.LoadChannel);
                            byteData[offset + 370] = ConversionMethods.Int32ToByte(gammaSetupInfo.ReduceCoeff);
                            byteData[offset + 371] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_0);
                            byteData[offset + 372] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_1);
                            byteData[offset + 373] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_2);
                            byteData[offset + 374] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_3);
                            byteData[offset + 375] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_4);
                            byteData[offset + 376] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_5);
                            byteData[offset + 377] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_6);
                            byteData[offset + 378] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_7);
                            byteData[offset + 379] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_8);
                            byteData[offset + 380] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_9);
                            byteData[offset + 381] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_10);
                            byteData[offset + 382] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_11);
                            byteData[offset + 383] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_12);
                            byteData[offset + 384] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_13);
                            byteData[offset + 385] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_14);
                            byteData[offset + 386] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_15);
                            byteData[offset + 387] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_16);
                            byteData[offset + 388] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_17);
                            byteData[offset + 389] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_18);
                            byteData[offset + 390] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_19);
                            byteData[offset + 391] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_20);
                            byteData[offset + 392] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_21);
                            byteData[offset + 393] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_22);
                            byteData[offset + 394] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_23);
                            byteData[offset + 395] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_24);
                            byteData[offset + 396] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_25);
                            byteData[offset + 397] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_26);
                            byteData[offset + 398] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_27);
                            byteData[offset + 399] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_28);
                            byteData[offset + 400] = ConversionMethods.Int32ToByte(gammaSetupInfo.Reserved_29);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertGammaSetupInfoToBytes(BHM_Config_GammaSetupInfo, Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertGammaSetupInfoToBytes(BHM_Config_GammaSetupInfo, Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertGammaSetupInfoToBytes(BHM_Config_GammaSetupInfo, Byte[]):\nInput BHM_Config_GammaSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertGammaSetupInfoToBytes(BHM_Config_GammaSetupInfo, Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_InitialParameters InitializeEmptyInitialParameters()
        {
            BHM_ConfigComponent_InitialParameters initialParameters = new BHM_ConfigComponent_InitialParameters();
            try
            {
                initialParameters.Day = 0;
                initialParameters.Month = 0;
                initialParameters.Year = 0;
                initialParameters.Hour = 0;
                initialParameters.Min = 0;

                initialParameters.Reserved_0 = 0;
                initialParameters.Reserved_1 = 0;
                initialParameters.Reserved_2 = 0;
                initialParameters.Reserved_3 = 0;
                initialParameters.Reserved_4 = 0;
                initialParameters.Reserved_5 = 0;
                initialParameters.Reserved_6 = 0;
                initialParameters.Reserved_7 = 0;
                initialParameters.Reserved_8 = 0;
                initialParameters.Reserved_9 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialParameters;
        }

        private BHM_ConfigComponent_InitialParameters ExtractInitialParametersDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_InitialParameters initialParameters = null;
            try
            {
                int offset = 549;

                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        initialParameters = new BHM_ConfigComponent_InitialParameters();

                        initialParameters.Day = ConversionMethods.ByteToInt32(byteData[offset]);
                        initialParameters.Month = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                        initialParameters.Year = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                        initialParameters.Hour = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                        initialParameters.Min = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                        // this jump in offset is correct
                        initialParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 91]);
                        initialParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 92]);
                        initialParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 93]);
                        initialParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 94]);
                        initialParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 95]);
                        initialParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 96]);
                        initialParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 97]);
                        initialParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 98]);
                        initialParameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 99]);
                        initialParameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 100]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialParameters;
        }

        private void ConvertInitialParametersToBytes(BHM_ConfigComponent_InitialParameters initialParameters, ref Byte[] byteData)
        {
            try
            {
                int offset = 549;

                if (initialParameters != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[offset] = ConversionMethods.Int32ToByte(initialParameters.Day);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(initialParameters.Month);
                            byteData[offset + 2] = ConversionMethods.Int32ToByte(initialParameters.Year);
                            byteData[offset + 3] = ConversionMethods.Int32ToByte(initialParameters.Hour);
                            byteData[offset + 4] = ConversionMethods.Int32ToByte(initialParameters.Min);
                            // this jump in offset is correct
                            byteData[offset + 91] = ConversionMethods.Int32ToByte(initialParameters.Reserved_0);
                            byteData[offset + 92] = ConversionMethods.Int32ToByte(initialParameters.Reserved_1);
                            byteData[offset + 93] = ConversionMethods.Int32ToByte(initialParameters.Reserved_2);
                            byteData[offset + 94] = ConversionMethods.Int32ToByte(initialParameters.Reserved_3);
                            byteData[offset + 95] = ConversionMethods.Int32ToByte(initialParameters.Reserved_4);
                            byteData[offset + 96] = ConversionMethods.Int32ToByte(initialParameters.Reserved_5);
                            byteData[offset + 97] = ConversionMethods.Int32ToByte(initialParameters.Reserved_6);
                            byteData[offset + 98] = ConversionMethods.Int32ToByte(initialParameters.Reserved_7);
                            byteData[offset + 99] = ConversionMethods.Int32ToByte(initialParameters.Reserved_8);
                            byteData[offset + 100] = ConversionMethods.Int32ToByte(initialParameters.Reserved_9);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertInitialParametersToBytes(BHM_Config_InitialParameters, Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertInitialParametersToBytes(BHM_Config_InitialParameters, Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertInitialParametersToBytes(BHM_Config_InitialParameters, Byte[]):\nInput BHM_Config_InitialParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertInitialParametersToBytes(BHM_Config_InitialParameters, Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_ZkSetupInfo InitializeEmptyZkSetupInfo()
        {
            BHM_ConfigComponent_ZkSetupInfo zkSetupInfo = new BHM_ConfigComponent_ZkSetupInfo();
            try
            {
                zkSetupInfo.CalcZkOnSide = 0;
                zkSetupInfo.MaxParameterChange = 187;
                zkSetupInfo.AlarmHysteresis = 107;
                zkSetupInfo.DaysToCalculateTrend = 134;
                zkSetupInfo.AveragingForZk = 5;
                zkSetupInfo.ReReadOnAlarm = 178;
                zkSetupInfo.ZkDays = 30;
                zkSetupInfo.Empty = 23;
                zkSetupInfo.FaultDate = 0;

                zkSetupInfo.Reserved_0 = 81;
                zkSetupInfo.Reserved_1 = 167;
                zkSetupInfo.Reserved_2 = 95;
                zkSetupInfo.Reserved_3 = 128;
                zkSetupInfo.Reserved_4 = 96;
                zkSetupInfo.Reserved_5 = 150;
                zkSetupInfo.Reserved_6 = 190;
                zkSetupInfo.Reserved_7 = 107;
                zkSetupInfo.Reserved_8 = 216;
                zkSetupInfo.Reserved_9 = 135;
                zkSetupInfo.Reserved_10 = 11;
                zkSetupInfo.Reserved_11 = 2;
                zkSetupInfo.Reserved_12 = 232;
                zkSetupInfo.Reserved_13 = 42;
                zkSetupInfo.Reserved_14 = 203;
                zkSetupInfo.Reserved_15 = 21;
                zkSetupInfo.Reserved_16 = 98;
                zkSetupInfo.Reserved_17 = 44;
                zkSetupInfo.Reserved_18 = 192;
                zkSetupInfo.Reserved_19 = 10;
                zkSetupInfo.Reserved_20 = 3;
                zkSetupInfo.Reserved_21 = 43;
                zkSetupInfo.Reserved_22 = 164;
                zkSetupInfo.Reserved_23 = 76;
                zkSetupInfo.Reserved_24 = 8;
                zkSetupInfo.Reserved_25 = 88;
                zkSetupInfo.Reserved_26 = 240;
                zkSetupInfo.Reserved_27 = 4;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractZkSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSetupInfo;
        }

        private BHM_ConfigComponent_ZkSetupInfo ExtractZkSetupInfoDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_ZkSetupInfo zkSetupInfo = null;
            try
            {
                int offset = 794;

                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        zkSetupInfo = new BHM_ConfigComponent_ZkSetupInfo();

                        zkSetupInfo.CalcZkOnSide = ConversionMethods.ByteToInt32(byteData[offset]);
                        zkSetupInfo.MaxParameterChange = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                        zkSetupInfo.AlarmHysteresis = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                        zkSetupInfo.DaysToCalculateTrend = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                        zkSetupInfo.AveragingForZk = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                        zkSetupInfo.ReReadOnAlarm = ConversionMethods.ByteToInt32(byteData[offset + 5]);
                        zkSetupInfo.ZkDays = ConversionMethods.ByteToInt32(byteData[offset + 6]);
                        zkSetupInfo.Empty = ConversionMethods.ByteToInt32(byteData[offset + 7]);
                        // this jump in offset is correct
                        zkSetupInfo.FaultDate = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 43], byteData[offset + 44], byteData[offset + 45], byteData[offset + 46]);
                        zkSetupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 47]);
                        zkSetupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 48]);
                        zkSetupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 49]);
                        zkSetupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 50]);
                        zkSetupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 51]);
                        zkSetupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 52]);
                        zkSetupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 53]);
                        zkSetupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 54]);
                        zkSetupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 55]);
                        zkSetupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 56]);
                        zkSetupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 57]);
                        zkSetupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 58]);
                        zkSetupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 59]);
                        zkSetupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 60]);
                        zkSetupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 61]);
                        zkSetupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 62]);
                        zkSetupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[offset + 63]);
                        zkSetupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[offset + 64]);
                        zkSetupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[offset + 65]);
                        zkSetupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[offset + 66]);
                        zkSetupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[offset + 67]);
                        zkSetupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[offset + 68]);
                        zkSetupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[offset + 69]);
                        zkSetupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[offset + 70]);
                        zkSetupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[offset + 71]);
                        zkSetupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[offset + 72]);
                        zkSetupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[offset + 73]);
                        zkSetupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[offset + 74]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractZkSetupInfoDataFromByteData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractZkSetupInfoDataFromByteData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractZkSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return zkSetupInfo;
        }

        private void ConvertZkSetupInfoToBytes(BHM_ConfigComponent_ZkSetupInfo zkSetupInfo, ref Byte[] byteData)
        {
            try
            {
                int offset = 794;
                if (zkSetupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[offset] = ConversionMethods.Int32ToByte(zkSetupInfo.CalcZkOnSide);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(zkSetupInfo.MaxParameterChange);
                            byteData[offset + 2] = ConversionMethods.Int32ToByte(zkSetupInfo.AlarmHysteresis);
                            byteData[offset + 3] = ConversionMethods.Int32ToByte(zkSetupInfo.DaysToCalculateTrend);
                            byteData[offset + 4] = ConversionMethods.Int32ToByte(zkSetupInfo.AveragingForZk);
                            byteData[offset + 5] = ConversionMethods.Int32ToByte(zkSetupInfo.ReReadOnAlarm);
                            byteData[offset + 6] = ConversionMethods.Int32ToByte(zkSetupInfo.ZkDays);
                            byteData[offset + 7] = ConversionMethods.Int32ToByte(zkSetupInfo.Empty);
                            Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)zkSetupInfo.FaultDate), 0, byteData, offset + 43, 4);
                            // this jump in offset is correct
                            byteData[offset + 47] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_0);
                            byteData[offset + 48] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_1);
                            byteData[offset + 49] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_2);
                            byteData[offset + 50] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_3);
                            byteData[offset + 51] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_4);
                            byteData[offset + 52] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_5);
                            byteData[offset + 53] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_6);
                            byteData[offset + 54] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_7);
                            byteData[offset + 55] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_8);
                            byteData[offset + 56] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_9);
                            byteData[offset + 57] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_10);
                            byteData[offset + 58] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_11);
                            byteData[offset + 59] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_12);
                            byteData[offset + 60] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_13);
                            byteData[offset + 61] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_14);
                            byteData[offset + 62] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_15);
                            byteData[offset + 63] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_16);
                            byteData[offset + 64] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_17);
                            byteData[offset + 65] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_18);
                            byteData[offset + 66] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_19);
                            byteData[offset + 67] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_20);
                            byteData[offset + 68] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_21);
                            byteData[offset + 69] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_22);
                            byteData[offset + 70] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_23);
                            byteData[offset + 71] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_24);
                            byteData[offset + 72] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_25);
                            byteData[offset + 73] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_26);
                            byteData[offset + 74] = ConversionMethods.Int32ToByte(zkSetupInfo.Reserved_27);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertZkSetupInfoToBytes(BHM_Config_ZkSetupInfo, Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertZkSetupInfoToBytes(BHM_Config_ZkSetupInfo, Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertZkSetupInfoToBytes(BHM_Config_ZkSetupInfo, Byte[]):\nInput BHM_Config_ZkSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertZkSetupInfoToBytes(BHM_Config_ZkSetupInfo, Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private BHM_ConfigComponent_InitialZkParameters InitializeEmptyInitialZkParameters()
        {
            BHM_ConfigComponent_InitialZkParameters initialZkParameters = new BHM_ConfigComponent_InitialZkParameters();
            try
            {
                initialZkParameters.Day = 0;
                initialZkParameters.Month = 0;
                initialZkParameters.Year = 0;
                initialZkParameters.Hour = 0;
                initialZkParameters.Min = 0;

                initialZkParameters.Reserved_0 = 44;
                initialZkParameters.Reserved_1 = 138;
                initialZkParameters.Reserved_2 = 128;
                initialZkParameters.Reserved_3 = 224;
                initialZkParameters.Reserved_4 = 52;
                initialZkParameters.Reserved_5 = 3;
                initialZkParameters.Reserved_6 = 132;
                initialZkParameters.Reserved_7 = 171;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractInitialZkParametersDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialZkParameters;
        }

        private BHM_ConfigComponent_InitialZkParameters ExtractInitialZkParametersDataFromByteData(Byte[] byteData)
        {
            BHM_ConfigComponent_InitialZkParameters initialZkParameters = null;
            try
            {
                int offset = 869;

                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        initialZkParameters = new BHM_ConfigComponent_InitialZkParameters();

                        initialZkParameters.Day = ConversionMethods.ByteToInt32(byteData[offset]);
                        initialZkParameters.Month = ConversionMethods.ByteToInt32(byteData[offset + 1]);
                        initialZkParameters.Year = ConversionMethods.ByteToInt32(byteData[offset + 2]);
                        initialZkParameters.Hour = ConversionMethods.ByteToInt32(byteData[offset + 3]);
                        initialZkParameters.Min = ConversionMethods.ByteToInt32(byteData[offset + 4]);
                        // this jump in offset is correct
                        initialZkParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 121]);
                        initialZkParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 122]);
                        initialZkParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 123]);
                        initialZkParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 124]);
                        initialZkParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 125]);
                        initialZkParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 126]);
                        initialZkParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 127]);
                        initialZkParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 128]);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractInitialZkParametersDataFromByteData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractInitialZkParametersDataFromByteData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractInitialZkParametersDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return initialZkParameters;
        }

        private void ConvertInitialZkParametersToBytes(BHM_ConfigComponent_InitialZkParameters initialZkParameters, ref Byte[] byteData)
        {
            try
            {
                int offset = 869;

                if (initialZkParameters != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[offset] = ConversionMethods.Int32ToByte(initialZkParameters.Day);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(initialZkParameters.Month);
                            byteData[offset + 2] = ConversionMethods.Int32ToByte(initialZkParameters.Year);
                            byteData[offset + 3] = ConversionMethods.Int32ToByte(initialZkParameters.Hour);
                            byteData[offset + 4] = ConversionMethods.Int32ToByte(initialZkParameters.Min);
                            // this jump in offset is correct
                            byteData[offset + 121] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_0);
                            byteData[offset + 122] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_1);
                            byteData[offset + 123] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_2);
                            byteData[offset + 124] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_3);
                            byteData[offset + 125] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_4);
                            byteData[offset + 126] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_5);
                            byteData[offset + 127] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_6);
                            byteData[offset + 128] = ConversionMethods.Int32ToByte(initialZkParameters.Reserved_7);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertInitialZkParametersToBytes(BHM_Config_InitialZkParameters, Byte[]):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertInitialZkParametersToBytes(BHM_Config_InitialZkParameters, Byte[]):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertInitialZkParametersToBytes(BHM_Config_InitialZkParameters, Byte[]):\nInput BHM_Config_InitialZkParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertInitialZkParametersToBytes(BHM_Config_InitialZkParameters, Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private BHM_ConfigComponent_SideToSideData InitializeEmptySideToSideData()
        {
            BHM_ConfigComponent_SideToSideData sideToSideData = new BHM_ConfigComponent_SideToSideData();
            try
            {
                sideToSideData.Amplitude1_0 = 0;
                sideToSideData.Amplitude1_1 = 0;
                sideToSideData.Amplitude1_2 = 0;
                sideToSideData.Amplitude2_0 = 0;
                sideToSideData.Amplitude2_1 = 0;
                sideToSideData.Amplitude2_2 = 0;
                sideToSideData.PhaseShift_0 = 0;
                sideToSideData.PhaseShift_1 = 0;
                sideToSideData.PhaseShift_2 = 0;
                sideToSideData.CurrentAmpl_0 = 0;
                sideToSideData.CurrentAmpl_1 = 0;
                sideToSideData.CurrentAmpl_2 = 0;
                sideToSideData.CurrentPhase_0 = 0;
                sideToSideData.CurrentPhase_1 = 0;
                sideToSideData.CurrentPhase_2 = 0;
                sideToSideData.ZkAmpl_0 = 0;
                sideToSideData.ZkAmpl_1 = 0;
                sideToSideData.ZkAmpl_2 = 0;
                sideToSideData.ZkPhase_0 = 0;
                sideToSideData.ZkPhase_1 = 0;
                sideToSideData.ZkPhase_2 = 0;
                sideToSideData.CurrentChannelShift = 0;
                sideToSideData.ZkPercent_0 = 2818490066;
                sideToSideData.ZkPercent_1 = 1358585598;
                sideToSideData.ZkPercent_2 = 2117962;

                sideToSideData.Reserved_0 = 227;
                sideToSideData.Reserved_1 = 255;
                sideToSideData.Reserved_2 = 118;
                sideToSideData.Reserved_3 = 52;
                sideToSideData.Reserved_4 = 49;
                sideToSideData.Reserved_5 = 122;
                sideToSideData.Reserved_6 = 200;
                sideToSideData.Reserved_7 = 0;
                sideToSideData.Reserved_8 = 144;
                sideToSideData.Reserved_9 = 21;
                sideToSideData.Reserved_10 = 225;
                sideToSideData.Reserved_11 = 110;
                sideToSideData.Reserved_12 = 52;
                sideToSideData.Reserved_13 = 0;
                sideToSideData.Reserved_14 = 138;
                sideToSideData.Reserved_15 = 25;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitializeEmptySideToSideData()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }


        private BHM_ConfigComponent_SideToSideData ExtractSideToSideDataDataFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_ConfigComponent_SideToSideData sideToSideData = null;
            try
            {
                int offset = -1;
                if (sideNumber == 0)
                {
                    offset = 874;
                }

                if (offset != -1)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            sideToSideData = new BHM_ConfigComponent_SideToSideData();

                            sideToSideData.Amplitude1_0 = ConversionMethods.BytesToSingle(byteData[offset], byteData[offset + 1], byteData[offset + 2], byteData[offset + 3]);
                            sideToSideData.Amplitude1_1 = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                            sideToSideData.Amplitude1_2 = ConversionMethods.BytesToSingle(byteData[offset + 8], byteData[offset + 9], byteData[offset + 10], byteData[offset + 11]);
                            sideToSideData.Amplitude2_0 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            sideToSideData.Amplitude2_1 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            sideToSideData.Amplitude2_2 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                            sideToSideData.PhaseShift_0 = ConversionMethods.BytesToSingle(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                            sideToSideData.PhaseShift_1 = ConversionMethods.BytesToSingle(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                            sideToSideData.PhaseShift_2 = ConversionMethods.BytesToSingle(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);
                            sideToSideData.CurrentAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 36], byteData[offset + 37], byteData[offset + 38], byteData[offset + 39]);
                            sideToSideData.CurrentAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 40], byteData[offset + 41], byteData[offset + 42], byteData[offset + 43]);
                            sideToSideData.CurrentAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 44], byteData[offset + 45], byteData[offset + 46], byteData[offset + 47]);
                            sideToSideData.CurrentPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 48], byteData[offset + 49], byteData[offset + 50], byteData[offset + 51]);
                            sideToSideData.CurrentPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 52], byteData[offset + 53], byteData[offset + 54], byteData[offset + 55]);
                            sideToSideData.CurrentPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 56], byteData[offset + 57], byteData[offset + 58], byteData[offset + 59]);
                            sideToSideData.ZkAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 60], byteData[offset + 61], byteData[offset + 62], byteData[offset + 63]);
                            sideToSideData.ZkAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 64], byteData[offset + 65], byteData[offset + 66], byteData[offset + 67]);
                            sideToSideData.ZkAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 68], byteData[offset + 69], byteData[offset + 70], byteData[offset + 71]);
                            sideToSideData.ZkPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 72], byteData[offset + 73], byteData[offset + 74], byteData[offset + 75]);
                            sideToSideData.ZkPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 76], byteData[offset + 77], byteData[offset + 78], byteData[offset + 79]);
                            sideToSideData.ZkPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 80], byteData[offset + 81], byteData[offset + 82], byteData[offset + 83]);
                            sideToSideData.CurrentChannelShift = ConversionMethods.BytesToSingle(byteData[offset + 84], byteData[offset + 85], byteData[offset + 86], byteData[offset + 87]);
                            sideToSideData.ZkPercent_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 88], byteData[offset + 89], byteData[offset + 90], byteData[offset + 91]);
                            sideToSideData.ZkPercent_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 92], byteData[offset + 93], byteData[offset + 94], byteData[offset + 95]);
                            sideToSideData.ZkPercent_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 96], byteData[offset + 97], byteData[offset + 98], byteData[offset + 99]);
                            sideToSideData.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 100]);
                            sideToSideData.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 101]);
                            sideToSideData.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 102]);
                            sideToSideData.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 103]);
                            sideToSideData.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 104]);
                            sideToSideData.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 105]);
                            sideToSideData.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 106]);
                            sideToSideData.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 107]);
                            sideToSideData.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 108]);
                            sideToSideData.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 109]);
                            sideToSideData.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 110]);
                            sideToSideData.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 111]);
                            sideToSideData.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 112]);
                            sideToSideData.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 113]);
                            sideToSideData.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 114]);
                            sideToSideData.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 115]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[], Guid):\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ExtractInitialParametersDataFromByteData(Byte[], Guid):\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ExtractSideToSideDataDataFromByteData(Byte[], int):\nBad value for sideNumber, expect 0 or 1, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractSideToSideDataDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }

        private void ConvertSideToSideDataToBytes(BHM_ConfigComponent_SideToSideData sideToSideData, int sideNumber, ref Byte[] byteData)
        {
            try
            {
                int offset = -1;

                if (sideNumber == 0)
                {
                    offset = 874;
                }

                if (offset != -1)
                {
                    if (sideToSideData != null)
                    {
                        if (byteData != null)
                        {
                            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                            {
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude1_0), 0, byteData, offset, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude1_1), 0, byteData, offset + 4, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude1_2), 0, byteData, offset + 8, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude2_0), 0, byteData, offset + 12, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude2_1), 0, byteData, offset + 16, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.Amplitude2_2), 0, byteData, offset + 20, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.PhaseShift_0), 0, byteData, offset + 24, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.PhaseShift_1), 0, byteData, offset + 28, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.PhaseShift_2), 0, byteData, offset + 32, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentAmpl_0), 0, byteData, offset + 36, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentAmpl_1), 0, byteData, offset + 40, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentAmpl_2), 0, byteData, offset + 44, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentPhase_0), 0, byteData, offset + 48, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentPhase_1), 0, byteData, offset + 52, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentPhase_2), 0, byteData, offset + 56, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkAmpl_0), 0, byteData, offset + 60, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkAmpl_1), 0, byteData, offset + 64, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkAmpl_2), 0, byteData, offset + 68, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkPhase_0), 0, byteData, offset + 72, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkPhase_1), 0, byteData, offset + 76, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.ZkPhase_2), 0, byteData, offset + 80, 4);
                                Array.Copy(ConversionMethods.SingleToBytes((Single)sideToSideData.CurrentChannelShift), 0, byteData, offset + 84, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)sideToSideData.ZkPercent_0), 0, byteData, offset + 88, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)sideToSideData.ZkPercent_1), 0, byteData, offset + 92, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)sideToSideData.ZkPercent_2), 0, byteData, offset + 96, 4);
                                byteData[offset + 100] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_0);
                                byteData[offset + 101] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_1);
                                byteData[offset + 102] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_2);
                                byteData[offset + 103] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_3);
                                byteData[offset + 104] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_4);
                                byteData[offset + 105] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_5);
                                byteData[offset + 106] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_6);
                                byteData[offset + 107] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_7);
                                byteData[offset + 108] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_8);
                                byteData[offset + 109] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_9);
                                byteData[offset + 110] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_10);
                                byteData[offset + 111] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_11);
                                byteData[offset + 112] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_12);
                                byteData[offset + 113] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_13);
                                byteData[offset + 114] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_14);
                                byteData[offset + 115] = ConversionMethods.Int32ToByte(sideToSideData.Reserved_15);
                            }
                            else
                            {
                                string errorMessage = "Error in BHM_Configuration.ConvertSideToSideDataToBytes(BHM_Config_SideToSideData, Byte[]):\nInput Byte[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertSideToSideDataToBytes(BHM_Config_SideToSideData, Byte[]):\nInput Byte[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertSideToSideDataToBytes(BHM_Config_SideToSideData, Byte[]):\nInput BHM_Config_SideToSideData was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertSideToSideDataToBytes(BHM_Config_SideToSideData, ref Byte[]):\nBad value for sideNumber, expected 0, got " + sideNumber.ToString();
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertSideToSideDataToBytes(BHM_Config_SideToSideData, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private BHM_Config_BaseLineZk ExtractBaseLineZkDataFromByteData(Byte[] byteData, Guid configurationRootID, DateTime extractDateTime)
        //        {
        //            BHM_Config_BaseLineZk baseLineZk = new BHM_Config_BaseLineZk();

        //            int offset = EXPECTED_LENGTH_OF_BYTE_ARRAY;

        //            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
        //            {
        //                baseLineZk.ID = Guid.NewGuid();
        //                baseLineZk.ConfigurationRootID = configurationRootID;
        //                baseLineZk.DateAdded = extractDateTime;
        //                baseLineZk.EntryNumber = 0;

        //                baseLineZk.ZkP_0_0 = ConversionMethods.BytesToSingle(byteData[offset], byteData[offset + 1], byteData[offset + 2], byteData[offset + 3]);
        //                baseLineZk.ZkP_0_1 = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
        //                baseLineZk.ZkP_0_2 = ConversionMethods.BytesToSingle(byteData[offset + 8], byteData[offset + 9], byteData[offset + 10], byteData[offset + 11]);
        //                baseLineZk.ZkP_1_0 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
        //                baseLineZk.ZkP_1_1 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
        //                baseLineZk.ZkP_1_2 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
        //                baseLineZk.ZkP_2_0 = ConversionMethods.BytesToSingle(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
        //                baseLineZk.ZkP_2_1 = ConversionMethods.BytesToSingle(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
        //                baseLineZk.ZkP_2_2 = ConversionMethods.BytesToSingle(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);
        //                baseLineZk.ZkP_3_0 = ConversionMethods.BytesToSingle(byteData[offset + 36], byteData[offset + 37], byteData[offset + 38], byteData[offset + 39]);
        //                baseLineZk.ZkP_3_1 = ConversionMethods.BytesToSingle(byteData[offset + 40], byteData[offset + 41], byteData[offset + 42], byteData[offset + 43]);
        //                baseLineZk.ZkP_3_2 = ConversionMethods.BytesToSingle(byteData[offset + 44], byteData[offset + 45], byteData[offset + 46], byteData[offset + 47]);
        //                baseLineZk.ZkP_4_0 = ConversionMethods.BytesToSingle(byteData[offset + 48], byteData[offset + 49], byteData[offset + 50], byteData[offset + 51]);
        //                baseLineZk.ZkP_4_1 = ConversionMethods.BytesToSingle(byteData[offset + 52], byteData[offset + 53], byteData[offset + 54], byteData[offset + 55]);
        //                baseLineZk.ZkP_4_2 = ConversionMethods.BytesToSingle(byteData[offset + 56], byteData[offset + 57], byteData[offset + 58], byteData[offset + 59]);
        //                baseLineZk.ZkP_5_0 = ConversionMethods.BytesToSingle(byteData[offset + 60], byteData[offset + 61], byteData[offset + 62], byteData[offset + 63]);
        //                baseLineZk.ZkP_5_1 = ConversionMethods.BytesToSingle(byteData[offset + 64], byteData[offset + 65], byteData[offset + 66], byteData[offset + 67]);
        //                baseLineZk.ZkP_5_2 = ConversionMethods.BytesToSingle(byteData[offset + 68], byteData[offset + 69], byteData[offset + 70], byteData[offset + 71]);
        //                baseLineZk.ZkP_6_0 = ConversionMethods.BytesToSingle(byteData[offset + 72], byteData[offset + 73], byteData[offset + 74], byteData[offset + 75]);
        //                baseLineZk.ZkP_6_1 = ConversionMethods.BytesToSingle(byteData[offset + 76], byteData[offset + 77], byteData[offset + 78], byteData[offset + 79]);
        //                baseLineZk.ZkP_6_2 = ConversionMethods.BytesToSingle(byteData[offset + 80], byteData[offset + 81], byteData[offset + 82], byteData[offset + 83]);
        //                baseLineZk.ZkP_7_0 = ConversionMethods.BytesToSingle(byteData[offset + 84], byteData[offset + 85], byteData[offset + 86], byteData[offset + 87]);
        //                baseLineZk.ZkP_7_1 = ConversionMethods.BytesToSingle(byteData[offset + 88], byteData[offset + 89], byteData[offset + 90], byteData[offset + 91]);
        //                baseLineZk.ZkP_7_2 = ConversionMethods.BytesToSingle(byteData[offset + 92], byteData[offset + 93], byteData[offset + 94], byteData[offset + 95]);
        //                baseLineZk.ZkP_8_0 = ConversionMethods.BytesToSingle(byteData[offset + 96], byteData[offset + 97], byteData[offset + 98], byteData[offset + 99]);
        //                baseLineZk.ZkP_8_1 = ConversionMethods.BytesToSingle(byteData[offset + 100], byteData[offset + 101], byteData[offset + 102], byteData[offset + 103]);
        //                baseLineZk.ZkP_8_2 = ConversionMethods.BytesToSingle(byteData[offset + 104], byteData[offset + 105], byteData[offset + 106], byteData[offset + 107]);
        //                baseLineZk.ZkP_9_0 = ConversionMethods.BytesToSingle(byteData[offset + 108], byteData[offset + 109], byteData[offset + 110], byteData[offset + 111]);
        //                baseLineZk.ZkP_9_1 = ConversionMethods.BytesToSingle(byteData[offset + 112], byteData[offset + 113], byteData[offset + 114], byteData[offset + 115]);
        //                baseLineZk.ZkP_9_2 = ConversionMethods.BytesToSingle(byteData[offset + 116], byteData[offset + 117], byteData[offset + 118], byteData[offset + 119]);

        //                baseLineZk.ZkPCount_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 120], byteData[offset + 121], byteData[offset + 122], byteData[offset + 123]);
        //                baseLineZk.ZkPCount_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 124], byteData[offset + 125], byteData[offset + 126], byteData[offset + 127]);
        //                baseLineZk.ZkPCount_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 128], byteData[offset + 129], byteData[offset + 130], byteData[offset + 131]);
        //                baseLineZk.ZkPCount_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 132], byteData[offset + 133], byteData[offset + 134], byteData[offset + 135]);
        //                baseLineZk.ZkPCount_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 136], byteData[offset + 137], byteData[offset + 138], byteData[offset + 139]);
        //                baseLineZk.ZkPCount_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 140], byteData[offset + 141], byteData[offset + 142], byteData[offset + 143]);
        //                baseLineZk.ZkPCount_6 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 144], byteData[offset + 145], byteData[offset + 146], byteData[offset + 147]);
        //                baseLineZk.ZkPCount_7 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 148], byteData[offset + 149], byteData[offset + 150], byteData[offset + 151]);
        //                baseLineZk.ZkPCount_8 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 152], byteData[offset + 153], byteData[offset + 154], byteData[offset + 155]);
        //                baseLineZk.ZkPCount_9 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 156], byteData[offset + 157], byteData[offset + 158], byteData[offset + 159]);

        //                baseLineZk.ZkPOnDate_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 160], byteData[offset + 161], byteData[offset + 162], byteData[offset + 163]);
        //                baseLineZk.ZkPOnDate_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 164], byteData[offset + 165], byteData[offset + 166], byteData[offset + 167]);
        //                baseLineZk.ZkPOnDate_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 168], byteData[offset + 169], byteData[offset + 170], byteData[offset + 171]);
        //                baseLineZk.ZkPOnDate_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 172], byteData[offset + 173], byteData[offset + 174], byteData[offset + 175]);
        //                baseLineZk.ZkPOnDate_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 176], byteData[offset + 177], byteData[offset + 178], byteData[offset + 179]);
        //                baseLineZk.ZkPOnDate_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 180], byteData[offset + 181], byteData[offset + 182], byteData[offset + 183]);
        //                baseLineZk.ZkPOnDate_6 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 184], byteData[offset + 185], byteData[offset + 186], byteData[offset + 187]);
        //                baseLineZk.ZkPOnDate_7 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 188], byteData[offset + 189], byteData[offset + 190], byteData[offset + 191]);
        //                baseLineZk.ZkPOnDate_8 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 192], byteData[offset + 193], byteData[offset + 194], byteData[offset + 195]);
        //                baseLineZk.ZkPOnDate_9 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 196], byteData[offset + 197], byteData[offset + 198], byteData[offset + 199]);
        //            }
        //            else
        //            {
        //                string errorMessage = "Error in BHM_Configuration.ExtractBaseLineZkDataFromByteData(Byte[], Guid):\nSize of byteData is too small.";
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return baseLineZk;
        //        }

        //        private void ConvertBaseLineZkToBytes(BHM_Config_BaseLineZk baseLineZk, ref Byte[] byteData)
        //        {
        //            int offset = EXPECTED_LENGTH_OF_BYTE_ARRAY;

        //            if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
        //            {
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_0_0), 0, byteData, offset, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_0_1), 0, byteData, offset + 4, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_0_2), 0, byteData, offset + 8, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_1_0), 0, byteData, offset + 12, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_1_1), 0, byteData, offset + 16, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_1_2), 0, byteData, offset + 20, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_2_0), 0, byteData, offset + 24, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_2_1), 0, byteData, offset + 28, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_2_2), 0, byteData, offset + 32, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_3_0), 0, byteData, offset + 36, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_3_1), 0, byteData, offset + 40, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_3_2), 0, byteData, offset + 44, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_4_0), 0, byteData, offset + 48, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_4_1), 0, byteData, offset + 52, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_4_2), 0, byteData, offset + 56, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_5_0), 0, byteData, offset + 60, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_5_1), 0, byteData, offset + 64, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_5_2), 0, byteData, offset + 68, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_6_0), 0, byteData, offset + 72, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_6_1), 0, byteData, offset + 76, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_6_2), 0, byteData, offset + 80, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_7_0), 0, byteData, offset + 84, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_7_1), 0, byteData, offset + 88, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_7_2), 0, byteData, offset + 92, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_8_0), 0, byteData, offset + 96, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_8_1), 0, byteData, offset + 100, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_8_2), 0, byteData, offset + 104, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_9_0), 0, byteData, offset + 108, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_9_1), 0, byteData, offset + 112, 4);
        //                Array.Copy(ConversionMethods.SingleToBytes((Single)baseLineZk.ZkP_9_2), 0, byteData, offset + 116, 4);

        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_0), 0, byteData, offset + 120, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_1), 0, byteData, offset + 124, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_2), 0, byteData, offset + 128, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_3), 0, byteData, offset + 132, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_4), 0, byteData, offset + 136, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_5), 0, byteData, offset + 140, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_6), 0, byteData, offset + 144, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_7), 0, byteData, offset + 148, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_8), 0, byteData, offset + 152, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPCount_9), 0, byteData, offset + 156, 4);

        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_0), 0, byteData, offset + 160, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_1), 0, byteData, offset + 164, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_2), 0, byteData, offset + 168, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_3), 0, byteData, offset + 172, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_4), 0, byteData, offset + 176, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_5), 0, byteData, offset + 180, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_6), 0, byteData, offset + 184, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_7), 0, byteData, offset + 188, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_8), 0, byteData, offset + 192, 4);
        //                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)baseLineZk.ZkPOnDate_9), 0, byteData, offset + 196, 4);
        //            }
        //            else
        //            {
        //                string errorMessage = "Error in BHM_Configuration.ConvertInitialParametersToBytes(BHM_Config_InitialParameters, Byte[]):\nSize of byteData is too small.";
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private List<BHM_ConfigComponent_MeasurementsInfo> InitializeEmptyMeasurementsInfoList()
        {
            List<BHM_ConfigComponent_MeasurementsInfo> measurementsInfoList = new List<BHM_ConfigComponent_MeasurementsInfo>();
            try
            {
                BHM_ConfigComponent_MeasurementsInfo singleMeasurementInfo;

                for (int i = 0; i < 50; i++)
                {
                    singleMeasurementInfo = new BHM_ConfigComponent_MeasurementsInfo();
                    singleMeasurementInfo.ItemNumber = i + 1;
                    singleMeasurementInfo.Hour = 0;
                    singleMeasurementInfo.Minute = 0;
                    measurementsInfoList.Add(singleMeasurementInfo);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        private List<BHM_ConfigComponent_MeasurementsInfo> ExtractMeasurementsInfoDataFromByteData(Byte[] byteData)
        {
            List<BHM_ConfigComponent_MeasurementsInfo> measurementsInfoList = null;
            try
            {
                BHM_ConfigComponent_MeasurementsInfo singleMeasurementInfo;
                List<BHM_ConfigComponent_MeasurementsInfo> sortedMeasurementsInfoList;
                int offset = 13;
                int hour;
                int min;
                int measurementIndex = 0;
                int totalMeasurements = 0;
                if (byteData != null)
                {
                    if (byteData.Length >= (offset + 100))
                    {
                        measurementsInfoList = new List<BHM_ConfigComponent_MeasurementsInfo>();
                        for (int i = 0; i < 50; i++)
                        {
                            hour = ConversionMethods.ByteToInt32(byteData[offset + measurementIndex]);
                            measurementIndex++;
                            min = ConversionMethods.ByteToInt32(byteData[offset + measurementIndex]);
                            measurementIndex++;

                            if ((hour > 0) || (min > 0))
                            {
                                singleMeasurementInfo = new BHM_ConfigComponent_MeasurementsInfo();
                                singleMeasurementInfo.ItemNumber = i + 1;
                                singleMeasurementInfo.Hour = hour;
                                singleMeasurementInfo.Minute = min;
                                measurementsInfoList.Add(singleMeasurementInfo);
                            }
                        }

                        var measurementsQuery =
                         from item in measurementsInfoList
                         orderby item.Hour, item.Minute
                         select item;

                        sortedMeasurementsInfoList = measurementsQuery.ToList();
                        totalMeasurements = sortedMeasurementsInfoList.Count;
                        for (int i = 0; i < totalMeasurements; i++)
                        {
                            sortedMeasurementsInfoList[i].ItemNumber = i + 1;
                        }

                        measurementsInfoList = sortedMeasurementsInfoList;
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in BHM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in BHM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        private void ConvertMeasurementsInfoToBytes(List<BHM_ConfigComponent_MeasurementsInfo> measurementsInfo, ref Byte[] byteData)
        {
            try
            {
                int offset = 13;
                int measurementIndex = 0;

                if (measurementsInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= (offset + 100))
                        {
                            foreach (BHM_ConfigComponent_MeasurementsInfo singleMeasurementInfo in measurementsInfo)
                            {
                                byteData[offset + measurementIndex] = ConversionMethods.Int32ToByte(singleMeasurementInfo.Hour);
                                measurementIndex++;
                                byteData[offset + measurementIndex] = ConversionMethods.Int32ToByte(singleMeasurementInfo.Minute);
                                measurementIndex++;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_Configuration.ConvertMeasurementsInfoToBytes(List<BHM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.ConvertMeasurementsInfoToBytes(List<BHM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.ConvertMeasurementsInfoToBytes(List<BHM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.ConvertMeasurementsInfoToBytes(List<BHM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region Configuration Comparison

        public bool ConfigurationIsTheSame(BHM_Configuration externalConfiguration)
        {
            bool isTheSame = true;
            try
            {
                /// The user cannot change these values
                //if (isTheSame && !CalibrationCoefficientIsTheSame(externalConfiguration.calibrationCoeff))
                //{
                //    isTheSame = false;
                //}
                if (isTheSame && !GammaSetupInfoIsTheSame(externalConfiguration.gammaSetupInfo))
                {
                    isTheSame = false;
                }
                if (isTheSame && !GammaSideSetupIsTheSame(gammaSideSetupSideOne, externalConfiguration.gammaSideSetupSideOne))
                {
                    isTheSame = false;
                }
                if (isTheSame && !GammaSideSetupIsTheSame(gammaSideSetupSideTwo, externalConfiguration.gammaSideSetupSideTwo))
                {
                    isTheSame = false;
                }
                if (isTheSame && !InitialParametersAreTheSame(externalConfiguration.initialParameters))
                {
                    isTheSame = false;
                }
                if (isTheSame && !InitialZkParametersAreTheSame(externalConfiguration.initialZkParameters))
                {
                    isTheSame = false;
                }
                if (isTheSame && !MeasurementsInfoListIsTheSame(externalConfiguration.measurementsInfoList))
                {
                    isTheSame = false;
                }
                if (isTheSame && !SetupInfoIsTheSame(externalConfiguration.setupInfo))
                {
                    isTheSame = false;
                }
                if (isTheSame && !SideToSideDataIsTheSame(externalConfiguration.sideToSideData))
                {
                    isTheSame = false;
                }
                if (isTheSame && !TransformerSideParametersAreTheSame(trSideParamSideOne, externalConfiguration.trSideParamSideOne))
                {
                    isTheSame = false;
                }
                if (isTheSame && !TransformerSideParametersAreTheSame(trSideParamSideTwo, externalConfiguration.trSideParamSideTwo))
                {
                    isTheSame = false;
                }
                if (isTheSame && !ZkSetupInfoIsTheSame(externalConfiguration.zkSetupInfo))
                {
                    isTheSame = false;
                }
                if (isTheSame && !ZkSideToSideSetupIsTheSame(externalConfiguration.zkSideToSideSetup))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.CalibrationCoefficientIsTheSame(BHM_ConfigComponent_CalibrationCoeff)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool CalibrationCoefficientIsTheSame(BHM_ConfigComponent_CalibrationCoeff externalCalibrationCoefficient)
        {
            bool isTheSame = true;
            try
            {
                if (Math.Abs(calibrationCoeff.TemperatureK_0 - externalCalibrationCoefficient.TemperatureK_0) > epsilon)
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureK_1 - externalCalibrationCoefficient.TemperatureK_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureK_2 - externalCalibrationCoefficient.TemperatureK_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureK_3 - externalCalibrationCoefficient.TemperatureK_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureB_0 - externalCalibrationCoefficient.TemperatureB_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureB_1 - externalCalibrationCoefficient.TemperatureB_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureB_2 - externalCalibrationCoefficient.TemperatureB_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.TemperatureB_3 - externalCalibrationCoefficient.TemperatureB_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentK_0 - externalCalibrationCoefficient.CurrentK_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentK_1 - externalCalibrationCoefficient.CurrentK_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentK_2 - externalCalibrationCoefficient.CurrentK_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentK_3 - externalCalibrationCoefficient.CurrentK_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumidityOffset_0 - externalCalibrationCoefficient.HumidityOffset_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumidityOffset_1 - externalCalibrationCoefficient.HumidityOffset_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumidityOffset_2 - externalCalibrationCoefficient.HumidityOffset_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumidityOffset_3 - externalCalibrationCoefficient.HumidityOffset_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumiditySlope_0 - externalCalibrationCoefficient.HumiditySlope_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumiditySlope_1 - externalCalibrationCoefficient.HumiditySlope_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumiditySlope_2 - externalCalibrationCoefficient.HumiditySlope_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.HumiditySlope_3 - externalCalibrationCoefficient.HumiditySlope_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_0_0 - externalCalibrationCoefficient.VoltageK_0_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_0_1 - externalCalibrationCoefficient.VoltageK_0_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_0_2 - externalCalibrationCoefficient.VoltageK_0_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_1_0 - externalCalibrationCoefficient.VoltageK_1_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_1_1 - externalCalibrationCoefficient.VoltageK_1_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.VoltageK_1_2 - externalCalibrationCoefficient.VoltageK_1_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentB_0 - externalCalibrationCoefficient.CurrentB_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentB_1 - externalCalibrationCoefficient.CurrentB_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentB_2 - externalCalibrationCoefficient.CurrentB_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentB_3 - externalCalibrationCoefficient.CurrentB_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.I4_20B - externalCalibrationCoefficient.I4_20B) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.I4_20K - externalCalibrationCoefficient.I4_20K) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentChannelOnPhase_0 - externalCalibrationCoefficient.CurrentChannelOnPhase_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentChannelOnPhase_1 - externalCalibrationCoefficient.CurrentChannelOnPhase_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentChannelOnPhase_2 - externalCalibrationCoefficient.CurrentChannelOnPhase_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(calibrationCoeff.CurrentChannelOnPhase_3 - externalCalibrationCoefficient.CurrentChannelOnPhase_3) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (calibrationCoeff.Reserved_0 != externalCalibrationCoefficient.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_1 != externalCalibrationCoefficient.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_2 != externalCalibrationCoefficient.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_3 != externalCalibrationCoefficient.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_4 != externalCalibrationCoefficient.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_5 != externalCalibrationCoefficient.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_6 != externalCalibrationCoefficient.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_7 != externalCalibrationCoefficient.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_8 != externalCalibrationCoefficient.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_9 != externalCalibrationCoefficient.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_10 != externalCalibrationCoefficient.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (calibrationCoeff.Reserved_11 != externalCalibrationCoefficient.Reserved_11))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.CalibrationCoefficientIsTheSame(BHM_ConfigComponent_CalibrationCoeff)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool GammaSetupInfoIsTheSame(BHM_ConfigComponent_GammaSetupInfo externalGammaSetupInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (gammaSetupInfo.ReadOnSide != externalGammaSetupInfo.ReadOnSide))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.MaxParameterChange != externalGammaSetupInfo.MaxParameterChange))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.AlarmHysteresis != externalGammaSetupInfo.AlarmHysteresis))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.AllowedPhaseDispersion != externalGammaSetupInfo.AllowedPhaseDispersion))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.DaysToCalculateTrend != externalGammaSetupInfo.DaysToCalculateTrend))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.DaysToCalculateTCoefficient != externalGammaSetupInfo.DaysToCalculateTCoefficient))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.AveragingForGamma != externalGammaSetupInfo.AveragingForGamma))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.ReReadOnAlarm != externalGammaSetupInfo.ReReadOnAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.MinDiagGamma != externalGammaSetupInfo.MinDiagGamma))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.NEGtg != externalGammaSetupInfo.NEGtg))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.DaysToCalculateBASELINE != externalGammaSetupInfo.DaysToCalculateBASELINE))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.LoadChannel != externalGammaSetupInfo.LoadChannel))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.ReduceCoeff != externalGammaSetupInfo.ReduceCoeff))
                {
                    isTheSame = false;
                }

                if (isTheSame && (gammaSetupInfo.Reserved_0 != externalGammaSetupInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_1 != externalGammaSetupInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_2 != externalGammaSetupInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_3 != externalGammaSetupInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_4 != externalGammaSetupInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_5 != externalGammaSetupInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_6 != externalGammaSetupInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_7 != externalGammaSetupInfo.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_8 != externalGammaSetupInfo.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_9 != externalGammaSetupInfo.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_10 != externalGammaSetupInfo.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_11 != externalGammaSetupInfo.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_12 != externalGammaSetupInfo.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_13 != externalGammaSetupInfo.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_14 != externalGammaSetupInfo.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_15 != externalGammaSetupInfo.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_16 != externalGammaSetupInfo.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_17 != externalGammaSetupInfo.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_18 != externalGammaSetupInfo.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_19 != externalGammaSetupInfo.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_20 != externalGammaSetupInfo.Reserved_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_21 != externalGammaSetupInfo.Reserved_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_22 != externalGammaSetupInfo.Reserved_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_23 != externalGammaSetupInfo.Reserved_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_24 != externalGammaSetupInfo.Reserved_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_25 != externalGammaSetupInfo.Reserved_25))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_26 != externalGammaSetupInfo.Reserved_26))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_27 != externalGammaSetupInfo.Reserved_27))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_28 != externalGammaSetupInfo.Reserved_28))
                {
                    isTheSame = false;
                }
                if (isTheSame && (gammaSetupInfo.Reserved_29 != externalGammaSetupInfo.Reserved_29))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GammaSetupInfoIsTheSame(BHM_ConfigComponent_GammaSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool GammaSideSetupIsTheSame(BHM_ConfigComponent_GammaSideSetup internalGammaSideSetup, BHM_ConfigComponent_GammaSideSetup externalGammaSideSetup)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (internalGammaSideSetup.AlarmEnable != externalGammaSideSetup.AlarmEnable))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.GammaYellowThreshold != externalGammaSideSetup.GammaYellowThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.GammaRedThreshold != externalGammaSideSetup.GammaRedThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TempCoefficient != externalGammaSideSetup.TempCoefficient))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TrendAlarm != externalGammaSideSetup.TrendAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Temperature0 != externalGammaSideSetup.Temperature0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputC_0 != externalGammaSideSetup.InputC_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputC_1 != externalGammaSideSetup.InputC_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputC_2 != externalGammaSideSetup.InputC_2))
                {
                    isTheSame = false;
                }

                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputCoeff_0 - externalGammaSideSetup.InputCoeff_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputCoeff_1 - externalGammaSideSetup.InputCoeff_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputCoeff_2 - externalGammaSideSetup.InputCoeff_2) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalGammaSideSetup.Tg0_0 != externalGammaSideSetup.Tg0_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Tg0_1 != externalGammaSideSetup.Tg0_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Tg0_2 != externalGammaSideSetup.Tg0_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.C0_0 != externalGammaSideSetup.C0_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.C0_1 != externalGammaSideSetup.C0_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.C0_2 != externalGammaSideSetup.C0_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputImpedance_0 != externalGammaSideSetup.InputImpedance_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputImpedance_1 != externalGammaSideSetup.InputImpedance_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.InputImpedance_2 != externalGammaSideSetup.InputImpedance_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.MinTemperature1 != externalGammaSideSetup.MinTemperature1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.AvgTemperature1 != externalGammaSideSetup.AvgTemperature1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.MaxTemperature1 != externalGammaSideSetup.MaxTemperature1))
                {
                    isTheSame = false;
                }

                if (isTheSame && (Math.Abs(internalGammaSideSetup.B_0 - externalGammaSideSetup.B_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.B_1 - externalGammaSideSetup.B_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.B_2 - externalGammaSideSetup.B_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.K_0 - externalGammaSideSetup.K_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.K_1 - externalGammaSideSetup.K_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.K_2 - externalGammaSideSetup.K_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputVoltage_0 - externalGammaSideSetup.InputVoltage_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputVoltage_1 - externalGammaSideSetup.InputVoltage_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.InputVoltage_2 - externalGammaSideSetup.InputVoltage_2) > epsilon))
                {
                    isTheSame = false;
                }

                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaTg_0 != externalGammaSideSetup.STABLEDeltaTg_0))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaTg_1 != externalGammaSideSetup.STABLEDeltaTg_1))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaTg_2 != externalGammaSideSetup.STABLEDeltaTg_2))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaC_0 != externalGammaSideSetup.STABLEDeltaC_0))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaC_1 != externalGammaSideSetup.STABLEDeltaC_1))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDeltaC_2 != externalGammaSideSetup.STABLEDeltaC_2))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEDate != externalGammaSideSetup.STABLEDate))
                //{
                //    isTheSame = false;
                //}
                if (isTheSame && (internalGammaSideSetup.HeatDate != externalGammaSideSetup.HeatDate))
                {
                    isTheSame = false;
                }
                //if (isTheSame && (internalGammaSideSetup.STABLETemperature != externalGammaSideSetup.STABLETemperature))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLETg_0 != externalGammaSideSetup.STABLETg_0))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLETg_1 != externalGammaSideSetup.STABLETg_1))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLETg_2 != externalGammaSideSetup.STABLETg_2))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEC_0 != externalGammaSideSetup.STABLEC_0))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEC_1 != externalGammaSideSetup.STABLEC_1))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLEC_2 != externalGammaSideSetup.STABLEC_2))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalGammaSideSetup.STABLESaved != externalGammaSideSetup.STABLESaved))
                //{
                //    isTheSame = false;
                //}

                if (isTheSame && (Math.Abs(internalGammaSideSetup.RatedVoltage - externalGammaSideSetup.RatedVoltage) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalGammaSideSetup.RatedCurrent - externalGammaSideSetup.RatedCurrent) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalGammaSideSetup.StablePhaseAmplitude_0 != externalGammaSideSetup.StablePhaseAmplitude_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StablePhaseAmplitude_1 != externalGammaSideSetup.StablePhaseAmplitude_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StablePhaseAmplitude_2 != externalGammaSideSetup.StablePhaseAmplitude_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSourceAmplitude_0 != externalGammaSideSetup.StableSourceAmplitude_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSourceAmplitude_1 != externalGammaSideSetup.StableSourceAmplitude_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSourceAmplitude_2 != externalGammaSideSetup.StableSourceAmplitude_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSignalPhase_0 != externalGammaSideSetup.StableSignalPhase_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSignalPhase_1 != externalGammaSideSetup.StableSignalPhase_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSignalPhase_2 != externalGammaSideSetup.StableSignalPhase_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSourcePhase_0 != externalGammaSideSetup.StableSourcePhase_0))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalGammaSideSetup.StableSourcePhase_1 != externalGammaSideSetup.StableSourcePhase_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.StableSourcePhase_2 != externalGammaSideSetup.StableSourcePhase_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.STABLEAvgCurrent != externalGammaSideSetup.STABLEAvgCurrent))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TgYellowThreshold != externalGammaSideSetup.TgYellowThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TgRedThreshold != externalGammaSideSetup.TgRedThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TgVariationThreshold != externalGammaSideSetup.TgVariationThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ImpedanceValue_0 != externalGammaSideSetup.ImpedanceValue_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ImpedanceValue_1 != externalGammaSideSetup.ImpedanceValue_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ImpedanceValue_2 != externalGammaSideSetup.ImpedanceValue_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.TemperatureConfig != externalGammaSideSetup.TemperatureConfig))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ExtImpedanceValue_0 != externalGammaSideSetup.ExtImpedanceValue_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ExtImpedanceValue_1 != externalGammaSideSetup.ExtImpedanceValue_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ExtImpedanceValue_2 != externalGammaSideSetup.ExtImpedanceValue_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.ExternalSync != externalGammaSideSetup.ExternalSync))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalGammaSideSetup.Reserved_0 != externalGammaSideSetup.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_1 != externalGammaSideSetup.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_2 != externalGammaSideSetup.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_3 != externalGammaSideSetup.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_4 != externalGammaSideSetup.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_5 != externalGammaSideSetup.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_6 != externalGammaSideSetup.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_7 != externalGammaSideSetup.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_8 != externalGammaSideSetup.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_9 != externalGammaSideSetup.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_10 != externalGammaSideSetup.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_11 != externalGammaSideSetup.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_12 != externalGammaSideSetup.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_13 != externalGammaSideSetup.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_14 != externalGammaSideSetup.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_15 != externalGammaSideSetup.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalGammaSideSetup.Reserved_16 != externalGammaSideSetup.Reserved_16))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GammaSideSetupIsTheSame(BHM_ConfigComponent_GammaSideSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InitialParametersAreTheSame(BHM_ConfigComponent_InitialParameters externalInitialParameters)
        {
            bool isTheSame = true;
            try
            {
                //if (isTheSame && (initialParameters.Month != externalInitialParameters.Month))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (initialParameters.Year != externalInitialParameters.Year))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (initialParameters.Hour != externalInitialParameters.Hour))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (initialParameters.Min != externalInitialParameters.Min))
                //{
                //    isTheSame = false;
                //}
                if (isTheSame && (initialParameters.Reserved_0 != externalInitialParameters.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_1 != externalInitialParameters.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_2 != externalInitialParameters.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_3 != externalInitialParameters.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_4 != externalInitialParameters.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_5 != externalInitialParameters.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_6 != externalInitialParameters.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_7 != externalInitialParameters.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_8 != externalInitialParameters.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialParameters.Reserved_9 != externalInitialParameters.Reserved_9))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitialParametersAreTheSame(BHM_ConfigComponent_InitialParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InitialZkParametersAreTheSame(BHM_ConfigComponent_InitialZkParameters externalInitialZkParameters)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (initialZkParameters.Day != externalInitialZkParameters.Day))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Month != externalInitialZkParameters.Month))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Year != externalInitialZkParameters.Year))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Hour != externalInitialZkParameters.Hour))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Min != externalInitialZkParameters.Min))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_0 != externalInitialZkParameters.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_1 != externalInitialZkParameters.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_2 != externalInitialZkParameters.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_3 != externalInitialZkParameters.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_4 != externalInitialZkParameters.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_5 != externalInitialZkParameters.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_6 != externalInitialZkParameters.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (initialZkParameters.Reserved_7 != externalInitialZkParameters.Reserved_7))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.InitialZkParametersAreTheSame(BHM_ConfigComponent_InitialZkParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool MeasurementsInfoIsTheSame(BHM_ConfigComponent_MeasurementsInfo internalMeasurementsInfo, BHM_ConfigComponent_MeasurementsInfo externalMeausurementsInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (internalMeasurementsInfo.ItemNumber != externalMeausurementsInfo.ItemNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalMeasurementsInfo.ItemNumber != externalMeausurementsInfo.ItemNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalMeasurementsInfo.ItemNumber != externalMeausurementsInfo.ItemNumber))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.MeasurementsInfoIsTheSame(BHM_ConfigComponent_MeasurementsInfo, BHM_ConfigComponent_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool MeasurementsInfoListIsTheSame(List<BHM_ConfigComponent_MeasurementsInfo> externalMeasurementsInfoList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.measurementsInfoList != null)
                {
                    if (externalMeasurementsInfoList != null)
                    {
                        count = this.measurementsInfoList.Count;
                        if (count == externalMeasurementsInfoList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = MeasurementsInfoIsTheSame(this.measurementsInfoList[i], externalMeasurementsInfoList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.MeasurementsInfoListIsTheSame(List<BHM_ConfigComponent_MeasurementsInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.MeasurementsInfoListIsTheSame(List<BHM_ConfigComponent_MeasurementsInfo>)\nthis.measurementsInfoList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.MeasurementsInfoIsTheSame(BHM_ConfigComponent_MeasurementsInfo, BHM_ConfigComponent_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool SetupInfoIsTheSame(BHM_ConfigComponent_SetupInfo externalSetupInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (setupInfo.DeviceNumber != externalSetupInfo.DeviceNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.BaudRate != externalSetupInfo.BaudRate))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.OutTime != externalSetupInfo.OutTime))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.DisplayFlag != externalSetupInfo.DisplayFlag))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.SaveDays != externalSetupInfo.SaveDays))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Stopped != externalSetupInfo.Stopped))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Relay != externalSetupInfo.Relay))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.TimeOfRelayAlarm != externalSetupInfo.TimeOfRelayAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ScheduleType != externalSetupInfo.ScheduleType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.DTime_Hour != externalSetupInfo.DTime_Hour))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.DTime_Min != externalSetupInfo.DTime_Min))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.SyncType != externalSetupInfo.SyncType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.InternalSyncFrequency != externalSetupInfo.InternalSyncFrequency))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ModBusProtocol != externalSetupInfo.ModBusProtocol))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ReadAnalogFromRegister != externalSetupInfo.ReadAnalogFromRegister))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.HeaterOnTemperature != externalSetupInfo.HeaterOnTemperature))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AutoBalans_Month != externalSetupInfo.AutoBalans_Month))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AutoBalans_Year != externalSetupInfo.AutoBalans_Year))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AutoBalans_Day != externalSetupInfo.AutoBalans_Day))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AutoBalans_Hour != externalSetupInfo.AutoBalans_Hour))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AutoBalansActive != externalSetupInfo.AutoBalansActive))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ReadAnalogFromRegister != externalSetupInfo.ReadAnalogFromRegister))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.NoLoadTestActive != externalSetupInfo.NoLoadTestActive))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.WorkingDays != externalSetupInfo.WorkingDays))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_0 != externalSetupInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_1 != externalSetupInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_2 != externalSetupInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_3 != externalSetupInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_4 != externalSetupInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_5 != externalSetupInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_6 != externalSetupInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_7 != externalSetupInfo.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_8 != externalSetupInfo.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_9 != externalSetupInfo.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_10 != externalSetupInfo.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_11 != externalSetupInfo.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_12 != externalSetupInfo.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_13 != externalSetupInfo.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_14 != externalSetupInfo.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_15 != externalSetupInfo.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_16 != externalSetupInfo.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_17 != externalSetupInfo.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_18 != externalSetupInfo.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_19 != externalSetupInfo.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_20 != externalSetupInfo.Reserved_20))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.SetupInfoIsTheSame(BHM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;


        }
        private bool SideToSideDataIsTheSame(BHM_ConfigComponent_SideToSideData externalSideToSideData)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude1_0 - externalSideToSideData.Amplitude1_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude1_1 - externalSideToSideData.Amplitude1_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude1_2 - externalSideToSideData.Amplitude1_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude2_0 - externalSideToSideData.Amplitude2_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude2_1 - externalSideToSideData.Amplitude2_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.Amplitude2_2 - externalSideToSideData.Amplitude2_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.PhaseShift_0 - externalSideToSideData.PhaseShift_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.PhaseShift_1 - externalSideToSideData.PhaseShift_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.PhaseShift_2 - externalSideToSideData.PhaseShift_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentAmpl_0 - externalSideToSideData.CurrentAmpl_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentAmpl_1 - externalSideToSideData.CurrentAmpl_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentAmpl_2 - externalSideToSideData.CurrentAmpl_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentPhase_0 - externalSideToSideData.CurrentPhase_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentPhase_1 - externalSideToSideData.CurrentPhase_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentPhase_2 - externalSideToSideData.CurrentPhase_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkAmpl_0 - externalSideToSideData.ZkAmpl_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkAmpl_1 - externalSideToSideData.ZkAmpl_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkAmpl_2 - externalSideToSideData.ZkAmpl_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkPhase_0 - externalSideToSideData.ZkPhase_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkPhase_1 - externalSideToSideData.ZkPhase_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.ZkPhase_2 - externalSideToSideData.ZkPhase_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(sideToSideData.CurrentChannelShift - externalSideToSideData.CurrentChannelShift) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (sideToSideData.ZkPercent_0 != externalSideToSideData.ZkPercent_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.ZkPercent_1 != externalSideToSideData.ZkPercent_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.ZkPercent_2 != externalSideToSideData.ZkPercent_2))
                {
                    isTheSame = false;
                }

                if (isTheSame && (sideToSideData.Reserved_0 != externalSideToSideData.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_1 != externalSideToSideData.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_2 != externalSideToSideData.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_3 != externalSideToSideData.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_4 != externalSideToSideData.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_5 != externalSideToSideData.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_6 != externalSideToSideData.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_7 != externalSideToSideData.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_8 != externalSideToSideData.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_9 != externalSideToSideData.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_10 != externalSideToSideData.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_11 != externalSideToSideData.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_12 != externalSideToSideData.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_13 != externalSideToSideData.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_14 != externalSideToSideData.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (sideToSideData.Reserved_15 != externalSideToSideData.Reserved_15))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.SideToSideDataIsTheSame(BHM_ConfigComponent_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool TransformerSideParametersAreTheSame(BHM_ConfigComponent_TrSideParameters internalTrSideParameters, BHM_ConfigComponent_TrSideParameters externalTrSideParameters)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (internalTrSideParameters.ChPhaseShift != externalTrSideParameters.ChPhaseShift))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.PhaseAmplitude_0 != externalTrSideParameters.PhaseAmplitude_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.PhaseAmplitude_1 != externalTrSideParameters.PhaseAmplitude_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.PhaseAmplitude_2 != externalTrSideParameters.PhaseAmplitude_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SourceAmplitude_0 != externalTrSideParameters.SourceAmplitude_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SourceAmplitude_1 != externalTrSideParameters.SourceAmplitude_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SourceAmplitude_2 != externalTrSideParameters.SourceAmplitude_2))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalTrSideParameters.Temperature != externalTrSideParameters.Temperature))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.GammaAmplitude != externalTrSideParameters.GammaAmplitude))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.GammaPhase != externalTrSideParameters.GammaPhase))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.KT != externalTrSideParameters.KT))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.KTPhase != externalTrSideParameters.KTPhase))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.DefectCode != externalTrSideParameters.DefectCode))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.KTSaved != externalTrSideParameters.KTSaved))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalTrSideParameters.SignalPhase_0 != externalTrSideParameters.SignalPhase_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SignalPhase_1 != externalTrSideParameters.SignalPhase_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SignalPhase_2 != externalTrSideParameters.SignalPhase_2))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalTrSideParameters.SourcePhase_0 != externalTrSideParameters.SourcePhase_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SourcePhase_1 != externalTrSideParameters.SourcePhase_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.SourcePhase_2 != externalTrSideParameters.SourcePhase_2))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalTrSideParameters.RPN != externalTrSideParameters.RPN))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.TrSideParamCurrent != externalTrSideParameters.TrSideParamCurrent))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalTrSideParameters.Reserved_0 != externalTrSideParameters.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.Reserved_1 != externalTrSideParameters.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.Reserved_2 != externalTrSideParameters.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTrSideParameters.Reserved_3 != externalTrSideParameters.Reserved_3))
                {
                    isTheSame = false;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.TransformerSideParametersAreTheSame(BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_TrSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool ZkSetupInfoIsTheSame(BHM_ConfigComponent_ZkSetupInfo externalZkSetupInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (zkSetupInfo.CalcZkOnSide != externalZkSetupInfo.CalcZkOnSide))
                {
                    isTheSame = false;
                }

                if (isTheSame && (zkSetupInfo.MaxParameterChange != externalZkSetupInfo.MaxParameterChange))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.AlarmHysteresis != externalZkSetupInfo.AlarmHysteresis))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.DaysToCalculateTrend != externalZkSetupInfo.DaysToCalculateTrend))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.AveragingForZk != externalZkSetupInfo.AveragingForZk))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.ReReadOnAlarm != externalZkSetupInfo.ReReadOnAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.ZkDays != externalZkSetupInfo.ZkDays))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Empty != externalZkSetupInfo.Empty))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.FaultDate != externalZkSetupInfo.FaultDate))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_0 != externalZkSetupInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_1 != externalZkSetupInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_2 != externalZkSetupInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_3 != externalZkSetupInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_4 != externalZkSetupInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_5 != externalZkSetupInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_6 != externalZkSetupInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_7 != externalZkSetupInfo.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_8 != externalZkSetupInfo.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_9 != externalZkSetupInfo.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_10 != externalZkSetupInfo.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_11 != externalZkSetupInfo.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_12 != externalZkSetupInfo.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_13 != externalZkSetupInfo.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_14 != externalZkSetupInfo.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_15 != externalZkSetupInfo.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_16 != externalZkSetupInfo.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_17 != externalZkSetupInfo.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_18 != externalZkSetupInfo.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_19 != externalZkSetupInfo.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_20 != externalZkSetupInfo.Reserved_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_21 != externalZkSetupInfo.Reserved_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_22 != externalZkSetupInfo.Reserved_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_23 != externalZkSetupInfo.Reserved_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_24 != externalZkSetupInfo.Reserved_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_25 != externalZkSetupInfo.Reserved_25))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_26 != externalZkSetupInfo.Reserved_26))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSetupInfo.Reserved_27 != externalZkSetupInfo.Reserved_27))
                {
                    isTheSame = false;
                }
            }

            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.TransformerSideParametersAreTheSame(BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_TrSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool ZkSideToSideSetupIsTheSame(BHM_ConfigComponent_ZkSideToSideSetup externalZkSideToSideSetup)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (zkSideToSideSetup.ZkYellowThreshold != externalZkSideToSideSetup.ZkYellowThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.ZkRedThreshold != externalZkSideToSideSetup.ZkRedThreshold))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.ZkTrendAlarm != externalZkSideToSideSetup.ZkTrendAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_0 != externalZkSideToSideSetup.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_1 != externalZkSideToSideSetup.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_2 != externalZkSideToSideSetup.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_3 != externalZkSideToSideSetup.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_4 != externalZkSideToSideSetup.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_5 != externalZkSideToSideSetup.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_6 != externalZkSideToSideSetup.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_7 != externalZkSideToSideSetup.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_8 != externalZkSideToSideSetup.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_9 != externalZkSideToSideSetup.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_10 != externalZkSideToSideSetup.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_11 != externalZkSideToSideSetup.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_12 != externalZkSideToSideSetup.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_13 != externalZkSideToSideSetup.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_14 != externalZkSideToSideSetup.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_15 != externalZkSideToSideSetup.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_16 != externalZkSideToSideSetup.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_17 != externalZkSideToSideSetup.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_18 != externalZkSideToSideSetup.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_19 != externalZkSideToSideSetup.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_20 != externalZkSideToSideSetup.Reserved_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_21 != externalZkSideToSideSetup.Reserved_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_22 != externalZkSideToSideSetup.Reserved_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_23 != externalZkSideToSideSetup.Reserved_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_24 != externalZkSideToSideSetup.Reserved_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_25 != externalZkSideToSideSetup.Reserved_25))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_26 != externalZkSideToSideSetup.Reserved_26))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_27 != externalZkSideToSideSetup.Reserved_27))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_28 != externalZkSideToSideSetup.Reserved_28))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_29 != externalZkSideToSideSetup.Reserved_29))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_30 != externalZkSideToSideSetup.Reserved_30))
                {
                    isTheSame = false;
                }
                if (isTheSame && (zkSideToSideSetup.Reserved_31 != externalZkSideToSideSetup.Reserved_31))
                {
                    isTheSame = false;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.TransformerSideParametersAreTheSame(BHM_ConfigComponent_TrSideParameters, BHM_ConfigComponent_TrSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        #endregion

        public static BHM_Configuration CopyConfiguration(BHM_Configuration inputConfiguration)
        {
            BHM_Configuration outputConfiguration = null;
            try
            {
                Byte[] byteData;
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        byteData = inputConfiguration.ConvertConfigurationToByteArray();
                        outputConfiguration = new BHM_Configuration(byteData, inputConfiguration.setupInfo.FirmwareVersion);
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.CopyConfiguration(BHM_Configuration)\nInput BHM_Configuration was missing some key elements, treated as empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.CopyConfiguration(BHM_Configuration)\nInput BHM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.CopyConfiguration(BHM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputConfiguration;
        }

        public List<string> GetConfigurationAsListOfStrings()
        {
            List<string> configurationValues = new List<string>();
            try
            {
                string entry;
                if (AllConfigurationMembersAreNonNull())
                {
                    configurationValues.Add("Firmware Version: " + Math.Round(setupInfo.FirmwareVersion, 2).ToString());
                    entry = "Monitoring: ";
                    if (setupInfo.Stopped == 0)
                    {
                        entry += "Disabled";
                    }
                    else
                    {
                        entry += "Enabled";
                    }
                    configurationValues.Add(entry);

                    configurationValues.Add("Modbus Address: " + setupInfo.DeviceNumber.ToString());
                    configurationValues.Add("Hysterisis: " + gammaSetupInfo.AlarmHysteresis.ToString() + " %");
                    configurationValues.Add("Confirmation Measurement: " + gammaSetupInfo.ReReadOnAlarm.ToString() + " minutes");
                    configurationValues.Add("Averaging Unn: " + gammaSetupInfo.AveragingForGamma.ToString() + " cycles");
                    configurationValues.Add("Allowable phase difference: " + gammaSetupInfo.AllowedPhaseDispersion.ToString() + " degrees");
                    configurationValues.Add("Baud rate: " + GetBaudRateFromConfigurationValue(setupInfo.BaudRate).ToString());
                    configurationValues.Add("Days to calculate trend: " + gammaSetupInfo.DaysToCalculateTrend.ToString());
                    configurationValues.Add("Days to calculate temperature coefficient: " + gammaSetupInfo.DaysToCalculateTCoefficient.ToString());
                    configurationValues.Add("Days to calculate tangent: " + gammaSetupInfo.MinDiagGamma.ToString());
                    configurationValues.Add("Measurement schedule");
                    if (setupInfo.ScheduleType == 0)
                    {
                        configurationValues.Add("Every " + setupInfo.DTime_Hour.ToString() + "hours and " + setupInfo.DTime_Min.ToString() + " minutes");
                    }
                    else
                    {
                        foreach (BHM_ConfigComponent_MeasurementsInfo measurementsInfo in measurementsInfoList)
                        {
                            configurationValues.Add(measurementsInfo.Hour.ToString() + " hours   " + measurementsInfo.Minute.ToString() + " minutes"); 
                        }
                    }

                    configurationValues.Add("Side one configuration:");
                    configurationValues.AddRange(GetBushingSetConfigurationAsListOfStrings(1));
                    configurationValues.Add("");
                    configurationValues.Add("Side two configuration:");
                    configurationValues.AddRange(GetBushingSetConfigurationAsListOfStrings(2));
                    configurationValues.Add("");
                    configurationValues.Add("Initial data:");
                    configurationValues.Add("Measurement date: " + ConversionMethods.ConvertIntegerValuesToDateTime(initialParameters.Month, initialParameters.Day,
                                                                                                 initialParameters.Year + 2000, initialParameters.Hour,
                                                                                                 initialParameters.Min).ToString());
                    configurationValues.Add("Side one:");
                    configurationValues.AddRange(GetInitialBalanceDataAsListOfString(1));
                    configurationValues.Add("Side two:");
                    configurationValues.AddRange(GetInitialBalanceDataAsListOfString(2));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GetConfigurationAsListOfStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationValues;
        }

        List<String> GetBushingSetConfigurationAsListOfStrings(int sideNumber)
        {
            List<string> bushingConfigurationValues = new List<string>();
            try
            {
                string entry;
                BHM_ConfigComponent_GammaSideSetup gammaSideSetup = null;
                int alarmEnable = 0;
                int temperatureSensor = 0;
                int readOnSide = gammaSetupInfo.ReadOnSide;
                bool measurementsAreEnabled = false;
                bool temperatureCoefficientAlarmEnabled = false;
                bool rateOfUnnChangealarmEnabled = false;
                if (sideNumber == 1)
                {
                    gammaSideSetup = gammaSideSetupSideOne;
                }
                else if (sideNumber == 2)
                {
                    gammaSideSetup = gammaSideSetupSideTwo;
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.GetBushingSetConfigurationAsListOfStrings(BHM_ConfigComponent_TrSideParameters)\nInput value for side number was " + sideNumber.ToString()+ ", which is incorrect";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                if (gammaSideSetup != null)
                {
                    if (AllConfigurationMembersAreNonNull())
                    {
                        entry = "Measurements: ";
                        if (sideNumber == 1)
                        {
                            if ((readOnSide == 1) || (readOnSide == 3))
                            {
                                measurementsAreEnabled = true;
                            }
                        }
                        else if (sideNumber == 2)
                        {
                            if ((readOnSide == 2) || (readOnSide == 3))
                            {
                                measurementsAreEnabled = true;
                            }
                        }
                        if (measurementsAreEnabled)
                        {
                            entry += "Enabled";
                        }
                        else
                        {
                            entry += "Disabled";
                        }
                        bushingConfigurationValues.Add(entry);

                        bushingConfigurationValues.Add("Voltage settings: " + Math.Round(gammaSideSetup.RatedVoltage, 2).ToString() + " kV");
                        bushingConfigurationValues.Add("Current settings: " + Math.Round(gammaSideSetup.RatedCurrent, 2).ToString() + " amps");

                        bushingConfigurationValues.Add("Input impedence phase 1: " + Math.Round(gammaSideSetup.InputImpedance_0 / 100.0, 2).ToString() + " ohms");
                        bushingConfigurationValues.Add("Input impedence phase 2: " + Math.Round(gammaSideSetup.InputImpedance_1 / 100.0, 2).ToString() + " ohms");
                        bushingConfigurationValues.Add("Input impedence phase 3: " + Math.Round(gammaSideSetup.InputImpedance_1 / 100.0, 2).ToString() + " ohms");

                        bushingConfigurationValues.Add("Amplitude Unn warning threshold value: " + Math.Round(gammaSideSetup.GammaYellowThreshold / 20.0, 2).ToString());
                        bushingConfigurationValues.Add("Amplitude Unn alarm threshold value: " + Math.Round(gammaSideSetup.GammaRedThreshold / 20.0, 2).ToString());

                        alarmEnable = gammaSideSetup.AlarmEnable;
                        if (alarmEnable == 3)
                        {
                            temperatureCoefficientAlarmEnabled = true;
                        }
                        else if (alarmEnable == 5)
                        {
                            rateOfUnnChangealarmEnabled = true;
                        }
                        else if (alarmEnable == 7)
                        {
                            temperatureCoefficientAlarmEnabled = true;
                            rateOfUnnChangealarmEnabled = true;
                        }

                        entry = "Temperature coefficient alarm: ";
                        if (temperatureCoefficientAlarmEnabled)
                        {
                            entry += "Enabled";
                        }
                        else
                        {
                            entry += "Disabled";
                        }
                        bushingConfigurationValues.Add(entry);
                        bushingConfigurationValues.Add("Temperature coefficient alarm threshold value: " + Math.Round(gammaSideSetup.TempCoefficient / 500.0, 2).ToString());

                        entry = "Rate of Unn change alarm: ";
                        if (rateOfUnnChangealarmEnabled)
                        {
                            entry += "Enabled";
                        }
                        else
                        {
                            entry += "Disabled";
                        }
                        bushingConfigurationValues.Add(entry);
                        bushingConfigurationValues.Add("Rate if Unn change alarm threshold value: " + Math.Round(gammaSideSetup.TrendAlarm / 5.0, 2).ToString());

                        bushingConfigurationValues.Add("Transformer nameplate data:");
                        bushingConfigurationValues.Add("Tangent phase 1: " + Math.Round(gammaSideSetup.Tg0_0 / 100.0, 2).ToString());
                        bushingConfigurationValues.Add("Tangent phase 2: " + Math.Round(gammaSideSetup.Tg0_1 / 100.0, 2).ToString());
                        bushingConfigurationValues.Add("Tangent phase 3: " + Math.Round(gammaSideSetup.Tg0_2 / 100.0, 2).ToString());
                        bushingConfigurationValues.Add("Capacitance phase 1: " + Math.Round(gammaSideSetup.C0_0 / 10.0, 2).ToString());
                        bushingConfigurationValues.Add("Capacitance phase 2: " + Math.Round(gammaSideSetup.C0_1 / 10.0, 2).ToString());
                        bushingConfigurationValues.Add("Capacitance phase 3: " + Math.Round(gammaSideSetup.C0_2 / 10.0, 2).ToString());
                        bushingConfigurationValues.Add("Temperature at measurement: " + (gammaSideSetup.Temperature0 - 70).ToString());

                        temperatureSensor = gammaSideSetup.TemperatureConfig;

                        entry = "Temperature sensor: ";
                        if (temperatureSensor == 0)
                        {
                            entry += "Not Connected/Data from SCADA";
                        }
                        else if (temperatureSensor == 1)
                        {
                            entry += "Connected to channel 1";
                        }
                        else if (temperatureSensor == 2)
                        {
                            entry += "Connected to channel 2";
                        }
                        else if (temperatureSensor == 3)
                        {
                            entry += "Connected to channel 3";
                        }
                        bushingConfigurationValues.Add(entry);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GetBushingSetConfigurationAsListOfStrings(BHM_ConfigComponent_TrSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return bushingConfigurationValues;
        }

        private List<String> GetInitialBalanceDataAsListOfString(int sideNumber)
        {
            List<String> balanceDataList = new List<string>();
            try
            {
                BHM_ConfigComponent_TrSideParameters trSideParameters = null;

                if (sideNumber == 1)
                {
                    trSideParameters = trSideParamSideOne;
                }
                else if (sideNumber == 2)
                {
                    trSideParameters = trSideParamSideTwo;
                }

                if (trSideParameters != null)
                {
                    if (AllConfigurationMembersAreNonNull())
                    {
                        balanceDataList.Add("Imbalance: " + Math.Round(trSideParameters.GammaAmplitude / 100.0, 2).ToString() + " %");
                        balanceDataList.Add("Imbalance phase gradient: " + Math.Round(trSideParameters.GammaPhase / 100.0, 2).ToString());
                        balanceDataList.Add("Temperature: " + (trSideParameters.Temperature - 70).ToString() + " ºC");
                        balanceDataList.Add("Input signal amplitude phase 1: " + Math.Round(trSideParameters.PhaseAmplitude_0 / 10.0, 2).ToString() + " mV");
                        balanceDataList.Add("Input signal amplitude phase 2: " + Math.Round(trSideParameters.PhaseAmplitude_1 / 10.0, 2).ToString() + " mV");
                        balanceDataList.Add("Input signal amplitude phase 3: " + Math.Round(trSideParameters.PhaseAmplitude_2 / 10.0, 2).ToString() + " mV");
                        balanceDataList.Add("Input signal shift phase 1: " + Math.Round(trSideParameters.SignalPhase_0 / 100.0, 2).ToString() + " deg");
                        balanceDataList.Add("Input signal shift phase 2: " + Math.Round(trSideParameters.SignalPhase_1 / 100.0, 2).ToString() + " deg");
                        balanceDataList.Add("Input signal shift phase 3: " + Math.Round(trSideParameters.SignalPhase_2 / 100.0, 2).ToString() + " deg");
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GetInitialBalanceDataAsListOfString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return balanceDataList;
        }

        private int GetBaudRateFromConfigurationValue(int configValue)
        {
            int baudRate = 0;
            try
            {
                switch (configValue)
                {
                    case 0:
                        baudRate = 9600;
                        break;
                    case 1:
                        baudRate = 38400;
                        break;
                    case 2:
                        baudRate = 57600;
                        break;
                    case 3:
                        baudRate = 115200;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.GetBaudRateFromConfigurationValue(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return baudRate;
        }

    }
}
