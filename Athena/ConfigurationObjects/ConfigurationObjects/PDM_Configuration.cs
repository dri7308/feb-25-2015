﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using GeneralUtilities;

namespace ConfigurationObjects
{
    public class PDM_Configuration
    {
        public List<PDM_ConfigComponent_ChannelInfo> channelInfoList = null;
        public List<PDM_ConfigComponent_MeasurementsInfo> measurementsInfoList = null;
        public PDM_ConfigComponent_PDSetupInfo pdSetupInfo = null;
        public PDM_ConfigComponent_SetupInfo setupInfo = null;

        private static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 1432;
        public static int expectedLengthOfPDMConfigurationArray
        {
            get
            {
                return EXPECTED_LENGTH_OF_BYTE_ARRAY;
            }
        }

        private double epsilon = 1.0e-5;

        public PDM_Configuration(List<PDM_ConfigComponent_ChannelInfo> inputChannelInfoList, List<PDM_ConfigComponent_MeasurementsInfo> inputMeasurementsInfoList,
                                 PDM_ConfigComponent_PDSetupInfo inputPDSetupInfo, PDM_ConfigComponent_SetupInfo inputSetupInfo)
        {
            try
            {
                if (inputChannelInfoList != null)
                {
                    if (inputChannelInfoList.Count == 15)
                    {
                        channelInfoList = inputChannelInfoList;
                        measurementsInfoList = inputMeasurementsInfoList;
                        pdSetupInfo = inputPDSetupInfo;
                        setupInfo = inputSetupInfo;
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.PDM_Configuration()\nThe channel info list has the wrong number of items in it.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.PDM_Configuration()\nThe channel info list was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
                if (!AllMembersAreNonNull())
                {
                    SetAllMembersToNull();

                    string errorMessage = "Error in PDM_Configuration.PDM_Configuration()\nFailed to read some entries from the database";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.PDM_Configuration(List<PDM_ConfigComponent_ChannelInfo>, List<PDM_ConfigComponent_MeasurementsInfo>, PDM_ConfigComponent_PDSetupInfo, PDM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public PDM_Configuration(Byte[] byteData, double firmwareVersion)
        {
            try
            {
                SetConfiguration(byteData, firmwareVersion);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.PDM_Configuration(Byte[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public PDM_Configuration(Byte[] byteData)
        {
            try
            {
                SetConfiguration(byteData, 0.00);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.PDM_Configuration(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public PDM_Configuration()
        {
            try
            {
                channelInfoList = new List<PDM_ConfigComponent_ChannelInfo>();
                measurementsInfoList = new List<PDM_ConfigComponent_MeasurementsInfo>();
                pdSetupInfo = new PDM_ConfigComponent_PDSetupInfo();
                setupInfo = new PDM_ConfigComponent_SetupInfo();
                setupInfo.FirmwareVersion = 0.0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.PDM_Configuration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public void InitializeConfigurationToZeroes()
        {
            try
            {
                channelInfoList = InitializeEmptyChannelInfoList();
                measurementsInfoList = InitializeEmptyMeasurementsInfoList();
                pdSetupInfo = InitializeEmptyPDSetupInfo();
                setupInfo = InitializeEmptySetupInfo();
                setupInfo.FirmwareVersion = 0.0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.InitializeConfigurationToZeroes()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void SetConfiguration(Byte[] byteData, double firmwareVersion)
        {
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        channelInfoList = ExtractChannelInfoDataFromByteData(byteData);
                        measurementsInfoList = ExtractMeasurementsInfoDataFromByteData(byteData);
                        pdSetupInfo = ExtractPDSetupInfoDataFromByteData(byteData);
                        setupInfo = ExtractSetupInfoDataFromByteData(byteData);
                        setupInfo.FirmwareVersion = firmwareVersion;

                        if (!AllMembersAreNonNull())
                        {
                            SetAllMembersToNull();
                            string errorMessage = "Error in PDM_Configuration.SetConfiguration(Byte[], double)\nFailed to convert the byte data to internal data";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        SetAllMembersToNull();
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.SetConfiguration(Byte[], double)\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.SetConfiguration(Byte[], double)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Byte[] ConvertConfigurationToByteArray()
        {
            Byte[] byteData = null;
            try
            {
                if (AllMembersAreNonNull())
                {
                    byteData = new Byte[EXPECTED_LENGTH_OF_BYTE_ARRAY];
                    ConvertChannelInfoToBytes(channelInfoList, ref byteData);
                    ConvertMeasurementsInfoToBytes(measurementsInfoList, ref byteData);
                    ConvertPDSetupInfoToBytes(pdSetupInfo, ref byteData);
                    ConvertSetupInfoToBytes(setupInfo, ref byteData);
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ConvertConfigurationToByteArray()\nSome members were null, so you cannot create the byte array";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConvertConfigurationToByteArray()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return byteData;
        }

        public bool AllMembersAreNonNull()
        {
            bool allNonNull = true;
            try
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("The following PDM_Configuration members are null: ");

                if (channelInfoList == null)
                {
                    allNonNull = false;
                    errorMessage.Append("channelInfoList ");
                }
                if (measurementsInfoList == null)
                {
                    allNonNull = false;
                    errorMessage.Append("measurementsInfoList ");
                }
                if (pdSetupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("pdSetupInfo ");
                }
                if (setupInfo == null)
                {
                    allNonNull = false;
                    errorMessage.Append("setupInfo ");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.AllMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allNonNull;
        }

        private void SetAllMembersToNull()
        {
            channelInfoList = null;
            measurementsInfoList = null;
            pdSetupInfo = null;
            setupInfo = null;
        }

        public void DeleteAllInternalData()
        {
            SetAllMembersToNull();
        }

        #region Configuration

        #region ChannelInfo

        private List<PDM_ConfigComponent_ChannelInfo> InitializeEmptyChannelInfoList()
        {
            List<PDM_ConfigComponent_ChannelInfo> channelInfoList = new List<PDM_ConfigComponent_ChannelInfo>();
            try
            {
                PDM_ConfigComponent_ChannelInfo channelInfo;

                for (int i = 0; i < 15; i++)
                {
                    channelInfo = new PDM_ConfigComponent_ChannelInfo();

                    channelInfo.ChannelNumber = i;
                    channelInfo.ChannelIsOn = 0;
                    channelInfo.ChannelSensitivity = 10.0;
                    channelInfo.PDMaxWidth = 1;
                    channelInfo.PDIntervalTime = 2;
                    channelInfo.P_0 = 5;
                    channelInfo.P_1 = 30;
                    channelInfo.P_2 = 100;
                    channelInfo.P_3 = 50;
                    channelInfo.P_4 = 500;
                    channelInfo.P_5 = 5000;

                    channelInfo.CHPhase = 0;
                    channelInfo.CalcPDILimit = 32;
                    channelInfo.Filter = 0;
                    channelInfo.NoiseOn_0 = 0;
                    channelInfo.NoiseOn_1 = 0;
                    channelInfo.NoiseOn_2 = 0;
                    channelInfo.NoiseType_0 = 0;
                    channelInfo.NoiseType_1 = 0;
                    channelInfo.NoiseType_2 = 0;
                    channelInfo.NoiseShift_0 = 6;
                    channelInfo.NoiseShift_1 = 6;
                    channelInfo.NoiseShift_2 = 6;
                    channelInfo.MinNoiseLevel_0 = 3;
                    channelInfo.MinNoiseLevel_1 = 3;
                    channelInfo.MinNoiseLevel_2 = 3;
                    channelInfo.TimeOfArrival_0 = 1;
                    channelInfo.Polarity_0 = 0;
                    channelInfo.Ref_0 = 0;
                    channelInfo.RefShift_0 = 9;

                    channelInfo.Reserved_0 = 0;
                    channelInfo.Reserved_1 = 0;
                    channelInfo.Reserved_2 = 0;
                    channelInfo.Reserved_3 = 0;
                    channelInfo.Reserved_4 = 0;
                    channelInfo.Reserved_5 = 0;
                    channelInfo.Reserved_6 = 0;
                    channelInfo.Reserved_7 = 0;

                    channelInfoList.Add(channelInfo);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.InitializeEmptyChannelInfoList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfoList;
        }

        private List<PDM_ConfigComponent_ChannelInfo> ExtractChannelInfoDataFromByteData(Byte[] byteData)
        {
            List<PDM_ConfigComponent_ChannelInfo> channelInfoList = null;
            try
            {
                int offset = 328;
                PDM_ConfigComponent_ChannelInfo channelInfo;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        channelInfoList = new List<PDM_ConfigComponent_ChannelInfo>();

                        for (int i = 0; i < 15; i++)
                        {
                            channelInfo = new PDM_ConfigComponent_ChannelInfo();

                            channelInfo.ChannelNumber = i;
                            channelInfo.ChannelIsOn = ConversionMethods.ByteToInt32(byteData[offset]);
                            // skipped 3 bytes, probably due to alignment, not used at all by the device
                            channelInfo.ChannelSensitivity = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                            channelInfo.PDMaxWidth = ConversionMethods.ByteToInt32(byteData[offset + 8]);
                            channelInfo.PDIntervalTime = ConversionMethods.ByteToInt32(byteData[offset + 9]);
                            // skipped 2 bytes, probably due to alignment, not used at all by the device
                            channelInfo.P_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            channelInfo.P_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            channelInfo.P_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                            channelInfo.P_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                            channelInfo.P_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                            channelInfo.P_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);
                            //
                            channelInfo.CHPhase = ConversionMethods.ByteToInt32(byteData[offset + 36]);
                            channelInfo.CalcPDILimit = ConversionMethods.ByteToInt32(byteData[offset + 37]);
                            channelInfo.Filter = ConversionMethods.ByteToInt32(byteData[offset + 38]);
                            channelInfo.NoiseOn_0 = ConversionMethods.ByteToInt32(byteData[offset + 39]);
                            channelInfo.NoiseOn_1 = ConversionMethods.ByteToInt32(byteData[offset + 40]);
                            channelInfo.NoiseOn_2 = ConversionMethods.ByteToInt32(byteData[offset + 41]);
                            channelInfo.NoiseType_0 = ConversionMethods.ByteToInt32(byteData[offset + 42]);
                            channelInfo.NoiseType_1 = ConversionMethods.ByteToInt32(byteData[offset + 43]);
                            channelInfo.NoiseType_2 = ConversionMethods.ByteToInt32(byteData[offset + 44]);
                            channelInfo.NoiseShift_0 = ConversionMethods.ByteToInt32(byteData[offset + 45]);
                            channelInfo.NoiseShift_1 = ConversionMethods.ByteToInt32(byteData[offset + 46]);
                            channelInfo.NoiseShift_2 = ConversionMethods.ByteToInt32(byteData[offset + 47]);
                            channelInfo.MinNoiseLevel_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 48], byteData[offset + 49]);
                            channelInfo.MinNoiseLevel_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 50], byteData[offset + 51]);
                            channelInfo.MinNoiseLevel_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 52], byteData[offset + 53]);
                            channelInfo.TimeOfArrival_0 = ConversionMethods.ByteToInt32(byteData[offset + 54]);
                            channelInfo.Polarity_0 = ConversionMethods.ByteToInt32(byteData[offset + 55]);
                            channelInfo.Ref_0 = ConversionMethods.ByteToInt32(byteData[offset + 56]);
                            channelInfo.RefShift_0 = ConversionMethods.ByteToInt32(byteData[offset + 57]);

                            channelInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 58]);
                            channelInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 59]);
                            channelInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 60]);
                            channelInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 61]);
                            channelInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 62]);
                            channelInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 63]);
                            channelInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 64]);
                            channelInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 65]);

                            channelInfoList.Add(channelInfo);

                            offset += 68;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ExtractChannelInfoDataFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        channelInfoList = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ExtractChannelInfoDataFromByteData(Byte[])\nbInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ExtractChannelInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelInfoList;
        }

        private void ConvertChannelInfoToBytes(List<PDM_ConfigComponent_ChannelInfo> channelInfoList, ref Byte[] byteData)
        {
            try
            {
                int offset;
                if (channelInfoList != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            foreach (PDM_ConfigComponent_ChannelInfo channelInfo in channelInfoList)
                            {
                                offset = 328 + channelInfo.ChannelNumber * 68;

                                byteData[offset] = ConversionMethods.Int32ToByte(channelInfo.ChannelIsOn);
                                // skip 3 bytes, probably for alignment, not used at all by device
                                Array.Copy(ConversionMethods.SingleToBytes((Single)channelInfo.ChannelSensitivity), 0, byteData, offset + 4, 4);
                                byteData[offset + 8] = ConversionMethods.Int32ToByte(channelInfo.PDMaxWidth);
                                byteData[offset + 9] = ConversionMethods.Int32ToByte(channelInfo.PDIntervalTime);
                                // skip 2 bytes, probably for alignment, not used at all by device
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_0), 0, byteData, offset + 12, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_1), 0, byteData, offset + 16, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_2), 0, byteData, offset + 20, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_3), 0, byteData, offset + 24, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_4), 0, byteData, offset + 28, 4);
                                Array.Copy(ConversionMethods.UInt32ToUnsignedBytes((UInt32)channelInfo.P_5), 0, byteData, offset + 32, 4);
                                //
                                byteData[offset + 36] = ConversionMethods.Int32ToByte(channelInfo.CHPhase);
                                byteData[offset + 37] = ConversionMethods.Int32ToByte(channelInfo.CalcPDILimit);
                                byteData[offset + 38] = ConversionMethods.Int32ToByte(channelInfo.Filter);
                                byteData[offset + 39] = ConversionMethods.Int32ToByte(channelInfo.NoiseOn_0);
                                byteData[offset + 40] = ConversionMethods.Int32ToByte(channelInfo.NoiseOn_1);
                                byteData[offset + 41] = ConversionMethods.Int32ToByte(channelInfo.NoiseOn_2);
                                byteData[offset + 42] = ConversionMethods.Int32ToByte(channelInfo.NoiseType_0);
                                byteData[offset + 43] = ConversionMethods.Int32ToByte(channelInfo.NoiseType_1);
                                byteData[offset + 44] = ConversionMethods.Int32ToByte(channelInfo.NoiseType_2);
                                byteData[offset + 45] = ConversionMethods.Int32ToByte(channelInfo.NoiseShift_0);
                                byteData[offset + 46] = ConversionMethods.Int32ToByte(channelInfo.NoiseShift_1);
                                byteData[offset + 47] = ConversionMethods.Int32ToByte(channelInfo.NoiseShift_2);
                                //
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)channelInfo.MinNoiseLevel_0), 0, byteData, offset + 48, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)channelInfo.MinNoiseLevel_1), 0, byteData, offset + 50, 2);
                                Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)channelInfo.MinNoiseLevel_2), 0, byteData, offset + 52, 2);
                                //
                                byteData[offset + 54] = ConversionMethods.Int32ToByte(channelInfo.TimeOfArrival_0);
                                byteData[offset + 55] = ConversionMethods.Int32ToByte(channelInfo.Polarity_0);
                                byteData[offset + 56] = ConversionMethods.Int32ToByte(channelInfo.Ref_0);
                                byteData[offset + 57] = ConversionMethods.Int32ToByte(channelInfo.RefShift_0);
                                //
                                byteData[offset + 58] = ConversionMethods.Int32ToByte(channelInfo.Reserved_0);
                                byteData[offset + 59] = ConversionMethods.Int32ToByte(channelInfo.Reserved_1);
                                byteData[offset + 60] = ConversionMethods.Int32ToByte(channelInfo.Reserved_2);
                                byteData[offset + 61] = ConversionMethods.Int32ToByte(channelInfo.Reserved_3);
                                byteData[offset + 62] = ConversionMethods.Int32ToByte(channelInfo.Reserved_4);
                                byteData[offset + 63] = ConversionMethods.Int32ToByte(channelInfo.Reserved_5);
                                byteData[offset + 64] = ConversionMethods.Int32ToByte(channelInfo.Reserved_6);
                                byteData[offset + 65] = ConversionMethods.Int32ToByte(channelInfo.Reserved_7);
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(List<PDM_Config_ChannelInfo>, ref Byte[])\nbyteData array does not have the expected minimum number of entries.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(List<PDM_Config_ChannelInfo>, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(List<PDM_Config_ChannelInfo>, ref Byte[])\nInput List<PDM_Config_ChannelInfo> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConvertSetupInfoToBytes(List<PDM_Config_ChannelInfo>, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        #region MeasurementsInfo

        private List<PDM_ConfigComponent_MeasurementsInfo> InitializeEmptyMeasurementsInfoList()
        {
             List<PDM_ConfigComponent_MeasurementsInfo> measurementsInfoList = new List<PDM_ConfigComponent_MeasurementsInfo>();
             try
             {
                 PDM_ConfigComponent_MeasurementsInfo measurementsInfo;

                 for (int i = 0; i < 50; i++)
                 {
                     measurementsInfo = new PDM_ConfigComponent_MeasurementsInfo();
                     measurementsInfo.ItemNumber = i;
                     measurementsInfo.Hour = 0;
                     measurementsInfo.Minute = 0;
                     measurementsInfoList.Add(measurementsInfo);
                 }
             }
             catch (Exception ex)
             {
                 string errorMessage = "Exception thrown in PDM_Configuration.InitializeEmptyMeasurementsInfoList()\nMessage: " + ex.Message;
                 LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
             }
            return measurementsInfoList;
        }

        private List<PDM_ConfigComponent_MeasurementsInfo> ExtractMeasurementsInfoDataFromByteData(Byte[] byteData)
        {
            List<PDM_ConfigComponent_MeasurementsInfo> measurementsInfoList = null;
            try
            {
                PDM_ConfigComponent_MeasurementsInfo measurementsInfo;
                List<PDM_ConfigComponent_MeasurementsInfo> sortedMeasurementsInfoList;
                int offset = 3;
                int hour;
                int minute;
                int measurementIndex = 0;
                int totalMeasurements = 0;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        measurementsInfoList = new List<PDM_ConfigComponent_MeasurementsInfo>();
                        
                        for (int i = 0; i < 50; i++)
                        {
                            hour = ConversionMethods.ByteToInt32(byteData[offset + measurementIndex]);
                            measurementIndex++;
                            minute = ConversionMethods.ByteToInt32(byteData[offset + measurementIndex]);
                            measurementIndex++;
                            if ((hour != 0) || (minute != 0))
                            {
                                measurementsInfo = new PDM_ConfigComponent_MeasurementsInfo();
                                measurementsInfo.ItemNumber = i;
                                measurementsInfo.Hour = hour;
                                measurementsInfo.Minute = minute;
                                measurementsInfoList.Add(measurementsInfo);
                            }
                        }

                        var measurementsQuery =
                        from item in measurementsInfoList
                        orderby item.Hour, item.Minute
                        select item;

                        sortedMeasurementsInfoList = measurementsQuery.ToList();
                        totalMeasurements = sortedMeasurementsInfoList.Count;
                        for (int i = 0; i < totalMeasurements; i++)
                        {
                            sortedMeasurementsInfoList[i].ItemNumber = i + 1;
                        }

                        measurementsInfoList = sortedMeasurementsInfoList;

                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        measurementsInfoList = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ExtractMeasurementsInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return measurementsInfoList;
        }

        private void ConvertMeasurementsInfoToBytes(List<PDM_ConfigComponent_MeasurementsInfo> measurementsInfoList, ref Byte[] byteData)
        {
            try
            {
                int offset;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        foreach (PDM_ConfigComponent_MeasurementsInfo measurementsInfo in measurementsInfoList)
                        {
                            offset = 3 + measurementsInfo.ItemNumber * 2;
                            byteData[offset] = ConversionMethods.Int32ToByte(measurementsInfo.Hour);
                            byteData[offset + 1] = ConversionMethods.Int32ToByte(measurementsInfo.Minute);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ConvertMeasurementsInfoToBytes(List<PDM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ConvertMeasurementsInfoToBytes(List<PDM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConvertMeasurementsInfoToBytes(List<PDM_ConfigComponent_MeasurementsInfo>, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        #endregion

        private PDM_ConfigComponent_PDSetupInfo InitializeEmptyPDSetupInfo()
        {
            PDM_ConfigComponent_PDSetupInfo pdSetupInfo = new PDM_ConfigComponent_PDSetupInfo();
            try
            {
                pdSetupInfo.SaveMode = 0;
                pdSetupInfo.ReadingSinPeriods = 1;
                pdSetupInfo.SaveDays = 0;
                pdSetupInfo.SaveNum = 0;
                pdSetupInfo.PhaseShift = 0;
                pdSetupInfo.PhShift = 0;
                pdSetupInfo.CalcSpeedStackSize = 0;
                pdSetupInfo.PSpeed_0 = 0;
                pdSetupInfo.PSpeed_1 = 0;
                pdSetupInfo.PJump_0 = 0;
                pdSetupInfo.PJump_1 = 0;
                pdSetupInfo.SyncType = 0;
                pdSetupInfo.InternalSyncFrequency = 60.0;
                pdSetupInfo.PulsesAmpl = 3.0;
                // 
                pdSetupInfo.Reserved_0 = 0;
                pdSetupInfo.Reserved_1 = 0;
                pdSetupInfo.Reserved_2 = 0;
                pdSetupInfo.Reserved_3 = 0;
                pdSetupInfo.Reserved_4 = 0;
                pdSetupInfo.Reserved_5 = 0;
                pdSetupInfo.Reserved_6 = 0;
                pdSetupInfo.Reserved_7 = 0;
                pdSetupInfo.Reserved_8 = 0;
                pdSetupInfo.Reserved_9 = 0;
                pdSetupInfo.Reserved_10 = 0;
                pdSetupInfo.Reserved_11 = 0;
                pdSetupInfo.Reserved_12 = 0;
                pdSetupInfo.Reserved_13 = 0;
                pdSetupInfo.Reserved_14 = 0;
                pdSetupInfo.Reserved_15 = 0;
                pdSetupInfo.Reserved_16 = 0;
                pdSetupInfo.Reserved_17 = 0;
                pdSetupInfo.Reserved_18 = 0;
                pdSetupInfo.Reserved_19 = 0;
                pdSetupInfo.Reserved_20 = 0;
                pdSetupInfo.Reserved_21 = 0;
                pdSetupInfo.Reserved_22 = 0;
                pdSetupInfo.Reserved_23 = 0;
                pdSetupInfo.Reserved_24 = 0;
                pdSetupInfo.Reserved_25 = 0;
                pdSetupInfo.Reserved_26 = 0;
                pdSetupInfo.Reserved_27 = 0;
                pdSetupInfo.Reserved_28 = 0;
                pdSetupInfo.Reserved_29 = 0;
                pdSetupInfo.Reserved_30 = 0;
                pdSetupInfo.Reserved_31 = 0;
                pdSetupInfo.Reserved_32 = 0;
                pdSetupInfo.Reserved_33 = 0;
                pdSetupInfo.Reserved_34 = 0;
                pdSetupInfo.Reserved_35 = 0;
                pdSetupInfo.Reserved_36 = 0;
                pdSetupInfo.Reserved_37 = 0;
                pdSetupInfo.Reserved_38 = 0;
                pdSetupInfo.Reserved_39 = 0;
                pdSetupInfo.Reserved_40 = 0;
                pdSetupInfo.Reserved_41 = 0;
                pdSetupInfo.Reserved_42 = 0;
                pdSetupInfo.Reserved_43 = 0;
                pdSetupInfo.Reserved_44 = 0;
                pdSetupInfo.Reserved_45 = 0;
                pdSetupInfo.Reserved_46 = 0;
                pdSetupInfo.Reserved_47 = 0;
                pdSetupInfo.Reserved_48 = 0;
                pdSetupInfo.Reserved_49 = 0;
                pdSetupInfo.Reserved_50 = 0;
                pdSetupInfo.Reserved_51 = 0;
                pdSetupInfo.Reserved_52 = 0;
                pdSetupInfo.Reserved_53 = 0;
                pdSetupInfo.Reserved_54 = 0;
                pdSetupInfo.Reserved_55 = 0;
                pdSetupInfo.Reserved_56 = 0;
                pdSetupInfo.Reserved_57 = 0;
                pdSetupInfo.Reserved_58 = 0;
                pdSetupInfo.Reserved_59 = 0;
                pdSetupInfo.Reserved_60 = 0;
                pdSetupInfo.Reserved_61 = 0;
                pdSetupInfo.Reserved_62 = 0;
                pdSetupInfo.Reserved_63 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.InitializeEmptyPDSetupInfo()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdSetupInfo;
        }


        private PDM_ConfigComponent_PDSetupInfo ExtractPDSetupInfoDataFromByteData(Byte[] byteData)
        {
            PDM_ConfigComponent_PDSetupInfo pdSetupInfo = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        pdSetupInfo = new PDM_ConfigComponent_PDSetupInfo();

                        pdSetupInfo.SaveMode = ConversionMethods.ByteToInt32(byteData[316]);
                        // skip a byte, probaby for alignment, not used at all by device
                        pdSetupInfo.ReadingSinPeriods = ConversionMethods.UnsignedShortBytesToUInt16(byteData[318], byteData[319]);
                        pdSetupInfo.SaveDays = ConversionMethods.ByteToInt32(byteData[320]);
                        pdSetupInfo.SaveNum = ConversionMethods.ByteToInt32(byteData[321]);
                        pdSetupInfo.PhaseShift = ConversionMethods.ByteToInt32(byteData[322]);
                        // skip a byte, probaby for alignment, not used at all by device
                        pdSetupInfo.PhShift = ConversionMethods.UnsignedShortBytesToUInt16(byteData[324], byteData[325]);
                        pdSetupInfo.CalcSpeedStackSize = ConversionMethods.ByteToInt32(byteData[326]);
                        // skip a lot of bytes where the ChannelInfo data is held, that data goes to a different table
                        pdSetupInfo.PSpeed_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[1348], byteData[1349]);
                        pdSetupInfo.PSpeed_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[1350], byteData[1351]);
                        pdSetupInfo.PJump_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[1352], byteData[1353]);
                        pdSetupInfo.PJump_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[1354], byteData[1355]);
                        pdSetupInfo.SyncType = ConversionMethods.ByteToInt32(byteData[1356]);
                        // skip 3 bytes, probably for alignment, not used at all by device
                        pdSetupInfo.InternalSyncFrequency = ConversionMethods.BytesToSingle(byteData[1360], byteData[1361], byteData[1362], byteData[1363]);
                        pdSetupInfo.PulsesAmpl = ConversionMethods.BytesToSingle(byteData[1364], byteData[1365], byteData[1366], byteData[1367]);
                        // 
                        pdSetupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[1368]);
                        pdSetupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[1369]);
                        pdSetupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[1370]);
                        pdSetupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[1371]);
                        pdSetupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[1372]);
                        pdSetupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[1373]);
                        pdSetupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[1374]);
                        pdSetupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[1375]);
                        pdSetupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[1376]);
                        pdSetupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[1377]);
                        pdSetupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[1378]);
                        pdSetupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[1379]);
                        pdSetupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[1380]);
                        pdSetupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[1381]);
                        pdSetupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[1382]);
                        pdSetupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[1383]);
                        pdSetupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[1384]);
                        pdSetupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[1385]);
                        pdSetupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[1386]);
                        pdSetupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[1387]);
                        pdSetupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[1388]);
                        pdSetupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[1389]);
                        pdSetupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[1390]);
                        pdSetupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[1391]);
                        pdSetupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[1392]);
                        pdSetupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[1393]);
                        pdSetupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[1394]);
                        pdSetupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[1395]);
                        pdSetupInfo.Reserved_28 = ConversionMethods.ByteToInt32(byteData[1396]);
                        pdSetupInfo.Reserved_29 = ConversionMethods.ByteToInt32(byteData[1397]);
                        pdSetupInfo.Reserved_30 = ConversionMethods.ByteToInt32(byteData[1398]);
                        pdSetupInfo.Reserved_31 = ConversionMethods.ByteToInt32(byteData[1399]);
                        pdSetupInfo.Reserved_32 = ConversionMethods.ByteToInt32(byteData[1400]);
                        pdSetupInfo.Reserved_33 = ConversionMethods.ByteToInt32(byteData[1401]);
                        pdSetupInfo.Reserved_34 = ConversionMethods.ByteToInt32(byteData[1402]);
                        pdSetupInfo.Reserved_35 = ConversionMethods.ByteToInt32(byteData[1403]);
                        pdSetupInfo.Reserved_36 = ConversionMethods.ByteToInt32(byteData[1404]);
                        pdSetupInfo.Reserved_37 = ConversionMethods.ByteToInt32(byteData[1405]);
                        pdSetupInfo.Reserved_38 = ConversionMethods.ByteToInt32(byteData[1406]);
                        pdSetupInfo.Reserved_39 = ConversionMethods.ByteToInt32(byteData[1407]);
                        pdSetupInfo.Reserved_40 = ConversionMethods.ByteToInt32(byteData[1408]);
                        pdSetupInfo.Reserved_41 = ConversionMethods.ByteToInt32(byteData[1409]);
                        pdSetupInfo.Reserved_42 = ConversionMethods.ByteToInt32(byteData[1410]);
                        pdSetupInfo.Reserved_43 = ConversionMethods.ByteToInt32(byteData[1411]);
                        pdSetupInfo.Reserved_44 = ConversionMethods.ByteToInt32(byteData[1412]);
                        pdSetupInfo.Reserved_45 = ConversionMethods.ByteToInt32(byteData[1413]);
                        pdSetupInfo.Reserved_46 = ConversionMethods.ByteToInt32(byteData[1414]);
                        pdSetupInfo.Reserved_47 = ConversionMethods.ByteToInt32(byteData[1415]);
                        pdSetupInfo.Reserved_48 = ConversionMethods.ByteToInt32(byteData[1416]);
                        pdSetupInfo.Reserved_49 = ConversionMethods.ByteToInt32(byteData[1417]);
                        pdSetupInfo.Reserved_50 = ConversionMethods.ByteToInt32(byteData[1418]);
                        pdSetupInfo.Reserved_51 = ConversionMethods.ByteToInt32(byteData[1419]);
                        pdSetupInfo.Reserved_52 = ConversionMethods.ByteToInt32(byteData[1420]);
                        pdSetupInfo.Reserved_53 = ConversionMethods.ByteToInt32(byteData[1421]);
                        pdSetupInfo.Reserved_54 = ConversionMethods.ByteToInt32(byteData[1422]);
                        pdSetupInfo.Reserved_55 = ConversionMethods.ByteToInt32(byteData[1423]);
                        pdSetupInfo.Reserved_56 = ConversionMethods.ByteToInt32(byteData[1424]);
                        pdSetupInfo.Reserved_57 = ConversionMethods.ByteToInt32(byteData[1425]);
                        pdSetupInfo.Reserved_58 = ConversionMethods.ByteToInt32(byteData[1426]);
                        pdSetupInfo.Reserved_59 = ConversionMethods.ByteToInt32(byteData[1427]);
                        pdSetupInfo.Reserved_60 = ConversionMethods.ByteToInt32(byteData[1428]);
                        pdSetupInfo.Reserved_61 = ConversionMethods.ByteToInt32(byteData[1429]);
                        pdSetupInfo.Reserved_62 = ConversionMethods.ByteToInt32(byteData[1430]);
                        pdSetupInfo.Reserved_63 = ConversionMethods.ByteToInt32(byteData[1431]);
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nbyteInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        pdSetupInfo = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ExtractPDSetupInfoDataFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdSetupInfo;
        }

        private void ConvertPDSetupInfoToBytes(PDM_ConfigComponent_PDSetupInfo pdSetupInfo, ref Byte[] byteData)
        {
            try
            {
                if (pdSetupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[316] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveMode);
                            // skip a byte, probaby for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.ReadingSinPeriods), 0, byteData, 318, 2);
                            byteData[320] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveDays);
                            byteData[321] = ConversionMethods.Int32ToByte(pdSetupInfo.SaveNum);
                            byteData[322] = ConversionMethods.Int32ToByte(pdSetupInfo.PhaseShift);
                            // skip a byte, probaby for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PhShift), 0, byteData, 324, 2);
                            byteData[326] = ConversionMethods.Int32ToByte(pdSetupInfo.CalcSpeedStackSize);
                            // skip a lot of bytes where the ChannelInfo data is held, that data goes to a different table
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PSpeed_0), 0, byteData, 1348, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PSpeed_1), 0, byteData, 1350, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PJump_0), 0, byteData, 1352, 2);
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)pdSetupInfo.PJump_1), 0, byteData, 1354, 2);
                            byteData[1356] = ConversionMethods.Int32ToByte(pdSetupInfo.SyncType);
                            // skip 3 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)pdSetupInfo.InternalSyncFrequency), 0, byteData, 1360, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)pdSetupInfo.PulsesAmpl), 0, byteData, 1364, 4);
                            //
                            byteData[1368] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_0);
                            byteData[1369] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_1);
                            byteData[1370] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_2);
                            byteData[1371] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_3);
                            byteData[1372] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_4);
                            byteData[1373] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_5);
                            byteData[1374] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_6);
                            byteData[1375] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_7);
                            byteData[1376] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_8);
                            byteData[1377] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_9);
                            byteData[1378] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_10);
                            byteData[1379] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_11);
                            byteData[1380] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_12);
                            byteData[1381] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_13);
                            byteData[1382] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_14);
                            byteData[1383] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_15);
                            byteData[1384] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_16);
                            byteData[1385] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_17);
                            byteData[1386] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_18);
                            byteData[1387] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_19);
                            byteData[1388] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_20);
                            byteData[1389] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_21);
                            byteData[1390] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_22);
                            byteData[1391] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_23);
                            byteData[1392] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_24);
                            byteData[1393] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_25);
                            byteData[1394] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_26);
                            byteData[1395] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_27);
                            byteData[1396] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_28);
                            byteData[1397] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_29);
                            byteData[1398] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_30);
                            byteData[1399] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_31);
                            byteData[1400] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_32);
                            byteData[1401] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_33);
                            byteData[1402] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_34);
                            byteData[1403] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_35);
                            byteData[1404] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_36);
                            byteData[1405] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_37);
                            byteData[1406] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_38);
                            byteData[1407] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_39);
                            byteData[1408] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_40);
                            byteData[1409] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_41);
                            byteData[1410] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_42);
                            byteData[1411] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_43);
                            byteData[1412] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_44);
                            byteData[1413] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_45);
                            byteData[1414] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_46);
                            byteData[1415] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_47);
                            byteData[1416] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_48);
                            byteData[1417] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_49);
                            byteData[1418] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_50);
                            byteData[1419] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_51);
                            byteData[1420] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_52);
                            byteData[1421] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_53);
                            byteData[1422] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_54);
                            byteData[1423] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_55);
                            byteData[1424] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_56);
                            byteData[1425] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_57);
                            byteData[1426] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_58);
                            byteData[1427] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_59);
                            byteData[1428] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_60);
                            byteData[1429] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_61);
                            byteData[1430] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_62);
                            byteData[1431] = ConversionMethods.Int32ToByte(pdSetupInfo.Reserved_63);
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_Configuration.ConvertPDSetupInfoToBytes(PDM_ConfigComponent_PDSetupInfo, ref Byte[])\nInput Byte[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ConvertPDSetupInfoToBytes(PDM_ConfigComponent_PDSetupInfo, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ConvertPDSetupInfoToBytes(PDM_ConfigComponent_PDSetupInfo, ref Byte[])\nInput PDM_ConfigComponent_PDSetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConvertPDSetupInfoToBytes(PDM_ConfigComponent_PDSetupInfo, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private PDM_ConfigComponent_SetupInfo InitializeEmptySetupInfo()
        {
            PDM_ConfigComponent_SetupInfo setupInfo = new PDM_ConfigComponent_SetupInfo();
            try
            {
                setupInfo.ScheduleType = 0;
                setupInfo.DTime_Hour = 0;
                setupInfo.DTime_Minute = 0;
                setupInfo.DisplayFlag = 2047;
                setupInfo.RelayMode = 0;
                setupInfo.TimeOfRelayAlarm = 100;
                setupInfo.OutTime = 3;
                setupInfo.DeviceNumber = 20;
                setupInfo.BaudRate = 3;
                setupInfo.ModBusProtocol = 0;
                setupInfo.Stopped = 0;
                setupInfo.AlarmEnable = 0;
                setupInfo.Termostat = 0;
                setupInfo.ReReadOnAlarm = 1;
                //
                setupInfo.RatedCurrent_0 = 1.0;
                setupInfo.RatedCurrent_1 = 1.0;
                setupInfo.RatedCurrent_2 = 1.0;
                setupInfo.RatedCurrent_3 = 1.0;
                setupInfo.RatedCurrent_4 = 1.0;
                setupInfo.RatedCurrent_5 = 1.0;
                setupInfo.RatedCurrent_6 = 1.0;
                setupInfo.RatedCurrent_7 = 1.0;
                setupInfo.RatedCurrent_8 = 1.0;
                setupInfo.RatedCurrent_9 = 1.0;
                setupInfo.RatedCurrent_10 = 1.0;
                setupInfo.RatedCurrent_11 = 1.0;
                setupInfo.RatedCurrent_12 = 1.0;
                setupInfo.RatedCurrent_13 = 1.0;
                setupInfo.RatedCurrent_14 = 1.0;
                //
                setupInfo.RatedVoltage_0 = 220.0;
                setupInfo.RatedVoltage_1 = 220.0;
                setupInfo.RatedVoltage_2 = 220.0;
                setupInfo.RatedVoltage_3 = 220.0;
                setupInfo.RatedVoltage_4 = 220.0;
                setupInfo.RatedVoltage_5 = 220.0;
                setupInfo.RatedVoltage_6 = 220.0;
                setupInfo.RatedVoltage_7 = 220.0;
                setupInfo.RatedVoltage_8 = 220.0;
                setupInfo.RatedVoltage_9 = 220.0;
                setupInfo.RatedVoltage_10 = 220.0;
                setupInfo.RatedVoltage_11 = 220.0;
                setupInfo.RatedVoltage_12 = 220.0;
                setupInfo.RatedVoltage_13 = 220.0;
                setupInfo.RatedVoltage_14 = 220.0;
                //
                setupInfo.ReadAnalogFromRegisters = 0;
                // this setting makes the neutral type (which the user cannot set in athena) common
                setupInfo.ObjectType = 1;

                setupInfo.Power = 10.0;
                setupInfo.ExtTemperature = 0;
                setupInfo.ExtHumidity = 0;
                setupInfo.ExtLoadActive = 0;
                setupInfo.ExtLoadReactive = 127;
                //
                setupInfo.Reserved_0 = 0;
                setupInfo.Reserved_1 = 0;
                setupInfo.Reserved_2 = 0;
                setupInfo.Reserved_3 = 0;
                setupInfo.Reserved_4 = 0;
                setupInfo.Reserved_5 = 0;
                setupInfo.Reserved_6 = 0;
                setupInfo.Reserved_7 = 0;
                setupInfo.Reserved_8 = 0;
                setupInfo.Reserved_9 = 0;
                setupInfo.Reserved_10 = 0;
                setupInfo.Reserved_11 = 0;
                setupInfo.Reserved_12 = 0;
                setupInfo.Reserved_13 = 0;
                setupInfo.Reserved_14 = 0;
                setupInfo.Reserved_15 = 0;
                setupInfo.Reserved_16 = 0;
                setupInfo.Reserved_17 = 0;
                setupInfo.Reserved_18 = 0;
                setupInfo.Reserved_19 = 0;
                setupInfo.Reserved_20 = 0;
                setupInfo.Reserved_21 = 0;
                setupInfo.Reserved_22 = 0;
                setupInfo.Reserved_23 = 0;
                setupInfo.Reserved_24 = 0;
                setupInfo.Reserved_25 = 0;
                setupInfo.Reserved_26 = 0;
                setupInfo.Reserved_27 = 0;
                setupInfo.Reserved_28 = 0;
                setupInfo.Reserved_29 = 0;
                setupInfo.Reserved_30 = 0;
                setupInfo.Reserved_31 = 0;
                setupInfo.Reserved_32 = 0;
                setupInfo.Reserved_33 = 0;
                setupInfo.Reserved_34 = 0;
                setupInfo.Reserved_35 = 0;
                setupInfo.Reserved_36 = 0;
                setupInfo.Reserved_37 = 0;
                setupInfo.Reserved_38 = 0;
                setupInfo.Reserved_39 = 0;
                setupInfo.Reserved_40 = 0;
                setupInfo.Reserved_41 = 0;
                setupInfo.Reserved_42 = 0;
                setupInfo.Reserved_43 = 0;
                setupInfo.Reserved_44 = 0;
                setupInfo.Reserved_45 = 0;
                setupInfo.Reserved_46 = 0;
                setupInfo.Reserved_47 = 0;
                setupInfo.Reserved_48 = 0;
                setupInfo.Reserved_49 = 0;
                setupInfo.Reserved_50 = 0;
                setupInfo.Reserved_51 = 0;
                setupInfo.Reserved_52 = 0;
                setupInfo.Reserved_53 = 0;
                setupInfo.Reserved_54 = 0;
                setupInfo.Reserved_55 = 0;
                setupInfo.Reserved_56 = 0;
                setupInfo.Reserved_57 = 0;
                setupInfo.Reserved_58 = 0;
                setupInfo.Reserved_59 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        private PDM_ConfigComponent_SetupInfo ExtractSetupInfoDataFromByteData(Byte[] byteData)
        {
            PDM_ConfigComponent_SetupInfo setupInfo = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        setupInfo = new PDM_ConfigComponent_SetupInfo();

                        setupInfo.ScheduleType = ConversionMethods.ByteToInt32(byteData[0]);
                        setupInfo.DTime_Hour = ConversionMethods.ByteToInt32(byteData[1]);
                        setupInfo.DTime_Minute = ConversionMethods.ByteToInt32(byteData[2]);
                        // many bytes skipped that hold MeasurementsInfo
                        setupInfo.DisplayFlag = ConversionMethods.UnsignedShortBytesToUInt16(byteData[104], byteData[105]);
                        setupInfo.RelayMode = ConversionMethods.ByteToInt32(byteData[106]);
                        // skipped a byte, probably for alignment, not used at all by device
                        setupInfo.TimeOfRelayAlarm = ConversionMethods.UnsignedShortBytesToUInt16(byteData[108], byteData[109]);
                        setupInfo.OutTime = ConversionMethods.ByteToInt32(byteData[110]);
                        setupInfo.DeviceNumber = ConversionMethods.ByteToInt32(byteData[111]);
                        setupInfo.BaudRate = ConversionMethods.ByteToInt32(byteData[112]);
                        setupInfo.ModBusProtocol = ConversionMethods.ByteToInt32(byteData[113]);
                        setupInfo.Stopped = ConversionMethods.ByteToInt32(byteData[114]);
                        // skipped a byte, probably for alignment, not used at all by device
                        setupInfo.AlarmEnable = ConversionMethods.UnsignedShortBytesToUInt16(byteData[116], byteData[117]);
                        setupInfo.Termostat = ConversionMethods.ShortBytesToInt16(byteData[118], byteData[119]);
                        setupInfo.ReReadOnAlarm = ConversionMethods.ByteToInt32(byteData[120]);
                        // skipped 3 bytes, probably for alignment, not used at all by device
                        // truckload o' floats
                        setupInfo.RatedCurrent_0 = ConversionMethods.BytesToSingle(byteData[124], byteData[125], byteData[126], byteData[127]);
                        setupInfo.RatedCurrent_1 = ConversionMethods.BytesToSingle(byteData[128], byteData[129], byteData[130], byteData[131]);
                        setupInfo.RatedCurrent_2 = ConversionMethods.BytesToSingle(byteData[132], byteData[133], byteData[134], byteData[135]);
                        setupInfo.RatedCurrent_3 = ConversionMethods.BytesToSingle(byteData[136], byteData[137], byteData[138], byteData[139]);
                        setupInfo.RatedCurrent_4 = ConversionMethods.BytesToSingle(byteData[140], byteData[141], byteData[142], byteData[143]);
                        setupInfo.RatedCurrent_5 = ConversionMethods.BytesToSingle(byteData[144], byteData[145], byteData[146], byteData[147]);
                        setupInfo.RatedCurrent_6 = ConversionMethods.BytesToSingle(byteData[148], byteData[149], byteData[150], byteData[151]);
                        setupInfo.RatedCurrent_7 = ConversionMethods.BytesToSingle(byteData[152], byteData[153], byteData[154], byteData[155]);
                        setupInfo.RatedCurrent_8 = ConversionMethods.BytesToSingle(byteData[156], byteData[157], byteData[158], byteData[159]);
                        setupInfo.RatedCurrent_9 = ConversionMethods.BytesToSingle(byteData[160], byteData[161], byteData[162], byteData[163]);
                        setupInfo.RatedCurrent_10 = ConversionMethods.BytesToSingle(byteData[164], byteData[165], byteData[166], byteData[167]);
                        setupInfo.RatedCurrent_11 = ConversionMethods.BytesToSingle(byteData[168], byteData[169], byteData[170], byteData[171]);
                        setupInfo.RatedCurrent_12 = ConversionMethods.BytesToSingle(byteData[172], byteData[173], byteData[174], byteData[175]);
                        setupInfo.RatedCurrent_13 = ConversionMethods.BytesToSingle(byteData[176], byteData[177], byteData[178], byteData[179]);
                        setupInfo.RatedCurrent_14 = ConversionMethods.BytesToSingle(byteData[180], byteData[181], byteData[182], byteData[183]);
                        //
                        setupInfo.RatedVoltage_0 = ConversionMethods.BytesToSingle(byteData[184], byteData[185], byteData[186], byteData[187]);
                        setupInfo.RatedVoltage_1 = ConversionMethods.BytesToSingle(byteData[188], byteData[189], byteData[190], byteData[191]);
                        setupInfo.RatedVoltage_2 = ConversionMethods.BytesToSingle(byteData[192], byteData[193], byteData[194], byteData[195]);
                        setupInfo.RatedVoltage_3 = ConversionMethods.BytesToSingle(byteData[196], byteData[197], byteData[198], byteData[199]);
                        setupInfo.RatedVoltage_4 = ConversionMethods.BytesToSingle(byteData[200], byteData[201], byteData[202], byteData[203]);
                        setupInfo.RatedVoltage_5 = ConversionMethods.BytesToSingle(byteData[204], byteData[205], byteData[206], byteData[207]);
                        setupInfo.RatedVoltage_6 = ConversionMethods.BytesToSingle(byteData[208], byteData[209], byteData[210], byteData[211]);
                        setupInfo.RatedVoltage_7 = ConversionMethods.BytesToSingle(byteData[212], byteData[213], byteData[214], byteData[215]);
                        setupInfo.RatedVoltage_8 = ConversionMethods.BytesToSingle(byteData[216], byteData[217], byteData[218], byteData[219]);
                        setupInfo.RatedVoltage_9 = ConversionMethods.BytesToSingle(byteData[220], byteData[221], byteData[222], byteData[223]);
                        setupInfo.RatedVoltage_10 = ConversionMethods.BytesToSingle(byteData[224], byteData[225], byteData[226], byteData[227]);
                        setupInfo.RatedVoltage_11 = ConversionMethods.BytesToSingle(byteData[228], byteData[229], byteData[230], byteData[231]);
                        setupInfo.RatedVoltage_12 = ConversionMethods.BytesToSingle(byteData[232], byteData[233], byteData[234], byteData[235]);
                        setupInfo.RatedVoltage_13 = ConversionMethods.BytesToSingle(byteData[236], byteData[237], byteData[238], byteData[239]);
                        setupInfo.RatedVoltage_14 = ConversionMethods.BytesToSingle(byteData[240], byteData[241], byteData[242], byteData[243]);
                        //
                        setupInfo.ReadAnalogFromRegisters = ConversionMethods.ByteToInt32(byteData[244]);
                        setupInfo.ObjectType = ConversionMethods.ByteToInt32(byteData[245]);
                        // skipped 2 bytes, probably for alignment, not used at all by device
                        setupInfo.Power = ConversionMethods.BytesToSingle(byteData[248], byteData[249], byteData[250], byteData[251]);
                        setupInfo.ExtTemperature = ConversionMethods.ByteToInt32(byteData[252]);
                        setupInfo.ExtHumidity = ConversionMethods.ByteToInt32(byteData[253]);
                        setupInfo.ExtLoadActive = ConversionMethods.ByteToInt32(byteData[254]);
                        setupInfo.ExtLoadReactive = ConversionMethods.ByteToInt32(byteData[255]);
                        //
                        setupInfo.Reserved_0 = ConversionMethods.ByteToInt32(byteData[256]);
                        setupInfo.Reserved_1 = ConversionMethods.ByteToInt32(byteData[257]);
                        setupInfo.Reserved_2 = ConversionMethods.ByteToInt32(byteData[258]);
                        setupInfo.Reserved_3 = ConversionMethods.ByteToInt32(byteData[259]);
                        setupInfo.Reserved_4 = ConversionMethods.ByteToInt32(byteData[260]);
                        setupInfo.Reserved_5 = ConversionMethods.ByteToInt32(byteData[261]);
                        setupInfo.Reserved_6 = ConversionMethods.ByteToInt32(byteData[262]);
                        setupInfo.Reserved_7 = ConversionMethods.ByteToInt32(byteData[263]);
                        setupInfo.Reserved_8 = ConversionMethods.ByteToInt32(byteData[264]);
                        setupInfo.Reserved_9 = ConversionMethods.ByteToInt32(byteData[265]);
                        setupInfo.Reserved_10 = ConversionMethods.ByteToInt32(byteData[266]);
                        setupInfo.Reserved_11 = ConversionMethods.ByteToInt32(byteData[267]);
                        setupInfo.Reserved_12 = ConversionMethods.ByteToInt32(byteData[268]);
                        setupInfo.Reserved_13 = ConversionMethods.ByteToInt32(byteData[269]);
                        setupInfo.Reserved_14 = ConversionMethods.ByteToInt32(byteData[270]);
                        setupInfo.Reserved_15 = ConversionMethods.ByteToInt32(byteData[271]);
                        setupInfo.Reserved_16 = ConversionMethods.ByteToInt32(byteData[272]);
                        setupInfo.Reserved_17 = ConversionMethods.ByteToInt32(byteData[273]);
                        setupInfo.Reserved_18 = ConversionMethods.ByteToInt32(byteData[274]);
                        setupInfo.Reserved_19 = ConversionMethods.ByteToInt32(byteData[275]);
                        setupInfo.Reserved_20 = ConversionMethods.ByteToInt32(byteData[276]);
                        setupInfo.Reserved_21 = ConversionMethods.ByteToInt32(byteData[277]);
                        setupInfo.Reserved_22 = ConversionMethods.ByteToInt32(byteData[278]);
                        setupInfo.Reserved_23 = ConversionMethods.ByteToInt32(byteData[279]);
                        setupInfo.Reserved_24 = ConversionMethods.ByteToInt32(byteData[280]);
                        setupInfo.Reserved_25 = ConversionMethods.ByteToInt32(byteData[281]);
                        setupInfo.Reserved_26 = ConversionMethods.ByteToInt32(byteData[282]);
                        setupInfo.Reserved_27 = ConversionMethods.ByteToInt32(byteData[283]);
                        setupInfo.Reserved_28 = ConversionMethods.ByteToInt32(byteData[284]);
                        setupInfo.Reserved_29 = ConversionMethods.ByteToInt32(byteData[285]);
                        setupInfo.Reserved_30 = ConversionMethods.ByteToInt32(byteData[286]);
                        setupInfo.Reserved_31 = ConversionMethods.ByteToInt32(byteData[287]);
                        setupInfo.Reserved_32 = ConversionMethods.ByteToInt32(byteData[288]);
                        setupInfo.Reserved_33 = ConversionMethods.ByteToInt32(byteData[289]);
                        setupInfo.Reserved_34 = ConversionMethods.ByteToInt32(byteData[290]);
                        setupInfo.Reserved_35 = ConversionMethods.ByteToInt32(byteData[291]);
                        setupInfo.Reserved_36 = ConversionMethods.ByteToInt32(byteData[292]);
                        setupInfo.Reserved_37 = ConversionMethods.ByteToInt32(byteData[293]);
                        setupInfo.Reserved_38 = ConversionMethods.ByteToInt32(byteData[294]);
                        setupInfo.Reserved_39 = ConversionMethods.ByteToInt32(byteData[295]);
                        setupInfo.Reserved_40 = ConversionMethods.ByteToInt32(byteData[296]);
                        setupInfo.Reserved_41 = ConversionMethods.ByteToInt32(byteData[297]);
                        setupInfo.Reserved_42 = ConversionMethods.ByteToInt32(byteData[298]);
                        setupInfo.Reserved_43 = ConversionMethods.ByteToInt32(byteData[299]);
                        setupInfo.Reserved_44 = ConversionMethods.ByteToInt32(byteData[300]);
                        setupInfo.Reserved_45 = ConversionMethods.ByteToInt32(byteData[301]);
                        setupInfo.Reserved_46 = ConversionMethods.ByteToInt32(byteData[302]);
                        setupInfo.Reserved_47 = ConversionMethods.ByteToInt32(byteData[303]);
                        setupInfo.Reserved_48 = ConversionMethods.ByteToInt32(byteData[304]);
                        setupInfo.Reserved_49 = ConversionMethods.ByteToInt32(byteData[305]);
                        setupInfo.Reserved_50 = ConversionMethods.ByteToInt32(byteData[306]);
                        setupInfo.Reserved_51 = ConversionMethods.ByteToInt32(byteData[307]);
                        setupInfo.Reserved_52 = ConversionMethods.ByteToInt32(byteData[308]);
                        setupInfo.Reserved_53 = ConversionMethods.ByteToInt32(byteData[309]);
                        setupInfo.Reserved_54 = ConversionMethods.ByteToInt32(byteData[310]);
                        setupInfo.Reserved_55 = ConversionMethods.ByteToInt32(byteData[311]);
                        setupInfo.Reserved_56 = ConversionMethods.ByteToInt32(byteData[312]);
                        setupInfo.Reserved_57 = ConversionMethods.ByteToInt32(byteData[313]);
                        setupInfo.Reserved_58 = ConversionMethods.ByteToInt32(byteData[314]);
                        setupInfo.Reserved_59 = ConversionMethods.ByteToInt32(byteData[315]);
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        setupInfo = null;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ExtractSetupInfoDateFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setupInfo;
        }

        private void ConvertSetupInfoToBytes(PDM_ConfigComponent_SetupInfo setupInfo, ref Byte[] byteData)
        {
            try
            {
                if (setupInfo != null)
                {
                    if (byteData != null)
                    {
                        if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                        {
                            byteData[0] = ConversionMethods.Int32ToByte(setupInfo.ScheduleType);
                            byteData[1] = ConversionMethods.Int32ToByte(setupInfo.DTime_Hour);
                            byteData[2] = ConversionMethods.Int32ToByte(setupInfo.DTime_Minute);
                            //
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.DisplayFlag), 0, byteData, 104, 2);
                            byteData[106] = ConversionMethods.Int32ToByte(setupInfo.RelayMode);
                            // skip a byte, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.TimeOfRelayAlarm), 0, byteData, 108, 2);
                            byteData[110] = ConversionMethods.Int32ToByte(setupInfo.OutTime);
                            byteData[111] = ConversionMethods.Int32ToByte(setupInfo.DeviceNumber);
                            byteData[112] = ConversionMethods.Int32ToByte(setupInfo.BaudRate);
                            byteData[113] = ConversionMethods.Int32ToByte(setupInfo.ModBusProtocol);
                            byteData[114] = ConversionMethods.Int32ToByte(setupInfo.Stopped);
                            // skip a byte, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.UInt16ToUnsignedShortBytes((UInt16)setupInfo.AlarmEnable), 0, byteData, 116, 2);
                            Array.Copy(ConversionMethods.Int16ToShortBytes((Int16)setupInfo.Termostat), 0, byteData, 118, 2);
                            byteData[120] = ConversionMethods.Int32ToByte(setupInfo.ReReadOnAlarm);
                            // skip 3 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_0), 0, byteData, 124, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_1), 0, byteData, 128, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_2), 0, byteData, 132, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_3), 0, byteData, 136, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_4), 0, byteData, 140, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_5), 0, byteData, 144, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_6), 0, byteData, 148, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_7), 0, byteData, 152, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_8), 0, byteData, 156, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_9), 0, byteData, 160, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_10), 0, byteData, 164, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_11), 0, byteData, 168, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_12), 0, byteData, 172, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_13), 0, byteData, 176, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedCurrent_14), 0, byteData, 180, 4);
                            //
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_0), 0, byteData, 184, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_1), 0, byteData, 188, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_2), 0, byteData, 192, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_3), 0, byteData, 196, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_4), 0, byteData, 200, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_5), 0, byteData, 204, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_6), 0, byteData, 208, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_7), 0, byteData, 212, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_8), 0, byteData, 216, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_9), 0, byteData, 220, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_10), 0, byteData, 224, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_11), 0, byteData, 228, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_12), 0, byteData, 232, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_13), 0, byteData, 236, 4);
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.RatedVoltage_14), 0, byteData, 240, 4);
                            //
                            byteData[244] = ConversionMethods.Int32ToByte(setupInfo.ReadAnalogFromRegisters);
                            byteData[245] = ConversionMethods.Int32ToByte(setupInfo.ObjectType);
                            // skip 2 bytes, probably for alignment, not used at all by device
                            Array.Copy(ConversionMethods.SingleToBytes((Single)setupInfo.Power), 0, byteData, 248, 4);
                            byteData[252] = ConversionMethods.Int32ToByte(setupInfo.ExtTemperature);
                            byteData[253] = ConversionMethods.Int32ToByte(setupInfo.ExtHumidity);
                            byteData[254] = ConversionMethods.Int32ToByte(setupInfo.ExtLoadActive);
                            byteData[255] = ConversionMethods.Int32ToByte(setupInfo.ExtLoadReactive);
                            //
                            byteData[256] = ConversionMethods.Int32ToByte(setupInfo.Reserved_0);
                            byteData[257] = ConversionMethods.Int32ToByte(setupInfo.Reserved_1);
                            byteData[258] = ConversionMethods.Int32ToByte(setupInfo.Reserved_2);
                            byteData[259] = ConversionMethods.Int32ToByte(setupInfo.Reserved_3);
                            byteData[260] = ConversionMethods.Int32ToByte(setupInfo.Reserved_4);
                            byteData[261] = ConversionMethods.Int32ToByte(setupInfo.Reserved_5);
                            byteData[262] = ConversionMethods.Int32ToByte(setupInfo.Reserved_6);
                            byteData[263] = ConversionMethods.Int32ToByte(setupInfo.Reserved_7);
                            byteData[264] = ConversionMethods.Int32ToByte(setupInfo.Reserved_8);
                            byteData[265] = ConversionMethods.Int32ToByte(setupInfo.Reserved_9);
                            byteData[266] = ConversionMethods.Int32ToByte(setupInfo.Reserved_10);
                            byteData[267] = ConversionMethods.Int32ToByte(setupInfo.Reserved_11);
                            byteData[268] = ConversionMethods.Int32ToByte(setupInfo.Reserved_12);
                            byteData[269] = ConversionMethods.Int32ToByte(setupInfo.Reserved_13);
                            byteData[270] = ConversionMethods.Int32ToByte(setupInfo.Reserved_14);
                            byteData[271] = ConversionMethods.Int32ToByte(setupInfo.Reserved_15);
                            byteData[272] = ConversionMethods.Int32ToByte(setupInfo.Reserved_16);
                            byteData[273] = ConversionMethods.Int32ToByte(setupInfo.Reserved_17);
                            byteData[274] = ConversionMethods.Int32ToByte(setupInfo.Reserved_18);
                            byteData[275] = ConversionMethods.Int32ToByte(setupInfo.Reserved_19);
                            byteData[276] = ConversionMethods.Int32ToByte(setupInfo.Reserved_20);
                            byteData[277] = ConversionMethods.Int32ToByte(setupInfo.Reserved_21);
                            byteData[278] = ConversionMethods.Int32ToByte(setupInfo.Reserved_22);
                            byteData[279] = ConversionMethods.Int32ToByte(setupInfo.Reserved_23);
                            byteData[280] = ConversionMethods.Int32ToByte(setupInfo.Reserved_24);
                            byteData[281] = ConversionMethods.Int32ToByte(setupInfo.Reserved_25);
                            byteData[282] = ConversionMethods.Int32ToByte(setupInfo.Reserved_26);
                            byteData[283] = ConversionMethods.Int32ToByte(setupInfo.Reserved_27);
                            byteData[284] = ConversionMethods.Int32ToByte(setupInfo.Reserved_28);
                            byteData[285] = ConversionMethods.Int32ToByte(setupInfo.Reserved_29);
                            byteData[286] = ConversionMethods.Int32ToByte(setupInfo.Reserved_30);
                            byteData[287] = ConversionMethods.Int32ToByte(setupInfo.Reserved_31);
                            byteData[288] = ConversionMethods.Int32ToByte(setupInfo.Reserved_32);
                            byteData[289] = ConversionMethods.Int32ToByte(setupInfo.Reserved_33);
                            byteData[290] = ConversionMethods.Int32ToByte(setupInfo.Reserved_34);
                            byteData[291] = ConversionMethods.Int32ToByte(setupInfo.Reserved_35);
                            byteData[292] = ConversionMethods.Int32ToByte(setupInfo.Reserved_36);
                            byteData[293] = ConversionMethods.Int32ToByte(setupInfo.Reserved_37);
                            byteData[294] = ConversionMethods.Int32ToByte(setupInfo.Reserved_38);
                            byteData[295] = ConversionMethods.Int32ToByte(setupInfo.Reserved_39);
                            byteData[296] = ConversionMethods.Int32ToByte(setupInfo.Reserved_40);
                            byteData[297] = ConversionMethods.Int32ToByte(setupInfo.Reserved_41);
                            byteData[298] = ConversionMethods.Int32ToByte(setupInfo.Reserved_42);
                            byteData[299] = ConversionMethods.Int32ToByte(setupInfo.Reserved_43);
                            byteData[300] = ConversionMethods.Int32ToByte(setupInfo.Reserved_44);
                            byteData[301] = ConversionMethods.Int32ToByte(setupInfo.Reserved_45);
                            byteData[302] = ConversionMethods.Int32ToByte(setupInfo.Reserved_46);
                            byteData[303] = ConversionMethods.Int32ToByte(setupInfo.Reserved_47);
                            byteData[304] = ConversionMethods.Int32ToByte(setupInfo.Reserved_48);
                            byteData[305] = ConversionMethods.Int32ToByte(setupInfo.Reserved_49);
                            byteData[306] = ConversionMethods.Int32ToByte(setupInfo.Reserved_50);
                            byteData[307] = ConversionMethods.Int32ToByte(setupInfo.Reserved_51);
                            byteData[308] = ConversionMethods.Int32ToByte(setupInfo.Reserved_52);
                            byteData[309] = ConversionMethods.Int32ToByte(setupInfo.Reserved_53);
                            byteData[310] = ConversionMethods.Int32ToByte(setupInfo.Reserved_54);
                            byteData[311] = ConversionMethods.Int32ToByte(setupInfo.Reserved_55);
                            byteData[312] = ConversionMethods.Int32ToByte(setupInfo.Reserved_56);
                            byteData[313] = ConversionMethods.Int32ToByte(setupInfo.Reserved_57);
                            byteData[314] = ConversionMethods.Int32ToByte(setupInfo.Reserved_58);
                            byteData[315] = ConversionMethods.Int32ToByte(setupInfo.Reserved_59);
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(PDM_Config_SetupInfo, ref Byte[])\nInput Byte[] has too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(PDM_Config_SetupInfo, ref Byte[])\nInput Byte[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ConvertSetupInfoToBytes(PDM_Config_SetupInfo, ref Byte[])\nInput PDM_Config_SetupInfo was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConvertSetupInfoToBytes(PDM_Config_SetupInfo, ref Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
        #endregion


        #region Configuration Comparison

        public bool ConfigurationIsTheSame(PDM_Configuration externalConfiguration)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && !ChannelInfoListIsTheSame(externalConfiguration.channelInfoList))
                {
                    isTheSame = false;
                }
                if (isTheSame && !MeasurementsInfoListIsTheSame(externalConfiguration.measurementsInfoList))
                {
                    isTheSame = false;
                }
                if (isTheSame && !PDSetupInfoIsTheSame(externalConfiguration.pdSetupInfo, true))
                {
                    isTheSame = false;
                }
                if (isTheSame && !SetupInfoIsTheSame(externalConfiguration.setupInfo, true))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConfigurationIsTheSame(BHM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        public bool ConfigurationIsTheSameExceptForMeasurementsSchedule(PDM_Configuration externalConfiguration)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && !ChannelInfoListIsTheSame(externalConfiguration.channelInfoList))
                {
                    isTheSame = false;
                }               
                if (isTheSame && !PDSetupInfoIsTheSame(externalConfiguration.pdSetupInfo, false))
                {
                    isTheSame = false;
                }
                if (isTheSame && !SetupInfoIsTheSame(externalConfiguration.setupInfo, false))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ConfigurationIsTheSameExceptForMeasurementsSchedule(PDM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }


        private bool ChannelInfoIsTheSame(PDM_ConfigComponent_ChannelInfo internalChannelInfo, PDM_ConfigComponent_ChannelInfo externalChannelInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (internalChannelInfo.ChannelNumber != externalChannelInfo.ChannelNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.ChannelIsOn != externalChannelInfo.ChannelIsOn))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalChannelInfo.ChannelSensitivity - externalChannelInfo.ChannelSensitivity) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.PDMaxWidth != externalChannelInfo.PDMaxWidth))
                {
                    isTheSame = false;
                } 
                if (isTheSame && (internalChannelInfo.PDIntervalTime != externalChannelInfo.PDIntervalTime))
                {
                    isTheSame = false;
                } 
                if (isTheSame && (internalChannelInfo.P_0 != externalChannelInfo.P_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.P_1 != externalChannelInfo.P_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.P_2 != externalChannelInfo.P_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.P_3 != externalChannelInfo.P_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.P_4 != externalChannelInfo.P_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.P_5 != externalChannelInfo.P_5))
                {
                    isTheSame = false;
                }

                if (isTheSame && (internalChannelInfo.CHPhase != externalChannelInfo.CHPhase))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.CalcPDILimit != externalChannelInfo.CalcPDILimit))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Filter != externalChannelInfo.Filter))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseOn_0 != externalChannelInfo.NoiseOn_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseOn_1 != externalChannelInfo.NoiseOn_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseOn_2 != externalChannelInfo.NoiseOn_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseType_0 != externalChannelInfo.NoiseType_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseType_1 != externalChannelInfo.NoiseType_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseType_2 != externalChannelInfo.NoiseType_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseShift_0 != externalChannelInfo.NoiseShift_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseShift_1 != externalChannelInfo.NoiseShift_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.NoiseShift_2 != externalChannelInfo.NoiseShift_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.MinNoiseLevel_0 != externalChannelInfo.MinNoiseLevel_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.MinNoiseLevel_1 != externalChannelInfo.MinNoiseLevel_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.MinNoiseLevel_2 != externalChannelInfo.MinNoiseLevel_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.TimeOfArrival_0 != externalChannelInfo.TimeOfArrival_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Polarity_0 != externalChannelInfo.Polarity_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Ref_0 != externalChannelInfo.Ref_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.RefShift_0 != externalChannelInfo.RefShift_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_0 != externalChannelInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_1 != externalChannelInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_2 != externalChannelInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_3 != externalChannelInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_4 != externalChannelInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_5 != externalChannelInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_6 != externalChannelInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalChannelInfo.Reserved_7 != externalChannelInfo.Reserved_7))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ChannelInfoIsTheSame(PDM_ConfigComponent_ChannelInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool ChannelInfoListIsTheSame(List<PDM_ConfigComponent_ChannelInfo> externalChannelInfoList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.channelInfoList != null)
                {
                    if (externalChannelInfoList != null)
                    {
                        count = this.channelInfoList.Count;
                        if (count == externalChannelInfoList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = ChannelInfoIsTheSame(this.channelInfoList[i], externalChannelInfoList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.ChannelInfoListIsTheSame(List<PDM_ConfigComponent_ChannelInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.ChannelInfoListIsTheSame(List<PDM_ConfigComponent_ChannelInfo>)\nthis.channelInfoList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ChannelInfoListIsTheSame(PDM_ConfigComponent_ChannelInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool MeasurementsInfoIsTheSame(PDM_ConfigComponent_MeasurementsInfo internalMeasurementsInfo, PDM_ConfigComponent_MeasurementsInfo externalMeausurementsInfo)
        {
            bool isTheSame = true;
            try
            {
                if (isTheSame && (internalMeasurementsInfo.ItemNumber != externalMeausurementsInfo.ItemNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalMeasurementsInfo.Hour != externalMeausurementsInfo.Hour))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalMeasurementsInfo.Minute != externalMeausurementsInfo.Minute))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.MeasurementsInfoIsTheSame(PDM_ConfigComponent_MeasurementsInfo, PDM_ConfigComponent_MeasurementsInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool MeasurementsInfoListIsTheSame(List<PDM_ConfigComponent_MeasurementsInfo> externalMeasurementsInfoList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.measurementsInfoList != null)
                {
                    if (externalMeasurementsInfoList != null)
                    {
                        count = this.measurementsInfoList.Count;
                        if (count == externalMeasurementsInfoList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = MeasurementsInfoIsTheSame(this.measurementsInfoList[i], externalMeasurementsInfoList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_Configuration.MeasurementsInfoListIsTheSame(List<PDM_ConfigComponent_MeasurementsInfo>)\nInput List<BHM_ConfigComponent_MeasurementsInfo> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_Configuration.MeasurementsInfoListIsTheSame(List<PDM_ConfigComponent_MeasurementsInfo>)\nthis.measurementsInfoList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_Configuration.MeasurementsInfoIsTheSame(List<PDM_ConfigComponent_MeasurementsInfo>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool PDSetupInfoIsTheSame(PDM_ConfigComponent_PDSetupInfo externalPDSetupInfo, bool measurementsInfoMatters)
        {
            bool isTheSame = true;
            try
            {
                if (measurementsInfoMatters)
                {
                    if (isTheSame && (pdSetupInfo.SaveMode != externalPDSetupInfo.SaveMode))
                    {
                        isTheSame = false;
                    }
                   if (isTheSame && (pdSetupInfo.SaveDays != externalPDSetupInfo.SaveDays))
                    {
                        isTheSame = false;
                    }
                    if (isTheSame && (pdSetupInfo.SaveNum != externalPDSetupInfo.SaveNum))
                    {
                        isTheSame = false;
                    }
                }
                if (isTheSame && (pdSetupInfo.ReadingSinPeriods != externalPDSetupInfo.ReadingSinPeriods))
                {
                    isTheSame = false;
                }                    
                if (isTheSame && (pdSetupInfo.PhaseShift != externalPDSetupInfo.PhaseShift))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.PhShift != externalPDSetupInfo.PhShift))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.CalcSpeedStackSize != externalPDSetupInfo.CalcSpeedStackSize))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.PSpeed_0 != externalPDSetupInfo.PSpeed_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.PSpeed_1 != externalPDSetupInfo.PSpeed_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.PJump_0 != externalPDSetupInfo.PJump_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.PJump_1 != externalPDSetupInfo.PJump_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.SyncType != externalPDSetupInfo.SyncType))
                {
                    isTheSame = false;
                }

                if (isTheSame && (Math.Abs(pdSetupInfo.InternalSyncFrequency - externalPDSetupInfo.InternalSyncFrequency) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(pdSetupInfo.PulsesAmpl - externalPDSetupInfo.PulsesAmpl) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (pdSetupInfo.Reserved_0 != externalPDSetupInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_1 != externalPDSetupInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_2 != externalPDSetupInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_3 != externalPDSetupInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_4 != externalPDSetupInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_5 != externalPDSetupInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_6 != externalPDSetupInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_7 != externalPDSetupInfo.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_8 != externalPDSetupInfo.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_9 != externalPDSetupInfo.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_10 != externalPDSetupInfo.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_11 != externalPDSetupInfo.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_12 != externalPDSetupInfo.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_13 != externalPDSetupInfo.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_14 != externalPDSetupInfo.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_15 != externalPDSetupInfo.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_16 != externalPDSetupInfo.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_17 != externalPDSetupInfo.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_18 != externalPDSetupInfo.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_19 != externalPDSetupInfo.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_20 != externalPDSetupInfo.Reserved_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_21 != externalPDSetupInfo.Reserved_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_22 != externalPDSetupInfo.Reserved_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_23 != externalPDSetupInfo.Reserved_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_24 != externalPDSetupInfo.Reserved_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_25 != externalPDSetupInfo.Reserved_25))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_26 != externalPDSetupInfo.Reserved_26))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_27 != externalPDSetupInfo.Reserved_27))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_28 != externalPDSetupInfo.Reserved_28))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_29 != externalPDSetupInfo.Reserved_29))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_30 != externalPDSetupInfo.Reserved_30))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_31 != externalPDSetupInfo.Reserved_31))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_32 != externalPDSetupInfo.Reserved_32))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_33 != externalPDSetupInfo.Reserved_33))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_34 != externalPDSetupInfo.Reserved_34))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_35 != externalPDSetupInfo.Reserved_35))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_36 != externalPDSetupInfo.Reserved_36))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_37 != externalPDSetupInfo.Reserved_37))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_38 != externalPDSetupInfo.Reserved_38))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_39 != externalPDSetupInfo.Reserved_39))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_40 != externalPDSetupInfo.Reserved_40))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_41 != externalPDSetupInfo.Reserved_41))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_42 != externalPDSetupInfo.Reserved_42))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_43 != externalPDSetupInfo.Reserved_43))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_44 != externalPDSetupInfo.Reserved_44))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_45 != externalPDSetupInfo.Reserved_45))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_46 != externalPDSetupInfo.Reserved_46))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_47 != externalPDSetupInfo.Reserved_47))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_48 != externalPDSetupInfo.Reserved_48))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_49 != externalPDSetupInfo.Reserved_49))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_50 != externalPDSetupInfo.Reserved_50))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_51 != externalPDSetupInfo.Reserved_51))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_52 != externalPDSetupInfo.Reserved_52))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_53 != externalPDSetupInfo.Reserved_53))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_54 != externalPDSetupInfo.Reserved_54))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_55 != externalPDSetupInfo.Reserved_55))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_56 != externalPDSetupInfo.Reserved_56))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_57 != externalPDSetupInfo.Reserved_57))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_58 != externalPDSetupInfo.Reserved_58))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_59 != externalPDSetupInfo.Reserved_59))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_60 != externalPDSetupInfo.Reserved_60))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_61 != externalPDSetupInfo.Reserved_61))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_62 != externalPDSetupInfo.Reserved_62))
                {
                    isTheSame = false;
                }
                if (isTheSame && (pdSetupInfo.Reserved_63 != externalPDSetupInfo.Reserved_63))
                {
                    isTheSame = false;
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.PDSetupInfoIsTheSame(PDM_ConfigComponent_PDSetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame; 
        }

        private bool SetupInfoIsTheSame(PDM_ConfigComponent_SetupInfo externalSetupInfo, bool measurementsInfoMatters)
        {
            bool isTheSame = true;
            try
            {
                if (measurementsInfoMatters)
                {
                    if (isTheSame && (setupInfo.ScheduleType != externalSetupInfo.ScheduleType))
                    {
                        isTheSame = false;
                    }
                    if (isTheSame && (setupInfo.DTime_Hour != externalSetupInfo.DTime_Hour))
                    {
                        isTheSame = false;
                    }
                    if (isTheSame && (setupInfo.DTime_Minute != externalSetupInfo.DTime_Minute))
                    {
                        isTheSame = false;
                    }
                    if (isTheSame && (setupInfo.Stopped != externalSetupInfo.Stopped))
                    {
                        isTheSame = false;
                    }
                }
                if (isTheSame && (setupInfo.DisplayFlag != externalSetupInfo.DisplayFlag))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.RelayMode != externalSetupInfo.RelayMode))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.TimeOfRelayAlarm != externalSetupInfo.TimeOfRelayAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.OutTime != externalSetupInfo.OutTime))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.DeviceNumber != externalSetupInfo.DeviceNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.BaudRate != externalSetupInfo.BaudRate))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ModBusProtocol != externalSetupInfo.ModBusProtocol))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.AlarmEnable != externalSetupInfo.AlarmEnable))
                {
                    isTheSame = false;
                }
                //if (isTheSame && (setupInfo.Termostat != externalSetupInfo.Termostat))
                //{
                //    isTheSame = false;
                //}
                if (isTheSame && (setupInfo.ReReadOnAlarm != externalSetupInfo.ReReadOnAlarm))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_0 - externalSetupInfo.RatedCurrent_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_1 - externalSetupInfo.RatedCurrent_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_2 - externalSetupInfo.RatedCurrent_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_3 - externalSetupInfo.RatedCurrent_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_4 - externalSetupInfo.RatedCurrent_4) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_5 - externalSetupInfo.RatedCurrent_5) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_6 - externalSetupInfo.RatedCurrent_6) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_7 - externalSetupInfo.RatedCurrent_7) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_8 - externalSetupInfo.RatedCurrent_8) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_9 - externalSetupInfo.RatedCurrent_9) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_10 - externalSetupInfo.RatedCurrent_10) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_11 - externalSetupInfo.RatedCurrent_11) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_12 - externalSetupInfo.RatedCurrent_12) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_13 - externalSetupInfo.RatedCurrent_13) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedCurrent_14 - externalSetupInfo.RatedCurrent_14) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_0 - externalSetupInfo.RatedVoltage_0) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_1 - externalSetupInfo.RatedVoltage_1) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_2 - externalSetupInfo.RatedVoltage_2) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_3 - externalSetupInfo.RatedVoltage_3) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_4 - externalSetupInfo.RatedVoltage_4) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_5 - externalSetupInfo.RatedVoltage_5) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_6 - externalSetupInfo.RatedVoltage_6) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_7 - externalSetupInfo.RatedVoltage_7) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_8 - externalSetupInfo.RatedVoltage_8) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_9 - externalSetupInfo.RatedVoltage_9) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_10 - externalSetupInfo.RatedVoltage_10) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_11 - externalSetupInfo.RatedVoltage_11) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_12 - externalSetupInfo.RatedVoltage_12) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_13 - externalSetupInfo.RatedVoltage_13) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(setupInfo.RatedVoltage_14 - externalSetupInfo.RatedVoltage_14) > epsilon))
                {
                    isTheSame = false;
                }

                if (isTheSame && (setupInfo.ReadAnalogFromRegisters != externalSetupInfo.ReadAnalogFromRegisters))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.ObjectType != externalSetupInfo.ObjectType))
                {
                    isTheSame = false;
                }

                if (isTheSame && (Math.Abs(setupInfo.Power - externalSetupInfo.Power) > epsilon))
                {
                    isTheSame = false;
                }

                //if (isTheSame && (setupInfo.ExtTemperature != externalSetupInfo.ExtTemperature))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (setupInfo.ExtHumidity != externalSetupInfo.ExtHumidity))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (setupInfo.ExtLoadActive != externalSetupInfo.ExtLoadActive))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (setupInfo.ExtLoadReactive != externalSetupInfo.ExtLoadReactive))
                //{
                //    isTheSame = false;
                //}

                if (isTheSame && (setupInfo.Reserved_0 != externalSetupInfo.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_1 != externalSetupInfo.Reserved_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_2 != externalSetupInfo.Reserved_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_3 != externalSetupInfo.Reserved_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_4 != externalSetupInfo.Reserved_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_5 != externalSetupInfo.Reserved_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_6 != externalSetupInfo.Reserved_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_7 != externalSetupInfo.Reserved_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_8 != externalSetupInfo.Reserved_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_9 != externalSetupInfo.Reserved_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_10 != externalSetupInfo.Reserved_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_11 != externalSetupInfo.Reserved_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_12 != externalSetupInfo.Reserved_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_13 != externalSetupInfo.Reserved_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_14 != externalSetupInfo.Reserved_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_15 != externalSetupInfo.Reserved_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_16 != externalSetupInfo.Reserved_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_17 != externalSetupInfo.Reserved_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_18 != externalSetupInfo.Reserved_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_19 != externalSetupInfo.Reserved_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_20 != externalSetupInfo.Reserved_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_21 != externalSetupInfo.Reserved_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_22 != externalSetupInfo.Reserved_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_23 != externalSetupInfo.Reserved_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_24 != externalSetupInfo.Reserved_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_25 != externalSetupInfo.Reserved_25))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_26 != externalSetupInfo.Reserved_26))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_27 != externalSetupInfo.Reserved_27))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_28 != externalSetupInfo.Reserved_28))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_29 != externalSetupInfo.Reserved_29))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_30 != externalSetupInfo.Reserved_30))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_31 != externalSetupInfo.Reserved_31))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_32 != externalSetupInfo.Reserved_32))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_33 != externalSetupInfo.Reserved_33))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_34 != externalSetupInfo.Reserved_34))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_35 != externalSetupInfo.Reserved_35))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_36 != externalSetupInfo.Reserved_36))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_37 != externalSetupInfo.Reserved_37))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_38 != externalSetupInfo.Reserved_38))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_39 != externalSetupInfo.Reserved_39))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_40 != externalSetupInfo.Reserved_40))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_41 != externalSetupInfo.Reserved_41))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_42 != externalSetupInfo.Reserved_42))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_43 != externalSetupInfo.Reserved_43))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_44 != externalSetupInfo.Reserved_44))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_45 != externalSetupInfo.Reserved_45))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_46 != externalSetupInfo.Reserved_46))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_47 != externalSetupInfo.Reserved_47))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_48 != externalSetupInfo.Reserved_48))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_49 != externalSetupInfo.Reserved_49))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_50 != externalSetupInfo.Reserved_50))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_51 != externalSetupInfo.Reserved_51))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_52 != externalSetupInfo.Reserved_52))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_53 != externalSetupInfo.Reserved_53))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_54 != externalSetupInfo.Reserved_54))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_55 != externalSetupInfo.Reserved_55))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_56 != externalSetupInfo.Reserved_56))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_57 != externalSetupInfo.Reserved_57))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_58 != externalSetupInfo.Reserved_58))
                {
                    isTheSame = false;
                }
                if (isTheSame && (setupInfo.Reserved_59 != externalSetupInfo.Reserved_59))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.SetupInfoIsTheSame(PDM_ConfigComponent_SetupInfo)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        #endregion

        public static PDM_Configuration CopyConfiguration(PDM_Configuration inputConfiguration)
        {
            PDM_Configuration outputConfiguration = null;
            try
            {
                Byte[] byteData;
                if (inputConfiguration != null)
                {
                    if (inputConfiguration.AllMembersAreNonNull())
                    {
                        byteData = inputConfiguration.ConvertConfigurationToByteArray();
                        outputConfiguration = new PDM_Configuration(byteData, inputConfiguration.setupInfo.FirmwareVersion);
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_Configuration.CopyConfiguration(PDM_Configuration)\nInput PDM_Configuration was missing some key elements, treated as empty.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_Configuration.CopyConfiguration(PDM_Configuration)\nInput PDM_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.CopyConfiguration(PDM_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputConfiguration;
        }

        public int ActiveChannelCount()
        {
            int activeChannelCount = 0;
            try
            {
                if (this.AllMembersAreNonNull())
                {
                    for (int i = 0; i < 15; i++)
                    {
                        if (this.channelInfoList[i].ChannelIsOn == 1)
                        {
                            activeChannelCount++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_Configuration.ActiveChannelCount()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannelCount;
        }

    }
}
