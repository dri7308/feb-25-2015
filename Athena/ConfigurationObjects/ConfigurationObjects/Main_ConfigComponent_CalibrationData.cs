﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_CalibrationData
    {
        public int ChannelNumber;
        public int ChannelNoise;
        [XmlElement(ElementName = "NullLine")]
        public int ZeroLine;
        public double Multiplier;
        public double Offset;
    }
}
