﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_GammaSideSetup
    {
        [XmlElement(ElementName = "TemperatureCoefficientAndRateOfUnnChangeAlarmEnableBitCoded")]
        public int AlarmEnable;
        [XmlElement(ElementName = "AmplitudeUnnWarningThresholdValueUnscaled")]
        public int GammaYellowThreshold;
        [XmlElement(ElementName = "AmplitudeUnnAlarmThresholdValueUnscaled")]
        public int GammaRedThreshold;
        [XmlElement(ElementName = "TemperatureCoefficientAlarmThresholdValueUnscaled")]
        public int TempCoefficient;
        [XmlElement(ElementName = "RateOfUnnChangeAlarmThresholdValueUnscaled")]
        public int TrendAlarm;
        [XmlElement(ElementName = "TransformerNameplateDataTemperatureAtMeasurementUnscaled")]
        public int Temperature0;
        public int InputC_0;
        public int InputC_1;
        public int InputC_2;
        public double InputCoeff_0;
        public double InputCoeff_1;
        public double InputCoeff_2;
        [XmlElement(ElementName = "TransformerNameplateDataTangentPhaseOneUnscaled")]
        public int Tg0_0;
        [XmlElement(ElementName = "TransformerNameplateDataTangentPhaseTwoUnscaled")]
        public int Tg0_1;
        [XmlElement(ElementName = "TransformerNameplateDataTangentPhaseThreeUnscaled")]
        public int Tg0_2;
        [XmlElement(ElementName = "TransformerNameplateDataCapacitancePhaseOneUnscaled")]
        public int C0_0;
        [XmlElement(ElementName = "TransformerNameplateDataCapacitancePhaseTwoUnscaled")]
        public int C0_1;
        [XmlElement(ElementName = "TransformerNameplateDataCapacitancePhaseThreeUnscaled")]
        public int C0_2;
        [XmlElement(ElementName = "InputImpedencePhaseOneInOhmsUnscaled")]
        public int InputImpedance_0;
        [XmlElement(ElementName = "InputImpedencePhaseTwoInOhmsUnscaled")]
        public int InputImpedance_1;
        [XmlElement(ElementName = "InputImpedencePhaseThreeInOhmsUnscaled")]
        public int InputImpedance_2;
        public int MinTemperature1;
        public int AvgTemperature1;
        public int MaxTemperature1;
        public double B_0;
        public double B_1;
        public double B_2;
        public double K_0;
        public double K_1;
        public double K_2;
        public double InputVoltage_0;
        public double InputVoltage_1;
        public double InputVoltage_2;
        public int STABLEDeltaTg_0;
        public int STABLEDeltaTg_1;
        public int STABLEDeltaTg_2;
        public int STABLEDeltaC_0;
        public int STABLEDeltaC_1;
        public int STABLEDeltaC_2;
        public UInt32 STABLEDate;
        public UInt32 HeatDate;
        public int STABLETemperature;
        public int STABLETg_0;
        public int STABLETg_1;
        public int STABLETg_2;
        public int STABLEC_0;
        public int STABLEC_1;
        public int STABLEC_2;
        public int STABLESaved;
        [XmlElement(ElementName = "VoltageSettingInKVolts")]
        public double RatedVoltage;
        [XmlElement(ElementName = "CurrentSettingInAmps")]
        public double RatedCurrent;
        public int StablePhaseAmplitude_0;
        public int StablePhaseAmplitude_1;
        public int StablePhaseAmplitude_2;
        public int StableSourceAmplitude_0;
        public int StableSourceAmplitude_1;
        public int StableSourceAmplitude_2;
        public int StableSignalPhase_0;
        public int StableSignalPhase_1;
        public int StableSignalPhase_2;
        public int StableSourcePhase_0;
        public int StableSourcePhase_1;
        public int StableSourcePhase_2;
        public int STABLEAvgCurrent;
        public int TgYellowThreshold;
        public int TgRedThreshold;
        public int TgVariationThreshold;
        public int ImpedanceValue_0;
        public int ImpedanceValue_1;
        public int ImpedanceValue_2;
        [XmlElement(ElementName = "TemperatureSensor")]
        public int TemperatureConfig;
        public int ExtImpedanceValue_0;
        public int ExtImpedanceValue_1;
        public int ExtImpedanceValue_2;
        public int ExternalSync;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
    }
}
