﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace ConfigurationObjects
{
    public class PDM_ConfigComponent_ChannelInfo
    {
        public int ChannelNumber;
        [XmlElement(ElementName = "EnableChannel")]
        public int ChannelIsOn;
        [XmlElement(ElementName = "Sensitivity")]
        public double ChannelSensitivity;
        [XmlElement(ElementName = "PulseWidth")]
        public int PDMaxWidth;
        [XmlElement(ElementName = "DeadTime")]
        public int PDIntervalTime;
        public long P_0;
        [XmlElement(ElementName = "PDIAlarmsPDIWarningInMillivoltsUnscaled")]
        public long P_1;
         [XmlElement(ElementName = "PDIAlarmsPDIAlarmInMillivoltsUnscaled")]
        public long P_2;
        public long P_3;
         [XmlElement(ElementName = "QmaxAlarmsQmaxWarningInMillivolts")]
        public long P_4;
         [XmlElement(ElementName = "QmaxAlarmsQmaxTrendInMillivolts")]
        public long P_5;
        [XmlElement(ElementName = "SensorPhase")]
        public int CHPhase;
        [XmlElement(ElementName = "CalculatePDIupto")]
        public int CalcPDILimit;
        public int Filter;
        [XmlElement(ElementName = "AmplitudeFilterByGroupOneEnable")]
        public int NoiseOn_0;
        [XmlElement(ElementName = "AmplitudeFilterByGroupTwoEnable")]
        public int NoiseOn_1;
        [XmlElement(ElementName = "AmplitudeFilterByGroupThreeEnable")]
        public int NoiseOn_2;
        [XmlElement(ElementName = "AmplitudeFilterByGroupOneThresholdType")]
        public int NoiseType_0;
        [XmlElement(ElementName = "AmplitudeFilterByGroupTwoThresholdType")]
        public int NoiseType_1;
        [XmlElement(ElementName = "AmplitudeFilterByGroupThreeThresholdType")]
        public int NoiseType_2;
        [XmlElement(ElementName = "AmplitudeFilterByGroupOneThresholdOffsetUnscaled")]
        public int NoiseShift_0;
        [XmlElement(ElementName = "AmplitudeFilterByGroupTwoThresholdOffsetUnscaled")]
        public int NoiseShift_1;
        [XmlElement(ElementName = "AmplitudeFilterByGroupThreeThresholdOffsetUnscaled")]
        public int NoiseShift_2;
        [XmlElement(ElementName = "AmplitudeFilterByGroupOneThresholdStop")]
        public int MinNoiseLevel_0;
         [XmlElement(ElementName = "AmplitudeFilterByGroupTwoThresholdStop")]
        public int MinNoiseLevel_1;
         [XmlElement(ElementName = "AmplitudeFilterByGroupThreeThresholdStop")]
        public int MinNoiseLevel_2;
        [XmlElement(ElementName = "EnableTimeOfArrival")]
        public int TimeOfArrival_0;
        [XmlElement(ElementName = "EnablePolarityAndEnableOppositePolarityBitCoded")]
        public int Polarity_0;
        [XmlElement(ElementName = "ReferenceChannel")]
        public int Ref_0;
        [XmlElement(ElementName = "ReferenceChannelShiftOppositeSign")]
        public int RefShift_0;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
    }
}
