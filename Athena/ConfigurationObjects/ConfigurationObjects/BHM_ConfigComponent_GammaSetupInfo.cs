﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_GammaSetupInfo
    {
        [XmlElement(ElementName = "MeasurementsEnableForBothBushingSetsBitCoded")]
        public int ReadOnSide;
        public int MaxParameterChange;
        [XmlElement(ElementName = "HysterisisPercent")]
        public int AlarmHysteresis;
         [XmlElement(ElementName = "AllowablePhaseDifferenceInDegrees")]
        public int AllowedPhaseDispersion;
        public int DaysToCalculateTrend;
        [XmlElement(ElementName = "DaysToCalculateTemperatureCoefficient")]
        public int DaysToCalculateTCoefficient;
        [XmlElement(ElementName = "AveragingUnnInCycles")]
        public int AveragingForGamma;
        [XmlElement(ElementName = "ConfirmationMeasurementInMinutes")]
        public int ReReadOnAlarm;
        [XmlElement(ElementName = "DaysToCalculateTangent")]
        public int MinDiagGamma;
        public int NEGtg;
        public int DaysToCalculateBASELINE;
        public int LoadChannel;
        public int ReduceCoeff;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
        public int Reserved_21;
        public int Reserved_22;
        public int Reserved_23;
        public int Reserved_24;
        public int Reserved_25;
        public int Reserved_26;
        public int Reserved_27;
        public int Reserved_28;
        public int Reserved_29;
    }
}
