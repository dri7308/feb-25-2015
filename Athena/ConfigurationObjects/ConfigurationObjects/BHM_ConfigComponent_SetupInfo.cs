﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_SetupInfo
    {
        public double FirmwareVersion;
        [XmlElement(ElementName = "ModBusAddress")]
        public int DeviceNumber;
        public int BaudRate;
        public int OutTime;
        public int DisplayFlag;
        public int SaveDays;
        [XmlElement(ElementName = "DisableMonitoring")]
        public int Stopped;
        public int Relay;
        public int TimeOfRelayAlarm;
        [XmlElement(ElementName = "EnableMeasurementScheduleBySchedule")]
        public int ScheduleType;
        [XmlElement(ElementName = "MeasurementScheduleHours")]
        public int DTime_Hour;
        [XmlElement(ElementName = "MeasurementScheduleMinutes")]
        public int DTime_Min;
        public int SyncType;
        public int InternalSyncFrequency;
        public int ModBusProtocol;
        public int ReadAnalogFromRegister;
        public int HeaterOnTemperature;
        public int AutoBalans_Month;
        public int AutoBalans_Year;
        public int AutoBalans_Day;
        public int AutoBalans_Hour;
        public int AutoBalansActive;
        public int NoLoadTestActive;
        public int WorkingDays;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
    }
}
