﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigurationObjects
{
    public class ADM_ConfigComponent_ChannelInfo
    {
        public int ChannelNumber;
        public int ChannelIsOn;
        public double ChannelSensitivity;
        public int PDMaxWidth;
        public int PDIntervalTime;
        public long P_0;
        public long P_1;
        public long P_2;
        public long P_3;
        public long P_4;
        public long P_5;
       
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
    }
}
