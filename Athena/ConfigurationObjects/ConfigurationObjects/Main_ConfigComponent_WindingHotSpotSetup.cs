﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_WindingHotSpotSetup
    {
        public int FirmwareVersion;

        // For clarity, I save all bit-encoded values as strings of 1s and 0s so it's easy
        // to see which bits are active and which are not.
        [XmlElement(ElementName = "ConfigurationParametersRepresentedAsBitString")]
        public string RatingAsBitString;

        [XmlElement(ElementName = "TemperatureRiseOverTopOilHighVoltagePhaseAUnscaled")]
        public int H_RatedHotSpotRisePhaseA;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilHighVoltagePhaseBUnscaled")]
        public int H_RatedHotSpotRisePhaseB;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilHighVoltagePhaseCUnscaled")]
        public int H_RatedHotSpotRisePhaseC;
/*
        [XmlElement(ElementName = "TemperatureRiseOverTopOilLowVoltagePhaseAUnscaled")]
        public int X_RatedHotSpotRisePhaseA;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilLowVoltagePhaseBUnscaled")]
        public int X_RatedHotSpotRisePhaseB;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilLowVoltagePhaseCUnscaled")]
        public int X_RatedHotSpotRisePhaseC;

        [XmlElement(ElementName = "TemperatureRiseOverTopOilTertiaryVoltagePhaseAUnscaled")]
        public int Y_RatedHotSpotRisePhaseA;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilTertiaryVoltagePhaseBUnscaled")]
        public int Y_RatedHotSpotRisePhaseB;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilTertiaryVoltagePhaseCUnscaled")]
        public int Y_RatedHotSpotRisePhaseC;

        [XmlElement(ElementName = "TemperatureRiseOverTopOilCommonPhaseAUnscaled")]
        public int C_RatedHotSpotRisePhaseA;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilCommonPhaseBUnscaled")]
        public int C_RatedHotSpotRisePhaseB;
        [XmlElement(ElementName = "TemperatureRiseOverTopOilCommonPhaseCUnscaled")]
        public int C_RatedHotSpotRisePhaseC;
 */

        [XmlElement(ElementName = "MaximumRatedCurrentHighVoltagePhaseAUnscaled")]
        public int H_MaximumRatedCurrentPhaseA;
        [XmlElement(ElementName = "MaximumRatedCurrentHighVoltagePhaseBUnscaled")]
        public int H_MaximumRatedCurrentPhaseB;
        [XmlElement(ElementName = "MaximumRatedCurrentHighVoltagePhaseCUnscaled")]
        public int H_MaximumRatedCurrentPhaseC;

        [XmlElement(ElementName = "HotSpoFactor")]
        public int HotSpotFactor;

        [XmlElement(ElementName = "fan_1BaseCurrentUnscaled")]
        public int fan_1BaseCurrent;
        [XmlElement(ElementName = "fan_2BaseCurrentUnscaled")]
        public int fan_2BaseCurrent;
        [XmlElement(ElementName = "NumberOfCoolingGroups")]
        public int NumberOfCoolingGroups;
        [XmlElement(ElementName = "fan_LowLimitAlarmUnscaled")]
        public int fan_lowLimitAlarm;
        [XmlElement(ElementName = "fan_HighLimitAlarmUnscaled")]
        public int fan_highLimitAlarm;

/*
        [XmlElement(ElementName = "MaximumRatedCurrentLowVoltagePhaseAUnscaled")]
        public int X_MaximumRatedCurrentPhaseA;
        [XmlElement(ElementName = "MaximumRatedCurrentLowVoltagePhaseBUnscaled")]
        public int X_MaximumRatedCurrentPhaseB;
        [XmlElement(ElementName = "MaximumRatedCurrentLowVoltagePhaseCUnscaled")]
        public int X_MaximumRatedCurrentPhaseC;

        [XmlElement(ElementName = "MaximumRatedCurrentTertiaryVoltagePhaseAUnscaled")]
        public int Y_MaximumRatedCurrentPhaseA;
        [XmlElement(ElementName = "MaximumRatedCurrentTertiaryVoltagePhaseBUnscaled")]
        public int Y_MaximumRatedCurrentPhaseB;
        [XmlElement(ElementName = "MaximumRatedCurrentTertiaryVoltagePhaseCUnscaled")]
        public int Y_MaximumRatedCurrentPhaseC;

        [XmlElement(ElementName = "MaximumRatedCurrentCommonPhaseAUnscaled")]
        public int C_MaximumRatedCurrentPhaseA;
        [XmlElement(ElementName = "MaximumRatedCurrentCommonPhaseBUnscaled")]
        public int C_MaximumRatedCurrentPhaseB;
        [XmlElement(ElementName = "MaximumRatedCurrentCommonPhaseCUnscaled")]
        public int C_MaximumRatedCurrentPhaseC;
 */
        [XmlElement(ElementName = "FanStartTemperatureFanBankOneWHSTempInDegreesCentigrade")]
        public int FanSetpoint_H;
        [XmlElement(ElementName = "FanStartTemperatureFanBankTwoWHSTempInDegreesCentigrade")]
        public int FanSetpoint_HH;

        [XmlElement(ElementName = "ExponentUnscaled")]
        public int Exponent;
        [XmlElement(ElementName = "ExponentUnscaled_B")]
        public int Exponent_B;
        [XmlElement(ElementName = "ExponentUnscaled_C")]
        public int Exponent_C;

        [XmlElement(ElementName = "TimeConstantUnscaled")]
        public int WindingTimeConstant;
        [XmlElement(ElementName = "TimeConstantUnscaled_B")]
        public int WindingTimeConstant_B;
        [XmlElement(ElementName = "TimeConstantUnscaled_C")]
        public int WindingTimeConstant_C;

        [XmlElement(ElementName = "DeadBandTemperature")]
        public int AlarmDeadBandTemperature;
        [XmlElement(ElementName = "AlarmSettingsAlarmDelayInSeconds")]
        public int AlarmDelayInSeconds;

        [XmlElement(ElementName = "AlarmSettingsWHSTooHighLowAlarmSetPointInDegreesCentigrade")]
        public int AlarmMaxSetPointTemperature_H;
        [XmlElement(ElementName = "AlarmSettingsWHSTooHighHighAlarmSetPointInDegreesCentigrade")]
        public int AlarmMaxSetPointTemperature_HH;

        [XmlElement(ElementName = "AlarmSettingsTopOilTempTooHighLowAlarmSetPointInDegreesCentigrade")]
        public int AlarmTopOilTemperature_H;
        [XmlElement(ElementName = "AlarmSettingsTopOilTempTooHighHighAlarmSetPointInDegreesCentigrade")]
        public int AlarmTopOilTemperature_HH;

        [XmlElement(ElementName = "TemperatureInputTopOilPhaseA")]
        public int TopOilTempPhaseASourceRegisterNumber;
/*
        [XmlElement(ElementName = "TemperatureInputTopOilPhaseB")]
        public int TopOilTempPhaseBSourceRegisterNumber;
        [XmlElement(ElementName = "TemperatureInputTopOilPhaseC")]
        public int TopOilTempPhaseCSourceRegisterNumber;
*/

        [XmlElement(ElementName = "CurrentInputHighVoltagePhaseA")]
        public int H_LoadCurrentPhaseASourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputHighVoltagePhaseB")]
        public int H_LoadCurrentPhaseBSourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputHighVoltagePhaseC")]
        public int H_LoadCurrentPhaseCSourceRegisterNumber;
/*
        [XmlElement(ElementName = "CurrentInputLowVoltagePhaseA")]
        public int X_LoadCurrentPhaseASourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputLowVoltagePhaseB")]
        public int X_LoadCurrentPhaseBSourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputLowVoltagePhaseC")]
        public int X_LoadCurrentPhaseCSourceRegisterNumber;

        [XmlElement(ElementName = "CurrentInputTertiaryVoltagePhaseA")]
        public int Y_LoadCurrentPhaseASourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputTertiaryVoltagePhaseB")]
        public int Y_LoadCurrentPhaseBSourceRegisterNumber;
        [XmlElement(ElementName = "CurrentInputTertiaryVoltagePhaseC")]
        public int Y_LoadCurrentPhaseCSourceRegisterNumber;
*/
        [XmlElement(ElementName = "FanStartTemperatureFanBankOneTopOilTempInDegreesCentigrade")]
        public int TopOilTemperatureFanSetpoint_H;
        [XmlElement(ElementName = "FanStartTemperatureFanBankTwoTopOilTempInDegreesCentigrade")]
        public int TopOilTemperatureFanSetpoint_HH;

        public int MinimumFanRunTimeInMinutes;
        [XmlElement(ElementName = "FanAutoExerciseSettingExerciseEveryInDays")]
        public int TimeBetweenFanExerciseInDays;
        [XmlElement(ElementName = "FanAutoExerciseSettingExerciseForInMinutes")]
        public int TimeToExerciseFanInMinutes;
        public DateTime DateOfLastFanExercise;
        public int FanOneRuntimeInHours;
        public int FanTwoRuntimeInHours;
       
        [XmlElement(ElementName = "FanBankSwapEveryInHours")]
        public int FanBankSwitchingTimeInHours;

        // code added by cfk 12/17/2013
        [XmlElement(ElementName = "whsFan1NumberOfStarts")]
        public int whsFan1NumberOfStarts;

        [XmlElement(ElementName = "whsFan2NumberOfStarts")]
        public int whsFan2NumberOfStarts;

        [XmlElement(ElementName = "whsPreviousAgingPhaseA")]
        public double whsPreviousAgingPhaseA;
       
        [XmlElement(ElementName = "whsPreviousAgingPhaseB")]
        public double whsPreviousAgingPhaseB;

        [XmlElement(ElementName = "whsPreviousAgingPhaseC")]
        public double whsPreviousAgingPhaseC;

        [XmlElement(ElementName = "FanTest")]
        public int fanTest;



        public int Reserved;
    }
}
