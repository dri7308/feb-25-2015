﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_DeviceSetup
    {
        public int SetupStatus;
        [XmlElement(ElementName = "EnableMonitoring")]
        public int Monitoring;
        public int Frequency;
        [XmlElement(ElementName = "RelayAlarmSetting")]
        public int AlarmControl;
        [XmlElement(ElementName = "RelayWarningSetting")]
        public int WarningControl;
        [XmlElement(ElementName = "AllowDirectAccessToInternalModulesOverRS485")]
        public int AllowDirectAccess;
    }
}
