﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class PDM_ConfigComponent_SetupInfo
    {
        public double FirmwareVersion;

        [XmlElement(ElementName = "EnableMeasurementScheduleBySchedule")]
        public int ScheduleType;
        [XmlElement(ElementName = "MeasurementScheduleHours")]
        public int DTime_Hour;
        [XmlElement(ElementName = "MeasurementScheduleMinutes")]
        public int DTime_Minute;
        public int DisplayFlag;
        public int RelayMode;
        public int TimeOfRelayAlarm;
        public int OutTime;
        [XmlElement(ElementName = "ModbusAddress")]
        public int DeviceNumber;
        public int BaudRate;
        public int ModBusProtocol;
        [XmlElement(ElementName = "DisableMonitoring")]
        public int Stopped;
        [XmlElement(ElementName = "EnableAlarmsOnPDIAndQmaxBitCoded")]
        public int AlarmEnable;
        public int Termostat;
        public int ReReadOnAlarm;

        public double RatedCurrent_0;
        public double RatedCurrent_1;
        public double RatedCurrent_2;
        public double RatedCurrent_3;
        public double RatedCurrent_4;
        public double RatedCurrent_5;
        public double RatedCurrent_6;
        public double RatedCurrent_7;
        public double RatedCurrent_8;
        public double RatedCurrent_9;
        public double RatedCurrent_10;
        public double RatedCurrent_11;
        public double RatedCurrent_12;
        public double RatedCurrent_13;
        public double RatedCurrent_14;

        [XmlElement(ElementName = "RatedVoltageChannelsChannelAtoChannelC")]
        public double RatedVoltage_0;
        [XmlElement(ElementName = "RatedVoltageChannelsChannelatoChannelc")]
        public double RatedVoltage_1;
        [XmlElement(ElementName = "RatedVoltageChannelsChannelPD1toPD6")]
        public double RatedVoltage_2;
        [XmlElement(ElementName = "RatedVoltageChannelsChannelN1toN3")]
        public double RatedVoltage_3;
        public double RatedVoltage_4;
        public double RatedVoltage_5;
        public double RatedVoltage_6;
        public double RatedVoltage_7;
        public double RatedVoltage_8;
        public double RatedVoltage_9;
        public double RatedVoltage_10;
        public double RatedVoltage_11;
        public double RatedVoltage_12;
        public double RatedVoltage_13;
        public double RatedVoltage_14;

        public int ReadAnalogFromRegisters;
        [XmlElement(ElementName = "NeutralType")]
        public int ObjectType;
        public double Power;
        public int ExtTemperature;
        public int ExtHumidity;
        public int ExtLoadActive;
        public int ExtLoadReactive;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
        public int Reserved_21;
        public int Reserved_22;
        public int Reserved_23;
        public int Reserved_24;
        public int Reserved_25;
        public int Reserved_26;
        public int Reserved_27;
        public int Reserved_28;
        public int Reserved_29;
        public int Reserved_30;
        public int Reserved_31;
        public int Reserved_32;
        public int Reserved_33;
        public int Reserved_34;
        public int Reserved_35;
        public int Reserved_36;
        public int Reserved_37;
        public int Reserved_38;
        public int Reserved_39;
        public int Reserved_40;
        public int Reserved_41;
        public int Reserved_42;
        public int Reserved_43;
        public int Reserved_44;
        public int Reserved_45;
        public int Reserved_46;
        public int Reserved_47;
        public int Reserved_48;
        public int Reserved_49;
        public int Reserved_50;
        public int Reserved_51;
        public int Reserved_52;
        public int Reserved_53;
        public int Reserved_54;
        public int Reserved_55;
        public int Reserved_56;
        public int Reserved_57;
        public int Reserved_58;
        public int Reserved_59;
    }
}
