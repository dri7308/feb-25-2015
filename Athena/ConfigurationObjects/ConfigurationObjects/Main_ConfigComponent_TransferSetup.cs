﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace ConfigurationObjects
{
    public class Main_ConfigComponent_TransferSetup
    {
        public int TransferSetupNumber;
        public int TransferNumber;
        [XmlElement(ElementName = "Enabled")]
        public int Working;
        [XmlElement(ElementName = "MasterDeviceModbusAddress")]
        public int SourceModuleNumber;
        [XmlElement(ElementName = "MasterDeviceRegisterNumber")]
        public int SourceRegisterNumber;
        [XmlElement(ElementName = "SlaveDeviceModbusAddress")]
        public int DestinationModuleNumber;
        [XmlElement(ElementName = "SlaveDeviceRegisterNumber")]
        public int DestinationRegisterNumber;
        [XmlElement(ElementName = "Slope")]
        public double Multiplier;
        public int Reserved;
    }
}
