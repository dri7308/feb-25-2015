﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class Main_ConfigComponent_General
    {
        public int DeviceType;
        public double FirmwareVersion;
        public int GlobalStatus;
        public int DevicesError;
        public DateTime DeviceDateTime;
        [XmlElement(ElementName = "BoardNameFirstTwoCharactersAsShort")]
        public int UnitName_0;
        [XmlElement(ElementName = "BoardNameSecondTwoCharactersAsShort")]
        public int UnitName_1;
        [XmlElement(ElementName = "BoardNameThirdTwoCharactersAsShort")]
        public int UnitName_2;
        [XmlElement(ElementName = "BoardNameFourthTwoCharactersAsShort")]
        public int UnitName_3;
        [XmlElement(ElementName = "BoardNameFifthTwoCharactersAsShort")]
        public int UnitName_4;
        [XmlElement(ElementName = "BoardNameSixthTwoCharactersAsShort")]
        public int UnitName_5;
        [XmlElement(ElementName = "BoardNameSeventhTwoCharactersAsShort")]
        public int UnitName_6;
        [XmlElement(ElementName = "BoardNameEighthTwoCharactersAsShort")]
        public int UnitName_7;
        [XmlElement(ElementName = "BoardNameNinthTwoCharactersAsShort")]
        public int UnitName_8;
        [XmlElement(ElementName = "BoardNameTenthTwoCharactersAsShort")]
        public int UnitName_9;
        [XmlElement(ElementName = "BoardNameEleventhTwoCharactersAsShort")]
        public int UnitName_10;
        [XmlElement(ElementName = "BoardNameTwelfthTwoCharactersAsShort")]
        public int UnitName_11;
        [XmlElement(ElementName = "BoardNameThirteenthTwoCharactersAsShort")]
        public int UnitName_12;
        [XmlElement(ElementName = "BoardNameFourteenthTwoCharactersAsShort")]
        public int UnitName_13;
        [XmlElement(ElementName = "BoardNameFifteenthTwoCharactersAsShort")]
        public int UnitName_14;
        [XmlElement(ElementName = "BoardNameSixteenthTwoCharactersAsShort")]
        public int UnitName_15;
        [XmlElement(ElementName = "BoardNameSeventeenthTwoCharactersAsShort")]
        public int UnitName_16;
        [XmlElement(ElementName = "BoardNameEighteenthTwoCharactersAsShort")]
        public int UnitName_17;
        [XmlElement(ElementName = "BoardNameNineteenthTwoCharactersAsShort")]
        public int UnitName_18;
        [XmlElement(ElementName = "BoardNameTwentyithTwoCharactersAsShort")]
        public int UnitName_19;

    }
}
