﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_InitialParameters
    {
        public int Day;
        public int Month;
        public int Year;
        public int Hour;
        public int Min;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
    }
}
