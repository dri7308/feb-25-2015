﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_MeasurementsInfo
    {
        [XmlElement(ElementName = "ScheduledMeasurementNumber")]
        public int ItemNumber;
        [XmlElement(ElementName = "ScheduledMeasurementHour")]
        public int Hour;
        [XmlElement(ElementName = "ScheduledMeasurementMinute")]
        public int Minute;
    }
}
