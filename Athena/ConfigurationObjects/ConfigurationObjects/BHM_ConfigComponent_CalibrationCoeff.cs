﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigurationObjects
{
    public class BHM_ConfigComponent_CalibrationCoeff
    {
        [XmlElement(ElementName = "TemperatureK_0")]
        public double TemperatureK_0;
        public double TemperatureK_1;
        public double TemperatureK_2;
        public double TemperatureK_3;
        public double TemperatureB_0;
        public double TemperatureB_1;
        public double TemperatureB_2;
        public double TemperatureB_3;
        public double CurrentK_0;
        public double CurrentK_1;
        public double CurrentK_2;
        public double CurrentK_3;
        public double HumidityOffset_0;
        public double HumidityOffset_1;
        public double HumidityOffset_2;
        public double HumidityOffset_3;
        public double HumiditySlope_0;
        public double HumiditySlope_1;
        public double HumiditySlope_2;
        public double HumiditySlope_3;
        public double VoltageK_0_0;
        public double VoltageK_0_1;
        public double VoltageK_0_2;
        public double VoltageK_1_0;
        public double VoltageK_1_1;
        public double VoltageK_1_2;
        public double CurrentB_0;
        public double CurrentB_1;
        public double CurrentB_2;
        public double CurrentB_3;
        public double I4_20K;
        public double I4_20B;
        public int CurrentChannelOnPhase_0;
        public int CurrentChannelOnPhase_1;
        public int CurrentChannelOnPhase_2;
        public int CurrentChannelOnPhase_3;

        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;

    }
}
