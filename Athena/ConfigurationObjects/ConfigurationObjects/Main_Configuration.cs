﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using GeneralUtilities;

namespace ConfigurationObjects
{
    public class Main_Configuration
    {
        [XmlIgnore]
        public Int16[] rawValues;

        public Main_ConfigComponent_General generalConfigObject;
        [XmlArray ("shortStatusInformationConfigObjects")]
        [XmlArrayItem ("Main_ConfigComponent_ShortStatusInformation")]
        public List<Main_ConfigComponent_ShortStatusInformation> shortStatusInformationConfigObjects;
        public Main_ConfigComponent_DeviceSetup deviceSetupConfigObject;
        public Main_ConfigComponent_CommunicationSetup communicationSetupConfigObject;
        [XmlArray ("fullStatusInformationConfigObjects")]
        [XmlArrayItem ("Main_ConfigComponent_FullStatusInformation")]
        public List<Main_ConfigComponent_FullStatusInformation> fullStatusInformationConfigObjects;
        [XmlArray("inputChannelConfigObjects")]
        [XmlArrayItem("Main_ConfigComponent_InputChannel")]
        public List<Main_ConfigComponent_InputChannel> inputChannelConfigObjects;
        [XmlArray("inputChannelEffluviaConfigObjects")]
        [XmlArrayItem("Main_ConfigComponent_InputChannelEffluvia")]
        public List<Main_ConfigComponent_InputChannelEffluvia> inputChannelEffluviaConfigObjects;
        [XmlArray("inputChannelThresholdConfigObjects")]
        [XmlArrayItem("Main_ConfigComponent_InputChannelThreshold")]
        public List<Main_ConfigComponent_InputChannelThreshold> inputChannelThresholdConfigObjects;
        [XmlArray("calibrationDataConfigObjects")]
        [XmlArrayItem("Main_ConfigComponent_CalibrationData")]
        public List<Main_ConfigComponent_CalibrationData> calibrationDataConfigObjects;
        [XmlArray("transferSetupConfigObjects")]
        [XmlArrayItem("Main_ConfigComponent_TransferSetup")]
        public List<Main_ConfigComponent_TransferSetup> transferSetupConfigObjects;

        private double epsilon = 1.0e-5;

        public Main_Configuration(Int16[] shortValues)
        {
            rawValues = shortValues;

            InitializeInternalData(shortValues);
        }

        public Main_Configuration(Main_ConfigComponent_General argGeneralConfigObject,
                                  List<Main_ConfigComponent_ShortStatusInformation> argShortStatusInformationConfigObjects,
                                  Main_ConfigComponent_DeviceSetup argDeviceSetupConfigObject,
                                  Main_ConfigComponent_CommunicationSetup argCommunicationSetupConfigObject,
                                  List<Main_ConfigComponent_FullStatusInformation> argFullStatusInformationConfigObjects,
                                  List<Main_ConfigComponent_InputChannel> argInputChannelConfigObjects,
                                  List<Main_ConfigComponent_InputChannelEffluvia> arginputChannelEffluviaConfigObjects,
                                  List<Main_ConfigComponent_InputChannelThreshold> argInputChannelThresholdConfigObjects,
                                  List<Main_ConfigComponent_CalibrationData> argCalibrationDataConfigObjects,
                                  List<Main_ConfigComponent_TransferSetup> argTransferSetupConfigObjects)
                                  
        {
            try
            {
                generalConfigObject = argGeneralConfigObject;
                shortStatusInformationConfigObjects = argShortStatusInformationConfigObjects;
                deviceSetupConfigObject = argDeviceSetupConfigObject;
                communicationSetupConfigObject = argCommunicationSetupConfigObject;
                fullStatusInformationConfigObjects = argFullStatusInformationConfigObjects;
                inputChannelConfigObjects = argInputChannelConfigObjects;
                inputChannelEffluviaConfigObjects = arginputChannelEffluviaConfigObjects;
                inputChannelThresholdConfigObjects = argInputChannelThresholdConfigObjects;
                calibrationDataConfigObjects = argCalibrationDataConfigObjects;
                transferSetupConfigObjects = argTransferSetupConfigObjects;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.Main_Configuration(Main_ConfigComponent_General, List<Main_ConfigComponent_ShortStatusInformation>, Main_ConfigComponent_DeviceSetup, Main_ConfigComponent_CommunicationSetup, List<Main_ConfigComponent_FullStatusInformation>, List<Main_ConfigComponent_InputChannel>, List<Main_ConfigComponent_InputChannelEffluvia>, List<Main_ConfigComponent_InputChannelThreshold>, List<Main_ConfigComponent_CalibrationData>, List<Main_ConfigComponent_TransferSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_Configuration()
        {
            generalConfigObject = new Main_ConfigComponent_General();
            shortStatusInformationConfigObjects = new List<Main_ConfigComponent_ShortStatusInformation>();
            deviceSetupConfigObject = new Main_ConfigComponent_DeviceSetup();
            communicationSetupConfigObject = new Main_ConfigComponent_CommunicationSetup();
            fullStatusInformationConfigObjects = new List<Main_ConfigComponent_FullStatusInformation>();
            inputChannelConfigObjects = new List<Main_ConfigComponent_InputChannel>();
            inputChannelEffluviaConfigObjects = new List<Main_ConfigComponent_InputChannelEffluvia>();
            inputChannelThresholdConfigObjects = new List<Main_ConfigComponent_InputChannelThreshold>();
            calibrationDataConfigObjects = new List<Main_ConfigComponent_CalibrationData>();
            transferSetupConfigObjects = new List<Main_ConfigComponent_TransferSetup>();
        }

        public void InitializeDataToZeroes()
        {
            generalConfigObject = InitializeEmptyGeneral();
            shortStatusInformationConfigObjects = InitializeEmptyShortStatusInformationList();
            deviceSetupConfigObject = InitializeEmptyDeviceSetup();
            communicationSetupConfigObject = InitializeEmptyCommunicationSetup();
            fullStatusInformationConfigObjects = InitializeEmptyFullStatusInformationList();
            inputChannelConfigObjects = InitializeEmptyInputChannelList();
            inputChannelEffluviaConfigObjects = InitializeEmptyInputChannelEffluviaList();
            inputChannelThresholdConfigObjects = InitializeEmptyInputChannelThresholdList();
            calibrationDataConfigObjects = InitializeEmptyCalibrationDataList();
            transferSetupConfigObjects = InitializeEmptyTransferSetupList();
        }

        private void InitializeInternalData(Int16[] shortValues)
        {
            try
            {
                if (shortValues != null)
                {
                    if (shortValues.Length >= 6000)
                    {
                        this.generalConfigObject = ExtractGeneralFromInputData(shortValues);
                        this.shortStatusInformationConfigObjects = ExtractAllShortStatusInformationFromInputData(shortValues);
                        this.deviceSetupConfigObject = ExtractDeviceSetupFromInputData(shortValues);
                        this.communicationSetupConfigObject = ExtractCommunicationSetupFromInputData(shortValues);
                        this.fullStatusInformationConfigObjects = ExtractAllFullStatusInformationFromInputData(shortValues);
                        this.inputChannelConfigObjects = ExtractAllInputChannelFromInputData(shortValues);
                        this.inputChannelEffluviaConfigObjects = ExtractAllInputChannelEffluviaFromInputData(shortValues);
                        this.inputChannelThresholdConfigObjects = ExtractAllInputChannelThresholdsFromInputData(shortValues);
                        this.calibrationDataConfigObjects = ExtractAllCalibrationDataFromInputData(shortValues);
                        this.transferSetupConfigObjects = ExtractAllTransferSetupFromInputData(shortValues);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.InitializeInternalData(Int16[])\nInput Int16[] was less than 6000 entries long.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.InitializeInternalData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeInternalData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public bool AllConfigurationMembersAreNonNull()
        {
            bool allNonNull = true;
            try
            {
                StringBuilder errorMessage = new StringBuilder();

                errorMessage.Append("The following Main_Configuration members are null: ");

                if (generalConfigObject == null)
                {
                    allNonNull = false;
                    errorMessage.Append("generalConfigObject ");
                }

                if (shortStatusInformationConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("shortStatusInformationConfigObjects ");
                }
                if (deviceSetupConfigObject == null)
                {
                    allNonNull = false;
                    errorMessage.Append("deviceSetupConfigObject ");
                }
                if (communicationSetupConfigObject == null)
                {
                    allNonNull = false;
                    errorMessage.Append("communicationSetupConfigObject ");
                }
                if (fullStatusInformationConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("fullStatusInformationConfigObjects ");
                }
                if (inputChannelConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("inputChannelConfigObjects ");
                }
                if (inputChannelEffluviaConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("inputChannelEffluviaConfigObjects ");
                }
                if (inputChannelThresholdConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("inputChannelThresholdConfigObjects ");
                }
                if (calibrationDataConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("calibrationDataConfigObjects ");
                }
                if (transferSetupConfigObjects == null)
                {
                    allNonNull = false;
                    errorMessage.Append("transferSetupConfigObjects ");
                }

                if (!allNonNull)
                {
                    LogMessage.LogError(errorMessage.ToString());
#if DEBUG
                    MessageBox.Show(errorMessage.ToString());
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.AllConfigurationMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allNonNull;
        }

        public Int16[] GetShortValuesFromAllContributors()
        {
            Int16[] outputValues = null;
            try
            {
                if (this.rawValues != null)
                {
                    int length = this.rawValues.Length;
                    outputValues = new Int16[length];
                    Array.Copy(this.rawValues, outputValues, length);
                }
                else
                {
                    outputValues = new Int16[6000];
                }
                ConvertGeneralToShorts(generalConfigObject, ref outputValues);
                ConvertAllShortStatusInformationToShorts(shortStatusInformationConfigObjects, ref outputValues);
                ConvertDeviceSetupToShorts(this.deviceSetupConfigObject, ref outputValues);
                ConvertCommunicationSetupToShorts(this.communicationSetupConfigObject, ref outputValues);
                ConvertAllFullStatusInformationToShorts(this.fullStatusInformationConfigObjects, ref outputValues);
                ConvertAllInputChannelToShorts(this.inputChannelConfigObjects, ref outputValues);
                ConvertAllInputChannelEffluviaToShorts(this.inputChannelEffluviaConfigObjects, ref outputValues);
                ConvertAllInputChannelThresholdsToShorts(this.inputChannelThresholdConfigObjects, ref outputValues);
                ConvertAllCalibrationDataToShorts(this.calibrationDataConfigObjects, ref outputValues);
                ConvertAllTransferSetupToShorts(this.transferSetupConfigObjects, ref outputValues);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.GetShortValuesFromAllContributors()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputValues;
        }

        public Main_ConfigComponent_General InitializeEmptyGeneral()
        {
            Main_ConfigComponent_General general = new Main_ConfigComponent_General();
            try
            {
                general.DeviceType = 101;
                general.FirmwareVersion = 0;
                general.GlobalStatus = 1;
                general.DevicesError = 0;
                general.DeviceDateTime = ConversionMethods.MinimumDateTime();

                general.UnitName_0 = 17477;
                general.UnitName_1 = 8265;
                general.UnitName_2 = 24909;
                general.UnitName_3 = 28265;
                general.UnitName_4 = 25120;
                general.UnitName_5 = 24943;
                general.UnitName_6 = 25714;
                general.UnitName_7 = 8224;
                general.UnitName_8 = 8224;
                general.UnitName_9 = 8224;
                general.UnitName_10 = 8224;
                general.UnitName_11 = 8224;
                general.UnitName_12 = 8224;
                general.UnitName_13 = 8224;
                general.UnitName_14 = 8224;
                general.UnitName_15 = 8224;
                general.UnitName_16 = 8224;
                general.UnitName_17 = 8224;
                general.UnitName_18 = 8224;
                general.UnitName_19 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractGeneralFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return general;
        }

        public static Main_ConfigComponent_General ExtractGeneralFromInputData(Int16[] shortValues)
        {
            Main_ConfigComponent_General general = null;
            try
            {
                Main_ConfigComponent_General newGeneral;
                string mainBoardName;
                if (shortValues != null)
                {
                    if (shortValues.Length >= 40)
                    {
                        general = new Main_ConfigComponent_General();

                        general.DeviceType = shortValues[0];
                        general.FirmwareVersion = shortValues[1] / 100.0;
                        general.GlobalStatus = shortValues[3];
                        general.DevicesError = shortValues[8];
                        general.DeviceDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(shortValues[9], shortValues[10], shortValues[11], shortValues[12], shortValues[13], shortValues[14]);

                        general.UnitName_0 = shortValues[20];
                        general.UnitName_1 = shortValues[21];
                        general.UnitName_2 = shortValues[22];
                        general.UnitName_3 = shortValues[23];
                        general.UnitName_4 = shortValues[24];
                        general.UnitName_5 = shortValues[25];
                        general.UnitName_6 = shortValues[26];
                        general.UnitName_7 = shortValues[27];
                        general.UnitName_8 = shortValues[28];
                        general.UnitName_9 = shortValues[29];
                        general.UnitName_10 = shortValues[30];
                        general.UnitName_11 = shortValues[31];
                        general.UnitName_12 = shortValues[32];
                        general.UnitName_13 = shortValues[33];
                        general.UnitName_14 = shortValues[34];
                        general.UnitName_15 = shortValues[35];
                        general.UnitName_16 = shortValues[36];
                        general.UnitName_17 = shortValues[37];
                        general.UnitName_18 = shortValues[38];
                        general.UnitName_19 = shortValues[39];

                        /// the following is an attempt to rid myself of a bunch of 
                        /// garbage that is contained in the string 
                        mainBoardName = GetBoardNameFromRegisterValues(general);
                        //length = mainBoardName.Length;
                        
                        mainBoardName.Replace('\0',' ');

                        newGeneral = ConvertBoardNameToRegisterValues(mainBoardName);

                        general.UnitName_0 = newGeneral.UnitName_0;
                        general.UnitName_1 = newGeneral.UnitName_1;
                        general.UnitName_2 = newGeneral.UnitName_2;
                        general.UnitName_3 = newGeneral.UnitName_3;
                        general.UnitName_4 = newGeneral.UnitName_4;
                        general.UnitName_5 = newGeneral.UnitName_5;
                        general.UnitName_6 = newGeneral.UnitName_6;
                        general.UnitName_7 = newGeneral.UnitName_7;
                        general.UnitName_8 = newGeneral.UnitName_8;
                        general.UnitName_9 = newGeneral.UnitName_9;
                        general.UnitName_10 = newGeneral.UnitName_10;
                        general.UnitName_11 = newGeneral.UnitName_11;
                        general.UnitName_12 = newGeneral.UnitName_12;
                        general.UnitName_13 = newGeneral.UnitName_13;
                        general.UnitName_14 = newGeneral.UnitName_14;
                        general.UnitName_15 = newGeneral.UnitName_15;
                        general.UnitName_16 = newGeneral.UnitName_16;
                        general.UnitName_17 = newGeneral.UnitName_17;
                        general.UnitName_18 = newGeneral.UnitName_18;
                        general.UnitName_19 = newGeneral.UnitName_19;
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractGeneralFromInputData(Int16[])\nInput Int16[] had less than 40 entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractGeneralFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractGeneralFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return general;
        }





        public static void ConvertGeneralToShorts(Main_ConfigComponent_General general, ref Int16[] shortValues)
        {
            try
            {
                if (general != null)
                {
                    if (shortValues != null)
                    {
                        if (shortValues.Length >= 40)
                        {
                            shortValues[0] = 101;
                            shortValues[1] = (Int16)(Math.Round((general.FirmwareVersion * 100), 0));
                            shortValues[3] = (Int16)general.GlobalStatus;
                            shortValues[8] = (Int16)general.DevicesError;

                            SetDeviceDateTimeToCurrentDateTime(ref shortValues);

                            shortValues[20] = (Int16)general.UnitName_0;
                            shortValues[21] = (Int16)general.UnitName_1;
                            shortValues[22] = (Int16)general.UnitName_2;
                            shortValues[23] = (Int16)general.UnitName_3;
                            shortValues[24] = (Int16)general.UnitName_4;
                            shortValues[25] = (Int16)general.UnitName_5;
                            shortValues[26] = (Int16)general.UnitName_6;
                            shortValues[27] = (Int16)general.UnitName_7;
                            shortValues[28] = (Int16)general.UnitName_8;
                            shortValues[29] = (Int16)general.UnitName_9;
                            shortValues[30] = (Int16)general.UnitName_10;
                            shortValues[31] = (Int16)general.UnitName_11;
                            shortValues[32] = (Int16)general.UnitName_12;
                            shortValues[33] = (Int16)general.UnitName_13;
                            shortValues[34] = (Int16)general.UnitName_14;
                            shortValues[35] = (Int16)general.UnitName_15;
                            shortValues[36] = (Int16)general.UnitName_16;
                            shortValues[37] = (Int16)general.UnitName_17;
                            shortValues[38] = (Int16)general.UnitName_18;
                            shortValues[39] = (Int16)general.UnitName_19;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertGeneralToShorts(Main_ConfigComponent_General, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertGeneralToShorts(Main_ConfigComponent_General, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertGeneralToShorts(Main_ConfigComponent_General, ref Int16[])\nInput Main_ConfigComponent_General was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertGeneralToShorts(Main_ConfigComponent_General, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public static void SetDeviceDateTimeToCurrentDateTime(ref Int16[] shortValues)
        {
            try
            {
                Int16[] timeAsIntegers;
                if (shortValues != null)
                {
                    if (shortValues.Length >= 6)
                    {
                        timeAsIntegers = ConversionMethods.ConvertDateToIntegerArray(DateTime.Now);

                        shortValues[9] = timeAsIntegers[0];
                        shortValues[10] = timeAsIntegers[1];
                        shortValues[11] = timeAsIntegers[2];
                        shortValues[12] = timeAsIntegers[3];
                        shortValues[13] = timeAsIntegers[4];
                        shortValues[14] = timeAsIntegers[5];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_ShortStatusInformation InitializeEmptyShortStatusInformation(int moduleNumber)
        {
            Main_ConfigComponent_ShortStatusInformation shortStatusInfo = new Main_ConfigComponent_ShortStatusInformation();
            try
            {
                shortStatusInfo.ModuleNumber = moduleNumber;
                shortStatusInfo.ModuleStatus = 0;
                shortStatusInfo.ModuleType = 0;
                shortStatusInfo.Reserved = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shortStatusInfo;
        }

        public static Main_ConfigComponent_ShortStatusInformation ExtractShortStatusInformationFromInputData(Int16[] shortValues, int moduleNumber)
        {
            Main_ConfigComponent_ShortStatusInformation shortStatusInfo = null;
            try
            {
                int offset;
                if (shortValues != null)
                {
                    offset = 50 + (3 * moduleNumber);
                    if (shortValues.Length >= (offset + 3))
                    {
                        shortStatusInfo = new Main_ConfigComponent_ShortStatusInformation();

                        shortStatusInfo.ModuleNumber = moduleNumber;
                        shortStatusInfo.ModuleStatus = shortValues[offset];
                        shortStatusInfo.ModuleType = shortValues[offset + 1];
                        shortStatusInfo.Reserved = shortValues[offset + 2];
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractShortStatusInformationFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shortStatusInfo;
        }

        public static void ConvertShortStatusInformationToShorts(Main_ConfigComponent_ShortStatusInformation shortStatusInformation, ref Int16[] shortValues)
        {
            try
            {
                int offset;
                if (shortStatusInformation != null)
                {
                    if (shortValues != null)
                    {
                        offset = 50 + (3 * shortStatusInformation.ModuleNumber);
                        if (shortValues.Length >= (offset + 3))
                        {
                            shortValues[offset] = (Int16)shortStatusInformation.ModuleStatus;
                            shortValues[offset + 1] = (Int16)shortStatusInformation.ModuleType;
                            shortValues[offset + 2] = (Int16)shortStatusInformation.Reserved;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertShortStatusInformationToShorts(Main_ConfigComponent_ShortStatusInformation, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertShortStatusInformationToShorts(Main_ConfigComponent_ShortStatusInformation, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertShortStatusInformationToShorts(Main_ConfigComponent_ShortStatusInformation, ref Int16[])\nInput Main_ConfigComponent_ShortStatusInformation was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertShortStatusInformationToShorts(Main_ConfigComponent_ShortStatusInformation, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_ShortStatusInformation> InitializeEmptyShortStatusInformationList()
        {
            List<Main_ConfigComponent_ShortStatusInformation> allShortStatusInformation = new List<Main_ConfigComponent_ShortStatusInformation>();
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    allShortStatusInformation.Add(InitializeEmptyShortStatusInformation(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllShortStatusInformationFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allShortStatusInformation;
        }


        public static List<Main_ConfigComponent_ShortStatusInformation> ExtractAllShortStatusInformationFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_ShortStatusInformation> allShortStatusInformation = null;
            try
            {
                Main_ConfigComponent_ShortStatusInformation shortStatusInformation;
                if (shortValues != null)
                {
                    allShortStatusInformation = new List<Main_ConfigComponent_ShortStatusInformation>();

                    for (int i = 0; i < 15; i++)
                    {
                        shortStatusInformation = ExtractShortStatusInformationFromInputData(shortValues, i);
                        if (shortStatusInformation != null)
                        {
                            allShortStatusInformation.Add(shortStatusInformation);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ExtractAllShortStatusInformationFromInputData(Int16[])\nTried to add a null item to the return list.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllShortStatusInformationFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllShortStatusInformationFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allShortStatusInformation;
        }

        public static void ConvertAllShortStatusInformationToShorts(List<Main_ConfigComponent_ShortStatusInformation> shortStatusInformationList, ref Int16[] shortValues)
        {
            try
            {
                if (shortStatusInformationList != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_ShortStatusInformation entry in shortStatusInformationList)
                        {
                            ConvertShortStatusInformationToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllShortStatusInformationToShorts(List<Main_ConfigComponent_ShortStatusInformation>,  ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllShortStatusInformationToShorts(List<Main_ConfigComponent_ShortStatusInformation>,  ref Int16[])\nInput List<Main_ConfigComponent_ShortStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllShortStatusInformationToShorts(List<Main_ConfigComponent_ShortStatusInformation>,  ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_DeviceSetup InitializeEmptyDeviceSetup()
        {
            Main_ConfigComponent_DeviceSetup deviceSetup = new Main_ConfigComponent_DeviceSetup();
            try
            {
                deviceSetup.SetupStatus = 0;
                deviceSetup.Monitoring = 0;
                deviceSetup.Frequency = 60;
                deviceSetup.AlarmControl = 0;
                deviceSetup.WarningControl = 0;
                deviceSetup.AllowDirectAccess = 1;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeEmptyDeviceSetup()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceSetup;
        }

        public static Main_ConfigComponent_DeviceSetup ExtractDeviceSetupFromInputData(Int16[] shortValues)
        {
            Main_ConfigComponent_DeviceSetup deviceSetup = null;
            try
            {
                if (shortValues != null)
                {
                    if (shortValues.Length >= 126)
                    {
                        deviceSetup = new Main_ConfigComponent_DeviceSetup();

                        deviceSetup.SetupStatus = shortValues[120];
                        deviceSetup.Monitoring = shortValues[121];
                        deviceSetup.Frequency = shortValues[122];
                        deviceSetup.AlarmControl = shortValues[123];
                        deviceSetup.WarningControl = shortValues[124];
                        deviceSetup.AllowDirectAccess = shortValues[125];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractDeviceSetupFromInputData(Int16[])\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractDeviceSetupFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractDeviceSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return deviceSetup;
        }

        public static void ConvertDeviceSetupToShorts(Main_ConfigComponent_DeviceSetup deviceSetup, ref Int16[] shortValues)
        {
            try
            {
                if (deviceSetup != null)
                {
                    if (shortValues != null)
                    {
                        if (shortValues.Length >= 126)
                        {
                            shortValues[120] = (Int16)deviceSetup.SetupStatus;
                            shortValues[121] = (Int16)deviceSetup.Monitoring;
                            shortValues[122] = (Int16)deviceSetup.Frequency;
                            shortValues[123] = (Int16)deviceSetup.AlarmControl;
                            shortValues[124] = (Int16)deviceSetup.WarningControl;
                            shortValues[125] = (Int16)deviceSetup.AllowDirectAccess;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertDeviceSetupToShorts(Main_ConfigComponent_DeviceSetup, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertDeviceSetupToShorts(Main_ConfigComponent_DeviceSetup, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertDeviceSetupToShorts(Main_ConfigComponent_DeviceSetup, ref Int16[])\nInput Main_ConfigComponent_DeviceSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertDeviceSetupToShorts(Main_ConfigComponent_DeviceSetup, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_CommunicationSetup InitializeEmptyCommunicationSetup()
        {
            Main_ConfigComponent_CommunicationSetup communicationSetup = new Main_ConfigComponent_CommunicationSetup();
            try
            {
                communicationSetup.ModBusAddress = 1;
                communicationSetup.BaudRateRS485 = 1152;
                communicationSetup.ModBusProtocol = 0;
                communicationSetup.BaudRateXport = 1152;
                communicationSetup.ModBusProtocolOverXport = 0;
                communicationSetup.IP1 = 192;
                communicationSetup.IP2 = 168;
                communicationSetup.IP3 = 1;
                communicationSetup.IP4 = 101;
                communicationSetup.SaveDataIntervalInSeconds = 60;
                communicationSetup.SaveDataIfChangedByPercent = 10;
                communicationSetup.SaveDataIfChangedStatus = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeEmptyCommunicationSetup()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return communicationSetup;
        }

        public static Main_ConfigComponent_CommunicationSetup ExtractCommunicationSetupFromInputData(Int16[] shortValues)
        {
            Main_ConfigComponent_CommunicationSetup communicationSetup = null;
            try
            {
                if (shortValues != null)
                {
                    if (shortValues.Length >= 138)
                    {
                        communicationSetup = new Main_ConfigComponent_CommunicationSetup();

                        communicationSetup.ModBusAddress = shortValues[126];
                        communicationSetup.BaudRateRS485 = shortValues[127];
                        ///communicationSetup.modBusProtocol = shortValues[128];
                        communicationSetup.ModBusProtocol = 0;
                        communicationSetup.BaudRateXport = shortValues[129];
                        communicationSetup.ModBusProtocolOverXport = shortValues[130];
                        // communicationSetup.ModBusProtocolOverXport = 0;
                        communicationSetup.IP1 = shortValues[131];
                        communicationSetup.IP2 = shortValues[132];
                        communicationSetup.IP3 = shortValues[133];
                        communicationSetup.IP4 = shortValues[134];
                        communicationSetup.SaveDataIntervalInSeconds = shortValues[135];
                        communicationSetup.SaveDataIfChangedByPercent = shortValues[136];
                        communicationSetup.SaveDataIfChangedStatus = shortValues[137];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractCommunicationSetupFromInputData(Int16[])\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractCommunicationSetupFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractCommunicationSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return communicationSetup;
        }

        public static void ConvertCommunicationSetupToShorts(Main_ConfigComponent_CommunicationSetup communicationSetup, ref Int16[] shortValues)
        {
            try
            {
                if (communicationSetup != null)
                {
                    if (shortValues != null)
                    {
                        if (shortValues.Length >= 138)
                        {
                            shortValues[126] = (Int16)communicationSetup.ModBusAddress;
                            shortValues[127] = (Int16)communicationSetup.BaudRateRS485;
                            // we never want to use any connection type other than RTU
                            shortValues[128] = 0;
                            shortValues[129] = (Int16)communicationSetup.BaudRateXport;
                            shortValues[130] = (Int16)communicationSetup.ModBusProtocolOverXport;
                            //shortValues[130] = 0;
                            shortValues[131] = (Int16)communicationSetup.IP1;
                            shortValues[132] = (Int16)communicationSetup.IP2;
                            shortValues[133] = (Int16)communicationSetup.IP3;
                            shortValues[134] = (Int16)communicationSetup.IP4;
                            shortValues[135] = (Int16)communicationSetup.SaveDataIntervalInSeconds;
                            shortValues[136] = (Int16)communicationSetup.SaveDataIfChangedByPercent;
                            shortValues[137] = (Int16)communicationSetup.SaveDataIfChangedStatus;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertCommunicationSetupToShorts(Main_ConfigComponent_CommunicationSetup, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertCommunicationSetupToShorts(Main_ConfigComponent_CommunicationSetup, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertCommunicationSetupToShorts(Main_ConfigComponent_CommunicationSetup, ref Int16[])\nInput Main_ConfigComponent_CommunicationSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertCommunicationSetupToShorts(Main_ConfigComponent_CommunicationSetup, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_FullStatusInformation InitializeEmptyFullStatusInformation(int moduleNumber)
        {
            Main_ConfigComponent_FullStatusInformation fullStatusInformation = new Main_ConfigComponent_FullStatusInformation();
            try
            {
                fullStatusInformation.ModuleNumber = moduleNumber;
                fullStatusInformation.ModuleStatus = 0;
                fullStatusInformation.ModuleType = 0;
                fullStatusInformation.ModBusAddress = 0;
                fullStatusInformation.BaudRate = 1152;
                fullStatusInformation.ReadDatePeriodInSeconds = 300;
                fullStatusInformation.ConnectionType = 0;
                fullStatusInformation.Reserved_0 = 0;
                fullStatusInformation.Reserved_1 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractFullStatusInformationFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return fullStatusInformation;
        }

        public static Main_ConfigComponent_FullStatusInformation ExtractFullStatusInformationFromInputData(Int16[] shortValues, int moduleNumber)
        {
            Main_ConfigComponent_FullStatusInformation fullStatusInformation = new Main_ConfigComponent_FullStatusInformation();
            try
            {
                int offset;
                if (shortValues != null)
                {
                    offset = 150 + (moduleNumber * 8);
                    if (shortValues.Length >= (offset + 8))
                    {
                        fullStatusInformation.ModuleNumber = moduleNumber;
                        fullStatusInformation.ModuleStatus = shortValues[offset];
                        fullStatusInformation.ModuleType = shortValues[offset + 1];
                        fullStatusInformation.ModBusAddress = shortValues[offset + 2];
                        fullStatusInformation.BaudRate = shortValues[offset + 3];
                        fullStatusInformation.ReadDatePeriodInSeconds = shortValues[offset + 4];
                        fullStatusInformation.ConnectionType = shortValues[offset + 5];
                        fullStatusInformation.Reserved_0 = shortValues[offset + 6];
                        fullStatusInformation.Reserved_1 = shortValues[offset + 7];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractFullStatusInformationFromInputData(Int16[], int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractFullStatusInformationFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractFullStatusInformationFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return fullStatusInformation;
        }

        public static void ConvertFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation fullStatusInformation, ref Int16[] shortValues)
        {
            try
            {
                int offset;
                if (fullStatusInformation != null)
                {
                    if (shortValues != null)
                    {
                        offset = 150 + (fullStatusInformation.ModuleNumber * 8);
                        if (shortValues.Length >= (offset + 8))
                        {
                            shortValues[offset] = (Int16)fullStatusInformation.ModuleStatus;
                            shortValues[offset + 1] = (Int16)fullStatusInformation.ModuleType;
                            shortValues[offset + 2] = (Int16)fullStatusInformation.ModBusAddress;
                            shortValues[offset + 3] = (Int16)fullStatusInformation.BaudRate;
                            shortValues[offset + 4] = (Int16)fullStatusInformation.ReadDatePeriodInSeconds;
                            shortValues[offset + 5] = (Int16)fullStatusInformation.ConnectionType;
                            shortValues[offset + 6] = (Int16)fullStatusInformation.Reserved_0;
                            shortValues[offset + 7] = (Int16)fullStatusInformation.Reserved_1;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation, ref Int16[])\nInput Main_ConfigComponent_FullStatusInformation was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_FullStatusInformation> InitializeEmptyFullStatusInformationList()
        {
            List<Main_ConfigComponent_FullStatusInformation> allFullStatusInformation = new List<Main_ConfigComponent_FullStatusInformation>();
            try
            {
                for (int i = 0; i < 15; i++)
                {
                    allFullStatusInformation.Add(InitializeEmptyFullStatusInformation(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllFullStatusInformationFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allFullStatusInformation;
        }

        public static List<Main_ConfigComponent_FullStatusInformation> ExtractAllFullStatusInformationFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_FullStatusInformation> allFullStatusInformation = null;
            try
            {
                Main_ConfigComponent_FullStatusInformation fullStatusInformation;
                if (shortValues != null)
                {
                    allFullStatusInformation = new List<Main_ConfigComponent_FullStatusInformation>();

                    for (int i = 0; i < 15; i++)
                    {
                        fullStatusInformation = ExtractFullStatusInformationFromInputData(shortValues, i);
                        allFullStatusInformation.Add(fullStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllFullStatusInformationFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllFullStatusInformationFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allFullStatusInformation;
        }

        public static void ConvertAllFullStatusInformationToShorts(List<Main_ConfigComponent_FullStatusInformation> allFullStatusInformation, ref Int16[] shortValues)
        {
            try
            {
                if (allFullStatusInformation != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_FullStatusInformation entry in allFullStatusInformation)
                        {
                            ConvertFullStatusInformationToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllFullStatusInformationToShorts(List<Main_ConfigComponent_FullStatusInformation>, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllFullStatusInformationToShorts(List<Main_ConfigComponent_FullStatusInformation>, ref Int16[])\nInput List<Main_ConfigComponent_FullStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllFullStatusInformationToShorts(Main_ConfigComponent_FullStatusInformation, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_InputChannel InitializeEmptyInputChannel(int inputChannelNumber)
        {
            Main_ConfigComponent_InputChannel inputChannel = new Main_ConfigComponent_InputChannel();
            try
            {
                inputChannel.InputChannelNumber = inputChannelNumber;
                inputChannel.ReadStatus = 0;
                inputChannel.SensorType = 0;
                inputChannel.Unit = 0;
                inputChannel.SensorID = 0;
                inputChannel.SensorNumber = 0;
                inputChannel.Sensitivity = 0;
                inputChannel.HighPassFilter = 1000;
                inputChannel.LowPassFilter = 0;
                inputChannel.ReadFrequency = 2560;
                inputChannel.ReadPoints = 1024;
                inputChannel.Averages = 0;
                inputChannel.SaveDataIfChangedPercent = 0;
                inputChannel.SaveDataIfChangedValue = 0;
                inputChannel.JunkValue_1 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeEmptyInputChannel(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannel;
        }

        public static Main_ConfigComponent_InputChannel ExtractInputChannelFromInputData(Int16[] shortValues, int inputChannelNumber)
        {
            Main_ConfigComponent_InputChannel inputChannel = null;
            try
            {
                int offset;
                if (shortValues != null)
                {
                    offset = 300 + (inputChannelNumber * 200);
                    if (shortValues.Length >= (offset + 16))
                    {
                        inputChannel = new Main_ConfigComponent_InputChannel();

                        inputChannel.InputChannelNumber = inputChannelNumber;
                        inputChannel.ReadStatus = shortValues[offset + 1];
                        inputChannel.SensorType = shortValues[offset + 2];
                        inputChannel.Unit = shortValues[offset + 3];
                        inputChannel.SensorID = shortValues[offset + 4];
                        inputChannel.SensorNumber = shortValues[offset + 5];
                        inputChannel.Sensitivity = ConversionMethods.Int16sToSingle(shortValues[offset + 6], shortValues[offset + 7]);
                        inputChannel.HighPassFilter = shortValues[offset + 8];
                        inputChannel.LowPassFilter = shortValues[offset + 9];
                        inputChannel.ReadFrequency = shortValues[offset + 10];
                        inputChannel.ReadPoints = shortValues[offset + 11];
                        inputChannel.Averages = shortValues[offset + 12];
                        inputChannel.SaveDataIfChangedPercent = shortValues[offset + 13];
                        inputChannel.SaveDataIfChangedValue = shortValues[offset + 14];
                        inputChannel.JunkValue_1 = shortValues[offset + 15];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractInputChannelFromInputData(Int16[], int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractInputChannelFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractInputChannelFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannel;
        }

        public static void ConvertInputChannelToShorts(Main_ConfigComponent_InputChannel inputChannel, ref Int16[] shortValues)
        {
            try
            {
                Int16[] twoShorts;
                int offset;
                if (inputChannel != null)
                {
                    if (shortValues != null)
                    {
                        offset = 300 + (inputChannel.InputChannelNumber * 200);

                        if (shortValues.Length >= (offset + 16))
                        {
                            shortValues[offset] = (Int16)inputChannel.InputChannelNumber;
                            shortValues[offset + 1] = (Int16)inputChannel.ReadStatus;
                            shortValues[offset + 2] = (Int16)inputChannel.SensorType;
                            shortValues[offset + 3] = (Int16)inputChannel.Unit;
                            shortValues[offset + 4] = (Int16)inputChannel.SensorID;
                            shortValues[offset + 5] = (Int16)inputChannel.SensorNumber;

                            twoShorts = ConversionMethods.SingleToInt16s((Single)inputChannel.Sensitivity);
                            shortValues[offset + 6] = twoShorts[0];
                            shortValues[offset + 7] = twoShorts[1];

                            shortValues[offset + 8] = (Int16)inputChannel.HighPassFilter;
                            shortValues[offset + 9] = (Int16)inputChannel.LowPassFilter;
                            shortValues[offset + 10] = (Int16)inputChannel.ReadFrequency;
                            shortValues[offset + 11] = (Int16)inputChannel.ReadPoints;
                            shortValues[offset + 12] = (Int16)inputChannel.Averages;
                            shortValues[offset + 13] = (Int16)inputChannel.SaveDataIfChangedPercent;
                            shortValues[offset + 14] = (Int16)inputChannel.SaveDataIfChangedValue;
                            shortValues[offset + 15] = (Int16)inputChannel.JunkValue_1;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertInputChannelToShorts(Main_ConfigComponent_InputChannel, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertInputChannelToShorts(Main_ConfigComponent_InputChannel, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertInputChannelToShorts(Main_ConfigComponent_InputChannel, ref Int16[])\nInput Main_ConfigComponent_InputChannel was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertInputChannelToShorts(Main_ConfigComponent_InputChannel, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_InputChannel> InitializeEmptyInputChannelList()
        {
            List<Main_ConfigComponent_InputChannel> allInputChannel = new List<Main_ConfigComponent_InputChannel>();
            try
            {
                for (int i = 0; i < 25; i++)
                {
                    allInputChannel.Add(InitializeEmptyInputChannel(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllInputChannelFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannel;
        }

        public static List<Main_ConfigComponent_InputChannel> ExtractAllInputChannelFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_InputChannel> allInputChannel = null;
            try
            {
                Main_ConfigComponent_InputChannel inputChannel;
                if (shortValues != null)
                {
                    allInputChannel = new List<Main_ConfigComponent_InputChannel>();

                    for (int i = 0; i < 25; i++)
                    {
                        inputChannel = ExtractInputChannelFromInputData(shortValues, i);
                        allInputChannel.Add(inputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllInputChannelFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllInputChannelFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannel;
        }

        public static void ConvertAllInputChannelToShorts(List<Main_ConfigComponent_InputChannel> allInputChannel, ref Int16[] shortValues)
        {
            try
            {
                if (allInputChannel != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_InputChannel entry in allInputChannel)
                        {
                            ConvertInputChannelToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelToShorts(List<Main_ConfigComponent_InputChannel>, Int16[])\nInput List<Main_ConfigComponent_InputChannel> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelToShorts(List<Main_ConfigComponent_InputChannel>, Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllInputChannelToShorts(List<Main_ConfigComponent_InputChannel>, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_InputChannelEffluvia InitializeEmptyInputChannelEffluvia(int inputChannelNumber)
        {
            Main_ConfigComponent_InputChannelEffluvia inputChannelEffluvia = new Main_ConfigComponent_InputChannelEffluvia();
            try
            {
                inputChannelEffluvia.InputChannelNumber = inputChannelNumber;
                inputChannelEffluvia.Value_1 = 0;
                inputChannelEffluvia.Value_2 = 0;
                inputChannelEffluvia.Value_3 = 0;
                inputChannelEffluvia.Value_4 = 0;
                inputChannelEffluvia.Value_5 = 0;
                inputChannelEffluvia.Value_6 = 0;
                inputChannelEffluvia.Value_7 = 0;
                inputChannelEffluvia.Value_8 = 0;
                inputChannelEffluvia.Value_9 = 0;
                inputChannelEffluvia.Value_10 = 0;
                inputChannelEffluvia.Value_11 = 0;
                inputChannelEffluvia.Value_12 = 0;
                inputChannelEffluvia.Value_13 = 0;
                inputChannelEffluvia.Value_14 = 0;
                inputChannelEffluvia.Value_15 = 0;
                inputChannelEffluvia.Value_16 = 0;
                inputChannelEffluvia.Value_17 = 0;
                inputChannelEffluvia.Value_18 = 0;
                inputChannelEffluvia.Value_19 = 0;
                inputChannelEffluvia.Value_20 = 0;
                inputChannelEffluvia.Value_21 = 0;
                inputChannelEffluvia.Value_22 = 0;
                inputChannelEffluvia.Value_23 = 0;
                inputChannelEffluvia.Value_24 = 0;
                inputChannelEffluvia.Value_25 = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractInputChannelEffluviaFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannelEffluvia;
        }

        public static Main_ConfigComponent_InputChannelEffluvia ExtractInputChannelEffluviaFromInputData(Int16[] shortValues, int inputChannelNumber)
        {
            Main_ConfigComponent_InputChannelEffluvia inputChannelEffluvia = null;
            try
            {
                int offset; 
                if (shortValues != null)
                {
                    offset = 400 + (inputChannelNumber * 200);
                    if (shortValues.Length >= (offset + 95))
                    {
                        inputChannelEffluvia = new Main_ConfigComponent_InputChannelEffluvia();

                        inputChannelEffluvia.InputChannelNumber = inputChannelNumber;
                        inputChannelEffluvia.Value_1 = shortValues[offset + 3];
                        inputChannelEffluvia.Value_2 = shortValues[offset + 4];
                        inputChannelEffluvia.Value_3 = shortValues[offset + 5];
                        inputChannelEffluvia.Value_4 = shortValues[offset + 16];
                        inputChannelEffluvia.Value_5 = shortValues[offset + 21];
                        inputChannelEffluvia.Value_6 = shortValues[offset + 22];
                        inputChannelEffluvia.Value_7 = shortValues[offset + 23];
                        inputChannelEffluvia.Value_8 = shortValues[offset + 24];
                        inputChannelEffluvia.Value_9 = shortValues[offset + 25];
                        inputChannelEffluvia.Value_10 = shortValues[offset + 26];
                        inputChannelEffluvia.Value_11 = shortValues[offset + 27];
                        inputChannelEffluvia.Value_12 = shortValues[offset + 29];
                        inputChannelEffluvia.Value_13 = shortValues[offset + 32];
                        inputChannelEffluvia.Value_14 = shortValues[offset + 33];
                        inputChannelEffluvia.Value_15 = shortValues[offset + 37];
                        inputChannelEffluvia.Value_16 = shortValues[offset + 38];
                        inputChannelEffluvia.Value_17 = shortValues[offset + 42];
                        inputChannelEffluvia.Value_18 = shortValues[offset + 49];
                        inputChannelEffluvia.Value_19 = shortValues[offset + 50];
                        inputChannelEffluvia.Value_20 = shortValues[offset + 51];
                        inputChannelEffluvia.Value_21 = shortValues[offset + 55];
                        inputChannelEffluvia.Value_22 = shortValues[offset + 62];
                        inputChannelEffluvia.Value_23 = shortValues[offset + 68];
                        inputChannelEffluvia.Value_24 = shortValues[offset + 81];
                        inputChannelEffluvia.Value_25 = shortValues[offset + 94];
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractInputChannelEffluviaFromInputData(Int16[], int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractInputChannelEffluviaFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractInputChannelEffluviaFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannelEffluvia;
        }

        public static void ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia inputChannelEffluvia, ref Int16[] shortValues)
        {
            try
            {
                int offset; 
                if (inputChannelEffluvia != null)
                {
                    if (shortValues != null)
                    {
                        offset = 400 + (inputChannelEffluvia.InputChannelNumber * 200);

                        if (shortValues.Length >= (offset + 95))
                        {
                            shortValues[offset + 3] = (Int16)inputChannelEffluvia.Value_1;
                            shortValues[offset + 4] = (Int16)inputChannelEffluvia.Value_2;
                            shortValues[offset + 5] = (Int16)inputChannelEffluvia.Value_3;
                            shortValues[offset + 16] = (Int16)inputChannelEffluvia.Value_4;
                            shortValues[offset + 21] = (Int16)inputChannelEffluvia.Value_5;
                            shortValues[offset + 22] = (Int16)inputChannelEffluvia.Value_6;
                            shortValues[offset + 23] = (Int16)inputChannelEffluvia.Value_7;
                            shortValues[offset + 24] = (Int16)inputChannelEffluvia.Value_8;
                            shortValues[offset + 25] = (Int16)inputChannelEffluvia.Value_9;
                            shortValues[offset + 26] = (Int16)inputChannelEffluvia.Value_10;
                            shortValues[offset + 27] = (Int16)inputChannelEffluvia.Value_11;
                            shortValues[offset + 29] = (Int16)inputChannelEffluvia.Value_12;
                            shortValues[offset + 32] = (Int16)inputChannelEffluvia.Value_13;
                            shortValues[offset + 33] = (Int16)inputChannelEffluvia.Value_14;
                            shortValues[offset + 37] = (Int16)inputChannelEffluvia.Value_15;
                            shortValues[offset + 38] = (Int16)inputChannelEffluvia.Value_16;
                            shortValues[offset + 42] = (Int16)inputChannelEffluvia.Value_17;
                            shortValues[offset + 49] = (Int16)inputChannelEffluvia.Value_18;
                            shortValues[offset + 50] = (Int16)inputChannelEffluvia.Value_19;
                            shortValues[offset + 51] = (Int16)inputChannelEffluvia.Value_20;
                            shortValues[offset + 55] = (Int16)inputChannelEffluvia.Value_21;
                            shortValues[offset + 62] = (Int16)inputChannelEffluvia.Value_22;
                            shortValues[offset + 68] = (Int16)inputChannelEffluvia.Value_23;
                            shortValues[offset + 81] = (Int16)inputChannelEffluvia.Value_24;
                            shortValues[offset + 94] = (Int16)inputChannelEffluvia.Value_25;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nInput Main_ConfigComponent_InputChannelEffluvia was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertInputChannelEffluviaToShorts(Main_ConfigComponent_InputChannelEffluvia, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_InputChannelEffluvia> InitializeEmptyInputChannelEffluviaList()
        {
            List<Main_ConfigComponent_InputChannelEffluvia> allInputChannel = new List<Main_ConfigComponent_InputChannelEffluvia>();
            try
            {
                for (int i = 0; i < 25; i++)
                {
                    allInputChannel.Add(InitializeEmptyInputChannelEffluvia(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeEmptyInputChannelEffluviaList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannel;
        }

        public static List<Main_ConfigComponent_InputChannelEffluvia> ExtractAllInputChannelEffluviaFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_InputChannelEffluvia> allInputChannel = null;
            try
            {
                Main_ConfigComponent_InputChannelEffluvia inputChannel;
                if (shortValues != null)
                {
                    allInputChannel = new List<Main_ConfigComponent_InputChannelEffluvia>();

                    for (int i = 0; i < 25; i++)
                    {
                        inputChannel = ExtractInputChannelEffluviaFromInputData(shortValues, i);
                        allInputChannel.Add(inputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllInputChannelFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllInputChannelFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannel;
        }

        public static void ConvertAllInputChannelEffluviaToShorts(List<Main_ConfigComponent_InputChannelEffluvia> allInputChannel, ref Int16[] shortValues)
        {
            try
            {
                if (allInputChannel != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_InputChannelEffluvia entry in allInputChannel)
                        {
                            ConvertInputChannelEffluviaToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelEffluviaToShorts(List<Main_ConfigComponent_InputChannelEffluvia>, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelEffluviaToShorts(List<Main_ConfigComponent_InputChannelEffluvia>, ref Int16[])\nInput List<Main_ConfigComponent_InputChannelEffluvia> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllInputChannelEffluviaToShorts(List<Main_ConfigComponent_InputChannelEffluvia>, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_InputChannelThreshold InitializeEmptyInputChannelThreshold(int inputChannelNumber, int thresholdNumber)
        {
            Main_ConfigComponent_InputChannelThreshold inputChannelThreshold = new Main_ConfigComponent_InputChannelThreshold();
            try
            {
                inputChannelThreshold.InputChannelNumber = inputChannelNumber;
                inputChannelThreshold.ThresholdNumber = thresholdNumber;
                inputChannelThreshold.ThresholdEnable = 0;
                inputChannelThreshold.ThresholdStatus = 0;
                inputChannelThreshold.RelayMode = 0;
                inputChannelThreshold.Calculation = 0;
                inputChannelThreshold.WorkingBy = 0;
                inputChannelThreshold.LowFrequencyOfSignal = 0;
                inputChannelThreshold.HighFrequencyOfSignal = 0;
                inputChannelThreshold.RelayStatusOnDelay = 0;
                inputChannelThreshold.RelayStatusOffDelay = 0;
                inputChannelThreshold.ThresholdValue = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractInputChannelThresholdsFromInputData(Int16[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannelThreshold;
        }

        public static Main_ConfigComponent_InputChannelThreshold ExtractInputChannelThresholdsFromInputData(Int16[] shortValues, int inputChannelNumber, int thresholdNumber)
        {
            Main_ConfigComponent_InputChannelThreshold inputChannelThreshold = null;
            try
            {
                int offset;
                if (shortValues != null)
                {
                    offset = 300 + (inputChannelNumber * 200) + 25 + (thresholdNumber * 13);
                    if (shortValues.Length >= (offset + 12))
                    {
                        inputChannelThreshold = new Main_ConfigComponent_InputChannelThreshold();

                        inputChannelThreshold.InputChannelNumber = inputChannelNumber;
                        inputChannelThreshold.ThresholdNumber = thresholdNumber;
                        inputChannelThreshold.ThresholdEnable = shortValues[offset + 1];
                        inputChannelThreshold.ThresholdStatus = shortValues[offset + 2];
                        inputChannelThreshold.RelayMode = shortValues[offset + 3];
                        inputChannelThreshold.Calculation = shortValues[offset + 4];
                        inputChannelThreshold.WorkingBy = shortValues[offset + 5];
                        inputChannelThreshold.LowFrequencyOfSignal = shortValues[offset + 6];
                        inputChannelThreshold.HighFrequencyOfSignal = shortValues[offset + 7];
                        inputChannelThreshold.RelayStatusOnDelay = shortValues[offset + 8];
                        inputChannelThreshold.RelayStatusOffDelay = shortValues[offset + 9];
                        inputChannelThreshold.ThresholdValue = ConversionMethods.Int16sToSingle(shortValues[offset + 10], shortValues[offset + 11]);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractInputChannelThresholdsFromInputData(Int16[], int, int)\nInput Int16[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractInputChannelThresholdsFromInputData(Int16[], int, int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractInputChannelThresholdsFromInputData(Int16[], int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return inputChannelThreshold;
        }

        public static void ConvertInputChannelThresholdToShorts(Main_ConfigComponent_InputChannelThreshold inputChannelThreshold, ref Int16[] shortValues)
        {
            try
            {
                Int16[] twoShorts;
                int offset;
                if (inputChannelThreshold != null)
                {
                    offset = 300 + (inputChannelThreshold.InputChannelNumber * 200) + 25 + (inputChannelThreshold.ThresholdNumber * 13);
                    if (shortValues != null)
                    {
                        if (shortValues.Length >= (offset + 12))
                        {
                            shortValues[offset] = (Int16)inputChannelThreshold.ThresholdNumber;
                            shortValues[offset + 1] = (Int16)inputChannelThreshold.ThresholdEnable;
                            shortValues[offset + 2] = (Int16)inputChannelThreshold.ThresholdStatus;
                            shortValues[offset + 3] = (Int16)inputChannelThreshold.RelayMode;
                            shortValues[offset + 4] = (Int16)inputChannelThreshold.Calculation;
                            shortValues[offset + 5] = (Int16)inputChannelThreshold.WorkingBy;
                            shortValues[offset + 6] = (Int16)inputChannelThreshold.LowFrequencyOfSignal;
                            shortValues[offset + 7] = (Int16)inputChannelThreshold.HighFrequencyOfSignal;
                            shortValues[offset + 8] = (Int16)inputChannelThreshold.RelayStatusOnDelay;
                            shortValues[offset + 9] = (Int16)inputChannelThreshold.RelayStatusOffDelay;

                            twoShorts = ConversionMethods.SingleToInt16s((Single)inputChannelThreshold.ThresholdValue);

                            shortValues[offset + 10] = twoShorts[0];
                            shortValues[offset + 11] = twoShorts[1];
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertInputChannelThresholdToShorts(Main_ConfigComponent_InputChannelThreshold, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertInputChannelThresholdToShorts(Main_ConfigComponent_InputChannelThreshold, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertInputChannelThresholdToShorts(Main_ConfigComponent_InputChannelThreshold, ref Int16[])\nInput Main_ConfigComponent_InputChannelThreshold was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertInputChannelThresholdToShorts(Main_ConfigComponent_InputChannelThreshold, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_InputChannelThreshold> InitializeEmptyInputChannelThresholdList()
        {
            List<Main_ConfigComponent_InputChannelThreshold> allInputChannelThresholds = new List<Main_ConfigComponent_InputChannelThreshold>();
            try
            {
                int i, j;

                for (i = 0; i < 25; i++)
                {
                    for (j = 0; j < 6; j++)
                    {
                        allInputChannelThresholds.Add(InitializeEmptyInputChannelThreshold(i, j));
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllInputChannelThresholdsFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannelThresholds;
        }

        public static List<Main_ConfigComponent_InputChannelThreshold> ExtractAllInputChannelThresholdsFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_InputChannelThreshold> allInputChannelThresholds = null;
            try
            {
                Main_ConfigComponent_InputChannelThreshold inputChannelThreshold;
                int i, j;

                if (shortValues != null)
                {
                    allInputChannelThresholds = new List<Main_ConfigComponent_InputChannelThreshold>();

                    for (i = 0; i < 25; i++)
                    {
                        for (j = 0; j < 6; j++)
                        {
                            inputChannelThreshold = ExtractInputChannelThresholdsFromInputData(shortValues, i, j);
                            allInputChannelThresholds.Add(inputChannelThreshold);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllInputChannelThresholdsFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllInputChannelThresholdsFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allInputChannelThresholds;
        }

        public static void ConvertAllInputChannelThresholdsToShorts(List<Main_ConfigComponent_InputChannelThreshold> allInputChannelThresholds, ref Int16[] shortValues)
        {
            try
            {
                if (allInputChannelThresholds != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_InputChannelThreshold entry in allInputChannelThresholds)
                        {
                            ConvertInputChannelThresholdToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelThresholdsToShorts(List<Main_ConfigComponent_InputChannelThreshold>, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllInputChannelThresholdsToShorts(List<Main_ConfigComponent_InputChannelThreshold>, ref Int16[])\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllInputChannelThresholdsToShorts(List<Main_ConfigComponent_InputChannelThreshold>, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_CalibrationData InitializeEmptyCalibrationData(int inputChannelNumber)
        {
            Main_ConfigComponent_CalibrationData calibrationData = new Main_ConfigComponent_CalibrationData();
            try
            {
                calibrationData.ChannelNumber = inputChannelNumber;
                calibrationData.ChannelNoise = 0;
                calibrationData.ZeroLine = 0;
                calibrationData.Multiplier = 0;
                calibrationData.Offset = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractCalibrationDataFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationData;
        }

        public static Main_ConfigComponent_CalibrationData ExtractCalibrationDataFromInputData(Int16[] shortValues, int inputChannelNumber)
        {
            Main_ConfigComponent_CalibrationData calibrationData = null;
            try
            {
                int offset;
                if (shortValues != null)
                {
                    offset = 5300 + (inputChannelNumber * 6);
                    if (shortValues.Length >= (offset + 6))
                    {
                        calibrationData = new Main_ConfigComponent_CalibrationData();

                        calibrationData.ChannelNumber = inputChannelNumber;
                        calibrationData.ChannelNoise = shortValues[offset];
                        calibrationData.ZeroLine = shortValues[offset + 1];
                        calibrationData.Multiplier = ConversionMethods.Int16sToSingle(shortValues[offset + 2], shortValues[offset + 3]);
                        calibrationData.Offset = ConversionMethods.Int16sToSingle(shortValues[offset + 4], shortValues[offset + 5]);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ExtractCalibrationDataFromInputData(Int16[], int)\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractCalibrationDataFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }

            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractCalibrationDataFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return calibrationData;
        }

        public static void ConvertCalibrationDataToShorts(Main_ConfigComponent_CalibrationData calibrationData, ref Int16[] shortValues)
        {
            try
            {
                Int16[] twoShorts;
                int offset;
                if (calibrationData != null)
                {
                    if (shortValues != null)
                    {
                        offset = 5300 + (calibrationData.ChannelNumber * 6);
                        if (shortValues.Length >= (offset + 6))
                        {
                            shortValues[offset] = (Int16)calibrationData.ChannelNoise;
                            shortValues[offset + 1] = (Int16)calibrationData.ZeroLine;

                            twoShorts = ConversionMethods.SingleToInt16s((Single)calibrationData.Multiplier);
                            shortValues[offset + 2] = twoShorts[0];
                            shortValues[offset + 3] = twoShorts[1];

                            twoShorts = ConversionMethods.SingleToInt16s((Single)calibrationData.Offset);
                            shortValues[offset + 4] = twoShorts[0];
                            shortValues[offset + 5] = twoShorts[1];
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertCalibrationDataToShorts(Main_ConfigComponent_CalibrationData, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertCalibrationDataToShorts(Main_ConfigComponent_CalibrationData, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertCalibrationDataToShorts(Main_ConfigComponent_CalibrationData, ref Int16[])\nInput Main_ConfigComponent_CalibrationData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertCalibrationDataToShorts(Main_ConfigComponent_CalibrationData, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_CalibrationData> InitializeEmptyCalibrationDataList()
        {
            List<Main_ConfigComponent_CalibrationData> allCalibrationData = new List<Main_ConfigComponent_CalibrationData>();
            try
            {
                for (int i = 0; i < 25; i++)
                {
                    allCalibrationData.Add(InitializeEmptyCalibrationData(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InitializeEmptyCalibrationDataList()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allCalibrationData;
        }

        public static List<Main_ConfigComponent_CalibrationData> ExtractAllCalibrationDataFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_CalibrationData> allCalibrationData = null;
            try
            {
                Main_ConfigComponent_CalibrationData calibrationData;
                if (shortValues != null)
                {
                    allCalibrationData = new List<Main_ConfigComponent_CalibrationData>();

                    for (int i = 0; i < 25; i++)
                    {
                        calibrationData = ExtractCalibrationDataFromInputData(shortValues, i);
                        allCalibrationData.Add(calibrationData);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllCalibrationDataFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllCalibrationDataFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allCalibrationData;
        }

        public static void ConvertAllCalibrationDataToShorts(List<Main_ConfigComponent_CalibrationData> allCalibrationData, ref Int16[] shortValues)
        {
            try
            {
                if (allCalibrationData != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_CalibrationData entry in allCalibrationData)
                        {
                            ConvertCalibrationDataToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Exception thrown in Main_Configuration.ExtractAllCalibrationDataFromInputData(List<Main_ConfigComponent_CalibrationData>, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Exception thrown in Main_Configuration.ExtractAllCalibrationDataFromInputData(List<Main_ConfigComponent_CalibrationData>, ref Int16[])\nInput List<Main_ConfigComponent_CalibrationData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllCalibrationDataFromInputData(List<Main_ConfigComponent_CalibrationData>, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_ConfigComponent_TransferSetup InitializeEmptyTransferSetup(int transferSetupNumber)
        {
            Main_ConfigComponent_TransferSetup transferSetup = new Main_ConfigComponent_TransferSetup();
            try
            {
                transferSetup.TransferSetupNumber = transferSetupNumber;
                transferSetup.TransferNumber = transferSetupNumber;
                transferSetup.Working = 0;
                transferSetup.SourceModuleNumber = 0;
                transferSetup.SourceRegisterNumber = 0;
                transferSetup.DestinationModuleNumber = 0;
                transferSetup.DestinationRegisterNumber = 0;
                transferSetup.Multiplier = 0;
                transferSetup.Reserved = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractTransferSetupFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transferSetup;
        }

        public static Main_ConfigComponent_TransferSetup ExtractTransferSetupFromInputData(Int16[] shortValues, int transferSetupNumber)
        {
            Main_ConfigComponent_TransferSetup transferSetup = null;
            try
            {
                int offset;

                if (shortValues != null)
                {
                    offset = 5600 + (transferSetupNumber * 9);
                    if (shortValues.Length >= (offset + 9))
                    {
                        transferSetup = new Main_ConfigComponent_TransferSetup();

                        transferSetup.TransferSetupNumber = transferSetupNumber;
                        transferSetup.TransferNumber = shortValues[offset];
                        transferSetup.Working = shortValues[offset + 1];
                        transferSetup.SourceModuleNumber = shortValues[offset + 2];
                        transferSetup.SourceRegisterNumber = shortValues[offset + 3];
                        transferSetup.DestinationModuleNumber = shortValues[offset + 4];
                        transferSetup.DestinationRegisterNumber = shortValues[offset + 5];
                        transferSetup.Multiplier = ConversionMethods.Int16sToSingle(shortValues[offset + 6], shortValues[offset + 7]);
                        transferSetup.Reserved = shortValues[offset + 8];
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractTransferSetupFromInputData(Int16[], int)\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractTransferSetupFromInputData(Int16[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transferSetup;
        }

        private static void ConvertTransferSetupToShorts(Main_ConfigComponent_TransferSetup transferSetup, ref Int16[] shortValues)
        {
            try
            {
                Int16[] twoShorts;
                int offset;

                if (transferSetup != null)
                {
                    if (shortValues != null)
                    {
                        offset = 5600 + (transferSetup.TransferSetupNumber * 9);

                        if (shortValues.Length >= (offset + 9))
                        {
                            shortValues[offset] = (Int16)transferSetup.TransferNumber;
                            shortValues[offset + 1] = (Int16)transferSetup.Working;
                            shortValues[offset + 2] = (Int16)transferSetup.SourceModuleNumber;
                            shortValues[offset + 3] = (Int16)transferSetup.SourceRegisterNumber;
                            shortValues[offset + 4] = (Int16)transferSetup.DestinationModuleNumber;
                            shortValues[offset + 5] = (Int16)transferSetup.DestinationRegisterNumber;

                            twoShorts = ConversionMethods.SingleToInt16s((Single)transferSetup.Multiplier);
                            shortValues[offset + 6] = twoShorts[0];
                            shortValues[offset + 7] = twoShorts[1];

                            shortValues[offset + 8] = (Int16)transferSetup.Reserved;
                        }
                        else
                        {
                            string errorMessage = "Error in Main_Configuration.ConvertTransferSetupToShorts(Main_ConfigComponent_TransferSetup, ref Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertTransferSetupToShorts(Main_ConfigComponent_TransferSetup, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertTransferSetupToShorts(Main_ConfigComponent_TransferSetup, ref Int16[])\nInput Main_ConfigComponent_TransferSetup was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertTransferSetupToShorts(Main_ConfigComponent_TransferSetup, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public List<Main_ConfigComponent_TransferSetup> InitializeEmptyTransferSetupList()
        {
            List<Main_ConfigComponent_TransferSetup> allTransferSetup = new List<Main_ConfigComponent_TransferSetup>();
            try
            {
                for (int i = 0; i < 32; i++)
                {
                    allTransferSetup.Add(InitializeEmptyTransferSetup(i));
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllTransferSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allTransferSetup;
        }

        public static List<Main_ConfigComponent_TransferSetup> ExtractAllTransferSetupFromInputData(Int16[] shortValues)
        {
            List<Main_ConfigComponent_TransferSetup> allTransferSetup = null;
            try
            {
                Main_ConfigComponent_TransferSetup transferSetup;

                if (shortValues != null)
                {
                    allTransferSetup = new List<Main_ConfigComponent_TransferSetup>();
                    for (int i = 0; i < 32; i++)
                    {
                        transferSetup = ExtractTransferSetupFromInputData(shortValues, i);
                        allTransferSetup.Add(transferSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ExtractAllTransferSetupFromInputData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllTransferSetupFromInputData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return allTransferSetup;
        }

        private static void ConvertAllTransferSetupToShorts(List<Main_ConfigComponent_TransferSetup> allTransferSetup, ref Int16[] shortValues)
        {
            try
            {
                if (allTransferSetup != null)
                {
                    if (shortValues != null)
                    {
                        foreach (Main_ConfigComponent_TransferSetup entry in allTransferSetup)
                        {
                            ConvertTransferSetupToShorts(entry, ref shortValues);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ConvertAllTransferSetupToShorts(List<Main_ConfigComponent_TransferSetup>, ref Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ConvertAllTransferSetupToShorts(List<Main_ConfigComponent_TransferSetup>, ref Int16[])\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllTransferSetupToShorts(List<Main_ConfigComponent_TransferSetup>, ref Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        public static Main_ConfigComponent_ChannelCalibration ExtractChannelCalibrationFromInputData(Int16[] shortValues, int channelNumber)
//        {
//            Main_ConfigComponent_ChannelCalibration channelCalibration = null;
//            try
//            {
//                int offset;

//                if (shortValues != null)
//                {
//                    offset = 5300 + channelNumber * 6;
//                    if (shortValues.Length >= (offset + 6))
//                    {
//                        channelCalibration = new Main_ConfigComponent_ChannelCalibration();

//                        channelCalibration.ChannelNumber = channelNumber;
//                        channelCalibration.Noise = shortValues[offset];
//                        channelCalibration.ZeroLine = shortValues[offset + 1];
//                        channelCalibration.CalibrationCoefficient1 = ConversionMethods.Int16sToSingle(shortValues[offset + 2], shortValues[offset + 3]);
//                        channelCalibration.CalibrationCoefficient2 = ConversionMethods.Int16sToSingle(shortValues[offset + 4], shortValues[offset + 5]);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ExtractChannelCalibrationFromInputData(Int16[], int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return channelCalibration;
//        }

//        public static void ConvertChannelCalibrationToShorts(Main_ConfigComponent_ChannelCalibration channelCalibration, ref Int16[] shortValues)
//        {
//            try
//            {
//                int offset;
//                Int16[] twoShorts;
//                if (channelCalibration != null)
//                {
//                    if (shortValues != null)
//                    {
//                        offset = 5300 + channelCalibration.ChannelNumber * 6;
//                        shortValues[offset] = (Int16)channelCalibration.Noise;
//                        shortValues[offset+1] = (Int16)channelCalibration.ZeroLine;

//                        twoShorts = ConversionMethods.SingleToInt16s((Single)channelCalibration.CalibrationCoefficient1);
//                        shortValues[offset + 2] = twoShorts[0];
//                        shortValues[offset + 3] = twoShorts[1];

//                        twoShorts = ConversionMethods.SingleToInt16s((Single)channelCalibration.CalibrationCoefficient2);
//                        shortValues[offset + 4] = twoShorts[0];
//                        shortValues[offset + 5] = twoShorts[1];
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ConvertChannelCalibrationToShorts(Main_ConfigComponent_ChannelCalibration, Int16[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        public static List<Main_ConfigComponent_ChannelCalibration> ExtractAllChannelCalibrationFromInputData(Int16[] shortValues)
//        {
//            List<Main_ConfigComponent_ChannelCalibration> allChannelCalibration = null;
//            try
//            {
//                Main_ConfigComponent_ChannelCalibration channelCalibration;

//                if (shortValues != null)
//                {
//                    allChannelCalibration = new List<Main_ConfigComponent_ChannelCalibration>();
//                    for (int i = 0; i < 8; i++)
//                    {
//                        channelCalibration = ExtractChannelCalibrationFromInputData(shortValues, i);
//                        allChannelCalibration.Add(channelCalibration);
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_Configuration.ExtractAllChannelCalibrationFromInputData(Int16[])\nInput Int16[] was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ExtractAllChannelCalibrationFromInputData(Int16[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return allChannelCalibration;
//        }

//        private void ConvertAllChannelCalibrationToShorts(List<Main_ConfigComponent_ChannelCalibration> allChannelCalibration, ref Int16[] shortValues)
//        {
//            try
//            {
//                if (allChannelCalibration != null)
//                {
//                    if (shortValues != null)
//                    {
//                        foreach (Main_ConfigComponent_ChannelCalibration entry in allChannelCalibration)
//                        {
//                            ConvertChannelCalibrationToShorts(entry, ref shortValues);
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in Main_Configuration.ConvertAllChannelCalibrationToShorts(List<Main_ConfigComponent_ChannelCalibration>, Int16[])\nInput Int16[] was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_Configuration.ConvertAllChannelCalibrationToShorts(List<Main_ConfigComponent_ChannelCalibration>, Int16[])\nInput List<Main_ConfigComponent_ChannelCalibration> was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ConvertAllChannelCalibrationToShorts(List<Main_ConfigComponent_ChannelCalibration>, Int16[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        #region Compare Configurations

        public bool ConfigurationIsTheSame(Main_Configuration externalConfiguration)
        {
            bool isTheSame = true;
            try
            {
                isTheSame = CalibrationDataListIsTheSame(externalConfiguration.calibrationDataConfigObjects);
                if (isTheSame)
                {
                    isTheSame = CommunicationSetupIsTheSame(externalConfiguration.communicationSetupConfigObject);
                }
                if (isTheSame)
                {
                    isTheSame = DeviceSetupIsTheSame(externalConfiguration.deviceSetupConfigObject);
                }
                if (isTheSame)
                {
                    isTheSame = FullStatusInformationListIsTheSame(externalConfiguration.fullStatusInformationConfigObjects);
                }
                if (isTheSame)
                {
                    isTheSame = GeneralIsTheSame(externalConfiguration.generalConfigObject);
                }
                if (isTheSame)
                {
                    isTheSame = InputChannelListIsTheSame(externalConfiguration.inputChannelConfigObjects);
                }
                //if (isTheSame)
                //{
                //    isTheSame = InputChannelEffluviaListIsTheSame(externalConfiguration.inputChannelEffluviaConfigObjects);
                //}
                if (isTheSame)
                {
                    isTheSame = InputChannelThresholdListIsTheSame(externalConfiguration.inputChannelThresholdConfigObjects);
                }
                if (isTheSame)
                {
                    isTheSame = ShortStatusInformationListIsTheSame(externalConfiguration.shortStatusInformationConfigObjects);
                }
                if (isTheSame)
                {
                    isTheSame = TransferSetupListIsTheSame(externalConfiguration.transferSetupConfigObjects);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ConfigurationsAreTheSame(Main_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool CalibrationDataListIsTheSame(List<Main_ConfigComponent_CalibrationData> externalCalibrationDataList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.calibrationDataConfigObjects != null)
                {
                    if (externalCalibrationDataList != null)
                    {
                        count = this.calibrationDataConfigObjects.Count;
                        if (count == externalCalibrationDataList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = CalibrationDataIsTheSame(this.calibrationDataConfigObjects[i], externalCalibrationDataList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.CalibrationDataListIsTheSame(List<Main_ConfigComponent_CalibrationData>)\nInput List<Main_ConfigComponent_CalibrationData> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CalibrationDataListIsTheSame(List<Main_ConfigComponent_CalibrationData>)\nthis.calibrationDataConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CalibrationDataListIsTheSame(List<Main_ConfigComponent_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool CalibrationDataIsTheSame(Main_ConfigComponent_CalibrationData internalCalibrationData, Main_ConfigComponent_CalibrationData externalCalibrationData)
        {
            bool isTheSame = true;
            try
            {
                if (internalCalibrationData.ChannelNumber != externalCalibrationData.ChannelNumber)
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalCalibrationData.ChannelNoise != externalCalibrationData.ChannelNoise))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalCalibrationData.ZeroLine != externalCalibrationData.ZeroLine))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalCalibrationData.Multiplier - externalCalibrationData.Multiplier) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalCalibrationData.Offset - externalCalibrationData.Offset) > epsilon))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CalibrationDataIsTheSame(Main_ConfigComponent_CalibrationData, Main_ConfigComponent_CalibrationData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

//        private bool ChannelCalibrationIsTheSame(Main_ConfigComponent_ChannelCalibration internalChannelCalibration, Main_ConfigComponent_ChannelCalibration externalChannelCalibration)
//        {
//            bool isTheSame = true;
//            try
//            {
//                if (internalChannelCalibration != null)
//                {
//                    if (externalChannelCalibration != null)
//                    {
//                        if (internalChannelCalibration.Noise != externalChannelCalibration.Noise)
//                        {
//                            isTheSame = false;
//                        }
//                        if (internalChannelCalibration.ZeroLine != externalChannelCalibration.ZeroLine)
//                        {
//                            isTheSame = false;
//                        }
//                        if (isTheSame && (Math.Abs(internalChannelCalibration.CalibrationCoefficient1 - externalChannelCalibration.CalibrationCoefficient1) > epsilon))
//                        {
//                            isTheSame = false;
//                        }
//                        if (isTheSame && (Math.Abs(internalChannelCalibration.CalibrationCoefficient2 - externalChannelCalibration.CalibrationCoefficient2) > epsilon))
//                        {
//                            isTheSame = false;
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in Main_Configuration.ChannelCalibrationIsTheSame(Main_ConfigComponent_ChannelCalibration, Main_ConfigComponent_ChannelCalibration)\nInput externalChannelCalibration was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_Configuration.ChannelCalibrationIsTheSame(Main_ConfigComponent_ChannelCalibration, Main_ConfigComponent_ChannelCalibration)\nInput internalChannelCalibration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ChannelCalibrationIsTheSame(Main_ConfigComponent_ChannelCalibration, Main_ConfigComponent_ChannelCalibration)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return isTheSame;
//        }

//        private bool ChannelCalibrationListIsTheSame(List<Main_ConfigComponent_ChannelCalibration> externalChannelCalibrationList)
//        {
//            bool isTheSame = true;
//            try
//            {
//                int count = 0;
//                if (this.channelCalibrationConfigObjects != null)
//                {
//                    if (externalChannelCalibrationList != null)
//                    {
//                        count = this.channelCalibrationConfigObjects.Count;
//                        if (count == externalChannelCalibrationList.Count)
//                        {
//                            for (int i = 0; i < count; i++)
//                            {
//                                isTheSame = ChannelCalibrationIsTheSame(this.channelCalibrationConfigObjects[i], externalChannelCalibrationList[i]);
//                                if (!isTheSame)
//                                {
//                                    break;
//                                }
//                            }
//                        }
//                        else
//                        {
//                            isTheSame = false;
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in Main_Configuration.ChannelCalibrationListIsTheSame(List<Main_ConfigComponent_ChannelCalibration>)\nInput List<Main_ConfigComponent_ChannelCalibration> was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_Configuration.ChannelCalibrationListIsTheSame(List<Main_ConfigComponent_ChannelCalibration>)\nthis.channelCalibrationConfigObjects was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_Configuration.ChannelCalibrationListIsTheSame(List<Main_ConfigComponent_ChannelCalibration>)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return isTheSame;
//        }


        private bool CommunicationSetupIsTheSame(Main_ConfigComponent_CommunicationSetup externalCommunicationSetup)
        {
            bool isTheSame = true;
            try
            {
                if (communicationSetupConfigObject.ModBusAddress != externalCommunicationSetup.ModBusAddress)
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.BaudRateRS485 != externalCommunicationSetup.BaudRateRS485))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.ModBusProtocol != externalCommunicationSetup.ModBusProtocol))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.BaudRateXport != externalCommunicationSetup.BaudRateXport))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.ModBusProtocolOverXport != externalCommunicationSetup.ModBusProtocolOverXport))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.IP1 != externalCommunicationSetup.IP1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.IP2 != externalCommunicationSetup.IP2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.IP3 != externalCommunicationSetup.IP3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.IP4 != externalCommunicationSetup.IP4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.SaveDataIntervalInSeconds != externalCommunicationSetup.SaveDataIntervalInSeconds))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.SaveDataIfChangedByPercent != externalCommunicationSetup.SaveDataIfChangedByPercent))
                {
                    isTheSame = false;
                }
                if (isTheSame && (communicationSetupConfigObject.SaveDataIfChangedStatus != externalCommunicationSetup.SaveDataIfChangedStatus))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CommunicationSetupIsTheSame(Main_ConfigComponent_CommunicationSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool DeviceSetupIsTheSame(Main_ConfigComponent_DeviceSetup externalDeviceSetup)
        {
            bool isTheSame = true;
            try
            {
                if (deviceSetupConfigObject.SetupStatus != externalDeviceSetup.SetupStatus)
                {
                    isTheSame = false;
                }
                if (isTheSame && (deviceSetupConfigObject.Monitoring != externalDeviceSetup.Monitoring))
                {
                    isTheSame = false;
                }
                if (isTheSame && (deviceSetupConfigObject.Frequency != externalDeviceSetup.Frequency))
                {
                    isTheSame = false;
                }
                if (isTheSame && (deviceSetupConfigObject.AlarmControl != externalDeviceSetup.AlarmControl))
                {
                    isTheSame = false;
                }
                if (isTheSame && (deviceSetupConfigObject.WarningControl != externalDeviceSetup.WarningControl))
                {
                    isTheSame = false;
                }
                if (isTheSame && (deviceSetupConfigObject.AllowDirectAccess != externalDeviceSetup.AllowDirectAccess))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.DeviceSetupIsTheSame(Main_ConfigComponent_DeviceSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool FullStatusInformationListIsTheSame(List<Main_ConfigComponent_FullStatusInformation> externalFullStatusInformationList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.fullStatusInformationConfigObjects != null)
                {
                    if (externalFullStatusInformationList != null)
                    {
                        count = this.fullStatusInformationConfigObjects.Count;
                        if (count == externalFullStatusInformationList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = FullStatusInformationIsTheSame(this.fullStatusInformationConfigObjects[i], externalFullStatusInformationList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.FullStatusInformationListIsTheSame(List<Main_ConfigComponent_FullStatusInformation>)\nInput List<Main_ConfigComponent_FullStatusInformation> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.FullStatusInformationListIsTheSame(List<Main_ConfigComponent_FullStatusInformation>)\nthis.fullStatusInformationConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.FullStatusInformationListIsTheSame(List<Main_ConfigComponent_FullStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool FullStatusInformationIsTheSame(Main_ConfigComponent_FullStatusInformation internalFullStatusInformation, Main_ConfigComponent_FullStatusInformation externalFullStatusInformation)
        {
            bool isTheSame = true;
            try
            {
                if (internalFullStatusInformation.ModuleNumber != externalFullStatusInformation.ModuleNumber)
                {
                    isTheSame = false;
                }
                //if (isTheSame && (internalFullStatusInformation.ModuleStatus != externalFullStatusInformation.ModuleStatus))
                //{
                //    isTheSame = false;
                //}
                if (isTheSame && (internalFullStatusInformation.ModuleType != externalFullStatusInformation.ModuleType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.ModBusAddress != externalFullStatusInformation.ModBusAddress))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.BaudRate != externalFullStatusInformation.BaudRate))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.ReadDatePeriodInSeconds != externalFullStatusInformation.ReadDatePeriodInSeconds))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.ConnectionType != externalFullStatusInformation.ConnectionType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.Reserved_0 != externalFullStatusInformation.Reserved_0))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalFullStatusInformation.Reserved_1 != externalFullStatusInformation.Reserved_1))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.FullStatusInformationIsTheSame(Main_ConfigComponent_FullStatusInformation, Main_ConfigComponent_FullStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool GeneralIsTheSame(Main_ConfigComponent_General externalGeneral)
        {
            bool isTheSame = true;
            try
            {
                string internalBoardName = GetBoardNameFromRegisterValues(this.generalConfigObject).Trim();
                string externalBoardName = GetBoardNameFromRegisterValues(externalGeneral).Trim();

                if (generalConfigObject.DeviceType != externalGeneral.DeviceType)
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(generalConfigObject.FirmwareVersion - externalGeneral.FirmwareVersion) > epsilon))
                {
                    isTheSame = false;
                }

                //if (isTheSame && (generalConfigObject.GlobalStatus != externalGeneral.GlobalStatus))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.DevicesError != externalGeneral.DevicesError))
                //{
                //    isTheSame = false;
                //}

                if (isTheSame && (internalBoardName.CompareTo(externalBoardName) != 0))
                {
                    isTheSame = false;
                }

                //if (isTheSame && (generalConfigObject.DeviceDateTime != externalGeneral.DeviceDateTime))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_0 != externalGeneral.UnitName_0))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_1 != externalGeneral.UnitName_1))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_2 != externalGeneral.UnitName_2))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_3 != externalGeneral.UnitName_3))
                //{
                //    isTheSame = false;
                //} 
                //if (isTheSame && (generalConfigObject.UnitName_4 != externalGeneral.UnitName_4))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_5 != externalGeneral.UnitName_5))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_6 != externalGeneral.UnitName_6))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_7 != externalGeneral.UnitName_7))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_8 != externalGeneral.UnitName_8))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_9 != externalGeneral.UnitName_9))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_10 != externalGeneral.UnitName_10))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_11 != externalGeneral.UnitName_11))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_12 != externalGeneral.UnitName_12))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_13 != externalGeneral.UnitName_13))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_14 != externalGeneral.UnitName_14))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_15 != externalGeneral.UnitName_15))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_16 != externalGeneral.UnitName_16))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_17 != externalGeneral.UnitName_17))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_18 != externalGeneral.UnitName_18))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (generalConfigObject.UnitName_19 != externalGeneral.UnitName_19))
                //{
                //    isTheSame = false;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.GeneralIsTheSame(Main_ConfigComponent_General)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        public static string GetBoardNameFromRegisterValues(Main_ConfigComponent_General inputGeneral)
        {
            string boardName = string.Empty;
            try
            {
                byte[] twoBytes;
                StringBuilder boardNameStringBuilder = new StringBuilder();

                if (inputGeneral != null)
                {
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_0);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_1);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_2);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_3);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_4);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_5);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_6);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_7);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_8);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_9);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_10);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_11);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_12);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_13);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_14);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_15);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_16);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_17);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_18);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);
                    twoBytes = ConversionMethods.Int16ToShortBytes((Int16)inputGeneral.UnitName_19);
                    boardNameStringBuilder.Append((char)twoBytes[0]);
                    boardNameStringBuilder.Append((char)twoBytes[1]);

                    boardName = boardNameStringBuilder.ToString();
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetBoardNameFromRegisterValues(Main_ConfigComponent_General)\nInput Main_Config_General was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetBoardNameFromRegisterValues(Main_ConfigComponent_General)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return boardName;
        }

        private static Main_ConfigComponent_General ConvertBoardNameToRegisterValues(string boardName)
        {
            Main_ConfigComponent_General generalConfigObject = new Main_ConfigComponent_General();
            try
            {
                byte[] twoBytes = new byte[2];
                StringBuilder boardNameStringBuilder = new StringBuilder(boardName.Trim());
                boardNameStringBuilder.Append("                                   ");

                boardName = boardNameStringBuilder.ToString();

                twoBytes[0] = (byte)((int)boardName[0]);
                twoBytes[1] = (byte)((int)boardName[1]);
                generalConfigObject.UnitName_0 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[2]);
                twoBytes[1] = (byte)((int)boardName[3]);
                generalConfigObject.UnitName_1 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[4]);
                twoBytes[1] = (byte)((int)boardName[5]);
                generalConfigObject.UnitName_2 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[6]);
                twoBytes[1] = (byte)((int)boardName[7]);
                generalConfigObject.UnitName_3 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[8]);
                twoBytes[1] = (byte)((int)boardName[9]);
                generalConfigObject.UnitName_4 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[10]);
                twoBytes[1] = (byte)((int)boardName[11]);
                generalConfigObject.UnitName_5 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[12]);
                twoBytes[1] = (byte)((int)boardName[13]);
                generalConfigObject.UnitName_6 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[14]);
                twoBytes[1] = (byte)((int)boardName[15]);
                generalConfigObject.UnitName_7 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[16]);
                twoBytes[1] = (byte)((int)boardName[17]);
                generalConfigObject.UnitName_8 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[18]);
                twoBytes[1] = (byte)((int)boardName[19]);
                generalConfigObject.UnitName_9 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[20]);
                twoBytes[1] = (byte)((int)boardName[21]);
                generalConfigObject.UnitName_10 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[22]);
                twoBytes[1] = (byte)((int)boardName[23]);
                generalConfigObject.UnitName_11 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[24]);
                twoBytes[1] = (byte)((int)boardName[25]);
                generalConfigObject.UnitName_12 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[26]);
                twoBytes[1] = (byte)((int)boardName[27]);
                generalConfigObject.UnitName_13 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[28]);
                twoBytes[1] = (byte)((int)boardName[29]);
                generalConfigObject.UnitName_14 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[30]);
                twoBytes[1] = (byte)((int)boardName[31]);
                generalConfigObject.UnitName_15 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[32]);
                twoBytes[1] = (byte)((int)boardName[33]);
                generalConfigObject.UnitName_16 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[34]);
                twoBytes[1] = (byte)((int)boardName[35]);
                generalConfigObject.UnitName_17 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[36]);
                twoBytes[1] = (byte)((int)boardName[37]);
                generalConfigObject.UnitName_18 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
                twoBytes[0] = (byte)((int)boardName[38]);
                twoBytes[1] = (byte)((int)boardName[39]);
                generalConfigObject.UnitName_19 = ConversionMethods.ShortBytesToInt16(twoBytes[0], twoBytes[1]);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ConvertBoardNameToRegisterValues(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return generalConfigObject;
        }

        private bool InputChannelListIsTheSame(List<Main_ConfigComponent_InputChannel> externalInputChannelList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.inputChannelConfigObjects != null)
                {
                    if (externalInputChannelList != null)
                    {
                        count = this.inputChannelConfigObjects.Count;
                        if (count == externalInputChannelList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = InputChannelIsTheSame(this.inputChannelConfigObjects[i], externalInputChannelList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.InputChannelListIsTheSame(List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.InputChannelListIsTheSame(List<Main_ConfigComponent_InputChannel>)\nthis.inputChannelConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelListIsTheSame(List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }


        private bool InputChannelIsTheSame(Main_ConfigComponent_InputChannel internalInputChannel, Main_ConfigComponent_InputChannel externalInputChannel)
        {
            bool isTheSame = true;
            try
            {
                if (internalInputChannel.InputChannelNumber != externalInputChannel.InputChannelNumber)
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.ReadStatus != externalInputChannel.ReadStatus))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.SensorType != externalInputChannel.SensorType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.Unit != externalInputChannel.Unit))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.SensorID != externalInputChannel.SensorID))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.SensorNumber != externalInputChannel.SensorNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalInputChannel.Sensitivity - externalInputChannel.Sensitivity) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.HighPassFilter != externalInputChannel.HighPassFilter))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.LowPassFilter != externalInputChannel.LowPassFilter))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.ReadFrequency != externalInputChannel.ReadFrequency))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.ReadPoints != externalInputChannel.ReadPoints))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.Averages != externalInputChannel.Averages))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.SaveDataIfChangedPercent != externalInputChannel.SaveDataIfChangedPercent))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.SaveDataIfChangedValue != externalInputChannel.SaveDataIfChangedValue))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannel.JunkValue_1 != externalInputChannel.JunkValue_1))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelIsTheSame(Main_ConfigComponent_InputChannel, Main_ConfigComponent_InputChannel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InputChannelEffluviaListIsTheSame(List<Main_ConfigComponent_InputChannelEffluvia> externalInputChannelEffluviaList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.inputChannelEffluviaConfigObjects != null)
                {
                    if (externalInputChannelEffluviaList != null)
                    {
                        count = this.inputChannelEffluviaConfigObjects.Count;
                        if (count == externalInputChannelEffluviaList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = InputChannelEffluviaIsTheSame(this.inputChannelEffluviaConfigObjects[i], externalInputChannelEffluviaList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.InputChannelEffluviaListIsTheSame(List<Main_ConfigComponent_InputChannelEffluvia>)\nInput List<Main_ConfigComponent_InputChannelEffluvia> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.InputChannelEffluviaListIsTheSame(List<Main_ConfigComponent_InputChannelEffluvia>)\nthis.inputChannelEffluviaConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelEffluviaListIsTheSame(List<Main_ConfigComponent_InputChannelEffluvia>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InputChannelEffluviaIsTheSame(Main_ConfigComponent_InputChannelEffluvia internalInputChannelEffluvia, Main_ConfigComponent_InputChannelEffluvia externalInputChannelEffluvia)
        {
            bool isTheSame = true;
            try
            {
                if (internalInputChannelEffluvia.InputChannelNumber != externalInputChannelEffluvia.InputChannelNumber)
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_1 != externalInputChannelEffluvia.Value_1))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_2 != externalInputChannelEffluvia.Value_2))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_3 != externalInputChannelEffluvia.Value_3))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_4 != externalInputChannelEffluvia.Value_4))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_5 != externalInputChannelEffluvia.Value_5))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_6 != externalInputChannelEffluvia.Value_6))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_7 != externalInputChannelEffluvia.Value_7))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_8 != externalInputChannelEffluvia.Value_8))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_9 != externalInputChannelEffluvia.Value_9))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_10 != externalInputChannelEffluvia.Value_10))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_11 != externalInputChannelEffluvia.Value_11))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_12 != externalInputChannelEffluvia.Value_12))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_13 != externalInputChannelEffluvia.Value_13))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_14 != externalInputChannelEffluvia.Value_14))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_15 != externalInputChannelEffluvia.Value_15))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_16 != externalInputChannelEffluvia.Value_16))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_17 != externalInputChannelEffluvia.Value_17))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_18 != externalInputChannelEffluvia.Value_18))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_19 != externalInputChannelEffluvia.Value_19))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_20 != externalInputChannelEffluvia.Value_20))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_21 != externalInputChannelEffluvia.Value_21))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_22 != externalInputChannelEffluvia.Value_22))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_23 != externalInputChannelEffluvia.Value_23))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_24 != externalInputChannelEffluvia.Value_24))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalInputChannelEffluvia.Value_25 != externalInputChannelEffluvia.Value_25))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelEffluviaIsTheSame(Main_ConfigComponent_InputChannelEffluvia, Main_ConfigComponent_InputChannelEffluvia)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InputChannelThresholdListIsTheSame(List<Main_ConfigComponent_InputChannelThreshold> externalInputChannelThresholdList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.inputChannelThresholdConfigObjects != null)
                {
                    if (externalInputChannelThresholdList != null)
                    {
                        count = this.inputChannelThresholdConfigObjects.Count;
                        if (count == externalInputChannelThresholdList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = InputChannelThresholdIsTheSame(this.inputChannelThresholdConfigObjects[i], externalInputChannelThresholdList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.InputChannelThresholdListIsTheSame(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.InputChannelThresholdListIsTheSame(List<Main_ConfigComponent_InputChannelThreshold>)\nthis.inputChannelThresholdConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelThresholdListIsTheSame(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool InputChannelThresholdIsTheSame(Main_ConfigComponent_InputChannelThreshold internalInputChannelThreshold, Main_ConfigComponent_InputChannelThreshold externalInputChannelThreshold)
        {
            bool isTheSame = true;
            try
            {
                //if (internalInputChannelThreshold.InputChannelNumber != externalInputChannelThreshold.InputChannelNumber)
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.ThresholdNumber != externalInputChannelThreshold.ThresholdNumber))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.ThresholdEnable != externalInputChannelThreshold.ThresholdEnable))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.ThresholdStatus != externalInputChannelThreshold.ThresholdStatus))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.ThresholdType != externalInputChannelThreshold.ThresholdType))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.RelayMode != externalInputChannelThreshold.RelayMode))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.Calculation != externalInputChannelThreshold.Calculation))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.WorkingBy != externalInputChannelThreshold.WorkingBy))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.LowFrequencyOfSignal != externalInputChannelThreshold.LowFrequencyOfSignal))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.HighFrequencyOfSignal != externalInputChannelThreshold.HighFrequencyOfSignal))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.RelayStatusOnDelay != externalInputChannelThreshold.RelayStatusOnDelay))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (internalInputChannelThreshold.RelayStatusOffDelay != externalInputChannelThreshold.RelayStatusOffDelay))
                //{
                //    isTheSame = false;
                //}
                //if (isTheSame && (Math.Abs(internalInputChannelThreshold.ThresholdValue - externalInputChannelThreshold.ThresholdValue) > epsilon))
                //{
                //    isTheSame = false;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.InputChannelThresholdIsTheSame(Main_ConfigComponent_InputChannelThreshold, Main_ConfigComponent_InputChannelThreshold)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool ShortStatusInformationListIsTheSame(List<Main_ConfigComponent_ShortStatusInformation> externalShortStatusInformationList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.shortStatusInformationConfigObjects != null)
                {
                    if (externalShortStatusInformationList != null)
                    {
                        count = this.shortStatusInformationConfigObjects.Count;
                        if (count == externalShortStatusInformationList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = ShortStatusInformationIsTheSame(this.shortStatusInformationConfigObjects[i], externalShortStatusInformationList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.ShortStatusInformationListIsTheSame(List<Main_ConfigComponent_ShortStatusInformation>)\nInput List<Main_ConfigComponent_ShortStatusInformation> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.ShortStatusInformationListIsTheSame(List<Main_ConfigComponent_ShortStatusInformation>)\nthis.shortStatusInformationConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ShortStatusInformationListIsTheSame(List<Main_ConfigComponent_ShortStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool ShortStatusInformationIsTheSame(Main_ConfigComponent_ShortStatusInformation internalShortStatusInformation, Main_ConfigComponent_ShortStatusInformation externalShortStatusInformation)
        {
            bool isTheSame = true;
            try
            {
                if (internalShortStatusInformation.ModuleNumber != externalShortStatusInformation.ModuleNumber)
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalShortStatusInformation.ModuleStatus != externalShortStatusInformation.ModuleStatus))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalShortStatusInformation.ModuleType != externalShortStatusInformation.ModuleType))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalShortStatusInformation.Reserved != externalShortStatusInformation.Reserved))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.ShortStatusInformationIsTheSame(Main_ConfigComponent_ShortStatusInformation, Main_ConfigComponent_ShortStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool TransferSetupListIsTheSame(List<Main_ConfigComponent_TransferSetup> externalTransferSetupList)
        {
            bool isTheSame = true;
            try
            {
                int count = 0;
                if (this.transferSetupConfigObjects != null)
                {
                    if (externalTransferSetupList != null)
                    {
                        count = this.transferSetupConfigObjects.Count;
                        if (count == externalTransferSetupList.Count)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                isTheSame = TransferSetupIsTheSame(this.transferSetupConfigObjects[i], externalTransferSetupList[i]);
                                if (!isTheSame)
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            isTheSame = false;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_Configuration.TransferSetupListIsTheSame(List<Main_ConfigComponent_TransferSetup>)\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.TransferSetupListIsTheSame(List<Main_ConfigComponent_TransferSetup>)\nthis.transferSetupConfigObjects was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.TransferSetupListIsTheSame(List<Main_ConfigComponent_TransferSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        private bool TransferSetupIsTheSame(Main_ConfigComponent_TransferSetup internalTransferSetup, Main_ConfigComponent_TransferSetup externalTransferSetup)
        {
            bool isTheSame = true;
            try
            {
                if (internalTransferSetup.TransferSetupNumber != externalTransferSetup.TransferSetupNumber)
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.TransferNumber != externalTransferSetup.TransferNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.Working != externalTransferSetup.Working))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.SourceModuleNumber != externalTransferSetup.SourceModuleNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.SourceRegisterNumber != externalTransferSetup.SourceRegisterNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.DestinationModuleNumber != externalTransferSetup.DestinationModuleNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.DestinationRegisterNumber != externalTransferSetup.DestinationRegisterNumber))
                {
                    isTheSame = false;
                }
                if (isTheSame && (Math.Abs(internalTransferSetup.Multiplier - externalTransferSetup.Multiplier) > epsilon))
                {
                    isTheSame = false;
                }
                if (isTheSame && (internalTransferSetup.Reserved != externalTransferSetup.Reserved))
                {
                    isTheSame = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.TransferSetupIsTheSame(Main_ConfigComponent_TransferSetup, Main_ConfigComponent_TransferSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return isTheSame;
        }

        #endregion

        #region Copy Configuration
     
        public static Main_Configuration CopyConfiguration(Main_Configuration inputConfiguration)
        {
            Main_Configuration copyOfConfiguration = null;
            try
            {
                Int16[] registerData = null;
                if (inputConfiguration != null)
                {
                    registerData = inputConfiguration.GetShortValuesFromAllContributors();
                    if (registerData != null)
                    {
                        copyOfConfiguration = new Main_Configuration(registerData);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyCalibrationDataList(List<Main_ConfigComponent_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfConfiguration;
        }

        private static List<Main_ConfigComponent_CalibrationData> CopyCalibrationDataList(List<Main_ConfigComponent_CalibrationData> inputCalibrationDataList)
        {
            List<Main_ConfigComponent_CalibrationData> copyOfCalibrationDataList = new List<Main_ConfigComponent_CalibrationData>();
            try
            {
                Main_ConfigComponent_CalibrationData singleCalibrationData;
                if (inputCalibrationDataList != null)
                {
                    foreach (Main_ConfigComponent_CalibrationData entry in inputCalibrationDataList)
                    {
                        singleCalibrationData = CopyCalibrationData(entry);
                        copyOfCalibrationDataList.Add(singleCalibrationData);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyCalibrationDataList(List<Main_ConfigComponent_CalibrationData>)\nInput List<Main_ConfigComponent_CalibrationData> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyCalibrationDataList(List<Main_ConfigComponent_CalibrationData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfCalibrationDataList;
        }

        private static Main_ConfigComponent_CalibrationData CopyCalibrationData(Main_ConfigComponent_CalibrationData inputCalibrationData)
        {
            Main_ConfigComponent_CalibrationData copyOfCalibrationData = new Main_ConfigComponent_CalibrationData();
            try
            {
                copyOfCalibrationData.ChannelNumber = inputCalibrationData.ChannelNumber;
                copyOfCalibrationData.ChannelNoise = inputCalibrationData.ChannelNoise;
                copyOfCalibrationData.ZeroLine = inputCalibrationData.ZeroLine;
                copyOfCalibrationData.Multiplier = inputCalibrationData.Multiplier;
                copyOfCalibrationData.Offset = inputCalibrationData.Offset;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyCalibrationData(Main_ConfigComponent_CalibrationData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfCalibrationData;
        }
 
        private static Main_ConfigComponent_CommunicationSetup CopyCommunicationSetup(Main_ConfigComponent_CommunicationSetup inputCommunicationSetup)
        {
            Main_ConfigComponent_CommunicationSetup copyOfCommuncationSetup = new Main_ConfigComponent_CommunicationSetup();
            try
            {
                copyOfCommuncationSetup.ModBusAddress = inputCommunicationSetup.ModBusAddress;
                copyOfCommuncationSetup.BaudRateRS485 = inputCommunicationSetup.BaudRateRS485;
                copyOfCommuncationSetup.ModBusProtocol = inputCommunicationSetup.ModBusProtocol;
                copyOfCommuncationSetup.BaudRateXport = inputCommunicationSetup.BaudRateXport;
                copyOfCommuncationSetup.ModBusProtocolOverXport = inputCommunicationSetup.ModBusProtocolOverXport;
                copyOfCommuncationSetup.IP1 = inputCommunicationSetup.IP1;
                copyOfCommuncationSetup.IP2 = inputCommunicationSetup.IP2;
                copyOfCommuncationSetup.IP3 = inputCommunicationSetup.IP3;
                copyOfCommuncationSetup.IP4 = inputCommunicationSetup.IP4;
                copyOfCommuncationSetup.SaveDataIntervalInSeconds = inputCommunicationSetup.SaveDataIntervalInSeconds;
                copyOfCommuncationSetup.SaveDataIfChangedByPercent = inputCommunicationSetup.SaveDataIfChangedByPercent;
                copyOfCommuncationSetup.SaveDataIfChangedStatus = inputCommunicationSetup.SaveDataIfChangedStatus;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyCommunicationSetup(Main_ConfigComponent_CommunicationSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfCommuncationSetup;
        }

        private static Main_ConfigComponent_DeviceSetup CopyDeviceSetup(Main_ConfigComponent_DeviceSetup inputDeviceSetup)
        {
            Main_ConfigComponent_DeviceSetup copyOfDeviceSetup = new Main_ConfigComponent_DeviceSetup();
            try
            {
                copyOfDeviceSetup.SetupStatus = inputDeviceSetup.SetupStatus;
                copyOfDeviceSetup.Monitoring = inputDeviceSetup.Monitoring;
                copyOfDeviceSetup.Frequency = inputDeviceSetup.Frequency;
                copyOfDeviceSetup.AlarmControl = inputDeviceSetup.AlarmControl;
                copyOfDeviceSetup.WarningControl = inputDeviceSetup.WarningControl;
                copyOfDeviceSetup.AllowDirectAccess = inputDeviceSetup.AllowDirectAccess;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyDeviceSetup(Main_ConfigComponent_DeviceSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfDeviceSetup;
        }

        private static List<Main_ConfigComponent_FullStatusInformation> CopyFullStatusInformationList(List<Main_ConfigComponent_FullStatusInformation> inputFullStatusInformationList)
        {
            List<Main_ConfigComponent_FullStatusInformation> copyOfFullStatusInformationList = new List<Main_ConfigComponent_FullStatusInformation>();
            try
            {
                Main_ConfigComponent_FullStatusInformation singleFullStatusInformation;
                if (inputFullStatusInformationList != null)
                {
                    foreach (Main_ConfigComponent_FullStatusInformation entry in inputFullStatusInformationList)
                    {
                        singleFullStatusInformation = CopyFullStatusInformation(entry);
                        copyOfFullStatusInformationList.Add(singleFullStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyFullStatusInformationList(List<Main_ConfigComponent_FullStatusInformation>)\nInput List<Main_ConfigComponent_FullStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyFullStatusInformationList(List<Main_ConfigComponent_FullStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfFullStatusInformationList;
        }

        private static Main_ConfigComponent_FullStatusInformation CopyFullStatusInformation(Main_ConfigComponent_FullStatusInformation inputFullStatusInformation)
        {
            Main_ConfigComponent_FullStatusInformation copyOfFullStatusInformation = new Main_ConfigComponent_FullStatusInformation();
            try
            {
                copyOfFullStatusInformation.ModuleNumber = inputFullStatusInformation.ModuleNumber;
                copyOfFullStatusInformation.ModuleStatus = inputFullStatusInformation.ModuleStatus;
                copyOfFullStatusInformation.ModuleType = inputFullStatusInformation.ModuleType;
                copyOfFullStatusInformation.ModBusAddress = inputFullStatusInformation.ModBusAddress;
                copyOfFullStatusInformation.BaudRate = inputFullStatusInformation.BaudRate;
                copyOfFullStatusInformation.ReadDatePeriodInSeconds = inputFullStatusInformation.ReadDatePeriodInSeconds;
                copyOfFullStatusInformation.ConnectionType = inputFullStatusInformation.ConnectionType;
                copyOfFullStatusInformation.Reserved_0 = inputFullStatusInformation.Reserved_0;
                copyOfFullStatusInformation.Reserved_1 = inputFullStatusInformation.Reserved_1;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyFullStatusInformation(Main_ConfigComponent_FullStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfFullStatusInformation;
        }

        private static Main_ConfigComponent_General CopyGeneral(Main_ConfigComponent_General inputGeneral)
        {
            Main_ConfigComponent_General copyOfGeneral = new Main_ConfigComponent_General();
            try
            {
                copyOfGeneral.DeviceType = inputGeneral.DeviceType;
                copyOfGeneral.FirmwareVersion = inputGeneral.FirmwareVersion;
                copyOfGeneral.GlobalStatus = inputGeneral.GlobalStatus;
                copyOfGeneral.DevicesError = inputGeneral.DevicesError;
                copyOfGeneral.DeviceDateTime = inputGeneral.DeviceDateTime;
                copyOfGeneral.UnitName_0 = inputGeneral.UnitName_0;
                copyOfGeneral.UnitName_1 = inputGeneral.UnitName_1;
                copyOfGeneral.UnitName_2 = inputGeneral.UnitName_2;
                copyOfGeneral.UnitName_3 = inputGeneral.UnitName_3;
                copyOfGeneral.UnitName_4 = inputGeneral.UnitName_4;
                copyOfGeneral.UnitName_5 = inputGeneral.UnitName_5;
                copyOfGeneral.UnitName_6 = inputGeneral.UnitName_6;
                copyOfGeneral.UnitName_7 = inputGeneral.UnitName_7;
                copyOfGeneral.UnitName_8 = inputGeneral.UnitName_8;
                copyOfGeneral.UnitName_9 = inputGeneral.UnitName_9;
                copyOfGeneral.UnitName_10 = inputGeneral.UnitName_10;
                copyOfGeneral.UnitName_11 = inputGeneral.UnitName_11;
                copyOfGeneral.UnitName_12 = inputGeneral.UnitName_12;
                copyOfGeneral.UnitName_13 = inputGeneral.UnitName_13;
                copyOfGeneral.UnitName_14 = inputGeneral.UnitName_14;
                copyOfGeneral.UnitName_15 = inputGeneral.UnitName_15;
                copyOfGeneral.UnitName_16 = inputGeneral.UnitName_16;
                copyOfGeneral.UnitName_17 = inputGeneral.UnitName_17;
                copyOfGeneral.UnitName_18 = inputGeneral.UnitName_18;
                copyOfGeneral.UnitName_19 = inputGeneral.UnitName_19;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyGeneral(Main_ConfigComponent_General)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfGeneral;
        }

        private static List<Main_ConfigComponent_InputChannel> CopyInputChannelList(List<Main_ConfigComponent_InputChannel> inputInputChannelList)
        {
            List<Main_ConfigComponent_InputChannel> copyOfInputChannelList = new List<Main_ConfigComponent_InputChannel>();
            try
            {
                Main_ConfigComponent_InputChannel singleInputChannel;
                if (inputInputChannelList != null)
                {
                    foreach (Main_ConfigComponent_InputChannel entry in inputInputChannelList)
                    {
                        singleInputChannel = CopyInputChannel(entry);
                        copyOfInputChannelList.Add(singleInputChannel);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyInputChannelList(List<Main_ConfigComponent_InputChannel>)\nInput List<Main_ConfigComponent_InputChannel> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannelList(List<Main_ConfigComponent_InputChannel>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannelList;
        }

        private static Main_ConfigComponent_InputChannel CopyInputChannel(Main_ConfigComponent_InputChannel inputInputChannel)
        {
            Main_ConfigComponent_InputChannel copyOfInputChannel = new Main_ConfigComponent_InputChannel();
            try
            {
                copyOfInputChannel.InputChannelNumber = inputInputChannel.InputChannelNumber;
                copyOfInputChannel.ReadStatus = inputInputChannel.ReadStatus;
                copyOfInputChannel.SensorType = inputInputChannel.SensorType;
                copyOfInputChannel.Unit = inputInputChannel.Unit;
                copyOfInputChannel.SensorID = inputInputChannel.SensorID;
                copyOfInputChannel.SensorNumber = inputInputChannel.SensorNumber;
                copyOfInputChannel.Sensitivity = inputInputChannel.Sensitivity;
                copyOfInputChannel.HighPassFilter = inputInputChannel.HighPassFilter;
                copyOfInputChannel.LowPassFilter = inputInputChannel.LowPassFilter;
                copyOfInputChannel.ReadFrequency = inputInputChannel.ReadFrequency;
                copyOfInputChannel.ReadPoints = inputInputChannel.ReadPoints;
                copyOfInputChannel.Averages = inputInputChannel.Averages;
                copyOfInputChannel.SaveDataIfChangedPercent = inputInputChannel.SaveDataIfChangedPercent;
                copyOfInputChannel.SaveDataIfChangedValue = inputInputChannel.SaveDataIfChangedValue;
                copyOfInputChannel.JunkValue_1 = inputInputChannel.JunkValue_1;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannel(Main_ConfigComponent_InputChannel)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannel;
        }

        private static List<Main_ConfigComponent_InputChannelEffluvia> CopyInputChannelEffluviaList(List<Main_ConfigComponent_InputChannelEffluvia> inputInputChannelEffluviaList)
        {
            List<Main_ConfigComponent_InputChannelEffluvia> copyOfInputChannelEffluviaList = new List<Main_ConfigComponent_InputChannelEffluvia>();
            try
            {
                Main_ConfigComponent_InputChannelEffluvia singleInputChannelEffluvia;
                if (inputInputChannelEffluviaList != null)
                {
                    foreach (Main_ConfigComponent_InputChannelEffluvia entry in inputInputChannelEffluviaList)
                    {
                        singleInputChannelEffluvia = CopyInputChannelEffluvia(entry);
                        copyOfInputChannelEffluviaList.Add(singleInputChannelEffluvia);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyInputChannelEffluviaList(List<Main_ConfigComponent_InputChannelEffluvia>)\nInput List<Main_ConfigComponent_InputChannelEffluvia> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannelEffluviaList(List<Main_ConfigComponent_InputChannelEffluvia>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannelEffluviaList;
        }

        private static Main_ConfigComponent_InputChannelEffluvia CopyInputChannelEffluvia(Main_ConfigComponent_InputChannelEffluvia inputInputChannelEffluvia)
        {
            Main_ConfigComponent_InputChannelEffluvia copyOfInputChannelEffluvia = new Main_ConfigComponent_InputChannelEffluvia();
            try
            {
                copyOfInputChannelEffluvia.Value_1 = inputInputChannelEffluvia.Value_1;
                copyOfInputChannelEffluvia.Value_2 = inputInputChannelEffluvia.Value_2;
                copyOfInputChannelEffluvia.Value_3 = inputInputChannelEffluvia.Value_3;
                copyOfInputChannelEffluvia.Value_4 = inputInputChannelEffluvia.Value_4;
                copyOfInputChannelEffluvia.Value_5 = inputInputChannelEffluvia.Value_5;
                copyOfInputChannelEffluvia.Value_6 = inputInputChannelEffluvia.Value_6;
                copyOfInputChannelEffluvia.Value_7 = inputInputChannelEffluvia.Value_7;
                copyOfInputChannelEffluvia.Value_8 = inputInputChannelEffluvia.Value_8;
                copyOfInputChannelEffluvia.Value_9 = inputInputChannelEffluvia.Value_9;
                copyOfInputChannelEffluvia.Value_10 = inputInputChannelEffluvia.Value_10;
                copyOfInputChannelEffluvia.Value_11 = inputInputChannelEffluvia.Value_11;
                copyOfInputChannelEffluvia.Value_12 = inputInputChannelEffluvia.Value_12;
                copyOfInputChannelEffluvia.Value_13 = inputInputChannelEffluvia.Value_13;
                copyOfInputChannelEffluvia.Value_14 = inputInputChannelEffluvia.Value_14;
                copyOfInputChannelEffluvia.Value_15 = inputInputChannelEffluvia.Value_15;
                copyOfInputChannelEffluvia.Value_16 = inputInputChannelEffluvia.Value_16;
                copyOfInputChannelEffluvia.Value_17 = inputInputChannelEffluvia.Value_17;
                copyOfInputChannelEffluvia.Value_18 = inputInputChannelEffluvia.Value_18;
                copyOfInputChannelEffluvia.Value_19 = inputInputChannelEffluvia.Value_19;
                copyOfInputChannelEffluvia.Value_20 = inputInputChannelEffluvia.Value_20;
                copyOfInputChannelEffluvia.Value_21 = inputInputChannelEffluvia.Value_21;
                copyOfInputChannelEffluvia.Value_22 = inputInputChannelEffluvia.Value_22;
                copyOfInputChannelEffluvia.Value_23 = inputInputChannelEffluvia.Value_23;
                copyOfInputChannelEffluvia.Value_24 = inputInputChannelEffluvia.Value_24;
                copyOfInputChannelEffluvia.Value_25 = inputInputChannelEffluvia.Value_25;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannelEffluvia(Main_ConfigComponent_InputChannelEffluvia)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannelEffluvia;
        }

        private static List<Main_ConfigComponent_InputChannelThreshold> CopyInputChannelThresholdList(List<Main_ConfigComponent_InputChannelThreshold> inputInputChannelThresholdList)
        {
            List<Main_ConfigComponent_InputChannelThreshold> copyOfInputChannelThresholdList = new List<Main_ConfigComponent_InputChannelThreshold>();
            try
            {
                Main_ConfigComponent_InputChannelThreshold singleInputChannelThreshold;
                if (inputInputChannelThresholdList != null)
                {
                    foreach (Main_ConfigComponent_InputChannelThreshold entry in inputInputChannelThresholdList)
                    {
                        singleInputChannelThreshold = CopyInputChannelThreshold(entry);
                        copyOfInputChannelThresholdList.Add(singleInputChannelThreshold);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyInputChannelThresholdList(List<Main_ConfigComponent_InputChannelThreshold>)\nInput List<Main_ConfigComponent_InputChannelThreshold> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannelThresholdList(List<Main_ConfigComponent_InputChannelThreshold>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannelThresholdList;
        }

        private static Main_ConfigComponent_InputChannelThreshold CopyInputChannelThreshold(Main_ConfigComponent_InputChannelThreshold inputInputChannelThreshold)
        {
            Main_ConfigComponent_InputChannelThreshold copyOfInputChannelThreshold = new Main_ConfigComponent_InputChannelThreshold();
            try
            {
                copyOfInputChannelThreshold.InputChannelNumber = inputInputChannelThreshold.InputChannelNumber;
                copyOfInputChannelThreshold.ThresholdNumber = inputInputChannelThreshold.ThresholdNumber;
                copyOfInputChannelThreshold.ThresholdEnable = inputInputChannelThreshold.ThresholdEnable;
                copyOfInputChannelThreshold.ThresholdStatus = inputInputChannelThreshold.ThresholdStatus;
                copyOfInputChannelThreshold.ThresholdType = inputInputChannelThreshold.ThresholdType;
                copyOfInputChannelThreshold.RelayMode = inputInputChannelThreshold.RelayMode;
                copyOfInputChannelThreshold.Calculation = inputInputChannelThreshold.Calculation;
                copyOfInputChannelThreshold.WorkingBy = inputInputChannelThreshold.WorkingBy;
                copyOfInputChannelThreshold.LowFrequencyOfSignal = inputInputChannelThreshold.LowFrequencyOfSignal;
                copyOfInputChannelThreshold.HighFrequencyOfSignal = inputInputChannelThreshold.HighFrequencyOfSignal;
                copyOfInputChannelThreshold.RelayStatusOnDelay = inputInputChannelThreshold.RelayStatusOnDelay;
                copyOfInputChannelThreshold.RelayStatusOffDelay = inputInputChannelThreshold.RelayStatusOffDelay;
                copyOfInputChannelThreshold.ThresholdValue = inputInputChannelThreshold.ThresholdValue;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyInputChannelThreshold(Main_ConfigComponent_InputChannelThreshold)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfInputChannelThreshold;
        }

        private static List<Main_ConfigComponent_ShortStatusInformation> CopyShortStatusInformationList(List<Main_ConfigComponent_ShortStatusInformation> inputShortStatusInformationList)
        {
            List<Main_ConfigComponent_ShortStatusInformation> copyOfShortStatusInformationList = new List<Main_ConfigComponent_ShortStatusInformation>();
            try
            {
                Main_ConfigComponent_ShortStatusInformation singleShortStatusInformation;
                if (inputShortStatusInformationList != null)
                {
                    foreach (Main_ConfigComponent_ShortStatusInformation entry in inputShortStatusInformationList)
                    {
                        singleShortStatusInformation = CopyShortStatusInformation(entry);
                        copyOfShortStatusInformationList.Add(singleShortStatusInformation);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyShortStatusInformationList(List<Main_ConfigComponent_ShortStatusInformation>)\nInput List<Main_ConfigComponent_ShortStatusInformation> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyShortStatusInformationList(List<Main_ConfigComponent_ShortStatusInformation>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfShortStatusInformationList;
        }

        private static Main_ConfigComponent_ShortStatusInformation CopyShortStatusInformation(Main_ConfigComponent_ShortStatusInformation inputShortStatusInformation)
        {
            Main_ConfigComponent_ShortStatusInformation copyOfShortStatusInformation = new Main_ConfigComponent_ShortStatusInformation();
            try
            {
                copyOfShortStatusInformation.ModuleNumber = inputShortStatusInformation.ModuleNumber;
                copyOfShortStatusInformation.ModuleStatus = inputShortStatusInformation.ModuleStatus;
                copyOfShortStatusInformation.ModuleType = inputShortStatusInformation.ModuleType;
                copyOfShortStatusInformation.Reserved = inputShortStatusInformation.Reserved;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyShortStatusInformation(Main_ConfigComponent_ShortStatusInformation)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfShortStatusInformation;
        }

        private static List<Main_ConfigComponent_TransferSetup> CopyTransferSetupList(List<Main_ConfigComponent_TransferSetup> inputTransferSetupList)
        {
            List<Main_ConfigComponent_TransferSetup> copyOfTransferSetupList = new List<Main_ConfigComponent_TransferSetup>();
            try
            {
                Main_ConfigComponent_TransferSetup singleTransferSetup;
                if (inputTransferSetupList != null)
                {
                    foreach (Main_ConfigComponent_TransferSetup entry in inputTransferSetupList)
                    {
                        singleTransferSetup = CopyTransferSetup(entry);
                        copyOfTransferSetupList.Add(singleTransferSetup);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_Configuration.CopyTransferSetupList(List<Main_ConfigComponent_TransferSetup>)\nInput List<Main_ConfigComponent_TransferSetup> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyTransferSetupList(List<Main_ConfigComponent_TransferSetup>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfTransferSetupList;
        }

        private static Main_ConfigComponent_TransferSetup CopyTransferSetup(Main_ConfigComponent_TransferSetup inputTransferSetup)
        {
            Main_ConfigComponent_TransferSetup copyOfTransferSetup = new Main_ConfigComponent_TransferSetup();
            try
            {
                copyOfTransferSetup.TransferSetupNumber = inputTransferSetup.TransferSetupNumber;
                copyOfTransferSetup.TransferNumber = inputTransferSetup.TransferNumber;
                copyOfTransferSetup.Working = inputTransferSetup.Working;
                copyOfTransferSetup.SourceModuleNumber = inputTransferSetup.SourceModuleNumber;
                copyOfTransferSetup.SourceRegisterNumber = inputTransferSetup.SourceRegisterNumber;
                copyOfTransferSetup.DestinationModuleNumber = inputTransferSetup.DestinationModuleNumber;
                copyOfTransferSetup.DestinationRegisterNumber = inputTransferSetup.DestinationRegisterNumber;
                copyOfTransferSetup.Multiplier = inputTransferSetup.Multiplier;
                copyOfTransferSetup.Reserved = inputTransferSetup.Reserved;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.CopyTransferSetup(Main_ConfigComponent_TransferSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return copyOfTransferSetup;
        }


        #endregion

        public List<string> GetConfigurationAsListOfStrings()
        {
            List<string> configurationValues = new List<string>();
            try
            {
                string entry;
                if (AllConfigurationMembersAreNonNull())
                {
                    configurationValues.Add("Firmware Version: " + Math.Round(generalConfigObject.FirmwareVersion, 2).ToString());
                    entry = "Monitoring: ";
                    if (deviceSetupConfigObject.Monitoring == 1)
                    {
                        entry += "Enabled";
                    }
                    else
                    {
                        entry += "Disabled";
                    }
                    configurationValues.Add(entry);
                    configurationValues.Add("Frequency: " + deviceSetupConfigObject.Frequency.ToString());
                    configurationValues.Add("Board name: " + GetBoardNameFromRegisterValues(generalConfigObject));

                    configurationValues.Add("Modbus Address: " + communicationSetupConfigObject.ModBusAddress.ToString());
                    configurationValues.Add("RS 485 Baud Rate: " + (communicationSetupConfigObject.BaudRateRS485 * 100).ToString());
                    configurationValues.Add("Ethernet Baud Rate: " + (communicationSetupConfigObject.BaudRateXport * 100).ToString());

                    entry = "Ethernet protocol: ";
                    if (communicationSetupConfigObject.ModBusProtocolOverXport == 0)
                    {
                        entry += "RTU";
                    }
                    else
                    {
                        entry += "TCP";
                    }
                    configurationValues.Add(entry);

                    configurationValues.Add("IP address: " + communicationSetupConfigObject.IP1.ToString() + "." +
                                                             communicationSetupConfigObject.IP2.ToString() + "." +
                                                             communicationSetupConfigObject.IP3.ToString() + "." +
                                                             communicationSetupConfigObject.IP4.ToString());

                    configurationValues.Add("Relay alarm: " + GetRelayWarningAndAlarmSettingAsString(deviceSetupConfigObject.AlarmControl));
                    configurationValues.Add("Relay warning: " + GetRelayWarningAndAlarmSettingAsString(deviceSetupConfigObject.WarningControl));
                    configurationValues.Add("Mode save step: " + (communicationSetupConfigObject.SaveDataIntervalInSeconds / 60).ToString() + " minutes");

                    entry = "Mode save take into account results of diagnostic: ";  
                    if (communicationSetupConfigObject.SaveDataIfChangedStatus == 1)
                    {
                        entry += "Enabled";
                    }
                    else
                    {
                        entry += "Disabled";
                    }
                    configurationValues.Add(entry);






                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.GetConfigurationAsListOfStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return configurationValues;
        }

        private string GetRelayWarningAndAlarmSettingAsString(int relaySetting)
        {
            string setting = string.Empty;
            try
            {
                switch (relaySetting)
                {
                    case 0:
                        setting = "off";
                        break;
                    case 1:
                        setting = "on";
                        break;
                    case 2:
                        setting = "External devices";
                        break;
                    case 3:
                        setting = "Null device";
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_Configuration.GetRelayWarningAndAlarmSettingAsString(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return setting;
        }


    }
}
