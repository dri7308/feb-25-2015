﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class Main_DataComponent_WindingHotSpotData
    {
        public DateTime ReadingDateTime;
        public double WhsTopOilTemp;
        public double WhsMax;
        public double WhsFan1Starts;
        public double WhsFan2Starts;
        public double WhsFan1Hours;
        public double WhsFan2Hours;
        public double MaxWindingTempPhaseA;
        public double MaxWindingTempPhaseB;
        public double MaxWindingTempPhaseC;
        public double AgingFactorA;
        public double AgingFactorB;
        public double AgingFactorC;
        public double AccumulatedAgingA;
        public double AccumulatedAgingB;
        public double AccumulatedAgingC;
        public string AlarmSourceAsBitString;
        public string ErrorSourceAsBitString;
        public int Reserved;
    }
}
