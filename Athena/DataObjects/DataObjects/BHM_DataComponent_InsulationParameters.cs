﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class BHM_DataComponent_InsulationParameters
    {
        public int TrSideCount;
        public int RPN_0;
        public int RPN_1;
        public double Current_0;
        public double Current_1;
        public int Temperature_0;
        public int Temperature_1;
        public int Temperature_2;
        public int Temperature_3;
        public double CurrentR_0;
        public double CurrentR_1;
        public int Humidity;
        public int CalcZk;
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
    }
}
