﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class PDM_DataComponent_ChPDParameters
    {
        public int FromChannel;
        public int WithChannels_0;
        public double ChannelSensitivity;
        public int Q02;
        public int Q02Plus;
        public int Q02Minus;
        public double Q02mV;
        public double PDI;
        public double PDIPlus;
        public double PDIMinus;
        public long SumPDAmpl;
        public long SumPDPlus;
        public long SumPDMinus;
        public int PDI_t;
        public int PDI_j;
        public int Q02_t;
        public int Q02_j;
        public int Separations_0;
        public int RefShift_0;
        public int WasBlockedByT;
        public int WasBlockedByP;
        public double RatedVoltage;
        public double RatedCurrent;
        public int State;
        public int AlarmStatus;
        public long P_0;
        public long P_1;
        public long P_2;
        public long P_3;
        public long P_4;
        public long P_5;
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
        public int Reserved_21;
        public int Reserved_22;
        public int Reserved_23;
        public int Reserved_24;
        public int Reserved_25;
        public int Reserved_26;
        public int Reserved_27;
    }
}
