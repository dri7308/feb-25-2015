﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class BHM_DataComponent_SideToSideData
    {
        public int SideNumber;
        public double Amplitude1_0;
        public double Amplitude1_1;
        public double Amplitude1_2;
        public double Amplitude2_0;
        public double Amplitude2_1;
        public double Amplitude2_2;
        public double PhaseShift_0;
        public double PhaseShift_1;
        public double PhaseShift_2;
        public double CurrentAmpl_0;
        public double CurrentAmpl_1;
        public double CurrentAmpl_2;
        public double CurrentPhase_0;
        public double CurrentPhase_1;
        public double CurrentPhase_2;
        public double ZkAmpl_0;
        public double ZkAmpl_1;
        public double ZkAmpl_2;
        public double ZkPhase_0;
        public double ZkPhase_1;
        public double ZkPhase_2;
        public double CurrentChannelShift;
        public long ZkPercent_0;
        public long ZkPercent_1;
        public long ZkPercent_2;
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
    }
}
