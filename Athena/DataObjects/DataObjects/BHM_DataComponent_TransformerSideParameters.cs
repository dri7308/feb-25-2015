﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class BHM_DataComponent_TransformerSideParameters
    {
        public int SideNumber;
        public double SourceAmplitude_0;
        public double SourceAmplitude_1;
        public double SourceAmplitude_2;
        public double PhaseAmplitude_0;
        public double PhaseAmplitude_1;
        public double PhaseAmplitude_2;
        public double Gamma;
        public double GammaPhase;
        public int KT;
        public int AlarmStatus;
        public int RemainingLife_0;
        public int RemainingLife_1;
        public int RemainingLife_2;
        public int DefectCode;
        public int ChPhaseShift;
        public int Trend;
        public int KTPhase;
        public double SignalPhase_0;
        public double SignalPhase_1;
        public double SignalPhase_2;
        public double SourcePhase_0;
        public double SourcePhase_1;
        public double SourcePhase_2;
        public double Frequency;
        public int Temperature;
        public double C_0;
        public double C_1;
        public double C_2;
        public double Tg_0;
        public double Tg_1;
        public double Tg_2;
        public int ExternalSyncShift;
        public int Reserved_0;
    }
}
