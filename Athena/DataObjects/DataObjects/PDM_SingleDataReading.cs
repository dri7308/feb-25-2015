﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DataObjects
{
    public class PDM_SingleDataReading
    {
        /// <summary>
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// 
        /// IHM data file structure:
        /// 
        /// This was kind of tricky.  IHM does not save the data exactly as downloaded from the device.  It adds a certain amount of information.  It even adds
        /// information about inactive channels interleaved with the phase resolved data.  This was not easy to detect until I was faced with PRD that had
        /// gaps in the active channels.  I ended up setting my device to the same settings and taking a measurement.  I found a lot of junk data in there 
        /// that I didn't know was there before.
        /// 
        /// So the way it works is:
        /// 
        /// 188 byte header
        /// 2048 bytes of common and channel data
        /// Phase resolved data (varies by number of active channels)
        /// 420 byte footer
        /// 
        /// I don't know what's in the header or footer.
        /// 
        /// The phase resolved data is saved as follows.  For each channel, if the phase resolved data was saved, a 6144 byte block of data is in the file.  Immediately
        /// following the block of data is a 1286 byte addendum of information that I don't know how to translate.  If the phase resolved data is not saved, then you 
        /// just have the 1286 byte addendum.  So that addendum is always there.
        /// 
        /// While I did not test this for every configuration, if I use this formula I can predict the exact number of bytes in a IHM PDM file for every one I checked.
        ///  
        /// </summary>


        public static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 2048;

        public DateTime readingDateTime;

        public PDM_DataComponent_Parameters parameters = null;
        public PDM_DataComponent_PDParameters pdParameters = null;
        public List<PDM_DataComponent_ChPDParameters> chPDParameters = null;
        public List<PDM_DataComponent_PhaseResolvedData> phaseResolvedData = null;

        private bool[] channelIsActive = null;
        private int activeChannelCount = 0;

        //public PDM_SingleDataReading(Byte[] byteData, Int32[] phaseResolvedDataAsArray)
        //{
        //    parameters = ExtractParametersFromByteData(byteData);

        //    pdParameters = ExtractPDParametersFromByteData(byteData);
        //    Boolean[] channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
        //    chPDParameters = ConvertOneRecordOfChannelDataToDbData(byteData, channelIsActive);
        //    if ((pdParameters.Matrix == 1) && phaseResolvedDataAsArray !=null)
        //    {
        //        phaseResolvedData = ConvertArchiveDataToPhaseResolvedData(phaseResolvedDataAsArray, channelIsActive);
        //    }           
        //}

        public PDM_SingleDataReading(Byte[] commonAndChannelData, Byte[] rawPhaseResolvedData)
        {
            try
            {
                if (commonAndChannelData != null)
                {
                    if (InitializeCommonAndChannelDataObjects(commonAndChannelData))
                    {
                        if ((parameters.PDExists == 1)&&(pdParameters.Matrix == 1))
                        {
                            if (rawPhaseResolvedData != null)
                            {
                                if (!InitializePhaseResolvedDataObjects(rawPhaseResolvedData))
                                {
                                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[], Byte[]):\nSomething was wrong with the rawPhaseResolvedData array, initialization failed.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[], Byte[]):\nSecond input Byte[] was null.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[], Byte[]):\nFailed to properly initialize the command and channel data objects.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[], Byte[]):\nFirst input Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PDM_SingleDataReading(Byte[], Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public PDM_SingleDataReading(Byte[] byteData)
        {
            try
            {
                Byte[] commonAndChannelData = null;
                Byte[] rawPhaseResolvedData = null;
                if (byteData != null)
                {
                    if(InputDataHasCorrectLength(byteData))
                    {                  
                        commonAndChannelData = ExtractRawCommonAndChannelDataFromFileData(byteData);
                        if (commonAndChannelData != null)
                        {
                            if (InitializeCommonAndChannelDataObjects(commonAndChannelData))
                            {
                                if ((parameters.PDExists == 1) && (pdParameters.Matrix == 1))
                                {
                                    rawPhaseResolvedData = ExtractRawPhaseResolvedDataFromFileData(byteData, this.channelIsActive, this.activeChannelCount);

                                    if (rawPhaseResolvedData != null)
                                    {
                                        if (!InitializePhaseResolvedDataObjects(rawPhaseResolvedData))
                                        {
                                            string errorMessage = "Error in DataObjects.PDM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nPhase resolved data should be present; either it was not present or the format was incorrect.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error in DataObjects.PDM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nPhase resolved data should be present; either it was not present or the format was incorrect.";
                                        LogMessage.LogError(errorMessage);
#if DEBUG
                                        MessageBox.Show(errorMessage);
#endif
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[]):\nFailed to initialize the commandAndChannelData objects.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif

                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[]):\nFailed to extract the commandAndChannelData from the input data.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[]):\nInput Byte[] had an incorrect number of elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.PDM_SingleDataReading(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PDM_SingleDataReading(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool InitializeCommonAndChannelDataObjects(Byte[] commonAndChannelData)
        {
            bool success = true;
            try
            {
                if (commonAndChannelData != null)
                {
                    parameters = ExtractParametersFromByteData(commonAndChannelData);
                    if (parameters == null)
                    {
                        success = false;
                    }
                    if (success)
                    {
                        pdParameters = ExtractPDParametersFromByteData(commonAndChannelData);
                        if (pdParameters == null)
                        {
                            success = false;
                        }
                    }
                    if (success)
                    {
                        this.channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
                        this.activeChannelCount = GetActiveChannelCount(channelIsActive);
                        chPDParameters = ConvertOneRecordOfChannelDataToDbData(commonAndChannelData, channelIsActive);
                        if (chPDParameters == null)
                        {
                            success = false;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.InitializeCommonAndChannelDataObjects(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    success = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InitializeCommonAndChannelDataObjects(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool InitializePhaseResolvedDataObjects(Byte[] rawPhaseResolvedData)
        {
            bool success = true;
            try
            {
                Int32[] archivePhaseResolvedData = null;
                if (rawPhaseResolvedData != null)
                {
                    archivePhaseResolvedData = ConvertRawPhaseResolvedDataToArchiveFormat(rawPhaseResolvedData);
                    if (archivePhaseResolvedData != null)
                    {
                        phaseResolvedData = ConvertArchiveDataToPhaseResolvedData(archivePhaseResolvedData, channelIsActive);
                        if (phaseResolvedData == null)
                        {
                            success = false;
                        }
                    }
                    else
                    {
                        success = false;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.InitializePhaseResolvedDataObjects(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                    success = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InitializePhaseResolvedDataObjects(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private int GetActiveChannelCount(bool[] channelIsActive)
        {
            int activeChannelCount = 0;
            try
            {
                if (this.channelIsActive != null)
                {
                    if (this.channelIsActive.Length >= 15)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            if (this.channelIsActive[i])
                            {
                                activeChannelCount++;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.GetActiveChannelCount(bool[])\nInput bool[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.GetActiveChannelCount(bool[])\nInput bool[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.GetActiveChannelCount(bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannelCount;
        }

        private bool InitializeObjectUsingByteData(Byte[] commonAndChannelData, Byte[] rawPhaseResolvedData)
        {
            bool success = true;
            try
            {
                if (commonAndChannelData != null)
                {
                    parameters = ExtractParametersFromByteData(commonAndChannelData);

                    pdParameters = ExtractPDParametersFromByteData(commonAndChannelData);
                    Boolean[] channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
                    chPDParameters = ConvertOneRecordOfChannelDataToDbData(commonAndChannelData, channelIsActive);
                    if ((parameters.PDExists == 1) && (pdParameters.Matrix == 1))
                    {
                        if (rawPhaseResolvedData != null)
                        {
                            Int32[] archivePhaseResolvedData = ConvertRawPhaseResolvedDataToArchiveFormat(rawPhaseResolvedData);
                            phaseResolvedData = ConvertArchiveDataToPhaseResolvedData(archivePhaseResolvedData, channelIsActive);
                        }
                        else
                        {
                            string errorMessage = "Error in DataObjects.PDM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nSecond input Byte[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nFirst input Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        public PDM_SingleDataReading(DateTime argReadingDateTime, PDM_DataComponent_Parameters argParamaters, PDM_DataComponent_PDParameters argPDParameters,
                                     List<PDM_DataComponent_ChPDParameters> argChPDParameters, List<PDM_DataComponent_PhaseResolvedData> argPhaseResolvedData)
        {
            try
            {
                readingDateTime = argReadingDateTime;
                parameters = argParamaters;
                pdParameters = argPDParameters;
                chPDParameters = argChPDParameters;
                phaseResolvedData = argPhaseResolvedData;

                if (!EnoughMembersAreCorrect())
                {
                    SetAllMembersToNull();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PDM_SingleDataReading(DateTime, PDM_DataComponent_Parameters, PDM_DataComponent_PDParameterss, List<PDM_DataComponent_ChPDParameters>, List<PDM_DataComponent_PhaseResolvedData>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //public static List<PDM_SingleDataReading> GetAllDataForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        //{
        //    return GetAllDataForOneMonitor(monitorID, ConversionMethods.MinimumDateTime(), db);
        //}

        //public static List<PDM_SingleDataReading> GetAllDataForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        //{
        //    List<PDM_SingleDataReading> pdmSingleDataReadingsList = new List<PDM_SingleDataReading>();

        //    int chPDParametersIndex = 0;
        //    int phaseResolvedDataIndex = 0;
        //    int measurementNumber;

        //    List<PDM_Data_DataRoot> allDataRootTableEntries = DatabaseInterface.PDM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<PDM_Data_Parameters> allParametersTableEntries = DatabaseInterface.PDM_Data_GetAllParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<PDM_Data_PDParameters> allPDParametersTableEntries = DatabaseInterface.PDM_Data_GetAllPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<PDM_Data_ChPDParameters> allChPDParametersTableEntries = DatabaseInterface.PDM_Data_GetAllChPDParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<PDM_Data_PhaseResolvedData> allPhaseResolvedDataTableEntries = DatabaseInterface.PDM_Data_GetAllPhaseResolvedDataTableEntriesForOneMonitor(monitorID, minimumDateTime, db);

        //    PDM_Data_DataRoot dataRootForOneReading = null;
        //    PDM_Data_Parameters parametersForOneReading = null;
        //    PDM_Data_PDParameters pdParamatersForOneReading = null;
        //    List<PDM_Data_ChPDParameters> chPDParametersForOneReading = null;
        //    List<PDM_Data_PhaseResolvedData> phaseResolvedDataForOneReading = null;

        //    int totalDataReadings = allDataRootTableEntries.Count;
        //    int totalChPDParametersEntries = allChPDParametersTableEntries.Count;
        //    int totalPhaseResolvedDataEntries = allPhaseResolvedDataTableEntries.Count;

        //    DateTime currentReadingDateTime;

        //    PDM_SingleDataReading pdmSingleDataReading;

        //    for (measurementNumber = 0; measurementNumber < totalDataReadings; measurementNumber++)
        //    {
        //        dataRootForOneReading = allDataRootTableEntries[measurementNumber];
        //        currentReadingDateTime = dataRootForOneReading.ReadingDateTime;
        //        if (allParametersTableEntries[measurementNumber].ReadingDateTime.CompareTo(currentReadingDateTime) == 0)
        //        {
        //            parametersForOneReading = allParametersTableEntries[measurementNumber];
        //        }
        //        if (allPDParametersTableEntries[measurementNumber].ReadingDateTime.CompareTo(currentReadingDateTime) == 0)
        //        {
        //            pdParamatersForOneReading = allPDParametersTableEntries[measurementNumber];
        //        }

        //        chPDParametersForOneReading = new List<PDM_Data_ChPDParameters>();
        //        while ((chPDParametersIndex < totalChPDParametersEntries) &&
        //              (allChPDParametersTableEntries[chPDParametersIndex].ReadingDateTime.CompareTo(currentReadingDateTime) == 0))
        //        {
        //            chPDParametersForOneReading.Add(allChPDParametersTableEntries[chPDParametersIndex]);
        //            chPDParametersIndex++;
        //        }

        //        phaseResolvedDataForOneReading = new List<PDM_Data_PhaseResolvedData>();
        //        while ((phaseResolvedDataIndex < totalPhaseResolvedDataEntries) &&
        //            (allPhaseResolvedDataTableEntries[phaseResolvedDataIndex].ReadingDateTime.CompareTo(currentReadingDateTime) == 0))
        //        {
        //            phaseResolvedDataForOneReading.Add(allPhaseResolvedDataTableEntries[phaseResolvedDataIndex]);
        //            phaseResolvedDataIndex++;
        //        }

        //        pdmSingleDataReading = new PDM_SingleDataReading(true, dataRootForOneReading, parametersForOneReading, pdParamatersForOneReading, chPDParametersForOneReading, phaseResolvedDataForOneReading);
        //        if (pdmSingleDataReading.EnoughMembersAreCorrect())
        //        {
        //            pdmSingleDataReadingsList.Add(pdmSingleDataReading);
        //        }
        //    }

        //    return pdmSingleDataReadingsList;
        //}

        public bool InputDataHasCorrectLength(Byte[] byteData)
        {
            bool lengthIsCorrect = false;
            try
            {
                int length;
                if (byteData != null)
                {
                    length = byteData.Length;
                    if (InputDataLengthHasDeviceLength(length) || InputDataLengthHasFileLength(length))
                    {
                        lengthIsCorrect = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.InputDataHasCorrectLength(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InputDataHasCorrectLength(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public bool InputDataLengthHasDeviceLength(int length)
        {
            bool lengthIsCorrect = true;
            try
            {
                if (length < 2048)
                {
                    lengthIsCorrect = false;
                }
                else if (length > 2048)
                {
                    if (((length - 2048) % 6144) != 0)
                    {
                        lengthIsCorrect = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InputDataLengthHasDeviceLength(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public bool InputDataLengthHasFileLength(int length)
        {
            bool lengthIsCorrect = true;
            try
            {
                // the minimum number of bytes in a PDM file is 21946
                int remainder = length - 21946;

                if (remainder < 0)
                {
                    lengthIsCorrect = false;
                }
                else if (remainder > 0)
                {
                    if (remainder % 6144 != 0)
                    {
                        lengthIsCorrect = false;
                    }
                }           
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.InputDataLengthHasFileLength(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public static Byte[] ExtractRawCommonAndChannelDataFromFileData(Byte[] byteData)
        {
            Byte[] commonAndChannelData = null;
            try
            {
                int length;
                if (byteData != null)
                {
                    length = byteData.Length;

                    if (length >= 2236)
                    {
                        commonAndChannelData = new Byte[2048];
                        Array.Copy(byteData, 188, commonAndChannelData, 0, 2048);
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return commonAndChannelData;
        }

        public static Byte[] ExtractRawPhaseResolvedDataFromFileData(Byte[] byteData, bool[] channelIsActive, int activeChannelCount)
        {
            Byte[] rawPhaseResolvedData = null;
            try
            {
                //int numberOfEntries;
                int sourceIndexForCopyiong = 2236;
                int destinationIndexForCopying = 0;
                int length;

                if (byteData != null)
                {
                    if (channelIsActive != null)
                    {
                        if (channelIsActive.Length > 14)
                        {
                            length = byteData.Length;
                            // As written at the top, we have 1286 bytes for each channel, whether or not it's active, as well as
                            // 188 + 2040 = 2236 header + short record bytes, 6144 * active channel count prd bytes, and 420 footer bytes.
                            if (length == (15 * 1286) + (activeChannelCount * 6144) + 2236 + 420)
                            //if ((length - 2236 - (activeChannelCount * 7430)) >= 0)
                            {
                                rawPhaseResolvedData = new Byte[activeChannelCount * 6144];
                                for (int i = 0; i < 15; i++)
                                {
                                    if (channelIsActive[i])
                                    {
                                        Array.Copy(byteData, sourceIndexForCopyiong, rawPhaseResolvedData, destinationIndexForCopying, 6144);
                                        sourceIndexForCopyiong += 6144;
                                        destinationIndexForCopying += 6144;
                                    }
                                    // we always have to add for the prd overhead for each channel, even if no prd was saved for that channel
                                    sourceIndexForCopyiong += 1286;
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int):\nInput Byte[] did not have the expected number of elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int):\nInput bool[] did not have the expected number of elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                            
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int):\nInput bool[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.PDM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return rawPhaseResolvedData;
        }

        public static bool PhaseResolvedDataWasSaved(Byte[] byteData)
        {
            bool wasSaved = false;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= 15)
                    {
                        if (byteData[14] > 0)
                        {
                            wasSaved = true;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return wasSaved;
        }

        public static int ActiveChannelCount(Byte[] byteData)
        {
            int activeChannelCount = 0;
            try
            {
                string channels = ConversionMethods.UnsignedShortBytesToBitEncodedString(byteData[12], byteData[13]);
                for (int i = 0; i < channels.Length; i++)
                {
                    if (channels[i].CompareTo('1') == 0)
                    {
                        activeChannelCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ActiveChannelCount(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannelCount;
        }

        public Dictionary<int, int> GetMatrixCountsPerChannelForOneDataReading()
        {
            Dictionary<int, int> matrixCountsPerChannel = null;
            try
            {
                int zeroIndexChannelNumber;
                if (this.phaseResolvedData != null)
                {
                    matrixCountsPerChannel = new Dictionary<int, int>();
                    foreach (PDM_DataComponent_PhaseResolvedData entry in this.phaseResolvedData)
                    {
                        zeroIndexChannelNumber = entry.ChannelNumber;
                        if (matrixCountsPerChannel.ContainsKey(zeroIndexChannelNumber))
                        {
                            matrixCountsPerChannel[zeroIndexChannelNumber]++;
                        }
                        else
                        {
                            matrixCountsPerChannel.Add(zeroIndexChannelNumber, 1);
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.GetMatrixCountsPerChannelForOneDataReading()\nPrivate data this.phaseResovedData was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.GetMatrixCountsPerChannelForOneDataReading()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return matrixCountsPerChannel;
        }

        public static int GetMatrixCount(List<PDM_SingleDataReading> singleDataReadingsList)
        {
            int count = 0;
            //int count2 = 0;
            try
            {
                if (singleDataReadingsList != null)
                {
                    foreach (PDM_SingleDataReading entry in singleDataReadingsList)
                    {
                        if (entry.pdParameters.Matrix > 0)
                        {
                            count++;
                        }
                        //if (entry.parameters.PDExists > 0)
                        //{
                        //    count2++;
                        //}
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.GetMatrixCount(List<PDM_SingleDataReading>)\nInput List<PDM_SingleDataReading> was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.GetMatrixCount(List<PDM_SingleDataReading>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return count;
        }

        public bool EnoughMembersAreCorrect()
        {
            bool enoughAreCorrect = true;
            try
            {
                if ((parameters == null) || (pdParameters == null) || (chPDParameters == null))
                {
                    enoughAreCorrect = false;
                }
                if (enoughAreCorrect && chPDParameters.Count == 0)
                {
                    enoughAreCorrect = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.EnoughMembersAreCorrect()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return enoughAreCorrect;
        }

        private void SetAllMembersToNull()
        {
            parameters = null;
            pdParameters = null;
            chPDParameters = null;
            phaseResolvedData = null;
        }

        private PDM_DataComponent_Parameters ExtractParametersFromByteData(Byte[] byteData)
        {
            PDM_DataComponent_Parameters parameters = null;
            try
            {
                DateTime localReadingDateTime;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        localReadingDateTime = ConversionMethods.ConvertStringValuesToDateTime(ConversionMethods.ByteToInt32(byteData[1]).ToString(),
                                                                                                       ConversionMethods.ByteToInt32(byteData[0]).ToString(),
                                                                                                       (ConversionMethods.ByteToInt32(byteData[2]) + 2000).ToString(),
                                                                                                       ConversionMethods.ByteToInt32(byteData[3]).ToString(),
                                                                                                       ConversionMethods.ByteToInt32(byteData[4]).ToString());
                        if (!(localReadingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0))
                        {
                            string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedCommonDataToDbData(Byte[], Guid)\nBad date from data, throwing away data point.";
                            LogMessage.LogError(errorMessage);
                            parameters = null;
                        }
                        else
                        {
                            this.readingDateTime = localReadingDateTime;

                            parameters = new PDM_DataComponent_Parameters();

                            parameters.Tempfile = ConversionMethods.ByteToInt32(byteData[5]);
                            parameters.PDExists = ConversionMethods.ByteToInt32(byteData[6]);
                            //skipped a byte, probably for alignment
                            parameters.AlarmEnable = ConversionMethods.UnsignedShortBytesToUInt16(byteData[8], byteData[9]);

                            parameters.Initial = ConversionMethods.ByteToInt32(byteData[1952]);
                            parameters.Humidity = ConversionMethods.ByteToInt32(byteData[1953]);
                            parameters.Temperature = ConversionMethods.ByteToInt32(byteData[1954]) - 70;

                            Single externalLoadActive = ConversionMethods.BytesToSingle(byteData[1956], byteData[1957], byteData[1958], byteData[1959]);
                            externalLoadActive = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(externalLoadActive);
                            parameters.ExtLoadActive = externalLoadActive;

                            Single externalLoadReactive = ConversionMethods.BytesToSingle(byteData[1960], byteData[1961], byteData[1962], byteData[1963]);
                            externalLoadReactive = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(externalLoadReactive);
                            parameters.ExtLoadReactive = externalLoadReactive;
                            
                            //CommonRegisters[0] = LTC Position Number
                            //CommonRegisters[1] = Temp 2
                            //CommonRegisters[2] = Temp 3
                            //CommonRegisters[3] = Temp 4
                            //CommonRegisters[4] = Load Current 2
                            //CommonRegisters[5] = Load Current 3
                            //CommonRegisters[6] = Voltage 1
                            //CommonRegisters[7] = Voltage 2
                            //CommonRegisters[8] = Voltage 3

                            // CFK modified the following byte offsets 11/21/2013 lines 975-1047

                            // This is the LTC Position Number
                            parameters.CommonRegisters_0 = ConversionMethods.ShortBytesToInt16(byteData[1962], byteData[1963]);

                            /// These are temperatures 2, 3, and 4
                            parameters.CommonRegisters_1 = ConversionMethods.ShortBytesToInt16(byteData[1964], byteData[1965]) - 70;
                            parameters.CommonRegisters_2 = ConversionMethods.ShortBytesToInt16(byteData[1966], byteData[1967]) - 70;
                            parameters.CommonRegisters_3 = ConversionMethods.ShortBytesToInt16(byteData[1968], byteData[1969]) - 70;

                            /// load currents 2 and 3
                            parameters.CommonRegisters_4 = ConversionMethods.ShortBytesToInt16(byteData[1970], byteData[1971]);
                            parameters.CommonRegisters_5 = ConversionMethods.ShortBytesToInt16(byteData[1972], byteData[1973]);

                            /// voltages 1, 2, and 3
                            parameters.CommonRegisters_6 = ConversionMethods.ShortBytesToInt16(byteData[1974], byteData[1975]);
                            parameters.CommonRegisters_7 = ConversionMethods.ShortBytesToInt16(byteData[1976], byteData[1977]);
                            parameters.CommonRegisters_8 = ConversionMethods.ShortBytesToInt16(byteData[1978], byteData[1979]);

                            /// The rest are undefined at the moment
                            parameters.CommonRegisters_9 = ConversionMethods.ShortBytesToInt16(byteData[1980], byteData[1981]);
                            parameters.CommonRegisters_10 = ConversionMethods.ShortBytesToInt16(byteData[1982], byteData[1983]);
                            parameters.CommonRegisters_11 = ConversionMethods.ShortBytesToInt16(byteData[1984], byteData[1985]);
                            parameters.CommonRegisters_12 = ConversionMethods.ShortBytesToInt16(byteData[1986], byteData[1987]);
                            parameters.CommonRegisters_13 = ConversionMethods.ShortBytesToInt16(byteData[1988], byteData[1989]);
                            parameters.CommonRegisters_14 = ConversionMethods.ShortBytesToInt16(byteData[1990], byteData[1991]);
                            parameters.CommonRegisters_15 = ConversionMethods.ShortBytesToInt16(byteData[1992], byteData[1993]);
                            parameters.CommonRegisters_16 = ConversionMethods.ShortBytesToInt16(byteData[1994], byteData[1995]);
                            parameters.CommonRegisters_17 = ConversionMethods.ShortBytesToInt16(byteData[1996], byteData[1997]);
                            parameters.CommonRegisters_18 = ConversionMethods.ShortBytesToInt16(byteData[1998], byteData[1999]);
                            parameters.CommonRegisters_19 = ConversionMethods.ShortBytesToInt16(byteData[2000], byteData[2001]);

                            parameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[2002]);
                            parameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[2003]);
                            parameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[2004]);
                            parameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[2005]);
                            parameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[2006]);
                            parameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[2007]);
                            parameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[2008]);
                            parameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[2009]);
                            parameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[2010]);
                            parameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[2011]);
                            parameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[2012]);
                            parameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[2013]);
                            parameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[2014]);
                            parameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[2015]);
                            parameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[2016]);
                            parameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[2017]);
                            parameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[2018]);
                            parameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[2019]);
                            parameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[2020]);
                            parameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[2021]);
                            parameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[2022]);
                            parameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[2023]);
                            parameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[2024]);
                            parameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[2025]);
                            parameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[2026]);
                            parameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[2027]);
                            parameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[2028]);
                            parameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[2029]);
                            parameters.Reserved_28 = ConversionMethods.ByteToInt32(byteData[2030]);
                            parameters.Reserved_29 = ConversionMethods.ByteToInt32(byteData[2031]);
                            parameters.Reserved_30 = ConversionMethods.ByteToInt32(byteData[2032]);
                            parameters.Reserved_31 = ConversionMethods.ByteToInt32(byteData[2033]);
                            parameters.Reserved_32 = ConversionMethods.ByteToInt32(byteData[2034]);
                            parameters.Reserved_33 = ConversionMethods.ByteToInt32(byteData[2035]);
                            parameters.Reserved_34 = ConversionMethods.ByteToInt32(byteData[2036]);
                            parameters.Reserved_35 = ConversionMethods.ByteToInt32(byteData[2037]);
                            parameters.Reserved_36 = ConversionMethods.ByteToInt32(byteData[2038]);
                            parameters.Reserved_37 = ConversionMethods.ByteToInt32(byteData[2039]);
                            parameters.Reserved_38 = ConversionMethods.ByteToInt32(byteData[2040]);
                            parameters.Reserved_39 = ConversionMethods.ByteToInt32(byteData[2041]);
                            parameters.Reserved_40 = ConversionMethods.ByteToInt32(byteData[2042]);
                            parameters.Reserved_41 = ConversionMethods.ByteToInt32(byteData[2043]);
                            parameters.Reserved_42 = ConversionMethods.ByteToInt32(byteData[2044]);
                            parameters.Reserved_43 = ConversionMethods.ByteToInt32(byteData[2045]);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ExtractParametersFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ExtractParametersFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ExtractParametersFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return parameters;
        }

        private PDM_DataComponent_PDParameters ExtractPDParametersFromByteData(Byte[] byteData)
        {
            PDM_DataComponent_PDParameters pdParameters = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        pdParameters = new PDM_DataComponent_PDParameters();

                        pdParameters.Channels = ConversionMethods.UnsignedShortBytesToBitEncodedString(byteData[12], byteData[13]);
                        pdParameters.Matrix = ConversionMethods.ByteToInt32(byteData[14]);
                        pdParameters.MaxPDIChannel = ConversionMethods.ByteToInt32(byteData[15]);
                        pdParameters.MaxQ02Channel = ConversionMethods.ByteToInt32(byteData[16]);
                        pdParameters.NumberOfPeriods = ConversionMethods.UnsignedShortBytesToUInt16(byteData[18], byteData[19]);

                        Single frequency = ConversionMethods.BytesToSingle(byteData[20], byteData[21], byteData[22], byteData[23]);
                        frequency = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(frequency);
                        pdParameters.Frequency = frequency;

                        pdParameters.PSpeed_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[24], byteData[25]);
                        pdParameters.PSpeed_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[26], byteData[27]);

                        pdParameters.PJump_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[28], byteData[29]);
                        pdParameters.PJump_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[30], byteData[31]);

                        pdParameters.State = ConversionMethods.ByteToInt32(byteData[32]);

                        Single zoneWidth = ConversionMethods.BytesToSingle(byteData[36], byteData[37], byteData[38], byteData[39]);
                        zoneWidth = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zoneWidth);
                        pdParameters.ZoneWidth = zoneWidth;

                        pdParameters.ZonesCount = ConversionMethods.ByteToInt32(byteData[40]);

                        Single angleStep = ConversionMethods.BytesToSingle(byteData[44], byteData[45], byteData[46], byteData[47]);
                        angleStep = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(angleStep);
                        pdParameters.AngleStep = angleStep;

                        pdParameters.AnglesCount = ConversionMethods.ByteToInt32(byteData[48]);

                        pdParameters.TablesOnChannel = ConversionMethods.ByteToInt32(byteData[49]);

                        pdParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[1852]);
                        pdParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[1853]);
                        pdParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[1854]);
                        pdParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[1855]);
                        pdParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[1856]);
                        pdParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[1857]);
                        pdParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[1858]);
                        pdParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[1859]);
                        pdParameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[1860]);
                        pdParameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[1861]);
                        pdParameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[1862]);
                        pdParameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[1863]);
                        pdParameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[1864]);
                        pdParameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[1865]);
                        pdParameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[1866]);
                        pdParameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[1867]);
                        pdParameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[1868]);
                        pdParameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[1869]);
                        pdParameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[1870]);
                        pdParameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[1871]);
                        pdParameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[1872]);
                        pdParameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[1873]);
                        pdParameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[1874]);
                        pdParameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[1875]);
                        pdParameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[1876]);
                        pdParameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[1877]);
                        pdParameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[1878]);
                        pdParameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[1879]);
                        pdParameters.Reserved_28 = ConversionMethods.ByteToInt32(byteData[1880]);
                        pdParameters.Reserved_29 = ConversionMethods.ByteToInt32(byteData[1881]);
                        pdParameters.Reserved_30 = ConversionMethods.ByteToInt32(byteData[1882]);
                        pdParameters.Reserved_31 = ConversionMethods.ByteToInt32(byteData[1883]);
                        pdParameters.Reserved_32 = ConversionMethods.ByteToInt32(byteData[1884]);
                        pdParameters.Reserved_33 = ConversionMethods.ByteToInt32(byteData[1885]);
                        pdParameters.Reserved_34 = ConversionMethods.ByteToInt32(byteData[1886]);
                        pdParameters.Reserved_35 = ConversionMethods.ByteToInt32(byteData[1887]);
                        pdParameters.Reserved_36 = ConversionMethods.ByteToInt32(byteData[1888]);
                        pdParameters.Reserved_37 = ConversionMethods.ByteToInt32(byteData[1889]);
                        pdParameters.Reserved_38 = ConversionMethods.ByteToInt32(byteData[1890]);
                        pdParameters.Reserved_39 = ConversionMethods.ByteToInt32(byteData[1891]);
                        pdParameters.Reserved_40 = ConversionMethods.ByteToInt32(byteData[1892]);
                        pdParameters.Reserved_41 = ConversionMethods.ByteToInt32(byteData[1893]);
                        pdParameters.Reserved_42 = ConversionMethods.ByteToInt32(byteData[1894]);
                        pdParameters.Reserved_43 = ConversionMethods.ByteToInt32(byteData[1895]);
                        pdParameters.Reserved_44 = ConversionMethods.ByteToInt32(byteData[1896]);
                        pdParameters.Reserved_45 = ConversionMethods.ByteToInt32(byteData[1897]);
                        pdParameters.Reserved_46 = ConversionMethods.ByteToInt32(byteData[1898]);
                        pdParameters.Reserved_47 = ConversionMethods.ByteToInt32(byteData[1899]);
                        pdParameters.Reserved_48 = ConversionMethods.ByteToInt32(byteData[1900]);
                        pdParameters.Reserved_49 = ConversionMethods.ByteToInt32(byteData[1901]);
                        pdParameters.Reserved_50 = ConversionMethods.ByteToInt32(byteData[1902]);
                        pdParameters.Reserved_51 = ConversionMethods.ByteToInt32(byteData[1903]);
                        pdParameters.Reserved_52 = ConversionMethods.ByteToInt32(byteData[1904]);
                        pdParameters.Reserved_53 = ConversionMethods.ByteToInt32(byteData[1905]);
                        pdParameters.Reserved_54 = ConversionMethods.ByteToInt32(byteData[1906]);
                        pdParameters.Reserved_55 = ConversionMethods.ByteToInt32(byteData[1907]);
                        pdParameters.Reserved_56 = ConversionMethods.ByteToInt32(byteData[1908]);
                        pdParameters.Reserved_57 = ConversionMethods.ByteToInt32(byteData[1909]);
                        pdParameters.Reserved_58 = ConversionMethods.ByteToInt32(byteData[1910]);
                        pdParameters.Reserved_59 = ConversionMethods.ByteToInt32(byteData[1911]);
                        pdParameters.Reserved_60 = ConversionMethods.ByteToInt32(byteData[1912]);
                        pdParameters.Reserved_61 = ConversionMethods.ByteToInt32(byteData[1913]);
                        pdParameters.Reserved_62 = ConversionMethods.ByteToInt32(byteData[1914]);
                        pdParameters.Reserved_63 = ConversionMethods.ByteToInt32(byteData[1915]);
                        pdParameters.Reserved_64 = ConversionMethods.ByteToInt32(byteData[1916]);
                        pdParameters.Reserved_65 = ConversionMethods.ByteToInt32(byteData[1917]);
                        pdParameters.Reserved_66 = ConversionMethods.ByteToInt32(byteData[1918]);
                        pdParameters.Reserved_67 = ConversionMethods.ByteToInt32(byteData[1919]);
                        pdParameters.Reserved_68 = ConversionMethods.ByteToInt32(byteData[1920]);
                        pdParameters.Reserved_69 = ConversionMethods.ByteToInt32(byteData[1921]);
                        pdParameters.Reserved_70 = ConversionMethods.ByteToInt32(byteData[1922]);
                        pdParameters.Reserved_71 = ConversionMethods.ByteToInt32(byteData[1923]);
                        pdParameters.Reserved_72 = ConversionMethods.ByteToInt32(byteData[1924]);
                        pdParameters.Reserved_73 = ConversionMethods.ByteToInt32(byteData[1925]);
                        pdParameters.Reserved_74 = ConversionMethods.ByteToInt32(byteData[1926]);
                        pdParameters.Reserved_75 = ConversionMethods.ByteToInt32(byteData[1927]);
                        pdParameters.Reserved_76 = ConversionMethods.ByteToInt32(byteData[1928]);
                        pdParameters.Reserved_77 = ConversionMethods.ByteToInt32(byteData[1929]);
                        pdParameters.Reserved_78 = ConversionMethods.ByteToInt32(byteData[1930]);
                        pdParameters.Reserved_79 = ConversionMethods.ByteToInt32(byteData[1931]);
                        pdParameters.Reserved_80 = ConversionMethods.ByteToInt32(byteData[1932]);
                        pdParameters.Reserved_81 = ConversionMethods.ByteToInt32(byteData[1933]);
                        pdParameters.Reserved_82 = ConversionMethods.ByteToInt32(byteData[1934]);
                        pdParameters.Reserved_83 = ConversionMethods.ByteToInt32(byteData[1935]);
                        pdParameters.Reserved_84 = ConversionMethods.ByteToInt32(byteData[1936]);
                        pdParameters.Reserved_85 = ConversionMethods.ByteToInt32(byteData[1937]);
                        pdParameters.Reserved_86 = ConversionMethods.ByteToInt32(byteData[1938]);
                        pdParameters.Reserved_87 = ConversionMethods.ByteToInt32(byteData[1939]);
                        pdParameters.Reserved_88 = ConversionMethods.ByteToInt32(byteData[1940]);
                        pdParameters.Reserved_89 = ConversionMethods.ByteToInt32(byteData[1941]);
                        pdParameters.Reserved_90 = ConversionMethods.ByteToInt32(byteData[1942]);
                        pdParameters.Reserved_91 = ConversionMethods.ByteToInt32(byteData[1943]);
                        pdParameters.Reserved_92 = ConversionMethods.ByteToInt32(byteData[1944]);
                        pdParameters.Reserved_93 = ConversionMethods.ByteToInt32(byteData[1945]);
                        pdParameters.Reserved_94 = ConversionMethods.ByteToInt32(byteData[1946]);
                        pdParameters.Reserved_95 = ConversionMethods.ByteToInt32(byteData[1947]);
                        pdParameters.Reserved_96 = ConversionMethods.ByteToInt32(byteData[1948]);
                        pdParameters.Reserved_97 = ConversionMethods.ByteToInt32(byteData[1949]);
                        pdParameters.Reserved_98 = ConversionMethods.ByteToInt32(byteData[1950]);
                        pdParameters.Reserved_99 = ConversionMethods.ByteToInt32(byteData[1951]);
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ExtractPDParametersFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ExtractPDParametersFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ExtractPDParametersFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParameters;
        }

        private List<PDM_DataComponent_ChPDParameters> ConvertOneRecordOfChannelDataToDbData(Byte[] byteData, bool[] activeChannels)
        {
            List<PDM_DataComponent_ChPDParameters> convertedData = new List<PDM_DataComponent_ChPDParameters>();
            try
            {
                int offset = 52;
                int channelNumber;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        if (activeChannels != null)
                        {
                            if (activeChannels.Length >= 15)
                            {
                                for (int i = 0; i < 15; i++)
                                {
                                    if (activeChannels[i])
                                    {
                                        channelNumber = ConversionMethods.ByteToInt32(byteData[offset]);
                                        if (channelNumber == i)
                                        {
                                            convertedData.Add(ExtractChPDParametersFromByteData(byteData, offset));
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], bool[]):\nApparent mismatch between expected channel number and the channel number in the data.  Expected " + i.ToString() + ", got " + channelNumber.ToString();
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                        }
                                    }
                                    offset += 120;
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], bool[]):\nInput bool[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], bool[]):\nInput bool[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], bool[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.PDM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], bool[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PDM_ConvertOneArchivedChannelDataToDbData(Byte[], bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedData;
        }

        private PDM_DataComponent_ChPDParameters ExtractChPDParametersFromByteData(Byte[] byteData, int startingOffset)
        {
            PDM_DataComponent_ChPDParameters chPDParameters = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        chPDParameters = new PDM_DataComponent_ChPDParameters();

                        // we will adjust this value to conform to a 1-indexed channel designation
                        chPDParameters.FromChannel = ConversionMethods.ByteToInt32(byteData[startingOffset]) + 1;

                        chPDParameters.WithChannels_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 1]);
                        // we need to skip a few bytes for proper byte alignment of the data(apparently), so no worries, this is correct
                        Single channelSensitivity = ConversionMethods.BytesToSingle(byteData[startingOffset + 4], byteData[startingOffset + 5],
                                                                                    byteData[startingOffset + 6], byteData[startingOffset + 7]);
                        channelSensitivity = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(channelSensitivity);
                        chPDParameters.ChannelSensitivity = channelSensitivity;

                        chPDParameters.Q02 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 8], byteData[startingOffset + 9]);
                        chPDParameters.Q02Plus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 10], byteData[startingOffset + 11]);
                        chPDParameters.Q02Minus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 12], byteData[startingOffset + 13]);
                        // we need to skip a few bytes for proper byte alignment of the data(apparently), so no worries, this is correct
                        Single q02mV = ConversionMethods.BytesToSingle(byteData[startingOffset + 16], byteData[startingOffset + 17],
                                                                       byteData[startingOffset + 18], byteData[startingOffset + 19]);
                        q02mV = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(q02mV);
                        chPDParameters.Q02mV = q02mV;

                        chPDParameters.PDI = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 20], byteData[startingOffset + 21],
                                                                                 byteData[startingOffset + 22], byteData[startingOffset + 23]) / 10.0;
                        chPDParameters.PDIPlus = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 24], byteData[startingOffset + 25],
                                                                                     byteData[startingOffset + 26], byteData[startingOffset + 27]) / 10.0;
                        chPDParameters.PDIMinus = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 28], byteData[startingOffset + 29],
                                                                                      byteData[startingOffset + 30], byteData[startingOffset + 31]) / 10.0;
                        chPDParameters.SumPDAmpl = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 32], byteData[startingOffset + 33],
                                                                                          byteData[startingOffset + 34], byteData[startingOffset + 35]);
                        chPDParameters.SumPDPlus = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 36], byteData[startingOffset + 37],
                                                                                          byteData[startingOffset + 38], byteData[startingOffset + 39]);
                        chPDParameters.SumPDMinus = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 40], byteData[startingOffset + 41],
                                                                                           byteData[startingOffset + 42], byteData[startingOffset + 43]);
                        chPDParameters.PDI_t = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 44], byteData[startingOffset + 45]);
                        chPDParameters.PDI_j = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 46], byteData[startingOffset + 47]);

                        chPDParameters.Q02_t = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 48], byteData[startingOffset + 49]);
                        chPDParameters.Q02_j = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 50], byteData[startingOffset + 51]);

                        chPDParameters.Separations_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 52]);

                        chPDParameters.RefShift_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 53]);
                        chPDParameters.WasBlockedByT = ConversionMethods.ByteToInt32(byteData[startingOffset + 54]);
                        chPDParameters.WasBlockedByP = ConversionMethods.ByteToInt32(byteData[startingOffset + 55]);
                        Single ratedVoltage = ConversionMethods.BytesToSingle(byteData[startingOffset + 56], byteData[startingOffset + 57],
                                                                                byteData[startingOffset + 58], byteData[startingOffset + 59]);
                        ratedVoltage = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(ratedVoltage);
                        chPDParameters.RatedVoltage = ratedVoltage;
                        Single ratedCurrent = ConversionMethods.BytesToSingle(byteData[startingOffset + 60], byteData[startingOffset + 61],
                                                                                byteData[startingOffset + 62], byteData[startingOffset + 63]);
                        ratedCurrent = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(ratedCurrent);
                        chPDParameters.RatedCurrent = ratedCurrent;

                        chPDParameters.State = ConversionMethods.ByteToInt32(byteData[startingOffset + 64]);

                        chPDParameters.AlarmStatus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 66], byteData[startingOffset + 67]);

                        chPDParameters.P_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 68], byteData[startingOffset + 69],
                                                                                    byteData[startingOffset + 70], byteData[startingOffset + 71]);
                        chPDParameters.P_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 72], byteData[startingOffset + 73],
                                                                                    byteData[startingOffset + 74], byteData[startingOffset + 75]);
                        chPDParameters.P_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 76], byteData[startingOffset + 77],
                                                                                    byteData[startingOffset + 78], byteData[startingOffset + 79]);
                        chPDParameters.P_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 80], byteData[startingOffset + 81],
                                                                                    byteData[startingOffset + 82], byteData[startingOffset + 83]);
                        chPDParameters.P_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 84], byteData[startingOffset + 85],
                                                                                    byteData[startingOffset + 86], byteData[startingOffset + 87]);
                        chPDParameters.P_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 88], byteData[startingOffset + 89],
                                                                                    byteData[startingOffset + 90], byteData[startingOffset + 91]);

                        chPDParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 92]);
                        chPDParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[startingOffset + 93]);
                        chPDParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[startingOffset + 94]);
                        chPDParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[startingOffset + 95]);
                        chPDParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[startingOffset + 96]);
                        chPDParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[startingOffset + 97]);
                        chPDParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[startingOffset + 98]);
                        chPDParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[startingOffset + 99]);
                        chPDParameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[startingOffset + 100]);
                        chPDParameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[startingOffset + 101]);
                        chPDParameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[startingOffset + 102]);
                        chPDParameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[startingOffset + 103]);
                        chPDParameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[startingOffset + 104]);
                        chPDParameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[startingOffset + 105]);
                        chPDParameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[startingOffset + 106]);
                        chPDParameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[startingOffset + 107]);
                        chPDParameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[startingOffset + 108]);
                        chPDParameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[startingOffset + 109]);
                        chPDParameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[startingOffset + 110]);
                        chPDParameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[startingOffset + 111]);
                        chPDParameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[startingOffset + 112]);
                        chPDParameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[startingOffset + 113]);
                        chPDParameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[startingOffset + 114]);
                        chPDParameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[startingOffset + 115]);
                        chPDParameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[startingOffset + 116]);
                        chPDParameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[startingOffset + 117]);
                        chPDParameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[startingOffset + 118]);
                        chPDParameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[startingOffset + 119]);
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ExtractChPDParametersFromByteData(Byte[],int)\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ExtractChPDParametersFromByteData(Byte[],int)\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ExtractChPDParametersFromByteData(Byte[],int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParameters;
        }

        public static Int32[] ConvertRawPhaseResolvedDataToArchiveFormat(Byte[] byteData)
        {
            Int32[] archiveData = null;
            try
            {
                int numberOfEntries;
                int length;
                if (byteData != null)
                {
                    length = byteData.Length;

                    if ((length % 6144) == 0)
                    {
                        numberOfEntries = length / 2;
                        archiveData = new Int32[numberOfEntries];
                        int j = 0;
                        for (int i = 0; i < numberOfEntries; i++)
                        {
                            archiveData[i] = ConversionMethods.UnsignedShortBytesToUInt16(byteData[j], byteData[j + 1]);
                            j += 2;
                        }
                        archiveData = ReorderPhaseResolvedData(archiveData);
                    }
                    else
                    {
                        string errorMessage = "Error in Error in PDM_SingleDataReading.ConvertRawPhaseResolvedDataToArchiveFormat(Byte[]):\nInput Byte[] has too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Error in PDM_SingleDataReading.ConvertRawPhaseResolvedDataToArchiveFormat(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ConvertRawPhaseResolvedDataToArchiveFormat(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return archiveData;
        }

        private static Int32[] ReorderPhaseResolvedData(Int32[] phaseResolvedData)
        {
            Int32[] reorderedPhaseResolvedData = null;
            try
            {
                int totalPoints;
                int totalChannels;
                int offset;
                int oldDataIndex;
                int i, row, column;

                if (phaseResolvedData != null)
                {
                    totalPoints = phaseResolvedData.Length;
                    totalChannels = totalPoints / 3072;
                    offset = 0;
                    oldDataIndex = 0;

                    if ((totalPoints % 3072) == 0)
                    {
                        reorderedPhaseResolvedData = new Int32[totalPoints];
                        for (i = 0; i < totalChannels; i++)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                for (column = 0; column < 48; column++)
                                {
                                    for (row = 0; row < 32; row++)
                                    {
                                        reorderedPhaseResolvedData[offset + (row * 48) + (column)] = phaseResolvedData[oldDataIndex];
                                        oldDataIndex++;
                                    }
                                }
                                offset += 1536;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.PDM_ReorderPhaseResolvedData(Int32[]):\nInput Int32[] did not have the correct number of elements, which must be a multiple of 3072.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.PDM_ReorderPhaseResolvedData(Int32[]):\nInput Int32[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif

                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.PDM_ReorderPhaseResolvedData(Int32[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return reorderedPhaseResolvedData;
        }

        public static List<PDM_DataComponent_PhaseResolvedData> ConvertArchiveDataToPhaseResolvedData(Int32[] archivedRecords, bool[] channelIsActive)
        {
            List<PDM_DataComponent_PhaseResolvedData> phaseResolvedDataList = null;
            try
            {
                PDM_DataComponent_PhaseResolvedData phaseResolvedData;
                int totalMatrices;
                int matricesExtracted;
                int offset;
                int numberOfArchiveRecords;

                if (archivedRecords != null)
                {
                    numberOfArchiveRecords = archivedRecords.Length;

                    if (numberOfArchiveRecords % 3072 == 0)
                    {
                        if (channelIsActive != null)
                        {
                            if (channelIsActive.Length >= 15)
                            {
                                phaseResolvedDataList = new List<PDM_DataComponent_PhaseResolvedData>();

                                totalMatrices = numberOfArchiveRecords / 3072;
                                matricesExtracted = 0;
                                offset = 0;
                                for (int i = 0; i < 15; i++)
                                {
                                    if (channelIsActive[i])
                                    {
                                        if (matricesExtracted < totalMatrices)
                                        {
                                            phaseResolvedData = ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(archivedRecords, offset, "Plus", i + 1);
                                            if (phaseResolvedData != null)
                                            {
                                                phaseResolvedDataList.Add(phaseResolvedData);
                                            }

                                            phaseResolvedData = ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(archivedRecords, offset + 1536, "Minus", i + 1);
                                            if (phaseResolvedData != null)
                                            {
                                                phaseResolvedDataList.Add(phaseResolvedData);
                                            }
                                            offset += 3072;
                                            matricesExtracted++;
                                        }
                                        else
                                        {
                                            string errorMessage = "Error in Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[]):\nAt least one channel did not have a matrix present, broke from loop.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[]):\nInput bool[] had too few elements.";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[]):\nInput bool[] was null.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[])\nInput Int32 did not have the correct number of elements, which must be a multiple of 3072.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[])\nInput Int32[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedData(Int32[], bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedDataList;
        }

        public static PDM_DataComponent_PhaseResolvedData ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(Int32[] archivedRecords, int startIndex, string phasePolarity, int channelNumber)
        {
            PDM_DataComponent_PhaseResolvedData phaseResolvedData = new PDM_DataComponent_PhaseResolvedData();
            try
            {
                StringBuilder matrixEntries = new StringBuilder();
                bool atLeastOneNonZeroEntryWasPresent = false;
                int matrixEntryValue;

                if (archivedRecords != null)
                {
                    if (archivedRecords.Length >= (startIndex + 1536))
                    {
                        phaseResolvedData.ChannelNumber = channelNumber;
                        phaseResolvedData.PhasePolarity = phasePolarity;

                        for (int row = 0; row < 32; row++)
                        {
                            for (int column = 0; column < 48; column++)
                            {
                                matrixEntryValue = archivedRecords[startIndex + (48 * row) + column];
                                if (matrixEntryValue > 0)
                                {
                                    atLeastOneNonZeroEntryWasPresent = true;
                                    matrixEntries.Append((row + 1).ToString() + "," + (column + 1).ToString() + "," + matrixEntryValue.ToString() + ";");
                                }
                            }
                        }
                        if (atLeastOneNonZeroEntryWasPresent)
                        {
                            phaseResolvedData.NonZeroEntries = matrixEntries.ToString();
                        }
                        else
                        {
                            phaseResolvedData = null;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(Int32[], int, string, int)\nInput Int32[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(Int32[], int, string, int)\nInput Int32[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ConvertArchiveDataToPhaseResolvedDataForOneChannelAndOnePolarity(Int32[], int, string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return phaseResolvedData;
        }

        public List<String> OutputParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("Parameters Values");
                if (parameters != null)
                {
                    outputList.Add("ReadingDateTime: " + this.readingDateTime.ToString());
                    outputList.Add("Tempfile: " + parameters.Tempfile.ToString());
                    outputList.Add("PDExists: " + parameters.PDExists.ToString());
                    outputList.Add("AlarmEnable: " + parameters.AlarmEnable.ToString());
                    outputList.Add("Initial: " + parameters.Initial.ToString());
                    outputList.Add("Humidity: " + parameters.Humidity.ToString());
                    outputList.Add("Temperature: " + parameters.Temperature.ToString());
                    outputList.Add("ExtLoadActive: " + parameters.ExtLoadActive.ToString());
                    outputList.Add("ExtLoadReactive: " + parameters.ExtLoadReactive.ToString());
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.OutputParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<String> OutputPDParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("PDParameters Values");
                if (pdParameters != null)
                {
                    outputList.Add("ReadingDateTime: " + this.readingDateTime.ToString());
                    outputList.Add("Channels: " + pdParameters.Channels);
                    outputList.Add("Matrix: " + pdParameters.Matrix.ToString());
                    outputList.Add("MaxPDIChannel: " + pdParameters.MaxPDIChannel.ToString());
                    outputList.Add("MaxQ02Channel: " + pdParameters.MaxQ02Channel.ToString());
                    outputList.Add("NumberOfPeriods: " + pdParameters.NumberOfPeriods.ToString());
                    outputList.Add("Frequency: " + pdParameters.Frequency.ToString());
                    outputList.Add("PSpeed_0: " + pdParameters.PSpeed_0.ToString());
                    outputList.Add("PSpeed_1: " + pdParameters.PSpeed_1.ToString());
                    outputList.Add("PJump_0: " + pdParameters.PJump_0.ToString());
                    outputList.Add("PJump_1: " + pdParameters.PJump_1.ToString());
                    outputList.Add("State: " + pdParameters.State.ToString());
                    outputList.Add("ZoneWidth: " + pdParameters.ZoneWidth.ToString());
                    outputList.Add("ZonesCount: " + pdParameters.ZonesCount.ToString());
                    outputList.Add("AngleStep: " + pdParameters.AngleStep.ToString());
                    outputList.Add("AnglesCount: " + pdParameters.AnglesCount.ToString());
                    outputList.Add("TablesOnChannel: " + pdParameters.TablesOnChannel.ToString());
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.OutputPDParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<String> OutputChPDParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("Entries for all Channels");

                if (chPDParameters != null)
                {
                    foreach (PDM_DataComponent_ChPDParameters chPDParametersEntry in this.chPDParameters)
                    {
                        outputList.Add("PDParameters Values");

                        outputList.Add("FromChannel: " + chPDParametersEntry.FromChannel.ToString());
                        outputList.Add("WithChannels_0: " + chPDParametersEntry.WithChannels_0.ToString());
                        outputList.Add("ChannelSensitivity: " + chPDParametersEntry.ChannelSensitivity.ToString());
                        outputList.Add("Q02: " + chPDParametersEntry.Q02.ToString());
                        outputList.Add("Q02Plus: " + chPDParametersEntry.Q02Plus.ToString());
                        outputList.Add("Q02Minus: " + chPDParametersEntry.Q02Minus.ToString());
                        outputList.Add("Q02mv: " + chPDParametersEntry.Q02mV.ToString());
                        outputList.Add("PDI: " + chPDParametersEntry.PDI.ToString());
                        outputList.Add("PDIPlus: " + chPDParametersEntry.PDIPlus.ToString());
                        outputList.Add("PDIMinus: " + chPDParametersEntry.PDIMinus.ToString());
                        outputList.Add("SumPDAmpl: " + chPDParametersEntry.SumPDAmpl.ToString());
                        outputList.Add("SumPDPlus: " + chPDParametersEntry.SumPDPlus.ToString());
                        outputList.Add("SumPDMinus: " + chPDParametersEntry.SumPDMinus.ToString());
                        outputList.Add("PDI_t: " + chPDParametersEntry.PDI_t.ToString());
                        outputList.Add("PDI_j: " + chPDParametersEntry.PDI_j.ToString());
                        outputList.Add("Q02_t: " + chPDParametersEntry.Q02_t.ToString());
                        outputList.Add("Q02_j: " + chPDParametersEntry.Q02_j.ToString());
                        outputList.Add("Separations_0: " + chPDParametersEntry.Separations_0.ToString());
                        outputList.Add("RefShift_0: " + chPDParametersEntry.RefShift_0.ToString());
                        outputList.Add("WasBlockedByT: " + chPDParametersEntry.WasBlockedByT.ToString());
                        outputList.Add("WasBlockedByP: " + chPDParametersEntry.WasBlockedByP.ToString());
                        outputList.Add("RatedVoltage: " + chPDParametersEntry.RatedVoltage.ToString());
                        outputList.Add("RatedCurrent: " + chPDParametersEntry.RatedCurrent.ToString());
                        outputList.Add("State: " + chPDParametersEntry.State.ToString());
                        outputList.Add("AlarmStatus: " + chPDParametersEntry.AlarmStatus.ToString());
                        outputList.Add("P_0: " + chPDParametersEntry.P_0.ToString());
                        outputList.Add("P_1: " + chPDParametersEntry.P_1.ToString());
                        outputList.Add("P_2: " + chPDParametersEntry.P_2.ToString());
                        outputList.Add("P_3: " + chPDParametersEntry.P_3.ToString());
                        outputList.Add("P_4: " + chPDParametersEntry.P_4.ToString());
                        outputList.Add("P_5: " + chPDParametersEntry.P_5.ToString());
                        outputList.Add("------End of channel entry------");
                    }
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.OutputChPDParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<String> OutputPhaseResolvedDataValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("Phase Resolved Data Entries for all Channels");

                if (phaseResolvedData != null)
                {
                    foreach (PDM_DataComponent_PhaseResolvedData phaseResolvedDataEntry in this.phaseResolvedData)
                    {
                        outputList.Add("PhaseResolvedData Values");

                        outputList.Add("ChannelNumber: " + phaseResolvedDataEntry.ChannelNumber.ToString());
                        outputList.Add("PhasePolarity: " + phaseResolvedDataEntry.PhasePolarity);
                        outputList.Add("NonZeroEntries: " + phaseResolvedDataEntry.NonZeroEntries);
                        outputList.Add("------End of phase resolved data entry------");
                    }
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.OutputPhaseResolvedDataValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public bool MatrixWasRecorded()
        {
            bool recorded = false;
            try
            {
                if ((phaseResolvedData != null) && (phaseResolvedData.Count > 0))
                {
                    recorded = true;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.MatrixWasRecorded()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return recorded;
        }

        public int GetAlarmStateForAllChannels()
        {
            int alarmState = -1;
            try
            {
                if (EnoughMembersAreCorrect())
                {
                    alarmState = 0;
                    if (this.chPDParameters != null)
                    {
                        foreach (PDM_DataComponent_ChPDParameters entry in chPDParameters)
                        {
                            if (entry.AlarmStatus > alarmState)
                            {
                                alarmState = entry.AlarmStatus;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.GetAlarmStateForAllChannels()\nPrivate data this.chPDParameters was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.GetAlarmStateForAllChannels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        public string GetAlarmSourceForAllChannels()
        {
            StringBuilder alarmSource = new StringBuilder();
            try
            {
                bool[] channelIsActive;
                int j = 0;
                if (this.pdParameters != null)
                {
                    channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
                    if (channelIsActive != null)
                    {
                        if (channelIsActive.Length >= 15)
                        {
                            if (EnoughMembersAreCorrect())
                            {
                                for (int i = 0; i < 15; i++)
                                {
                                    if (channelIsActive[i])
                                    {
                                        if (chPDParameters[j].FromChannel == (i + 1))
                                        {
                                            if (chPDParameters[j].AlarmStatus > 0)
                                            {
                                                alarmSource = alarmSource.Insert(0, "1");
                                            }
                                            else
                                            {
                                                alarmSource = alarmSource.Insert(0, "0");
                                            }
                                            j++;
                                        }
                                       /* else
                                        {
                                            System.Diagnostics.Debugger.Break();
                                            string errorMessage = "Error in PDM_SingleDataReading.GetAlarmSourceForAllChannels()\nIndices mismatch, channel data is not correctly organized.";
                                            LogMessage.LogError(errorMessage);
#if DEBUG
                                            MessageBox.Show(errorMessage);
#endif                                            
                                        }*/
                                    }
                                    else
                                    {
                                        alarmSource = alarmSource.Insert(0, "0");
                                    }
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in PDM_SingleDataReading.GetAlarmSourceForAllChannels()\nInternal computation of the active channels had an incorrect number of elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif                                        
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.GetAlarmSourceForAllChannels()\nInternal computation of the active channels returned a null array.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif                         
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.GetAlarmSourceForAllChannels()\nPrivate data this.pdParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.GetAlarmSourceForAllChannels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmSource.ToString();
        }

        private Boolean[] ConvertBitEncodingOfActiveChannelsToBoolean(UInt16 bitEncodedActiveChannels)
        {
            Boolean[] channelIsActive = new Boolean[16];
            try
            {
                int[] powersOfTwo = new int[16] { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
                for (int i = 0; i < 16; i++)
                {
                    if ((bitEncodedActiveChannels & powersOfTwo[i]) != 0)
                    {
                        channelIsActive[15 - i] = true;
                    }
                    else
                    {
                        channelIsActive[15 - i] = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ConvertBitEncodingOfActiveChannelsToBoolean(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelIsActive;
        }

        private Boolean[] ConvertStringBitEncodingOfActiveChannelsToBoolean(string stringRepresentationOfBits)
        {
            Boolean[] channelIsActive = null;
            try
            {
                int count;
                if (stringRepresentationOfBits != null)
                {
                    count = stringRepresentationOfBits.Length;
                    if (count == 16)
                    {
                        channelIsActive = new Boolean[16];
                        for (int i = 0; i < 16; i++)
                        {
                            if (stringRepresentationOfBits[i].CompareTo('1') == 0)
                            {
                                channelIsActive[15 - i] = true;
                            }
                            else if (stringRepresentationOfBits[i].CompareTo('0') == 0)
                            {
                                channelIsActive[15 - i] = false;
                            }
                            else
                            {
                                string errorMessage = "Error in PDM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string):\nInput string contains wrong character, expected 0 or 1, got " + stringRepresentationOfBits[i];
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }  
                    else
                    {
                        string errorMessage = "Error in PDM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string):\nInput string does not have 16 characters in it.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in PDM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string):\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelIsActive;
        }
    }
}
