﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DataObjects
{
    public class BHM_SingleDataReading
    {
        public static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 288;

        public DateTime readingDateTime;

        public BHM_DataComponent_InsulationParameters insulationParameters;
        public BHM_DataComponent_TransformerSideParameters transformerSideParametersSideOne;
        public BHM_DataComponent_TransformerSideParameters transformerSideParametersSideTwo;
        public BHM_DataComponent_SideToSideData sideToSideData;

        /// <summary>
        /// Creates an instance of the class using the byte data read by function 72 method or from a file saved by IHM
        /// </summary>
        /// <param name="byteData"></param>
        /// <param name="monitorID"></param>
        public BHM_SingleDataReading(Byte[] byteData)
        {
            try
            {
                Byte[] measurements = null;
                int transformerSideCount;

                if (byteData != null)
                {
                    int length = byteData.Length;

                    if ((length == 288) || (length == 16137))
                    {
                        if (length > 288)
                        {
                            measurements = new Byte[288];
                            Array.Copy(byteData, 189, measurements, 0, 288);
                        }
                        else
                        {
                            measurements = byteData;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.BHM_SingleDataReading.BHM_SingleDataReading(Byte[]):\nInput array is incorrect.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        //MessageBox.Show(errorMessage);
#endif
                    }

                    if (measurements != null)
                    {
                        insulationParameters = ExtractInsulationParametersFromByteData(measurements);
                        if (insulationParameters != null)
                        {
                            /// this value appears to be bit coded, since when both sides were active in a data set the value was 3, and in a case when only side one
                            /// was active, the value was 1
                            transformerSideCount = insulationParameters.TrSideCount;

                            if ((transformerSideCount == 1) || (transformerSideCount == 3))
                            {
                                transformerSideParametersSideOne = ExtractTransformerSideParametersFromByteData(measurements, 0);
                            }
                            if ((transformerSideCount == 2) || (transformerSideCount == 3))
                            {
                                transformerSideParametersSideTwo = ExtractTransformerSideParametersFromByteData(measurements, 1);
                            }
                        }

                        if (!EnoughMembersAreNonNull())
                        {
                            SetAllMembersToNull();
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleDataReading.BHM_SingleDataReading(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.BHM_SingleDataReading(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public BHM_SingleDataReading(DateTime argReadingDateTime, BHM_DataComponent_InsulationParameters argInsulationParameters, BHM_DataComponent_TransformerSideParameters argTransformerSideParametersSideOne,
            BHM_DataComponent_TransformerSideParameters argTransformerSideParametersSideTwo, BHM_DataComponent_SideToSideData argSideToSideData)
        {
            try
            {
                DateTime dataReadingDate = ConversionMethods.MinimumDateTime();

                this.readingDateTime = argReadingDateTime;
                insulationParameters = argInsulationParameters;
                transformerSideParametersSideOne = argTransformerSideParametersSideOne;
                transformerSideParametersSideTwo = argTransformerSideParametersSideTwo;
                sideToSideData = argSideToSideData;

                if (!EnoughMembersAreNonNull())
                {
                    SetAllMembersToNull();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.BHM_SingleDataReading(DateTime, BHM_DataComponent_InsulationParameters, BHM_DataComponent_TransformerSideParameters, BHM_DataComponent_TransformerSideParameters, BHM_DataComponent_SideToSideData)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //public static List<BHM_SingleDataReading> GetAllDataForOneMonitor(Guid monitorID, MonitorInterfaceDB db)
        //{
        //    return GetAllDataForOneMonitor(monitorID, ConversionMethods.MinimumDateTime(), db);
        //}

        //public static List<BHM_SingleDataReading> GetAllDataForOneMonitor(Guid monitorID, DateTime minimumDateTime, MonitorInterfaceDB db)
        //{
        //    List<BHM_SingleDataReading> bhmSingleDataReadingsList = new List<BHM_SingleDataReading>();

        //    //int allDataRootTableEntriesTableEntriesIndex = 0;
        //    //int allInsulationParametersTableEntriesIndex = 0;
        //    int allTransformerSideParametersTableEntriesIndex = 0;
        //    //int allSideToSideDataTableEntriesIndex = 0;

        //    int measurmentNumber;

        //    List<BHM_Data_DataRoot> allDataRootTableEntriesForOneMonitor = DatabaseInterface.BHM_Data_GetAllDataRootTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<BHM_Data_InsulationParameters> allInsulationParametersTableEntriesForOneMonitor = DatabaseInterface.BHM_Data_GetAllInsulationParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    List<BHM_Data_TransformerSideParameters> allTransformerSideParametersTableEntriesForOneMonitor = DatabaseInterface.BHM_Data_GetAllTransformerSideParametersTableEntriesForOneMonitor(monitorID, minimumDateTime, db);
        //    //List<BHM_Data_SideToSideData> allSideToSideDataForOneMonitor = DatabaseInterface.BHM_Data_GetAllSideToSideDataTableEntriesForOneMonitor(monitorID, db);

        //    BHM_Data_DataRoot dataRootForOneMeasurement;
        //    BHM_Data_InsulationParameters insulationParametersForOneMeasurement;
        //   // BHM_Data_SideToSideData sideToSide
        //    BHM_Data_TransformerSideParameters transformerSideParametersSideOneForOneMeausrement = null;
        //    BHM_Data_TransformerSideParameters transformerSideParametersSideTwoForOneMeausrement = null;

        //    BHM_SingleDataReading singleDataReading;

        //    DateTime currentDateTime;

        //    int totalMeasurements = allDataRootTableEntriesForOneMonitor.Count;
        //    int totalTransformerSideParametersEntries = allTransformerSideParametersTableEntriesForOneMonitor.Count;


        //    /// All of these algorithms depend on the fact that we acquire all data sorted by date
        //    for (measurmentNumber = 0; measurmentNumber < totalMeasurements; measurmentNumber++)
        //    {
        //        currentDateTime = allDataRootTableEntriesForOneMonitor[measurmentNumber].ReadingDateTime;
        //        dataRootForOneMeasurement = allDataRootTableEntriesForOneMonitor[measurmentNumber];
        //        insulationParametersForOneMeasurement = allInsulationParametersTableEntriesForOneMonitor[measurmentNumber];
        //        /// this loop depends on the fact that the data are sorted first by date.  while the data are also sorted by side number, 
        //        /// we don't worry about that as much since I am not sure if the data for a given side can be missing for some reason.  this
        //        /// way it doesn't matter.
        //        while ((allTransformerSideParametersTableEntriesIndex < totalTransformerSideParametersEntries) &&
        //               (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].ReadingDateTime.CompareTo(currentDateTime) == 0))
        //        {
        //            if (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].SideNumber == 0)
        //            {
        //                transformerSideParametersSideOneForOneMeausrement = allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex];
        //            }
        //            else if (allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex].SideNumber == 1)
        //            {
        //                transformerSideParametersSideTwoForOneMeausrement = allTransformerSideParametersTableEntriesForOneMonitor[allTransformerSideParametersTableEntriesIndex];
        //            }
        //            allTransformerSideParametersTableEntriesIndex++;
        //        }

        //        singleDataReading = new BHM_SingleDataReading(true, dataRootForOneMeasurement, insulationParametersForOneMeasurement, transformerSideParametersSideOneForOneMeausrement,
        //            transformerSideParametersSideTwoForOneMeausrement, null);

        //        if (singleDataReading.EnoughMembersAreNonNull())
        //        {
        //            bhmSingleDataReadingsList.Add(singleDataReading);
        //        }
        //    }
        //    return bhmSingleDataReadingsList;
        //}

    
        //private bool AlreadyInDatabase(MonitorInterfaceDB db)
        //{
        //    bool alreadyInDataBase = false;
        //    /// here one would try to retrieve all object using their keys, and if you can get at least one, this method
        //    /// would return true.  however, this is a really expensive way to do business.

        //    return alreadyInDataBase;
        //}

        private void SetAllMembersToNull()
        {            
            insulationParameters = null;
            transformerSideParametersSideOne = null;
            transformerSideParametersSideTwo = null;
            sideToSideData = null;

            string errorMessage = "Error in BHM_SingleDataReading.SetAllMembersToNull()\nData item did not have a minimum number of non-null entries, set all entries to null.";
            LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
        }

        public bool EnoughMembersAreNonNull()
        {
            bool enoughNonNull = true;
            try
            {
                if (insulationParameters == null)
                {
                    enoughNonNull = false;
                }

                if ((transformerSideParametersSideOne == null) && (transformerSideParametersSideTwo == null))
                {
                    enoughNonNull = false;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.EnoughMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return enoughNonNull;
        }

        private BHM_DataComponent_InsulationParameters ExtractInsulationParametersFromByteData(Byte[] byteData)
        {
            BHM_DataComponent_InsulationParameters insulationParameters = null;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        int day = byteData[0];
                        int month = byteData[1];
                        int year = byteData[2] + 2000;
                        int hour = byteData[3];
                        int minute = byteData[4];

                        DateTime localReadingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(month, day, year, hour, minute);
                        if (localReadingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                        {
                            string errorMessage = "Error in BHM_SingleDataReading.ExtractInsultationParametersFromByteData(Byte[])\nBad value for the DateTime, data assumed to be corrupt";
                            LogMessage.LogError(errorMessage);
                        }
                        else
                        {
                            insulationParameters = new BHM_DataComponent_InsulationParameters();

                            this.readingDateTime = localReadingDateTime;

                            insulationParameters.TrSideCount = ConversionMethods.ByteToInt32(byteData[5]);
                            // skip of many bytes

                            // I would not be surprised if these numbers need to be adjusted, but I don't know the factor needed
                            insulationParameters.RPN_0 = ConversionMethods.ByteToInt32(byteData[244]);
                            insulationParameters.RPN_1 = ConversionMethods.ByteToInt32(byteData[245]);

                            Single current_0 = ConversionMethods.BytesToSingle(byteData[246], byteData[247], byteData[248], byteData[249]);
                            current_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(current_0);
                            insulationParameters.Current_0 = current_0 / 10.0;

                            Single current_1 = ConversionMethods.BytesToSingle(byteData[250], byteData[251], byteData[252], byteData[253]);
                            current_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(current_1);
                            insulationParameters.Current_1 = current_1 / 10.0;

                            /// it appears all temperatures must be adjusted by 70 degrees (we moved this to an external adjustment,
                            /// as these are part of our Dynamics variables which are specified by the user)  It was requested that 
                            /// I put the adjustments back in here
                            insulationParameters.Temperature_0 = ConversionMethods.ByteToInt32(byteData[254]) - 70;
                            insulationParameters.Temperature_1 = ConversionMethods.ByteToInt32(byteData[255]) - 70;
                            insulationParameters.Temperature_2 = ConversionMethods.ByteToInt32(byteData[256]) - 70;
                            insulationParameters.Temperature_3 = ConversionMethods.ByteToInt32(byteData[257]) - 70;

                            // again, these numbers might need to be adjusted by some factor, but I cannot tell what it might be
                            Single currentR_0 = ConversionMethods.BytesToSingle(byteData[258], byteData[259], byteData[260], byteData[261]);
                            currentR_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentR_0);
                            insulationParameters.CurrentR_0 = currentR_0 / 10.0;

                            Single currentR_1 = ConversionMethods.BytesToSingle(byteData[262], byteData[263], byteData[264], byteData[265]);
                            currentR_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentR_1);
                            insulationParameters.CurrentR_1 = currentR_1 / 10.0;

                            insulationParameters.Humidity = ConversionMethods.ByteToInt32(byteData[266]);
                            insulationParameters.CalcZk = ConversionMethods.ByteToInt32(byteData[267]);
                            
                            // My understanding is that these are now voltages... all adjustments have to be done when reading the data at the data view level
                            // cfk made changes 11/22/13 to reserved variables.  went from bytetoInt32 to below as in PDM

                            insulationParameters.Reserved_0 = ConversionMethods.ShortBytesToInt16(byteData[270], byteData[271]);
                            insulationParameters.Reserved_1 = ConversionMethods.ShortBytesToInt16(byteData[272], byteData[273]);
                            insulationParameters.Reserved_2 = ConversionMethods.ShortBytesToInt16(byteData[274], byteData[275]);

                            // The rest of these are still undefined as far as I know DWL 11/29/13
                            insulationParameters.Reserved_3 = ConversionMethods.ShortBytesToInt16(byteData[276], byteData[277]);
                            insulationParameters.Reserved_4 = ConversionMethods.ShortBytesToInt16(byteData[278], byteData[279]);
                            insulationParameters.Reserved_5 = ConversionMethods.ShortBytesToInt16(byteData[280], byteData[281]);
                            insulationParameters.Reserved_6 = ConversionMethods.ShortBytesToInt16(byteData[282], byteData[283]);
                            insulationParameters.Reserved_7 = ConversionMethods.ShortBytesToInt16(byteData[284], byteData[285]);
                            insulationParameters.Reserved_8 = ConversionMethods.ShortBytesToInt16(byteData[286], byteData[287]);
                            insulationParameters.Reserved_9 = ConversionMethods.ShortBytesToInt16(byteData[288], byteData[289]);
                            insulationParameters.Reserved_10 = ConversionMethods.ShortBytesToInt16(byteData[290], byteData[291]);
                            insulationParameters.Reserved_11 = ConversionMethods.ShortBytesToInt16(byteData[292], byteData[293]);
                            insulationParameters.Reserved_12 = ConversionMethods.ShortBytesToInt16(byteData[294], byteData[295]);
                            insulationParameters.Reserved_13 = ConversionMethods.ShortBytesToInt16(byteData[296], byteData[297]);
                            insulationParameters.Reserved_14 = ConversionMethods.ShortBytesToInt16(byteData[298], byteData[299]);
                            insulationParameters.Reserved_15 = ConversionMethods.ShortBytesToInt16(byteData[300], byteData[301]);
                            insulationParameters.Reserved_16 = ConversionMethods.ShortBytesToInt16(byteData[302], byteData[303]);
                            insulationParameters.Reserved_17 = ConversionMethods.ShortBytesToInt16(byteData[304], byteData[305]);
                            insulationParameters.Reserved_18 = ConversionMethods.ShortBytesToInt16(byteData[306], byteData[307]);
                            insulationParameters.Reserved_19 = ConversionMethods.ShortBytesToInt16(byteData[308], byteData[309]);
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_SingleDataReading.ExtractInsulationParametersFromByteData(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleDataReading.ExtractInsulationParametersFromByteData(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.EnoughMembersAreNonNull()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return insulationParameters;
        }

        private BHM_DataComponent_TransformerSideParameters ExtractTransformerSideParametersFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_DataComponent_TransformerSideParameters transformerSideParameters = null;
            try
            {
                int offset = 0;
                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        if (sideNumber == 0)
                        {
                            offset = 6;
                        }
                        else if (sideNumber == 1)
                        {
                            offset = 67;
                        }

                        if (offset != 0)
                        {
                            transformerSideParameters = new BHM_DataComponent_TransformerSideParameters();

                            transformerSideParameters.SideNumber = sideNumber;

                            transformerSideParameters.SourceAmplitude_0 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset], byteData[offset + 1]) / 10.0;
                            transformerSideParameters.SourceAmplitude_1 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 2], byteData[offset + 3]) / 10.0;
                            transformerSideParameters.SourceAmplitude_2 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 4], byteData[offset + 5]) / 10.0;

                            /// I guessed that these need to be divided by 10, I don't know if that's true
                            transformerSideParameters.PhaseAmplitude_0 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 6], byteData[offset + 7]) / 10.0;
                            transformerSideParameters.PhaseAmplitude_1 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 8], byteData[offset + 9]) / 10.0;
                            transformerSideParameters.PhaseAmplitude_2 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 10], byteData[offset + 11]) / 10.0;

                            transformerSideParameters.Gamma = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 12], byteData[offset + 13]) / 100.0;
                            transformerSideParameters.GammaPhase = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 14], byteData[offset + 15]) / 100.0;

                            transformerSideParameters.KT = ConversionMethods.ByteToInt32(byteData[offset + 16]);
                            transformerSideParameters.AlarmStatus = ConversionMethods.ByteToInt32(byteData[offset + 17]);

                            transformerSideParameters.RemainingLife_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 18], byteData[offset + 19]);
                            transformerSideParameters.RemainingLife_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 20], byteData[offset + 21]);
                            transformerSideParameters.RemainingLife_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 22], byteData[offset + 23]);

                            transformerSideParameters.DefectCode = ConversionMethods.ByteToInt32(byteData[offset + 24]);
                            transformerSideParameters.ChPhaseShift = ConversionMethods.ShortBytesToInt16(byteData[offset + 25], byteData[offset + 26]);

                            transformerSideParameters.Trend = ConversionMethods.ByteToInt32(byteData[offset + 27]);
                            transformerSideParameters.KTPhase = ConversionMethods.ByteToInt32(byteData[offset + 28]);

                            transformerSideParameters.SignalPhase_0 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 29], byteData[offset + 30]) / 100.0;
                            transformerSideParameters.SignalPhase_1 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 31], byteData[offset + 32]) / 100.0;
                            transformerSideParameters.SignalPhase_2 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 33], byteData[offset + 34]) / 100.0;

                            transformerSideParameters.SourcePhase_0 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 35], byteData[offset + 36]) / 100.0;
                            transformerSideParameters.SourcePhase_1 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 37], byteData[offset + 38]) / 100.0;
                            transformerSideParameters.SourcePhase_2 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 39], byteData[offset + 40]) / 100.0;

                            Single frequency = ConversionMethods.BytesToSingle(byteData[offset + 41], byteData[offset + 42], byteData[offset + 43], byteData[offset + 44]);
                            frequency = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(frequency);
                            transformerSideParameters.Frequency = frequency;

                            transformerSideParameters.Temperature = ConversionMethods.ByteToInt32(byteData[offset + 45]) - 70;

                            transformerSideParameters.C_0 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 46], byteData[offset + 47]) / 10.0;
                            transformerSideParameters.C_1 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 48], byteData[offset + 49]) / 10.0;
                            transformerSideParameters.C_2 = ConversionMethods.UnsignedShortBytesToDouble(byteData[offset + 50], byteData[offset + 51]) / 10.0;

                            transformerSideParameters.Tg_0 = ConversionMethods.ShortBytesToDouble(byteData[offset + 52], byteData[offset + 53]) / 100.0;
                            transformerSideParameters.Tg_1 = ConversionMethods.ShortBytesToDouble(byteData[offset + 54], byteData[offset + 55]) / 100.0;
                            transformerSideParameters.Tg_2 = ConversionMethods.ShortBytesToDouble(byteData[offset + 56], byteData[offset + 57]) / 100.0;

                            transformerSideParameters.ExternalSyncShift = ConversionMethods.UnsignedShortBytesToUInt16(byteData[offset + 58], byteData[offset + 59]);

                            transformerSideParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 60]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_SingleDataReading.ExtractTransformerSideParametersFromByteData(Byte[], int, Guid)\nIncorrect value for sideNumber";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_SingleDataReading.ExtractTransformerSideParametersFromByteData(Byte[], int, Guid)\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleDataReading.ExtractTransformerSideParametersFromByteData(Byte[], int, Guid)\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.ExtractTransformerSideParametersFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return transformerSideParameters;
        }

        private BHM_DataComponent_SideToSideData ExtractSideToSideDataFromByteData(Byte[] byteData, int sideNumber)
        {
            BHM_DataComponent_SideToSideData sideToSideData = null;
            try
            {
                int offset = 0;

                if (byteData != null)
                {
                    if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                    {
                        if (sideNumber == 0)
                        {
                            offset = 128;
                        }

                        if (offset != 0)
                        {
                            sideToSideData = new BHM_DataComponent_SideToSideData();

                            sideToSideData.SideNumber = sideNumber;

                            Single amplitude1_0 = ConversionMethods.BytesToSingle(byteData[offset], byteData[offset + 1], byteData[offset + 2], byteData[offset + 3]);
                            amplitude1_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude1_0);
                            sideToSideData.Amplitude1_0 = amplitude1_0;

                            Single amplitude1_1 = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                            amplitude1_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude1_1);
                            sideToSideData.Amplitude1_1 = amplitude1_1;

                            Single amplitude1_2 = ConversionMethods.BytesToSingle(byteData[offset + 8], byteData[offset + 9], byteData[offset + 10], byteData[offset + 11]);
                            amplitude1_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude1_2);
                            sideToSideData.Amplitude1_2 = amplitude1_2;

                            Single amplitude2_0 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            amplitude2_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude2_0);
                            sideToSideData.Amplitude2_0 = amplitude2_0;

                            Single amplitude2_1 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            amplitude2_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude2_1);
                            sideToSideData.Amplitude2_1 = amplitude2_1;

                            Single amplitude2_2 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);
                            amplitude2_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(amplitude2_2);
                            sideToSideData.Amplitude2_2 = amplitude2_2;

                            Single phaseShift_0 = ConversionMethods.BytesToSingle(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                            phaseShift_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(phaseShift_0);
                            sideToSideData.PhaseShift_0 = phaseShift_0;

                            Single phaseShift_1 = ConversionMethods.BytesToSingle(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                            phaseShift_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(phaseShift_1);
                            sideToSideData.PhaseShift_1 = phaseShift_1;

                            Single phaseShift_2 = ConversionMethods.BytesToSingle(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);
                            phaseShift_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(phaseShift_2);
                            sideToSideData.PhaseShift_2 = phaseShift_2;

                            Single currentAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 36], byteData[offset + 37], byteData[offset + 38], byteData[offset + 39]);
                            currentAmpl_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentAmpl_0);
                            sideToSideData.CurrentAmpl_0 = currentAmpl_0;

                            Single currentAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 40], byteData[offset + 41], byteData[offset + 42], byteData[offset + 43]);
                            currentAmpl_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentAmpl_1);
                            sideToSideData.CurrentAmpl_1 = currentAmpl_1;

                            Single currentAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 44], byteData[offset + 45], byteData[offset + 46], byteData[offset + 47]);
                            currentAmpl_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentAmpl_2);
                            sideToSideData.CurrentAmpl_2 = currentAmpl_2;

                            Single currentPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 48], byteData[offset + 49], byteData[offset + 50], byteData[offset + 51]);
                            currentPhase_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentPhase_0);
                            sideToSideData.CurrentPhase_0 = currentPhase_0;

                            Single currentPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 52], byteData[offset + 53], byteData[offset + 54], byteData[offset + 55]);
                            currentPhase_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentPhase_1);
                            sideToSideData.CurrentPhase_1 = currentPhase_1;

                            Single currentPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 56], byteData[offset + 57], byteData[offset + 58], byteData[offset + 59]);
                            currentPhase_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentPhase_2);
                            sideToSideData.CurrentPhase_2 = currentPhase_2;

                            Single zkAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 60], byteData[offset + 61], byteData[offset + 62], byteData[offset + 63]);
                            zkAmpl_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkAmpl_0);
                            sideToSideData.ZkAmpl_0 = zkAmpl_0;

                            Single zkAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 64], byteData[offset + 65], byteData[offset + 66], byteData[offset + 67]);
                            zkAmpl_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkAmpl_1);
                            sideToSideData.ZkAmpl_1 = zkAmpl_1;

                            Single zkAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 68], byteData[offset + 69], byteData[offset + 70], byteData[offset + 71]);
                            zkAmpl_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkAmpl_2);
                            sideToSideData.ZkAmpl_2 = zkAmpl_2;

                            Single zkPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 72], byteData[offset + 73], byteData[offset + 74], byteData[offset + 75]);
                            zkPhase_0 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkPhase_0);
                            sideToSideData.ZkPhase_0 = zkPhase_0;

                            Single zkPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 76], byteData[offset + 77], byteData[offset + 78], byteData[offset + 79]);
                            zkPhase_1 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkPhase_1);
                            sideToSideData.ZkPhase_1 = zkPhase_1;

                            Single zkPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 80], byteData[offset + 81], byteData[offset + 82], byteData[offset + 83]);
                            zkPhase_2 = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(zkPhase_2);
                            sideToSideData.ZkPhase_2 = zkPhase_2;

                            Single currentChannelShift = ConversionMethods.BytesToSingle(byteData[offset + 84], byteData[offset + 85], byteData[offset + 86], byteData[offset + 87]);
                            currentChannelShift = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(currentChannelShift);
                            sideToSideData.CurrentChannelShift = currentChannelShift;


                            //sideToSideData.Amplitude1_0 = ConversionMethods.BytesToSingle(byteData[offset], byteData[offset + 1], byteData[offset + 2], byteData[offset + 3]);
                            //sideToSideData.Amplitude1_1 = ConversionMethods.BytesToSingle(byteData[offset + 4], byteData[offset + 5], byteData[offset + 6], byteData[offset + 7]);
                            //sideToSideData.Amplitude1_2 = ConversionMethods.BytesToSingle(byteData[offset + 8], byteData[offset + 9], byteData[offset + 10], byteData[offset + 11]);

                            //sideToSideData.Amplitude2_0 = ConversionMethods.BytesToSingle(byteData[offset + 12], byteData[offset + 13], byteData[offset + 14], byteData[offset + 15]);
                            //sideToSideData.Amplitude2_1 = ConversionMethods.BytesToSingle(byteData[offset + 16], byteData[offset + 17], byteData[offset + 18], byteData[offset + 19]);
                            //sideToSideData.Amplitude2_2 = ConversionMethods.BytesToSingle(byteData[offset + 20], byteData[offset + 21], byteData[offset + 22], byteData[offset + 23]);

                            //sideToSideData.PhaseShift_0 = ConversionMethods.BytesToSingle(byteData[offset + 24], byteData[offset + 25], byteData[offset + 26], byteData[offset + 27]);
                            //sideToSideData.PhaseShift_1 = ConversionMethods.BytesToSingle(byteData[offset + 28], byteData[offset + 29], byteData[offset + 30], byteData[offset + 31]);
                            //sideToSideData.PhaseShift_2 = ConversionMethods.BytesToSingle(byteData[offset + 32], byteData[offset + 33], byteData[offset + 34], byteData[offset + 35]);

                            //sideToSideData.CurrentAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 36], byteData[offset + 37], byteData[offset + 38], byteData[offset + 39]);
                            //sideToSideData.CurrentAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 40], byteData[offset + 41], byteData[offset + 42], byteData[offset + 43]);
                            //sideToSideData.CurrentAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 44], byteData[offset + 45], byteData[offset + 46], byteData[offset + 47]);

                            //sideToSideData.CurrentPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 48], byteData[offset + 49], byteData[offset + 50], byteData[offset + 51]);
                            //sideToSideData.CurrentPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 52], byteData[offset + 53], byteData[offset + 54], byteData[offset + 55]);
                            //sideToSideData.CurrentPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 56], byteData[offset + 57], byteData[offset + 58], byteData[offset + 59]);

                            //sideToSideData.ZkAmpl_0 = ConversionMethods.BytesToSingle(byteData[offset + 60], byteData[offset + 61], byteData[offset + 62], byteData[offset + 63]);
                            //sideToSideData.ZkAmpl_1 = ConversionMethods.BytesToSingle(byteData[offset + 64], byteData[offset + 65], byteData[offset + 66], byteData[offset + 67]);
                            //sideToSideData.ZkAmpl_2 = ConversionMethods.BytesToSingle(byteData[offset + 68], byteData[offset + 69], byteData[offset + 70], byteData[offset + 71]);

                            //sideToSideData.ZkPhase_0 = ConversionMethods.BytesToSingle(byteData[offset + 72], byteData[offset + 73], byteData[offset + 74], byteData[offset + 75]);
                            //sideToSideData.ZkPhase_1 = ConversionMethods.BytesToSingle(byteData[offset + 76], byteData[offset + 77], byteData[offset + 78], byteData[offset + 79]);
                            //sideToSideData.ZkPhase_2 = ConversionMethods.BytesToSingle(byteData[offset + 80], byteData[offset + 81], byteData[offset + 82], byteData[offset + 83]);

                            //sideToSideData.CurrentChannelShift = ConversionMethods.BytesToSingle(byteData[offset + 84], byteData[offset + 85], byteData[offset + 86], byteData[offset + 87]);

                            sideToSideData.ZkPercent_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 88], byteData[offset + 89], byteData[offset + 90], byteData[offset + 91]);
                            sideToSideData.ZkPercent_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 92], byteData[offset + 93], byteData[offset + 94], byteData[offset + 95]);
                            sideToSideData.ZkPercent_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[offset + 96], byteData[offset + 97], byteData[offset + 98], byteData[offset + 99]);

                            sideToSideData.Reserved_0 = ConversionMethods.ByteToInt32(byteData[offset + 100]);
                            sideToSideData.Reserved_1 = ConversionMethods.ByteToInt32(byteData[offset + 101]);
                            sideToSideData.Reserved_2 = ConversionMethods.ByteToInt32(byteData[offset + 102]);
                            sideToSideData.Reserved_3 = ConversionMethods.ByteToInt32(byteData[offset + 103]);
                            sideToSideData.Reserved_4 = ConversionMethods.ByteToInt32(byteData[offset + 104]);
                            sideToSideData.Reserved_5 = ConversionMethods.ByteToInt32(byteData[offset + 105]);
                            sideToSideData.Reserved_6 = ConversionMethods.ByteToInt32(byteData[offset + 106]);
                            sideToSideData.Reserved_7 = ConversionMethods.ByteToInt32(byteData[offset + 107]);
                            sideToSideData.Reserved_8 = ConversionMethods.ByteToInt32(byteData[offset + 108]);
                            sideToSideData.Reserved_9 = ConversionMethods.ByteToInt32(byteData[offset + 109]);
                            sideToSideData.Reserved_10 = ConversionMethods.ByteToInt32(byteData[offset + 110]);
                            sideToSideData.Reserved_11 = ConversionMethods.ByteToInt32(byteData[offset + 111]);
                            sideToSideData.Reserved_12 = ConversionMethods.ByteToInt32(byteData[offset + 112]);
                            sideToSideData.Reserved_13 = ConversionMethods.ByteToInt32(byteData[offset + 113]);
                            sideToSideData.Reserved_14 = ConversionMethods.ByteToInt32(byteData[offset + 114]);
                            sideToSideData.Reserved_15 = ConversionMethods.ByteToInt32(byteData[offset + 115]);
                        }
                        else
                        {
                            string errorMessage = "Error in BHM_SingleDataReading.ExtractSideToSideDataFromByteData(Byte[], int, Guid)\nIncorrect value for sideNumber";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in BHM_SingleDataReading.ExtractSideToSideDataFromByteData(Byte[], int, Guid)\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleDataReading.ExtractSideToSideDataFromByteData(Byte[], int, Guid)\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.ExtractSideToSideDataFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return sideToSideData;
        }

        public List<String> OutputInsulationParametersValuesAsStrings()
        {
            List<String> outputList = new List<string>();
            try
            {
                if (insulationParameters != null)
                {
                    outputList.Add("Date: " + this.readingDateTime.ToString());
                    outputList.Add(" ");
                    outputList.Add("InsulationParameters Entries");


                    outputList.Add("TrSideCount: " + insulationParameters.TrSideCount.ToString());
                    outputList.Add("RPN_0: " + insulationParameters.RPN_0.ToString());
                    outputList.Add("RPN_1 " + insulationParameters.RPN_1.ToString());
                    outputList.Add("Current_0: " + insulationParameters.Current_0.ToString());
                    outputList.Add("Current_1: " + insulationParameters.Current_1.ToString());
                    outputList.Add("Temperature_0: " + insulationParameters.Temperature_0.ToString());
                    outputList.Add("Temperature_1: " + insulationParameters.Temperature_1.ToString());
                    outputList.Add("Temperature_2: " + insulationParameters.Temperature_2.ToString());
                    outputList.Add("Temperature_3: " + insulationParameters.Temperature_3.ToString());
                    outputList.Add("CurrentR_0: " + insulationParameters.CurrentR_0.ToString());
                    outputList.Add("CurrentR_1: " + insulationParameters.CurrentR_1.ToString());
                    outputList.Add("Humidity: " + insulationParameters.Humidity.ToString());
                    outputList.Add("CalcZk: " + insulationParameters.CalcZk.ToString());
                }
                else
                {
                    string errorMessage = "Error in BHM_SingleDataReading.OutputInsulationParametersValuesAsStrings()\nPrivate data insulationParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.OutputInsulationParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<string> OutputTransformerSideParametersValuesAsStrings()
        {
            List<string> outputList = new List<string>();
            try
            {
                outputList.AddRange(OutputTransformerSideParametersForOneSide(transformerSideParametersSideOne));
                outputList.AddRange(OutputTransformerSideParametersForOneSide(transformerSideParametersSideTwo));
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.OutputTransformerSideParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        private List<string> OutputTransformerSideParametersForOneSide(BHM_DataComponent_TransformerSideParameters argTransformerSideParameters)
        {
            List<string> outputList = new List<string>();
            try
            {
                if (argTransformerSideParameters != null)
                {
                    outputList.Add("TransformerSideParameters Values");

                    outputList.Add("SideNumber: " + argTransformerSideParameters.SideNumber.ToString());
                    outputList.Add("SourceAmplitude_0: " + argTransformerSideParameters.SourceAmplitude_0.ToString());
                    outputList.Add("SourceAmplitude_1: " + argTransformerSideParameters.SourceAmplitude_1.ToString());
                    outputList.Add("SourceAmplitude_2: " + argTransformerSideParameters.SourceAmplitude_2.ToString());
                    outputList.Add("Gamma: " + argTransformerSideParameters.Gamma.ToString());
                    outputList.Add("GammaPhase: " + argTransformerSideParameters.GammaPhase.ToString());
                    outputList.Add("KT: " + argTransformerSideParameters.KT.ToString());
                    outputList.Add("AlarmStatus: " + argTransformerSideParameters.AlarmStatus.ToString());
                    outputList.Add("RemainingLife_0: " + argTransformerSideParameters.RemainingLife_0.ToString());
                    outputList.Add("RemainingLife_1: " + argTransformerSideParameters.RemainingLife_1.ToString());
                    outputList.Add("RemainingLife_2: " + argTransformerSideParameters.RemainingLife_2.ToString());
                    outputList.Add("DefectCode: " + argTransformerSideParameters.DefectCode.ToString());
                    outputList.Add("ChPhaseShift: " + argTransformerSideParameters.ChPhaseShift.ToString());
                    outputList.Add("Trend: " + argTransformerSideParameters.Trend.ToString());
                    outputList.Add("KTPhase: " + argTransformerSideParameters.KTPhase.ToString());
                    outputList.Add("SignalPhase_0: " + argTransformerSideParameters.SignalPhase_0.ToString());
                    outputList.Add("SignalPhase_1: " + argTransformerSideParameters.SignalPhase_1.ToString());
                    outputList.Add("SignalPhase_2: " + argTransformerSideParameters.SignalPhase_2.ToString());
                    outputList.Add("SourcePhase_0: " + argTransformerSideParameters.SourcePhase_0.ToString());
                    outputList.Add("SourcePhase_1: " + argTransformerSideParameters.SourcePhase_1.ToString());
                    outputList.Add("SourcePhase_2: " + argTransformerSideParameters.SourcePhase_2.ToString());
                    outputList.Add("Frequency: " + argTransformerSideParameters.Frequency.ToString());
                    outputList.Add("Temperature: " + argTransformerSideParameters.Temperature.ToString());
                    outputList.Add("C_0: " + argTransformerSideParameters.C_0.ToString());
                    outputList.Add("C_1: " + argTransformerSideParameters.C_1.ToString());
                    outputList.Add("C_2: " + argTransformerSideParameters.C_2.ToString());
                    outputList.Add("tg_0: " + argTransformerSideParameters.Tg_0);
                    outputList.Add("tg_1: " + argTransformerSideParameters.Tg_1);
                    outputList.Add("tg_2: " + argTransformerSideParameters.Tg_2);
                    outputList.Add("ExternalSyncShift: " + argTransformerSideParameters.ExternalSyncShift);
                    outputList.Add("-------End of TransformerSideParameters Entry-----------");
                }
                else
                {
                    string errorMessage = "Exception thrown in BHM_SingleDataReading.OutputTransformerSideParametersForOneSide(BHM_DataComponent_TransformerSideParameters)\nInput BHM_DataComponent_TransformerSideParameters was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in BHM_SingleDataReading.OutputTransformerSideParametersForOneSide(BHM_DataComponent_TransformerSideParameters)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }
    }
}
