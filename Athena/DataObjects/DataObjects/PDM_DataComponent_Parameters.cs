﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class PDM_DataComponent_Parameters
    {
        public int Tempfile;
        public int PDExists;
        public int AlarmEnable;
        public int Initial;
        public int Humidity;
        public int Temperature;
        public double ExtLoadActive;
        public double ExtLoadReactive;
        public int CommonRegisters_0;
        public int CommonRegisters_1;
        public int CommonRegisters_2;
        public int CommonRegisters_3;
        public int CommonRegisters_4;
        public int CommonRegisters_5;
        public int CommonRegisters_6;
        public int CommonRegisters_7;
        public int CommonRegisters_8;
        public int CommonRegisters_9;
        public int CommonRegisters_10;
        public int CommonRegisters_11;
        public int CommonRegisters_12;
        public int CommonRegisters_13;
        public int CommonRegisters_14;
        public int CommonRegisters_15;
        public int CommonRegisters_16;
        public int CommonRegisters_17;
        public int CommonRegisters_18;
        public int CommonRegisters_19;
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
        public int Reserved_21;
        public int Reserved_22;
        public int Reserved_23;
        public int Reserved_24;
        public int Reserved_25;
        public int Reserved_26;
        public int Reserved_27;
        public int Reserved_28;
        public int Reserved_29;
        public int Reserved_30;
        public int Reserved_31;
        public int Reserved_32;
        public int Reserved_33;
        public int Reserved_34;
        public int Reserved_35;
        public int Reserved_36;
        public int Reserved_37;
        public int Reserved_38;
        public int Reserved_39;
        public int Reserved_40;
        public int Reserved_41;
        public int Reserved_42;
        public int Reserved_43;
    }
}
