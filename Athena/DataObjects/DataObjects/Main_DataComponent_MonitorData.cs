﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class Main_DataComponent_MonitorData
    {
        public DateTime ReadingDateTime;
        public int State;
        public int ProperWorkingOrder;
        public int Reserved_0;
        public int Reserved_1;
        public double VibrationOneValue;
        public int VibrationOneStatus;
        public double VibrationTwoValue;
        public int VibrationTwoStatus;
        public double VibrationThreeValue;
        public int VibrationThreeStatus;
        public double VibrationFourValue;
        public int VibrationFourStatus;
        public double PressureOneValue;
        public int PressureOneStatus;
        public double PressureTwoValue;
        public int PressureTwoStatus;
        public double PressureThreeValue;
        public int PressureThreeStatus;
        public double PressureFourValue;
        public int PressureFourStatus;
        public double PressureFiveValue;
        public int PressureFiveStatus;
        public double PressureSixValue;
        public int PressureSixStatus;
        public double CurrentOneValue;
        public int CurrentOneStatus;
        public double CurrentTwoValue;
        public int CurrentTwoStatus;
        public double CurrentThreeValue;
        public int CurrentThreeStatus;
        public double VoltageOneValue;
        public int VoltageOneStatus;
        public double VoltageTwoValue;
        public int VoltageTwoStatus;
        public double VoltageThreeValue;
        public int VoltageThreeStatus;
        public double HumidityValue;
        public int HumidityStatus;
        public double TemperatureOneValue;
        public int TemperatureOneStatus;
        public double TemperatureTwoValue;
        public int TemperatureTwoStatus;
        public double TemperatureThreeValue;
        public int TemperatureThreeStatus;
        public double TemperatureFourValue;
        public int TemperatureFourStatus;
        public double TemperatureFiveValue;
        public int TemperatureFiveStatus;
        public double TemperatureSixValue;
        public int TemperatureSixStatus;
        public double TemperatureSevenValue;
        public int TemperatureSevenStatus;
        public double ChassisTemperatureValue;
        public int ChassisTemperatureStatus;
    }
}
