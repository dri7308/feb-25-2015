﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class PDM_DataComponent_PhaseResolvedData
    {
        public int ChannelNumber;
        public string PhasePolarity;
        public string NonZeroEntries;
    }
}
