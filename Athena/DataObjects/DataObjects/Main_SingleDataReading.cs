﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GeneralUtilities;

namespace DataObjects
{
    public class Main_SingleDataReading
    {
        public Main_DataComponent_MonitorData monitorData;
        public Main_DataComponent_WindingHotSpotData whsData;

        int EXPECTED_DATA_LENGTH = 200;

        public Main_SingleDataReading(Int16[] registerData)
        {
            try
            {
                Int16[] processedRegisterData;
                if (registerData.Length >= 70)
                {
                    /// I guess this worked for some imported data, but not for some other data I saw
                    /// if (registerData.Length == 4316)
                    if(registerData.Length > 300)
                    {
                        processedRegisterData = new Int16[70];
                        Array.Copy(registerData, 0, processedRegisterData, 0, 70);
                    }
                    else
                    {
                        processedRegisterData = registerData;
                    }
                    monitorData = ExtractMonitorDataFromRegisterData(processedRegisterData);
                    if (processedRegisterData.Length >= 99)
                    {
                        whsData = ExtractWHSDataFromRegisterData(registerData);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_SingleDataReading.Main_SingleDataReading(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public Main_SingleDataReading(Main_DataComponent_MonitorData inputMonitorData, Main_DataComponent_WindingHotSpotData inputWindingHotSpotData)
        {
            monitorData = inputMonitorData;
            whsData = inputWindingHotSpotData;
        }

        public bool EnoughMembersAreNonNull()
        {
            if (monitorData != null)
            {
                return true;
            }
            return false;
        }

        public Main_DataComponent_MonitorData ExtractMonitorDataFromRegisterData(Int16[] registerData)
        {
            Main_DataComponent_MonitorData monitorData = null;
            try
            {
                int offset = 10;
                if (registerData != null)
                {
                    if (registerData.Length >= 70)
                    {
                        monitorData = new Main_DataComponent_MonitorData();

                        DateTime readingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime((int)registerData[offset], (int)registerData[offset + 1], (int)registerData[offset + 2],
                                                                                                    (int)registerData[offset + 3], (int)registerData[offset + 4], (int)registerData[offset+5]);

                        if (readingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0)
                        {
                            monitorData.ReadingDateTime = readingDateTime;

                            monitorData.State = registerData[offset + 6];
                            monitorData.ProperWorkingOrder = registerData[offset + 7];
                            monitorData.Reserved_0 = registerData[offset + 8];
                            monitorData.Reserved_1 = registerData[offset + 9];
                            monitorData.VibrationOneValue = registerData[offset + 10] / 100.0;
                            monitorData.VibrationOneStatus = registerData[offset + 11];
                            monitorData.VibrationTwoValue = registerData[offset + 12] / 100.0;
                            monitorData.VibrationTwoStatus = registerData[offset + 13];
                            monitorData.VibrationThreeValue = registerData[offset + 14] / 100.0;
                            monitorData.VibrationThreeStatus = registerData[offset + 15];
                            monitorData.VibrationFourValue = registerData[offset + 16] / 100.0;
                            monitorData.VibrationFourStatus = registerData[offset + 17];
                            monitorData.PressureOneValue = registerData[offset + 18] / 100.0;
                            monitorData.PressureOneStatus = registerData[offset + 19];
                            monitorData.PressureTwoValue = registerData[offset + 20] / 100.0;
                            monitorData.PressureTwoStatus = registerData[offset + 21];
                            monitorData.PressureThreeValue = registerData[offset + 22] / 100.0;
                            monitorData.PressureThreeStatus = registerData[offset + 23];
                            monitorData.PressureFourValue = registerData[offset + 24] / 100.0;
                            monitorData.PressureFourStatus = registerData[offset + 25];
                            monitorData.PressureFiveValue = registerData[offset + 26] / 100.0;
                            monitorData.PressureFiveStatus = registerData[offset + 27];
                            monitorData.PressureSixValue = registerData[offset + 28] / 100.0;
                            monitorData.PressureSixStatus = registerData[offset + 29];
                            monitorData.CurrentOneValue = registerData[offset + 30] / 10.0;
                            monitorData.CurrentOneStatus = registerData[offset + 31];
                            monitorData.CurrentTwoValue = registerData[offset + 32] / 10.0;
                            monitorData.CurrentTwoStatus = registerData[offset + 33];
                            monitorData.CurrentThreeValue = registerData[offset + 34] / 10.0;
                            monitorData.CurrentThreeStatus = registerData[offset + 35];
                            monitorData.VoltageOneValue = registerData[offset + 36] / 10.0;
                            monitorData.VoltageOneStatus = registerData[offset + 37];
                            monitorData.VoltageTwoValue = registerData[offset + 38] / 10.0;
                            monitorData.VoltageTwoStatus = registerData[offset + 39];
                            monitorData.VoltageThreeValue = registerData[offset + 40] / 10.0;
                            monitorData.VoltageThreeStatus = registerData[offset + 41];
                            monitorData.HumidityValue = registerData[offset + 42] / 10.0;
                            monitorData.HumidityStatus = registerData[offset + 43];
                            monitorData.TemperatureOneValue = registerData[offset + 44] / 10.0;
                            monitorData.TemperatureOneStatus = registerData[offset + 45];
                            monitorData.TemperatureTwoValue = registerData[offset + 46] / 10.0;
                            monitorData.TemperatureTwoStatus = registerData[offset + 47];
                            monitorData.TemperatureThreeValue = registerData[offset + 48] / 10.0;
                            monitorData.TemperatureThreeStatus = registerData[offset + 49];
                            monitorData.TemperatureFourValue = registerData[offset + 50] / 10.0;
                            monitorData.TemperatureFourStatus = registerData[offset + 51];
                            monitorData.TemperatureFiveValue = registerData[offset + 52] / 10.0;
                            monitorData.TemperatureFiveStatus = registerData[offset + 53];
                            monitorData.TemperatureSixValue = registerData[offset + 54] / 10.0;
                            monitorData.TemperatureSixStatus = registerData[offset + 55];
                            monitorData.TemperatureSevenValue = registerData[offset + 56] / 10.0;
                            monitorData.TemperatureSevenStatus = registerData[offset + 57];
                            monitorData.ChassisTemperatureValue = registerData[offset + 58] / 10.0;
                            monitorData.ChassisTemperatureStatus = registerData[offset + 59];
                        }
                        else
                        {
                            monitorData = null;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_SingleDataReading.ExtractMonitorDataFromRegisterData(Int16[])\nInput Int16[] did not have enough entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_SingleDataReading.ExtractMonitorDataFromRegisterData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_SingleDataReading.ExtractMonitorDataFromRegisterData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorData;
        }

        public Main_DataComponent_WindingHotSpotData ExtractWHSDataFromRegisterData(Int16[] registerData)
        {
            Main_DataComponent_WindingHotSpotData localWHSData = null;
            try
            {
                int offset = 70;
                if (registerData != null)
                {
                    if (registerData.Length >= 99)
                    {
                        localWHSData = new Main_DataComponent_WindingHotSpotData();

                        

                        localWHSData.MaxWindingTempPhaseA = registerData[offset + 0] / 10.0;
                        localWHSData.MaxWindingTempPhaseB = registerData[offset + 1] / 10.0;
                        localWHSData.MaxWindingTempPhaseC = registerData[offset + 2] / 10.0;
                        localWHSData.WhsMax = registerData[offset + 3] / 10.0;
                        localWHSData.WhsTopOilTemp = registerData[offset + 4] / 10.0;

                        localWHSData.AgingFactorA = ConversionMethods.Int16sToSingle(registerData[offset + 5], registerData[offset + 6]);
                        localWHSData.AgingFactorB = ConversionMethods.Int16sToSingle(registerData[offset + 7], registerData[offset + 8]);
                        localWHSData.AgingFactorC = ConversionMethods.Int16sToSingle(registerData[offset + 9], registerData[offset + 10]);

                        localWHSData.AccumulatedAgingA = ConversionMethods.Int16sToSingle(registerData[offset + 11], registerData[offset + 12]);
                        localWHSData.AccumulatedAgingB = ConversionMethods.Int16sToSingle(registerData[offset + 13], registerData[offset + 14]);
                        localWHSData.AccumulatedAgingC = ConversionMethods.Int16sToSingle(registerData[offset + 15], registerData[offset + 16]);
                        
                        localWHSData.WhsFan1Starts = registerData[offset + 19];
                        localWHSData.WhsFan2Starts = registerData[offset + 20];
                        UInt16 fahrs11 = (UInt16)registerData[offset + 21];
                        UInt16 fahrs12 = (UInt16)registerData[offset + 22];
                        UInt16 fahrs13 = (UInt16)registerData[offset + 23];
                        UInt16 fahrs14 = (UInt16)registerData[offset + 24];

                        localWHSData.WhsFan1Hours = (fahrs11 + (fahrs12 * Math.Pow(2, 16))) / 3600;
                        localWHSData.WhsFan2Hours = (fahrs13 + (fahrs14 * Math.Pow(2, 16))) / 3600;
                        localWHSData.AlarmSourceAsBitString = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits(ConversionMethods.Int16BytesToUInt16Value(registerData[offset + 23]));
                        localWHSData.ErrorSourceAsBitString = ConversionMethods.ConvertBitEncodingToStringRepresentationOfBits(ConversionMethods.Int16BytesToUInt16Value(registerData[offset + 24]));
                        localWHSData.Reserved = ConversionMethods.Int16BytesToUInt16Value(registerData[offset + 28]);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_SingleDataReading.ExtractWHSDataFromRegisterData(Int16[])\nInput Int16[] did not have enough entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_SingleDataReading.ExtractWHSDataFromRegisterData(Int16[])\nInput Int16[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_SingleDataReading.ExtractWHSDataFromRegisterData(Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return localWHSData;
        }
    }
}
