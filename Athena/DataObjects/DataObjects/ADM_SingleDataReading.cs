﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneralUtilities;
using System.Windows.Forms;

namespace DataObjects
{
    public class ADM_SingleDataReading
    {
        public static int EXPECTED_LENGTH_OF_BYTE_ARRAY = 2048;

        public DateTime readingDateTime;

        public ADM_DataComponent_Parameters parameters = null;
        public ADM_DataComponent_PDParameters pdParameters = null;
        public List<ADM_DataComponent_ChPDParameters> chPDParameters = null;

        private bool[] channelIsActive = null;
        private int activeChannelCount = 0;

        //public ADM_SingleDataReading(Byte[] byteData, Int32[] phaseResolvedDataAsArray)
        //{
        //    parameters = ExtractParametersFromByteData(byteData);

        //    pdParameters = ExtractPDParametersFromByteData(byteData);
        //    Boolean[] channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
        //    chPDParameters = ConvertOneRecordOfChannelDataToDbData(byteData, channelIsActive);
        //    if ((pdParameters.Matrix == 1) && phaseResolvedDataAsArray !=null)
        //    {
        //        phaseResolvedData = ConvertArchiveDataToPhaseResolvedData(phaseResolvedDataAsArray, channelIsActive);
        //    }           
        //}

        public ADM_SingleDataReading(Byte[] commonAndChannelData, Byte[] rawPhaseResolvedData)
        {
            try
            {
//                if (commonAndChannelData != null)
//                {
//                    if (InitializeCommonAndChannelDataObjects(commonAndChannelData))
//                    {
//                        if (parameters.PDExists == 1)
//                        {
//                            //                            if (rawPhaseResolvedData != null)
//                            //                            {
//                            //                                if (!InitializePhaseResolvedDataObjects(rawPhaseResolvedData))
//                            //                                {
//                            //                                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[], Byte[]):\nSomething was wrong with the rawPhaseResolvedData array, initialization failed.";
//                            //                                    LogMessage.LogError(errorMessage);
//                            //#if DEBUG
//                            //                                MessageBox.Show(errorMessage);
//                            //#endif
//                            //                                }
//                            //                            }
//                            //                            else
//                            //                            {
//                            //                                string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[], Byte[]):\nPhase resolved data was required, but input data was null.";
//                            //                                LogMessage.LogError(errorMessage);
//                            //#if DEBUG
//                            //                            MessageBox.Show(errorMessage);
//                            //#endif
//                            //                            }
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[], Byte[]):\nSomething was wrong with the commonAndChannelData array, initialization failed.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[], Byte[]):\nThe input commandAndChannelData was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ADM_SingleDataReading(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public ADM_SingleDataReading(Byte[] byteData)
        {
            try
            {
                //Byte[] commonAndChannelData = null;
                //Byte[] rawPhaseResolvedData = null;

                if (byteData != null)
                {
                    if (byteData.Length >= 560)
                    {
                        parameters = ExtractParametersFromByteData(byteData);
                        pdParameters = ExtractPDParametersFromByteData(byteData);

                        chPDParameters = ConvertOneRecordOfChannelDataToDbData(byteData);

                        //                        commonAndChannelData = ExtractRawCommonAndChannelDataFromFileData(byteData);
                        //                        if (commonAndChannelData != null)
                        //                        {
                        //                            if (InitializeCommonAndChannelDataObjects(commonAndChannelData))
                        //                            {
                        //                                if (parameters.PDExists == 1)
                        //                                {
                        //                                    rawPhaseResolvedData = ExtractRawPhaseResolvedDataFromFileData(byteData, this.activeChannelCount);

                        //                                    //                                    if (rawPhaseResolvedData != null)
                        //                                    //                                    {
                        //                                    //                                        if (!InitializePhaseResolvedDataObjects(rawPhaseResolvedData))
                        //                                    //                                        {
                        //                                    //                                            string errorMessage = "Error in DataObjects.ADM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nPhase resolved data should be present; either it was not present or the format was incorrect.";
                        //                                    //                                            LogMessage.LogError(errorMessage);
                        //                                    //#if DEBUG
                        //                                    //                                        MessageBox.Show(errorMessage);
                        //                                    //#endif
                        //                                    //                                        }
                        //                                    //                                    }
                        //                                    //                                    else
                        //                                    //                                    {
                        //                                    //                                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nPhase resolved data should be present; either it was not present or the format was incorrect.";
                        //                                    //                                        LogMessage.LogError(errorMessage);
                        //                                    //#if DEBUG
                        //                                    //                                    MessageBox.Show(errorMessage);
                        //                                    //#endif
                        //                                    //                                    }
                        //                                }
                        //                            }
                        //                            else
                        //                            {
                        //                                string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[]):\nFailed to initialize the commandAndChannelData objects.";
                        //                                LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                                MessageBox.Show(errorMessage);
                        //#endif

                        //                            }
                        //                        }
                        //                        else
                        //                        {
                        //                            string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[]):\nFailed to extract the commandAndChannelData from the input data.";
                        //                            LogMessage.LogError(errorMessage);
                        //#if DEBUG
                        //                            MessageBox.Show(errorMessage);
                        //#endif
                        //                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ADM_SingleDataReading(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ADM_SingleDataReading(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        private bool InitializeCommonAndChannelDataObjects(byte[] commonAndChannelData)
//        {
//            bool success = true;
//            try
//            {
//                parameters = ExtractParametersFromByteData(commonAndChannelData);
//                if (parameters == null)
//                {
//                    success = false;
//                }
//                if (success)
//                {
//                    pdParameters = ExtractPDParametersFromByteData(commonAndChannelData);
//                    if (pdParameters == null)
//                    {
//                        success = false;
//                    }
//                }
//                if (success)
//                {
//                    channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
//                    activeChannelCount = GetActiveChannelCount(channelIsActive);
//                    chPDParameters = ConvertOneRecordOfChannelDataToDbData(commonAndChannelData, channelIsActive);
//                    if (chPDParameters == null)
//                    {
//                        success = false;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ADM_SingleDataReading.InitializeCommonAndChannelDataObjects(Byte[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return success;
//        }

        private int GetActiveChannelCount(bool[] channelIsActive)
        {
            int activeChannelCount = 0;
            try
            {
                if (channelIsActive != null)
                {
                    if (channelIsActive.Length >= 15)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            if (channelIsActive[i])
                            {
                                activeChannelCount++;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_SingleDataReading.GetActiveChannelCount(bool[])\nInput bool[] has too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_SingleDataReading.GetActiveChannelCount(bool[])\nInput bool[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.GetActiveChannelCount(bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return activeChannelCount;
        }

//        private bool InitializeObjectUsingByteData(Byte[] commonAndChannelData, Byte[] rawPhaseResolvedData)
//        {
//            bool success = true;
//            try
//            {
//                if (commonAndChannelData != null)
//                {
//                    parameters = ExtractParametersFromByteData(commonAndChannelData);

//                    pdParameters = ExtractPDParametersFromByteData(commonAndChannelData);
//                    Boolean[] channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
//                    chPDParameters = ConvertOneRecordOfChannelDataToDbData(commonAndChannelData, channelIsActive);
//                    //                    if ((parameters.PDExists == 1) && (rawPhaseResolvedData != null))
//                    //                    {
//                    //                        Int32[] archivePhaseResolvedData = ConvertRawPhaseResolvedDataToArchiveFormat(rawPhaseResolvedData);                    
//                    //                    }
//                    //                    else
//                    //                    {
//                    //                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nPhase resolved data should be present, but the format was incorrect.";
//                    //                        LogMessage.LogError(errorMessage);
//                    //#if DEBUG
//                    //                    MessageBox.Show(errorMessage);
//                    //#endif
//                    //                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[]):\nInput data is incorrect.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in ADM_SingleDataReading.InitializeObjectUsingByteData(Byte[], Byte[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return success;
//        }

        public ADM_SingleDataReading(DateTime argReadingDateTime, ADM_DataComponent_Parameters argParamaters, ADM_DataComponent_PDParameters argPDParameters,
                                     List<ADM_DataComponent_ChPDParameters> argChPDParameters)
        {
            try
            {
                readingDateTime = argReadingDateTime;
                parameters = argParamaters;
                pdParameters = argPDParameters;
                chPDParameters = argChPDParameters;

                if (!EnoughMembersAreCorrect())
                {
                    SetAllMembersToNull();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ADM_SingleDataReading(DateTime, ADM_DataComponent_Parameters, ADM_DataComponent_PDParameters, List<ADM_DataComponent_ChPDParameters>)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        public bool InputDataHasCorrectLength(Byte[] byteData)
        {
            bool lengthIsCorrect = false;
            try
            {
                int length;

                if (byteData != null)
                {
                    length = byteData.Length;
                    if (InputDataLengthHasDeviceLength(length) || InputDataLengthHasFileLength(length))
                    {
                        lengthIsCorrect = true;
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_SingleDataReading.InputDataHasCorrectLength(Byte[])\nInput Byte[] was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.InputDataHasCorrectLength(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public bool InputDataLengthHasDeviceLength(int length)
        {
            bool lengthIsCorrect = true;
            try
            {
                if (length < 2048)
                {
                    lengthIsCorrect = false;
                }
                else if (length > 2048)
                {
                    if (((length - 2048) % 6144) != 0)
                    {
                        lengthIsCorrect = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.InputDataLengthHasDeviceLength(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public bool InputDataLengthHasFileLength(int length)
        {
            bool lengthIsCorrect = true;
            try
            {
                if (length < (2236 + 11994))
                {
                    lengthIsCorrect = false;
                }
                else if (length > (2236 + 11994))
                {
                    if (((length - (2236 + 11994)) % 7430) != 0)
                    {
                        lengthIsCorrect = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.InputDataLengthHasFileLength(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return lengthIsCorrect;
        }

        public static Byte[] ExtractRawCommonAndChannelDataFromFileData(Byte[] byteData)
        {
            Byte[] commonAndChannelData = null;
            try
            {
                int length;
                if (byteData != null)
                {
                    length = byteData.Length;
                    if (length >= 2236)
                    {
                        commonAndChannelData = new Byte[2048];
                        Array.Copy(byteData, 188, commonAndChannelData, 0, 2048);
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ExtractRawCommonAndChannelDataFromFileData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return commonAndChannelData;
        }

        public static Byte[] ExtractRawPhaseResolvedDataFromFileData(Byte[] byteData, int activeChannelCount)
        {
            Byte[] rawPhaseResolvedData = null;
            try
            {
                //int numberOfEntries;
                int startingIndexForCopying = 2236;
                int length;

                if (byteData != null)
                {
                    length = byteData.Length;

                    //int numberOfBytesContainingPhaseResolvedData = length - 2236 - 11994;
                    //if ((numberOfBytesContainingPhaseResolvedData % 7430) == 0)
                    if ((length - 2236 - (activeChannelCount * 7430)) >= 0)
                    {
                        rawPhaseResolvedData = new Byte[activeChannelCount * 6144];
                        for (int i = 0; i < activeChannelCount; i++)
                        {
                            Array.Copy(byteData, startingIndexForCopying, rawPhaseResolvedData, i * 6144, 6144);
                            startingIndexForCopying += 7430;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[]):\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in DataObjects.ADM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[]):\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ExtractRawPhaseResolvedDataFromFileData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return rawPhaseResolvedData;
        }

        public static bool PhaseResolvedDataWasSaved(Byte[] byteData)
        {
            bool wasSaved = false;
            try
            {
                if (byteData != null)
                {
                    if (byteData.Length >= 15)
                    {
                        if (byteData[14] > 0)
                        {
                            wasSaved = true;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in ADM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nInput Byte[] had too few elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nInput Byte[] was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.PhaseResolvedDataWasSaved(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return wasSaved;
        }

        //public Dictionary<int, int> GetMatrixCountsPerChannelForOneDataReading()
        //{
        //    Dictionary<int, int> matrixCountsPerChannel = new Dictionary<int, int>();
        //    int zeroIndexChannelNumber;
        //    foreach (ADM_DataComponent_PhaseResolvedData entry in this.phaseResolvedData)
        //    {
        //        zeroIndexChannelNumber = entry.ChannelNumber;
        //        if (matrixCountsPerChannel.ContainsKey(zeroIndexChannelNumber))
        //        {
        //            matrixCountsPerChannel[zeroIndexChannelNumber]++;
        //        }
        //        else
        //        {
        //            matrixCountsPerChannel.Add(zeroIndexChannelNumber, 1);
        //        }
        //    }
        //    return matrixCountsPerChannel;
        //}

        public static int GetMatrixCount(List<ADM_SingleDataReading> singleDataReadingsList)
        {
            int count = 0;

            foreach (ADM_SingleDataReading entry in singleDataReadingsList)
            {
                if (entry.pdParameters.Matrix > 0)
                {
                    count++;
                }
            }

            return count;
        }

        public bool EnoughMembersAreCorrect()
        {
            bool enoughAreCorrect = true;

            if ((parameters == null) || (pdParameters == null) || (chPDParameters == null))
            {
                enoughAreCorrect = false;
            }
            if (enoughAreCorrect && chPDParameters.Count == 0)
            {
                enoughAreCorrect = false;
            }
            return enoughAreCorrect;
        }

        private void SetAllMembersToNull()
        {
            parameters = null;
            pdParameters = null;
            chPDParameters = null;
        }

        private ADM_DataComponent_Parameters ExtractParametersFromByteData(Byte[] byteData)
        {
            ADM_DataComponent_Parameters parameters = new ADM_DataComponent_Parameters();
            try
            {
                DateTime localReadingDateTime = ConversionMethods.ConvertStringValuesToDateTime(ConversionMethods.ByteToInt32(byteData[1]).ToString(),
                                                                                               ConversionMethods.ByteToInt32(byteData[0]).ToString(),
                                                                                               (ConversionMethods.ByteToInt32(byteData[2]) + 2000).ToString(),
                                                                                               ConversionMethods.ByteToInt32(byteData[3]).ToString(),
                                                                                               ConversionMethods.ByteToInt32(byteData[4]).ToString());
                if (!(localReadingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) > 0))
                {
                    string errorMessage = "Error in ADM_SingleDataReading.ADM_ConvertOneArchivedCommonDataToDbData(Byte[], Guid)\nBad date from data, throwing away data point.";
                    LogMessage.LogError(errorMessage);
                    parameters = null;
                }
                else
                {
                    this.readingDateTime = localReadingDateTime;

                    parameters = new ADM_DataComponent_Parameters();

                    parameters.readingDateTime = this.readingDateTime;

                    parameters.Tempfile = ConversionMethods.ByteToInt32(byteData[5]);
                    parameters.PDExists = ConversionMethods.ByteToInt32(byteData[6]);
                    //skipped a byte, probably for alignment
                    parameters.AlarmEnable = ConversionMethods.UnsignedShortBytesToUInt16(byteData[8], byteData[9]);
                    // Here the PD parameters data structure starts, an the skip continues through the channel data as well

                    parameters.Initial = ConversionMethods.ByteToInt32(byteData[880]);
                    parameters.Humidity = ConversionMethods.ByteToInt32(byteData[881]);
                    parameters.Temperature = ConversionMethods.ByteToInt32(byteData[882]);

                    parameters.ExtLoadActive = ConversionMethods.BytesToSingle(byteData[884], byteData[885], byteData[886], byteData[887]);
                    parameters.ExtLoadReactive = ConversionMethods.BytesToSingle(byteData[888], byteData[889], byteData[890], byteData[891]);

                    /// just for testing, see if these values look correct or not, can see the values using a break point.
                    double externalLoadActive = parameters.ExtLoadActive;
                    double externalLoadReactive = parameters.ExtLoadReactive;

                    parameters.CommonRegisters_0 = ConversionMethods.ShortBytesToInt16(byteData[892], byteData[893]);
                    parameters.CommonRegisters_1 = ConversionMethods.ShortBytesToInt16(byteData[894], byteData[895]);
                    parameters.CommonRegisters_2 = ConversionMethods.ShortBytesToInt16(byteData[896], byteData[897]);
                    parameters.CommonRegisters_3 = ConversionMethods.ShortBytesToInt16(byteData[898], byteData[899]);
                    parameters.CommonRegisters_4 = ConversionMethods.ShortBytesToInt16(byteData[900], byteData[901]);
                    parameters.CommonRegisters_5 = ConversionMethods.ShortBytesToInt16(byteData[902], byteData[903]);
                    parameters.CommonRegisters_6 = ConversionMethods.ShortBytesToInt16(byteData[904], byteData[905]);
                    parameters.CommonRegisters_7 = ConversionMethods.ShortBytesToInt16(byteData[906], byteData[907]);
                    parameters.CommonRegisters_8 = ConversionMethods.ShortBytesToInt16(byteData[908], byteData[909]);
                    parameters.CommonRegisters_9 = ConversionMethods.ShortBytesToInt16(byteData[910], byteData[911]);
                    parameters.CommonRegisters_10 = ConversionMethods.ShortBytesToInt16(byteData[912], byteData[913]);
                    parameters.CommonRegisters_11 = ConversionMethods.ShortBytesToInt16(byteData[914], byteData[915]);
                    parameters.CommonRegisters_12 = ConversionMethods.ShortBytesToInt16(byteData[916], byteData[917]);
                    parameters.CommonRegisters_13 = ConversionMethods.ShortBytesToInt16(byteData[918], byteData[919]);
                    parameters.CommonRegisters_14 = ConversionMethods.ShortBytesToInt16(byteData[920], byteData[921]);
                    parameters.CommonRegisters_15 = ConversionMethods.ShortBytesToInt16(byteData[922], byteData[923]);
                    parameters.CommonRegisters_16 = ConversionMethods.ShortBytesToInt16(byteData[924], byteData[925]);
                    parameters.CommonRegisters_17 = ConversionMethods.ShortBytesToInt16(byteData[926], byteData[927]);
                    parameters.CommonRegisters_18 = ConversionMethods.ShortBytesToInt16(byteData[928], byteData[929]);
                    parameters.CommonRegisters_19 = ConversionMethods.ShortBytesToInt16(byteData[930], byteData[931]);

                    parameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[932]);
                    parameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[933]);
                    parameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[934]);
                    parameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[935]);
                    parameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[936]);
                    parameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[937]);
                    parameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[938]);
                    parameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[939]);
                    parameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[940]);
                    parameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[941]);
                    parameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[942]);
                    parameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[943]);
                    parameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[944]);
                    parameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[945]);
                    parameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[946]);
                    parameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[947]);
                    parameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[948]);
                    parameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[949]);
                    parameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[950]);
                    parameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[951]);
                    parameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[952]);
                    parameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[953]);
                    parameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[954]);
                    parameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[955]);
                    parameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[956]);
                    parameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[957]);
                    parameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[958]);
                    parameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[959]);
                    parameters.Reserved_28 = ConversionMethods.ByteToInt32(byteData[960]);
                    parameters.Reserved_29 = ConversionMethods.ByteToInt32(byteData[961]);
                    parameters.Reserved_30 = ConversionMethods.ByteToInt32(byteData[962]);
                    parameters.Reserved_31 = ConversionMethods.ByteToInt32(byteData[963]);
                    parameters.Reserved_32 = ConversionMethods.ByteToInt32(byteData[964]);
                    parameters.Reserved_33 = ConversionMethods.ByteToInt32(byteData[965]);
                    parameters.Reserved_34 = ConversionMethods.ByteToInt32(byteData[966]);
                    parameters.Reserved_35 = ConversionMethods.ByteToInt32(byteData[967]);
                    parameters.Reserved_36 = ConversionMethods.ByteToInt32(byteData[968]);
                    parameters.Reserved_37 = ConversionMethods.ByteToInt32(byteData[969]);
                    parameters.Reserved_38 = ConversionMethods.ByteToInt32(byteData[970]);
                    parameters.Reserved_39 = ConversionMethods.ByteToInt32(byteData[971]);
                    parameters.Reserved_40 = ConversionMethods.ByteToInt32(byteData[972]);
                    parameters.Reserved_41 = ConversionMethods.ByteToInt32(byteData[973]);
                    parameters.Reserved_42 = ConversionMethods.ByteToInt32(byteData[974]);
                    parameters.Reserved_43 = ConversionMethods.ByteToInt32(byteData[975]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ExtractParametersFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }

            return parameters;
        }

        private ADM_DataComponent_PDParameters ExtractPDParametersFromByteData(Byte[] byteData)
        {
            ADM_DataComponent_PDParameters pdParameters = null;
            try
            {
                if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                {
                    pdParameters = new ADM_DataComponent_PDParameters();

                    pdParameters.ID_0 = ConversionMethods.ByteToInt32(byteData[12]);
                    pdParameters.ID_1 = ConversionMethods.ByteToInt32(byteData[13]);
                    pdParameters.ID_2 = ConversionMethods.ByteToInt32(byteData[14]);
                    pdParameters.ID_3 = ConversionMethods.ByteToInt32(byteData[15]);
                    pdParameters.ID_4 = ConversionMethods.ByteToInt32(byteData[16]);
                    pdParameters.ID_5 = ConversionMethods.ByteToInt32(byteData[17]);
                    pdParameters.ID_6 = ConversionMethods.ByteToInt32(byteData[18]);
                    // skipped a byte
                    pdParameters.FW_Ver = ConversionMethods.UnsignedBytesToUInt32(byteData[20], byteData[21], byteData[22], byteData[23]) / 100.0;


                    pdParameters.Channels = ConversionMethods.UnsignedShortBytesToBitEncodedString(byteData[24], byteData[25]);
                    pdParameters.Matrix = ConversionMethods.ByteToInt32(byteData[26]);
                    pdParameters.MaxPDIChannel = ConversionMethods.ByteToInt32(byteData[27]);
                    pdParameters.MaxQ02Channel = ConversionMethods.ByteToInt32(byteData[28]);
                    // skipped a byte
                    pdParameters.NumberOfPeriods = ConversionMethods.UnsignedShortBytesToUInt16(byteData[30], byteData[31]);
                    pdParameters.Frequency = ConversionMethods.BytesToSingle(byteData[32], byteData[33], byteData[34], byteData[35]);

                    pdParameters.PSpeed_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[36], byteData[37]);
                    pdParameters.PSpeed_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[38], byteData[39]);

                    pdParameters.PJump_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[40], byteData[41]);
                    pdParameters.PJump_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[42], byteData[43]);

                    pdParameters.State = ConversionMethods.ByteToInt32(byteData[44]);
                    // skipped 3 bytes


                    pdParameters.ZoneWidth = ConversionMethods.BytesToSingle(byteData[48], byteData[49], byteData[50], byteData[51]);
                    pdParameters.ZonesCount = ConversionMethods.ByteToInt32(byteData[52]);
                    // skipped 3 bytes
                    pdParameters.AngleStep = ConversionMethods.BytesToSingle(byteData[56], byteData[57], byteData[58], byteData[59]);
                    pdParameters.AnglesCount = ConversionMethods.ByteToInt32(byteData[60]);

                    pdParameters.TablesOnChannel = ConversionMethods.ByteToInt32(byteData[61]);

                    pdParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[816]);
                    pdParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[817]);
                    pdParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[818]);
                    pdParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[819]);
                    pdParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[820]);
                    pdParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[821]);
                    pdParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[822]);
                    pdParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[823]);
                    pdParameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[824]);
                    pdParameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[825]);
                    pdParameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[826]);
                    pdParameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[827]);
                    pdParameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[828]);
                    pdParameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[829]);
                    pdParameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[830]);
                    pdParameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[831]);
                    pdParameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[832]);
                    pdParameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[833]);
                    pdParameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[834]);
                    pdParameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[835]);
                    pdParameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[836]);
                    pdParameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[837]);
                    pdParameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[838]);
                    pdParameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[839]);
                    pdParameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[840]);
                    pdParameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[841]);
                    pdParameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[842]);
                    pdParameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[843]);
                    pdParameters.Reserved_28 = ConversionMethods.ByteToInt32(byteData[844]);
                    pdParameters.Reserved_29 = ConversionMethods.ByteToInt32(byteData[845]);
                    pdParameters.Reserved_30 = ConversionMethods.ByteToInt32(byteData[846]);
                    pdParameters.Reserved_31 = ConversionMethods.ByteToInt32(byteData[847]);
                    pdParameters.Reserved_32 = ConversionMethods.ByteToInt32(byteData[848]);
                    pdParameters.Reserved_33 = ConversionMethods.ByteToInt32(byteData[849]);
                    pdParameters.Reserved_34 = ConversionMethods.ByteToInt32(byteData[850]);
                    pdParameters.Reserved_35 = ConversionMethods.ByteToInt32(byteData[851]);
                    pdParameters.Reserved_36 = ConversionMethods.ByteToInt32(byteData[852]);
                    pdParameters.Reserved_37 = ConversionMethods.ByteToInt32(byteData[853]);
                    pdParameters.Reserved_38 = ConversionMethods.ByteToInt32(byteData[854]);
                    pdParameters.Reserved_39 = ConversionMethods.ByteToInt32(byteData[855]);
                    pdParameters.Reserved_40 = ConversionMethods.ByteToInt32(byteData[856]);
                    pdParameters.Reserved_41 = ConversionMethods.ByteToInt32(byteData[857]);
                    pdParameters.Reserved_42 = ConversionMethods.ByteToInt32(byteData[858]);
                    pdParameters.Reserved_43 = ConversionMethods.ByteToInt32(byteData[859]);
                    pdParameters.Reserved_44 = ConversionMethods.ByteToInt32(byteData[860]);
                    pdParameters.Reserved_45 = ConversionMethods.ByteToInt32(byteData[861]);
                    pdParameters.Reserved_46 = ConversionMethods.ByteToInt32(byteData[862]);
                    pdParameters.Reserved_47 = ConversionMethods.ByteToInt32(byteData[863]);
                    pdParameters.Reserved_48 = ConversionMethods.ByteToInt32(byteData[864]);
                    pdParameters.Reserved_49 = ConversionMethods.ByteToInt32(byteData[865]);
                    pdParameters.Reserved_50 = ConversionMethods.ByteToInt32(byteData[866]);
                    pdParameters.Reserved_51 = ConversionMethods.ByteToInt32(byteData[867]);
                    pdParameters.Reserved_52 = ConversionMethods.ByteToInt32(byteData[868]);
                    pdParameters.Reserved_53 = ConversionMethods.ByteToInt32(byteData[869]);
                    pdParameters.Reserved_54 = ConversionMethods.ByteToInt32(byteData[870]);
                    pdParameters.Reserved_55 = ConversionMethods.ByteToInt32(byteData[871]);
                    pdParameters.Reserved_56 = ConversionMethods.ByteToInt32(byteData[872]);
                    pdParameters.Reserved_57 = ConversionMethods.ByteToInt32(byteData[873]);
                    pdParameters.Reserved_58 = ConversionMethods.ByteToInt32(byteData[874]);
                    pdParameters.Reserved_59 = ConversionMethods.ByteToInt32(byteData[875]);
                    pdParameters.Reserved_60 = ConversionMethods.ByteToInt32(byteData[876]);
                    pdParameters.Reserved_61 = ConversionMethods.ByteToInt32(byteData[877]);
                    pdParameters.Reserved_62 = ConversionMethods.ByteToInt32(byteData[878]);
                    pdParameters.Reserved_63 = ConversionMethods.ByteToInt32(byteData[879]);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ExtractPDParametersFromByteData(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return pdParameters;
        }

        private List<ADM_DataComponent_ChPDParameters> ConvertOneRecordOfChannelDataToDbData(Byte[] byteData)
        {
            List<ADM_DataComponent_ChPDParameters> convertedData = new List<ADM_DataComponent_ChPDParameters>();
            try
            {
                int offset = 64;
                // int channelNumber;
                ADM_DataComponent_ChPDParameters oneChannelsWorthOfData;
                for (int i = 0; i < 4; i++)
                {
                    oneChannelsWorthOfData = ExtractChPDParametersFromByteData(byteData, offset);
                    if (oneChannelsWorthOfData != null)
                    {
                        convertedData.Add(oneChannelsWorthOfData);
                    }

//                    channelNumber = ConversionMethods.ByteToInt32(byteData[offset]);
//                    if (channelNumber == i)
//                    {
//                        convertedData.Add(ExtractChPDParametersFromByteData(byteData, offset));
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in ADM_SingleDataReading.ADM_ConvertOneArchivedSetOfChannelDataToDbData(Byte[], Guid, bool[]):\nApparent mismatch between expected channel number and the channel number in the data.  Expected " + i.ToString() + ", got " + channelNumber.ToString();
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }

                    offset += 188;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ADM_ConvertOneArchivedChannelDataToDbData(Byte[], Guid, bool[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return convertedData;
        }

        private ADM_DataComponent_ChPDParameters ExtractChPDParametersFromByteData(Byte[] byteData, int startingOffset)
        {
            ADM_DataComponent_ChPDParameters chPDParameters = null;
            try
            {
                if (byteData.Length >= EXPECTED_LENGTH_OF_BYTE_ARRAY)
                {
                    chPDParameters = new ADM_DataComponent_ChPDParameters();

                    DateTime channelReadingDateTime = ConversionMethods.ConvertIntegerValuesToDateTime(byteData[startingOffset + 1],
                                                                                                       byteData[startingOffset],
                                                                                                       byteData[startingOffset + 2],
                                                                                                       byteData[startingOffset + 3],
                                                                                                       byteData[startingOffset + 4]);

                    if (channelReadingDateTime.CompareTo(ConversionMethods.MinimumDateTime()) == 0)
                    {
                        string errorMessage = "Error in ADM_SingleDataReading.ExtractChPDParametersFromByteData(Byte[], int):\nDate and time are incorrect, exiting function";
                        LogMessage.LogError(errorMessage);
                        chPDParameters = null;
                    }
                    else
                    {
                        chPDParameters.PDA_State = ConversionMethods.ByteToInt32(byteData[startingOffset + 8]);
                        // skip 3 bytes
                        chPDParameters.SumAPD = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 12], byteData[startingOffset + 13],
                                                                                          byteData[startingOffset + 14], byteData[startingOffset + 15]);
                        chPDParameters.MaxAPD = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 16], byteData[startingOffset + 17]);
                        chPDParameters.SredAmpl = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 18], byteData[startingOffset + 19]);

                        chPDParameters.PDA_Reserved_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 20]);
                        chPDParameters.PDA_Reserved_1 = ConversionMethods.ByteToInt32(byteData[startingOffset + 21]);
                        chPDParameters.PDA_Reserved_2 = ConversionMethods.ByteToInt32(byteData[startingOffset + 22]);
                        chPDParameters.PDA_Reserved_3 = ConversionMethods.ByteToInt32(byteData[startingOffset + 23]);
                        chPDParameters.PDA_Reserved_4 = ConversionMethods.ByteToInt32(byteData[startingOffset + 24]);
                        chPDParameters.PDA_Reserved_5 = ConversionMethods.ByteToInt32(byteData[startingOffset + 25]);
                        chPDParameters.PDA_Reserved_6 = ConversionMethods.ByteToInt32(byteData[startingOffset + 26]);
                        chPDParameters.PDA_Reserved_7 = ConversionMethods.ByteToInt32(byteData[startingOffset + 27]);

                        chPDParameters.Channels = ConversionMethods.ByteToInt32(byteData[startingOffset + 28]);
                        chPDParameters.Exist3D = ConversionMethods.ByteToInt32(byteData[startingOffset + 29]);
                        chPDParameters.Count3D = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 30], byteData[startingOffset + 31]);
                        chPDParameters.ExistMeas = ConversionMethods.ByteToInt32(byteData[startingOffset + 32]);
                        // skip 3 bytes
                        chPDParameters.CouPoint = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 36], byteData[startingOffset + 37],
                                                                                          byteData[startingOffset + 38], byteData[startingOffset + 39]);
                        chPDParameters.DX = ConversionMethods.BytesToSingle(byteData[startingOffset + 40], byteData[startingOffset + 41],
                                                                            byteData[startingOffset + 42], byteData[startingOffset + 43]);
                        chPDParameters.DY = ConversionMethods.BytesToSingle(byteData[startingOffset + 44], byteData[startingOffset + 45],
                                                                            byteData[startingOffset + 46], byteData[startingOffset + 47]);

                        chPDParameters.Coordinate_0 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 48], byteData[startingOffset + 49]);
                        chPDParameters.Coordinate_1 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 50], byteData[startingOffset + 51]);
                        chPDParameters.Coordinate_2 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 52], byteData[startingOffset + 53]);

                        chPDParameters.SizeX = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 54], byteData[startingOffset + 55]);
                        chPDParameters.SizeY = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 56], byteData[startingOffset + 57]);
                        chPDParameters.SizeZ = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 58], byteData[startingOffset + 59]);
                        chPDParameters.Scorost = ConversionMethods.ByteToInt32(byteData[startingOffset + 60]);
                        // skipped 1 byte
                        chPDParameters.Porog = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 62], byteData[startingOffset + 63]);
                        chPDParameters.Dopusk = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 64], byteData[startingOffset + 65]);

                        // we will adjust this value to conform to a 1-indexed channel designation
                        chPDParameters.FromChannel = ConversionMethods.ByteToInt32(byteData[startingOffset + 66]) + 1;

                        chPDParameters.WithChannels_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 67]);

                        chPDParameters.ChannelSensitivity = ConversionMethods.BytesToSingle(byteData[startingOffset + 68], byteData[startingOffset + 69],
                                                                                           byteData[startingOffset + 70], byteData[startingOffset + 71]);

                        chPDParameters.Q02 = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 72], byteData[startingOffset + 73]);
                        chPDParameters.Q02Plus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 74], byteData[startingOffset + 75]);
                        chPDParameters.Q02Minus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 76], byteData[startingOffset + 77]);
                        // skip two bytes
                        chPDParameters.Q02mV = ConversionMethods.BytesToSingle(byteData[startingOffset + 80], byteData[startingOffset + 81],
                                                                              byteData[startingOffset + 82], byteData[startingOffset + 83]);
                        chPDParameters.PDI = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 84], byteData[startingOffset + 85],
                                                                                     byteData[startingOffset + 86], byteData[startingOffset + 87]) / 10.0;
                        chPDParameters.PDIPlus = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 88], byteData[startingOffset + 89],
                                                                                         byteData[startingOffset + 90], byteData[startingOffset + 91]) / 10.0;
                        chPDParameters.PDIMinus = ConversionMethods.UnsignedBytesToDouble(byteData[startingOffset + 92], byteData[startingOffset + 93],
                                                                                          byteData[startingOffset + 94], byteData[startingOffset + 95]) / 10.0;
                        chPDParameters.SumPDAmpl = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 96], byteData[startingOffset + 97],
                                                                                           byteData[startingOffset + 98], byteData[startingOffset + 99]);
                        chPDParameters.SumPDPlus = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 100], byteData[startingOffset + 101],
                                                                                           byteData[startingOffset + 102], byteData[startingOffset + 103]);
                        chPDParameters.SumPDMinus = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 104], byteData[startingOffset + 105],
                                                                                            byteData[startingOffset + 106], byteData[startingOffset + 107]);
                        chPDParameters.PDI_t = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 108], byteData[startingOffset + 109]);
                        chPDParameters.PDI_j = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 110], byteData[startingOffset + 111]);

                        chPDParameters.Q02_t = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 112], byteData[startingOffset + 113]);
                        chPDParameters.Q02_j = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 114], byteData[startingOffset + 115]);

                        chPDParameters.Separations_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 116]);

                        chPDParameters.RefShift_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 117]);
                        chPDParameters.WasBlockedByT = ConversionMethods.ByteToInt32(byteData[startingOffset + 118]);
                        chPDParameters.WasBlockedByP = ConversionMethods.ByteToInt32(byteData[startingOffset + 119]);

                        Single ratedVoltage = ConversionMethods.BytesToSingle(byteData[startingOffset + 120], byteData[startingOffset + 121],
                                                                              byteData[startingOffset + 122], byteData[startingOffset + 123]);
                        ratedVoltage = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(ratedVoltage);
                        chPDParameters.RatedVoltage = ratedVoltage;

                        Single ratedCurrent = ConversionMethods.BytesToSingle(byteData[startingOffset + 124], byteData[startingOffset + 125],
                                                                              byteData[startingOffset + 126], byteData[startingOffset + 127]);
                        ratedCurrent = ConversionMethods.FixBadOutputFromConversionFromByteToSingle(ratedCurrent);
                        chPDParameters.RatedCurrent = ratedCurrent;

                        chPDParameters.State = ConversionMethods.ByteToInt32(byteData[startingOffset + 128]);
                        // skip a byte
                        chPDParameters.AlarmStatus = ConversionMethods.UnsignedShortBytesToUInt16(byteData[startingOffset + 130], byteData[startingOffset + 131]);

                        chPDParameters.P_0 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 132], byteData[startingOffset + 133],
                                                                                     byteData[startingOffset + 134], byteData[startingOffset + 135]);
                        chPDParameters.P_1 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 136], byteData[startingOffset + 137],
                                                                                     byteData[startingOffset + 138], byteData[startingOffset + 139]);
                        chPDParameters.P_2 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 140], byteData[startingOffset + 141],
                                                                                     byteData[startingOffset + 142], byteData[startingOffset + 143]);
                        chPDParameters.P_3 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 144], byteData[startingOffset + 145],
                                                                                     byteData[startingOffset + 146], byteData[startingOffset + 147]);
                        chPDParameters.P_4 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 148], byteData[startingOffset + 149],
                                                                                     byteData[startingOffset + 150], byteData[startingOffset + 151]);
                        chPDParameters.P_5 = ConversionMethods.UnsignedBytesToUInt32(byteData[startingOffset + 152], byteData[startingOffset + 153],
                                                                                     byteData[startingOffset + 154], byteData[startingOffset + 155]);

                        chPDParameters.Reserved_0 = ConversionMethods.ByteToInt32(byteData[startingOffset + 156]);
                        chPDParameters.Reserved_1 = ConversionMethods.ByteToInt32(byteData[startingOffset + 157]);
                        chPDParameters.Reserved_2 = ConversionMethods.ByteToInt32(byteData[startingOffset + 158]);
                        chPDParameters.Reserved_3 = ConversionMethods.ByteToInt32(byteData[startingOffset + 159]);
                        chPDParameters.Reserved_4 = ConversionMethods.ByteToInt32(byteData[startingOffset + 160]);
                        chPDParameters.Reserved_5 = ConversionMethods.ByteToInt32(byteData[startingOffset + 161]);
                        chPDParameters.Reserved_6 = ConversionMethods.ByteToInt32(byteData[startingOffset + 162]);
                        chPDParameters.Reserved_7 = ConversionMethods.ByteToInt32(byteData[startingOffset + 163]);
                        chPDParameters.Reserved_8 = ConversionMethods.ByteToInt32(byteData[startingOffset + 164]);
                        chPDParameters.Reserved_9 = ConversionMethods.ByteToInt32(byteData[startingOffset + 165]);
                        chPDParameters.Reserved_10 = ConversionMethods.ByteToInt32(byteData[startingOffset + 166]);
                        chPDParameters.Reserved_11 = ConversionMethods.ByteToInt32(byteData[startingOffset + 167]);
                        chPDParameters.Reserved_12 = ConversionMethods.ByteToInt32(byteData[startingOffset + 168]);
                        chPDParameters.Reserved_13 = ConversionMethods.ByteToInt32(byteData[startingOffset + 169]);
                        chPDParameters.Reserved_14 = ConversionMethods.ByteToInt32(byteData[startingOffset + 170]);
                        chPDParameters.Reserved_15 = ConversionMethods.ByteToInt32(byteData[startingOffset + 171]);
                        chPDParameters.Reserved_16 = ConversionMethods.ByteToInt32(byteData[startingOffset + 172]);
                        chPDParameters.Reserved_17 = ConversionMethods.ByteToInt32(byteData[startingOffset + 173]);
                        chPDParameters.Reserved_18 = ConversionMethods.ByteToInt32(byteData[startingOffset + 174]);
                        chPDParameters.Reserved_19 = ConversionMethods.ByteToInt32(byteData[startingOffset + 175]);
                        chPDParameters.Reserved_20 = ConversionMethods.ByteToInt32(byteData[startingOffset + 176]);
                        chPDParameters.Reserved_21 = ConversionMethods.ByteToInt32(byteData[startingOffset + 177]);
                        chPDParameters.Reserved_22 = ConversionMethods.ByteToInt32(byteData[startingOffset + 178]);
                        chPDParameters.Reserved_23 = ConversionMethods.ByteToInt32(byteData[startingOffset + 179]);
                        chPDParameters.Reserved_24 = ConversionMethods.ByteToInt32(byteData[startingOffset + 180]);
                        chPDParameters.Reserved_25 = ConversionMethods.ByteToInt32(byteData[startingOffset + 181]);
                        chPDParameters.Reserved_26 = ConversionMethods.ByteToInt32(byteData[startingOffset + 182]);
                        chPDParameters.Reserved_27 = ConversionMethods.ByteToInt32(byteData[startingOffset + 183]);
                        chPDParameters.Reserved_28 = ConversionMethods.ByteToInt32(byteData[startingOffset + 184]);
                        chPDParameters.Reserved_29 = ConversionMethods.ByteToInt32(byteData[startingOffset + 185]);
                        chPDParameters.Reserved_30 = ConversionMethods.ByteToInt32(byteData[startingOffset + 186]);
                        chPDParameters.Reserved_31 = ConversionMethods.ByteToInt32(byteData[startingOffset + 187]);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ExtractChPDParametersFromByteData(Byte[], int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return chPDParameters;
        }

        public static Int32[] ConvertRawPhaseResolvedDataToArchiveFormat(Byte[] byteData)
        {
            Int32[] archiveData = null;
            try
            {
                int length = byteData.Length;
                int numberOfEntries;
                if ((length % 6144) == 0)
                {
                    numberOfEntries = length / 2;
                    archiveData = new Int32[numberOfEntries];
                    int j = 0;
                    for (int i = 0; i < numberOfEntries; i++)
                    {
                        archiveData[i] = ConversionMethods.UnsignedShortBytesToUInt16(byteData[j], byteData[j + 1]);
                        j += 2;
                    }
                    archiveData = ReorderPhaseResolvedData(archiveData);
                }

                else
                {
                    string errorMessage = "Error in Error in ADM_SingleDataReading.ConvertRawPhaseResolvedDataToArchiveFormat(Byte[]):\nInput array is the wrong size.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ConvertRawPhaseResolvedDataToArchiveFormat(Byte[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return archiveData;
        }

        private static Int32[] ReorderPhaseResolvedData(Int32[] phaseResolvedData)
        {
            Int32[] reorderedPhaseResolvedData = null;
            try
            {
                int totalPoints = phaseResolvedData.Length;
                int totalChannels = totalPoints / 3072;
                int offset = 0;
                int oldDataIndex = 0;
                int i, row, column;
                if ((totalPoints % 3072) == 0)
                {
                    reorderedPhaseResolvedData = new Int32[totalPoints];
                    for (i = 0; i < totalChannels; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            for (column = 0; column < 48; column++)
                            {
                                for (row = 0; row < 32; row++)
                                {
                                    reorderedPhaseResolvedData[offset + (row * 48) + (column)] = phaseResolvedData[oldDataIndex];
                                    oldDataIndex++;
                                }
                            }
                            offset += 1536;
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_SingleDataReading.ADM_ReorderPhaseResolvedData(Int32[]):\nInput array is not of an expected size";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ADM_ReorderPhaseResolvedData(Int32[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return reorderedPhaseResolvedData;
        }

        public List<String> OutputParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("Parameters Values");
                if (parameters != null)
                {
                    outputList.Add("ReadingDateTime: " + this.readingDateTime.ToString());
                    outputList.Add("Tempfile: " + parameters.Tempfile.ToString());
                    outputList.Add("PDExists: " + parameters.PDExists.ToString());
                    outputList.Add("AlarmEnable: " + parameters.AlarmEnable.ToString());
                    outputList.Add("Initial: " + parameters.Initial.ToString());
                    outputList.Add("Humidity: " + parameters.Humidity.ToString());
                    outputList.Add("Temperature: " + parameters.Temperature.ToString());
                    outputList.Add("ExtLoadActive: " + parameters.ExtLoadActive.ToString());
                    outputList.Add("ExtLoadReactive: " + parameters.ExtLoadReactive.ToString());
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.OutputParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<String> OutputPDParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("PDParameters Values");
                if (pdParameters != null)
                {
                    outputList.Add("ID_0: " + pdParameters.ID_0.ToString());
                    outputList.Add("ID_1: " + pdParameters.ID_1.ToString());
                    outputList.Add("ID_2: " + pdParameters.ID_2.ToString());
                    outputList.Add("ID_3: " + pdParameters.ID_3.ToString());
                    outputList.Add("ID_4: " + pdParameters.ID_4.ToString());
                    outputList.Add("ID_5: " + pdParameters.ID_5.ToString());
                    outputList.Add("ID_6: " + pdParameters.ID_6.ToString());

                    outputList.Add("FW_Ver: " + pdParameters.FW_Ver.ToString());
                    outputList.Add("Channels: " + pdParameters.Channels);
                    outputList.Add("Matrix: " + pdParameters.Matrix.ToString());
                    outputList.Add("MaxPDIChannel: " + pdParameters.MaxPDIChannel.ToString());
                    outputList.Add("MaxQ02Channel: " + pdParameters.MaxQ02Channel.ToString());
                    outputList.Add("NumberOfPeriods: " + pdParameters.NumberOfPeriods.ToString());
                    outputList.Add("Frequency: " + pdParameters.Frequency.ToString());
                    outputList.Add("PSpeed_0: " + pdParameters.PSpeed_0.ToString());
                    outputList.Add("PSpeed_1: " + pdParameters.PSpeed_1.ToString());
                    outputList.Add("PJump_0: " + pdParameters.PJump_0.ToString());
                    outputList.Add("PJump_1: " + pdParameters.PJump_1.ToString());
                    outputList.Add("State: " + pdParameters.State.ToString());
                    outputList.Add("ZoneWidth: " + pdParameters.ZoneWidth.ToString());
                    outputList.Add("ZonesCount: " + pdParameters.ZonesCount.ToString());
                    outputList.Add("AngleStep: " + pdParameters.AngleStep.ToString());
                    outputList.Add("AnglesCount: " + pdParameters.AnglesCount.ToString());
                    outputList.Add("TablesOnChannel: " + pdParameters.TablesOnChannel.ToString());
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.OutputPDParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        public List<String> OutputChPDParametersValuesAsStrings()
        {
            List<String> outputList = new List<String>();
            try
            {
                outputList.Add("Entries for all Channels");

                if (chPDParameters != null)
                {
                    foreach (ADM_DataComponent_ChPDParameters chPDParametersEntry in this.chPDParameters)
                    {
                        outputList.Add("ChPDParameters Values");

                        outputList.Add("PDA_State: " + chPDParametersEntry.PDA_State.ToString());
                        outputList.Add("SumAPD: " + chPDParametersEntry.SumAPD.ToString());
                        outputList.Add("MaxAPD: " + chPDParametersEntry.MaxAPD.ToString());
                        outputList.Add("SredAmpl: " + chPDParametersEntry.SredAmpl.ToString());
                        outputList.Add("Channels: " + chPDParametersEntry.Channels.ToString());
                        outputList.Add("Exist3D: " + chPDParametersEntry.Exist3D.ToString());
                        outputList.Add("Count3D: " + chPDParametersEntry.Count3D.ToString());
                        outputList.Add("ExistMeas: " + chPDParametersEntry.ExistMeas.ToString());
                        outputList.Add("CouPoint: " + chPDParametersEntry.CouPoint.ToString());
                        outputList.Add("DX: " + chPDParametersEntry.DX.ToString());
                        outputList.Add("DY: " + chPDParametersEntry.DY.ToString());
                        outputList.Add("Coordinate_0: " + chPDParametersEntry.Coordinate_0.ToString());
                        outputList.Add("Coordinate_1: " + chPDParametersEntry.Coordinate_1.ToString());
                        outputList.Add("Coordinate_2: " + chPDParametersEntry.Coordinate_2.ToString());
                        outputList.Add("SizeX: " + chPDParametersEntry.SizeX.ToString());
                        outputList.Add("SizeY: " + chPDParametersEntry.SizeY.ToString());
                        outputList.Add("SizeZ: " + chPDParametersEntry.SizeZ.ToString());
                        outputList.Add("Scorost: " + chPDParametersEntry.Scorost.ToString());
                        outputList.Add("Porog: " + chPDParametersEntry.Porog.ToString());
                        outputList.Add("Dopusk: " + chPDParametersEntry.Dopusk.ToString());


                        outputList.Add("FromChannel: " + chPDParametersEntry.FromChannel.ToString());
                        outputList.Add("WithChannels_0: " + chPDParametersEntry.WithChannels_0.ToString());
                        outputList.Add("ChannelSensitivity: " + chPDParametersEntry.ChannelSensitivity.ToString());
                        outputList.Add("Q02: " + chPDParametersEntry.Q02.ToString());
                        outputList.Add("Q02Plus: " + chPDParametersEntry.Q02Plus.ToString());
                        outputList.Add("Q02Minus: " + chPDParametersEntry.Q02Minus.ToString());
                        outputList.Add("Q02mv: " + chPDParametersEntry.Q02mV.ToString());
                        outputList.Add("PDI: " + chPDParametersEntry.PDI.ToString());
                        outputList.Add("PDIPlus: " + chPDParametersEntry.PDIPlus.ToString());
                        outputList.Add("PDIMinus: " + chPDParametersEntry.PDIMinus.ToString());
                        outputList.Add("SumPDAmpl: " + chPDParametersEntry.SumPDAmpl.ToString());
                        outputList.Add("SumPDPlus: " + chPDParametersEntry.SumPDPlus.ToString());
                        outputList.Add("SumPDMinus: " + chPDParametersEntry.SumPDMinus.ToString());
                        outputList.Add("PDI_t: " + chPDParametersEntry.PDI_t.ToString());
                        outputList.Add("PDI_j: " + chPDParametersEntry.PDI_j.ToString());
                        outputList.Add("Q02_t: " + chPDParametersEntry.Q02_t.ToString());
                        outputList.Add("Q02_j: " + chPDParametersEntry.Q02_j.ToString());
                        outputList.Add("Separations_0: " + chPDParametersEntry.Separations_0.ToString());
                        outputList.Add("RefShift_0: " + chPDParametersEntry.RefShift_0.ToString());
                        outputList.Add("WasBlockedByT: " + chPDParametersEntry.WasBlockedByT.ToString());
                        outputList.Add("WasBlockedByP: " + chPDParametersEntry.WasBlockedByP.ToString());
                        outputList.Add("RatedVoltage: " + chPDParametersEntry.RatedVoltage.ToString());
                        outputList.Add("RatedCurrent: " + chPDParametersEntry.RatedCurrent.ToString());
                        outputList.Add("State: " + chPDParametersEntry.State.ToString());
                        outputList.Add("AlarmStatus: " + chPDParametersEntry.AlarmStatus.ToString());
                        outputList.Add("P_0: " + chPDParametersEntry.P_0.ToString());
                        outputList.Add("P_1: " + chPDParametersEntry.P_1.ToString());
                        outputList.Add("P_2: " + chPDParametersEntry.P_2.ToString());
                        outputList.Add("P_3: " + chPDParametersEntry.P_3.ToString());
                        outputList.Add("P_4: " + chPDParametersEntry.P_4.ToString());
                        outputList.Add("P_5: " + chPDParametersEntry.P_5.ToString());
                        outputList.Add("------End of channel entry------");
                    }
                }
                else
                {
                    outputList.Add("Value for this entry was null");
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.OutputChPDParametersValuesAsStrings()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return outputList;
        }

        //public List<String> OutputPhaseResolvedDataValuesAsStrings()
        //{
        //    List<String> outputList = new List<String>();

        //    outputList.Add("Phase Resolved Data Entries for all Channels");

        //    if (phaseResolvedData != null)
        //    {
        //        foreach (ADM_DataComponent_PhaseResolvedData phaseResolvedDataEntry in this.phaseResolvedData)
        //        {
        //            outputList.Add("PhaseResolvedData Values");

        //            outputList.Add("ChannelNumber: " + phaseResolvedDataEntry.ChannelNumber.ToString());
        //            outputList.Add("PhasePolarity: " + phaseResolvedDataEntry.PhasePolarity);
        //            outputList.Add("NonZeroEntries: " + phaseResolvedDataEntry.NonZeroEntries);
        //            outputList.Add("------End of phase resolved data entry------");
        //        }
        //    }
        //    else
        //    {
        //        outputList.Add("Value for this entry was null");
        //    }
        //    return outputList;
        //}

        //public bool MatrixWasRecorded()
        //{
        //    bool recorded = false;
        //    //if (pdParameters.Matrix > 0)
        //    //{
        //    //    recorded = true;
        //    //}
        //    if ((phaseResolvedData != null) && (phaseResolvedData.Count > 0))
        //    {
        //        recorded = true;
        //    }
        //    return recorded;
        //}

        public int GetAlarmStateForAllChannels()
        {
            int alarmState = -1;
            try
            {
                if (EnoughMembersAreCorrect())
                {
                    alarmState = 0;
                    foreach (ADM_DataComponent_ChPDParameters entry in chPDParameters)
                    {
                        if (entry.AlarmStatus > alarmState)
                        {
                            alarmState = entry.AlarmStatus;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.GetAlarmStateForAllChannels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmState;
        }

        public string GetAlarmSourceForAllChannels()
        {
            StringBuilder alarmSource = new StringBuilder();
            try
            {
                if (EnoughMembersAreCorrect())
                {
                    bool[] channelIsActive = ConvertStringBitEncodingOfActiveChannelsToBoolean(pdParameters.Channels);
                    int j = 0;
                    for (int i = 0; i < 15; i++)
                    {
                        if (channelIsActive[i])
                        {
                            if (chPDParameters[j].FromChannel == (i + 1))
                            {
                                if (chPDParameters[j].AlarmStatus > 0)
                                {
                                    alarmSource = alarmSource.Insert(0, "1");
                                }
                                else
                                {
                                    alarmSource = alarmSource.Insert(0, "0");
                                }
                                j++;
                            }
                            else
                            {
                                MessageBox.Show("Indices mismatch in GetAlarmSourceForAllChannels");
                            }
                        }
                        else
                        {
                            alarmSource = alarmSource.Insert(0, "0");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.GetAlarmSourceForAllChannels()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return alarmSource.ToString();
        }

        private Boolean[] ConvertBitEncodingOfActiveChannelsToBoolean(UInt16 bitEncodedActiveChannels)
        {
            Boolean[] channelIsActive = new Boolean[16];
            try
            {
                int[] powersOfTwo = new int[16] { 32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1 };
                for (int i = 0; i < 16; i++)
                {
                    if ((bitEncodedActiveChannels & powersOfTwo[i]) != 0)
                    {
                        channelIsActive[15 - i] = true;
                    }
                    else
                    {
                        channelIsActive[15 - i] = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ConvertBitEncodingOfActiveChannelsToBoolean(UInt16)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelIsActive;
        }

        private Boolean[] ConvertStringBitEncodingOfActiveChannelsToBoolean(string stringRepresentationOfBits)
        {
            Boolean[] channelIsActive = null;
            try
            {
                int count = stringRepresentationOfBits.Length;

                if (count == 16)
                {
                    channelIsActive = new Boolean[16];
                    for (int i = 0; i < 16; i++)
                    {
                        if (stringRepresentationOfBits[i].CompareTo('1') == 0)
                        {
                            channelIsActive[15 - i] = true;
                        }
                        else if (stringRepresentationOfBits[i].CompareTo('0') == 0)
                        {
                            channelIsActive[15 - i] = false;
                        }
                        else
                        {
                            string errorMessage = "Error in ADM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string):\nInput string contains wrong character, expected 0 or 1, got " + stringRepresentationOfBits[i];
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in ADM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string):\nInput string does not have 16 characters in it.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in ADM_SingleDataReading.ConvertStringBitEncodingOfActiveChannelsToBoolean(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return channelIsActive;
        }
    }
}
