﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataObjects
{
    public class ADM_DataComponent_PDParameters
    {
        public int ID_0;
        public int ID_1;
        public int ID_2;
        public int ID_3;
        public int ID_4;
        public int ID_5;
        public int ID_6;
        public double FW_Ver;
        public string Channels;
        public int Matrix;
        public int MaxPDIChannel;
        public int MaxQ02Channel;
        public int NumberOfPeriods;
        public double Frequency;
        public int PSpeed_0;
        public int PSpeed_1;
        public int PJump_0;
        public int PJump_1;
        public int State;
        public double ZoneWidth;
        public int ZonesCount;
        public double AngleStep;
        public int AnglesCount;
        public int TablesOnChannel;
        public int Reserved_0;
        public int Reserved_1;
        public int Reserved_2;
        public int Reserved_3;
        public int Reserved_4;
        public int Reserved_5;
        public int Reserved_6;
        public int Reserved_7;
        public int Reserved_8;
        public int Reserved_9;
        public int Reserved_10;
        public int Reserved_11;
        public int Reserved_12;
        public int Reserved_13;
        public int Reserved_14;
        public int Reserved_15;
        public int Reserved_16;
        public int Reserved_17;
        public int Reserved_18;
        public int Reserved_19;
        public int Reserved_20;
        public int Reserved_21;
        public int Reserved_22;
        public int Reserved_23;
        public int Reserved_24;
        public int Reserved_25;
        public int Reserved_26;
        public int Reserved_27;
        public int Reserved_28;
        public int Reserved_29;
        public int Reserved_30;
        public int Reserved_31;
        public int Reserved_32;
        public int Reserved_33;
        public int Reserved_34;
        public int Reserved_35;
        public int Reserved_36;
        public int Reserved_37;
        public int Reserved_38;
        public int Reserved_39;
        public int Reserved_40;
        public int Reserved_41;
        public int Reserved_42;
        public int Reserved_43;
        public int Reserved_44;
        public int Reserved_45;
        public int Reserved_46;
        public int Reserved_47;
        public int Reserved_48;
        public int Reserved_49;
        public int Reserved_50;
        public int Reserved_51;
        public int Reserved_52;
        public int Reserved_53;
        public int Reserved_54;
        public int Reserved_55;
        public int Reserved_56;
        public int Reserved_57;
        public int Reserved_58;
        public int Reserved_59;
        public int Reserved_60;
        public int Reserved_61;
        public int Reserved_62;
        public int Reserved_63;        
    }
}
