﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;

namespace MainConfigurationLite
{

    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        #region System Configuration Tab

        private static string duplicateModbusAddressDetectedText = "Duplicate Modbus address detected.  The ModBus address must be unique.";
        private static string modbusAddressOutOfRangeText = "The ModBus address must be between 1 and 255 inclusive.";
        private static string monitorNumberSystemConfigGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Monitor Number</html>";
        private static string monitorTypeSystemConfigGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Monitor Type</html>";
        private static string monitorTypeNoneSystemConfigGridViewText = "Not Selected";
        private static string monitorTypeBushingSystemConfigGridViewText = "Bushing Monitor";
        private static string monitorTypePDMonitorSystemConfigGridViewText = "PD Monitor";
        private static string monitorTypeAcousticSystemConfigGridViewText = "Acoustic Monitor";
        private static string modbusAddressSystemConfigGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Modbus Address</html>";
        private static string baudRateSystemConfigGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Baud Rate</html>";
        private static string dataTransferSystemConfigGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Data Transfer<br>Interval (sec)</html>";


        private void SelectSystemConfigurationTab()
        {
            configurationRadPageView.SelectedPage = systemConfigurationRadPageViewPage;
        }

        private void DisableSystemConfigurationTabConfigurationEditObjects()
        {
            systemConfigurationRadGridView.ReadOnly = true;
        }

        private void EnableSystemConfigurationTabConfigurationEditObjects()
        {
            systemConfigurationRadGridView.ReadOnly = false;
        }

        private void InitializeSystemConfigurationRadGridView()
        {
            try
            {
                GridViewTextBoxColumn transferSetupNumberGridViewTextBoxColumn = new GridViewTextBoxColumn("MonitorNumber");
                transferSetupNumberGridViewTextBoxColumn.Name = "MonitorNumber";
                transferSetupNumberGridViewTextBoxColumn.HeaderText = monitorNumberSystemConfigGridViewText;
                transferSetupNumberGridViewTextBoxColumn.DisableHTMLRendering = false;
                transferSetupNumberGridViewTextBoxColumn.Width = 95;
                transferSetupNumberGridViewTextBoxColumn.AllowSort = false;
                transferSetupNumberGridViewTextBoxColumn.IsPinned = true;
                transferSetupNumberGridViewTextBoxColumn.ReadOnly = true;

                GridViewComboBoxColumn deviceTypeGridViewComboBoxColumn = new GridViewComboBoxColumn("MonitorType");
                deviceTypeGridViewComboBoxColumn.Name = "MonitorType";
                deviceTypeGridViewComboBoxColumn.HeaderText = monitorTypeSystemConfigGridViewText;
                deviceTypeGridViewComboBoxColumn.DataSource = new String[] { monitorTypeNoneSystemConfigGridViewText, monitorTypeBushingSystemConfigGridViewText, monitorTypePDMonitorSystemConfigGridViewText, monitorTypeAcousticSystemConfigGridViewText };
                deviceTypeGridViewComboBoxColumn.DisableHTMLRendering = false;
                deviceTypeGridViewComboBoxColumn.Width = 115;
                deviceTypeGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn deviceModBusAddressGridViewDecimalColumn = new GridViewDecimalColumn("MonitorModbusAddress");
                deviceModBusAddressGridViewDecimalColumn.Name = "MonitorModbusAddress";
                deviceModBusAddressGridViewDecimalColumn.HeaderText = modbusAddressSystemConfigGridViewText;
                deviceModBusAddressGridViewDecimalColumn.DecimalPlaces = 0;
                deviceModBusAddressGridViewDecimalColumn.DisableHTMLRendering = false;
                deviceModBusAddressGridViewDecimalColumn.Width = 95;
                deviceModBusAddressGridViewDecimalColumn.AllowSort = false;
                deviceModBusAddressGridViewDecimalColumn.Minimum = 0;
                deviceModBusAddressGridViewDecimalColumn.Maximum = 255;

                GridViewComboBoxColumn baudRateGridViewComboBoxColumn = new GridViewComboBoxColumn("BaudRate");
                baudRateGridViewComboBoxColumn.Name = "BaudRate";
                baudRateGridViewComboBoxColumn.HeaderText = baudRateSystemConfigGridViewText;
                baudRateGridViewComboBoxColumn.DataSource = new String[] { "9600", "115200" };
                baudRateGridViewComboBoxColumn.DisableHTMLRendering = false;
                baudRateGridViewComboBoxColumn.Width = 90;
                baudRateGridViewComboBoxColumn.AllowSort = false;

                GridViewDecimalColumn dataTransferIntervalGridViewDecimalColumn = new GridViewDecimalColumn("DataTransferInterval");
                dataTransferIntervalGridViewDecimalColumn.Name = "DataTransferInterval";
                dataTransferIntervalGridViewDecimalColumn.HeaderText = dataTransferSystemConfigGridViewText;
                dataTransferIntervalGridViewDecimalColumn.DecimalPlaces = 0;
                dataTransferIntervalGridViewDecimalColumn.DisableHTMLRendering = false;
                dataTransferIntervalGridViewDecimalColumn.Width = 101;
                dataTransferIntervalGridViewDecimalColumn.AllowSort = false;
                dataTransferIntervalGridViewDecimalColumn.Minimum = 1;
                // dataTransferIntervalGridViewDecimalColumn.Maximum = 255;

                this.systemConfigurationRadGridView.MasterTemplate.Columns.Add(transferSetupNumberGridViewTextBoxColumn);
                this.systemConfigurationRadGridView.MasterTemplate.Columns.Add(deviceTypeGridViewComboBoxColumn);
                this.systemConfigurationRadGridView.MasterTemplate.Columns.Add(deviceModBusAddressGridViewDecimalColumn);
                this.systemConfigurationRadGridView.MasterTemplate.Columns.Add(baudRateGridViewComboBoxColumn);
                this.systemConfigurationRadGridView.MasterTemplate.Columns.Add(dataTransferIntervalGridViewDecimalColumn);

                this.systemConfigurationRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.systemConfigurationRadGridView.AllowColumnReorder = false;
                this.systemConfigurationRadGridView.AllowColumnChooser = false;
                this.systemConfigurationRadGridView.ShowGroupPanel = false;
                this.systemConfigurationRadGridView.EnableGrouping = false;
                this.systemConfigurationRadGridView.AllowAddNewRow = false;
                this.systemConfigurationRadGridView.AllowDeleteRow = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeSystemConfigurationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        //        private MainConfigUISystemConfigurationData GetSystemConfigurationDataForOneModule(int moduleNumber)
        //        {
        //            MainConfigUISystemConfigurationData configurationData = null;
        //            try
        //            {
        //                if (currentConfiguration != null)
        //                {
        //                    if (currentConfiguration.fullStatusInformationConfigObjects != null)
        //                    {
        //                        if (currentConfiguration.fullStatusInformationConfigObjects.Count > moduleNumber)
        //                        {
        //                            configurationData = new MainConfigUISystemConfigurationData();

        //                            configurationData.moduleNumber = moduleNumber + 1;

        //                            Main_ConfigComponent_FullStatusInformation statusInfo = currentConfiguration.fullStatusInformationConfigObjects[moduleNumber];

        //                            if (statusInfo.ModuleStatus == 1)
        //                            {
        //                                configurationData.deviceType = GetModuleNameFromIntegerModuleType(statusInfo.ModuleType);
        //                            }
        //                            else
        //                            {
        //                                configurationData.deviceType = "None";
        //                            }

        //                            configurationData.modBusAddress = statusInfo.ModBusAddress;
        //                            configurationData.baudRate = GetFullBaudRateFromShortenedBaudRate(statusInfo.BaudRate);
        //                            configurationData.dataTransferInterval = statusInfo.ReadDatePeriodInSeconds;
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataForOneModule(int)\nInput int exceeds the capacity of this.currentConfiguration.fullStatusInformationConfigObjects.";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataForOneModule(int)\nthis.currentConfiguration.fullStatusInformationConfigObjects was null.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataForOneModule(int)\nthis.currentConfiguration was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSystemConfigurationDataForOneModule(int)\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return configurationData;
        //        }

        //        private void FillSystemConfigurationList()
        //        {
        //            try
        //            {
        //                if (this.systemConfigurationList != null)
        //                {
        //                    this.systemConfigurationList.Clear();
        //                    for (int i = 0; i < 15; i++)
        //                    {
        //                        this.systemConfigurationList.Add(GetSystemConfigurationDataForOneModule(i));
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.FillSystemConfigurationList()\nthis.systemConfigurationList was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.FillSystemConfigurationList()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        private void AddDataToSystemConfigurationGrid(List<Main_ConfigComponent_FullStatusInformation> fullStatusInformationList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_FullStatusInformation fullStatusInformation;
                int rowCount = this.systemConfigurationRadGridView.RowCount;
                string deviceType;
                int modBusAddress;
                string baudRate;
                int dataTransferInterval;
                if (rowCount == 15)
                {
                    if (fullStatusInformationList.Count >= rowCount)
                    {
                        for (int i = 0; i < rowCount; i++)
                        {
                            fullStatusInformation = fullStatusInformationList[i];
                            rowInfo = this.systemConfigurationRadGridView.Rows[i];

                            deviceType = GetModuleNameFromIntegerModuleType(fullStatusInformation.ModuleType);
                            modBusAddress = fullStatusInformation.ModBusAddress;
                            baudRate = GetFullBaudRateFromShortenedBaudRate(fullStatusInformation.BaudRate).ToString();
                            dataTransferInterval = fullStatusInformation.ReadDatePeriodInSeconds;

                            rowInfo.Cells[1].Value = deviceType;
                            rowInfo.Cells[2].Value = modBusAddress;
                            rowInfo.Cells[3].Value = baudRate;
                            rowInfo.Cells[4].Value = dataTransferInterval;
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToSystemConfigurationGrid()\nthis.fullStatusInformationList did not have enough elements.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToSystemConfigurationGrid()\nthis.systemConfigurationRadGridView.RowCount was not 15 as it should be.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToSystemConfigurationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToTheSystemConfigurationGrid()
        {
            try
            {
                this.systemConfigurationRadGridView.Rows.Clear();
                for (int i = 0; i < 15; i++)
                {
                    this.systemConfigurationRadGridView.Rows.Add((i + 1), "None", 0, 9600, 0);
                }
                this.systemConfigurationRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToTheSystemConfigurationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        //        private void AddRowsToTheSystemConfigurationGrid()
        //        {
        //            try
        //            {
        //                if (currentConfiguration != null)
        //                {
        //                    if (currentConfiguration.fullStatusInformationConfigObjects != null)
        //                    {
        //                        int moduleNumber = 1;
        //                        string deviceType;
        //                        int modBusAddress;
        //                        string baudRate;
        //                        int dataTransferInterval;
        //                        this.systemConfigurationRadGridView.Rows.Clear();
        //                        foreach (Main_ConfigComponent_FullStatusInformation entry in currentConfiguration.fullStatusInformationConfigObjects)
        //                        {
        //                            deviceType = GetModuleNameFromIntegerModuleType(entry.ModuleType);
        //                            modBusAddress = entry.ModBusAddress;
        //                            baudRate = GetFullBaudRateFromShortenedBaudRate(entry.BaudRate).ToString();
        //                            dataTransferInterval = entry.ReadDatePeriodInSeconds;

        //                            this.systemConfigurationRadGridView.Rows.Add(moduleNumber, deviceType, modBusAddress, baudRate, dataTransferInterval);
        //                            moduleNumber++;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheSystemConfigurationGrid()\nthis.currentConfiguration.fullStatusInformationConfigObjects was null.";
        //                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                        MessageBox.Show(errorMessage);
        //#endif
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.AddRowsToTheSystemConfigurationGrid()\nthis.currentConfiguration was null.";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddRowsToTheSystemConfigurationGrid()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //        }

        /// <summary>
        /// Gets the system configuration, expects it to already be error-checked
        /// </summary>
        /// <returns></returns>
        private List<MonitorInfo> GetModulesFromSystemConfigurationByEntryOrder()
        {
            List<MonitorInfo> monitorsByEntryOrder = new List<MonitorInfo>();
            try
            {
                GridViewRowInfo rowInfo;
                int modBusAddress;
                int deviceType;
                MonitorInfo monitorInfo;
                if (this.systemConfigurationRadGridView.Rows.Count == 15)
                {
                    for (int i = 0; i < 15; i++)
                    {
                        rowInfo = this.systemConfigurationRadGridView.Rows[i];
                        if (rowInfo.Cells.Count > 4)
                        {
                            modBusAddress = 0;
                            deviceType = GetIntegerModuleTypeFromModuleName(rowInfo.Cells[1].Value.ToString());
                            Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out modBusAddress);
                            if (deviceType != 0)
                            {
                                if (modBusAddress != 0)
                                {
                                    monitorInfo = new MonitorInfo();
                                    monitorInfo.monitorTypeAsInteger = deviceType;
                                    monitorInfo.modBusAddress = modBusAddress;
                                    monitorsByEntryOrder.Add(monitorInfo);
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByEntryOrder()\nInsufficent cells in this.systemConfigurationRadGridView.Rows[" + i.ToString() + "]";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByEntryOrder()\nInsufficent rows in this.systemConfigurationRadGridView";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByModbusAddress()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return monitorsByEntryOrder;
        }


        /// <summary>
        /// Gets the system configuration, expects it to already be error-checked
        /// </summary>
        /// <returns></returns>
        //        private SortedDictionary<int, int> GetModulesFromSystemConfigurationByModbusAddress()
        //        {
        //            SortedDictionary<int, int> modulesByModbusAddress = new SortedDictionary<int, int>();
        //            try
        //            {
        //                GridViewRowInfo rowInfo;
        //                int modBusAddress;
        //                int deviceType;
        //                if (this.systemConfigurationRadGridView.Rows.Count == 15)
        //                {
        //                    for (int i = 0; i < 15; i++)
        //                    {
        //                        rowInfo = this.systemConfigurationRadGridView.Rows[i];
        //                        if (rowInfo.Cells.Count > 4)
        //                        {
        //                            modBusAddress = 0;
        //                            deviceType = GetIntegerModuleTypeFromModuleName(rowInfo.Cells[1].Value.ToString());
        //                            Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out modBusAddress);
        //                            if (deviceType != 0)
        //                            {
        //                                if (modBusAddress != 0)
        //                                {
        //                                    if (!modulesByModbusAddress.ContainsKey(modBusAddress))
        //                                    {
        //                                        modulesByModbusAddress.Add(modBusAddress, deviceType);
        //                                    }
        //                                    else
        //                                    {
        //                                        string errorMessage = "Error in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByModbusAddress()\nDuplicate Modbus entry found in system configuration.";
        //                                        LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                                        MessageBox.Show(errorMessage);
        //#endif
        //                                        modulesByModbusAddress = null;
        //                                        break;
        //                                    }
        //                                }
        //                            }                          
        //                        }
        //                        else
        //                        {
        //                            string errorMessage = "Error in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByModbusAddress()\nInsufficent cells in this.systemConfigurationRadGridView.Rows[" + i.ToString() + "]";
        //                            LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                            MessageBox.Show(errorMessage);
        //#endif
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    string errorMessage = "Error in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByModbusAddress()\nInsufficent rows in this.systemConfigurationRadGridView";
        //                    LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                    MessageBox.Show(errorMessage);
        //#endif
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetModulesFromSystemConfigurationByModbusAddress()\nMessage: " + ex.Message;
        //                LogMessage.LogError(errorMessage);
        //#if DEBUG
        //                MessageBox.Show(errorMessage);
        //#endif
        //            }
        //            return modulesByModbusAddress;
        //        }

        private void WriteSystemConfigurationGridDataToCurrentConfiguration()
        {
            try
            {
                GridViewRowInfo rowInfo;
                int deviceType;
                int enabled;
                int modBusAddress;
                int baudRate;
                int dataTransferInterval;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.fullStatusInformationConfigObjects != null)
                    {
                        if (this.currentConfiguration.fullStatusInformationConfigObjects.Count > 14)
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                rowInfo = this.systemConfigurationRadGridView.Rows[i];
                                if (rowInfo.Cells.Count > 4)
                                {
                                    modBusAddress = 0;
                                    baudRate = 0;
                                    dataTransferInterval = 0;
                                    enabled = 0;

                                    deviceType = GetIntegerModuleTypeFromModuleName(rowInfo.Cells[1].Value.ToString());
                                    if (deviceType > 0)
                                    {
                                        enabled = 1;
                                    }
                                    Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out modBusAddress);
                                    Int32.TryParse(rowInfo.Cells[3].Value.ToString(), out baudRate);
                                    baudRate = GetShortendBaudRateFromFullBaudRate(baudRate);
                                    Int32.TryParse(rowInfo.Cells[4].Value.ToString(), out dataTransferInterval);

                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].ModuleType = deviceType;
                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].ModuleStatus = enabled;
                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].ModBusAddress = modBusAddress;
                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].BaudRate = baudRate;
                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].ReadDatePeriodInSeconds = dataTransferInterval;
                                    this.currentConfiguration.fullStatusInformationConfigObjects[i].ConnectionType = 0;
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(ref List<Main_Config_FullStatusInformation>)\nInsufficent cells in this.systemConfigurationRadGridView.Rows[" + i.ToString() + "]";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(List<Main_Config_FullStatusInformation>)\nthis.currentConfiguration.fullStatusInformation had too few elements, needs to have 15.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(List<Main_Config_FullStatusInformation>)\nthis.currentConfiguration.fullStatusInformation was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(List<Main_Config_FullStatusInformation>)\nthis.currentConfiguration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(GetSystemConfigurationDataFromGrid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool MissingValueInSystemConfigurationRadGridView()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int cellCount;
                int rowCount = this.systemConfigurationRadGridView.RowCount;
                int i, j;
                for (i = 0; i < rowCount; i++)
                {
                    rowInfo = this.systemConfigurationRadGridView.Rows[i];
                    cellCount = rowInfo.Cells.Count;
                    for (j = 0; j < cellCount; j++)
                    {
                        if (rowInfo.Cells[j].Value == null)
                        {
                            SelectSystemConfigurationTab();
                            rowInfo.Cells[j].IsSelected = true;
                            RadMessageBox.Show(this, emptyCellErrorMessage);
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.MissingValueInSystemConfigurationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        /// <summary>
        /// Right now makes sure that a ModBus address is not unique and that a ModBus address is assigned to every active device
        /// </summary>
        /// <returns></returns>
        private bool ErrorIsPresentInASystemConfigurationTabObject()
        {
            bool errorIsPresent = false;
            try
            {
                GridViewRowInfo rowInfo;
                int modBusAddress;
                int deviceType;
                Dictionary<int, int> modBusAddressesUsed = new Dictionary<int, int>();
                if (this.systemConfigurationRadGridView.Rows.Count == 15)
                {
                    errorIsPresent = MissingValueInSystemConfigurationRadGridView();

                    if (!errorIsPresent)
                    {
                        for (int i = 0; i < 15; i++)
                        {
                            rowInfo = this.systemConfigurationRadGridView.Rows[i];
                            if (rowInfo.Cells.Count > 4)
                            {
                                modBusAddress = 0;
                                deviceType = GetIntegerModuleTypeFromModuleName(rowInfo.Cells[1].Value.ToString());
                                Int32.TryParse(rowInfo.Cells[2].Value.ToString(), out modBusAddress);
                                if (deviceType != 0)
                                {
                                    if (modBusAddress != 0)
                                    {
                                        if ((modBusAddress > 0) && (modBusAddress < 256))
                                        {
                                            if (!modBusAddressesUsed.ContainsKey(modBusAddress))
                                            {
                                                modBusAddressesUsed.Add(modBusAddress, deviceType);
                                            }
                                            else
                                            {
                                                SelectSystemConfigurationTab();
                                                rowInfo.Cells[2].IsSelected = true;
                                                RadMessageBox.Show(this, duplicateModbusAddressDetectedText);
                                                errorIsPresent = true;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            rowInfo.Cells[2].IsSelected = true;
                                            RadMessageBox.Show(this, modbusAddressOutOfRangeText);
                                            errorIsPresent = true;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        rowInfo.Cells[2].IsSelected = true;
                                        RadMessageBox.Show(this, modbusAddressOutOfRangeText);
                                        errorIsPresent = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(ref List<Main_Config_FullStatusInformation>)\nInsufficent cells in this.systemConfigurationRadGridView.Rows[" + i.ToString() + "]";
                                LogMessage.LogError(errorMessage);
#if DEBUG
                                MessageBox.Show(errorMessage);
#endif
                            }
                        }
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(ref List<Main_Config_FullStatusInformation>)\nInsufficent rows in this.systemConfigurationRadGridView";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSystemConfigurationDataFromGrid(GetSystemConfigurationDataFromGrid)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return errorIsPresent;
        }

        private void CopyFullStatusInformationToShortStatusInformation(ref Main_Configuration configuration)
        {
            try
            {
                if (configuration != null)
                {
                    if (configuration.fullStatusInformationConfigObjects != null)
                    {
                        if (configuration.fullStatusInformationConfigObjects.Count > 14)
                        {
                            for (int i = 0; i < 15; i++)
                            {
                                configuration.shortStatusInformationConfigObjects[i].ModuleStatus = configuration.fullStatusInformationConfigObjects[i].ModuleStatus;
                                configuration.shortStatusInformationConfigObjects[i].ModuleType = configuration.fullStatusInformationConfigObjects[i].ModuleType;
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.CopyFullStatusInformationToShortStatusInformation(ref Main_Configuration)\nInput Main_Configuration.fullStatusInformationConfigObjects had too few elements, needs to be 15.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.CopyFullStatusInformationToShortStatusInformation(ref Main_Configuration)\nInput Main_Configuration.fullStatusInformationConfigObjects was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.CopyFullStatusInformationToShortStatusInformation(ref Main_Configuration)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.CopyFullStatusInformationToShortStatusInformation(ref Main_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private int GetSystemConfigBaudRateFromSelectedIndex(int selectedIndex)
        {
            int baudRate = 0;
            try
            {
                switch (selectedIndex)
                {
                    case 0:
                        baudRate = 96;
                        break;
                    case 1:
                        baudRate = 1152;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSystemConfigBaudRateFromSelectedIndex(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return baudRate;
        }

        private int GetSystemConfigSelectedIndexFromBaudRate(int baudRate)
        {
            int selectedIndex = 0;
            try
            {
                switch (baudRate)
                {
                    case 96:
                        selectedIndex = 0;
                        break;
                    case 1152:
                        selectedIndex = 1;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetSystemConfigSelectedIndexFromBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return selectedIndex;
        }

        private int GetFullBaudRateFromShortenedBaudRate(int shortenedBaudRate)
        {
            int fullBaudRate = 0;
            try
            {
                switch (shortenedBaudRate)
                {
                    case 96:
                        fullBaudRate = 9600;
                        break;
                    case 384:
                        fullBaudRate = 38400;
                        break;
                    case 576:
                        fullBaudRate = 57600;
                        break;
                    case 1152:
                        fullBaudRate = 115200;
                        break;
                    case 2304:
                        fullBaudRate = 230400;
                        break;
                    case 5000:
                        fullBaudRate = 500000;
                        break;
                    case 10000:
                        fullBaudRate = 1000000;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetFullBaudRateFromShortenedBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return fullBaudRate;
        }

        private int GetShortendBaudRateFromFullBaudRate(int fullBaudRate)
        {
            int shortenedBaudRate = 0;
            try
            {
                switch (fullBaudRate)
                {
                    case 9600:
                        shortenedBaudRate = 96;
                        break;
                    case 38400:
                        shortenedBaudRate = 384;
                        break;
                    case 57600:
                        shortenedBaudRate = 576;
                        break;
                    case 115200:
                        shortenedBaudRate = 1152;
                        break;
                    case 230400:
                        shortenedBaudRate = 2304;
                        break;
                    case 500000:
                        shortenedBaudRate = 5000;
                        break;
                    case 1000000:
                        shortenedBaudRate = 10000;
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetShortendBaudRateFromFullBaudRate(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return shortenedBaudRate;
        }

        private string GetThresholdTimeAsStringFromInteger(int thresholdTimeAsInteger)
        {
            string thresholdTimeAsString = "0 s.";
            try
            {
                switch (thresholdTimeAsInteger)
                {
                    case 0:
                        thresholdTimeAsString = "0 s.";
                        break;
                    case 1:
                        thresholdTimeAsString = "1 s.";
                        break;
                    case 2:
                        thresholdTimeAsString = "2 s.";
                        break;
                    case 3:
                        thresholdTimeAsString = "3 s.";
                        break;
                    case 4:
                        thresholdTimeAsString = "4 s.";
                        break;
                    case 5:
                        thresholdTimeAsString = "5 s.";
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetThresholdTimeAsStringFromInteger(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return thresholdTimeAsString;
        }

        private int GetThresholdTimeAsIntegerFromString(string thresholdTimeAsString)
        {
            int thresholdTimeAsInteger = 0;
            try
            {
                if (thresholdTimeAsString != null)
                {
                    switch (thresholdTimeAsString)
                    {
                        case "0 s.":
                            thresholdTimeAsInteger = 0;
                            break;
                        case "1 s.":
                            thresholdTimeAsInteger = 1;
                            break;
                        case "2 s.":
                            thresholdTimeAsInteger = 2;
                            break;
                        case "3 s.":
                            thresholdTimeAsInteger = 3;
                            break;
                        case "4 s.":
                            thresholdTimeAsInteger = 4;
                            break;
                        case "5 s.":
                            thresholdTimeAsInteger = 5;
                            break;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetThresholdTimeAsIntegerFromString(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetThresholdTimeAsIntegerFromString(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return thresholdTimeAsInteger;
        }

        //private int GetSystemConfigDeviceTypeFromSelectedIndex(int selectedIndex)
        //{
        //    int deviceType = 0;
        //    RadMessageBox.Show(this, "Called an empty function");
        //    switch (selectedIndex)
        //    {




        //    }

        //    return deviceType;
        //}

        //private int GetSystemConfigSelectedIndexFromDeviceType(int deviceType)
        //{
        //    int selectedIndex = 0;
        //    RadMessageBox.Show(this, "Called an empty function");
        //    switch (deviceType)
        //    {



        //    }

        //    return selectedIndex;
        //}

        private string GetModuleNameFromIntegerModuleType(int deviceType)
        {
            string moduleName = "Not Selected";
            try
            {
                switch (deviceType)
                {
                    case 101:
                        moduleName = "Main Monitor";
                        break;
                    case 505:
                        moduleName = "PD Monitor";
                        break;
                    case 15002:
                        moduleName = "Bushing Monitor";
                        break;
                    case 6001:
                        moduleName = "Acoustic Monitor";
                        break;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetModuleNameFromIntegerModuleType(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return moduleName;
        }

        private int GetIntegerModuleTypeFromModuleName(string moduleName)
        {
            int moduleType = 0;
            try
            {
                if (moduleName != null)
                {
                    switch (moduleName)
                    {
                        case "Main Monitor":
                            moduleType = 101;
                            break;
                        case "PD Monitor":
                            moduleType = 505;
                            break;
                        case "Bushing Monitor":
                            moduleType = 15002;
                            break;
                        case "Acoustic Monitor":
                            moduleType = 6001;
                            break;
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.GetIntegerModuleTypeFromModuleName(string)\nInput string was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.GetIntegerModuleTypeFromModuleName(string)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return moduleType;
        }

        //private int GetIntegerDataSourceFromStringDataSource(string stringDataSource)
        //{
        //    int integerDataSource = 0;

        //    switch (stringDataSource)
        //    {


        //    }
        //    return integerDataSource;
        //}

        #endregion
    }
   
}
