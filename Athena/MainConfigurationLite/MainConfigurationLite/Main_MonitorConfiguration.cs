using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

using Telerik.WinControls;
using Telerik.WinControls.UI;
using DatabaseInterface;
using FormatConversion;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;

namespace MainConfigurationLite
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        GridViewTemplate vibrationThresholdGridViewTemplate;
        GridViewTemplate analogInThresholdGridViewTemplate;
        GridViewTemplate currentThresholdGridViewTemplate;
        GridViewTemplate voltageThresholdGridViewTemplate;
        GridViewTemplate humidityThresholdGridViewTemplate;
        GridViewTemplate temperatureThresholdGridViewTemplate;
        GridViewTemplate chassisTemperatureThresholdGridViewTemplate;

        GridViewRelation vibrationGridViewRelation;
        GridViewRelation analogInGridViewRelation;
        GridViewRelation currentGridViewRelation;
        GridViewRelation voltageGridViewRelation;
        GridViewRelation humidityGridViewRelation;
        GridViewRelation temperatureGridViewRelation;
        GridViewRelation chassisTemperatureGridViewRelation;

        //List<ThresholdData> vibrationThresholdDataList;
        //List<ThresholdData> analogInThresholdDataList;
        //List<ThresholdData> currentThresholdDataList;
        //List<ThresholdData> voltageThresholdDataList;
        //List<ThresholdData> humidityThresholdDataList;
        //List<ThresholdData> temperatureThresholdDataList;
        //List<ThresholdData> chassisTemperatureThresholdDataList;

        //List<VibrationData> vibrationDataList;
        //List<PressureData> pressureDataList;
        //List<CurrentData> currentDataList;
        //List<VoltageData> voltageDataList;
        //HumidityData humidityData;
        //List<TemperatureData> temperatureDataList;
        //ChassisTemperatureData chassisTemperatureData;

        Main_Configuration currentConfiguration;
        Main_Configuration uneditedCurrentConfiguration;
        //Main_Configuration configurationFromDatabase;
        //Main_Configuration configurationFromDevice;

        List<MainConfigUISystemConfigurationData> systemConfigurationList;

        List<TransferSetupData> transferSetupDataList;

      
        //private bool downloadWasInProgress;

        //private bool configurationError;

        private int tableHeaderHeight = 50;
        private int entryTypeWidth = 120;
        private int sensorNumberWidth = 60;
        private int toSaveWidth = 120;
        private int enableEntryWidth = 65;
        private int sensitivityWidth = 80;

        //private int availableConfigurationsSelectedIndex;

        //int readDelayInMicroseconds;

        ///// <summary>
        /////  just for testing to find values I can't figure out in the configuration
        ///// </summary>
        //private Int16[] previousConfigurationAsArray;
        ///// <summary>
        /////  just for testing to find values I can't figure out in the configuration
        ///// </summary>
        //private Int16[] currentConfigurationAsArray;

        //private static string htmlPrefix = "<html>";
        //private static string htmlSuffix = "</html>";
        //private static string htmlFontType = "<font=Microsoft Sans Serif>";
        //private static string htmlStandardFontSize = "<size=8.25>";

        //private static string mainMonitorConfigurationTitleText = "Main Monitor Configuration";

        //private static string availableConfigurationsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configurations by Date Saved</html>";
        //private static string saveCurrentConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Working Copy<br>of Configuration<br>to Database</html>";
        //private static string saveDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Save Device<br>Configuration<br>to Database</html>";
        //private static string loadConfigurationFromDatabaseRadButtonText = "<html><font=Microsoft Sans Serif>Load Selected<br>Configuration from<br>Database</html>";
        //private static string programDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Program Device<br>with Working Copy<br>of Configuration</html>";
        //private static string loadConfigurationFromDeviceRadButtonText = "<html><font=Microsoft Sans Serif>Load<br>Configuration from<br>Device</html>";
        //private static string deleteConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Delete Selected<br>Configuration from<br>Database</html>";
        //private static string configurationViewSelectRadGroupBoxText = "<html><font=Microsoft Sans Serif>Configuration View Select</html>";
        //private static string fromDeviceRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Device (Read Only)</html>";
        //private static string fromDatabaseRadRadioButtonText = "<html><font=Microsoft Sans Serif>From Database (Read Only)</html>";
        //private static string currentConfigurationRadRadioButtonText = "<html><font=Microsoft Sans Serif>Working Copy</html>";
        //private static string copyDeviceConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Device Configuration<br>to Working Copy</html>";
        //private static string copyDatabaseConfigurationRadButtonText = "<html><font=Microsoft Sans Serif>Copy Database Configuration<br>to Working Copy</html>";

        private static string emptyCellErrorMessage = "There is a value missing from the grid.";
        private static string uploadingConfigurationText = "Uploading Configuration";
        private static string downloadingConfigurationText = "Downloading Configuration";
        //private static string couldNotFindCurrentConfigurationDisplayingMostRecentText = "Could not find the current device configuration, displaying most recent configuration saved.";
        //private static string couldNotFindAnyDeviceConfigurationText = "Could not find any device configurations in the database.";
        //private static string failedToUploadConfigurationText = "Failed to upload the configuration for some reason.";
        private static string errorSendingPasswordToDeviceText = "Error in sending the password to the device.\nPlease check the connection.";
        //private static string noDatabaseConfigurationLoadedText = "No database configuration has been loaded.";
        //private static string noDeviceConfigurationLoadedText = "No device configuration has been loaded.";
        private static string currentConfigurationNotSavedWarningText = "You have made changes to the working copy that have not been saved.  Exit anyway?";
        private static string exitWithoutSavingQuestionText = "Exit without saving?";

        //private static string overwriteCurrentConfigurationText = "This will overwrite the working copy of the configuration.  Any changes will be lost.";

        private static string offRelayText = "off";
        private static string onRelayText = "on";
        private static string externalDevicesRelayText = "External devices";
        private static string nullDeviceRelayText = "null device";

        //private static string generalSettingsRadPageViewPageText = "<html><font=Microsoft Sans Serif>General Settings</html>";
        //private static string monitoringRadGroupBoxText = "<html><font=Microsoft Sans Serif>Monitoring</html>";
        //private static string enableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Enable</html>";
        //private static string disableMonitoringRadRadioButtonText = "<html><font=Microsoft Sans Serif>Disable</html>";
        //private static string frequencyRadGroupBoxText = "<html><font=Microsoft Sans Serif>Frequency</html>";
        //private static string hertzRadLabelText = "<html><font=Microsoft Sans Serif>Hz.</html>";
        //private static string versionRadGroupBoxText = "<html><font=Microsoft Sans Serif>Firmware Version</html>";
        //private static string boardNameRadGroupBoxText = "<html><font=Microsoft Sans Serif>Board Name</html>";
        //private static string connectionParametersRadGroupBoxText = "<html><font=Microsoft Sans Serif>Connection Parameters</html>";
        //private static string modbusAddressRadLabelText = "<html><font=Microsoft Sans Serif>Modbus Address</html>";
        //private static string serialConnectionRadGroupBoxText = "<html><font=Microsoft Sans Serif>RS 485</html>";
        //private static string serialBaudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        //private static string ethernetRadGroupBoxText = "<html><font=Microsoft Sans Serif>Ethernet</html>";
        //private static string ethernetBaudRateRadLabelText = "<html><font=Microsoft Sans Serif>Baud Rate</html>";
        //private static string ipAddressRadLabelText = "<html><font=Microsoft Sans Serif>I.P. Address</html>";
        //private static string relayRadGroupBoxText = "<html><font=Microsoft Sans Serif>Relay</html>";
        //private static string generalSettingsAlarmRadLabelText = "<html><font=Microsoft Sans Serif>Alarm</html>";
        //private static string generalSettingsWarningRadLabelText = "<html><font=Microsoft Sans Serif>Warning</html>";
        //private static string modeSaveRadGroupBoxText = "<html><font=Microsoft Sans Serif>Mode Save</html>";
        //private static string stepRadLabelText = "<html><font=Microsoft Sans Serif>Step</html>";
        //private static string minutesRadLabelText = "<html><font=Microsoft Sans Serif>minutes</html>";
        //private static string useDiagnosticResultsRadCheckBoxText = "<html><font=Microsoft Sans Serif>Take into account<br>results of diagnostic</html>";
        //private static string otherOptionsRadGroupBoxText = "<html><font=Microsoft Sans Serif>Other options</html>";
        //private static string enableCurrentSignatureAnalysisRadCheckBoxText = "<html><font=Microsoft Sans Serif>Enable current signature analysis</html>";
        //private static string allowDirectAccessRadCheckBoxText = "<html><font=Microsoft Sans Serif>Allow direct access to the<br>internal modules over RS 485</html>";

        //private static string ethernetProtocolRadLabelText = "Protocol";

        //private static string systemConfigurationRadPageViewPageText = "System Configuration";
      
        //private static string analogInputsRadPageViewPageText = "Analog Inputs";
        //private static string controlParametersRadGroupBoxText = "Control Parameters";
        //private static string vibrationRadRadioButtonText = "Vibration";
        //private static string pressureRadRadioButtonText = "Pressure";
        //private static string currentRadRadioButtonText = "Current";
        //private static string voltageRadRadioButtonText = "Voltage";
        //private static string humidityRadRadioButtonText = "Humidity";
        //private static string temperatureRadRadioButtonText = "Temperature";
        //private static string chassisTemperatureRadRadioButtonText = "Chassis Temperature";
       

        //private static string calibrationRadPageViewPageText = "Calibration";
        //private static string dataTransferRadPageViewPageText = "Data Transfer";


        private List<Main_Config_ConfigurationRoot> templateConfigurations;
        private int templateConfigurationsSelectedIndex;
        private string noTemplateConfigurationsAvailable = "No template configurations are available";
        private string templateDbConnectionString;

        private static string currentConfigName = "Current Device Configuration";
        public static string CurrentConfigName
        {
            get
            {
                return currentConfigName;
            }
        }

        //private string dbConnectionString;
        //private string fullPathToXmlLanguageFile;

        bool systemConfigurationIsCorrect = false;

        //private string serialPort;
        //private int baudRate;

        private int modBusAddress;
        private int numberOfNonInteractiveDeviceCommunicationTries;
        private int totalNumberOfDeviceCommunicationTries;

        public Main_MonitorConfiguration(int inputModBusAddress, int inputNumberOfNonInteractiveDeviceCommunicationTries, int inputTotalNumberOfDeviceCommunicationTries, string inputTemplateDbConnectionString)
        {
            try
            {
                InitializeComponent();              

                this.modBusAddress = inputModBusAddress;
                this.numberOfNonInteractiveDeviceCommunicationTries = inputNumberOfNonInteractiveDeviceCommunicationTries;
                this.totalNumberOfDeviceCommunicationTries = inputTotalNumberOfDeviceCommunicationTries;
                this.templateDbConnectionString = inputTemplateDbConnectionString;

                this.StartPosition = FormStartPosition.CenterParent;

//                if (inputMonitor != null)
//                {
//                    string connectionType = this.monitor.ConnectionType.Trim();
//                    if (connectionType.CompareTo("USB") == 0)
//                    {
//                        2 = MainDisplay.directConnectionUSBReadDelay;
//                    }
//                    else if (connectionType.CompareTo("SOE") == 0)
//                    {
//                        2 = MainDisplay.directConnectionEthernetReadDelay;
//                    }
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_MonitorConfiguration.Main_MonitorConfiguration(Monitor, bool)\nInput Monitor was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_MonitorConfiguration(Monitor, bool)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void Main_MonitorConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                RadMessageBox.ThemeName = "Office2007Black";
                Cursor.Current = Cursors.WaitCursor;
                this.UseWaitCursor = true;
                this.systemConfigurationList = new List<MainConfigUISystemConfigurationData>();
         
                DisableProgressBars();

                InitializationsForDataTransferVariables();

                InitializeDataTransferMasterDeviceRegisterNamesList();
                InitializeDataTransferADMSlaveDeviceRegisterNamesList();
                InitializeDataTransferBHMSlaveDeviceRegisterNamesList();
                InitializeDataTransferPDMSlaveDeviceRegisterNamesList();

                InitializeSystemConfigurationRadGridView();
                InitializeAnalogInputsRadGridViews();
                InitializeAllThresholdRadGridViews();
                InitializeCalibrationRadGridView();
                InitializeDataTransferRadGridView();

                EstablishAllParentChildGridRelations();
                SetAllAnalogInputsGridViewLocationAndSize();

                // GetConfigurationInformation();

                AddEmptyRowsToTheSystemConfigurationGrid();
                AddEmptyRowsToAllAnalogInputsGrids();
                AddEmptyRowsToTheCalibrationGrid();
                AddEmptyRowsToTheDataTransferGrid();

                this.transferSetupDataList = new List<TransferSetupData>();
                //InitializeDataStorageLists();
                //InitializeThresholdDataStorageLists();

                this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

                serialBaudRateRadDropDownList.DataSource = new string[] { "9600", "38400", "57600", "115200" };
                ethernetBaudRateRadDropDownList.DataSource = new string[] { "9600", "38400", "57600", "115200", "921600" };
                ethernetProtocolRadDropDownList.DataSource = new string[] { "RTU", "TCP" };
                relayAlarmRadDropDownList.DataSource = new string[] { offRelayText, onRelayText, externalDevicesRelayText, nullDeviceRelayText};
                relayWarningRadDropDownList.DataSource = new string[] { offRelayText, onRelayText, externalDevicesRelayText, nullDeviceRelayText };

                // DisplayMostRecentConfigurationInTheDatabase();

                this.enableCurrentSignatureAnalysisRadCheckBox.Enabled = false;

                this.firmwareVersionRadTextBox.ReadOnly = true;

                // this.objectNameRadTextBox.ReadOnly = true;

                //serialBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                //ethernetBaudRateRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                //relayAlarmRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;
                //relayWarningRadDropDownList.DropDownListElement.TextBox.TextBoxItem.ReadOnly = true;

                configurationRadPageView.SelectedPage = generalSettingsRadPageViewPage;

                CollapseGridViewForAllAnalogInputsGrids();

                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                {
                    LoadTemplateConfigurations(templateDB);
                }
                LoadConfigurationFromDeviceAndAssignItToConfigObject();
               
               
                this.UseWaitCursor = false;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_MonitorConfiguration_Load(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

//        public void AssignStringValuesToInterfaceObjects()
//        {
//            try
//            {
//                this.Text = mainMonitorConfigurationTitleText;

//                generalSettingsRadPageViewPage.Text = generalSettingsRadPageViewPageText;
//                monitoringRadGroupBox.Text = monitoringRadGroupBoxText;
//                enableMonitoringRadRadioButton.Text = enableMonitoringRadRadioButtonText;
//                disableMonitoringRadRadioButton.Text = disableMonitoringRadRadioButtonText;
//                frequencyRadGroupBox.Text = frequencyRadGroupBoxText;
//                hertzRadLabel.Text = hertzRadLabelText;
//                versionRadGroupBox.Text = versionRadGroupBoxText;
//                boardNameRadGroupBox.Text = boardNameRadGroupBoxText;
//                connectionParametersRadGroupBox.Text = connectionParametersRadGroupBoxText;
//                modbusAddressRadLabel.Text = modbusAddressRadLabelText;
//                serialConnectionRadGroupBox.Text = serialConnectionRadGroupBoxText;
//                serialBaudRateRadLabel.Text = serialBaudRateRadLabelText;
//                ethernetRadGroupBox.Text = ethernetRadGroupBoxText;
//                ethernetBaudRateRadLabel.Text = ethernetBaudRateRadLabelText;
//                ipAddressRadLabel.Text = ipAddressRadLabelText;
//                relayRadGroupBox.Text = relayRadGroupBoxText;
//                generalSettingsAlarmRadLabel.Text = generalSettingsAlarmRadLabelText;
//                generalSettingsWarningRadLabel.Text = generalSettingsWarningRadLabelText;
//                modeSaveRadGroupBox.Text = modeSaveRadGroupBoxText;
//                stepRadLabel.Text = stepRadLabelText;
//                minutesRadLabel.Text = minutesRadLabelText;
//                useDiagnosticResultsRadCheckBox.Text = useDiagnosticResultsRadCheckBoxText;
//                otherOptionsRadGroupBox.Text = otherOptionsRadGroupBoxText;
//                enableCurrentSignatureAnalysisRadCheckBox.Text = enableCurrentSignatureAnalysisRadCheckBoxText;
//                allowDirectAccessRadCheckBox.Text = allowDirectAccessRadCheckBoxText;
//                availableConfigurationsGeneralSettingsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
//                saveCurrentConfigurationGeneralSettingsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
//                saveDeviceConfigurationGeneralSettingsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
//                loadConfigurationFromDatabaseGeneralSettingsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
//                programDeviceGeneralSettingsTabRadButton.Text = programDeviceRadButtonText;
//                loadConfigurationFromDeviceGeneralSettingsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
//                deleteConfigurationGeneralSettingsTabRadButton.Text = deleteConfigurationRadButtonText;
//                configurationViewSelectGeneralSettingsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
//                fromDeviceGeneralSettingsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
//                fromDatabaseGeneralSettingsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
//                currentGeneralSettingsTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
//                copyDeviceConfigurationGeneralSettingsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
//                copyDatabaseConfigurationGeneralSettingsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

//                ethernetProtocolRadLabel.Text = ethernetBaudRateRadLabelText;

//                systemConfigurationRadPageViewPage.Text = systemConfigurationRadPageViewPageText;
//                availableConfigurationsSystemConfigurationTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
//                saveCurrentConfigurationSystemConfigurationTabRadButton.Text = saveCurrentConfigurationRadButtonText;
//                saveDeviceConfigurationSystemConfigurationTabRadButton.Text = saveDeviceConfigurationRadButtonText;
//                loadConfigurationFromDatabaseSystemConfigurationTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
//                programDeviceSystemConfigurationTabRadButton.Text = programDeviceRadButtonText;
//                loadConfigurationFromDeviceSystemConfigurationTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
//                deleteConfigurationSystemConfigurationTabRadButton.Text = deleteConfigurationRadButtonText;
//                configurationViewSelectSystemConfigurationTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
//                fromDeviceSystemConfigurationTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
//                fromDatabaseSystemConfigurationTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
//                currentSystemConfigurationTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
//                copyDeviceConfigurationSystemConfigurationTabRadButton.Text = copyDeviceConfigurationRadButtonText;
//                copyDatabaseConfigurationSystemConfigurationTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

//                analogInputsRadPageViewPage.Text = analogInputsRadPageViewPageText;
//                controlParametersRadGroupBox.Text = controlParametersRadGroupBoxText;
//                vibrationRadRadioButton.Text = vibrationRadRadioButtonText;
//                pressureRadRadioButton.Text = pressureRadRadioButtonText;
//                currentRadRadioButton.Text = currentRadRadioButtonText;
//                voltageRadRadioButton.Text = voltageRadRadioButtonText;
//                humidityRadRadioButton.Text = humidityRadRadioButtonText;
//                temperatureRadRadioButton.Text = temperatureRadRadioButtonText;
//                chassisTemperatureRadRadioButton.Text = chassisTemperatureRadRadioButtonText;
//                availableConfigurationsAnalogInputsTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
//                saveCurrentConfigurationAnalogInputsTabRadButton.Text = saveCurrentConfigurationRadButtonText;
//                saveDeviceConfigurationAnalogInputsTabRadButton.Text = saveDeviceConfigurationRadButtonText;
//                loadConfigurationFromDatabaseAnalogInputsTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
//                programDeviceAnalogInputsTabRadButton.Text = programDeviceRadButtonText;
//                loadConfigurationFromDeviceAnalogInputsTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
//                deleteConfigurationAnalogInputsTabRadButton.Text = deleteConfigurationRadButtonText;
//                configurationViewSelectAnalogInputsTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
//                fromDeviceAnalogInputsTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
//                fromDatabaseAnalogInputsTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
//                currentAnalogInputsTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
//                copyDeviceConfigurationAnalogInputsTabRadButton.Text = copyDeviceConfigurationRadButtonText;
//                copyDatabaseConfigurationAnalogInputsTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

//                calibrationRadPageViewPage.Text = calibrationRadPageViewPageText;
//                availableConfigurationsCalibrationTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
//                saveCurrentConfigurationCalibrationTabRadButton.Text = saveCurrentConfigurationRadButtonText;
//                saveDeviceConfigurationCalibrationTabRadButton.Text = saveDeviceConfigurationRadButtonText;
//                loadConfigurationFromDatabaseCalibrationTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
//                programDeviceCalibrationTabRadButton.Text = programDeviceRadButtonText;
//                loadConfigurationFromDeviceCalibrationTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
//                deleteConfigurationCalibrationTabRadButton.Text = deleteConfigurationRadButtonText;
//                configurationViewSelectCalibrationTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
//                fromDeviceCalibrationTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
//                fromDatabaseCalibrationTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
//                currentCalibrationTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
//                copyDeviceConfigurationCalibrationTabRadButton.Text = copyDeviceConfigurationRadButtonText;
//                copyDatabaseConfigurationCalibrationTabRadButton.Text = copyDatabaseConfigurationRadButtonText;

//                dataTransferRadPageViewPage.Text = dataTransferRadPageViewPageText;
//                availableConfigurationsDataTransferTabRadGroupBox.Text = availableConfigurationsRadGroupBoxText;
//                saveCurrentConfigurationDataTransferTabRadButton.Text = saveCurrentConfigurationRadButtonText;
//                saveDeviceConfigurationDataTransferTabRadButton.Text = saveDeviceConfigurationRadButtonText;
//                loadConfigurationFromDatabaseDataTransferTabRadButton.Text = loadConfigurationFromDatabaseRadButtonText;
//                programDeviceDataTransferTabRadButton.Text = programDeviceRadButtonText;
//                loadConfigurationFromDeviceDataTransferTabRadButton.Text = loadConfigurationFromDeviceRadButtonText;
//                deleteConfigurationDataTransferTabRadButton.Text = deleteConfigurationRadButtonText;
//                configurationViewSelectDataTransferTabRadGroupBox.Text = configurationViewSelectRadGroupBoxText;
//                fromDeviceDataTransferTabRadRadioButton.Text = fromDeviceRadRadioButtonText;
//                fromDatabaseDataTransferTabRadRadioButton.Text = fromDatabaseRadRadioButtonText;
//                currentDataTransferTabRadRadioButton.Text = currentConfigurationRadRadioButtonText;
//                copyDeviceConfigurationDataTransferTabRadButton.Text = copyDeviceConfigurationRadButtonText;
//                copyDatabaseConfigurationDataTransferTabRadButton.Text = copyDatabaseConfigurationRadButtonText;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AssignStringValuesToInterfaceObjects()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        public static void AssignValuesToInternalStaticStrings()
//        {
//            try
//            {
//                htmlFontType = LanguageConversion.GetStringAssociatedWithTag("HTMLFontType", htmlFontType, "", "", "");
//                htmlStandardFontSize = LanguageConversion.GetStringAssociatedWithTag("HTMLStandardFontSize", htmlStandardFontSize, "", "", "");

//                duplicateModbusAddressDetectedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDuplicateModbusAddressDetectedText", duplicateModbusAddressDetectedText, htmlFontType, htmlStandardFontSize, "");
//                modbusAddressOutOfRangeText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModbusAddressOutOfRangeText", modbusAddressOutOfRangeText, htmlFontType, htmlStandardFontSize, "");
//                monitorNumberSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorNumberSystemConfigGridViewText", monitorNumberSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                monitorTypeSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeSystemConfigGridViewText", monitorTypeSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                monitorTypeNoneSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeNoneSystemConfigGridViewText", monitorTypeNoneSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                monitorTypeBushingSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeBushingSystemConfigGridViewText", monitorTypeBushingSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                monitorTypePDMonitorSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypePDMonitorSystemConfigGridViewText", monitorTypePDMonitorSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                monitorTypeAcousticSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMonitorTypeAcousticSystemConfigGridViewText", monitorTypeAcousticSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                modbusAddressSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModbusAddressSystemConfigGridViewText", modbusAddressSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                baudRateSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationBaudRateSystemConfigGridViewText", baudRateSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");
//                dataTransferSystemConfigGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDataTransferSystemConfigGridViewText", dataTransferSystemConfigGridViewText, htmlFontType, htmlStandardFontSize, "");

//                failedToSaveConfigurationToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToSaveConfigurationToDatabaseText", failedToSaveConfigurationToDatabaseText, htmlFontType, htmlStandardFontSize, "");
//                noCurrentConfigurationDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoCurrentConfigurationDefinedText", noCurrentConfigurationDefinedText, htmlFontType, htmlStandardFontSize, "");
//                noConfigurationLoadedFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoConfigurationLoadedFromDeviceText", noConfigurationLoadedFromDeviceText, htmlFontType, htmlStandardFontSize, "");
//                configurationBeingSavedIsFromDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationBeingSavedIsFromDeviceText", configurationBeingSavedIsFromDeviceText, htmlFontType, htmlStandardFontSize, "");
//                deviceConfigurationSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationSavedToDatabaseText", deviceConfigurationSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
//                deviceConfigurationNotSavedToDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationNotSavedToDatabaseText", deviceConfigurationNotSavedToDatabaseText, htmlFontType, htmlStandardFontSize, "");
//                errorInConfigurationLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationErrorInConfigurationLoadedFromDatabaseText", errorInConfigurationLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
//                configurationCouldNotBeLoadedFromDatabaseText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationCouldNotBeLoadedFromDatabaseText", configurationCouldNotBeLoadedFromDatabaseText, htmlFontType, htmlStandardFontSize, "");
//                configurationDeleteFromDatabaseWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationDeleteFromDatabaseWarningText", configurationDeleteFromDatabaseWarningText, htmlFontType, htmlStandardFontSize, "");
//                deleteAsQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeleteAsQuestionText", deleteAsQuestionText, htmlFontType, htmlStandardFontSize, "");
//                cannotDeleteCurrentConfigurationWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCannotDeleteCurrentConfigurationWarningText", cannotDeleteCurrentConfigurationWarningText, htmlFontType, htmlStandardFontSize, "");
//                noConfigurationSelectedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoConfigurationSelectedText", noConfigurationSelectedText, htmlFontType, htmlStandardFontSize, "");
//                failedToDownloadDeviceConfigurationDataText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToDownloadDeviceConfigurationDataText", failedToDownloadDeviceConfigurationDataText, htmlFontType, htmlStandardFontSize, "");
//                commandToReadDeviceErrorStateFailedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCommandToReadDeviceErrorStateFailedText", commandToReadDeviceErrorStateFailedText, htmlFontType, htmlStandardFontSize, "");
//                lostDeviceConnectionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLostDeviceConnectionText", lostDeviceConnectionText, htmlFontType, htmlStandardFontSize, "");
//                notConnectedToMainMonitorWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNotConnectedToMainMonitorWarningText", notConnectedToMainMonitorWarningText, htmlFontType, htmlStandardFontSize, "");
//                serialPortNotSetWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSerialPortNotSetWarningText", serialPortNotSetWarningText, htmlFontType, htmlStandardFontSize, "");
//                failedToOpenMonitorConnectionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToOpenMonitorConnectionText", failedToOpenMonitorConnectionText, htmlFontType, htmlStandardFontSize, "");
//                deviceCommunicationNotProperlyConfiguredText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceCommunicationNotProperlyConfiguredText", deviceCommunicationNotProperlyConfiguredText, htmlFontType, htmlStandardFontSize, "");
//                downloadWasInProgressWhenInterfaceWasOpenedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDownloadWasInProgressWhenInterfaceWasOpenedText", downloadWasInProgressWhenInterfaceWasOpenedText, htmlFontType, htmlStandardFontSize, "");
//                failedToWriteTheConfigurationToTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToWriteTheConfigurationToTheDevice", failedToWriteTheConfigurationToTheDevice, htmlFontType, htmlStandardFontSize, "");
//                configurationWasReadFromTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationWasReadFromTheDevice", configurationWasReadFromTheDevice, htmlFontType, htmlStandardFontSize, "");
//                configurationWasWrittenToTheDevice = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationConfigurationWasWrittenToTheDevice", configurationWasWrittenToTheDevice, htmlFontType, htmlStandardFontSize, "");
//                deviceConfigurationDoesNotMatchDatabaseConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceConfigurationDoesNotMatchDatabaseConfigurationText", deviceConfigurationDoesNotMatchDatabaseConfigurationText, htmlFontType, htmlStandardFontSize, "");
//                deviceCommunicationNotSavedInDatabaseYetText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDeviceCommunicationNotSavedInDatabaseYetText", deviceCommunicationNotSavedInDatabaseYetText, htmlFontType, htmlStandardFontSize, "");
//                saveDeviceConfigurationQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSaveDeviceConfigurationQuestionText", saveDeviceConfigurationQuestionText, htmlFontType, htmlStandardFontSize, "");
//                currentConfigurationNotDefinedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentConfigurationNotDefinedText", currentConfigurationNotDefinedText, htmlFontType, htmlStandardFontSize, "");
//                commandToReadDeviceTypeFailed = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCommandToReadDeviceTypeFailed", commandToReadDeviceTypeFailed, htmlFontType, htmlStandardFontSize, "");

//                formatErrorInIPAddressText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFormatErrorInIPAddressText", formatErrorInIPAddressText, htmlFontType, htmlStandardFontSize, "");
//                digitErrorInIPAddressText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDigitErrorInIPAddressText", digitErrorInIPAddressText, htmlFontType, htmlStandardFontSize, "");

//                noValuesAvailableDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoValuesAvailableDataTransferText", noValuesAvailableDataTransferText, htmlFontType, htmlStandardFontSize, "");
//                noValueSetDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoValueSetDataTransferText", noValueSetDataTransferText, htmlFontType, htmlStandardFontSize, "");
//                noneDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoneDataTransferText", noneDataTransferText, htmlFontType, htmlStandardFontSize, "");
//                humidityDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityDataTransferText", humidityDataTransferText, htmlFontType, htmlStandardFontSize, "");
//                ambientTempDataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAmbientTempDataTransferText", ambientTempDataTransferText, htmlFontType, htmlStandardFontSize, "");
//                temp1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp1DataTransferText", temp1DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                temp2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp2DataTransferText", temp2DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                temp3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemp3DataTransferText", temp3DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                loadCurrent1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent1DataTransferText", loadCurrent1DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                loadCurrent2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent2DataTransferText", loadCurrent2DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                loadCurrent3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLoadCurrent3DataTransferText", loadCurrent3DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                voltage1DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage1DataTransferText", voltage1DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                voltage2DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage2DataTransferText", voltage2DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                voltage3DataTransferText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltage3DataTransferText", voltage3DataTransferText, htmlFontType, htmlStandardFontSize, "");
//                transferSetupNumberDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTransferSetupNumberDataTransferGridViewText", transferSetupNumberDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelDataTransferGridViewText", enableChannelDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                masterDeviceRegisterDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMasterDeviceRegisterDataTransferGridViewText", masterDeviceRegisterDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                slaveDeviceNameDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceNameDataTransferGridViewText", slaveDeviceNameDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                slaveDeviceModbusAddressDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceModbusAddressDataTransferGridViewText", slaveDeviceModbusAddressDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                slaveDeviceRegisterDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlaveDeviceRegisterDataTransferGridViewText", slaveDeviceRegisterDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                slopeDataTransferGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSlopeDataTransferGridViewText", slopeDataTransferGridViewText, htmlFontType, htmlStandardFontSize, "");
//                incorrectDataTransferTabEntryMessage = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationIncorrectDataTransferTabEntryMessage", incorrectDataTransferTabEntryMessage, htmlFontType, htmlStandardFontSize, "");

//                transferParameterNameCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTransferParameterNameCalibrationGridViewText", transferParameterNameCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                nullLineCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNullLineCalibrationGridViewText", nullLineCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                multiplierCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationMultiplierCalibrationGridViewText", multiplierCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                offsetCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffsetCalibrationGridViewText", offsetCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                channelNoiseCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChannelNoiseCalibrationGridViewText", channelNoiseCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                vibrationCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVibrationCalibrationGridViewText", vibrationCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                pressureCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationPressureCalibrationGridViewText", pressureCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                currentCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentCalibrationGridViewText", currentCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                voltageCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltageCalibrationGridViewText", voltageCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                humidityCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityCalibrationGridViewText", humidityCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                temperatureCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemperatureCalibrationGridViewText", temperatureCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                chassisTemperatureCalibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChassisTemperatureCalibrationGridViewText", chassisTemperatureCalibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeVibrationGridViewText", entryTypeVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelVibrationGridViewText", enableChannelVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberVibrationGridViewText", sensorNumberVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensitivityVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityVibrationGridViewText", sensitivityVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                boundaryFrequencyVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationBoundaryFrequencyVibrationGridViewText", boundaryFrequencyVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                numberOfPointsVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNumberOfPointsVibrationGridViewText", numberOfPointsVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeVibrationGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeVibrationGridViewText", toSaveAtChangeVibrationGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypePressureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypePressureGridViewText", entryTypePressureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelPressureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelPressureGridViewText", enableChannelPressureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberPressureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberPressureGridViewText", sensorNumberPressureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensitivityPressureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityPressureGridViewText", sensitivityPressureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangePressureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangePressureGridViewText", toSaveAtChangePressureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeCurrentGridViewText", entryTypeCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelCurrentGridViewText", enableChannelCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberCurrentGridViewText", sensorNumberCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensitivityCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityCurrentGridViewText", sensitivityCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeCurrentGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeCurrentGridViewText", toSaveAtChangeCurrentGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeVoltageGridViewText", entryTypeVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelVoltageGridViewText", enableChannelVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberVoltageGridViewText", sensorNumberVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensitivityVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensitivityVoltageGridViewText", sensitivityVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeVoltageGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeVoltageGridViewText", toSaveAtChangeVoltageGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeHumidityGridViewText", entryTypeHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelHumidityGridViewText", enableChannelHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberHumidityGridViewText", sensorNumberHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeHumidityGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeHumidityGridViewText", toSaveAtChangeHumidityGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeTemperatureGridViewText", entryTypeTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                enableChannelTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelTemperatureGridViewText", enableChannelTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                sensorNumberTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationSensorNumberTemperatureGridViewText", sensorNumberTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeTemperatureGridViewText", toSaveAtChangeTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeChassisTemperatureGridViewText", entryTypeChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");

//                enableChannelChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEnableChannelChassisTemperatureGridViewText", enableChannelChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                inclusionThresholdChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInclusionThresholdChassisTemperatureGridViewText", inclusionThresholdChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");
//                toSaveAtChangeChassisTemperatureGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationToSaveAtChangeChassisTemperatureGridViewText", toSaveAtChangeChassisTemperatureGridViewText, htmlFontType, htmlStandardFontSize, "");

//                onThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOnThresholdText", onThresholdText, htmlFontType, htmlStandardFontSize, "");
//                offThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffThresholdText", offThresholdText, htmlFontType, htmlStandardFontSize, "");
//                exceedingThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExceedingThresholdText", exceedingThresholdText, htmlFontType, htmlStandardFontSize, "");
//                diminishingThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationDiminishingThresholdText", diminishingThresholdText, htmlFontType, htmlStandardFontSize, "");
//                warningThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationWarningThresholdText", warningThresholdText, htmlFontType, htmlStandardFontSize, "");
//                alarmThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationAlarmThresholdText", alarmThresholdText, htmlFontType, htmlStandardFontSize, "");
//                ledOnlyThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLedOnlyThresholdText", ledOnlyThresholdText, htmlFontType, htmlStandardFontSize, "");
//                ledAndRelayThresholdText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLedAndRelayThresholdText", ledAndRelayThresholdText, htmlFontType, htmlStandardFontSize, "");
//                entryTypeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationEntryTypeThresholdGridViewText", entryTypeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                thresholdNumberThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationThresholdNumberThresholdGridViewText", thresholdNumberThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                statusThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationStatusThresholdGridViewText", statusThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                modeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationModeThresholdGridViewText", modeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                typeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTypeThresholdGridViewText", typeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                resultThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationResultThresholdGridViewText", resultThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                timeThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTimeThresholdGridViewText", timeThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                levelThresholdGridViewText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationLevelThresholdGridViewText", levelThresholdGridViewText, htmlFontType, htmlStandardFontSize, "");
//                vibrationGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVibrationGridViewEntryText", vibrationGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                pressureGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationPressureGridViewEntryText", pressureGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                currentGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentGridViewEntryText", currentGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                voltageGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationVoltageGridViewEntryText", voltageGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                humidityGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationHumidityGridViewEntryText", humidityGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                temperatureGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTemperatureGridViewEntryText", temperatureGridViewEntryText, htmlFontType, htmlStandardFontSize, "");
//                chassisTemperatureGridViewEntryText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationChassisTemperatureGridViewEntryText", chassisTemperatureGridViewEntryText, htmlFontType, htmlStandardFontSize, "");

//                availableConfigurationsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAvailableConfigurationsRadGroupBoxText", availableConfigurationsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                saveCurrentConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSaveCurrentConfigurationRadButtonText", saveCurrentConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                saveDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSaveDeviceConfigurationRadButtonText", saveDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                loadConfigurationFromDatabaseRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceLoadConfigFromDatabaseRadButtonText", loadConfigurationFromDatabaseRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                programDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceProgramDeviceRadButtonText", programDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                loadConfigurationFromDeviceRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceLoadConfigurationFromDeviceRadButtonText", loadConfigurationFromDeviceRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                deleteConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDeleteConfigurationRadButtonText", deleteConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                configurationViewSelectRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceConfigurationViewSelectRadGroupBoxText", configurationViewSelectRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                fromDeviceRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceFromDeviceRadioButtonText", fromDeviceRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                fromDatabaseRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceFromDatabaseRadioButtonText", fromDatabaseRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                currentConfigurationRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCurrentConfigurationRadioButtonText", currentConfigurationRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                copyDeviceConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCopyDeviceConfigurationRadButtonText", copyDeviceConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");
//                copyDatabaseConfigurationRadButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCopyDatabaseConfigurationRadButtonText", copyDatabaseConfigurationRadButtonText, htmlFontType, htmlStandardFontSize, "");

//                emptyCellErrorMessage = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationEmptyCellErrorMessageText", emptyCellErrorMessage, htmlFontType, htmlStandardFontSize, "");
//                uploadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationUploadingConfigurationText", uploadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
//                downloadingConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationDownloadingConfigurationText", downloadingConfigurationText, htmlFontType, htmlStandardFontSize, "");
//                couldNotFindCurrentConfigurationDisplayingMostRecentText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationCouldNotFindCurrentDeviceConfigurationDisplayingMostRecentText", couldNotFindCurrentConfigurationDisplayingMostRecentText, htmlFontType, htmlStandardFontSize, "");
//                couldNotFindAnyDeviceConfigurationText = LanguageConversion.GetStringAssociatedWithTag("BHMMonitorConfigurationNoDeviceConfigurationsInDatabaseText", couldNotFindAnyDeviceConfigurationText, htmlFontType, htmlStandardFontSize, "");

//                failedToUploadConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationFailedToUploadConfigurationText", failedToUploadConfigurationText, htmlFontType, htmlStandardFontSize, "");
//                errorSendingPasswordToDeviceText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationErrorSendingPasswordToDeviceText", errorSendingPasswordToDeviceText, htmlFontType, htmlStandardFontSize, "");
//                noDatabaseConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoDatabaseConfigurationLoadedText", noDatabaseConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
//                noDeviceConfigurationLoadedText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNoDeviceConfigurationLoadedText", noDeviceConfigurationLoadedText, htmlFontType, htmlStandardFontSize, "");
//                currentConfigurationNotSavedWarningText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentConfigurationNotSavedWarningText", currentConfigurationNotSavedWarningText, htmlFontType, htmlStandardFontSize, "");
//                exitWithoutSavingQuestionText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExitWithoutSavingQuestionText", exitWithoutSavingQuestionText, htmlFontType, htmlStandardFontSize, "");
//                overwriteCurrentConfigurationText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOverwriteCurrentConfigurationText", overwriteCurrentConfigurationText, htmlFontType, htmlStandardFontSize, "");
//                offRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOffRelayText", offRelayText, "", "", "").Trim();
//                onRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationOnRelayText", onRelayText, "", "", "").Trim();
//                externalDevicesRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationExternalDevicesText", externalDevicesRelayText, htmlFontType, htmlStandardFontSize, "");
//                nullDeviceRelayText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationNullDeviceText", nullDeviceRelayText, htmlFontType, htmlStandardFontSize, "");

//                mainMonitorConfigurationTitleText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationTitleText", mainMonitorConfigurationTitleText, htmlFontType, htmlStandardFontSize, "");

//                generalSettingsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabText", generalSettingsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
//                monitoringRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringRadGroupBoxText", monitoringRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                enableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringEnableMonitoringRadRadioButtonText", enableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                disableMonitoringRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabMonitoringDisableMonitoringRadRadioButtonText", disableMonitoringRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                frequencyRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabFrequencyRadGroupBoxText", frequencyRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                hertzRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabHertzRadLabelText", hertzRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                versionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabVersionRadGroupBoxText", versionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                boardNameRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabBoardNameRadGroupBoxText", boardNameRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                connectionParametersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabConnectionParametersRadGroupBoxText", connectionParametersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                modbusAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModbusAddressRadLabelText", modbusAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                serialConnectionRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabSerialConnectionRadGroupBoxText", serialConnectionRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                serialBaudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabSerialBaudRateRadLabelText", serialBaudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                ethernetRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetRadGroupBoxText", ethernetRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                ethernetBaudRateRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetBaudRateRadLabelText", ethernetBaudRateRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                ipAddressRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabIPAddressRadLabelText", ipAddressRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                relayRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabRelayRadGroupBoxText", relayRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                generalSettingsAlarmRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabGeneralSettingsAlarmRadLabelText", generalSettingsAlarmRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                generalSettingsWarningRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabGeneralSettingsWarningRadLabelText", generalSettingsWarningRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                modeSaveRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveRadGroupBoxText", modeSaveRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                stepRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveStepRadLabelText", stepRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                minutesRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabModeSaveMinutesRadLabelText", minutesRadLabelText, htmlFontType, htmlStandardFontSize, "");
//                useDiagnosticResultsRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabUseDiagnosticeResultsRadCheckBoxText", useDiagnosticResultsRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
//                otherOptionsRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabOtherOptionsRadGroupBoxText", otherOptionsRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                enableCurrentSignatureAnalysisRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEnableCurrentSignatureAnalysisRadCheckBoxText", enableCurrentSignatureAnalysisRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
//                allowDirectAccessRadCheckBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabAllowDirectAccessRadCheckBoxText", allowDirectAccessRadCheckBoxText, htmlFontType, htmlStandardFontSize, "");
//                ethernetProtocolRadLabelText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceGeneralSettingsTabEthernetProtocolRadLabelText", ethernetProtocolRadLabelText, htmlFontType, htmlStandardFontSize, "");

//                systemConfigurationRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceSystemConfigurationTabText", systemConfigurationRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");

//                analogInputsRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabText", analogInputsRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
//                controlParametersRadGroupBoxText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabControlParametersRadGroupBoxText", controlParametersRadGroupBoxText, htmlFontType, htmlStandardFontSize, "");
//                vibrationRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabVibrationRadRadioButtonText", vibrationRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                pressureRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabPressureRadRadioButtonText", pressureRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                currentRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabCurrentRadRadioButtonText", currentRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                voltageRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabVoltageRadRadioButtonText", voltageRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                humidityRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabHumidityRadRadioButtonText", humidityRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                temperatureRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabTemperatureRadRadioButtonText", temperatureRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");
//                chassisTemperatureRadRadioButtonText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceAnalogInputsTabChassisTemperatureRadRadioButtonText", chassisTemperatureRadRadioButtonText, htmlFontType, htmlStandardFontSize, "");

//                calibrationRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceCalibrationTabText", calibrationRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");
//                dataTransferRadPageViewPageText = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationInterfaceDataTransferTabText", dataTransferRadPageViewPageText, htmlFontType, htmlStandardFontSize, "");

//                currentConfigName = LanguageConversion.GetStringAssociatedWithTag("MainMonitorConfigurationCurrentDeviceConfigurationText", currentConfigName, "", "", "").Trim();
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AssignValuesToInternalStaticStrings()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private bool ErrorIsPresentInSomeTabObject()
        {
            bool errorIsPresent = false;

            errorIsPresent = ErrorIsPresentInAGeneralSettingsTabObject();
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInASystemConfigurationTabObject();
            }
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInAnAnalogInputsTabObject();
            }
            if (!errorIsPresent)
            {
                errorIsPresent = ErrorIsPresentInADataTransferTabObject();
            }
            return errorIsPresent;
        }

        private void WriteAllInterfaceDataToCurrentConfiguration()
        {
            try
            {
                if (this.currentConfiguration != null)
                {
                    WriteGeneralSettingsTabValuesToCurrentConfiguration();
                    WriteSystemConfigurationGridDataToCurrentConfiguration();
                    WriteAnalogInputDataToCurrentConfiguration();
                    WriteCalibrationTabDataToCurrentConfiguration();
                    WriteDataTransferTabDataToCurrentConfiguration();
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteAllInterfaceDataToCurrentConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private bool AddDataToAllTransientInterfaceObjects(Main_Configuration inputConfiguration)
        {
            bool noRegisterErrorsSeen = true;
            try
            {
                if (inputConfiguration != null)
                {
                    SetGeneralSettingsTabValuesFromConfiguration(inputConfiguration);
                    AddDataToSystemConfigurationGrid(inputConfiguration.fullStatusInformationConfigObjects);
                    // FillSystemConfigurationList();
                    // AddRowsToTheSystemConfigurationGrid();

                    AddDataToTheAnalogInputsGrids(inputConfiguration);
                    AddDataToTheCalibrationGrid(inputConfiguration.calibrationDataConfigObjects);

                    this.systemConfigurationIsCorrect = !ErrorIsPresentInASystemConfigurationTabObject();
                    if (this.systemConfigurationIsCorrect)
                    {
                        GetSystemConfigurationForDataTransfer();
                        if (this.dataTransferRadGridView.RowCount == 0)
                        {
                            AddEmptyRowsToTheDataTransferGrid();
                        }
                        noRegisterErrorsSeen = AddDataToTheDataTransferGrid(inputConfiguration.transferSetupConfigObjects);
                    }
                    else
                    {
                        this.dataTransferRadGridView.Rows.Clear();
                    }

                    //FillAllPrimaryAnalogInputDataLists();
                    // FillAllThresholdDataLists();
                    // AddAllDataToTheAnalogInputsGrids();
                    //FillTransferSetupDataList();

                    this.chassisTemperatureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                    this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nInput Main_Configuration was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return noRegisterErrorsSeen;
        }

//        private void AddDataToAllTransientInterfaceObjects(Main_Configuration inputConfiguration)
//        {
//            try
//            {
//                if (inputConfiguration != null)
//                {
//                    SetGeneralSettingsTabValuesFromConfiguration(inputConfiguration);
//                    AddDataToSystemConfigurationGrid(inputConfiguration.fullStatusInformationConfigObjects);
//                    // FillSystemConfigurationList();
//                    // AddRowsToTheSystemConfigurationGrid();

//                    AddDataToTheAnalogInputsGrids(inputConfiguration);
//                    AddDataToTheCalibrationGrid(inputConfiguration.calibrationDataConfigObjects);

//                    this.systemConfigurationIsCorrect = !ErrorIsPresentInASystemConfigurationTabObject();
//                    if (this.systemConfigurationIsCorrect)
//                    {
//                        GetSystemConfigurationForDataTransfer();
//                        if (this.dataTransferRadGridView.RowCount == 0)
//                        {
//                            AddEmptyRowsToTheDataTransferGrid();
//                        }
//                        AddDataToTheDataTransferGrid(inputConfiguration.transferSetupConfigObjects);
//                    }
//                    else
//                    {
//                        this.dataTransferRadGridView.Rows.Clear();
//                    }

//                    //FillAllPrimaryAnalogInputDataLists();
//                    // FillAllThresholdDataLists();
//                    // AddAllDataToTheAnalogInputsGrids();
//                    //FillTransferSetupDataList();

//                    this.chassisTemperatureRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                    this.vibrationRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
//                }
//                else
//                {
//                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nInput Main_Configuration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToAllTransientInterfaceObjects(Main_Configuration)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }      

        private void programDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                ProgramDevice();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.programDeviceCalibrationCoefficientsTabRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadConfigurationFromDeviceRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                LoadConfigurationFromDeviceAndAssignItToConfigObject();
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.loadConfigurationFromDeviceRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
     
//        private bool UpdateConfigurationWithCurrentInterfaceValues()
//        {
//            bool errorIsPresent = false;
//            try
//            {
//                //Int16[] registerData;
//                //registerData = currentConfiguration.GetShortValuesFromAllContributors();
//                //currentConfiguration = null;
//                //currentConfiguration = new Main_Configuration(registerData);
//                errorIsPresent = ErrorIsPresentInSomeTabObject();
//                if(!errorIsPresent)
//                {
//                    WriteAllInterfaceDataToCurrentConfiguration();
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateConfigurationWithCurrentInterfaceValues()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//            return errorIsPresent;
//        }

//        private void WriteUserInterfaceViewedConfigurationToCurrentConfiguration()
//        {
//            try
//            {
//                this.configurationError = false;
//                if (this.currentConfiguration != null)
//                {
//                    if (this.currentConfiguration.fullStatusInformationConfigObjects != null)
//                    {
//                        if (this.currentConfiguration.inputChannelConfigObjects != null)
//                        {
//                            if (this.currentConfiguration.inputChannelThresholdConfigObjects != null)
//                            {
//                                if (this.currentConfiguration.transferSetupConfigObjects != null)
//                                {
//                                    TranslateGeneralSettingsTabValuesToConfigurationValues(ref currentConfiguration);

//                                    if (!this.configurationError)
//                                    {
//                                        GetSystemConfigurationDataFromGrid(ref currentConfiguration.fullStatusInformationConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        CopyFullStatusInformationToShortStatusInformation(ref currentConfiguration);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetAllInputChannelDataFromGrids(ref currentConfiguration.inputChannelConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetAllThresholdDataFromGrids(ref currentConfiguration.inputChannelThresholdConfigObjects);
//                                    }

//                                    if (!this.configurationError)
//                                    {
//                                        GetDataTransferDataFromGrid(ref currentConfiguration.transferSetupConfigObjects);
//                                    }
//                                }
//                                else
//                                {
//                                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.currentConfiguration.transferSetupConfigObjects was null.";
//                                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                                    MessageBox.Show(errorMessage);
//#endif
//                                }
//                            }
//                            else
//                            {
//                                string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.currentConfiguration.inputChannelThresholdConfigObjects was null.";
//                                LogMessage.LogError(errorMessage);
//#if DEBUG
//                                MessageBox.Show(errorMessage);
//#endif
//                            }
//                        }
//                        else
//                        {
//                            string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.currentConfiguration.inputChannelConfigObjects was null.";
//                            LogMessage.LogError(errorMessage);
//#if DEBUG
//                            MessageBox.Show(errorMessage);
//#endif
//                        }
//                    }
//                    else
//                    {
//                        string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.currentConfiguration.fullStatusInformationConfigObjects was null.";
//                        LogMessage.LogError(errorMessage);
//#if DEBUG
//                        MessageBox.Show(errorMessage);
//#endif
//                    }

//                }
//                else
//                {
//                    string errorMessage = "Error in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nthis.currentConfiguration was null.";
//                    LogMessage.LogError(errorMessage);
//#if DEBUG
//                    MessageBox.Show(errorMessage);
//#endif
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.WriteUserInterfaceViewedConfigurationToCurrentConfiguration()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

       

        /// <summary>
        /// Testing program that shows some specific register values, only for debugging
        /// </summary>
        /// <param name="latestRegisterData"></param>
        private void ShowSomeValuesThatArePissingMeOff(Int16[] latestRegisterData)
        {
            int offset = 300;

            StringBuilder entry = new StringBuilder();

            int tempIndex;

            for (int i = 0; i < 25; i++)
            {
                for (int j = 14; j < 17; j++)
                {
                    tempIndex = offset + j;
                    entry.Append("Register " + tempIndex.ToString() + ":  " + latestRegisterData[tempIndex].ToString());
                    entry.Append("\n");
                }
                offset += 200;
                entry.Append("\n");
            }

            MessageBox.Show(entry.ToString());
        }

        /// <summary>
        /// Compares two configurations using their register data, only for debugging
        /// </summary>
        /// <param name="latestRegisterData"></param>
//        private void CheckLatestConfigurationAgainstPreviousConfiguration(Int16[] latestRegisterData)
//        {
//            try
//            {
//                List<String> badValues = new List<string>();
//                // Int16[] previousRegisterData = currentConfiguration.GetShortValuesFromAllContributors();
//                string entry;

//                if ((this.previousConfigurationAsArray != null) && (this.currentConfigurationAsArray != null))
//                {
//                    for (int i = 0; i < 6000; i++)
//                    {
//                        if (previousConfigurationAsArray[i] != currentConfigurationAsArray[i])
//                        {
//                            entry = "Register " + i.ToString() + ": Old Value = " + previousConfigurationAsArray[i].ToString() + "   New Value = " + currentConfigurationAsArray[i].ToString();
//                            badValues.Add(entry);
//                        }
//                    }

//                    if (badValues.Count > 0)
//                    {
//                        StringBuilder allerrors = new StringBuilder();
//                        allerrors.Append("Found mismatches between old and new register values\n\n");
//                        foreach (string error in badValues)
//                        {
//                            allerrors.Append(error);
//                            allerrors.Append("\n");
//                        }
//                        MessageBox.Show(allerrors.ToString());
//                    }
//                    else
//                    {
//                        MessageBox.Show("New values loaded equal to old values");
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in BHM_MonitorConfiguration.CheckLatestConfigurationAgainstPreviousConfiguration(Int16[])\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        public Int16[] Main_GetDeviceSetup(int modBusAddress)
        {
            Int16[] returnedRegisterValues = null;
            try
            {
                Int16[] allRegisterValues = null;
                Int16[] currentRegisterValues = null;
                bool readIsIncorrect = true;
                int offset = 0;
                int registersReturned = 0;
                int registersPerDataRequest = 100;
                bool receivedAllData = false;
                int chunkCount = 0;
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                allRegisterValues = new Int16[6100];

                DisableAllControls();
                SetProgressBarsToDownloadState();
                EnableProgressBars();
                SetProgressBarProgress(0, 60);
                while (!receivedAllData)
                {
                    readIsIncorrect = true;
                    while (readIsIncorrect && !receivedAllData)
                    {
                        currentRegisterValues = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, offset, registersPerDataRequest, 2, 1, 20,parentWindowInformation);
                        if (currentRegisterValues != null)
                        {
                            registersReturned = currentRegisterValues.Length;
                            Array.Copy(currentRegisterValues, 0, allRegisterValues, offset, registersReturned);
                            offset += registersReturned;
                            readIsIncorrect = false;
                            if (offset >= 6000)
                            {
                                receivedAllData = true;
                            }
                            chunkCount++;
                        }
                        else
                        {
                            //RadMessageBox.Show(this, "Failed to download monitor configuration data.\nThe error log may have some clue as to why it failed.");
                            receivedAllData = true;
                            allRegisterValues = null;
                        }
                        Application.DoEvents();
                    }
                    SetProgressBarProgress(chunkCount, 60);
                    // UpdateDownloadChunkCountDisplay(chunkCount);
                    Application.DoEvents();
                }

                if (allRegisterValues != null)
                {
                    returnedRegisterValues = new Int16[offset];
                    Array.Copy(allRegisterValues, 0, returnedRegisterValues, 0, offset);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_GetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return returnedRegisterValues;
        }


        public bool Main_SetDeviceSetup(int modBusAddress)
        {
            bool success = false;
            try
            {
                string connectionType = string.Empty;
                string ipAddress = string.Empty;
                string modBusAddressAsString = string.Empty;
                Int16[] allRegisterValues = null;
                // Int16[] currentRegisterValues = null;
                //int registersPerDataSend = 100;
                List<int> errorCodes = new List<int>();

                string downloadErrorMessage = string.Empty;
                // ErrorCode error = ErrorCode.None;

                allRegisterValues = this.currentConfiguration.GetShortValuesFromAllContributors();
                // currentRegisterValues = new Int16[registersPerDataSend];

                // write the password so we can write the rest of the values

                // DeviceCommunication.Main_SetDeviceSetup(modBusAddress, 

                if (WritePasswordToDeviceAndCheckIfTheWriteWorked(modBusAddress))
                {
                    DisableAllControls();
                    SetProgressBarsToUploadState();
                    SetProgressBarProgress(0, 1000);
                    EnableProgressBars();

                    //Int16[] value = DeviceCommunication.ReadMultipleRegisters(modBusAddress, 121, 1, 2, ref downloadErrorMessage);
                    //if (value != null)
                    //{
                    //if (value[0] == 1)
                    //{
                    //success = WriteShortStatusInformationToDevice(modBusAddress, allRegisterValues);

                    success = WriteBoardNameToDevice(modBusAddress, allRegisterValues);

                    Application.DoEvents();

                    if (success)
                    {
                        success = WriteInputChannelDataToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }

                    if (success)
                    {
                        success = WriteTransferSetupToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }

                    if (success)
                    {
                        success = WriteFullStatusInformationToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteCalibrationDataToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteSetupAndCommunicationInformationToDevice(modBusAddress, allRegisterValues);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteUnknownValuesAsZeroesToDevice(modBusAddress);
                        Application.DoEvents();
                    }
                    if (success)
                    {
                        success = WriteConfigurationFinalCommandToDevice(modBusAddress);
                        Application.DoEvents();
                    }
                    //if (success)
                    //{
                    //    success = WriteCalibrationDataToDevice(modBusAddress, allRegisterValues);
                    //    Application.DoEvents();
                    //}
                    //if (success)
                    //{
                    //    success = WriteTransferSetupToDevice(modBusAddress, allRegisterValues);
                    //    Application.DoEvents();
                    //}
                    //if (!success)
                    //{
                    //    RadMessageBox.Show(this, failedToUploadConfigurationText);
                    //}
                    //// reset the configuration to read only
                    //DeviceCommunication.WriteSingleRegister(modBusAddress, 121, 0, 2);

                    //}
                    //else
                    //{
                    //    RadMessageBox.Show(this, "Error in password sent to device.\nPlease check the connection.");
                    //}
                    //}
                    //else
                    //{
                    //    if (downloadErrorMessage.CompareTo(DeviceCommunication.DownloadFailedMessage) == 0)
                    //    {
                    //        error = ErrorCode.DownloadFailed;
                    //    }
                    //    else
                    //    {
                    //        error = ErrorCode.DownloadCancelled;
                    //    }
                    //}
                }
                else
                {
                    RadMessageBox.Show(this, errorSendingPasswordToDeviceText);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.Main_SetDeviceSetup(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            finally
            {
                EnableAllControls();
                DisableProgressBars();
            }
            return success;
        }

        private bool WritePasswordToDeviceAndCheckIfTheWriteWorked(int modBusAddress)
        {
            bool success = false;
            try
            {
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                Int16[] password = new Int16[1];
                Int16[] responseMessage = null;
                password[0] = (Int16)5421;

                if (InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, password, 2, 1, 20, parentWindowInformation))
                {
                    responseMessage = InteractiveDeviceCommunication.Main_GetDeviceSetup(modBusAddress, 120, 1, 2, 1, 20, parentWindowInformation);
                }
                if (responseMessage != null)
                {
                    if (responseMessage[0] == 1)
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteConfigurationFinalCommandToDevice(int modBusAddress)
        {
            bool success = false;
            try
            {
                Int16[] outputValue = new Int16[1];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                outputValue[0] = 0;

                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 120, outputValue, 2,1, 20,parentWindowInformation);            
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }


        private bool WriteUnknownValuesAsZeroesToDevice(int modBusAddress) // cfk code change 12/21/13  this is overwriting some whs stuff
        {
          /*  bool success = false;
            try
            {
                Int16[] outputValues = new Int16[48];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);

                for (int i = 0; i < 48; i++)
                {
                    outputValues[i] = 0;
                }

                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 5500, outputValues, 2,1, 20, parentWindowInformation);
                SetProgressBarProgress(20 + 2550 + 288 + 120 + 24 + 48, 3050);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WritePasswordToDeviceAndCheckIfTheWriteWorked(int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }*/
            SetProgressBarProgress(20 + 2550 + 288 + 120 + 24 + 48, 3050);
            return true;
        }

        private bool WriteBoardNameToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[20];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 40)
                        {
                            Array.Copy(allRegisters, 20, registersToWrite, 0, 20);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 20, registersToWrite, 2,1, 20,parentWindowInformation);
                            SetProgressBarProgress(20, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteBoardNameToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteShortStatusInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[45];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 95)
                        {
                            Array.Copy(allRegisters, 50, registersToWrite, 0, 45);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 50, registersToWrite, 2,1, 20,parentWindowInformation);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteShortStatusInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteSetupAndCommunicationInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[24];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 138)
                        {
                            Array.Copy(allRegisters, 121, registersToWrite, 0, 24);
                            /// value found in setup write from IHM using usb sniffer, don't know where it comes from
                            registersToWrite[21] = 10;
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 121, registersToWrite, 2,1, 20,parentWindowInformation);
                            SetProgressBarProgress(20 + 2550 + 288 + 120 + 24, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteSetupAndCommunicationInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteFullStatusInformationToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[120];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 270)
                        {
                            Array.Copy(allRegisters, 150, registersToWrite, 0, 120);
                            success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, 150, registersToWrite, 2,1, 20,parentWindowInformation);
                            SetProgressBarProgress(20 + 2550 + 288 + 120, 3050);
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteFullStatusInformationToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteInputChannelDataToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[102];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                bool uploading = true;
                int count = 0;
                int offset = 300;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5300)
                        {
                            while (uploading)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 102);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, 2,1, 20,parentWindowInformation);
                                offset += 200;
                                count++;
                                SetProgressBarProgress(20 + (count * 102), 3050);
                                if (!success || (count == 25))
                                {
                                    uploading = false;
                                }
                                Application.DoEvents();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteInputChannelDataToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteCalibrationDataToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[6];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                int offset = 5300;
                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5450)
                        {
                            for (int i = 0; i < 25; i++)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 6);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, 2,1, 20,parentWindowInformation);
                                if (!success)
                                {
                                    break;
                                }
                                Application.DoEvents();
                                offset += 6;
                            }                            
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteCalibrationDataToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

        private bool WriteTransferSetupToDevice(int modBusAddress, Int16[] allRegisters)
        {
            bool success = false;
            try
            {
                Int16[] registersToWrite = new Int16[9];
                ParentWindowInformation parentWindowInformation = new ParentWindowInformation(this.Location, this.Size);
                bool uploading = true;
                int count = 0;
                int offset = 5600;

                if ((modBusAddress > 0) && (modBusAddress < 256))
                {
                    if (allRegisters != null)
                    {
                        if (allRegisters.Length > 5888)
                        {
                            while (uploading)
                            {
                                Array.Copy(allRegisters, offset, registersToWrite, 0, 9);
                                success = InteractiveDeviceCommunication.Main_SetDeviceSetup(modBusAddress, offset, registersToWrite, 2, 1, 20, parentWindowInformation);
                                offset += 9;
                                count++;
                                SetProgressBarProgress(20 + 2550 + (count * 9), 3050);
                                if (!success || (count == 32))
                                {
                                    uploading = false;
                                }
                                Application.DoEvents();
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput Int16[] had too few elements.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput Int16[] was null.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nInput int needs to be between 1 and 255 inclusive.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteTransferSetupToDevice(int, Int16[])\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
            return success;
        }

//        private void UpdateDownloadChunkCountDisplay(int chunkCount)
//        {
//            try
//            {
//                string text = "Downloading Chunk " + chunkCount.ToString() + " of 60";

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateDownloadChunkCountDisplay(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void UpdateUploadChunkCountDisplay(int chunkCount)
//        {
//            try
//            {
//                string text = "Uploading Chunk " + chunkCount.ToString() + " of 60";

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.UpdateUploadChunkCountDisplay(int)\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

//        private void ClearChunkCountDisplay()
//        {
//            try
//            {
//                string text = string.Empty;

//                generalSettingsChunkRadLabel.Text = text;
//                systemConfigurationChunkRadLabel.Text = text;
//                analogInputsChunkRadLabel.Text = text;
//                dataTransferChunkRadLabel.Text = text;
//            }
//            catch (Exception ex)
//            {
//                string errorMessage = "Exception thrown in Main_MonitorConfiguration.ClearChunkCountDisplay()\nMessage: " + ex.Message;
//                LogMessage.LogError(errorMessage);
//#if DEBUG
//                MessageBox.Show(errorMessage);
//#endif
//            }
//        }

        private void configurationRadPageView_SelectedPageChanged(object sender, EventArgs e)
        {
            try
            {               
                if (configurationRadPageView.SelectedPage == dataTransferRadPageViewPage)
                {
                    //if (!ErrorIsPresentInASystemConfigurationTabObject())
                    //{
                    GetSystemConfigurationForDataTransfer();
                    UpdateDataTransferPageSlaveMonitorInformation();
                    UpdateDataTransferPageMainRegisterNameDropDownListEntries();
                    UpdateDataTransferPageRadGridViewMainRegisterNames();
                    if (this.dataTransferRadGridView.Rows.Count == 0)
                    {
                        AddEmptyRowsToTheDataTransferGrid();
                    }
                    if (this.currentConfiguration != null)
                    {
                        AddDataToTheDataTransferGrid(this.currentConfiguration.transferSetupConfigObjects);
                    }

                    //}
                    //else
                    //{
                    //    RadMessageBox.Show(this, "The system configuration as defined in the \"System Configuration\" tab is incorrect.\nThe transfer setup will not be displayed until it is corrected.");
                    //    this.dataTransferRadGridView.Rows.Clear();
                    //}
                }
                //else
                //{
                //    /// save the current information in the Data Transfer tab if we could have been editing it.
                //    if (this.currentGeneralSettingsTabRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                //    {
                //        if (this.currentConfiguration != null)
                //        {

                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.configurationRadPageView_SelectedPageChanged(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void configurationRadPageView_SelectedPageChanging(object sender, RadPageViewCancelEventArgs e)
        {
            try
            {
                //bool cancel = false;
                //if (!CheckSystemConfigurationForCorrectness())
                //{
                //    cancel = true;
                //}
                //else if (!CheckDataTransferRadGridViewElementsForOverallCorrectness())
                //{
                //    //cancel = true;
                //}
                //if (cancel)
                //{
                //    e.Cancel = true;
                //}
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.configurationRadPageView_SelectedPageChanging(object, RadPageViewCancelEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void Main_MonitorConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.currentConfiguration != null)
            {
                WriteAllInterfaceDataToCurrentConfiguration();
                if (!this.currentConfiguration.ConfigurationIsTheSame(this.uneditedCurrentConfiguration))
                {
                    if (RadMessageBox.Show(this, currentConfigurationNotSavedWarningText, exitWithoutSavingQuestionText, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void DisableAllControls()
        {
            programDeviceGeneralSettingsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceGeneralSettingsTabRadButton.Enabled = false;
           
            programDeviceSystemConfigurationTabRadButton.Enabled = false;
            loadConfigurationFromDeviceSystemConfigurationTabRadButton.Enabled = false;

            programDeviceAnalogInputsTabRadButton.Enabled = false;
            loadConfigurationFromDeviceAnalogInputsTabRadButton.Enabled = false;

            programDeviceCalibrationTabRadButton.Enabled = false;
            loadConfigurationFromDeviceCalibrationTabRadButton.Enabled = false;

            programDeviceDataTransferTabRadButton.Enabled = false;
            loadConfigurationFromDeviceDataTransferTabRadButton.Enabled = false;

            DisableGeneralSettingsTabConfigurationEditObjects();
            DisableSystemConfigurationTabConfigurationEditObjects();
            DisableAllAnalogInputsTabConfigurationEditObjects();
            DisableCalibrationTabConfigurationEditObjects();
            DisableDataTransferTabConfigurationEditObjects(); 
        }

        private void EnableAllControls()
        {
            programDeviceGeneralSettingsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceGeneralSettingsTabRadButton.Enabled = true;

            programDeviceSystemConfigurationTabRadButton.Enabled = true;
            loadConfigurationFromDeviceSystemConfigurationTabRadButton.Enabled = true;

            programDeviceAnalogInputsTabRadButton.Enabled = true;
            loadConfigurationFromDeviceAnalogInputsTabRadButton.Enabled = true;

            programDeviceCalibrationTabRadButton.Enabled = true;
            loadConfigurationFromDeviceCalibrationTabRadButton.Enabled = true;

            programDeviceDataTransferTabRadButton.Enabled = true;
            loadConfigurationFromDeviceDataTransferTabRadButton.Enabled = true;

            EnableGeneralSettingsTabConfigurationEditObjects();
            EnableSystemConfigurationTabConfigurationEditObjects();
            EnableAllAnalogInputsTabConfigurationEditObjects();
            EnableCalibrationTabConfigurationEditObjects();
            EnableDataTransferTabConfigurationEditObjects(); 
        }

        private void EnableProgressBars()
        {
            generalSettingsRadProgressBar.Visible = true;
            systemConfigurationRadProgressBar.Visible = true;
            analogInputsRadProgressBar.Visible = true;
            calibrationRadProgressBar.Visible = true;
            dataTransferRadProgressBar.Visible = true;
        }

        private void DisableProgressBars()
        {
            generalSettingsRadProgressBar.Visible = false;
            systemConfigurationRadProgressBar.Visible = false;
            analogInputsRadProgressBar.Visible = false;
            calibrationRadProgressBar.Visible = false;
            dataTransferRadProgressBar.Visible = false;
        }

        private void SetProgressBarsToUploadState()
        {
            generalSettingsRadProgressBar.Text = uploadingConfigurationText;
            systemConfigurationRadProgressBar.Text = uploadingConfigurationText;
            analogInputsRadProgressBar.Text = uploadingConfigurationText;
            calibrationRadProgressBar.Text = uploadingConfigurationText;
            dataTransferRadProgressBar.Text = uploadingConfigurationText;
        }

        private void SetProgressBarsToDownloadState()
        {
            generalSettingsRadProgressBar.Text = downloadingConfigurationText;
            systemConfigurationRadProgressBar.Text = downloadingConfigurationText;
            analogInputsRadProgressBar.Text = downloadingConfigurationText;
            calibrationRadProgressBar.Text = downloadingConfigurationText;
            dataTransferRadProgressBar.Text = downloadingConfigurationText;
        }

        private void SetProgressBarProgress(int currentValue, int maxValue)
        {
            try
            {
                int currentProgress;

                if ((currentValue >= 0) && (maxValue > 0))
                {
                    if (currentValue > maxValue)
                    {
                        currentValue = maxValue;
                    }
                    currentProgress = (currentValue * 100) / maxValue;
                    generalSettingsRadProgressBar.Value1 = currentProgress;
                    systemConfigurationRadProgressBar.Value1 = currentProgress;
                    analogInputsRadProgressBar.Value1 = currentProgress;
                    calibrationRadProgressBar.Value1 = currentProgress;
                    dataTransferRadProgressBar.Value1 = currentProgress;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nInput values were incorrect.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetProgressBarProgress(int, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void vibrationRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (vibrationRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Vibration);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.vibrationRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void analogInRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (analogInRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Pressure);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.analogInRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void currentRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (currentRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Current);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.currentRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void voltageRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (voltageRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Voltage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.voltageRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void humidityRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (humidityRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Humidity);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.humidityRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void temperatureRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (temperatureRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.Temperature);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.temperatureRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void chassisTemperatureRadRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            try
            {
                if (chassisTemperatureRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                {
                    SetActiveAnalogInputsRadGridView(ParameterType.ChassisTemperature);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.chassisTemperatureRadRadioButton_ToggleStateChanged(object, StateChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }


        private void SetTemplateConfigurationsSelectedIndex()
        {
            try
            {
                if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                {
                    string description = this.templateConfigurations[this.templateConfigurationsSelectedIndex].Description;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsGeneralSettingsTabRadDropDownList.Text = description;
                    templateConfigurationsSystemConfigurationTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsSystemConfigurationTabRadDropDownList.Text = description;
                    templateConfigurationsAnalogInputsTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsAnalogInputsTabRadDropDownList.Text = description;
                    templateConfigurationsCalibrationTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsCalibrationTabRadDropDownList.Text = description;
                    templateConfigurationsDataTransferTabRadDropDownList.SelectedIndex = this.templateConfigurationsSelectedIndex;
                    templateConfigurationsDataTransferTabRadDropDownList.Text = description;
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.SetTemplateConfigurationsSelectedIndex()\nSelected index is out of range";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.SetAvailableConfigurationsSelectedIndex()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsGeneralSettingsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsSystemConfigurationTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsSystemConfigurationTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsSystemConfigurationTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsAnalogInputsTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsAnalogInputsTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsAnalogInputsTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsCalibrationTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsCalibrationTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsCalibrationTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void templateConfigurationsDataTransferTabRadDropDownList_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            try
            {
                int selectedIndex = templateConfigurationsDataTransferTabRadDropDownList.SelectedIndex;
                if (selectedIndex != -1)
                {
                    if (selectedIndex != this.templateConfigurationsSelectedIndex)
                    {
                        this.templateConfigurationsSelectedIndex = selectedIndex;
                        SetTemplateConfigurationsSelectedIndex();
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.templateConfigurationsDataTransferTabRadDropDownList_SelectedIndexChanged(object, Telerik.WinControls.UI.Data.PositionChangedEventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadSelectedTemplateConfigurationRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                Main_Config_ConfigurationRoot templateConfigurationRoot;
                Main_Configuration templateConfiguration;

                if (this.templateConfigurations != null)
                {
                    if (this.templateConfigurations.Count > 0)
                    {
                        if ((this.templateConfigurationsSelectedIndex > -1) && (this.templateConfigurationsSelectedIndex < this.templateConfigurations.Count))
                        {
                            templateConfigurationRoot = this.templateConfigurations[this.templateConfigurationsSelectedIndex];
                            if (templateConfigurationRoot != null)
                            {
                                using (MonitorInterfaceDB templateDB = new MonitorInterfaceDB(this.templateDbConnectionString))
                                {
                                    templateConfiguration = ConfigurationConversion.GetMain_ConfigurationFromDatabase(templateConfigurationRoot.ID, templateDB);
                                    if (templateConfiguration.AllConfigurationMembersAreNonNull())
                                    {
                                        this.currentConfiguration = templateConfiguration;
                                        this.uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(this.currentConfiguration);
                                        AddDataToAllTransientInterfaceObjects(this.currentConfiguration);
                                        RadMessageBox.Show("Loaded template configuration");
                                    }
                                    else
                                    {
                                        RadMessageBox.Show("Failed to load the template configuration");
                                    }
                                }
                            }
                            else
                            {
                                RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationFailedToLoadFromDatabase));
                            }
                        }
                        else
                        {
                            RadMessageBox.Show(this, ErrorCodeDisplay.GetErrorCodeErrorMessage(ErrorCode.ConfigurationNotSelected));
                        }
                    }
                    else
                    {
                        RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    }
                }
                else
                {
                    RadMessageBox.Show(this, noTemplateConfigurationsAvailable);
                    string errorMessage = "Error in PDM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nthis.templateConfigurations was null";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in PDM_MonitorConfiguration.loadSelectedTemplateConfigurationRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void LoadTemplateConfigurations(MonitorInterfaceDB templateDB)
        {
            try
            {
                //List<PDM_Config_ConfigurationRoot> templateConfigurations;
                int templateCount;
                string[] templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                string[] templateConfigurationsSystemConfigurationTabRadDropDownListDataSource;
                string[] templateConfigurationsAnalogInputsTabRadDropDownListDataSource;
                string[] templateConfigurationsCalibrationTabRadDropDownListDataSource;
                string[] templateConfigurationsDataTransferTabRadDropDownListDataSource;

                string description;

                this.templateConfigurations = MainMonitor_DatabaseMethods.Main_Config_ReadAllConfigurationRootTableEntriesInTheDatabase(templateDB);

                if ((this.templateConfigurations != null) && (this.templateConfigurations.Count > 0))
                {
                    templateCount = this.templateConfigurations.Count;

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsCalibrationTabRadDropDownListDataSource = new string[templateCount];
                    templateConfigurationsDataTransferTabRadDropDownListDataSource = new string[templateCount];

                    for (int i = 0; i < templateCount; i++)
                    {
                        description = this.templateConfigurations[i].Description.Trim();

                        templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsSystemConfigurationTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsAnalogInputsTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsCalibrationTabRadDropDownListDataSource[i] = description;
                        templateConfigurationsDataTransferTabRadDropDownListDataSource[i] = description;
                    }
                }
                else
                {
                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource = new string[1];
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource = new string[1]; ;
                    templateConfigurationsCalibrationTabRadDropDownListDataSource = new string[1]; ;
                    templateConfigurationsDataTransferTabRadDropDownListDataSource = new string[1];

                    templateConfigurationsGeneralSettingsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsSystemConfigurationTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsAnalogInputsTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsCalibrationTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                    templateConfigurationsDataTransferTabRadDropDownListDataSource[0] = noTemplateConfigurationsAvailable;
                }

                templateConfigurationsGeneralSettingsTabRadDropDownList.DataSource = templateConfigurationsGeneralSettingsTabRadDropDownListDataSource;
                templateConfigurationsSystemConfigurationTabRadDropDownList.DataSource = templateConfigurationsSystemConfigurationTabRadDropDownListDataSource;
                templateConfigurationsAnalogInputsTabRadDropDownList.DataSource = templateConfigurationsAnalogInputsTabRadDropDownListDataSource;
                templateConfigurationsCalibrationTabRadDropDownList.DataSource = templateConfigurationsCalibrationTabRadDropDownListDataSource;
                templateConfigurationsDataTransferTabRadDropDownList.DataSource = templateConfigurationsDataTransferTabRadDropDownListDataSource;

                templateConfigurationsGeneralSettingsTabRadDropDownList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.LoadAvailableConfigurations()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void loadFromFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string sourceFileName;
                XmlSerializer serializer;
                bool loadFailed = false;

                sourceFileName = FileUtilities.GetFileNameWithFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml");
                if ((sourceFileName != null) && (sourceFileName != string.Empty))
                {
                    this.currentConfiguration = null;
                    serializer = new XmlSerializer(typeof(Main_Configuration));
                    try
                    {
                        using(Stream s = File.OpenRead(sourceFileName))
                        {
                            this.currentConfiguration = (Main_Configuration)serializer.Deserialize(s);
                            this.uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(this.currentConfiguration);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to deserialize configuration from file, exception message was: " + ex.Message);
                        this.currentConfiguration = new Main_Configuration();
                        this.currentConfiguration.InitializeDataToZeroes();
                        this.uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllTransientInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if ((!loadFailed) && (!this.currentConfiguration.AllConfigurationMembersAreNonNull()))
                    {
                        MessageBox.Show("Configuration loaded from file was incomplete or contained errors");
                        this.currentConfiguration = new Main_Configuration();
                        this.currentConfiguration.InitializeDataToZeroes();
                        this.uneditedCurrentConfiguration = Main_Configuration.CopyConfiguration(this.currentConfiguration);
                        AddDataToAllTransientInterfaceObjects(this.currentConfiguration);
                        loadFailed = true;
                    }
                    if (!loadFailed)
                    {
                        if (!AddDataToAllTransientInterfaceObjects(this.currentConfiguration))
                        {
                            RadMessageBox.Show(this, dataTransferContainsDepricatedDestinationRegister);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.loadFromFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void saveToFileRadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string destinationFileName;
                XmlSerializer serializer;

                LoadConfigurationFromDeviceAndAssignItToConfigObject();
                if ((this.currentConfiguration != null) && (this.currentConfiguration.AllConfigurationMembersAreNonNull()))
                {
                    destinationFileName = FileUtilities.GetSaveFileNameWithFullPath("MainMonitorConfiguration", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "xml", true, false);
                    if ((destinationFileName != null) && (destinationFileName != string.Empty))
                    {
                        serializer = new XmlSerializer(typeof(Main_Configuration));
                        using (Stream s = File.Create(destinationFileName))
                        {
                            serializer.Serialize(s, this.currentConfiguration);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.saveToFileRadButton_Click(object, EventArgs)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void analogInputsRadPageViewPage_Paint(object sender, PaintEventArgs e)
        {

        }
    }

    public class TransferSetupData
    {
        public int transferSetupNumber;
        public bool enabled;
        public int readFrequency;
        public int readPoints;
        public int averages;
        public double saveData;
        // transfer setup number : enable measurement : master dev register : slave dev name : slave dev modbus : slave dev register : slope
    }


    public class MainConfigUISystemConfigurationData
    {
        public int moduleNumber;
        public string deviceType;
        public int modBusAddress;
        public int baudRate;
        public int dataTransferInterval;
    }

    public class InputChannelData
    {
        public int inputChannelNumber;
        public int readStatus;
        public int sensorType;
        public int unit;
        public int sensorID;
        public int sensorNumber;
        public double sensitivity;
        public int highPassFilter;
        public int lowPassFilter;
        public int masterDeviceModBusAddress;
        public int masterDeviceRegister;
        public int slaveDeviceModBusAddress;
        public int slaveDeviceRegister;
        public double slope;
        public int reserved;
        public int working;
    }

    public enum ConfigurationDisplayed
    {
        FromDatabase,
        FromDevice,
        Current, 
        None
    }
}
