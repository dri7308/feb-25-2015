﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using DatabaseInterface;

namespace MainConfigurationLite
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    { 
        private static string firmwareVersionUnknownText = "unknown";

        private static string temperatureRiseOverTopOilText = "<html>Winding Rise<br>Over Top Oil</html>";
        private static string maximumRatedCurrentText = "<html>Maximum Rated<br>Current<br>(Amps)</html>";
        private static string windingText = "Winding";
        private static string highVoltageText = "Winding 1";
        private static string highVoltagePhaseAText = "Winding 1";
        private static string highVoltagePhaseBText = "Winding 2";
        private static string highVoltagePhaseCText = "Winding 3";
        private static string lowVoltageText = "Winding 2";
        private static string lowVoltagePhaseAText = "Low Voltage - A";
        private static string lowVoltagePhaseBText = "Low Voltage - B";
        private static string lowVoltagePhaseCText = "Low Voltage - C";
        private static string tertiaryVoltageText = "Winding 3";
        private static string tertiaryVoltagePhaseAText = "Tertiary Voltage - A";
        private static string tertiaryVoltagePhaseBText = "Tertiary Voltage - B";
        private static string tertiaryVoltagePhaseCText = "Tertiary Voltage - C";
        private static string commonText = "Common";
        private static string commonPhaseAText = "Common - A";
        private static string commonPhaseBText = "Common - B";
        private static string commonPhaseCText = "Common - C";
        private static string topOilTempInputNoneText = "None";
        private static string topOilTempInputRTDOneText = "RTD 01";
        private static string topOilTempInputRTDTwoText = "RTD 02";
        private static string topOilTempInputRTDThreeText = "RTD 03";
        private static string topOilTempInputRTDFourText = "RTD 04";
        private static string topOilTempInputRTDFiveText = "RTD 05";
        private static string topOilTempInputRTDSixText = "RTD 06";
        private static string topOilTempInputRTDSevenText = "RTD 07";
        private static string agingCalculationMethodIECText = "IEC";
        private static string agingCalculationMethodIEEESixtyFiveText = "IEEE 65";
        private static string agingCalculationMethodIEEEFiftyFiveText = "IEEE 55";
        private static string agingCalculationMethodNomexText = "Nomex";
        private static string windingTimeConstantHeaderText = "Time Constant";
        private static string windingExponentHeaderText = "Exponent";
        private static string windingPreviousAgingHeaderText = "Previous Aging (Days)";
        private static string inputText = "Current Input";
        private static string topOilGroupBoxHeader = "Top Oil Temperature Input";
        private static string agingMethodGroupBoxHeaderText = "Aging Calculation Method";

        private static string currentInputNoneText = "None";
        private static string currentInputIAText = "I-A";
        private static string currentInputIBText = "I-B";
        private static string currentInputICText = "I-C";
        private static string currentInputIOneText = "I-1";
        private static string currentInputITwoText = "I-2";
        private static string currentInputIThreeText = "I-3";
        private static string hotSpotFactorLabelTextLabel = "Hot Spot Factor";
        private static string numberOfCoolingGroupsLabelText = "Groups Enabled";
        private static string coolingGroupsZero = "None";
        private static string coolingGroupsOne = "1";
        private static string coolingGroupsTwo = "2";
        private static string coolingGroupsThree = " 1 and 2";
        private static string coolingCurrentAlarmGroupBoxText = "Per Unit Current Alarm Set Points";
        private static string lowCoolingAlarmLabelText = "Low Current";
        private static string highCoolingAlarmLabelText = "High Current";
       
        private static string transformerConfigurationThreeSinglePhaseText = "3 Single Phase";
        private static string transformerConfigurationSingleThreePhaseText = "1 Three Phase";

        private static string transformerTypeStandardText = "Standard";
        private static string transformerTypeAutoText = "Auto";

        private static string maximumRatedCurrentForHighVoltageOutOfRangeText = "The maximum rated current for the high voltage windings must be between 1 and 5000 Amps";

        private int tempRiseSettingsRadGroupBoxNormalHeight = 300;
        private int tempRiseSettingsRadGroupBoxShorterHeight = 156;
        private int maximumRatedCurrentRadGroupBoxNormalHeight = 300;
        private int maximumRatedCurrentRadGroupBoxShorterHeight = 156;     

        private void SelectWhsGeneralSettingsTab()
        {
            configurationRadPageView.SelectedPage = whsGeneralSettingsRadPageViewPage;
        }

        private void InitializeWhsGeneralSettingsTabObjects()
        {
            try
            {
                InitializeTemperatureRiseOverTopOilRadGridView();
                // InitializeMaximumRatedCurrentRadGridView();
                topOilTemperatureComboBox.DataSource = new string[] { topOilTempInputNoneText, topOilTempInputRTDOneText, topOilTempInputRTDTwoText, topOilTempInputRTDThreeText, topOilTempInputRTDFourText, topOilTempInputRTDFiveText, topOilTempInputRTDSixText, topOilTempInputRTDSevenText };
                agingCalculationMethodRadGroupBox.Text = agingMethodGroupBoxHeaderText;
                hotSpotFactorLabelText.Text = hotSpotFactorLabelTextLabel;
                NumberOfCoolingGroupsComboBox.DataSource = new string[] { coolingGroupsZero, coolingGroupsOne, coolingGroupsTwo, coolingGroupsThree };
                NumberofFanGroupsLabel.Text = numberOfCoolingGroupsLabelText;
                lowCoolingAlarmlabel.Text = lowCoolingAlarmLabelText;
                highCoolingAlarmLabel.Text = highCoolingAlarmLabelText;
                coolingAlarmGroupBox.Text = coolingCurrentAlarmGroupBoxText;
                TopOilGroupBox.Text = topOilGroupBoxHeader;
                
                radButton5_Click(null, null);
                updateErrorButton_Click(null, null);
                // this.transformerConfigurationRadDropDownList.DataSource = new String[] { transformerConfigurationThreeSinglePhaseText, transformerConfigurationSingleThreePhaseText };
                // this.transformerTypeRadDropDownList.DataSource = new String[] { transformerTypeAutoText, transformerTypeStandardText };
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        // enabled/disbaled
                        if (enableMonitoringRadRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                        {
                            this.ratingsBitEncodingAsIntegerArray[15] = 1;
                        }
                        else
                        {
                            this.ratingsBitEncodingAsIntegerArray[15] = 0;
                        }

                        this.ratingsBitEncodingAsIntegerArray[14] = 0; // 3 phase txf
                        this.ratingsBitEncodingAsIntegerArray[13] = 0;  //std txf

                        WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration();
                        // WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration();
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration contains null member objects";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteConfigurationDataToWhsGeneralSettingsInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                string bitString;
                // enabled/disabled
                if (windingHotSpotSetup.FirmwareVersion > 0)
                {
                    this.firmwareVersionValueRadLabel.Text = Math.Round(windingHotSpotSetup.FirmwareVersion / 100.0, 2).ToString();
                }
                else
                {
                    this.firmwareVersionValueRadLabel.Text = firmwareVersionUnknownText;
                }

                bitString = windingHotSpotSetup.RatingAsBitString;
                if (bitString[15] == '1')
                {
                    enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
                }
                else
                {
                    enableMonitoringRadRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off;
                }
               
                AddDataToTemperatureRiseOverTopOilRadGridView(windingHotSpotSetup);
                // AddDataToMaximumRatedCurrentRadGridView(windingHotSpotSetup);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteWhsGeneralSettingsInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableWhsGeneralSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.enableMonitoringRadRadioButton.Enabled = true;
                this.disableMonitoringRadRadioButton.Enabled = true;

                this.transformerConfigurationRadDropDownList.Enabled = true;
                this.transformerTypeRadDropDownList.Enabled = true;
                this.temperatureRiseOverTopOilRadGridView.ReadOnly = false;
                this.maximumRatedCurrentRadGridView.ReadOnly = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableWhsGeneralSettingsTabConfigurationEditObjects()
        {
            try
            {
                this.enableMonitoringRadRadioButton.Enabled = false;
                this.disableMonitoringRadRadioButton.Enabled = false;

                this.transformerConfigurationRadDropDownList.Enabled = false;
                this.transformerTypeRadDropDownList.Enabled = false;
                this.temperatureRiseOverTopOilRadGridView.ReadOnly = true;
                this.maximumRatedCurrentRadGridView.ReadOnly = true;             
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableWhsGeneralSettingsTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool ErrorIsPresentInAWhsGeneralSettingsTabObject()
        {
            /// The values for the maximum rated current are not consistent for all voltages.  The high voltage case only allows
            /// 5K Amps for the maximum current, while the low voltage case allows for 50K Amps.  This makes sense since the ratio of the 
            /// voltage to current is inversely proportional.
            bool errorIsPresent = false;
            try
            {
                int currentRating;
                if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                {
                    if (this.maximumRatedCurrentRadGridView.Rows.Count == 9)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (Int32.TryParse(this.maximumRatedCurrentRadGridView.Rows[i].Cells[1].Value.ToString(), out currentRating))
                            {
                                if (currentRating > 5000)
                                {
                                    errorIsPresent = true;
                                }
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nFailed to convert this.maximumRatedCurrentRadGridView.Rows[" + i.ToString() + "].Cells[1].Value to an integer, value was " + this.maximumRatedCurrentRadGridView.Rows[i].Cells[1].Value.ToString();
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                                errorIsPresent = true;
                            }
                            if (errorIsPresent)
                            {
                                SelectWhsGeneralSettingsTab();
                                this.maximumRatedCurrentRadGridView.Rows[i].Cells[1].IsSelected = true;
                                RadMessageBox.Show(this, maximumRatedCurrentForHighVoltageOutOfRangeText);
                                break;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nIncorrect number of rows in this.maximumRatedCurrentRadGridView.  Expected 9, found " + this.maximumRatedCurrentRadGridView.Rows.Count.ToString();
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                {
                    if (this.maximumRatedCurrentRadGridView.Rows.Count == 3)
                    {
                        if (Int32.TryParse(this.maximumRatedCurrentRadGridView.Rows[0].Cells[1].Value.ToString(), out currentRating))
                        {
                            if (currentRating > 5000)
                            {
                                errorIsPresent = true;
                            }
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nIncorrect number of rows in this.maximumRatedCurrentRadGridView.  Expected 3, found " + this.maximumRatedCurrentRadGridView.Rows.Count.ToString();
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                    if (errorIsPresent)
                    {
                        SelectWhsGeneralSettingsTab();
                        this.maximumRatedCurrentRadGridView.Rows[0].Cells[1].IsSelected = true;
                        RadMessageBox.Show(this, maximumRatedCurrentForHighVoltageOutOfRangeText);
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nBad value in this.transformerConfigurationRadDropDownList";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.ErrorIsPresentInAWhsGeneralSettingsTabObject()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return errorIsPresent;
        }

        private void InitializeTemperatureRiseOverTopOilRadGridView()
        {
            try
            {
                this.temperatureRiseOverTopOilRadGridView.Columns.Clear();

                GridViewTextBoxColumn windingNameGridViewTextBoxColumn = new GridViewTextBoxColumn("WindingName");
                windingNameGridViewTextBoxColumn.Name = "WindingName";
                windingNameGridViewTextBoxColumn.HeaderText = windingText;
                windingNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                windingNameGridViewTextBoxColumn.Width = 80;
                windingNameGridViewTextBoxColumn.AllowSort = false;
                windingNameGridViewTextBoxColumn.IsPinned = true;
                windingNameGridViewTextBoxColumn.ReadOnly = true;
                windingNameGridViewTextBoxColumn.AllowResize = false;

                GridViewDecimalColumn temperatureRiseGridViewDecimalColumn = new GridViewDecimalColumn("TemperatureRiseOverTopOil");
                temperatureRiseGridViewDecimalColumn.Name = "TemperatureRiseOverTopOil";
                temperatureRiseGridViewDecimalColumn.HeaderText = temperatureRiseOverTopOilText;
                temperatureRiseGridViewDecimalColumn.Minimum = 1;
                temperatureRiseGridViewDecimalColumn.Maximum = 50;
                temperatureRiseGridViewDecimalColumn.DecimalPlaces = 1;
                temperatureRiseGridViewDecimalColumn.Step = .1M;
                temperatureRiseGridViewDecimalColumn.DisableHTMLRendering = false;
                temperatureRiseGridViewDecimalColumn.Width = 80;
                temperatureRiseGridViewDecimalColumn.AllowSort = false;
                temperatureRiseGridViewDecimalColumn.AllowResize = false;
                temperatureRiseGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn maximumRatedCurrentGridViewDecimalColumn = new GridViewDecimalColumn("MaximumRatedCurrent");
                maximumRatedCurrentGridViewDecimalColumn.Name = "MaximumRatedCurrent";
                maximumRatedCurrentGridViewDecimalColumn.HeaderText = maximumRatedCurrentText;
                maximumRatedCurrentGridViewDecimalColumn.DecimalPlaces = 0;
                maximumRatedCurrentGridViewDecimalColumn.Step = 1;
                maximumRatedCurrentGridViewDecimalColumn.Minimum = 1;
                maximumRatedCurrentGridViewDecimalColumn.Maximum = 50000;
                maximumRatedCurrentGridViewDecimalColumn.DisableHTMLRendering = false;
                maximumRatedCurrentGridViewDecimalColumn.Width = 80;
                maximumRatedCurrentGridViewDecimalColumn.AllowSort = false;
                maximumRatedCurrentGridViewDecimalColumn.AllowResize = false;
                maximumRatedCurrentGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn windingExponentGridViewDecimalColumn = new GridViewDecimalColumn("WindingExponent");
                windingExponentGridViewDecimalColumn.Name = "WindingExponent";
                windingExponentGridViewDecimalColumn.HeaderText = windingExponentHeaderText;
                windingExponentGridViewDecimalColumn.DecimalPlaces = 1;
                windingExponentGridViewDecimalColumn.Step = .1M;
                windingExponentGridViewDecimalColumn.Minimum = 0.5M;
                windingExponentGridViewDecimalColumn.Maximum = 2;
                windingExponentGridViewDecimalColumn.DisableHTMLRendering = false;
                windingExponentGridViewDecimalColumn.Width = 80;
                windingExponentGridViewDecimalColumn.AllowSort = false;
                windingExponentGridViewDecimalColumn.AllowResize = false;
                windingExponentGridViewDecimalColumn.WrapText = true;

                GridViewDecimalColumn windingTimeConstantGridViewDecimalColumn = new GridViewDecimalColumn("TimeConstant");
                windingTimeConstantGridViewDecimalColumn.Name = "TimeConstant";
                windingTimeConstantGridViewDecimalColumn.HeaderText = windingTimeConstantHeaderText;
                windingTimeConstantGridViewDecimalColumn.DecimalPlaces = 1;
                windingTimeConstantGridViewDecimalColumn.Step = .1M;
                windingTimeConstantGridViewDecimalColumn.Minimum = 0.5M;
                windingTimeConstantGridViewDecimalColumn.Maximum = 20;
                windingTimeConstantGridViewDecimalColumn.DisableHTMLRendering = false;
                windingTimeConstantGridViewDecimalColumn.Width = 80;
                windingTimeConstantGridViewDecimalColumn.AllowSort = false;
                windingTimeConstantGridViewDecimalColumn.AllowResize = false;
                windingTimeConstantGridViewDecimalColumn.WrapText = true;

                GridViewComboBoxColumn inputGridViewComboBoxColumn = new GridViewComboBoxColumn("Input");
                inputGridViewComboBoxColumn.Name = "Input";
                inputGridViewComboBoxColumn.HeaderText = inputText;
                inputGridViewComboBoxColumn.DataSource = new string[] { currentInputNoneText, currentInputIAText, currentInputIBText, currentInputICText };
                inputGridViewComboBoxColumn.DisableHTMLRendering = false;
                inputGridViewComboBoxColumn.Width = 80;
                inputGridViewComboBoxColumn.AllowSort = false;
                inputGridViewComboBoxColumn.AllowResize = false;
                inputGridViewComboBoxColumn.WrapText = true;

                GridViewDecimalColumn windingPreviousAgeingDecimalColumn = new GridViewDecimalColumn("PreviousAging");
                windingPreviousAgeingDecimalColumn.Name = "PreviousAging";
                windingPreviousAgeingDecimalColumn.HeaderText = windingPreviousAgingHeaderText;
                windingPreviousAgeingDecimalColumn.DecimalPlaces = 1;
                windingPreviousAgeingDecimalColumn.Step = 1;
                windingPreviousAgeingDecimalColumn.Minimum = 0;
                windingPreviousAgeingDecimalColumn.Maximum = 65000;
                windingPreviousAgeingDecimalColumn.DisableHTMLRendering = false;
                windingPreviousAgeingDecimalColumn.Width = 80;
                windingPreviousAgeingDecimalColumn.AllowSort = false;
                windingPreviousAgeingDecimalColumn.AllowResize = false;
                windingPreviousAgeingDecimalColumn.WrapText = true;

                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingNameGridViewTextBoxColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(temperatureRiseGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(maximumRatedCurrentGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingExponentGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingTimeConstantGridViewDecimalColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(inputGridViewComboBoxColumn);
                this.temperatureRiseOverTopOilRadGridView.MasterTemplate.Columns.Add(windingPreviousAgeingDecimalColumn);

                this.temperatureRiseOverTopOilRadGridView.TableElement.TableHeaderHeight = 50;
                this.temperatureRiseOverTopOilRadGridView.AllowColumnReorder = false;
                this.temperatureRiseOverTopOilRadGridView.AllowColumnChooser = false;
                this.temperatureRiseOverTopOilRadGridView.ShowGroupPanel = false;
                this.temperatureRiseOverTopOilRadGridView.EnableGrouping = false;
                this.temperatureRiseOverTopOilRadGridView.AllowAddNewRow = false;
                this.temperatureRiseOverTopOilRadGridView.AllowDeleteRow = false;
                this.temperatureRiseOverTopOilRadGridView.MultiSelect = true;
                this.temperatureRiseOverTopOilRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
                this.temperatureRiseOverTopOilRadGridView.AutoSize = true;
                // set 
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeTemperatureRiseOverTopOilRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void AddDataToTemperatureRiseOverTopOilRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            string bitString;
            try
            {
                this.temperatureRiseOverTopOilRadGridView.Rows.Clear();

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseAText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseA / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseA),
                    (decimal)(windingHotSpotSetup.Exponent / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseA / 4));

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseBText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseB / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseB),
                    (decimal)(windingHotSpotSetup.Exponent_B / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant_B / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseB / 4));

                this.temperatureRiseOverTopOilRadGridView.Rows.Add(highVoltagePhaseCText,
                    (decimal)(windingHotSpotSetup.H_RatedHotSpotRisePhaseC / 10M),
                    (decimal)(windingHotSpotSetup.H_MaximumRatedCurrentPhaseC),
                    (decimal)(windingHotSpotSetup.Exponent_C / 10M),
                    (decimal)(windingHotSpotSetup.WindingTimeConstant_C / 10M),
                    (GetCurrentInputRegisterNameFromRegisterNumber(windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber)),
                    (decimal)(windingHotSpotSetup.whsPreviousAgingPhaseC / 4));

                // aging selection
                bitString = windingHotSpotSetup.RatingAsBitString;
                if (bitString[11] == '0')
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 0;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIECText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 1;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEESixtyFiveText;
                    }
                }
                else
                {
                    if (bitString[12] == '0')
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 2;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodIEEEFiftyFiveText;
                    }
                    else
                    {
                        this.agingCalculationMethodRadDropDownList.SelectedIndex = 3;
                        this.agingCalculationMethodRadDropDownList.Text = agingCalculationMethodNomexText;
                    }
                }

                if (bitString[10] == '1')
                {
                    this.EnableFanCurrentMonitoring.Checked = true;
                }
                else
                {
                    this.EnableFanCurrentMonitoring.Checked = false;
                    
                }

                // hot spot Factor
                this.hotSpotFactorSpinEditor.Value = windingHotSpotSetup.HotSpotFactor / 100M;
                
                // top oil temp reference
               // System.Diagnostics.Debugger.Break();
                this.topOilTemperatureComboBox.Text = GetTopOilTemperatureInputRegisterNameFromRegisterNumber(windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber);
               
                // do fan currents
                this.fanCurrent1SpinEditor.Value = (windingHotSpotSetup.fan_1BaseCurrent / 10M);
                this.fanCurrent2SpinEditor.Value = (windingHotSpotSetup.fan_2BaseCurrent / 10M);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToTemperatureRiseOverTopOilRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()
        { 
            try
            {
                RadMessageBox.ThemeName = "Office2007Black";
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        //rated hotspot
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[1].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[1].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.H_RatedHotSpotRisePhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[1].Value.ToString()) * 10.0, 0));
                    
                        // max current
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[2].Value.ToString()), 0));
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[2].Value.ToString()), 0));
                        this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[2].Value.ToString()), 0));

                        //exponents
                        this.workingConfiguration.windingHotSpotSetup.Exponent = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[3].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.Exponent_B = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[3].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.Exponent_C = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[3].Value.ToString()) * 10.0, 0));

                        // time constants
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[4].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant_B = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[4].Value.ToString()) * 10.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.WindingTimeConstant_C = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[4].Value.ToString()) * 10.0, 0));

                        //current inputs
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseASourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[5].Value.ToString());
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseBSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[5].Value.ToString());
                        this.workingConfiguration.windingHotSpotSetup.H_LoadCurrentPhaseCSourceRegisterNumber = GetCurrentInputRegisterNumberFromRegisterName(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[5].Value.ToString());

                        //whs previous aging
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseA = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[0].Cells[6].Value.ToString()) * 4.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseB = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[1].Cells[6].Value.ToString()) * 4.0, 0));
                        this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseC = (Int32)(Math.Round(Double.Parse(this.temperatureRiseOverTopOilRadGridView.Rows[2].Cells[6].Value.ToString()) * 4.0, 0));
                    
                        // hot spot factor
                        this.workingConfiguration.windingHotSpotSetup.HotSpotFactor = (Int16)((hotSpotFactorSpinEditor.Value * 100M));

                        // aging model
                        if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIECText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEESixtyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 0;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodIEEEFiftyFiveText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 0;
                        }
                        else if (this.agingCalculationMethodRadDropDownList.Text.CompareTo(agingCalculationMethodNomexText) == 0)
                        {
                            this.ratingsBitEncodingAsIntegerArray[11] = 1;
                            this.ratingsBitEncodingAsIntegerArray[12] = 1;
                        }
                        
                        //top oil temp input
                        //System.Diagnostics.Debugger.Break();
                        this.workingConfiguration.windingHotSpotSetup.TopOilTempPhaseASourceRegisterNumber = GetTopOilTemperatureInputRegisterNumberFromRegisterName(this.topOilTemperatureComboBox.Text);
                    
                        // write fan currents
                        
                        this.workingConfiguration.windingHotSpotSetup.fan_1BaseCurrent = (Int16)(fanCurrent1SpinEditor.Value * 10M);
                        this.workingConfiguration.windingHotSpotSetup.fan_2BaseCurrent = (Int16)(fanCurrent2SpinEditor.Value * 10M);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration had some null members";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteTemperatureRiseOverTopOilRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void InitializeMaximumRatedCurrentRadGridView()
        {
            try
            {
                this.maximumRatedCurrentRadGridView.Columns.Clear();

                GridViewTextBoxColumn windingNameGridViewTextBoxColumn = new GridViewTextBoxColumn("WindingName");
                windingNameGridViewTextBoxColumn.Name = "WindingName";
                windingNameGridViewTextBoxColumn.HeaderText = windingText;
                windingNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                windingNameGridViewTextBoxColumn.Width = 120;
                windingNameGridViewTextBoxColumn.AllowSort = false;
                windingNameGridViewTextBoxColumn.IsPinned = true;
                windingNameGridViewTextBoxColumn.ReadOnly = true;
                windingNameGridViewTextBoxColumn.AllowResize = false;

                this.maximumRatedCurrentRadGridView.MasterTemplate.Columns.Add(windingNameGridViewTextBoxColumn);
                //  this.maximumRatedCurrentRadGridView.MasterTemplate.Columns.Add(maximumRatedCurrentGridViewDecimalColumn);

                this.maximumRatedCurrentRadGridView.TableElement.TableHeaderHeight = 50;
                this.maximumRatedCurrentRadGridView.AllowColumnReorder = false;
                this.maximumRatedCurrentRadGridView.AllowColumnChooser = false;
                this.maximumRatedCurrentRadGridView.ShowGroupPanel = false;
                this.maximumRatedCurrentRadGridView.EnableGrouping = false;
                this.maximumRatedCurrentRadGridView.AllowAddNewRow = false;
                this.maximumRatedCurrentRadGridView.AllowDeleteRow = false;
                this.maximumRatedCurrentRadGridView.MultiSelect = true;
                this.maximumRatedCurrentRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.InitializeMaximumRatedCurrentRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                this.maximumRatedCurrentRadGridView.Rows.Clear();

                if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                {
                    this.maximumRatedCurrentRadGroupBox.Height = maximumRatedCurrentRadGroupBoxNormalHeight;

                    this.maximumRatedCurrentRadGridView.Rows.Add(highVoltagePhaseAText, windingHotSpotSetup.H_MaximumRatedCurrentPhaseA);
                    this.maximumRatedCurrentRadGridView.Rows.Add(highVoltagePhaseBText, windingHotSpotSetup.H_MaximumRatedCurrentPhaseB);
                    this.maximumRatedCurrentRadGridView.Rows.Add(highVoltagePhaseCText, windingHotSpotSetup.H_MaximumRatedCurrentPhaseC);

                    // this.maximumRatedCurrentRadGridView.Rows.Add(lowVoltagePhaseAText, windingHotSpotSetup.X_MaximumRatedCurrentPhaseA);
                    //  this.maximumRatedCurrentRadGridView.Rows.Add(lowVoltagePhaseBText, windingHotSpotSetup.X_MaximumRatedCurrentPhaseB);
                    //  this.maximumRatedCurrentRadGridView.Rows.Add(lowVoltagePhaseCText, windingHotSpotSetup.X_MaximumRatedCurrentPhaseC);

                    if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeAutoText) == 0)
                    {
                        // this.maximumRatedCurrentRadGridView.Rows.Add(commonPhaseAText, windingHotSpotSetup.C_MaximumRatedCurrentPhaseA);
                        // this.maximumRatedCurrentRadGridView.Rows.Add(commonPhaseBText, windingHotSpotSetup.C_MaximumRatedCurrentPhaseB);
                        // this.maximumRatedCurrentRadGridView.Rows.Add(commonPhaseCText, windingHotSpotSetup.C_MaximumRatedCurrentPhaseC);
                    }
                    else if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeStandardText) == 0)
                    {
                        // this.maximumRatedCurrentRadGridView.Rows.Add(tertiaryVoltagePhaseAText, windingHotSpotSetup.Y_MaximumRatedCurrentPhaseA);
                        //  this.maximumRatedCurrentRadGridView.Rows.Add(tertiaryVoltagePhaseBText, windingHotSpotSetup.Y_MaximumRatedCurrentPhaseB);
                        //  this.maximumRatedCurrentRadGridView.Rows.Add(tertiaryVoltagePhaseCText, windingHotSpotSetup.Y_MaximumRatedCurrentPhaseC);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nBad entry in this.transformerTypeRadDropDownList";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                {
                    this.maximumRatedCurrentRadGroupBox.Height = maximumRatedCurrentRadGroupBoxShorterHeight;

                    this.maximumRatedCurrentRadGridView.Rows.Add(highVoltageText, windingHotSpotSetup.H_MaximumRatedCurrentPhaseA);
                    // this.maximumRatedCurrentRadGridView.Rows.Add(lowVoltageText, windingHotSpotSetup.X_MaximumRatedCurrentPhaseA);

                    if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeAutoText) == 0)
                    {
                        //  this.maximumRatedCurrentRadGridView.Rows.Add(commonText, windingHotSpotSetup.C_MaximumRatedCurrentPhaseA);
                    }
                    else if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeStandardText) == 0)
                    {
                        //this.maximumRatedCurrentRadGridView.Rows.Add(tertiaryVoltageText, windingHotSpotSetup.Y_MaximumRatedCurrentPhaseA);
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nBad entry in this.transformerTypeRadDropDownList";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nBad entry in this.transformerConfigurationRadDropDownList";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.AddDataToMaximumRatedCurrentRadGridView(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()
        {
            try
            {
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationThreeSinglePhaseText) == 0)
                        {
                            this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[0].Cells[1].Value.ToString());
                            this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseB = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[1].Cells[1].Value.ToString());
                            this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseC = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[2].Cells[1].Value.ToString());

                            //  this.workingConfiguration.windingHotSpotSetup.X_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[3].Cells[1].Value.ToString());
                            //  this.workingConfiguration.windingHotSpotSetup.X_MaximumRatedCurrentPhaseB = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[4].Cells[1].Value.ToString());
                            //  this.workingConfiguration.windingHotSpotSetup.X_MaximumRatedCurrentPhaseC = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[5].Cells[1].Value.ToString());

                            if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeAutoText) == 0)
                            {
                                //  this.workingConfiguration.windingHotSpotSetup.C_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[6].Cells[1].Value.ToString());
                                //  this.workingConfiguration.windingHotSpotSetup.C_MaximumRatedCurrentPhaseB = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[7].Cells[1].Value.ToString());
                                //  this.workingConfiguration.windingHotSpotSetup.C_MaximumRatedCurrentPhaseC = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[8].Cells[1].Value.ToString());
                            }
                            else if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeStandardText) == 0)
                            {
                                //  this.workingConfiguration.windingHotSpotSetup.Y_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[6].Cells[1].Value.ToString());
                                // this.workingConfiguration.windingHotSpotSetup.Y_MaximumRatedCurrentPhaseB = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[7].Cells[1].Value.ToString());
                                // this.workingConfiguration.windingHotSpotSetup.Y_MaximumRatedCurrentPhaseC = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[8].Cells[1].Value.ToString());
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nBad entry in this.transformerTypeRadDropDownList";
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                            }
                        }
                        else if (this.transformerConfigurationRadDropDownList.Text.CompareTo(transformerConfigurationSingleThreePhaseText) == 0)
                        {
                            this.workingConfiguration.windingHotSpotSetup.H_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[0].Cells[1].Value.ToString());
                            // this.workingConfiguration.windingHotSpotSetup.X_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[1].Cells[1].Value.ToString());

                            if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeAutoText) == 0)
                            {
                                // this.workingConfiguration.windingHotSpotSetup.C_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[2].Cells[1].Value.ToString());
                            }
                            else if (this.transformerTypeRadDropDownList.Text.CompareTo(transformerTypeStandardText) == 0)
                            {
                                // this.workingConfiguration.windingHotSpotSetup.Y_MaximumRatedCurrentPhaseA = Int32.Parse(this.maximumRatedCurrentRadGridView.Rows[2].Cells[1].Value.ToString());
                            }
                            else
                            {
                                string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nBad entry in this.transformerTypeRadDropDownList";
                                LogMessage.LogError(errorMessage);
                                #if DEBUG
                                MessageBox.Show(errorMessage);
                                #endif
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nBad entry in this.transformerConfigurationRadDropDownList";
                            LogMessage.LogError(errorMessage);
                            #if DEBUG
                            MessageBox.Show(errorMessage);
                            #endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration contained at least one null member";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteMaximumRatedCurrentRadGridViewValuesToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }
    }
}