﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;
using DatabaseInterface;

namespace MainConfigurationLite
{
    public partial class Main_WHSMonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private void InitializeAlarmsAndFansTabObjects()
        {
            UpdateErrorListBox();
            radButton5_Click(null, null);
        }

        private void WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()
        {
            try
            {
                
                if (this.workingConfiguration != null)
                {
                    if (this.workingConfiguration.AllConfigurationMembersAreNonNull())
                    {
                        this.workingConfiguration.windingHotSpotSetup.AlarmDeadBandTemperature = (int)this.deadBandTemperatureRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmDelayInSeconds = (int)this.alarmDelayRadSpinEditor.Value;                       

                        this.workingConfiguration.windingHotSpotSetup.AlarmMaxSetPointTemperature_H = (int)this.whsLowAlarmSetPointRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmMaxSetPointTemperature_HH = (int)this.whsHighAlarmSetPointRadSpinEditor.Value;                       
                        this.workingConfiguration.windingHotSpotSetup.AlarmTopOilTemperature_H = (int)this.topOilTempLowAlarmSetPointRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.AlarmTopOilTemperature_HH = (int)this.topOilTempHighAlarmSetPointRadSpinEditor.Value;
                        
                        this.workingConfiguration.windingHotSpotSetup.FanSetpoint_H = (int)this.fanBankOneStartTempWhsRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.FanSetpoint_HH = (int)this.fanBankTwoStartTempWhsRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TopOilTemperatureFanSetpoint_H = (int)this.fanBankOneStartTempTopOilRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH = (int)this.fanBankTwoStartTempTopOilRadSpinEditor.Value;

                        this.workingConfiguration.windingHotSpotSetup.TimeBetweenFanExerciseInDays = (int)this.fanExerciseIntervalInDaysRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.TimeToExerciseFanInMinutes = (int)this.fanExerciseTimeInMinutesRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.MinimumFanRunTimeInMinutes = (int)this.minimumfanRunTimeInMinutesRadSpinEditor.Value;
                        this.workingConfiguration.windingHotSpotSetup.FanBankSwitchingTimeInHours = (int)this.fanBankSwapIntervalInHoursRadSpinEditor.Value;
                        //code updated cfk 12/17/31
                        //------------------------------------
                        this.workingConfiguration.windingHotSpotSetup.FanOneRuntimeInHours = Convert.ToInt16(fan1RuntimeHoursSpinEditor.Value / 3.0M);
                        this.workingConfiguration.windingHotSpotSetup.FanTwoRuntimeInHours = Convert.ToInt16(fan2RuntimeHoursSpinEditor.Value / 3.0M);
                        this.workingConfiguration.windingHotSpotSetup.whsFan1NumberOfStarts = Convert.ToInt16(this.fan1NumOfStartsSpinEditor.Value);
                        this.workingConfiguration.windingHotSpotSetup.whsFan2NumberOfStarts = Convert.ToInt16(this.fan2NumOfStartsSpinEdito.Value);
                        this.workingConfiguration.windingHotSpotSetup.NumberOfCoolingGroups = Convert.ToInt16(this.NumberOfCoolingGroupsComboBox.SelectedIndex);
                        this.workingConfiguration.windingHotSpotSetup.fan_lowLimitAlarm = Convert.ToInt16(Convert.ToDouble(this.lowCoolingAlarmSpinEditor.Value) * 100.0);
                        this.workingConfiguration.windingHotSpotSetup.fan_highLimitAlarm = Convert.ToInt16(Convert.ToDouble(this.HighCoolingAlarmSpinEditor.Value) * 100.0);
                       
                        
                        // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseA = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseA.Text) * 4.0);
                       // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseB = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseB.Text) * 4.0);
                       // this.workingConfiguration.windingHotSpotSetup.whsPreviousAgingPhaseC = Convert.ToInt16(Convert.ToDouble(this.PreviousAgingDaysPhaseC.Text) * 4.0);
                        //-------------------------------------------------------
                        
                        return;
                    }
                    else
                    {
                        string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration contained at least on null member";
                        LogMessage.LogError(errorMessage);
                        #if DEBUG
                        MessageBox.Show(errorMessage);
                        #endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nthis.workingConfiguration was null";
                    LogMessage.LogError(errorMessage);
                    #if DEBUG
                    MessageBox.Show(errorMessage);
                    #endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteAlarmsAndFansInterfaceDataToWorkingConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
            return;
        }

        private void WriteConfigurationDataToAlarmsAndFansInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup windingHotSpotSetup)
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.Value = windingHotSpotSetup.AlarmDeadBandTemperature;
                this.alarmDelayRadSpinEditor.Value = windingHotSpotSetup.AlarmDelayInSeconds;
                this.whsLowAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmMaxSetPointTemperature_H;
                this.whsHighAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmMaxSetPointTemperature_HH;
                this.topOilTempLowAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmTopOilTemperature_H;
                this.topOilTempHighAlarmSetPointRadSpinEditor.Value = windingHotSpotSetup.AlarmTopOilTemperature_HH;
               
                this.fanBankOneStartTempWhsRadSpinEditor.Value = windingHotSpotSetup.FanSetpoint_H;
                this.fanBankTwoStartTempWhsRadSpinEditor.Value = windingHotSpotSetup.FanSetpoint_HH;
                this.fanBankOneStartTempTopOilRadSpinEditor.Value = windingHotSpotSetup.TopOilTemperatureFanSetpoint_H;
                this.fanBankTwoStartTempTopOilRadSpinEditor.Value = windingHotSpotSetup.TopOilTemperatureFanSetpoint_HH;

                this.fanExerciseIntervalInDaysRadSpinEditor.Value = windingHotSpotSetup.TimeBetweenFanExerciseInDays;
                this.fanExerciseTimeInMinutesRadSpinEditor.Value = windingHotSpotSetup.TimeToExerciseFanInMinutes;

                this.minimumfanRunTimeInMinutesRadSpinEditor.Value = windingHotSpotSetup.MinimumFanRunTimeInMinutes;

                this.fanBankSwapIntervalInHoursRadSpinEditor.Value = windingHotSpotSetup.FanBankSwitchingTimeInHours;
                // code added cfk 12/17/13
                this.fan1RuntimeHoursSpinEditor.Value = (windingHotSpotSetup.FanOneRuntimeInHours * 3.0M);
                this.fan2RuntimeHoursSpinEditor.Value = (windingHotSpotSetup.FanTwoRuntimeInHours * 3.0M);
                this.fan1NumOfStartsSpinEditor.Value = windingHotSpotSetup.whsFan1NumberOfStarts;
                this.fan2NumOfStartsSpinEdito.Value = windingHotSpotSetup.whsFan2NumberOfStarts;
                this.fanBankSwapIntervalInHoursRadSpinEditor.Value = windingHotSpotSetup.FanBankSwitchingTimeInHours;
                this.NumberOfCoolingGroupsComboBox.SelectedIndex = windingHotSpotSetup.NumberOfCoolingGroups;
                this.lowCoolingAlarmSpinEditor.Value = windingHotSpotSetup.fan_lowLimitAlarm / 100M;
                this.HighCoolingAlarmSpinEditor.Value = windingHotSpotSetup.fan_highLimitAlarm / 100M;

               
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.WriteConfigurationDataToAlarmsAndFansInterfaceObjects(Main_ConfigComponent_WindingHotSpotSetup)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void EnableAlarmsAndFansTabConfigurationEditObjects()
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.ReadOnly = false;
                this.alarmDelayRadSpinEditor.ReadOnly = false;
                this.whsLowAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.whsHighAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ReadOnly = false;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ReadOnly = false;

                this.fanBankOneStartTempWhsRadSpinEditor.ReadOnly = false;
                this.fanBankOneStartTempTopOilRadSpinEditor.ReadOnly = false;
                this.fanBankTwoStartTempWhsRadSpinEditor.ReadOnly = false;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ReadOnly = false;
                this.fanExerciseIntervalInDaysRadSpinEditor.ReadOnly = false;
                this.fanExerciseTimeInMinutesRadSpinEditor.ReadOnly = false;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ReadOnly = false;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ReadOnly = false;

                this.deadBandTemperatureRadSpinEditor.ShowUpDownButtons = true;
                this.alarmDelayRadSpinEditor.ShowUpDownButtons = true;
                this.whsLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.whsHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = true;

                this.fanBankOneStartTempWhsRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankOneStartTempTopOilRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankTwoStartTempWhsRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ShowUpDownButtons = true;
                this.fanExerciseIntervalInDaysRadSpinEditor.ShowUpDownButtons = true;
                this.fanExerciseTimeInMinutesRadSpinEditor.ShowUpDownButtons = true;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ShowUpDownButtons = true;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ShowUpDownButtons = true;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.EnableAlarmsAndFansTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private void DisableAlarmsAndFansTabConfigurationEditObjects()
        {
            try
            {
                this.deadBandTemperatureRadSpinEditor.ReadOnly = true;
                this.alarmDelayRadSpinEditor.ReadOnly = true;
                this.whsLowAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.whsHighAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ReadOnly = true;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ReadOnly = true;

                this.fanBankOneStartTempWhsRadSpinEditor.ReadOnly = true;
                this.fanBankOneStartTempTopOilRadSpinEditor.ReadOnly = true;
                this.fanBankTwoStartTempWhsRadSpinEditor.ReadOnly = true;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ReadOnly = true;
                this.fanExerciseIntervalInDaysRadSpinEditor.ReadOnly = true;
                this.fanExerciseTimeInMinutesRadSpinEditor.ReadOnly = true;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ReadOnly = true;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ReadOnly = true;

                this.deadBandTemperatureRadSpinEditor.ShowUpDownButtons = false;
                this.alarmDelayRadSpinEditor.ShowUpDownButtons = false;
                this.whsLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.whsHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.topOilTempLowAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;
                this.topOilTempHighAlarmSetPointRadSpinEditor.ShowUpDownButtons = false;

                this.fanBankOneStartTempWhsRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankOneStartTempTopOilRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankTwoStartTempWhsRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankTwoStartTempTopOilRadSpinEditor.ShowUpDownButtons = false;
                this.fanExerciseIntervalInDaysRadSpinEditor.ShowUpDownButtons = false;
                this.fanExerciseTimeInMinutesRadSpinEditor.ShowUpDownButtons = false;
                this.minimumfanRunTimeInMinutesRadSpinEditor.ShowUpDownButtons = false;
                this.fanBankSwapIntervalInHoursRadSpinEditor.ShowUpDownButtons = false;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_WHSMonitorConfiguration.DisableAlarmsAndFansTabObjects()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
                #if DEBUG
                MessageBox.Show(errorMessage);
                #endif
            }
        }

        private bool ErrorIsPresentInAnAlarmsAndFansTabObject()
        {
            return false;
        }
    }
}
