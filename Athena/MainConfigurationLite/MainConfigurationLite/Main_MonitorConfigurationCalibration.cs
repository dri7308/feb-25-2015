﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using GeneralUtilities;
using ConfigurationObjects;
using MonitorInterface;

namespace MainConfigurationLite
{
    public partial class Main_MonitorConfiguration : Telerik.WinControls.UI.RadForm
    {
        private static string transferParameterNameCalibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Transfer<br>Parameter Name</html>";
        private static string nullLineCalibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Null Line</html>";
        private static string multiplierCalibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Multiplier</html>";
        private static string offsetCalibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Offset</html>";
        private static string channelNoiseCalibrationGridViewText = "<html><font=Microsoft Sans Serif><size=8.25>Channel Noise</html>";

        private static string vibrationCalibrationGridViewText = "Vibration";
        private static string pressureCalibrationGridViewText = "Analog In";
        private static string currentCalibrationGridViewText = "Current";
        private static string voltageCalibrationGridViewText = "Voltage";
        private static string humidityCalibrationGridViewText = "Humidity";
        private static string temperatureCalibrationGridViewText = "Temperature";
        private static string chassisTemperatureCalibrationGridViewText = "Chassis Temperature";

        private void SelectCalibrationTab()
        {
            configurationRadPageView.SelectedPage = calibrationRadPageViewPage;
        }

        private void DisableCalibrationTabConfigurationEditObjects()
        {
            this.calibrationRadGridView.ReadOnly = true;
        }

        private void EnableCalibrationTabConfigurationEditObjects()
        {
            this.calibrationRadGridView.ReadOnly = false;
        }

        private void InitializeCalibrationRadGridView()
        {
            try
            {
                this.calibrationRadGridView.MasterTemplate.Columns.Clear();

                GridViewTextBoxColumn parameterNameGridViewTextBoxColumn = new GridViewTextBoxColumn("ParameterName");
                parameterNameGridViewTextBoxColumn.Name = "ParameterName";
                parameterNameGridViewTextBoxColumn.HeaderText = transferParameterNameCalibrationGridViewText;
                parameterNameGridViewTextBoxColumn.DisableHTMLRendering = false;
                parameterNameGridViewTextBoxColumn.Width = 130;
                parameterNameGridViewTextBoxColumn.AllowSort = false;
                parameterNameGridViewTextBoxColumn.IsPinned = true;
                parameterNameGridViewTextBoxColumn.ReadOnly = true;

                GridViewDecimalColumn nullLineGridViewDecimalColumn = new GridViewDecimalColumn("NullLine");
                nullLineGridViewDecimalColumn.Name = "NullLine";
                nullLineGridViewDecimalColumn.HeaderText = nullLineCalibrationGridViewText;
                nullLineGridViewDecimalColumn.DecimalPlaces = 0;
                nullLineGridViewDecimalColumn.DisableHTMLRendering = false;
                nullLineGridViewDecimalColumn.Width = 101;
                nullLineGridViewDecimalColumn.AllowSort = false;
                nullLineGridViewDecimalColumn.Minimum = 0;
                nullLineGridViewDecimalColumn.Maximum = 32767;

                GridViewDecimalColumn multiplierAGridViewDecimalColumn = new GridViewDecimalColumn("Multiplier");
                multiplierAGridViewDecimalColumn.Name = "Multiplier";
                multiplierAGridViewDecimalColumn.HeaderText = multiplierCalibrationGridViewText;
                multiplierAGridViewDecimalColumn.DecimalPlaces = 5;
                multiplierAGridViewDecimalColumn.DisableHTMLRendering = false;
                multiplierAGridViewDecimalColumn.Width = 101;
                multiplierAGridViewDecimalColumn.AllowSort = false;
               // multiplierAGridViewDecimalColumn.Minimum = 0;
                // multiplierAGridViewDecimalColumn.Maximum = 10000;

                GridViewDecimalColumn offsetGridViewDecimalColumn = new GridViewDecimalColumn("Offset");
                offsetGridViewDecimalColumn.Name = "Offset";
                offsetGridViewDecimalColumn.HeaderText = offsetCalibrationGridViewText;
                offsetGridViewDecimalColumn.DecimalPlaces = 5;
                offsetGridViewDecimalColumn.DisableHTMLRendering = false;
                offsetGridViewDecimalColumn.Width = 101;
                offsetGridViewDecimalColumn.AllowSort = false;
               // offsetGridViewDecimalColumn.Minimum = 0;
                // offsetGridViewDecimalColumn.Maximum = 10000;

                GridViewDecimalColumn channelNoiseGridViewDecimalColumn = new GridViewDecimalColumn("ChannelNoise");
                channelNoiseGridViewDecimalColumn.Name = "ChannelNoise";
                channelNoiseGridViewDecimalColumn.HeaderText = channelNoiseCalibrationGridViewText;
                channelNoiseGridViewDecimalColumn.DecimalPlaces = 0;
                channelNoiseGridViewDecimalColumn.DisableHTMLRendering = false;
                channelNoiseGridViewDecimalColumn.Width = 101;
                channelNoiseGridViewDecimalColumn.AllowSort = false;
                channelNoiseGridViewDecimalColumn.Minimum = 0;
                channelNoiseGridViewDecimalColumn.Maximum = 32767;

                this.calibrationRadGridView.MasterTemplate.Columns.Add(parameterNameGridViewTextBoxColumn);
                this.calibrationRadGridView.MasterTemplate.Columns.Add(nullLineGridViewDecimalColumn);
                this.calibrationRadGridView.MasterTemplate.Columns.Add(multiplierAGridViewDecimalColumn);
                this.calibrationRadGridView.MasterTemplate.Columns.Add(offsetGridViewDecimalColumn);
                this.calibrationRadGridView.MasterTemplate.Columns.Add(channelNoiseGridViewDecimalColumn);

                this.calibrationRadGridView.TableElement.TableHeaderHeight = this.tableHeaderHeight;
                this.calibrationRadGridView.AllowColumnReorder = false;
                this.calibrationRadGridView.AllowColumnChooser = false;
                this.calibrationRadGridView.ShowGroupPanel = false;
                this.calibrationRadGridView.EnableGrouping = false;
                this.calibrationRadGridView.AllowAddNewRow = false;
                this.calibrationRadGridView.AllowDeleteRow = false;
                this.calibrationRadGridView.MultiSelect = true;
                this.calibrationRadGridView.SelectionMode = GridViewSelectionMode.FullRowSelect;
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.InitializeCalibrationRadGridView()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(string parameterType, int numberOfParameters)
        {
            try
            {
                if (parameterType != null)
                {
                    if (parameterType.Length != 0)
                    {
                        if (numberOfParameters > 1)
                        {
                            for (int i = 0; i < numberOfParameters; i++)
                            {
                                this.calibrationRadGridView.Rows.Add(parameterType + " " + (i + 1).ToString(), 0, 0, 0, 0);
                            }
                        }
                        else
                        {
                            this.calibrationRadGridView.Rows.Add(parameterType, 0, 0, 0, 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(string, int)\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddEmptyRowsToTheCalibrationGrid()
        {
            try
            {
                this.calibrationRadGridView.Rows.Clear();

                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(vibrationCalibrationGridViewText, 4);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(pressureCalibrationGridViewText, 6);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(currentCalibrationGridViewText, 3);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(voltageCalibrationGridViewText, 3);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(humidityCalibrationGridViewText, 1);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(temperatureCalibrationGridViewText, 7);
                AddEmptyRowsForOneParamaterTypeToTheCalibrationGrid(chassisTemperatureCalibrationGridViewText, 1);

                this.calibrationRadGridView.TableElement.ScrollToRow(0);
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddEmptyRowsToTheSystemConfigurationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void AddDataToTheCalibrationGrid(List<Main_ConfigComponent_CalibrationData> calibrationDataList)
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_CalibrationData calibrationData;

                if (calibrationDataList != null)
                {
                    if (calibrationDataList.Count > 24)
                    {
                        if (this.calibrationRadGridView.Rows.Count > 24)
                        {
                            for (int i = 0; i < 25; i++)
                            {
                                rowInfo = this.calibrationRadGridView.Rows[i];
                                calibrationData = calibrationDataList[i];

                                if (rowInfo.Cells.Count > 4)
                                {
                                    rowInfo.Cells[1].Value = calibrationData.ZeroLine;
                                    rowInfo.Cells[2].Value = (Math.Round(calibrationData.Multiplier, 5));
                                    rowInfo.Cells[3].Value = (Math.Round(calibrationData.Offset, 5));
                                    rowInfo.Cells[4].Value = calibrationData.ChannelNoise;
                                }
                                else
                                {
                                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCalibrationGrid()\nGrid row had too few cells.";
                                    LogMessage.LogError(errorMessage);
#if DEBUG
                                    MessageBox.Show(errorMessage);
#endif
                                }
                            }
                        }
                        else
                        {
                            string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCalibrationGrid()\nthis.calibrationRadGridView had too few rows.";
                            LogMessage.LogError(errorMessage);
#if DEBUG
                            MessageBox.Show(errorMessage);
#endif
                        }
                    }
                    else
                    {
                        string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCalibrationGrid()\ncalibrationDataList had too few entries.";
                        LogMessage.LogError(errorMessage);
#if DEBUG
                        MessageBox.Show(errorMessage);
#endif
                    }
                }
                else
                {
                    string errorMessage = "Error in Main_MonitorConfiguration.AddDataToTheCalibrationGrid()\ncalibrationDataList was null.";
                    LogMessage.LogError(errorMessage);
#if DEBUG
                    MessageBox.Show(errorMessage);
#endif
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.AddDataToTheCalibrationGrid()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }

        private void WriteCalibrationTabDataToCurrentConfiguration()
        {
            try
            {
                GridViewRowInfo rowInfo;
                Main_ConfigComponent_CalibrationData calibrationData;

                if (this.currentConfiguration != null)
                {
                    if (this.currentConfiguration.calibrationDataConfigObjects != null)
                    {
                        if (this.currentConfiguration.calibrationDataConfigObjects.Count > 24)
                        {
                            if (this.calibrationRadGridView.Rows.Count > 24)
                            {
                                for (int i = 0; i < 25; i++)
                                {
                                    rowInfo = this.calibrationRadGridView.Rows[i];
                                    calibrationData = this.currentConfiguration.calibrationDataConfigObjects[i];
                                    if (rowInfo.Cells.Count > 4)
                                    {
                                        Int32.TryParse(rowInfo.Cells[1].Value.ToString(), out calibrationData.ZeroLine);
                                        Double.TryParse(rowInfo.Cells[2].Value.ToString(), out calibrationData.Multiplier);
                                        Double.TryParse(rowInfo.Cells[3].Value.ToString(), out calibrationData.Offset);
                                        Int32.TryParse(rowInfo.Cells[4].Value.ToString(), out calibrationData.ChannelNoise);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Exception thrown in Main_MonitorConfiguration.WriteCalibrationTabDataToCurrentConfiguration()\nMessage: " + ex.Message;
                LogMessage.LogError(errorMessage);
#if DEBUG
                MessageBox.Show(errorMessage);
#endif
            }
        }
    }
}
